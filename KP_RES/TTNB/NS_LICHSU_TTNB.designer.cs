﻿namespace KP_RES
{
    partial class NS_LICHSU_TTNB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupCDSKHTT = new DevExpress.XtraEditors.GroupControl();
            this.gvLichSuDiem = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayNop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LYDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.bnt_dong = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bnt_hienthitongquan = new System.Windows.Forms.ToolStripMenuItem();
            this.bnt_hienthichitiet = new System.Windows.Forms.ToolStripMenuItem();
            this.bnt_hienthichitiettheotp = new System.Windows.Forms.ToolStripMenuItem();
            this.bnt_hienthichitiet_theoqh = new System.Windows.Forms.ToolStripMenuItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtSoDuTaiKhoan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtNgaySinh = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtSDT = new DevExpress.XtraEditors.TextEdit();
            this.txt_MaKHTT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCmnd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cbGioiTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel2 = new DevExpress.XtraEditors.PanelControl();
            this.palButton = new DevExpress.XtraEditors.PanelControl();
            this.btn_Close = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupCDSKHTT)).BeginInit();
            this.groupCDSKHTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvLichSuDiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDuTaiKhoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_MaKHTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCmnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).BeginInit();
            this.palButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1285, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // groupCDSKHTT
            // 
            this.groupCDSKHTT.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupCDSKHTT.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupCDSKHTT.AppearanceCaption.Options.UseFont = true;
            this.groupCDSKHTT.AppearanceCaption.Options.UseForeColor = true;
            this.groupCDSKHTT.Controls.Add(this.gvLichSuDiem);
            this.groupCDSKHTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupCDSKHTT.Location = new System.Drawing.Point(2, 108);
            this.groupCDSKHTT.Name = "groupCDSKHTT";
            this.groupCDSKHTT.ShowCaption = false;
            this.groupCDSKHTT.Size = new System.Drawing.Size(1281, 354);
            this.groupCDSKHTT.TabIndex = 57;
            this.groupCDSKHTT.Text = "Lịch Sử Cộng Trừ Điểm";
            // 
            // gvLichSuDiem
            // 
            this.gvLichSuDiem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvLichSuDiem.Location = new System.Drawing.Point(2, 2);
            this.gvLichSuDiem.MainView = this.gridView1;
            this.gvLichSuDiem.MenuManager = this.barManager1;
            this.gvLichSuDiem.Name = "gvLichSuDiem";
            this.gvLichSuDiem.Size = new System.Drawing.Size(1277, 350);
            this.gvLichSuDiem.TabIndex = 1;
            this.gvLichSuDiem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 35;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENNHANVIEN,
            this.SoTien,
            this.TENLOAI,
            this.NgayNop,
            this.LYDO,
            this.gridColumn1});
            this.gridView1.GridControl = this.gvLichSuDiem;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 35;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 44;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Tên Nhân Viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 4;
            this.TENNHANVIEN.Width = 283;
            // 
            // SoTien
            // 
            this.SoTien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SoTien.AppearanceCell.Options.UseFont = true;
            this.SoTien.AppearanceCell.Options.UseTextOptions = true;
            this.SoTien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SoTien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SoTien.AppearanceHeader.Options.UseFont = true;
            this.SoTien.AppearanceHeader.Options.UseTextOptions = true;
            this.SoTien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoTien.Caption = "Số Tiền";
            this.SoTien.DisplayFormat.FormatString = "#,0";
            this.SoTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SoTien.FieldName = "SoTien";
            this.SoTien.MinWidth = 100;
            this.SoTien.Name = "SoTien";
            this.SoTien.OptionsColumn.AllowEdit = false;
            this.SoTien.OptionsColumn.AllowFocus = false;
            this.SoTien.OptionsColumn.FixedWidth = true;
            this.SoTien.Visible = true;
            this.SoTien.VisibleIndex = 3;
            this.SoTien.Width = 137;
            // 
            // TENLOAI
            // 
            this.TENLOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENLOAI.AppearanceCell.Options.UseFont = true;
            this.TENLOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAI.AppearanceHeader.Options.UseFont = true;
            this.TENLOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAI.Caption = "Tên Loại";
            this.TENLOAI.FieldName = "TENLOAI";
            this.TENLOAI.Name = "TENLOAI";
            this.TENLOAI.OptionsColumn.AllowEdit = false;
            this.TENLOAI.OptionsColumn.AllowFocus = false;
            this.TENLOAI.OptionsColumn.FixedWidth = true;
            this.TENLOAI.Visible = true;
            this.TENLOAI.VisibleIndex = 2;
            this.TENLOAI.Width = 150;
            // 
            // NgayNop
            // 
            this.NgayNop.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NgayNop.AppearanceCell.Options.UseFont = true;
            this.NgayNop.AppearanceCell.Options.UseTextOptions = true;
            this.NgayNop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayNop.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NgayNop.AppearanceHeader.Options.UseFont = true;
            this.NgayNop.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayNop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayNop.Caption = "Ngày Giao Dịch";
            this.NgayNop.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm:ss";
            this.NgayNop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayNop.FieldName = "NgayNop";
            this.NgayNop.Name = "NgayNop";
            this.NgayNop.OptionsColumn.AllowEdit = false;
            this.NgayNop.OptionsColumn.AllowFocus = false;
            this.NgayNop.OptionsColumn.FixedWidth = true;
            this.NgayNop.Visible = true;
            this.NgayNop.VisibleIndex = 1;
            this.NgayNop.Width = 155;
            // 
            // LYDO
            // 
            this.LYDO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LYDO.AppearanceCell.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LYDO.AppearanceHeader.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Options.UseTextOptions = true;
            this.LYDO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LYDO.Caption = "Lý Do";
            this.LYDO.FieldName = "LYDO";
            this.LYDO.Name = "LYDO";
            this.LYDO.OptionsColumn.AllowEdit = false;
            this.LYDO.OptionsColumn.AllowFocus = false;
            this.LYDO.OptionsColumn.FixedWidth = true;
            this.LYDO.Visible = true;
            this.LYDO.VisibleIndex = 5;
            this.LYDO.Width = 400;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "MAKH";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bnt_dong,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 8;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1285, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 508);
            this.barDockControlBottom.Size = new System.Drawing.Size(1285, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // dockManager1
            // 
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // bnt_dong
            // 
            this.bnt_dong.Caption = "&2.Đóng";
            this.bnt_dong.Id = 5;
            this.bnt_dong.Name = "bnt_dong";
            this.bnt_dong.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "&1.Cập Nhật";
            this.barButtonItem1.Id = 7;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnt_hienthitongquan,
            this.bnt_hienthichitiet,
            this.bnt_hienthichitiettheotp,
            this.bnt_hienthichitiet_theoqh});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(339, 92);
            // 
            // bnt_hienthitongquan
            // 
            this.bnt_hienthitongquan.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_hienthitongquan.Name = "bnt_hienthitongquan";
            this.bnt_hienthitongquan.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.bnt_hienthitongquan.Size = new System.Drawing.Size(338, 22);
            this.bnt_hienthitongquan.Text = "Hiển Thị Tổng Quan";
            this.bnt_hienthitongquan.Visible = false;
            // 
            // bnt_hienthichitiet
            // 
            this.bnt_hienthichitiet.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_hienthichitiet.Name = "bnt_hienthichitiet";
            this.bnt_hienthichitiet.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.bnt_hienthichitiet.Size = new System.Drawing.Size(338, 22);
            this.bnt_hienthichitiet.Text = "Hiển Thị Chi Tiết";
            this.bnt_hienthichitiet.Visible = false;
            // 
            // bnt_hienthichitiettheotp
            // 
            this.bnt_hienthichitiettheotp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_hienthichitiettheotp.Name = "bnt_hienthichitiettheotp";
            this.bnt_hienthichitiettheotp.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.T)));
            this.bnt_hienthichitiettheotp.Size = new System.Drawing.Size(338, 22);
            this.bnt_hienthichitiettheotp.Text = "Hiển Thị Chi Tiết Theo Tỉnh - TP";
            this.bnt_hienthichitiettheotp.Visible = false;
            // 
            // bnt_hienthichitiet_theoqh
            // 
            this.bnt_hienthichitiet_theoqh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_hienthichitiet_theoqh.Name = "bnt_hienthichitiet_theoqh";
            this.bnt_hienthichitiet_theoqh.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.bnt_hienthichitiet_theoqh.Size = new System.Drawing.Size(338, 22);
            this.bnt_hienthichitiet_theoqh.Text = "Hiển Thị Chi Tiết Theo Quận Huyện";
            this.bnt_hienthichitiet_theoqh.Visible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.groupCDSKHTT);
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 44);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1285, 464);
            this.groupControl1.TabIndex = 78;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtSoDuTaiKhoan);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.txtNgaySinh);
            this.panelControl2.Controls.Add(this.txtDiaChi);
            this.panelControl2.Controls.Add(this.txtSDT);
            this.panelControl2.Controls.Add(this.txt_MaKHTT);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.txtCmnd);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.cbGioiTinh);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.txtHoTen);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1281, 106);
            this.panelControl2.TabIndex = 0;
            // 
            // txtSoDuTaiKhoan
            // 
            this.txtSoDuTaiKhoan.Location = new System.Drawing.Point(893, 35);
            this.txtSoDuTaiKhoan.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSoDuTaiKhoan.Name = "txtSoDuTaiKhoan";
            this.txtSoDuTaiKhoan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDuTaiKhoan.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtSoDuTaiKhoan.Properties.Appearance.Options.UseFont = true;
            this.txtSoDuTaiKhoan.Properties.Appearance.Options.UseForeColor = true;
            this.txtSoDuTaiKhoan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoDuTaiKhoan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSoDuTaiKhoan.Properties.DisplayFormat.FormatString = "N0";
            this.txtSoDuTaiKhoan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoDuTaiKhoan.Properties.EditFormat.FormatString = "N0";
            this.txtSoDuTaiKhoan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoDuTaiKhoan.Properties.ReadOnly = true;
            this.txtSoDuTaiKhoan.Size = new System.Drawing.Size(248, 26);
            this.txtSoDuTaiKhoan.TabIndex = 15;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl13.Location = new System.Drawing.Point(759, 42);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(114, 19);
            this.labelControl13.TabIndex = 14;
            this.labelControl13.Text = "Số Dư Hiện Tại:";
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(499, 3);
            this.txtNgaySinh.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.txtNgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Properties.ReadOnly = true;
            this.txtNgaySinh.Size = new System.Drawing.Size(248, 26);
            this.txtNgaySinh.TabIndex = 7;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(499, 71);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Properties.Appearance.Options.UseFont = true;
            this.txtDiaChi.Properties.ReadOnly = true;
            this.txtDiaChi.Size = new System.Drawing.Size(642, 26);
            this.txtDiaChi.TabIndex = 11;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(893, 2);
            this.txtSDT.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDT.Properties.Appearance.Options.UseFont = true;
            this.txtSDT.Properties.ReadOnly = true;
            this.txtSDT.Size = new System.Drawing.Size(248, 26);
            this.txtSDT.TabIndex = 13;
            // 
            // txt_MaKHTT
            // 
            this.txt_MaKHTT.Location = new System.Drawing.Point(156, 3);
            this.txt_MaKHTT.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txt_MaKHTT.Name = "txt_MaKHTT";
            this.txt_MaKHTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MaKHTT.Properties.Appearance.Options.UseFont = true;
            this.txt_MaKHTT.Properties.ReadOnly = true;
            this.txt_MaKHTT.Size = new System.Drawing.Size(248, 26);
            this.txt_MaKHTT.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(6, 8);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(115, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mã Khách Hàng:";
            // 
            // txtCmnd
            // 
            this.txtCmnd.Location = new System.Drawing.Point(156, 71);
            this.txtCmnd.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtCmnd.Name = "txtCmnd";
            this.txtCmnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCmnd.Properties.Appearance.Options.UseFont = true;
            this.txtCmnd.Properties.ReadOnly = true;
            this.txtCmnd.Size = new System.Drawing.Size(248, 26);
            this.txtCmnd.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Location = new System.Drawing.Point(6, 75);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 19);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Số CMND:";
            // 
            // cbGioiTinh
            // 
            this.cbGioiTinh.EditValue = "Nữ";
            this.cbGioiTinh.Location = new System.Drawing.Point(499, 37);
            this.cbGioiTinh.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.cbGioiTinh.Name = "cbGioiTinh";
            this.cbGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.cbGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGioiTinh.Properties.ReadOnly = true;
            this.cbGioiTinh.Size = new System.Drawing.Size(248, 26);
            this.cbGioiTinh.TabIndex = 9;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl10.Location = new System.Drawing.Point(760, 8);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(106, 19);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "Số Điện Thoại:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl8.Location = new System.Drawing.Point(412, 75);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(57, 19);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "Địa Chỉ:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(412, 42);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Giới Tính:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(412, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ngày Sinh:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(156, 37);
            this.txtHoTen.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(248, 26);
            this.txtHoTen.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(6, 42);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(147, 19);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Họ Tên Khách Hàng:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(-92, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(59, 14);
            this.labelControl2.TabIndex = 75;
            this.labelControl2.Text = "Tỉnh - Tp :";
            this.labelControl2.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.palButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1285, 44);
            this.panel2.TabIndex = 60;
            this.panel2.Visible = false;
            // 
            // palButton
            // 
            this.palButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palButton.Controls.Add(this.btn_Close);
            this.palButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.palButton.Location = new System.Drawing.Point(733, 2);
            this.palButton.Name = "palButton";
            this.palButton.Size = new System.Drawing.Size(550, 40);
            this.palButton.TabIndex = 47;
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Close.Appearance.Options.UseFont = true;
            this.btn_Close.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btn_Close.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Close.Location = new System.Drawing.Point(405, 2);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(145, 35);
            this.btn_Close.TabIndex = 43;
            this.btn_Close.Text = "&Đóng";
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // NS_LICHSU_TTNB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 508);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NS_LICHSU_TTNB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NS_LICHSU_TTNB";
            ((System.ComponentModel.ISupportInitialize)(this.groupCDSKHTT)).EndInit();
            this.groupCDSKHTT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvLichSuDiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDuTaiKhoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_MaKHTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCmnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).EndInit();
            this.palButton.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupCDSKHTT;
        private DevExpress.XtraGrid.GridControl gvLichSuDiem;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn NgayNop;
        private DevExpress.XtraGrid.Columns.GridColumn SoTien;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarButtonItem bnt_dong;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bnt_hienthitongquan;
        private System.Windows.Forms.ToolStripMenuItem bnt_hienthichitiet;
        private System.Windows.Forms.ToolStripMenuItem bnt_hienthichitiettheotp;
        private System.Windows.Forms.ToolStripMenuItem bnt_hienthichitiet_theoqh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtSoDuTaiKhoan;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.TextEdit txtSDT;
        private DevExpress.XtraEditors.TextEdit txt_MaKHTT;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtCmnd;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit cbGioiTinh;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn LYDO;
        private DevExpress.XtraEditors.PanelControl panel2;
        private DevExpress.XtraEditors.PanelControl palButton;
        private DevExpress.XtraEditors.SimpleButton btn_Close;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}