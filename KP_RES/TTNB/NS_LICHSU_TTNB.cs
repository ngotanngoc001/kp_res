﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class NS_LICHSU_TTNB : DevExpress.XtraEditors.XtraForm
    {
        
        public NS_LICHSU_TTNB( string maKH)
        {
            InitializeComponent();            
                LoadDuLieu(maKH);
            //
        }
        private void LoadDuLieu(string makhachhang)
        {
            DataTable dt=new DataTable();
            string sql;
            try
            {

                sql = "select  khnb.MAKHTTNB as Ma_KHTT, TEN as HoTenKHTT,NGAYSINH as NgaySinh,(case GIOITINH when 1 then 'Nam' else N'Nữ' end) as GioiTinh,CMND as SoCMND, DIENTHOAI as DienThoaiDiDong,DIACHI as DiaChi,SoDuTaiKhoan  from NHACUNGCAP ncc inner join  KHACHHANG_THANHTOAN_NOIBO khnb on ncc.MA=khnb.Ma_KHTT where khnb.MAKHTTNB='" + makhachhang + "' and ncc.SuDung='true'  ";
                dt = clsMain.ReturnDataTable (sql);
                if (dt.Rows.Count > 0)
                {
                    txt_MaKHTT.Text = dt.Rows[0]["Ma_KHTT"].ToString();
                    txtHoTen.Text = dt.Rows[0]["HoTenKHTT"].ToString();
                    txtSDT.Text = dt.Rows[0]["DienThoaiDiDong"].ToString();
                    txtNgaySinh.Text = string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["NgaySinh"]);
                    txtDiaChi.Text = dt.Rows[0]["DiaChi"].ToString();
                    txtCmnd.Text = dt.Rows[0]["SoCMND"].ToString();
                    txtSoDuTaiKhoan.EditValue = int.Parse( dt.Rows[0]["SoDuTaiKhoan"].ToString());
                    cbGioiTinh.Text = dt.Rows[0]["GioiTinh"].ToString();
                    
                }
                else
                {
                    XtraMessageBox.Show("Không tìm thấy!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                gvLichSuDiem.DataSource = clsMain.ReturnDataTable("select MA_KH, TENNHANVIEN,SoTien,(case flag when 1 then N'Nạp Tiền' else N'Rút Tiền' end)as TENLOAI ,NgayNop,LYDO from KHTT_CHITIET_NOPTIEN ct inner join DM_NHANVIEN nv on ct.MANV=nv.MANHANVIEN where MA_KH='" + txt_MaKHTT.Text + "'");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public delegate void GetString(Int16 loai);
        public GetString MyGetData;
      

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}