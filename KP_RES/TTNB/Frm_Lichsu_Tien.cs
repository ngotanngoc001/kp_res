﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Lichsu_Tien : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Lichsu_Tien( string maKH)
        {
            InitializeComponent();
            LoadCombo();
            LoadDuLieu(maKH);
            LoaddataGridView();
        }

        private void LoadCombo()
        {
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
            dt1.Rows.Add("1", "Nam");
            dt1.Rows.Add("0", "Nữ");
            cboGioitinh.Properties.DataSource = dt1;
            cboGioitinh.EditValue = null;
        }

        private void LoadDuLieu(string makhachhang)
        {
            DataTable dt = cls_KHTT.ReturnKhachHangThanThiet(makhachhang);
            if (dt.Rows.Count > 0)
            {
                txtMaKH.Text = dt.Rows[0]["MA"].ToString();
                txtTEN.Text = dt.Rows[0]["TEN"].ToString();
                txtDIACHI.Text = dt.Rows[0]["DIACHI"].ToString();
                txtDIENTHOAI.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                txtEMAIL.Text = dt.Rows[0]["EMAIL"].ToString();
                txtWebsite.Text = dt.Rows[0]["WEBSITE"].ToString();
                chkSUDUNG.Checked = bool.Parse(dt.Rows[0]["SUDUNG"].ToString());
                txtCMND.Text = dt.Rows[0]["CMND"].ToString();
                txtSodiem.Text = dt.Rows[0]["SoDuTaiKhoan"].ToString();
                dtpNgaysinh.EditValue = DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                cboGioitinh.EditValue = bool.Parse(dt.Rows[0]["GIOITINH"].ToString()) == true ? "1" : "0";
            }
            else
            {
                XtraMessageBox.Show("Không tìm thấy!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoaddataGridView()
        {
            string sSQL = "select MA_KH, TENNHANVIEN,SOTIEN,(case FLAG when '01' then N'Nạp tiền' else N'Rút tiền' end)as TENLOAI ,NGAYNOP,LYDO from KHTT_CHITIET_NOPTIEN cd inner join DM_NHANVIEN nv on cd.MANV=nv.MANHANVIEN where MA_KH='" + txtMaKH.Text + "'";
            gvLichSuDiem.DataSource = clsMain.ReturnDataTable(sSQL);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }
    }
}