﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;

namespace KP_RES
{
    public partial class NS_DANHSACH_KHTTNB : DevExpress.XtraEditors.XtraForm
    {
        
        public NS_DANHSACH_KHTTNB()
        {
            InitializeComponent();
            //bool[] role = Database.NhanVienRole(LOGIN.CPermission.MaNV, "bt_DSCapDoKHTT");
            //bnt_xemchitiet.Enabled = role[0];
           
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (optGroup.SelectedIndex == 0 || optGroup.SelectedIndex == 4)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }
            if (optGroup.SelectedIndex == 0)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                lbGT.Visible=chkNam.Visible = chkNu.Visible = false;
            }
            else
                if (optGroup.SelectedIndex == 1)
                {
                    lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = true;
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                    lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
                }
                else
                    if (optGroup.SelectedIndex == 2)
                    {
                        lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                        lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                        lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
                    }
                    else
                        if (optGroup.SelectedIndex == 3)
                        {
                            lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                            lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                            lbGT.Visible = chkNam.Visible = chkNu.Visible = true;
                        }
                        else
                            if (optGroup.SelectedIndex == 4)
                            {
                                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                                lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
                            }
                
                   
        }

    
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                if (optGroup.SelectedIndex == 4)
                {
                    btnNapTien.Visible = btnRutTien.Visible = btnHuyKHTT.Visible = btnInMaKH.Visible = false;
                    pn1.Visible = pn2.Visible = pn3.Visible =pn4.Visible= false;
                    btn_ThemKHTT.Visible = true;
                }
                else
                {
                    btn_ThemKHTT.Visible = false;
                    pn1.Visible = pn2.Visible = pn3.Visible = pn4.Visible = true;
                    btnNapTien.Visible = btnRutTien.Visible = btnHuyKHTT.Visible =btnInMaKH.Visible= true;
                }
                string sql = "";
                if (optGroup.EditValue.Equals("TC"))
                {
                    sql = "exec sp_LIST_CUSTOMER_TTNB_SEARCH '','','','','','1'";
                }
                else
                    if (optGroup.EditValue.Equals("SDTK"))
                    {
                        if (txtTuSoDu.Text == "" || txtDenSoDu.Text == "")
                            throw new Exception("Mời nhập số tiền");
                        sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH '','','{0}','{1}','','1'", txtTuSoDu.Text.Replace(",", ""), txtDenSoDu.Text.Replace(",", ""));
                    }
                    else
                        if (optGroup.EditValue.Equals("Ngay"))
                        {

                            sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH '{0:yyyyMMdd}','{1:yyyyMMdd}','','','','1'", date_tungay.Value, date_denngay.Value);
                        }
                        else
                            if (optGroup.EditValue.Equals("GT"))
                            {
                                sql = "exec sp_LIST_CUSTOMER_TTNB_SEARCH '','','','','" + chkNam.Checked + "','1'";
                               
                            }
                            else
                                if (optGroup.EditValue.Equals("HUY"))
                                {

                                    sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH '','','','','','0'", date_tungay.Value, date_denngay.Value);
                                }
                   
                        

                gvcShowKHTTNB.DataSource = clsMain.ReturnDataTable(sql);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnHuyKHTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Chưa chọn đối tượng cần hủy");
                }
                if (gridView1.FocusedRowHandle >= 0)
                {
                    int vitri = gridView1.FocusedRowHandle;
                    if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn hủy khách hàng thanh toán nội bộ với Khách Hàng : " + gridView1.GetFocusedRowCellValue(DT_HOTEN).ToString() + " không ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        string sql = string.Format("update KHACHHANG_THANHTOAN_NOIBO set SuDung='false' where MAKHTTNB='{0}'", gridView1.GetFocusedRowCellValue(DT_DOITUONGID).ToString());
                        
                        if (clsMain.ExecuteSQL(sql)  )
                        {
                           
                            btnTimkiem_Click(sender, e);
                            throw new Exception("Hủy khách hàng thanh toán nội bộ thành công");
                        }
                        else
                            throw new Exception("Hủy khách hàng thanh toán nội bộ thất bại");
                    }
                    gridView1.FocusedRowHandle = vitri;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCong_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0)
                    throw new Exception("Không Có Khách hàng !");
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Hãy Chọn Khách Hàng Cần nạp tiền");
                }
                if (gridView1.FocusedRowHandle >= 0)
                {
                    int vitri = gridView1.FocusedRowHandle;
                    DataTable dt = clsMain.ReturnDataTable("select MAKHTTNB as DT_DOITUONGID,MAKHTTNB as Ma_KHTT,TEN as HoTenKHTT,NGAYSINH as  NgaySinh,GIOITINH as GioiTinh,CMND as SoCMND, EMAIL as Email,DIENTHOAI as DienThoaiDiDong,DIACHI as DiaChi ,SoDuTaiKhoan            from NHACUNGCAP dt inner join KHACHHANG_THANHTOAN_NOIBO kh on dt.MA=kh.Ma_KHTT where kh.SuDung='true' and kh.MAKHTTNB='" + gridView1.GetFocusedRowCellValue(DT_DOITUONGID).ToString() + "'");
                    PU_KH_THANHTOAN_NOPTIEN frm = new PU_KH_THANHTOAN_NOPTIEN(dt, true);
                    frm.ShowDialog();
                    if (frm.ActiveMdiChild == null)
                        btnTimkiem_Click(sender, e);
                    gridView1.FocusedRowHandle = vitri;
                   
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnTru_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0)
                    throw new Exception("Không Có Khách hàng !");
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Hãy Chọn Khách Hàng Cần Rút Tiền");
                }
                if (gridView1.FocusedRowHandle >= 0)
                {
                    int vitri = gridView1.FocusedRowHandle;
                    DataTable dt = clsMain.ReturnDataTable("select MAKHTTNB as DT_DOITUONGID,MAKHTTNB as Ma_KHTT, TEN as HoTenKHTT,NGAYSINH as NgaySinh,GIOITINH as GioiTinh,CMND as SoCMND, EMAIL as Email,DIENTHOAI as DienThoaiDiDong,DIACHI as DiaChi ,SoDuTaiKhoan            from NHACUNGCAP dt inner join KHACHHANG_THANHTOAN_NOIBO kh on dt.MA=kh.Ma_KHTT where kh.SuDung='true' and kh.SoDuTaiKhoan!=0  and kh.MAKHTTNB='" + gridView1.GetFocusedRowCellValue(DT_DOITUONGID).ToString() + "'");
                    if (dt.Rows.Count > 0)
                    {
                        PU_KH_THANHTOAN_NOPTIEN frm = new PU_KH_THANHTOAN_NOPTIEN(dt, false);
                        frm.ShowDialog();
                        if (frm.ActiveMdiChild == null)
                            btnTimkiem_Click(sender, e);
                    }
                    else
                    {
                        XtraMessageBox.Show("Khách hàng " + gridView1.GetFocusedRowCellValue(DT_HOTEN).ToString().ToUpper() + " có số dư bằng 0 nên không thể rút tiền được.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    gridView1.FocusedRowHandle = vitri;
                  
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gvcShowKHTTNB.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gvcShowKHTTNB.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void chkNam_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNam.Checked)
            {
                chkNu.Checked = false;
            }
            else
            {
                chkNu.Checked = true;
            }
        }

        private void chkNu_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNu.Checked)
            {
                chkNam.Checked = false;
            }
            else
            {
                chkNam.Checked = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataTable dt = (DataTable)gvcShowKHTTNB.DataSource;
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 15;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void btn_lichSu_Click(object sender, EventArgs e)
        {
            try
            {
                int vitri = gridView1.FocusedRowHandle;
                if (gridView1.RowCount > 0)
                {
                    if (gridView1.FocusedRowHandle >= 0)
                    {
                        //Frm_Lichsu frm = new Frm_Lichsu();
                        //frm.WindowState = FormWindowState.Maximized;
                        //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        //frm.Mode = 2;
                        //frm.ma = gridView1.GetFocusedRowCellValue(DT_MADOITUONG).ToString();
                        //frm.ShowDialog();
                        //frm.Dispose();
                    }
                    else
                        throw new Exception("Bạn chưa chọn khách hàng");
                }
                else
                {
                  throw new Exception( "không có khách hàng ");
                }
                gridView1.FocusedRowHandle = vitri;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void btn_ThemKHTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {

                    if (gridView1.FocusedRowHandle >= 0)
                    {
                        string sql = "update KHACHHANG_THANHTOAN_NOIBO set  SuDung=1 where MAKHTTNB='" + gridView1.GetFocusedRowCellValue(DT_MADOITUONG).ToString() + "'";
                        if (clsMain.ExecuteSQL(sql))
                        {
                            btnTimkiem_Click(sender, e);
                            throw new Exception("Thêm khách hàng thành công");
                        }
                        else
                            throw new Exception("Thêm khách hàng thất bại");
                    }
                    else
                    {
                        throw new Exception("Bạn Chưa Chọn Khách Hàng nào.");
                    }
                }
                else
                {
                    throw new Exception("Không có khách hàng nào!");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnInMaKH_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    if (gridView1.FocusedRowHandle >= 0)
                    {
                        
                        DataTable dt = new DataTable();
                        dt.Columns.Add("MAKH", typeof(string));
                        dt.Columns.Add("TENKH", typeof(string));
                        dt.Columns.Add("NGAYSINH", typeof(string));
                        DataRow row = dt.NewRow();
                        row["MAKH"] =string.Format("{0:00000000}", gridView1.GetFocusedRowCellValue(DT_MADOITUONG));
                        row["TENKH"] = gridView1.GetFocusedRowCellValue(DT_HOTEN);
                        row["NGAYSINH"] =string.Format("{0:dd/MM/yyyy}", gridView1.GetFocusedRowCellValue(DT_NGAYSINH));
                        dt.Rows.Add(row);
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dt;
                        frm.Mode = 20;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        throw new Exception("Bạn chưa chọn khách hàng nào!");
                    }
                }
                else
                {
                    throw new Exception("Không có khách hàng nào!");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

       
    }
}