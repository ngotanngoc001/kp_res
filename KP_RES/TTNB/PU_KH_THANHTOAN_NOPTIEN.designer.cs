﻿namespace KP_RES
{
    partial class PU_KH_THANHTOAN_NOPTIEN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_MaKHTT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtSoTienNap = new DevExpress.XtraEditors.TextEdit();
            this.txtChu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTienRut = new DevExpress.XtraEditors.TextEdit();
            this.lbTra = new DevExpress.XtraEditors.LabelControl();
            this.lbNap = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtSoDu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtNgaySinh = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtSDT = new DevExpress.XtraEditors.TextEdit();
            this.txtCmnd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cbGioiTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_luu = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_dong = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.txt_MaKHTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienNap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienRut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCmnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_MaKHTT
            // 
            this.txt_MaKHTT.Location = new System.Drawing.Point(146, 40);
            this.txt_MaKHTT.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txt_MaKHTT.Name = "txt_MaKHTT";
            this.txt_MaKHTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MaKHTT.Properties.Appearance.Options.UseFont = true;
            this.txt_MaKHTT.Properties.ReadOnly = true;
            this.txt_MaKHTT.Size = new System.Drawing.Size(147, 26);
            this.txt_MaKHTT.TabIndex = 11;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(16, 75);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(106, 19);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "Số Điện Thoại:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(319, 75);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(57, 19);
            this.labelControl8.TabIndex = 6;
            this.labelControl8.Text = "Địa Chỉ:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(16, 40);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(115, 19);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Mã Khách Hàng:";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.txtSoTienNap);
            this.groupControl1.Controls.Add(this.txtChu);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtSoTienRut);
            this.groupControl1.Controls.Add(this.lbTra);
            this.groupControl1.Controls.Add(this.lbNap);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 250);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(824, 92);
            this.groupControl1.TabIndex = 53;
            this.groupControl1.Text = "Chọn số tiền";
            // 
            // txtSoTienNap
            // 
            this.txtSoTienNap.Location = new System.Drawing.Point(146, 31);
            this.txtSoTienNap.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSoTienNap.Name = "txtSoTienNap";
            this.txtSoTienNap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienNap.Properties.Appearance.Options.UseFont = true;
            this.txtSoTienNap.Size = new System.Drawing.Size(670, 26);
            this.txtSoTienNap.TabIndex = 17;
            this.txtSoTienNap.EditValueChanged += new System.EventHandler(this.txtSoTienNap_EditValueChanged);
            this.txtSoTienNap.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoTienNap_KeyUp);
            // 
            // txtChu
            // 
            this.txtChu.Location = new System.Drawing.Point(146, 62);
            this.txtChu.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtChu.Name = "txtChu";
            this.txtChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtChu.Properties.Appearance.Options.UseFont = true;
            this.txtChu.Properties.ReadOnly = true;
            this.txtChu.Size = new System.Drawing.Size(670, 26);
            this.txtChu.TabIndex = 16;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(16, 66);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(107, 19);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Viết Bằng Chữ:";
            // 
            // txtSoTienRut
            // 
            this.txtSoTienRut.Location = new System.Drawing.Point(146, 31);
            this.txtSoTienRut.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSoTienRut.Name = "txtSoTienRut";
            this.txtSoTienRut.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienRut.Properties.Appearance.Options.UseFont = true;
            this.txtSoTienRut.Size = new System.Drawing.Size(670, 26);
            this.txtSoTienRut.TabIndex = 11;
            this.txtSoTienRut.EditValueChanged += new System.EventHandler(this.txtSoTienTra_EditValueChanged);
            this.txtSoTienRut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSoTienTra_KeyPress);
            this.txtSoTienRut.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoTienTra_KeyUp);
            // 
            // lbTra
            // 
            this.lbTra.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTra.Location = new System.Drawing.Point(16, 36);
            this.lbTra.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.lbTra.Name = "lbTra";
            this.lbTra.Size = new System.Drawing.Size(89, 19);
            this.lbTra.TabIndex = 10;
            this.lbTra.Text = "Số Tiền Rút:";
            // 
            // lbNap
            // 
            this.lbNap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNap.Location = new System.Drawing.Point(16, 36);
            this.lbNap.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.lbNap.Name = "lbNap";
            this.lbNap.Size = new System.Drawing.Size(93, 19);
            this.lbNap.TabIndex = 0;
            this.lbNap.Text = "Số Tiền Nạp:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(319, 40);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(147, 19);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Họ Tên Khách Hàng:";
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl2.Controls.Add(this.txtSoDu);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.txtNgaySinh);
            this.groupControl2.Controls.Add(this.txtDiaChi);
            this.groupControl2.Controls.Add(this.txtSDT);
            this.groupControl2.Controls.Add(this.txt_MaKHTT);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.txtCmnd);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.cbGioiTinh);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.txtHoTen);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 36);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(824, 214);
            this.groupControl2.TabIndex = 51;
            this.groupControl2.Text = "Thông Tin Khách Hàng";
            // 
            // txtSoDu
            // 
            this.txtSoDu.Location = new System.Drawing.Point(470, 142);
            this.txtSoDu.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSoDu.Name = "txtSoDu";
            this.txtSoDu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDu.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtSoDu.Properties.Appearance.Options.UseFont = true;
            this.txtSoDu.Properties.Appearance.Options.UseForeColor = true;
            this.txtSoDu.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoDu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSoDu.Properties.ReadOnly = true;
            this.txtSoDu.Size = new System.Drawing.Size(346, 26);
            this.txtSoDu.TabIndex = 18;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(319, 145);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(114, 19);
            this.labelControl13.TabIndex = 17;
            this.labelControl13.Text = "Số Dư Hiện Tại:";
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(146, 108);
            this.txtNgaySinh.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.txtNgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Properties.ReadOnly = true;
            this.txtNgaySinh.Size = new System.Drawing.Size(147, 26);
            this.txtNgaySinh.TabIndex = 16;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(470, 74);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Properties.Appearance.Options.UseFont = true;
            this.txtDiaChi.Properties.ReadOnly = true;
            this.txtDiaChi.Size = new System.Drawing.Size(346, 26);
            this.txtDiaChi.TabIndex = 14;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(146, 74);
            this.txtSDT.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDT.Properties.Appearance.Options.UseFont = true;
            this.txtSDT.Properties.ReadOnly = true;
            this.txtSDT.Size = new System.Drawing.Size(147, 26);
            this.txtSDT.TabIndex = 12;
            // 
            // txtCmnd
            // 
            this.txtCmnd.Location = new System.Drawing.Point(146, 142);
            this.txtCmnd.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtCmnd.Name = "txtCmnd";
            this.txtCmnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCmnd.Properties.Appearance.Options.UseFont = true;
            this.txtCmnd.Properties.ReadOnly = true;
            this.txtCmnd.Size = new System.Drawing.Size(147, 26);
            this.txtCmnd.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(16, 145);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Số CMND:";
            // 
            // cbGioiTinh
            // 
            this.cbGioiTinh.EditValue = "Nữ";
            this.cbGioiTinh.Location = new System.Drawing.Point(470, 108);
            this.cbGioiTinh.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.cbGioiTinh.Name = "cbGioiTinh";
            this.cbGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.cbGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGioiTinh.Properties.ReadOnly = true;
            this.cbGioiTinh.Size = new System.Drawing.Size(346, 26);
            this.cbGioiTinh.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(319, 110);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Giới Tính:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 110);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ngày Sinh:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(470, 40);
            this.txtHoTen.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(346, 26);
            this.txtHoTen.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.bnt_luu);
            this.panelControl1.Controls.Add(this.bnt_dong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 342);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(824, 47);
            this.panelControl1.TabIndex = 49;
            // 
            // bnt_luu
            // 
            this.bnt_luu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luu.Appearance.Options.UseFont = true;
            this.bnt_luu.Image = global::KP_RES.Properties.Resources.save_26;
            this.bnt_luu.Location = new System.Drawing.Point(588, 6);
            this.bnt_luu.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.bnt_luu.Name = "bnt_luu";
            this.bnt_luu.Size = new System.Drawing.Size(108, 36);
            this.bnt_luu.TabIndex = 0;
            this.bnt_luu.Text = "&1.Lưu";
            this.bnt_luu.Click += new System.EventHandler(this.bnt_luu_Click);
            // 
            // bnt_dong
            // 
            this.bnt_dong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_dong.Appearance.Options.UseFont = true;
            this.bnt_dong.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bnt_dong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.bnt_dong.Location = new System.Drawing.Point(708, 6);
            this.bnt_dong.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.bnt_dong.Name = "bnt_dong";
            this.bnt_dong.Size = new System.Drawing.Size(108, 36);
            this.bnt_dong.TabIndex = 2;
            this.bnt_dong.Text = "&2.Đóng";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_700x30;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(824, 36);
            this.panel1.TabIndex = 50;
            // 
            // PU_KH_THANHTOAN_NOPTIEN
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 389);
            this.ControlBox = false;
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "PU_KH_THANHTOAN_NOPTIEN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nạp Tiền Vào Thẻ Thanh Toán Nội Bộ";
            ((System.ComponentModel.ISupportInitialize)(this.txt_MaKHTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienNap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTienRut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCmnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txt_MaKHTT;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lbNap;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtCmnd;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit cbGioiTinh;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.SimpleButton bnt_luu;
        private DevExpress.XtraEditors.SimpleButton bnt_dong;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtSDT;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.TextEdit txtSoDu;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtSoTienRut;
        private DevExpress.XtraEditors.LabelControl lbTra;
        private DevExpress.XtraEditors.TextEdit txtChu;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtSoTienNap;
    }
}