﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Congtrutien : DevExpress.XtraEditors.XtraForm
    {
        bool _flag;
        string _maKH;
        public Frm_Congtrutien(string maKH,bool flag)
        {
            InitializeComponent();
            _flag = flag;
            _maKH = maKH;
            DataTable dtKH = cls_KHTT .ReturnKhachHangThanThiet (maKH);
            txtMaKH.Text = dtKH.Rows[0]["MA"].ToString();
            txtTenKhachHang.Text = dtKH.Rows[0]["TEN"].ToString();
            txtDiem.Text = dtKH.Rows[0]["SoDuTaiKhoan"].ToString();
            if (_flag)
            {
                this.Text = "NẠP TIỀN VÀO THẺ THANH TOÁN NỘI BỘ";
                lbDiem.Text = "Số Tiền Nạp";
                lbLyDo.Text = "Lý Do";
                txtLyDo.Text = "Nạp tiền vào tài khoản ";
            }
            else
            {
                this.Text = "RÚT TIỀN TỪ THẺ THANH TOÁN NỘI BỘ";
                lbDiem.Text = "Số Tiền Rút";
                lbLyDo.Text = "Lý Do";
                txtLyDo.Text = "Rút tiền khỏi tài khoản ";
            }
        }

        private void bnt_dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            if (txtSoDiem.Text.Trim() == "")
            {
                txtSoDiem.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Số Tiền ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return ;
            }
            if (txtLyDo.Text.Trim() == "")
            {
                txtLyDo.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Lý Do ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return ;
            }

            try
            {
                int soThuongNho = 0;
                int soLanTinh = 0;
                int soThuongLon = 0;
                if (_flag)//nạp tiền
                {
                    bool  bSQL = cls_KHTT.CongTien_TTTNB(_maKH,decimal .Parse ( txtSoDiem.Text.Replace(",", "")), txtLyDo.Text);
                    if (bSQL )
                    {
                        XtraMessageBox.Show("Đã Nạp Tiền Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else//rút tiền
                {
                    int diemht = int.Parse(txtDiem.Text .Replace (",",""));
                    if (int.Parse(txtSoDiem.Text) > diemht)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Số Tiền Rút Không Thể Lớn Hơn Số Dư Hiện Tại ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    bool bSQL = cls_KHTT.TruTien_TTTNB(_maKH, decimal.Parse(txtSoDiem.Text.Replace(",", "")), txtLyDo.Text);
                    if (bSQL)
                    {
                        XtraMessageBox.Show("Đã Rút Tiền Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}