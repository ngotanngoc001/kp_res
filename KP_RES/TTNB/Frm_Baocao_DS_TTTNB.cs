﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;

namespace KP_RES
{
    public partial class Frm_Baocao_DS_TTTNB : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_DS_TTTNB()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (optGroup.SelectedIndex == 0 || optGroup.SelectedIndex == 4)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }

            if (optGroup.SelectedIndex == 0)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
            }
            else if (optGroup.SelectedIndex == 1)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = true;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
            }
            else if (optGroup.SelectedIndex == 2)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
            }
            else if (optGroup.SelectedIndex == 3)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                lbGT.Visible = chkNam.Visible = chkNu.Visible = true;
            }
            else if (optGroup.SelectedIndex == 4)
            {
                lbTuSoDu.Visible = lbDenSoDu.Visible = txtTuSoDu.Visible = txtDenSoDu.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                lbGT.Visible = chkNam.Visible = chkNu.Visible = false;
            }
        }
    
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "";
                if (optGroup.EditValue.Equals("TC"))
                {
                    sql = "exec sp_LIST_CUSTOMER_TTNB_SEARCH_VE '','','','','','1'";
                }
                else if (optGroup.EditValue.Equals("SDTK"))
                {
                    if (txtTuSoDu.Text == "" || txtDenSoDu.Text == "")
                        throw new Exception("Mời nhập số tiền");
                    sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH_VE '','','{0}','{1}','','1'", txtTuSoDu.Text.Replace(",", ""), txtDenSoDu.Text.Replace(",", ""));
                }
                else if (optGroup.EditValue.Equals("Ngay"))
                {

                    sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH_VE '{0:yyyyMMdd}','{1:yyyyMMdd}','','','','1'", date_tungay.Value, date_denngay.Value);
                }
                else if (optGroup.EditValue.Equals("GT"))
                {
                    sql = "exec sp_LIST_CUSTOMER_TTNB_SEARCH_VE '','','','','" + chkNam.Checked + "','1'";

                }
                else if (optGroup.EditValue.Equals("HUY"))
                {

                    sql = string.Format("exec sp_LIST_CUSTOMER_TTNB_SEARCH_VE '','','','','','0'", date_tungay.Value, date_denngay.Value);
                }
                gvcShowKHTTNB.DataSource = clsMain.ReturnDataTable(sql);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gvcShowKHTTNB.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gvcShowKHTTNB.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void chkNam_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNam.Checked)
            {
                chkNu.Checked = false;
            }
            else
            {
                chkNu.Checked = true;
            }
        }

        private void chkNu_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNu.Checked)
            {
                chkNam.Checked = false;
            }
            else
            {
                chkNam.Checked = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    //DataTable dt = (DataTable)gvcShowKHTTNB.DataSource;
                    //Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    //frm.WindowState = FormWindowState.Maximized;
                    //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    //frm.dtSource = dt;
                    //frm.Mode = 29;
                    //frm.ShowDialog();
                    //frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void btn_lichSu_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    int vitri = gridView1.FocusedRowHandle;
            //    if (gridView1.RowCount > 0)
            //    {
            //        if (gridView1.FocusedRowHandle >= 0)
            //        {
            //            Frm_Lichsu frm = new Frm_Lichsu();
            //            frm.WindowState = FormWindowState.Maximized;
            //            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            frm.Mode = 2;
            //            frm.ma = gridView1.GetFocusedRowCellValue(MAKHTTNB).ToString();
            //            frm.ShowDialog();
            //            frm.Dispose();
            //        }
            //        else
            //            throw new Exception("Bạn chưa chọn khách hàng");
            //    }
            //    else
            //    {
            //      throw new Exception( "không có khách hàng ");
            //    }
            //    gridView1.FocusedRowHandle = vitri;
            //}
            //catch (Exception ex)
            //{
            //    XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

            try
            {
                if (gridView1.RowCount > 0)
                {
                    if (gridView1.FocusedRowHandle >= 0)
                    {
                        int vitri = gridView1.FocusedRowHandle;
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.Mode = 61;
                        frm.ma = gridView1.GetFocusedRowCellValue(MAKHTTNB).ToString();
                        frm.ShowDialog();
                        frm.Dispose();
                        gridView1.FocusedRowHandle = vitri;
                    }
                    else
                    {
                        throw new Exception("Bạn Chưa Chọn Khách Hàng nào.");
                    }
                }
                else
                {
                    throw new Exception("Không có khác hàng nào!");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}