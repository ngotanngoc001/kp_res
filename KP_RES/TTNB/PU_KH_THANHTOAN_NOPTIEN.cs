﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
namespace KP_RES
{
    public partial class PU_KH_THANHTOAN_NOPTIEN : DevExpress.XtraEditors.XtraForm
    {
        DataTable _dt;
        bool _flag; //biến kiểm tra trả hay nạp tiền true:Nạp,false trả
        public PU_KH_THANHTOAN_NOPTIEN(DataTable dt,bool flag)
        {
            InitializeComponent();
            _dt = dt;
            _flag = flag;
            LoadDuLieu(dt);  
            if (flag)
            {
                lbTra.Visible =txtSoTienRut.Visible=  false;
                lbNap.Visible =txtSoTienNap.Visible=  true;
                txtSoTienNap.Focus();
            }
            else
            {
                lbTra.Visible =txtSoTienRut.Visible=  true;
                lbNap.Visible = txtSoTienNap.Visible=false;
                txtSoTienRut.Focus();
            }
           // txtSoTienRut.Focus();        
        }
    
        private string ChuyenSo(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48]+ " đồng";
            string c = doc[0].ToString();

            doc = doc.Remove(0, 1);
            doc = doc.Insert(0, c.ToUpper());

            return doc + "đồng";
        }
        private void LoadDuLieu(DataTable dt)
        {
          

            txt_MaKHTT.Text = dt.Rows[0]["Ma_KHTT"].ToString();

            txtHoTen.Text = dt.Rows[0]["HoTenKHTT"].ToString();

            if (bool.Parse(dt.Rows[0]["GioiTinh"].ToString()) == false)
                cbGioiTinh.Text = "Nữ";
            else
                cbGioiTinh.Text = "Nam";
            txtCmnd.Text = dt.Rows[0]["SoCMND"].ToString();
            txtSDT.Text = dt.Rows[0]["DienThoaiDiDong"].ToString();
            txtDiaChi.Text = dt.Rows[0]["DiaChi"].ToString();
            txtNgaySinh.Text =string.Format("{0:dd/MM/yyyy}",dt.Rows[0]["NgaySinh"]);
            txtSoDu.Text =string.Format("{0:#,0}", dt.Rows[0]["SoDuTaiKhoan"]);
            txtChu.Text = "";
        }
       
       
        private void bnt_luu_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sql="";
                if (_flag)//nạp tiền
                {
                    if (txtSoTienNap.Text == "" || txtSoTienNap.Text == "0")
                        throw new Exception("Mời Bạn Nhập Số Tiền Nạp");
                    int diemcong;
                    DataTable dtdiem = clsMain.ReturnDataTable("select SODIEM   from KHTT_HOATDONG where MAHD='04' and SUDUNG=1");
                    if (dtdiem.Rows.Count > 0)
                    {
                        diemcong = int.Parse(dtdiem.Rows[0][0].ToString());
                        string lydo = "Cộng Điểm Khi Mua Bất Kỳ Mệnh Giá Thẻ Trả Trước";
                        string sqlcongdiem = "insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI) values('" + _dt.Rows[0]["Ma_KHTT"].ToString() + "'," + diemcong + ",N'" + lydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','01')";
                        clsMain.ExecuteSQL(sqlcongdiem);

                        clsMain.ExecuteSQL("update KHACHHANG_THANTHIET set SoDiem=SoDiem+" + diemcong + " where MAKHTT='" + _dt.Rows[0]["Ma_KHTT"].ToString() + "'");
                        cls_KHTT.ActiveKHTT(_dt.Rows[0]["Ma_KHTT"].ToString());
                    }

                    clsMain.ReturnDataTable("update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=SoDuTaiKhoan+" + decimal.Parse(txtSoTienNap.Text.Replace(",", "")) + " where MAKHTTNB='" + _dt.Rows[0]["Ma_KHTT"].ToString() + "'");
                    sql = string.Format("insert into KHTT_CHITIET_NOPTIEN values('{0}','{1}','{2:dd/MM/yyyy HH:mm:ss}','{3}','{4}',N'{5}')", _dt.Rows[0]["Ma_KHTT"].ToString(), txtSoTienNap.Text.Replace(",", ""), KP_UserManagement.clsMain.GetServerDate(), "true", clsUserManagement.ReturnMaNVOfUserLogin(), "Nạp tiền vào tài khoản ");
                }
                else//rút tiền
                {
                    if (txtSoTienRut.Text == "" || txtSoTienRut.Text == "0")
                        throw new Exception("Mời Bạn Nhập Số Tiền Nạp");
                    if (decimal.Parse(txtSoTienRut.Text.Replace(",", "")) > decimal.Parse(txtSoDu.Text.Replace(",", "")))
                    {
                        txtSoTienRut.Focus();
                        throw new Exception("Số tiền rút không được lớn hơn số dư tài khoản");
                    }
                    clsMain.ReturnDataTable("update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=SoDuTaiKhoan-" + decimal.Parse(txtSoTienRut.Text.Replace(",", "")) + " where MAKHTTNB='" + _dt.Rows[0]["Ma_KHTT"].ToString() + "'");
                    sql = string.Format("insert into KHTT_CHITIET_NOPTIEN values('{0}','{1}','{2:dd/MM/yyyy HH:mm:ss}','{3}','{4}',N'{5}')", _dt.Rows[0]["Ma_KHTT"].ToString(), txtSoTienRut.Text.Replace(",", ""), KP_UserManagement.clsMain.GetServerDate(), "false", clsUserManagement.ReturnMaNVOfUserLogin(), "Rút tiền khỏi tài khoản");
                }
                clsMain.ReturnDataTable(sql);
                this.Close();
                if (_flag)
                {
                    throw new Exception("Khách Hàng " + txtHoTen.Text.ToUpper() + " Đã Nộp vào tài khoản số tiền :" + txtSoTienNap.Text.ToUpper() + " VND.");
                }
                else
                {
                    throw new Exception("Khách Hàng " + txtHoTen.Text.ToUpper() + " Đã rút số tiền :" + txtSoTienRut.Text.ToUpper() + " VND.");
                }
                
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message,"Thông Báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void txtSoTienTra_EditValueChanged(object sender, EventArgs e)
        {
            if (txtSoTienRut.Text.Trim() != "")
                txtChu.Text = ChuyenSo(txtSoTienRut.Text.Replace(",", ""));
            else
                txtChu.Text = "";
        }

       
        private void txtSoTienTra_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (char.IsDigit(e.KeyChar) || (int)e.KeyChar == 13 || (int)e.KeyChar == 8)
                    e.Handled = false;
                else
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTienTra_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (char.IsDigit((char)e.KeyValue) || e.KeyValue == 46 || e.KeyValue == 8 || e.KeyValue == 96 || e.KeyValue == 97 || e.KeyValue == 98 || e.KeyValue == 99 || e.KeyValue == 100 || e.KeyValue == 101 || e.KeyValue == 102 || e.KeyValue == 103 || e.KeyValue == 104 || e.KeyValue == 105)
                {
                    if (txtSoTienRut.Text == "")
                    {
                        return;
                    }
                    int checkin = txtSoTienRut.SelectionStart;
                    txtSoTienRut.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTienRut.Text.Replace(",", "")));
                    int find = 0;
                    string chuoi = txtSoTienRut.Text;
                    //string chuoi1=chuoi.Substring
                    if (chuoi.Length == 5 || chuoi.Length == 9 || chuoi.Length == 13 || chuoi.Length == 17 || chuoi.Length == 21 || chuoi.Length == 25 || chuoi.Length == 29 || chuoi.Length == 33 || chuoi.Length == 39 || chuoi.Length == 43)
                    {
                        find = 1;
                    }
                    if (checkin == txtSoTienRut.Text.Length - find)
                    {
                        txtSoTienRut.SelectionStart = txtSoTienRut.Text.Length;
                    }
                    else
                    {
                        if (e.KeyValue == 46 || e.KeyValue == 8)
                        {
                            txtSoTienRut.SelectionStart = checkin;
                        }
                        else
                        {
                            txtSoTienRut.SelectionStart = checkin;
                        }
                    }
                }

                if (decimal.Parse(txtSoTienRut.Text.Replace(",", "")) > decimal.Parse(txtSoDu.Text.Replace(",", "")))
                {
                    txtSoTienRut.Focus();
                    throw new Exception("Số tiền rút không được lớn hơn số dư tài khoản");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTienNap_EditValueChanged(object sender, EventArgs e)
        {
            if (txtSoTienNap.Text.Trim() != "")
                txtChu.Text = ChuyenSo(txtSoTienNap.Text.Replace(",", ""));
            else
                txtChu.Text = "";
        }

        private void txtSoTienNap_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (char.IsDigit((char)e.KeyValue) || e.KeyValue == 46 || e.KeyValue == 8 || e.KeyValue == 96 || e.KeyValue == 97 || e.KeyValue == 98 || e.KeyValue == 99 || e.KeyValue == 100 || e.KeyValue == 101 || e.KeyValue == 102 || e.KeyValue == 103 || e.KeyValue == 104 || e.KeyValue == 105)
                {
                    if (txtSoTienNap.Text == "")
                    {
                        return;
                    }
                    int checkin = txtSoTienNap.SelectionStart;
                    txtSoTienNap.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTienNap.Text.Replace(",", "")));
                    int find = 0;
                    string chuoi = txtSoTienNap.Text;
                    //string chuoi1=chuoi.Substring
                    if (chuoi.Length == 5 || chuoi.Length == 9 || chuoi.Length == 13 || chuoi.Length == 17 || chuoi.Length == 21 || chuoi.Length == 25 || chuoi.Length == 29 || chuoi.Length == 33 || chuoi.Length == 39 || chuoi.Length == 43)
                    {
                        find = 1;
                    }
                    if (checkin == txtSoTienNap.Text.Length - find)
                    {
                        txtSoTienNap.SelectionStart = txtSoTienNap.Text.Length;
                    }
                    else
                    {
                        if (e.KeyValue == 46 || e.KeyValue == 8)
                        {
                            txtSoTienNap.SelectionStart = checkin;
                        }
                        else
                        {
                            txtSoTienNap.SelectionStart = checkin;
                        }
                    }
                }

              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

       
       
    }
}