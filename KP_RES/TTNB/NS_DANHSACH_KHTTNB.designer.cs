﻿namespace KP_RES
{
    partial class NS_DANHSACH_KHTTNB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NS_DANHSACH_KHTTNB));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.lbGT = new DevExpress.XtraEditors.LabelControl();
            this.chkNam = new DevExpress.XtraEditors.CheckEdit();
            this.chkNu = new DevExpress.XtraEditors.CheckEdit();
            this.date_tungay = new System.Windows.Forms.DateTimePicker();
            this.date_denngay = new System.Windows.Forms.DateTimePicker();
            this.lbNgayTu = new DevExpress.XtraEditors.LabelControl();
            this.lbNgayDen = new DevExpress.XtraEditors.LabelControl();
            this.txtDenSoDu = new DevExpress.XtraEditors.TextEdit();
            this.lbTuSoDu = new DevExpress.XtraEditors.LabelControl();
            this.txtTuSoDu = new DevExpress.XtraEditors.TextEdit();
            this.lbDenSoDu = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.gcKHTT = new DevExpress.XtraEditors.GroupControl();
            this.gvcShowKHTTNB = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_MADOITUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_SOCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoDuTaiKhoan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayThamGia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbDenNgay = new DevExpress.XtraEditors.LabelControl();
            this.lbTuNgay = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new DevExpress.XtraEditors.PanelControl();
            this.palButton = new DevExpress.XtraEditors.PanelControl();
            this.btn_ThemKHTT = new DevExpress.XtraEditors.SimpleButton();
            this.pn5 = new DevExpress.XtraEditors.PanelControl();
            this.btnInMaKH = new DevExpress.XtraEditors.SimpleButton();
            this.pn4 = new DevExpress.XtraEditors.PanelControl();
            this.btnNapTien = new DevExpress.XtraEditors.SimpleButton();
            this.pn3 = new DevExpress.XtraEditors.PanelControl();
            this.btnRutTien = new DevExpress.XtraEditors.SimpleButton();
            this.pn2 = new DevExpress.XtraEditors.PanelControl();
            this.btn_lichSu = new DevExpress.XtraEditors.SimpleButton();
            this.pn1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHuyKHTT = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenSoDu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTuSoDu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcKHTT)).BeginInit();
            this.gcKHTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTTNB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).BeginInit();
            this.palButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 44);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 655);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 216);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 402);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 80);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 320);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.lbGT);
            this.pnChoose.Controls.Add(this.chkNam);
            this.pnChoose.Controls.Add(this.chkNu);
            this.pnChoose.Controls.Add(this.date_tungay);
            this.pnChoose.Controls.Add(this.date_denngay);
            this.pnChoose.Controls.Add(this.lbNgayTu);
            this.pnChoose.Controls.Add(this.lbNgayDen);
            this.pnChoose.Controls.Add(this.txtDenSoDu);
            this.pnChoose.Controls.Add(this.lbTuSoDu);
            this.pnChoose.Controls.Add(this.txtTuSoDu);
            this.pnChoose.Controls.Add(this.lbDenSoDu);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 78);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // lbGT
            // 
            this.lbGT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbGT.Location = new System.Drawing.Point(8, 8);
            this.lbGT.Name = "lbGT";
            this.lbGT.Size = new System.Drawing.Size(113, 19);
            this.lbGT.TabIndex = 20;
            this.lbGT.Text = "Chọn Giới Tính:";
            this.lbGT.Visible = false;
            // 
            // chkNam
            // 
            this.chkNam.EditValue = true;
            this.chkNam.Location = new System.Drawing.Point(9, 40);
            this.chkNam.Name = "chkNam";
            this.chkNam.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkNam.Properties.Appearance.Options.UseFont = true;
            this.chkNam.Properties.Caption = "Nam";
            this.chkNam.Size = new System.Drawing.Size(75, 24);
            this.chkNam.TabIndex = 18;
            this.chkNam.Visible = false;
            this.chkNam.CheckedChanged += new System.EventHandler(this.chkNam_CheckedChanged);
            // 
            // chkNu
            // 
            this.chkNu.Location = new System.Drawing.Point(115, 42);
            this.chkNu.Name = "chkNu";
            this.chkNu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkNu.Properties.Appearance.Options.UseFont = true;
            this.chkNu.Properties.Caption = "Nữ";
            this.chkNu.Size = new System.Drawing.Size(75, 24);
            this.chkNu.TabIndex = 19;
            this.chkNu.Visible = false;
            this.chkNu.CheckedChanged += new System.EventHandler(this.chkNu_CheckedChanged);
            // 
            // date_tungay
            // 
            this.date_tungay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.CustomFormat = "dd/MM/yyyy";
            this.date_tungay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_tungay.Location = new System.Drawing.Point(90, 3);
            this.date_tungay.Name = "date_tungay";
            this.date_tungay.Size = new System.Drawing.Size(126, 27);
            this.date_tungay.TabIndex = 8;
            this.date_tungay.Visible = false;
            // 
            // date_denngay
            // 
            this.date_denngay.CustomFormat = "dd/MM/yyyy";
            this.date_denngay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_denngay.Location = new System.Drawing.Point(90, 39);
            this.date_denngay.Name = "date_denngay";
            this.date_denngay.Size = new System.Drawing.Size(126, 27);
            this.date_denngay.TabIndex = 9;
            this.date_denngay.Visible = false;
            // 
            // lbNgayTu
            // 
            this.lbNgayTu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayTu.Location = new System.Drawing.Point(4, 8);
            this.lbNgayTu.Name = "lbNgayTu";
            this.lbNgayTu.Size = new System.Drawing.Size(72, 19);
            this.lbNgayTu.TabIndex = 10;
            this.lbNgayTu.Text = "Từ Ngày :";
            this.lbNgayTu.Visible = false;
            // 
            // lbNgayDen
            // 
            this.lbNgayDen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayDen.Location = new System.Drawing.Point(4, 40);
            this.lbNgayDen.Name = "lbNgayDen";
            this.lbNgayDen.Size = new System.Drawing.Size(80, 19);
            this.lbNgayDen.TabIndex = 11;
            this.lbNgayDen.Text = "Đến Ngày :";
            this.lbNgayDen.Visible = false;
            // 
            // txtDenSoDu
            // 
            this.txtDenSoDu.Location = new System.Drawing.Point(90, 38);
            this.txtDenSoDu.Name = "txtDenSoDu";
            this.txtDenSoDu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDenSoDu.Properties.Appearance.Options.UseFont = true;
            this.txtDenSoDu.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtDenSoDu.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtDenSoDu.Properties.Mask.EditMask = "N0";
            this.txtDenSoDu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDenSoDu.Size = new System.Drawing.Size(126, 26);
            this.txtDenSoDu.TabIndex = 15;
            this.txtDenSoDu.Visible = false;
            // 
            // lbTuSoDu
            // 
            this.lbTuSoDu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbTuSoDu.Location = new System.Drawing.Point(4, 7);
            this.lbTuSoDu.Name = "lbTuSoDu";
            this.lbTuSoDu.Size = new System.Drawing.Size(75, 19);
            this.lbTuSoDu.TabIndex = 12;
            this.lbTuSoDu.Text = "Từ Số Dư:";
            this.lbTuSoDu.Visible = false;
            // 
            // txtTuSoDu
            // 
            this.txtTuSoDu.Location = new System.Drawing.Point(90, 4);
            this.txtTuSoDu.Name = "txtTuSoDu";
            this.txtTuSoDu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTuSoDu.Properties.Appearance.Options.UseFont = true;
            this.txtTuSoDu.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtTuSoDu.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTuSoDu.Properties.Mask.EditMask = "N0";
            this.txtTuSoDu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTuSoDu.Size = new System.Drawing.Size(126, 26);
            this.txtTuSoDu.TabIndex = 14;
            this.txtTuSoDu.Visible = false;
            // 
            // lbDenSoDu
            // 
            this.lbDenSoDu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbDenSoDu.Location = new System.Drawing.Point(4, 41);
            this.lbDenSoDu.Name = "lbDenSoDu";
            this.lbDenSoDu.Size = new System.Drawing.Size(83, 19);
            this.lbDenSoDu.TabIndex = 13;
            this.lbDenSoDu.Text = "Đến Số Dư:";
            this.lbDenSoDu.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 214);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "TC";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TC", "Tất Cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("SDTK", "Theo Số Dư Tài Khoản"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Ngay", "Theo ngày tham gia"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("GT", "Theo Giới Tính"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("HUY", "Danh Sách KHTBNB Hủy")});
            this.optGroup.Size = new System.Drawing.Size(215, 210);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 618);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1333, 44);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 655);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 331);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 171);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 251);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 168);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 573);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 162);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // gcKHTT
            // 
            this.gcKHTT.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gcKHTT.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.gcKHTT.AppearanceCaption.Options.UseFont = true;
            this.gcKHTT.AppearanceCaption.Options.UseForeColor = true;
            this.gcKHTT.Controls.Add(this.gvcShowKHTTNB);
            this.gcKHTT.Controls.Add(this.lbDenNgay);
            this.gcKHTT.Controls.Add(this.lbTuNgay);
            this.gcKHTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcKHTT.Location = new System.Drawing.Point(223, 44);
            this.gcKHTT.Name = "gcKHTT";
            this.gcKHTT.ShowCaption = false;
            this.gcKHTT.Size = new System.Drawing.Size(1110, 655);
            this.gcKHTT.TabIndex = 61;
            this.gcKHTT.Text = "Danh Sách Khách Hàng ";
            // 
            // gvcShowKHTTNB
            // 
            this.gvcShowKHTTNB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvcShowKHTTNB.Location = new System.Drawing.Point(2, 2);
            this.gvcShowKHTTNB.MainView = this.gridView1;
            this.gvcShowKHTTNB.Name = "gvcShowKHTTNB";
            this.gvcShowKHTTNB.Size = new System.Drawing.Size(1106, 651);
            this.gvcShowKHTTNB.TabIndex = 0;
            this.gvcShowKHTTNB.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.DT_MADOITUONG,
            this.DT_HOTEN,
            this.DT_NGAYSINH,
            this.GT,
            this.DT_SOCMND,
            this.DT_DIACHI,
            this.DT_DIENTHOAI,
            this.SoDuTaiKhoan,
            this.NgayThamGia,
            this.FILL,
            this.DT_DOITUONGID});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gvcShowKHTTNB;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.DT_HOTEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // DT_MADOITUONG
            // 
            this.DT_MADOITUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_MADOITUONG.AppearanceCell.Options.UseFont = true;
            this.DT_MADOITUONG.AppearanceCell.Options.UseTextOptions = true;
            this.DT_MADOITUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_MADOITUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_MADOITUONG.AppearanceHeader.Options.UseFont = true;
            this.DT_MADOITUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_MADOITUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_MADOITUONG.Caption = "Mã KHTT";
            this.DT_MADOITUONG.FieldName = "DT_MADOITUONG";
            this.DT_MADOITUONG.Name = "DT_MADOITUONG";
            this.DT_MADOITUONG.OptionsColumn.AllowEdit = false;
            this.DT_MADOITUONG.OptionsColumn.AllowFocus = false;
            this.DT_MADOITUONG.OptionsColumn.FixedWidth = true;
            this.DT_MADOITUONG.Visible = true;
            this.DT_MADOITUONG.VisibleIndex = 1;
            this.DT_MADOITUONG.Width = 110;
            // 
            // DT_HOTEN
            // 
            this.DT_HOTEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_HOTEN.AppearanceCell.Options.UseFont = true;
            this.DT_HOTEN.AppearanceCell.Options.UseTextOptions = true;
            this.DT_HOTEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DT_HOTEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_HOTEN.AppearanceHeader.Options.UseFont = true;
            this.DT_HOTEN.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_HOTEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_HOTEN.Caption = "Tên KHTT";
            this.DT_HOTEN.FieldName = "DT_HOTEN";
            this.DT_HOTEN.Name = "DT_HOTEN";
            this.DT_HOTEN.OptionsColumn.AllowEdit = false;
            this.DT_HOTEN.OptionsColumn.AllowFocus = false;
            this.DT_HOTEN.OptionsColumn.FixedWidth = true;
            this.DT_HOTEN.Visible = true;
            this.DT_HOTEN.VisibleIndex = 2;
            this.DT_HOTEN.Width = 200;
            // 
            // DT_NGAYSINH
            // 
            this.DT_NGAYSINH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_NGAYSINH.AppearanceCell.Options.UseFont = true;
            this.DT_NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.DT_NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.DT_NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_NGAYSINH.Caption = "Ngày Sinh";
            this.DT_NGAYSINH.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.DT_NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DT_NGAYSINH.FieldName = "DT_NGAYSINH";
            this.DT_NGAYSINH.Name = "DT_NGAYSINH";
            this.DT_NGAYSINH.OptionsColumn.AllowEdit = false;
            this.DT_NGAYSINH.OptionsColumn.AllowFocus = false;
            this.DT_NGAYSINH.OptionsColumn.FixedWidth = true;
            this.DT_NGAYSINH.Visible = true;
            this.DT_NGAYSINH.VisibleIndex = 4;
            this.DT_NGAYSINH.Width = 100;
            // 
            // GT
            // 
            this.GT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GT.AppearanceCell.Options.UseFont = true;
            this.GT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GT.AppearanceHeader.Options.UseFont = true;
            this.GT.AppearanceHeader.Options.UseTextOptions = true;
            this.GT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GT.Caption = "Giới Tính";
            this.GT.FieldName = "GT";
            this.GT.Name = "GT";
            this.GT.OptionsColumn.AllowEdit = false;
            this.GT.OptionsColumn.AllowFocus = false;
            this.GT.OptionsColumn.FixedWidth = true;
            this.GT.Visible = true;
            this.GT.VisibleIndex = 5;
            this.GT.Width = 90;
            // 
            // DT_SOCMND
            // 
            this.DT_SOCMND.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_SOCMND.AppearanceCell.Options.UseFont = true;
            this.DT_SOCMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_SOCMND.AppearanceHeader.Options.UseFont = true;
            this.DT_SOCMND.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_SOCMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_SOCMND.Caption = "Số CMND";
            this.DT_SOCMND.FieldName = "DT_SOCMND";
            this.DT_SOCMND.Name = "DT_SOCMND";
            this.DT_SOCMND.OptionsColumn.AllowEdit = false;
            this.DT_SOCMND.OptionsColumn.AllowFocus = false;
            this.DT_SOCMND.OptionsColumn.FixedWidth = true;
            this.DT_SOCMND.Visible = true;
            this.DT_SOCMND.VisibleIndex = 6;
            this.DT_SOCMND.Width = 114;
            // 
            // DT_DIACHI
            // 
            this.DT_DIACHI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_DIACHI.AppearanceCell.Options.UseFont = true;
            this.DT_DIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DT_DIACHI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DT_DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DT_DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_DIACHI.Caption = "Địa Chỉ";
            this.DT_DIACHI.FieldName = "DT_DIACHI";
            this.DT_DIACHI.Name = "DT_DIACHI";
            this.DT_DIACHI.OptionsColumn.AllowEdit = false;
            this.DT_DIACHI.OptionsColumn.AllowFocus = false;
            this.DT_DIACHI.OptionsColumn.FixedWidth = true;
            this.DT_DIACHI.Visible = true;
            this.DT_DIACHI.VisibleIndex = 7;
            this.DT_DIACHI.Width = 306;
            // 
            // DT_DIENTHOAI
            // 
            this.DT_DIENTHOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_DIENTHOAI.AppearanceCell.Options.UseFont = true;
            this.DT_DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DT_DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DT_DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DT_DIENTHOAI.Caption = "ĐTDĐ";
            this.DT_DIENTHOAI.FieldName = "DT_DIENTHOAI";
            this.DT_DIENTHOAI.Name = "DT_DIENTHOAI";
            this.DT_DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DT_DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DT_DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DT_DIENTHOAI.Visible = true;
            this.DT_DIENTHOAI.VisibleIndex = 8;
            this.DT_DIENTHOAI.Width = 100;
            // 
            // SoDuTaiKhoan
            // 
            this.SoDuTaiKhoan.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SoDuTaiKhoan.AppearanceCell.Options.UseFont = true;
            this.SoDuTaiKhoan.AppearanceCell.Options.UseTextOptions = true;
            this.SoDuTaiKhoan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SoDuTaiKhoan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SoDuTaiKhoan.AppearanceHeader.Options.UseFont = true;
            this.SoDuTaiKhoan.AppearanceHeader.Options.UseTextOptions = true;
            this.SoDuTaiKhoan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoDuTaiKhoan.Caption = "Số Dư Tài Khoản";
            this.SoDuTaiKhoan.DisplayFormat.FormatString = "#,0";
            this.SoDuTaiKhoan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SoDuTaiKhoan.FieldName = "SoDuTaiKhoan";
            this.SoDuTaiKhoan.Name = "SoDuTaiKhoan";
            this.SoDuTaiKhoan.OptionsColumn.AllowEdit = false;
            this.SoDuTaiKhoan.OptionsColumn.AllowFocus = false;
            this.SoDuTaiKhoan.OptionsColumn.FixedWidth = true;
            this.SoDuTaiKhoan.Visible = true;
            this.SoDuTaiKhoan.VisibleIndex = 3;
            this.SoDuTaiKhoan.Width = 155;
            // 
            // NgayThamGia
            // 
            this.NgayThamGia.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NgayThamGia.AppearanceCell.Options.UseFont = true;
            this.NgayThamGia.AppearanceCell.Options.UseTextOptions = true;
            this.NgayThamGia.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayThamGia.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NgayThamGia.AppearanceHeader.Options.UseFont = true;
            this.NgayThamGia.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayThamGia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayThamGia.Caption = "Ngày Tham Gia";
            this.NgayThamGia.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayThamGia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayThamGia.FieldName = "NgayThamGia";
            this.NgayThamGia.Name = "NgayThamGia";
            this.NgayThamGia.OptionsColumn.AllowEdit = false;
            this.NgayThamGia.OptionsColumn.AllowFocus = false;
            this.NgayThamGia.OptionsColumn.FixedWidth = true;
            this.NgayThamGia.Visible = true;
            this.NgayThamGia.VisibleIndex = 9;
            this.NgayThamGia.Width = 150;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 10;
            this.FILL.Width = 20;
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.Caption = "gridColumn2";
            this.DT_DOITUONGID.FieldName = "DT_DOITUONGID";
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            this.DT_DOITUONGID.OptionsColumn.AllowEdit = false;
            this.DT_DOITUONGID.OptionsColumn.AllowFocus = false;
            this.DT_DOITUONGID.OptionsColumn.FixedWidth = true;
            // 
            // lbDenNgay
            // 
            this.lbDenNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbDenNgay.Location = new System.Drawing.Point(208, 303);
            this.lbDenNgay.Name = "lbDenNgay";
            this.lbDenNgay.Size = new System.Drawing.Size(61, 14);
            this.lbDenNgay.TabIndex = 11;
            this.lbDenNgay.Text = "Đến Ngày :";
            // 
            // lbTuNgay
            // 
            this.lbTuNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbTuNgay.Location = new System.Drawing.Point(-2, 304);
            this.lbTuNgay.Name = "lbTuNgay";
            this.lbTuNgay.Size = new System.Drawing.Size(55, 14);
            this.lbTuNgay.TabIndex = 10;
            this.lbTuNgay.Text = "Từ Ngày :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.palButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1372, 44);
            this.panel2.TabIndex = 49;
            // 
            // palButton
            // 
            this.palButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palButton.Controls.Add(this.btn_ThemKHTT);
            this.palButton.Controls.Add(this.pn5);
            this.palButton.Controls.Add(this.btnInMaKH);
            this.palButton.Controls.Add(this.pn4);
            this.palButton.Controls.Add(this.btnNapTien);
            this.palButton.Controls.Add(this.pn3);
            this.palButton.Controls.Add(this.btnRutTien);
            this.palButton.Controls.Add(this.pn2);
            this.palButton.Controls.Add(this.btn_lichSu);
            this.palButton.Controls.Add(this.pn1);
            this.palButton.Controls.Add(this.btnHuyKHTT);
            this.palButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.palButton.Location = new System.Drawing.Point(169, 2);
            this.palButton.Name = "palButton";
            this.palButton.Size = new System.Drawing.Size(1201, 40);
            this.palButton.TabIndex = 47;
            // 
            // btn_ThemKHTT
            // 
            this.btn_ThemKHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ThemKHTT.Appearance.Options.UseFont = true;
            this.btn_ThemKHTT.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_ThemKHTT.Image = ((System.Drawing.Image)(resources.GetObject("btn_ThemKHTT.Image")));
            this.btn_ThemKHTT.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_ThemKHTT.Location = new System.Drawing.Point(94, 0);
            this.btn_ThemKHTT.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ThemKHTT.Name = "btn_ThemKHTT";
            this.btn_ThemKHTT.Size = new System.Drawing.Size(172, 40);
            this.btn_ThemKHTT.TabIndex = 56;
            this.btn_ThemKHTT.Text = "&Thêm KHTTNB";
            this.btn_ThemKHTT.Visible = false;
            this.btn_ThemKHTT.Click += new System.EventHandler(this.btn_ThemKHTT_Click);
            // 
            // pn5
            // 
            this.pn5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn5.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn5.Location = new System.Drawing.Point(266, 0);
            this.pn5.Name = "pn5";
            this.pn5.Size = new System.Drawing.Size(15, 40);
            this.pn5.TabIndex = 57;
            // 
            // btnInMaKH
            // 
            this.btnInMaKH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInMaKH.Appearance.Options.UseFont = true;
            this.btnInMaKH.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnInMaKH.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnInMaKH.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnInMaKH.Location = new System.Drawing.Point(281, 0);
            this.btnInMaKH.Margin = new System.Windows.Forms.Padding(4);
            this.btnInMaKH.Name = "btnInMaKH";
            this.btnInMaKH.Size = new System.Drawing.Size(172, 40);
            this.btnInMaKH.TabIndex = 58;
            this.btnInMaKH.Text = "&In Thẻ KHTTNB";
            this.btnInMaKH.Click += new System.EventHandler(this.btnInMaKH_Click);
            // 
            // pn4
            // 
            this.pn4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn4.Location = new System.Drawing.Point(453, 0);
            this.pn4.Name = "pn4";
            this.pn4.Size = new System.Drawing.Size(15, 40);
            this.pn4.TabIndex = 55;
            // 
            // btnNapTien
            // 
            this.btnNapTien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNapTien.Appearance.Options.UseFont = true;
            this.btnNapTien.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNapTien.Image = ((System.Drawing.Image)(resources.GetObject("btnNapTien.Image")));
            this.btnNapTien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnNapTien.Location = new System.Drawing.Point(468, 0);
            this.btnNapTien.Margin = new System.Windows.Forms.Padding(4);
            this.btnNapTien.Name = "btnNapTien";
            this.btnNapTien.Size = new System.Drawing.Size(172, 40);
            this.btnNapTien.TabIndex = 45;
            this.btnNapTien.Text = "&Nạp Tiền";
            this.btnNapTien.Click += new System.EventHandler(this.btnCong_Click);
            // 
            // pn3
            // 
            this.pn3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn3.Location = new System.Drawing.Point(640, 0);
            this.pn3.Name = "pn3";
            this.pn3.Size = new System.Drawing.Size(15, 40);
            this.pn3.TabIndex = 54;
            // 
            // btnRutTien
            // 
            this.btnRutTien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRutTien.Appearance.Options.UseFont = true;
            this.btnRutTien.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRutTien.Image = ((System.Drawing.Image)(resources.GetObject("btnRutTien.Image")));
            this.btnRutTien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnRutTien.Location = new System.Drawing.Point(655, 0);
            this.btnRutTien.Margin = new System.Windows.Forms.Padding(4);
            this.btnRutTien.Name = "btnRutTien";
            this.btnRutTien.Size = new System.Drawing.Size(172, 40);
            this.btnRutTien.TabIndex = 44;
            this.btnRutTien.Text = "&Rút Tiền";
            this.btnRutTien.Click += new System.EventHandler(this.btnTru_Click);
            // 
            // pn2
            // 
            this.pn2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn2.Location = new System.Drawing.Point(827, 0);
            this.pn2.Name = "pn2";
            this.pn2.Size = new System.Drawing.Size(15, 40);
            this.pn2.TabIndex = 53;
            // 
            // btn_lichSu
            // 
            this.btn_lichSu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_lichSu.Appearance.Options.UseFont = true;
            this.btn_lichSu.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_lichSu.Image = global::KP_RES.Properties.Resources.View_Details_26;
            this.btn_lichSu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_lichSu.Location = new System.Drawing.Point(842, 0);
            this.btn_lichSu.Margin = new System.Windows.Forms.Padding(4);
            this.btn_lichSu.Name = "btn_lichSu";
            this.btn_lichSu.Size = new System.Drawing.Size(172, 40);
            this.btn_lichSu.TabIndex = 46;
            this.btn_lichSu.Text = "&Lịch Sử Giao Dịch";
            this.btn_lichSu.Click += new System.EventHandler(this.btn_lichSu_Click);
            // 
            // pn1
            // 
            this.pn1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn1.Location = new System.Drawing.Point(1014, 0);
            this.pn1.Name = "pn1";
            this.pn1.Size = new System.Drawing.Size(15, 40);
            this.pn1.TabIndex = 52;
            // 
            // btnHuyKHTT
            // 
            this.btnHuyKHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyKHTT.Appearance.Options.UseFont = true;
            this.btnHuyKHTT.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHuyKHTT.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnHuyKHTT.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuyKHTT.Location = new System.Drawing.Point(1029, 0);
            this.btnHuyKHTT.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyKHTT.Name = "btnHuyKHTT";
            this.btnHuyKHTT.Size = new System.Drawing.Size(172, 40);
            this.btnHuyKHTT.TabIndex = 43;
            this.btnHuyKHTT.Text = "&Hủy KHTTNB";
            this.btnHuyKHTT.Click += new System.EventHandler(this.btnHuyKHTT_Click);
            // 
            // NS_DANHSACH_KHTTNB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 699);
            this.Controls.Add(this.gcKHTT);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NS_DANHSACH_KHTTNB";
            this.Text = "Danh Sách Khách Hàng Thanh Toán Nội Bộ";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenSoDu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTuSoDu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcKHTT)).EndInit();
            this.gcKHTT.ResumeLayout(false);
            this.gcKHTT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTTNB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).EndInit();
            this.palButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.GroupControl gcKHTT;
        private DevExpress.XtraGrid.GridControl gvcShowKHTTNB;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn DT_MADOITUONG;
        private DevExpress.XtraGrid.Columns.GridColumn DT_HOTEN;
        private DevExpress.XtraGrid.Columns.GridColumn DT_NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GT;
        private DevExpress.XtraGrid.Columns.GridColumn DT_SOCMND;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn SoDuTaiKhoan;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraGrid.Columns.GridColumn NgayThamGia;
        private System.Windows.Forms.DateTimePicker date_denngay;
        private System.Windows.Forms.DateTimePicker date_tungay;
        private DevExpress.XtraEditors.LabelControl lbDenNgay;
        private DevExpress.XtraEditors.LabelControl lbTuNgay;
        private DevExpress.XtraEditors.LabelControl lbNgayTu;
        private DevExpress.XtraEditors.LabelControl lbNgayDen;
        private DevExpress.XtraEditors.LabelControl lbTuSoDu;
        private DevExpress.XtraEditors.LabelControl lbDenSoDu;
        private DevExpress.XtraEditors.TextEdit txtTuSoDu;
        private DevExpress.XtraEditors.TextEdit txtDenSoDu;
        private DevExpress.XtraEditors.PanelControl panel2;
        private DevExpress.XtraEditors.PanelControl palButton;
        private DevExpress.XtraEditors.SimpleButton btnNapTien;
        private DevExpress.XtraEditors.SimpleButton btnRutTien;
        private DevExpress.XtraEditors.SimpleButton btnHuyKHTT;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.CheckEdit chkNu;
        private DevExpress.XtraEditors.CheckEdit chkNam;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.LabelControl lbGT;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btn_lichSu;
        private DevExpress.XtraEditors.PanelControl pn4;
        private DevExpress.XtraEditors.PanelControl pn3;
        private DevExpress.XtraEditors.PanelControl pn2;
        private DevExpress.XtraEditors.PanelControl pn1;
        private DevExpress.XtraEditors.SimpleButton btn_ThemKHTT;
        private DevExpress.XtraEditors.SimpleButton btnInMaKH;
        private DevExpress.XtraEditors.PanelControl pn5;



    }
}