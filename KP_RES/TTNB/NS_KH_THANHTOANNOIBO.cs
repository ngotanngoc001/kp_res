﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
namespace KP_RES
{
    public partial class NS_KH_THANHTOANNOIBO : DevExpress.XtraEditors.XtraForm
    {
        public NS_KH_THANHTOANNOIBO()
        {
            InitializeComponent();
            LoadDuLieu();
            LoadPermission();
        }

        public void LoadDuLieu()
        {
            DataTable dt = clsMain.ReturnDataTable("select MA as Ma_KHTT, TEN as HoTenKHTT,NGAYSINH ,(CASE WHEN GIOITINH='1' THEN N'Nam' ELSE N'Nữ' END) AS GIOITINH ,CMND, EMAIL as Email,DIENTHOAI as DienThoaiDiDong,DIACHI as DiaChi from NHACUNGCAP where LOAI='0' and SUDUNG=1 and MA not in( select Ma_KHTT from KHACHHANG_THANHTOAN_NOIBO where SuDung=1)");
            gvcShowKHTT.DataSource = dt;
        }
       
         private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

         private Boolean CheckMAKH(string maKH)
         {
             DataTable dtsdt = clsMain.ReturnDataTable("select MAKHTT from  KHACHHANG_THANTHIET  where MAKHTT='" + maKH + "'");
             if (dtsdt.Rows.Count > 0)
             {

                 return false;
             }
             return true;
         }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0)
                    throw new Exception("Không có khách hàng nào ");
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception("Bạn chưa chọn khách hàng cần thêm! ");
                DataTable dtCheck = clsMain.ReturnDataTable("select * from KHACHHANG_THANHTOAN_NOIBO where Ma_KHTT='" + gridView1.GetFocusedRowCellValue(Ma_KHTT).ToString() + "' ");
                int MaCapDo = int.Parse(clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](0)").Rows[0][0].ToString());
                if (dtCheck.Rows.Count > 0)
                {
                    string sql = "update KHACHHANG_THANHTOAN_NOIBO set  SuDung=1 where Ma_KHTT='" + gridView1.GetFocusedRowCellValue(Ma_KHTT).ToString() + "'";
                    clsMain.ExecuteSQL(sql);
                }
                else
                {

                    string smaKH;
                    DataTable dtMaKH = clsMain.ReturnDataTable("select top(1) MAKHTT from KHACHHANG_THANTHIET where FLAGKHTT!=0 order by FLAGKHTT desc");
                    if (dtMaKH.Rows.Count == 0)
                    {
                        smaKH = "KH00000001";
                    }
                    else
                    {
                        smaKH = "KH" + string.Format("{0:00000000}", int.Parse(dtMaKH.Rows[0][0].ToString().Substring(2, dtMaKH.Rows[0][0].ToString().Length - 2)) + 1);
                    }
                    while (!CheckMAKH(smaKH))
                    {

                        smaKH = "KH" + string.Format("{0:00000000}", int.Parse(smaKH.Substring(2, smaKH.Length - 2)) + 1);
                    }

                    clsMain.ExecuteSQL("Insert into KHACHHANG_THANTHIET(Ma_KHTT,SoLe,SoDiem,MACAPDO,SoLanChoThuong,SoLanDaDuocThuong,SuDung,SoLanReset,SoLanThuongReSet,SoLanTinh,NgayThamGia,LastUpdate,MAKHTT,FLAGKHTT) Values('" + gridView1.GetFocusedRowCellValue(Ma_KHTT).ToString() + "',0,0," + MaCapDo + ",0,0,0,0,0,0,getdate(),getdate(),'" + smaKH + "',1)");

                    clsMain.ExecuteSQL("Insert into KHACHHANG_THANHTOAN_NOIBO(Ma_KHTT,SoDuTaiKhoan,SuDung,NgayThamGia,MAKHTTNB,FLAGKHTTNB) Values('" + gridView1.GetFocusedRowCellValue(Ma_KHTT).ToString() + "',0,1,getdate(),'" + smaKH + "',1)");
                }
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        
        private void NS_KH_THANHTOANNOIBO_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("52");
        }
    }    
}
      