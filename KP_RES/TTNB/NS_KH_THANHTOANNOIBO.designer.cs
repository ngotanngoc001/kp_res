﻿namespace KP_RES
{
    partial class NS_KH_THANHTOANNOIBO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NS_KH_THANHTOANNOIBO));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.pn_WapperContent = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pnViewProduct = new DevExpress.XtraEditors.PanelControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grCtrlDSHangMua = new DevExpress.XtraGrid.GridControl();
            this.gVDSHangMua = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.HH_HANGHOAID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HH_BARCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HH_TENHANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HH_DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.So_Luong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.soLuongtra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HH_GIABANLE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Khuyen_Mai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HH_VAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.K_MAK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CH_MACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Q_MAQ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT_ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.btn_Add = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.btn_Exc = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btn_Del = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TrungHere = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lb_RowsCount = new DevExpress.XtraEditors.LabelControl();
            this.lb_STT = new DevExpress.XtraEditors.LabelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelThanhTien = new DevExpress.XtraEditors.LabelControl();
            this.labelSoLuong = new DevExpress.XtraEditors.LabelControl();
            this.lableShowTenHang = new DevExpress.XtraEditors.LabelControl();
            this.pnInBaoCao = new DevExpress.XtraEditors.PanelControl();
            this.lbTienTraLai = new DevExpress.XtraEditors.LabelControl();
            this.lb_TienTraLai = new DevExpress.XtraEditors.LabelControl();
            this.btnInBaoCao = new DevExpress.XtraEditors.SimpleButton();
            this.lableTongDoanhThu = new DevExpress.XtraEditors.LabelControl();
            this.lbTongDoanhThu = new DevExpress.XtraEditors.LabelControl();
            this.pnWapperDeletePlusBut = new DevExpress.XtraEditors.PanelControl();
            this.btnDeleteAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Tru = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.pn_ThanhToan1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.pnPassWork = new DevExpress.XtraEditors.PanelControl();
            this.lb_MesgasFuntion = new DevExpress.XtraEditors.LabelControl();
            this.btn_XacNhanSua = new DevExpress.XtraEditors.SimpleButton();
            this.txtPassWorkConfig = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.pnMaHoaDonTraHang = new DevExpress.XtraEditors.PanelControl();
            this.btnQuayLai = new DevExpress.XtraEditors.SimpleButton();
            this.btnTraTatCa = new DevExpress.XtraEditors.SimpleButton();
            this.txtSoLuongTra = new DevExpress.XtraEditors.TextEdit();
            this.txtMaHangTra = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.btnTra = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaHoaDonTra = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.pnRePrinterBuil = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grCtrReviewBuil = new DevExpress.XtraGrid.GridControl();
            this.gvListHoaDon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this._HoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cl_Ngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.btnUpListHd = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowListHd = new DevExpress.XtraEditors.SimpleButton();
            this.pnBaoCaoBanHang = new DevExpress.XtraEditors.PanelControl();
            this.btnOkBaoCaoBanH = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.dt_denngay = new DevExpress.XtraEditors.DateEdit();
            this.dt_tungay = new DevExpress.XtraEditors.DateEdit();
            this.btnXemNgayKhac = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemTrongNgay = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaHangBaoCao = new DevExpress.XtraEditors.TextEdit();
            this.pnBanHang = new DevExpress.XtraEditors.PanelControl();
            this.lbTienTruocThue = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelThueVAT = new DevExpress.XtraEditors.LabelControl();
            this.label_GiamGia = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt_soluong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.LabelControl();
            this.txt_Mahang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pnHoaDonDoi = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gr_listhoadon = new DevExpress.XtraGrid.GridControl();
            this.gv_listhoadon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cl_STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cl_HoaDon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.btnUpBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowBill = new DevExpress.XtraEditors.SimpleButton();
            this.pnContainer = new DevExpress.XtraEditors.PanelControl();
            this.drSearchKho = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.lbmahang = new DevExpress.XtraEditors.LabelControl();
            this.txtSearchMaHang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.btnSearchTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.pnInforOder = new DevExpress.XtraEditors.PanelControl();
            this.btn_TraCuuKH = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtIdProduct = new DevExpress.XtraEditors.TextEdit();
            this.pnKiemKe = new DevExpress.XtraEditors.PanelControl();
            this.btn_LayKK = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuTamFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnSeverFile = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaHangKK = new DevExpress.XtraEditors.TextEdit();
            this.txtSoLuongKK = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.tab_Option = new DevExpress.XtraTab.XtraTabControl();
            this.tabThuNgan = new DevExpress.XtraTab.XtraTabPage();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnByProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnCashDrawer = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuHoaDonDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayHoaDonDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnKhachHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnTraHang = new DevExpress.XtraEditors.SimpleButton();
            this.tabNghiepVu = new DevExpress.XtraTab.XtraTabPage();
            this.btnKetCa = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.btnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.btnKiemKe = new DevExpress.XtraEditors.SimpleButton();
            this.btnLogoOut = new DevExpress.XtraEditors.SimpleButton();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.btnBaoCaoTraHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnBaoCaoBanHang = new DevExpress.XtraEditors.SimpleButton();
            this.pnFuntionThanhToan = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.btn_TopNgT = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ButtonNgT = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.gr_ListNguyenTe = new DevExpress.XtraGrid.GridControl();
            this.gv_listnguyente = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Ma_NguyenTe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Nguyen_Te = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Ty_Gia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Phu_Thu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.scrolCTNgyuenTe = new DevExpress.XtraEditors.PanelControl();
            this.lb_TyGia = new DevExpress.XtraEditors.LabelControl();
            this.lbLoaiTien = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.btnHinhThucThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhapLaiTien = new DevExpress.XtraEditors.SimpleButton();
            this.btnKhachHangThanThiet = new DevExpress.XtraEditors.SimpleButton();
            this.btn_boqua = new DevExpress.XtraEditors.SimpleButton();
            this.btn_khonginhoadon = new DevExpress.XtraEditors.SimpleButton();
            this.btnKhuyenMaiGiamGia = new DevExpress.XtraEditors.SimpleButton();
            this.pn_bigparent = new DevExpress.XtraEditors.PanelControl();
            this.pnctrlParent = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pnSearch = new DevExpress.XtraEditors.PanelControl();
            this.txtHangSX = new DevExpress.XtraEditors.TextEdit();
            this.txtGiaBan = new DevExpress.XtraEditors.TextEdit();
            this.txtTenHH = new DevExpress.XtraEditors.TextEdit();
            this.txtMaHH = new DevExpress.XtraEditors.TextEdit();
            this.txtSTT = new DevExpress.XtraEditors.TextEdit();
            this.gcSanPham = new DevExpress.XtraGrid.GridControl();
            this.gvSanPham = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HangSX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.cbHangSx = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbPhanNhom = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbNhom = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbNganh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.cbSoTrang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbSoTrang = new DevExpress.XtraEditors.LabelControl();
            this.txtPageNow = new DevExpress.XtraEditors.TextEdit();
            this.btn_Next = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9Cot = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Last = new DevExpress.XtraEditors.SimpleButton();
            this.lb_sotrang = new DevExpress.XtraEditors.LabelControl();
            this.btn_First = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7cot = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Prev = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5cot = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pn_banphim = new DevExpress.XtraEditors.PanelControl();
            this.btn_Payments = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ABC = new DevExpress.XtraEditors.SimpleButton();
            this.btn_000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_enter = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_back = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            this.pnKeyBoardCharacter = new DevExpress.XtraEditors.PanelControl();
            this.btn_backchar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PaymentsChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_OKChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_L = new DevExpress.XtraEditors.SimpleButton();
            this.btn_K = new DevExpress.XtraEditors.SimpleButton();
            this.btn_U = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Y = new DevExpress.XtraEditors.SimpleButton();
            this.btn_123 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_M = new DevExpress.XtraEditors.SimpleButton();
            this.btn_J = new DevExpress.XtraEditors.SimpleButton();
            this.btn_I = new DevExpress.XtraEditors.SimpleButton();
            this.btn_T = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DeleteChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_N = new DevExpress.XtraEditors.SimpleButton();
            this.btn_H = new DevExpress.XtraEditors.SimpleButton();
            this.btn_O = new DevExpress.XtraEditors.SimpleButton();
            this.btn_R = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_B = new DevExpress.XtraEditors.SimpleButton();
            this.btn_G = new DevExpress.XtraEditors.SimpleButton();
            this.btn_P = new DevExpress.XtraEditors.SimpleButton();
            this.btn_E = new DevExpress.XtraEditors.SimpleButton();
            this.btn_X = new DevExpress.XtraEditors.SimpleButton();
            this.btn_V = new DevExpress.XtraEditors.SimpleButton();
            this.btn_F = new DevExpress.XtraEditors.SimpleButton();
            this.btn_A = new DevExpress.XtraEditors.SimpleButton();
            this.btn_W = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Z = new DevExpress.XtraEditors.SimpleButton();
            this.btn_C = new DevExpress.XtraEditors.SimpleButton();
            this.btn_D = new DevExpress.XtraEditors.SimpleButton();
            this.btn_S = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Q = new DevExpress.XtraEditors.SimpleButton();
            this.pnTienThanhToan = new DevExpress.XtraEditors.PanelControl();
            this.pnCtrlNgNhPhNhom = new DevExpress.XtraEditors.PanelControl();
            this.gr_ChonPhanNhom = new DevExpress.XtraEditors.GroupControl();
            this.Scrol_ShowPhanNhom = new DevExpress.XtraEditors.XtraScrollableControl();
            this.txtShowPhanNhom3 = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNhom3 = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNganh3 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.lbltrangPNhom = new DevExpress.XtraEditors.LabelControl();
            this.btnBackPNhom = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ClosePhanNganh = new DevExpress.XtraEditors.SimpleButton();
            this.btnNextPNhom = new DevExpress.XtraEditors.SimpleButton();
            this.gr_ChonNganh = new DevExpress.XtraEditors.GroupControl();
            this.scrol_Nganh = new DevExpress.XtraEditors.XtraScrollableControl();
            this.txtShowPhanNhom = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNhom = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNganh = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.lbPages_ng = new DevExpress.XtraEditors.LabelControl();
            this.btnNextNg = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PrevNg = new DevExpress.XtraEditors.SimpleButton();
            this.gr_ChonNhom = new DevExpress.XtraEditors.GroupControl();
            this.Scrol_ShowNhom = new DevExpress.XtraEditors.XtraScrollableControl();
            this.txtShowPanNhom2 = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNhom2 = new DevExpress.XtraEditors.SimpleButton();
            this.txtShowNganh2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.btn_NextNhom = new DevExpress.XtraEditors.SimpleButton();
            this.lblSoTrangNhom = new DevExpress.XtraEditors.LabelControl();
            this.btn_CloseTabNhom = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackNhom = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.lb_TienTra = new DevExpress.XtraEditors.LabelControl();
            this.txtTienThanhToan = new DevExpress.XtraEditors.TextEdit();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.lable_lan3 = new DevExpress.XtraEditors.LabelControl();
            this.lable_tralan3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.lable_lan2 = new DevExpress.XtraEditors.LabelControl();
            this.lable_tralan2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.lable_lan1 = new DevExpress.XtraEditors.LabelControl();
            this.lable_tralan1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.lb_khuyenmai = new DevExpress.XtraEditors.LabelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tientrakhach = new DevExpress.XtraEditors.LabelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.lbTongCongTien = new DevExpress.XtraEditors.LabelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tienkhachtra = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tienthanhtoan = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl26 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.gvcShowKHTT = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Ma_KHTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HoTenKHTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DienThoaiDiDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new DevExpress.XtraEditors.PanelControl();
            this.palButton = new DevExpress.XtraEditors.PanelControl();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pn_WapperContent)).BeginInit();
            this.pn_WapperContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnViewProduct)).BeginInit();
            this.pnViewProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCtrlDSHangMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVDSHangMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Exc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Del)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrungHere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnInBaoCao)).BeginInit();
            this.pnInBaoCao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapperDeletePlusBut)).BeginInit();
            this.pnWapperDeletePlusBut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_ThanhToan1)).BeginInit();
            this.pn_ThanhToan1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnPassWork)).BeginInit();
            this.pnPassWork.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassWorkConfig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMaHoaDonTraHang)).BeginInit();
            this.pnMaHoaDonTraHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuongTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDonTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRePrinterBuil)).BeginInit();
            this.pnRePrinterBuil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCtrReviewBuil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBaoCaoBanHang)).BeginInit();
            this.pnBaoCaoBanHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_denngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_denngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_tungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_tungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangBaoCao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBanHang)).BeginInit();
            this.pnBanHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mahang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHoaDonDoi)).BeginInit();
            this.pnHoaDonDoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_listhoadon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_listhoadon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnContainer)).BeginInit();
            this.pnContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drSearchKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchMaHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforOder)).BeginInit();
            this.pnInforOder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnKiemKe)).BeginInit();
            this.pnKiemKe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangKK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuongKK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).BeginInit();
            this.tab_Option.SuspendLayout();
            this.tabThuNgan.SuspendLayout();
            this.tabNghiepVu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnFuntionThanhToan)).BeginInit();
            this.pnFuntionThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ListNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_listnguyente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scrolCTNgyuenTe)).BeginInit();
            this.scrolCTNgyuenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_bigparent)).BeginInit();
            this.pn_bigparent.SuspendLayout();
            this.pnctrlParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnSearch)).BeginInit();
            this.pnSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHangSX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHangSx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPhanNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNganh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSoTrang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim)).BeginInit();
            this.pn_banphim.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnKeyBoardCharacter)).BeginInit();
            this.pnKeyBoardCharacter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTienThanhToan)).BeginInit();
            this.pnTienThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnCtrlNgNhPhNhom)).BeginInit();
            this.pnCtrlNgNhPhNhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonPhanNhom)).BeginInit();
            this.gr_ChonPhanNhom.SuspendLayout();
            this.Scrol_ShowPhanNhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNganh)).BeginInit();
            this.gr_ChonNganh.SuspendLayout();
            this.scrol_Nganh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNhom)).BeginInit();
            this.gr_ChonNhom.SuspendLayout();
            this.Scrol_ShowNhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).BeginInit();
            this.panelControl26.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).BeginInit();
            this.palButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_WapperContent
            // 
            this.pn_WapperContent.Controls.Add(this.panelControl7);
            this.pn_WapperContent.Controls.Add(this.pnInBaoCao);
            this.pn_WapperContent.Controls.Add(this.pnWapperDeletePlusBut);
            this.pn_WapperContent.Controls.Add(this.panelControl8);
            this.pn_WapperContent.Controls.Add(this.pn_bigparent);
            this.pn_WapperContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_WapperContent.Location = new System.Drawing.Point(0, 0);
            this.pn_WapperContent.Name = "pn_WapperContent";
            this.pn_WapperContent.Size = new System.Drawing.Size(978, 517);
            this.pn_WapperContent.TabIndex = 66;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.pnViewProduct);
            this.panelControl7.Controls.Add(this.panelControl2);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(565, 184);
            this.panelControl7.TabIndex = 16;
            // 
            // pnViewProduct
            // 
            this.pnViewProduct.Controls.Add(this.gridSplitContainer1);
            this.pnViewProduct.Controls.Add(this.panelControl4);
            this.pnViewProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnViewProduct.Location = new System.Drawing.Point(2, 31);
            this.pnViewProduct.Name = "pnViewProduct";
            this.pnViewProduct.Size = new System.Drawing.Size(561, 151);
            this.pnViewProduct.TabIndex = 45;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.grCtrlDSHangMua;
            this.gridSplitContainer1.Location = new System.Drawing.Point(2, 2);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.grCtrlDSHangMua);
            this.gridSplitContainer1.Size = new System.Drawing.Size(519, 147);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // grCtrlDSHangMua
            // 
            this.grCtrlDSHangMua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCtrlDSHangMua.Location = new System.Drawing.Point(0, 0);
            this.grCtrlDSHangMua.MainView = this.gVDSHangMua;
            this.grCtrlDSHangMua.Name = "grCtrlDSHangMua";
            this.grCtrlDSHangMua.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.btn_Add,
            this.repositoryItemCalcEdit1,
            this.btn_Exc,
            this.btn_Del,
            this.TrungHere,
            this.repositoryItemGridLookUpEdit1});
            this.grCtrlDSHangMua.Size = new System.Drawing.Size(519, 147);
            this.grCtrlDSHangMua.TabIndex = 15;
            this.grCtrlDSHangMua.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gVDSHangMua,
            this.gridView3});
            // 
            // gVDSHangMua
            // 
            this.gVDSHangMua.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gVDSHangMua.Appearance.Row.Options.UseFont = true;
            this.gVDSHangMua.Appearance.Row.Options.UseTextOptions = true;
            this.gVDSHangMua.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gVDSHangMua.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.HH_HANGHOAID,
            this.HH_BARCODE,
            this.HH_TENHANGHOA,
            this.HH_DVT,
            this.So_Luong,
            this.soLuongtra,
            this.HH_GIABANLE,
            this.Khuyen_Mai,
            this.HH_VAT,
            this.Thanh_Tien,
            this.K_MAK,
            this.CH_MACH,
            this.Q_MAQ,
            this.STT_});
            this.gVDSHangMua.GridControl = this.grCtrlDSHangMua;
            this.gVDSHangMua.Name = "gVDSHangMua";
            this.gVDSHangMua.OptionsView.EnableAppearanceEvenRow = true;
            this.gVDSHangMua.OptionsView.ShowGroupPanel = false;
            this.gVDSHangMua.RowHeight = 45;
            this.gVDSHangMua.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // HH_HANGHOAID
            // 
            this.HH_HANGHOAID.Caption = "Mã Hàng Hóa";
            this.HH_HANGHOAID.FieldName = "HH_HANGHOAID";
            this.HH_HANGHOAID.Name = "HH_HANGHOAID";
            // 
            // HH_BARCODE
            // 
            this.HH_BARCODE.AppearanceCell.Options.UseTextOptions = true;
            this.HH_BARCODE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_BARCODE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_BARCODE.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HH_BARCODE.AppearanceHeader.Options.UseFont = true;
            this.HH_BARCODE.AppearanceHeader.Options.UseTextOptions = true;
            this.HH_BARCODE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_BARCODE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_BARCODE.Caption = "Mã Hàng";
            this.HH_BARCODE.FieldName = "HH_BARCODE";
            this.HH_BARCODE.Name = "HH_BARCODE";
            this.HH_BARCODE.OptionsColumn.AllowEdit = false;
            this.HH_BARCODE.OptionsColumn.AllowFocus = false;
            this.HH_BARCODE.Width = 109;
            // 
            // HH_TENHANGHOA
            // 
            this.HH_TENHANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.HH_TENHANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_TENHANGHOA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_TENHANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HH_TENHANGHOA.AppearanceHeader.Options.UseFont = true;
            this.HH_TENHANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.HH_TENHANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_TENHANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_TENHANGHOA.Caption = "Tên Hàng";
            this.HH_TENHANGHOA.FieldName = "HH_TENHANGHOA";
            this.HH_TENHANGHOA.Name = "HH_TENHANGHOA";
            this.HH_TENHANGHOA.OptionsColumn.AllowEdit = false;
            this.HH_TENHANGHOA.OptionsColumn.AllowFocus = false;
            this.HH_TENHANGHOA.Visible = true;
            this.HH_TENHANGHOA.VisibleIndex = 1;
            this.HH_TENHANGHOA.Width = 129;
            // 
            // HH_DVT
            // 
            this.HH_DVT.AppearanceCell.Options.UseTextOptions = true;
            this.HH_DVT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_DVT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HH_DVT.AppearanceHeader.Options.UseFont = true;
            this.HH_DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.HH_DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_DVT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_DVT.Caption = "ĐV.Tính";
            this.HH_DVT.FieldName = "HH_DVT";
            this.HH_DVT.Name = "HH_DVT";
            this.HH_DVT.OptionsColumn.AllowEdit = false;
            this.HH_DVT.OptionsColumn.AllowFocus = false;
            this.HH_DVT.Visible = true;
            this.HH_DVT.VisibleIndex = 2;
            this.HH_DVT.Width = 54;
            // 
            // So_Luong
            // 
            this.So_Luong.AppearanceCell.Options.UseTextOptions = true;
            this.So_Luong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.So_Luong.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.So_Luong.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.So_Luong.AppearanceHeader.Options.UseFont = true;
            this.So_Luong.AppearanceHeader.Options.UseTextOptions = true;
            this.So_Luong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.So_Luong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.So_Luong.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.So_Luong.Caption = "SL/TL";
            this.So_Luong.DisplayFormat.FormatString = "{0:#,###0}";
            this.So_Luong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.So_Luong.FieldName = "So_Luong";
            this.So_Luong.Name = "So_Luong";
            this.So_Luong.OptionsColumn.AllowEdit = false;
            this.So_Luong.OptionsColumn.AllowFocus = false;
            this.So_Luong.Visible = true;
            this.So_Luong.VisibleIndex = 3;
            this.So_Luong.Width = 50;
            // 
            // soLuongtra
            // 
            this.soLuongtra.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.soLuongtra.AppearanceCell.Options.UseFont = true;
            this.soLuongtra.AppearanceCell.Options.UseTextOptions = true;
            this.soLuongtra.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.soLuongtra.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.soLuongtra.AppearanceHeader.Options.UseFont = true;
            this.soLuongtra.AppearanceHeader.Options.UseTextOptions = true;
            this.soLuongtra.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.soLuongtra.Caption = "Sl/TL-Trả";
            this.soLuongtra.DisplayFormat.FormatString = "{0:#,###0}";
            this.soLuongtra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.soLuongtra.FieldName = "soLuongtra";
            this.soLuongtra.Name = "soLuongtra";
            this.soLuongtra.OptionsColumn.AllowEdit = false;
            this.soLuongtra.OptionsColumn.AllowFocus = false;
            // 
            // HH_GIABANLE
            // 
            this.HH_GIABANLE.AppearanceCell.Options.UseTextOptions = true;
            this.HH_GIABANLE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_GIABANLE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_GIABANLE.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HH_GIABANLE.AppearanceHeader.Options.UseFont = true;
            this.HH_GIABANLE.AppearanceHeader.Options.UseTextOptions = true;
            this.HH_GIABANLE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_GIABANLE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HH_GIABANLE.Caption = "Đơn Giá ";
            this.HH_GIABANLE.DisplayFormat.FormatString = "{0:#,###0}";
            this.HH_GIABANLE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.HH_GIABANLE.FieldName = "HH_GIABANLE";
            this.HH_GIABANLE.Name = "HH_GIABANLE";
            this.HH_GIABANLE.OptionsColumn.AllowEdit = false;
            this.HH_GIABANLE.OptionsColumn.AllowFocus = false;
            this.HH_GIABANLE.Visible = true;
            this.HH_GIABANLE.VisibleIndex = 4;
            this.HH_GIABANLE.Width = 63;
            // 
            // Khuyen_Mai
            // 
            this.Khuyen_Mai.AppearanceCell.Options.UseTextOptions = true;
            this.Khuyen_Mai.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Khuyen_Mai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Khuyen_Mai.AppearanceHeader.Options.UseFont = true;
            this.Khuyen_Mai.AppearanceHeader.Options.UseTextOptions = true;
            this.Khuyen_Mai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Khuyen_Mai.Caption = "KM";
            this.Khuyen_Mai.DisplayFormat.FormatString = "{0:#,###0}";
            this.Khuyen_Mai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Khuyen_Mai.FieldName = "Khuyen_Mai";
            this.Khuyen_Mai.Name = "Khuyen_Mai";
            this.Khuyen_Mai.OptionsColumn.AllowEdit = false;
            this.Khuyen_Mai.OptionsColumn.AllowFocus = false;
            this.Khuyen_Mai.Visible = true;
            this.Khuyen_Mai.VisibleIndex = 5;
            this.Khuyen_Mai.Width = 56;
            // 
            // HH_VAT
            // 
            this.HH_VAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.HH_VAT.AppearanceCell.Options.UseFont = true;
            this.HH_VAT.AppearanceCell.Options.UseTextOptions = true;
            this.HH_VAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_VAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.HH_VAT.AppearanceHeader.Options.UseFont = true;
            this.HH_VAT.AppearanceHeader.Options.UseTextOptions = true;
            this.HH_VAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HH_VAT.Caption = "T- VAT";
            this.HH_VAT.FieldName = "HH_VAT";
            this.HH_VAT.Name = "HH_VAT";
            this.HH_VAT.OptionsColumn.AllowEdit = false;
            this.HH_VAT.OptionsColumn.AllowFocus = false;
            this.HH_VAT.Visible = true;
            this.HH_VAT.VisibleIndex = 6;
            this.HH_VAT.Width = 56;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 7;
            this.Thanh_Tien.Width = 115;
            // 
            // K_MAK
            // 
            this.K_MAK.Caption = "Mã Kho";
            this.K_MAK.FieldName = "K_MAK";
            this.K_MAK.Name = "K_MAK";
            // 
            // CH_MACH
            // 
            this.CH_MACH.Caption = "Mã Cửa Hàng";
            this.CH_MACH.FieldName = "CH_MACH";
            this.CH_MACH.Name = "CH_MACH";
            // 
            // Q_MAQ
            // 
            this.Q_MAQ.Caption = "Mã Q.Hàng";
            this.Q_MAQ.FieldName = "Q_MAQ";
            this.Q_MAQ.Name = "Q_MAQ";
            // 
            // STT_
            // 
            this.STT_.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT_.AppearanceCell.Options.UseFont = true;
            this.STT_.AppearanceCell.Options.UseTextOptions = true;
            this.STT_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT_.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT_.AppearanceHeader.Options.UseFont = true;
            this.STT_.AppearanceHeader.Options.UseTextOptions = true;
            this.STT_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT_.Caption = "STT";
            this.STT_.FieldName = "STT_";
            this.STT_.Name = "STT_";
            this.STT_.OptionsColumn.AllowEdit = false;
            this.STT_.OptionsColumn.AllowFocus = false;
            this.STT_.Visible = true;
            this.STT_.VisibleIndex = 0;
            this.STT_.Width = 24;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // btn_Add
            // 
            this.btn_Add.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Add.Appearance.Options.UseFont = true;
            this.btn_Add.AutoHeight = false;
            this.btn_Add.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // btn_Exc
            // 
            this.btn_Exc.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exc.Appearance.Options.UseFont = true;
            this.btn_Exc.AutoHeight = false;
            this.btn_Exc.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Exc.Name = "btn_Exc";
            this.btn_Exc.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btn_Del
            // 
            this.btn_Del.AutoHeight = false;
            this.btn_Del.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // TrungHere
            // 
            this.TrungHere.Appearance.Options.UseTextOptions = true;
            this.TrungHere.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TrungHere.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TrungHere.Name = "TrungHere";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.grCtrlDSHangMua;
            this.gridView3.Name = "gridView3";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.labelControl17);
            this.panelControl4.Controls.Add(this.lb_RowsCount);
            this.panelControl4.Controls.Add(this.lb_STT);
            this.panelControl4.Controls.Add(this.btn_up);
            this.panelControl4.Controls.Add(this.btn_Dow);
            this.panelControl4.Controls.Add(this.btn_DowLast);
            this.panelControl4.Controls.Add(this.btn_UpFirst);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(521, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(38, 147);
            this.panelControl4.TabIndex = 16;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Location = new System.Drawing.Point(7, 68);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(20, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "----";
            // 
            // lb_RowsCount
            // 
            this.lb_RowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_RowsCount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_RowsCount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_RowsCount.Location = new System.Drawing.Point(10, 80);
            this.lb_RowsCount.Name = "lb_RowsCount";
            this.lb_RowsCount.Size = new System.Drawing.Size(14, 13);
            this.lb_RowsCount.TabIndex = 6;
            this.lb_RowsCount.Text = "00";
            // 
            // lb_STT
            // 
            this.lb_STT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_STT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_STT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_STT.Location = new System.Drawing.Point(10, 54);
            this.lb_STT.Name = "lb_STT";
            this.lb_STT.Size = new System.Drawing.Size(14, 13);
            this.lb_STT.TabIndex = 5;
            this.lb_STT.Text = "00";
            // 
            // btn_up
            // 
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.Location = new System.Drawing.Point(4, 71);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(31, 53);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            // 
            // btn_Dow
            // 
            this.btn_Dow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.Location = new System.Drawing.Point(4, 24);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(31, 53);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Text = "simpleButton6";
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.Location = new System.Drawing.Point(4, 83);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(31, 53);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.Location = new System.Drawing.Point(4, 13);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(31, 53);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelThanhTien);
            this.panelControl2.Controls.Add(this.labelSoLuong);
            this.panelControl2.Controls.Add(this.lableShowTenHang);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(561, 29);
            this.panelControl2.TabIndex = 16;
            // 
            // labelThanhTien
            // 
            this.labelThanhTien.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelThanhTien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelThanhTien.Location = new System.Drawing.Point(426, 5);
            this.labelThanhTien.Name = "labelThanhTien";
            this.labelThanhTien.Size = new System.Drawing.Size(0, 19);
            this.labelThanhTien.TabIndex = 11;
            // 
            // labelSoLuong
            // 
            this.labelSoLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelSoLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelSoLuong.Location = new System.Drawing.Point(250, 5);
            this.labelSoLuong.Name = "labelSoLuong";
            this.labelSoLuong.Size = new System.Drawing.Size(0, 19);
            this.labelSoLuong.TabIndex = 10;
            // 
            // lableShowTenHang
            // 
            this.lableShowTenHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lableShowTenHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lableShowTenHang.Location = new System.Drawing.Point(23, 5);
            this.lableShowTenHang.Name = "lableShowTenHang";
            this.lableShowTenHang.Size = new System.Drawing.Size(0, 19);
            this.lableShowTenHang.TabIndex = 9;
            // 
            // pnInBaoCao
            // 
            this.pnInBaoCao.Controls.Add(this.lbTienTraLai);
            this.pnInBaoCao.Controls.Add(this.lb_TienTraLai);
            this.pnInBaoCao.Controls.Add(this.btnInBaoCao);
            this.pnInBaoCao.Controls.Add(this.lableTongDoanhThu);
            this.pnInBaoCao.Controls.Add(this.lbTongDoanhThu);
            this.pnInBaoCao.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnInBaoCao.Location = new System.Drawing.Point(2, 186);
            this.pnInBaoCao.Name = "pnInBaoCao";
            this.pnInBaoCao.Size = new System.Drawing.Size(565, 34);
            this.pnInBaoCao.TabIndex = 10;
            // 
            // lbTienTraLai
            // 
            this.lbTienTraLai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbTienTraLai.Appearance.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienTraLai.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTienTraLai.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbTienTraLai.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTienTraLai.Location = new System.Drawing.Point(315, 6);
            this.lbTienTraLai.Name = "lbTienTraLai";
            this.lbTienTraLai.Size = new System.Drawing.Size(10, 22);
            this.lbTienTraLai.TabIndex = 4;
            this.lbTienTraLai.Text = "0";
            // 
            // lb_TienTraLai
            // 
            this.lb_TienTraLai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_TienTraLai.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lb_TienTraLai.Location = new System.Drawing.Point(234, 8);
            this.lb_TienTraLai.Name = "lb_TienTraLai";
            this.lb_TienTraLai.Size = new System.Drawing.Size(76, 17);
            this.lb_TienTraLai.TabIndex = 3;
            this.lb_TienTraLai.Text = "Tiền trả lại:";
            // 
            // btnInBaoCao
            // 
            this.btnInBaoCao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInBaoCao.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnInBaoCao.Appearance.Options.UseFont = true;
            this.btnInBaoCao.Location = new System.Drawing.Point(405, 1);
            this.btnInBaoCao.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.btnInBaoCao.Name = "btnInBaoCao";
            this.btnInBaoCao.Size = new System.Drawing.Size(157, 32);
            this.btnInBaoCao.TabIndex = 2;
            this.btnInBaoCao.Text = "In Báo Cáo";
            // 
            // lableTongDoanhThu
            // 
            this.lableTongDoanhThu.Appearance.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lableTongDoanhThu.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lableTongDoanhThu.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lableTongDoanhThu.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lableTongDoanhThu.Location = new System.Drawing.Point(126, 5);
            this.lableTongDoanhThu.Name = "lableTongDoanhThu";
            this.lableTongDoanhThu.Size = new System.Drawing.Size(10, 22);
            this.lableTongDoanhThu.TabIndex = 1;
            this.lableTongDoanhThu.Text = "0";
            // 
            // lbTongDoanhThu
            // 
            this.lbTongDoanhThu.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTongDoanhThu.Location = new System.Drawing.Point(2, 8);
            this.lbTongDoanhThu.Name = "lbTongDoanhThu";
            this.lbTongDoanhThu.Size = new System.Drawing.Size(121, 17);
            this.lbTongDoanhThu.TabIndex = 0;
            this.lbTongDoanhThu.Text = "Tổng doanh thu: ";
            // 
            // pnWapperDeletePlusBut
            // 
            this.pnWapperDeletePlusBut.Controls.Add(this.btnDeleteAll);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_Cong);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_xoa);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_Tru);
            this.pnWapperDeletePlusBut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnWapperDeletePlusBut.Location = new System.Drawing.Point(2, 220);
            this.pnWapperDeletePlusBut.Name = "pnWapperDeletePlusBut";
            this.pnWapperDeletePlusBut.Size = new System.Drawing.Size(565, 39);
            this.pnWapperDeletePlusBut.TabIndex = 44;
            this.pnWapperDeletePlusBut.Visible = false;
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteAll.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnDeleteAll.Appearance.Options.UseFont = true;
            this.btnDeleteAll.Appearance.Options.UseForeColor = true;
            this.btnDeleteAll.Location = new System.Drawing.Point(4, 2);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(116, 34);
            this.btnDeleteAll.TabIndex = 9;
            this.btnDeleteAll.Text = "Xóa tất cả";
            // 
            // btn_Cong
            // 
            this.btn_Cong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cong.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cong.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Cong.Appearance.Options.UseFont = true;
            this.btn_Cong.Appearance.Options.UseForeColor = true;
            this.btn_Cong.Location = new System.Drawing.Point(332, 2);
            this.btn_Cong.Name = "btn_Cong";
            this.btn_Cong.Size = new System.Drawing.Size(109, 34);
            this.btn_Cong.TabIndex = 6;
            this.btn_Cong.Text = "+";
            // 
            // btn_xoa
            // 
            this.btn_xoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_xoa.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Appearance.Options.UseForeColor = true;
            this.btn_xoa.Location = new System.Drawing.Point(213, 2);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(116, 34);
            this.btn_xoa.TabIndex = 8;
            this.btn_xoa.Text = "Xóa";
            // 
            // btn_Tru
            // 
            this.btn_Tru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Tru.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Tru.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Tru.Appearance.Options.UseFont = true;
            this.btn_Tru.Appearance.Options.UseForeColor = true;
            this.btn_Tru.Location = new System.Drawing.Point(447, 2);
            this.btn_Tru.Name = "btn_Tru";
            this.btn_Tru.Size = new System.Drawing.Size(113, 34);
            this.btn_Tru.TabIndex = 7;
            this.btn_Tru.Text = "-";
            // 
            // panelControl8
            // 
            this.panelControl8.Appearance.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panelControl8.Appearance.Options.UseBackColor = true;
            this.panelControl8.Controls.Add(this.pn_ThanhToan1);
            this.panelControl8.Controls.Add(this.pnFuntionThanhToan);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl8.Location = new System.Drawing.Point(2, 259);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(565, 256);
            this.panelControl8.TabIndex = 35;
            // 
            // pn_ThanhToan1
            // 
            this.pn_ThanhToan1.Controls.Add(this.panelControl12);
            this.pn_ThanhToan1.Controls.Add(this.tab_Option);
            this.pn_ThanhToan1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_ThanhToan1.Location = new System.Drawing.Point(2, 2);
            this.pn_ThanhToan1.Name = "pn_ThanhToan1";
            this.pn_ThanhToan1.Size = new System.Drawing.Size(561, 252);
            this.pn_ThanhToan1.TabIndex = 11;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.pnPassWork);
            this.panelControl12.Controls.Add(this.pnMaHoaDonTraHang);
            this.panelControl12.Controls.Add(this.pnRePrinterBuil);
            this.panelControl12.Controls.Add(this.pnBaoCaoBanHang);
            this.panelControl12.Controls.Add(this.pnBanHang);
            this.panelControl12.Controls.Add(this.pnHoaDonDoi);
            this.panelControl12.Controls.Add(this.pnContainer);
            this.panelControl12.Controls.Add(this.pnInforOder);
            this.panelControl12.Controls.Add(this.pnKiemKe);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(347, 2);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(212, 248);
            this.panelControl12.TabIndex = 6;
            // 
            // pnPassWork
            // 
            this.pnPassWork.Controls.Add(this.lb_MesgasFuntion);
            this.pnPassWork.Controls.Add(this.btn_XacNhanSua);
            this.pnPassWork.Controls.Add(this.txtPassWorkConfig);
            this.pnPassWork.Controls.Add(this.labelControl31);
            this.pnPassWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnPassWork.Location = new System.Drawing.Point(2, 2);
            this.pnPassWork.Name = "pnPassWork";
            this.pnPassWork.Size = new System.Drawing.Size(208, 244);
            this.pnPassWork.TabIndex = 7;
            // 
            // lb_MesgasFuntion
            // 
            this.lb_MesgasFuntion.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_MesgasFuntion.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lb_MesgasFuntion.Location = new System.Drawing.Point(7, 92);
            this.lb_MesgasFuntion.Name = "lb_MesgasFuntion";
            this.lb_MesgasFuntion.Size = new System.Drawing.Size(254, 13);
            this.lb_MesgasFuntion.TabIndex = 3;
            this.lb_MesgasFuntion.Text = "Bạn không có quyền truy cập chức năng này !";
            // 
            // btn_XacNhanSua
            // 
            this.btn_XacNhanSua.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_XacNhanSua.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn_XacNhanSua.Appearance.Options.UseFont = true;
            this.btn_XacNhanSua.Location = new System.Drawing.Point(75, 119);
            this.btn_XacNhanSua.Name = "btn_XacNhanSua";
            this.btn_XacNhanSua.Size = new System.Drawing.Size(49, 43);
            this.btn_XacNhanSua.TabIndex = 2;
            this.btn_XacNhanSua.Text = "OK";
            // 
            // txtPassWorkConfig
            // 
            this.txtPassWorkConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassWorkConfig.Location = new System.Drawing.Point(14, 50);
            this.txtPassWorkConfig.Name = "txtPassWorkConfig";
            this.txtPassWorkConfig.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtPassWorkConfig.Properties.Appearance.Options.UseFont = true;
            this.txtPassWorkConfig.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPassWorkConfig.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPassWorkConfig.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPassWorkConfig.Properties.PasswordChar = '*';
            this.txtPassWorkConfig.Size = new System.Drawing.Size(179, 30);
            this.txtPassWorkConfig.TabIndex = 1;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl31.Location = new System.Drawing.Point(18, 23);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(107, 13);
            this.labelControl31.TabIndex = 0;
            this.labelControl31.Text = "Mật khẩu xác nhận";
            // 
            // pnMaHoaDonTraHang
            // 
            this.pnMaHoaDonTraHang.Controls.Add(this.btnQuayLai);
            this.pnMaHoaDonTraHang.Controls.Add(this.btnTraTatCa);
            this.pnMaHoaDonTraHang.Controls.Add(this.txtSoLuongTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.txtMaHangTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.labelControl13);
            this.pnMaHoaDonTraHang.Controls.Add(this.labelControl12);
            this.pnMaHoaDonTraHang.Controls.Add(this.btnTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.txtMaHoaDonTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.labelControl11);
            this.pnMaHoaDonTraHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMaHoaDonTraHang.Location = new System.Drawing.Point(2, 2);
            this.pnMaHoaDonTraHang.Name = "pnMaHoaDonTraHang";
            this.pnMaHoaDonTraHang.Size = new System.Drawing.Size(208, 244);
            this.pnMaHoaDonTraHang.TabIndex = 3;
            // 
            // btnQuayLai
            // 
            this.btnQuayLai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuayLai.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnQuayLai.Appearance.Options.UseFont = true;
            this.btnQuayLai.Location = new System.Drawing.Point(75, 191);
            this.btnQuayLai.Name = "btnQuayLai";
            this.btnQuayLai.Size = new System.Drawing.Size(118, 46);
            this.btnQuayLai.TabIndex = 6;
            this.btnQuayLai.Text = "Quay Lại";
            // 
            // btnTraTatCa
            // 
            this.btnTraTatCa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTraTatCa.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnTraTatCa.Appearance.Options.UseFont = true;
            this.btnTraTatCa.Enabled = false;
            this.btnTraTatCa.Location = new System.Drawing.Point(11, 191);
            this.btnTraTatCa.Name = "btnTraTatCa";
            this.btnTraTatCa.Size = new System.Drawing.Size(108, 46);
            this.btnTraTatCa.TabIndex = 4;
            this.btnTraTatCa.Text = "Trả Tất Cả";
            // 
            // txtSoLuongTra
            // 
            this.txtSoLuongTra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuongTra.Location = new System.Drawing.Point(68, 74);
            this.txtSoLuongTra.MinimumSize = new System.Drawing.Size(170, 30);
            this.txtSoLuongTra.Name = "txtSoLuongTra";
            this.txtSoLuongTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSoLuongTra.Properties.Appearance.Options.UseFont = true;
            this.txtSoLuongTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoLuongTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSoLuongTra.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTra.Size = new System.Drawing.Size(170, 30);
            this.txtSoLuongTra.TabIndex = 1;
            // 
            // txtMaHangTra
            // 
            this.txtMaHangTra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHangTra.Location = new System.Drawing.Point(68, 118);
            this.txtMaHangTra.MinimumSize = new System.Drawing.Size(170, 30);
            this.txtMaHangTra.Name = "txtMaHangTra";
            this.txtMaHangTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMaHangTra.Properties.Appearance.Options.UseFont = true;
            this.txtMaHangTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaHangTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMaHangTra.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtMaHangTra.Size = new System.Drawing.Size(170, 30);
            this.txtMaHangTra.TabIndex = 2;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.labelControl13.Location = new System.Drawing.Point(3, 123);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(52, 13);
            this.labelControl13.TabIndex = 5;
            this.labelControl13.Text = "Mã Hàng:";
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.labelControl12.Location = new System.Drawing.Point(3, 83);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(42, 14);
            this.labelControl12.TabIndex = 4;
            this.labelControl12.Text = "SL Trả:";
            // 
            // btnTra
            // 
            this.btnTra.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTra.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnTra.Appearance.Options.UseFont = true;
            this.btnTra.Location = new System.Drawing.Point(13, 150);
            this.btnTra.Name = "btnTra";
            this.btnTra.Size = new System.Drawing.Size(180, 34);
            this.btnTra.TabIndex = 3;
            this.btnTra.Text = "Trả";
            // 
            // txtMaHoaDonTra
            // 
            this.txtMaHoaDonTra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHoaDonTra.Location = new System.Drawing.Point(7, 29);
            this.txtMaHoaDonTra.Name = "txtMaHoaDonTra";
            this.txtMaHoaDonTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtMaHoaDonTra.Properties.Appearance.Options.UseFont = true;
            this.txtMaHoaDonTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaHoaDonTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMaHoaDonTra.Size = new System.Drawing.Size(198, 30);
            this.txtMaHoaDonTra.TabIndex = 0;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl11.Location = new System.Drawing.Point(21, 5);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(158, 17);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "Nhập mã hóa đơn bán:";
            // 
            // pnRePrinterBuil
            // 
            this.pnRePrinterBuil.Controls.Add(this.groupControl2);
            this.pnRePrinterBuil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRePrinterBuil.Location = new System.Drawing.Point(2, 2);
            this.pnRePrinterBuil.Name = "pnRePrinterBuil";
            this.pnRePrinterBuil.Size = new System.Drawing.Size(208, 244);
            this.pnRePrinterBuil.TabIndex = 6;
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Navy;
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl2.Controls.Add(this.grCtrReviewBuil);
            this.groupControl2.Controls.Add(this.panelControl25);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(204, 240);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Danh Sách Hóa Đơn";
            // 
            // grCtrReviewBuil
            // 
            this.grCtrReviewBuil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCtrReviewBuil.Location = new System.Drawing.Point(2, 24);
            this.grCtrReviewBuil.MainView = this.gvListHoaDon;
            this.grCtrReviewBuil.Name = "grCtrReviewBuil";
            this.grCtrReviewBuil.Size = new System.Drawing.Size(162, 214);
            this.grCtrReviewBuil.TabIndex = 0;
            this.grCtrReviewBuil.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListHoaDon,
            this.gridView4});
            // 
            // gvListHoaDon
            // 
            this.gvListHoaDon.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gvListHoaDon.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.gvListHoaDon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._STT,
            this._HoaDon,
            this.cl_Ngay});
            this.gvListHoaDon.GridControl = this.grCtrReviewBuil;
            this.gvListHoaDon.Name = "gvListHoaDon";
            this.gvListHoaDon.OptionsView.EnableAppearanceEvenRow = true;
            this.gvListHoaDon.OptionsView.ShowGroupPanel = false;
            this.gvListHoaDon.OptionsView.ShowIndicator = false;
            this.gvListHoaDon.RowHeight = 45;
            this.gvListHoaDon.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // _STT
            // 
            this._STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._STT.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this._STT.AppearanceCell.Options.UseFont = true;
            this._STT.AppearanceCell.Options.UseForeColor = true;
            this._STT.AppearanceCell.Options.UseTextOptions = true;
            this._STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._STT.AppearanceHeader.Options.UseFont = true;
            this._STT.AppearanceHeader.Options.UseTextOptions = true;
            this._STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._STT.Caption = "STT";
            this._STT.FieldName = "cl_STT";
            this._STT.Name = "_STT";
            this._STT.OptionsColumn.AllowEdit = false;
            this._STT.OptionsColumn.AllowFocus = false;
            this._STT.Visible = true;
            this._STT.VisibleIndex = 0;
            this._STT.Width = 33;
            // 
            // _HoaDon
            // 
            this._HoaDon.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._HoaDon.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this._HoaDon.AppearanceCell.Options.UseFont = true;
            this._HoaDon.AppearanceCell.Options.UseForeColor = true;
            this._HoaDon.AppearanceCell.Options.UseTextOptions = true;
            this._HoaDon.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._HoaDon.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._HoaDon.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._HoaDon.AppearanceHeader.Options.UseFont = true;
            this._HoaDon.AppearanceHeader.Options.UseTextOptions = true;
            this._HoaDon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._HoaDon.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._HoaDon.Caption = "Hóa Đơn";
            this._HoaDon.FieldName = "Ma_HoaDon";
            this._HoaDon.Name = "_HoaDon";
            this._HoaDon.OptionsColumn.AllowEdit = false;
            this._HoaDon.OptionsColumn.AllowFocus = false;
            this._HoaDon.Visible = true;
            this._HoaDon.VisibleIndex = 1;
            this._HoaDon.Width = 91;
            // 
            // cl_Ngay
            // 
            this.cl_Ngay.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cl_Ngay.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.cl_Ngay.AppearanceCell.Options.UseFont = true;
            this.cl_Ngay.AppearanceCell.Options.UseForeColor = true;
            this.cl_Ngay.AppearanceCell.Options.UseTextOptions = true;
            this.cl_Ngay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_Ngay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_Ngay.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cl_Ngay.AppearanceHeader.Options.UseFont = true;
            this.cl_Ngay.AppearanceHeader.Options.UseTextOptions = true;
            this.cl_Ngay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_Ngay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_Ngay.Caption = "Ngày Tạo";
            this.cl_Ngay.FieldName = "Ngay_Ban";
            this.cl_Ngay.Name = "cl_Ngay";
            this.cl_Ngay.OptionsColumn.AllowEdit = false;
            this.cl_Ngay.OptionsColumn.AllowFocus = false;
            this.cl_Ngay.Visible = true;
            this.cl_Ngay.VisibleIndex = 2;
            this.cl_Ngay.Width = 122;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grCtrReviewBuil;
            this.gridView4.Name = "gridView4";
            // 
            // panelControl25
            // 
            this.panelControl25.Controls.Add(this.btnUpListHd);
            this.panelControl25.Controls.Add(this.btnDowListHd);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl25.Location = new System.Drawing.Point(164, 24);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(38, 214);
            this.panelControl25.TabIndex = 3;
            // 
            // btnUpListHd
            // 
            this.btnUpListHd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnUpListHd.Image = ((System.Drawing.Image)(resources.GetObject("btnUpListHd.Image")));
            this.btnUpListHd.Location = new System.Drawing.Point(4, 6);
            this.btnUpListHd.Name = "btnUpListHd";
            this.btnUpListHd.Size = new System.Drawing.Size(31, 69);
            this.btnUpListHd.TabIndex = 9;
            this.btnUpListHd.Text = "simpleButton7";
            // 
            // btnDowListHd
            // 
            this.btnDowListHd.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDowListHd.Image = ((System.Drawing.Image)(resources.GetObject("btnDowListHd.Image")));
            this.btnDowListHd.Location = new System.Drawing.Point(4, 140);
            this.btnDowListHd.Name = "btnDowListHd";
            this.btnDowListHd.Size = new System.Drawing.Size(31, 69);
            this.btnDowListHd.TabIndex = 8;
            this.btnDowListHd.Text = "simpleButton6";
            // 
            // pnBaoCaoBanHang
            // 
            this.pnBaoCaoBanHang.Controls.Add(this.btnOkBaoCaoBanH);
            this.pnBaoCaoBanHang.Controls.Add(this.labelControl16);
            this.pnBaoCaoBanHang.Controls.Add(this.labelControl15);
            this.pnBaoCaoBanHang.Controls.Add(this.dt_denngay);
            this.pnBaoCaoBanHang.Controls.Add(this.dt_tungay);
            this.pnBaoCaoBanHang.Controls.Add(this.btnXemNgayKhac);
            this.pnBaoCaoBanHang.Controls.Add(this.btnXemTrongNgay);
            this.pnBaoCaoBanHang.Controls.Add(this.labelControl14);
            this.pnBaoCaoBanHang.Controls.Add(this.txtMaHangBaoCao);
            this.pnBaoCaoBanHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBaoCaoBanHang.Location = new System.Drawing.Point(2, 2);
            this.pnBaoCaoBanHang.Name = "pnBaoCaoBanHang";
            this.pnBaoCaoBanHang.Size = new System.Drawing.Size(208, 244);
            this.pnBaoCaoBanHang.TabIndex = 3;
            // 
            // btnOkBaoCaoBanH
            // 
            this.btnOkBaoCaoBanH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOkBaoCaoBanH.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnOkBaoCaoBanH.Appearance.Options.UseFont = true;
            this.btnOkBaoCaoBanH.Image = ((System.Drawing.Image)(resources.GetObject("btnOkBaoCaoBanH.Image")));
            this.btnOkBaoCaoBanH.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnOkBaoCaoBanH.Location = new System.Drawing.Point(90, 203);
            this.btnOkBaoCaoBanH.Name = "btnOkBaoCaoBanH";
            this.btnOkBaoCaoBanH.Size = new System.Drawing.Size(41, 41);
            this.btnOkBaoCaoBanH.TabIndex = 10;
            this.btnOkBaoCaoBanH.Text = "OK";
            // 
            // labelControl16
            // 
            this.labelControl16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl16.Location = new System.Drawing.Point(7, 172);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(71, 17);
            this.labelControl16.TabIndex = 9;
            this.labelControl16.Text = "Đến ngày:";
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl15.Location = new System.Drawing.Point(7, 132);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(62, 17);
            this.labelControl15.TabIndex = 8;
            this.labelControl15.Text = "Từ ngày:";
            // 
            // dt_denngay
            // 
            this.dt_denngay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dt_denngay.EditValue = null;
            this.dt_denngay.EnterMoveNextControl = true;
            this.dt_denngay.Location = new System.Drawing.Point(79, 164);
            this.dt_denngay.Name = "dt_denngay";
            this.dt_denngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.dt_denngay.Properties.Appearance.Options.UseFont = true;
            this.dt_denngay.Properties.Appearance.Options.UseTextOptions = true;
            this.dt_denngay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dt_denngay.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.dt_denngay.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.dt_denngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_denngay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.dt_denngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dt_denngay.Size = new System.Drawing.Size(122, 20);
            this.dt_denngay.TabIndex = 7;
            // 
            // dt_tungay
            // 
            this.dt_tungay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dt_tungay.EditValue = null;
            this.dt_tungay.Location = new System.Drawing.Point(79, 122);
            this.dt_tungay.Name = "dt_tungay";
            this.dt_tungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.dt_tungay.Properties.Appearance.Options.UseFont = true;
            this.dt_tungay.Properties.Appearance.Options.UseTextOptions = true;
            this.dt_tungay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dt_tungay.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.dt_tungay.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.dt_tungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_tungay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.dt_tungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dt_tungay.Size = new System.Drawing.Size(122, 20);
            this.dt_tungay.TabIndex = 6;
            // 
            // btnXemNgayKhac
            // 
            this.btnXemNgayKhac.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnXemNgayKhac.Appearance.Options.UseFont = true;
            this.btnXemNgayKhac.Location = new System.Drawing.Point(147, 69);
            this.btnXemNgayKhac.Name = "btnXemNgayKhac";
            this.btnXemNgayKhac.Size = new System.Drawing.Size(104, 37);
            this.btnXemNgayKhac.TabIndex = 3;
            this.btnXemNgayKhac.Text = "Xem ngày khác";
            // 
            // btnXemTrongNgay
            // 
            this.btnXemTrongNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnXemTrongNgay.Appearance.Options.UseFont = true;
            this.btnXemTrongNgay.Location = new System.Drawing.Point(7, 69);
            this.btnXemTrongNgay.Name = "btnXemTrongNgay";
            this.btnXemTrongNgay.Size = new System.Drawing.Size(106, 37);
            this.btnXemTrongNgay.TabIndex = 2;
            this.btnXemTrongNgay.Text = "Hôm nay";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl14.Location = new System.Drawing.Point(7, 7);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(61, 16);
            this.labelControl14.TabIndex = 1;
            this.labelControl14.Text = "Mã Hàng:";
            // 
            // txtMaHangBaoCao
            // 
            this.txtMaHangBaoCao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHangBaoCao.Location = new System.Drawing.Point(7, 29);
            this.txtMaHangBaoCao.Name = "txtMaHangBaoCao";
            this.txtMaHangBaoCao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtMaHangBaoCao.Properties.Appearance.Options.UseFont = true;
            this.txtMaHangBaoCao.Size = new System.Drawing.Size(194, 30);
            this.txtMaHangBaoCao.TabIndex = 0;
            // 
            // pnBanHang
            // 
            this.pnBanHang.Controls.Add(this.lbTienTruocThue);
            this.pnBanHang.Controls.Add(this.labelControl20);
            this.pnBanHang.Controls.Add(this.labelThueVAT);
            this.pnBanHang.Controls.Add(this.label_GiamGia);
            this.pnBanHang.Controls.Add(this.labelControl1);
            this.pnBanHang.Controls.Add(this.labelControl3);
            this.pnBanHang.Controls.Add(this.txt_soluong);
            this.pnBanHang.Controls.Add(this.labelControl4);
            this.pnBanHang.Controls.Add(this.labelControl7);
            this.pnBanHang.Controls.Add(this.lb_tongcong);
            this.pnBanHang.Controls.Add(this.txt_Mahang);
            this.pnBanHang.Controls.Add(this.labelControl8);
            this.pnBanHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBanHang.Location = new System.Drawing.Point(2, 2);
            this.pnBanHang.Name = "pnBanHang";
            this.pnBanHang.Size = new System.Drawing.Size(208, 244);
            this.pnBanHang.TabIndex = 12;
            // 
            // lbTienTruocThue
            // 
            this.lbTienTruocThue.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTienTruocThue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbTienTruocThue.Location = new System.Drawing.Point(103, 159);
            this.lbTienTruocThue.Name = "lbTienTruocThue";
            this.lbTienTruocThue.Size = new System.Drawing.Size(38, 16);
            this.lbTienTruocThue.TabIndex = 7;
            this.lbTienTruocThue.Text = "0 VND";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl20.Location = new System.Drawing.Point(9, 160);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(84, 13);
            this.labelControl20.TabIndex = 6;
            this.labelControl20.Text = "TC Trước Thuế:";
            // 
            // labelThueVAT
            // 
            this.labelThueVAT.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelThueVAT.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelThueVAT.Location = new System.Drawing.Point(79, 127);
            this.labelThueVAT.Name = "labelThueVAT";
            this.labelThueVAT.Size = new System.Drawing.Size(38, 16);
            this.labelThueVAT.TabIndex = 5;
            this.labelThueVAT.Text = "0 VND";
            // 
            // label_GiamGia
            // 
            this.label_GiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label_GiamGia.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label_GiamGia.Location = new System.Drawing.Point(79, 93);
            this.label_GiamGia.Name = "label_GiamGia";
            this.label_GiamGia.Size = new System.Drawing.Size(38, 16);
            this.label_GiamGia.TabIndex = 4;
            this.label_GiamGia.Text = "0 VND";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(9, 93);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Giảm Giá:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(9, 127);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(56, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Thuế VAT:";
            // 
            // txt_soluong
            // 
            this.txt_soluong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_soluong.Location = new System.Drawing.Point(67, 4);
            this.txt_soluong.MinimumSize = new System.Drawing.Size(170, 30);
            this.txt_soluong.Name = "txt_soluong";
            this.txt_soluong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt_soluong.Properties.Appearance.Options.UseFont = true;
            this.txt_soluong.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_soluong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_soluong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_soluong.Size = new System.Drawing.Size(170, 30);
            this.txt_soluong.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(4, 195);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(98, 19);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Tổng Cộng: ";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(9, 51);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(49, 13);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Mã Hàng";
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_tongcong.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tongcong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lb_tongcong.Location = new System.Drawing.Point(107, 193);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Size = new System.Drawing.Size(69, 24);
            this.lb_tongcong.TabIndex = 1;
            this.lb_tongcong.Text = "0  VND";
            // 
            // txt_Mahang
            // 
            this.txt_Mahang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Mahang.Location = new System.Drawing.Point(67, 43);
            this.txt_Mahang.MinimumSize = new System.Drawing.Size(170, 30);
            this.txt_Mahang.Name = "txt_Mahang";
            this.txt_Mahang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt_Mahang.Properties.Appearance.Options.UseFont = true;
            this.txt_Mahang.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_Mahang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_Mahang.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_Mahang.Size = new System.Drawing.Size(170, 30);
            this.txt_Mahang.TabIndex = 1;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(9, 14);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(52, 13);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Số Lượng";
            // 
            // pnHoaDonDoi
            // 
            this.pnHoaDonDoi.Controls.Add(this.groupControl1);
            this.pnHoaDonDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHoaDonDoi.Location = new System.Drawing.Point(2, 2);
            this.pnHoaDonDoi.Name = "pnHoaDonDoi";
            this.pnHoaDonDoi.Size = new System.Drawing.Size(208, 244);
            this.pnHoaDonDoi.TabIndex = 6;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Navy;
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.gr_listhoadon);
            this.groupControl1.Controls.Add(this.panelControl22);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(204, 240);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Danh Sách Hóa Đơn Đợi";
            // 
            // gr_listhoadon
            // 
            this.gr_listhoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_listhoadon.Location = new System.Drawing.Point(2, 24);
            this.gr_listhoadon.MainView = this.gv_listhoadon;
            this.gr_listhoadon.Name = "gr_listhoadon";
            this.gr_listhoadon.Size = new System.Drawing.Size(162, 214);
            this.gr_listhoadon.TabIndex = 1;
            this.gr_listhoadon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_listhoadon,
            this.gridView5});
            // 
            // gv_listhoadon
            // 
            this.gv_listhoadon.Appearance.Row.Options.UseTextOptions = true;
            this.gv_listhoadon.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gv_listhoadon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cl_STT,
            this.cl_HoaDon});
            this.gv_listhoadon.GridControl = this.gr_listhoadon;
            this.gv_listhoadon.Name = "gv_listhoadon";
            this.gv_listhoadon.OptionsView.EnableAppearanceEvenRow = true;
            this.gv_listhoadon.OptionsView.ShowGroupPanel = false;
            this.gv_listhoadon.OptionsView.ShowIndicator = false;
            this.gv_listhoadon.RowHeight = 30;
            this.gv_listhoadon.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // cl_STT
            // 
            this.cl_STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.cl_STT.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.cl_STT.AppearanceCell.Options.UseFont = true;
            this.cl_STT.AppearanceCell.Options.UseForeColor = true;
            this.cl_STT.AppearanceCell.Options.UseTextOptions = true;
            this.cl_STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cl_STT.AppearanceHeader.Options.UseFont = true;
            this.cl_STT.AppearanceHeader.Options.UseTextOptions = true;
            this.cl_STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_STT.Caption = "STT";
            this.cl_STT.FieldName = "cl_STT";
            this.cl_STT.Name = "cl_STT";
            this.cl_STT.OptionsColumn.AllowEdit = false;
            this.cl_STT.OptionsColumn.AllowFocus = false;
            this.cl_STT.Visible = true;
            this.cl_STT.VisibleIndex = 0;
            this.cl_STT.Width = 59;
            // 
            // cl_HoaDon
            // 
            this.cl_HoaDon.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.cl_HoaDon.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.cl_HoaDon.AppearanceCell.Options.UseFont = true;
            this.cl_HoaDon.AppearanceCell.Options.UseForeColor = true;
            this.cl_HoaDon.AppearanceCell.Options.UseTextOptions = true;
            this.cl_HoaDon.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_HoaDon.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_HoaDon.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cl_HoaDon.AppearanceHeader.Options.UseFont = true;
            this.cl_HoaDon.AppearanceHeader.Options.UseTextOptions = true;
            this.cl_HoaDon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cl_HoaDon.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.cl_HoaDon.Caption = "Hóa Đơn";
            this.cl_HoaDon.FieldName = "cl_HoaDon";
            this.cl_HoaDon.Name = "cl_HoaDon";
            this.cl_HoaDon.OptionsColumn.AllowEdit = false;
            this.cl_HoaDon.OptionsColumn.AllowFocus = false;
            this.cl_HoaDon.Visible = true;
            this.cl_HoaDon.VisibleIndex = 1;
            this.cl_HoaDon.Width = 176;
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.gr_listhoadon;
            this.gridView5.Name = "gridView5";
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.btnUpBill);
            this.panelControl22.Controls.Add(this.btnDowBill);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl22.Location = new System.Drawing.Point(164, 24);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(38, 214);
            this.panelControl22.TabIndex = 2;
            // 
            // btnUpBill
            // 
            this.btnUpBill.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnUpBill.Image = ((System.Drawing.Image)(resources.GetObject("btnUpBill.Image")));
            this.btnUpBill.Location = new System.Drawing.Point(4, 6);
            this.btnUpBill.Name = "btnUpBill";
            this.btnUpBill.Size = new System.Drawing.Size(31, 69);
            this.btnUpBill.TabIndex = 9;
            this.btnUpBill.Text = "simpleButton7";
            // 
            // btnDowBill
            // 
            this.btnDowBill.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDowBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowBill.Image")));
            this.btnDowBill.Location = new System.Drawing.Point(4, 140);
            this.btnDowBill.Name = "btnDowBill";
            this.btnDowBill.Size = new System.Drawing.Size(31, 69);
            this.btnDowBill.TabIndex = 8;
            this.btnDowBill.Text = "simpleButton6";
            // 
            // pnContainer
            // 
            this.pnContainer.Controls.Add(this.drSearchKho);
            this.pnContainer.Controls.Add(this.labelControl23);
            this.pnContainer.Controls.Add(this.lbmahang);
            this.pnContainer.Controls.Add(this.txtSearchMaHang);
            this.pnContainer.Controls.Add(this.labelControl21);
            this.pnContainer.Controls.Add(this.btnSearchTonKho);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(2, 2);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.Size = new System.Drawing.Size(208, 244);
            this.pnContainer.TabIndex = 6;
            // 
            // drSearchKho
            // 
            this.drSearchKho.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.drSearchKho.Location = new System.Drawing.Point(68, 49);
            this.drSearchKho.Name = "drSearchKho";
            this.drSearchKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.drSearchKho.Properties.Appearance.Options.UseFont = true;
            this.drSearchKho.Properties.AutoSearchColumnIndex = 1;
            this.drSearchKho.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.drSearchKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.drSearchKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("K_IDKHO", "Mã Kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("K_TENK", "Tên Kho")});
            this.drSearchKho.Properties.DisplayMember = "K_TENK";
            this.drSearchKho.Properties.NullText = "";
            this.drSearchKho.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.drSearchKho.Properties.ValueMember = "K_IDKHO";
            this.drSearchKho.Size = new System.Drawing.Size(130, 30);
            this.drSearchKho.TabIndex = 6;
            // 
            // labelControl23
            // 
            this.labelControl23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelControl23.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl23.Location = new System.Drawing.Point(21, 13);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(173, 13);
            this.labelControl23.TabIndex = 5;
            this.labelControl23.Text = "Nhập thông tin để xem tồn kho";
            // 
            // lbmahang
            // 
            this.lbmahang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbmahang.Location = new System.Drawing.Point(4, 104);
            this.lbmahang.Name = "lbmahang";
            this.lbmahang.Size = new System.Drawing.Size(55, 13);
            this.lbmahang.TabIndex = 4;
            this.lbmahang.Text = "Mã Hàng :";
            // 
            // txtSearchMaHang
            // 
            this.txtSearchMaHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchMaHang.Location = new System.Drawing.Point(67, 96);
            this.txtSearchMaHang.Name = "txtSearchMaHang";
            this.txtSearchMaHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtSearchMaHang.Properties.Appearance.Options.UseFont = true;
            this.txtSearchMaHang.Size = new System.Drawing.Size(132, 30);
            this.txtSearchMaHang.TabIndex = 2;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl21.Location = new System.Drawing.Point(2, 57);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(61, 13);
            this.labelControl21.TabIndex = 1;
            this.labelControl21.Text = "Chọn  kho :";
            // 
            // btnSearchTonKho
            // 
            this.btnSearchTonKho.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchTonKho.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnSearchTonKho.Appearance.Options.UseFont = true;
            this.btnSearchTonKho.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTonKho.Image")));
            this.btnSearchTonKho.Location = new System.Drawing.Point(56, 164);
            this.btnSearchTonKho.Name = "btnSearchTonKho";
            this.btnSearchTonKho.Size = new System.Drawing.Size(104, 49);
            this.btnSearchTonKho.TabIndex = 0;
            this.btnSearchTonKho.Text = "Xem ";
            // 
            // pnInforOder
            // 
            this.pnInforOder.Controls.Add(this.btn_TraCuuKH);
            this.pnInforOder.Controls.Add(this.labelControl10);
            this.pnInforOder.Controls.Add(this.labelControl9);
            this.pnInforOder.Controls.Add(this.labelControl18);
            this.pnInforOder.Controls.Add(this.labelControl19);
            this.pnInforOder.Controls.Add(this.txtIdProduct);
            this.pnInforOder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInforOder.Location = new System.Drawing.Point(2, 2);
            this.pnInforOder.Name = "pnInforOder";
            this.pnInforOder.Size = new System.Drawing.Size(208, 244);
            this.pnInforOder.TabIndex = 6;
            // 
            // btn_TraCuuKH
            // 
            this.btn_TraCuuKH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_TraCuuKH.Location = new System.Drawing.Point(79, 72);
            this.btn_TraCuuKH.Name = "btn_TraCuuKH";
            this.btn_TraCuuKH.Size = new System.Drawing.Size(37, 38);
            this.btn_TraCuuKH.TabIndex = 5;
            this.btn_TraCuuKH.Text = "OK";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(11, 216);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(65, 13);
            this.labelControl10.TabIndex = 4;
            this.labelControl10.Text = "Điểm tích lũy:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(9, 169);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(66, 13);
            this.labelControl9.TabIndex = 3;
            this.labelControl9.Text = "Số điện thoại:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(9, 126);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(36, 13);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "Họ tên:";
            // 
            // labelControl19
            // 
            this.labelControl19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl19.Location = new System.Drawing.Point(17, 9);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(149, 17);
            this.labelControl19.TabIndex = 1;
            this.labelControl19.Text = "Nhập Mã Khách Hàng";
            // 
            // txtIdProduct
            // 
            this.txtIdProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIdProduct.Location = new System.Drawing.Point(9, 38);
            this.txtIdProduct.Name = "txtIdProduct";
            this.txtIdProduct.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtIdProduct.Properties.Appearance.Options.UseFont = true;
            this.txtIdProduct.Size = new System.Drawing.Size(194, 30);
            this.txtIdProduct.TabIndex = 0;
            // 
            // pnKiemKe
            // 
            this.pnKiemKe.Controls.Add(this.btn_LayKK);
            this.pnKiemKe.Controls.Add(this.btnLuuTamFile);
            this.pnKiemKe.Controls.Add(this.btnSeverFile);
            this.pnKiemKe.Controls.Add(this.txtMaHangKK);
            this.pnKiemKe.Controls.Add(this.txtSoLuongKK);
            this.pnKiemKe.Controls.Add(this.labelControl22);
            this.pnKiemKe.Controls.Add(this.labelControl24);
            this.pnKiemKe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKiemKe.Location = new System.Drawing.Point(2, 2);
            this.pnKiemKe.Name = "pnKiemKe";
            this.pnKiemKe.Size = new System.Drawing.Size(208, 244);
            this.pnKiemKe.TabIndex = 2;
            // 
            // btn_LayKK
            // 
            this.btn_LayKK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_LayKK.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_LayKK.Appearance.Options.UseFont = true;
            this.btn_LayKK.Location = new System.Drawing.Point(11, 132);
            this.btn_LayKK.Name = "btn_LayKK";
            this.btn_LayKK.Size = new System.Drawing.Size(112, 39);
            this.btn_LayKK.TabIndex = 6;
            this.btn_LayKK.Text = "Lấy File Kiểm Kê";
            // 
            // btnLuuTamFile
            // 
            this.btnLuuTamFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuuTamFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnLuuTamFile.Appearance.Options.UseFont = true;
            this.btnLuuTamFile.Location = new System.Drawing.Point(92, 132);
            this.btnLuuTamFile.Name = "btnLuuTamFile";
            this.btnLuuTamFile.Size = new System.Drawing.Size(102, 39);
            this.btnLuuTamFile.TabIndex = 5;
            this.btnLuuTamFile.Text = "Lưu Tạm";
            // 
            // btnSeverFile
            // 
            this.btnSeverFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSeverFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSeverFile.Appearance.Options.UseFont = true;
            this.btnSeverFile.Location = new System.Drawing.Point(11, 191);
            this.btnSeverFile.Name = "btnSeverFile";
            this.btnSeverFile.Size = new System.Drawing.Size(182, 39);
            this.btnSeverFile.TabIndex = 4;
            this.btnSeverFile.Text = "Xuất Ra File Excel";
            // 
            // txtMaHangKK
            // 
            this.txtMaHangKK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHangKK.Location = new System.Drawing.Point(90, 65);
            this.txtMaHangKK.Name = "txtMaHangKK";
            this.txtMaHangKK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtMaHangKK.Properties.Appearance.Options.UseFont = true;
            this.txtMaHangKK.Size = new System.Drawing.Size(113, 32);
            this.txtMaHangKK.TabIndex = 3;
            // 
            // txtSoLuongKK
            // 
            this.txtSoLuongKK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuongKK.Location = new System.Drawing.Point(90, 14);
            this.txtSoLuongKK.Name = "txtSoLuongKK";
            this.txtSoLuongKK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtSoLuongKK.Properties.Appearance.Options.UseFont = true;
            this.txtSoLuongKK.Size = new System.Drawing.Size(113, 32);
            this.txtSoLuongKK.TabIndex = 2;
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl22.Location = new System.Drawing.Point(11, 77);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(61, 16);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Mã Hàng:";
            // 
            // labelControl24
            // 
            this.labelControl24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl24.Location = new System.Drawing.Point(11, 27);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(72, 17);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "Số Lượng:";
            // 
            // tab_Option
            // 
            this.tab_Option.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tab_Option.Appearance.Options.UseFont = true;
            this.tab_Option.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.tab_Option.AppearancePage.Header.Options.UseFont = true;
            this.tab_Option.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 16F);
            this.tab_Option.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tab_Option.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tab_Option.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.tab_Option.Dock = System.Windows.Forms.DockStyle.Left;
            this.tab_Option.Location = new System.Drawing.Point(2, 2);
            this.tab_Option.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.tab_Option.Name = "tab_Option";
            this.tab_Option.SelectedTabPage = this.tabThuNgan;
            this.tab_Option.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.True;
            this.tab_Option.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tab_Option.Size = new System.Drawing.Size(345, 248);
            this.tab_Option.TabIndex = 1;
            this.tab_Option.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabNghiepVu,
            this.tabThuNgan});
            // 
            // tabThuNgan
            // 
            this.tabThuNgan.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tabThuNgan.Appearance.Header.Options.UseFont = true;
            this.tabThuNgan.Appearance.Header.Options.UseTextOptions = true;
            this.tabThuNgan.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tabThuNgan.Controls.Add(this.simpleButton3);
            this.tabThuNgan.Controls.Add(this.btnByProduct);
            this.tabThuNgan.Controls.Add(this.btnCashDrawer);
            this.tabThuNgan.Controls.Add(this.btnLuuHoaDonDoi);
            this.tabThuNgan.Controls.Add(this.btnLayHoaDonDoi);
            this.tabThuNgan.Controls.Add(this.btnKhachHang);
            this.tabThuNgan.Controls.Add(this.btnTraHang);
            this.tabThuNgan.Name = "tabThuNgan";
            this.tabThuNgan.Size = new System.Drawing.Size(339, 216);
            this.tabThuNgan.Text = "Thu Ngân";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.simpleButton3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(14, 111);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(143, 46);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "In Lại Hóa Đơn";
            // 
            // btnByProduct
            // 
            this.btnByProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnByProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnByProduct.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnByProduct.Appearance.Options.UseFont = true;
            this.btnByProduct.Appearance.Options.UseForeColor = true;
            this.btnByProduct.Image = ((System.Drawing.Image)(resources.GetObject("btnByProduct.Image")));
            this.btnByProduct.Location = new System.Drawing.Point(185, 11);
            this.btnByProduct.Margin = new System.Windows.Forms.Padding(0);
            this.btnByProduct.Name = "btnByProduct";
            this.btnByProduct.Size = new System.Drawing.Size(143, 95);
            this.btnByProduct.TabIndex = 4;
            this.btnByProduct.Text = "Bán Hàng";
            // 
            // btnCashDrawer
            // 
            this.btnCashDrawer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCashDrawer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCashDrawer.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnCashDrawer.Appearance.Options.UseFont = true;
            this.btnCashDrawer.Appearance.Options.UseForeColor = true;
            this.btnCashDrawer.Image = ((System.Drawing.Image)(resources.GetObject("btnCashDrawer.Image")));
            this.btnCashDrawer.Location = new System.Drawing.Point(185, 112);
            this.btnCashDrawer.Margin = new System.Windows.Forms.Padding(0);
            this.btnCashDrawer.Name = "btnCashDrawer";
            this.btnCashDrawer.Size = new System.Drawing.Size(143, 46);
            this.btnCashDrawer.TabIndex = 5;
            this.btnCashDrawer.Text = "Cash Drawer";
            // 
            // btnLuuHoaDonDoi
            // 
            this.btnLuuHoaDonDoi.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuHoaDonDoi.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnLuuHoaDonDoi.Appearance.Options.UseFont = true;
            this.btnLuuHoaDonDoi.Appearance.Options.UseForeColor = true;
            this.btnLuuHoaDonDoi.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuHoaDonDoi.Image")));
            this.btnLuuHoaDonDoi.Location = new System.Drawing.Point(14, 10);
            this.btnLuuHoaDonDoi.Margin = new System.Windows.Forms.Padding(0);
            this.btnLuuHoaDonDoi.Name = "btnLuuHoaDonDoi";
            this.btnLuuHoaDonDoi.Size = new System.Drawing.Size(143, 46);
            this.btnLuuHoaDonDoi.TabIndex = 0;
            this.btnLuuHoaDonDoi.Text = "Lưu Hóa Đơn Đợi";
            // 
            // btnLayHoaDonDoi
            // 
            this.btnLayHoaDonDoi.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayHoaDonDoi.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnLayHoaDonDoi.Appearance.Options.UseFont = true;
            this.btnLayHoaDonDoi.Appearance.Options.UseForeColor = true;
            this.btnLayHoaDonDoi.Image = ((System.Drawing.Image)(resources.GetObject("btnLayHoaDonDoi.Image")));
            this.btnLayHoaDonDoi.Location = new System.Drawing.Point(14, 60);
            this.btnLayHoaDonDoi.Margin = new System.Windows.Forms.Padding(0);
            this.btnLayHoaDonDoi.Name = "btnLayHoaDonDoi";
            this.btnLayHoaDonDoi.Size = new System.Drawing.Size(143, 46);
            this.btnLayHoaDonDoi.TabIndex = 1;
            this.btnLayHoaDonDoi.Text = "Lấy Hóa Đơn Đợi";
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhachHang.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnKhachHang.Appearance.Options.UseFont = true;
            this.btnKhachHang.Appearance.Options.UseForeColor = true;
            this.btnKhachHang.Image = ((System.Drawing.Image)(resources.GetObject("btnKhachHang.Image")));
            this.btnKhachHang.Location = new System.Drawing.Point(14, 164);
            this.btnKhachHang.Margin = new System.Windows.Forms.Padding(0);
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.Size = new System.Drawing.Size(143, 46);
            this.btnKhachHang.TabIndex = 3;
            this.btnKhachHang.Text = "Khác Hàng";
            // 
            // btnTraHang
            // 
            this.btnTraHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTraHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraHang.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnTraHang.Appearance.Options.UseFont = true;
            this.btnTraHang.Appearance.Options.UseForeColor = true;
            this.btnTraHang.Image = ((System.Drawing.Image)(resources.GetObject("btnTraHang.Image")));
            this.btnTraHang.Location = new System.Drawing.Point(185, 165);
            this.btnTraHang.Margin = new System.Windows.Forms.Padding(0);
            this.btnTraHang.Name = "btnTraHang";
            this.btnTraHang.Size = new System.Drawing.Size(143, 46);
            this.btnTraHang.TabIndex = 6;
            this.btnTraHang.Text = "Trả Hàng";
            // 
            // tabNghiepVu
            // 
            this.tabNghiepVu.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tabNghiepVu.Appearance.Header.Options.UseFont = true;
            this.tabNghiepVu.Appearance.Header.Options.UseTextOptions = true;
            this.tabNghiepVu.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tabNghiepVu.Controls.Add(this.btnKetCa);
            this.tabNghiepVu.Controls.Add(this.btnXemTonKho);
            this.tabNghiepVu.Controls.Add(this.btnThoat);
            this.tabNghiepVu.Controls.Add(this.btnKiemKe);
            this.tabNghiepVu.Controls.Add(this.btnLogoOut);
            this.tabNghiepVu.Controls.Add(this.btnHelp);
            this.tabNghiepVu.Controls.Add(this.btnBaoCaoTraHang);
            this.tabNghiepVu.Controls.Add(this.btnBaoCaoBanHang);
            this.tabNghiepVu.Name = "tabNghiepVu";
            this.tabNghiepVu.Size = new System.Drawing.Size(339, 216);
            this.tabNghiepVu.Text = "Nghiệp Vụ";
            // 
            // btnKetCa
            // 
            this.btnKetCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetCa.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnKetCa.Appearance.Options.UseFont = true;
            this.btnKetCa.Appearance.Options.UseForeColor = true;
            this.btnKetCa.Image = ((System.Drawing.Image)(resources.GetObject("btnKetCa.Image")));
            this.btnKetCa.Location = new System.Drawing.Point(11, 7);
            this.btnKetCa.Margin = new System.Windows.Forms.Padding(0);
            this.btnKetCa.Name = "btnKetCa";
            this.btnKetCa.Size = new System.Drawing.Size(146, 46);
            this.btnKetCa.TabIndex = 9;
            this.btnKetCa.Text = "Kết Ca";
            // 
            // btnXemTonKho
            // 
            this.btnXemTonKho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemTonKho.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnXemTonKho.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnXemTonKho.Appearance.Options.UseFont = true;
            this.btnXemTonKho.Appearance.Options.UseForeColor = true;
            this.btnXemTonKho.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTonKho.Image")));
            this.btnXemTonKho.Location = new System.Drawing.Point(180, 113);
            this.btnXemTonKho.Margin = new System.Windows.Forms.Padding(0);
            this.btnXemTonKho.Name = "btnXemTonKho";
            this.btnXemTonKho.Size = new System.Drawing.Size(142, 46);
            this.btnXemTonKho.TabIndex = 8;
            this.btnXemTonKho.Text = "Xem Tồn Kho";
            // 
            // btnThoat
            // 
            this.btnThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnThoat.Appearance.Options.UseFont = true;
            this.btnThoat.Appearance.Options.UseForeColor = true;
            this.btnThoat.Image = ((System.Drawing.Image)(resources.GetObject("btnThoat.Image")));
            this.btnThoat.Location = new System.Drawing.Point(11, 167);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(0);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(146, 45);
            this.btnThoat.TabIndex = 7;
            this.btnThoat.Text = "Thoát";
            // 
            // btnKiemKe
            // 
            this.btnKiemKe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKiemKe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnKiemKe.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnKiemKe.Appearance.Options.UseFont = true;
            this.btnKiemKe.Appearance.Options.UseForeColor = true;
            this.btnKiemKe.Image = ((System.Drawing.Image)(resources.GetObject("btnKiemKe.Image")));
            this.btnKiemKe.Location = new System.Drawing.Point(180, 165);
            this.btnKiemKe.Margin = new System.Windows.Forms.Padding(0);
            this.btnKiemKe.Name = "btnKiemKe";
            this.btnKiemKe.Size = new System.Drawing.Size(142, 46);
            this.btnKiemKe.TabIndex = 1;
            this.btnKiemKe.Text = "Kiểm Kê";
            // 
            // btnLogoOut
            // 
            this.btnLogoOut.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoOut.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnLogoOut.Appearance.Options.UseFont = true;
            this.btnLogoOut.Appearance.Options.UseForeColor = true;
            this.btnLogoOut.Image = ((System.Drawing.Image)(resources.GetObject("btnLogoOut.Image")));
            this.btnLogoOut.Location = new System.Drawing.Point(11, 114);
            this.btnLogoOut.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogoOut.Name = "btnLogoOut";
            this.btnLogoOut.Size = new System.Drawing.Size(146, 46);
            this.btnLogoOut.TabIndex = 2;
            this.btnLogoOut.Text = "Đăng Xuất";
            // 
            // btnHelp
            // 
            this.btnHelp.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnHelp.Appearance.Options.UseFont = true;
            this.btnHelp.Appearance.Options.UseForeColor = true;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.Location = new System.Drawing.Point(11, 60);
            this.btnHelp.Margin = new System.Windows.Forms.Padding(0);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(146, 46);
            this.btnHelp.TabIndex = 3;
            this.btnHelp.Text = "Trợ Giúp";
            // 
            // btnBaoCaoTraHang
            // 
            this.btnBaoCaoTraHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBaoCaoTraHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBaoCaoTraHang.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBaoCaoTraHang.Appearance.Options.UseFont = true;
            this.btnBaoCaoTraHang.Appearance.Options.UseForeColor = true;
            this.btnBaoCaoTraHang.Image = ((System.Drawing.Image)(resources.GetObject("btnBaoCaoTraHang.Image")));
            this.btnBaoCaoTraHang.Location = new System.Drawing.Point(180, 59);
            this.btnBaoCaoTraHang.Margin = new System.Windows.Forms.Padding(0);
            this.btnBaoCaoTraHang.Name = "btnBaoCaoTraHang";
            this.btnBaoCaoTraHang.Size = new System.Drawing.Size(142, 46);
            this.btnBaoCaoTraHang.TabIndex = 4;
            this.btnBaoCaoTraHang.Text = "Báo Cáo Trả Hàng";
            // 
            // btnBaoCaoBanHang
            // 
            this.btnBaoCaoBanHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBaoCaoBanHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBaoCaoBanHang.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBaoCaoBanHang.Appearance.Options.UseFont = true;
            this.btnBaoCaoBanHang.Appearance.Options.UseForeColor = true;
            this.btnBaoCaoBanHang.Image = ((System.Drawing.Image)(resources.GetObject("btnBaoCaoBanHang.Image")));
            this.btnBaoCaoBanHang.Location = new System.Drawing.Point(180, 6);
            this.btnBaoCaoBanHang.Margin = new System.Windows.Forms.Padding(0);
            this.btnBaoCaoBanHang.Name = "btnBaoCaoBanHang";
            this.btnBaoCaoBanHang.Size = new System.Drawing.Size(142, 46);
            this.btnBaoCaoBanHang.TabIndex = 3;
            this.btnBaoCaoBanHang.Text = "Báo Cáo Bán Hàng";
            // 
            // pnFuntionThanhToan
            // 
            this.pnFuntionThanhToan.Controls.Add(this.panelControl13);
            this.pnFuntionThanhToan.Controls.Add(this.panelControl24);
            this.pnFuntionThanhToan.Controls.Add(this.scrolCTNgyuenTe);
            this.pnFuntionThanhToan.Controls.Add(this.btnHinhThucThanhToan);
            this.pnFuntionThanhToan.Controls.Add(this.btnNhapLaiTien);
            this.pnFuntionThanhToan.Controls.Add(this.btnKhachHangThanThiet);
            this.pnFuntionThanhToan.Controls.Add(this.btn_boqua);
            this.pnFuntionThanhToan.Controls.Add(this.btn_khonginhoadon);
            this.pnFuntionThanhToan.Controls.Add(this.btnKhuyenMaiGiamGia);
            this.pnFuntionThanhToan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFuntionThanhToan.Location = new System.Drawing.Point(2, 2);
            this.pnFuntionThanhToan.Name = "pnFuntionThanhToan";
            this.pnFuntionThanhToan.Size = new System.Drawing.Size(561, 252);
            this.pnFuntionThanhToan.TabIndex = 11;
            // 
            // panelControl13
            // 
            this.panelControl13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelControl13.Controls.Add(this.btn_TopNgT);
            this.panelControl13.Controls.Add(this.btn_ButtonNgT);
            this.panelControl13.Location = new System.Drawing.Point(151, 50);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(38, 202);
            this.panelControl13.TabIndex = 40;
            // 
            // btn_TopNgT
            // 
            this.btn_TopNgT.Image = ((System.Drawing.Image)(resources.GetObject("btn_TopNgT.Image")));
            this.btn_TopNgT.Location = new System.Drawing.Point(3, 6);
            this.btn_TopNgT.Name = "btn_TopNgT";
            this.btn_TopNgT.Size = new System.Drawing.Size(31, 53);
            this.btn_TopNgT.TabIndex = 5;
            this.btn_TopNgT.Text = "simpleButton1";
            // 
            // btn_ButtonNgT
            // 
            this.btn_ButtonNgT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ButtonNgT.Image = ((System.Drawing.Image)(resources.GetObject("btn_ButtonNgT.Image")));
            this.btn_ButtonNgT.Location = new System.Drawing.Point(3, 148);
            this.btn_ButtonNgT.Name = "btn_ButtonNgT";
            this.btn_ButtonNgT.Size = new System.Drawing.Size(31, 53);
            this.btn_ButtonNgT.TabIndex = 4;
            this.btn_ButtonNgT.Text = "simpleButton4";
            // 
            // panelControl24
            // 
            this.panelControl24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl24.Controls.Add(this.gr_ListNguyenTe);
            this.panelControl24.Controls.Add(this.simpleButton1);
            this.panelControl24.Location = new System.Drawing.Point(2, 0);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(148, 251);
            this.panelControl24.TabIndex = 39;
            // 
            // gr_ListNguyenTe
            // 
            this.gr_ListNguyenTe.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_ListNguyenTe.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_ListNguyenTe.Location = new System.Drawing.Point(2, 48);
            this.gr_ListNguyenTe.MainView = this.gv_listnguyente;
            this.gr_ListNguyenTe.Name = "gr_ListNguyenTe";
            this.gr_ListNguyenTe.Size = new System.Drawing.Size(144, 201);
            this.gr_ListNguyenTe.TabIndex = 1;
            this.gr_ListNguyenTe.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_listnguyente,
            this.gridView6});
            // 
            // gv_listnguyente
            // 
            this.gv_listnguyente.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Ma_NguyenTe,
            this.Nguyen_Te,
            this.Ty_Gia,
            this.Phu_Thu});
            this.gv_listnguyente.GridControl = this.gr_ListNguyenTe;
            this.gv_listnguyente.Name = "gv_listnguyente";
            this.gv_listnguyente.OptionsView.ShowGroupPanel = false;
            this.gv_listnguyente.RowHeight = 43;
            this.gv_listnguyente.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // Ma_NguyenTe
            // 
            this.Ma_NguyenTe.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ma_NguyenTe.AppearanceHeader.Options.UseFont = true;
            this.Ma_NguyenTe.AppearanceHeader.Options.UseTextOptions = true;
            this.Ma_NguyenTe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Ma_NguyenTe.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Ma_NguyenTe.Caption = "Mã Nguyên Tệ";
            this.Ma_NguyenTe.FieldName = "Ma_NguyenTe";
            this.Ma_NguyenTe.Name = "Ma_NguyenTe";
            this.Ma_NguyenTe.OptionsColumn.AllowEdit = false;
            this.Ma_NguyenTe.OptionsColumn.AllowFocus = false;
            // 
            // Nguyen_Te
            // 
            this.Nguyen_Te.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Nguyen_Te.AppearanceCell.Options.UseFont = true;
            this.Nguyen_Te.AppearanceCell.Options.UseTextOptions = true;
            this.Nguyen_Te.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Nguyen_Te.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Nguyen_Te.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nguyen_Te.AppearanceHeader.Options.UseFont = true;
            this.Nguyen_Te.AppearanceHeader.Options.UseTextOptions = true;
            this.Nguyen_Te.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Nguyen_Te.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Nguyen_Te.Caption = "Nguyên Tệ";
            this.Nguyen_Te.FieldName = "Nguyen_Te";
            this.Nguyen_Te.Name = "Nguyen_Te";
            this.Nguyen_Te.OptionsColumn.AllowEdit = false;
            this.Nguyen_Te.OptionsColumn.AllowFocus = false;
            this.Nguyen_Te.Visible = true;
            this.Nguyen_Te.VisibleIndex = 0;
            this.Nguyen_Te.Width = 88;
            // 
            // Ty_Gia
            // 
            this.Ty_Gia.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Ty_Gia.AppearanceCell.Options.UseFont = true;
            this.Ty_Gia.AppearanceCell.Options.UseTextOptions = true;
            this.Ty_Gia.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Ty_Gia.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Ty_Gia.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ty_Gia.AppearanceHeader.Options.UseFont = true;
            this.Ty_Gia.AppearanceHeader.Options.UseTextOptions = true;
            this.Ty_Gia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Ty_Gia.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Ty_Gia.Caption = "Tỷ Giá";
            this.Ty_Gia.FieldName = "Ty_Gia";
            this.Ty_Gia.Name = "Ty_Gia";
            this.Ty_Gia.OptionsColumn.AllowEdit = false;
            this.Ty_Gia.OptionsColumn.AllowFocus = false;
            this.Ty_Gia.Visible = true;
            this.Ty_Gia.VisibleIndex = 1;
            this.Ty_Gia.Width = 74;
            // 
            // Phu_Thu
            // 
            this.Phu_Thu.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Phu_Thu.AppearanceCell.Options.UseFont = true;
            this.Phu_Thu.AppearanceCell.Options.UseTextOptions = true;
            this.Phu_Thu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Phu_Thu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Phu_Thu.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phu_Thu.AppearanceHeader.Options.UseFont = true;
            this.Phu_Thu.AppearanceHeader.Options.UseTextOptions = true;
            this.Phu_Thu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Phu_Thu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Phu_Thu.Caption = "% Phụ Thu";
            this.Phu_Thu.FieldName = "Phu_Thu";
            this.Phu_Thu.Name = "Phu_Thu";
            this.Phu_Thu.OptionsColumn.AllowEdit = false;
            this.Phu_Thu.OptionsColumn.AllowFocus = false;
            this.Phu_Thu.Visible = true;
            this.Phu_Thu.VisibleIndex = 2;
            this.Phu_Thu.Width = 77;
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.gr_ListNguyenTe;
            this.gridView6.Name = "gridView6";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(2, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(144, 46);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Ngoại Tệ";
            // 
            // scrolCTNgyuenTe
            // 
            this.scrolCTNgyuenTe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scrolCTNgyuenTe.Controls.Add(this.lb_TyGia);
            this.scrolCTNgyuenTe.Controls.Add(this.lbLoaiTien);
            this.scrolCTNgyuenTe.Controls.Add(this.labelControl32);
            this.scrolCTNgyuenTe.Controls.Add(this.labelControl27);
            this.scrolCTNgyuenTe.Location = new System.Drawing.Point(151, 4);
            this.scrolCTNgyuenTe.Name = "scrolCTNgyuenTe";
            this.scrolCTNgyuenTe.Size = new System.Drawing.Size(409, 45);
            this.scrolCTNgyuenTe.TabIndex = 35;
            // 
            // lb_TyGia
            // 
            this.lb_TyGia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_TyGia.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lb_TyGia.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_TyGia.Location = new System.Drawing.Point(221, 17);
            this.lb_TyGia.Name = "lb_TyGia";
            this.lb_TyGia.Size = new System.Drawing.Size(6, 16);
            this.lb_TyGia.TabIndex = 7;
            this.lb_TyGia.Text = "-";
            // 
            // lbLoaiTien
            // 
            this.lbLoaiTien.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbLoaiTien.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbLoaiTien.Location = new System.Drawing.Point(74, 16);
            this.lbLoaiTien.Name = "lbLoaiTien";
            this.lbLoaiTien.Size = new System.Drawing.Size(6, 16);
            this.lbLoaiTien.TabIndex = 6;
            this.lbLoaiTien.Text = "-";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl32.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl32.Location = new System.Drawing.Point(2, 16);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(66, 17);
            this.labelControl32.TabIndex = 5;
            this.labelControl32.Text = "Loại Tiền:";
            // 
            // labelControl27
            // 
            this.labelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl27.Location = new System.Drawing.Point(166, 17);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(51, 17);
            this.labelControl27.TabIndex = 4;
            this.labelControl27.Text = "Tỷ Giá :";
            // 
            // btnHinhThucThanhToan
            // 
            this.btnHinhThucThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHinhThucThanhToan.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHinhThucThanhToan.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnHinhThucThanhToan.Appearance.Options.UseFont = true;
            this.btnHinhThucThanhToan.Appearance.Options.UseForeColor = true;
            this.btnHinhThucThanhToan.Image = ((System.Drawing.Image)(resources.GetObject("btnHinhThucThanhToan.Image")));
            this.btnHinhThucThanhToan.Location = new System.Drawing.Point(201, 58);
            this.btnHinhThucThanhToan.Name = "btnHinhThucThanhToan";
            this.btnHinhThucThanhToan.Size = new System.Drawing.Size(156, 58);
            this.btnHinhThucThanhToan.TabIndex = 34;
            this.btnHinhThucThanhToan.Text = "Thẻ Thanh Toán";
            // 
            // btnNhapLaiTien
            // 
            this.btnNhapLaiTien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNhapLaiTien.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhapLaiTien.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnNhapLaiTien.Appearance.Options.UseFont = true;
            this.btnNhapLaiTien.Appearance.Options.UseForeColor = true;
            this.btnNhapLaiTien.Image = ((System.Drawing.Image)(resources.GetObject("btnNhapLaiTien.Image")));
            this.btnNhapLaiTien.Location = new System.Drawing.Point(392, 57);
            this.btnNhapLaiTien.Name = "btnNhapLaiTien";
            this.btnNhapLaiTien.Size = new System.Drawing.Size(156, 58);
            this.btnNhapLaiTien.TabIndex = 33;
            this.btnNhapLaiTien.Text = "Nhập Lại Tiền Trả";
            // 
            // btnKhachHangThanThiet
            // 
            this.btnKhachHangThanThiet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKhachHangThanThiet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKhachHangThanThiet.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnKhachHangThanThiet.Appearance.Options.UseFont = true;
            this.btnKhachHangThanThiet.Appearance.Options.UseForeColor = true;
            this.btnKhachHangThanThiet.Image = ((System.Drawing.Image)(resources.GetObject("btnKhachHangThanThiet.Image")));
            this.btnKhachHangThanThiet.Location = new System.Drawing.Point(202, 187);
            this.btnKhachHangThanThiet.Name = "btnKhachHangThanThiet";
            this.btnKhachHangThanThiet.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.btnKhachHangThanThiet.Size = new System.Drawing.Size(156, 58);
            this.btnKhachHangThanThiet.TabIndex = 31;
            this.btnKhachHangThanThiet.Text = "Khách Hàng TT";
            // 
            // btn_boqua
            // 
            this.btn_boqua.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_boqua.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_boqua.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_boqua.Appearance.Options.UseFont = true;
            this.btn_boqua.Appearance.Options.UseForeColor = true;
            this.btn_boqua.Image = ((System.Drawing.Image)(resources.GetObject("btn_boqua.Image")));
            this.btn_boqua.Location = new System.Drawing.Point(392, 188);
            this.btn_boqua.Name = "btn_boqua";
            this.btn_boqua.Size = new System.Drawing.Size(156, 58);
            this.btn_boqua.TabIndex = 29;
            this.btn_boqua.Text = "Hủy Thanh Toán";
            // 
            // btn_khonginhoadon
            // 
            this.btn_khonginhoadon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_khonginhoadon.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_khonginhoadon.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_khonginhoadon.Appearance.Options.UseFont = true;
            this.btn_khonginhoadon.Appearance.Options.UseForeColor = true;
            this.btn_khonginhoadon.Image = ((System.Drawing.Image)(resources.GetObject("btn_khonginhoadon.Image")));
            this.btn_khonginhoadon.Location = new System.Drawing.Point(392, 122);
            this.btn_khonginhoadon.Name = "btn_khonginhoadon";
            this.btn_khonginhoadon.Size = new System.Drawing.Size(156, 58);
            this.btn_khonginhoadon.TabIndex = 30;
            this.btn_khonginhoadon.Text = "Không In Hóa Đơn";
            // 
            // btnKhuyenMaiGiamGia
            // 
            this.btnKhuyenMaiGiamGia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKhuyenMaiGiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnKhuyenMaiGiamGia.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnKhuyenMaiGiamGia.Appearance.Options.UseFont = true;
            this.btnKhuyenMaiGiamGia.Appearance.Options.UseForeColor = true;
            this.btnKhuyenMaiGiamGia.Enabled = false;
            this.btnKhuyenMaiGiamGia.Image = ((System.Drawing.Image)(resources.GetObject("btnKhuyenMaiGiamGia.Image")));
            this.btnKhuyenMaiGiamGia.Location = new System.Drawing.Point(202, 123);
            this.btnKhuyenMaiGiamGia.Name = "btnKhuyenMaiGiamGia";
            this.btnKhuyenMaiGiamGia.Size = new System.Drawing.Size(156, 58);
            this.btnKhuyenMaiGiamGia.TabIndex = 28;
            this.btnKhuyenMaiGiamGia.Text = "Chiết Khấu";
            // 
            // pn_bigparent
            // 
            this.pn_bigparent.Controls.Add(this.pnctrlParent);
            this.pn_bigparent.Controls.Add(this.panelControl5);
            this.pn_bigparent.Controls.Add(this.panelControl3);
            this.pn_bigparent.Controls.Add(this.panelControl11);
            this.pn_bigparent.Controls.Add(this.pnTienThanhToan);
            this.pn_bigparent.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_bigparent.Location = new System.Drawing.Point(567, 2);
            this.pn_bigparent.Name = "pn_bigparent";
            this.pn_bigparent.Size = new System.Drawing.Size(409, 513);
            this.pn_bigparent.TabIndex = 5;
            // 
            // pnctrlParent
            // 
            this.pnctrlParent.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnctrlParent.Appearance.Options.UseBackColor = true;
            this.pnctrlParent.AutoScroll = false;
            this.pnctrlParent.Controls.Add(this.pnSearch);
            this.pnctrlParent.Controls.Add(this.gcSanPham);
            this.pnctrlParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnctrlParent.Location = new System.Drawing.Point(2, 40);
            this.pnctrlParent.Margin = new System.Windows.Forms.Padding(0);
            this.pnctrlParent.Name = "pnctrlParent";
            this.pnctrlParent.Size = new System.Drawing.Size(405, 183);
            this.pnctrlParent.TabIndex = 6;
            // 
            // pnSearch
            // 
            this.pnSearch.Controls.Add(this.txtHangSX);
            this.pnSearch.Controls.Add(this.txtGiaBan);
            this.pnSearch.Controls.Add(this.txtTenHH);
            this.pnSearch.Controls.Add(this.txtMaHH);
            this.pnSearch.Controls.Add(this.txtSTT);
            this.pnSearch.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnSearch.Location = new System.Drawing.Point(0, 161);
            this.pnSearch.Name = "pnSearch";
            this.pnSearch.Size = new System.Drawing.Size(405, 22);
            this.pnSearch.TabIndex = 6;
            // 
            // txtHangSX
            // 
            this.txtHangSX.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtHangSX.Location = new System.Drawing.Point(321, 2);
            this.txtHangSX.Name = "txtHangSX";
            this.txtHangSX.Size = new System.Drawing.Size(81, 20);
            this.txtHangSX.TabIndex = 5;
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtGiaBan.Location = new System.Drawing.Point(222, 2);
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Size = new System.Drawing.Size(99, 20);
            this.txtGiaBan.TabIndex = 4;
            // 
            // txtTenHH
            // 
            this.txtTenHH.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtTenHH.Location = new System.Drawing.Point(114, 2);
            this.txtTenHH.Name = "txtTenHH";
            this.txtTenHH.Size = new System.Drawing.Size(108, 20);
            this.txtTenHH.TabIndex = 3;
            // 
            // txtMaHH
            // 
            this.txtMaHH.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtMaHH.Location = new System.Drawing.Point(31, 2);
            this.txtMaHH.Name = "txtMaHH";
            this.txtMaHH.Size = new System.Drawing.Size(83, 20);
            this.txtMaHH.TabIndex = 2;
            // 
            // txtSTT
            // 
            this.txtSTT.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtSTT.Location = new System.Drawing.Point(2, 2);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Size = new System.Drawing.Size(29, 20);
            this.txtSTT.TabIndex = 1;
            // 
            // gcSanPham
            // 
            this.gcSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSanPham.Location = new System.Drawing.Point(0, 0);
            this.gcSanPham.MainView = this.gvSanPham;
            this.gcSanPham.Name = "gcSanPham";
            this.gcSanPham.Size = new System.Drawing.Size(405, 183);
            this.gcSanPham.TabIndex = 0;
            this.gcSanPham.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSanPham});
            // 
            // gvSanPham
            // 
            this.gvSanPham.ColumnPanelRowHeight = 22;
            this.gvSanPham.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.MAHH,
            this.TenHH,
            this.GiaBan,
            this.HangSX});
            this.gvSanPham.GridControl = this.gcSanPham;
            this.gvSanPham.Name = "gvSanPham";
            this.gvSanPham.OptionsView.ShowFooter = true;
            this.gvSanPham.OptionsView.ShowGroupPanel = false;
            this.gvSanPham.OptionsView.ShowIndicator = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "STT";
            this.gridColumn1.FieldName = "STT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 29;
            // 
            // MAHH
            // 
            this.MAHH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.MAHH.AppearanceHeader.Options.UseFont = true;
            this.MAHH.AppearanceHeader.Options.UseTextOptions = true;
            this.MAHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAHH.Caption = "Mã Hàng Hóa";
            this.MAHH.FieldName = "MAHH";
            this.MAHH.Name = "MAHH";
            this.MAHH.OptionsColumn.AllowEdit = false;
            this.MAHH.OptionsColumn.AllowFocus = false;
            this.MAHH.Visible = true;
            this.MAHH.VisibleIndex = 1;
            this.MAHH.Width = 83;
            // 
            // TenHH
            // 
            this.TenHH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TenHH.AppearanceHeader.Options.UseFont = true;
            this.TenHH.AppearanceHeader.Options.UseTextOptions = true;
            this.TenHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenHH.Caption = "Tên Hàng Hóa";
            this.TenHH.FieldName = "TENHH";
            this.TenHH.Name = "TenHH";
            this.TenHH.OptionsColumn.AllowEdit = false;
            this.TenHH.OptionsColumn.AllowFocus = false;
            this.TenHH.Visible = true;
            this.TenHH.VisibleIndex = 2;
            this.TenHH.Width = 109;
            // 
            // GiaBan
            // 
            this.GiaBan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.GiaBan.AppearanceHeader.Options.UseFont = true;
            this.GiaBan.AppearanceHeader.Options.UseTextOptions = true;
            this.GiaBan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GiaBan.Caption = "Giá Bán";
            this.GiaBan.FieldName = "GIABAN";
            this.GiaBan.Name = "GiaBan";
            this.GiaBan.OptionsColumn.AllowEdit = false;
            this.GiaBan.OptionsColumn.AllowFocus = false;
            this.GiaBan.Visible = true;
            this.GiaBan.VisibleIndex = 3;
            this.GiaBan.Width = 99;
            // 
            // HangSX
            // 
            this.HangSX.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.HangSX.AppearanceHeader.Options.UseFont = true;
            this.HangSX.AppearanceHeader.Options.UseTextOptions = true;
            this.HangSX.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HangSX.Caption = "Hãng Sản Xuất";
            this.HangSX.FieldName = "HANGSX";
            this.HangSX.Name = "HangSX";
            this.HangSX.OptionsColumn.AllowEdit = false;
            this.HangSX.OptionsColumn.AllowFocus = false;
            this.HangSX.Visible = true;
            this.HangSX.VisibleIndex = 4;
            this.HangSX.Width = 83;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.cbHangSx);
            this.panelControl5.Controls.Add(this.cbPhanNhom);
            this.panelControl5.Controls.Add(this.cbNhom);
            this.panelControl5.Controls.Add(this.cbNganh);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(405, 38);
            this.panelControl5.TabIndex = 6;
            // 
            // cbHangSx
            // 
            this.cbHangSx.Location = new System.Drawing.Point(304, 5);
            this.cbHangSx.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbHangSx.Name = "cbHangSx";
            this.cbHangSx.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cbHangSx.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHangSx.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.cbHangSx.Properties.Appearance.Options.UseBackColor = true;
            this.cbHangSx.Properties.Appearance.Options.UseFont = true;
            this.cbHangSx.Properties.Appearance.Options.UseForeColor = true;
            this.cbHangSx.Properties.Appearance.Options.UseTextOptions = true;
            this.cbHangSx.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbHangSx.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.cbHangSx.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbHangSx.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHangSx.Size = new System.Drawing.Size(100, 25);
            this.cbHangSx.TabIndex = 3;
            // 
            // cbPhanNhom
            // 
            this.cbPhanNhom.Location = new System.Drawing.Point(203, 5);
            this.cbPhanNhom.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbPhanNhom.Name = "cbPhanNhom";
            this.cbPhanNhom.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cbPhanNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPhanNhom.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.cbPhanNhom.Properties.Appearance.Options.UseBackColor = true;
            this.cbPhanNhom.Properties.Appearance.Options.UseFont = true;
            this.cbPhanNhom.Properties.Appearance.Options.UseForeColor = true;
            this.cbPhanNhom.Properties.Appearance.Options.UseTextOptions = true;
            this.cbPhanNhom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbPhanNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.cbPhanNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbPhanNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPhanNhom.Size = new System.Drawing.Size(100, 25);
            this.cbPhanNhom.TabIndex = 2;
            // 
            // cbNhom
            // 
            this.cbNhom.Location = new System.Drawing.Point(102, 5);
            this.cbNhom.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbNhom.Name = "cbNhom";
            this.cbNhom.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cbNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNhom.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.cbNhom.Properties.Appearance.Options.UseBackColor = true;
            this.cbNhom.Properties.Appearance.Options.UseFont = true;
            this.cbNhom.Properties.Appearance.Options.UseForeColor = true;
            this.cbNhom.Properties.Appearance.Options.UseTextOptions = true;
            this.cbNhom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.cbNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNhom.Size = new System.Drawing.Size(100, 25);
            this.cbNhom.TabIndex = 1;
            // 
            // cbNganh
            // 
            this.cbNganh.Location = new System.Drawing.Point(1, 5);
            this.cbNganh.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbNganh.Name = "cbNganh";
            this.cbNganh.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cbNganh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNganh.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.cbNganh.Properties.Appearance.Options.UseBackColor = true;
            this.cbNganh.Properties.Appearance.Options.UseFont = true;
            this.cbNganh.Properties.Appearance.Options.UseForeColor = true;
            this.cbNganh.Properties.Appearance.Options.UseTextOptions = true;
            this.cbNganh.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbNganh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.cbNganh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbNganh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNganh.Size = new System.Drawing.Size(100, 25);
            this.cbNganh.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.panelControl3.Appearance.Options.UseFont = true;
            this.panelControl3.Controls.Add(this.cbSoTrang);
            this.panelControl3.Controls.Add(this.lbSoTrang);
            this.panelControl3.Controls.Add(this.txtPageNow);
            this.panelControl3.Controls.Add(this.btn_Next);
            this.panelControl3.Controls.Add(this.btn_9Cot);
            this.panelControl3.Controls.Add(this.btn_Last);
            this.panelControl3.Controls.Add(this.lb_sotrang);
            this.panelControl3.Controls.Add(this.btn_First);
            this.panelControl3.Controls.Add(this.btn_7cot);
            this.panelControl3.Controls.Add(this.btn_Prev);
            this.panelControl3.Controls.Add(this.btn_5cot);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 223);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(405, 32);
            this.panelControl3.TabIndex = 5;
            // 
            // cbSoTrang
            // 
            this.cbSoTrang.Location = new System.Drawing.Point(357, 2);
            this.cbSoTrang.MinimumSize = new System.Drawing.Size(0, 28);
            this.cbSoTrang.Name = "cbSoTrang";
            this.cbSoTrang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSoTrang.Size = new System.Drawing.Size(41, 28);
            this.cbSoTrang.TabIndex = 8;
            // 
            // lbSoTrang
            // 
            this.lbSoTrang.Location = new System.Drawing.Point(309, 11);
            this.lbSoTrang.Name = "lbSoTrang";
            this.lbSoTrang.Size = new System.Drawing.Size(47, 13);
            this.lbSoTrang.TabIndex = 7;
            this.lbSoTrang.Text = "Số Trang:";
            // 
            // txtPageNow
            // 
            this.txtPageNow.Location = new System.Drawing.Point(194, 2);
            this.txtPageNow.MinimumSize = new System.Drawing.Size(0, 28);
            this.txtPageNow.Name = "txtPageNow";
            this.txtPageNow.Size = new System.Drawing.Size(20, 28);
            this.txtPageNow.TabIndex = 6;
            // 
            // btn_Next
            // 
            this.btn_Next.Location = new System.Drawing.Point(156, 2);
            this.btn_Next.Name = "btn_Next";
            this.btn_Next.Size = new System.Drawing.Size(35, 28);
            this.btn_Next.TabIndex = 3;
            this.btn_Next.Text = ">";
            // 
            // btn_9Cot
            // 
            this.btn_9Cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9Cot.Appearance.Options.UseFont = true;
            this.btn_9Cot.Location = new System.Drawing.Point(78, 2);
            this.btn_9Cot.Name = "btn_9Cot";
            this.btn_9Cot.Size = new System.Drawing.Size(35, 28);
            this.btn_9Cot.TabIndex = 4;
            this.btn_9Cot.Text = "9";
            // 
            // btn_Last
            // 
            this.btn_Last.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn_Last.Location = new System.Drawing.Point(117, 2);
            this.btn_Last.Name = "btn_Last";
            this.btn_Last.Size = new System.Drawing.Size(35, 28);
            this.btn_Last.TabIndex = 2;
            this.btn_Last.Text = ">>";
            // 
            // lb_sotrang
            // 
            this.lb_sotrang.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lb_sotrang.Location = new System.Drawing.Point(218, 10);
            this.lb_sotrang.Name = "lb_sotrang";
            this.lb_sotrang.Size = new System.Drawing.Size(12, 16);
            this.lb_sotrang.TabIndex = 2;
            this.lb_sotrang.Text = "/0";
            // 
            // btn_First
            // 
            this.btn_First.Location = new System.Drawing.Point(271, 2);
            this.btn_First.Name = "btn_First";
            this.btn_First.Size = new System.Drawing.Size(35, 28);
            this.btn_First.TabIndex = 5;
            this.btn_First.Text = "<<";
            // 
            // btn_7cot
            // 
            this.btn_7cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7cot.Appearance.Options.UseFont = true;
            this.btn_7cot.Location = new System.Drawing.Point(40, 2);
            this.btn_7cot.Name = "btn_7cot";
            this.btn_7cot.Size = new System.Drawing.Size(35, 28);
            this.btn_7cot.TabIndex = 4;
            this.btn_7cot.Text = "7";
            // 
            // btn_Prev
            // 
            this.btn_Prev.Location = new System.Drawing.Point(233, 2);
            this.btn_Prev.Name = "btn_Prev";
            this.btn_Prev.Size = new System.Drawing.Size(35, 28);
            this.btn_Prev.TabIndex = 4;
            this.btn_Prev.Text = "<";
            // 
            // btn_5cot
            // 
            this.btn_5cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5cot.Appearance.Options.UseFont = true;
            this.btn_5cot.Location = new System.Drawing.Point(3, 2);
            this.btn_5cot.Name = "btn_5cot";
            this.btn_5cot.Size = new System.Drawing.Size(35, 28);
            this.btn_5cot.TabIndex = 3;
            this.btn_5cot.Text = "5";
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.pn_banphim);
            this.panelControl11.Controls.Add(this.pnKeyBoardCharacter);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 255);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(405, 256);
            this.panelControl11.TabIndex = 0;
            // 
            // pn_banphim
            // 
            this.pn_banphim.Controls.Add(this.btn_Payments);
            this.pn_banphim.Controls.Add(this.btn_ABC);
            this.pn_banphim.Controls.Add(this.btn_000);
            this.pn_banphim.Controls.Add(this.btn_00);
            this.pn_banphim.Controls.Add(this.btn_0);
            this.pn_banphim.Controls.Add(this.btn_enter);
            this.pn_banphim.Controls.Add(this.btn_delete);
            this.pn_banphim.Controls.Add(this.btn_6);
            this.pn_banphim.Controls.Add(this.btn_9);
            this.pn_banphim.Controls.Add(this.btn_8);
            this.pn_banphim.Controls.Add(this.btn_7);
            this.pn_banphim.Controls.Add(this.btn_back);
            this.pn_banphim.Controls.Add(this.btn_5);
            this.pn_banphim.Controls.Add(this.btn_4);
            this.pn_banphim.Controls.Add(this.btn_3);
            this.pn_banphim.Controls.Add(this.btn_2);
            this.pn_banphim.Controls.Add(this.btn_1);
            this.pn_banphim.Controls.Add(this.btn_ESC);
            this.pn_banphim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_banphim.Location = new System.Drawing.Point(2, 2);
            this.pn_banphim.Name = "pn_banphim";
            this.pn_banphim.Size = new System.Drawing.Size(401, 252);
            this.pn_banphim.TabIndex = 4;
            this.pn_banphim.Visible = false;
            // 
            // btn_Payments
            // 
            this.btn_Payments.AllowFocus = false;
            this.btn_Payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Payments.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payments.Appearance.Options.UseFont = true;
            this.btn_Payments.Image = ((System.Drawing.Image)(resources.GetObject("btn_Payments.Image")));
            this.btn_Payments.Location = new System.Drawing.Point(267, 139);
            this.btn_Payments.Name = "btn_Payments";
            this.btn_Payments.Size = new System.Drawing.Size(124, 108);
            this.btn_Payments.TabIndex = 29;
            this.btn_Payments.Text = "Thanh Toán\r\n\r\n[ F1 ]";
            // 
            // btn_ABC
            // 
            this.btn_ABC.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btn_ABC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ABC.Appearance.Options.UseBorderColor = true;
            this.btn_ABC.Appearance.Options.UseFont = true;
            this.btn_ABC.Location = new System.Drawing.Point(9, 190);
            this.btn_ABC.Name = "btn_ABC";
            this.btn_ABC.Size = new System.Drawing.Size(60, 55);
            this.btn_ABC.TabIndex = 28;
            this.btn_ABC.Text = "ABC";
            // 
            // btn_000
            // 
            this.btn_000.AllowFocus = false;
            this.btn_000.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_000.Appearance.Options.UseFont = true;
            this.btn_000.Location = new System.Drawing.Point(202, 128);
            this.btn_000.Name = "btn_000";
            this.btn_000.Size = new System.Drawing.Size(60, 55);
            this.btn_000.TabIndex = 27;
            this.btn_000.Text = "000";
            // 
            // btn_00
            // 
            this.btn_00.AllowFocus = false;
            this.btn_00.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_00.Appearance.Options.UseFont = true;
            this.btn_00.Location = new System.Drawing.Point(176, 192);
            this.btn_00.Name = "btn_00";
            this.btn_00.Size = new System.Drawing.Size(86, 55);
            this.btn_00.TabIndex = 26;
            this.btn_00.Text = "00";
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Location = new System.Drawing.Point(73, 192);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(97, 55);
            this.btn_0.TabIndex = 23;
            this.btn_0.Text = "0";
            // 
            // btn_enter
            // 
            this.btn_enter.AllowFocus = false;
            this.btn_enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_enter.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_enter.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_enter.Appearance.Options.UseFont = true;
            this.btn_enter.Appearance.Options.UseForeColor = true;
            this.btn_enter.Location = new System.Drawing.Point(267, 65);
            this.btn_enter.Name = "btn_enter";
            this.btn_enter.Size = new System.Drawing.Size(124, 70);
            this.btn_enter.TabIndex = 0;
            this.btn_enter.Text = "OK";
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Location = new System.Drawing.Point(331, 4);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(60, 55);
            this.btn_delete.TabIndex = 24;
            this.btn_delete.Text = "Del";
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Location = new System.Drawing.Point(136, 63);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(60, 55);
            this.btn_6.TabIndex = 19;
            this.btn_6.Text = "6";
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Location = new System.Drawing.Point(136, 4);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(60, 55);
            this.btn_9.TabIndex = 22;
            this.btn_9.Text = "9";
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Location = new System.Drawing.Point(72, 4);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(60, 55);
            this.btn_8.TabIndex = 21;
            this.btn_8.Text = "8";
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Location = new System.Drawing.Point(7, 4);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(60, 55);
            this.btn_7.TabIndex = 20;
            this.btn_7.Text = "7";
            // 
            // btn_back
            // 
            this.btn_back.AllowFocus = false;
            this.btn_back.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.Appearance.Options.UseFont = true;
            this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
            this.btn_back.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_back.Location = new System.Drawing.Point(267, 2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(60, 55);
            this.btn_back.TabIndex = 16;
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Location = new System.Drawing.Point(72, 61);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(60, 55);
            this.btn_5.TabIndex = 15;
            this.btn_5.Text = "5";
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Location = new System.Drawing.Point(7, 61);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(60, 55);
            this.btn_4.TabIndex = 14;
            this.btn_4.Text = "4";
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Location = new System.Drawing.Point(136, 126);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(60, 55);
            this.btn_3.TabIndex = 13;
            this.btn_3.Text = "3";
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Location = new System.Drawing.Point(72, 126);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(60, 55);
            this.btn_2.TabIndex = 12;
            this.btn_2.Text = "2";
            // 
            // btn_1
            // 
            this.btn_1.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btn_1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.Options.UseBorderColor = true;
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Location = new System.Drawing.Point(7, 126);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(60, 55);
            this.btn_1.TabIndex = 11;
            this.btn_1.Text = "1";
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Location = new System.Drawing.Point(207, 2);
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(50, 114);
            this.btn_ESC.TabIndex = 10;
            this.btn_ESC.Text = "ESC";
            // 
            // pnKeyBoardCharacter
            // 
            this.pnKeyBoardCharacter.Controls.Add(this.btn_backchar);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_PaymentsChar);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_OKChar);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_L);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_K);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_U);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_Y);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_123);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_M);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_J);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_I);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_T);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_DeleteChar);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_N);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_H);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_O);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_R);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_ESCChar);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_B);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_G);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_P);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_E);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_X);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_V);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_F);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_A);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_W);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_Z);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_C);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_D);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_S);
            this.pnKeyBoardCharacter.Controls.Add(this.btn_Q);
            this.pnKeyBoardCharacter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKeyBoardCharacter.Location = new System.Drawing.Point(2, 2);
            this.pnKeyBoardCharacter.Name = "pnKeyBoardCharacter";
            this.pnKeyBoardCharacter.Size = new System.Drawing.Size(401, 252);
            this.pnKeyBoardCharacter.TabIndex = 0;
            // 
            // btn_backchar
            // 
            this.btn_backchar.Image = ((System.Drawing.Image)(resources.GetObject("btn_backchar.Image")));
            this.btn_backchar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_backchar.Location = new System.Drawing.Point(315, 124);
            this.btn_backchar.Name = "btn_backchar";
            this.btn_backchar.Size = new System.Drawing.Size(85, 45);
            this.btn_backchar.TabIndex = 31;
            this.btn_backchar.Text = "back";
            // 
            // btn_PaymentsChar
            // 
            this.btn_PaymentsChar.Image = ((System.Drawing.Image)(resources.GetObject("btn_PaymentsChar.Image")));
            this.btn_PaymentsChar.Location = new System.Drawing.Point(64, 175);
            this.btn_PaymentsChar.Name = "btn_PaymentsChar";
            this.btn_PaymentsChar.Size = new System.Drawing.Size(125, 73);
            this.btn_PaymentsChar.TabIndex = 30;
            this.btn_PaymentsChar.Text = "Thanh Toán";
            // 
            // btn_OKChar
            // 
            this.btn_OKChar.Location = new System.Drawing.Point(195, 174);
            this.btn_OKChar.Name = "btn_OKChar";
            this.btn_OKChar.Size = new System.Drawing.Size(85, 74);
            this.btn_OKChar.TabIndex = 29;
            this.btn_OKChar.Text = "OK";
            // 
            // btn_L
            // 
            this.btn_L.Location = new System.Drawing.Point(349, 65);
            this.btn_L.Name = "btn_L";
            this.btn_L.Size = new System.Drawing.Size(40, 45);
            this.btn_L.TabIndex = 28;
            this.btn_L.Text = "L";
            // 
            // btn_K
            // 
            this.btn_K.Location = new System.Drawing.Point(307, 65);
            this.btn_K.Name = "btn_K";
            this.btn_K.Size = new System.Drawing.Size(40, 45);
            this.btn_K.TabIndex = 27;
            this.btn_K.Text = "K";
            // 
            // btn_U
            // 
            this.btn_U.Location = new System.Drawing.Point(240, 7);
            this.btn_U.Name = "btn_U";
            this.btn_U.Size = new System.Drawing.Size(40, 45);
            this.btn_U.TabIndex = 26;
            this.btn_U.Text = "U";
            // 
            // btn_Y
            // 
            this.btn_Y.Location = new System.Drawing.Point(200, 7);
            this.btn_Y.Name = "btn_Y";
            this.btn_Y.Size = new System.Drawing.Size(40, 45);
            this.btn_Y.TabIndex = 25;
            this.btn_Y.Text = "Y";
            // 
            // btn_123
            // 
            this.btn_123.Location = new System.Drawing.Point(0, 193);
            this.btn_123.Name = "btn_123";
            this.btn_123.Size = new System.Drawing.Size(60, 55);
            this.btn_123.TabIndex = 24;
            this.btn_123.Text = "?123";
            // 
            // btn_M
            // 
            this.btn_M.Location = new System.Drawing.Point(269, 124);
            this.btn_M.Name = "btn_M";
            this.btn_M.Size = new System.Drawing.Size(40, 45);
            this.btn_M.TabIndex = 23;
            this.btn_M.Text = "M";
            // 
            // btn_J
            // 
            this.btn_J.Location = new System.Drawing.Point(264, 65);
            this.btn_J.Name = "btn_J";
            this.btn_J.Size = new System.Drawing.Size(40, 45);
            this.btn_J.TabIndex = 22;
            this.btn_J.Text = "J";
            // 
            // btn_I
            // 
            this.btn_I.Location = new System.Drawing.Point(320, 7);
            this.btn_I.Name = "btn_I";
            this.btn_I.Size = new System.Drawing.Size(40, 45);
            this.btn_I.TabIndex = 21;
            this.btn_I.Text = "I";
            // 
            // btn_T
            // 
            this.btn_T.Location = new System.Drawing.Point(160, 7);
            this.btn_T.Name = "btn_T";
            this.btn_T.Size = new System.Drawing.Size(40, 45);
            this.btn_T.TabIndex = 20;
            this.btn_T.Text = "T";
            // 
            // btn_DeleteChar
            // 
            this.btn_DeleteChar.Location = new System.Drawing.Point(286, 189);
            this.btn_DeleteChar.Name = "btn_DeleteChar";
            this.btn_DeleteChar.Size = new System.Drawing.Size(50, 59);
            this.btn_DeleteChar.TabIndex = 19;
            this.btn_DeleteChar.Text = "Delete";
            // 
            // btn_N
            // 
            this.btn_N.Location = new System.Drawing.Point(225, 124);
            this.btn_N.Name = "btn_N";
            this.btn_N.Size = new System.Drawing.Size(40, 45);
            this.btn_N.TabIndex = 18;
            this.btn_N.Text = "N";
            // 
            // btn_H
            // 
            this.btn_H.Location = new System.Drawing.Point(224, 65);
            this.btn_H.Name = "btn_H";
            this.btn_H.Size = new System.Drawing.Size(38, 45);
            this.btn_H.TabIndex = 17;
            this.btn_H.Text = "H";
            // 
            // btn_O
            // 
            this.btn_O.Location = new System.Drawing.Point(280, 7);
            this.btn_O.Name = "btn_O";
            this.btn_O.Size = new System.Drawing.Size(40, 45);
            this.btn_O.TabIndex = 16;
            this.btn_O.Text = "O";
            // 
            // btn_R
            // 
            this.btn_R.Location = new System.Drawing.Point(120, 7);
            this.btn_R.Name = "btn_R";
            this.btn_R.Size = new System.Drawing.Size(40, 45);
            this.btn_R.TabIndex = 15;
            this.btn_R.Text = "R";
            // 
            // btn_ESCChar
            // 
            this.btn_ESCChar.Location = new System.Drawing.Point(342, 186);
            this.btn_ESCChar.Name = "btn_ESCChar";
            this.btn_ESCChar.Size = new System.Drawing.Size(56, 62);
            this.btn_ESCChar.TabIndex = 14;
            this.btn_ESCChar.Text = "ESC";
            // 
            // btn_B
            // 
            this.btn_B.Location = new System.Drawing.Point(181, 124);
            this.btn_B.Name = "btn_B";
            this.btn_B.Size = new System.Drawing.Size(40, 45);
            this.btn_B.TabIndex = 13;
            this.btn_B.Text = "B";
            // 
            // btn_G
            // 
            this.btn_G.Location = new System.Drawing.Point(182, 65);
            this.btn_G.Name = "btn_G";
            this.btn_G.Size = new System.Drawing.Size(40, 45);
            this.btn_G.TabIndex = 12;
            this.btn_G.Text = "G";
            // 
            // btn_P
            // 
            this.btn_P.Location = new System.Drawing.Point(360, 7);
            this.btn_P.Name = "btn_P";
            this.btn_P.Size = new System.Drawing.Size(40, 45);
            this.btn_P.TabIndex = 11;
            this.btn_P.Text = "P";
            // 
            // btn_E
            // 
            this.btn_E.Location = new System.Drawing.Point(80, 7);
            this.btn_E.Name = "btn_E";
            this.btn_E.Size = new System.Drawing.Size(40, 45);
            this.btn_E.TabIndex = 10;
            this.btn_E.Text = "E";
            // 
            // btn_X
            // 
            this.btn_X.Location = new System.Drawing.Point(46, 124);
            this.btn_X.Name = "btn_X";
            this.btn_X.Size = new System.Drawing.Size(40, 45);
            this.btn_X.TabIndex = 9;
            this.btn_X.Text = "X";
            // 
            // btn_V
            // 
            this.btn_V.Location = new System.Drawing.Point(137, 124);
            this.btn_V.Name = "btn_V";
            this.btn_V.Size = new System.Drawing.Size(40, 45);
            this.btn_V.TabIndex = 8;
            this.btn_V.Text = "V";
            // 
            // btn_F
            // 
            this.btn_F.Location = new System.Drawing.Point(138, 65);
            this.btn_F.Name = "btn_F";
            this.btn_F.Size = new System.Drawing.Size(40, 45);
            this.btn_F.TabIndex = 7;
            this.btn_F.Text = "F";
            // 
            // btn_A
            // 
            this.btn_A.Location = new System.Drawing.Point(11, 65);
            this.btn_A.Name = "btn_A";
            this.btn_A.Size = new System.Drawing.Size(40, 45);
            this.btn_A.TabIndex = 6;
            this.btn_A.Text = "A";
            // 
            // btn_W
            // 
            this.btn_W.Location = new System.Drawing.Point(40, 7);
            this.btn_W.Name = "btn_W";
            this.btn_W.Size = new System.Drawing.Size(40, 45);
            this.btn_W.TabIndex = 5;
            this.btn_W.Text = "W";
            // 
            // btn_Z
            // 
            this.btn_Z.Location = new System.Drawing.Point(0, 124);
            this.btn_Z.Name = "btn_Z";
            this.btn_Z.Size = new System.Drawing.Size(40, 45);
            this.btn_Z.TabIndex = 4;
            this.btn_Z.Text = "Z";
            // 
            // btn_C
            // 
            this.btn_C.Location = new System.Drawing.Point(92, 124);
            this.btn_C.Name = "btn_C";
            this.btn_C.Size = new System.Drawing.Size(40, 45);
            this.btn_C.TabIndex = 3;
            this.btn_C.Text = "C";
            // 
            // btn_D
            // 
            this.btn_D.Location = new System.Drawing.Point(95, 65);
            this.btn_D.Name = "btn_D";
            this.btn_D.Size = new System.Drawing.Size(40, 45);
            this.btn_D.TabIndex = 2;
            this.btn_D.Text = "D";
            // 
            // btn_S
            // 
            this.btn_S.Location = new System.Drawing.Point(53, 65);
            this.btn_S.Name = "btn_S";
            this.btn_S.Size = new System.Drawing.Size(40, 45);
            this.btn_S.TabIndex = 1;
            this.btn_S.Text = "S";
            // 
            // btn_Q
            // 
            this.btn_Q.Location = new System.Drawing.Point(0, 7);
            this.btn_Q.Name = "btn_Q";
            this.btn_Q.Size = new System.Drawing.Size(40, 45);
            this.btn_Q.TabIndex = 0;
            this.btn_Q.Text = "Q";
            // 
            // pnTienThanhToan
            // 
            this.pnTienThanhToan.Controls.Add(this.pnCtrlNgNhPhNhom);
            this.pnTienThanhToan.Controls.Add(this.panelControl23);
            this.pnTienThanhToan.Controls.Add(this.panelControl21);
            this.pnTienThanhToan.Controls.Add(this.panelControl14);
            this.pnTienThanhToan.Controls.Add(this.panelControl15);
            this.pnTienThanhToan.Controls.Add(this.labelControl25);
            this.pnTienThanhToan.Controls.Add(this.panelControl16);
            this.pnTienThanhToan.Controls.Add(this.panelControl17);
            this.pnTienThanhToan.Controls.Add(this.panelControl18);
            this.pnTienThanhToan.Controls.Add(this.panelControl19);
            this.pnTienThanhToan.Controls.Add(this.panelControl20);
            this.pnTienThanhToan.Controls.Add(this.labelControl34);
            this.pnTienThanhToan.Location = new System.Drawing.Point(0, 0);
            this.pnTienThanhToan.Name = "pnTienThanhToan";
            this.pnTienThanhToan.Size = new System.Drawing.Size(411, 413);
            this.pnTienThanhToan.TabIndex = 0;
            // 
            // pnCtrlNgNhPhNhom
            // 
            this.pnCtrlNgNhPhNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnCtrlNgNhPhNhom.Controls.Add(this.gr_ChonPhanNhom);
            this.pnCtrlNgNhPhNhom.Controls.Add(this.gr_ChonNganh);
            this.pnCtrlNgNhPhNhom.Controls.Add(this.gr_ChonNhom);
            this.pnCtrlNgNhPhNhom.Location = new System.Drawing.Point(0, 2);
            this.pnCtrlNgNhPhNhom.Name = "pnCtrlNgNhPhNhom";
            this.pnCtrlNgNhPhNhom.Size = new System.Drawing.Size(411, 379);
            this.pnCtrlNgNhPhNhom.TabIndex = 10;
            // 
            // gr_ChonPhanNhom
            // 
            this.gr_ChonPhanNhom.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gr_ChonPhanNhom.AppearanceCaption.Options.UseFont = true;
            this.gr_ChonPhanNhom.Controls.Add(this.Scrol_ShowPhanNhom);
            this.gr_ChonPhanNhom.Controls.Add(this.panelControl6);
            this.gr_ChonPhanNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_ChonPhanNhom.Location = new System.Drawing.Point(2, 2);
            this.gr_ChonPhanNhom.Name = "gr_ChonPhanNhom";
            this.gr_ChonPhanNhom.ShowCaption = false;
            this.gr_ChonPhanNhom.Size = new System.Drawing.Size(407, 375);
            this.gr_ChonPhanNhom.TabIndex = 12;
            this.gr_ChonPhanNhom.Text = "Chọn Phân Nhóm";
            // 
            // Scrol_ShowPhanNhom
            // 
            this.Scrol_ShowPhanNhom.AutoScroll = false;
            this.Scrol_ShowPhanNhom.Controls.Add(this.txtShowPhanNhom3);
            this.Scrol_ShowPhanNhom.Controls.Add(this.txtShowNhom3);
            this.Scrol_ShowPhanNhom.Controls.Add(this.txtShowNganh3);
            this.Scrol_ShowPhanNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Scrol_ShowPhanNhom.Location = new System.Drawing.Point(2, 39);
            this.Scrol_ShowPhanNhom.Name = "Scrol_ShowPhanNhom";
            this.Scrol_ShowPhanNhom.Size = new System.Drawing.Size(403, 334);
            this.Scrol_ShowPhanNhom.TabIndex = 1;
            // 
            // txtShowPhanNhom3
            // 
            this.txtShowPhanNhom3.Appearance.BackColor = System.Drawing.Color.White;
            this.txtShowPhanNhom3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowPhanNhom3.Appearance.Options.UseBackColor = true;
            this.txtShowPhanNhom3.Appearance.Options.UseFont = true;
            this.txtShowPhanNhom3.Location = new System.Drawing.Point(183, 255);
            this.txtShowPhanNhom3.Name = "txtShowPhanNhom3";
            this.txtShowPhanNhom3.Size = new System.Drawing.Size(97, 23);
            this.txtShowPhanNhom3.TabIndex = 15;
            this.txtShowPhanNhom3.Text = "Chọn phân nhóm...";
            // 
            // txtShowNhom3
            // 
            this.txtShowNhom3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowNhom3.Appearance.Options.UseFont = true;
            this.txtShowNhom3.Location = new System.Drawing.Point(94, 255);
            this.txtShowNhom3.Name = "txtShowNhom3";
            this.txtShowNhom3.Size = new System.Drawing.Size(85, 23);
            this.txtShowNhom3.TabIndex = 14;
            this.txtShowNhom3.Text = "Chọn nhóm...";
            // 
            // txtShowNganh3
            // 
            this.txtShowNganh3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowNganh3.Appearance.Options.UseFont = true;
            this.txtShowNganh3.Location = new System.Drawing.Point(3, 254);
            this.txtShowNganh3.Name = "txtShowNganh3";
            this.txtShowNganh3.Size = new System.Drawing.Size(85, 24);
            this.txtShowNganh3.TabIndex = 13;
            this.txtShowNganh3.Text = "Chọn Ngành...";
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.lbltrangPNhom);
            this.panelControl6.Controls.Add(this.btnBackPNhom);
            this.panelControl6.Controls.Add(this.btn_ClosePhanNganh);
            this.panelControl6.Controls.Add(this.btnNextPNhom);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(403, 37);
            this.panelControl6.TabIndex = 16;
            // 
            // lbltrangPNhom
            // 
            this.lbltrangPNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbltrangPNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbltrangPNhom.Location = new System.Drawing.Point(169, 10);
            this.lbltrangPNhom.Name = "lbltrangPNhom";
            this.lbltrangPNhom.Size = new System.Drawing.Size(20, 13);
            this.lbltrangPNhom.TabIndex = 7;
            this.lbltrangPNhom.Text = "0/0";
            // 
            // btnBackPNhom
            // 
            this.btnBackPNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnBackPNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnBackPNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btnBackPNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnBackPNhom.Appearance.Options.UseBackColor = true;
            this.btnBackPNhom.Appearance.Options.UseBorderColor = true;
            this.btnBackPNhom.Image = ((System.Drawing.Image)(resources.GetObject("btnBackPNhom.Image")));
            this.btnBackPNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBackPNhom.Location = new System.Drawing.Point(71, 0);
            this.btnBackPNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnBackPNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBackPNhom.Name = "btnBackPNhom";
            this.btnBackPNhom.Size = new System.Drawing.Size(84, 35);
            this.btnBackPNhom.TabIndex = 5;
            this.btnBackPNhom.Text = "Back";
            this.btnBackPNhom.ToolTip = "Back";
            // 
            // btn_ClosePhanNganh
            // 
            this.btn_ClosePhanNganh.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btn_ClosePhanNganh.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_ClosePhanNganh.Appearance.Options.UseFont = true;
            this.btn_ClosePhanNganh.Appearance.Options.UseForeColor = true;
            this.btn_ClosePhanNganh.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_ClosePhanNganh.Location = new System.Drawing.Point(364, 2);
            this.btn_ClosePhanNganh.Name = "btn_ClosePhanNganh";
            this.btn_ClosePhanNganh.Size = new System.Drawing.Size(37, 33);
            this.btn_ClosePhanNganh.TabIndex = 0;
            this.btn_ClosePhanNganh.Text = "X";
            // 
            // btnNextPNhom
            // 
            this.btnNextPNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnNextPNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnNextPNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btnNextPNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnNextPNhom.Appearance.Options.UseBackColor = true;
            this.btnNextPNhom.Appearance.Options.UseBorderColor = true;
            this.btnNextPNhom.Image = ((System.Drawing.Image)(resources.GetObject("btnNextPNhom.Image")));
            this.btnNextPNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNextPNhom.Location = new System.Drawing.Point(206, 0);
            this.btnNextPNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnNextPNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnNextPNhom.Name = "btnNextPNhom";
            this.btnNextPNhom.Size = new System.Drawing.Size(84, 35);
            this.btnNextPNhom.TabIndex = 6;
            this.btnNextPNhom.Text = "Next";
            this.btnNextPNhom.ToolTip = "Next";
            // 
            // gr_ChonNganh
            // 
            this.gr_ChonNganh.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gr_ChonNganh.AppearanceCaption.Options.UseFont = true;
            this.gr_ChonNganh.Controls.Add(this.scrol_Nganh);
            this.gr_ChonNganh.Controls.Add(this.panelControl9);
            this.gr_ChonNganh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_ChonNganh.Location = new System.Drawing.Point(2, 2);
            this.gr_ChonNganh.Name = "gr_ChonNganh";
            this.gr_ChonNganh.ShowCaption = false;
            this.gr_ChonNganh.Size = new System.Drawing.Size(407, 375);
            this.gr_ChonNganh.TabIndex = 11;
            this.gr_ChonNganh.Text = "Chọ Ngành";
            // 
            // scrol_Nganh
            // 
            this.scrol_Nganh.AutoScroll = false;
            this.scrol_Nganh.Controls.Add(this.txtShowPhanNhom);
            this.scrol_Nganh.Controls.Add(this.txtShowNhom);
            this.scrol_Nganh.Controls.Add(this.txtShowNganh);
            this.scrol_Nganh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrol_Nganh.Location = new System.Drawing.Point(2, 39);
            this.scrol_Nganh.Name = "scrol_Nganh";
            this.scrol_Nganh.Size = new System.Drawing.Size(403, 334);
            this.scrol_Nganh.TabIndex = 1;
            // 
            // txtShowPhanNhom
            // 
            this.txtShowPhanNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.txtShowPhanNhom.Appearance.Options.UseBackColor = true;
            this.txtShowPhanNhom.Appearance.Options.UseFont = true;
            this.txtShowPhanNhom.Location = new System.Drawing.Point(182, 255);
            this.txtShowPhanNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.txtShowPhanNhom.Name = "txtShowPhanNhom";
            this.txtShowPhanNhom.Size = new System.Drawing.Size(97, 23);
            this.txtShowPhanNhom.TabIndex = 12;
            this.txtShowPhanNhom.Text = "Chọn Phân Nhóm";
            // 
            // txtShowNhom
            // 
            this.txtShowNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.txtShowNhom.Appearance.Options.UseBackColor = true;
            this.txtShowNhom.Appearance.Options.UseFont = true;
            this.txtShowNhom.Location = new System.Drawing.Point(92, 255);
            this.txtShowNhom.Name = "txtShowNhom";
            this.txtShowNhom.Size = new System.Drawing.Size(86, 23);
            this.txtShowNhom.TabIndex = 11;
            this.txtShowNhom.Text = "Chọn Nhóm";
            // 
            // txtShowNganh
            // 
            this.txtShowNganh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.txtShowNganh.Appearance.Options.UseFont = true;
            this.txtShowNganh.Appearance.Options.UseForeColor = true;
            this.txtShowNganh.Location = new System.Drawing.Point(2, 255);
            this.txtShowNganh.Name = "txtShowNganh";
            this.txtShowNganh.Size = new System.Drawing.Size(86, 23);
            this.txtShowNganh.TabIndex = 10;
            this.txtShowNganh.Text = "Chọn Ngành";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.simpleButton26);
            this.panelControl9.Controls.Add(this.lbPages_ng);
            this.panelControl9.Controls.Add(this.btnNextNg);
            this.panelControl9.Controls.Add(this.btn_PrevNg);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(403, 37);
            this.panelControl9.TabIndex = 8;
            // 
            // simpleButton26
            // 
            this.simpleButton26.Appearance.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.simpleButton26.Appearance.ForeColor = System.Drawing.Color.Red;
            this.simpleButton26.Appearance.Options.UseFont = true;
            this.simpleButton26.Appearance.Options.UseForeColor = true;
            this.simpleButton26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("simpleButton26.BackgroundImage")));
            this.simpleButton26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.simpleButton26.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton26.Location = new System.Drawing.Point(360, 2);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(41, 33);
            this.simpleButton26.TabIndex = 0;
            this.simpleButton26.Text = "X";
            // 
            // lbPages_ng
            // 
            this.lbPages_ng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbPages_ng.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPages_ng.Location = new System.Drawing.Point(164, 10);
            this.lbPages_ng.Name = "lbPages_ng";
            this.lbPages_ng.Size = new System.Drawing.Size(20, 13);
            this.lbPages_ng.TabIndex = 7;
            this.lbPages_ng.Text = "0/0";
            // 
            // btnNextNg
            // 
            this.btnNextNg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnNextNg.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnNextNg.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btnNextNg.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnNextNg.Appearance.Options.UseBackColor = true;
            this.btnNextNg.Appearance.Options.UseBorderColor = true;
            this.btnNextNg.Image = ((System.Drawing.Image)(resources.GetObject("btnNextNg.Image")));
            this.btnNextNg.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNextNg.Location = new System.Drawing.Point(201, 0);
            this.btnNextNg.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnNextNg.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnNextNg.Name = "btnNextNg";
            this.btnNextNg.Size = new System.Drawing.Size(84, 35);
            this.btnNextNg.TabIndex = 6;
            this.btnNextNg.ToolTip = "Next";
            // 
            // btn_PrevNg
            // 
            this.btn_PrevNg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btn_PrevNg.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btn_PrevNg.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btn_PrevNg.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btn_PrevNg.Appearance.Options.UseBackColor = true;
            this.btn_PrevNg.Appearance.Options.UseBorderColor = true;
            this.btn_PrevNg.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrevNg.Image")));
            this.btn_PrevNg.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_PrevNg.Location = new System.Drawing.Point(66, 0);
            this.btn_PrevNg.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btn_PrevNg.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_PrevNg.Name = "btn_PrevNg";
            this.btn_PrevNg.Size = new System.Drawing.Size(84, 35);
            this.btn_PrevNg.TabIndex = 5;
            this.btn_PrevNg.ToolTip = "Back";
            // 
            // gr_ChonNhom
            // 
            this.gr_ChonNhom.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gr_ChonNhom.AppearanceCaption.Options.UseFont = true;
            this.gr_ChonNhom.Controls.Add(this.Scrol_ShowNhom);
            this.gr_ChonNhom.Controls.Add(this.panelControl10);
            this.gr_ChonNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_ChonNhom.Location = new System.Drawing.Point(2, 2);
            this.gr_ChonNhom.Name = "gr_ChonNhom";
            this.gr_ChonNhom.ShowCaption = false;
            this.gr_ChonNhom.Size = new System.Drawing.Size(407, 375);
            this.gr_ChonNhom.TabIndex = 12;
            this.gr_ChonNhom.Text = "Chọ Nhóm";
            // 
            // Scrol_ShowNhom
            // 
            this.Scrol_ShowNhom.AutoScroll = false;
            this.Scrol_ShowNhom.Controls.Add(this.txtShowPanNhom2);
            this.Scrol_ShowNhom.Controls.Add(this.txtShowNhom2);
            this.Scrol_ShowNhom.Controls.Add(this.txtShowNganh2);
            this.Scrol_ShowNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Scrol_ShowNhom.Location = new System.Drawing.Point(2, 39);
            this.Scrol_ShowNhom.Name = "Scrol_ShowNhom";
            this.Scrol_ShowNhom.Size = new System.Drawing.Size(403, 334);
            this.Scrol_ShowNhom.TabIndex = 1;
            // 
            // txtShowPanNhom2
            // 
            this.txtShowPanNhom2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowPanNhom2.Appearance.Options.UseFont = true;
            this.txtShowPanNhom2.Location = new System.Drawing.Point(176, 238);
            this.txtShowPanNhom2.Name = "txtShowPanNhom2";
            this.txtShowPanNhom2.Size = new System.Drawing.Size(95, 23);
            this.txtShowPanNhom2.TabIndex = 15;
            this.txtShowPanNhom2.Text = "Chọn Phân Nhóm";
            // 
            // txtShowNhom2
            // 
            this.txtShowNhom2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowNhom2.Appearance.Options.UseBackColor = true;
            this.txtShowNhom2.Appearance.Options.UseFont = true;
            this.txtShowNhom2.Location = new System.Drawing.Point(87, 238);
            this.txtShowNhom2.Name = "txtShowNhom2";
            this.txtShowNhom2.Size = new System.Drawing.Size(87, 23);
            this.txtShowNhom2.TabIndex = 14;
            this.txtShowNhom2.Text = "Chọn Nhóm";
            // 
            // txtShowNganh2
            // 
            this.txtShowNganh2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtShowNganh2.Appearance.Options.UseFont = true;
            this.txtShowNganh2.Location = new System.Drawing.Point(2, 238);
            this.txtShowNganh2.Name = "txtShowNganh2";
            this.txtShowNganh2.Size = new System.Drawing.Size(84, 23);
            this.txtShowNganh2.TabIndex = 13;
            this.txtShowNganh2.Text = "Chọn Ngành";
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.btn_NextNhom);
            this.panelControl10.Controls.Add(this.lblSoTrangNhom);
            this.panelControl10.Controls.Add(this.btn_CloseTabNhom);
            this.panelControl10.Controls.Add(this.btnBackNhom);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(403, 37);
            this.panelControl10.TabIndex = 8;
            // 
            // btn_NextNhom
            // 
            this.btn_NextNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btn_NextNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.Options.UseBackColor = true;
            this.btn_NextNhom.Appearance.Options.UseBorderColor = true;
            this.btn_NextNhom.Image = ((System.Drawing.Image)(resources.GetObject("btn_NextNhom.Image")));
            this.btn_NextNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextNhom.Location = new System.Drawing.Point(201, 0);
            this.btn_NextNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btn_NextNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_NextNhom.Name = "btn_NextNhom";
            this.btn_NextNhom.Size = new System.Drawing.Size(84, 35);
            this.btn_NextNhom.TabIndex = 6;
            this.btn_NextNhom.ToolTip = "Next";
            // 
            // lblSoTrangNhom
            // 
            this.lblSoTrangNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblSoTrangNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblSoTrangNhom.Location = new System.Drawing.Point(164, 10);
            this.lblSoTrangNhom.Name = "lblSoTrangNhom";
            this.lblSoTrangNhom.Size = new System.Drawing.Size(20, 13);
            this.lblSoTrangNhom.TabIndex = 7;
            this.lblSoTrangNhom.Text = "0/0";
            // 
            // btn_CloseTabNhom
            // 
            this.btn_CloseTabNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btn_CloseTabNhom.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_CloseTabNhom.Appearance.Options.UseFont = true;
            this.btn_CloseTabNhom.Appearance.Options.UseForeColor = true;
            this.btn_CloseTabNhom.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_CloseTabNhom.Location = new System.Drawing.Point(360, 2);
            this.btn_CloseTabNhom.Name = "btn_CloseTabNhom";
            this.btn_CloseTabNhom.Size = new System.Drawing.Size(41, 33);
            this.btn_CloseTabNhom.TabIndex = 0;
            this.btn_CloseTabNhom.Text = "X";
            // 
            // btnBackNhom
            // 
            this.btnBackNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnBackNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.Options.UseBackColor = true;
            this.btnBackNhom.Appearance.Options.UseBorderColor = true;
            this.btnBackNhom.Image = ((System.Drawing.Image)(resources.GetObject("btnBackNhom.Image")));
            this.btnBackNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBackNhom.Location = new System.Drawing.Point(66, 0);
            this.btnBackNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnBackNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBackNhom.Name = "btnBackNhom";
            this.btnBackNhom.Size = new System.Drawing.Size(84, 35);
            this.btnBackNhom.TabIndex = 5;
            this.btnBackNhom.ToolTip = "Back";
            // 
            // panelControl23
            // 
            this.panelControl23.Controls.Add(this.lb_TienTra);
            this.panelControl23.Controls.Add(this.txtTienThanhToan);
            this.panelControl23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl23.Location = new System.Drawing.Point(2, 355);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(407, 56);
            this.panelControl23.TabIndex = 20;
            // 
            // lb_TienTra
            // 
            this.lb_TienTra.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TienTra.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_TienTra.Location = new System.Drawing.Point(3, 17);
            this.lb_TienTra.Name = "lb_TienTra";
            this.lb_TienTra.Size = new System.Drawing.Size(133, 23);
            this.lb_TienTra.TabIndex = 6;
            this.lb_TienTra.Text = "Nhập tiền trả:";
            // 
            // txtTienThanhToan
            // 
            this.txtTienThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTienThanhToan.Location = new System.Drawing.Point(137, 14);
            this.txtTienThanhToan.Name = "txtTienThanhToan";
            this.txtTienThanhToan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtTienThanhToan.Properties.Appearance.Options.UseFont = true;
            this.txtTienThanhToan.Size = new System.Drawing.Size(258, 30);
            this.txtTienThanhToan.TabIndex = 0;
            // 
            // panelControl21
            // 
            this.panelControl21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl21.Controls.Add(this.lable_lan3);
            this.panelControl21.Controls.Add(this.lable_tralan3);
            this.panelControl21.Location = new System.Drawing.Point(2, 141);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(396, 29);
            this.panelControl21.TabIndex = 15;
            // 
            // lable_lan3
            // 
            this.lable_lan3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_lan3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_lan3.Location = new System.Drawing.Point(5, 1);
            this.lable_lan3.Name = "lable_lan3";
            this.lable_lan3.Size = new System.Drawing.Size(0, 23);
            this.lable_lan3.TabIndex = 0;
            // 
            // lable_tralan3
            // 
            this.lable_tralan3.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_tralan3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_tralan3.Dock = System.Windows.Forms.DockStyle.Right;
            this.lable_tralan3.Location = new System.Drawing.Point(396, 0);
            this.lable_tralan3.Name = "lable_tralan3";
            this.lable_tralan3.Size = new System.Drawing.Size(0, 24);
            this.lable_tralan3.TabIndex = 0;
            // 
            // panelControl14
            // 
            this.panelControl14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.lable_lan2);
            this.panelControl14.Controls.Add(this.lable_tralan2);
            this.panelControl14.Location = new System.Drawing.Point(2, 171);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(396, 29);
            this.panelControl14.TabIndex = 19;
            // 
            // lable_lan2
            // 
            this.lable_lan2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_lan2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_lan2.Location = new System.Drawing.Point(5, 1);
            this.lable_lan2.Name = "lable_lan2";
            this.lable_lan2.Size = new System.Drawing.Size(0, 23);
            this.lable_lan2.TabIndex = 0;
            // 
            // lable_tralan2
            // 
            this.lable_tralan2.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_tralan2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_tralan2.Dock = System.Windows.Forms.DockStyle.Right;
            this.lable_tralan2.Location = new System.Drawing.Point(396, 0);
            this.lable_tralan2.Name = "lable_tralan2";
            this.lable_tralan2.Size = new System.Drawing.Size(0, 24);
            this.lable_tralan2.TabIndex = 0;
            // 
            // panelControl15
            // 
            this.panelControl15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.lable_lan1);
            this.panelControl15.Controls.Add(this.lable_tralan1);
            this.panelControl15.Location = new System.Drawing.Point(2, 201);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(396, 29);
            this.panelControl15.TabIndex = 16;
            // 
            // lable_lan1
            // 
            this.lable_lan1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_lan1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_lan1.Location = new System.Drawing.Point(5, 1);
            this.lable_lan1.Name = "lable_lan1";
            this.lable_lan1.Size = new System.Drawing.Size(0, 23);
            this.lable_lan1.TabIndex = 0;
            // 
            // lable_tralan1
            // 
            this.lable_tralan1.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_tralan1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lable_tralan1.Dock = System.Windows.Forms.DockStyle.Right;
            this.lable_tralan1.Location = new System.Drawing.Point(396, 0);
            this.lable_tralan1.Name = "lable_tralan1";
            this.lable_tralan1.Size = new System.Drawing.Size(0, 24);
            this.lable_tralan1.TabIndex = 0;
            // 
            // labelControl25
            // 
            this.labelControl25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Location = new System.Drawing.Point(22, 233);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(380, 1);
            this.labelControl25.TabIndex = 18;
            // 
            // panelControl16
            // 
            this.panelControl16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.labelControl26);
            this.panelControl16.Controls.Add(this.lb_khuyenmai);
            this.panelControl16.Location = new System.Drawing.Point(2, 37);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(396, 26);
            this.panelControl16.TabIndex = 17;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl26.Location = new System.Drawing.Point(5, 1);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(159, 23);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Chiết Khấu        :";
            // 
            // lb_khuyenmai
            // 
            this.lb_khuyenmai.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_khuyenmai.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_khuyenmai.Dock = System.Windows.Forms.DockStyle.Right;
            this.lb_khuyenmai.Location = new System.Drawing.Point(386, 0);
            this.lb_khuyenmai.Name = "lb_khuyenmai";
            this.lb_khuyenmai.Size = new System.Drawing.Size(10, 22);
            this.lb_khuyenmai.TabIndex = 0;
            this.lb_khuyenmai.Text = "0";
            // 
            // panelControl17
            // 
            this.panelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Controls.Add(this.labelControl28);
            this.panelControl17.Controls.Add(this.lb_tientrakhach);
            this.panelControl17.Location = new System.Drawing.Point(2, 284);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(396, 32);
            this.panelControl17.TabIndex = 11;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl28.Location = new System.Drawing.Point(5, 3);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(146, 23);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Tiền trả lại      :";
            // 
            // lb_tientrakhach
            // 
            this.lb_tientrakhach.Appearance.BackColor = System.Drawing.Color.White;
            this.lb_tientrakhach.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tientrakhach.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tientrakhach.Dock = System.Windows.Forms.DockStyle.Right;
            this.lb_tientrakhach.Location = new System.Drawing.Point(385, 0);
            this.lb_tientrakhach.Name = "lb_tientrakhach";
            this.lb_tientrakhach.Size = new System.Drawing.Size(11, 24);
            this.lb_tientrakhach.TabIndex = 0;
            this.lb_tientrakhach.Text = "0";
            // 
            // panelControl18
            // 
            this.panelControl18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.labelControl29);
            this.panelControl18.Controls.Add(this.lbTongCongTien);
            this.panelControl18.Location = new System.Drawing.Point(2, 68);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(393, 30);
            this.panelControl18.TabIndex = 12;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl29.Location = new System.Drawing.Point(5, 1);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(142, 23);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "TC thanh toán:";
            // 
            // lbTongCongTien
            // 
            this.lbTongCongTien.Appearance.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongCongTien.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTongCongTien.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbTongCongTien.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTongCongTien.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTongCongTien.Location = new System.Drawing.Point(382, 0);
            this.lbTongCongTien.Name = "lbTongCongTien";
            this.lbTongCongTien.Size = new System.Drawing.Size(11, 24);
            this.lbTongCongTien.TabIndex = 0;
            this.lbTongCongTien.Text = "0";
            // 
            // panelControl19
            // 
            this.panelControl19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl19.Controls.Add(this.labelControl30);
            this.panelControl19.Controls.Add(this.lb_tienkhachtra);
            this.panelControl19.Location = new System.Drawing.Point(2, 243);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(396, 29);
            this.panelControl19.TabIndex = 13;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl30.Location = new System.Drawing.Point(5, 1);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(143, 23);
            this.labelControl30.TabIndex = 0;
            this.labelControl30.Text = "Tiền khách trả:";
            // 
            // lb_tienkhachtra
            // 
            this.lb_tienkhachtra.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tienkhachtra.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tienkhachtra.Dock = System.Windows.Forms.DockStyle.Right;
            this.lb_tienkhachtra.Location = new System.Drawing.Point(385, 0);
            this.lb_tienkhachtra.Name = "lb_tienkhachtra";
            this.lb_tienkhachtra.Size = new System.Drawing.Size(11, 24);
            this.lb_tienkhachtra.TabIndex = 0;
            this.lb_tienkhachtra.Text = "0";
            // 
            // panelControl20
            // 
            this.panelControl20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl20.Controls.Add(this.labelControl33);
            this.panelControl20.Controls.Add(this.lb_tienthanhtoan);
            this.panelControl20.Location = new System.Drawing.Point(2, 4);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(393, 28);
            this.panelControl20.TabIndex = 10;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl33.Location = new System.Drawing.Point(5, 3);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(157, 23);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "Tiền khách mua:";
            // 
            // lb_tienthanhtoan
            // 
            this.lb_tienthanhtoan.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tienthanhtoan.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tienthanhtoan.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lb_tienthanhtoan.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lb_tienthanhtoan.Dock = System.Windows.Forms.DockStyle.Right;
            this.lb_tienthanhtoan.Location = new System.Drawing.Point(383, 0);
            this.lb_tienthanhtoan.Name = "lb_tienthanhtoan";
            this.lb_tienthanhtoan.Size = new System.Drawing.Size(10, 22);
            this.lb_tienthanhtoan.TabIndex = 0;
            this.lb_tienthanhtoan.Text = "0";
            // 
            // labelControl34
            // 
            this.labelControl34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl34.Location = new System.Drawing.Point(17, 65);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(380, 1);
            this.labelControl34.TabIndex = 9;
            // 
            // panelControl26
            // 
            this.panelControl26.Controls.Add(this.panel1);
            this.panelControl26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl26.Location = new System.Drawing.Point(0, 0);
            this.panelControl26.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.panelControl26.Name = "panelControl26";
            this.panelControl26.Size = new System.Drawing.Size(978, 68);
            this.panelControl26.TabIndex = 65;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.labelControl35);
            this.panel1.Controls.Add(this.lbgiay);
            this.panel1.Controls.Add(this.lbGio);
            this.panel1.Controls.Add(this.lbtimeby);
            this.panel1.Controls.Add(this.lb_NhanVien);
            this.panel1.Controls.Add(this.lb_Quay);
            this.panel1.Controls.Add(this.lb_Kho);
            this.panel1.Controls.Add(this.lbCaBan);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 64);
            this.panel1.TabIndex = 6;
            // 
            // labelControl35
            // 
            this.labelControl35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl35.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl35.Location = new System.Drawing.Point(779, 41);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(33, 19);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "Giờ:";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbgiay.Location = new System.Drawing.Point(916, 25);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 11;
            this.lbgiay.Text = "00";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbGio.Location = new System.Drawing.Point(865, 41);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 12;
            this.lbGio.Text = "00:00";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(547, 41);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 8;
            this.lbtimeby.Text = "Ca";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(547, 8);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 10;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(329, 17);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(61, 29);
            this.lb_Quay.TabIndex = 6;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.Location = new System.Drawing.Point(414, 17);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(47, 29);
            this.lb_Kho.TabIndex = 7;
            this.lb_Kho.Text = "Kho";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(775, 8);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(39, 19);
            this.lbCaBan.TabIndex = 9;
            this.lbCaBan.Text = "ngay";
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(978, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 492);
            this.barDockControl2.Size = new System.Drawing.Size(978, 25);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(978, 0);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl5.Location = new System.Drawing.Point(0, 0);
            this.barDockControl5.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl6.Location = new System.Drawing.Point(978, 0);
            this.barDockControl6.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl7.Location = new System.Drawing.Point(0, 517);
            this.barDockControl7.Size = new System.Drawing.Size(978, 0);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl8.Location = new System.Drawing.Point(0, 0);
            this.barDockControl8.Size = new System.Drawing.Size(978, 0);
            // 
            // gvcShowKHTT
            // 
            this.gvcShowKHTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvcShowKHTT.Location = new System.Drawing.Point(0, 44);
            this.gvcShowKHTT.MainView = this.gridView1;
            this.gvcShowKHTT.Name = "gvcShowKHTT";
            this.gvcShowKHTT.Size = new System.Drawing.Size(1086, 473);
            this.gvcShowKHTT.TabIndex = 0;
            this.gvcShowKHTT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.Ma_KHTT,
            this.HoTenKHTT,
            this.DiaChi,
            this.DienThoaiDiDong,
            this.DT_DOITUONGID,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gvcShowKHTT;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.HoTenKHTT, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Ma_KHTT", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 60;
            // 
            // Ma_KHTT
            // 
            this.Ma_KHTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.Ma_KHTT.AppearanceCell.Options.UseFont = true;
            this.Ma_KHTT.AppearanceCell.Options.UseTextOptions = true;
            this.Ma_KHTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Ma_KHTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Ma_KHTT.AppearanceHeader.Options.UseFont = true;
            this.Ma_KHTT.AppearanceHeader.Options.UseTextOptions = true;
            this.Ma_KHTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Ma_KHTT.Caption = "Mã KH";
            this.Ma_KHTT.FieldName = "Ma_KHTT";
            this.Ma_KHTT.Name = "Ma_KHTT";
            this.Ma_KHTT.OptionsColumn.AllowEdit = false;
            this.Ma_KHTT.OptionsColumn.AllowFocus = false;
            this.Ma_KHTT.OptionsColumn.FixedWidth = true;
            this.Ma_KHTT.Visible = true;
            this.Ma_KHTT.VisibleIndex = 1;
            this.Ma_KHTT.Width = 80;
            // 
            // HoTenKHTT
            // 
            this.HoTenKHTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.HoTenKHTT.AppearanceCell.Options.UseFont = true;
            this.HoTenKHTT.AppearanceCell.Options.UseTextOptions = true;
            this.HoTenKHTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.HoTenKHTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HoTenKHTT.AppearanceHeader.Options.UseFont = true;
            this.HoTenKHTT.AppearanceHeader.Options.UseTextOptions = true;
            this.HoTenKHTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HoTenKHTT.Caption = "Tên KH";
            this.HoTenKHTT.FieldName = "HoTenKHTT";
            this.HoTenKHTT.Name = "HoTenKHTT";
            this.HoTenKHTT.OptionsColumn.AllowEdit = false;
            this.HoTenKHTT.OptionsColumn.AllowFocus = false;
            this.HoTenKHTT.OptionsColumn.FixedWidth = true;
            this.HoTenKHTT.Visible = true;
            this.HoTenKHTT.VisibleIndex = 2;
            this.HoTenKHTT.Width = 200;
            // 
            // DiaChi
            // 
            this.DiaChi.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DiaChi.AppearanceCell.Options.UseFont = true;
            this.DiaChi.AppearanceCell.Options.UseTextOptions = true;
            this.DiaChi.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DiaChi.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DiaChi.AppearanceHeader.Options.UseFont = true;
            this.DiaChi.AppearanceHeader.Options.UseTextOptions = true;
            this.DiaChi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DiaChi.Caption = "Địa Chỉ";
            this.DiaChi.FieldName = "DiaChi";
            this.DiaChi.Name = "DiaChi";
            this.DiaChi.OptionsColumn.AllowEdit = false;
            this.DiaChi.OptionsColumn.AllowFocus = false;
            this.DiaChi.OptionsColumn.FixedWidth = true;
            this.DiaChi.Visible = true;
            this.DiaChi.VisibleIndex = 6;
            this.DiaChi.Width = 250;
            // 
            // DienThoaiDiDong
            // 
            this.DienThoaiDiDong.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DienThoaiDiDong.AppearanceCell.Options.UseFont = true;
            this.DienThoaiDiDong.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DienThoaiDiDong.AppearanceHeader.Options.UseFont = true;
            this.DienThoaiDiDong.AppearanceHeader.Options.UseTextOptions = true;
            this.DienThoaiDiDong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DienThoaiDiDong.Caption = "Điện thoại";
            this.DienThoaiDiDong.FieldName = "DienThoaiDiDong";
            this.DienThoaiDiDong.Name = "DienThoaiDiDong";
            this.DienThoaiDiDong.OptionsColumn.AllowEdit = false;
            this.DienThoaiDiDong.OptionsColumn.AllowFocus = false;
            this.DienThoaiDiDong.OptionsColumn.FixedWidth = true;
            this.DienThoaiDiDong.Visible = true;
            this.DienThoaiDiDong.VisibleIndex = 7;
            this.DienThoaiDiDong.Width = 120;
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.Caption = "gridColumn2";
            this.DT_DOITUONGID.FieldName = "DT_DOITUONGID";
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "CMND";
            this.gridColumn2.FieldName = "CMND";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Giới tính";
            this.gridColumn3.FieldName = "GIOITINH";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 80;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Ngày sinh";
            this.gridColumn4.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn4.FieldName = "NGAYSINH";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 120;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Email";
            this.gridColumn5.FieldName = "Email";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 8;
            this.gridColumn5.Width = 54;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.palButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1125, 44);
            this.panel2.TabIndex = 48;
            // 
            // palButton
            // 
            this.palButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palButton.Controls.Add(this.btnThem);
            this.palButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.palButton.Location = new System.Drawing.Point(573, 2);
            this.palButton.Name = "palButton";
            this.palButton.Size = new System.Drawing.Size(550, 40);
            this.palButton.TabIndex = 47;
            // 
            // btnThem
            // 
            this.btnThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(239, 3);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(304, 35);
            this.btnThem.TabIndex = 43;
            this.btnThem.Text = "&Thêm Khách Hàng Thanh toán nội bộ";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl27);
            this.panelControl1.Controls.Add(this.btnBanphim);
            this.panelControl1.Controls.Add(this.btnCapnhat);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(1086, 44);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(39, 473);
            this.panelControl1.TabIndex = 49;
            // 
            // panelControl27
            // 
            this.panelControl27.AutoSize = true;
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.panelControl28);
            this.panelControl27.Controls.Add(this.btnXuongit);
            this.panelControl27.Controls.Add(this.btnXuongnhieu);
            this.panelControl27.Controls.Add(this.btnLenit);
            this.panelControl27.Controls.Add(this.btnLennhieu);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl27.Location = new System.Drawing.Point(2, 82);
            this.panelControl27.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(35, 309);
            this.panelControl27.TabIndex = 14;
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl28.Location = new System.Drawing.Point(0, 160);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(35, 0);
            this.panelControl28.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 149);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 229);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 391);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // NS_KH_THANHTOANNOIBO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 517);
            this.Controls.Add(this.gvcShowKHTT);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panel2);
            this.Name = "NS_KH_THANHTOANNOIBO";
            this.Text = "Khách hàng thanh toán nội bộ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.NS_KH_THANHTOANNOIBO_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pn_WapperContent)).EndInit();
            this.pn_WapperContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnViewProduct)).EndInit();
            this.pnViewProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCtrlDSHangMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVDSHangMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Exc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Del)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrungHere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnInBaoCao)).EndInit();
            this.pnInBaoCao.ResumeLayout(false);
            this.pnInBaoCao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapperDeletePlusBut)).EndInit();
            this.pnWapperDeletePlusBut.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_ThanhToan1)).EndInit();
            this.pn_ThanhToan1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnPassWork)).EndInit();
            this.pnPassWork.ResumeLayout(false);
            this.pnPassWork.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassWorkConfig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMaHoaDonTraHang)).EndInit();
            this.pnMaHoaDonTraHang.ResumeLayout(false);
            this.pnMaHoaDonTraHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuongTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDonTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRePrinterBuil)).EndInit();
            this.pnRePrinterBuil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCtrReviewBuil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBaoCaoBanHang)).EndInit();
            this.pnBaoCaoBanHang.ResumeLayout(false);
            this.pnBaoCaoBanHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_denngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_denngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_tungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_tungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangBaoCao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnBanHang)).EndInit();
            this.pnBanHang.ResumeLayout(false);
            this.pnBanHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mahang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHoaDonDoi)).EndInit();
            this.pnHoaDonDoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_listhoadon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_listhoadon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnContainer)).EndInit();
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drSearchKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchMaHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforOder)).EndInit();
            this.pnInforOder.ResumeLayout(false);
            this.pnInforOder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnKiemKe)).EndInit();
            this.pnKiemKe.ResumeLayout(false);
            this.pnKiemKe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHangKK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuongKK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).EndInit();
            this.tab_Option.ResumeLayout(false);
            this.tabThuNgan.ResumeLayout(false);
            this.tabNghiepVu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnFuntionThanhToan)).EndInit();
            this.pnFuntionThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_ListNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_listnguyente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scrolCTNgyuenTe)).EndInit();
            this.scrolCTNgyuenTe.ResumeLayout(false);
            this.scrolCTNgyuenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_bigparent)).EndInit();
            this.pn_bigparent.ResumeLayout(false);
            this.pnctrlParent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnSearch)).EndInit();
            this.pnSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtHangSX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbHangSx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPhanNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNganh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSoTrang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim)).EndInit();
            this.pn_banphim.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnKeyBoardCharacter)).EndInit();
            this.pnKeyBoardCharacter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTienThanhToan)).EndInit();
            this.pnTienThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnCtrlNgNhPhNhom)).EndInit();
            this.pnCtrlNgNhPhNhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonPhanNhom)).EndInit();
            this.gr_ChonPhanNhom.ResumeLayout(false);
            this.Scrol_ShowPhanNhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNganh)).EndInit();
            this.gr_ChonNganh.ResumeLayout(false);
            this.scrol_Nganh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNhom)).EndInit();
            this.gr_ChonNhom.ResumeLayout(false);
            this.Scrol_ShowNhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            this.panelControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            this.panelControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            this.panelControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).EndInit();
            this.panelControl26.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palButton)).EndInit();
            this.palButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pn_WapperContent;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pnViewProduct;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl grCtrlDSHangMua;
        private DevExpress.XtraGrid.Views.Grid.GridView gVDSHangMua;
        private DevExpress.XtraGrid.Columns.GridColumn HH_HANGHOAID;
        private DevExpress.XtraGrid.Columns.GridColumn HH_BARCODE;
        private DevExpress.XtraGrid.Columns.GridColumn HH_TENHANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn HH_DVT;
        private DevExpress.XtraGrid.Columns.GridColumn So_Luong;
        private DevExpress.XtraGrid.Columns.GridColumn soLuongtra;
        private DevExpress.XtraGrid.Columns.GridColumn HH_GIABANLE;
        private DevExpress.XtraGrid.Columns.GridColumn Khuyen_Mai;
        private DevExpress.XtraGrid.Columns.GridColumn HH_VAT;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn K_MAK;
        private DevExpress.XtraGrid.Columns.GridColumn CH_MACH;
        private DevExpress.XtraGrid.Columns.GridColumn Q_MAQ;
        private DevExpress.XtraGrid.Columns.GridColumn STT_;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Add;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Exc;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Del;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit TrungHere;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lb_RowsCount;
        private DevExpress.XtraEditors.LabelControl lb_STT;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelThanhTien;
        private DevExpress.XtraEditors.LabelControl labelSoLuong;
        private DevExpress.XtraEditors.LabelControl lableShowTenHang;
        private DevExpress.XtraEditors.PanelControl pnInBaoCao;
        private DevExpress.XtraEditors.LabelControl lbTienTraLai;
        private DevExpress.XtraEditors.LabelControl lb_TienTraLai;
        private DevExpress.XtraEditors.SimpleButton btnInBaoCao;
        private DevExpress.XtraEditors.LabelControl lableTongDoanhThu;
        private DevExpress.XtraEditors.LabelControl lbTongDoanhThu;
        private DevExpress.XtraEditors.PanelControl pnWapperDeletePlusBut;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAll;
        private DevExpress.XtraEditors.SimpleButton btn_Cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn_Tru;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pn_ThanhToan1;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl pnPassWork;
        private DevExpress.XtraEditors.LabelControl lb_MesgasFuntion;
        private DevExpress.XtraEditors.SimpleButton btn_XacNhanSua;
        private DevExpress.XtraEditors.TextEdit txtPassWorkConfig;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.PanelControl pnMaHoaDonTraHang;
        private DevExpress.XtraEditors.SimpleButton btnQuayLai;
        private DevExpress.XtraEditors.SimpleButton btnTraTatCa;
        private DevExpress.XtraEditors.TextEdit txtSoLuongTra;
        private DevExpress.XtraEditors.TextEdit txtMaHangTra;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.SimpleButton btnTra;
        private DevExpress.XtraEditors.TextEdit txtMaHoaDonTra;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PanelControl pnRePrinterBuil;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl grCtrReviewBuil;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn _STT;
        private DevExpress.XtraGrid.Columns.GridColumn _HoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn cl_Ngay;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.SimpleButton btnUpListHd;
        private DevExpress.XtraEditors.SimpleButton btnDowListHd;
        private DevExpress.XtraEditors.PanelControl pnBaoCaoBanHang;
        private DevExpress.XtraEditors.SimpleButton btnOkBaoCaoBanH;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.DateEdit dt_denngay;
        private DevExpress.XtraEditors.DateEdit dt_tungay;
        private DevExpress.XtraEditors.SimpleButton btnXemNgayKhac;
        private DevExpress.XtraEditors.SimpleButton btnXemTrongNgay;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtMaHangBaoCao;
        private DevExpress.XtraEditors.PanelControl pnBanHang;
        private DevExpress.XtraEditors.LabelControl lbTienTruocThue;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelThueVAT;
        private DevExpress.XtraEditors.LabelControl label_GiamGia;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt_soluong;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lb_tongcong;
        private DevExpress.XtraEditors.TextEdit txt_Mahang;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl pnHoaDonDoi;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gr_listhoadon;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_listhoadon;
        private DevExpress.XtraGrid.Columns.GridColumn cl_STT;
        private DevExpress.XtraGrid.Columns.GridColumn cl_HoaDon;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.SimpleButton btnUpBill;
        private DevExpress.XtraEditors.SimpleButton btnDowBill;
        private DevExpress.XtraEditors.PanelControl pnContainer;
        private DevExpress.XtraEditors.LookUpEdit drSearchKho;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl lbmahang;
        private DevExpress.XtraEditors.TextEdit txtSearchMaHang;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.SimpleButton btnSearchTonKho;
        private DevExpress.XtraEditors.PanelControl pnInforOder;
        private DevExpress.XtraEditors.SimpleButton btn_TraCuuKH;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtIdProduct;
        private DevExpress.XtraEditors.PanelControl pnKiemKe;
        private DevExpress.XtraEditors.SimpleButton btn_LayKK;
        private DevExpress.XtraEditors.SimpleButton btnLuuTamFile;
        private DevExpress.XtraEditors.SimpleButton btnSeverFile;
        private DevExpress.XtraEditors.TextEdit txtMaHangKK;
        private DevExpress.XtraEditors.TextEdit txtSoLuongKK;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraTab.XtraTabControl tab_Option;
        private DevExpress.XtraTab.XtraTabPage tabThuNgan;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton btnByProduct;
        private DevExpress.XtraEditors.SimpleButton btnCashDrawer;
        private DevExpress.XtraEditors.SimpleButton btnLuuHoaDonDoi;
        private DevExpress.XtraEditors.SimpleButton btnLayHoaDonDoi;
        private DevExpress.XtraEditors.SimpleButton btnKhachHang;
        private DevExpress.XtraEditors.SimpleButton btnTraHang;
        private DevExpress.XtraTab.XtraTabPage tabNghiepVu;
        private DevExpress.XtraEditors.SimpleButton btnKetCa;
        private DevExpress.XtraEditors.SimpleButton btnXemTonKho;
        private DevExpress.XtraEditors.SimpleButton btnThoat;
        private DevExpress.XtraEditors.SimpleButton btnKiemKe;
        private DevExpress.XtraEditors.SimpleButton btnLogoOut;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnBaoCaoTraHang;
        private DevExpress.XtraEditors.SimpleButton btnBaoCaoBanHang;
        private DevExpress.XtraEditors.PanelControl pnFuntionThanhToan;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.SimpleButton btn_TopNgT;
        private DevExpress.XtraEditors.SimpleButton btn_ButtonNgT;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraGrid.GridControl gr_ListNguyenTe;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_listnguyente;
        private DevExpress.XtraGrid.Columns.GridColumn Ma_NguyenTe;
        private DevExpress.XtraGrid.Columns.GridColumn Nguyen_Te;
        private DevExpress.XtraGrid.Columns.GridColumn Ty_Gia;
        private DevExpress.XtraGrid.Columns.GridColumn Phu_Thu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl scrolCTNgyuenTe;
        private DevExpress.XtraEditors.LabelControl lb_TyGia;
        private DevExpress.XtraEditors.LabelControl lbLoaiTien;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.SimpleButton btnHinhThucThanhToan;
        private DevExpress.XtraEditors.SimpleButton btnNhapLaiTien;
        private DevExpress.XtraEditors.SimpleButton btnKhachHangThanThiet;
        private DevExpress.XtraEditors.SimpleButton btn_boqua;
        private DevExpress.XtraEditors.SimpleButton btn_khonginhoadon;
        private DevExpress.XtraEditors.SimpleButton btnKhuyenMaiGiamGia;
        private DevExpress.XtraEditors.PanelControl pn_bigparent;
        private DevExpress.XtraEditors.XtraScrollableControl pnctrlParent;
        private DevExpress.XtraEditors.PanelControl pnSearch;
        private DevExpress.XtraEditors.TextEdit txtHangSX;
        private DevExpress.XtraEditors.TextEdit txtGiaBan;
        private DevExpress.XtraEditors.TextEdit txtTenHH;
        private DevExpress.XtraEditors.TextEdit txtMaHH;
        private DevExpress.XtraEditors.TextEdit txtSTT;
        private DevExpress.XtraGrid.GridControl gcSanPham;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSanPham;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn MAHH;
        private DevExpress.XtraGrid.Columns.GridColumn TenHH;
        private DevExpress.XtraGrid.Columns.GridColumn GiaBan;
        private DevExpress.XtraGrid.Columns.GridColumn HangSX;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit cbHangSx;
        private DevExpress.XtraEditors.ComboBoxEdit cbPhanNhom;
        private DevExpress.XtraEditors.ComboBoxEdit cbNhom;
        private DevExpress.XtraEditors.ComboBoxEdit cbNganh;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit cbSoTrang;
        private DevExpress.XtraEditors.LabelControl lbSoTrang;
        private DevExpress.XtraEditors.TextEdit txtPageNow;
        private DevExpress.XtraEditors.SimpleButton btn_Next;
        private DevExpress.XtraEditors.SimpleButton btn_9Cot;
        private DevExpress.XtraEditors.SimpleButton btn_Last;
        private DevExpress.XtraEditors.LabelControl lb_sotrang;
        private DevExpress.XtraEditors.SimpleButton btn_First;
        private DevExpress.XtraEditors.SimpleButton btn_7cot;
        private DevExpress.XtraEditors.SimpleButton btn_Prev;
        private DevExpress.XtraEditors.SimpleButton btn_5cot;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl pn_banphim;
        private DevExpress.XtraEditors.SimpleButton btn_Payments;
        private DevExpress.XtraEditors.SimpleButton btn_ABC;
        private DevExpress.XtraEditors.SimpleButton btn_000;
        private DevExpress.XtraEditors.SimpleButton btn_00;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_enter;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btn_back;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.PanelControl pnKeyBoardCharacter;
        private DevExpress.XtraEditors.SimpleButton btn_backchar;
        private DevExpress.XtraEditors.SimpleButton btn_PaymentsChar;
        private DevExpress.XtraEditors.SimpleButton btn_OKChar;
        private DevExpress.XtraEditors.SimpleButton btn_L;
        private DevExpress.XtraEditors.SimpleButton btn_K;
        private DevExpress.XtraEditors.SimpleButton btn_U;
        private DevExpress.XtraEditors.SimpleButton btn_Y;
        private DevExpress.XtraEditors.SimpleButton btn_123;
        private DevExpress.XtraEditors.SimpleButton btn_M;
        private DevExpress.XtraEditors.SimpleButton btn_J;
        private DevExpress.XtraEditors.SimpleButton btn_I;
        private DevExpress.XtraEditors.SimpleButton btn_T;
        private DevExpress.XtraEditors.SimpleButton btn_DeleteChar;
        private DevExpress.XtraEditors.SimpleButton btn_N;
        private DevExpress.XtraEditors.SimpleButton btn_H;
        private DevExpress.XtraEditors.SimpleButton btn_O;
        private DevExpress.XtraEditors.SimpleButton btn_R;
        private DevExpress.XtraEditors.SimpleButton btn_ESCChar;
        private DevExpress.XtraEditors.SimpleButton btn_B;
        private DevExpress.XtraEditors.SimpleButton btn_G;
        private DevExpress.XtraEditors.SimpleButton btn_P;
        private DevExpress.XtraEditors.SimpleButton btn_E;
        private DevExpress.XtraEditors.SimpleButton btn_X;
        private DevExpress.XtraEditors.SimpleButton btn_V;
        private DevExpress.XtraEditors.SimpleButton btn_F;
        private DevExpress.XtraEditors.SimpleButton btn_A;
        private DevExpress.XtraEditors.SimpleButton btn_W;
        private DevExpress.XtraEditors.SimpleButton btn_Z;
        private DevExpress.XtraEditors.SimpleButton btn_C;
        private DevExpress.XtraEditors.SimpleButton btn_D;
        private DevExpress.XtraEditors.SimpleButton btn_S;
        private DevExpress.XtraEditors.SimpleButton btn_Q;
        private DevExpress.XtraEditors.PanelControl pnTienThanhToan;
        private DevExpress.XtraEditors.PanelControl pnCtrlNgNhPhNhom;
        private DevExpress.XtraEditors.GroupControl gr_ChonPhanNhom;
        private DevExpress.XtraEditors.XtraScrollableControl Scrol_ShowPhanNhom;
        private DevExpress.XtraEditors.SimpleButton txtShowPhanNhom3;
        private DevExpress.XtraEditors.SimpleButton txtShowNhom3;
        private DevExpress.XtraEditors.SimpleButton txtShowNganh3;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl lbltrangPNhom;
        private DevExpress.XtraEditors.SimpleButton btnBackPNhom;
        private DevExpress.XtraEditors.SimpleButton btn_ClosePhanNganh;
        private DevExpress.XtraEditors.SimpleButton btnNextPNhom;
        private DevExpress.XtraEditors.GroupControl gr_ChonNganh;
        private DevExpress.XtraEditors.XtraScrollableControl scrol_Nganh;
        private DevExpress.XtraEditors.SimpleButton txtShowPhanNhom;
        private DevExpress.XtraEditors.SimpleButton txtShowNhom;
        private DevExpress.XtraEditors.SimpleButton txtShowNganh;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton simpleButton26;
        private DevExpress.XtraEditors.LabelControl lbPages_ng;
        private DevExpress.XtraEditors.SimpleButton btnNextNg;
        private DevExpress.XtraEditors.SimpleButton btn_PrevNg;
        private DevExpress.XtraEditors.GroupControl gr_ChonNhom;
        private DevExpress.XtraEditors.XtraScrollableControl Scrol_ShowNhom;
        private DevExpress.XtraEditors.SimpleButton txtShowPanNhom2;
        private DevExpress.XtraEditors.SimpleButton txtShowNhom2;
        private DevExpress.XtraEditors.SimpleButton txtShowNganh2;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton btn_NextNhom;
        private DevExpress.XtraEditors.LabelControl lblSoTrangNhom;
        private DevExpress.XtraEditors.SimpleButton btn_CloseTabNhom;
        private DevExpress.XtraEditors.SimpleButton btnBackNhom;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.LabelControl lb_TienTra;
        private DevExpress.XtraEditors.TextEdit txtTienThanhToan;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.LabelControl lable_lan3;
        private DevExpress.XtraEditors.LabelControl lable_tralan3;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl lable_lan2;
        private DevExpress.XtraEditors.LabelControl lable_tralan2;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.LabelControl lable_lan1;
        private DevExpress.XtraEditors.LabelControl lable_tralan1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl lb_khuyenmai;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl lb_tientrakhach;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl lbTongCongTien;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl lb_tienkhachtra;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl lb_tienthanhtoan;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.PanelControl panelControl26;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraGrid.GridControl gvcShowKHTT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn Ma_KHTT;
        private DevExpress.XtraGrid.Columns.GridColumn HoTenKHTT;
        private DevExpress.XtraGrid.Columns.GridColumn DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn DienThoaiDiDong;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraEditors.PanelControl panel2;
        private DevExpress.XtraEditors.PanelControl palButton;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
    }
}