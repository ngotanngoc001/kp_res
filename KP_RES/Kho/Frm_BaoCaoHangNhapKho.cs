﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BaoCaoHangNhapKho : DevExpress.XtraEditors.XtraForm
    {
        DataTable myDT;
        public Frm_BaoCaoHangNhapKho()
        {
            InitializeComponent();
            dtpTuNgay.EditValue = clsGlobal.gdServerDate;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BaoCaoHangNhapKho_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboData.Properties.DataSource = null;
            cboData.Properties.Columns.Clear();
            cboData.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Cửa hàng
            {
                myDT = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Cửa hàng";
            }
            else if (optGroup.SelectedIndex == 1)//Kho
            {
                myDT = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Kho";
            }
            else if (optGroup.SelectedIndex == 2)//Nhóm hàng
            {
                myDT = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Nhóm hàng";
            }
            else if (optGroup.SelectedIndex == 3)//Hàng hóa
            {
                myDT = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Hàng hóa";
            }
            else if (optGroup.SelectedIndex == 4)//Nhà cung cấp
            {
                myDT = clsMain.ReturnDataTable("select MA,TEN from NHACUNGCAP where SUDUNG=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Nhà cung cấp";
            }
            else if (optGroup.SelectedIndex == 5)//Nhân viên
            {
                myDT = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Nhân viên";
            }
            else if (optGroup.SelectedIndex == 6)//Phương thức
            {
                myDT = clsMain.ReturnDataTable("select PT_MA as MA,PT_TEN as TEN from KHO_PHUONGTHUC where LOAIPHIEU_MA=1  Order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên phương thức" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = "Phương thức";
            }

            cboData.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboData.Properties.Columns.Add(info);
                }
                cboData.Properties.BestFitMode = BestFitMode.None;
                cboData.Properties.SearchMode = SearchMode.AutoFilter;
                cboData.Properties.DisplayMember = FieldName[1];
                cboData.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboData.Properties.Columns[0].Width = 100;
                    cboData.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboData.Properties.Columns[1].Width = 100;
                    cboData.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboData.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboData.EditValue == null || cboData.EditValue == DBNull.Value)
                    throw new Exception("Chưa chọn đối tượng cần xem");

                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_NHAPKHO ";
                sSQL += clsMain.SQLString(cboData.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue)) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue)) + ",";
                sSQL += optGroup.SelectedIndex;

                myDT = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
            this.Refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboData.Text;
                    if (ContainColumn("FILTER", myDT) == false)
                        myDT.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue);
                    if (ContainColumn("TUNGAY", myDT) == false)
                        myDT.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue);
                    if (ContainColumn("DENNGAY", myDT) == false)
                        myDT.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 109;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }
        
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {

                string MA = clsMain.SQLString(cboData.EditValue.ToString());
                string Ngaydi = clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue));
                string Ngayden = clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue));
                int Style = optGroup.SelectedIndex;
                string ID = clsMain.SQLString(gridView1.GetFocusedRowCellValue(MAVACH).ToString());
                Frm_BC_Chitietnhapxuat frm = new Frm_BC_Chitietnhapxuat(MA, Ngaydi, Ngayden, Style, ID, 1);
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
            }
            else
                XtraMessageBox.Show("Chưa chọn phiếu cần xem", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}