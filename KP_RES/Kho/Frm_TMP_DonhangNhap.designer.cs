﻿namespace KP_RES
{
    partial class Frm_TMP_DonhangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TMP_DonhangNhap));
            this.grc_NCC = new DevExpress.XtraGrid.GridControl();
            this.grv_ncctmp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_NGAYCHUNGTU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PT_TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_NHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_PT_MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grc_dshhtmp = new DevExpress.XtraGrid.GridControl();
            this.grv_dshhtmp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIENTCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_TIENCHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_GIAVON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_TIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmp_TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_mahanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_gianhap = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtvat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soserial = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cmb_tenhanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaphieutam = new DevExpress.XtraEditors.SimpleButton();
            this.btnChitiet = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.grc_NCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grv_ncctmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grc_dshhtmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grv_dshhtmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.SuspendLayout();
            // 
            // grc_NCC
            // 
            this.grc_NCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grc_NCC.Location = new System.Drawing.Point(0, 0);
            this.grc_NCC.MainView = this.grv_ncctmp;
            this.grc_NCC.Name = "grc_NCC";
            this.grc_NCC.Size = new System.Drawing.Size(1323, 338);
            this.grc_NCC.TabIndex = 0;
            this.grc_NCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grv_ncctmp});
            // 
            // grv_ncctmp
            // 
            this.grv_ncctmp.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grv_ncctmp.Appearance.FooterPanel.Options.UseFont = true;
            this.grv_ncctmp.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grv_ncctmp.Appearance.GroupPanel.Options.UseFont = true;
            this.grv_ncctmp.ColumnPanelRowHeight = 30;
            this.grv_ncctmp.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.tmp_NGAYCHUNGTU,
            this.TEN,
            this.KHO,
            this.TENNHANVIEN,
            this.tmp_GHICHU,
            this.PT_TEN,
            this.tmp_ID,
            this.tmp_NHACUNGCAP,
            this.tmp_KHO,
            this.tmp_NHANVIEN,
            this.tmp_PT_MA,
            this.FILL});
            this.grv_ncctmp.GridControl = this.grc_NCC;
            this.grv_ncctmp.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.grv_ncctmp.Name = "grv_ncctmp";
            this.grv_ncctmp.OptionsView.EnableAppearanceEvenRow = true;
            this.grv_ncctmp.OptionsView.ShowAutoFilterRow = true;
            this.grv_ncctmp.OptionsView.ShowFooter = true;
            this.grv_ncctmp.OptionsView.ShowIndicator = false;
            this.grv_ncctmp.RowHeight = 30;
            this.grv_ncctmp.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grv_ncctmp_CustomDrawCell);
            this.grv_ncctmp.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grv_ncctmp_FocusedRowChanged);
            this.grv_ncctmp.Click += new System.EventHandler(this.grv_ncctmp_Click);
            this.grv_ncctmp.DoubleClick += new System.EventHandler(this.grv_ncctmp_DoubleClick);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // tmp_NGAYCHUNGTU
            // 
            this.tmp_NGAYCHUNGTU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tmp_NGAYCHUNGTU.AppearanceCell.Options.UseFont = true;
            this.tmp_NGAYCHUNGTU.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_NGAYCHUNGTU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_NGAYCHUNGTU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.tmp_NGAYCHUNGTU.AppearanceHeader.Options.UseFont = true;
            this.tmp_NGAYCHUNGTU.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_NGAYCHUNGTU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_NGAYCHUNGTU.Caption = "Ngày chứng từ";
            this.tmp_NGAYCHUNGTU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.tmp_NGAYCHUNGTU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tmp_NGAYCHUNGTU.FieldName = "tmp_NGAYCHUNGTU";
            this.tmp_NGAYCHUNGTU.Name = "tmp_NGAYCHUNGTU";
            this.tmp_NGAYCHUNGTU.OptionsColumn.AllowEdit = false;
            this.tmp_NGAYCHUNGTU.OptionsColumn.AllowFocus = false;
            this.tmp_NGAYCHUNGTU.OptionsColumn.FixedWidth = true;
            this.tmp_NGAYCHUNGTU.Visible = true;
            this.tmp_NGAYCHUNGTU.VisibleIndex = 1;
            this.tmp_NGAYCHUNGTU.Width = 130;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Nhà cung cấp";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 5;
            this.TEN.Width = 200;
            // 
            // KHO
            // 
            this.KHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KHO.AppearanceCell.Options.UseFont = true;
            this.KHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KHO.AppearanceHeader.Options.UseFont = true;
            this.KHO.AppearanceHeader.Options.UseTextOptions = true;
            this.KHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KHO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHO.Caption = "Kho nhập";
            this.KHO.FieldName = "KHO";
            this.KHO.Name = "KHO";
            this.KHO.OptionsColumn.AllowEdit = false;
            this.KHO.OptionsColumn.AllowFocus = false;
            this.KHO.OptionsColumn.FixedWidth = true;
            this.KHO.Visible = true;
            this.KHO.VisibleIndex = 3;
            this.KHO.Width = 150;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.Caption = "Nhân viên nhập";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 2;
            this.TENNHANVIEN.Width = 200;
            // 
            // tmp_GHICHU
            // 
            this.tmp_GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_GHICHU.AppearanceCell.Options.UseFont = true;
            this.tmp_GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_GHICHU.AppearanceHeader.Options.UseFont = true;
            this.tmp_GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.tmp_GHICHU.Caption = "Ghi chú";
            this.tmp_GHICHU.FieldName = "tmp_GHICHU";
            this.tmp_GHICHU.Name = "tmp_GHICHU";
            this.tmp_GHICHU.OptionsColumn.AllowEdit = false;
            this.tmp_GHICHU.OptionsColumn.AllowFocus = false;
            this.tmp_GHICHU.Visible = true;
            this.tmp_GHICHU.VisibleIndex = 6;
            this.tmp_GHICHU.Width = 250;
            // 
            // PT_TEN
            // 
            this.PT_TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PT_TEN.AppearanceCell.Options.UseFont = true;
            this.PT_TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PT_TEN.AppearanceHeader.Options.UseFont = true;
            this.PT_TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.PT_TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PT_TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PT_TEN.Caption = "Phương thức";
            this.PT_TEN.FieldName = "PT_TEN";
            this.PT_TEN.Name = "PT_TEN";
            this.PT_TEN.OptionsColumn.AllowEdit = false;
            this.PT_TEN.OptionsColumn.AllowFocus = false;
            this.PT_TEN.OptionsColumn.FixedWidth = true;
            this.PT_TEN.Visible = true;
            this.PT_TEN.VisibleIndex = 4;
            this.PT_TEN.Width = 150;
            // 
            // tmp_ID
            // 
            this.tmp_ID.Caption = "tmp_ID";
            this.tmp_ID.FieldName = "tmp_ID";
            this.tmp_ID.Name = "tmp_ID";
            // 
            // tmp_NHACUNGCAP
            // 
            this.tmp_NHACUNGCAP.Caption = "tmp_NHACUNGCAP";
            this.tmp_NHACUNGCAP.FieldName = "tmp_NHACUNGCAP";
            this.tmp_NHACUNGCAP.Name = "tmp_NHACUNGCAP";
            // 
            // tmp_KHO
            // 
            this.tmp_KHO.Caption = "tmp_KHO";
            this.tmp_KHO.FieldName = "tmp_KHO";
            this.tmp_KHO.Name = "tmp_KHO";
            // 
            // tmp_NHANVIEN
            // 
            this.tmp_NHANVIEN.Caption = "tmp_NHANVIEN";
            this.tmp_NHANVIEN.FieldName = "tmp_NHANVIEN";
            this.tmp_NHANVIEN.Name = "tmp_NHANVIEN";
            // 
            // tmp_PT_MA
            // 
            this.tmp_PT_MA.Caption = "tmp_PT_MA";
            this.tmp_PT_MA.FieldName = "tmp_PT_MA";
            this.tmp_PT_MA.Name = "tmp_PT_MA";
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Width = 191;
            // 
            // grc_dshhtmp
            // 
            this.grc_dshhtmp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grc_dshhtmp.Location = new System.Drawing.Point(0, 338);
            this.grc_dshhtmp.MainView = this.grv_dshhtmp;
            this.grc_dshhtmp.Name = "grc_dshhtmp";
            this.grc_dshhtmp.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_mahanghoa,
            this.txt_soluong,
            this.txt_gianhap,
            this.txt_chietkhau,
            this.txtvat,
            this.txt_soserial,
            this.cmb_tenhanghoa});
            this.grc_dshhtmp.Size = new System.Drawing.Size(1323, 266);
            this.grc_dshhtmp.TabIndex = 0;
            this.grc_dshhtmp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grv_dshhtmp});
            // 
            // grv_dshhtmp
            // 
            this.grv_dshhtmp.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grv_dshhtmp.Appearance.FooterPanel.Options.UseFont = true;
            this.grv_dshhtmp.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grv_dshhtmp.Appearance.GroupPanel.Options.UseFont = true;
            this.grv_dshhtmp.ColumnPanelRowHeight = 30;
            this.grv_dshhtmp.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTHH,
            this.tmp_HANGHOA,
            this.TEN_HANGHOA,
            this.tmp_DVT,
            this.tmp_SOLUONG,
            this.tmp_DONGIA,
            this.THANHTIENTCK,
            this.tmp_CHIETKHAU,
            this.tmp_TIENCHIETKHAU,
            this.tmp_GIAVON,
            this.tmp_THANHTIEN,
            this.tmp_THUE,
            this.tmp_TIENTHUE,
            this.tmp_TONGCONG,
            this.FILL1});
            this.grv_dshhtmp.GridControl = this.grc_dshhtmp;
            this.grv_dshhtmp.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.grv_dshhtmp.Name = "grv_dshhtmp";
            this.grv_dshhtmp.OptionsView.EnableAppearanceEvenRow = true;
            this.grv_dshhtmp.OptionsView.ShowAutoFilterRow = true;
            this.grv_dshhtmp.OptionsView.ShowFooter = true;
            this.grv_dshhtmp.OptionsView.ShowIndicator = false;
            this.grv_dshhtmp.RowHeight = 30;
            this.grv_dshhtmp.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grv_dshhtmp_CustomDrawCell);
            this.grv_dshhtmp.Click += new System.EventHandler(this.grv_dshhtmp_Click);
            // 
            // STTHH
            // 
            this.STTHH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceCell.Options.UseFont = true;
            this.STTHH.AppearanceCell.Options.UseTextOptions = true;
            this.STTHH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceHeader.Options.UseFont = true;
            this.STTHH.AppearanceHeader.Options.UseTextOptions = true;
            this.STTHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTHH.Caption = "STT";
            this.STTHH.FieldName = "STTHH";
            this.STTHH.Name = "STTHH";
            this.STTHH.OptionsColumn.AllowEdit = false;
            this.STTHH.OptionsColumn.AllowFocus = false;
            this.STTHH.OptionsColumn.FixedWidth = true;
            this.STTHH.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STTHH.Visible = true;
            this.STTHH.VisibleIndex = 0;
            this.STTHH.Width = 50;
            // 
            // tmp_HANGHOA
            // 
            this.tmp_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.tmp_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_HANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.tmp_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_HANGHOA.Caption = "Mã";
            this.tmp_HANGHOA.FieldName = "tmp_HANGHOA";
            this.tmp_HANGHOA.Name = "tmp_HANGHOA";
            this.tmp_HANGHOA.OptionsColumn.AllowEdit = false;
            this.tmp_HANGHOA.OptionsColumn.AllowFocus = false;
            this.tmp_HANGHOA.Width = 51;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 189;
            // 
            // tmp_DVT
            // 
            this.tmp_DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_DVT.AppearanceCell.Options.UseFont = true;
            this.tmp_DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_DVT.AppearanceHeader.Options.UseFont = true;
            this.tmp_DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_DVT.Caption = "ĐVT";
            this.tmp_DVT.FieldName = "tmp_DVT";
            this.tmp_DVT.Name = "tmp_DVT";
            this.tmp_DVT.OptionsColumn.AllowEdit = false;
            this.tmp_DVT.OptionsColumn.AllowFocus = false;
            this.tmp_DVT.OptionsColumn.FixedWidth = true;
            this.tmp_DVT.Visible = true;
            this.tmp_DVT.VisibleIndex = 2;
            this.tmp_DVT.Width = 79;
            // 
            // tmp_SOLUONG
            // 
            this.tmp_SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_SOLUONG.AppearanceCell.Options.UseFont = true;
            this.tmp_SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.tmp_SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_SOLUONG.Caption = "SL";
            this.tmp_SOLUONG.DisplayFormat.FormatString = "n";
            this.tmp_SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tmp_SOLUONG.FieldName = "tmp_SOLUONG";
            this.tmp_SOLUONG.Name = "tmp_SOLUONG";
            this.tmp_SOLUONG.OptionsColumn.AllowEdit = false;
            this.tmp_SOLUONG.OptionsColumn.AllowFocus = false;
            this.tmp_SOLUONG.OptionsColumn.FixedWidth = true;
            this.tmp_SOLUONG.Visible = true;
            this.tmp_SOLUONG.VisibleIndex = 3;
            this.tmp_SOLUONG.Width = 80;
            // 
            // tmp_DONGIA
            // 
            this.tmp_DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_DONGIA.AppearanceCell.Options.UseFont = true;
            this.tmp_DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_DONGIA.AppearanceHeader.Options.UseFont = true;
            this.tmp_DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_DONGIA.Caption = "Giá nhập";
            this.tmp_DONGIA.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_DONGIA.FieldName = "tmp_DONGIA";
            this.tmp_DONGIA.Name = "tmp_DONGIA";
            this.tmp_DONGIA.OptionsColumn.AllowEdit = false;
            this.tmp_DONGIA.OptionsColumn.AllowFocus = false;
            this.tmp_DONGIA.OptionsColumn.FixedWidth = true;
            this.tmp_DONGIA.Visible = true;
            this.tmp_DONGIA.VisibleIndex = 4;
            this.tmp_DONGIA.Width = 99;
            // 
            // THANHTIENTCK
            // 
            this.THANHTIENTCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIENTCK.AppearanceCell.Options.UseFont = true;
            this.THANHTIENTCK.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIENTCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIENTCK.AppearanceHeader.Options.UseFont = true;
            this.THANHTIENTCK.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIENTCK.Caption = "Thành tiền TCK";
            this.THANHTIENTCK.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIENTCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIENTCK.FieldName = "tmp_THANHTIENTCK";
            this.THANHTIENTCK.Name = "THANHTIENTCK";
            this.THANHTIENTCK.OptionsColumn.AllowEdit = false;
            this.THANHTIENTCK.OptionsColumn.AllowFocus = false;
            this.THANHTIENTCK.OptionsColumn.FixedWidth = true;
            this.THANHTIENTCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tmp_THANHTIENTCK", "{0:#,###0}")});
            this.THANHTIENTCK.Visible = true;
            this.THANHTIENTCK.VisibleIndex = 5;
            this.THANHTIENTCK.Width = 142;
            // 
            // tmp_CHIETKHAU
            // 
            this.tmp_CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.tmp_CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.tmp_CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_CHIETKHAU.Caption = "CK";
            this.tmp_CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0.#}%";
            this.tmp_CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tmp_CHIETKHAU.FieldName = "tmp_CHIETKHAU";
            this.tmp_CHIETKHAU.Name = "tmp_CHIETKHAU";
            this.tmp_CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.tmp_CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.tmp_CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.tmp_CHIETKHAU.Visible = true;
            this.tmp_CHIETKHAU.VisibleIndex = 6;
            this.tmp_CHIETKHAU.Width = 50;
            // 
            // tmp_TIENCHIETKHAU
            // 
            this.tmp_TIENCHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TIENCHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.tmp_TIENCHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_TIENCHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_TIENCHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TIENCHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.tmp_TIENCHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_TIENCHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_TIENCHIETKHAU.Caption = "Tiền CK";
            this.tmp_TIENCHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_TIENCHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_TIENCHIETKHAU.FieldName = "tmp_TIENCHIETKHAU";
            this.tmp_TIENCHIETKHAU.Name = "tmp_TIENCHIETKHAU";
            this.tmp_TIENCHIETKHAU.OptionsColumn.AllowEdit = false;
            this.tmp_TIENCHIETKHAU.OptionsColumn.AllowFocus = false;
            this.tmp_TIENCHIETKHAU.OptionsColumn.FixedWidth = true;
            this.tmp_TIENCHIETKHAU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tmp_TIENCHIETKHAU", "{0:#,###0}")});
            this.tmp_TIENCHIETKHAU.Visible = true;
            this.tmp_TIENCHIETKHAU.VisibleIndex = 7;
            this.tmp_TIENCHIETKHAU.Width = 120;
            // 
            // tmp_GIAVON
            // 
            this.tmp_GIAVON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_GIAVON.AppearanceCell.Options.UseFont = true;
            this.tmp_GIAVON.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_GIAVON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_GIAVON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_GIAVON.AppearanceHeader.Options.UseFont = true;
            this.tmp_GIAVON.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_GIAVON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_GIAVON.Caption = "Giá Vốn";
            this.tmp_GIAVON.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_GIAVON.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_GIAVON.FieldName = "tmp_GIAVON";
            this.tmp_GIAVON.Name = "tmp_GIAVON";
            this.tmp_GIAVON.OptionsColumn.AllowEdit = false;
            this.tmp_GIAVON.OptionsColumn.AllowFocus = false;
            this.tmp_GIAVON.Width = 87;
            // 
            // tmp_THANHTIEN
            // 
            this.tmp_THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.tmp_THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.tmp_THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_THANHTIEN.Caption = "Thành tiền SCK";
            this.tmp_THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_THANHTIEN.FieldName = "tmp_THANHTIEN";
            this.tmp_THANHTIEN.Name = "tmp_THANHTIEN";
            this.tmp_THANHTIEN.OptionsColumn.AllowEdit = false;
            this.tmp_THANHTIEN.OptionsColumn.AllowFocus = false;
            this.tmp_THANHTIEN.OptionsColumn.FixedWidth = true;
            this.tmp_THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tmp_THANHTIEN", "{0:#,###0}")});
            this.tmp_THANHTIEN.Visible = true;
            this.tmp_THANHTIEN.VisibleIndex = 8;
            this.tmp_THANHTIEN.Width = 150;
            // 
            // tmp_THUE
            // 
            this.tmp_THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_THUE.AppearanceCell.Options.UseFont = true;
            this.tmp_THUE.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_THUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_THUE.AppearanceHeader.Options.UseFont = true;
            this.tmp_THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_THUE.Caption = "VAT";
            this.tmp_THUE.DisplayFormat.FormatString = "{0:#,###0}%";
            this.tmp_THUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_THUE.FieldName = "tmp_THUE";
            this.tmp_THUE.Name = "tmp_THUE";
            this.tmp_THUE.OptionsColumn.AllowEdit = false;
            this.tmp_THUE.OptionsColumn.AllowFocus = false;
            this.tmp_THUE.OptionsColumn.FixedWidth = true;
            this.tmp_THUE.Visible = true;
            this.tmp_THUE.VisibleIndex = 9;
            this.tmp_THUE.Width = 50;
            // 
            // tmp_TIENTHUE
            // 
            this.tmp_TIENTHUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TIENTHUE.AppearanceCell.Options.UseFont = true;
            this.tmp_TIENTHUE.AppearanceCell.Options.UseTextOptions = true;
            this.tmp_TIENTHUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tmp_TIENTHUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TIENTHUE.AppearanceHeader.Options.UseFont = true;
            this.tmp_TIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_TIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_TIENTHUE.Caption = "Tiền VAT";
            this.tmp_TIENTHUE.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_TIENTHUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_TIENTHUE.FieldName = "tmp_TIENTHUE";
            this.tmp_TIENTHUE.Name = "tmp_TIENTHUE";
            this.tmp_TIENTHUE.OptionsColumn.AllowEdit = false;
            this.tmp_TIENTHUE.OptionsColumn.AllowFocus = false;
            this.tmp_TIENTHUE.OptionsColumn.FixedWidth = true;
            this.tmp_TIENTHUE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tmp_TIENTHUE", "{0:#,###0}")});
            this.tmp_TIENTHUE.Visible = true;
            this.tmp_TIENTHUE.VisibleIndex = 10;
            this.tmp_TIENTHUE.Width = 120;
            // 
            // tmp_TONGCONG
            // 
            this.tmp_TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TONGCONG.AppearanceCell.Options.UseFont = true;
            this.tmp_TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmp_TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.tmp_TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.tmp_TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tmp_TONGCONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.tmp_TONGCONG.Caption = "Tổng cộng";
            this.tmp_TONGCONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.tmp_TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.tmp_TONGCONG.FieldName = "tmp_TONGCONG";
            this.tmp_TONGCONG.Name = "tmp_TONGCONG";
            this.tmp_TONGCONG.OptionsColumn.AllowEdit = false;
            this.tmp_TONGCONG.OptionsColumn.AllowFocus = false;
            this.tmp_TONGCONG.OptionsColumn.FixedWidth = true;
            this.tmp_TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tmp_TONGCONG", "{0:#,###0}")});
            this.tmp_TONGCONG.Visible = true;
            this.tmp_TONGCONG.VisibleIndex = 11;
            this.tmp_TONGCONG.Width = 150;
            // 
            // FILL1
            // 
            this.FILL1.Name = "FILL1";
            this.FILL1.OptionsColumn.AllowEdit = false;
            this.FILL1.OptionsColumn.AllowFocus = false;
            this.FILL1.Visible = true;
            this.FILL1.VisibleIndex = 12;
            this.FILL1.Width = 42;
            // 
            // txt_mahanghoa
            // 
            this.txt_mahanghoa.AutoHeight = false;
            this.txt_mahanghoa.Name = "txt_mahanghoa";
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            // 
            // txt_gianhap
            // 
            this.txt_gianhap.AutoHeight = false;
            this.txt_gianhap.Name = "txt_gianhap";
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.AutoHeight = false;
            this.txt_chietkhau.Name = "txt_chietkhau";
            // 
            // txtvat
            // 
            this.txtvat.AutoHeight = false;
            this.txtvat.Name = "txtvat";
            // 
            // txt_soserial
            // 
            this.txt_soserial.AutoHeight = false;
            this.txt_soserial.Name = "txt_soserial";
            // 
            // cmb_tenhanghoa
            // 
            this.cmb_tenhanghoa.AutoHeight = false;
            this.cmb_tenhanghoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb_tenhanghoa.Name = "cmb_tenhanghoa";
            // 
            // panel4
            // 
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.grc_dshhtmp);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 52);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1323, 604);
            this.panel4.TabIndex = 47;
            // 
            // panel5
            // 
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.grc_NCC);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1323, 338);
            this.panel5.TabIndex = 47;
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl1);
            this.header.Controls.Add(this.panelControl2);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1362, 52);
            this.header.TabIndex = 49;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(320, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1042, 52);
            this.panelControl1.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(1038, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSize = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl2.Controls.Add(this.title);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(81, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(239, 52);
            this.panelControl2.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(227, 32);
            this.title.TabIndex = 1;
            this.title.Text = "Danh sách phiếu tạm";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            this.pnlHome.Click += new System.EventHandler(this.pnlHome_Click);
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnBanphim);
            this.panelControl3.Controls.Add(this.btnXoaphieutam);
            this.panelControl3.Controls.Add(this.btnChitiet);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(1323, 52);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(39, 604);
            this.panelControl3.TabIndex = 50;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.btnXuongit);
            this.panelControl4.Controls.Add(this.btnXuongnhieu);
            this.panelControl4.Controls.Add(this.btnLenit);
            this.panelControl4.Controls.Add(this.btnLennhieu);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(2, 162);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 360);
            this.panelControl4.TabIndex = 14;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 160);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(35, 40);
            this.panelControl5.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 200);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 280);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 522);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnXoaphieutam
            // 
            this.btnXoaphieutam.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaphieutam.Appearance.Options.UseFont = true;
            this.btnXoaphieutam.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXoaphieutam.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaphieutam.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaphieutam.Location = new System.Drawing.Point(2, 82);
            this.btnXoaphieutam.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaphieutam.Name = "btnXoaphieutam";
            this.btnXoaphieutam.Size = new System.Drawing.Size(35, 80);
            this.btnXoaphieutam.TabIndex = 15;
            this.btnXoaphieutam.Text = "&In";
            this.btnXoaphieutam.Click += new System.EventHandler(this.btnXoaphieutam_Click);
            // 
            // btnChitiet
            // 
            this.btnChitiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChitiet.Appearance.Options.UseFont = true;
            this.btnChitiet.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnChitiet.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnChitiet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChitiet.Location = new System.Drawing.Point(2, 2);
            this.btnChitiet.Margin = new System.Windows.Forms.Padding(4);
            this.btnChitiet.Name = "btnChitiet";
            this.btnChitiet.Size = new System.Drawing.Size(35, 80);
            this.btnChitiet.TabIndex = 13;
            this.btnChitiet.Text = "&In";
            this.btnChitiet.Click += new System.EventHandler(this.grv_ncctmp_DoubleClick);
            // 
            // Frm_TMP_DonhangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 656);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.header);
            this.Name = "Frm_TMP_DonhangNhap";
            this.Text = "Danh Sách Phiếu Tạm";
            this.Load += new System.EventHandler(this.frm_TMP_DonhangNhap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grc_NCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grv_ncctmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grc_dshhtmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grv_dshhtmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grc_dshhtmp;
        private DevExpress.XtraGrid.Views.Grid.GridView grv_dshhtmp;
        private DevExpress.XtraGrid.Columns.GridColumn STTHH;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_DVT;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_THUE;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_TIENTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_GIAVON;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_TIENCHIETKHAU;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_mahanghoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_gianhap;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtvat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soserial;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cmb_tenhanghoa;
        private DevExpress.XtraGrid.GridControl grc_NCC;
        private DevExpress.XtraGrid.Views.Grid.GridView grv_ncctmp;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn KHO;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn PT_TEN;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_ID;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_NHACUNGCAP;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_KHO;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_NHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_PT_MA;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_TONGCONG;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnChitiet;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn FILL1;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIENTCK;
        private DevExpress.XtraGrid.Columns.GridColumn tmp_NGAYCHUNGTU;
        private DevExpress.XtraEditors.SimpleButton btnXoaphieutam;
    }
}