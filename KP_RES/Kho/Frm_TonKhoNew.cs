﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_TonKhoNew : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        public Frm_TonKhoNew()
        {
            InitializeComponent();
            SLTONDAUKY.Caption = "Số lượng \ntồn đầu kỳ";
            TRIGIATONDAUKY.Caption = "Trị giá \ntồn đầu kỳ";
            SLNHAP.Caption = "Số lượng \nnhập";
            TRIGIANHAP.Caption = "Trị giá \nnhập";
            SLXUAT.Caption = "Số lượng \nxuất";
            TRIGIAXUAT.Caption = "Trị giá \nxuất";
            SLTONCUOIKY.Caption = "Số lượng \ntồn cuối kỳ";
            TRIGIATONCUOIKY.Caption = "Trị giá \ntồn cuối kỳ";
            LoadCombo();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void  LoadCombo()
        {
            try
            {
                string sSQL = "";
                sSQL += "Select MA_CUAHANG as MA,TEN_CUAHANG as TEN" + "\n";
                sSQL += "From CUAHANG" + "\n";
                sSQL += "Where SUDUNG=1" + "\n";
                sSQL += "Order by TEN";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                cboCuahang.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboCuahang.EditValue =int.Parse (dt.Rows[0]["MA"].ToString());
                }

                sSQL  = "";
                sSQL += "Select MA_KHO as MA,TEN_KHO as TEN" + "\n";
                sSQL += "From KHO" + "\n";
                sSQL += "Where MA_CUAHANG=" + clsMain.SQLString(cboCuahang.EditValue.ToString()) + "\n";
                sSQL += "Order by TEN";
                dt = clsMain.ReturnDataTable(sSQL);
                cboKho.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboKho.EditValue = int.Parse ( dt.Rows[0]["MA"].ToString());
                }

                sSQL  = "";
                sSQL += "Select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN" + "\n";
                sSQL += "From NHOMHANG" + "\n";
                sSQL += "where SUDUNG=1" + "\n";
                sSQL += "Order by TEN";
                dt = clsMain.ReturnDataTable(sSQL);
                cboNhom.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                if (dt.Rows.Count > 0)
                {
                    cboNhom.EditValue = 0;
                }

                sSQL  = "";
                sSQL += "Select MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
                sSQL += "From HANGHOA" + "\n";
                sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(cboNhom.EditValue.ToString()) + "\n";
                sSQL += "Order by TEN";
                dt = clsMain.ReturnDataTable(sSQL);
                dt.Rows.Add("0", "Tất cả");
                cboHH.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboHH.EditValue = 0;
                }
                dtpTungay.EditValue = clsGlobal.gdServerDate;
                dtpDenngay.EditValue = clsGlobal.gdServerDate;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cboCuahang_EditValueChanged(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MA_KHO as MA,TEN_KHO as TEN" + "\n";
            sSQL += "From KHO" + "\n";
            sSQL += "Where MA_CUAHANG=" + clsMain.SQLString(cboCuahang.EditValue.ToString()) + "\n";
            sSQL += "Order by TEN";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboKho.Properties.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                cboKho.EditValue =int.Parse(dt.Rows[0]["MA"].ToString());
            }
        }

        private void cboNhom_EditValueChanged(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(cboNhom.EditValue.ToString()) + "\n";
            sSQL += "Order by TEN";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            dt.Rows.Add("0", "Tất cả");
            cboHH.Properties.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                cboHH.EditValue = 0;
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try 
            {
                if (cboKho.EditValue == null)
                {
                    XtraMessageBox.Show("Chưa chọn kho","Thông báo");
                    return;
                }
                gridControl2.DataSource = null;
                string nhom=string.Empty;
                string hh = string.Empty;
                if (cboNhom.EditValue == null)
                    nhom = "";
                else
                    nhom = cboNhom.EditValue.ToString();
                if (cboHH.EditValue == null)
                    hh = "";
                else
                    hh = cboHH.EditValue.ToString();
                string sSQL = "";
                sSQL += "EXEC SP_KHO_TONKHO_NEW @sCuaHang=" + clsMain.SQLString(cboCuahang.EditValue.ToString()) + ",";
                sSQL += "@sKho=" + clsMain.SQLString(cboKho.EditValue.ToString()) + ",";
                sSQL += "@sTuNgay =" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", dtpTungay.EditValue) + " 00:00:00") + ",";
                sSQL += "@sDenNgay=" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59") + ",";
                sSQL += "@sNhom=" + clsMain.SQLString(nhom) + ",";
                sSQL += "@sHangHoa=" + clsMain.SQLString(hh);
                dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    int n = dt.Rows.Count;
                    if (chkHientonkho.Checked)
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (float.Parse(dt.Rows[i]["SLTONDAUKY"].ToString()) == 0 && float.Parse(dt.Rows[i]["SLNHAP"].ToString()) == 0 && float.Parse(dt.Rows[i]["SLXUAT"].ToString()) == 0)
                            {
                                dt.Rows.RemoveAt(i);
                                i -= 1;
                                n -= 1;
                            }
                        }
                        gridControl2.DataSource = dt;
                    }
                    else
                        gridControl2.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo");
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView2.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl2.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl2.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            if (gridView2.RowCount > 0)
            {
                DataColumn colTUNGAY = new DataColumn();
                colTUNGAY.ColumnName = "TUNGAY";
                colTUNGAY.DataType = System.Type.GetType("System.String");
                colTUNGAY.DefaultValue = dtpTungay.Text ;
                DataColumn colDENNGAY = new DataColumn();
                colDENNGAY.ColumnName = "DENNGAY";
                colDENNGAY.DataType = System.Type.GetType("System.String");
                colDENNGAY.DefaultValue = dtpDenngay.Text;
                DataColumn colCUAHANG = new DataColumn();
                colCUAHANG.ColumnName = "CUAHANG";
                colCUAHANG.DataType = System.Type.GetType("System.String");
                colCUAHANG.DefaultValue = cboCuahang.Text;
                DataColumn colKHO = new DataColumn();
                colKHO.ColumnName = "KHO";
                colKHO.DataType = System.Type.GetType("System.String");
                colKHO.DefaultValue = cboKho.Text;
                DataColumn colNHOM = new DataColumn();
                colNHOM.ColumnName = "NHOM";
                colNHOM.DataType = System.Type.GetType("System.String");
                colNHOM.DefaultValue = cboNhom.Text;
                DataColumn colHANGHOA = new DataColumn();
                colHANGHOA.ColumnName = "HANGHOA";
                colHANGHOA.DataType = System.Type.GetType("System.String");
                colHANGHOA.DefaultValue = cboHH.Text;

                if (ContainColumn("CUAHANG", dt) == false)
                    dt.Columns.Add(colCUAHANG);
                if (ContainColumn("KHO", dt) == false)
                    dt.Columns.Add(colKHO);
                if (ContainColumn("NHOM", dt) == false)
                    dt.Columns.Add(colNHOM);
                if (ContainColumn("TUNGAY", dt) == false)
                    dt.Columns.Add(colTUNGAY);
                if (ContainColumn("DENNGAY", dt) == false)
                    dt.Columns.Add(colDENNGAY);
                if (ContainColumn("HANGHOA", dt) == false)
                    dt.Columns.Add(colHANGHOA);

                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 2;
                frm.ShowDialog();
                frm.Dispose();
            }
            else
            {
                XtraMessageBox.Show("Không có dữ liệu","Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl5.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl5.Visible = true;
            }
            this.Refresh();
        }
    }
}