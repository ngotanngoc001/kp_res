﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_BC_Chitietnhapxuat : Form
    {
        int NhapXuat;
        public Frm_BC_Chitietnhapxuat(string MA, string Tungay, string Denngay, int style, string ID, int mode)
        {
            if (mode == 0)                   //Xuất
            {
                InitializeComponent();
                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_CHITIETXUATKHO ";
                sSQL += MA + ", ";
                sSQL += Tungay + ", ";
                sSQL += Denngay + ", ";
                sSQL += style + ", ";
                sSQL += ID;
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = myDT;
                NhapXuat = 0;
            }
            else                             //Nhập
            {
                InitializeComponent();
                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_CHITIETNHAPKHO ";
                sSQL += MA + ", ";
                sSQL += Tungay + ", ";
                sSQL += Denngay + ", ";
                sSQL += style + ", ";
                sSQL += ID;
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = myDT;
                NhapXuat = 1;
            }

           
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Paint(object sender, PaintEventArgs e)
        {
            this.Close();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void Frm_BC_Banhang3_Load(object sender, EventArgs e)
        {
            if(NhapXuat == 0)
            {
                title.Text = "Báo cáo chi tiết xuất kho";
            }
            else
                title.Text = "Báo cáo chi tiết nhập kho";
        }
    }
}
