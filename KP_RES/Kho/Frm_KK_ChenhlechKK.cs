﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraEditors;

namespace KP_RES.Nghiepvu
{
    public partial class Frm_KK_ChenhlechKK : Form
    {
        
        public Frm_KK_ChenhlechKK()
        {
            InitializeComponent();
            DSChuaCANDOI();
            //DataTable dt = clsMain.ReturnDataTable("SELECT TOP 1 MAKK FROM KHO_KIEMKE ORDER BY MAKK DESC");
            //cboPhieu.Properties.DataSource = dt;
        }

        private void DSChuaCANDOI()
        {
            try
            {
                DataTable DTSOUCRE = new DataTable();
                DTSOUCRE.Columns.Add("MAKK", typeof(string));
                DTSOUCRE.Columns.Add("NGAYTAO", typeof(DateTime));
                DTSOUCRE.Columns.Add("MA_HANGHOA", typeof(int));
                DTSOUCRE.Columns.Add("TEN_HANGHOA", typeof(string));
                DTSOUCRE.Columns.Add("DVT", typeof(string));
                DTSOUCRE.Columns.Add("MA_KHO", typeof(string));
                DTSOUCRE.Columns.Add("TEN_KHO", typeof(string));
                DTSOUCRE.Columns.Add("SOLUONG", typeof(float));
                DTSOUCRE.Columns.Add("GIANHAP", typeof(float));
                DTSOUCRE.Columns.Add("SLTONKHO", typeof(float));
                DTSOUCRE.Columns.Add("SLCHENHLECH", typeof(float));
                DTSOUCRE.Columns.Add("CHECK", typeof(bool));
                DataTable dt = clsMain.ReturnDataTable(@"SELECT MAKK,NGAYTAO,h.MA_HANGHOA,TEN_DONVITINH AS DVT,h.TEN_HANGHOA,k.MA_KHO,
                (SELECT TEN_KHO FROM KHO WHERE MA_KHO = k.MA_KHO) AS TEN_KHO, k.SOLUONG,NGAY_KK FROM HANGHOA h,KHO_KIEMKE k,DONVITINH d 
                WHERE h.MA_HANGHOA = k.MA_HANGHOA AND D.MA_DONVITINH=H.MA_DONVITINH and DACANDOI=0");
                foreach (DataRow dr in dt.Rows)
                {
                    float slton = 0;
                    DataTable dthh = clsMain.ReturnDataTable("select GIANHAP from HANGHOA WHERE MA_HANGHOA='" + dr["MA_HANGHOA"].ToString() + "'");
                    DataTable tk = clsMain.ReturnDataTable(@"select DISTINCT ISNULL(SOLUONGTONCUOIKY,0) AS SOLUONGTONCUOIKY 
                                                             FROM HANGHOA H INNER JOIN TONKHO  T ON H.MA_HANGHOA=T.HANGHOA AND
                                                             T.KHO='" + dr["MA_KHO"].ToString() + "' AND T.HANGHOA='" + dr["MA_HANGHOA"].ToString() + "'");
                    if (tk.Rows.Count > 0)
                    {
                        slton = float.Parse(tk.Rows[0][0].ToString());
                    }

                    float cl = float.Parse(dr["SOLUONG"].ToString()) - slton;
                    DTSOUCRE.Rows.Add(dr["MAKK"].ToString(), dr["NGAYTAO"].ToString(), dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(),
                                      dr["DVT"].ToString(), dr["MA_KHO"].ToString(), dr["TEN_KHO"].ToString(),
                                      dr["SOLUONG"].ToString(), dthh.Rows[0]["GIANHAP"].ToString(), slton, cl.ToString(), 0);
                }
                gridControl1.DataSource = DTSOUCRE;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            DSChuaCANDOI();
        }
        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }


        private void chkALL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkALL.Checked)
            {
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    gridView1.SetRowCellValue(i, "CHECK", 1);
                }
            }
            else
            {
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    gridView1.SetRowCellValue(i, "CHECK", 0);
                }
            }
        }

        private void btnNhapCD_Click(object sender, EventArgs e)
        {
            try
            {
                checkdaura(true);
                DataTable dt = new DataTable();
                dt.Columns.Add("MA_KHO", typeof(int));
                dt.Columns.Add("TEN_KHO", typeof(string));
                dt.Columns.Add("MA", typeof(int));
                dt.Columns.Add("TEN", typeof(string));
                dt.Columns.Add("DVT", typeof(string));
                dt.Columns.Add("SL", typeof(float));
                dt.Columns.Add("GIANHAP", typeof(float));
                dt.Columns.Add("GIAVON", typeof(float));
                dt.Columns.Add("THANHTIENTCK", typeof(float));
                dt.Columns.Add("CK", typeof(float));
                dt.Columns.Add("TIENCK", typeof(float));
                dt.Columns.Add("VAT", typeof(float));
                dt.Columns.Add("TIENVAT", typeof(float));
                dt.Columns.Add("THANHTIEN", typeof(float));
                dt.Columns.Add("TONGCONG", typeof(float));
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()) == true)
                    {
                        float TT = float.Parse(gridView1.GetRowCellValue(i, "GIANHAP").ToString()) * Math.Abs(float.Parse(gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString()));
                        dt.Rows.Add(gridView1.GetRowCellValue(i, "MA_KHO").ToString(), gridView1.GetRowCellValue(i, "TEN_KHO").ToString(),
                        gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString(), gridView1.GetRowCellValue(i, "TEN_HANGHOA").ToString(),
                        gridView1.GetRowCellValue(i, "DVT").ToString(), gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString(),
                        gridView1.GetRowCellValue(i, "GIANHAP").ToString(), gridView1.GetRowCellValue(i, "GIANHAP").ToString(), TT, 0, 0, 0, 0, TT, TT);
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource =dt;
                    frm.Mode = 29;
                    frm.ShowDialog();
                    frm.Dispose();
                    if (cls_KP_RES.Fcandoi)//Nhập cân đối thành công
                    {
                        cls_KP_RES.Fcandoi = false;
                        int n = gridView1.DataRowCount;
                        string sql = string.Empty;
                        for (int i = 0; i < n; i++)
                        {
                            if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()) == true)
                            {
                                sql += "update KHO_KIEMKE set DACANDOI=1 where MA_HANGHOA='" + gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString() + "' and MAKK='"
                                       + gridView1.GetRowCellValue(i, "MAKK").ToString() + "' and MA_KHO='" + gridView1.GetRowCellValue(i, "MA_KHO").ToString() + "' \n";
                            }
                        }
                        if (clsMain.ExecuteSQL(sql))
                            DSChuaCANDOI();//Cập nhật lại
                    }
                }
                else
                    throw new Exception("Chưa chọn dòng nào để cân đối");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatCD_Click(object sender, EventArgs e)
        {
            try
            {
                checkdaura(false);
                DataTable dt = new DataTable();
                dt.Columns.Add("MA_KHO", typeof(int));
                dt.Columns.Add("TEN_KHO", typeof(string));
                dt.Columns.Add("MA", typeof(int));
                dt.Columns.Add("TEN", typeof(string));
                dt.Columns.Add("DVT", typeof(string));
                dt.Columns.Add("SL", typeof(float));
                dt.Columns.Add("GIAXUAT", typeof(float));
                dt.Columns.Add("VAT", typeof(float));
                dt.Columns.Add("TIENVAT", typeof(float));
                dt.Columns.Add("THANHTIEN", typeof(float));
                dt.Columns.Add("TONGCONG", typeof(float));
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()) == true)
                    {
                        float TT = float.Parse(gridView1.GetRowCellValue(i, "GIANHAP").ToString()) * Math.Abs(float.Parse(gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString()));
                        dt.Rows.Add(gridView1.GetRowCellValue(i, "MA_KHO").ToString(), gridView1.GetRowCellValue(i, "TEN_KHO").ToString(),
                        gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString(), gridView1.GetRowCellValue(i, "TEN_HANGHOA").ToString(),
                        gridView1.GetRowCellValue(i, "DVT").ToString(), Math.Abs(float.Parse(gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString())),
                        gridView1.GetRowCellValue(i, "GIANHAP").ToString(), 0, 0, TT, TT);
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 30;
                    frm.ShowDialog();
                    frm.Dispose();
                    if (cls_KP_RES.Fcandoi)//xuất cân đối thành công
                    {
                        cls_KP_RES.Fcandoi = false;
                        int n = gridView1.DataRowCount;
                        string sql = string.Empty;
                        for (int i = 0; i < n; i++)
                        {
                            if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()) == true)
                            {
                                sql += "update KHO_KIEMKE set DACANDOI=1 where MA_HANGHOA='" + gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString() + "' and MAKK='"
                                       + gridView1.GetRowCellValue(i, "MAKK").ToString() + "' and MA_KHO='" + gridView1.GetRowCellValue(i, "MA_KHO").ToString() + "' \n";
                            }
                        }
                        if (clsMain.ExecuteSQL(sql))
                            DSChuaCANDOI();//cập nhật lại
                    }
                }
                else
                    throw new Exception("Chưa chọn dòng nào để cân đối");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void checkdaura(bool nhapxuat)
        {
            DataTable dtcheck=new DataTable();
            dtcheck.Columns.Add("MA_KHO", typeof(int));
            dtcheck.Columns.Add("SOLUONG", typeof(float));
            for (int i = 0; i < gridView1.DataRowCount; i++)
            {
                if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()))
                {
                    dtcheck.Rows.Add(int.Parse(gridView1.GetRowCellValue(i, "MA_KHO").ToString()), float.Parse(gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString()));
                }
            }
            if (dtcheck.Rows.Count > 0)
            {
                for (int k = 0; k < dtcheck.Rows.Count; k++)
                {
                    if (nhapxuat)
                    {
                        if (float.Parse(dtcheck.Rows[k]["SOLUONG"].ToString()) < 0)
                            throw new Exception("Sản phẩm kiểm kê ít hơn trong kho");
                    }
                    else
                    {
                        if (float.Parse(dtcheck.Rows[k]["SOLUONG"].ToString()) > 0)
                            throw new Exception("Sản phẩm kiểm kê nhiều hơn trong kho");
                    }
                    if (float.Parse(dtcheck.Rows[k]["SOLUONG"].ToString()).Equals(0))
                        throw new Exception("Không chọn sản phẩm đã cân đối");
                    for (int h = k + 1; h < dtcheck.Rows.Count; h++)
                    {
                        if (dtcheck.Rows[k]["MA_KHO"].ToString() != dtcheck.Rows[h]["MA_KHO"].ToString())
                            throw new Exception("Sản phẩm được chọn phải cùng kho");
                    }
                }
            }
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkcandoi()
        {
            bool flag = true;
            int n=gridView1.DataRowCount;
            for (int i = 0; i < n; i++)
            {
                if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()))
                {
                    if (int.Parse(gridView1.GetRowCellValue(i, "SLCHENHLECH").ToString()) != 0)
                    {
                        flag=false;
                        break;
                    }
                }
            }
            return flag;
        }

        private void btnCandoi_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = checkcandoi();
                string sql = string.Empty;
                if (!flag)
                {
                    if (DialogResult.Yes == (XtraMessageBox.Show("Tồn tại hàng hóa số lượng chênh lệch khác 0,bạn có muốn tiếp tục ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                    {
                        int n = gridView1.DataRowCount;
                        for (int i = 0; i < n; i++)
                        {
                            if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()))
                            {
                                sql += "update KHO_KIEMKE set DACANDOI=1 where MA_HANGHOA='" + gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString() + "' and MAKK='"
                                    + gridView1.GetRowCellValue(i, "MAKK").ToString() + "' and MA_KHO='" + gridView1.GetRowCellValue(i, "MA_KHO").ToString() + "' \n";
                            }
                        }
                        if (clsMain.ExecuteSQL(sql))
                        {
                            XtraMessageBox.Show("Hàng hóa đã cân đối", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DSChuaCANDOI();
                        }
                        else
                        {
                            XtraMessageBox.Show("Cân đối không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

                else
                {
                    int n = gridView1.DataRowCount;
                    for (int i = 0; i < n; i++)
                    {
                        if (bool.Parse(gridView1.GetRowCellValue(i, "CHECK").ToString()))
                        {
                            sql += "update KHO_KIEMKE set DACANDOI=1 where MA_HANGHOA='" + gridView1.GetRowCellValue(i, "MA_HANGHOA").ToString() + "' and MAKK='"
                                + gridView1.GetRowCellValue(i, "MAKK").ToString() + "' and MA_KHO='" + gridView1.GetRowCellValue(i, "MA_KHO").ToString() + "' \n";
                        }
                    }
                    if (clsMain.ExecuteSQL(sql))
                    {
                        XtraMessageBox.Show("Hàng hóa đã cân đối", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DSChuaCANDOI();
                    }
                    else
                    {
                        XtraMessageBox.Show("Cân đối không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }
    }   
}
