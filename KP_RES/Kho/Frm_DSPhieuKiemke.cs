﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_DSPhieuKiemke : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        DataTable dt1;
        public Frm_DSPhieuKiemke()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_DSPhieuKiemke_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Cửa hàng
            {
                dt1 = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Cửa hàng";
            }
            else if (optGroup.SelectedIndex == 1)//Kho
            {
                dt1 = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Kho";
            }
            else if (optGroup.SelectedIndex == 2)//Nhóm hàng
            {
                dt1 = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhóm hàng";
            }
            else if (optGroup.SelectedIndex == 3)//Hàng hóa
            {
                dt1 = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên hàng hóa" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Hàng hóa";
            }
            else if (optGroup.SelectedIndex == 4 )//Nhân viên
            {
                dt1 = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhân viên";
            }
            else if (optGroup.SelectedIndex == 5)//Nhân viên
            {
                dt1 = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt1;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Người tạo";
            }
            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt1.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt1.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt1.Rows[0][0].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value)
                    throw new Exception("Chưa chọn đối tượng cần xem");
                string _tungaydenngay = "";
                _tungaydenngay += "NGAYTAO Between " + clsMain.SQLString(string.Format("{0:yyyyMMdd}", dtpTungay.EditValue)) + "\n"; 
                _tungaydenngay += "AND '" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59'";
                if (optGroup.SelectedIndex == 0)//Cửa hàng
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,KHO K,DM_NHANVIEN NV" + "\n";
                    sSQL += "WHERE NV.MANHANVIEN=PK.MANHANVIEN AND PK.MA_KHO=K.MA_KHO" + "\n";
                    sSQL += "AND K.MA_CUAHANG=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 1)//Kho
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,DM_NHANVIEN NV" + "\n";
                    sSQL += "WHERE NV.MANHANVIEN=PK.MANHANVIEN" + "\n";
                    sSQL += "AND MA_KHO=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 2)//Nhóm hàng
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,DM_NHANVIEN NV,NHOMHANG NH, HANGHOA HH" + "\n";
                    sSQL += "WHERE NV.MANHANVIEN=PK.MANHANVIEN AND PK.MA_HANGHOA=HH.MA_HANGHOA AND HH.MA_NHOMHANG=NH.MA_NHOMHANG" + "\n";
                    sSQL += "AND NH.MA_NHOMHANG=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 3)//Hàng hóa
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,DM_NHANVIEN NV WHERE NV.MANHANVIEN=PK.MANHANVIEN" + "\n";
                    sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 4)//Nhân viên
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,DM_NHANVIEN NV WHERE NV.MANHANVIEN=PK.MANHANVIEN" + "\n";
                    sSQL += "AND PK.MANHANVIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 5)//Nhân viên
                {
                    sSQL += "Select DISTINCT MAKK,TENNHANVIEN,NGAYTAO,NGAY_KK,PK.GHICHU" + "\n";
                    sSQL += "From KHO_KIEMKE PK,DM_NHANVIEN NV WHERE NV.MANHANVIEN=PK.MANHANVIEN" + "\n";
                    sSQL += "AND PK.NGUOITAO=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    sSQL += "AND " + _tungaydenngay;
                }
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    string sSQL = "";
                    sSQL += "SELECT MAKK as SOPHIEU,NGAYTAO,PK.MA_KHO,TEN_KHO,PK.MA_HANGHOA,TENNHANVIEN," + "\n";
                    sSQL += "TEN_HANGHOA,TEN_DONVITINH,PK.SOLUONG,PK.GHICHU" + "\n";
                    sSQL += "FROM KHO_KIEMKE PK,KHO K,HANGHOA H,DONVITINH D,DM_NHANVIEN NV" + "\n";
                    sSQL += "WHERE PK.MA_KHO=K.MA_KHO AND PK.MA_HANGHOA=H.MA_HANGHOA " + "\n";
                    sSQL += "AND NV.MANHANVIEN=PK.MANHANVIEN AND H.MA_DONVITINH=D.MA_DONVITINH" + "\n";
                    sSQL += "AND MAKK=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MAKK).ToString());

                    frm.dtSource = clsMain.ReturnDataTable(sSQL);
                    frm.Mode = 10;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa chọn phiếu cần in");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                Frm_BCChitietKiemke frm = new Frm_BCChitietKiemke(gridView1.GetFocusedRowCellValue(MAKK).ToString());
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
            }
            else
                XtraMessageBox.Show("Chưa chọn phiếu cần xem", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                Fill .Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                Fill.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
            this.Refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 51;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }







    }
}