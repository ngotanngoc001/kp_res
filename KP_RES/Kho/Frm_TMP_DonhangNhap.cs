﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraEditors;

namespace KP_RES
{
    public partial class Frm_TMP_DonhangNhap : Form
    {
        BindingSource bs = new BindingSource();
        DataTable DTSOUCRE = new DataTable();
        DataTable dtdshh = new DataTable();
        bool flag = false;
        string mancc = "";
        string makho = "";
        string manv = "";
        string mapt = "";
        string note = "";
        string ngayct = "";
        int idncc;
        string grid = "grv_ncctmp";

        public Frm_TMP_DonhangNhap(string nv)
        {
            InitializeComponent();
            manv = nv;
        }

        private void frm_TMP_DonhangNhap_Load(object sender, EventArgs e)
        {
            loaddatagridview();
            khoitaodatatruyen();
        }

        private void loaddatagridview()
        {
            string sql = "select distinct TEN,TEN_KHO as KHO,TENNHANVIEN,PT_TEN,tmp_ID,tmp_NHACUNGCAP,tmp_KHO,tmp_NHANVIEN,tmp_GHICHU,tmp_PT_MA,";
            sql += "tmp_NGAYCHUNGTU from TMP_DSNCC t,NHACUNGCAP n,HANGHOA h,KHO k,DM_NHANVIEN nv,KHO_PHUONGTHUC pt ";
            sql += "where t.tmp_NHACUNGCAP=n.MA and t.tmp_KHO=k.MA_KHO and t.tmp_NHANVIEN=nv.MANHANVIEN and pt.PT_MA=t.tmp_PT_MA ";
            sql += "and tmp_LOAI=1 and tmp_NHANVIEN='" + manv + "'";
            DataTable dt = clsMain.ReturnDataTable(sql);
            grc_NCC.DataSource = dt;
        }

        private void loadDSHH()
        {
            string sql = "select tmp_HANGHOA,TEN_HANGHOA,tmp_DVT,tmp_THUE,tmp_SOLUONG,tmp_DONGIA,tmp_SOLUONG*tmp_DONGIA As tmp_THANHTIENTCK,tmp_TIENTHUE,tmp_THANHTIEN,tmp_TONGCONG,tmp_MAVACH,";
            sql += "tmp_GIAVON,tmp_CHIETKHAU,tmp_TIENCHIETKHAU from TMP_DSHH thh,HANGHOA h where thh.tmp_HANGHOA=h.MA_HANGHOA and tmp_ID=" + idncc;
            dtdshh = clsMain.ReturnDataTable(sql);
            grc_dshhtmp.DataSource = dtdshh;
        }

        private void khoitaodatatruyen()
        {
            DTSOUCRE.Columns.Add("MA", typeof(string));
            DTSOUCRE.Columns.Add("TEN", typeof(string));
            DTSOUCRE.Columns.Add("DVT", typeof(string));
            DTSOUCRE.Columns.Add("SL", typeof(float));
            DTSOUCRE.Columns.Add("GIANHAP", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIENTCK", typeof(float));
            DTSOUCRE.Columns.Add("CK", typeof(float));
            DTSOUCRE.Columns.Add("TIENCK", typeof(float));
            DTSOUCRE.Columns.Add("GIAVON", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIEN", typeof(float));
            DTSOUCRE.Columns.Add("VAT", typeof(float));
            DTSOUCRE.Columns.Add("TIENVAT", typeof(float));
            DTSOUCRE.Columns.Add("TONGCONG", typeof(float));
            DTSOUCRE.Columns.Add("MA_VACH", typeof(string));
            //DTSOUCRE.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null);
        }

        private void truyendulieu()
        {
            DTSOUCRE.Clear();
            if (dtdshh.Rows.Count > 0)
            {
                for (int i = 0; i <= dtdshh.Rows.Count - 1; i++)
                {
                    DataRow dr = DTSOUCRE.NewRow();
                    dr["MA"] = dtdshh.Rows[i]["tmp_HANGHOA"];
                    dr["TEN"] = dtdshh.Rows[i]["TEN_HANGHOA"];
                    dr["DVT"] = dtdshh.Rows[i]["tmp_DVT"];
                    dr["SL"] = dtdshh.Rows[i]["tmp_SOLUONG"];
                    dr["GIANHAP"] = dtdshh.Rows[i]["tmp_DONGIA"];
                    dr["THANHTIENTCK"] = dtdshh.Rows[i]["tmp_THANHTIENTCK"];
                    dr["CK"] = dtdshh.Rows[i]["tmp_CHIETKHAU"];
                    dr["TIENCK"] = dtdshh.Rows[i]["tmp_TIENCHIETKHAU"];
                    dr["GIAVON"] = dtdshh.Rows[i]["tmp_GIAVON"];
                    dr["THANHTIEN"] = dtdshh.Rows[i]["tmp_THANHTIEN"];
                    dr["VAT"] = dtdshh.Rows[i]["tmp_THUE"];
                    dr["TIENVAT"] = dtdshh.Rows[i]["tmp_TIENTHUE"];
                    dr["TONGCONG"] = dtdshh.Rows[i]["tmp_TONGCONG"];
                    dr["MA_VACH"] = dtdshh.Rows[i]["tmp_MAVACH"];
                    DTSOUCRE.Rows.Add(dr);
                    DTSOUCRE.AcceptChanges();
                }
                Frm_Nhapkho.tmp_MANCC = mancc;
                Frm_Nhapkho.tmp_MAKHONHAP = makho;
                Frm_Nhapkho.tmp_PT_MAPHUONGTHUC = mapt;
                Frm_Nhapkho.tmp_Ngayct = ngayct;
                Frm_Nhapkho.tmp_NOTE = note;
                Frm_Nhapkho.tmp_ID = idncc;
                Frm_Nhapkho.tmp_MAPHIEU = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (1)").Rows[0][0].ToString();
                Frm_Nhapkho.BS.DataSource = DTSOUCRE;
                this.Close();
            }

        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_ncctmp")
                {
                    grv_ncctmp.FocusedRowHandle = grv_ncctmp.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (grv_ncctmp.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    grv_dshhtmp.FocusedRowHandle = grv_dshhtmp.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (grv_dshhtmp.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_ncctmp")
                {
                    grv_ncctmp.FocusedRowHandle = grv_ncctmp.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (grv_ncctmp.FocusedRowHandle == grv_ncctmp.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    grv_dshhtmp.FocusedRowHandle = grv_dshhtmp.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (grv_dshhtmp.FocusedRowHandle == grv_dshhtmp.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_ncctmp")
                {
                    grv_ncctmp.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    grv_dshhtmp.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_ncctmp")
                {
                    grv_ncctmp.FocusedRowHandle = grv_ncctmp.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    grv_dshhtmp.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void grv_ncctmp_Click(object sender, EventArgs e)
        {
            grid = "grv_ncctmp";
            if (grv_ncctmp.FocusedRowHandle >= 0)
            {
                if (grv_ncctmp.SelectedRowsCount > 0)
                {
                    idncc = Convert.ToInt32(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_ID"]).ToString());
                    mancc = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NHACUNGCAP"]).ToString());
                    makho = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_KHO"]).ToString());
                    manv = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NHANVIEN"]).ToString());
                    mapt = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_PT_MA"]).ToString());
                    note = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_GHICHU"]).ToString());
                    ngayct = string.Format("{0:dd/MM/yyyy}", grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NGAYCHUNGTU"]));
                    loadDSHH();
                }
            }
        }

        private void grv_ncctmp_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grv_ncctmp.FocusedRowHandle >= 0)
            {
                if (grv_ncctmp.SelectedRowsCount > 0)
                {
                    if (flag)
                    {
                        grv_ncctmp.FocusedRowHandle = 0;
                        flag = false;
                    }
                    idncc = Convert.ToInt32(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_ID"]).ToString());
                    mancc = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NHACUNGCAP"]).ToString());
                    makho = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_KHO"]).ToString());
                    manv = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NHANVIEN"]).ToString());
                    mapt = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_PT_MA"]).ToString());
                    note = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_GHICHU"]).ToString());
                    ngayct = Convert.ToString(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_NGAYCHUNGTU"]).ToString());
                    loadDSHH();
                }
            }
        }

        private void grv_ncctmp_DoubleClick(object sender, EventArgs e)
        {
            truyendulieu();
        }

        private void grv_ncctmp_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void grv_dshhtmp_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void grv_dshhtmp_Click(object sender, EventArgs e)
        {
            grid = "grv_dshhtmp";
        }

        private void btnXoaphieutam_Click(object sender, EventArgs e)
        {
            try
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                string sql  = "delete TMP_DSHH where tmp_ID='" + idncc + "'\n";
                       sql += "delete TMP_DSNCC where tmp_ID='" + idncc + "'\n";
                if (clsMain.ExecuteSQL(sql))
                {
                    flag = true;
                    loaddatagridview();
                    if (grv_ncctmp.RowCount <= 0)
                    {
                        grc_dshhtmp.DataSource = null;
                    }
                    else
                    {
                        idncc = Convert.ToInt32(grv_ncctmp.GetRowCellValue(grv_ncctmp.FocusedRowHandle, grv_ncctmp.Columns["tmp_ID"]).ToString());
                        loadDSHH();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }   
}
