﻿namespace KP_RES 
{
    partial class Frm_Nhapkho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Nhapkho));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnLayPhieuXuat = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayPhieuNhap = new DevExpress.XtraEditors.SimpleButton();
            this.chkCONGNO = new DevExpress.XtraEditors.CheckEdit();
            this.pnlCongNo = new DevExpress.XtraEditors.PanelControl();
            this.dtpNgayHenTra = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDaTra = new DevExpress.XtraEditors.TextEdit();
            this.txtTienConLai = new DevExpress.XtraEditors.TextEdit();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.txtNVnhap = new DevExpress.XtraEditors.TextEdit();
            this.txtSophieu = new DevExpress.XtraEditors.TextEdit();
            this.btnLayphieutam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_tam = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgay = new DevExpress.XtraEditors.DateEdit();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cboKho = new DevExpress.XtraEditors.LookUpEdit();
            this.cboNCC = new DevExpress.XtraEditors.LookUpEdit();
            this.cboPT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_mavach2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_HANGHOA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_ten2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_HANGHOA3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GIANHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_gianhap = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GIABAN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIENTCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.TIENCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIAVON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_vat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.TIENVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnFile_Mo = new DevExpress.XtraEditors.SimpleButton();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCONGNO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCongNo)).BeginInit();
            this.pnlCongNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDaTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienConLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVnhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_mavach2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnLayPhieuXuat);
            this.panelControl1.Controls.Add(this.btnLayPhieuNhap);
            this.panelControl1.Controls.Add(this.chkCONGNO);
            this.panelControl1.Controls.Add(this.pnlCongNo);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.txtNVnhap);
            this.panelControl1.Controls.Add(this.txtSophieu);
            this.panelControl1.Controls.Add(this.btnLayphieutam);
            this.panelControl1.Controls.Add(this.btn_tam);
            this.panelControl1.Controls.Add(this.dtpNgay);
            this.panelControl1.Controls.Add(this.txtGHICHU);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cboKho);
            this.panelControl1.Controls.Add(this.cboNCC);
            this.panelControl1.Controls.Add(this.cboPT);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lblGHICHU);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1741, 145);
            this.panelControl1.TabIndex = 0;
            // 
            // btnLayPhieuXuat
            // 
            this.btnLayPhieuXuat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhieuXuat.Appearance.Options.UseFont = true;
            this.btnLayPhieuXuat.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnLayPhieuXuat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLayPhieuXuat.Location = new System.Drawing.Point(776, 103);
            this.btnLayPhieuXuat.Margin = new System.Windows.Forms.Padding(4);
            this.btnLayPhieuXuat.Name = "btnLayPhieuXuat";
            this.btnLayPhieuXuat.Size = new System.Drawing.Size(146, 35);
            this.btnLayPhieuXuat.TabIndex = 120;
            this.btnLayPhieuXuat.Text = "&Lấy phiếu xuất";
            this.btnLayPhieuXuat.Click += new System.EventHandler(this.btnLayPhieuXuat_Click);
            // 
            // btnLayPhieuNhap
            // 
            this.btnLayPhieuNhap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhieuNhap.Appearance.Options.UseFont = true;
            this.btnLayPhieuNhap.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnLayPhieuNhap.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLayPhieuNhap.Location = new System.Drawing.Point(622, 103);
            this.btnLayPhieuNhap.Margin = new System.Windows.Forms.Padding(4);
            this.btnLayPhieuNhap.Name = "btnLayPhieuNhap";
            this.btnLayPhieuNhap.Size = new System.Drawing.Size(146, 35);
            this.btnLayPhieuNhap.TabIndex = 119;
            this.btnLayPhieuNhap.Text = "&Lấy phiếu nhập";
            this.btnLayPhieuNhap.Click += new System.EventHandler(this.btnLayPhieuNhap_Click);
            // 
            // chkCONGNO
            // 
            this.chkCONGNO.EnterMoveNextControl = true;
            this.chkCONGNO.Location = new System.Drawing.Point(968, 103);
            this.chkCONGNO.Name = "chkCONGNO";
            this.chkCONGNO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCONGNO.Properties.Appearance.Options.UseFont = true;
            this.chkCONGNO.Properties.Caption = "Công nợ";
            this.chkCONGNO.Size = new System.Drawing.Size(92, 24);
            this.chkCONGNO.TabIndex = 118;
            this.chkCONGNO.CheckedChanged += new System.EventHandler(this.chkCONGNO_CheckedChanged);
            // 
            // pnlCongNo
            // 
            this.pnlCongNo.Controls.Add(this.dtpNgayHenTra);
            this.pnlCongNo.Controls.Add(this.labelControl5);
            this.pnlCongNo.Controls.Add(this.labelControl7);
            this.pnlCongNo.Controls.Add(this.labelControl8);
            this.pnlCongNo.Controls.Add(this.txtTienDaTra);
            this.pnlCongNo.Controls.Add(this.txtTienConLai);
            this.pnlCongNo.Location = new System.Drawing.Point(970, 5);
            this.pnlCongNo.Name = "pnlCongNo";
            this.pnlCongNo.Size = new System.Drawing.Size(222, 92);
            this.pnlCongNo.TabIndex = 117;
            // 
            // dtpNgayHenTra
            // 
            this.dtpNgayHenTra.EditValue = null;
            this.dtpNgayHenTra.EnterMoveNextControl = true;
            this.dtpNgayHenTra.Location = new System.Drawing.Point(70, 63);
            this.dtpNgayHenTra.Name = "dtpNgayHenTra";
            this.dtpNgayHenTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHenTra.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayHenTra.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHenTra.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayHenTra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayHenTra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHenTra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHenTra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayHenTra.Size = new System.Drawing.Size(148, 26);
            this.dtpNgayHenTra.TabIndex = 118;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(6, 5);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 19);
            this.labelControl5.TabIndex = 114;
            this.labelControl5.Text = "Đã trả";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl7.Location = new System.Drawing.Point(6, 66);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 19);
            this.labelControl7.TabIndex = 117;
            this.labelControl7.Text = "Ngày trả";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(6, 37);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 19);
            this.labelControl8.TabIndex = 115;
            this.labelControl8.Text = "Còn lại";
            // 
            // txtTienDaTra
            // 
            this.txtTienDaTra.EditValue = "0";
            this.txtTienDaTra.Location = new System.Drawing.Point(70, 2);
            this.txtTienDaTra.Name = "txtTienDaTra";
            this.txtTienDaTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTienDaTra.Properties.Appearance.Options.UseFont = true;
            this.txtTienDaTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDaTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDaTra.Properties.Mask.EditMask = "N0";
            this.txtTienDaTra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDaTra.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienDaTra.Size = new System.Drawing.Size(148, 26);
            this.txtTienDaTra.TabIndex = 112;
            this.txtTienDaTra.TextChanged += new System.EventHandler(this.txtTienDaTra_TextChanged);
            // 
            // txtTienConLai
            // 
            this.txtTienConLai.EditValue = "0";
            this.txtTienConLai.Location = new System.Drawing.Point(70, 33);
            this.txtTienConLai.Name = "txtTienConLai";
            this.txtTienConLai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTienConLai.Properties.Appearance.Options.UseFont = true;
            this.txtTienConLai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienConLai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienConLai.Properties.Mask.EditMask = "N0";
            this.txtTienConLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienConLai.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienConLai.Properties.ReadOnly = true;
            this.txtTienConLai.Size = new System.Drawing.Size(148, 26);
            this.txtTienConLai.TabIndex = 113;
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(247, 103);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(106, 35);
            this.btnXoa.TabIndex = 16;
            this.btnXoa.Text = "&Xóa dòng";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtNVnhap
            // 
            this.txtNVnhap.EnterMoveNextControl = true;
            this.txtNVnhap.Location = new System.Drawing.Point(737, 6);
            this.txtNVnhap.Name = "txtNVnhap";
            this.txtNVnhap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVnhap.Properties.Appearance.Options.UseFont = true;
            this.txtNVnhap.Properties.ReadOnly = true;
            this.txtNVnhap.Size = new System.Drawing.Size(230, 26);
            this.txtNVnhap.TabIndex = 5;
            // 
            // txtSophieu
            // 
            this.txtSophieu.EnterMoveNextControl = true;
            this.txtSophieu.Location = new System.Drawing.Point(71, 6);
            this.txtSophieu.Name = "txtSophieu";
            this.txtSophieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSophieu.Properties.Appearance.Options.UseFont = true;
            this.txtSophieu.Properties.ReadOnly = true;
            this.txtSophieu.Size = new System.Drawing.Size(230, 26);
            this.txtSophieu.TabIndex = 1;
            // 
            // btnLayphieutam
            // 
            this.btnLayphieutam.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayphieutam.Appearance.Options.UseFont = true;
            this.btnLayphieutam.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnLayphieutam.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLayphieutam.Location = new System.Drawing.Point(468, 103);
            this.btnLayphieutam.Margin = new System.Windows.Forms.Padding(4);
            this.btnLayphieutam.Name = "btnLayphieutam";
            this.btnLayphieutam.Size = new System.Drawing.Size(146, 35);
            this.btnLayphieutam.TabIndex = 18;
            this.btnLayphieutam.Text = "&Lấy phiếu tạm";
            this.btnLayphieutam.Click += new System.EventHandler(this.btnLayphieutam_Click);
            // 
            // btn_tam
            // 
            this.btn_tam.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tam.Appearance.Options.UseFont = true;
            this.btn_tam.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btn_tam.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_tam.Location = new System.Drawing.Point(361, 103);
            this.btn_tam.Margin = new System.Windows.Forms.Padding(4);
            this.btn_tam.Name = "btn_tam";
            this.btn_tam.Size = new System.Drawing.Size(99, 35);
            this.btn_tam.TabIndex = 17;
            this.btn_tam.Text = "&Lưu tạm";
            this.btn_tam.Click += new System.EventHandler(this.btn_luutam_Click);
            // 
            // dtpNgay
            // 
            this.dtpNgay.EditValue = null;
            this.dtpNgay.EnterMoveNextControl = true;
            this.dtpNgay.Location = new System.Drawing.Point(402, 6);
            this.dtpNgay.Name = "dtpNgay";
            this.dtpNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.Appearance.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgay.Size = new System.Drawing.Size(230, 26);
            this.dtpNgay.TabIndex = 3;
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(71, 70);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(896, 26);
            this.txtGHICHU.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 73);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Ghi chú";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(308, 9);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 19);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Ngày";
            // 
            // cboKho
            // 
            this.cboKho.EnterMoveNextControl = true;
            this.cboKho.Location = new System.Drawing.Point(71, 38);
            this.cboKho.Name = "cboKho";
            this.cboKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.Appearance.Options.UseFont = true;
            this.cboKho.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKho.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "Cửa hàng"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã kho", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên kho")});
            this.cboKho.Properties.DisplayMember = "TEN";
            this.cboKho.Properties.DropDownItemHeight = 40;
            this.cboKho.Properties.NullText = "";
            this.cboKho.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKho.Properties.ShowHeader = false;
            this.cboKho.Properties.ValueMember = "MA";
            this.cboKho.Size = new System.Drawing.Size(230, 26);
            this.cboKho.TabIndex = 7;
            // 
            // cboNCC
            // 
            this.cboNCC.EnterMoveNextControl = true;
            this.cboNCC.Location = new System.Drawing.Point(737, 38);
            this.cboNCC.Name = "cboNCC";
            this.cboNCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNCC.Properties.Appearance.Options.UseFont = true;
            this.cboNCC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNCC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNCC.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNCC.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã NCC", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên NCC")});
            this.cboNCC.Properties.DisplayMember = "TEN";
            this.cboNCC.Properties.DropDownItemHeight = 40;
            this.cboNCC.Properties.NullText = "";
            this.cboNCC.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNCC.Properties.ShowHeader = false;
            this.cboNCC.Properties.ValueMember = "MA";
            this.cboNCC.Size = new System.Drawing.Size(230, 26);
            this.cboNCC.TabIndex = 11;
            // 
            // cboPT
            // 
            this.cboPT.EnterMoveNextControl = true;
            this.cboPT.Location = new System.Drawing.Point(402, 38);
            this.cboPT.Name = "cboPT";
            this.cboPT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPT.Properties.Appearance.Options.UseFont = true;
            this.cboPT.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPT.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPT.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboPT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã PT", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên PT")});
            this.cboPT.Properties.DisplayMember = "TEN";
            this.cboPT.Properties.DropDownItemHeight = 40;
            this.cboPT.Properties.NullText = "";
            this.cboPT.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboPT.Properties.ShowHeader = false;
            this.cboPT.Properties.ValueMember = "MA";
            this.cboPT.Size = new System.Drawing.Size(230, 26);
            this.cboPT.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(639, 9);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Nhân viên";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(639, 41);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(96, 19);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Nhà cung cấp";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(308, 41);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 19);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Phương thức";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(71, 103);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 14;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(159, 103);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 15;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(6, 41);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(27, 19);
            this.lblGHICHU.TabIndex = 6;
            this.lblGHICHU.Text = "Kho";
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 9);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(62, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Số phiếu";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 145);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_soluong,
            this.txt_gianhap,
            this.txt_chietkhau,
            this.txt_vat,
            this.look_mavach2,
            this.look_ten2});
            this.gridControl1.Size = new System.Drawing.Size(1702, 588);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl1_ProcessGridKey);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 60;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_VACH,
            this.TEN,
            this.DVT,
            this.SL,
            this.GIANHAP,
            this.GIABAN1,
            this.THANHTIENTCK,
            this.CK,
            this.TIENCK,
            this.GIAVON,
            this.THANHTIEN,
            this.VAT,
            this.TIENVAT,
            this.TONGCONG,
            this.MA,
            this.FILL});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_VACH
            // 
            this.MA_VACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH.AppearanceCell.Options.UseFont = true;
            this.MA_VACH.AppearanceCell.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH.Caption = "Mã hàng";
            this.MA_VACH.ColumnEdit = this.look_mavach2;
            this.MA_VACH.FieldName = "MA_VACH";
            this.MA_VACH.Name = "MA_VACH";
            this.MA_VACH.OptionsColumn.FixedWidth = true;
            this.MA_VACH.Visible = true;
            this.MA_VACH.VisibleIndex = 1;
            this.MA_VACH.Width = 180;
            // 
            // look_mavach2
            // 
            this.look_mavach2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_mavach2.Appearance.Options.UseFont = true;
            this.look_mavach2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_mavach2.AppearanceDropDown.Options.UseFont = true;
            this.look_mavach2.AutoComplete = false;
            this.look_mavach2.AutoHeight = false;
            this.look_mavach2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_mavach2.DisplayMember = "MAVACH";
            this.look_mavach2.Name = "look_mavach2";
            this.look_mavach2.NullText = "";
            this.look_mavach2.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.look_mavach2.PopupFormSize = new System.Drawing.Size(180, 0);
            this.look_mavach2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_mavach2.ValueMember = "MAVACH";
            this.look_mavach2.View = this.repositoryItemGridLookUpEdit2View;
            this.look_mavach2.EditValueChanged += new System.EventHandler(this.look_mavach2_EditValueChanged);
            this.look_mavach2.Leave += new System.EventHandler(this.look_mavach2_Leave);
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_HANGHOA2,
            this.MA_VACH2});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // MA_HANGHOA2
            // 
            this.MA_HANGHOA2.Caption = "Mã hàng hóa";
            this.MA_HANGHOA2.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA2.Name = "MA_HANGHOA2";
            this.MA_HANGHOA2.Width = 192;
            // 
            // MA_VACH2
            // 
            this.MA_VACH2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH2.AppearanceCell.Options.UseFont = true;
            this.MA_VACH2.AppearanceCell.Options.UseTextOptions = true;
            this.MA_VACH2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH2.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH2.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH2.Caption = "Mã hàng";
            this.MA_VACH2.FieldName = "MAVACH";
            this.MA_VACH2.Name = "MA_VACH2";
            this.MA_VACH2.OptionsColumn.FixedWidth = true;
            this.MA_VACH2.Visible = true;
            this.MA_VACH2.VisibleIndex = 0;
            this.MA_VACH2.Width = 180;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN.Caption = "Hàng hóa";
            this.TEN.ColumnEdit = this.look_ten2;
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 200;
            // 
            // look_ten2
            // 
            this.look_ten2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_ten2.Appearance.Options.UseFont = true;
            this.look_ten2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_ten2.AppearanceDropDown.Options.UseFont = true;
            this.look_ten2.AutoComplete = false;
            this.look_ten2.AutoHeight = false;
            this.look_ten2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_ten2.DisplayMember = "TEN_HANGHOA";
            this.look_ten2.Name = "look_ten2";
            this.look_ten2.NullText = "";
            this.look_ten2.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.look_ten2.PopupFormSize = new System.Drawing.Size(200, 0);
            this.look_ten2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_ten2.ValueMember = "TEN_HANGHOA";
            this.look_ten2.View = this.repositoryItemGridLookUpEdit1View;
            this.look_ten2.EditValueChanged += new System.EventHandler(this.look_ten2_EditValueChanged);
            this.look_ten2.Leave += new System.EventHandler(this.look_ten2_Leave);
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_HANGHOA3,
            this.TEN_HANGHOA2});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // MA_HANGHOA3
            // 
            this.MA_HANGHOA3.Caption = "Mã hàng hóa";
            this.MA_HANGHOA3.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA3.Name = "MA_HANGHOA3";
            // 
            // TEN_HANGHOA2
            // 
            this.TEN_HANGHOA2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA2.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA2.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_HANGHOA2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA2.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA2.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA2.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA2.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA2.Name = "TEN_HANGHOA2";
            this.TEN_HANGHOA2.Visible = true;
            this.TEN_HANGHOA2.VisibleIndex = 0;
            this.TEN_HANGHOA2.Width = 200;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 3;
            this.DVT.Width = 90;
            // 
            // SL
            // 
            this.SL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SL.AppearanceCell.Options.UseFont = true;
            this.SL.AppearanceCell.Options.UseTextOptions = true;
            this.SL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SL.Caption = "SL";
            this.SL.ColumnEdit = this.txt_soluong;
            this.SL.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.SL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SL.OptionsColumn.FixedWidth = true;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 5;
            this.SL.Width = 76;
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            this.txt_soluong.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_soluong_EditValueChanging);
            this.txt_soluong.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_soluong_KeyPress);
            // 
            // GIANHAP
            // 
            this.GIANHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIANHAP.AppearanceCell.Options.UseFont = true;
            this.GIANHAP.AppearanceCell.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIANHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIANHAP.AppearanceHeader.Options.UseFont = true;
            this.GIANHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIANHAP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIANHAP.Caption = "Giá nhập";
            this.GIANHAP.ColumnEdit = this.txt_gianhap;
            this.GIANHAP.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.GIANHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIANHAP.FieldName = "GIANHAP";
            this.GIANHAP.Name = "GIANHAP";
            this.GIANHAP.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIANHAP.OptionsColumn.FixedWidth = true;
            this.GIANHAP.Visible = true;
            this.GIANHAP.VisibleIndex = 6;
            this.GIANHAP.Width = 108;
            // 
            // txt_gianhap
            // 
            this.txt_gianhap.AutoHeight = false;
            this.txt_gianhap.Name = "txt_gianhap";
            this.txt_gianhap.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_gianhap_EditValueChanging);
            this.txt_gianhap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_soluong_KeyPress);
            // 
            // GIABAN1
            // 
            this.GIABAN1.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIABAN1.AppearanceHeader.Options.UseFont = true;
            this.GIABAN1.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN1.Caption = "Giá bán";
            this.GIABAN1.DisplayFormat.FormatString = "N0";
            this.GIABAN1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIABAN1.FieldName = "GIABAN1";
            this.GIABAN1.Name = "GIABAN1";
            this.GIABAN1.OptionsColumn.AllowEdit = false;
            this.GIABAN1.OptionsColumn.AllowFocus = false;
            this.GIABAN1.OptionsColumn.FixedWidth = true;
            this.GIABAN1.Visible = true;
            this.GIABAN1.VisibleIndex = 4;
            this.GIABAN1.Width = 110;
            // 
            // THANHTIENTCK
            // 
            this.THANHTIENTCK.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIENTCK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIENTCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIENTCK.AppearanceHeader.Options.UseFont = true;
            this.THANHTIENTCK.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTIENTCK.Caption = "Thành tiền TCK";
            this.THANHTIENTCK.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.THANHTIENTCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIENTCK.FieldName = "THANHTIENTCK";
            this.THANHTIENTCK.Name = "THANHTIENTCK";
            this.THANHTIENTCK.OptionsColumn.AllowEdit = false;
            this.THANHTIENTCK.OptionsColumn.AllowFocus = false;
            this.THANHTIENTCK.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTIENTCK.OptionsColumn.FixedWidth = true;
            this.THANHTIENTCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIENTCK", "{0:#,###0.000}")});
            this.THANHTIENTCK.Visible = true;
            this.THANHTIENTCK.VisibleIndex = 7;
            this.THANHTIENTCK.Width = 139;
            // 
            // CK
            // 
            this.CK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CK.AppearanceCell.Options.UseFont = true;
            this.CK.AppearanceCell.Options.UseTextOptions = true;
            this.CK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CK.AppearanceHeader.Options.UseFont = true;
            this.CK.AppearanceHeader.Options.UseTextOptions = true;
            this.CK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CK.Caption = "CK";
            this.CK.ColumnEdit = this.txt_chietkhau;
            this.CK.DisplayFormat.FormatString = "{0:#,###0.#}%";
            this.CK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CK.FieldName = "CK";
            this.CK.Name = "CK";
            this.CK.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CK.OptionsColumn.FixedWidth = true;
            this.CK.Visible = true;
            this.CK.VisibleIndex = 8;
            this.CK.Width = 55;
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.AutoHeight = false;
            this.txt_chietkhau.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_chietkhau.Name = "txt_chietkhau";
            this.txt_chietkhau.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_chietkhau_EditValueChanging);
            this.txt_chietkhau.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_chietkhau_KeyPress);
            // 
            // TIENCK
            // 
            this.TIENCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENCK.AppearanceCell.Options.UseFont = true;
            this.TIENCK.AppearanceCell.Options.UseTextOptions = true;
            this.TIENCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENCK.AppearanceHeader.Options.UseFont = true;
            this.TIENCK.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENCK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TIENCK.Caption = "Tiền CK";
            this.TIENCK.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.TIENCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENCK.FieldName = "TIENCK";
            this.TIENCK.Name = "TIENCK";
            this.TIENCK.OptionsColumn.AllowEdit = false;
            this.TIENCK.OptionsColumn.AllowFocus = false;
            this.TIENCK.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TIENCK.OptionsColumn.FixedWidth = true;
            this.TIENCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENCK", "{0:#,###0.000}")});
            this.TIENCK.Visible = true;
            this.TIENCK.VisibleIndex = 9;
            this.TIENCK.Width = 120;
            // 
            // GIAVON
            // 
            this.GIAVON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIAVON.AppearanceCell.Options.UseFont = true;
            this.GIAVON.AppearanceCell.Options.UseTextOptions = true;
            this.GIAVON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIAVON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIAVON.AppearanceHeader.Options.UseFont = true;
            this.GIAVON.AppearanceHeader.Options.UseTextOptions = true;
            this.GIAVON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIAVON.Caption = "Giá Vốn";
            this.GIAVON.FieldName = "GIAVON";
            this.GIAVON.Name = "GIAVON";
            this.GIAVON.OptionsColumn.AllowEdit = false;
            this.GIAVON.OptionsColumn.AllowFocus = false;
            this.GIAVON.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIAVON.OptionsColumn.FixedWidth = true;
            this.GIAVON.Width = 100;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTIEN.Caption = "Thành tiền SCK";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0.000}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 10;
            this.THANHTIEN.Width = 150;
            // 
            // VAT
            // 
            this.VAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VAT.AppearanceCell.Options.UseFont = true;
            this.VAT.AppearanceCell.Options.UseTextOptions = true;
            this.VAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.VAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VAT.AppearanceHeader.Options.UseFont = true;
            this.VAT.AppearanceHeader.Options.UseTextOptions = true;
            this.VAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VAT.Caption = "VAT";
            this.VAT.ColumnEdit = this.txt_vat;
            this.VAT.DisplayFormat.FormatString = "{0:#,###0}%";
            this.VAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.VAT.FieldName = "VAT";
            this.VAT.Name = "VAT";
            this.VAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.VAT.OptionsColumn.FixedWidth = true;
            this.VAT.Visible = true;
            this.VAT.VisibleIndex = 11;
            this.VAT.Width = 50;
            // 
            // txt_vat
            // 
            this.txt_vat.AutoHeight = false;
            this.txt_vat.Name = "txt_vat";
            this.txt_vat.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_vat_EditValueChanging);
            this.txt_vat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_chietkhau_KeyPress);
            // 
            // TIENVAT
            // 
            this.TIENVAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENVAT.AppearanceCell.Options.UseFont = true;
            this.TIENVAT.AppearanceCell.Options.UseTextOptions = true;
            this.TIENVAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENVAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENVAT.AppearanceHeader.Options.UseFont = true;
            this.TIENVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TIENVAT.Caption = "Tiền VAT";
            this.TIENVAT.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.TIENVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENVAT.FieldName = "TIENVAT";
            this.TIENVAT.Name = "TIENVAT";
            this.TIENVAT.OptionsColumn.AllowEdit = false;
            this.TIENVAT.OptionsColumn.AllowFocus = false;
            this.TIENVAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TIENVAT.OptionsColumn.FixedWidth = true;
            this.TIENVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENVAT", "{0:#,###0.000}")});
            this.TIENVAT.Visible = true;
            this.TIENVAT.VisibleIndex = 12;
            this.TIENVAT.Width = 120;
            // 
            // TONGCONG
            // 
            this.TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceCell.Options.UseFont = true;
            this.TONGCONG.AppearanceCell.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGCONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TONGCONG.Caption = "Tổng cộng";
            this.TONGCONG.DisplayFormat.FormatString = "{0:#,###0.000}";
            this.TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGCONG.FieldName = "TONGCONG";
            this.TONGCONG.Name = "TONGCONG";
            this.TONGCONG.OptionsColumn.AllowEdit = false;
            this.TONGCONG.OptionsColumn.AllowFocus = false;
            this.TONGCONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TONGCONG.OptionsColumn.FixedWidth = true;
            this.TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0.000}")});
            this.TONGCONG.Visible = true;
            this.TONGCONG.VisibleIndex = 13;
            this.TONGCONG.Width = 150;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceCell.Options.UseFont = true;
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Width = 50;
            // 
            // FILL
            // 
            this.FILL.MinWidth = 10;
            this.FILL.Name = "FILL";
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 14;
            this.FILL.Width = 102;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnFile_Mo);
            this.panelControl2.Controls.Add(this.btnFile);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1702, 145);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 588);
            this.panelControl2.TabIndex = 4;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(2, 307);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 96);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 403);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 61);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 464);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 61);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 246);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 61);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 185);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 61);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 525);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 61);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnFile_Mo
            // 
            this.btnFile_Mo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile_Mo.Appearance.Options.UseFont = true;
            this.btnFile_Mo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFile_Mo.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnFile_Mo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFile_Mo.Location = new System.Drawing.Point(2, 124);
            this.btnFile_Mo.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile_Mo.Name = "btnFile_Mo";
            this.btnFile_Mo.Size = new System.Drawing.Size(35, 61);
            this.btnFile_Mo.TabIndex = 99;
            this.btnFile_Mo.Click += new System.EventHandler(this.btnFile_Mo_Click);
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFile.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(2, 63);
            this.btnFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(35, 61);
            this.btnFile.TabIndex = 98;
            this.btnFile.Text = "&Import";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 61);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_Nhapkho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1741, 733);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Nhapkho";
            this.Text = "Nhập kho";
            this.Load += new System.EventHandler(this.Frm_Nhapkho_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Nhapkho_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCONGNO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCongNo)).EndInit();
            this.pnlCongNo.ResumeLayout(false);
            this.pnlCongNo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDaTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienConLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVnhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_mavach2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboKho;
        private DevExpress.XtraEditors.LookUpEdit cboNCC;
        private DevExpress.XtraEditors.LookUpEdit cboPT;
        private DevExpress.XtraEditors.SimpleButton btnLayphieutam;
        private DevExpress.XtraEditors.SimpleButton btn_tam;
        private DevExpress.XtraEditors.DateEdit dtpNgay;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraGrid.Columns.GridColumn GIANHAP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_gianhap;
        private DevExpress.XtraGrid.Columns.GridColumn CK;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraGrid.Columns.GridColumn TIENCK;
        private DevExpress.XtraGrid.Columns.GridColumn GIAVON;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn VAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_vat;
        private DevExpress.XtraGrid.Columns.GridColumn TIENVAT;
        private DevExpress.XtraGrid.Columns.GridColumn TONGCONG;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.TextEdit txtNVnhap;
        private DevExpress.XtraEditors.TextEdit txtSophieu;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIENTCK;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.PanelControl pnlCongNo;
        private DevExpress.XtraEditors.DateEdit dtpNgayHenTra;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTienDaTra;
        private DevExpress.XtraEditors.TextEdit txtTienConLai;
        private DevExpress.XtraEditors.CheckEdit chkCONGNO;
        private DevExpress.XtraEditors.SimpleButton btnLayPhieuNhap;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit look_mavach2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA2;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit look_ten2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA3;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA2;
        private DevExpress.XtraEditors.SimpleButton btnLayPhieuXuat;
        private DevExpress.XtraEditors.SimpleButton btnFile_Mo;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN1;




    }
}