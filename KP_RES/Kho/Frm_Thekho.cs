﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_Thekho : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        public Frm_Thekho()
        {
            InitializeComponent();
            SLTONDAUKY.Caption = "Số lượng \ntồn đầu kỳ";
            TRIGIATONDAUKY.Caption = "Trị giá \ntồn đầu kỳ";
            SLNHAP.Caption = "Số lượng \nnhập";
            TRIGIANHAP.Caption = "Trị giá \nnhập";
            SLXUAT.Caption = "Số lượng \nxuất";
            TRIGIAXUAT.Caption = "Trị giá \nxuất";
            SLTONCUOIKY.Caption = "Số lượng \ntồn cuối kỳ";
            TRIGIATONCUOIKY.Caption = "Trị giá \ntồn cuối kỳ";
            LoadCombo();
            Click = "1";
        }

        private void Frm_Thekho_Load(object sender, EventArgs e)
        {
            btnPhieuNhap.Width = panelControl9.Width / 3;
            btnHoadon.Width = panelControl9.Width / 3;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void LoadCombo()
        {
            try
            {
                DataTable dt = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN" );
                cboCuahang.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboCuahang.EditValue =int.Parse (dt.Rows[0]["MA"].ToString());
                }
                dt = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where MA_CUAHANG='" + cboCuahang.EditValue.ToString() + "' Order by TEN");
                cboKho.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboKho.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());
                }
                dt = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1 Order by TEN");
                cboNhom.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                if (dt.Rows.Count > 0)
                {
                    cboNhom.EditValue = 0;
                }
                dt = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA Order by TEN");
                cboHH.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    cboHH.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());
                }
                dtpTungay.EditValue = clsGlobal.gdServerDate;
                dtpDenngay.EditValue = clsGlobal.gdServerDate;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cboCuahang_EditValueChanged(object sender, EventArgs e)
        {
            DataTable dt = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where MA_CUAHANG='" + cboCuahang.EditValue.ToString() + "' Order by TEN");
            cboKho.Properties.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                cboKho.EditValue =int.Parse(dt.Rows[0]["MA"].ToString());
            }
        }

        private void cboNhom_EditValueChanged(object sender, EventArgs e)
        {
            string sSQL = "";
            if(cboNhom.Text=="Tất cả")
                sSQL="select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA Order by TEN";
            else
                sSQL = "select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where MA_NHOMHANG='" + cboNhom.EditValue.ToString() + "' Order by TEN";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboHH.Properties.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                cboHH.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try 
            {
                if (cboKho.EditValue == null)
                {
                    XtraMessageBox.Show("Chưa chọn kho","Thông báo");
                    return;
                }
                string _tungaydenngay = "NGAYTAO between '" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue) + "' and '"
                                     + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " " + DateTime.Now.ToString("HH:mm:ss") + "'";
                gridControl2.DataSource = null;
                string nhom=string.Empty;
                string hh = string.Empty;
                if (cboNhom.EditValue == null)
                    nhom = "";
                else
                    nhom = cboNhom.EditValue.ToString();
                if (cboHH.EditValue == null)
                    hh = "";
                else
                    hh = cboHH.EditValue.ToString();
                string sSQL = "EXEC SP_KHO_TONKHO_NEW @sCuaHang = '" + cboCuahang.EditValue.ToString() + "', @sKho = '" + cboKho.EditValue.ToString()
                    + "',@sTuNgay ='" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue) + " 00:00:00"
                    + "',@sDenNgay = '" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59"
                    + "',@sNhom = '" + nhom + "',@sHangHoa = '" + hh + "'";
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl2.DataSource = dt;

                sSQL = string.Format("EXEC Select_Thekho_phieunhap @MAHANG={0},@TUNGAY='{1}',@DENNGAY='{2}',@MA_KHO='{3}'",
                       cboHH.EditValue.ToString(), string.Format("{0:yyyyMMdd}", dtpTungay.EditValue), string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue), cboKho.EditValue.ToString());
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;

                sSQL = string.Format("EXEC Select_Thekho_phieuxuat @MAHANG={0},@TUNGAY='{1}',@DENNGAY='{2}',@MA_KHO='{3}'",
                      cboHH.EditValue.ToString(), string.Format("{0:yyyyMMdd}", dtpTungay.EditValue), string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue), cboKho.EditValue.ToString());
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl3.DataSource = dt;

                sSQL = string.Format("EXEC Select_Thekho_hoadon @MAHANG={0},@TUNGAY='{1}',@DENNGAY='{2}',@MA_KHO='{3}'",
                      cboHH.EditValue.ToString(), string.Format("{0:yyyyMMdd}", dtpTungay.EditValue), string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue),cboKho.EditValue.ToString());
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl4.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo");
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            //if (this.gridView2.RowCount == 0)
            //{
            //    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //else
            //{
            //    STT.Visible = false;
            //    using (SaveFileDialog saveDialog = new SaveFileDialog())
            //    {
            //        saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
            //        if (saveDialog.ShowDialog() != DialogResult.Cancel)
            //        {
            //            string exportFilePath = saveDialog.FileName;
            //            string fileExtenstion = new FileInfo(exportFilePath).Extension;
            //            switch (fileExtenstion)
            //            {
            //                case ".xls":
            //                    gridControl2.ExportToXls(exportFilePath);
            //                    break;
            //                case ".xlsx":
            //                    gridControl2.ExportToXlsx(exportFilePath);
            //                    break;
            //                default:
            //                    break;
            //            }
            //        }
            //    }
            //    STT.Visible = true;
            //}
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (Click == "1")
                {
                    if (gridView1.FocusedRowHandle >= 0)
                    {
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.ma = sMa;
                        frm.Mode = 1;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                        throw new Exception("Chưa chọn phiếu cần in");
                }
                else if (Click == "2")
                {
                    if (gridView3.FocusedRowHandle >= 0)
                    {
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.ma = sMa;
                        frm.Mode = 0;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                        throw new Exception("Chưa chọn phiếu cần in");
                }
                else if (Click == "3")
                {
                    /*
                    if (gridView4.FocusedRowHandle >= 0)
                    {
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.ma = sMa;
                        frm.Mode = 1;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                        throw new Exception("Chưa chọn phiếu cần in");
                     * */
                }
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


            //if (gridView2.RowCount > 0)
            //{
            //    DataColumn colTUNGAY = new DataColumn();
            //    colTUNGAY.ColumnName = "TUNGAY";
            //    colTUNGAY.DataType = System.Type.GetType("System.String");
            //    colTUNGAY.DefaultValue = dtpTungay.Text ;
            //    DataColumn colDENNGAY = new DataColumn();
            //    colDENNGAY.ColumnName = "DENNGAY";
            //    colDENNGAY.DataType = System.Type.GetType("System.String");
            //    colDENNGAY.DefaultValue = dtpDenngay.Text;
            //    DataColumn colCUAHANG = new DataColumn();
            //    colCUAHANG.ColumnName = "CUAHANG";
            //    colCUAHANG.DataType = System.Type.GetType("System.String");
            //    colCUAHANG.DefaultValue = cboCuahang.Text;
            //    DataColumn colKHO = new DataColumn();
            //    colKHO.ColumnName = "KHO";
            //    colKHO.DataType = System.Type.GetType("System.String");
            //    colKHO.DefaultValue = cboKho.Text;
            //    DataColumn colNHOM = new DataColumn();
            //    colNHOM.ColumnName = "NHOM";
            //    colNHOM.DataType = System.Type.GetType("System.String");
            //    colNHOM.DefaultValue = cboNhom.Text;
            //    DataColumn colHANGHOA = new DataColumn();
            //    colHANGHOA.ColumnName = "HANGHOA";
            //    colHANGHOA.DataType = System.Type.GetType("System.String");
            //    colHANGHOA.DefaultValue = cboHH.Text;

            //    if (ContainColumn("CUAHANG", dt) == false)
            //        dt.Columns.Add(colCUAHANG);
            //    if (ContainColumn("KHO", dt) == false)
            //        dt.Columns.Add(colKHO);
            //    if (ContainColumn("NHOM", dt) == false)
            //        dt.Columns.Add(colNHOM);
            //    if (ContainColumn("TUNGAY", dt) == false)
            //        dt.Columns.Add(colTUNGAY);
            //    if (ContainColumn("DENNGAY", dt) == false)
            //        dt.Columns.Add(colDENNGAY);
            //    if (ContainColumn("HANGHOA", dt) == false)
            //        dt.Columns.Add(colHANGHOA);

            //    Frm_BCInPhieu frm = new Frm_BCInPhieu();
            //    frm.WindowState = FormWindowState.Maximized;
            //    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //    frm.dtSource = dt;
            //    frm.Mode = 2;
            //    frm.ShowDialog();
            //    frm.Dispose();
            //}
            //else
            //{
            //    XtraMessageBox.Show("Không có dữ liệu","Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl5.Visible = false;

                btnPhieuNhap.Width = panelControl9.Width / 3;
                btnHoadon.Width = panelControl9.Width / 3;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl5.Visible = true;

                btnPhieuNhap.Width = panelControl9.Width / 3;
                btnHoadon.Width = panelControl9.Width / 3;
            }
            this.Refresh();
        }

        String Click = "";
        private void btnPhieuNhap_Click(object sender, EventArgs e)
        {
            TabOption.SelectedTabPage = Tab_Phieunhap;
            btnPhieuNhap.ForeColor = Color.Red;
            btnPhieuxuat.ForeColor = btnCapnhat.ForeColor;
            btnHoadon.ForeColor = btnCapnhat.ForeColor;
            Click = "1";
        }

        private void btnPhieuxuat_Click(object sender, EventArgs e)
        {
            TabOption.SelectedTabPage = Tab_Phieuxuat;
            btnPhieuxuat.ForeColor = Color.Red;
            btnPhieuNhap.ForeColor = btnCapnhat.ForeColor;
            btnHoadon.ForeColor = btnCapnhat.ForeColor;
            Click = "2";
        }

        private void btnHoadon_Click(object sender, EventArgs e)
        {
            TabOption.SelectedTabPage = Tab_Hoadonbanhang;
            btnHoadon.ForeColor = Color.Red;
            btnPhieuNhap.ForeColor = btnCapnhat.ForeColor;
            btnPhieuxuat.ForeColor = btnCapnhat.ForeColor;
            Click = "3";
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTPN & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTPX & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void gridView4_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHD & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        String sMa = "";
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            sMa = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
        }

        private void gridView3_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView3.RowCount == 0 || gridView3.FocusedRowHandle < 0)
                return;

            sMa = gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "MA").ToString();
        }

        private void gridView4_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView4.RowCount == 0 || gridView4.FocusedRowHandle < 0)
                return;

            sMa = gridView4.GetRowCellValue(gridView4.FocusedRowHandle, "MA_HOADON").ToString();
        }
    }
}