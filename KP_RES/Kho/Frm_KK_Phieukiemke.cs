﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Base;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Phieukiemke : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource bs = new BindingSource();
        public Frm_Phieukiemke()
        {
            InitializeComponent();
            this.Text = rm.GetString("kiemke", culture);
            LoadPermission();
            SetDulieuKhoiTao();
            dodulieunguon();
            KhoaMoControl(false);
        }
        private void dodulieunguon()
        {
            string sSQL = "";
            dtpNgayKK.EditValue = clsGlobal.gdServerDate;

            sSQL += "Select MANHANVIEN as MA,TENNHANVIEN as TEN" + "\n";
            sSQL += "From DM_NHANVIEN" + "\n";
            sSQL += "Where SUDUNG=1";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                cboNhanvien.Properties.DataSource = dt;
                cboNhanvien.EditValue = clsGlobal.gsMaNVOfUserLogin;
            }

            sSQL = "";
            sSQL += "Select MA_HANGHOA,TEN_HANGHOA,THUE,GIANHAP,TEN_DONVITINH" + "\n";
            sSQL += "From HANGHOA h,DONVITINH d" + "\n";
            sSQL += "Where TONKHO=1 AND h.MA_DONVITINH=d.MA_DONVITINH order by TEN_HANGHOA";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                look_Ten.DataSource = dt;
                string[] displayname = new string[] { "Mã hàng", "Tên hàng", "THUE", "GIANHAP", "TEN_DONVITINH" };
                string[] hidename = new string[] { "MA_HANGHOA", "TEN_HANGHOA", "THUE", "GIANHAP", "TEN_DONVITINH" };
                int[] width = new int[] { 100, 300, 20, 20, 20 };
                bool[] visible = new bool[] { true, true, false, false, false };
                for (int i = 0; i < displayname.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = hidename[i];
                    info.Caption = displayname[i];
                    info.Visible = visible[i];
                    info.Width = width[i];
                    look_Ten.Columns.Add(info);
                }
            }

            sSQL  = "";
            sSQL += "Select TEN_CUAHANG,MA_KHO,TEN_KHO" + "\n";
            sSQL += "From KHO k,CUAHANG c" + "\n";
            sSQL += "Where k.MA_CUAHANG=c.MA_CUAHANG AND K.SUDUNG=1 Order by TEN_KHO";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                look_Kho.DataSource = dt;
                string[] displayname = new string[] { "Cửa hàng", "Mã", "Tên"};
                string[] hidename = new string[] { "TEN_CUAHANG", "MA_KHO", "TEN_KHO" };
                int[] width = new int[] { 800, 20, 600};
                bool[] visible = new bool[] { true, false,true};
                for (int i = 0; i < displayname.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = hidename[i];
                    info.Caption = displayname[i];
                    info.Visible = visible[i];
                    info.Width = width[i];
                    look_Kho.Columns.Add(info);
                }
            }
        }
        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            dtpNgayKK.EditValue = clsGlobal.gdServerDate;

            sSQL += "Select MANHANVIEN as MA,TENNHANVIEN as TEN" + "\n";
            sSQL += "From DM_NHANVIEN" + "\n";
            sSQL += "Where SUDUNG=1";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                cboNhanvien.Properties.DataSource = dt;
                cboNhanvien.EditValue = clsGlobal.gsMaNVOfUserLogin;
            }

            sSQL  = "";
            sSQL += "Select MA_HANGHOA,TEN_HANGHOA,THUE,GIANHAP,TEN_DONVITINH" + "\n";
            sSQL += "From HANGHOA h,DONVITINH d" + "\n";
            sSQL += "Where TONKHO=1 AND h.MA_DONVITINH=d.MA_DONVITINH order by TEN_HANGHOA";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                look_Ten.DataSource = dt;
            }

            sSQL = "";
            sSQL += "Select TEN_CUAHANG,MA_KHO,TEN_KHO" + "\n";
            sSQL += "From KHO k,CUAHANG c" + "\n";
            sSQL += "Where k.MA_CUAHANG=c.MA_CUAHANG AND K.SUDUNG=1 Order by TEN_KHO";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                look_Kho.DataSource = dt;
            }

        }
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }
        private void bindingsoucre()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SOLUONG", typeof(float));
            dt.Columns.Add("MA_HANGHOA", typeof(int));
            dt.Columns.Add("TEN_HANGHOA", typeof(String));
            dt.Columns.Add("DVT", typeof(String));
            dt.Columns.Add("MA_KHO", typeof(int));
            dt.Columns.Add("TEN_KHO", typeof(String));
            bs.DataSource = dt;
            gridControl1.DataSource = bs;
        }
       

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void txtSoluong_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar) & e.KeyChar != '-')
                        e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(true);
            SetDulieuKhoiTao();
            txtSophieu.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKIEMKE]  (1)").Rows[0][0].ToString();
            txtSophieu.Focus();
        }
        private void KhoaMoControl(bool flag)
        {
            txtGHICHU.Properties.ReadOnly = !flag;
            gridView1.OptionsBehavior.Editable = flag;
            txtSophieu.Properties.ReadOnly = true;
            cboNhanvien.Properties.ReadOnly = !flag;
        }
        private void SetDulieuKhoiTao()
        {
            dtpNgayKK.EditValue = clsGlobal.gdServerDate;
            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("MA_KHO", typeof(int));
            DTSOUCRE.Columns.Add("TEN_KHO", typeof(string));
            DTSOUCRE.Columns.Add("MA_HANGHOA", typeof(int));
            DTSOUCRE.Columns.Add("TEN_HANGHOA", typeof(string));
            DTSOUCRE.Columns.Add("DVT", typeof(string));
            DTSOUCRE.Columns.Add("SOLUONG", typeof(float));
            DTSOUCRE.Rows.Add(null, null, null, null, null, null);
            bs.DataSource = DTSOUCRE;
            gridControl1.DataSource = bs;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("12");
            btnLuu.Enabled = btnThem.Enabled;
        }

        private void SetRongControl()
        {
            txtGHICHU.Text = "";
        }
        private void checkinputbeforeLuu()
        {
            if (txtSophieu.Text == "")
                throw new Exception("Chưa nhập đủ thông tin");
            if (gridView1.RowCount == 1 && gridView1.GetFocusedRowCellValue(TEN_HANGHOA).ToString() == "")
                throw new Exception("Chưa có mặt hàng nào ");

            bool flag = false;
            DataTable dt = (DataTable)bs.DataSource;
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (flag)
                    break;
                int dem = 0;
                foreach (DataRow tam in dt.Rows)
                {
                    if (gridView1.GetRowCellValue(i, MA_KHO).ToString() == tam["MA_KHO"].ToString() && gridView1.GetRowCellValue(i, MA_HANGHOA).ToString() == tam["MA_HANGHOA"].ToString())
                        dem += 1;
                    if (dem > 1)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag)
                throw new Exception("Có hàng hóa giống nhau trong cùng một kho");
            
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                checkinputbeforeLuu();
                string sSQL = "";
                DataTable dt = (DataTable)bs.DataSource;
                foreach (DataRow tam in dt.Rows)
                {
                    if (tam["MA_HANGHOA"].ToString() != "")
                    {
                        if (tam["MA_KHO"].ToString() == "")
                        {
                            throw new Exception("Chưa chọn kho cho hàng hóa");
                        }
                        sSQL += "INSERT INTO KHO_KIEMKE (MAKK,MA_HANGHOA,SOLUONG,MA_KHO,NGAY_KK,NGAYTAO,DACANDOI,MANHANVIEN,NGUOITAO,GHICHU)" + "\n";
                        sSQL += "Values (";
                        sSQL += clsMain.SQLString(txtSophieu.Text) + ",";
                        sSQL += clsMain.SQLString(tam["MA_HANGHOA"].ToString()) + ",";
                        sSQL += clsMain.SQLString(tam["SOLUONG"].ToString()) + ",";
                        sSQL += clsMain.SQLString(tam["MA_KHO"].ToString()) + ","; 
                        sSQL += clsMain.SQLString(string.Format("{0:yyyyMMdd}", dtpNgayKK.EditValue)) + ",";
                        sSQL += "GETDATE()" + ",";
                        sSQL += "0" + ",";
                        sSQL += clsMain.SQLString(cboNhanvien.EditValue.ToString()) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text.Trim()) + ")" + "\n";
                    }
                }
                if (DialogResult.Yes == (XtraMessageBox.Show("Bạn có chắc muốn tạo phiếu kiểm kê này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    if (clsMain.ExecuteSQL(sSQL))
                    {
                        string MAKK = txtSophieu.Text;
                        btnThem_Click(sender, e);
                        txtSophieu.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKIEMKE]  (1)").Rows[0][0].ToString();
                        XtraMessageBox.Show("Thêm phiếu kiểm kê thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        
                        sSQL  = "";
                        sSQL += "SELECT MAKK as SOPHIEU,NGAYTAO,PK.MA_KHO,TEN_KHO,PK.MA_HANGHOA," + "\n";
                        sSQL += "TENNHANVIEN,TEN_HANGHOA,TEN_DONVITINH,PK.SOLUONG,PK.GHICHU" + "\n";
                        sSQL += "FROM KHO_KIEMKE PK,KHO K,HANGHOA H,DONVITINH D,DM_NHANVIEN NV" + "\n";
                        sSQL += "WHERE PK.MA_KHO=K.MA_KHO AND PK.MA_HANGHOA=H.MA_HANGHOA AND NV.MANHANVIEN=PK.MANHANVIEN" + "\n";
                        sSQL += "AND H.MA_DONVITINH=D.MA_DONVITINH AND MAKK=" + clsMain.SQLString(MAKK);
                        frm.dtSource = clsMain.ReturnDataTable(sSQL);
                        frm.Mode = 10;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                        XtraMessageBox.Show("Thêm phiếu kiểm kê không thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnChenhLechKiemKe_Click(object sender, EventArgs e)
        {
            Nghiepvu.Frm_KK_ChenhlechKK frm = new Nghiepvu.Frm_KK_ChenhlechKK();
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                if (gridView1.GetFocusedRowCellValue(MA_HANGHOA).ToString() != string.Empty)
                    gridView1.DeleteSelectedRows();
            }
        }

        private void look_Ten_EditValueChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            
            DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
            if (editor.GetColumnValue("MA_HANGHOA") == null)
                return;
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, MA_HANGHOA, editor.GetColumnValue("MA_HANGHOA").ToString());
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, TEN_HANGHOA, editor.GetColumnValue("TEN_HANGHOA").ToString());
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, DVT, editor.GetColumnValue("TEN_DONVITINH").ToString());
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SOLUONG", 1);

            this.Cursor = Cursors.Default;
        }

        private void look_Kho_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_KHO", editor.GetColumnValue("MA_KHO").ToString());
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_KHO", editor.GetColumnValue("TEN_KHO").ToString());
        }

        private void look_Ten_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_Phieukiemke.bs.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA_HANGHOA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA_HANGHOA"].ToString() == "")
                {
                }
                else
                    dttmp.Rows.Add(null, null, null, null, null, null);
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            bindingsoucre();
            OpenFileDialog OPEN = new OpenFileDialog();
            OPEN.Filter = "Excel |*.xls;*.xlsx|Text Document (*.txt)|*.txt";
            OPEN.RestoreDirectory = false;
            try
            {
                if (OPEN.ShowDialog() == DialogResult.OK)
                {
                    KhoaMoControl(true);
                    txtSophieu.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKIEMKE]  (1)").Rows[0][0].ToString();

                    FileInfo info = new FileInfo(OPEN.FileName);
                    string Exten = info.Extension;
                    #region file txt
                    //
                    //Tùy chỉnh theo máy kiểm tồn kho
                    //
                    if (Exten == ".txt" || Exten == ".TXT")
                    {

                        string line = "";
                        string filepath = OPEN.FileName;
                        FileStream fs = new FileStream(filepath, FileMode.Open);
                        StreamReader rd = new StreamReader(fs, Encoding.ASCII);
                        DataTable dtbs = (DataTable)bs.DataSource;
                        while ((line = rd.ReadLine()) != null)
                        {

                            string[] arr;
                            arr = line.Split(',');
                            int n = arr.Length;
                            string sqltest = "select h.MA_HANGHOA,h.TEN_HANGHOA,h.TEN_DONVITINH as DVT,k.MA_KHO, k.TEN_KHO from HANGHOA h,KHO k,DONVITINH d ";
                            sqltest += "where d.MA_DONVITINH=h.MA_DONVITINH AND h.MA_HANGHOA='" + arr[0] + "'";
                            DataTable dtcheck = clsMain.ReturnDataTable(sqltest);
                            if (dtcheck.Rows.Count > 0)
                            {
                                DataRow drbs = dtbs.NewRow();
                                try
                                {
                                    if (arr[1] != "")
                                    {
                                        drbs["SOLUONG"] = float.Parse(arr[1]);
                                    }
                                }
                                catch
                                {
                                    throw new Exception("Lỗi Convert Số Lượng Kiểm Kê");
                                }
                                drbs["MA_HANGHOA"] = dtcheck.Rows[0]["MA_HANGHOA"].ToString();
                                drbs["TEN_HANGHOA"] = dtcheck.Rows[0]["TEN_HANGHOA"].ToString();
                                drbs["DVT"] = dtcheck.Rows[0]["DVT"].ToString();
                                drbs["MA_KHO"] = dtcheck.Rows[0]["MA_KHO"].ToString();
                                drbs["TEN_KHO"] = dtcheck.Rows[0]["TEN_KHO"].ToString();
                                dtbs.Rows.Add(drbs);
                            }
                            dtbs.Rows.Add(null, null, null, null, null, null);
                        }
                    }
                    #endregion
                    #region file Excel
                    else
                    {
                        this.Cursor = Cursors.WaitCursor;
                        string sql = "Select [NGAYKIEMKE],[MA_KHO],[MA_HANGHOA],[TENHANGHOA],[SOLUONG] From [Sheet1$]";
                        Class.cls_Import im = new Class.cls_Import();
                        DataTable dt = im.loaddulieu(OPEN.FileName, sql);
                        if (dt.Rows.Count >= 0)
                        {
                            DataTable dtbs = (DataTable)bs.DataSource;
                            foreach (DataRow dr in dt.Rows)
                            {
                                string sqltest = @"select h.MA_HANGHOA, h.TEN_HANGHOA,TEN_DONVITINH as DVT, k.MA_KHO, k.TEN_KHO from HANGHOA h,KHO k,DONVITINH d where 
                                                   h.MA_HANGHOA='" + dr["MA_HANGHOA"].ToString() + "' and k.MA_KHO='" + dr["MA_KHO"].ToString() + "'";
                                DataTable dtcheck = clsMain.ReturnDataTable(sqltest);
                                if (dtcheck.Rows.Count > 0)
                                {
                                    DataRow drbs = dtbs.NewRow();
                                    try
                                    {
                                        if (dr["SOLUONG"] != DBNull.Value)
                                        {
                                            drbs["SOLUONG"] = float.Parse(dr["SOLUONG"].ToString());
                                        }
                                    }
                                    catch
                                    {
                                        throw new Exception("Lỗi Convert Số Lượng Kiểm Kê");
                                    }
                                    drbs["MA_HANGHOA"] = dtcheck.Rows[0]["MA_HANGHOA"].ToString();
                                    drbs["TEN_HANGHOA"] = dtcheck.Rows[0]["TEN_HANGHOA"].ToString();
                                    drbs["DVT"] = dtcheck.Rows[0]["DVT"].ToString();
                                    drbs["MA_KHO"] = dtcheck.Rows[0]["MA_KHO"].ToString();
                                    drbs["TEN_KHO"] = dtcheck.Rows[0]["TEN_KHO"].ToString();
                                    dtbs.Rows.Add(drbs);
                                }
                            }
                            dtpNgayKK.EditValue = DateTime.Parse(dt.Rows[0]["NGAYKIEMKE"].ToString());
                            dtbs.Rows.Add(null, null, null, null, null, null);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                bindingsoucre();
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }   
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                gridView1.ActiveEditor.SelectAll();
            }));
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Phieukiemke_Load(object sender, EventArgs e)
        {
            this.Text = rm.GetString("kiemke", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnFile_Mo_Click(object sender, EventArgs e)
        {
            string filename = Application.StartupPath + "\\FileKiemKe.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                System.Diagnostics.Process.Start(filename);
            }
        }

    }
}