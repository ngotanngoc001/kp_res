﻿namespace KP_RES 
{
    partial class Frm_Phieukiemke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Phieukiemke));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_Kho = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_Ten = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtSoluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cboNhanvien = new DevExpress.XtraEditors.LookUpEdit();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayphieutam = new DevExpress.XtraEditors.SimpleButton();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnChenhLechKiemKe = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSophieu = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayKK = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnFile_Mo = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_Kho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanvien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKK.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKK.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1222, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 474);
            this.panelControl2.TabIndex = 2;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 223);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 88);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 311);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 81);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 258);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 13);
            this.panelControl4.TabIndex = 14;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 170);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 88);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 82);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 88);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 392);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.gridControl1);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 113);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1263, 478);
            this.panelControl5.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtSoluong,
            this.look_Kho,
            this.look_Ten});
            this.gridControl1.Size = new System.Drawing.Size(1220, 474);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_HANGHOA,
            this.MA_KHO,
            this.TEN_KHO,
            this.TEN_HANGHOA,
            this.DVT,
            this.SOLUONG,
            this.FILL});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 59;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "MA_HANGHOA";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            // 
            // MA_KHO
            // 
            this.MA_KHO.Caption = "MA_KHO";
            this.MA_KHO.FieldName = "MA_KHO";
            this.MA_KHO.Name = "MA_KHO";
            // 
            // TEN_KHO
            // 
            this.TEN_KHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHO.AppearanceCell.Options.UseFont = true;
            this.TEN_KHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHO.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHO.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_KHO.Caption = "Kho";
            this.TEN_KHO.ColumnEdit = this.look_Kho;
            this.TEN_KHO.FieldName = "TEN_KHO";
            this.TEN_KHO.Name = "TEN_KHO";
            this.TEN_KHO.OptionsColumn.FixedWidth = true;
            this.TEN_KHO.Visible = true;
            this.TEN_KHO.VisibleIndex = 1;
            this.TEN_KHO.Width = 320;
            // 
            // look_Kho
            // 
            this.look_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_Kho.Appearance.Options.UseFont = true;
            this.look_Kho.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_Kho.AppearanceDropDown.Options.UseFont = true;
            this.look_Kho.AutoHeight = false;
            this.look_Kho.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_Kho.DisplayMember = "TEN_KHO";
            this.look_Kho.DropDownItemHeight = 40;
            this.look_Kho.Name = "look_Kho";
            this.look_Kho.NullText = "";
            this.look_Kho.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_Kho.ShowHeader = false;
            this.look_Kho.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_Kho.ValueMember = "TEN_KHO";
            this.look_Kho.EditValueChanged += new System.EventHandler(this.look_Kho_EditValueChanged);
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Hàng hóa";
            this.TEN_HANGHOA.ColumnEdit = this.look_Ten;
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 2;
            this.TEN_HANGHOA.Width = 294;
            // 
            // look_Ten
            // 
            this.look_Ten.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_Ten.Appearance.Options.UseFont = true;
            this.look_Ten.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_Ten.AppearanceDropDown.Options.UseFont = true;
            this.look_Ten.AutoHeight = false;
            this.look_Ten.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_Ten.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_Ten.DisplayMember = "TEN_HANGHOA";
            this.look_Ten.DropDownItemHeight = 40;
            this.look_Ten.Name = "look_Ten";
            this.look_Ten.NullText = "";
            this.look_Ten.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.look_Ten.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_Ten.ShowHeader = false;
            this.look_Ten.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_Ten.ValueMember = "TEN_HANGHOA";
            this.look_Ten.EditValueChanged += new System.EventHandler(this.look_Ten_EditValueChanged);
            this.look_Ten.Leave += new System.EventHandler(this.look_Ten_Leave);
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceCell.Options.UseTextOptions = true;
            this.DVT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 3;
            this.DVT.Width = 112;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "Số lượng";
            this.SOLUONG.ColumnEdit = this.txtSoluong;
            this.SOLUONG.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 4;
            this.SOLUONG.Width = 170;
            // 
            // txtSoluong
            // 
            this.txtSoluong.AutoHeight = false;
            this.txtSoluong.Name = "txtSoluong";
            this.txtSoluong.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSoluong_KeyPress);
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 5;
            this.FILL.Width = 263;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnFile_Mo);
            this.panelControl1.Controls.Add(this.cboNhanvien);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLayphieutam);
            this.panelControl1.Controls.Add(this.txtGHICHU);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.btnChenhLechKiemKe);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtSophieu);
            this.panelControl1.Controls.Add(this.dtpNgayKK);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1263, 113);
            this.panelControl1.TabIndex = 0;
            // 
            // cboNhanvien
            // 
            this.cboNhanvien.EnterMoveNextControl = true;
            this.cboNhanvien.Location = new System.Drawing.Point(731, 5);
            this.cboNhanvien.Name = "cboNhanvien";
            this.cboNhanvien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanvien.Properties.Appearance.Options.UseFont = true;
            this.cboNhanvien.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanvien.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanvien.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhanvien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanvien.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã PT", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên PT")});
            this.cboNhanvien.Properties.DisplayMember = "TEN";
            this.cboNhanvien.Properties.DropDownItemHeight = 40;
            this.cboNhanvien.Properties.NullText = "";
            this.cboNhanvien.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanvien.Properties.ShowHeader = false;
            this.cboNhanvien.Properties.ValueMember = "MA";
            this.cboNhanvien.Size = new System.Drawing.Size(248, 26);
            this.cboNhanvien.TabIndex = 13;
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(253, 73);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(106, 35);
            this.btnXoa.TabIndex = 10;
            this.btnXoa.Text = "&Xóa dòng";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLayphieutam
            // 
            this.btnLayphieutam.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayphieutam.Appearance.Options.UseFont = true;
            this.btnLayphieutam.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnLayphieutam.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLayphieutam.Location = new System.Drawing.Point(367, 73);
            this.btnLayphieutam.Margin = new System.Windows.Forms.Padding(4);
            this.btnLayphieutam.Name = "btnLayphieutam";
            this.btnLayphieutam.Size = new System.Drawing.Size(128, 35);
            this.btnLayphieutam.TabIndex = 11;
            this.btnLayphieutam.Text = "&Nhập dữ liệu";
            this.btnLayphieutam.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(77, 37);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(902, 26);
            this.txtGHICHU.TabIndex = 7;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 40);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 19);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Ghi chú";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(651, 8);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(71, 19);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Nhân viên";
            // 
            // btnChenhLechKiemKe
            // 
            this.btnChenhLechKiemKe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChenhLechKiemKe.Appearance.Options.UseFont = true;
            this.btnChenhLechKiemKe.Image = global::KP_RES.Properties.Resources.scales_26;
            this.btnChenhLechKiemKe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChenhLechKiemKe.Location = new System.Drawing.Point(568, 73);
            this.btnChenhLechKiemKe.Margin = new System.Windows.Forms.Padding(4);
            this.btnChenhLechKiemKe.Name = "btnChenhLechKiemKe";
            this.btnChenhLechKiemKe.Size = new System.Drawing.Size(197, 35);
            this.btnChenhLechKiemKe.TabIndex = 12;
            this.btnChenhLechKiemKe.Text = "&Điều chỉnh chênh lệch";
            this.btnChenhLechKiemKe.Click += new System.EventHandler(this.btnChenhLechKiemKe_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(77, 73);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 8;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(165, 73);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 9;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 8);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Số phiếu";
            // 
            // txtSophieu
            // 
            this.txtSophieu.EnterMoveNextControl = true;
            this.txtSophieu.Location = new System.Drawing.Point(77, 5);
            this.txtSophieu.Name = "txtSophieu";
            this.txtSophieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSophieu.Properties.Appearance.Options.UseFont = true;
            this.txtSophieu.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtSophieu.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtSophieu.Properties.ReadOnly = true;
            this.txtSophieu.Size = new System.Drawing.Size(248, 26);
            this.txtSophieu.TabIndex = 1;
            // 
            // dtpNgayKK
            // 
            this.dtpNgayKK.EditValue = null;
            this.dtpNgayKK.EnterMoveNextControl = true;
            this.dtpNgayKK.Location = new System.Drawing.Point(396, 5);
            this.dtpNgayKK.Name = "dtpNgayKK";
            this.dtpNgayKK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKK.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayKK.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKK.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayKK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayKK.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKK.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKK.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKK.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKK.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayKK.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayKK.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayKK.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(332, 8);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 19);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Ngày";
            // 
            // btnFile_Mo
            // 
            this.btnFile_Mo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile_Mo.Appearance.Options.UseFont = true;
            this.btnFile_Mo.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnFile_Mo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFile_Mo.Location = new System.Drawing.Point(503, 73);
            this.btnFile_Mo.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile_Mo.Name = "btnFile_Mo";
            this.btnFile_Mo.Size = new System.Drawing.Size(57, 35);
            this.btnFile_Mo.TabIndex = 92;
            this.btnFile_Mo.Click += new System.EventHandler(this.btnFile_Mo_Click);
            // 
            // Frm_Phieukiemke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 591);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Phieukiemke";
            this.Text = "Kiểm kê";
            this.Load += new System.EventHandler(this.Frm_Phieukiemke_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_Kho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanvien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKK.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKK.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.DateEdit dtpNgayKK;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSophieu;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.SimpleButton btnChenhLechKiemKe;
        private DevExpress.XtraGrid.Columns.GridColumn MA_KHO;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHO;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtSoluong;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnLayphieutam;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit look_Kho;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit look_Ten;
        private DevExpress.XtraEditors.LookUpEdit cboNhanvien;
        private DevExpress.XtraEditors.SimpleButton btnFile_Mo;




    }
}