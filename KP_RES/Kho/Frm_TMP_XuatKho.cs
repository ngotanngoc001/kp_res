﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraEditors;

namespace KP_RES
{
    public partial class Frm_TMP_XuatKho : Form
    {
        BindingSource bs = new BindingSource();
        DataTable DTSOUCRE = new DataTable();
        DataTable dtdshh = new DataTable();
        bool flag = false;
        string mancc = "";
        string makho = "";
        string sMaNV = "";
        string mapt = "";
        string ngayct = "";
        string note = "";
        int idncc;
        string grid = "grv_PhieuKho";
        public int Mode = 0;

        public Frm_TMP_XuatKho(String MaNV)
        {
            InitializeComponent();
            sMaNV = MaNV;
        }

        private void Frm_TMP_XuatKho_Load(object sender, EventArgs e)
        {
            loaddatagridview();
            khoitaodatatruyen();
        }

        private void loaddatagridview()
        {
            String sSQL = "";
            sSQL += "SELECT A.MA, A.SOPHIEU, A.NGAYCHUNGTU, A.NHANVIEN,  B.TENNHANVIEN, A.KHO, C.TEN_KHO, A.NHACUNGCAP AS MANHACUNGCAP, D.TEN AS NHACUNGCAP, A.GHICHU, A.PT_MA, E.PT_TEN" + "\n";
            sSQL += "FROM KHO_PHIEUKHO A" + "\n";
            sSQL += "INNER JOIN DM_NHANVIEN B ON A.NHANVIEN = B.MANHANVIEN" + "\n";
            sSQL += "INNER JOIN KHO C ON A.KHO = C.MA_KHO" + "\n";
            sSQL += "INNER JOIN NHACUNGCAP D ON A.NHACUNGCAP = D.MA" + "\n";
            sSQL += "INNER JOIN KHO_PHUONGTHUC E ON A.PT_MA = E.PT_MA" + "\n";
            sSQL += "WHERE A.LOAI = 0" + "\n";
            //sSQL += "AND ACTIVE = 1" + "\n";

            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            grc_PhieuKho.DataSource = myDT;
        }

        private void loadDSHH()
        {
            String sSQL = "";
            sSQL += "SELECT A.HANGHOA, B.TEN_HANGHOA, A.TEN_DONVITINH, A.THUE, A.TIENTHUE, A.CHIETKHAU, A.TIENCHIETKHAU, A.SOLUONG, A.DONGIA, A.THANHTIEN, A.TONGCONG, A.GIAVON, ISNULL(A.DONGIA,0) * ISNULL(A.SOLUONG,0) AS TIENTCK, B.MAVACH, A.SLTONCUOIKY" + "\n";
            sSQL += "FROM CHITIETPHIEUKHO A" + "\n";
            sSQL += "INNER JOIN HANGHOA B ON A.HANGHOA = B.MA_HANGHOA" + "\n";
            sSQL += "WHERE PHIEUKHO = " + clsMain.SQLString(idncc.ToString()) + "\n";

            dtdshh = clsMain.ReturnDataTable(sSQL);
            grc_Detail_PhieuKho.DataSource = dtdshh;
        }

        private float LoadSLTonKho(string sMaKho, string sMaHangHoa)
        {

            float SLTonKho = 0;
            try
            {
                string sMacuahang;
                DataTable dt;
                dt = clsMain.ReturnDataTable("Exec SELECT_CUAHANG_KHO1" + clsMain.SQLString(sMaKho));
                sMacuahang = dt.Rows[0]["MA_CUAHANG"].ToString();
                string sSQL = "";

                sSQL += "EXEC SP_KHO_TONKHO_NEW4 @sCuaHang=" + clsMain.SQLString(sMacuahang) + ",";
                sSQL += "@sKho=" + clsMain.SQLString(sMaKho) + ",";
                sSQL += "@sTuNgay =" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", "20000801") + " 00:00:00") + ",";
                sSQL += "@sDenNgay=" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", DateTime.Now) + " 23:59:59") + ",";
                sSQL += "@sNhom=" + clsMain.SQLString("0") + ",";
                sSQL += "@sHangHoa=" + clsMain.SQLString(sMaHangHoa);
                dt = clsMain.ReturnDataTable(sSQL);

                SLTonKho = float.Parse(dt.Rows[0]["SLTONCUOIKY"].ToString());
            }
            catch (Exception)
            {
            }

            return SLTonKho;
        }

        private void khoitaodatatruyen()
        {
            DTSOUCRE.Columns.Add("MA", typeof(string));
            DTSOUCRE.Columns.Add("TEN", typeof(string));
            DTSOUCRE.Columns.Add("DVT", typeof(string));
            DTSOUCRE.Columns.Add("SL", typeof(float));
            DTSOUCRE.Columns.Add("GIAXUAT", typeof(float));
            DTSOUCRE.Columns.Add("GIAVON", typeof(float));
            DTSOUCRE.Columns.Add("GIANHAP", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIENTCK", typeof(float));
            DTSOUCRE.Columns.Add("CK", typeof(float));
            DTSOUCRE.Columns.Add("TIENCK", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIEN", typeof(float));
            DTSOUCRE.Columns.Add("VAT", typeof(float));
            DTSOUCRE.Columns.Add("TIENVAT", typeof(float));
            DTSOUCRE.Columns.Add("TONGCONG", typeof(float));
			DTSOUCRE.Columns.Add("MA_VACH", typeof(string));
            DTSOUCRE.Columns.Add("SLTONCUOIKY", typeof(float));
            DTSOUCRE.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }

        private void truyendulieu()
        {
            DTSOUCRE.Clear();
            if (dtdshh.Rows.Count > 0)
            {
                if (Mode == 0) // truyền dữ liệu qua form Frm_XuatKho
                {
                    Frm_Xuatkho.tmp_MANCC = mancc;
                    Frm_Xuatkho.tmp_MAKHONHAP = makho;
                    Frm_Xuatkho.tmp_PT_MAPHUONGTHUC = mapt;
                    Frm_Xuatkho.tmp_Ngayct = ngayct;
                    Frm_Xuatkho.tmp_NOTE = note;
                    Frm_Xuatkho.tmp_ID = idncc;
                    Frm_Xuatkho.tmp_MAPHIEU = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (0)").Rows[0][0].ToString();

                    for (int i = 0; i <= dtdshh.Rows.Count - 1; i++)
                    {
                        DataRow dr = DTSOUCRE.NewRow();
                        dr["MA"] = dtdshh.Rows[i]["HANGHOA"];
                        dr["TEN"] = dtdshh.Rows[i]["TEN_HANGHOA"];
                        dr["DVT"] = dtdshh.Rows[i]["TEN_DONVITINH"];
                        dr["SL"] = dtdshh.Rows[i]["SOLUONG"];
                        dr["GIAXUAT"] = dtdshh.Rows[i]["DONGIA"];
                        dr["GIAVON"] = dtdshh.Rows[i]["GIAVON"];
                        dr["THANHTIENTCK"] = dtdshh.Rows[i]["TIENTCK"];
                        dr["CK"] = dtdshh.Rows[i]["CHIETKHAU"];
                        dr["TIENCK"] = dtdshh.Rows[i]["TIENCHIETKHAU"];
                        dr["THANHTIEN"] = dtdshh.Rows[i]["THANHTIEN"];
                        dr["VAT"] = dtdshh.Rows[i]["THUE"];
                        dr["TIENVAT"] = dtdshh.Rows[i]["TIENTHUE"];
                        dr["TONGCONG"] = dtdshh.Rows[i]["TONGCONG"];
                        dr["MA_VACH"] = dtdshh.Rows[i]["MAVACH"];
                        dr["SLTONCUOIKY"] = LoadSLTonKho(makho, dtdshh.Rows[i]["HANGHOA"].ToString());
                        DTSOUCRE.Rows.Add(dr);
                    }
                    DataRow dr_NULL = DTSOUCRE.NewRow();
                    DTSOUCRE.Rows.Add(dr_NULL);
                    DTSOUCRE.AcceptChanges();
                    Frm_Xuatkho.BS.DataSource = DTSOUCRE;
                }
                else if (Mode == 1) // truyền dữ liệu qua form Frm_NhapKho
                {
                    Frm_Nhapkho.tmp_MAPHIEU = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (1)").Rows[0][0].ToString();

                    for (int i = 0; i <= dtdshh.Rows.Count - 1; i++)
                    {
                        DataRow dr = DTSOUCRE.NewRow();
                        dr["MA"] = dtdshh.Rows[i]["HANGHOA"];
                        dr["TEN"] = dtdshh.Rows[i]["TEN_HANGHOA"];
                        dr["DVT"] = dtdshh.Rows[i]["TEN_DONVITINH"];
                        dr["SL"] = dtdshh.Rows[i]["SOLUONG"];
                        dr["GIANHAP"] = dtdshh.Rows[i]["DONGIA"];
                        dr["THANHTIENTCK"] = dtdshh.Rows[i]["TIENTCK"];
                        dr["CK"] = 0;
                        dr["TIENCK"] = 0;
                        dr["GIAVON"] = dtdshh.Rows[i]["GIAVON"];
                        dr["THANHTIEN"] = dtdshh.Rows[i]["TIENTCK"];
                        dr["VAT"] = 0;
                        dr["TIENVAT"] = 0;
                        dr["TONGCONG"] = dtdshh.Rows[i]["TIENTCK"];
                        dr["MA_VACH"] = dtdshh.Rows[i]["MAVACH"];
                        DTSOUCRE.Rows.Add(dr);
                    }
                    DataRow dr_NULL = DTSOUCRE.NewRow();
                    DTSOUCRE.Rows.Add(dr_NULL);
                    DTSOUCRE.AcceptChanges();
                    Frm_Nhapkho.BS.DataSource = DTSOUCRE;
                }
                this.Close();
            }

        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_PhieuKho")
                {
                    grv_PhieuKho.FocusedRowHandle = grv_PhieuKho.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (grv_PhieuKho.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    grv_Detail_PhieuKho.FocusedRowHandle = grv_Detail_PhieuKho.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (grv_Detail_PhieuKho.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_PhieuKho")
                {
                    grv_PhieuKho.FocusedRowHandle = grv_PhieuKho.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (grv_PhieuKho.FocusedRowHandle == grv_PhieuKho.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    grv_Detail_PhieuKho.FocusedRowHandle = grv_Detail_PhieuKho.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (grv_Detail_PhieuKho.FocusedRowHandle == grv_Detail_PhieuKho.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_PhieuKho")
                {
                    grv_PhieuKho.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    grv_Detail_PhieuKho.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (grid == "grv_PhieuKho")
                {
                    grv_PhieuKho.FocusedRowHandle = grv_PhieuKho.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    grv_Detail_PhieuKho.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void grv_ncctmp_Click(object sender, EventArgs e)
        {
            grid = "grv_PhieuKho";
            if (grv_PhieuKho.FocusedRowHandle >= 0)
            {
                if (grv_PhieuKho.SelectedRowsCount > 0)
                {
                    idncc = Convert.ToInt32(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["MA"]).ToString());
                    mancc = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["MANHACUNGCAP"]).ToString());
                    makho = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["KHO"]).ToString());
                    sMaNV = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["NHANVIEN"]).ToString());
                    mapt = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["PT_MA"]).ToString());
                    ngayct = string.Format("{0:dd/MM/yyyy}", grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["NGAYCHUNGTU"]));
                    note = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["GHICHU"]).ToString());
                    loadDSHH();
                }
            }
        }

        private void grv_ncctmp_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grv_PhieuKho.FocusedRowHandle >= 0)
            {
                if (grv_PhieuKho.SelectedRowsCount > 0)
                {
                    if (flag)
                    {
                        grv_PhieuKho.FocusedRowHandle = 0;
                        flag = false;
                    }
                    idncc = Convert.ToInt32(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["MA"]).ToString());
                    mancc = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["MANHACUNGCAP"]).ToString());
                    makho = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["KHO"]).ToString());
                    sMaNV = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["NHANVIEN"]).ToString());
                    mapt = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["PT_MA"]).ToString());
                    ngayct = string.Format("{0:dd/MM/yyyy}", grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["NGAYCHUNGTU"]));
                    note = Convert.ToString(grv_PhieuKho.GetRowCellValue(grv_PhieuKho.FocusedRowHandle, grv_PhieuKho.Columns["GHICHU"]).ToString());
                    loadDSHH();
                }
            }
        }

        private void grv_ncctmp_DoubleClick(object sender, EventArgs e)
        {
            truyendulieu();
        }

        private void grv_ncctmp_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void grv_dshhtmp_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            truyendulieu();
        }

        private void grv_dshhtmp_Click(object sender, EventArgs e)
        {
            grid = "grv_Detail_PhieuKho";
        }

    }   
}
