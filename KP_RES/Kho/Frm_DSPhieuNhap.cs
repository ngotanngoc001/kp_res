﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_DSPhieuNhap : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_DSPhieuNhap()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_DSPhieuNhap_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Cửa hàng
            {
                dt = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Cửa hàng";
            }
            else if (optGroup.SelectedIndex == 1)//Kho
            {
                dt = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Kho";
            }
            else if (optGroup.SelectedIndex == 2)//Nhóm hàng
            {
                dt = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhóm hàng";
            }
            else if (optGroup.SelectedIndex == 3)//Hàng hóa
            {
                dt = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Hàng hóa";
            }
            else if (optGroup.SelectedIndex == 4)//Nhà cung cấp
            {
                dt = clsMain.ReturnDataTable("select MA,TEN from NHACUNGCAP where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhà cung cấp";
            }
            else if (optGroup.SelectedIndex == 5)//Nhân viên
            {
                dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhân viên";
            }
            else if (optGroup.SelectedIndex == 6)//Phương thức
            {
                dt = clsMain.ReturnDataTable("select PT_MA as MA,PT_TEN as TEN from KHO_PHUONGTHUC where LOAIPHIEU_MA=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên phương thức" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Phương thức";
            }

            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            sMa = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            sSOPHIEU = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SOPHIEU").ToString();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value)
                    throw new Exception("Chưa chọn đối tượng cần xem");
                string _tungaydenngay = "NGAYTAO between '" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue)+" 00:00:00" + "' and '"
                                      + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59" + "'";
                if (optGroup.SelectedIndex == 0)//Cửa hàng
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,CUAHANG CH,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and CH.MA_CUAHANG=K.MA_CUAHANG  and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and K.MA_CUAHANG='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 1)//Kho
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and PK.KHO='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 2)//Nhóm hàng
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,HANGHOA H,KHO K,KHO_PHUONGTHUC PT,CHITIETPHIEUKHO CTPK,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and CTPK.PHIEUKHO=PK.MA and H.MA_HANGHOA=CTPK.HANGHOA ";
                    sql += "and NV.MANHANVIEN=PK.NHANVIEN and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and  H.MA_NHOMHANG='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 3)//Hàng hóa
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,HANGHOA H,KHO K,KHO_PHUONGTHUC PT,CHITIETPHIEUKHO CTPK,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and CTPK.PHIEUKHO=PK.MA and H.MA_HANGHOA=CTPK.HANGHOA ";
                    sql += "and NV.MANHANVIEN=PK.NHANVIEN and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and H.MA_HANGHOA='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 4)//Nhà cung cấp
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and PK.NHACUNGCAP='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
               
                else if (optGroup.SelectedIndex == 5)//Nhân viên
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and PK.NHANVIEN='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 6)//Phương thức
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 1 and PK.PT_MA='" + cboDATA.EditValue.ToString() + "' and " + _tungaydenngay;
                }
                else if (optGroup.SelectedIndex == 7)//Phương thức
                {
                    sql += "select distinct PK.MA,PK.SOPHIEU,TEN_KHO as KHO,PT_TEN,TEN,NGAYCHUNGTU,NGAYTAO,TONGTIEN,PK.GHICHU,TENNHANVIEN ";
                    sql += "from KHO_PHIEUKHO PK,NHACUNGCAP N,KHO K,KHO_PHUONGTHUC PT,DM_NHANVIEN NV ";
                    sql += "where PK.NHACUNGCAP=N.MA and PK.KHO=K.MA_KHO and PT.PT_MA=PK.PT_MA and NV.MANHANVIEN=PK.NHANVIEN ";
                    sql += "and PK.LOAI=1 AND ISNULL(ACTIVE,1) = 0  and " + _tungaydenngay;
                }
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.ma = gridView1.GetFocusedRowCellValue(MA).ToString();
                    frm.Mode = 1;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa chọn phiếu cần in");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                Frm_BCChitietphieukho frm = new Frm_BCChitietphieukho(gridView1.GetFocusedRowCellValue(MA).ToString(), 1);
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
            }
            else
                XtraMessageBox.Show("Chưa chọn phiếu cần xem", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
            this.Refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 49;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }
        
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        String sMa = "";
        String sSOPHIEU = "";
        

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (XtraMessageBox.Show("Xác nhận bạn muốn hủy phiếu nhập : "+ sSOPHIEU , "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "UPDATE KHO_PHIEUKHO SET ACTIVE = 0" + "\n";
            sSQL += "WHERE MA = " + clsMain.SQLString(sMa) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                XtraMessageBox.Show("Đã hủy thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnTimkiem_Click(null, null);
            }
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }










    }
}