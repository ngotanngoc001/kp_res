﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using System.Globalization;
using System.Resources;
using DevExpress.XtraGrid.Views.Base;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace KP_RES 
{
    public partial class Frm_Nhapkho : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();
        public static string tmp_MANCC;
        public static string tmp_MAKHONHAP;
        public static string tmp_PT_MAPHUONGTHUC;
        public static string tmp_NOTE;
        public static string tmp_MAPHIEU;
        public static string tmp_Ngayct;
        public static bool flag = false;
        public static int tmp_ID = 0;
        bool candoi = false;
        bool bGIANHAPTRUOCTHUE=true ;//Giá nhập : True-Trước thuế : False-Sau thuế
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);

        public Frm_Nhapkho()
        {
            InitializeComponent();
            this.Text = rm.GetString("nhapkho", culture);
            try
            {
                LoadPermission();
                SetDulieuKhoiTao();
                Load_Data();
                KhoaMoControl(false);
                KhoaMoControlCN(false);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public Frm_Nhapkho(DataTable dt)//Nhap kho kiểm kê
        {
            InitializeComponent();
            try
            {
                candoi = true;
                dtpNgay.EditValue = clsGlobal.gdServerDate;
                txtSophieu.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (1)").Rows[0][0].ToString();
                btnThem.Visible = btnXoa.Visible = btn_tam.Visible = btnLayphieutam.Visible = btnLayPhieuNhap.Visible = false;
                SL.OptionsColumn.ReadOnly = TEN.OptionsColumn.ReadOnly = CK.OptionsColumn.ReadOnly = GIANHAP.OptionsColumn.ReadOnly = VAT.OptionsColumn.ReadOnly = true;
                cboPT.Properties.ReadOnly = cboKho.Properties.ReadOnly = true;
                Load_Data();
                cboKho.EditValue = int.Parse(dt.Rows[0]["MA_KHO"].ToString());
                cboPT.EditValue = 4;//PT:Nhập cân đối
                BS.DataSource = dt;
                gridControl1.DataSource = BS;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Frm_Nhapkho_Load(object sender, EventArgs e)
        {
            this.Text = rm.GetString("nhapkho", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            btn_tam.Text = rm.GetString("luutam", culture);
            btnLayphieutam.Text = rm.GetString("layphieutam", culture);
            btnLayPhieuXuat.Text = rm.GetString("layphieuxuat", culture);
            btnLayPhieuNhap.Text = rm.GetString("layphieunhap", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["MA_VACH"].Caption = rm.GetString("mavach", culture);
            gridView1.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView1.Columns["DVT"].Caption = rm.GetString("dvt", culture);
            gridView1.Columns["SL"].Caption = rm.GetString("soluong", culture);
            gridView1.Columns["GIANHAP"].Caption = rm.GetString("gianhap", culture);
            gridView1.Columns["THANHTIENTCK"].Caption = rm.GetString("thanhtientck", culture);
            gridView1.Columns["CK"].Caption = rm.GetString("ck", culture);
            gridView1.Columns["TIENCK"].Caption = rm.GetString("tienck", culture);
            // gridView1.Columns["GIAVON"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["THANHTIEN"].Caption = rm.GetString("thanhtien", culture);
            gridView1.Columns["VAT"].Caption = rm.GetString("vat", culture);
            gridView1.Columns["TIENVAT"].Caption = rm.GetString("tienmat", culture);
            gridView1.Columns["TONGCONG"].Caption = rm.GetString("tongcong", culture);
            lblTEN.Text = rm.GetString("mavach", culture);
            lblGHICHU.Text = rm.GetString("kho", culture);
            labelControl6.Text = rm.GetString("ghichu", culture);
            labelControl4.Text = rm.GetString("ngay", culture);
            labelControl1.Text = rm.GetString("phuongthuc", culture);
            labelControl3.Text = rm.GetString("nhanvien", culture);
            labelControl2.Text = rm.GetString("nhacungcap", culture);
            labelControl5.Text = rm.GetString("datra", culture);
            labelControl8.Text = rm.GetString("conlai", culture);
            labelControl7.Text = rm.GetString("ngaytra", culture);
            chkCONGNO.Text = rm.GetString("congno", culture);
        }

        private void Frm_Nhapkho_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("12");
            btnLuu.Enabled = btnThem.Enabled;
            if (clsUserManagement.CheckActive("12"))
            {
                pnlCongNo.Visible = true;
                chkCONGNO.Visible = true;
            }
            else
            {
                pnlCongNo.Visible = false;
                chkCONGNO.Visible = false;
            }
        }

        private void Load_Data()
        {
            if (clsMain.ReturnDataTable("Select TEN,GIATRI From CAUHINH where TEN='GIAMUATRUOCTHUE'").Rows[0]["GIATRI"].ToString() == "1")
                bGIANHAPTRUOCTHUE = true;
            else
                bGIANHAPTRUOCTHUE = false;

            DataTable dt = clsMain.ReturnDataTable("select c.MA_CUAHANG,c.TEN_CUAHANG,k.MA_KHO as MA,k.TEN_KHO as TEN from KHO k,CUAHANG c where k.MA_CUAHANG=c.MA_CUAHANG and c.SUDUNG=1 and k.SUDUNG=1 order by TEN_CUAHANG,TEN");
            if (dt.Rows.Count > 0)
            {
                cboKho.Properties.DataSource = dt;
                cboKho.EditValue = dt.Rows[0]["MA"].ToString();
            }
            dt = null;
            dt = clsMain.ReturnDataTable("select MA,TEN from NHACUNGCAP where SUDUNG=1 and LOAI=1 order by TEN");
            if (dt.Rows.Count > 0)
            {
                cboNCC.Properties.DataSource = dt;
                cboNCC.EditValue = dt.Rows[0]["MA"].ToString();
            }
            dt = null;
            dt = clsMain.ReturnDataTable("select PT_MA as MA,PT_TEN as TEN from KHO_PHUONGTHUC where LOAIPHIEU_MA=1 order by TEN");
            if (dt.Rows.Count > 0)
            {
                cboPT.Properties.DataSource = dt;
                cboPT.EditValue = dt.Rows[0]["MA"].ToString();
            }
            dt = null;
            dt = clsMain.ReturnDataTable("select MANHANVIEN,TENNHANVIEN from DM_NHANVIEN N,SYS_USER U where N.MANHANVIEN=U.MaNV AND UserID='" + clsGlobal.gsUserID + "'");
            if (dt.Rows.Count > 0)
            {
                txtNVnhap.Text = dt.Rows[0]["TENNHANVIEN"].ToString();
                txtNVnhap.Tag = dt.Rows[0]["MANHANVIEN"].ToString();
            }
            //07/08 khang thêm dữ liệu cho cobobox mã vạch
            dt = null;
            dt = clsMain.ReturnDataTable("select MA_HANGHOA,TEN_HANGHOA,THUE,GIANHAP,TEN_DONVITINH,GIABAN1 , '' As TONHIENTAI, MAVACH from HANGHOA h,DONVITINH d where TONKHO=1 AND h.MA_DONVITINH=d.MA_DONVITINH order by TEN_HANGHOA");
            if (dt.Rows.Count > 0)
            {
                look_ten2.DataSource = dt;
                look_mavach2.DataSource = dt;
            }
        }

        private void SetDulieuKhoiTao()
        {
            dtpNgay.EditValue = clsGlobal.gdServerDate;
            dtpNgayHenTra.EditValue = clsGlobal.gdServerDate;
            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("MA", typeof(string));
            DTSOUCRE.Columns.Add("MA_VACH", typeof(string));
            DTSOUCRE.Columns.Add("TEN", typeof(string));
            DTSOUCRE.Columns.Add("DVT", typeof(string));
            DTSOUCRE.Columns.Add("GIABAN1", typeof(float));
            DTSOUCRE.Columns.Add("SL", typeof(float));
            DTSOUCRE.Columns.Add("GIANHAP", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIENTCK", typeof(float));
            DTSOUCRE.Columns.Add("CK", typeof(float));
            DTSOUCRE.Columns.Add("TIENCK", typeof(float));
            DTSOUCRE.Columns.Add("GIAVON", typeof(float));
            DTSOUCRE.Columns.Add("THANHTIEN", typeof(float));
            DTSOUCRE.Columns.Add("VAT", typeof(float));
            DTSOUCRE.Columns.Add("TIENVAT", typeof(float));
            DTSOUCRE.Columns.Add("TONGCONG", typeof(float));
            DTSOUCRE.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            BS.DataSource = DTSOUCRE;
            gridControl1.DataSource = BS;
        }

        private void KhoaMoControl(bool flag)
        {
            txtSophieu.Properties.ReadOnly = cboKho.Properties.ReadOnly = cboPT.Properties.ReadOnly = cboNCC.Properties.ReadOnly = txtNVnhap.Properties.ReadOnly = dtpNgay.Properties.ReadOnly = txtGHICHU.Properties.ReadOnly = !flag;
            gridView1.OptionsBehavior.Editable = flag;
            txtSophieu.Properties.ReadOnly = true;
            txtNVnhap.Properties.ReadOnly = true;
        }

        private void KhoaMoControlCN(bool flag)
        {
            txtTienDaTra.Properties.ReadOnly = !flag;
            dtpNgayHenTra.Properties.ReadOnly = !flag;
        }

        private void SetRongControl()
        {
            txtGHICHU.Text = "";
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                if (gridView1.GetFocusedRowCellValue(MA).ToString() != string.Empty)
                    gridView1.DeleteSelectedRows();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(true);
            SetDulieuKhoiTao();
            txtSophieu.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (1)").Rows[0][0].ToString();
            txtSophieu.Focus();
        }

        private void txt_soluong_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //sự kiện Keypress cho cột SL,Giá Nhập
                if (gridView1.FocusedRowHandle >= 0)
                {
                    if (gridView1.GetFocusedRowCellValue(MA).Equals(""))
                        e.Handled = true;
                    else
                    {
                        if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar) & e.KeyChar != '-')
                            e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txt_chietkhau_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                //sự kiện Keypress cho cột CK,VAT
                if (gridView1.GetFocusedRowCellValue(MA).Equals(""))
                    e.Handled = true;
                else
                {
                    if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
                        e.Handled = true;
                }
            }
        }

        private void txt_soluong_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                //values =null,0,không phải là số
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || float.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txt_soluong.EditValueChanging -= txt_soluong_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["SL"] = 1;
                    dv["THANHTIENTCK"] = float.Parse(dv["GIANHAP"].ToString());
                    dv["TIENCK"] = (float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100;
                    dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                    if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                        dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100;
                    else
                        dv["TIENVAT"] = float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100);
                    dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString());
                    if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                        dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                    else
                        dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString());
                    e.NewValue = 1;
                    BS.ResetCurrentItem();
                }
                // values la số # 0
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txt_soluong.EditValueChanging -= txt_soluong_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;
                        float _SOLUONG = float.Parse(e.NewValue.ToString());
                        dv["THANHTIENTCK"] = float.Parse(dv["GIANHAP"].ToString()) * _SOLUONG;
                        dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * _SOLUONG;
                        dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * _SOLUONG;
                        else
                            dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * _SOLUONG;
                        dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * _SOLUONG;
                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            dv["TONGCONG"] = (float.Parse(dv["GIAVON"].ToString()) * _SOLUONG) + float.Parse(dv["TIENVAT"].ToString());
                        else
                            dv["TONGCONG"] = (float.Parse(dv["GIAVON"].ToString()) * _SOLUONG);
                        dv["SL"] = e.NewValue;
                        BS.ResetCurrentItem();
                    }
                }
                TinhTienCongNo();
                txt_soluong.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_soluong_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txt_gianhap_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                //values = null,0,không phải số
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || float.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txt_gianhap.EditValueChanging -= txt_gianhap_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["GIANHAP"] = float.Parse(dv["GIANHAP"].ToString());
                    dv["THANHTIENTCK"] = float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["SL"].ToString());
                    dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                    dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                    if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                        dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                    else
                        dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                    dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                    if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                        dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                    else
                        dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                    e.NewValue = float.Parse(dv["GIANHAP"].ToString());
                    BS.ResetCurrentItem();
                }
                //values la số # 0
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txt_gianhap.EditValueChanging -= txt_gianhap_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;
                        float _GIANHAP = float.Parse(e.NewValue.ToString());
                        dv["GIANHAP"] = e.NewValue;
                        dv["THANHTIENTCK"] = float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["SL"].ToString());
                        dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                        dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                        else
                            dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                        dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                        else
                            dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                        BS.ResetCurrentItem();
                    }
                }
                
                TinhTienCongNo();
                txt_gianhap.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_gianhap_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txt_chietkhau_EditValueChanging(object sender, ChangingEventArgs e)
        {
            float convertam;
            float convertam1;
            // null,không phải số
            if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("")||!float.TryParse(e.NewValue.ToString(), out convertam1))
            {
                txt_chietkhau.EditValueChanging -= txt_chietkhau_EditValueChanging;
                DataRowView dv = (DataRowView)BS.Current;
                dv["CK"] = float.Parse(dv["CK"].ToString());
                dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                else
                    dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                else
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                e.NewValue = float.Parse(dv["CK"].ToString());
                BS.ResetCurrentItem();
            }
            //values la số 
            else if (float.TryParse(e.NewValue.ToString(), out convertam))
            {
                txt_chietkhau.EditValueChanging -= txt_chietkhau_EditValueChanging;
                DataRowView dv = (DataRowView)BS.Current;
                dv["CK"] = e.NewValue;
                dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                else
                    dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                else
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                BS.ResetCurrentItem();
            }
            TinhTienCongNo();
            txt_chietkhau.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_chietkhau_EditValueChanging);
        }

        private void txt_vat_EditValueChanging(object sender, ChangingEventArgs e)
        {
            float convertam;
            float convertam1;
           // values = null,không phải là số
            if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || !float.TryParse(e.NewValue.ToString(), out convertam1))
            {
                txt_vat.EditValueChanging -= txt_vat_EditValueChanging;
                DataRowView dv = (DataRowView)BS.Current;
                dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                dv["VAT"] = float.Parse(dv["VAT"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                else
                    dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                else
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                e.NewValue = float.Parse(dv["VAT"].ToString());
                BS.ResetCurrentItem();
            }
            // values la số
            else if (float.TryParse(e.NewValue.ToString(), out convertam))
            {
                txt_vat.EditValueChanging -= txt_vat_EditValueChanging;
                DataRowView dv = (DataRowView)BS.Current;
                dv["TIENCK"] = ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                dv["GIAVON"] = float.Parse(dv["GIANHAP"].ToString()) - ((float.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["CK"].ToString())) / 100);
                dv["VAT"] = e.NewValue;
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TIENVAT"] = ((float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["VAT"].ToString())) / 100) * float.Parse(dv["SL"].ToString());
                else
                    dv["TIENVAT"] = (float.Parse(dv["GIAVON"].ToString()) - float.Parse(dv["GIAVON"].ToString()) / (1 + float.Parse(dv["VAT"].ToString()) / 100)) * float.Parse(dv["SL"].ToString());
                dv["THANHTIEN"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString()) + float.Parse(dv["TIENVAT"].ToString());
                else
                    dv["TONGCONG"] = float.Parse(dv["GIAVON"].ToString()) * float.Parse(dv["SL"].ToString());
                BS.ResetCurrentItem();
            }
            TinhTienCongNo();
            txt_vat.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_vat_EditValueChanging);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                gridView1.ActiveEditor.SelectAll();
            }));
        }
        //08/08 khang bắt sự kiện enter trên Grid
        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                (gridControl1.FocusedView as ColumnView).FocusedRowHandle++;
                e.Handled = true;
            }
        }
        //08/08 khang thêm cobobox mã vạch bằng GridViewLookup
        private void look_mavach2_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_Nhapkho.BS.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() == "")
                {
                }
                else
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
        }
        //08/08 khang thêm cobobox mã vạch bằng GridViewLookup
        private void look_mavach2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                float tienvat;
                float total;
                DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA", ((DataRowView)editor.GetSelectedDataRow())["MA_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_VACH", ((DataRowView)editor.GetSelectedDataRow())["MAVACH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN", ((DataRowView)editor.GetSelectedDataRow())["TEN_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "VAT", ((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DVT", ((DataRowView)editor.GetSelectedDataRow())["TEN_DONVITINH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIABAN1", ((DataRowView)editor.GetSelectedDataRow())["GIABAN1"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL", 1);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIENTCK", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "CK", 0);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TIENCK", 0);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIAVON", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIEN", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    tienvat = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) * float.Parse(((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString()) / 100;
                else
                    tienvat = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) - float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) / (1 + float.Parse(((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString()) / 100);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TIENVAT", tienvat);
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    total = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) + tienvat;
                else
                    total = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TONGCONG", total);
                TinhTienCongNo();
            }
            catch
            {
            }
        }
        //08/08 khang thêm cobobox tên hàng hóa bằng GridViewLookup
        private void look_ten2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                float tienvat;
                float total;
                DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA", ((DataRowView)editor.GetSelectedDataRow())["MA_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_VACH", ((DataRowView)editor.GetSelectedDataRow())["MAVACH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN", ((DataRowView)editor.GetSelectedDataRow())["TEN_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "VAT", ((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DVT", ((DataRowView)editor.GetSelectedDataRow())["TEN_DONVITINH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIABAN1", ((DataRowView)editor.GetSelectedDataRow())["GIABAN1"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL", 1);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIENTCK", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "CK", 0);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TIENCK", 0);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIAVON", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIEN", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    tienvat = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) * float.Parse(((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString()) / 100;
                else
                    tienvat = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) - float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) / (1 + float.Parse(((DataRowView)editor.GetSelectedDataRow())["THUE"].ToString()) / 100);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TIENVAT", tienvat);
                if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    total = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString()) + tienvat;
                else
                    total = float.Parse(((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TONGCONG", total);
                TinhTienCongNo();
            }
            catch
            {
            }
        }
        //08/08 khang thêm cobobox tên hàng hóa bằng GridViewLookup
        private void look_ten2_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_Nhapkho.BS.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() == "")
                {
                }
                else
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                checkinputbeforeLuu();
                string sophieu = string.Empty;
                int ma;
                bool phieu = checkmaphieu(txtSophieu.Text);
                if (phieu)
                {
                    sophieu = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodePHIEUKHO]  (1)").Rows[0][0].ToString();
                    XtraMessageBox.Show("Mã phiếu " + txtSophieu.Text + " đã tồn tại,lấy mã phiếu mới là " + sophieu, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    sophieu = txtSophieu.Text;
                }
                float TONGTIEN = 0;
                DataTable dtma = clsMain.ReturnDataTable("SELECT IDENT_CURRENT('KHO_PHIEUKHO') as MA");
                if (dtma.Rows.Count > 0)
                {
                    ma = Convert.ToInt32(dtma.Rows[0]["MA"].ToString()) + 1;
                }
                else
                {
                    clsMain.ExecuteSQL("DBCC CHECKIDENT('KHO_PHIEUKHO', RESEED, 0)");
                    ma = 1;
                }
                string sql_Chitiet_Phieukho = laydanhsachsanpham(ma, ref TONGTIEN);
                string sql_Phieukho = string.Format("EXEC SP_PHIEUKHO_INSERT @LOAI='{0}',@NHACUNGCAP='{1}',@KHO='{2}',@SOPHIEU='{3}',@NGAYCHUNGTU='{4}',@NHANVIEN='{5}',@GHICHU=N'{6}',@NGAYTAO='{7}', @NGUOITAO='{8}', @TONGITEN='{9}',@PT_MA='{10}'", 1, cboNCC.EditValue.ToString(), cboKho.EditValue.ToString(), sophieu, dtpNgay.Text, txtNVnhap.Tag, txtGHICHU.Text.Trim(), String.Format("{0:dd/MM/yyyy HH:mm:ss}", clsMain.GetServerDate()), clsGlobal.gsUserID, TONGTIEN, cboPT.EditValue.ToString());
                if (DialogResult.Yes == (XtraMessageBox.Show("Bạn có chắc muốn tạo phiếu nhập này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    //Luu phieu kho
                    clsMain.ExecuteSQL(sql_Phieukho);
                    clsMain.ExecuteSQL(sql_Chitiet_Phieukho);
                    //Update ton kho
                    update_hangton(ma);
                    //Lưu giá vốn vào bảng KHO_DONGIABINHQUAN
                    clsMain.ExecuteSQL("EXEC SP_INSERT_DONGIABINHQUAN_PHIEUNHAP @MA=" + clsMain.SQLString(ma.ToString()));
                    //29/07/2016 chi dung khi co check cong no vi khong phai phieu nhap nao cung co cong no
                    if (chkCONGNO.Checked)
                    {
                        //Công nợ phiếu nhập kho
                        DataTable dtMA = clsMain.ReturnDataTable("SELECT TOP 1 MA FROM KHO_PHIEUKHO ORDER BY MA DESC");
                        cls_KHTT.CongCongNoNhaCungCap(cboNCC.EditValue.ToString(), dtMA.Rows[0]["MA"].ToString(), decimal.Parse(TONGTIEN.ToString()), decimal.Parse(txtTienDaTra.Text), DateTime.Parse(dtpNgayHenTra.EditValue.ToString()), true);
                    }
                    //In phiếu
                    XtraMessageBox.Show("Thêm phiếu nhập thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.ma = ma.ToString();
                    frm.Mode = 1;
                    frm.ShowDialog();
                    frm.Dispose();
                    //nhập cân đối thành công
                    if (candoi)
                    {
                        cls_KP_RES.Fcandoi = true;
                    }
                    btnThem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void checkinputbeforeLuu()
        {
            if (txtSophieu.Text == "")
                throw new Exception("Chưa nhập đủ thông tin");
            if (gridView1.RowCount == 1 && gridView1.GetFocusedRowCellValue(TEN).ToString() == "")
                throw new Exception("Chưa có mặt hàng nào ");
            if (cboPT.EditValue == null)
                throw new Exception("Chưa chọn loại phương thức ");
            if (cboNCC.EditValue == null)
                throw new Exception("Chưa chọn loại đối tượng ");
            if (cboKho.EditValue == null)
                throw new Exception("Chưa chọn kho cần nhập");
        }

        private bool checkmaphieu(string sophieu)
        {
            DataTable dt = clsMain.ReturnDataTable("select SOPHIEU from KHO_PHIEUKHO where SOPHIEU='" + sophieu + "'");
            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        private string laydanhsachsanpham(int maphieu, ref float tongtien)
        {
            string sSQL = "";
            DataTable dt = (DataTable)BS.DataSource;
            foreach (DataRow tam in dt.Rows)
            {
                float dongia = 0;
                float thanhtien = 0;
                float TIENVAT = 0;
                float _TONGTIEN = 0;
                if (tam["MA"].ToString() != "")
                {
                    dongia = float.Parse(tam["GIANHAP"].ToString());
                    thanhtien = float.Parse(tam["THANHTIEN"].ToString());
                    TIENVAT = float.Parse(tam["TIENVAT"].ToString());
                    if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                    {
                        tongtien = tongtien + (thanhtien + TIENVAT);
                        _TONGTIEN = thanhtien + TIENVAT;
                    }
                    else
                    {
                        tongtien = tongtien + thanhtien;
                        _TONGTIEN = thanhtien;
                    }
                    sSQL += "Insert into CHITIETPHIEUKHO(PHIEUKHO,HANGHOA,SOLUONG,DONGIA,THUE,CHIETKHAU,THANHTIEN,TIENTHUE,TIENCHIETKHAU,TONGCONG,NHAPXUAT,TEN_DONVITINH,GIAVON)" + "\n";
                    sSQL += "VALUES(";
                    sSQL += clsMain.SQLString(maphieu.ToString()) + ",";
                    sSQL += clsMain.SQLString(tam["MA"].ToString()) + ",";
                    sSQL += clsMain.SQLString(tam["SL"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dongia.ToString()) + ",";
                    sSQL += clsMain.SQLString(tam["VAT"].ToString()) + ",";
                    sSQL += clsMain.SQLString(tam["CK"].ToString()) + ",";
                    sSQL += clsMain.SQLString(thanhtien.ToString()) + ",";
                    sSQL += clsMain.SQLString(TIENVAT.ToString().Replace(',', '.')) + ",";
                    sSQL += clsMain.SQLString(tam["TIENCK"].ToString()) + ",";
                    sSQL += clsMain.SQLString(_TONGTIEN.ToString()) + ",";
                    sSQL += clsMain.SQLBit(true) + ",";
                    sSQL += clsMain.SQLStringUnicode(tam["DVT"].ToString()) + ",";
                    sSQL += tam["GIAVON"].ToString() + ")";
                    sSQL += "\n";
                }
                else
                {
                    sSQL += "\n";
                }
            }
            return sSQL;
        }

        private void update_hangton(int maphieu)
        {
            string sql_nhap = string.Empty;
            string sql_ton = string.Empty;
            float GIAVON = 0;
            float SOLUONG = 0;
            string sSQL = "";

            sSQL += "SELECT DISTINCT CTPK.HANGHOA,PK.KHO,CTPK.GIAVON" + "\n";
            sSQL += "FROM CHITIETPHIEUKHO CTPK INNER JOIN KHO_PHIEUKHO PK ON PK.MA = CTPK.PHIEUKHO" + "\n";
            sSQL += "WHERE PHIEUKHO=" + clsMain.SQLString(maphieu.ToString()) + "\n";
            DataTable dtphieunhap = clsMain.ReturnDataTable(sSQL);

            foreach (DataRow dr in dtphieunhap.Rows)
            {
                sSQL = "";
                sSQL += "SELECT MA_THANHPHAN,SOLUONG" + "\n";
                sSQL += "FROM HANGHOA" + "\n";
                sSQL += "WHERE ISNULL(MA_THANHPHAN,'') != '' AND MA_HANGHOA=" + clsMain.SQLString(dr["HANGHOA"].ToString()) + "\n";
                DataTable dthanghoathanhphan = clsMain.ReturnDataTable(sSQL);

                sSQL = "";
                sSQL += "SELECT CTPK.HANGHOA,PK.KHO,SUM(CTPK.SOLUONG) AS CTPK_SOLUONG,CTPK.GIAVON,SUM(CTPK.THANHTIEN)AS THANHTIEN,TEN_DONVITINH " + "\n";
                sSQL += "FROM KHO_PHIEUKHO PK INNER JOIN CHITIETPHIEUKHO CTPK ON PK.MA = CTPK.PHIEUKHO" + "\n";
                sSQL += "LEFT JOIN HANGHOA H ON CTPK.HANGHOA = H.MA_HANGHOA " + "\n";
                sSQL += "WHERE CTPK.PHIEUKHO=" + clsMain.SQLString(maphieu.ToString()) + "\n";
                sSQL += "AND CTPK.HANGHOA=" + clsMain.SQLString(dr["HANGHOA"].ToString()) + "\n";
                sSQL += "GROUP BY CTPK.HANGHOA,PK.KHO,CTPK.GIAVON,CTPK.THANHTIEN,TEN_DONVITINH" + "\n";
                DataTable dtdulieu = clsMain.ReturnDataTable(sSQL);

                DataTable dttonkho = new DataTable();
                if (dthanghoathanhphan.Rows.Count > 0)
                {
                    sSQL = "";
                    sSQL += "SELECT HANGHOA FROM TONKHO" + "\n";
                    sSQL += "WHERE HANGHOA=" + clsMain.SQLString(dthanghoathanhphan.Rows[0]["MA_THANHPHAN"].ToString()) + "\n";
                    sSQL += "AND KHO=" + clsMain.SQLString(dr["KHO"].ToString()) + "\n";
                    dttonkho = clsMain.ReturnDataTable(sSQL);

                    sSQL = "";
                    sSQL += "SELECT HANGHOA FROM TONKHO" + "\n";
                    sSQL += "WHERE HANGHOA=" + clsMain.SQLString(dr["HANGHOA"].ToString()) + "\n";
                    sSQL += "AND KHO=" + clsMain.SQLString(dr["KHO"].ToString()) + "\n";
                    DataTable dttonkhocha = clsMain.ReturnDataTable(sSQL);

                    SOLUONG = float.Parse(dtdulieu.Rows[0]["CTPK_SOLUONG"].ToString()) * float.Parse(dthanghoathanhphan.Rows[0]["SOLUONG"].ToString());

                    if (dttonkho.Rows.Count > 0)
                    {
                        sql_nhap += "\n update TONKHO set SOLUONGNHAP = SOLUONGNHAP + " + SOLUONG + ", TRIGIANHAP=TRIGIANHAP + " + float.Parse(dtdulieu.Rows[0]["THANHTIEN"].ToString()) + " WHERE HANGHOA='" + dttonkho.Rows[0]["HANGHOA"].ToString() + "' AND KHO='" + dr["KHO"].ToString() + "'";
                    }
                    else
                    {
                        sql_nhap += "\n insert into TONKHO(HANGHOA,KHO,SOLUONGNHAP,TRIGIANHAP) values('" + dthanghoathanhphan.Rows[0]["MA_THANHPHAN"].ToString() + "','" + dtdulieu.Rows[0]["KHO"].ToString() + "'," + SOLUONG + "," + dtdulieu.Rows[0]["THANHTIEN"].ToString() + ")";
                    }
                    if (dttonkhocha.Rows.Count > 0)
                    {
                        sql_nhap += "\n update TONKHO set SOLUONGNHAP = 0, TRIGIANHAP = 0 WHERE HANGHOA='" + dttonkhocha.Rows[0]["HANGHOA"].ToString() + "' AND KHO='" + dr["KHO"].ToString() + "'";
                    }
                    else
                    {
                        sql_nhap += "\n insert into TONKHO(HANGHOA,KHO,SOLUONGNHAP,TRIGIANHAP) values('" + dr["HANGHOA"].ToString() + "','" + dr["KHO"].ToString() + "'," + 0 + "," + 0 + ")";
                    }
                }
                else
                {
                    sSQL = "";
                    sSQL += "SELECT HANGHOA FROM TONKHO" + "\n";
                    sSQL += "WHERE HANGHOA=" + clsMain.SQLString(dr["HANGHOA"].ToString()) + "\n";
                    sSQL += "AND KHO=" + clsMain.SQLString(dr["KHO"].ToString()) + "\n";
                    dttonkho = clsMain.ReturnDataTable(sSQL);
                    if (dttonkho.Rows.Count > 0)
                    {
                        sql_nhap += "\n update TONKHO set SOLUONGNHAP = SOLUONGNHAP + " + float.Parse(dtdulieu.Rows[0]["CTPK_SOLUONG"].ToString()) + ", TRIGIANHAP=TRIGIANHAP + " + float.Parse(dtdulieu.Rows[0]["THANHTIEN"].ToString()) + " WHERE HANGHOA='" + dr["HANGHOA"].ToString() + "' AND KHO='" + dr["KHO"].ToString() + "'";
                    }
                    else
                    {
                        sql_nhap += "\n insert into TONKHO(HANGHOA,KHO,SOLUONGNHAP,TRIGIANHAP) values('" + dtdulieu.Rows[0]["HANGHOA"].ToString() + "','" + dtdulieu.Rows[0]["KHO"].ToString() + "'," + float.Parse(dtdulieu.Rows[0]["CTPK_SOLUONG"].ToString()) + "," + dtdulieu.Rows[0]["THANHTIEN"].ToString() + ")";
                    }
                }
            }
            clsMain.ExecuteSQL(sql_nhap);
            foreach (DataRow dr in dtphieunhap.Rows)
            {
                sSQL = "";
                sSQL += "SELECT MA_THANHPHAN,SOLUONG" + "\n";
                sSQL += "FROM HANGHOA" + "\n";
                sSQL += "WHERE ISNULL(MA_THANHPHAN,'') != '' AND MA_HANGHOA=" + clsMain.SQLString(dr["HANGHOA"].ToString()) + "\n";
                DataTable dthanghoathanhphan = clsMain.ReturnDataTable(sSQL);

                if (dthanghoathanhphan.Rows.Count > 0)
                {
                    sSQL = "";
                    sSQL = string.Format("EXEC SP_TINHGIABINHQUAN @HANGHOA='{0}'", dthanghoathanhphan.Rows[0]["MA_THANHPHAN"].ToString());
                    DataTable dtgiavon = clsMain.ReturnDataTable(sSQL);
                    GIAVON = float.Parse(dtgiavon.Rows[0][0].ToString());

                    sql_ton += "\n update TONKHO set SOLUONGTONCUOIKY = SOLUONGTONDAUKY + SOLUONGNHAP - SOLUONGXUAT, TRIGIATONCUOIKY = TRIGIATONDAUKY + TRIGIANHAP - TRIGIAXUAT ";
                    sql_ton += "WHERE HANGHOA='" + dthanghoathanhphan.Rows[0]["MA_THANHPHAN"].ToString() + "' AND KHO='" + dr["KHO"].ToString() + "'";
                    sql_ton += "\n update TONKHO set GIAVON = " + GIAVON + " WHERE HANGHOA='" + dthanghoathanhphan.Rows[0]["MA_THANHPHAN"].ToString() + "'";
                    sql_ton += "\n update TONKHO set GIAVON = " + GIAVON + " WHERE HANGHOA='" + dr["HANGHOA"].ToString() + "'";
                }
                else
                {
                    sSQL = "";
                    sSQL = string.Format("EXEC SP_TINHGIABINHQUAN @HANGHOA='{0}'", dr["HANGHOA"].ToString());
                    DataTable dtgiavon = clsMain.ReturnDataTable(sSQL);
                    GIAVON = float.Parse(dtgiavon.Rows[0][0].ToString());
                    sql_ton += "\n update TONKHO set SOLUONGTONCUOIKY = SOLUONGTONDAUKY + SOLUONGNHAP - SOLUONGXUAT, TRIGIATONCUOIKY = TRIGIATONDAUKY + TRIGIANHAP - TRIGIAXUAT ";
                    sql_ton += "WHERE HANGHOA='" + dr["HANGHOA"].ToString() + "' AND KHO='" + dr["KHO"].ToString() + "'";
                    sql_ton += "\n update TONKHO set GIAVON = " + GIAVON + " WHERE HANGHOA='" + dr["HANGHOA"].ToString() + "'";

                    DataTable dtcha = clsMain.ReturnDataTable("select MA_HANGHOA from HANGHOA where MA_THANHPHAN='" + dr["HANGHOA"].ToString() + "'");
                    if (dtcha.Rows.Count > 0)
                    {
                        sql_ton += "\n update TONKHO set GIAVON = " + GIAVON + " WHERE HANGHOA='" + dtcha.Rows[0]["MA_HANGHOA"].ToString() + "'";
                    }
                }
            }
            sql_ton += "\n";
            clsMain.ExecuteSQL(sql_ton);
        }

        private void btn_luutam_Click(object sender, EventArgs e)
        {
            try
            {
                checkinputbeforeLuu();
                if (XtraMessageBox.Show("Bạn muốn lưu tạm phiếu nhập này?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.No)
                {
                    saveDHtmp();
                    btnThem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveDHtmp()
        {
            try
            {
                DataTable dttmphh = (DataTable)BS.DataSource;
                string sql = string.Format("EXEC TMP_DSNCC_INSERT @tmp_LOAI='{0}',@tmp_NHACUNGCAP='{1}',@tmp_KHO='{2}',@tmp_NHANVIEN=N'{3}',@tmp_GHICHU=N'{4}',@tmp_PT_MA='{5}',@tmp_NGAYCHUNGTU='{6}'",
                    1, cboNCC.EditValue.ToString(), cboKho.EditValue.ToString(), txtNVnhap.Tag.ToString(), txtGHICHU.Text.Trim(), cboPT.EditValue.ToString(), dtpNgay.Text);
                clsMain.ExecuteSQL(sql);
                DataTable dttmp = clsMain.ReturnDataTable("SELECT top 1 tmp_ID from TMP_DSNCC order by tmp_ID desc");
                int vtmpmancc = Convert.ToInt32(dttmp.Rows[0][0]);
                if (gridView1.RowCount > 0)
                {
                    for (int i = 0; i < dttmphh.Rows.Count; i++)
                    {
                        if (dttmphh.Rows[i]["MA"].ToString() != "" & dttmphh.Rows[i]["MA"]!=DBNull.Value)
                        {
                            sql = string.Format("EXEC TMP_DSHH_INSERT @tmp_ID='{0}',@tmp_HANGHOA='{1}',@tmp_SOLUONG='{2}',@tmp_DONGIA='{3}',@tmp_THUE='{4}',@tmp_CHIETKHAU='{5}',@tmp_THANHTIEN='{6}',@tmp_TIENTHUE='{7}',@tmp_TIENCHIETKHAU='{8}', @tmp_TONGCONG='{9}',@tmp_DVT=N'{10}',@tmp_GIAVON='{11}',@tmp_MAVACH='{12}'",
                                vtmpmancc, dttmphh.Rows[i]["MA"], dttmphh.Rows[i]["SL"], dttmphh.Rows[i]["GIANHAP"], dttmphh.Rows[i]["VAT"], dttmphh.Rows[i]["CK"], dttmphh.Rows[i]["THANHTIEN"], dttmphh.Rows[i]["TIENVAT"], dttmphh.Rows[i]["TIENCK"], dttmphh.Rows[i]["TONGCONG"], dttmphh.Rows[i]["DVT"], dttmphh.Rows[i]["GIAVON"], dttmphh.Rows[i]["MA_VACH"]);
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
                XtraMessageBox.Show("Lưu Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnLayphieutam_Click(object sender, EventArgs e)
        {
            try
            {
                Frm_TMP_DonhangNhap frm = new Frm_TMP_DonhangNhap(txtNVnhap.Tag.ToString());
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();

                KhoaMoControl(true);

                txtGHICHU.Text =tmp_NOTE==null ? "": tmp_NOTE;
                cboNCC.EditValue = tmp_MANCC == null ? cboNCC.EditValue: Convert.ToInt32(tmp_MANCC);
                cboKho.EditValue = tmp_MAKHONHAP == null ? cboKho.EditValue : Convert.ToInt32(tmp_MAKHONHAP);
                cboPT.EditValue = tmp_PT_MAPHUONGTHUC == null ? cboPT.EditValue : Convert.ToInt32(tmp_PT_MAPHUONGTHUC);
                dtpNgay.EditValue = tmp_Ngayct == null ? dtpNgay.EditValue : tmp_Ngayct;
                txtSophieu.Text = tmp_MAPHIEU==null ? "":tmp_MAPHIEU;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void chkCONGNO_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCONGNO.Checked)
            {
                KhoaMoControlCN(true);
            }
            else
            {
                if (gridView1.RowCount > 0)
                {
                    double tongcong = 0;
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        if (gridView1.GetRowCellValue(i, TONGCONG).ToString() != "")
                            tongcong += double.Parse(gridView1.GetRowCellValue(i, TONGCONG).ToString());
                    }
                    txtTienDaTra.Text = tongcong.ToString();
                    txtTienConLai.Text = "0";
                }
                dtpNgayHenTra.EditValue = DateTime.Now;
                KhoaMoControlCN(false);
            }
        }

        private void txtTienDaTra_TextChanged(object sender, EventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                double tongcong = 0;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    if (gridView1.GetRowCellValue(i, TONGCONG).ToString() != "")
                        tongcong += double.Parse(gridView1.GetRowCellValue(i, TONGCONG).ToString());
                }
                txtTienConLai.Text = (tongcong - double.Parse(txtTienDaTra.Text)).ToString();
            }
        }

        private void TinhTienCongNo()
        {
            if (gridView1.RowCount > 0)
            {
                double tongcong = 0;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    if (gridView1.GetRowCellValue(i, TONGCONG).ToString() != "")
                        tongcong += double.Parse(gridView1.GetRowCellValue(i, TONGCONG).ToString());
                }
                txtTienDaTra.Text = (tongcong - double.Parse(txtTienConLai.Text)).ToString();
            }
            else
            {
                txtTienDaTra.Text = "0";
                txtTienConLai.Text = (0 - double.Parse(txtTienDaTra.Text)).ToString();
            }
        }

        private void btnLayPhieuNhap_Click(object sender, EventArgs e)
        {
            try
            {
                Frm_TMP_NhapKho frm = new Frm_TMP_NhapKho(txtNVnhap.Tag.ToString());
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();

                KhoaMoControl(true);

                txtGHICHU.Text = tmp_NOTE == null ? "" : tmp_NOTE;
                cboNCC.EditValue = tmp_MANCC == null ? cboNCC.EditValue : Convert.ToInt32(tmp_MANCC);
                cboKho.EditValue = tmp_MAKHONHAP == null ? cboKho.EditValue : Convert.ToInt32(tmp_MAKHONHAP);
                cboPT.EditValue = tmp_PT_MAPHUONGTHUC == null ? cboPT.EditValue : Convert.ToInt32(tmp_PT_MAPHUONGTHUC);
                dtpNgay.EditValue = tmp_Ngayct == null ? dtpNgay.EditValue : tmp_Ngayct;
                txtSophieu.Text = tmp_MAPHIEU == null ? "" : tmp_MAPHIEU;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //21/08 khang thêm button lấy phiếu xuất
        private void btnLayPhieuXuat_Click(object sender, EventArgs e)
        {
            try
            {
                Frm_TMP_XuatKho frm = new Frm_TMP_XuatKho(txtNVnhap.Tag.ToString());
                frm.Mode = 1; // truyền dữ liệu qua form Frm_TMP_XuatKho
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();
                KhoaMoControl(true);
                flag = true;

                txtGHICHU.Text = tmp_NOTE == null ? "" : tmp_NOTE;
                cboNCC.EditValue = tmp_MANCC == null ? cboNCC.EditValue : Convert.ToInt32(tmp_MANCC);
                cboKho.EditValue = tmp_MAKHONHAP == null ? cboKho.EditValue : Convert.ToInt32(tmp_MAKHONHAP);
                cboPT.EditValue = tmp_PT_MAPHUONGTHUC == null ? cboPT.EditValue : Convert.ToInt32(tmp_PT_MAPHUONGTHUC);
                dtpNgay.EditValue = tmp_Ngayct == null ? dtpNgay.EditValue : tmp_Ngayct;
                txtSophieu.Text = tmp_MAPHIEU == null ? "" : tmp_MAPHIEU;
                flag = false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnFile_Mo_Click(object sender, EventArgs e)
        {
            string filename = Application.StartupPath + "\\FileNhapKho.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
                System.Diagnostics.Process.Start(filename);
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sOpenFile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel._Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(1);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                DataTable dttmp = (DataTable)Frm_Nhapkho.BS.DataSource;

                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() == "")
                    dttmp.Rows.Remove(dttmp.Rows[dttmp.Rows.Count - 1]);

                for (int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    try
                    {
                        String sMaVach = (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 : "";
                        String sTenHangHoa = (String)(xlRange.Cells[i, 2] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 2] as Excel.Range).Value2.ToString() : "";
                        String sSoLuong = (String)(xlRange.Cells[i, 3] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 3] as Excel.Range).Value2.ToString() : "0";
                        String sGiaNhap = (String)(xlRange.Cells[i, 4] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 4] as Excel.Range).Value2.ToString() : "0";
                        String sChietKhau = (String)(xlRange.Cells[i, 5] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 5] as Excel.Range).Value2.ToString() : "0";
                        String sVAT = (String)(xlRange.Cells[i, 6] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 6] as Excel.Range).Value2.ToString() : "0";

                        float sTHANHTIENTCK = float.Parse(sSoLuong) * float.Parse(sGiaNhap);
                        float sTIENCK = ((float.Parse(sGiaNhap) * float.Parse(sChietKhau)) / 100) * float.Parse(sSoLuong);
                        float sGIAVON = float.Parse(sGiaNhap) - ((float.Parse(sGiaNhap) * float.Parse(sChietKhau)) / 100);
                        float sTIENVAT = 0;
                        float sTONGCONG = 0;
                        float sTHANHTIEN = sGIAVON * float.Parse(sSoLuong);

                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            sTIENVAT = ((sGIAVON * float.Parse(sVAT)) / 100) * float.Parse(sSoLuong);
                        else
                            sTIENVAT = (sGIAVON - sGIAVON / (1 + float.Parse(sVAT) / 100)) * float.Parse(sSoLuong);

                        if (bGIANHAPTRUOCTHUE)//Giá nhập : True-Trước thuế : False-Sau thuế
                            sTONGCONG = sGIAVON * float.Parse(sSoLuong) + sTIENVAT;
                        else
                            sTONGCONG = sGIAVON * float.Parse(sSoLuong);

                        String sSQL = "";
                        sSQL += "SELECT MA_HANGHOA,TEN_HANGHOA,TEN_DONVITINH ";
                        sSQL += "FROM HANGHOA A ";
                        sSQL += "INNER JOIN DONVITINH B ON A.MA_DONVITINH = B.MA_DONVITINH  ";
                        sSQL += "WHERE TONKHO = 1 AND MAVACH = " + clsMain.SQLString(sMaVach);
                        DataTable myDT = clsMain.ReturnDataTable(sSQL);

                        String sDonViTinh = myDT.Rows[0]["TEN_DONVITINH"].ToString();
                        sTenHangHoa = myDT.Rows[0]["TEN_HANGHOA"].ToString();
                        String sMaHangHoa = myDT.Rows[0]["MA_HANGHOA"].ToString();

                        dttmp.Rows.Add(sMaHangHoa, sMaVach, sTenHangHoa, sDonViTinh, sSoLuong, sGiaNhap, sTHANHTIENTCK, sChietKhau, sTIENCK, sGIAVON, sTHANHTIEN, sVAT, sTIENVAT, sTONGCONG);
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                    }
                }

                xlWorkbook.Close(true, null, null);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorksheet);
                Marshal.ReleaseComObject(xlWorkbook);
                Marshal.ReleaseComObject(xlApp);

                dttmp.Rows.Add(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                this.Cursor = Cursors.Default;
            }
        }

        public static String Filter()
        {
            String Filter = "";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }
    }
}