﻿namespace KP_RES 
{
    partial class Frm_Thekho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Thekho));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.cboHH = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cboNhom = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.cboKho = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cboCuahang = new DevExpress.XtraEditors.LookUpEdit();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIAVON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLTONDAUKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGIATONDAUKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLNHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGIANHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLXUAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGIAXUAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLTONCUOIKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGIATONCUOIKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.TabOption = new DevExpress.XtraTab.XtraTabControl();
            this.Tab_Phieunhap = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTPN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIENTCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENCHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Tab_Phieuxuat = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTPX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Tab_Hoadonbanhang = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnPhieuxuat = new DevExpress.XtraEditors.SimpleButton();
            this.btnPhieuNhap = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoadon = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabOption)).BeginInit();
            this.TabOption.SuspendLayout();
            this.Tab_Phieunhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.Tab_Phieuxuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.Tab_Hoadonbanhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(219, 615);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.btnTimkiem);
            this.panelControl5.Controls.Add(this.cboHH);
            this.panelControl5.Controls.Add(this.labelControl5);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.cboNhom);
            this.panelControl5.Controls.Add(this.lblTEN);
            this.panelControl5.Controls.Add(this.labelControl3);
            this.panelControl5.Controls.Add(this.dtpDenngay);
            this.panelControl5.Controls.Add(this.cboKho);
            this.panelControl5.Controls.Add(this.dtpTungay);
            this.panelControl5.Controls.Add(this.labelControl1);
            this.panelControl5.Controls.Add(this.labelControl2);
            this.panelControl5.Controls.Add(this.cboCuahang);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(215, 576);
            this.panelControl5.TabIndex = 0;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(131, 360);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 21;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // cboHH
            // 
            this.cboHH.EnterMoveNextControl = true;
            this.cboHH.Location = new System.Drawing.Point(6, 327);
            this.cboHH.Name = "cboHH";
            this.cboHH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHH.Properties.Appearance.Options.UseFont = true;
            this.cboHH.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHH.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboHH.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboHH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboHH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên kho")});
            this.cboHH.Properties.DisplayMember = "TEN";
            this.cboHH.Properties.DropDownItemHeight = 25;
            this.cboHH.Properties.DropDownRows = 30;
            this.cboHH.Properties.NullText = "";
            this.cboHH.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboHH.Properties.ShowHeader = false;
            this.cboHH.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboHH.Properties.ValueMember = "MA";
            this.cboHH.Size = new System.Drawing.Size(205, 26);
            this.cboHH.TabIndex = 20;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(6, 301);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 19);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Hàng hóa";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 6);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(59, 19);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Từ ngày";
            // 
            // cboNhom
            // 
            this.cboNhom.EnterMoveNextControl = true;
            this.cboNhom.Location = new System.Drawing.Point(6, 268);
            this.cboNhom.Name = "cboNhom";
            this.cboNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhom.Properties.Appearance.Options.UseFont = true;
            this.cboNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhom.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhom.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên kho")});
            this.cboNhom.Properties.DisplayMember = "TEN";
            this.cboNhom.Properties.DropDownItemHeight = 25;
            this.cboNhom.Properties.DropDownRows = 30;
            this.cboNhom.Properties.NullText = "";
            this.cboNhom.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhom.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboNhom.Properties.ShowHeader = false;
            this.cboNhom.Properties.ValueMember = "MA";
            this.cboNhom.Size = new System.Drawing.Size(205, 26);
            this.cboNhom.TabIndex = 18;
            this.cboNhom.EditValueChanged += new System.EventHandler(this.cboNhom_EditValueChanged);
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 124);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(68, 19);
            this.lblTEN.TabIndex = 13;
            this.lblTEN.Text = "Cửa hàng";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(6, 242);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(83, 19);
            this.labelControl3.TabIndex = 17;
            this.labelControl3.Text = "Nhóm hàng";
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(6, 91);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(205, 26);
            this.dtpDenngay.TabIndex = 3;
            // 
            // cboKho
            // 
            this.cboKho.EnterMoveNextControl = true;
            this.cboKho.Location = new System.Drawing.Point(6, 209);
            this.cboKho.Name = "cboKho";
            this.cboKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.Appearance.Options.UseFont = true;
            this.cboKho.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKho.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên kho")});
            this.cboKho.Properties.DisplayMember = "TEN";
            this.cboKho.Properties.DropDownItemHeight = 25;
            this.cboKho.Properties.DropDownRows = 30;
            this.cboKho.Properties.NullText = "";
            this.cboKho.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKho.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboKho.Properties.ShowHeader = false;
            this.cboKho.Properties.ValueMember = "MA";
            this.cboKho.Size = new System.Drawing.Size(205, 26);
            this.cboKho.TabIndex = 16;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(6, 32);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(205, 26);
            this.dtpTungay.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 65);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Đến ngày";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(6, 183);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 19);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Kho hàng";
            // 
            // cboCuahang
            // 
            this.cboCuahang.EnterMoveNextControl = true;
            this.cboCuahang.Location = new System.Drawing.Point(6, 150);
            this.cboCuahang.Name = "cboCuahang";
            this.cboCuahang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.Appearance.Options.UseFont = true;
            this.cboCuahang.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboCuahang.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboCuahang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCuahang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCuahang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "Tên")});
            this.cboCuahang.Properties.DisplayMember = "TEN";
            this.cboCuahang.Properties.DropDownItemHeight = 25;
            this.cboCuahang.Properties.DropDownRows = 30;
            this.cboCuahang.Properties.NullText = "";
            this.cboCuahang.Properties.ShowHeader = false;
            this.cboCuahang.Properties.ValueMember = "MA";
            this.cboCuahang.Size = new System.Drawing.Size(205, 26);
            this.cboCuahang.TabIndex = 14;
            this.cboCuahang.EditValueChanged += new System.EventHandler(this.cboCuahang_EditValueChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 578);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(215, 35);
            this.btnThunho.TabIndex = 13;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 13;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(937, 126);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 50;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_HANGHOA,
            this.TEN_HANGHOA,
            this.DVT,
            this.GIAVON,
            this.SLTONDAUKY,
            this.TRIGIATONDAUKY,
            this.SLNHAP,
            this.TRIGIANHAP,
            this.SLXUAT,
            this.TRIGIAXUAT,
            this.SLTONCUOIKY,
            this.TRIGIATONCUOIKY});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AllowFindPanel = false;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 50;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.MA_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HANGHOA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.MA_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_HANGHOA.Caption = "Mã";
            this.MA_HANGHOA.DisplayFormat.FormatString = "000";
            this.MA_HANGHOA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            this.MA_HANGHOA.OptionsColumn.FixedWidth = true;
            this.MA_HANGHOA.Visible = true;
            this.MA_HANGHOA.VisibleIndex = 1;
            this.MA_HANGHOA.Width = 50;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 2;
            this.TEN_HANGHOA.Width = 200;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceCell.Options.UseTextOptions = true;
            this.DVT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 3;
            this.DVT.Width = 60;
            // 
            // GIAVON
            // 
            this.GIAVON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIAVON.AppearanceCell.Options.UseFont = true;
            this.GIAVON.AppearanceCell.Options.UseTextOptions = true;
            this.GIAVON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIAVON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIAVON.AppearanceHeader.Options.UseFont = true;
            this.GIAVON.AppearanceHeader.Options.UseTextOptions = true;
            this.GIAVON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIAVON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIAVON.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIAVON.Caption = "Đơn giá";
            this.GIAVON.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIAVON.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIAVON.FieldName = "GIAVON";
            this.GIAVON.Name = "GIAVON";
            this.GIAVON.OptionsColumn.AllowEdit = false;
            this.GIAVON.OptionsColumn.AllowFocus = false;
            this.GIAVON.OptionsColumn.FixedWidth = true;
            this.GIAVON.Visible = true;
            this.GIAVON.VisibleIndex = 4;
            this.GIAVON.Width = 80;
            // 
            // SLTONDAUKY
            // 
            this.SLTONDAUKY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SLTONDAUKY.AppearanceCell.Options.UseFont = true;
            this.SLTONDAUKY.AppearanceCell.Options.UseTextOptions = true;
            this.SLTONDAUKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SLTONDAUKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SLTONDAUKY.AppearanceHeader.Options.UseFont = true;
            this.SLTONDAUKY.AppearanceHeader.Options.UseTextOptions = true;
            this.SLTONDAUKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLTONDAUKY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SLTONDAUKY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SLTONDAUKY.Caption = "Số lượng tồn đầu kỳ";
            this.SLTONDAUKY.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SLTONDAUKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SLTONDAUKY.FieldName = "SLTONDAUKY";
            this.SLTONDAUKY.Name = "SLTONDAUKY";
            this.SLTONDAUKY.OptionsColumn.AllowEdit = false;
            this.SLTONDAUKY.OptionsColumn.AllowFocus = false;
            this.SLTONDAUKY.OptionsColumn.FixedWidth = true;
            this.SLTONDAUKY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SLTONDAUKY", "{0:#,###0.00}")});
            this.SLTONDAUKY.Visible = true;
            this.SLTONDAUKY.VisibleIndex = 5;
            this.SLTONDAUKY.Width = 100;
            // 
            // TRIGIATONDAUKY
            // 
            this.TRIGIATONDAUKY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRIGIATONDAUKY.AppearanceCell.Options.UseFont = true;
            this.TRIGIATONDAUKY.AppearanceCell.Options.UseTextOptions = true;
            this.TRIGIATONDAUKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TRIGIATONDAUKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRIGIATONDAUKY.AppearanceHeader.Options.UseFont = true;
            this.TRIGIATONDAUKY.AppearanceHeader.Options.UseTextOptions = true;
            this.TRIGIATONDAUKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRIGIATONDAUKY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRIGIATONDAUKY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TRIGIATONDAUKY.Caption = "Trị giá tồn đầu kỳ";
            this.TRIGIATONDAUKY.DisplayFormat.FormatString = "{0:#,###0}";
            this.TRIGIATONDAUKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TRIGIATONDAUKY.FieldName = "TRIGIATONDAUKY";
            this.TRIGIATONDAUKY.Name = "TRIGIATONDAUKY";
            this.TRIGIATONDAUKY.OptionsColumn.AllowEdit = false;
            this.TRIGIATONDAUKY.OptionsColumn.AllowFocus = false;
            this.TRIGIATONDAUKY.OptionsColumn.FixedWidth = true;
            this.TRIGIATONDAUKY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRIGIATONDAUKY", "{0:#,###0}")});
            this.TRIGIATONDAUKY.Visible = true;
            this.TRIGIATONDAUKY.VisibleIndex = 6;
            this.TRIGIATONDAUKY.Width = 150;
            // 
            // SLNHAP
            // 
            this.SLNHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SLNHAP.AppearanceCell.Options.UseFont = true;
            this.SLNHAP.AppearanceCell.Options.UseTextOptions = true;
            this.SLNHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SLNHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SLNHAP.AppearanceHeader.Options.UseFont = true;
            this.SLNHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.SLNHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLNHAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SLNHAP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SLNHAP.Caption = "Số lượng nhập";
            this.SLNHAP.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SLNHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SLNHAP.FieldName = "SLNHAP";
            this.SLNHAP.Name = "SLNHAP";
            this.SLNHAP.OptionsColumn.AllowEdit = false;
            this.SLNHAP.OptionsColumn.AllowFocus = false;
            this.SLNHAP.OptionsColumn.FixedWidth = true;
            this.SLNHAP.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SLNHAP", "{0:#,###0.00}")});
            this.SLNHAP.Visible = true;
            this.SLNHAP.VisibleIndex = 7;
            this.SLNHAP.Width = 88;
            // 
            // TRIGIANHAP
            // 
            this.TRIGIANHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRIGIANHAP.AppearanceCell.Options.UseFont = true;
            this.TRIGIANHAP.AppearanceCell.Options.UseTextOptions = true;
            this.TRIGIANHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TRIGIANHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRIGIANHAP.AppearanceHeader.Options.UseFont = true;
            this.TRIGIANHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.TRIGIANHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRIGIANHAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRIGIANHAP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TRIGIANHAP.Caption = "Trị giá nhập";
            this.TRIGIANHAP.DisplayFormat.FormatString = "{0:#,###0}";
            this.TRIGIANHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TRIGIANHAP.FieldName = "TRIGIANHAP";
            this.TRIGIANHAP.Name = "TRIGIANHAP";
            this.TRIGIANHAP.OptionsColumn.AllowEdit = false;
            this.TRIGIANHAP.OptionsColumn.AllowFocus = false;
            this.TRIGIANHAP.OptionsColumn.FixedWidth = true;
            this.TRIGIANHAP.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRIGIANHAP", "{0:#,###0}")});
            this.TRIGIANHAP.Visible = true;
            this.TRIGIANHAP.VisibleIndex = 8;
            this.TRIGIANHAP.Width = 110;
            // 
            // SLXUAT
            // 
            this.SLXUAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SLXUAT.AppearanceCell.Options.UseFont = true;
            this.SLXUAT.AppearanceCell.Options.UseTextOptions = true;
            this.SLXUAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SLXUAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SLXUAT.AppearanceHeader.Options.UseFont = true;
            this.SLXUAT.AppearanceHeader.Options.UseTextOptions = true;
            this.SLXUAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLXUAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SLXUAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SLXUAT.Caption = "Số lượng xuất";
            this.SLXUAT.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SLXUAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SLXUAT.FieldName = "SLXUAT";
            this.SLXUAT.Name = "SLXUAT";
            this.SLXUAT.OptionsColumn.AllowEdit = false;
            this.SLXUAT.OptionsColumn.AllowFocus = false;
            this.SLXUAT.OptionsColumn.FixedWidth = true;
            this.SLXUAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SLXUAT", "{0:#,###0.00}")});
            this.SLXUAT.Visible = true;
            this.SLXUAT.VisibleIndex = 9;
            this.SLXUAT.Width = 91;
            // 
            // TRIGIAXUAT
            // 
            this.TRIGIAXUAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRIGIAXUAT.AppearanceCell.Options.UseFont = true;
            this.TRIGIAXUAT.AppearanceCell.Options.UseTextOptions = true;
            this.TRIGIAXUAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TRIGIAXUAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRIGIAXUAT.AppearanceHeader.Options.UseFont = true;
            this.TRIGIAXUAT.AppearanceHeader.Options.UseTextOptions = true;
            this.TRIGIAXUAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRIGIAXUAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRIGIAXUAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TRIGIAXUAT.Caption = "Trị giá xuất";
            this.TRIGIAXUAT.DisplayFormat.FormatString = "{0:#,###0}";
            this.TRIGIAXUAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TRIGIAXUAT.FieldName = "TRIGIAXUAT";
            this.TRIGIAXUAT.Name = "TRIGIAXUAT";
            this.TRIGIAXUAT.OptionsColumn.AllowEdit = false;
            this.TRIGIAXUAT.OptionsColumn.AllowFocus = false;
            this.TRIGIAXUAT.OptionsColumn.FixedWidth = true;
            this.TRIGIAXUAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRIGIAXUAT", "{0:#,###0}")});
            this.TRIGIAXUAT.Visible = true;
            this.TRIGIAXUAT.VisibleIndex = 10;
            this.TRIGIAXUAT.Width = 110;
            // 
            // SLTONCUOIKY
            // 
            this.SLTONCUOIKY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SLTONCUOIKY.AppearanceCell.Options.UseFont = true;
            this.SLTONCUOIKY.AppearanceCell.Options.UseTextOptions = true;
            this.SLTONCUOIKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SLTONCUOIKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SLTONCUOIKY.AppearanceHeader.Options.UseFont = true;
            this.SLTONCUOIKY.AppearanceHeader.Options.UseTextOptions = true;
            this.SLTONCUOIKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLTONCUOIKY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SLTONCUOIKY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SLTONCUOIKY.Caption = "Số lượng tồn cuối kỳ";
            this.SLTONCUOIKY.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SLTONCUOIKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SLTONCUOIKY.FieldName = "SLTONCUOIKY";
            this.SLTONCUOIKY.Name = "SLTONCUOIKY";
            this.SLTONCUOIKY.OptionsColumn.AllowEdit = false;
            this.SLTONCUOIKY.OptionsColumn.AllowFocus = false;
            this.SLTONCUOIKY.OptionsColumn.FixedWidth = true;
            this.SLTONCUOIKY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SLTONCUOIKY", "{0:#,###0.00}")});
            this.SLTONCUOIKY.Visible = true;
            this.SLTONCUOIKY.VisibleIndex = 11;
            this.SLTONCUOIKY.Width = 100;
            // 
            // TRIGIATONCUOIKY
            // 
            this.TRIGIATONCUOIKY.AppearanceCell.Options.UseTextOptions = true;
            this.TRIGIATONCUOIKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TRIGIATONCUOIKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRIGIATONCUOIKY.AppearanceHeader.Options.UseFont = true;
            this.TRIGIATONCUOIKY.AppearanceHeader.Options.UseTextOptions = true;
            this.TRIGIATONCUOIKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRIGIATONCUOIKY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TRIGIATONCUOIKY.Caption = "Trị giá tồn cuối kỳ";
            this.TRIGIATONCUOIKY.DisplayFormat.FormatString = "{0:#,###0}";
            this.TRIGIATONCUOIKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TRIGIATONCUOIKY.FieldName = "TRIGIATONCUOIKY";
            this.TRIGIATONCUOIKY.MinWidth = 100;
            this.TRIGIATONCUOIKY.Name = "TRIGIATONCUOIKY";
            this.TRIGIATONCUOIKY.OptionsColumn.AllowEdit = false;
            this.TRIGIATONCUOIKY.OptionsColumn.AllowFocus = false;
            this.TRIGIATONCUOIKY.OptionsColumn.AllowMove = false;
            this.TRIGIATONCUOIKY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRIGIATONCUOIKY", "{0:#,###0}")});
            this.TRIGIATONCUOIKY.Visible = true;
            this.TRIGIATONCUOIKY.VisibleIndex = 12;
            this.TRIGIATONCUOIKY.Width = 100;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1164, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 615);
            this.panelControl2.TabIndex = 5;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 291);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 131);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 211);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 533);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 162);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.panelControl7);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(219, 0);
            this.panelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(945, 615);
            this.panelControl6.TabIndex = 15;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl11);
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 132);
            this.panelControl8.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(941, 481);
            this.panelControl8.TabIndex = 18;
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.TabOption);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl11.Location = new System.Drawing.Point(2, 42);
            this.panelControl11.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(937, 437);
            this.panelControl11.TabIndex = 20;
            // 
            // TabOption
            // 
            this.TabOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabOption.Location = new System.Drawing.Point(2, 2);
            this.TabOption.Name = "TabOption";
            this.TabOption.SelectedTabPage = this.Tab_Phieunhap;
            this.TabOption.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.TabOption.Size = new System.Drawing.Size(933, 433);
            this.TabOption.TabIndex = 0;
            this.TabOption.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tab_Phieunhap,
            this.Tab_Phieuxuat,
            this.Tab_Hoadonbanhang});
            // 
            // Tab_Phieunhap
            // 
            this.Tab_Phieunhap.Controls.Add(this.gridControl1);
            this.Tab_Phieunhap.Name = "Tab_Phieunhap";
            this.Tab_Phieunhap.Size = new System.Drawing.Size(927, 427);
            this.Tab_Phieunhap.Text = "xtraTabPage1";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(927, 427);
            this.gridControl1.TabIndex = 19;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTPN,
            this.SOPHIEU,
            this.KHO,
            this.SOLUONG,
            this.DONGIA,
            this.THANHTIENTCK,
            this.NGAYTAO,
            this.CHIETKHAU,
            this.TIENCHIETKHAU,
            this.TENNHANVIEN,
            this.MA,
            this.THANHTIEN,
            this.THUE,
            this.TIENTHUE,
            this.TONGCONG});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STTPN
            // 
            this.STTPN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STTPN.AppearanceCell.Options.UseFont = true;
            this.STTPN.AppearanceCell.Options.UseTextOptions = true;
            this.STTPN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTPN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STTPN.AppearanceHeader.Options.UseFont = true;
            this.STTPN.AppearanceHeader.Options.UseTextOptions = true;
            this.STTPN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTPN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STTPN.Caption = "STT";
            this.STTPN.FieldName = "STT";
            this.STTPN.Name = "STTPN";
            this.STTPN.OptionsColumn.AllowEdit = false;
            this.STTPN.OptionsColumn.AllowFocus = false;
            this.STTPN.OptionsColumn.FixedWidth = true;
            this.STTPN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STTPN.Visible = true;
            this.STTPN.VisibleIndex = 0;
            this.STTPN.Width = 50;
            // 
            // SOPHIEU
            // 
            this.SOPHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOPHIEU.AppearanceCell.Options.UseFont = true;
            this.SOPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOPHIEU.AppearanceHeader.Options.UseFont = true;
            this.SOPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHIEU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOPHIEU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOPHIEU.Caption = "Mã phiếu";
            this.SOPHIEU.FieldName = "SOPHIEU";
            this.SOPHIEU.Name = "SOPHIEU";
            this.SOPHIEU.OptionsColumn.AllowEdit = false;
            this.SOPHIEU.OptionsColumn.AllowFocus = false;
            this.SOPHIEU.OptionsColumn.FixedWidth = true;
            this.SOPHIEU.Visible = true;
            this.SOPHIEU.VisibleIndex = 1;
            this.SOPHIEU.Width = 100;
            // 
            // KHO
            // 
            this.KHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.KHO.AppearanceCell.Options.UseFont = true;
            this.KHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KHO.AppearanceHeader.Options.UseFont = true;
            this.KHO.AppearanceHeader.Options.UseTextOptions = true;
            this.KHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KHO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.KHO.Caption = "Kho nhập";
            this.KHO.FieldName = "KHO";
            this.KHO.Name = "KHO";
            this.KHO.OptionsColumn.AllowEdit = false;
            this.KHO.OptionsColumn.AllowFocus = false;
            this.KHO.OptionsColumn.FixedWidth = true;
            this.KHO.Visible = true;
            this.KHO.VisibleIndex = 4;
            this.KHO.Width = 150;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOLUONG", "{0:#,###0.00}")});
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 5;
            this.SOLUONG.Width = 86;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DONGIA.AppearanceCell.Options.UseFont = true;
            this.DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DONGIA.Caption = "Giá nhập";
            this.DONGIA.DisplayFormat.FormatString = "{0:#,###0}";
            this.DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowEdit = false;
            this.DONGIA.OptionsColumn.AllowFocus = false;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 6;
            this.DONGIA.Width = 150;
            // 
            // THANHTIENTCK
            // 
            this.THANHTIENTCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIENTCK.AppearanceCell.Options.UseFont = true;
            this.THANHTIENTCK.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIENTCK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIENTCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIENTCK.AppearanceHeader.Options.UseFont = true;
            this.THANHTIENTCK.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIENTCK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTIENTCK.Caption = "Thành tiền TCK";
            this.THANHTIENTCK.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIENTCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIENTCK.FieldName = "THANHTIENTCK";
            this.THANHTIENTCK.Name = "THANHTIENTCK";
            this.THANHTIENTCK.OptionsColumn.AllowEdit = false;
            this.THANHTIENTCK.OptionsColumn.AllowFocus = false;
            this.THANHTIENTCK.OptionsColumn.FixedWidth = true;
            this.THANHTIENTCK.Visible = true;
            this.THANHTIENTCK.VisibleIndex = 7;
            this.THANHTIENTCK.Width = 124;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYTAO.AppearanceCell.Options.UseFont = true;
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYTAO.Caption = "Ngày tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 3;
            this.NGAYTAO.Width = 100;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "CK";
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 8;
            this.CHIETKHAU.Width = 53;
            // 
            // TIENCHIETKHAU
            // 
            this.TIENCHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENCHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.TIENCHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENCHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.TIENCHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENCHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENCHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENCHIETKHAU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TIENCHIETKHAU.Caption = "Tiền CK";
            this.TIENCHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENCHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENCHIETKHAU.FieldName = "TIENCHIETKHAU";
            this.TIENCHIETKHAU.Name = "TIENCHIETKHAU";
            this.TIENCHIETKHAU.OptionsColumn.AllowEdit = false;
            this.TIENCHIETKHAU.OptionsColumn.AllowFocus = false;
            this.TIENCHIETKHAU.OptionsColumn.FixedWidth = true;
            this.TIENCHIETKHAU.Visible = true;
            this.TIENCHIETKHAU.VisibleIndex = 9;
            this.TIENCHIETKHAU.Width = 110;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENNHANVIEN.Caption = "Người tạo";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 2;
            this.TENNHANVIEN.Width = 150;
            // 
            // MA
            // 
            this.MA.Caption = "MA";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.Caption = "Thành tiền SCK";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 10;
            this.THANHTIEN.Width = 142;
            // 
            // THUE
            // 
            this.THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THUE.AppearanceCell.Options.UseFont = true;
            this.THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUE.AppearanceHeader.Options.UseFont = true;
            this.THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.Caption = "VAT";
            this.THUE.DisplayFormat.FormatString = "{0:#,###0}%";
            this.THUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            this.THUE.OptionsColumn.AllowEdit = false;
            this.THUE.OptionsColumn.AllowFocus = false;
            this.THUE.Visible = true;
            this.THUE.VisibleIndex = 11;
            // 
            // TIENTHUE
            // 
            this.TIENTHUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENTHUE.AppearanceCell.Options.UseFont = true;
            this.TIENTHUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENTHUE.AppearanceHeader.Options.UseFont = true;
            this.TIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENTHUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENTHUE.Caption = "Tiền VAT";
            this.TIENTHUE.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENTHUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENTHUE.FieldName = "TIENTHUE";
            this.TIENTHUE.Name = "TIENTHUE";
            this.TIENTHUE.OptionsColumn.AllowEdit = false;
            this.TIENTHUE.OptionsColumn.AllowFocus = false;
            this.TIENTHUE.Visible = true;
            this.TIENTHUE.VisibleIndex = 12;
            this.TIENTHUE.Width = 103;
            // 
            // TONGCONG
            // 
            this.TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TONGCONG.AppearanceCell.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGCONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGCONG.Caption = "Tổng cộng";
            this.TONGCONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGCONG.FieldName = "TONGCONG";
            this.TONGCONG.Name = "TONGCONG";
            this.TONGCONG.OptionsColumn.AllowEdit = false;
            this.TONGCONG.OptionsColumn.AllowFocus = false;
            this.TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0}")});
            this.TONGCONG.Visible = true;
            this.TONGCONG.VisibleIndex = 13;
            this.TONGCONG.Width = 113;
            // 
            // Tab_Phieuxuat
            // 
            this.Tab_Phieuxuat.Controls.Add(this.gridControl3);
            this.Tab_Phieuxuat.Name = "Tab_Phieuxuat";
            this.Tab_Phieuxuat.Size = new System.Drawing.Size(927, 427);
            this.Tab_Phieuxuat.Text = "xtraTabPage2";
            // 
            // gridControl3
            // 
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(927, 427);
            this.gridControl3.TabIndex = 20;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 50;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTPX,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFind.ShowCloseButton = false;
            this.gridView3.OptionsPrint.AutoWidth = false;
            this.gridView3.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView3_RowCellClick);
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            // 
            // STTPX
            // 
            this.STTPX.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STTPX.AppearanceCell.Options.UseFont = true;
            this.STTPX.AppearanceCell.Options.UseTextOptions = true;
            this.STTPX.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTPX.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STTPX.AppearanceHeader.Options.UseFont = true;
            this.STTPX.AppearanceHeader.Options.UseTextOptions = true;
            this.STTPX.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTPX.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STTPX.Caption = "STT";
            this.STTPX.FieldName = "STT";
            this.STTPX.Name = "STTPX";
            this.STTPX.OptionsColumn.AllowEdit = false;
            this.STTPX.OptionsColumn.AllowFocus = false;
            this.STTPX.OptionsColumn.FixedWidth = true;
            this.STTPX.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STTPX.Visible = true;
            this.STTPX.VisibleIndex = 0;
            this.STTPX.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Mã phiếu";
            this.gridColumn2.FieldName = "SOPHIEU";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Kho xuất";
            this.gridColumn3.FieldName = "KHO";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 150;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "SL";
            this.gridColumn4.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn4.FieldName = "SOLUONG";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOLUONG", "{0:#,###0.00}")});
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 86;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Đơn giá";
            this.gridColumn5.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn5.FieldName = "DONGIA";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            this.gridColumn5.Width = 150;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Ngày tạo";
            this.gridColumn7.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn7.FieldName = "NGAYTAO";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Người tạo";
            this.gridColumn10.FieldName = "TENNHANVIEN";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 150;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "MA";
            this.gridColumn11.FieldName = "MA";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn12.AppearanceCell.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn12.Caption = "Thành tiền";
            this.gridColumn12.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn12.FieldName = "THANHTIEN";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.FixedWidth = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 7;
            this.gridColumn12.Width = 142;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn13.AppearanceCell.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn13.Caption = "VAT";
            this.gridColumn13.DisplayFormat.FormatString = "{0:#,###0}%";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn13.FieldName = "THUE";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 8;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn14.AppearanceCell.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.Caption = "Tiền VAT";
            this.gridColumn14.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn14.FieldName = "TIENTHUE";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 9;
            this.gridColumn14.Width = 103;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn15.AppearanceCell.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.Caption = "Tổng cộng";
            this.gridColumn15.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn15.FieldName = "TONGCONG";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 10;
            this.gridColumn15.Width = 113;
            // 
            // Tab_Hoadonbanhang
            // 
            this.Tab_Hoadonbanhang.Controls.Add(this.gridControl4);
            this.Tab_Hoadonbanhang.Name = "Tab_Hoadonbanhang";
            this.Tab_Hoadonbanhang.Size = new System.Drawing.Size(927, 427);
            this.Tab_Hoadonbanhang.Text = "xtraTabPage3";
            // 
            // gridControl4
            // 
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(927, 427);
            this.gridControl4.TabIndex = 21;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView4.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView4.Appearance.GroupRow.Options.UseFont = true;
            this.gridView4.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView4.ColumnPanelRowHeight = 50;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTHD,
            this.gridColumn6,
            this.gridColumn17,
            this.gridColumn8,
            this.gridColumn18,
            this.gridColumn9,
            this.FILL});
            this.gridView4.FooterPanelHeight = 30;
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFind.ShowCloseButton = false;
            this.gridView4.OptionsPrint.AutoWidth = false;
            this.gridView4.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFooter = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.RowHeight = 30;
            this.gridView4.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView4_RowCellClick);
            this.gridView4.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView4_CustomDrawCell);
            // 
            // STTHD
            // 
            this.STTHD.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STTHD.AppearanceCell.Options.UseFont = true;
            this.STTHD.AppearanceCell.Options.UseTextOptions = true;
            this.STTHD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHD.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STTHD.AppearanceHeader.Options.UseFont = true;
            this.STTHD.AppearanceHeader.Options.UseTextOptions = true;
            this.STTHD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STTHD.Caption = "STT";
            this.STTHD.FieldName = "STT";
            this.STTHD.Name = "STTHD";
            this.STTHD.OptionsColumn.AllowEdit = false;
            this.STTHD.OptionsColumn.AllowFocus = false;
            this.STTHD.OptionsColumn.FixedWidth = true;
            this.STTHD.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STTHD.Visible = true;
            this.STTHD.VisibleIndex = 0;
            this.STTHD.Width = 50;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Mã hóa đơn";
            this.gridColumn6.FieldName = "MA_HOADON";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.FixedWidth = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 179;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn17.AppearanceCell.Options.UseFont = true;
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Giờ ra bill";
            this.gridColumn17.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.gridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn17.FieldName = "GIORA";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.FixedWidth = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            this.gridColumn17.Width = 212;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Kho";
            this.gridColumn8.FieldName = "KHO";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 195;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn18.AppearanceCell.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "Nhân viên";
            this.gridColumn18.FieldName = "TENNHANVIEN";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.FixedWidth = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            this.gridColumn18.Width = 197;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "SL";
            this.gridColumn9.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn9.FieldName = "SOLUONG";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOLUONG", "{0:#,###0.00}")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            this.gridColumn9.Width = 110;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 6;
            this.FILL.Width = 150;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.btnPhieuxuat);
            this.panelControl9.Controls.Add(this.btnPhieuNhap);
            this.panelControl9.Controls.Add(this.btnHoadon);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(937, 40);
            this.panelControl9.TabIndex = 19;
            // 
            // btnPhieuxuat
            // 
            this.btnPhieuxuat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhieuxuat.Appearance.Options.UseFont = true;
            this.btnPhieuxuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPhieuxuat.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnPhieuxuat.Location = new System.Drawing.Point(245, 2);
            this.btnPhieuxuat.Margin = new System.Windows.Forms.Padding(4);
            this.btnPhieuxuat.Name = "btnPhieuxuat";
            this.btnPhieuxuat.Size = new System.Drawing.Size(443, 36);
            this.btnPhieuxuat.TabIndex = 3;
            this.btnPhieuxuat.Text = "Phiếu xuất";
            this.btnPhieuxuat.Click += new System.EventHandler(this.btnPhieuxuat_Click);
            // 
            // btnPhieuNhap
            // 
            this.btnPhieuNhap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhieuNhap.Appearance.Options.UseFont = true;
            this.btnPhieuNhap.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPhieuNhap.Image = global::KP_RES.Properties.Resources.import_32;
            this.btnPhieuNhap.Location = new System.Drawing.Point(2, 2);
            this.btnPhieuNhap.Margin = new System.Windows.Forms.Padding(4);
            this.btnPhieuNhap.Name = "btnPhieuNhap";
            this.btnPhieuNhap.Size = new System.Drawing.Size(243, 36);
            this.btnPhieuNhap.TabIndex = 4;
            this.btnPhieuNhap.Text = "Phiếu nhập";
            this.btnPhieuNhap.Click += new System.EventHandler(this.btnPhieuNhap_Click);
            // 
            // btnHoadon
            // 
            this.btnHoadon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHoadon.Appearance.Options.UseFont = true;
            this.btnHoadon.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHoadon.Image = global::KP_RES.Properties.Resources.bill_26;
            this.btnHoadon.Location = new System.Drawing.Point(688, 2);
            this.btnHoadon.Margin = new System.Windows.Forms.Padding(4);
            this.btnHoadon.Name = "btnHoadon";
            this.btnHoadon.Size = new System.Drawing.Size(247, 36);
            this.btnHoadon.TabIndex = 2;
            this.btnHoadon.Text = "Hóa đơn bán hàng";
            this.btnHoadon.Click += new System.EventHandler(this.btnHoadon_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.gridControl2);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(941, 130);
            this.panelControl7.TabIndex = 16;
            // 
            // Frm_Thekho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 615);
            this.Controls.Add(this.panelControl6);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Thekho";
            this.Text = "Thẻ kho";
            this.Load += new System.EventHandler(this.Frm_Thekho_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabOption)).EndInit();
            this.TabOption.ResumeLayout(false);
            this.Tab_Phieunhap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.Tab_Phieuxuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.Tab_Hoadonbanhang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn GIAVON;
        private DevExpress.XtraGrid.Columns.GridColumn SLTONDAUKY;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGIATONDAUKY;
        private DevExpress.XtraGrid.Columns.GridColumn SLNHAP;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGIANHAP;
        private DevExpress.XtraGrid.Columns.GridColumn SLXUAT;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGIAXUAT;
        private DevExpress.XtraGrid.Columns.GridColumn SLTONCUOIKY;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGIATONCUOIKY;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.LookUpEdit cboHH;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.LookUpEdit cboKho;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit cboCuahang;
        private DevExpress.XtraEditors.LookUpEdit cboNhom;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STTPN;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn KHO;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIENTCK;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TIENCHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn TIENTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn TONGCONG;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraTab.XtraTabControl TabOption;
        private DevExpress.XtraTab.XtraTabPage Tab_Phieunhap;
        private DevExpress.XtraTab.XtraTabPage Tab_Phieuxuat;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraTab.XtraTabPage Tab_Hoadonbanhang;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnPhieuxuat;
        private DevExpress.XtraEditors.SimpleButton btnPhieuNhap;
        private DevExpress.XtraEditors.SimpleButton btnHoadon;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STTPX;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn STTHD;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;




    }
}