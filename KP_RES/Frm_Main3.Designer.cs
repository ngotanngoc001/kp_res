﻿namespace KP_RES 
{
    partial class Frm_Main3 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main3));
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame1 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame2 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.timerGiaiphongbonho = new System.Windows.Forms.Timer(this.components);
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itDangxuat = new DevExpress.XtraEditors.TileItem();
            this.itTrogiup = new DevExpress.XtraEditors.TileItem();
            this.itCauhinh = new DevExpress.XtraEditors.TileItem();
            this.itHethong = new DevExpress.XtraEditors.TileItem();
            this.itThoat = new DevExpress.XtraEditors.TileItem();
            this.itThunho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itFastFood = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itBanverapphim = new DevExpress.XtraEditors.TileItem();
            this.itDatverapphim = new DevExpress.XtraEditors.TileItem();
            this.itSoatverapphim = new DevExpress.XtraEditors.TileItem();
            this.itThemKH = new DevExpress.XtraEditors.TileItem();
            this.itPhatthuong = new DevExpress.XtraEditors.TileItem();
            this.itTracuuKH = new DevExpress.XtraEditors.TileItem();
            this.SuspendLayout();
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // timerGiaiphongbonho
            // 
            this.timerGiaiphongbonho.Enabled = true;
            this.timerGiaiphongbonho.Interval = 120000;
            this.timerGiaiphongbonho.Tick += new System.EventHandler(this.timerGiaiphongbonho_Tick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Name = "tileGroup10";
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_1366x768_5;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(0, 0);
            this.tileControl1.MaxId = 35;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 62;
            this.tileControl1.RowCount = 4;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(984, 562);
            this.tileControl1.TabIndex = 3;
            this.tileControl1.Text = "Trang chủ";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itDangxuat);
            this.tileGroup2.Items.Add(this.itTrogiup);
            this.tileGroup2.Items.Add(this.itCauhinh);
            this.tileGroup2.Items.Add(this.itHethong);
            this.tileGroup2.Items.Add(this.itThoat);
            this.tileGroup2.Items.Add(this.itThunho);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itDangxuat
            // 
            this.itDangxuat.BackgroundImage = global::KP_RES.Properties.Resources.dangxuat3;
            this.itDangxuat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Đăng xuất";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDangxuat.Elements.Add(tileItemElement1);
            this.itDangxuat.Id = 2;
            this.itDangxuat.Name = "itDangxuat";
            this.itDangxuat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDangxuat_ItemClick);
            // 
            // itTrogiup
            // 
            this.itTrogiup.BackgroundImage = global::KP_RES.Properties.Resources.huongdan;
            this.itTrogiup.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Trợ giúp";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrogiup.Elements.Add(tileItemElement2);
            this.itTrogiup.Id = 3;
            this.itTrogiup.Name = "itTrogiup";
            this.itTrogiup.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrogiup_ItemClick);
            // 
            // itCauhinh
            // 
            this.itCauhinh.BackgroundImage = global::KP_RES.Properties.Resources.cauhinh;
            this.itCauhinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cấu hình";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinh.Elements.Add(tileItemElement3);
            this.itCauhinh.Id = 4;
            this.itCauhinh.Name = "itCauhinh";
            this.itCauhinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinh_ItemClick);
            // 
            // itHethong
            // 
            this.itHethong.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itHethong.BackgroundImage")));
            this.itHethong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Hệ thống";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHethong.Elements.Add(tileItemElement4);
            this.itHethong.Id = 5;
            this.itHethong.Name = "itHethong";
            this.itHethong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHethong_ItemClick);
            // 
            // itThoat
            // 
            this.itThoat.BackgroundImage = global::KP_RES.Properties.Resources.dong2;
            this.itThoat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thoát";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itThoat.Elements.Add(tileItemElement5);
            this.itThoat.Id = 0;
            this.itThoat.Name = "itThoat";
            this.itThoat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThoat_ItemClick);
            // 
            // itThunho
            // 
            this.itThunho.BackgroundImage = global::KP_RES.Properties.Resources.thunho;
            this.itThunho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Thu nhỏ";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itThunho.Elements.Add(tileItemElement6);
            this.itThunho.Id = 1;
            this.itThunho.Name = "itThunho";
            this.itThunho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThunho_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itFastFood);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itFastFood
            // 
            this.itFastFood.BackgroundImage = global::KP_RES.Properties.Resources.banhang9;
            this.itFastFood.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "FastFood";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itFastFood.Elements.Add(tileItemElement7);
            tileItemFrame1.AnimateBackgroundImage = true;
            tileItemFrame1.AnimateImage = true;
            tileItemFrame1.AnimateText = true;
            tileItemFrame1.BackgroundImage = global::KP_RES.Properties.Resources.banhang9;
            tileItemElement8.Text = "FastFood";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame1.Elements.Add(tileItemElement8);
            tileItemFrame1.UseBackgroundImage = true;
            tileItemFrame1.UseImage = true;
            tileItemFrame1.UseText = true;
            tileItemFrame2.AnimateBackgroundImage = true;
            tileItemFrame2.AnimateImage = true;
            tileItemFrame2.AnimateText = true;
            tileItemFrame2.BackgroundImage = global::KP_RES.Properties.Resources.fastfood;
            tileItemElement9.Text = "FastFood";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame2.Elements.Add(tileItemElement9);
            tileItemFrame2.UseBackgroundImage = true;
            tileItemFrame2.UseImage = true;
            tileItemFrame2.UseText = true;
            this.itFastFood.Frames.Add(tileItemFrame1);
            this.itFastFood.Frames.Add(tileItemFrame2);
            this.itFastFood.Id = 19;
            this.itFastFood.IsLarge = true;
            this.itFastFood.Name = "itFastFood";
            this.itFastFood.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itFastFood_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itBanverapphim);
            this.tileGroup9.Items.Add(this.itDatverapphim);
            this.tileGroup9.Items.Add(this.itSoatverapphim);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itThemKH);
            this.tileGroup11.Items.Add(this.itPhatthuong);
            this.tileGroup11.Items.Add(this.itTracuuKH);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itBanverapphim
            // 
            this.itBanverapphim.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            this.itBanverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Bán vé";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanverapphim.Elements.Add(tileItemElement10);
            this.itBanverapphim.Id = 29;
            this.itBanverapphim.IsLarge = true;
            this.itBanverapphim.Name = "itBanverapphim";
            this.itBanverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanverapphim_ItemClick);
            // 
            // itDatverapphim
            // 
            this.itDatverapphim.BackgroundImage = global::KP_RES.Properties.Resources.inmavach;
            this.itDatverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Đặt vé";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itDatverapphim.Elements.Add(tileItemElement11);
            this.itDatverapphim.Id = 30;
            this.itDatverapphim.IsLarge = true;
            this.itDatverapphim.Name = "itDatverapphim";
            this.itDatverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDatverapphim_ItemClick);
            // 
            // itSoatverapphim
            // 
            this.itSoatverapphim.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itSoatverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "In vé mua online";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itSoatverapphim.Elements.Add(tileItemElement12);
            this.itSoatverapphim.Id = 31;
            this.itSoatverapphim.IsLarge = true;
            this.itSoatverapphim.Name = "itSoatverapphim";
            this.itSoatverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSoatverapphim_ItemClick);
            // 
            // itThemKH
            // 
            this.itThemKH.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itThemKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Thêm mới khách hàng";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itThemKH.Elements.Add(tileItemElement13);
            this.itThemKH.Id = 32;
            this.itThemKH.IsLarge = true;
            this.itThemKH.Name = "itThemKH";
            this.itThemKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThemKH_ItemClick);
            // 
            // itPhatthuong
            // 
            this.itPhatthuong.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itPhatthuong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Phát thưởng";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhatthuong.Elements.Add(tileItemElement14);
            this.itPhatthuong.Id = 33;
            this.itPhatthuong.IsLarge = true;
            this.itPhatthuong.Name = "itPhatthuong";
            this.itPhatthuong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhatthuong_ItemClick);
            // 
            // itTracuuKH
            // 
            this.itTracuuKH.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itTracuuKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Tra cứu khách hàng thân thiết";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itTracuuKH.Elements.Add(tileItemElement15);
            this.itTracuuKH.Id = 34;
            this.itTracuuKH.IsLarge = true;
            this.itTracuuKH.Name = "itTracuuKH";
            this.itTracuuKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTracuuKH_ItemClick);
            // 
            // Frm_Main3
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.ControlBox = false;
            this.Controls.Add(this.tileControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Main3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KingPos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main3_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main3_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Main3_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itThoat;
        private DevExpress.XtraEditors.TileItem itThunho;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private System.Windows.Forms.Timer timerGiaiphongbonho;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itHethong;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itCauhinh;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TileItem itDangxuat;
        private DevExpress.XtraEditors.TileItem itTrogiup;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itFastFood;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itBanverapphim;
        private DevExpress.XtraEditors.TileItem itDatverapphim;
        private DevExpress.XtraEditors.TileItem itSoatverapphim;
        private DevExpress.XtraEditors.TileItem itThemKH;
        private DevExpress.XtraEditors.TileItem itPhatthuong;
        private DevExpress.XtraEditors.TileItem itTracuuKH;
    }
}