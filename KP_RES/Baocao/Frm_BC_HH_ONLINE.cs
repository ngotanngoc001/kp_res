﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid;

namespace KP_RES 
{
    public partial class Frm_BC_HH_ONLINE : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_HH_ONLINE()
        {
            InitializeComponent();
            dtpTuNgay.EditValue = clsGlobal.gdServerDate;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;
            String sSQL = "";

            if (optGroup.SelectedIndex == 0)
                pnChoose.Visible = false;
            else
                pnChoose.Visible = true;

            if (optGroup.SelectedIndex == 1)
            {
                sSQL = "SELECT MADIADIEM AS MA,TENDIADIEM AS TEN FROM DM_DIADIEM WHERE SUDUNG = 1 ORDER BY TEN";
                LoadCombo(sSQL);
            }
            else if (optGroup.SelectedIndex == 2)
            {
                sSQL = "SELECT MA_NHOMHANG AS MA, TEN_NHOMHANG AS TEN FROM NHOMHANG ";
                sSQL += "WHERE THUCDON = 1 AND SUDUNG = 1 ORDER BY TEN"; 
                LoadCombo(sSQL);
            }
            else if (optGroup.SelectedIndex == 3)
            {
                sSQL = "SELECT A.MA_HANGHOA AS MA, A.TEN_HANGHOA AS TEN FROM HANGHOA A INNER JOIN NHOMHANG B ON A.MA_NHOMHANG = B.MA_NHOMHANG WHERE B.THUCDON = 1 AND A.SUDUNG = 1 ORDER BY TEN";
                LoadCombo(sSQL);
            }
            else if (optGroup.SelectedIndex == 4)
            {
                sSQL = "SELECT A.UserID AS MA, B.TENNHANVIEN AS TEN ";
                sSQL += "FROM SYS_USER A INNER JOIN DM_NHANVIEN B ON A.MaNV = B.MANHANVIEN WHERE B.SUDUNG = 1 ORDER BY TEN"; 
                LoadCombo(sSQL);
            }

        }

        private void LoadCombo(String sSQL)
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable(sSQL);
            cboData.Properties.DataSource = myDT;
            cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                splashScreenManager1.ShowWaitForm();
                String sMa = "";
                String sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue);
                String sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue);

                if (optGroup.SelectedIndex != 0)
                {
                    sMa = cboData.EditValue.ToString();
                }

                String sSQL = "EXEC SP_SelectBCHangHoaOnline ";
                sSQL += clsMain.SQLString(sMa) + ",";
                sSQL += clsMain.SQLString(sTuNgay) + ",";
                sSQL += clsMain.SQLString(sDenNgay) + ",";
                sSQL += clsMain.SQLString(optGroup.SelectedIndex.ToString());

                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;
                splashScreenManager1.CloseWaitForm();
            }
            catch (Exception ex)
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");

                    if (optGroup.SelectedIndex == 0)
                    {
                        All.DefaultValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description + " )";
                    }
                    else
                    {
                        All.DefaultValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboData.Text + " )";
                    }

                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTuNgay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenNgay.Text;

                    if (ContainColumn("HEARDER", dt) == false)
                        dt.Columns.Add(All);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                  
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 68;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Không có dữ liệu");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}