﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BC_Doanhso : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();

        public Frm_BC_Doanhso()
        {
            InitializeComponent();
            LoadCombo();
        }
     
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = string.Empty;

                sql = "EXEC SP_BAOCAODOANHSO @TUNGAY='" + string.Format("{0:yyyyMMdd HH:mm}", dtpTungay.EditValue)
                    + "', @DENNGAY='" + string.Format("{0:yyyyMMdd HH:mm}", dtpDenngay.EditValue) + "', @MANHANVIEN='"
                    + cboNV.EditValue.ToString() + "'";
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.Columns["TENNHANVIEN"].GroupIndex = 0;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            if (info.Column.Caption == "Tên nhân viên")
            {
                info.GroupText = info.Column.Caption + ": <color=Blue>" + info.GroupValueText + "</color> ";
                info.GroupText += "<color=Red>" + view.GetGroupSummaryText(e.RowHandle) + "</color> ";
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void LoadCombo()
        {
            dtpTungay.EditValue = clsGlobal.gdServerDate.Date;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
            DataTable dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN, DIACHI from DM_NHANVIEN where SUDUNG=1 order by TEN");
            dt.Rows.Add("0", "Tất cả", "");
            cboNV.Properties.DataSource = dt;
            cboNV.EditValue = 0;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;
                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;
                    DataColumn colNHANVIEN = new DataColumn();
                    colNHANVIEN.ColumnName = "TNHANVIEN";
                    colNHANVIEN.DataType = System.Type.GetType("System.String");
                    colNHANVIEN.DefaultValue = cboNV.Text;
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TNHANVIEN", dt) == false)
                        dt.Columns.Add(colNHANVIEN);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                    frm.dtSource = dt;
                    frm.Mode = 5;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }  
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        string _thongbao = "";
        string _cuahang = "";
        string _kho = "";
        string _quay = "";
        string _nhomhang = "";
        string _mathang = "";
        string _nhanvien = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        string _chuachonhddexem = "";
        private void Frm_BC_Doanhso_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("baocaodoanhso", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["NGAYTAO"].Caption = rm.GetString("ngay", culture);
            gridView1.Columns["TENNHANVIEN"].Caption = rm.GetString("nhanvien", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gridView1.Columns["TEN_DONVITINH"].Caption = rm.GetString("dvt1", culture);
            gridView1.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView1.Columns["DONGIA"].Caption = rm.GetString("dongia", culture);
            gridView1.Columns["THANHTIEN"].Caption = rm.GetString("thanhtien", culture);
            gridView1.Columns["TIENTHUE"].Caption = rm.GetString("tienthue", culture);
            gridView1.Columns["TIENPHUTHU"].Caption = rm.GetString("phuthu1", culture);
            gridView1.Columns["TIENCHIETKHAU"].Caption = rm.GetString("chietkhau", culture);
            gridView1.Columns["TONGCONG"].Caption = rm.GetString("tongcong1", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            gridView1.GroupSummary[0].DisplayFormat = rm.GetString("doanhsotruocthue", culture);
            gridView1.GroupSummary[1].DisplayFormat = rm.GetString("sauthuesauchietkhau", culture);
            lb_From.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            btnNhanVien.Text = rm.GetString("nhanvien", culture);
        }

        private void btnXemtruockhiin_Tong_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;
                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;
                    DataColumn colNHANVIEN = new DataColumn();
                    colNHANVIEN.ColumnName = "TNHANVIEN";
                    colNHANVIEN.DataType = System.Type.GetType("System.String");
                    colNHANVIEN.DefaultValue = cboNV.Text;
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TNHANVIEN", dt) == false)
                        dt.Columns.Add(colNHANVIEN);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                    frm.dtSource = dt;
                    frm.Mode = 123;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }  
        }

       
    }
}