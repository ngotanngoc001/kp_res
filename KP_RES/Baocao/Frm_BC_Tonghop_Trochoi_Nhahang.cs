﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_Tonghop_Trochoi_Nhahang : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_Tonghop_Trochoi_Nhahang()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_Tonghop_Trochoi_Nhahang_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Cửa hàng
            {
                dt = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Cửa hàng";
            }
            else if (optGroup.SelectedIndex == 1)//Quầy
            {
                dt = clsMain.ReturnDataTable("select MA_QUAY as MA,TEN_QUAY as TEN from QUAY where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên quầy" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Quầy";
            }
            else if (optGroup.SelectedIndex == 2)//Loại trò chơi
            {
                dt = clsMain.ReturnDataTable("select MA_LOAITROCHOI as MA,TEN_LOAITROCHOI as TEN from LOAITROCHOI where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên loại trò chơi" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Loại trò chơi";
            }
            else if (optGroup.SelectedIndex == 3)//Trò chơi
            {
                dt = clsMain.ReturnDataTable("select MA_TROCHOI as MA,TEN_TROCHOI as TEN from TROCHOI where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên trò chơi" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Trò chơi";
            }
            else if (optGroup.SelectedIndex == 4)//Nhân viên
            {
                dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhân viên";
            }
            else if (optGroup.SelectedIndex == 5)//Khách hàng
            {
                dt = clsMain.ReturnDataTable("select MA, TEN from NHACUNGCAP where SUDUNG=1  and LOAI=0 Order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Khách hàng";
            }

            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value)
                    throw new Exception("Chưa chọn đối tượng cần xem");

                sql += "EXEC SP_BAOCAOBANHANG @TUNGAY='" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue)
                       + "', @DENNGAY='" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59"
                       + "',@DIEUKIEN='" + cboDATA.EditValue.ToString() + "',@TYPE='CH'";
                dt = clsMain.ReturnDataTable(sql);
                dt.Columns.Remove("MA_HANGHOA");
                dt.Columns.Remove("TYPE");
                dt.Columns.Remove("NAMETYPE");
                dt.Columns.Remove("THUE");
                dt.Columns.Remove("TIENTHUE");
                sql = "";
                sql += "select  'Khu vui chơi' as LOAI,TC.TEN_TROCHOI As TEN_HANGHOA,'Vé' as TEN_DONVITINH,COUNT(VB.MA_TROCHOI) AS SOLUONG,VB.GIABAN as DONGIA,SUM(VB.GIABAN) AS THANHTIEN,0 as TIENPHUTHU,0 as TIENCHIETKHAU,SUM(VB.GIABAN) AS TONGCONG" + "\n";
                sql += "from VETROCHOI VB,DM_NHANVIEN NV,TROCHOI TC,LOAITROCHOI LTC,CUAHANG CH,QUAY Q,KHO K " + "\n";
                sql += "where VB.MA_NHANVIEN=NV.MANHANVIEN " + "\n";
                sql += "and VB.MA_TROCHOI=TC.MA_TROCHOI " + "\n";
                sql += "and TC.MA_LOAITROCHOI=LTC.MA_LOAITROCHOI " + "\n";
                sql += "and VB.MA_QUAY=Q.MA_QUAY " + "\n";
                sql += "and Q.MA_KHO=K.MA_KHO " + "\n";
                sql += "and K.MA_CUAHANG=CH.MA_CUAHANG " + "\n";
                sql += "and VB.HUY =0 " + "\n";
                sql += "and NGAYBAN between " + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + " and " + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + "\n";
                if (optGroup.SelectedIndex == 0)//Cửa hàng
                {
                    sql += "and CH.MA_CUAHANG=" + clsMain.SQLString(cboDATA.EditValue.ToString());
                }
                sql += "\n";
                sql += "Group by TC.TEN_TROCHOI,VB.GIABAN" + "\n";
                sql += "Order by TC.TEN_TROCHOI";
                DataTable dtTemp = clsMain.ReturnDataTable(sql);

                foreach (DataRow dr in dtTemp.Rows)
                {
                    dt.Rows.Add(dr.ItemArray);
                }

                gridControl1.DataSource = dt;

                gridView1.ExpandAllGroups();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount  > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;
                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    DataColumn colFILTER = new DataColumn();
                    colFILTER.ColumnName = "FILTER";
                    colFILTER.DataType = System.Type.GetType("System.String");
                    colFILTER.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(colFILTER);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 52;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa có dữ liệu");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}