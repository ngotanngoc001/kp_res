﻿namespace KP_RES
{
    partial class Frm_BCChitietKiemke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_DONVITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_KHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_mahanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_gianhap = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtvat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soserial = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cmb_tenhanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 52);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_mahanghoa,
            this.txt_soluong,
            this.txt_gianhap,
            this.txt_chietkhau,
            this.txtvat,
            this.txt_soserial,
            this.cmb_tenhanghoa});
            this.gridControl1.Size = new System.Drawing.Size(1189, 333);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTHH,
            this.TEN_KHO,
            this.MA_HANGHOA,
            this.TEN_HANGHOA,
            this.TEN_DONVITINH,
            this.SOLUONG,
            this.FILL,
            this.MA_KHO});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STTHH
            // 
            this.STTHH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceCell.Options.UseFont = true;
            this.STTHH.AppearanceCell.Options.UseTextOptions = true;
            this.STTHH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceHeader.Options.UseFont = true;
            this.STTHH.AppearanceHeader.Options.UseTextOptions = true;
            this.STTHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTHH.Caption = "STT";
            this.STTHH.FieldName = "STTHH";
            this.STTHH.Name = "STTHH";
            this.STTHH.OptionsColumn.AllowEdit = false;
            this.STTHH.OptionsColumn.AllowFocus = false;
            this.STTHH.OptionsColumn.FixedWidth = true;
            this.STTHH.Visible = true;
            this.STTHH.VisibleIndex = 0;
            this.STTHH.Width = 83;
            // 
            // TEN_KHO
            // 
            this.TEN_KHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_KHO.AppearanceCell.Options.UseFont = true;
            this.TEN_KHO.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_KHO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TEN_KHO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_KHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_KHO.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHO.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHO.Caption = "Tên kho";
            this.TEN_KHO.FieldName = "TEN_KHO";
            this.TEN_KHO.Name = "TEN_KHO";
            this.TEN_KHO.OptionsColumn.AllowEdit = false;
            this.TEN_KHO.OptionsColumn.AllowFocus = false;
            this.TEN_KHO.OptionsColumn.FixedWidth = true;
            this.TEN_KHO.Visible = true;
            this.TEN_KHO.VisibleIndex = 2;
            this.TEN_KHO.Width = 250;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.MA_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.MA_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HANGHOA.Caption = "Mã hàng hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            this.MA_HANGHOA.OptionsColumn.FixedWidth = true;
            this.MA_HANGHOA.Visible = true;
            this.MA_HANGHOA.VisibleIndex = 3;
            this.MA_HANGHOA.Width = 150;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 4;
            this.TEN_HANGHOA.Width = 250;
            // 
            // TEN_DONVITINH
            // 
            this.TEN_DONVITINH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_DONVITINH.AppearanceCell.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_DONVITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_DONVITINH.AppearanceHeader.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_DONVITINH.Caption = "ĐVT";
            this.TEN_DONVITINH.FieldName = "TEN_DONVITINH";
            this.TEN_DONVITINH.Name = "TEN_DONVITINH";
            this.TEN_DONVITINH.OptionsColumn.AllowEdit = false;
            this.TEN_DONVITINH.OptionsColumn.AllowFocus = false;
            this.TEN_DONVITINH.OptionsColumn.FixedWidth = true;
            this.TEN_DONVITINH.Visible = true;
            this.TEN_DONVITINH.VisibleIndex = 5;
            this.TEN_DONVITINH.Width = 127;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 6;
            this.SOLUONG.Width = 127;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 7;
            this.FILL.Width = 50;
            // 
            // MA_KHO
            // 
            this.MA_KHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_KHO.AppearanceCell.Options.UseFont = true;
            this.MA_KHO.AppearanceCell.Options.UseTextOptions = true;
            this.MA_KHO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_KHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_KHO.AppearanceHeader.Options.UseFont = true;
            this.MA_KHO.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_KHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_KHO.Caption = "Mã kho";
            this.MA_KHO.FieldName = "MA_KHO";
            this.MA_KHO.Name = "MA_KHO";
            this.MA_KHO.OptionsColumn.AllowEdit = false;
            this.MA_KHO.OptionsColumn.AllowFocus = false;
            this.MA_KHO.OptionsColumn.FixedWidth = true;
            this.MA_KHO.Visible = true;
            this.MA_KHO.VisibleIndex = 1;
            this.MA_KHO.Width = 150;
            // 
            // txt_mahanghoa
            // 
            this.txt_mahanghoa.AutoHeight = false;
            this.txt_mahanghoa.Name = "txt_mahanghoa";
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            // 
            // txt_gianhap
            // 
            this.txt_gianhap.AutoHeight = false;
            this.txt_gianhap.Name = "txt_gianhap";
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.AutoHeight = false;
            this.txt_chietkhau.Name = "txt_chietkhau";
            // 
            // txtvat
            // 
            this.txtvat.AutoHeight = false;
            this.txtvat.Name = "txtvat";
            // 
            // txt_soserial
            // 
            this.txt_soserial.AutoHeight = false;
            this.txt_soserial.Name = "txt_soserial";
            // 
            // cmb_tenhanghoa
            // 
            this.cmb_tenhanghoa.AutoHeight = false;
            this.cmb_tenhanghoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb_tenhanghoa.Name = "cmb_tenhanghoa";
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl4);
            this.header.Controls.Add(this.panelControl2);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1189, 52);
            this.header.TabIndex = 48;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl4.Controls.Add(this.pictureEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(261, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(928, 52);
            this.panelControl4.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(924, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSize = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl2.Controls.Add(this.title);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(81, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(180, 52);
            this.panelControl2.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(168, 32);
            this.title.TabIndex = 1;
            this.title.Text = "Chi tiết kiểm kê";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            this.pnlHome.Click += new System.EventHandler(this.pnlHome_Click);
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // Frm_BCChitietKiemke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 385);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.header);
            this.Name = "Frm_BCChitietKiemke";
            this.Text = "Chi Tiết kiểm kê";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STTHH;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_DONVITINH;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_mahanghoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_gianhap;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtvat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soserial;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cmb_tenhanghoa;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHO;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn MA_KHO;
    }
}