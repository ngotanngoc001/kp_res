﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraCharts;
using System.Globalization;
using System.Resources;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_TK_Doanhso : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        int m_next = 0;
        DateTime date;

        public Frm_TK_Doanhso()
        {
            InitializeComponent();
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            chart.Series.Clear();
            chart.Titles.Clear();

            String FromDate = "";
            String ToDate = "";
            if (optGroup.SelectedIndex == 0)
            {
                DateTime date = DateTime.ParseExact("01/01/" + cboNam.EditValue, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                var firstDayOfMonth = new DateTime(date.Year, 1, 1);
                var lastDayOfMonth = new DateTime(date.Year, 12, 31);
                FromDate = firstDayOfMonth.Day.ToString("00") + "/" + firstDayOfMonth.Month.ToString("00") + "/" + firstDayOfMonth.Year.ToString("0000") + " 00:00:00";
                ToDate = lastDayOfMonth.Day.ToString("00") + "/" + lastDayOfMonth.Month.ToString("00") + "/" + lastDayOfMonth.Year.ToString("0000") + " 23:59:59";
            }
            else if (optGroup.SelectedIndex == 1)
            {
                DateTime date = DateTime.ParseExact("01/" + cboThang.EditValue + "/" + cboNam .EditValue , "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                FromDate = firstDayOfMonth.Day.ToString("00") + "/" + firstDayOfMonth.Month.ToString("00") + "/" + firstDayOfMonth.Year.ToString("0000")+" 00:00:00" ;
                ToDate = lastDayOfMonth.Day.ToString("00") + "/" + lastDayOfMonth.Month.ToString("00") + "/" + lastDayOfMonth.Year.ToString("0000") + " 23:59:59";
            }

            String sSQL = "";
            sSQL += "EXEC SP_BAOCAO_DOANHTHU ";
            sSQL += clsMain.SQLString(cboData.EditValue.ToString()) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", FromDate)) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", ToDate)) + ",";
            sSQL += optGroup.SelectedIndex;

            DataTable dt = clsMain.ReturnDataTable(sSQL);

            long itongtien = 0;
            if (dt.Rows.Count > 0)
            {
                int n = dt.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    if (dt.Rows[i]["Mem"].ToString().Length < 2)
                    {
                        dt.Rows[i]["Mem"]="0"+dt.Rows[i]["Mem"].ToString ();
                    }
                    itongtien = itongtien + int.Parse ( dt.Rows[i]["DoanhThu"].ToString ());
                }
            }
            chart.DataSource = dt;
            ChartTitle ct = new ChartTitle();
            if(cboThang .Visible )
            {
                 ct.Text = _thongkedoanhthu1.ToUpper () + " "+ cboThang .EditValue .ToString ()+"/"+ cboNam .EditValue .ToString ();
            }
            else 
            {
                ct.Text = _thongkedoanhthu1.ToUpper () + " " + cboNam.EditValue.ToString();
            }
           
            ct.Font = new Font("Tahoma", 20, FontStyle.Bold);
            Series series = new Series("Series1", ViewType.Bar);
            chart.Series.Add(series);
            chart.Titles.Add(ct);
            series.LegendText = _doanhthu+" : "+ itongtien .ToString ("N0");
            series.ArgumentDataMember = "Mem";
            series.ValueDataMembers.AddRange("DoanhThu");
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (optGroup.SelectedIndex == 0)
            {
                lbThang.Visible = false;
                cboThang.Visible = false;
                btnTimkiem.Location = new Point(btnTimkiem.Location.X,224);
            }
            else if(optGroup.SelectedIndex == 1)
            {
                lbThang.Visible = true;
                cboThang.Visible = true;
                btnTimkiem.Location = new Point(btnTimkiem.Location.X, 281);
            }
        }

        private void loadCombo()
        {
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-1).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-2).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-3).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-4).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-5).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-6).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-7).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-8).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-9).Year.ToString());
            cboNam.EditValue = clsGlobal.gdServerDate.Year.ToString();
            cboThang.EditValue = clsGlobal.gdServerDate.Month.ToString().Length == 1 ? "0" + clsGlobal.gdServerDate.Month.ToString() : clsGlobal.gdServerDate.Month.ToString();
            
            string sSQL = "";
            sSQL += "Select MA_CUAHANG AS MA,TEN_CUAHANG AS TEN" + "\n"; 
            sSQL += "From CUAHANG " + "\n";
            sSQL += "Where SUDUNG=1";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            dt.Rows.Add("0", "Tất cả");
            cboData.Properties.DataSource = dt;
            cboData.EditValue = 0;
        }

        public static int GetWeekOfMonth(DateTime date)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

            while (date.Date.AddDays(1).DayOfWeek != CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                date = date.AddDays(1);

            return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;
        }

        private void cboNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboThang.EditValue.ToString() != "")
            //{
            //    string[] arr = new string[2];
            //    arr = cboThang.EditValue.ToString().Split(' ');
            //    if (int.Parse(arr[1]) == 12)
            //        m_next = 1;
            //    else
            //        m_next = int.Parse(arr[1]) + 1;
            //}
        }

        private void cboThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboThang.EditValue.ToString() != "")
            //{
            //    string[] arr = new string[2];
            //    arr = cboThang.EditValue.ToString().Split(' ');
            //    if (int.Parse(arr[1]) == 12)
            //        m_next = 1;
            //    else
            //        m_next = int.Parse(arr[1]) + 1;
            //}
        }

        string _ngay = "";
        string _tuan11 = "";
        string _thang1 = "";
        string _doanhthu = "";
        string _thongkedoanhthu1 = ""; 
        private void Frm_TK_Doanhso_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("thongkedoanhso", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            lbNam.Text = rm.GetString("nam", culture);
            lbThang.Text = rm.GetString("thang", culture);
            optGroup.Properties.Items[0].Description = rm.GetString("xemtronam", culture);
            optGroup.Properties.Items[1].Description = rm.GetString("xemtrothang", culture);
            _ngay = rm.GetString("ngay", culture);
            _tuan11 = rm.GetString("tuan11", culture);
            _thang1 = rm.GetString("thang1", culture);
            _doanhthu = rm.GetString("doanhthu", culture);
            _thongkedoanhthu1 = rm.GetString("thongkedoanhthu1", culture);
            loadCombo();
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataColumn dc2 = new DataColumn();
            dc2.ColumnName = "TEMP";
            dc2.DefaultValue = "Temp";
            dt.Columns.Add(dc2);
            dt.Rows.Add("TEMP");

            Bitmap bmp = new Bitmap(panelControl5.ClientRectangle.Width, panelControl5.ClientRectangle.Height);
            panelControl5.DrawToBitmap(bmp, panelControl5.ClientRectangle);

            DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
            colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
            colIMAGE.AllowDBNull = true;
            colIMAGE.Caption = "IMAGE";
            colIMAGE.DefaultValue = ImageToByteArray(bmp);
            dt.Columns.Add(colIMAGE);

            DataColumn dc1 = new DataColumn();
            dc1.ColumnName = "NGAY";
            if (cboThang.Visible)
            {
                dc1.DefaultValue = cboThang .Text +"/"+ cboNam.Text;
            }
            else
            {
                dc1.DefaultValue = cboNam.Text;
            }
            
            dt.Columns.Add(dc1);

            
            if (optGroup.SelectedIndex == 0)
            {
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 57;
                frm.ShowDialog();
                frm.Dispose();
                
            }
            else
            {
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 58;
                frm.ShowDialog();
                frm.Dispose();
                
            }
        }

        private void btnXuatFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChartControl chart = (ChartControl)panelControl5.Controls[0];
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                chart.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                chart.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void panelControl6_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}