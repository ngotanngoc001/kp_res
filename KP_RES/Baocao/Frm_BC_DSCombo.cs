﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_DSCombo : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_DSCombo()
        {
            InitializeComponent();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (optGroup.SelectedIndex == 0)
                {
                    sql += "SELECT B.MA_HANGHOA AS ID,B.TEN_HANGHOA as NAME,GIANHAP as PRICE,A.SOLUONG as QTY,TEN_DONVITINH as DVT, B.SUDUNG as _USE,GIA as PRICE1,GIA as PRICE2,TMP.TEN_HANGHOA AS GROUPCOMBO,A.MA_COMBO as IDCOMBO FROM COMBO AS A INNER JOIN HANGHOA AS B ON B.MA_HANGHOA = A.MA_HANGHOA INNER JOIN (SELECT DISTINCT MA_COMBO, TEN_HANGHOA  FROM COMBO INNER JOIN HANGHOA ON HANGHOA.MA_HANGHOA = COMBO.MA_COMBO )AS TMP ON TMP.MA_COMBO = A.MA_COMBO inner join DONVITINH as C on B.MA_DONVITINH=C.MA_DONVITINH \n";
                    if (cboDATA1.EditValue.ToString() != "0")
                    {
                        sql += "Where A.MA_COMBO = " + clsMain.SQLString(cboDATA1.EditValue.ToString()) + "\n";
                    }
                }
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.Columns["GROUPCOMBO"].GroupIndex = 0;
                gridView1.ExpandAllGroups();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount >0)
                {
                    DataColumn colFILTER = new DataColumn();
                    colFILTER.ColumnName = "FILTER";
                    colFILTER.DataType = System.Type.GetType("System.String");
                    colFILTER.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA1.Text;

                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(colFILTER);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 44;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                Fill.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                Fill.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA1.Properties.DataSource = null;
            cboDATA1.Properties.Columns.Clear();
            cboDATA1.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)
            {
                DataTable dt = clsMain.ReturnDataTable("select DISTINCT MA_COMBO AS ID,TEN_HANGHOA as NAME from COMBO as a inner join HANGHOA as b on a.MA_COMBO = b.MA_HANGHOA where SUDUNG=1 Order by NAME");
                dt.Rows.Add("0", "Tất cả");
                cboDATA1.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, dt);
                lblData.Text = "Combo";
            }
            cboDATA1.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA1.Properties.Columns.Add(info);
                }
                cboDATA1.Properties.BestFitMode = BestFitMode.None;
                cboDATA1.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA1.Properties.DisplayMember = FieldName[1];
                cboDATA1.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA1.Properties.Columns[0].Width = 100;
                    cboDATA1.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA1.Properties.Columns[1].Width = 100;
                    cboDATA1.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA1.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void Frm_BC_DSCombo_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }
    }
}