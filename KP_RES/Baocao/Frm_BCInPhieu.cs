﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using System.IO;
using System.Diagnostics;

namespace KP_RES
{
    public partial class Frm_BCInPhieu : Form
    {
        public string ma = "";//Mã phiếu cần in báo cáo (để lấy dữ liệu)
        public DataTable dtSource = null;//Source report
        public int Mode = 1;//Mode report
        //0:Phiếu xuất kho
        //1:Phiếu nhập kho
        //2:Báo cáo tồn kho
        //3:Báo cáo bán hàng
        //4:Báo cáo kết quả kinh doanh
        //5:Báo cáo doanh số
        //6:Danh sách hóa đơn
        //7:Phiếu nhập kho đã có dữ liệu
        //8:Phiếu xuất kho đã có dữ liệu
        //9:Danh sách hàng hóa
        //10:Phiếu kiểm kê
        //12:Bảng giá theo khu
        //13:Cảnh báo tồn kho
        //14:Khách hàng thân thiết
        //15:Thẻ thanh toán nội bộ
        //16:In vé trò chơi
        //17:Báo cáo bán vé trò chơi
        //18:Lập phiếu thu
        //19:Lập phiếu chi
        //20:In mã khách hàng
        //21:Danh sách phiếu thu
        //22:Danh sách phiếu chi
        //23:Công nợ nhà cung cấp
        //24:Công nợ khách hàng
        //25:Tổng quan công nợ
        //26:Báo cáo quỹ tiền mặt
        //27:Báo cáo doanh thu Karaoke
        //28:Lập lịch chiếu phim
        //29:Nhập cân đối
        //30:Xuất cân đối
        //31:In bảng giá rạp phim
        //32:In lịch chiếu phim
        //33:Báo cáo bán vé rạp phim
        //34:Báo cáo hủy bán vé rạp phim
        //35:Báo cáo doanh thu rạp phim
        //36:Báo cáo đặt vé rạp phim
        //37:Chi tiết rạp
        //38:Chi tiết phim
        //39:Báo cáo bán hàng combo
        //40:Kết quả chiếu phim
        //41:Báo cáo bán vé trước
        //42:Danh sách hóa đơn bàn
        //43:Định lượng
        //44:Danh sách Combo
        //45:Danh sách người dùng
        //46:Danh sách hóa đơn
        //47:Danh sách hóa đơn hủy
        //48:Danh sách hóa in lại
        //49:Tong danh sach phieu nhap
        //50:Tong danh sach phieu xuat
        //51:Tong danh sach phieu kiem ke
        //52:Tong hop bao cao khu vui choi+ ban hang
        //53:Khu vuc (Rap phim)
        //54:Nuc san xuat (Rap phim)
        //55:The loai phim (Rap phim)
        //56:Cong nghe (Rap phim)
        //57:Bieu do doanh thu theo nam
        //58:Bieu do doanh thu theo thang
        //59:Thong ke hang ban chay
        //60:Lich su giao dich khach hang than thiet
        //61:Lich su cong tru tien TTTNB
        //62:Report Lich su cong tru diem
        //63:Report Lich su doi qua
        //64:Report doi qua
        //65:Bao cao ban ve Online
        //66:Bao cao lich su giao dich khtt
        //67:Bao cao bảng kê chi tiết bán vé
        //68:Bao cao bán hàng online
        //69:Bao cao cong van den
        //70:Bao cao cong van di
        //71:Bao cao lich su doi ma khtt
        //72:Bao cao chi tiet voucher
        //73:Bao cao tong hop voucher
        //74:Bao cao lich su lich chieu
        //75:Bao cao ung vien
        //76:Bao cao nhan vien
        //77:Bao cao hop dong lao dong
        //78:Bao cao khen thuong - ky luat
        //79:Bang luong
        //80:Bao cao tong hop ban ve theo khach hang
        //81:Bien ban giao nhan
        //82:Danh sach bien ban giao nhan
        //83:Chi tiet danh gia khach hang
        //84:Bieu do danh gia khach hang
        //85:Thong ke thu chi theo nam
        //86:Thong ke thu chi theo thang
        //87:Danh sach phieu de nghi chi
        //88:Phieu uy nhiem chi
        //89:Danh sach phieu uy nhiem chi
        //90:Danh sach ma khuyen mai
        //91:Danh sach theo doi hop dong
        //92:Danh sach theo doi chi hop dong
        //93:Danh sach khach - khach san
        //94:Danh sach thue phong - khach san
        //95:Bao cao so luong khach
        //96:Danh sach huy ban
        //97:Danh sach huy mon
        //98:Xem lai bill ban giao hang
        //99:Xem lai bill giao hang
        //100:Xem lai bill thao moc
        //101:Xem lai bill thao moc giao hang
        //102:Bao cao dich vu - nhan vien thuc hien
        //103:Bao cao ban hang sieu thi
        //104:Bao cao ton kho sieu thi
        //105:Them KHTT sieu thi
        //106:Phat thuong KHTT sieu thi
        //107:Bao cao tra hang sieu thi
        //108:Danh sach hoa don tra hang sieu thi
        //109:Bao cao nhap khi
        //110:Bao cao xuat kho
        //111:Bao cao xuat kho
        //112:Bao cao tra hang
        //113:Hoa don khach san
        //114:DS Hoa don khach san
        //115:DS Hủy dịch vụ
        //116:danh sách phòng hủy
        //117:danh sách dịch vụ khách hàng
        //118:phiếu xuất kho có số lượng tồn
        //119:hóa đơn bán lẻ Karaoke
        //120:phiếu đề nghị chi tiền
        //121:danh sach lay lai hoa don
        //122:hóa đơn bán hàng trà sữa
        //123:báo cáo doanh số tổng
        //124:báo cáo bán hàng tổng theo nhóm
        //125:báo cáo bán hàng theo nhóm in bill
        //126:hết món
        //127:danh sach hoa don khach san 1
        public Frm_BCInPhieu()
        {
            InitializeComponent();
        }

        private void frm_TMP_DonhangNhap_Load(object sender, EventArgs e)
        {
            try
            {
                if (Mode == 0)
                {
                    this.Text = "Phiếu xuất kho";
                    DataTable dt = clsMain.ReturnDataTable("EXEC SP_INPHIEUKHO_CHITIET_PHIEUXUAT @MAPHIEUID='" + ma + "'");
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuXuat";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 1)
                {
                    this.Text = "Phiếu nhập kho";
                    DataTable dt = clsMain.ReturnDataTable("EXEC SP_INPHIEUKHO_CHITIET_PHIEUNHAP @MAPHIEUID='" + ma + "'");
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuNhap";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 2)
                {
                    this.Text = "Báo cáo tồn kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptTonKho";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 3)
                {
                    this.Text = "Báo cáo bán hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoBanHang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 4)
                {
                    this.Text = "Báo cáo kết quả kinh doanh";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoKQKD";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 5)
                {
                    this.Text = "Báo cáo doanh số";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDoanhSo";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 6)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 7)
                {
                    this.Text = "Phiếu nhập kho";
                    DataTable dt =dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuNhap";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 8)
                {
                    this.Text = "Phiếu xuất kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuXuat";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 9)
                {
                    this.Text = "Danh sách hàng hóa";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptHangHoa";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 10)
                {
                    this.Text = "Phiếu kiểm kê";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuKiemKe";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 12)
                {
                    this.Text = "Bảng giá theo khu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBanggiatheokhu";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 13)
                {
                    this.Text = "Cảnh báo tồn kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptTonKhoToiThieu";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 14)
                {
                    this.Text = "Danh Sách Khách Hàng Thân Thiết";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_KHTT";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 15)
                {
                    this.Text = "Danh Sách Khách Hàng Thanh toán nội bộ";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_KHTTNB";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 16)
                {
                    this.Text = "In vé trò chơi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.DataSource = dt;
                    if (dt.Rows[0]["MAYIN"].ToString() == "0")//VÉ
                    {
                        rp.ReportName = "rptVe";
                    }
                    else if (dt.Rows[0]["MAYIN"].ToString() == "1")//BILL
                    {
                        rp.ReportName = "rptVeKhuvuichoi";
                    }
                    else//MA VACH
                    {
                        rp.ReportName = "rptVeKhuvuichoi";
                    }
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");

                    if (dt.Rows[0]["MAYIN"].ToString() == "0")
                    {
                        rp.PrinterName = KP_Terminal.clsKP_Terminal.Mayinve;
                    }
                    else if (dt.Rows[0]["MAYIN"].ToString() == "1")
                    {
                        rp.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    }
                    else
                    {
                        rp.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                    }
                    rp.Show();
                }
                else if (Mode == 17)
                {
                    this.Text = "Báo cáo bán vé trò chơi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBanvetrochoi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 18)
                {
                    this.Text = "Phiếu thu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    if (cls_KP_RES.Mode == 1)
                    {
                        rp.ReportName = "rptLapPhieuThu_A5";
                    }
                    else
                    {
                        rp.ReportName = "rptLapPhieuThu_A5";
                    }
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 19)
                {
                    this.Text = "Phiếu chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    if (cls_KP_RES.Mode == 1)
                    {
                        rp.ReportName = "rptLapPhieuChi_A5";
                    }
                    else
                    {
                        rp.ReportName = "rptLapPhieuChi_A5";
                    }
                    
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 20)
                {
                    this.Text = "In Mã Khách Hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptInMaKH";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 21)
                {
                    this.Text = "Danh sách phiếu thu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoPhieuThu";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 22)
                {
                    this.Text = "Danh sách phiếu chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoPhieuChi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 23)
                {
                    this.Text = "Báo cáo công nợ nhà cung cấp";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoCongNoNCC";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 24)
                {
                    this.Text = "Báo cáo công nợ khách hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoCongNoKH";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 25)
                {
                    this.Text = "Báo Cáo Tổng Quan Công Nợ";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoCongNoTongQuan";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 26)
                {
                    this.Text = "Báo Cáo Quỹ Tiền Mặt";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptQuyTienMat";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 27)
                {
                    this.Text = "Báo Cáo Doanh Thu Karaoke";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDoanhThuKaraoke";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 28)
                {
                    //this.Text = "Thêm xuất chiếu";
                    //Frm_LaplichchieuNew frm = new Frm_LaplichchieuNew();
                    //frm.WindowState = FormWindowState.Maximized;
                    //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    //frm.TopLevel = false;
                    //frm.Parent = pnlMain;
                    //frm.Show();
                }
                else if (Mode == 29)
                {
                    this.Text = "Nhập kho";
                    DataTable dt = dtSource;
                    Frm_Nhapkho frm = new Frm_Nhapkho(dt);
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 30)
                {
                    this.Text = "Xuất kho";
                    DataTable dt = dtSource;
                    Frm_Xuatkho frm = new Frm_Xuatkho(dt);
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 31)
                {
                    this.Text = "In bảng giá";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBanggiarapphim";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 32)
                {
                    this.Text = "In lịch chiếu phim";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    if (ma=="0")
                    {
                        rp.ReportName = "rptLichchieuphim";
                    }
                    else if (ma == "1")
                    {
                        rp.ReportName = "rptLichchieuphim1";
                    }
                    else if (ma == "2")
                    {
                        rp.ReportName = "rptLichchieuphim2";
                    }
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 33)
                {
                    this.Text = "Báo cáo bán vé";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptTonghopve";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 34)
                {
                    this.Text = "Báo cáo hủy vé";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaocaohuyrapphim";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 35)
                {
                    this.Text = "Báo cáo doanh thu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    if (ma == "0")
                    {
                        rp.ReportName = "rptDoanhthuve";
                    }
                    else if (ma == "1")
                    {
                        rp.ReportName = "rptThongkeveNPH";
                    }
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 36)
                {
                    this.Text = "Báo cáo đặt vé";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptThongkedatve";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 37)
                {
                    this.Text = "Chi tiết rạp";
                    Frm_Chitietrap frm = new Frm_Chitietrap();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.sma = ma;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 38)
                {
                    this.Text = "Chi tiết phim";
                    Frm_Chitietphim frm = new Frm_Chitietphim();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.sma = ma;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 39)
                {
                    this.Text = "Báo cáo bán hàng Combo";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoCombo";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 40)
                {
                    this.Text = "Báo cáo kết quả chiếu phim";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptKetquachieuphim";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 41)
                {
                    this.Text = "Báo cáo bán vé trước";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptTonghopve_truoc";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 42)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_ban";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 43)
                {
                    this.Text = "Định lượng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDinhluong";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 44)
                {
                    this.Text = "Danh sách Combo";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptCombo";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 45)
                {
                    this.Text = "Danh sách người dùng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptUser";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 46)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_Hoadon";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 47)
                {
                    this.Text = "Danh sách hóa đơn đã hủy";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_HoadonCancel";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 48)
                {
                    this.Text = "Danh sách hóa đơn in lại";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_HoadonInlai";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 49)
                {
                    this.Text = "Danh sách phiếu nhập";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_PhieuNhap";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 50)
                {
                    this.Text = "Danh sách phiếu xuất";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_PhieuXuat";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 51)
                {
                    this.Text = "Danh sách phiếu kiểm kê";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_PhieuKiemKe";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 52)
                {
                    this.Text = "Báo cáo tổng hợp khu vui chơi + nhà hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBanvetrochoi_nhahang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 53)
                {
                    this.Text = "Địa điểm";
                    Frm_Khuvuc frm = new Frm_Khuvuc();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 54)
                {
                    this.Text = "Nước sản xuất";
                    Frm_NuocSanXuat frm = new Frm_NuocSanXuat();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 55)
                {
                    this.Text = "Thể loại phim";
                    Frm_TheLoaiPhim frm = new Frm_TheLoaiPhim();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 56)
                {
                    this.Text = "Công nghệ làm phim";
                    Frm_CongNghe frm = new Frm_CongNghe();
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 57)
                {
                    this.Text = "Thống kê doanh thu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBCDoanhthu_TungNam";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 58)
                {
                    this.Text = "Thống kê doanh thu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBCDoanhthu_TungThang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 59)
                {
                    this.Text = "Thống kê hàng bán chạy";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBCHangBanChay";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 60)
                {
                    this.Text = "Lịch sử giao dịch";
                    Frm_Lichsu_Diem frm = new Frm_Lichsu_Diem(ma);
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 61)
                {
                    this.Text = "Lịch sử giao dịch";
                    Frm_Lichsu_Tien frm = new Frm_Lichsu_Tien(ma);
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.TopLevel = false;
                    frm.Parent = pnlMain;
                    frm.Show();
                }
                else if (Mode == 62)
                {
                    this.Text = "Lịch sử giao dịch";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptLSGD_KHTT";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 63)
                {
                    this.Text = "Lịch sử đổi quà";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptLSDQ_KHTT";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 64)
                {
                    this.Text = "Danh sách đổi quà";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_DS_DoiQua";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 65)
                {
                    this.Text = "Báo cáo bán vé Online";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoBanVeOnline";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 66)
                {
                    this.Text = "Báo cáo lịch sử giao dịch KHTT";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_LS_GiaoDich";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 67)
                {
                    this.Text = "Bảng kê chi tiết bán vé";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBangKeChiTietBanVe";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 68)
                {
                    this.Text = "Báo cáo bán hàng online";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoHangHoaOnline";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 69)
                {
                    this.Text = "Danh sách công văn đến";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_CongVanDen";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 70)
                {
                    this.Text = "Danh sách công văn đi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_CongVanDi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 71)
                {
                    this.Text = "Lịch sử đổi mã KHTT";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoLichSuDoiMa";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 72)
                {
                    this.Text = "Chi tiết voucher";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDanhSachVoucher";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 73)
                {
                    this.Text = "Tổng hợp voucher";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoTongHopVoucher";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 74)
                {
                    this.Text = "Lịch sử lịch chiếu";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoLichSuLichChieu";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 75)
                {
                    this.Text = "Danh sách ứng viên";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoUngVien";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 76)
                {
                    this.Text = "Danh sách nhân viên";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoNhanVien";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 77)
                {
                    this.Text = "Danh sách hợp đồng lao động";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoHopDongLaoDong";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 78)
                {
                    this.Text = "Danh sách khen thưởng - kỷ luật";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoKhenThuongKyLuat";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 79)
                {
                    this.Text = "Bảng lương";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoLuongThang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 80)
                {
                    this.Text = "Báo cáo tổng hợp bán hàng theo khách hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoTongHopBanHangKH";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 81)
                {
                    this.Text = "Biên bản giao nhận";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBienBanGiaoNhan";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 82)
                {
                    this.Text = "Danh sách biên bản giao nhận";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_BienBanGiaoNhan";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 83)
                {
                    this.Text = "Chi tiết đánh giá khách hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDanhGiaKH";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 84)
                {
                    this.Text = "Biểu đồ đánh giá khách hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBieuDoDanhGiaKH";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 85)
                {
                    this.Text = "Thống kê thu chi theo năm";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBCThuChi_TungNam";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 86)
                {
                    this.Text = "Thống kê thu chi theo tháng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBCThuChi_TungThang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 87)
                {
                    this.Text = "Danh sách phiếu đề nghị chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_DS_PhieuDNChi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 88)
                {
                    this.Text = "Phiếu ủy nhiệm chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptUyNhiemChi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 89)
                {
                    this.Text = "Danh sách phiếu ủy nhiệm chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoUyNhiemChi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 90)
                {
                    this.Text = "Danh sách mã khuyến mãi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoTheKhuyenMai";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 91)
                {
                    this.Text = "Danh sách hợp đồng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoSoTheoDoi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 92)
                {
                    this.Text = "Danh sách chi hợp đồng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoChiTheoDoi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 93)
                {
                    this.Text = "Danh sách khách";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_KS_Danhsachkhach";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 94)
                {
                    this.Text = "Danh sách thuê phòng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_KS_Danhsachthuephong";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 95)
                {
                    this.Text = "Báo cáo số lượng khách";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCao_Soluongkhach";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 96)
                {
                    this.Text = "Danh sách hủy bàn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_BanHuy";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 97)
                {
                    this.Text = "Danh sách hủy món";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDanhSachMonHuy";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 98)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_ban_giaohang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 99)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_giaohang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 100)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_thaomoc";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
				else if (Mode == 101)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_giaohang_thaomoc";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 102)
                {
                    this.Text = "Báo cáo dịch vụ - nhân viên thực hiện";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoBanHang1";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 103)
                {
                    this.Text = "Báo cáo bán hàng";
                    Frm_BC_Banhang Frm = new Frm_BC_Banhang();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 104)
                {
                    this.Text = "Báo cáo tồn kho";
                    Frm_TonKhoNew Frm = new Frm_TonKhoNew();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 105)
                {
                    this.Text = "Thêm khách hàng thân thiết";
                    Frm_Khachhang Frm = new Frm_Khachhang();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 106)
                {
                    this.Text = "Phát thưởng khách hàng thân thiết";
                    Frm_DoiDiemThuong Frm = new Frm_DoiDiemThuong();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 107)
                {
                    this.Text = "Báo cáo trả hàng";
                    Frm_BC_Trahang Frm = new Frm_BC_Trahang();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 108)
                {
                    this.Text = "Báo cáo danh sách trả hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_HoaDonTraHang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 109)
                {
                    this.Text = "Báo cáo nhập kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_BaoCaoHangNhapKho";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 110)
                {
                    this.Text = "Báo cáo xuất kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_BaoCaoHangXuatKho";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 111)
                {
                    this.Text = "Báo cáo xuất kho";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_BaoCaoHangXuatKho";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 112)
                {
                    this.Text = "Báo cáo trả hàng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoTraHang";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                // 21/07 khang xuất hóa đơn khách sạn
                else if (Mode == 113)
                {
                    this.Text = "Hóa đơn khách sạn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadonKhachSan";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
               // 21/07 khang xuất danh sách hóa đơn khách sạn
                else if (Mode == 114)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_Hoadon_Khachsan";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                // 25/07 khang danh sách hủy dịch vụ
                else if (Mode == 115)
                {
                    this.Text = "Danh sách hủy dịch vụ";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDanhSachDichVuHuy";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                //25/07 khang danh sách phòng hủy
                else if (Mode == 116)
                {
                    this.Text = "Danh sách hủy Phòng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_PhongHuy";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                //28/07 khang danh sách dịch vụ khách hàng
                else if (Mode == 117)
                {
                    this.Text = "Báo cáo dịch vụ - khách hàng thực hiện";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoBanHang2";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                //01/08 khang Phiếu xuất kho có SL Tồn
                if (Mode == 118)
                {
                    this.Text = "Phiếu xuất kho";
                    DataTable dt = clsMain.ReturnDataTable("EXEC SP_INPHIEUKHO_CHITIET_PHIEUXUAT2 @MAPHIEUID='" + ma + "'");
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuXuat2";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 119)
                {
                    this.Text = "Danh sách hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    //rp.ReportName = "rpt_inhoadon_Bill_Karaoke";
                    rp.ReportName = "rpt_inhoadonKaraoke";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                if (Mode == 120)
                {
                    this.Text = "Phiếu đề nghị lập phiếu chi";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptPhieuDeNghiLapPhieuChi";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                if (Mode == 121)
                {
                    this.Text = "Danh sách lấy lại hóa đơn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_LayLaiHD";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 122)
                {
                    this.Text = "Hóa đơn bán hàng trà sữa";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rpt_inhoadon_Bill_TraSua";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 123)
                {
                    this.Text = "Báo cáo doanh số tổng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoDoanhSo_Tong";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 124)
                {
                    this.Text = "Báo cáo bán hàng tổng";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptBaoCaoBanHang_Tong";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }
                else if (Mode == 125)
                {
                    this.Text = "Báo cáo bán hàng";
                    Frm_BC_Banhang3 Frm = new Frm_BC_Banhang3();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 126)
                {
                    this.Text = "Hết món";
                    Frm_HangHoaTamKhoa Frm = new Frm_HangHoaTamKhoa();
                    Frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    Frm.WindowState = FormWindowState.Maximized;
                    Frm.TopLevel = false;
                    Frm.Parent = pnlMain;
                    Frm.Show();
                }
                else if (Mode == 127)
                {
                    this.Text = "Danh sách hóa đơn khách sạn";
                    DataTable dt = dtSource;
                    Frm_Report1 rp = new Frm_Report1();
                    rp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    rp.WindowState = FormWindowState.Maximized;
                    rp.TopLevel = false;
                    rp.Parent = pnlMain;
                    rp.ReportName = "rptDS_Hoadon_Khachsan1";
                    rp.ExportName = rp.ReportName + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year.ToString("0000") + "_" + DateTime.Now.Hour.ToString("00") + "_" + DateTime.Now.Minute.ToString("00");
                    rp.DataSource = dt;
                    rp.Show();
                }

                title.Text = this.Text;
            }
            catch
            {
            }
        }
        
        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }   
}
