﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraCharts;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_TK_Hangbanchay : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_TK_Hangbanchay()
        {
            InitializeComponent();
            dtpTuNgay.EditValue = clsGlobal.gdServerDate.Date;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void Loadcheckbox()
        {
            dt = clsMain.ReturnDataTable("SELECT MA_NHOMHANG AS MA,TEN_NHOMHANG AS TEN FROM NHOMHANG WHERE SUDUNG='True' AND THUCDON='True'");
            dt.Rows.Add("0", "Tất cả");

            cboData.Properties.DataSource = dt;
            cboData.EditValue = int.Parse("0");   
        }
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            chart.Series.Clear();
            chart.Titles.Clear();

            String sSQL = "";
            sSQL += "EXEC SP_BAOCAO_HANGBANCHAY ";
            sSQL += clsMain.SQLString(cboData.EditValue.ToString()) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy HH:mm}", dtpTuNgay.EditValue)) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy HH:mm}", dtpDenNgay.EditValue)) + ",";
            sSQL += "0";

            DataTable myDT = clsMain.ReturnDataTable(sSQL);

            chart.DataSource = myDT;
            ChartTitle ct = new ChartTitle();
            ct.Text = _tkhbc.ToUpper() + " " +string.Format("{0: dd/MM/yyyy HH:mm}", dtpTuNgay.EditValue) + " - " + string.Format("{0: dd/MM/yyyy HH:mm}", dtpDenNgay.EditValue) ;
            ct.Font = new Font("Tahoma", 20,FontStyle.Bold);
            Series series = new Series("Series1", ViewType.Bar);
            chart.Series.Add(series);
            chart.Titles.Add(ct);
            series.LegendText = _soluong;
            series.ArgumentDataMember = "TEN_HANGHOA";
            series.ValueDataMembers.AddRange("SL");
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        string _soluong = "";
        string _tkhbc = "";
        private void Frm_TK_Doanhso_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("thongkebanhang", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            lbNhomHang.Text = rm.GetString("nhomhang", culture);
            _soluong = rm.GetString("soluong2", culture);
            lb_From.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            _tkhbc = rm.GetString("tkhbc", culture);
            
            Loadcheckbox();
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataColumn dc2 = new DataColumn();
            dc2.ColumnName = "TEMP";
            dc2.DefaultValue = "Temp";
            dt.Columns.Add(dc2);
            dt.Rows.Add("TEMP");

            Bitmap bmp = new Bitmap(panelControl5.ClientRectangle.Width, panelControl5.ClientRectangle.Height);
            panelControl5.DrawToBitmap(bmp, panelControl5.ClientRectangle);

            DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
            colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
            colIMAGE.AllowDBNull = true;
            colIMAGE.Caption = "IMAGE";
            colIMAGE.DefaultValue = ImageToByteArray(bmp);
            dt.Columns.Add(colIMAGE);

            DataColumn dc1 = new DataColumn();
            dc1.ColumnName = "NGAY";
            dc1.DefaultValue = "(Từ " + string.Format("{0: dd/MM/yyyy HH:mm}", dtpTuNgay.EditValue) + " đến " + string.Format("{0: dd/MM/yyyy HH:mm}", dtpDenNgay.EditValue) + ")";
            dt.Columns.Add(dc1);

            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.dtSource = dt;
            frm.Mode = 59;
            frm.ShowDialog();
            frm.Dispose();
                
            
        }

        private void btnXuatFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChartControl chart = (ChartControl)panelControl5.Controls[0];
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                chart.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                chart.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }
    }
}