﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_BCChitietKiemke : Form
    {
        public Frm_BCChitietKiemke(string ma)
        {
            InitializeComponent();
            string sql = @"SELECT PK.MA_KHO,TEN_KHO,PK.MA_HANGHOA,TEN_HANGHOA,TEN_DONVITINH,PK.SOLUONG
                           FROM KHO_KIEMKE PK,KHO K,HANGHOA H,DONVITINH D
                           WHERE PK.MA_KHO=K.MA_KHO AND PK.MA_HANGHOA=H.MA_HANGHOA AND H.MA_DONVITINH=D.MA_DONVITINH AND MAKK='" + ma + "'";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl1.DataSource = dt;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }   
}
