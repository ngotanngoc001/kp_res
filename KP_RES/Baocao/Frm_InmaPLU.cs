﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraSplashScreen;
using System.Data.OleDb;
using System.Diagnostics;

namespace KP_RES 
{
    public partial class Frm_InmaPLU : DevExpress.XtraEditors.XtraForm
    {
        public Frm_InmaPLU()
        {
            InitializeComponent();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnData.Visible = (optGroup.SelectedIndex != 5 && optGroup.SelectedIndex != 6) ? false : true;

            if (optGroup.SelectedIndex == 5)
            {
                lblData.Text = "Nhóm hàng";
                DataTable myDT = new DataTable();
                myDT = clsMain.ReturnDataTable("SELECT MA_NHOMHANG AS MA,TEN_NHOMHANG AS TEN FROM NHOMHANG WHERE SUDUNG = 1 ORDER BY TEN");
                cboData.Properties.DataSource = myDT;
                cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
            }
            else if (optGroup.SelectedIndex == 6)
            {
                lblData.Text = "Hàng hóa";
                DataTable myDT = new DataTable();
                myDT = clsMain.ReturnDataTable("SELECT MA_HANGHOA AS MA, TEN_HANGHOA AS TEN FROM HANGHOA WHERE SUDUNG = 1 ORDER BY TEN");
                cboData.Properties.DataSource = myDT;
                cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
            }
        }

        bool bCheck = false;
        private void btnCheckall_Click(object sender, EventArgs e)
        {
            if (bCheck)
            {
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    gridView1.SetRowCellValue(i, gridView1.Columns["ICHECK"], 0);
                }
            }
            else
            {
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    gridView1.SetRowCellValue(i, gridView1.Columns["ICHECK"], 1);
                }
            }
            bCheck = !bCheck;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                gridView1.ActiveEditor.SelectAll();
            }));
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";
                if (optGroup.SelectedIndex == 5 || optGroup.SelectedIndex == 6)
                    sMa = cboData.EditValue.ToString();

                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_IN_PLU ";
                sSQL += clsMain.SQLString(sMa) + ",";
                sSQL += clsMain.SQLString("") + ",";
                sSQL += clsMain.SQLString("") + ",";
                sSQL += optGroup.SelectedIndex;

                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                myDT.Columns.Add("SOLUONG");
                foreach (DataRow dr in myDT.Rows)
                    dr["SOLUONG"] = 1;
                gridControl1.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                try
                {
                    DataTable dt = ((DataView)gridView1.DataSource).ToTable();
                    dt.DefaultView.RowFilter = "ICHECK=1";
                    dt = dt.DefaultView.ToTable();

                    DataTable dtOk = new DataTable();
                    dtOk.Columns.Add("Department");
                    dtOk.Columns.Add("PLU");
                    dtOk.Columns.Add("DonViTinh");
                    dtOk.Columns.Add("MaHang");
                    dtOk.Columns.Add("TenHang");
                    dtOk.Columns.Add("Gia");
                    dtOk.Columns.Add("HanSuDung");

                    foreach (DataRow dr in dt.Rows)
                    {
                        dtOk.Rows.Add("1", dr["PLU"].ToString(), "1", dr["MAVACH"].ToString(), KP_Terminal.clsKP_Terminal.RemoveUnicode(dr["TEN_HANGHOA"].ToString().Replace(",", ".")), dr["GIABAN1"].ToString(), dr["HANSUDUNG"].ToString());
                    }

                    StringBuilder sb = new StringBuilder();
                    foreach (DataRow row in dtOk.Rows)
                    {
                        for (int i = 0; i < dtOk.Columns.Count; i++)
                        {
                            if (i == dtOk.Columns.Count - 1)
                            {
                                sb.Append((row[i].ToString() == "" ? "1" : row[i].ToString()));
                            }
                            else
                            {
                                sb.Append((row[i].ToString() == "" ? "1" : row[i].ToString()) + ",");
                            }
                        }
                        sb.AppendLine();
                    }

                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "CSV (.csv)|*.csv";
                        if (saveDialog.ShowDialog() != DialogResult.Cancel)
                        {
                            string exportFilePath = saveDialog.FileName;
                            File.WriteAllText(exportFilePath, sb.ToString());
                        }
                    }
                    XtraMessageBox.Show("Xuất mã PLU thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}