﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using KP_RES.Class;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_DSHoaDonCancel : DevExpress.XtraEditors.XtraForm
    {
        DataTable myDT;
        string Kytu_Combo = "   *  ";
        string Kytu_Monthem = "       +  ";
        string sIn_Number_Fastfood = "";
        public Frm_DSHoaDonCancel()
        {
            InitializeComponent();
            dtpTuNgay.EditValue = clsGlobal.gdServerDate;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                FILL.Visible = true ;
            }
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception(_chuachondtcanxem);
                string sql = "select * from CT_TIENGIOKARAOKE where MA_HOADON = '" + gridView1.GetFocusedRowCellValue(IDBILL).ToString() + "'";
                DataTable dtKaraoke = clsMain.ReturnDataTable(sql);
                if (dtKaraoke.Rows.Count > 0)//Hóa don Karaoke, có ti?n gi?
                {
                    Frm_BCChitietHoaDon_Karaoke frm = new Frm_BCChitietHoaDon_Karaoke(gridView1.GetFocusedRowCellValue(IDBILL).ToString());
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    Frm_BCChitietHoaDon frm = new Frm_BCChitietHoaDon(gridView1.GetFocusedRowCellValue(IDBILL).ToString());
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.ShowDialog();
                    frm.Dispose();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    string sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU,C.TEN AS TENKH,ISNULL(C.MA,'') AS MA_KHACHHANG,ISNULL(C.DIACHI,'') AS DIACHIGIAOHANG,ISNULL(C.DIENTHOAI,'') AS DIENTHOAI_KH,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) AS ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON, hd.MA_QUAY as MA_QUAY1 , Q.TEN_QUAY AS MA_QUAY ,ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'HÓA ĐƠN BÁN LẺ \n (HỦY)' as Ten_HoaDon,";
                    sSQL += "ISNULL(cthd.TEM,0) AS TEM, ISNULL(cthd.MACP,'C') AS MACP, ISNULL(cthd.Number,0) AS Number" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON inner join QUAY Q on hd.MA_QUAY = Q.MA_QUAY LEFT JOIN KHTT C ON hd.MA_KHACHHANG = C.MA" + "\n";
                    sSQL += "Where hd.MA_HOADON =" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(IDBILL).ToString());

                    DataTable dtReport;
                    dtReport = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INNUMBERFASTFOOD'");
                    sIn_Number_Fastfood = dtReport.Rows[0][0].ToString();

                    dtReport = clsMain.ReturnDataTable(sSQL);
                    if (sIn_Number_Fastfood != "0")
                    {
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            if (dtReport.Rows[i]["MACP"].ToString() == "P")
                            {
                                dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Monthem + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                            }
                            else if (dtReport.Rows[i]["MACP"].ToString() == "CBC")
                            {
                                dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Combo + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                            }
                        }
                    }

                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(IDBILL).ToString());
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        frm.Mode = (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "") ? 42 : 98;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        if (sIn_Number_Fastfood != "0")
                        {
                            frm.Mode = 121;
                        }
                        else if (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "")
                        {
                            frm.Mode = 6;
                        }
                        else
                            frm.Mode = 99;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
                panelControl8.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
                panelControl8.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboData.Properties.DataSource = null;
            cboData.Properties.Columns.Clear();
            cboData.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//cửa hàng
            {
                myDT = clsMain.ReturnDataTable("select MA_CUAHANG as ID,TEN_CUAHANG as NAME from CUAHANG where SUDUNG=1 order by NAME");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, myDT);
                lblData.Text = _cuahang;
            }
            else if (optGroup.SelectedIndex == 1)//quầy
            {
                myDT = clsMain.ReturnDataTable("select MA_QUAY as ID,TEN_QUAY as NAME from QUAY where SUDUNG=1 order by NAME");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên quầy" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, myDT);
                lblData.Text = _quay;
            }
            else if (optGroup.SelectedIndex == 2)//nhóm hàng
            {
                myDT = clsMain.ReturnDataTable("select MA_NHOMHANG as ID,TEN_NHOMHANG as NAME from NHOMHANG where SUDUNG=1 order by NAME");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, myDT);
                lblData.Text = _nhomhang;
            }
            else if (optGroup.SelectedIndex == 3)//hàng hóa
            {
                myDT = clsMain.ReturnDataTable("select MA_HANGHOA as ID,TEN_HANGHOA as NAME from HANGHOA where SUDUNG=1 order by NAME");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên mặt hàng" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, myDT);
                lblData.Text = _mathang;
            }
            else if (optGroup.SelectedIndex == 4)//nhân viên
            {
                myDT = clsMain.ReturnDataTable("select MANHANVIEN as ID,TENNHANVIEN as NAME from DM_NHANVIEN where SUDUNG=1 order by NAME");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "ID", "NAME" }, new bool[] { true, true }, myDT);
                lblData.Text = _nhanvien;
            }
            cboData.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboData.Properties.Columns.Add(info);
                }
                cboData.Properties.BestFitMode = BestFitMode.None;
                cboData.Properties.SearchMode = SearchMode.AutoFilter;
                cboData.Properties.DisplayMember = FieldName[1];
                cboData.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboData.Properties.Columns[0].Width = 100;
                    cboData.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboData.Properties.Columns[1].Width = 100;
                    cboData.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboData.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboData.EditValue == null || cboData.EditValue == DBNull.Value)
                    throw new Exception(_chuachondtcanxem);

                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_HOADON_HUY ";
                sSQL += clsMain.SQLString(cboData.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue)) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue)) + ",";
                sSQL += optGroup.SelectedIndex;

                myDT = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboData.Text;
                    if (ContainColumn("FILTER", myDT) == false)
                        myDT.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TODATE";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue);
                    if (ContainColumn("TODATE", myDT) == false)
                        myDT.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "FROMDATE";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue);
                    if (ContainColumn("FROMDATE", myDT) == false)
                        myDT.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 47;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }
       
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        string _thongbao = "";
        string _cuahang = "";
        string _quay = "";
        string _nhomhang = "";
        string _mathang = "";
        string _nhanvien = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        string _chuachonhddexem = "";
        private void Frm_DSHoaDonCancel_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("danhsachhoadonhuy", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["IDBILL"].Caption = rm.GetString("mahoadon", culture);
            gridView1.Columns["CASHIER"].Caption = rm.GetString("nhanvienban", culture);
            gridView1.Columns["CASHIER_CANCEL"].Caption = rm.GetString("nhanvienhuy", culture);
            gridView1.Columns["TRX_DATE"].Caption = rm.GetString("ngayban", culture);
            gridView1.Columns["TOTAL"].Caption = rm.GetString("tongtien", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            lb_From.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            optGroup.Properties.Items[0].Description = rm.GetString("theocuahang", culture);
            optGroup.Properties.Items[1].Description = rm.GetString("theoquay", culture);
            optGroup.Properties.Items[2].Description = rm.GetString("theonhomhang", culture);
            optGroup.Properties.Items[3].Description = rm.GetString("theohanghoa", culture);
            optGroup.Properties.Items[4].Description = rm.GetString("theonhanvien", culture);
            _chuachondtcanxem = rm.GetString("chuachondtcanxem", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _cuahang = rm.GetString("cuahang", culture);
            _quay = rm.GetString("quay", culture);
            _nhomhang = rm.GetString("nhomhang", culture);
            _mathang = rm.GetString("mathang", culture);
            _nhanvien = rm.GetString("nhanvien", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);
            _chuachonhddexem = rm.GetString("chuachonhddexem", culture);
            LoadcboDATA();
        }
    }
}