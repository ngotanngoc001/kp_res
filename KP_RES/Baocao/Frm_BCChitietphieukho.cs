﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_BCChitietphieukho : Form
    {
        int _Nhap_Xuat;

        public Frm_BCChitietphieukho(string ma,int nhapxuat)
        {
            InitializeComponent();
            string sql = "select HANGHOA,TEN_HANGHOA,TEN_DONVITINH,CTPK.THUE,CTPK.SOLUONG,DONGIA,CTPK.SOLUONG*DONGIA as THANHTIENTCK,TIENTHUE,THANHTIEN,TONGCONG,";
            sql += "GIAVON,CHIETKHAU,TIENCHIETKHAU from CHITIETPHIEUKHO CTPK,HANGHOA H where CTPK.HANGHOA=H.MA_HANGHOA and PHIEUKHO=" + Convert.ToInt32(ma);
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl1.DataSource = dt;
            _Nhap_Xuat = nhapxuat;
        }

        private void frm_TMP_DonhangNhap_Load(object sender, EventArgs e)
        {
            if (_Nhap_Xuat == 1) //1: Nhập, 0: Xuất
            {
                GIAVON.Visible = true;
                CHIETKHAU.Visible = true;
                TIENCHIETKHAU.Visible = true;
                THANHTIENTCK.Visible = true;
                GIAVON.Visible = false;
                DONGIA.Caption = "Giá nhập";
                this.Text = "Chi tiết phiếu nhập";
            }
            else if (_Nhap_Xuat == 0)
            {
                GIAVON.Visible = false;
                CHIETKHAU.Visible = true;
                TIENCHIETKHAU.Visible = true;
                THANHTIENTCK.Visible = true;
                THANHTIEN.Caption = "Thành Tiền";
                DONGIA.Caption = "Giá Xuất";
                this.Text = "Chi tiết phiếu xuất";
            }

            title.Text = this.Text;
        }

        private void grv_dshhtmp_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }   
}
