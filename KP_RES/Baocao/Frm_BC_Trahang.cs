﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BC_Trahang : DevExpress.XtraEditors.XtraForm
    {
        DataTable myDT;
        public Frm_BC_Trahang()
        {
            InitializeComponent();
            dtpTuNgay.EditValue = clsGlobal.gdServerDate;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboData.Properties.DataSource = null;
            cboData.Properties.Columns.Clear();
            cboData.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)
            {
                myDT = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _cuahang;
            }
            else if (optGroup.SelectedIndex == 1)
            {
                myDT = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _kho;
            }
            else if (optGroup.SelectedIndex == 2)
            {
                myDT = clsMain.ReturnDataTable("select MA_QUAY as MA,TEN_QUAY as TEN from QUAY where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên quầy" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _quay;
            }
            else if (optGroup.SelectedIndex == 3)
            {
                myDT = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _nhomhang;
            }
            else if (optGroup.SelectedIndex == 4)
            {
                myDT = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên mặt hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _mathang;
            }
            else if (optGroup.SelectedIndex == 5)
            {
                myDT = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1 order by TEN");
                cboData.Properties.DataSource = myDT;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, myDT);
                lblData.Text = _nhanvien;
            }
            cboData.Properties.AutoSearchColumnIndex = 1;
        }
        
        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboData.Properties.Columns.Add(info);
                }
                cboData.Properties.BestFitMode = BestFitMode.None;
                cboData.Properties.SearchMode = SearchMode.AutoFilter;
                cboData.Properties.DisplayMember = FieldName[1];
                cboData.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboData.Properties.Columns[0].Width = 100;
                    cboData.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboData.Properties.Columns[1].Width = 100;
                    cboData.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboData.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }
        
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboData.EditValue == null || cboData.EditValue == DBNull.Value)
                    throw new Exception(_chuachondtcanxem);

                String sSQL = "";
                sSQL += "EXEC SP_BAOCAO_TRAHANG ";
                sSQL += clsMain.SQLString(cboData.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue)) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue)) + ",";
                sSQL += optGroup.SelectedIndex;

                myDT = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {

                    DataColumn colFILTER = new DataColumn();
                    colFILTER.ColumnName = "FILTER";
                    colFILTER.DataType = System.Type.GetType("System.String");
                    colFILTER.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboData.Text;
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTuNgay.Text;
                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenNgay.Text;

                    if (ContainColumn("FILTER", myDT) == false)
                        myDT.Columns.Add(colFILTER);
                    if (ContainColumn("TUNGAY", myDT) == false)
                        myDT.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", myDT) == false)
                        myDT.Columns.Add(colDENNGAY);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 112;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
                panelControl8.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
                panelControl8.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        string _thongbao = "";
        string _cuahang = "";
        string _kho = "";
        string _quay = "";
        string _nhomhang = "";
        string _mathang = "";
        string _nhanvien = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        string _chuachonhddexem = "";
        private void Frm_BC_Trahang_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("baocaobanhang", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gridView1.Columns["TEN_DONVITINH"].Caption = rm.GetString("dvt1", culture);
            gridView1.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView1.Columns["DONGIA"].Caption = rm.GetString("dongia", culture);
            gridView1.Columns["THANHTIEN"].Caption = rm.GetString("thanhtien", culture);
            gridView1.Columns["TIENTHUE"].Caption = rm.GetString("tienthue", culture);
            gridView1.Columns["TIENPHUTHU"].Caption = rm.GetString("phuthu1", culture);
            gridView1.Columns["TIENCHIETKHAU"].Caption = rm.GetString("chietkhau", culture);
            gridView1.Columns["TONGCONG"].Caption = rm.GetString("tongcong1", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            lb_From.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            _chuachondtcanxem = rm.GetString("chuachondtcanxem", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _cuahang = rm.GetString("cuahang", culture);
            _kho = rm.GetString("kho", culture);
            _quay = rm.GetString("quay", culture);
            _nhomhang = rm.GetString("nhomhang", culture);
            _mathang = rm.GetString("mathang", culture);
            _nhanvien = rm.GetString("nhanvien", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);
            _chuachonhddexem = rm.GetString("chuachonhddexem", culture);
            optGroup.Properties.Items[0].Description = rm.GetString("theocuahang", culture);
            optGroup.Properties.Items[1].Description = rm.GetString("theokho", culture);
            optGroup.Properties.Items[2].Description = rm.GetString("theoquay", culture);
            optGroup.Properties.Items[3].Description = rm.GetString("theonhomhang", culture);
            optGroup.Properties.Items[4].Description = rm.GetString("theohanghoa", culture);
            optGroup.Properties.Items[5].Description = rm.GetString("theonhanvien", culture);

            LoadcboDATA();
        }
    }
}