﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_DSLayLaiHDChiTiet : Form
    {
        public Frm_DSLayLaiHDChiTiet(String ma)
        {
            InitializeComponent();

            string SQL = "";
            SQL = "EXEC SP_SelectChiTietLayLaiHD " + clsMain.SQLString(ma);
            DataTable dt = clsMain.ReturnDataTable(SQL);
            gridControl1.DataSource = dt;
        }
        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_DSHuyBanChiTiet_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("danhsachhoadon", culture);
            title.Text = rm.GetString("danhsachhoadon", culture);
            gridView1.Columns["STTHH"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gridView1.Columns["TEN_DONVITINH"].Caption = rm.GetString("dvt1", culture);
            gridView1.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView1.Columns["DONGIA"].Caption = rm.GetString("dongia", culture);
            gridView1.Columns["THANHTIEN"].Caption = rm.GetString("thanhtien", culture);
            //gridView1.Columns["THUE"].Caption = rm.GetString("tongtien", culture);
            gridView1.Columns["TIENTHUE"].Caption = rm.GetString("tienthue", culture);
            gridView1.Columns["TIENPHUTHU"].Caption = rm.GetString("phuthu1", culture);
            gridView1.Columns["TIENCHIETKHAU"].Caption = rm.GetString("chietkhau", culture);
            gridView1.Columns["TONGCONG"].Caption = rm.GetString("tongcong1", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
        }
    }   
}
