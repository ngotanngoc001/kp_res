﻿namespace KP_RES 
{
    partial class Frm_BC_Banhang2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BC_Banhang2));
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KHACHHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_DONVITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHACHHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENPHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENCHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.lblData = new DevExpress.XtraEditors.LabelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.cboDATA = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.lb_From = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.lb_To = new DevExpress.XtraEditors.LabelControl();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.gridControl1);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(219, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1641, 687);
            this.panelControl5.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1598, 683);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.KHACHHANG,
            this.DIENTHOAI,
            this.TEN_HANGHOA,
            this.TEN_DONVITINH,
            this.SOLUONG,
            this.DONGIA,
            this.THANHTIEN,
            this.TIENPHUTHU,
            this.TIENCHIETKHAU,
            this.TONGCONG,
            this.NHANVIEN,
            this.MA_HANGHOA,
            this.MAKHACHHANG,
            this.THUE,
            this.TIENTHUE,
            this.GHICHU1,
            this.FILL});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_HOADON.Caption = "Mã hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.True;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 150;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceCell.Options.UseFont = true;
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.Caption = "Ngày tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.OptionsColumn.ReadOnly = true;
            this.NGAYTAO.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.True;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 2;
            this.NGAYTAO.Width = 100;
            // 
            // KHACHHANG
            // 
            this.KHACHHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KHACHHANG.AppearanceCell.Options.UseFont = true;
            this.KHACHHANG.AppearanceCell.Options.UseTextOptions = true;
            this.KHACHHANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHACHHANG.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.KHACHHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KHACHHANG.AppearanceHeader.Options.UseFont = true;
            this.KHACHHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.KHACHHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KHACHHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHACHHANG.Caption = "Khách hàng";
            this.KHACHHANG.FieldName = "KHACHHANG";
            this.KHACHHANG.Name = "KHACHHANG";
            this.KHACHHANG.OptionsColumn.AllowEdit = false;
            this.KHACHHANG.OptionsColumn.AllowFocus = false;
            this.KHACHHANG.OptionsColumn.FixedWidth = true;
            this.KHACHHANG.Visible = true;
            this.KHACHHANG.VisibleIndex = 3;
            this.KHACHHANG.Width = 200;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIENTHOAI.AppearanceCell.Options.UseFont = true;
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DIENTHOAI.Caption = "Số điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.True;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 4;
            this.DIENTHOAI.Width = 150;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 5;
            this.TEN_HANGHOA.Width = 252;
            // 
            // TEN_DONVITINH
            // 
            this.TEN_DONVITINH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_DONVITINH.AppearanceCell.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_DONVITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_DONVITINH.AppearanceHeader.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_DONVITINH.Caption = "ĐVT";
            this.TEN_DONVITINH.FieldName = "TEN_DONVITINH";
            this.TEN_DONVITINH.Name = "TEN_DONVITINH";
            this.TEN_DONVITINH.OptionsColumn.AllowEdit = false;
            this.TEN_DONVITINH.OptionsColumn.AllowFocus = false;
            this.TEN_DONVITINH.OptionsColumn.FixedWidth = true;
            this.TEN_DONVITINH.Visible = true;
            this.TEN_DONVITINH.VisibleIndex = 6;
            this.TEN_DONVITINH.Width = 85;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 7;
            this.SOLUONG.Width = 46;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DONGIA.AppearanceCell.Options.UseFont = true;
            this.DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.Caption = "Đơn Giá";
            this.DONGIA.DisplayFormat.FormatString = "{0:#,###0}";
            this.DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowEdit = false;
            this.DONGIA.OptionsColumn.AllowFocus = false;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 8;
            this.DONGIA.Width = 96;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.Caption = "Thành Tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 9;
            this.THANHTIEN.Width = 113;
            // 
            // NHANVIEN
            // 
            this.NHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NHANVIEN.AppearanceCell.Options.UseFont = true;
            this.NHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.NHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIEN.Caption = "Nhân viên thực hiện";
            this.NHANVIEN.FieldName = "NHANVIEN";
            this.NHANVIEN.MinWidth = 180;
            this.NHANVIEN.Name = "NHANVIEN";
            this.NHANVIEN.OptionsColumn.AllowEdit = false;
            this.NHANVIEN.OptionsColumn.AllowFocus = false;
            this.NHANVIEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.NHANVIEN.Visible = true;
            this.NHANVIEN.VisibleIndex = 13;
            this.NHANVIEN.Width = 180;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "MA_HANGHOA";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            this.MA_HANGHOA.OptionsColumn.FixedWidth = true;
            // 
            // MAKHACHHANG
            // 
            this.MAKHACHHANG.Caption = "Mã khách hàng";
            this.MAKHACHHANG.FieldName = "MAKHACHHANG";
            this.MAKHACHHANG.Name = "MAKHACHHANG";
            this.MAKHACHHANG.Width = 100;
            // 
            // THUE
            // 
            this.THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THUE.AppearanceCell.Options.UseFont = true;
            this.THUE.AppearanceCell.Options.UseTextOptions = true;
            this.THUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THUE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUE.AppearanceHeader.Options.UseFont = true;
            this.THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.Caption = "VAT";
            this.THUE.DisplayFormat.FormatString = "{0:#,###0}%";
            this.THUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            this.THUE.OptionsColumn.AllowEdit = false;
            this.THUE.OptionsColumn.AllowFocus = false;
            this.THUE.OptionsColumn.FixedWidth = true;
            this.THUE.Width = 70;
            // 
            // TIENTHUE
            // 
            this.TIENTHUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENTHUE.AppearanceCell.Options.UseFont = true;
            this.TIENTHUE.AppearanceCell.Options.UseTextOptions = true;
            this.TIENTHUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENTHUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENTHUE.AppearanceHeader.Options.UseFont = true;
            this.TIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENTHUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENTHUE.Caption = "Tiền VAT";
            this.TIENTHUE.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENTHUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENTHUE.FieldName = "TIENTHUE";
            this.TIENTHUE.Name = "TIENTHUE";
            this.TIENTHUE.OptionsColumn.AllowEdit = false;
            this.TIENTHUE.OptionsColumn.AllowFocus = false;
            this.TIENTHUE.OptionsColumn.FixedWidth = true;
            this.TIENTHUE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENTHUE", "{0:#,###0}")});
            this.TIENTHUE.Width = 105;
            // 
            // TIENPHUTHU
            // 
            this.TIENPHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENPHUTHU.AppearanceCell.Options.UseFont = true;
            this.TIENPHUTHU.AppearanceCell.Options.UseTextOptions = true;
            this.TIENPHUTHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENPHUTHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENPHUTHU.AppearanceHeader.Options.UseFont = true;
            this.TIENPHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENPHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENPHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHUTHU.Caption = "Phụ thu";
            this.TIENPHUTHU.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENPHUTHU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENPHUTHU.FieldName = "TIENPHUTHU";
            this.TIENPHUTHU.Name = "TIENPHUTHU";
            this.TIENPHUTHU.OptionsColumn.AllowEdit = false;
            this.TIENPHUTHU.OptionsColumn.AllowFocus = false;
            this.TIENPHUTHU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENPHUTHU", "{0:#,###0}")});
            this.TIENPHUTHU.Visible = true;
            this.TIENPHUTHU.VisibleIndex = 10;
            this.TIENPHUTHU.Width = 98;
            // 
            // TIENCHIETKHAU
            // 
            this.TIENCHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENCHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.TIENCHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.TIENCHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENCHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENCHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.TIENCHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENCHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENCHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENCHIETKHAU.Caption = "Chiết Khấu";
            this.TIENCHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENCHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENCHIETKHAU.FieldName = "TIENCHIETKHAU";
            this.TIENCHIETKHAU.Name = "TIENCHIETKHAU";
            this.TIENCHIETKHAU.OptionsColumn.AllowEdit = false;
            this.TIENCHIETKHAU.OptionsColumn.AllowFocus = false;
            this.TIENCHIETKHAU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENCHIETKHAU", "{0:#,###0}")});
            this.TIENCHIETKHAU.Visible = true;
            this.TIENCHIETKHAU.VisibleIndex = 11;
            this.TIENCHIETKHAU.Width = 107;
            // 
            // TONGCONG
            // 
            this.TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TONGCONG.AppearanceCell.Options.UseFont = true;
            this.TONGCONG.AppearanceCell.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGCONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGCONG.Caption = "Tổng Cộng";
            this.TONGCONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGCONG.FieldName = "TONGCONG";
            this.TONGCONG.MinWidth = 150;
            this.TONGCONG.Name = "TONGCONG";
            this.TONGCONG.OptionsColumn.AllowEdit = false;
            this.TONGCONG.OptionsColumn.AllowFocus = false;
            this.TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0}")});
            this.TONGCONG.Visible = true;
            this.TONGCONG.VisibleIndex = 12;
            this.TONGCONG.Width = 150;
            // 
            // GHICHU1
            // 
            this.GHICHU1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GHICHU1.AppearanceCell.Options.UseFont = true;
            this.GHICHU1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU1.AppearanceHeader.Options.UseFont = true;
            this.GHICHU1.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU1.Caption = "Ghi chú";
            this.GHICHU1.FieldName = "GHICHU1";
            this.GHICHU1.Name = "GHICHU1";
            this.GHICHU1.OptionsColumn.AllowEdit = false;
            this.GHICHU1.OptionsColumn.AllowFocus = false;
            this.GHICHU1.OptionsColumn.FixedWidth = true;
            this.GHICHU1.Visible = true;
            this.GHICHU1.VisibleIndex = 14;
            this.GHICHU1.Width = 200;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Width = 200;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1600, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 683);
            this.panelControl2.TabIndex = 7;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 439);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 119);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 279);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 359);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 601);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 15;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.panelControl8);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(219, 687);
            this.panelControl1.TabIndex = 6;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl9);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 365);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(215, 285);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.lblData);
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Controls.Add(this.cboDATA);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 125);
            this.panelControl9.TabIndex = 8;
            // 
            // lblData
            // 
            this.lblData.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(3, 8);
            this.lblData.Margin = new System.Windows.Forms.Padding(4);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(68, 19);
            this.lblData.TabIndex = 0;
            this.lblData.Text = "Cửa hàng";
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(128, 71);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // cboDATA
            // 
            this.cboDATA.EnterMoveNextControl = true;
            this.cboDATA.Location = new System.Drawing.Point(3, 38);
            this.cboDATA.Name = "cboDATA";
            this.cboDATA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.Appearance.Options.UseFont = true;
            this.cboDATA.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDATA.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDATA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDATA.Properties.DisplayMember = "TEN";
            this.cboDATA.Properties.DropDownItemHeight = 25;
            this.cboDATA.Properties.DropDownRows = 30;
            this.cboDATA.Properties.NullText = "";
            this.cboDATA.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDATA.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboDATA.Properties.ShowHeader = false;
            this.cboDATA.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDATA.Properties.ValueMember = "MA";
            this.cboDATA.Size = new System.Drawing.Size(205, 26);
            this.cboDATA.TabIndex = 1;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 129);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(215, 236);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = true;
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Theo cửa hàng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo kho"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo quầy"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhóm hàng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo hàng hóa"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhân viên"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo khách hàng")});
            this.optGroup.Size = new System.Drawing.Size(211, 236);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.dtpTungay);
            this.panelControl8.Controls.Add(this.lb_From);
            this.panelControl8.Controls.Add(this.dtpDenngay);
            this.panelControl8.Controls.Add(this.lb_To);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 127);
            this.panelControl8.TabIndex = 0;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(6, 32);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(205, 26);
            this.dtpTungay.TabIndex = 1;
            // 
            // lb_From
            // 
            this.lb_From.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_From.Location = new System.Drawing.Point(6, 6);
            this.lb_From.Margin = new System.Windows.Forms.Padding(4);
            this.lb_From.Name = "lb_From";
            this.lb_From.Size = new System.Drawing.Size(59, 19);
            this.lb_From.TabIndex = 0;
            this.lb_From.Text = "Từ ngày";
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(6, 91);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(205, 26);
            this.dtpDenngay.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_To.Location = new System.Drawing.Point(6, 65);
            this.lb_To.Margin = new System.Windows.Forms.Padding(4);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(67, 19);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "Đến ngày";
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 650);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(215, 35);
            this.btnThunho.TabIndex = 5;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // Frm_BC_Banhang2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1860, 687);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_BC_Banhang2";
            this.Text = "Báo cáo dịch vụ - khách hàng sử dụng";
            this.Load += new System.EventHandler(this.Frm_BC_Banhang1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_DONVITINH;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TIENTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TONGCONG;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.LabelControl lblData;
        private DevExpress.XtraEditors.LookUpEdit cboDATA;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.LabelControl lb_From;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraEditors.LabelControl lb_To;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraGrid.Columns.GridColumn TIENCHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn TIENPHUTHU;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn KHACHHANG;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHACHHANG;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU1;




    }
}