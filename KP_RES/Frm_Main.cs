﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Metro.Navigation;
using System.IO;
using KP_UserManagement;
using KP_Terminal;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Data;
using System.Net;

namespace KP_RES 
{
    public partial class Frm_Main : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Main()
        {
            if (Process.GetProcessesByName("kingpos").Length > 1)
            {
                System.Environment.Exit(0);
            }
            InitializeComponent();
            this.defaultLookAndFeel1.LookAndFeel.SkinName = cls_KP_RES.LoadSkin();
            clsKP_Terminal.LoadMayIn();
            clsUserManagement.ShowFormDangNhap();
            LoadPermission();
            VisibleTileItem();
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        private void Frm_Main_Load(object sender, EventArgs e)
        {
           

            itDangxuat.Text = "(" + clsGlobal.gsUserID.ToUpper() + ")" + "\n" + "Đăng xuất";
            cls_KP_RES.LoadLayout(tileControl1);
            if (File.Exists(Application.StartupPath + "\\KP_Inventory.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\KP_Inventory.exe");
            }

            //Bat buoc cap nhat
            splashScreenManager1.ShowWaitForm();
            try
            {
                string fileLocation = "";
                string exeApp = "";
                string pass = "";
                string useName = "";
                string line;
                string lbversion = "";
                string lbCurrentVerson = "";

                DataTable table = new DataTable("Table1");
                table.Columns.Add("hotName", typeof(String));
                table.Columns.Add("startPart", typeof(String));
                table.Columns.Add("port", typeof(String));
                table.Columns.Add("USERNAME", typeof(String));
                table.Columns.Add("PASSWORD", typeof(String));
                table.Columns.Add("exeApp", typeof(String));
                using (Stream stream = new FileStream(Application.StartupPath + "\\systemupdate.xml", FileMode.Open, FileAccess.Read))
                {
                    table.ReadXml(stream);
                }
                fileLocation = table.Rows[0]["hotName"].ToString() + "//" + table.Rows[0]["startPart"].ToString() + "/";
                useName = table.Rows[0]["USERNAME"].ToString();
                pass = table.Rows[0]["PASSWORD"].ToString();
                exeApp = table.Rows[0]["exeApp"].ToString();

                System.IO.StreamReader filess = new System.IO.StreamReader(Application.StartupPath + "\\version.txt");
                if ((line = filess.ReadLine()) != null)
                {
                    lbversion = "Phiên bản hiện tại : " + line;
                }
                filess.Close();

                string fp = fileLocation + "/version.txt";
                //WebClient client = new WebClient();
                //client.Credentials = new NetworkCredential(useName, pass);
                //Stream _stream = client.OpenRead(fp);
                //StreamReader reader = new StreamReader(_stream);
                //String content = reader.ReadToEnd();

                String content = ""; ;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fp);
                request.Credentials = new NetworkCredential(useName, pass);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.UsePassive = false;
                using (Stream stream = request.GetResponse().GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    content = reader.ReadLine();
                }

                if (content != null)
                {
                    lbCurrentVerson = "Phiên bản mới nhất : " + content;
                }
                if (line != content)
                {
                    splashScreenManager1.CloseWaitForm();
                    Application.Exit();
                    if (File.Exists(Application.StartupPath + "\\KP_Update.exe"))
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\KP_Update.exe");
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\UpdateApp.exe");
                    }
                }
                else
                {
                    splashScreenManager1.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Thông báo : Lỗi kết nối Server 1");
                splashScreenManager1.CloseWaitForm();
            }
            //

        }

        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            cls_KP_RES.SaveSkin(defaultLookAndFeel1.LookAndFeel.SkinName);
            cls_KP_RES.SaveLayout(tileControl1);
            clsUserManagement.FreeLogin(clsUserManagement .ReturnUserLogin ());
            foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
            {
                proc.Kill();
            }
        }

        private void Frm_Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.F1)
            {
                this.WindowState =  FormWindowState.Minimized;
            }
        }

        int iTimenotuse = 0;
        int iXmouse = 0;
        int iDongbo = 0;
        private void timerGiaiphongbonho_Tick(object sender, EventArgs e)
        {
            GC.Collect();
            int iXmouseNew = System.Windows.Forms.Control.MousePosition.X;
            if (iXmouseNew != iXmouse)
            {
                iTimenotuse = 0;
                iXmouse = iXmouseNew;
            }
            if (iTimenotuse == 10)
            {
                itThoat_ItemClick(null, null);
            }
            else
            {
                iTimenotuse++;
            }
            if (!clsUserManagement.CheckLogin(clsGlobal.gsUserID))
            {
                itDangxuat_ItemClick(null, null);
            }
            if (cls_ConfigCashier.sAutodaybill == "1")
            {
                try
                {
                    if (cls_ConfigCashier.sStore_ID == "0")
                    {
                        cls_ConfigCashier.LoadCauHinh();
                    }
                    string sBill = "";
                    sBill = cls_Huyvietnam.postBILL_One(cls_ConfigCashier.sStore_ID);
                   
                    if (iDongbo == 40)
                    {
                        iDongbo = 0;
                        string sNhomhang = "";
                        sNhomhang = cls_Huyvietnam.getNhomhang(cls_ConfigCashier.sMenuID);
                        string sHanghoa = "";
                        sHanghoa = cls_Huyvietnam.getHanghoa(cls_ConfigCashier.sMenuID);
                        string sYeucauthem = "";
                        sYeucauthem = cls_Huyvietnam.getYeucauthem(cls_ConfigCashier.sStore_ID);
                    }
                    else
                    {
                        iDongbo++;
                    }
                }
                catch
                {
                }
            }
        }

        private void itThunho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThunho.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void itThoat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThoat.Checked )
            {
                try
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_Keyboard"))
                    {
                        proc.Kill();
                    }
                    foreach (Process proc in Process.GetProcessesByName("KP_Theater"))
                    {
                        proc.Kill();
                    }
                   
                    this.Close();
                }
                catch
                {
                    this.Close();
                }
            }
        }

        private void itCaidat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidat.Checked)
            {
                this.GoTo<Frm_Caidat>();
            }
        }

        private void itNhapxuatton_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapxuatton.Checked)
            {
                this.GoTo<Frm_Nhapxuat>();
            }
        }

        private void itHethong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHethong.Checked)
            {
                this.GoTo<Frm_Hethong>();
            }
        }

        private void itCauhinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinh.Checked)
            {
                this.GoTo<Frm_Cauhinh>();
            }
        }

        private void itDangxuat_ItemClick(object sender, TileItemEventArgs e)
        {
            cls_KP_RES.LoadDefaultLayout();
            if (itDangxuat.Checked)
            {
                Application.Restart();
            }
        }

        private void itNhahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhahang.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vào bán hàng
                Frm_BanHang frm1 = new Frm_BanHang();
                frm1.ShowDialog();
                frm1.Dispose();
            }
        }

        private void itTrogiup_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrogiup.Checked)
            {
                this.GoTo<Frm_Trogiup>();

            }
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.GoTo<Frm_Baocao>();
            }
        }

        private void itManhinhbep_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itManhinhbep.Checked)
            {
                this.GoTo<Frm_Phanhebep>();
            }
        }

        private void itKhuvuichoi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhuvuichoi.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //Vô bán hàng
                Frm_Banve frm = new Frm_Banve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itKaraok_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKaraok.Checked)
            {
                 //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //Vô bán hàng
                Frm_BanHang_Karaoke frm1 = new Frm_BanHang_Karaoke();
                frm1.ShowDialog();
                frm1.Dispose();
            }
        }

        private void itKhachsan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhachsan.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //Vô bán hàng
                Frm_BanHang_KhachSan frm1 = new Frm_BanHang_KhachSan();
                frm1.ShowDialog();
                frm1.Dispose();
            }
        }

        private void itVerapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVerapphim.Checked)
            {
                this.GoTo<Frm_Rapphim>();
             
            }
        }

        private void itCongno_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongno.Checked)
            {
                this.GoTo<Frm_Congno>();
            }
        }

        private void itFastFood_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itFastFood.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vô bán hàng
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Nhahang frm = new Frm_Manhinhhai_Nhahang();
                    Screen[] sc = Screen.AllScreens;
                    frm.Width = sc[1].Bounds.Width;
                    frm.Height = sc[1].Bounds.Height;
                    frm.Location = new Point(this.Width, 0);
                    frm.Show();

                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.MyGetData = new Frm_Banhang_FastFood.GetString(frm.GetValue);
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                else
                {
                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
        }

        private void itShowTime_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itShowTime.Checked)
            {
                if (File.Exists(Application.StartupPath + "\\MenuTicket.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("MenuTicket"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\MenuTicket.exe");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void itMenu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMenu.Checked)
            {
                if (File.Exists(Application.StartupPath + "\\MenuDisplay.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("MenuDisplay"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\MenuDisplay.exe");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void itKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT.Checked)
            {
                this.GoTo<Frm_KHTT>();
            }
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.GoTo<Frm_CongVan>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhansu.Checked)
            {
                this.GoTo<Frm_QuanLyCongTy>();
            }
        }

        private void itDanhgiaKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDanhgiaKH .Checked)
            {
                Frm_DanhGiaKhachHang frm = new Frm_DanhGiaKhachHang();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itTheodoihopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTheodoihopdong.Checked)
            {
                this.GoTo<Frm_SoTheoDoiHD>();
            }
        }

        private void itSieuthi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSieuthi.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vô bán hàng
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Nhahang frm = new Frm_Manhinhhai_Nhahang();
                    Screen[] sc = Screen.AllScreens;
                    frm.Width = sc[1].Bounds.Width;
                    frm.Height = sc[1].Bounds.Height;
                    frm.Location = new Point(this.Width, 0);
                    frm.Show();

                    Frm_BanHang_Mavach frm1 = new Frm_BanHang_Mavach();
                    frm1.MyGetData = new Frm_BanHang_Mavach.GetString(frm.GetValue);
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                else
                {
                    Frm_BanHang_Mavach frm1 = new Frm_BanHang_Mavach();
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
        }

        private void itTrasua_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrasua.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vô bán hàng
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Nhahang frm = new Frm_Manhinhhai_Nhahang();
                    Screen[] sc = Screen.AllScreens;
                    frm.Width = sc[1].Bounds.Width;
                    frm.Height = sc[1].Bounds.Height;
                    frm.Location = new Point(this.Width, 0);
                    frm.Show();

                    Frm_Banhang_TraSua frm1 = new Frm_Banhang_TraSua();
                    frm1.MyGetData = new Frm_Banhang_TraSua.GetString(frm.GetValue);
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                else
                {
                    Frm_Banhang_TraSua frm1 = new Frm_Banhang_TraSua();
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itNhapxuatton);
            cls_KP_RES.RemoveTileItem(itManhinhbep);
            cls_KP_RES.RemoveTileItem(itNhahang);
            cls_KP_RES.RemoveTileItem(itKaraok);
            cls_KP_RES.RemoveTileItem(itKhachsan);
            cls_KP_RES.RemoveTileItem(itKhuvuichoi);
            cls_KP_RES.RemoveTileItem(itVerapphim);
            cls_KP_RES.RemoveTileItem(itCongno);
            cls_KP_RES.RemoveTileItem(itFastFood);
            cls_KP_RES.RemoveTileItem(itShowTime);
            cls_KP_RES.RemoveTileItem(itMenu);
            cls_KP_RES.RemoveTileItem(itKHTT);
            cls_KP_RES.RemoveTileItem(itCaidat );
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itCongvan );
            cls_KP_RES.RemoveTileItem(itNhansu);
            cls_KP_RES.RemoveTileItem(itDanhgiaKH );
            cls_KP_RES.RemoveTileItem(itTheodoihopdong);
            cls_KP_RES.RemoveTileItem(itSieuthi);
            cls_KP_RES.RemoveTileItem(itTrasua);
            cls_KP_RES.RemoveTileItem(itTrasua_Ban);
            cls_KP_RES.RemoveTileItem(itBuffet);
        }

        private void LoadPermission()
        {
            itDangxuat.Checked = true;
            itTrogiup.Checked = true;
            itCauhinh.Checked = true;
            itHethong.Checked = true;
            itThoat.Checked = true;
            itThunho.Checked = true;
            itNhansu.Checked = (clsUserManagement.AllowView("5051601") || clsUserManagement.AllowView("5051602") || clsUserManagement.AllowView("6051601") || clsUserManagement.AllowView("6051603")) && clsUserManagement.CheckActive("17");
            itCongvan.Checked = clsUserManagement.AllowView("24031601") && clsUserManagement.CheckActive("16");
            itNhapxuatton.Checked = (clsUserManagement.AllowView("12") || clsUserManagement.AllowView("13")|| clsUserManagement.AllowView("14")) && clsUserManagement.CheckActive("1");
            itManhinhbep.Checked = clsUserManagement.AllowView("24")&& clsUserManagement.CheckActive("3");
            itNhahang.Checked = clsUserManagement.AllowView("23") && clsUserManagement.CheckActive("5");
            itKaraok.Checked = clsUserManagement.AllowView("62") && clsUserManagement.CheckActive("6");
            itKhachsan.Checked = clsUserManagement.AllowView("63") && clsUserManagement.CheckActive("7");
            itKhuvuichoi.Checked = clsUserManagement.AllowView("61") && clsUserManagement.CheckActive("9");
            itVerapphim.Checked = (clsUserManagement.AllowView("67")|| clsUserManagement.AllowView("80")|| clsUserManagement.AllowView("99")) && clsUserManagement.CheckActive("10");
            itCongno.Checked = (clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
            itFastFood.Checked = clsUserManagement.AllowView("102") && clsUserManagement.CheckActive("14");
            itShowTime.Checked = clsUserManagement.AllowView("105") && clsUserManagement.CheckActive("10");
            itMenu.Checked = clsUserManagement.AllowView("106") && clsUserManagement.CheckActive("15");
            itKHTT.Checked = (clsUserManagement.AllowView("52")||clsUserManagement.AllowView("107")) && clsUserManagement.CheckActive("4");

            itCaidat.Checked =clsUserManagement.AllowView("40") || clsUserManagement.AllowView("41") || clsUserManagement.AllowView("4") || clsUserManagement.AllowView("6") || clsUserManagement.AllowView("7") || clsUserManagement.AllowView("8") || clsUserManagement.AllowView("9") || clsUserManagement.AllowView("18") || clsUserManagement.AllowView("33") || clsUserManagement.AllowView("100")
                ||(clsUserManagement.AllowView("5") || clsUserManagement.AllowView("42")) && (clsUserManagement.CheckActive("5"))
                ||(clsUserManagement.AllowView("53") || clsUserManagement.AllowView("58")) && (clsUserManagement.CheckActive("4") || clsUserManagement.CheckActive("8"))
                ||(clsUserManagement.AllowView("68") || clsUserManagement.AllowView("69") || clsUserManagement.AllowView("70") || clsUserManagement.AllowView("71") || clsUserManagement.AllowView("72") || clsUserManagement.AllowView("73") || clsUserManagement.AllowView("74") || clsUserManagement.AllowView("75") || clsUserManagement.AllowView("76") || clsUserManagement.AllowView("77") || clsUserManagement.AllowView("78") || clsUserManagement.AllowView("79")) && (clsUserManagement.CheckActive("10"))
                ||(clsUserManagement.AllowView("60") && clsUserManagement.CheckActive("9"))
                || (clsUserManagement.AllowView("22071601") && clsUserManagement.CheckActive("220716"))
                ||(clsUserManagement.AllowView("60") || clsUserManagement.AllowView("68") || clsUserManagement.AllowView("69") || clsUserManagement.AllowView("70") || clsUserManagement.AllowView("71") || clsUserManagement.AllowView("72") || clsUserManagement.AllowView("73") || clsUserManagement.AllowView("74") || clsUserManagement.AllowView("75") || clsUserManagement.AllowView("76") || clsUserManagement.AllowView("77") || clsUserManagement.AllowView("78") || clsUserManagement.AllowView("79")) && (clsUserManagement.CheckActive("11"))
                ||(clsUserManagement.AllowView("5") || clsUserManagement.AllowView("42")) && (clsUserManagement.CheckActive("6"));

            itBaocao.Checked = clsUserManagement.AllowView("48") || clsUserManagement.AllowView("50") || clsUserManagement.AllowView("56") || clsUserManagement.AllowView("57")
                || (clsUserManagement.AllowView("17") || clsUserManagement.AllowView("26") || clsUserManagement.AllowView("47") || clsUserManagement.AllowView("27")) && (clsUserManagement.CheckActive("1"))
                || clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31")
                || (clsUserManagement.AllowView("22071603") && clsUserManagement.CheckActive("220716"))
                || (clsUserManagement.AllowView("54") || clsUserManagement.AllowView("55") || clsUserManagement.AllowView("59")) && (clsUserManagement.CheckActive("4") || clsUserManagement.CheckActive("8"))
                || (clsUserManagement.AllowView("81") || clsUserManagement.AllowView("82") || clsUserManagement.AllowView("83") || clsUserManagement.AllowView("84") || clsUserManagement.AllowView("85") || clsUserManagement.AllowView("86") || clsUserManagement.AllowView("87")) && (clsUserManagement.CheckActive("10"))
                || (clsUserManagement.AllowView("64") || clsUserManagement.AllowView("65") || clsUserManagement.AllowView("66")) && (clsUserManagement.CheckActive("9"))
                || (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98")) && clsUserManagement.CheckActive("12");

            itDanhgiaKH.Checked = clsUserManagement.AllowView("22071602") && clsUserManagement.CheckActive("220716");
            itTheodoihopdong.Checked = clsUserManagement.AllowView("30081602") && clsUserManagement.CheckActive("300816");
            itSieuthi.Checked = clsUserManagement.AllowView("22041701") && clsUserManagement.CheckActive("220417");

            itTrasua.Checked = clsUserManagement.AllowView("25121701") && clsUserManagement.CheckActive("251217");
            itTrasua_Ban.Checked = clsUserManagement.AllowView("18011801") && clsUserManagement.CheckActive("251217");

            itBuffet.Checked = clsUserManagement.AllowView("28071801") && clsUserManagement.CheckActive("5");

        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            itCaidat.Text = rm.GetString("caidat", culture);
           
            itDangxuat.Text = rm.GetString("dangxuat", culture);
            itTrogiup.Text = rm.GetString("trogiup", culture);
            itBaocao.Text = rm.GetString("baocao", culture);
            itCauhinh.Text = rm.GetString("cauhinh", culture);
            itThoat.Text = rm.GetString("thoat", culture);
            itHethong.Text = rm.GetString("hethong", culture);
            itThunho.Text = rm.GetString("thunho", culture);
            itNhapxuatton.Text = rm.GetString("khonhapxuat", culture);
            itKHTT.Text = rm.GetString("khtt", culture);
            itTrasua.Text = rm.GetString("trasua", culture);
            itTrasua_Ban.Text = rm.GetString("trasuatheoban", culture);
            itManhinhbep.Text = rm.GetString("manhinhbep", culture);
            if (cls_KP_RES.Mode == 8)
            {
                itNhahang.Text = "Spa 1";
                itFastFood.Text = "Spa";
            }
            else
            {
                itNhahang.Text = rm.GetString("nhahang", culture);
                itFastFood.Text = "FastFood";
            }
            this.Text = rm.GetString("trangchu", culture);
            tileControl1.Text = rm.GetString("trangchu", culture);
           
        }

        private void itTrasua_Ban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrasua_Ban.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vào bán hàng
                Frm_TraSua_TheoBan frm1 = new Frm_TraSua_TheoBan();
                frm1.ShowDialog();
                frm1.Dispose();
            }
        }

        private void itBuffet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBuffet.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vào bán hàng
                Frm_BanHang_Buffet frm1 = new Frm_BanHang_Buffet();
                frm1.ShowDialog();
                frm1.Dispose();
            }
        }

     
    }
}