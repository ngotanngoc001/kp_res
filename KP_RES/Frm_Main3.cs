﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Metro.Navigation;
using System.IO;
using KP_UserManagement;
using KP_Terminal;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace KP_RES 
{
    public partial class Frm_Main3 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Main3()
        {
            if (Process.GetProcessesByName("kingpos").Length > 1)
            {
                System.Environment.Exit(0);
            }
            InitializeComponent();
            this.defaultLookAndFeel1.LookAndFeel.SkinName = cls_KP_RES.LoadSkin();
            clsKP_Terminal.LoadMayIn();
            clsUserManagement.ShowFormDangNhap();
            LoadPermission();
            VisibleTileItem();
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        private void Frm_Main3_Load(object sender, EventArgs e)
        {
            itDangxuat.Text = "(" + clsGlobal.gsUserID.ToUpper() + ")" + "\n" + "Đăng xuất";
            cls_KP_RES.LoadLayout(tileControl1);
            if (File.Exists(Application.StartupPath + "\\KP_Inventory.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\KP_Inventory.exe");
            }
        }

        private void Frm_Main3_FormClosing(object sender, FormClosingEventArgs e)
        {
            cls_KP_RES.SaveSkin(defaultLookAndFeel1.LookAndFeel.SkinName);
            cls_KP_RES.SaveLayout(tileControl1);
            clsUserManagement.FreeLogin(clsUserManagement .ReturnUserLogin ());
            foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
            {
                proc.Kill();
            }
        }

        private void Frm_Main3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.F1)
            {
                this.WindowState =  FormWindowState.Minimized;
            }
        }

        private void timerGiaiphongbonho_Tick(object sender, EventArgs e)
        {
            GC.Collect();
            if (!clsUserManagement.CheckLogin(clsGlobal.gsUserID))
            {
                itDangxuat_ItemClick(null, null);
            }
        }

        private void itThunho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThunho.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void itThoat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThoat.Checked )
            {
                try
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_Keyboard"))
                    {
                        proc.Kill();
                    }
                    foreach (Process proc in Process.GetProcessesByName("KP_Theater"))
                    {
                        proc.Kill();
                    }
                   
                    this.Close();
                }
                catch
                {
                    this.Close();
                }
            }
        }

      
        private void itHethong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHethong.Checked)
            {
                this.GoTo<Frm_Hethong3>();
            }
        }

        private void itCauhinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinh.Checked)
            {
                this.GoTo<Frm_Cauhinh3>();
            }
        }

        private void itDangxuat_ItemClick(object sender, TileItemEventArgs e)
        {
            cls_KP_RES.LoadDefaultLayout();
            if (itDangxuat.Checked)
            {
                Application.Restart();
            }
        }

       

        private void itTrogiup_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrogiup.Checked)
            {
                this.GoTo<Frm_Trogiup>();
            }
        }

       



      

        private void itFastFood_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itFastFood.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vô bán hàng
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Nhahang frm = new Frm_Manhinhhai_Nhahang();
                    Screen[] sc = Screen.AllScreens;
                    frm.Width = sc[1].Bounds.Width;
                    frm.Height = sc[1].Bounds.Height;
                    frm.Location = new Point(this.Width, 0);
                    frm.Show();

                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.MyGetData = new Frm_Banhang_FastFood.GetString(frm.GetValue);
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                else
                {
                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
        }


       




        private void VisibleTileItem()
        {

            cls_KP_RES.RemoveTileItem(itBanverapphim);
            cls_KP_RES.RemoveTileItem(itDatverapphim);
            cls_KP_RES.RemoveTileItem(itSoatverapphim);
          
            cls_KP_RES.RemoveTileItem(itFastFood);
           
            cls_KP_RES.RemoveTileItem(itThemKH);
            cls_KP_RES.RemoveTileItem(itPhatthuong);
            cls_KP_RES.RemoveTileItem(itTracuuKH);
           
        }

        private void LoadPermission()
        {
            itDangxuat.Checked = true;
            itTrogiup.Checked = true;
            itCauhinh.Checked = true;
            itHethong.Checked = true;
            itThoat.Checked = true;
            itThunho.Checked = true;
           
            itBanverapphim.Checked = clsUserManagement.AllowView("67") && clsUserManagement.CheckActive("10");
            itDatverapphim.Checked = clsUserManagement.AllowView("80") && clsUserManagement.CheckActive("10");
            itSoatverapphim.Checked = clsUserManagement.AllowView("67") && clsUserManagement.CheckActive("10");
     
            itFastFood.Checked = clsUserManagement.AllowView("102") && clsUserManagement.CheckActive("14");
           

            itThemKH.Checked = (clsUserManagement.AllowView("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowView("58") && clsUserManagement.CheckActive("8"));
            itPhatthuong.Checked = clsUserManagement.AllowView("52") && clsUserManagement.CheckActive("4");
            itTracuuKH.Checked = clsUserManagement.AllowView("107") && clsUserManagement.CheckActive("4");


        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main3).Assembly);
           
           
            itDangxuat.Text = rm.GetString("dangxuat", culture);
            itTrogiup.Text = rm.GetString("trogiup", culture);
           
            itCauhinh.Text = rm.GetString("cauhinh", culture);
            itThoat.Text = rm.GetString("thoat", culture);
            itHethong.Text = rm.GetString("hethong", culture);
            itThunho.Text = rm.GetString("thunho", culture);

            itFastFood.Text = "FastFood";
            this.Text = rm.GetString("trangchu", culture);
            tileControl1.Text = rm.GetString("trangchu", culture);

           
           
        }

        private void itBanverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanverapphim.Checked)
            {
                //check ban hang
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //Gọi form quảng cáo
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Ve frmqc = new Frm_Manhinhhai_Ve();
                    Screen[] sc = Screen.AllScreens;
                    frmqc.Width = sc[1].Bounds.Width;
                    frmqc.Height = sc[1].Bounds.Height;
                    frmqc.Location = new Point(this.Width, 0);
                    frmqc.Show();
                }
                //Vô bán hàng
                Frm_RPBanve frm = new Frm_RPBanve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itDatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDatverapphim.Checked)
            {
                Frm_RPDatve frm = new Frm_RPDatve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itSoatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoatverapphim.Checked)
            {
                cls_ConfigCashier.LoadCauHinh();
                Frm_InveMuaTruoc frm = new Frm_InveMuaTruoc();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itThemKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThemKH.Checked)
            {
                this.GoTo<Frm_Khachhang>();
            }
        }

        private void itPhatthuong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhatthuong.Checked)
            {
                this.GoTo<Frm_DoiDiemThuong>();
            }
        }

        private void itTracuuKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTracuuKH.Checked)
            {

                if (File.Exists(Application.StartupPath + "\\KP_KHTT.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_KHTT"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\KP_KHTT.exe");
                }
                else if (File.Exists(Application.StartupPath + "\\CoopMart.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("CoopMart"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\CoopMart.exe");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

       

    }
}