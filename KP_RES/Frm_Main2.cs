﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Metro.Navigation;
using System.IO;
using KP_UserManagement;
using KP_Terminal;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Data;

namespace KP_RES 
{
    public partial class Frm_Main2 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Main2()
        {
            if (Process.GetProcessesByName("kingpos").Length > 1)
            {
                System.Environment.Exit(0);
            }
            InitializeComponent();
        }

        private void Frm_Main2_Load(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + "\\KP_Inventory.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\KP_Inventory.exe");
            }
        }

        private void Frm_Main2_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void Frm_Main2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.F1)
            {
                this.WindowState =  FormWindowState.Minimized;
            }
        }

        private void itThunho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThunho.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void itThoat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThoat.Checked )
            {
                try
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_Keyboard"))
                    {
                        proc.Kill();
                    }
                    foreach (Process proc in Process.GetProcessesByName("KP_Theater"))
                    {
                        proc.Kill();
                    }
                    Application.Exit();
                }
                catch
                {
                    Application.Exit();
                }
            }
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void itQuocthanh_ItemClick(object sender, TileItemEventArgs e)
        {
            ConnectServer("103.63.115.58", "sa", "cinestarqt", "tranvu123!@#", itQuocthanh.Text );
        }

        private void itDalat_ItemClick(object sender, TileItemEventArgs e)
        {
            ConnectServer("113.164.94.205", "sa", "cinestarqt", "tranvu123!@#",itDalat.Text );
        }

        private void itHaibatrung_ItemClick(object sender, TileItemEventArgs e)
        {
            ConnectServer("45.122.246.197", "sa", "cinestarqt", "tranvu123!@#", itHaibatrung.Text);
        }

        private void itQuocthanh2016_ItemClick(object sender, TileItemEventArgs e)
        {
            DevExpress.XtraEditors.XtraMessageBox.Show("Dữ liệu chưa được cài đặt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ConnectServer(string server, string user, string data, string pass,string mess)
        {
            string gsServer = server;
            string gsUserName = user;
            string gsPassword = pass;
            string gsDBName = data;
            string gsConnectionString = "Data Source=" + gsServer + ";Initial Catalog=" + gsDBName + ";User ID=" + gsUserName + ";Password=" + gsPassword + ";Connection Timeout=30";
            //Thử kết nối tới Server
            //Nếu không được hiện thông báo và hướng dẩn chọn Server khác
            //Nếu kết nối được thì đóng form này và lưu thông tin cho lần sử dụng sau
            if (!clsMain.CheckConnection(gsConnectionString))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Không thể kết nối đến cơ sở dữ liệu" + "\nNhập lại thông tin kết nối", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                dt.Columns.Add("ServerName");
                dt.Columns.Add("UserName");
                dt.Columns.Add("Password");
                dt.Columns.Add("DBName");
                dt.Rows.Add(gsServer, gsUserName, gsPassword, gsDBName);
                ds.Tables.Add(dt);
                if (File.Exists(Application.StartupPath + "\\ServerCode.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ServerCode.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds.WriteXml(Application.StartupPath + "\\ServerCode.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds.WriteXml(Application.StartupPath + "\\ServerCode.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ServerCode.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }

                DataSet ds1 = new DataSet();
                DataTable dt1 = new DataTable();

                dt1.Columns.Add("Theater");
                dt1.Rows.Add(mess );
                ds1.Tables.Add(dt1);
                if (File.Exists(Application.StartupPath + "\\Theater.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Theater.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds1.WriteXml(Application.StartupPath + "\\Theater.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds1.WriteXml(Application.StartupPath + "\\Theater.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Theater.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                if (File.Exists(Application.StartupPath + "\\KP_Theater.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_Theater"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\KP_Theater.exe");
                }
                this.Close();
            }
        }

       

        


    }
}