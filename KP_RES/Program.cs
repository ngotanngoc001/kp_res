﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace KP_RES {
    static class Program 
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() 
        {
            //iMode tùy vào khách hành sẻ chạy các giao diệnZ , report,.. theo yêu cầu . Chỉnh khi build cho khách
            //0 : mặc định
            //1 : thạnh thới
            //2 : ionah
            //3 : nhung chang trai
            //4 : huy viet nam
            //5 : a nho
            //6 : an nhien
            //7 : cinestar quan ly
            //8 : thao moc
            //9 : hattatsu image bill
            //10 : sieu thi - bill co ma vach
            //11 : cinestar ban hang
            int iMode = 0;
            cls_KP_RES.Mode = iMode;
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(cls_KP_RES .LoadSkin ());
            DevExpress.Skins.SkinManager.EnableFormSkins();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (Application.StartupPath.Contains("kingpos_temp"))
            {
                DirectoryInfo dir = new DirectoryInfo(Application.StartupPath);
                dir.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }

            if (iMode == 1)
            {
                Application.Run(new Frm_Main1());
            }
            else if (iMode == 7)
            {
                Frm_Main2 frm = new Frm_Main2();
                frm.ShowDialog();
                frm.Dispose();
                Application.Run(new Frm_Main());
            }
            else if (iMode == 11)
            {
                Application.Run(new Frm_Main3());
            }
            else
            {
                Application.Run(new Frm_Main());
            }
        }
    }
}
