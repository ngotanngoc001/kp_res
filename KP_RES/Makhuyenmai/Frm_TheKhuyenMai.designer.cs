﻿namespace KP_RES 
{
    partial class Frm_TheKhuyenMai 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnInfor = new DevExpress.XtraEditors.PanelControl();
            this.lblGiaTri = new DevExpress.XtraEditors.LabelControl();
            this.txtGiaTri = new DevExpress.XtraEditors.TextEdit();
            this.lblSoLuong = new DevExpress.XtraEditors.LabelControl();
            this.btnAuto = new DevExpress.XtraEditors.SimpleButton();
            this.timeExp = new DevExpress.XtraEditors.TimeEdit();
            this.timeActive = new DevExpress.XtraEditors.TimeEdit();
            this.chkTien = new DevExpress.XtraEditors.CheckEdit();
            this.chkPhanTram = new DevExpress.XtraEditors.CheckEdit();
            this.dtpExp = new DevExpress.XtraEditors.DateEdit();
            this.lblMa = new DevExpress.XtraEditors.LabelControl();
            this.txtMa = new DevExpress.XtraEditors.TextEdit();
            this.lblNgayHetHan = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayKhuyenMai = new DevExpress.XtraEditors.LabelControl();
            this.dtpActive = new DevExpress.XtraEditors.DateEdit();
            this.txtSoLuong = new DevExpress.XtraEditors.TextEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChu = new DevExpress.XtraEditors.TextEdit();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG_SD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_KM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_HSD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI_PT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI_T = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DATE_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DATE_EXP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_EXP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).BeginInit();
            this.pnInfor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeExp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPhanTram.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExp.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpActive.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // pnInfor
            // 
            this.pnInfor.Controls.Add(this.lblGiaTri);
            this.pnInfor.Controls.Add(this.txtGiaTri);
            this.pnInfor.Controls.Add(this.lblSoLuong);
            this.pnInfor.Controls.Add(this.btnAuto);
            this.pnInfor.Controls.Add(this.timeExp);
            this.pnInfor.Controls.Add(this.timeActive);
            this.pnInfor.Controls.Add(this.chkTien);
            this.pnInfor.Controls.Add(this.chkPhanTram);
            this.pnInfor.Controls.Add(this.dtpExp);
            this.pnInfor.Controls.Add(this.lblMa);
            this.pnInfor.Controls.Add(this.txtMa);
            this.pnInfor.Controls.Add(this.lblNgayHetHan);
            this.pnInfor.Controls.Add(this.lblNgayKhuyenMai);
            this.pnInfor.Controls.Add(this.dtpActive);
            this.pnInfor.Controls.Add(this.txtSoLuong);
            this.pnInfor.Controls.Add(this.btnLuu);
            this.pnInfor.Controls.Add(this.btnSua);
            this.pnInfor.Controls.Add(this.btnXoa);
            this.pnInfor.Controls.Add(this.btnThem);
            this.pnInfor.Controls.Add(this.lblGhiChu);
            this.pnInfor.Controls.Add(this.txtGhiChu);
            this.pnInfor.Controls.Add(this.lblTen);
            this.pnInfor.Controls.Add(this.txtTen);
            this.pnInfor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnInfor.Location = new System.Drawing.Point(0, 0);
            this.pnInfor.Name = "pnInfor";
            this.pnInfor.Size = new System.Drawing.Size(1199, 146);
            this.pnInfor.TabIndex = 0;
            // 
            // lblGiaTri
            // 
            this.lblGiaTri.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiaTri.Location = new System.Drawing.Point(368, 72);
            this.lblGiaTri.Margin = new System.Windows.Forms.Padding(4);
            this.lblGiaTri.Name = "lblGiaTri";
            this.lblGiaTri.Size = new System.Drawing.Size(43, 19);
            this.lblGiaTri.TabIndex = 11;
            this.lblGiaTri.Text = "Giá trị";
            // 
            // txtGiaTri
            // 
            this.txtGiaTri.EnterMoveNextControl = true;
            this.txtGiaTri.Location = new System.Drawing.Point(434, 69);
            this.txtGiaTri.Name = "txtGiaTri";
            this.txtGiaTri.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTri.Properties.Appearance.Options.UseFont = true;
            this.txtGiaTri.Properties.Mask.EditMask = "d";
            this.txtGiaTri.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaTri.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaTri.Size = new System.Drawing.Size(248, 26);
            this.txtGiaTri.TabIndex = 12;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(363, 8);
            this.lblSoLuong.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Size = new System.Drawing.Size(64, 19);
            this.lblSoLuong.TabIndex = 7;
            this.lblSoLuong.Text = "Số lượng";
            // 
            // btnAuto
            // 
            this.btnAuto.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuto.Appearance.Options.UseFont = true;
            this.btnAuto.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAuto.Location = new System.Drawing.Point(304, 5);
            this.btnAuto.Margin = new System.Windows.Forms.Padding(4);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(51, 26);
            this.btnAuto.TabIndex = 22;
            this.btnAuto.Text = "Auto";
            this.btnAuto.Click += new System.EventHandler(this.btn_Auto_Click);
            // 
            // timeExp
            // 
            this.timeExp.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeExp.EnterMoveNextControl = true;
            this.timeExp.Location = new System.Drawing.Point(965, 37);
            this.timeExp.Name = "timeExp";
            this.timeExp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeExp.Properties.Appearance.Options.UseFont = true;
            this.timeExp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeExp.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeExp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeExp.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeExp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeExp.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeExp.Size = new System.Drawing.Size(102, 26);
            this.timeExp.TabIndex = 18;
            // 
            // timeActive
            // 
            this.timeActive.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeActive.EnterMoveNextControl = true;
            this.timeActive.Location = new System.Drawing.Point(965, 5);
            this.timeActive.Name = "timeActive";
            this.timeActive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeActive.Properties.Appearance.Options.UseFont = true;
            this.timeActive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeActive.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeActive.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeActive.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeActive.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeActive.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeActive.Size = new System.Drawing.Size(102, 26);
            this.timeActive.TabIndex = 15;
            // 
            // chkTien
            // 
            this.chkTien.Location = new System.Drawing.Point(556, 39);
            this.chkTien.Name = "chkTien";
            this.chkTien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkTien.Properties.Appearance.Options.UseFont = true;
            this.chkTien.Properties.Caption = "Tiền";
            this.chkTien.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkTien.Properties.RadioGroupIndex = 1;
            this.chkTien.Size = new System.Drawing.Size(126, 24);
            this.chkTien.TabIndex = 10;
            this.chkTien.TabStop = false;
            // 
            // chkPhanTram
            // 
            this.chkPhanTram.EditValue = true;
            this.chkPhanTram.Location = new System.Drawing.Point(434, 39);
            this.chkPhanTram.Name = "chkPhanTram";
            this.chkPhanTram.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkPhanTram.Properties.Appearance.Options.UseFont = true;
            this.chkPhanTram.Properties.Caption = "Phần Trăm";
            this.chkPhanTram.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkPhanTram.Properties.RadioGroupIndex = 1;
            this.chkPhanTram.Size = new System.Drawing.Size(123, 24);
            this.chkPhanTram.TabIndex = 9;
            // 
            // dtpExp
            // 
            this.dtpExp.EditValue = null;
            this.dtpExp.EnterMoveNextControl = true;
            this.dtpExp.Location = new System.Drawing.Point(819, 37);
            this.dtpExp.Name = "dtpExp";
            this.dtpExp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExp.Properties.Appearance.Options.UseFont = true;
            this.dtpExp.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExp.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpExp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpExp.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpExp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpExp.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpExp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpExp.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpExp.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpExp.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpExp.Size = new System.Drawing.Size(140, 26);
            this.dtpExp.TabIndex = 17;
            // 
            // lblMa
            // 
            this.lblMa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMa.Location = new System.Drawing.Point(13, 8);
            this.lblMa.Margin = new System.Windows.Forms.Padding(4);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(20, 19);
            this.lblMa.TabIndex = 0;
            this.lblMa.Text = "Mã";
            // 
            // txtMa
            // 
            this.txtMa.EnterMoveNextControl = true;
            this.txtMa.Location = new System.Drawing.Point(107, 5);
            this.txtMa.Name = "txtMa";
            this.txtMa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Properties.Appearance.Options.UseFont = true;
            this.txtMa.Size = new System.Drawing.Size(190, 26);
            this.txtMa.TabIndex = 1;
            // 
            // lblNgayHetHan
            // 
            this.lblNgayHetHan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHetHan.Location = new System.Drawing.Point(689, 40);
            this.lblNgayHetHan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayHetHan.Name = "lblNgayHetHan";
            this.lblNgayHetHan.Size = new System.Drawing.Size(94, 19);
            this.lblNgayHetHan.TabIndex = 16;
            this.lblNgayHetHan.Text = "Ngày hết hạn";
            // 
            // lblNgayKhuyenMai
            // 
            this.lblNgayKhuyenMai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKhuyenMai.Location = new System.Drawing.Point(689, 8);
            this.lblNgayKhuyenMai.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayKhuyenMai.Name = "lblNgayKhuyenMai";
            this.lblNgayKhuyenMai.Size = new System.Drawing.Size(123, 19);
            this.lblNgayKhuyenMai.TabIndex = 13;
            this.lblNgayKhuyenMai.Text = "Ngày khuyến mãi";
            // 
            // dtpActive
            // 
            this.dtpActive.EditValue = null;
            this.dtpActive.EnterMoveNextControl = true;
            this.dtpActive.Location = new System.Drawing.Point(819, 5);
            this.dtpActive.Name = "dtpActive";
            this.dtpActive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpActive.Properties.Appearance.Options.UseFont = true;
            this.dtpActive.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpActive.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpActive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpActive.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpActive.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpActive.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpActive.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpActive.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpActive.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpActive.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpActive.Size = new System.Drawing.Size(140, 26);
            this.dtpActive.TabIndex = 14;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.EnterMoveNextControl = true;
            this.txtSoLuong.Location = new System.Drawing.Point(434, 5);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Properties.Appearance.Options.UseFont = true;
            this.txtSoLuong.Properties.Mask.EditMask = "d";
            this.txtSoLuong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(248, 26);
            this.txtSoLuong.TabIndex = 8;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(107, 102);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(78, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(281, 102);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(369, 102);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(193, 102);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.Location = new System.Drawing.Point(13, 72);
            this.lblGhiChu.Margin = new System.Windows.Forms.Padding(4);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Size = new System.Drawing.Size(57, 19);
            this.lblGhiChu.TabIndex = 5;
            this.lblGhiChu.Text = "Ghi Chú";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.EnterMoveNextControl = true;
            this.txtGhiChu.Location = new System.Drawing.Point(107, 69);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu.Size = new System.Drawing.Size(248, 26);
            this.txtGhiChu.TabIndex = 6;
            // 
            // lblTen
            // 
            this.lblTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Location = new System.Drawing.Point(13, 40);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(27, 19);
            this.lblTen.TabIndex = 3;
            this.lblTen.Text = "Tên";
            // 
            // txtTen
            // 
            this.txtTen.EnterMoveNextControl = true;
            this.txtTen.Location = new System.Drawing.Point(107, 37);
            this.txtTen.Name = "txtTen";
            this.txtTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Properties.Appearance.Options.UseFont = true;
            this.txtTen.Size = new System.Drawing.Size(248, 26);
            this.txtTen.TabIndex = 4;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 146);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1160, 541);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA,
            this.TEN,
            this.SOLUONG,
            this.SOLUONG_SD,
            this.NGAY_KM,
            this.NGAY_HSD,
            this.GIATRI_PT,
            this.GIATRI_T,
            this.GHICHU,
            this.DATE_ACTIVE,
            this.TIME_ACTIVE,
            this.DATE_EXP,
            this.TIME_EXP});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 128;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 224;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "Số lượng";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 3;
            this.SOLUONG.Width = 120;
            // 
            // SOLUONG_SD
            // 
            this.SOLUONG_SD.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG_SD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG_SD.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG_SD.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG_SD.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG_SD.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG_SD.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG_SD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG_SD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG_SD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.SOLUONG_SD.Caption = "Số lượng SD";
            this.SOLUONG_SD.FieldName = "SOLUONG_SD";
            this.SOLUONG_SD.Name = "SOLUONG_SD";
            this.SOLUONG_SD.OptionsColumn.AllowEdit = false;
            this.SOLUONG_SD.OptionsColumn.AllowFocus = false;
            this.SOLUONG_SD.OptionsColumn.FixedWidth = true;
            this.SOLUONG_SD.Width = 120;
            // 
            // NGAY_KM
            // 
            this.NGAY_KM.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_KM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_KM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_KM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_KM.AppearanceHeader.Options.UseFont = true;
            this.NGAY_KM.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_KM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_KM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_KM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAY_KM.Caption = "Ngày khuyến mãi";
            this.NGAY_KM.DisplayFormat.FormatString = "{0: dd/MM/yyyy HH:mm:ss}";
            this.NGAY_KM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_KM.FieldName = "NGAY_KM";
            this.NGAY_KM.Name = "NGAY_KM";
            this.NGAY_KM.OptionsColumn.AllowEdit = false;
            this.NGAY_KM.OptionsColumn.AllowFocus = false;
            this.NGAY_KM.OptionsColumn.FixedWidth = true;
            this.NGAY_KM.Visible = true;
            this.NGAY_KM.VisibleIndex = 4;
            this.NGAY_KM.Width = 198;
            // 
            // NGAY_HSD
            // 
            this.NGAY_HSD.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_HSD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_HSD.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_HSD.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAY_HSD.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAY_HSD.AppearanceHeader.Options.UseFont = true;
            this.NGAY_HSD.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_HSD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_HSD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_HSD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAY_HSD.Caption = "Ngày hết hạn";
            this.NGAY_HSD.DisplayFormat.FormatString = "{0: dd/MM/yyyy HH:mm:ss }";
            this.NGAY_HSD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_HSD.FieldName = "NGAY_HSD";
            this.NGAY_HSD.Name = "NGAY_HSD";
            this.NGAY_HSD.OptionsColumn.AllowEdit = false;
            this.NGAY_HSD.OptionsColumn.AllowFocus = false;
            this.NGAY_HSD.OptionsColumn.FixedWidth = true;
            this.NGAY_HSD.Visible = true;
            this.NGAY_HSD.VisibleIndex = 5;
            this.NGAY_HSD.Width = 198;
            // 
            // GIATRI_PT
            // 
            this.GIATRI_PT.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI_PT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI_PT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI_PT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI_PT.AppearanceHeader.Options.UseFont = true;
            this.GIATRI_PT.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI_PT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI_PT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI_PT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIATRI_PT.Caption = "Phần trăm";
            this.GIATRI_PT.DisplayFormat.FormatString = "#,0";
            this.GIATRI_PT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI_PT.FieldName = "GIATRI_PT";
            this.GIATRI_PT.Name = "GIATRI_PT";
            this.GIATRI_PT.OptionsColumn.AllowEdit = false;
            this.GIATRI_PT.OptionsColumn.AllowFocus = false;
            this.GIATRI_PT.OptionsColumn.FixedWidth = true;
            this.GIATRI_PT.Visible = true;
            this.GIATRI_PT.VisibleIndex = 6;
            this.GIATRI_PT.Width = 100;
            // 
            // GIATRI_T
            // 
            this.GIATRI_T.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI_T.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI_T.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI_T.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI_T.AppearanceHeader.Options.UseFont = true;
            this.GIATRI_T.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI_T.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI_T.Caption = "Tiền";
            this.GIATRI_T.DisplayFormat.FormatString = "#,0";
            this.GIATRI_T.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI_T.FieldName = "GIATRI_T";
            this.GIATRI_T.Name = "GIATRI_T";
            this.GIATRI_T.OptionsColumn.AllowEdit = false;
            this.GIATRI_T.OptionsColumn.AllowFocus = false;
            this.GIATRI_T.OptionsColumn.FixedWidth = true;
            this.GIATRI_T.Visible = true;
            this.GIATRI_T.VisibleIndex = 7;
            this.GIATRI_T.Width = 130;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 8;
            this.GHICHU.Width = 370;
            // 
            // DATE_ACTIVE
            // 
            this.DATE_ACTIVE.FieldName = "DATE_ACTIVE";
            this.DATE_ACTIVE.Name = "DATE_ACTIVE";
            this.DATE_ACTIVE.OptionsColumn.AllowEdit = false;
            this.DATE_ACTIVE.OptionsColumn.AllowFocus = false;
            this.DATE_ACTIVE.OptionsColumn.FixedWidth = true;
            // 
            // TIME_ACTIVE
            // 
            this.TIME_ACTIVE.FieldName = "TIME_ACTIVE";
            this.TIME_ACTIVE.Name = "TIME_ACTIVE";
            this.TIME_ACTIVE.OptionsColumn.AllowEdit = false;
            this.TIME_ACTIVE.OptionsColumn.AllowFocus = false;
            this.TIME_ACTIVE.OptionsColumn.FixedWidth = true;
            // 
            // DATE_EXP
            // 
            this.DATE_EXP.FieldName = "DATE_EXP";
            this.DATE_EXP.Name = "DATE_EXP";
            this.DATE_EXP.OptionsColumn.AllowEdit = false;
            this.DATE_EXP.OptionsColumn.AllowFocus = false;
            this.DATE_EXP.OptionsColumn.FixedWidth = true;
            // 
            // TIME_EXP
            // 
            this.TIME_EXP.FieldName = "TIME_EXP";
            this.TIME_EXP.Name = "TIME_EXP";
            this.TIME_EXP.OptionsColumn.AllowEdit = false;
            this.TIME_EXP.OptionsColumn.AllowFocus = false;
            this.TIME_EXP.OptionsColumn.FixedWidth = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1160, 146);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 541);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 377);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 57);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 217);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 297);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 459);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_TheKhuyenMai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 687);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnInfor);
            this.Name = "Frm_TheKhuyenMai";
            this.Text = "Phát hành mã khuyến mãi";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_TheKhuyenMai_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).EndInit();
            this.pnInfor.ResumeLayout(false);
            this.pnInfor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeExp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPhanTram.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExp.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpActive.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnInfor;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG_SD;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.LabelControl lblGhiChu;
        private DevExpress.XtraEditors.TextEdit txtGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_HSD;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lblNgayHetHan;
        private DevExpress.XtraEditors.LabelControl lblNgayKhuyenMai;
        private DevExpress.XtraEditors.DateEdit dtpActive;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI_PT;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI_T;
        private DevExpress.XtraEditors.LabelControl lblMa;
        private DevExpress.XtraEditors.TextEdit txtMa;
        private DevExpress.XtraEditors.DateEdit dtpExp;
        private DevExpress.XtraEditors.TextEdit txtSoLuong;
        private DevExpress.XtraEditors.CheckEdit chkTien;
        private DevExpress.XtraEditors.CheckEdit chkPhanTram;
        private DevExpress.XtraEditors.TimeEdit timeExp;
        private DevExpress.XtraEditors.TimeEdit timeActive;
        private DevExpress.XtraEditors.SimpleButton btnAuto;
        private DevExpress.XtraEditors.LabelControl lblGiaTri;
        private DevExpress.XtraEditors.TextEdit txtGiaTri;
        private DevExpress.XtraEditors.LabelControl lblSoLuong;
        private DevExpress.XtraGrid.Columns.GridColumn DATE_ACTIVE;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_ACTIVE;
        private DevExpress.XtraGrid.Columns.GridColumn DATE_EXP;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_EXP;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_KM;




    }
}