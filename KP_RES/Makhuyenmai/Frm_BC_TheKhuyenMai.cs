﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_BC_TheKhuyenMai : DevExpress.XtraEditors.XtraForm
    {
        public Frm_BC_TheKhuyenMai()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisibleChoose();
        }

        private void VisibleChoose()
        {
            pnChoose.Visible = (optGroup.SelectedIndex == 0) ? false : true;

            if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2)
                lblTuNgay.Visible = lblDenNgay.Visible = dtpDenNgay.Visible = dtpTuNgay.Visible = true;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sTuNgay = "";
                String sDenNgay = "";

                if (optGroup.EditValue.Equals("1") || optGroup.EditValue.Equals("2"))
                {
                    sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.Value);
                    sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.Value);
                }

                String sSQL = "EXEC SP_SelectBCTheKhuyenMai ";
                sSQL += clsMain.SQLString(sTuNgay) + ",";
                sSQL += clsMain.SQLString(sDenNgay) + ",";
                sSQL += clsMain.SQLString(optGroup.SelectedIndex.ToString());

                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                grdTheKhuyenMai.DataSource = myDT;
            }
            catch (Exception ex)
            {
                MessageInformation(ex.Message, "Thông Báo");
            }
        }

        private void grvTheKhuyenMai_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvTheKhuyenMai.Focus();
                grvTheKhuyenMai.FocusedRowHandle = grvTheKhuyenMai.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                grvTheKhuyenMai.Focus();
                grvTheKhuyenMai.FocusedRowHandle = grvTheKhuyenMai.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (grvTheKhuyenMai.FocusedRowHandle == grvTheKhuyenMai.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                grvTheKhuyenMai.Focus();
                grvTheKhuyenMai.FocusedRowHandle = grvTheKhuyenMai.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (grvTheKhuyenMai.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvTheKhuyenMai.Focus();
                grvTheKhuyenMai.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.grvTheKhuyenMai.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                grdTheKhuyenMai.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                grdTheKhuyenMai.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (Pan_Left.Width == 223)
            {
                Pan_Left.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                Pan_Left.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            String sValue = "";
            try
            {
                if (grvTheKhuyenMai.RowCount > 0)
                {
                    DataTable myDT = (DataTable)grdTheKhuyenMai.DataSource;

                    if (optGroup.SelectedIndex == 0)
                    {
                        sValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description + " )";
                    }
                    else if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2)
                    {
                        sValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description;
                        sValue += " Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";
                    }

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = sValue;
                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 90;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    MessageInformation("Không có dữ liệu", "Thông Báo");
            }
            catch{
            }

        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }

        private void btnHuyVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvTheKhuyenMai.FocusedRowHandle < 0)
                    return;
                   
                if (grvTheKhuyenMai.FocusedRowHandle >= 0)
                {
                    if (DialogResult.Yes == MessageQuestion("Xác nhận bạn muốn xóa ?","Thông Báo"))
                    {
                        String pMa = grvTheKhuyenMai.GetRowCellValue(grvTheKhuyenMai.FocusedRowHandle, "MA").ToString();
                        String sSQL = "EXEC SP_DeleteTheKhuyenMai " + clsMain.SQLString(pMa);

                        clsMain.ExecuteSQL(sSQL);
                        btnTimkiem_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageInformation(ex.Message, "Thông Báo");
            }
        }

        private void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MessageInformation(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private DialogResult MessageQuestion(String sString, String sCaption)
        {
            return XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

       
        

        

        

        


       










    }
}