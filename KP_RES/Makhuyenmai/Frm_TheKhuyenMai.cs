﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;

namespace KP_RES 
{
    public partial class Frm_TheKhuyenMai : DevExpress.XtraEditors.XtraForm
    {
        String sMa = "";

        public Frm_TheKhuyenMai()
        {
            InitializeComponent();
            LoaddataGridView();
        }

        private void Frm_TheKhuyenMai_Paint(object sender, PaintEventArgs e)
        {
         
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtMa.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if ( MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            String sSQL = "EXEC SP_DeleteTheKhuyenMai " + clsMain.SQLString(sMa);
            
            this.Cursor = Cursors.WaitCursor;
            clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            LoaddataGridView();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtMa.Properties.ReadOnly = true;
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            String sSQL = "";
            int Percent = 0;
            int Amount = 0;
            if (chkPhanTram.Checked == true)
                Percent = int.Parse(txtGiaTri.Text);
            else
                Amount = int.Parse(txtGiaTri.Text);

            if (sMa == "" && btnThem.Enabled)
            {
                sSQL += "EXEC SP_InsertTheKhuyenMai ";
                sSQL += clsMain.SQLString(txtMa.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                sSQL += clsMain.SQLString(txtSoLuong.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + ",";
                sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                sSQL += clsMain.SQLString(Amount.ToString()) + ",";
              //  sSQL += clsMain.SQLString(DateTime.Parse(dtpActive.Text + " " + timeActive.Text).ToString()) + ",";
               // sSQL += clsMain.SQLString(DateTime.Parse(dtpExp.Text + " " + timeExp.Text).ToString()) + ",";

                sSQL += clsMain.SQLString(dtpActive.Text + " " + timeActive.Text) + ",";
                sSQL += clsMain.SQLString(dtpExp.Text + " " + timeExp.Text) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID);
            }
            else if ((sMa != "" && btnSua.Enabled))
            {
                sSQL += "EXEC SP_UpdateTheKhuyenMai ";
                sSQL += clsMain.SQLString(txtMa.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                sSQL += clsMain.SQLString(txtSoLuong.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + ",";
                sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                //sSQL += clsMain.SQLString(DateTime.Parse(dtpActive.Text + " " + timeActive.Text).ToString()) + ",";
                //sSQL += clsMain.SQLString(DateTime.Parse(dtpExp.Text + " " + timeExp.Text).ToString()) + ",";
                sSQL += clsMain.SQLString(dtpActive.Text + " " + timeActive.Text) + ",";
                sSQL += clsMain.SQLString(dtpExp.Text + " " + timeExp.Text) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                MessageError("Lưu không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtMa.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtSoLuong.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOLUONG").ToString();
            txtGhiChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            dtpActive.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DATE_ACTIVE").ToString());
            timeActive.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIME_ACTIVE").ToString());
            dtpExp.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DATE_EXP").ToString());
            timeExp.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIME_EXP").ToString());

            if (int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_PT").ToString()) == 0)
            {
                chkTien.Checked = true;
                txtGiaTri.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_T").ToString();
            }
            else {
                chkPhanTram.Checked = true;
                txtGiaTri.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_PT").ToString();
            }

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectTheKhuyenMai");
            gridControl2.DataSource = myDT;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("9");
            btnSua.Enabled = clsUserManagement.AllowEdit("9");
            btnXoa.Enabled = clsUserManagement.AllowDelete("9");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtMa.Properties.ReadOnly = sBoolean;
            btnAuto.Enabled = !sBoolean;
            txtTen.Properties.ReadOnly = sBoolean;
            txtSoLuong.Properties.ReadOnly = sBoolean;
            chkTien.Properties.ReadOnly = sBoolean;
            chkPhanTram.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            txtGiaTri.Properties.ReadOnly = sBoolean;
            dtpActive.Properties.ReadOnly = sBoolean;
            timeActive.Properties.ReadOnly = sBoolean;
            dtpExp.Properties.ReadOnly = sBoolean;
            timeExp.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            txtMa.Text = "";
            txtTen.Text = "";
            txtSoLuong.Text = "";
            txtGhiChu.Text = "";
            txtGiaTri.Text = "";
            dtpActive.EditValue = clsMain.GetServerDate();
            timeActive.EditValue = "00:00:00";
            dtpExp.EditValue = clsMain.GetServerDate();
            timeExp.EditValue = "00:00:00";
        }

        private Boolean CheckInput()
        {
            if (txtMa.Text == "")
            {
                txtMa.Focus();
                MessageInformation("Chưa Nhập Mã", "Thông báo");
                return false;
            }
            if (sMa == "" && btnThem.Enabled && txtMa.Text != "" && clsMain.ReturnDataTable("EXEC SP_CheckTheKhuyenMai " + clsMain.SQLString(txtMa.Text)).Rows.Count > 0)
            {
                txtMa.Focus();
                MessageInformation("Mã Khuyến Mãi Đã Tồn Tại", "Thông báo");
                return false;
            }
            if ( txtTen.Text == "" )
            {
                txtTen.Focus();
                MessageInformation("Chưa Nhập Tên", "Thông báo");
             
                return false;
            }
            if (txtSoLuong.Text == "")
            {
                txtSoLuong.Focus();
                MessageInformation("Chưa Nhập Số Lượng Dùng", "Thông báo");
                return false;
            }
            if (txtGiaTri.Text == "")
            {
                txtGiaTri.Focus();
                MessageInformation("Chưa Nhập Giá Trị", "Thông báo");
                return false;
            }
            if (chkPhanTram.Checked == true)
            {
                if (int.Parse(txtGiaTri.Text) > 100 || int.Parse(txtGiaTri.Text) < 0)
                {
                    txtGiaTri.Focus();
                    MessageInformation("Vui Lòng Nhập Phần Trăm Nhỏ Hơn 100 Và Lớn Hơn 0", "Thông báo");
                    return false;
                }
            }
            else
            {
                if (int.Parse(txtGiaTri.Text) < 1000)
                {
                    txtGiaTri.Focus();
                    MessageInformation("Vui Lòng Nhập Số Tiền Voucher Lớn Hơn 1000", "Thông báo");
                    return false;
                }
            }

            return true;
        }

        private void btn_Auto_Click(object sender, EventArgs e)
        {
            txtMa.Text = clsMain.ReturnDataTable("select [dbo].[fc_NewCodeMaTheKhuyenMai]()").Rows[0][0].ToString();
        }

        private void MessageInformation(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private DialogResult MessageQuestion(String sString, String sCaption)
        {
            return XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }



    }
}