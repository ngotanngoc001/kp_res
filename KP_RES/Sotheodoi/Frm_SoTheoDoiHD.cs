﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_SoTheoDoiHD : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";

        public Frm_SoTheoDoiHD()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Nghiệp vụ";
            }
        }

        private void Frm_SoTheoDoiHD_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboSoHopDong.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteSoTheoDoi " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            cboSoHopDong.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            float sConLai = 0;
            float sTongTien = 0;
            if (chkTTDot1.Checked)
                sTongTien += float.Parse(txtTienDot1.Text);

            if (chkTTDot2.Checked)
                sTongTien += float.Parse(txtTienDot2.Text);

            if (chkTTDot3.Checked)
                sTongTien += float.Parse(txtTienDot3.Text);

            if (chkTTDot4.Checked)
                sTongTien += float.Parse(txtTienDot4.Text);

            if (chkTTDot5.Checked)
                sTongTien += float.Parse(txtTienDot5.Text);

            if (chkTTDot6.Checked)
                sTongTien += float.Parse(txtTienDot6.Text);

            if (chkTTDot7.Checked)
                sTongTien += float.Parse(txtTienDot7.Text);

            if (chkTTDot8.Checked)
                sTongTien += float.Parse(txtTienDot8.Text);

            sConLai = float.Parse(txtGiaTriHopDong.Text) - sTongTien;

            //if (txtTienDot1.Text == "0")
            //    dtpNgayDot1.EditValue = null;

            //if (txtTienDot2.Text == "0")
            //    dtpNgayDot2.EditValue = null;

            //if (txtTienDot3.Text == "0")
            //    dtpNgayDot3.EditValue = null;

            //if (txtTienDot4.Text == "0")
            //    dtpNgayDot4.EditValue = null;

            //if (txtTienDot5.Text == "0")
            //    dtpNgayDot5.EditValue = null;

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertSoTheoDoi";

                sParameter = new SqlParameter[30];
                sParameter[0] = new SqlParameter("@SOHOPDONG", (Object)cboSoHopDong.EditValue.ToString());
                sParameter[1] = new SqlParameter("@NHACUNGCAP", (Object)cboNhaCungCap.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[3] = new SqlParameter("@GIATRI", (Object)txtGiaTriHopDong.Text.Replace(",", ""));
                sParameter[4] = new SqlParameter("@NGAY_DOT1", (DateTime)dtpNgayDot1.EditValue);
                sParameter[5] = new SqlParameter("@TIEN_DOT1", (Object)txtTienDot1.Text.Replace(",",""));
                sParameter[6] = new SqlParameter("@TT_DOT1", (Object)(chkTTDot1.Checked == true ? "1" : "0"));
                sParameter[7] = new SqlParameter("@NGAY_DOT2", (DateTime)dtpNgayDot2.EditValue);
                sParameter[8] = new SqlParameter("@TIEN_DOT2", (Object)txtTienDot2.Text.Replace(",", ""));
                sParameter[9] = new SqlParameter("@TT_DOT2", (Object)(chkTTDot2.Checked == true ? "1" : "0"));
                sParameter[10] = new SqlParameter("@NGAY_DOT3", (DateTime)dtpNgayDot3.EditValue);
                sParameter[11] = new SqlParameter("@TIEN_DOT3", (Object)txtTienDot3.Text.Replace(",", ""));
                sParameter[12] = new SqlParameter("@TT_DOT3", (Object)(chkTTDot3.Checked == true ? "1" : "0"));
                sParameter[13] = new SqlParameter("@NGAY_DOT4", (DateTime)dtpNgayDot4.EditValue);
                sParameter[14] = new SqlParameter("@TIEN_DOT4", (Object)txtTienDot4.Text.Replace(",", ""));
                sParameter[15] = new SqlParameter("@TT_DOT4", (Object)(chkTTDot4.Checked == true ? "1" : "0"));
                sParameter[16] = new SqlParameter("@NGAY_DOT5", (DateTime)dtpNgayDot5.EditValue);
                sParameter[17] = new SqlParameter("@TIEN_DOT5", (Object)txtTienDot5.Text.Replace(",", ""));
                sParameter[18] = new SqlParameter("@TT_DOT5", (Object)(chkTTDot5.Checked == true ? "1" : "0"));
                sParameter[19] = new SqlParameter("@NGAY_DOT6", (DateTime)dtpNgayDot6.EditValue);
                sParameter[20] = new SqlParameter("@TIEN_DOT6", (Object)txtTienDot6.Text.Replace(",", ""));
                sParameter[21] = new SqlParameter("@TT_DOT6", (Object)(chkTTDot6.Checked == true ? "1" : "0"));
                sParameter[22] = new SqlParameter("@NGAY_DOT7", (DateTime)dtpNgayDot7.EditValue);
                sParameter[23] = new SqlParameter("@TIEN_DOT7", (Object)txtTienDot7.Text.Replace(",", ""));
                sParameter[24] = new SqlParameter("@TT_DOT7", (Object)(chkTTDot7.Checked == true ? "1" : "0"));
                sParameter[25] = new SqlParameter("@NGAY_DOT8", (DateTime)dtpNgayDot8.EditValue);
                sParameter[26] = new SqlParameter("@TIEN_DOT8", (Object)txtTienDot8.Text.Replace(",", ""));
                sParameter[27] = new SqlParameter("@TT_DOT8", (Object)(chkTTDot8.Checked == true ? "1" : "0"));
                sParameter[28] = new SqlParameter("@CONLAI", (Object)sConLai);
                sParameter[29] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateSoTheoDoi";

                sParameter = new SqlParameter[31];
                sParameter[0] = new SqlParameter("@SOHOPDONG", (Object)cboSoHopDong.EditValue.ToString());
                sParameter[1] = new SqlParameter("@NHACUNGCAP", (Object)cboNhaCungCap.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[3] = new SqlParameter("@GIATRI", (Object)txtGiaTriHopDong.Text.Replace(",", ""));
                sParameter[4] = new SqlParameter("@NGAY_DOT1", (DateTime)dtpNgayDot1.EditValue);
                sParameter[5] = new SqlParameter("@TIEN_DOT1", (Object)txtTienDot1.Text.Replace(",", ""));
                sParameter[6] = new SqlParameter("@TT_DOT1", (Object)(chkTTDot1.Checked == true ? "1" : "0"));
                sParameter[7] = new SqlParameter("@NGAY_DOT2", (DateTime)dtpNgayDot2.EditValue);
                sParameter[8] = new SqlParameter("@TIEN_DOT2", (Object)txtTienDot2.Text.Replace(",", ""));
                sParameter[9] = new SqlParameter("@TT_DOT2", (Object)(chkTTDot2.Checked == true ? "1" : "0"));
                sParameter[10] = new SqlParameter("@NGAY_DOT3", (DateTime)dtpNgayDot3.EditValue);
                sParameter[11] = new SqlParameter("@TIEN_DOT3", (Object)txtTienDot3.Text.Replace(",", ""));
                sParameter[12] = new SqlParameter("@TT_DOT3", (Object)(chkTTDot3.Checked == true ? "1" : "0"));
                sParameter[13] = new SqlParameter("@NGAY_DOT4", (DateTime)dtpNgayDot4.EditValue);
                sParameter[14] = new SqlParameter("@TIEN_DOT4", (Object)txtTienDot4.Text.Replace(",", ""));
                sParameter[15] = new SqlParameter("@TT_DOT4", (Object)(chkTTDot4.Checked == true ? "1" : "0"));
                sParameter[16] = new SqlParameter("@NGAY_DOT5", (DateTime)dtpNgayDot5.EditValue);
                sParameter[17] = new SqlParameter("@TIEN_DOT5", (Object)txtTienDot5.Text.Replace(",", ""));
                sParameter[18] = new SqlParameter("@TT_DOT5", (Object)(chkTTDot5.Checked == true ? "1" : "0"));
                sParameter[19] = new SqlParameter("@NGAY_DOT6", (DateTime)dtpNgayDot6.EditValue);
                sParameter[20] = new SqlParameter("@TIEN_DOT6", (Object)txtTienDot6.Text.Replace(",", ""));
                sParameter[21] = new SqlParameter("@TT_DOT6", (Object)(chkTTDot6.Checked == true ? "1" : "0"));
                sParameter[22] = new SqlParameter("@NGAY_DOT7", (DateTime)dtpNgayDot7.EditValue);
                sParameter[23] = new SqlParameter("@TIEN_DOT7", (Object)txtTienDot7.Text.Replace(",", ""));
                sParameter[24] = new SqlParameter("@TT_DOT7", (Object)(chkTTDot7.Checked == true ? "1" : "0"));
                sParameter[25] = new SqlParameter("@NGAY_DOT8", (DateTime)dtpNgayDot8.EditValue);
                sParameter[26] = new SqlParameter("@TIEN_DOT8", (Object)txtTienDot8.Text.Replace(",", ""));
                sParameter[27] = new SqlParameter("@TT_DOT8", (Object)(chkTTDot8.Checked == true ? "1" : "0"));
                sParameter[28] = new SqlParameter("@CONLAI", (Object)sConLai);
                sParameter[29] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[30] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtNoiDung.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NOIDUNG").ToString();
            txtGiaTriHopDong.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI");
            txtGhiChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            txtTienDot1.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT1");
            txtTienDot2.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT2");
            txtTienDot3.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT3");
            txtTienDot4.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT4");
            txtTienDot5.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT5");
            txtTienDot6.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT6");
            txtTienDot7.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT7");
            txtTienDot8.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEN_DOT8");
            chkTTDot1.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT1").ToString());
            chkTTDot2.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT2").ToString());
            chkTTDot3.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT3").ToString());
            chkTTDot4.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT4").ToString());
            chkTTDot5.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT5").ToString());
            chkTTDot6.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT6").ToString());
            chkTTDot7.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT7").ToString());
            chkTTDot8.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TT_DOT8").ToString());
            try
            {
                dtpNgayDot1.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT1").ToString());
            }
            catch
            {
                dtpNgayDot1.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot2.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT2").ToString());
            }
            catch
            {
                dtpNgayDot2.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot3.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT3").ToString());
            }
            catch
            {
                dtpNgayDot3.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot4.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT4").ToString());
            }
            catch
            {
                dtpNgayDot4.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot5.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT5").ToString());
            }
            catch
            {
                dtpNgayDot5.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot6.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT6").ToString());
            }
            catch
            {
                dtpNgayDot6.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot7.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT7").ToString());
            }
            catch
            {
                dtpNgayDot7.EditValue = clsMain.GetServerDate();
            }
            try
            {
                dtpNgayDot8.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_DOT8").ToString());
            }
            catch
            {
                dtpNgayDot8.EditValue = clsMain.GetServerDate();
            }
            cboSoHopDong.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ID_SOHOPDONG").ToString());
            cboNhaCungCap.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ID_NHACUNGCAP").ToString());

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectSoTheoDoi");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoSoHopDong");
            clsQLCongTy.LoadCombo(myDT, cboSoHopDong);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoNhaCungCap");
            clsQLCongTy.LoadCombo(myDT, cboNhaCungCap);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("30081602");
            btnSua.Enabled = clsUserManagement.AllowEdit("30081602");
            btnXoa.Enabled = clsUserManagement.AllowDelete("30081602");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtNoiDung.Properties.ReadOnly = sBoolean;
            txtGiaTriHopDong.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            txtTienDot1.Properties.ReadOnly = sBoolean;
            txtTienDot2.Properties.ReadOnly = sBoolean;
            txtTienDot3.Properties.ReadOnly = sBoolean;
            txtTienDot4.Properties.ReadOnly = sBoolean;
            txtTienDot5.Properties.ReadOnly = sBoolean;
            txtTienDot6.Properties.ReadOnly = sBoolean;
            txtTienDot7.Properties.ReadOnly = sBoolean;
            txtTienDot8.Properties.ReadOnly = sBoolean;
            chkTTDot1.Properties.ReadOnly = sBoolean;
            chkTTDot2.Properties.ReadOnly = sBoolean;
            chkTTDot3.Properties.ReadOnly = sBoolean;
            chkTTDot4.Properties.ReadOnly = sBoolean;
            chkTTDot5.Properties.ReadOnly = sBoolean;
            chkTTDot6.Properties.ReadOnly = sBoolean;
            chkTTDot7.Properties.ReadOnly = sBoolean;
            chkTTDot8.Properties.ReadOnly = sBoolean;
            dtpNgayDot1.Properties.ReadOnly = sBoolean;
            dtpNgayDot2.Properties.ReadOnly = sBoolean;
            dtpNgayDot3.Properties.ReadOnly = sBoolean;
            dtpNgayDot4.Properties.ReadOnly = sBoolean;
            dtpNgayDot5.Properties.ReadOnly = sBoolean;
            dtpNgayDot6.Properties.ReadOnly = sBoolean;
            dtpNgayDot7.Properties.ReadOnly = sBoolean;
            dtpNgayDot8.Properties.ReadOnly = sBoolean;
            cboSoHopDong.Properties.ReadOnly = sBoolean;
            cboNhaCungCap.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            txtNoiDung.Text = "";
            txtGiaTriHopDong.EditValue = "0";
            txtGhiChu.Text = "";
            txtTienDot1.EditValue = "0";
            txtTienDot2.EditValue = "0";
            txtTienDot3.EditValue = "0";
            txtTienDot4.EditValue = "0";
            txtTienDot5.EditValue = "0";
            txtTienDot6.EditValue = "0";
            txtTienDot7.EditValue = "0";
            txtTienDot8.EditValue = "0";
            chkTTDot1.Checked = false;
            chkTTDot2.Checked = false;
            chkTTDot3.Checked = false;
            chkTTDot4.Checked = false;
            chkTTDot5.Checked = false;
            chkTTDot6.Checked = false;
            chkTTDot7.Checked = false;
            chkTTDot8.Checked = false;
            dtpNgayDot1.EditValue = clsMain.GetServerDate();
            dtpNgayDot2.EditValue = clsMain.GetServerDate();
            dtpNgayDot3.EditValue = clsMain.GetServerDate();
            dtpNgayDot4.EditValue = clsMain.GetServerDate();
            dtpNgayDot5.EditValue = clsMain.GetServerDate();
            dtpNgayDot6.EditValue = clsMain.GetServerDate();
            dtpNgayDot7.EditValue = clsMain.GetServerDate();
            dtpNgayDot8.EditValue = clsMain.GetServerDate();
            cboSoHopDong.EditValue = cboSoHopDong.Properties.GetDataSourceValue(cboSoHopDong.Properties.ValueMember, 0);
            cboNhaCungCap.EditValue = cboNhaCungCap.Properties.GetDataSourceValue(cboNhaCungCap.Properties.ValueMember, 0);
        }

        private Boolean CheckInput()
        {
            if (cboSoHopDong.EditValue == "" || cboSoHopDong.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblSoHopDong.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboSoHopDong.Focus();
                return false;
            }
            if (cboNhaCungCap.EditValue == "" || cboNhaCungCap.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblNhaCungCap.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNhaCungCap.Focus();
                return false;
            }

            return true;
        }

       
    }
}