﻿namespace KP_RES
{
    partial class Frm_BaoCao_SoTheoDoiHD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCao_SoTheoDoiHD));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.cboData = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTuNgay = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblDenNgay = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ID_SOHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID_NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CONLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 674);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 96);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 541);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 80);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 459);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.dtpTuNgay);
            this.pnChoose.Controls.Add(this.cboData);
            this.pnChoose.Controls.Add(this.lblTuNgay);
            this.pnChoose.Controls.Add(this.dtpDenNgay);
            this.pnChoose.Controls.Add(this.lblTitle);
            this.pnChoose.Controls.Add(this.lblDenNgay);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 78);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(87, 4);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpTuNgay.TabIndex = 8;
            this.dtpTuNgay.Visible = false;
            // 
            // cboData
            // 
            this.cboData.Location = new System.Drawing.Point(3, 45);
            this.cboData.Name = "cboData";
            this.cboData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.Appearance.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboData.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboData.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboData.Properties.DisplayMember = "TEN";
            this.cboData.Properties.DropDownItemHeight = 40;
            this.cboData.Properties.NullText = "";
            this.cboData.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboData.Properties.ShowHeader = false;
            this.cboData.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboData.Properties.ValueMember = "MA";
            this.cboData.Size = new System.Drawing.Size(210, 26);
            this.cboData.TabIndex = 17;
            this.cboData.Visible = false;
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTuNgay.Location = new System.Drawing.Point(3, 8);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(72, 19);
            this.lblTuNgay.TabIndex = 10;
            this.lblTuNgay.Text = "Từ Ngày :";
            this.lblTuNgay.Visible = false;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(87, 44);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpDenNgay.TabIndex = 9;
            this.dtpDenNgay.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTitle.Location = new System.Drawing.Point(3, 8);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(84, 19);
            this.lblTitle.TabIndex = 16;
            this.lblTitle.Text = "Tên Cấp Độ";
            this.lblTitle.Visible = false;
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblDenNgay.Location = new System.Drawing.Point(3, 48);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(80, 19);
            this.lblDenNgay.TabIndex = 11;
            this.lblDenNgay.Text = "Đến Ngày :";
            this.lblDenNgay.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 94);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "0";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "Theo sổ hợp đồng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "Theo nhà cung cấp")});
            this.optGroup.Size = new System.Drawing.Size(215, 89);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 637);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatFile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1323, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 674);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 350);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 190);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 270);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 187);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 592);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 162);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatFile
            // 
            this.btnXuatFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatFile.Appearance.Options.UseFont = true;
            this.btnXuatFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatFile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatFile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatFile.Name = "btnXuatFile";
            this.btnXuatFile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatFile.TabIndex = 14;
            this.btnXuatFile.Text = "&Xuất File";
            this.btnXuatFile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(223, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1100, 674);
            this.gridControl2.TabIndex = 9;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ID_SOHOPDONG,
            this.ID_NHACUNGCAP,
            this.STT,
            this.SOHOPDONG,
            this.NHACUNGCAP,
            this.NOIDUNG,
            this.GIATRI,
            this.NGAY_DOT1,
            this.TIEN_DOT1,
            this.TT_DOT1,
            this.NGAY_DOT2,
            this.TIEN_DOT2,
            this.TT_DOT2,
            this.NGAY_DOT3,
            this.TIEN_DOT3,
            this.TT_DOT3,
            this.NGAY_DOT4,
            this.TIEN_DOT4,
            this.TT_DOT4,
            this.NGAY_DOT5,
            this.TIEN_DOT5,
            this.TT_DOT5,
            this.CONLAI,
            this.GHICHU,
            this.FILL,
            this.NGAY_DOT6,
            this.NGAY_DOT7,
            this.NGAY_DOT8,
            this.TIEN_DOT6,
            this.TIEN_DOT7,
            this.TIEN_DOT8,
            this.TT_DOT6,
            this.TT_DOT7,
            this.TT_DOT8});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // ID_SOHOPDONG
            // 
            this.ID_SOHOPDONG.FieldName = "ID_SOHOPDONG";
            this.ID_SOHOPDONG.Name = "ID_SOHOPDONG";
            this.ID_SOHOPDONG.OptionsColumn.AllowEdit = false;
            this.ID_SOHOPDONG.OptionsColumn.AllowFocus = false;
            this.ID_SOHOPDONG.OptionsColumn.AllowMove = false;
            // 
            // ID_NHACUNGCAP
            // 
            this.ID_NHACUNGCAP.FieldName = "ID_NHACUNGCAP";
            this.ID_NHACUNGCAP.Name = "ID_NHACUNGCAP";
            this.ID_NHACUNGCAP.OptionsColumn.AllowEdit = false;
            this.ID_NHACUNGCAP.OptionsColumn.AllowFocus = false;
            this.ID_NHACUNGCAP.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // SOHOPDONG
            // 
            this.SOHOPDONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOHOPDONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOHOPDONG.AppearanceHeader.Options.UseFont = true;
            this.SOHOPDONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOHOPDONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOHOPDONG.Caption = "Sổ hợp đồng";
            this.SOHOPDONG.FieldName = "SOHOPDONG";
            this.SOHOPDONG.Name = "SOHOPDONG";
            this.SOHOPDONG.OptionsColumn.AllowEdit = false;
            this.SOHOPDONG.OptionsColumn.AllowFocus = false;
            this.SOHOPDONG.OptionsColumn.FixedWidth = true;
            this.SOHOPDONG.Visible = true;
            this.SOHOPDONG.VisibleIndex = 1;
            this.SOHOPDONG.Width = 200;
            // 
            // NHACUNGCAP
            // 
            this.NHACUNGCAP.AppearanceCell.Options.UseTextOptions = true;
            this.NHACUNGCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHACUNGCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHACUNGCAP.AppearanceHeader.Options.UseFont = true;
            this.NHACUNGCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.NHACUNGCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHACUNGCAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHACUNGCAP.Caption = "Đơn vị cung cấp";
            this.NHACUNGCAP.FieldName = "NHACUNGCAP";
            this.NHACUNGCAP.Name = "NHACUNGCAP";
            this.NHACUNGCAP.OptionsColumn.AllowEdit = false;
            this.NHACUNGCAP.OptionsColumn.AllowFocus = false;
            this.NHACUNGCAP.OptionsColumn.FixedWidth = true;
            this.NHACUNGCAP.Visible = true;
            this.NHACUNGCAP.VisibleIndex = 2;
            this.NHACUNGCAP.Width = 200;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 3;
            this.NOIDUNG.Width = 250;
            // 
            // GIATRI
            // 
            this.GIATRI.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI.AppearanceHeader.Options.UseFont = true;
            this.GIATRI.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI.Caption = "Giá trị HĐ";
            this.GIATRI.DisplayFormat.FormatString = "N0";
            this.GIATRI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI.FieldName = "GIATRI";
            this.GIATRI.Name = "GIATRI";
            this.GIATRI.OptionsColumn.AllowEdit = false;
            this.GIATRI.OptionsColumn.AllowFocus = false;
            this.GIATRI.OptionsColumn.FixedWidth = true;
            this.GIATRI.Visible = true;
            this.GIATRI.VisibleIndex = 4;
            this.GIATRI.Width = 150;
            // 
            // NGAY_DOT1
            // 
            this.NGAY_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT1.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT1.Caption = "Ngày TT đợt 1";
            this.NGAY_DOT1.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT1.FieldName = "NGAY_DOT1";
            this.NGAY_DOT1.Name = "NGAY_DOT1";
            this.NGAY_DOT1.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT1.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT1.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT1.Visible = true;
            this.NGAY_DOT1.VisibleIndex = 5;
            this.NGAY_DOT1.Width = 150;
            // 
            // TIEN_DOT1
            // 
            this.TIEN_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT1.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT1.Caption = "Tiền TT đợt 1";
            this.TIEN_DOT1.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT1.FieldName = "TIEN_DOT1";
            this.TIEN_DOT1.Name = "TIEN_DOT1";
            this.TIEN_DOT1.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT1.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT1.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT1.Visible = true;
            this.TIEN_DOT1.VisibleIndex = 6;
            this.TIEN_DOT1.Width = 150;
            // 
            // TT_DOT1
            // 
            this.TT_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT1.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT1.Caption = "Đã TT đợt 1";
            this.TT_DOT1.FieldName = "TT_DOT1";
            this.TT_DOT1.Name = "TT_DOT1";
            this.TT_DOT1.OptionsColumn.AllowEdit = false;
            this.TT_DOT1.OptionsColumn.AllowFocus = false;
            this.TT_DOT1.OptionsColumn.FixedWidth = true;
            this.TT_DOT1.Visible = true;
            this.TT_DOT1.VisibleIndex = 7;
            this.TT_DOT1.Width = 150;
            // 
            // NGAY_DOT2
            // 
            this.NGAY_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT2.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT2.Caption = "Ngày TT đợt 2";
            this.NGAY_DOT2.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT2.FieldName = "NGAY_DOT2";
            this.NGAY_DOT2.Name = "NGAY_DOT2";
            this.NGAY_DOT2.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT2.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT2.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT2.Visible = true;
            this.NGAY_DOT2.VisibleIndex = 8;
            this.NGAY_DOT2.Width = 150;
            // 
            // TIEN_DOT2
            // 
            this.TIEN_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT2.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT2.Caption = "Tiền TT đợt 2";
            this.TIEN_DOT2.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT2.FieldName = "TIEN_DOT2";
            this.TIEN_DOT2.Name = "TIEN_DOT2";
            this.TIEN_DOT2.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT2.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT2.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT2.Visible = true;
            this.TIEN_DOT2.VisibleIndex = 9;
            this.TIEN_DOT2.Width = 150;
            // 
            // TT_DOT2
            // 
            this.TT_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT2.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT2.Caption = "Đã TT đợt 2";
            this.TT_DOT2.FieldName = "TT_DOT2";
            this.TT_DOT2.Name = "TT_DOT2";
            this.TT_DOT2.OptionsColumn.AllowEdit = false;
            this.TT_DOT2.OptionsColumn.AllowFocus = false;
            this.TT_DOT2.OptionsColumn.FixedWidth = true;
            this.TT_DOT2.Visible = true;
            this.TT_DOT2.VisibleIndex = 10;
            this.TT_DOT2.Width = 150;
            // 
            // NGAY_DOT3
            // 
            this.NGAY_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT3.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT3.Caption = "Ngày TT đợt 3";
            this.NGAY_DOT3.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT3.FieldName = "NGAY_DOT3";
            this.NGAY_DOT3.Name = "NGAY_DOT3";
            this.NGAY_DOT3.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT3.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT3.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT3.Visible = true;
            this.NGAY_DOT3.VisibleIndex = 11;
            this.NGAY_DOT3.Width = 150;
            // 
            // TIEN_DOT3
            // 
            this.TIEN_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT3.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT3.Caption = "Tiền TT đợt 3";
            this.TIEN_DOT3.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT3.FieldName = "TIEN_DOT3";
            this.TIEN_DOT3.Name = "TIEN_DOT3";
            this.TIEN_DOT3.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT3.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT3.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT3.Visible = true;
            this.TIEN_DOT3.VisibleIndex = 12;
            this.TIEN_DOT3.Width = 150;
            // 
            // TT_DOT3
            // 
            this.TT_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT3.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT3.Caption = "Đã TT đợt 3";
            this.TT_DOT3.FieldName = "TT_DOT3";
            this.TT_DOT3.Name = "TT_DOT3";
            this.TT_DOT3.OptionsColumn.AllowEdit = false;
            this.TT_DOT3.OptionsColumn.AllowFocus = false;
            this.TT_DOT3.OptionsColumn.FixedWidth = true;
            this.TT_DOT3.Visible = true;
            this.TT_DOT3.VisibleIndex = 13;
            this.TT_DOT3.Width = 150;
            // 
            // NGAY_DOT4
            // 
            this.NGAY_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT4.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT4.Caption = "Ngày TT đợt 4";
            this.NGAY_DOT4.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT4.FieldName = "NGAY_DOT4";
            this.NGAY_DOT4.Name = "NGAY_DOT4";
            this.NGAY_DOT4.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT4.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT4.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT4.Visible = true;
            this.NGAY_DOT4.VisibleIndex = 14;
            this.NGAY_DOT4.Width = 150;
            // 
            // TIEN_DOT4
            // 
            this.TIEN_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT4.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT4.Caption = "Tiền TT đợt 4";
            this.TIEN_DOT4.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT4.FieldName = "TIEN_DOT4";
            this.TIEN_DOT4.Name = "TIEN_DOT4";
            this.TIEN_DOT4.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT4.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT4.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT4.Visible = true;
            this.TIEN_DOT4.VisibleIndex = 15;
            this.TIEN_DOT4.Width = 150;
            // 
            // TT_DOT4
            // 
            this.TT_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT4.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT4.Caption = "Đã TT đợt 4";
            this.TT_DOT4.FieldName = "TT_DOT4";
            this.TT_DOT4.Name = "TT_DOT4";
            this.TT_DOT4.OptionsColumn.AllowEdit = false;
            this.TT_DOT4.OptionsColumn.AllowFocus = false;
            this.TT_DOT4.OptionsColumn.FixedWidth = true;
            this.TT_DOT4.Visible = true;
            this.TT_DOT4.VisibleIndex = 16;
            this.TT_DOT4.Width = 150;
            // 
            // NGAY_DOT5
            // 
            this.NGAY_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT5.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT5.Caption = "Ngày TT đợt 5";
            this.NGAY_DOT5.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT5.FieldName = "NGAY_DOT5";
            this.NGAY_DOT5.Name = "NGAY_DOT5";
            this.NGAY_DOT5.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT5.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT5.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT5.Visible = true;
            this.NGAY_DOT5.VisibleIndex = 17;
            this.NGAY_DOT5.Width = 150;
            // 
            // TIEN_DOT5
            // 
            this.TIEN_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT5.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT5.Caption = "Tiền TT đợt 5";
            this.TIEN_DOT5.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT5.FieldName = "TIEN_DOT5";
            this.TIEN_DOT5.Name = "TIEN_DOT5";
            this.TIEN_DOT5.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT5.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT5.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT5.Visible = true;
            this.TIEN_DOT5.VisibleIndex = 18;
            this.TIEN_DOT5.Width = 150;
            // 
            // TT_DOT5
            // 
            this.TT_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT5.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT5.Caption = "Đã TT đợt 5";
            this.TT_DOT5.FieldName = "TT_DOT5";
            this.TT_DOT5.Name = "TT_DOT5";
            this.TT_DOT5.OptionsColumn.AllowEdit = false;
            this.TT_DOT5.OptionsColumn.AllowFocus = false;
            this.TT_DOT5.OptionsColumn.FixedWidth = true;
            this.TT_DOT5.Visible = true;
            this.TT_DOT5.VisibleIndex = 19;
            this.TT_DOT5.Width = 150;
            // 
            // CONLAI
            // 
            this.CONLAI.AppearanceCell.Options.UseTextOptions = true;
            this.CONLAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CONLAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CONLAI.AppearanceHeader.Options.UseFont = true;
            this.CONLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.CONLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CONLAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.Caption = "Còn lại";
            this.CONLAI.DisplayFormat.FormatString = "N0";
            this.CONLAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CONLAI.FieldName = "CONLAI";
            this.CONLAI.Name = "CONLAI";
            this.CONLAI.OptionsColumn.AllowEdit = false;
            this.CONLAI.OptionsColumn.AllowFocus = false;
            this.CONLAI.OptionsColumn.FixedWidth = true;
            this.CONLAI.Visible = true;
            this.CONLAI.VisibleIndex = 29;
            this.CONLAI.Width = 150;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 30;
            this.GHICHU.Width = 200;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 31;
            this.FILL.Width = 20;
            // 
            // NGAY_DOT6
            // 
            this.NGAY_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT6.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT6.Caption = "Ngày TT đợt 6";
            this.NGAY_DOT6.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT6.FieldName = "NGAY_DOT6";
            this.NGAY_DOT6.Name = "NGAY_DOT6";
            this.NGAY_DOT6.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT6.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT6.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT6.Visible = true;
            this.NGAY_DOT6.VisibleIndex = 20;
            this.NGAY_DOT6.Width = 149;
            // 
            // NGAY_DOT7
            // 
            this.NGAY_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT7.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT7.Caption = "Ngày TT đợt 7";
            this.NGAY_DOT7.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT7.FieldName = "NGAY_DOT7";
            this.NGAY_DOT7.Name = "NGAY_DOT7";
            this.NGAY_DOT7.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT7.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT7.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT7.Visible = true;
            this.NGAY_DOT7.VisibleIndex = 23;
            this.NGAY_DOT7.Width = 160;
            // 
            // NGAY_DOT8
            // 
            this.NGAY_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT8.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT8.Caption = "Ngày TT đợt 8";
            this.NGAY_DOT8.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT8.FieldName = "NGAY_DOT8";
            this.NGAY_DOT8.Name = "NGAY_DOT8";
            this.NGAY_DOT8.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT8.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT8.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT8.Visible = true;
            this.NGAY_DOT8.VisibleIndex = 26;
            this.NGAY_DOT8.Width = 162;
            // 
            // TIEN_DOT6
            // 
            this.TIEN_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT6.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT6.Caption = "Tiền TT đợt 6";
            this.TIEN_DOT6.FieldName = "TIEN_DOT6";
            this.TIEN_DOT6.Name = "TIEN_DOT6";
            this.TIEN_DOT6.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT6.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT6.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT6.Visible = true;
            this.TIEN_DOT6.VisibleIndex = 21;
            this.TIEN_DOT6.Width = 138;
            // 
            // TIEN_DOT7
            // 
            this.TIEN_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT7.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT7.Caption = "Tiền TT đợt 7";
            this.TIEN_DOT7.FieldName = "TIEN_DOT7";
            this.TIEN_DOT7.Name = "TIEN_DOT7";
            this.TIEN_DOT7.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT7.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT7.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT7.Visible = true;
            this.TIEN_DOT7.VisibleIndex = 24;
            this.TIEN_DOT7.Width = 149;
            // 
            // TIEN_DOT8
            // 
            this.TIEN_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT8.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT8.Caption = "Tiền TT đợt 8";
            this.TIEN_DOT8.FieldName = "TIEN_DOT8";
            this.TIEN_DOT8.Name = "TIEN_DOT8";
            this.TIEN_DOT8.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT8.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT8.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT8.Visible = true;
            this.TIEN_DOT8.VisibleIndex = 27;
            this.TIEN_DOT8.Width = 161;
            // 
            // TT_DOT6
            // 
            this.TT_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT6.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT6.Caption = "Đã TT đợt 6";
            this.TT_DOT6.FieldName = "TT_DOT6";
            this.TT_DOT6.Name = "TT_DOT6";
            this.TT_DOT6.OptionsColumn.AllowEdit = false;
            this.TT_DOT6.OptionsColumn.AllowFocus = false;
            this.TT_DOT6.OptionsColumn.FixedWidth = true;
            this.TT_DOT6.Visible = true;
            this.TT_DOT6.VisibleIndex = 22;
            this.TT_DOT6.Width = 132;
            // 
            // TT_DOT7
            // 
            this.TT_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT7.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT7.Caption = "Đã TT đợt 7";
            this.TT_DOT7.FieldName = "TT_DOT7";
            this.TT_DOT7.Name = "TT_DOT7";
            this.TT_DOT7.OptionsColumn.AllowEdit = false;
            this.TT_DOT7.OptionsColumn.AllowFocus = false;
            this.TT_DOT7.OptionsColumn.FixedWidth = true;
            this.TT_DOT7.Visible = true;
            this.TT_DOT7.VisibleIndex = 25;
            this.TT_DOT7.Width = 142;
            // 
            // TT_DOT8
            // 
            this.TT_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT8.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT8.Caption = "Đã TT đợt 8";
            this.TT_DOT8.FieldName = "TT_DOT8";
            this.TT_DOT8.Name = "TT_DOT8";
            this.TT_DOT8.OptionsColumn.AllowEdit = false;
            this.TT_DOT8.OptionsColumn.AllowFocus = false;
            this.TT_DOT8.OptionsColumn.FixedWidth = true;
            this.TT_DOT8.Visible = true;
            this.TT_DOT8.VisibleIndex = 28;
            this.TT_DOT8.Width = 137;
            // 
            // Frm_BaoCao_SoTheoDoiHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 674);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_BaoCao_SoTheoDoiHD";
            this.Text = "Danh sách hợp đồng";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatFile;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private DevExpress.XtraEditors.LabelControl lblTuNgay;
        private DevExpress.XtraEditors.LabelControl lblDenNgay;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LookUpEdit cboData;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn ID_SOHOPDONG;
        private DevExpress.XtraGrid.Columns.GridColumn ID_NHACUNGCAP;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn SOHOPDONG;
        private DevExpress.XtraGrid.Columns.GridColumn NHACUNGCAP;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT2;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT2;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT2;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT5;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT5;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT5;
        private DevExpress.XtraGrid.Columns.GridColumn CONLAI;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT8;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT8;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT8;



    }
}