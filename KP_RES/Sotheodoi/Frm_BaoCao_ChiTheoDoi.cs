﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;

namespace KP_RES
{
    public partial class Frm_BaoCao_ChiTheoDoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_BaoCao_ChiTheoDoi()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisibleChoose();
        }

        private void VisibleChoose()
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

            pnChoose.Visible = (optGroup.SelectedIndex == 0) ? false : true;

            if (optGroup.SelectedIndex == 1)
            {
                Visible(true);
                clsQLCongTy.LoadCombo("SP_ComBoSoHopDong", cboData);
            }
            else if (optGroup.SelectedIndex == 2)
            {
                Visible(true);
                clsQLCongTy.LoadCombo("SP_ComBoNhaCungCap", cboData);
            }
            else if (optGroup.SelectedIndex == 3)
            {
                Visible(false);
            }
        }

        private void Visible(Boolean sBoolean)
        {
            lblTitle.Visible = cboData.Visible = sBoolean;
            lblTuNgay.Visible = lblDenNgay.Visible = dtpDenNgay.Visible = dtpTuNgay.Visible = !sBoolean;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";
                String sTuNgay = "";
                String sDenNgay = "";

                if (optGroup.EditValue.Equals("3"))
                {
                    sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.Value);
                    sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.Value);
                }
                else if (optGroup.EditValue != "0")
                    sMa = cboData.EditValue.ToString();

                SqlParameter[] sParameter = new SqlParameter[4];
                sParameter[0] = new SqlParameter("@MA", (Object)sMa);
                sParameter[1] = new SqlParameter("@TUNGAY", (Object)sTuNgay);
                sParameter[2] = new SqlParameter("@DENNGAY", (Object)sDenNgay);
                sParameter[3] = new SqlParameter("@STYLE", (Object)optGroup.EditValue);

                DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectBCChiTheoDoi", sParameter);
                grcCongVanDen.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void grvCongVanDen_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvCongVanDen.Focus();
                grvCongVanDen.FocusedRowHandle = grvCongVanDen.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                grvCongVanDen.Focus();
                grvCongVanDen.FocusedRowHandle = grvCongVanDen.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (grvCongVanDen.FocusedRowHandle == grvCongVanDen.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                grvCongVanDen.Focus();
                grvCongVanDen.FocusedRowHandle = grvCongVanDen.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (grvCongVanDen.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvCongVanDen.Focus();
                grvCongVanDen.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.grvCongVanDen.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                grcCongVanDen.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                grcCongVanDen.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvCongVanDen.RowCount > 0)
                {
                    DataTable myDT = (DataTable)grcCongVanDen.DataSource;

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2)
                        Description = "( " + Description + " : " + cboData.Text + " )";
                    else if (optGroup.SelectedIndex == 3)
                        Description = "( " + Description + " Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;

                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 92;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    clsQLCongTy.MessageInformation("Không có dữ liệu", "Thông báo");
            }
            catch{
            }
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            Boolean sBoolean = (columns.Contains(columnName)) ? true : false;

            return sBoolean;
        }

    }
}