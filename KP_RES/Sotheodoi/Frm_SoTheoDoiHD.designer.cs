﻿namespace KP_RES 
{
    partial class Frm_SoTheoDoiHD 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkTTDot8 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot7 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot6 = new DevExpress.XtraEditors.CheckEdit();
            this.txtTienDot8 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot8 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot7 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot7 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot6 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot6 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.chkTTDot5 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot4 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot3 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkTTDot1 = new DevExpress.XtraEditors.CheckEdit();
            this.txtTienDot5 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot5 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot4 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot4 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot3 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot3 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot2 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot2 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienDot1 = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayDot1 = new DevExpress.XtraEditors.DateEdit();
            this.txtGiaTriHopDong = new DevExpress.XtraEditors.TextEdit();
            this.cboNhaCungCap = new DevExpress.XtraEditors.LookUpEdit();
            this.cboSoHopDong = new DevExpress.XtraEditors.LookUpEdit();
            this.lblNoiDung = new DevExpress.XtraEditors.LabelControl();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.lblSoVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblLoaiVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblSoHopDong = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNhaCungCap = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ID_SOHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID_NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TT_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEN_DOT8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CONLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot8.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot7.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot6.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot5.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTriHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSoHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkTTDot8);
            this.panelControl1.Controls.Add(this.chkTTDot7);
            this.panelControl1.Controls.Add(this.chkTTDot6);
            this.panelControl1.Controls.Add(this.txtTienDot8);
            this.panelControl1.Controls.Add(this.dtpNgayDot8);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtTienDot7);
            this.panelControl1.Controls.Add(this.dtpNgayDot7);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txtTienDot6);
            this.panelControl1.Controls.Add(this.dtpNgayDot6);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.chkTTDot5);
            this.panelControl1.Controls.Add(this.chkTTDot4);
            this.panelControl1.Controls.Add(this.chkTTDot3);
            this.panelControl1.Controls.Add(this.chkTTDot2);
            this.panelControl1.Controls.Add(this.chkTTDot1);
            this.panelControl1.Controls.Add(this.txtTienDot5);
            this.panelControl1.Controls.Add(this.dtpNgayDot5);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtTienDot4);
            this.panelControl1.Controls.Add(this.dtpNgayDot4);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtTienDot3);
            this.panelControl1.Controls.Add(this.dtpNgayDot3);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtTienDot2);
            this.panelControl1.Controls.Add(this.dtpNgayDot2);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtGhiChu);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtTienDot1);
            this.panelControl1.Controls.Add(this.dtpNgayDot1);
            this.panelControl1.Controls.Add(this.txtGiaTriHopDong);
            this.panelControl1.Controls.Add(this.cboNhaCungCap);
            this.panelControl1.Controls.Add(this.cboSoHopDong);
            this.panelControl1.Controls.Add(this.lblNoiDung);
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.lblSoVanBan);
            this.panelControl1.Controls.Add(this.lblLoaiVanBan);
            this.panelControl1.Controls.Add(this.lblSoHopDong);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lblNhaCungCap);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1354, 205);
            this.panelControl1.TabIndex = 0;
            // 
            // chkTTDot8
            // 
            this.chkTTDot8.EnterMoveNextControl = true;
            this.chkTTDot8.Location = new System.Drawing.Point(1238, 100);
            this.chkTTDot8.Name = "chkTTDot8";
            this.chkTTDot8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot8.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot8.Properties.Caption = "Đã TT đợt 8";
            this.chkTTDot8.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot8.TabIndex = 45;
            // 
            // chkTTDot7
            // 
            this.chkTTDot7.EnterMoveNextControl = true;
            this.chkTTDot7.Location = new System.Drawing.Point(1238, 69);
            this.chkTTDot7.Name = "chkTTDot7";
            this.chkTTDot7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot7.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot7.Properties.Caption = "Đã TT đợt 7";
            this.chkTTDot7.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot7.TabIndex = 41;
            // 
            // chkTTDot6
            // 
            this.chkTTDot6.EnterMoveNextControl = true;
            this.chkTTDot6.Location = new System.Drawing.Point(1238, 38);
            this.chkTTDot6.Name = "chkTTDot6";
            this.chkTTDot6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot6.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot6.Properties.Caption = "Đã TT đợt 6";
            this.chkTTDot6.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot6.TabIndex = 37;
            // 
            // txtTienDot8
            // 
            this.txtTienDot8.EnterMoveNextControl = true;
            this.txtTienDot8.Location = new System.Drawing.Point(1098, 100);
            this.txtTienDot8.Name = "txtTienDot8";
            this.txtTienDot8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot8.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot8.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot8.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot8.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot8.Properties.Mask.EditMask = "N0";
            this.txtTienDot8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot8.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot8.TabIndex = 44;
            // 
            // dtpNgayDot8
            // 
            this.dtpNgayDot8.EditValue = null;
            this.dtpNgayDot8.EnterMoveNextControl = true;
            this.dtpNgayDot8.Location = new System.Drawing.Point(984, 100);
            this.dtpNgayDot8.Name = "dtpNgayDot8";
            this.dtpNgayDot8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot8.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot8.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot8.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot8.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot8.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot8.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot8.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot8.TabIndex = 43;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(910, 103);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(62, 19);
            this.labelControl6.TabIndex = 42;
            this.labelControl6.Text = "TT đợt 8";
            // 
            // txtTienDot7
            // 
            this.txtTienDot7.EnterMoveNextControl = true;
            this.txtTienDot7.Location = new System.Drawing.Point(1098, 69);
            this.txtTienDot7.Name = "txtTienDot7";
            this.txtTienDot7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot7.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot7.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot7.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot7.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot7.Properties.Mask.EditMask = "N0";
            this.txtTienDot7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot7.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot7.TabIndex = 40;
            // 
            // dtpNgayDot7
            // 
            this.dtpNgayDot7.EditValue = null;
            this.dtpNgayDot7.EnterMoveNextControl = true;
            this.dtpNgayDot7.Location = new System.Drawing.Point(984, 69);
            this.dtpNgayDot7.Name = "dtpNgayDot7";
            this.dtpNgayDot7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot7.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot7.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot7.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot7.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot7.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot7.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot7.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot7.TabIndex = 39;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(910, 72);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(62, 19);
            this.labelControl7.TabIndex = 38;
            this.labelControl7.Text = "TT đợt 7";
            // 
            // txtTienDot6
            // 
            this.txtTienDot6.EnterMoveNextControl = true;
            this.txtTienDot6.Location = new System.Drawing.Point(1098, 38);
            this.txtTienDot6.Name = "txtTienDot6";
            this.txtTienDot6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot6.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot6.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot6.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot6.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot6.Properties.Mask.EditMask = "N0";
            this.txtTienDot6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot6.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot6.TabIndex = 36;
            // 
            // dtpNgayDot6
            // 
            this.dtpNgayDot6.EditValue = null;
            this.dtpNgayDot6.EnterMoveNextControl = true;
            this.dtpNgayDot6.Location = new System.Drawing.Point(984, 38);
            this.dtpNgayDot6.Name = "dtpNgayDot6";
            this.dtpNgayDot6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot6.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot6.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot6.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot6.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot6.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot6.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot6.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot6.TabIndex = 35;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(910, 41);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(62, 19);
            this.labelControl8.TabIndex = 34;
            this.labelControl8.Text = "TT đợt 6";
            // 
            // chkTTDot5
            // 
            this.chkTTDot5.EnterMoveNextControl = true;
            this.chkTTDot5.Location = new System.Drawing.Point(1238, 5);
            this.chkTTDot5.Name = "chkTTDot5";
            this.chkTTDot5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot5.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot5.Properties.Caption = "Đã TT đợt 5";
            this.chkTTDot5.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot5.TabIndex = 29;
            // 
            // chkTTDot4
            // 
            this.chkTTDot4.EnterMoveNextControl = true;
            this.chkTTDot4.Location = new System.Drawing.Point(791, 98);
            this.chkTTDot4.Name = "chkTTDot4";
            this.chkTTDot4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot4.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot4.Properties.Caption = "Đã TT đợt 4";
            this.chkTTDot4.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot4.TabIndex = 25;
            // 
            // chkTTDot3
            // 
            this.chkTTDot3.EnterMoveNextControl = true;
            this.chkTTDot3.Location = new System.Drawing.Point(791, 67);
            this.chkTTDot3.Name = "chkTTDot3";
            this.chkTTDot3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot3.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot3.Properties.Caption = "Đã TT đợt 3";
            this.chkTTDot3.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot3.TabIndex = 21;
            // 
            // chkTTDot2
            // 
            this.chkTTDot2.EnterMoveNextControl = true;
            this.chkTTDot2.Location = new System.Drawing.Point(791, 36);
            this.chkTTDot2.Name = "chkTTDot2";
            this.chkTTDot2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot2.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot2.Properties.Caption = "Đã TT đợt 2";
            this.chkTTDot2.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot2.TabIndex = 17;
            // 
            // chkTTDot1
            // 
            this.chkTTDot1.EnterMoveNextControl = true;
            this.chkTTDot1.Location = new System.Drawing.Point(791, 5);
            this.chkTTDot1.Name = "chkTTDot1";
            this.chkTTDot1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTTDot1.Properties.Appearance.Options.UseFont = true;
            this.chkTTDot1.Properties.Caption = "Đã TT đợt 1";
            this.chkTTDot1.Size = new System.Drawing.Size(119, 24);
            this.chkTTDot1.TabIndex = 13;
            // 
            // txtTienDot5
            // 
            this.txtTienDot5.EnterMoveNextControl = true;
            this.txtTienDot5.Location = new System.Drawing.Point(1098, 5);
            this.txtTienDot5.Name = "txtTienDot5";
            this.txtTienDot5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot5.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot5.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot5.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot5.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot5.Properties.Mask.EditMask = "N0";
            this.txtTienDot5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot5.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot5.TabIndex = 28;
            // 
            // dtpNgayDot5
            // 
            this.dtpNgayDot5.EditValue = null;
            this.dtpNgayDot5.EnterMoveNextControl = true;
            this.dtpNgayDot5.Location = new System.Drawing.Point(984, 5);
            this.dtpNgayDot5.Name = "dtpNgayDot5";
            this.dtpNgayDot5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot5.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot5.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot5.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot5.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot5.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot5.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot5.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot5.TabIndex = 27;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(910, 8);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(62, 19);
            this.labelControl5.TabIndex = 26;
            this.labelControl5.Text = "TT đợt 5";
            // 
            // txtTienDot4
            // 
            this.txtTienDot4.EnterMoveNextControl = true;
            this.txtTienDot4.Location = new System.Drawing.Point(651, 98);
            this.txtTienDot4.Name = "txtTienDot4";
            this.txtTienDot4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot4.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot4.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot4.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot4.Properties.Mask.EditMask = "N0";
            this.txtTienDot4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot4.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot4.TabIndex = 24;
            // 
            // dtpNgayDot4
            // 
            this.dtpNgayDot4.EditValue = null;
            this.dtpNgayDot4.EnterMoveNextControl = true;
            this.dtpNgayDot4.Location = new System.Drawing.Point(537, 98);
            this.dtpNgayDot4.Name = "dtpNgayDot4";
            this.dtpNgayDot4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot4.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot4.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot4.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot4.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot4.TabIndex = 23;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(463, 101);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 19);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "TT đợt 4";
            // 
            // txtTienDot3
            // 
            this.txtTienDot3.EnterMoveNextControl = true;
            this.txtTienDot3.Location = new System.Drawing.Point(651, 67);
            this.txtTienDot3.Name = "txtTienDot3";
            this.txtTienDot3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot3.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot3.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot3.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot3.Properties.Mask.EditMask = "N0";
            this.txtTienDot3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot3.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot3.TabIndex = 20;
            // 
            // dtpNgayDot3
            // 
            this.dtpNgayDot3.EditValue = null;
            this.dtpNgayDot3.EnterMoveNextControl = true;
            this.dtpNgayDot3.Location = new System.Drawing.Point(537, 67);
            this.dtpNgayDot3.Name = "dtpNgayDot3";
            this.dtpNgayDot3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot3.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot3.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot3.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot3.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot3.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(463, 70);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 19);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "TT đợt 3";
            // 
            // txtTienDot2
            // 
            this.txtTienDot2.EnterMoveNextControl = true;
            this.txtTienDot2.Location = new System.Drawing.Point(651, 36);
            this.txtTienDot2.Name = "txtTienDot2";
            this.txtTienDot2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot2.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot2.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot2.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot2.Properties.Mask.EditMask = "N0";
            this.txtTienDot2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot2.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot2.TabIndex = 16;
            // 
            // dtpNgayDot2
            // 
            this.dtpNgayDot2.EditValue = null;
            this.dtpNgayDot2.EnterMoveNextControl = true;
            this.dtpNgayDot2.Location = new System.Drawing.Point(537, 36);
            this.dtpNgayDot2.Name = "dtpNgayDot2";
            this.dtpNgayDot2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot2.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot2.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot2.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot2.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot2.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(463, 39);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 19);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "TT đợt 2";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.EnterMoveNextControl = true;
            this.txtGhiChu.Location = new System.Drawing.Point(105, 129);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu.Size = new System.Drawing.Size(351, 26);
            this.txtGhiChu.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 132);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 19);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Ghi chú";
            // 
            // txtTienDot1
            // 
            this.txtTienDot1.EnterMoveNextControl = true;
            this.txtTienDot1.Location = new System.Drawing.Point(651, 5);
            this.txtTienDot1.Name = "txtTienDot1";
            this.txtTienDot1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienDot1.Properties.Appearance.Options.UseFont = true;
            this.txtTienDot1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienDot1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienDot1.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienDot1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot1.Properties.EditFormat.FormatString = "N0";
            this.txtTienDot1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtTienDot1.Properties.Mask.EditMask = "N0";
            this.txtTienDot1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienDot1.Size = new System.Drawing.Size(134, 26);
            this.txtTienDot1.TabIndex = 12;
            // 
            // dtpNgayDot1
            // 
            this.dtpNgayDot1.EditValue = null;
            this.dtpNgayDot1.EnterMoveNextControl = true;
            this.dtpNgayDot1.Location = new System.Drawing.Point(537, 5);
            this.dtpNgayDot1.Name = "dtpNgayDot1";
            this.dtpNgayDot1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot1.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayDot1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDot1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayDot1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayDot1.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayDot1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayDot1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayDot1.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayDot1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayDot1.Size = new System.Drawing.Size(108, 26);
            this.dtpNgayDot1.TabIndex = 11;
            // 
            // txtGiaTriHopDong
            // 
            this.txtGiaTriHopDong.EnterMoveNextControl = true;
            this.txtGiaTriHopDong.Location = new System.Drawing.Point(105, 67);
            this.txtGiaTriHopDong.Name = "txtGiaTriHopDong";
            this.txtGiaTriHopDong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTriHopDong.Properties.Appearance.Options.UseFont = true;
            this.txtGiaTriHopDong.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGiaTriHopDong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGiaTriHopDong.Properties.DisplayFormat.FormatString = "N0";
            this.txtGiaTriHopDong.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtGiaTriHopDong.Properties.EditFormat.FormatString = "N0";
            this.txtGiaTriHopDong.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtGiaTriHopDong.Properties.Mask.EditMask = "N0";
            this.txtGiaTriHopDong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGiaTriHopDong.Size = new System.Drawing.Size(351, 26);
            this.txtGiaTriHopDong.TabIndex = 5;
            // 
            // cboNhaCungCap
            // 
            this.cboNhaCungCap.EnterMoveNextControl = true;
            this.cboNhaCungCap.Location = new System.Drawing.Point(105, 36);
            this.cboNhaCungCap.Name = "cboNhaCungCap";
            this.cboNhaCungCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhaCungCap.Properties.Appearance.Options.UseFont = true;
            this.cboNhaCungCap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhaCungCap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhaCungCap.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhaCungCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNhaCungCap.Properties.DisplayMember = "TEN";
            this.cboNhaCungCap.Properties.DropDownItemHeight = 40;
            this.cboNhaCungCap.Properties.NullText = "";
            this.cboNhaCungCap.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhaCungCap.Properties.ShowHeader = false;
            this.cboNhaCungCap.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhaCungCap.Properties.ValueMember = "MA";
            this.cboNhaCungCap.Size = new System.Drawing.Size(351, 26);
            this.cboNhaCungCap.TabIndex = 3;
            // 
            // cboSoHopDong
            // 
            this.cboSoHopDong.EnterMoveNextControl = true;
            this.cboSoHopDong.Location = new System.Drawing.Point(105, 5);
            this.cboSoHopDong.Name = "cboSoHopDong";
            this.cboSoHopDong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSoHopDong.Properties.Appearance.Options.UseFont = true;
            this.cboSoHopDong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSoHopDong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboSoHopDong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboSoHopDong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboSoHopDong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboSoHopDong.Properties.DisplayMember = "TEN";
            this.cboSoHopDong.Properties.DropDownItemHeight = 40;
            this.cboSoHopDong.Properties.NullText = "";
            this.cboSoHopDong.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboSoHopDong.Properties.ShowHeader = false;
            this.cboSoHopDong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboSoHopDong.Properties.ValueMember = "MA";
            this.cboSoHopDong.Size = new System.Drawing.Size(351, 26);
            this.cboSoHopDong.TabIndex = 1;
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiDung.Location = new System.Drawing.Point(6, 101);
            this.lblNoiDung.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(65, 19);
            this.lblNoiDung.TabIndex = 6;
            this.lblNoiDung.Text = "Nội dung";
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(105, 98);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Size = new System.Drawing.Size(351, 26);
            this.txtNoiDung.TabIndex = 7;
            // 
            // lblSoVanBan
            // 
            this.lblSoVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanBan.Location = new System.Drawing.Point(463, 8);
            this.lblSoVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoVanBan.Name = "lblSoVanBan";
            this.lblSoVanBan.Size = new System.Drawing.Size(62, 19);
            this.lblSoVanBan.TabIndex = 10;
            this.lblSoVanBan.Text = "TT đợt 1";
            // 
            // lblLoaiVanBan
            // 
            this.lblLoaiVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiVanBan.Location = new System.Drawing.Point(6, 70);
            this.lblLoaiVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoaiVanBan.Name = "lblLoaiVanBan";
            this.lblLoaiVanBan.Size = new System.Drawing.Size(70, 19);
            this.lblLoaiVanBan.TabIndex = 4;
            this.lblLoaiVanBan.Text = "Giá trị HĐ";
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHopDong.Location = new System.Drawing.Point(6, 8);
            this.lblSoHopDong.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Size = new System.Drawing.Size(45, 19);
            this.lblSoHopDong.TabIndex = 0;
            this.lblSoHopDong.Text = "Sổ HĐ";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(105, 162);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 30;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(281, 162);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 32;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(369, 162);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 33;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(193, 162);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 31;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNhaCungCap
            // 
            this.lblNhaCungCap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhaCungCap.Location = new System.Drawing.Point(6, 39);
            this.lblNhaCungCap.Margin = new System.Windows.Forms.Padding(4);
            this.lblNhaCungCap.Name = "lblNhaCungCap";
            this.lblNhaCungCap.Size = new System.Drawing.Size(96, 19);
            this.lblNhaCungCap.TabIndex = 2;
            this.lblNhaCungCap.Text = "Nhà cung cấp";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 205);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1315, 446);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ID_SOHOPDONG,
            this.ID_NHACUNGCAP,
            this.STT,
            this.SOHOPDONG,
            this.NHACUNGCAP,
            this.NOIDUNG,
            this.GIATRI,
            this.NGAY_DOT1,
            this.TIEN_DOT1,
            this.TT_DOT1,
            this.NGAY_DOT2,
            this.TIEN_DOT2,
            this.TT_DOT2,
            this.NGAY_DOT3,
            this.TIEN_DOT3,
            this.TT_DOT3,
            this.NGAY_DOT4,
            this.TIEN_DOT4,
            this.TT_DOT4,
            this.NGAY_DOT5,
            this.TIEN_DOT5,
            this.TT_DOT5,
            this.TT_DOT6,
            this.TT_DOT7,
            this.TT_DOT8,
            this.NGAY_DOT6,
            this.TIEN_DOT6,
            this.NGAY_DOT7,
            this.TIEN_DOT7,
            this.NGAY_DOT8,
            this.TIEN_DOT8,
            this.CONLAI,
            this.GHICHU,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // ID_SOHOPDONG
            // 
            this.ID_SOHOPDONG.FieldName = "ID_SOHOPDONG";
            this.ID_SOHOPDONG.Name = "ID_SOHOPDONG";
            this.ID_SOHOPDONG.OptionsColumn.AllowEdit = false;
            this.ID_SOHOPDONG.OptionsColumn.AllowFocus = false;
            this.ID_SOHOPDONG.OptionsColumn.AllowMove = false;
            // 
            // ID_NHACUNGCAP
            // 
            this.ID_NHACUNGCAP.FieldName = "ID_NHACUNGCAP";
            this.ID_NHACUNGCAP.Name = "ID_NHACUNGCAP";
            this.ID_NHACUNGCAP.OptionsColumn.AllowEdit = false;
            this.ID_NHACUNGCAP.OptionsColumn.AllowFocus = false;
            this.ID_NHACUNGCAP.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // SOHOPDONG
            // 
            this.SOHOPDONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOHOPDONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOHOPDONG.AppearanceHeader.Options.UseFont = true;
            this.SOHOPDONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOHOPDONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOHOPDONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOHOPDONG.Caption = "Sổ hợp đồng";
            this.SOHOPDONG.FieldName = "SOHOPDONG";
            this.SOHOPDONG.Name = "SOHOPDONG";
            this.SOHOPDONG.OptionsColumn.AllowEdit = false;
            this.SOHOPDONG.OptionsColumn.AllowFocus = false;
            this.SOHOPDONG.OptionsColumn.FixedWidth = true;
            this.SOHOPDONG.Visible = true;
            this.SOHOPDONG.VisibleIndex = 1;
            this.SOHOPDONG.Width = 200;
            // 
            // NHACUNGCAP
            // 
            this.NHACUNGCAP.AppearanceCell.Options.UseTextOptions = true;
            this.NHACUNGCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHACUNGCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHACUNGCAP.AppearanceHeader.Options.UseFont = true;
            this.NHACUNGCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.NHACUNGCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHACUNGCAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHACUNGCAP.Caption = "Đơn vị cung cấp";
            this.NHACUNGCAP.FieldName = "NHACUNGCAP";
            this.NHACUNGCAP.Name = "NHACUNGCAP";
            this.NHACUNGCAP.OptionsColumn.AllowEdit = false;
            this.NHACUNGCAP.OptionsColumn.AllowFocus = false;
            this.NHACUNGCAP.OptionsColumn.FixedWidth = true;
            this.NHACUNGCAP.Visible = true;
            this.NHACUNGCAP.VisibleIndex = 2;
            this.NHACUNGCAP.Width = 200;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 3;
            this.NOIDUNG.Width = 250;
            // 
            // GIATRI
            // 
            this.GIATRI.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI.AppearanceHeader.Options.UseFont = true;
            this.GIATRI.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI.Caption = "Giá trị HĐ";
            this.GIATRI.DisplayFormat.FormatString = "N0";
            this.GIATRI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI.FieldName = "GIATRI";
            this.GIATRI.Name = "GIATRI";
            this.GIATRI.OptionsColumn.AllowEdit = false;
            this.GIATRI.OptionsColumn.AllowFocus = false;
            this.GIATRI.OptionsColumn.FixedWidth = true;
            this.GIATRI.Visible = true;
            this.GIATRI.VisibleIndex = 4;
            this.GIATRI.Width = 150;
            // 
            // NGAY_DOT1
            // 
            this.NGAY_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT1.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT1.Caption = "Ngày TT đợt 1";
            this.NGAY_DOT1.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT1.FieldName = "NGAY_DOT1";
            this.NGAY_DOT1.Name = "NGAY_DOT1";
            this.NGAY_DOT1.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT1.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT1.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT1.Visible = true;
            this.NGAY_DOT1.VisibleIndex = 5;
            this.NGAY_DOT1.Width = 150;
            // 
            // TIEN_DOT1
            // 
            this.TIEN_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT1.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT1.Caption = "Tiền TT đợt 1";
            this.TIEN_DOT1.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT1.FieldName = "TIEN_DOT1";
            this.TIEN_DOT1.Name = "TIEN_DOT1";
            this.TIEN_DOT1.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT1.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT1.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT1.Visible = true;
            this.TIEN_DOT1.VisibleIndex = 6;
            this.TIEN_DOT1.Width = 150;
            // 
            // TT_DOT1
            // 
            this.TT_DOT1.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT1.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT1.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT1.Caption = "Đã TT đợt 1";
            this.TT_DOT1.FieldName = "TT_DOT1";
            this.TT_DOT1.Name = "TT_DOT1";
            this.TT_DOT1.OptionsColumn.AllowEdit = false;
            this.TT_DOT1.OptionsColumn.AllowFocus = false;
            this.TT_DOT1.OptionsColumn.FixedWidth = true;
            this.TT_DOT1.Visible = true;
            this.TT_DOT1.VisibleIndex = 7;
            this.TT_DOT1.Width = 150;
            // 
            // NGAY_DOT2
            // 
            this.NGAY_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT2.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT2.Caption = "Ngày TT đợt 2";
            this.NGAY_DOT2.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT2.FieldName = "NGAY_DOT2";
            this.NGAY_DOT2.Name = "NGAY_DOT2";
            this.NGAY_DOT2.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT2.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT2.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT2.Visible = true;
            this.NGAY_DOT2.VisibleIndex = 8;
            this.NGAY_DOT2.Width = 150;
            // 
            // TIEN_DOT2
            // 
            this.TIEN_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT2.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT2.Caption = "Tiền TT đợt 2";
            this.TIEN_DOT2.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT2.FieldName = "TIEN_DOT2";
            this.TIEN_DOT2.Name = "TIEN_DOT2";
            this.TIEN_DOT2.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT2.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT2.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT2.Visible = true;
            this.TIEN_DOT2.VisibleIndex = 9;
            this.TIEN_DOT2.Width = 150;
            // 
            // TT_DOT2
            // 
            this.TT_DOT2.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT2.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT2.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT2.Caption = "Đã TT đợt 2";
            this.TT_DOT2.FieldName = "TT_DOT2";
            this.TT_DOT2.Name = "TT_DOT2";
            this.TT_DOT2.OptionsColumn.AllowEdit = false;
            this.TT_DOT2.OptionsColumn.AllowFocus = false;
            this.TT_DOT2.OptionsColumn.FixedWidth = true;
            this.TT_DOT2.Visible = true;
            this.TT_DOT2.VisibleIndex = 10;
            this.TT_DOT2.Width = 150;
            // 
            // NGAY_DOT3
            // 
            this.NGAY_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT3.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT3.Caption = "Ngày TT đợt 3";
            this.NGAY_DOT3.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT3.FieldName = "NGAY_DOT3";
            this.NGAY_DOT3.Name = "NGAY_DOT3";
            this.NGAY_DOT3.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT3.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT3.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT3.Visible = true;
            this.NGAY_DOT3.VisibleIndex = 11;
            this.NGAY_DOT3.Width = 150;
            // 
            // TIEN_DOT3
            // 
            this.TIEN_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT3.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT3.Caption = "Tiền TT đợt 3";
            this.TIEN_DOT3.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT3.FieldName = "TIEN_DOT3";
            this.TIEN_DOT3.Name = "TIEN_DOT3";
            this.TIEN_DOT3.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT3.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT3.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT3.Visible = true;
            this.TIEN_DOT3.VisibleIndex = 12;
            this.TIEN_DOT3.Width = 150;
            // 
            // TT_DOT3
            // 
            this.TT_DOT3.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT3.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT3.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT3.Caption = "Đã TT đợt 3";
            this.TT_DOT3.FieldName = "TT_DOT3";
            this.TT_DOT3.Name = "TT_DOT3";
            this.TT_DOT3.OptionsColumn.AllowEdit = false;
            this.TT_DOT3.OptionsColumn.AllowFocus = false;
            this.TT_DOT3.OptionsColumn.FixedWidth = true;
            this.TT_DOT3.Visible = true;
            this.TT_DOT3.VisibleIndex = 13;
            this.TT_DOT3.Width = 150;
            // 
            // NGAY_DOT4
            // 
            this.NGAY_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT4.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT4.Caption = "Ngày TT đợt 4";
            this.NGAY_DOT4.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT4.FieldName = "NGAY_DOT4";
            this.NGAY_DOT4.Name = "NGAY_DOT4";
            this.NGAY_DOT4.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT4.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT4.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT4.Visible = true;
            this.NGAY_DOT4.VisibleIndex = 14;
            this.NGAY_DOT4.Width = 150;
            // 
            // TIEN_DOT4
            // 
            this.TIEN_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT4.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT4.Caption = "Tiền TT đợt 4";
            this.TIEN_DOT4.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT4.FieldName = "TIEN_DOT4";
            this.TIEN_DOT4.Name = "TIEN_DOT4";
            this.TIEN_DOT4.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT4.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT4.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT4.Visible = true;
            this.TIEN_DOT4.VisibleIndex = 15;
            this.TIEN_DOT4.Width = 150;
            // 
            // TT_DOT4
            // 
            this.TT_DOT4.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT4.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT4.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT4.Caption = "Đã TT đợt 4";
            this.TT_DOT4.FieldName = "TT_DOT4";
            this.TT_DOT4.Name = "TT_DOT4";
            this.TT_DOT4.OptionsColumn.AllowEdit = false;
            this.TT_DOT4.OptionsColumn.AllowFocus = false;
            this.TT_DOT4.OptionsColumn.FixedWidth = true;
            this.TT_DOT4.Visible = true;
            this.TT_DOT4.VisibleIndex = 16;
            this.TT_DOT4.Width = 150;
            // 
            // NGAY_DOT5
            // 
            this.NGAY_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT5.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT5.Caption = "Ngày TT đợt 5";
            this.NGAY_DOT5.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT5.FieldName = "NGAY_DOT5";
            this.NGAY_DOT5.Name = "NGAY_DOT5";
            this.NGAY_DOT5.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT5.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT5.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT5.Visible = true;
            this.NGAY_DOT5.VisibleIndex = 17;
            this.NGAY_DOT5.Width = 150;
            // 
            // TIEN_DOT5
            // 
            this.TIEN_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT5.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT5.Caption = "Tiền TT đợt 5";
            this.TIEN_DOT5.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT5.FieldName = "TIEN_DOT5";
            this.TIEN_DOT5.Name = "TIEN_DOT5";
            this.TIEN_DOT5.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT5.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT5.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT5.Visible = true;
            this.TIEN_DOT5.VisibleIndex = 18;
            this.TIEN_DOT5.Width = 150;
            // 
            // TT_DOT5
            // 
            this.TT_DOT5.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT5.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT5.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT5.Caption = "Đã TT đợt 5";
            this.TT_DOT5.FieldName = "TT_DOT5";
            this.TT_DOT5.Name = "TT_DOT5";
            this.TT_DOT5.OptionsColumn.AllowEdit = false;
            this.TT_DOT5.OptionsColumn.AllowFocus = false;
            this.TT_DOT5.OptionsColumn.FixedWidth = true;
            this.TT_DOT5.Visible = true;
            this.TT_DOT5.VisibleIndex = 19;
            this.TT_DOT5.Width = 150;
            // 
            // TT_DOT6
            // 
            this.TT_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT6.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT6.Caption = "Đã TT đợt 6";
            this.TT_DOT6.FieldName = "TT_DOT6";
            this.TT_DOT6.Name = "TT_DOT6";
            this.TT_DOT6.OptionsColumn.AllowEdit = false;
            this.TT_DOT6.OptionsColumn.AllowFocus = false;
            this.TT_DOT6.OptionsColumn.FixedWidth = true;
            this.TT_DOT6.Visible = true;
            this.TT_DOT6.VisibleIndex = 22;
            this.TT_DOT6.Width = 129;
            // 
            // TT_DOT7
            // 
            this.TT_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT7.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT7.Caption = "Đã TT đợt 7";
            this.TT_DOT7.FieldName = "TT_DOT7";
            this.TT_DOT7.Name = "TT_DOT7";
            this.TT_DOT7.OptionsColumn.AllowEdit = false;
            this.TT_DOT7.OptionsColumn.AllowFocus = false;
            this.TT_DOT7.OptionsColumn.FixedWidth = true;
            this.TT_DOT7.Visible = true;
            this.TT_DOT7.VisibleIndex = 25;
            this.TT_DOT7.Width = 132;
            // 
            // TT_DOT8
            // 
            this.TT_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.TT_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TT_DOT8.AppearanceHeader.Options.UseFont = true;
            this.TT_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.TT_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TT_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TT_DOT8.Caption = "Đã TT đợt 8";
            this.TT_DOT8.FieldName = "TT_DOT8";
            this.TT_DOT8.Name = "TT_DOT8";
            this.TT_DOT8.OptionsColumn.AllowEdit = false;
            this.TT_DOT8.OptionsColumn.AllowFocus = false;
            this.TT_DOT8.OptionsColumn.FixedWidth = true;
            this.TT_DOT8.Visible = true;
            this.TT_DOT8.VisibleIndex = 28;
            this.TT_DOT8.Width = 143;
            // 
            // NGAY_DOT6
            // 
            this.NGAY_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT6.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT6.Caption = "Ngày TT đợt 6";
            this.NGAY_DOT6.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT6.FieldName = "NGAY_DOT6";
            this.NGAY_DOT6.Name = "NGAY_DOT6";
            this.NGAY_DOT6.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT6.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT6.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT6.Visible = true;
            this.NGAY_DOT6.VisibleIndex = 20;
            this.NGAY_DOT6.Width = 147;
            // 
            // TIEN_DOT6
            // 
            this.TIEN_DOT6.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT6.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT6.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT6.Caption = "Tiền TT đợt 6";
            this.TIEN_DOT6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT6.FieldName = "TIEN_DOT6";
            this.TIEN_DOT6.Name = "TIEN_DOT6";
            this.TIEN_DOT6.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT6.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT6.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT6.Visible = true;
            this.TIEN_DOT6.VisibleIndex = 21;
            this.TIEN_DOT6.Width = 153;
            // 
            // NGAY_DOT7
            // 
            this.NGAY_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT7.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT7.Caption = "Ngày TT đợt 7";
            this.NGAY_DOT7.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT7.FieldName = "NGAY_DOT7";
            this.NGAY_DOT7.Name = "NGAY_DOT7";
            this.NGAY_DOT7.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT7.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT7.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT7.Visible = true;
            this.NGAY_DOT7.VisibleIndex = 23;
            this.NGAY_DOT7.Width = 136;
            // 
            // TIEN_DOT7
            // 
            this.TIEN_DOT7.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT7.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT7.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT7.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT7.Caption = "Tiền TT đợt 7";
            this.TIEN_DOT7.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT7.FieldName = "TIEN_DOT7";
            this.TIEN_DOT7.Name = "TIEN_DOT7";
            this.TIEN_DOT7.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT7.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT7.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT7.Visible = true;
            this.TIEN_DOT7.VisibleIndex = 24;
            this.TIEN_DOT7.Width = 132;
            // 
            // NGAY_DOT8
            // 
            this.NGAY_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_DOT8.AppearanceHeader.Options.UseFont = true;
            this.NGAY_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_DOT8.Caption = "Ngày TT đợt 8";
            this.NGAY_DOT8.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_DOT8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_DOT8.FieldName = "NGAY_DOT8";
            this.NGAY_DOT8.Name = "NGAY_DOT8";
            this.NGAY_DOT8.OptionsColumn.AllowEdit = false;
            this.NGAY_DOT8.OptionsColumn.AllowFocus = false;
            this.NGAY_DOT8.OptionsColumn.FixedWidth = true;
            this.NGAY_DOT8.Visible = true;
            this.NGAY_DOT8.VisibleIndex = 26;
            this.NGAY_DOT8.Width = 148;
            // 
            // TIEN_DOT8
            // 
            this.TIEN_DOT8.AppearanceCell.Options.UseTextOptions = true;
            this.TIEN_DOT8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIEN_DOT8.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEN_DOT8.AppearanceHeader.Options.UseFont = true;
            this.TIEN_DOT8.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEN_DOT8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEN_DOT8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEN_DOT8.Caption = "Tiền TT đợt 8";
            this.TIEN_DOT8.DisplayFormat.FormatString = "N0";
            this.TIEN_DOT8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIEN_DOT8.FieldName = "TIEN_DOT8";
            this.TIEN_DOT8.Name = "TIEN_DOT8";
            this.TIEN_DOT8.OptionsColumn.AllowEdit = false;
            this.TIEN_DOT8.OptionsColumn.AllowFocus = false;
            this.TIEN_DOT8.OptionsColumn.FixedWidth = true;
            this.TIEN_DOT8.Visible = true;
            this.TIEN_DOT8.VisibleIndex = 27;
            this.TIEN_DOT8.Width = 139;
            // 
            // CONLAI
            // 
            this.CONLAI.AppearanceCell.Options.UseTextOptions = true;
            this.CONLAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CONLAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CONLAI.AppearanceHeader.Options.UseFont = true;
            this.CONLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.CONLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CONLAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.Caption = "Còn lại";
            this.CONLAI.DisplayFormat.FormatString = "N0";
            this.CONLAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CONLAI.FieldName = "CONLAI";
            this.CONLAI.Name = "CONLAI";
            this.CONLAI.OptionsColumn.AllowEdit = false;
            this.CONLAI.OptionsColumn.AllowFocus = false;
            this.CONLAI.OptionsColumn.FixedWidth = true;
            this.CONLAI.Visible = true;
            this.CONLAI.VisibleIndex = 29;
            this.CONLAI.Width = 150;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 30;
            this.GHICHU.Width = 147;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 31;
            this.FILL.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1315, 205);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 446);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 282);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 122);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 202);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 364);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 21;
            // 
            // Frm_SoTheoDoiHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_SoTheoDoiHD";
            this.Text = "Theo dõi hợp đồng";
            this.Load += new System.EventHandler(this.Frm_SoTheoDoiHD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot8.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot7.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot6.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDot1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot5.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienDot1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDot1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTriHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSoHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn SOHOPDONG;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT2;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.LabelControl lblNhaCungCap;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT2;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT1;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn NHACUNGCAP;
        private DevExpress.XtraEditors.LabelControl lblSoHopDong;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lblLoaiVanBan;
        private DevExpress.XtraEditors.LabelControl lblSoVanBan;
        private DevExpress.XtraEditors.LabelControl lblNoiDung;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraGrid.Columns.GridColumn ID_SOHOPDONG;
        private DevExpress.XtraGrid.Columns.GridColumn ID_NHACUNGCAP;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT3;
        private DevExpress.XtraGrid.Columns.GridColumn CONLAI;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.LookUpEdit cboSoHopDong;
        private DevExpress.XtraEditors.LookUpEdit cboNhaCungCap;
        private DevExpress.XtraEditors.TextEdit txtGiaTriHopDong;
        private DevExpress.XtraEditors.TextEdit txtTienDot5;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot5;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtTienDot4;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtTienDot3;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtTienDot2;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtGhiChu;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtTienDot1;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot1;
        private DevExpress.XtraEditors.CheckEdit chkTTDot5;
        private DevExpress.XtraEditors.CheckEdit chkTTDot4;
        private DevExpress.XtraEditors.CheckEdit chkTTDot3;
        private DevExpress.XtraEditors.CheckEdit chkTTDot2;
        private DevExpress.XtraEditors.CheckEdit chkTTDot1;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT4;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT5;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT5;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT5;
        private DevExpress.XtraEditors.CheckEdit chkTTDot8;
        private DevExpress.XtraEditors.CheckEdit chkTTDot7;
        private DevExpress.XtraEditors.CheckEdit chkTTDot6;
        private DevExpress.XtraEditors.TextEdit txtTienDot8;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTienDot7;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtTienDot6;
        private DevExpress.XtraEditors.DateEdit dtpNgayDot6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn TIEN_DOT8;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_DOT8;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT6;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT7;
        private DevExpress.XtraGrid.Columns.GridColumn TT_DOT8;




    }
}