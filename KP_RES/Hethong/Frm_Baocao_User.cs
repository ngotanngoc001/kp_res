﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_User : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_User()
        {
            InitializeComponent();

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Hanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itbaocaotaikhoan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itbaocaotaikhoan.Checked)
            {
                this.Parent.GoTo<Frm_BC_User>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itbaocaotaikhoan);
        }

        private void LoadPermission()
        {
            itbaocaotaikhoan.Checked = clsUserManagement.AllowView("11051502");
        }
    }
}