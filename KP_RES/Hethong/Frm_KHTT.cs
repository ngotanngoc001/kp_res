﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Diagnostics;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_KHTT : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_KHTT()
        {
            InitializeComponent();
            this.Text = rm.GetString("khtt-tnb", culture);
            tileControl1.Text = rm.GetString("khtt-tnb", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_KHTT_Paint(object sender, PaintEventArgs e)
        {
            

            LoadPermission();
        }

        private void itBanverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanverapphim.Checked)
            {
                this.Parent.GoTo<Frm_Khachhang>();
            }
        }

        private void itDatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDatverapphim.Checked)
            {
                this.Parent.GoTo<Frm_DoiDiemThuong>();
                //Frm_DoiDiemThuong frm = new Frm_DoiDiemThuong();
                //frm.ShowDialog();
                //frm.Dispose();
            }
        }

        private void itSoatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoatverapphim.Checked)
            {

                if (File.Exists(Application.StartupPath + "\\KP_KHTT.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_KHTT"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\KP_KHTT.exe");
                }
                else if (File.Exists(Application.StartupPath + "\\CoopMart.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("CoopMart"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\CoopMart.exe");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBanverapphim);
            cls_KP_RES.RemoveTileItem(itDatverapphim);
            cls_KP_RES.RemoveTileItem(itSoatverapphim);
        }

        private void LoadPermission()
        {
            itBanverapphim.Checked = (clsUserManagement.AllowView("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowView("58") && clsUserManagement.CheckActive("8"));
            itDatverapphim.Checked = clsUserManagement.AllowView("52") && clsUserManagement.CheckActive("4");
            itSoatverapphim.Checked = clsUserManagement.AllowView("107") && clsUserManagement.CheckActive("4");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            
            itBanverapphim.Text = rm.GetString("themmoikh",culture);
            itDatverapphim.Text = rm.GetString("phatthuong",culture);
            itSoatverapphim.Text = rm.GetString("tracuukhtt",culture);
        }
    }
}