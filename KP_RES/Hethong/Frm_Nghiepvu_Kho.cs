﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Nghiepvu_Kho : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Nghiepvu_Kho()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Nghiepvu_Kho_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itNhapKho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapKho.Checked)
            {
                this.Parent.GoTo<Frm_Nhapkho>();
            }
        }

        private void itXuatKho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itXuatKho.Checked)
            {
                this.Parent.GoTo<Frm_Xuatkho>();
            }
        }

        private void itKiemKe_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKiemKe.Checked)
            {
                this.Parent.GoTo<Frm_Phieukiemke>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itNhapKho);
            cls_KP_RES.RemoveTileItem(itXuatKho);
            cls_KP_RES.RemoveTileItem(itKiemKe);
        }

        private void LoadPermission()
        {
            itNhapKho.Checked = true;
            itXuatKho.Checked = true;
            itKiemKe.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       

        
    }
}