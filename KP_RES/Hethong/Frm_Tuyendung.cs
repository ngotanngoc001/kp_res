﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Tuyendung : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Tuyendung()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Tuyendung_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itDottuyendung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDottuyendung.Checked)
            {
                this.Parent.GoTo<Frm_DotTuyenDung>();
            }
        }

        private void itUngvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itUngvien.Checked)
            {
                this.Parent.GoTo<Frm_UngVien>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itDottuyendung);
            cls_KP_RES.RemoveTileItem(itUngvien);
        }

        private void LoadPermission()
        {
            itDottuyendung.Checked = clsUserManagement.AllowView("5051601");
            itUngvien.Checked = clsUserManagement.AllowView("5051602");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       
       

       

        

        

        

        

    }
}