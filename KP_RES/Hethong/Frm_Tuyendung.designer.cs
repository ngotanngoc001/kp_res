﻿namespace KP_RES 
{
    partial class Frm_Tuyendung 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itDottuyendung = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itUngvien = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 54;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itDottuyendung);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itDottuyendung
            // 
            this.itDottuyendung.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itDottuyendung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Đợt tuyển dụng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDottuyendung.Elements.Add(tileItemElement1);
            this.itDottuyendung.Id = 15;
            this.itDottuyendung.IsLarge = true;
            this.itDottuyendung.Name = "itDottuyendung";
            this.itDottuyendung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDottuyendung_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itUngvien);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itUngvien
            // 
            this.itUngvien.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itUngvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Ứng viên";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itUngvien.Elements.Add(tileItemElement2);
            this.itUngvien.Id = 53;
            this.itUngvien.IsLarge = true;
            this.itUngvien.Name = "itUngvien";
            this.itUngvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itUngvien_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_Tuyendung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Tuyendung";
            this.Text = "Tuyển dụng";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.PanelControl panelControl1;

        private DevExpress.XtraEditors.TileItem itDottuyendung;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itUngvien;
    }
}