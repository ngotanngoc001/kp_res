﻿namespace KP_RES 
{
    partial class Frm_Baocao_Nhahang1 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itHanghoa = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itNhapxuatton = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaobanhang = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 50;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itHanghoa);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Tồn kho";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement4);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Tồn kho";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement5);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itHanghoa
            // 
            this.itHanghoa.BackgroundImage = global::KP_RES.Properties.Resources.inmavach;
            this.itHanghoa.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Hàng hóa";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itHanghoa.Elements.Add(tileItemElement1);
            this.itHanghoa.Id = 47;
            this.itHanghoa.IsLarge = true;
            this.itHanghoa.Name = "itHanghoa";
            this.itHanghoa.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHanghoa_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itNhapxuatton);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itNhapxuatton
            // 
            this.itNhapxuatton.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itNhapxuatton.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Nhập - Xuất - Tồn";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhapxuatton.Elements.Add(tileItemElement2);
            this.itNhapxuatton.Id = 49;
            this.itNhapxuatton.IsLarge = true;
            this.itNhapxuatton.Name = "itNhapxuatton";
            this.itNhapxuatton.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhapxuatton_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itBaocaobanhang);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itBaocaobanhang
            // 
            this.itBaocaobanhang.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itBaocaobanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Báo cáo bán hàng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaobanhang.Elements.Add(tileItemElement3);
            this.itBaocaobanhang.Id = 48;
            this.itBaocaobanhang.IsLarge = true;
            this.itBaocaobanhang.Name = "itBaocaobanhang";
            this.itBaocaobanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaobanhang_ItemClick);
            // 
            // Frm_Baocao_Nhahang1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Nhahang1";
            this.Text = "Báo cáo";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Hanghoa_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itHanghoa;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itNhapxuatton;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itBaocaobanhang;
        private DevExpress.XtraEditors.TileGroup tileGroup2;



    }
}