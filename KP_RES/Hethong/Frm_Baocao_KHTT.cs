﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Resources;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_Baocao_KHTT : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Baocao_KHTT()
        {
            InitializeComponent();
            this.Text = rm.GetString("khtt-tnb", culture);
            tileControl1.Text = rm.GetString("khtt-tnb", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_KHTT_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT.Checked)
            {
                //this.Parent.GoTo<NS_DANHSACH_KHTT>();
            }
        }

        private void itDSKHCT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDSKHCT.Checked)
            {
                this.Parent.GoTo<Frm_BC_DS_DoiQua>();
            }
        }

        private void itThethanhtoannoibo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThethanhtoannoibo.Checked)
            {
                this.Parent.GoTo<NS_DANHSACH_KHTTNB>();
            }
        }

        private void itBC_KHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBC_KHTT.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_DS_KHTT>();
            }
        }

        private void itBC_TTNB_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBC_TTNB.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_DS_TTTNB>();
            }
        }

        private void itLichsu_Giaodich_KTTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLichsu_Giaodich_KTTT.Checked)
            {
                this.Parent.GoTo<Frm_BC_LS_GiaoDich>();
            }
        }

        private void itLichsudoima_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLichsudoima.Checked)
            {
                this.Parent.GoTo<Frm_BC_LichSuDoiMa_KHTT>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKHTT);
            cls_KP_RES.RemoveTileItem(itDSKHCT);
            cls_KP_RES.RemoveTileItem(itThethanhtoannoibo);
            cls_KP_RES.RemoveTileItem(itBC_KHTT);
            cls_KP_RES.RemoveTileItem(itBC_TTNB);
            cls_KP_RES.RemoveTileItem(itLichsu_Giaodich_KTTT);
            cls_KP_RES.RemoveTileItem(itLichsudoima);
        }

        private void LoadPermission()
        {
            itBC_KHTT.Checked = clsUserManagement.AllowView("54") && clsUserManagement.CheckActive("4");
            itBC_TTNB.Checked = clsUserManagement.AllowView("59") && clsUserManagement.CheckActive("8");
            itDSKHCT.Checked = clsUserManagement.AllowView("54") && clsUserManagement.CheckActive("4");
            itLichsu_Giaodich_KTTT.Checked = clsUserManagement.AllowView("54") && clsUserManagement.CheckActive("4");
            itLichsudoima.Checked = clsUserManagement.AllowView("54") && clsUserManagement.CheckActive("4");
            itKHTT.Checked = false ;
            itThethanhtoannoibo.Checked = false;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            itDSKHCT.Text = rm.GetString("lichsudoithuong",culture);
            itBC_KHTT.Text = rm.GetString("khtt",culture);
            itLichsu_Giaodich_KTTT.Text = rm.GetString("lichsugdkhtt",culture);
            itLichsudoima.Text = rm.GetString("lichsudoima",culture);
            itBC_TTNB.Text = rm.GetString("thettnb", culture);
        }

     
    
    }
}