﻿namespace KP_RES 
{
    partial class Frm_Baocao_Banhang_Karaoke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoDSHD = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoDSHD_INLAI = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoBanhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itDoanhthugio = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoKQKD = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoDoanhso = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itThongkedoanhso = new DevExpress.XtraEditors.TileItem();
            this.itThongkebanhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itHuyban = new DevExpress.XtraEditors.TileItem();
            this.itHuymon = new DevExpress.XtraEditors.TileItem();
            this.itLaylaihoadon = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup14);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 58;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 532;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itBaocaoDSHD);
            this.tileGroup7.Items.Add(this.itBaocaoDSHD_INLAI);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itBaocaoDSHD
            // 
            this.itBaocaoDSHD.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itBaocaoDSHD.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách hóa đơn";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD.Elements.Add(tileItemElement1);
            this.itBaocaoDSHD.Id = 21;
            this.itBaocaoDSHD.IsLarge = true;
            this.itBaocaoDSHD.Name = "itBaocaoDSHD";
            this.itBaocaoDSHD.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_ItemClick);
            // 
            // itBaocaoDSHD_INLAI
            // 
            this.itBaocaoDSHD_INLAI.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itBaocaoDSHD_INLAI.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Danh sách hóa đơn in lại";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD_INLAI.Elements.Add(tileItemElement2);
            this.itBaocaoDSHD_INLAI.Id = 50;
            this.itBaocaoDSHD_INLAI.IsLarge = true;
            this.itBaocaoDSHD_INLAI.Name = "itBaocaoDSHD_INLAI";
            this.itBaocaoDSHD_INLAI.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_INLAI_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itBaocaoBanhang);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itBaocaoBanhang
            // 
            this.itBaocaoBanhang.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itBaocaoBanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Báo cáo bán hàng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoBanhang.Elements.Add(tileItemElement3);
            this.itBaocaoBanhang.Id = 22;
            this.itBaocaoBanhang.IsLarge = true;
            this.itBaocaoBanhang.Name = "itBaocaoBanhang";
            this.itBaocaoBanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoBanhang_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itDoanhthugio);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itDoanhthugio
            // 
            this.itDoanhthugio.BackgroundImage = global::KP_RES.Properties.Resources.thoigian;
            this.itDoanhthugio.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Doanh thu tiền giờ";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itDoanhthugio.Elements.Add(tileItemElement4);
            this.itDoanhthugio.Id = 46;
            this.itDoanhthugio.IsLarge = true;
            this.itDoanhthugio.Name = "itDoanhthugio";
            this.itDoanhthugio.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDoanhthugio_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itBaocaoKQKD);
            this.tileGroup11.Items.Add(this.itBaocaoDoanhso);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itBaocaoKQKD
            // 
            this.itBaocaoKQKD.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itBaocaoKQKD.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Kết quả kinh doanh";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoKQKD.Elements.Add(tileItemElement5);
            this.itBaocaoKQKD.Id = 24;
            this.itBaocaoKQKD.IsLarge = true;
            this.itBaocaoKQKD.Name = "itBaocaoKQKD";
            this.itBaocaoKQKD.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoKQKD_ItemClick);
            // 
            // itBaocaoDoanhso
            // 
            this.itBaocaoDoanhso.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itBaocaoDoanhso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Báo cáo doanh số";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDoanhso.Elements.Add(tileItemElement6);
            this.itBaocaoDoanhso.Id = 23;
            this.itBaocaoDoanhso.IsLarge = true;
            this.itBaocaoDoanhso.Name = "itBaocaoDoanhso";
            this.itBaocaoDoanhso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDoanhso_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itThongkedoanhso);
            this.tileGroup12.Items.Add(this.itThongkebanhang);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itThongkedoanhso
            // 
            this.itThongkedoanhso.BackgroundImage = global::KP_RES.Properties.Resources.baocao1;
            this.itThongkedoanhso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Thống kê doanh số";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongkedoanhso.Elements.Add(tileItemElement7);
            this.itThongkedoanhso.Id = 44;
            this.itThongkedoanhso.IsLarge = true;
            this.itThongkedoanhso.Name = "itThongkedoanhso";
            this.itThongkedoanhso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongkedoanhso_ItemClick);
            // 
            // itThongkebanhang
            // 
            this.itThongkebanhang.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            this.itThongkebanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Thống kê hàng bán chạy";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongkebanhang.Elements.Add(tileItemElement8);
            this.itThongkebanhang.Id = 45;
            this.itThongkebanhang.IsLarge = true;
            this.itThongkebanhang.Name = "itThongkebanhang";
            this.itThongkebanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongkebanhang_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Items.Add(this.itHuyban);
            this.tileGroup14.Items.Add(this.itHuymon);
            this.tileGroup14.Items.Add(this.itLaylaihoadon);
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itHuyban
            // 
            this.itHuyban.BackgroundImage = global::KP_RES.Properties.Resources.saoluu;
            this.itHuyban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Hủy bàn";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuyban.Elements.Add(tileItemElement9);
            this.itHuyban.Id = 55;
            this.itHuyban.IsLarge = true;
            this.itHuyban.Name = "itHuyban";
            this.itHuyban.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuyban_ItemClick);
            // 
            // itHuymon
            // 
            this.itHuymon.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itHuymon.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Hủy món";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuymon.Elements.Add(tileItemElement10);
            this.itHuymon.Id = 56;
            this.itHuymon.IsLarge = true;
            this.itHuymon.Name = "itHuymon";
            this.itHuymon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuymon_ItemClick);
            // 
            // itLaylaihoadon
            // 
            this.itLaylaihoadon.BackgroundImage = global::KP_RES.Properties.Resources.saoluu1;
            this.itLaylaihoadon.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Lấy lại hóa đơn";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itLaylaihoadon.Elements.Add(tileItemElement11);
            this.itLaylaihoadon.Id = 57;
            this.itLaylaihoadon.IsLarge = true;
            this.itLaylaihoadon.Name = "itLaylaihoadon";
            this.itLaylaihoadon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLaylaihoadon_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Tồn kho";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement12);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Tồn kho";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement13);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // Frm_Baocao_Banhang_Karaoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Banhang_Karaoke";
            this.Text = "Báo cáo bán hàng Karaoke";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Banhang_Karaoke_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itBaocaoBanhang;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itDoanhthugio;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itBaocaoKQKD;
        private DevExpress.XtraEditors.TileItem itBaocaoDoanhso;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itThongkedoanhso;
        private DevExpress.XtraEditors.TileItem itThongkebanhang;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD_INLAI;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itHuyban;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itHuymon;
        private DevExpress.XtraEditors.TileItem itLaylaihoadon;



    }
}