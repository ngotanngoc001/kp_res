﻿namespace KP_RES 
{
    partial class  Frm_Nhahang1 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Nhahang1));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itCaidat = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocao = new DevExpress.XtraEditors.TileItem();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itKho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itFastfood = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itManhinhbep = new DevExpress.XtraEditors.TileItem();
            this.itMenudisplay = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 60;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 30;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itCaidat);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itCaidat
            // 
            this.itCaidat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itCaidat.BackgroundImage")));
            this.itCaidat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Cài đặt";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidat.Elements.Add(tileItemElement1);
            this.itCaidat.Id = 54;
            this.itCaidat.IsLarge = true;
            this.itCaidat.Name = "itCaidat";
            this.itCaidat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidat_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itBaocao);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itBaocao
            // 
            this.itBaocao.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itBaocao.BackgroundImage")));
            this.itBaocao.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Báo cáo";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocao.Elements.Add(tileItemElement2);
            this.itBaocao.Id = 55;
            this.itBaocao.IsLarge = true;
            this.itBaocao.Name = "itBaocao";
            this.itBaocao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocao_ItemClick);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itKho);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itKho
            // 
            this.itKho.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho2;
            this.itKho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Kho-Nhập-Xuất";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itKho.Elements.Add(tileItemElement3);
            this.itKho.Id = 56;
            this.itKho.IsLarge = true;
            this.itKho.Name = "itKho";
            this.itKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKho_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itFastfood);
            this.tileGroup5.Items.Add(this.itManhinhbep);
            this.tileGroup5.Items.Add(this.itMenudisplay);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itFastfood
            // 
            this.itFastfood.BackgroundImage = global::KP_RES.Properties.Resources.banhang9;
            this.itFastfood.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Fastfood";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itFastfood.Elements.Add(tileItemElement4);
            this.itFastfood.Id = 57;
            this.itFastfood.IsLarge = true;
            this.itFastfood.Name = "itFastfood";
            this.itFastfood.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itFastfood_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // itManhinhbep
            // 
            this.itManhinhbep.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep;
            this.itManhinhbep.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Màn hình bếp";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itManhinhbep.Elements.Add(tileItemElement5);
            this.itManhinhbep.Id = 58;
            this.itManhinhbep.IsLarge = true;
            this.itManhinhbep.Name = "itManhinhbep";
            this.itManhinhbep.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itManhinhbep_ItemClick);
            // 
            // itMenudisplay
            // 
            this.itMenudisplay.BackgroundImage = global::KP_RES.Properties.Resources.loaighe;
            this.itMenudisplay.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Menu Display";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itMenudisplay.Elements.Add(tileItemElement6);
            this.itMenudisplay.Id = 59;
            this.itMenudisplay.IsLarge = true;
            this.itMenudisplay.Name = "itMenudisplay";
            this.itMenudisplay.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMenudisplay_ItemClick);
            // 
            // Frm_Nhahang1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Nhahang1";
            this.Text = "Nhà hàng";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itCaidat;
        private DevExpress.XtraEditors.TileItem itBaocao;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itKho;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itFastfood;
        private DevExpress.XtraEditors.TileItem itManhinhbep;
        private DevExpress.XtraEditors.TileItem itMenudisplay;
    }
}