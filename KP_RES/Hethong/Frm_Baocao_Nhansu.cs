﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Nhansu : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Nhansu()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void itUngvien_ItemClick(object sender, TileItemEventArgs e)
        {

            if (itUngvien.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_UngVien>();
            }
        }

        private void itNhanvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhanvien.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_NhanVien>();
            }
        }

        private void itHopdonglaodong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHopdonglaodong.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_HopDongLaoDong>();
            }
        }

        private void itKhenthuong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhenthuong.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_KhenThuongKyLuat>();
            }
        }

        private void itLuongthang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLuongthang.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_LuongThang>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itUngvien );
            cls_KP_RES.RemoveTileItem(itNhanvien);
            cls_KP_RES.RemoveTileItem(itHopdonglaodong);
            cls_KP_RES.RemoveTileItem(itKhenthuong);
            cls_KP_RES.RemoveTileItem(itLuongthang);
           
        }

        private void LoadPermission()
        {
            itUngvien.Checked = clsUserManagement.AllowView("21051601") && clsUserManagement.CheckActive("17"); ;
            itNhanvien.Checked = clsUserManagement.AllowView("21051602") && clsUserManagement.CheckActive("17"); ;
            itHopdonglaodong.Checked = clsUserManagement.AllowView("21051603") && clsUserManagement.CheckActive("17"); ;
            itKhenthuong.Checked = clsUserManagement.AllowView("21051604") && clsUserManagement.CheckActive("17"); ;
            itLuongthang.Checked = clsUserManagement.AllowView("21051605") && clsUserManagement.CheckActive("17"); ;
        }

      
       

       
        
      

        
    }
}