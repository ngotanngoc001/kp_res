﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Cauhinh3 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinh3()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Cauhinh3_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itMayin_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMayin.Checked)
            {
                clsKP_Terminal .ShowConfig_Printer ();
            }
        }

        private void itHienthigia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHienthigia.Checked)
            {
                clsKP_Terminal.ShowConfig_CustomerDisplay();
            }
        }

        private void itKettien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHienthigia.Checked)
            {
                clsKP_Terminal.ShowConfig_CashDrawer();
            }
        }

        

        private void itQuay_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuay.Checked)
            {
                Frm_Cauhinhquay frmch = new Frm_Cauhinhquay();
                frmch.ShowDialog();
                frmch.Dispose();
            }
        }

        

       


        private void itGiaodien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiaodien.Checked)
            {
                Frm_Giaodien frm = new Frm_Giaodien();
                frm.ShowDialog();
                frm.Dispose();
            }
        }


        private void itMatkhaubanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMatkhaubanhang.Checked)
            {
                Frm_XacNhan frm = new  Frm_XacNhan ();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itRap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itRap.Checked)
            {
                Frm_Cauhinhrap frm = new Frm_Cauhinhrap();
                frm.ShowDialog();
                frm.Dispose();
            }
        }


   

    
        private void itNgonngu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNgonngu.Checked)
            {
                Frm_Ngonngu frm = new Frm_Ngonngu();
                frm.ShowDialog();
                frm.Dispose();
            }
        }






        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itMayin);
            cls_KP_RES.RemoveTileItem(itHienthigia);
            cls_KP_RES.RemoveTileItem(itKettien);
          
            cls_KP_RES.RemoveTileItem(itQuay);
           
            
            cls_KP_RES.RemoveTileItem(itMatkhaubanhang);
            cls_KP_RES.RemoveTileItem(itGiaodien);
            cls_KP_RES.RemoveTileItem(itRap );
           
            cls_KP_RES.RemoveTileItem(itNgonngu);
           
        }

        private void LoadPermission()
        {
            itMayin.Checked = clsUserManagement.AllowView("34");
            itHienthigia.Checked = clsUserManagement.AllowView("34");
            itKettien.Checked = clsUserManagement.AllowView("34");
          
            itQuay.Checked = clsUserManagement.AllowView("36");
            
            itMatkhaubanhang.Checked = clsUserManagement.AllowView("51");
            itRap.Checked = clsUserManagement.AllowView("88");
          
            itGiaodien.Checked = true;
           
            itNgonngu.Checked = true;
          
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            itGiaodien.Text = rm.GetString("giaodien", culture);
        
         
            itMayin.Text = rm.GetString("mayin", culture);
            itHienthigia.Text = rm.GetString("hienthigia", culture);
            itKettien.Text = rm.GetString("kettien", culture);
            itMatkhaubanhang.Text = rm.GetString("matkhaubanhang", culture);
            itQuay.Text = rm.GetString("cauhinhquay", culture);
           
            itRap.Text = rm.GetString("cauhinhrap", culture);
            
            itNgonngu.Text  = rm.GetString("cauhinhngonngu", culture);
        }

       

    

        

       

    }
}