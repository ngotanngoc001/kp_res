﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Sotheodoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Sotheodoi()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void itBCSoTheoDoiHD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBCSoTheoDoiHD.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_SoTheoDoiHD>();
            }
        }

        private void itBCChiTheoDoiHD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBCChiTheoDoiHD.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_ChiTheoDoi>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBCSoTheoDoiHD);
            cls_KP_RES.RemoveTileItem(itBCChiTheoDoiHD);
        }

        private void LoadPermission()
        {
            itBCSoTheoDoiHD.Checked = clsUserManagement.AllowView("30081603");
            itBCChiTheoDoiHD.Checked = clsUserManagement.AllowView("30081603");
        }

        private void Frm_Baocao_Voucher_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

     
    }
}