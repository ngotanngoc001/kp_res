﻿namespace KP_RES 
{
    partial class Frm_Baocao_KHTT 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itKHTT = new DevExpress.XtraEditors.TileItem();
            this.itDSKHCT = new DevExpress.XtraEditors.TileItem();
            this.itThethanhtoannoibo = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBC_KHTT = new DevExpress.XtraEditors.TileItem();
            this.itBC_TTNB = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itLichsu_Giaodich_KTTT = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itLichsudoima = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 48;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itKHTT);
            this.tileGroup9.Items.Add(this.itDSKHCT);
            this.tileGroup9.Items.Add(this.itThethanhtoannoibo);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itKHTT
            // 
            this.itKHTT.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách khách hàng thân tiết";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT.Elements.Add(tileItemElement1);
            this.itKHTT.Id = 29;
            this.itKHTT.IsLarge = true;
            this.itKHTT.Name = "itKHTT";
            this.itKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_ItemClick);
            // 
            // itDSKHCT
            // 
            this.itDSKHCT.BackgroundImage = global::KP_RES.Properties.Resources.danhsachkhct;
            this.itDSKHCT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Lịch sử phát thưởng";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDSKHCT.Elements.Add(tileItemElement2);
            this.itDSKHCT.Id = 30;
            this.itDSKHCT.IsLarge = true;
            this.itDSKHCT.Name = "itDSKHCT";
            this.itDSKHCT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDSKHCT_ItemClick);
            // 
            // itThethanhtoannoibo
            // 
            this.itThethanhtoannoibo.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itThethanhtoannoibo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Danh sách thẻ thanh toán nội bộ";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itThethanhtoannoibo.Elements.Add(tileItemElement3);
            this.itThethanhtoannoibo.Id = 33;
            this.itThethanhtoannoibo.IsLarge = true;
            this.itThethanhtoannoibo.Name = "itThethanhtoannoibo";
            this.itThethanhtoannoibo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThethanhtoannoibo_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBC_KHTT);
            this.tileGroup2.Items.Add(this.itBC_TTNB);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBC_KHTT
            // 
            this.itBC_KHTT.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itBC_KHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Khách hàng thân thiết";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itBC_KHTT.Elements.Add(tileItemElement4);
            this.itBC_KHTT.Id = 44;
            this.itBC_KHTT.IsLarge = true;
            this.itBC_KHTT.Name = "itBC_KHTT";
            this.itBC_KHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBC_KHTT_ItemClick);
            // 
            // itBC_TTNB
            // 
            this.itBC_TTNB.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itBC_TTNB.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thẻ thanh toán nội bộ";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itBC_TTNB.Elements.Add(tileItemElement5);
            this.itBC_TTNB.Id = 45;
            this.itBC_TTNB.IsLarge = true;
            this.itBC_TTNB.Name = "itBC_TTNB";
            this.itBC_TTNB.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBC_TTNB_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itLichsu_Giaodich_KTTT);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itLichsu_Giaodich_KTTT
            // 
            this.itLichsu_Giaodich_KTTT.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itLichsu_Giaodich_KTTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Lịch sử giao dịch KHTT";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itLichsu_Giaodich_KTTT.Elements.Add(tileItemElement6);
            this.itLichsu_Giaodich_KTTT.Id = 46;
            this.itLichsu_Giaodich_KTTT.IsLarge = true;
            this.itLichsu_Giaodich_KTTT.Name = "itLichsu_Giaodich_KTTT";
            this.itLichsu_Giaodich_KTTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLichsu_Giaodich_KTTT_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itLichsudoima);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itLichsudoima
            // 
            this.itLichsudoima.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itLichsudoima.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itLichsudoima.Checked = true;
            tileItemElement7.Text = "Lịch sử đổi mã";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itLichsudoima.Elements.Add(tileItemElement7);
            this.itLichsudoima.Id = 47;
            this.itLichsudoima.IsLarge = true;
            this.itLichsudoima.Name = "itLichsudoima";
            this.itLichsudoima.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLichsudoima_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Tồn kho";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement8);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Tồn kho";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement9);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // Frm_Baocao_KHTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_KHTT";
            this.Text = "Khách hàng thân thiết - Thẻ nội bộ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_KHTT_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itKHTT;
        private DevExpress.XtraEditors.TileItem itDSKHCT;
        private DevExpress.XtraEditors.TileItem itThethanhtoannoibo;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBC_KHTT;
        private DevExpress.XtraEditors.TileItem itBC_TTNB;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itLichsu_Giaodich_KTTT;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itLichsudoima;



    }
}