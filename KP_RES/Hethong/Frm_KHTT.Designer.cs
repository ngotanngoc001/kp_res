﻿namespace KP_RES 
{
    partial class Frm_KHTT 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBanverapphim = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itDatverapphim = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itSoatverapphim = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 20;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(927, 371);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBanverapphim);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBanverapphim
            // 
            this.itBanverapphim.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itBanverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Thêm mới khách hàng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanverapphim.Elements.Add(tileItemElement1);
            this.itBanverapphim.Id = 15;
            this.itBanverapphim.IsLarge = true;
            this.itBanverapphim.Name = "itBanverapphim";
            this.itBanverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanverapphim_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itDatverapphim);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itDatverapphim
            // 
            this.itDatverapphim.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itDatverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Phát thưởng";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDatverapphim.Elements.Add(tileItemElement2);
            this.itDatverapphim.Id = 16;
            this.itDatverapphim.IsLarge = true;
            this.itDatverapphim.Name = "itDatverapphim";
            this.itDatverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDatverapphim_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itSoatverapphim);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itSoatverapphim
            // 
            this.itSoatverapphim.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itSoatverapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Tra cứu khách hàng thân thiết";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itSoatverapphim.Elements.Add(tileItemElement3);
            this.itSoatverapphim.Id = 19;
            this.itSoatverapphim.IsLarge = true;
            this.itSoatverapphim.Name = "itSoatverapphim";
            this.itSoatverapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSoatverapphim_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(931, 375);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Tồn kho";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement4);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // Frm_KHTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 375);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_KHTT";
            this.Text = "Khách hàng thân thiết - Thẻ nội bộ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_KHTT_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBanverapphim;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itDatverapphim;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itSoatverapphim;



    }
}