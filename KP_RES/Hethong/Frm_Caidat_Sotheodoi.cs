﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Sotheodoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Sotheodoi()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Cài đặt";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Sotheodoi_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itSoHopDong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoHopDong.Checked)
            {
                this.Parent.GoTo<Frm_SoHopDong>();
            }
        }

        private void itNhacungcap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhacungcap.Checked)
            {
                this.Parent.GoTo<Frm_NCCKH>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itSoHopDong);
            cls_KP_RES.RemoveTileItem(itNhacungcap);
        }

        private void LoadPermission()
        {
            itSoHopDong.Checked = true;
            itNhacungcap.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

      

        

       

        

       
      

        
        

        

        

    }
}