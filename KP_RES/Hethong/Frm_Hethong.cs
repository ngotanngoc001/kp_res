﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_License;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using RestSharp;
using Newtonsoft.Json;
using System.Resources;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_Hethong : DevExpress.XtraEditors.XtraForm
    {

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Hethong()
        {
            InitializeComponent();
            this.Text = rm.GetString("hethong", culture);
            tileControl1.Text = rm.GetString("hethong", culture);
            LoadPermission();
            cls_KP_RES.RemoveTileItem(itDoimatkhau);
            cls_KP_RES.RemoveTileItem(itNguoidung);
            cls_KP_RES.RemoveTileItem(itSaoluudulieu);
            cls_KP_RES.RemoveTileItem(itPhuchoidulieu);
            cls_KP_RES.RemoveTileItem(itDangkysudung);
            cls_KP_RES.RemoveTileItem(itKichhoat);
            cls_KP_RES.RemoveTileItem(itCapnhat);
            cls_KP_RES.RemoveTileItem(itDaily);
            cls_KP_RES.RemoveTileItem(itXoadulieu);
            cls_KP_RES.RemoveTileItem(itDongbodulieu );
        }

        private void Frm_Hethong_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itDoimatkhau_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDoimatkhau.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.PF_Doimatkhau3>();
            }
        }

        private void itNguoidung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNguoidung.Checked)
            {
                this.Parent.GoTo<Frm_Nguoidung>();
            }
        }

        private void itSaoluudulieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSaoluudulieu.Checked )
            {
                clsUserManagement.BackupDatabase();
            }
        }

        private void itPhuchoidulieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhuchoidulieu.Checked)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn phục hồi dữ liệu ?" + "\nThao tác có thể làm mất dữ liệu", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                clsUserManagement.RestoreDatabase();
            }
        }

        private void itDangkysudung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDangkysudung.Checked)
            {
                this.Parent.GoTo<KP_License.Frm_Createmachinecode3>();
            }
        }

        private void itKichhoat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKichhoat.Checked)
            {
                KP_Terminal.PF_Cauhinh_Active frm = new KP_Terminal.PF_Cauhinh_Active();
                frm.ShowDialog();
                if (frm.status)
                {
                    this.Parent.GoTo<Frm_Kichhoatchucnang>();
                }
                frm.Dispose();
            }
        }

        private void itCapnhat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCapnhat.Checked)
            {
                Application.Exit();
                if (File.Exists(Application.StartupPath + "\\KP_Update.exe"))
                {
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\KP_Update.exe");
                }
                else
                {
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\UpdateApp.exe");
                }
            }
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        private void LoadPermission()
        {
            itDoimatkhau.Checked = clsUserManagement.AllowView("1");
            itNguoidung.Checked = clsUserManagement.AllowView("10") || clsUserManagement.AllowView("11") || clsUserManagement.AllowView("2") || clsUserManagement.AllowView("19") || clsUserManagement.AllowView("3");
            itSaoluudulieu.Checked = clsUserManagement.AllowView("20");
            itPhuchoidulieu .Checked = clsUserManagement.AllowView("21");
            itXoadulieu.Checked = clsUserManagement.AllowView("21");
            itDangkysudung.Checked = clsUserManagement.AllowView("43");
            itKichhoat.Checked = clsUserManagement.AllowView("44");
            itCapnhat.Checked = clsUserManagement.AllowView("45");
            itDaily.Checked = true;
            itDongbodulieu.Checked = clsUserManagement.AllowView("31071801");
        }

        private void itDaily_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDaily.Checked)
            {
                bool bCheck = false;
                KP_Terminal.PF_Cauhinh frm1 = new KP_Terminal.PF_Cauhinh();
                frm1.ShowDialog();
                bCheck = frm1.status;
                frm1.Dispose();

                if (bCheck)
                {
                    this.Parent.GoTo<Frm_Daily>();
                }
            }
        }

        private void itXoadulieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itXoadulieu.Checked)
            {
                bool bCheck = false;
                KP_Terminal.PF_Cauhinh frm1 = new KP_Terminal.PF_Cauhinh();
                frm1.ShowDialog();
                bCheck = frm1.status;
                frm1.Dispose();

                if (bCheck)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa dữ liệu bán hàng và kho ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string sSQL ="";
                        sSQL += "delete from ct_hoadon" + "\n";
                        sSQL += "delete from ct_moban" + "\n";
                        sSQL += "delete from display_order" + "\n";
                        sSQL += "delete from huy_mon" + "\n";
                        sSQL += "delete from huy_ban" + "\n";
                        sSQL += "delete from hoadon" + "\n";
                        sSQL += "delete from hoadon_inlai" + "\n";
                        sSQL += "delete from CT_HOADON_TRAHANG  " + "\n";
                        sSQL += "delete from HOADON_TRAHANG " + "\n";
                        sSQL += "delete from tonkho" + "\n";
                        sSQL += "delete from kho_dongiabinhquan" + "\n";
                        sSQL += "delete from chitietphieukho" + "\n";
                        sSQL += "delete from kho_phieukho" + "\n";
                        sSQL += "delete from KHACHHANG_THANHTOAN_NOIBO " + "\n";
                        sSQL += "delete from KHTT_DIEMTHUONG_QUATANG  " + "\n";
                        sSQL += "delete from KHTT_SOLE  " + "\n";
                        sSQL += "delete from KHACHHANG_THANTHIET  " + "\n";
                        sSQL += "delete from KHTT_CONG_TRU_DIEM    " + "\n";
                        sSQL += "delete from TMP_DSHH   " + "\n"; 
                        sSQL += "delete from TMP_DSNCC   " + "\n";
                     

                        if(clsMain .ExecuteSQL (sSQL ))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Đã xóa dữ liệu bán hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
        }

        private void itDongbodulieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDongbodulieu.Checked ==false )
            {
                return;
            }
            splashScreenManager1.ShowWaitForm();
           
            try
            {
                if (cls_ConfigCashier.sStore_ID == "0" || cls_ConfigCashier.sMenuID == "0")
                {
                    cls_ConfigCashier.LoadCauHinh();
                }

                Frm_Dongbo frm = new Frm_Dongbo();
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            splashScreenManager1.CloseWaitForm();
            
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            
            itDoimatkhau.Text = rm.GetString("doimatkhau", culture);
            itDangkysudung.Text = rm.GetString("dangkysudung", culture);
            itDaily.Text = rm.GetString("daily", culture);
            itDongbodulieu.Text = rm.GetString("dongbodulieu", culture);
            itKichhoat.Text = rm.GetString("kichhoatchucnang", culture);
            itNguoidung.Text = rm.GetString("quanlynguoidung", culture);
            itPhuchoidulieu.Text = rm.GetString("phuchoidulieu", culture);
            itSaoluudulieu.Text = rm.GetString("saoluudulieu", culture);
            itXoadulieu.Text = rm.GetString("xoadulieu", culture);
            itCapnhat.Text = rm.GetString("capnhatphanmem", culture);
            
        }


      

    }
}