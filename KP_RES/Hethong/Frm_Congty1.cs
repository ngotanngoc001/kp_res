﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Congty1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Congty1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Congty1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhonghanhchinh.Checked)
            {
                this.Parent.GoTo<Frm_Phonghanhchinh>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongnhansu.Checked)
            {
                this.Parent.GoTo<Frm_Phongnhansu >();
            }
        }

        private void itCongnothuchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongketoan.Checked)
            {
                this.Parent.GoTo<Frm_Phongketoan>();
            }
        }

        private void itPhongtaichinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongtaichinh.Checked)
            {
                this.Parent.GoTo<Frm_Phongtaichinh>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itPhonghanhchinh);
            cls_KP_RES.RemoveTileItem(itPhongnhansu);
            cls_KP_RES.RemoveTileItem(itPhongketoan);
            cls_KP_RES.RemoveTileItem(itPhongtaichinh);
        }

        private void LoadPermission()
        {
            itPhonghanhchinh.Checked = (clsUserManagement.AllowView("24031601") || clsUserManagement.AllowView("24031602") || clsUserManagement.AllowView("24031603")) && clsUserManagement.CheckActive("16");
            itPhongnhansu.Checked = (clsUserManagement.AllowView("21051601") || clsUserManagement.AllowView("21051602") || clsUserManagement.AllowView("21051603") || clsUserManagement.AllowView("21051604") || clsUserManagement.AllowView("21051605") || clsUserManagement.AllowView("6051602") || clsUserManagement.AllowView("5051601") || clsUserManagement.AllowView("5051602") || clsUserManagement.AllowView("6051601") || clsUserManagement.AllowView("6051603")) && clsUserManagement.CheckActive("17");
            itPhongketoan.Checked = (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98") || clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
            itPhongtaichinh.Checked = (clsUserManagement.AllowView("981601") || clsUserManagement.AllowView("98")) && clsUserManagement.CheckActive("12");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       

      

       
        

       

      

       

        

        

        

        

    }
}