﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Congvan1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Congvan1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Congvan1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNghiepvu.Checked)
            {
                this.Parent.GoTo<Frm_CongVan>();
            }
        }


        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_CongVan>();
            }
        }

        private void itCaidat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidat.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_CongVan>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCaidat);
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itNghiepvu);
        }

        private void LoadPermission()
        {
            itCaidat.Checked = clsUserManagement.AllowView("24031603") && clsUserManagement.CheckActive("16");
            itBaocao.Checked = clsUserManagement.AllowView("24031602") && clsUserManagement.CheckActive("16");
            itNghiepvu.Checked = clsUserManagement.AllowView("24031601") && clsUserManagement.CheckActive("16");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       
        

       

      

       

        

        

        

        

    }
}