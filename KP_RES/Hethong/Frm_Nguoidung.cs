﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;


namespace KP_RES 
{
    public partial class Frm_Nguoidung : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Nguoidung()
        {
            
            InitializeComponent();
            this.Text = rm.GetString("quanlynguoidung", culture);
            tileControl1.Text = rm.GetString("quanlynguoidung", culture);
            LoadPermission();
            cls_KP_RES.RemoveTileItem(itBophan);
            cls_KP_RES.RemoveTileItem(itNhanvien);
            cls_KP_RES.RemoveTileItem(itNhomnguoidung);
            cls_KP_RES.RemoveTileItem(itNguoidung);
            cls_KP_RES.RemoveTileItem(itPhanquyen);
        }

        private void Frm_Nguoidung_Paint(object sender, PaintEventArgs e)
        {
           
            LoadPermission();
           
        }

        private void itBophan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBophan.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Chucvu3>();
            }
        }

        private void itNhomnguoidung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhomnguoidung.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Nhomnguoidung3>();
            }
        }

        private void itNhanvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhanvien.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Nhanvien3>();
            }
        }

        private void itNguoidung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNguoidung.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Nguoidung3>();
            }
        }

        private void itPhanquyen_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhanquyen.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Phanquyen3>();
            }
        }

        private void LoadPermission()
        {
            itBophan .Checked = clsUserManagement.AllowView("10");
            itNhanvien.Checked = clsUserManagement.AllowView("11");
            itNhomnguoidung.Checked = clsUserManagement.AllowView("2");
            itNguoidung.Checked = clsUserManagement.AllowView("19");
            itPhanquyen.Checked = clsUserManagement.AllowView("3");
        }
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            itBophan.Text = rm.GetString("phongban", culture);
            itNhanvien.Text = rm.GetString("nhanvien", culture);
            itNhomnguoidung.Text = rm.GetString("nhomquyen", culture);
            itNguoidung.Text = rm.GetString("taikhoan", culture);
            itPhanquyen.Text = rm.GetString("phanquyen", culture);
        }
    }
}