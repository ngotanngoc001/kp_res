﻿namespace KP_RES 
{
    partial class  Frm_Congty1 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itPhonghanhchinh = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongnhansu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongketoan = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongtaichinh = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 60;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 30;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itPhonghanhchinh);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itPhonghanhchinh
            // 
            this.itPhonghanhchinh.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itPhonghanhchinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Phòng hành chính";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhonghanhchinh.Elements.Add(tileItemElement1);
            this.itPhonghanhchinh.Id = 56;
            this.itPhonghanhchinh.IsLarge = true;
            this.itPhonghanhchinh.Name = "itPhonghanhchinh";
            this.itPhonghanhchinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itPhongnhansu);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itPhongnhansu
            // 
            this.itPhongnhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itPhongnhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Phòng nhân sự";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongnhansu.Elements.Add(tileItemElement2);
            this.itPhongnhansu.Id = 57;
            this.itPhongnhansu.IsLarge = true;
            this.itPhongnhansu.Name = "itPhongnhansu";
            this.itPhongnhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itPhongketoan);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itPhongketoan
            // 
            this.itPhongketoan.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itPhongketoan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Phòng kế toán";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongketoan.Elements.Add(tileItemElement3);
            this.itPhongketoan.Id = 58;
            this.itPhongketoan.IsLarge = true;
            this.itPhongketoan.Name = "itPhongketoan";
            this.itPhongketoan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongnothuchi_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itPhongtaichinh);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itPhongtaichinh
            // 
            this.itPhongtaichinh.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itPhongtaichinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Phòng tài chính";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongtaichinh.Elements.Add(tileItemElement4);
            this.itPhongtaichinh.Id = 59;
            this.itPhongtaichinh.IsLarge = true;
            this.itPhongtaichinh.Name = "itPhongtaichinh";
            this.itPhongtaichinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongtaichinh_ItemClick);
            // 
            // Frm_Congty1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Congty1";
            this.Text = "Công ty";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itPhonghanhchinh;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itPhongnhansu;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itPhongketoan;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itPhongtaichinh;
    }
}