﻿namespace KP_RES 
{
    partial class Frm_Baocao_Banve 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itBanverap = new DevExpress.XtraEditors.TileItem();
            this.itBanvetruoc = new DevExpress.XtraEditors.TileItem();
            this.itBanveonline = new DevExpress.XtraEditors.TileItem();
            this.itHuyverap = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itInbanggia = new DevExpress.XtraEditors.TileItem();
            this.itLichchieu = new DevExpress.XtraEditors.TileItem();
            this.itLichsudoilich = new DevExpress.XtraEditors.TileItem();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itDatverap = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itDoanhthurap = new DevExpress.XtraEditors.TileItem();
            this.itBanvephathanh = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itBKCTBV = new DevExpress.XtraEditors.TileItem();
            this.itBanvetheoKH = new DevExpress.XtraEditors.TileItem();
            this.itBanvetheoShow = new DevExpress.XtraEditors.TileItem();
            this.itBanvetheoNhanvien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itBanverap);
            this.tileGroup12.Items.Add(this.itBanvetruoc);
            this.tileGroup12.Items.Add(this.itBanveonline);
            this.tileGroup12.Items.Add(this.itHuyverap);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itBanverap
            // 
            this.itBanverap.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itBanverap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.StretchVertical;
            tileItemElement1.Text = "Bán vé";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanverap.Elements.Add(tileItemElement1);
            this.itBanverap.Id = 38;
            this.itBanverap.IsLarge = true;
            this.itBanverap.Name = "itBanverap";
            this.itBanverap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanverap_ItemClick);
            // 
            // itBanvetruoc
            // 
            this.itBanvetruoc.BackgroundImage = global::KP_RES.Properties.Resources.banvetruoc;
            this.itBanvetruoc.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Bán vé trước";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanvetruoc.Elements.Add(tileItemElement2);
            this.itBanvetruoc.Id = 45;
            this.itBanvetruoc.IsLarge = true;
            this.itBanvetruoc.Name = "itBanvetruoc";
            this.itBanvetruoc.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanvetruoc_ItemClick);
            // 
            // itBanveonline
            // 
            this.itBanveonline.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho5;
            this.itBanveonline.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Bán vé Online";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanveonline.Elements.Add(tileItemElement3);
            this.itBanveonline.Id = 46;
            this.itBanveonline.IsLarge = true;
            this.itBanveonline.Name = "itBanveonline";
            this.itBanveonline.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanveonline_ItemClick);
            // 
            // itHuyverap
            // 
            this.itHuyverap.BackgroundImage = global::KP_RES.Properties.Resources.xuatkho4;
            this.itHuyverap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Hủy vé";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuyverap.Elements.Add(tileItemElement4);
            this.itHuyverap.Id = 40;
            this.itHuyverap.IsLarge = true;
            this.itHuyverap.Name = "itHuyverap";
            this.itHuyverap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuyverap_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itInbanggia);
            this.tileGroup11.Items.Add(this.itLichchieu);
            this.tileGroup11.Items.Add(this.itLichsudoilich);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itInbanggia
            // 
            this.itInbanggia.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itInbanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "In bảng giá";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itInbanggia.Elements.Add(tileItemElement5);
            this.itInbanggia.Id = 43;
            this.itInbanggia.IsLarge = true;
            this.itInbanggia.Name = "itInbanggia";
            this.itInbanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itInbanggia_ItemClick);
            // 
            // itLichchieu
            // 
            this.itLichchieu.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itLichchieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Lịch chiếu phim";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itLichchieu.Elements.Add(tileItemElement6);
            this.itLichchieu.Id = 37;
            this.itLichchieu.IsLarge = true;
            this.itLichchieu.Name = "itLichchieu";
            this.itLichchieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLichchieu_ItemClick);
            // 
            // itLichsudoilich
            // 
            this.itLichsudoilich.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itLichsudoilich.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Lịch sử lịch chiếu";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itLichsudoilich.Elements.Add(tileItemElement7);
            this.itLichsudoilich.Id = 52;
            this.itLichsudoilich.IsLarge = true;
            this.itLichsudoilich.Name = "itLichsudoilich";
            this.itLichsudoilich.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLichsudoilich_ItemClick);
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 53;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1306, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itDatverap);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itDatverap
            // 
            this.itDatverap.BackgroundImage = global::KP_RES.Properties.Resources.tonkho4;
            this.itDatverap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Đặt vé";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itDatverap.Elements.Add(tileItemElement8);
            this.itDatverap.Id = 39;
            this.itDatverap.IsLarge = true;
            this.itDatverap.Name = "itDatverap";
            this.itDatverap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDatverap_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itDoanhthurap);
            this.tileGroup8.Items.Add(this.itBanvephathanh);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itDoanhthurap
            // 
            this.itDoanhthurap.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itDoanhthurap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Doanh thu";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itDoanhthurap.Elements.Add(tileItemElement9);
            this.itDoanhthurap.Id = 41;
            this.itDoanhthurap.IsLarge = true;
            this.itDoanhthurap.Name = "itDoanhthurap";
            this.itDoanhthurap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDoanhthurap_ItemClick);
            // 
            // itBanvephathanh
            // 
            this.itBanvephathanh.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itBanvephathanh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Kết quả chiếu phim";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanvephathanh.Elements.Add(tileItemElement10);
            this.itBanvephathanh.Id = 44;
            this.itBanvephathanh.IsLarge = true;
            this.itBanvephathanh.Name = "itBanvephathanh";
            this.itBanvephathanh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanvephathanh_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itBKCTBV);
            this.tileGroup9.Items.Add(this.itBanvetheoKH);
            this.tileGroup9.Items.Add(this.itBanvetheoShow);
            this.tileGroup9.Items.Add(this.itBanvetheoNhanvien);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itBKCTBV
            // 
            this.itBKCTBV.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itBKCTBV.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Bảng kê chi tiết bán vé";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itBKCTBV.Elements.Add(tileItemElement11);
            this.itBKCTBV.Id = 47;
            this.itBKCTBV.IsLarge = true;
            this.itBKCTBV.Name = "itBKCTBV";
            this.itBKCTBV.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBKCTBV_ItemClick);
            // 
            // itBanvetheoKH
            // 
            this.itBanvetheoKH.BackgroundImage = global::KP_RES.Properties.Resources.danhsachkhct;
            this.itBanvetheoKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Tổng hợp bán hàng theo khách hàng";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanvetheoKH.Elements.Add(tileItemElement12);
            this.itBanvetheoKH.Id = 48;
            this.itBanvetheoKH.IsLarge = true;
            this.itBanvetheoKH.Name = "itBanvetheoKH";
            this.itBanvetheoKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanvetheoKH_ItemClick);
            // 
            // itBanvetheoShow
            // 
            this.itBanvetheoShow.BackgroundImage = global::KP_RES.Properties.Resources.Caban;
            this.itBanvetheoShow.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Tổng hợp bán hàng theo show diễn";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanvetheoShow.Elements.Add(tileItemElement13);
            this.itBanvetheoShow.Id = 49;
            this.itBanvetheoShow.IsLarge = true;
            this.itBanvetheoShow.Name = "itBanvetheoShow";
            this.itBanvetheoShow.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanvetheoShow_ItemClick);
            // 
            // itBanvetheoNhanvien
            // 
            this.itBanvetheoNhanvien.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itBanvetheoNhanvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Tổng hợp bán hàng theo nhân viên";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanvetheoNhanvien.Elements.Add(tileItemElement14);
            this.itBanvetheoNhanvien.Id = 50;
            this.itBanvetheoNhanvien.IsLarge = true;
            this.itBanvetheoNhanvien.Name = "itBanvetheoNhanvien";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1310, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Tồn kho";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement15);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Tồn kho";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement16);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // Frm_Baocao_Banve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1310, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Banve";
            this.Text = "Báo cáo bán vé rạp phim";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Banve_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itBanverap;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itInbanggia;
        private DevExpress.XtraEditors.TileItem itLichchieu;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itHuyverap;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itDatverap;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itBanvephathanh;
        private DevExpress.XtraEditors.TileItem itBanvetruoc;
        private DevExpress.XtraEditors.TileItem itDoanhthurap;
        private DevExpress.XtraEditors.TileItem itBanveonline;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itBKCTBV;
        private DevExpress.XtraEditors.TileItem itBanvetheoKH;
        private DevExpress.XtraEditors.TileItem itBanvetheoShow;
        private DevExpress.XtraEditors.TileItem itBanvetheoNhanvien;
        private DevExpress.XtraEditors.TileItem itLichsudoilich;



    }
}