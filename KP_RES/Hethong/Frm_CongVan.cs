﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_CongVan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CongVan()
        {
            InitializeComponent();
          if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Nghiệp vụ";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_CongVan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongVanDen_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongVanDen.Checked)
            {
                this.Parent.GoTo<Frm_CongVanDen>();
            }
        }

        private void itCongVanDi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongVanDi.Checked)
            {
                this.Parent.GoTo<Frm_CongVanDi>();
            }
        }

        private void itBienBanGiaoNhan_ItemClick(object sender, TileItemEventArgs e)
        {
            //if (itBienBanGiaoNhan.Checked)
            //{
            //    this.Parent.GoTo<Frm_BienBan_GiaoNhan>();
            //}
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCongVanDen);
            cls_KP_RES.RemoveTileItem(itCongVanDi);
          //  cls_KP_RES.RemoveTileItem(itBienBanGiaoNhan);
        }

        private void LoadPermission()
        {
            itCongVanDen.Checked = true;
            itCongVanDi.Checked = true;
           // itBienBanGiaoNhan.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

       

        

        

        

        

    }
}