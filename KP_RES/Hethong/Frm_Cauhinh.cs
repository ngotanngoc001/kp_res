﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Cauhinh : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinh()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            InitializeComponent();
            this.Text = rm.GetString("cauhinh", culture);
            tileControl1.Text = rm.GetString("cauhinh", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Cauhinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itMayin_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMayin.Checked)
            {
                clsKP_Terminal .ShowConfig_Printer ();
            }
        }

        private void itHienthigia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHienthigia.Checked)
            {
                clsKP_Terminal.ShowConfig_CustomerDisplay();
            }
        }

        private void itKettien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHienthigia.Checked)
            {
                clsKP_Terminal.ShowConfig_CashDrawer();
            }
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                KP_Report.Frm_Cauhinhbaocao frm = new KP_Report.Frm_Cauhinhbaocao();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itQuay_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuay.Checked)
            {
                Frm_Cauhinhquay frmch = new Frm_Cauhinhquay();
                frmch.ShowDialog();
                frmch.Dispose();
            }
        }

        private void itCauhinhgiaThue_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhgiaThue.Checked)
            {
                Frm_Cauhinhdongia frm = new Frm_Cauhinhdongia();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itBanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanggia.Checked)
            {
                Frm_Cauhinhbanggia frm = new Frm_Cauhinhbanggia();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itBep_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBep.Checked)
            {
                Frm_CauhinhManhinhbep frm = new Frm_CauhinhManhinhbep();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhxuatkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhxuatkho.Checked)
            {
                Frm_Cauhinhxuatam frm = new Frm_Cauhinhxuatam();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itGiaodien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiaodien.Checked)
            {
                Frm_Giaodien frm = new Frm_Giaodien();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhemail_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhemail.Checked)
            {
                Frm_Cauhinhguimail frm = new Frm_Cauhinhguimail();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itMatkhaubanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMatkhaubanhang.Checked)
            {
                Frm_XacNhan frm = new  Frm_XacNhan ();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itRap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itRap.Checked)
            {
                Frm_Cauhinhrap frm = new Frm_Cauhinhrap();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhthoigianbanve_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhthoigianbanve.Checked)
            {
                Frm_CauhinhThoigianngungbanve frm = new Frm_CauhinhThoigianngungbanve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhmenu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhmenu.Checked)
            {
                Frm_Cauhinhtukhoidong frm = new Frm_Cauhinhtukhoidong();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhkhuvuc_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhkhuvuc.Checked)
            {
                Frm_CauhinhKhuvuc_Nhomhang frm = new Frm_CauhinhKhuvuc_Nhomhang();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhcaban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhcaban.Checked)
            {
                Frm_Cauhinhcaban frm = new Frm_Cauhinhcaban();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itNgaytinhton_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNgaytinhton.Checked)
            {
                Frm_Cauhinhtonkho frm = new Frm_Cauhinhtonkho();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itNgonngu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNgonngu.Checked)
            {
                Frm_Ngonngu frm = new Frm_Ngonngu();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhuser_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhuser.Checked)
            {
                Frm_Cauhinhkichhoatuser frm = new Frm_Cauhinhkichhoatuser();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCanhbaotonkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCanhbaotonkho.Checked)
            {
                Frm_ConfigInventory frm = new Frm_ConfigInventory();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itKHTT_Sole_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT_Sole.Checked)
            {
                Frm_CauhinhSole frm = new Frm_CauhinhSole();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itKHTT_Tinhdiem_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT_Tinhdiem.Checked)
            {
                Frm_CauhinhCongDiemKHTT frm = new Frm_CauhinhCongDiemKHTT();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCongnothuchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongnothuchi.Checked)
            {
                Frm_Cauhinh_Phieuchi frm = new Frm_Cauhinh_Phieuchi();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhintem_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhintem.Checked)
            {
                Frm_Cauhinhintem frm = new Frm_Cauhinhintem();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itCauhinhtinhgiobida_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhtinhgiobida.Checked)
            {
                Frm_Cauhinhtinhgiobida frm = new Frm_Cauhinhtinhgiobida();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itInTongBill_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itInTongBill.Checked)
            {
                Frm_InBillTong frm = new Frm_InBillTong();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itMayin);
            cls_KP_RES.RemoveTileItem(itHienthigia);
            cls_KP_RES.RemoveTileItem(itKettien);
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itQuay);
            cls_KP_RES.RemoveTileItem(itBep);
            cls_KP_RES.RemoveTileItem(itCauhinhgiaThue);
            cls_KP_RES.RemoveTileItem(itBanggia);
            cls_KP_RES.RemoveTileItem(itCauhinhxuatkho);
            cls_KP_RES.RemoveTileItem(itCauhinhemail);
            cls_KP_RES.RemoveTileItem(itMatkhaubanhang);
            cls_KP_RES.RemoveTileItem(itGiaodien);
            cls_KP_RES.RemoveTileItem(itRap );
            cls_KP_RES.RemoveTileItem(itCauhinhthoigianbanve);
            cls_KP_RES.RemoveTileItem(itCauhinhmenu);
            cls_KP_RES.RemoveTileItem(itCauhinhkhuvuc);
            cls_KP_RES.RemoveTileItem(itCauhinhcaban);
            cls_KP_RES.RemoveTileItem(itNgaytinhton);
            cls_KP_RES.RemoveTileItem(itNgonngu);
            cls_KP_RES.RemoveTileItem(itCauhinhuser);
            cls_KP_RES.RemoveTileItem(itCanhbaotonkho);
            cls_KP_RES.RemoveTileItem(itKHTT_Sole);
            cls_KP_RES.RemoveTileItem(itKHTT_Tinhdiem);
            cls_KP_RES.RemoveTileItem(itCongnothuchi );
            cls_KP_RES.RemoveTileItem(itCauhinhintem);
            cls_KP_RES.RemoveTileItem(itCauhinhtinhgiobida);
            cls_KP_RES.RemoveTileItem(itInTongBill);
        }

        private void LoadPermission()
        {
            itMayin.Checked = clsUserManagement.AllowView("34");
            itHienthigia.Checked = clsUserManagement.AllowView("34");
            itKettien.Checked = clsUserManagement.AllowView("34");
            itBaocao.Checked = clsUserManagement.AllowView("35");
            itQuay.Checked = clsUserManagement.AllowView("36");
            itBep.Checked = clsUserManagement.AllowView("37");
            itCauhinhgiaThue.Checked = clsUserManagement.AllowView("38");
            itBanggia.Checked = clsUserManagement.AllowView("39");
            itCauhinhxuatkho.Checked = clsUserManagement.AllowView("46");
            itCauhinhemail.Checked = clsUserManagement.AllowView("49");
            itMatkhaubanhang.Checked = clsUserManagement.AllowView("51");
            itRap.Checked = clsUserManagement.AllowView("88");
            itCauhinhthoigianbanve.Checked = clsUserManagement.AllowView("101");
            itCauhinhmenu.Checked = clsUserManagement.AllowView("108");
            itGiaodien.Checked = true;
            itCauhinhkhuvuc.Checked = clsUserManagement.AllowView("13041501");
            itCauhinhcaban.Checked = clsUserManagement.AllowView("36");
            itNgaytinhton.Checked = clsUserManagement.AllowView("12051501");
            itCauhinhuser.Checked = clsUserManagement.AllowView("1906201501");
            itCanhbaotonkho.Checked = clsUserManagement.AllowView("14111501");
            itNgonngu.Checked = true;
            itKHTT_Sole.Checked = clsUserManagement.AllowView("26121501"); ;
            itKHTT_Tinhdiem.Checked = clsUserManagement.AllowView("26121501");
            itCongnothuchi.Checked = clsUserManagement.AllowView("26071603");
            itCauhinhintem.Checked = clsUserManagement.AllowView("34");
            itCauhinhtinhgiobida.Checked = clsUserManagement.AllowView("39");
            itInTongBill.Checked = clsUserManagement.AllowView("34");

        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            itGiaodien.Text = rm.GetString("giaodien", culture);
            itBaocao.Text = rm.GetString("cauhinhbaocao", culture);
            itBep.Text = rm.GetString("cauhinhbep", culture);
            itMayin.Text = rm.GetString("mayin", culture);
            itHienthigia.Text = rm.GetString("hienthigia", culture);
            itKettien.Text = rm.GetString("kettien", culture);
            itMatkhaubanhang.Text = rm.GetString("matkhaubanhang", culture);
            itQuay.Text = rm.GetString("cauhinhquay", culture);
            itCauhinhgiaThue.Text = rm.GetString("cauhinhdongiathue", culture);
            itCauhinhxuatkho.Text = rm.GetString("cauhinhxuatkho", culture);
            itBanggia.Text = rm.GetString("cauhinhbanggia", culture);
            itCauhinhkhuvuc.Text = rm.GetString("cauhinhkhuvuc", culture);
            itCauhinhemail.Text = rm.GetString("cauhinhemail", culture);
            itRap.Text = rm.GetString("cauhinhrap", culture);
            itCauhinhthoigianbanve.Text = rm.GetString("cauhinhthoigianngungbanve", culture);
            itCauhinhmenu.Text = rm.GetString("cauhinhmenu", culture);
            itCauhinhkhuvuc.Text = rm.GetString("cauhinhkhuvuc", culture);
            itCauhinhcaban.Text = rm.GetString("cauhinhcaban", culture);
            itNgaytinhton.Text = rm.GetString("cauhinhngaytinhton", culture);
            itNgonngu.Text  = rm.GetString("cauhinhngonngu", culture);
            itCanhbaotonkho.Text = rm.GetString("canhbaotonkho", culture);
            itCauhinhintem.Text = rm.GetString("cauhinhintem", culture);
            itKHTT_Sole.Text = rm.GetString("cauhinhsolekhtt", culture);
            itKHTT_Tinhdiem.Text = rm.GetString("cauhinhtinhdiemkhtt", culture);
            itCauhinhtinhgiobida.Text = rm.GetString("cauhinhbida", culture);

            this.Text = rm.GetString("cauhinh", culture);
            tileControl1.Text = rm.GetString("cauhinh", culture);
        }

      
        
    

        

       

    }
}