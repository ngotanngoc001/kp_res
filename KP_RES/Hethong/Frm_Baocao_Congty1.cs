﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Congty1 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Congty1()
        {
            InitializeComponent();

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Hanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_CongVan>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhansu.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhansu>();
            }
        }

        private void itCongnothuchi_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCongvan);
            cls_KP_RES.RemoveTileItem(itNhansu);
        }

        private void LoadPermission()
        {
            itCongvan.Checked = clsUserManagement.AllowView("24031602") && clsUserManagement.CheckActive("16"); ;
            itNhansu.Checked = (clsUserManagement.AllowView("21051601") || clsUserManagement.AllowView("21051602") || clsUserManagement.AllowView("21051603") || clsUserManagement.AllowView("21051604") || clsUserManagement.AllowView("21051605")) && clsUserManagement.CheckActive("17"); ;
        }

      

      
    }
}