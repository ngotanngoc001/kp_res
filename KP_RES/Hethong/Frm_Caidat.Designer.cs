﻿namespace KP_RES 
{
    partial class Frm_Caidat 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itCaidatchung = new DevExpress.XtraEditors.TileItem();
            this.itNhahang = new DevExpress.XtraEditors.TileItem();
            this.itKaraoke = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itKhachangthanthiet = new DevExpress.XtraEditors.TileItem();
            this.itVetrochoi = new DevExpress.XtraEditors.TileItem();
            this.itCongvan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itKhuyenmai = new DevExpress.XtraEditors.TileItem();
            this.itVoucher = new DevExpress.XtraEditors.TileItem();
            this.itKhachsan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itVe = new DevExpress.XtraEditors.TileItem();
            this.itNhansu = new DevExpress.XtraEditors.TileItem();
            this.itDanhgiaKH = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itTheodoihopdong = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.itMakhuyenmai = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup14);
            this.tileControl1.Groups.Add(this.tileGroup15);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 64;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 415;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 467);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itCaidatchung);
            this.tileGroup2.Items.Add(this.itNhahang);
            this.tileGroup2.Items.Add(this.itKaraoke);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itCaidatchung
            // 
            this.itCaidatchung.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itCaidatchung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Cài đặt chung";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidatchung.Elements.Add(tileItemElement1);
            this.itCaidatchung.Id = 49;
            this.itCaidatchung.IsLarge = true;
            this.itCaidatchung.Name = "itCaidatchung";
            this.itCaidatchung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidatchung_ItemClick);
            // 
            // itNhahang
            // 
            this.itNhahang.BackgroundImage = global::KP_RES.Properties.Resources.nhahang28;
            this.itNhahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Nhà hàng";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhahang.Elements.Add(tileItemElement2);
            this.itNhahang.Id = 50;
            this.itNhahang.IsLarge = true;
            this.itNhahang.Name = "itNhahang";
            this.itNhahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhahang_ItemClick);
            // 
            // itKaraoke
            // 
            this.itKaraoke.BackgroundImage = global::KP_RES.Properties.Resources.karaoke3;
            this.itKaraoke.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Karaoke";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itKaraoke.Elements.Add(tileItemElement3);
            this.itKaraoke.Id = 56;
            this.itKaraoke.IsLarge = true;
            this.itKaraoke.Name = "itKaraoke";
            this.itKaraoke.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKaraoke_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itKhachangthanthiet);
            this.tileGroup10.Items.Add(this.itVetrochoi);
            this.tileGroup10.Items.Add(this.itCongvan);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itKhachangthanthiet
            // 
            this.itKhachangthanthiet.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKhachangthanthiet.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Khách hàng thân thiết";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachangthanthiet.Elements.Add(tileItemElement4);
            this.itKhachangthanthiet.Id = 52;
            this.itKhachangthanthiet.IsLarge = true;
            this.itKhachangthanthiet.Name = "itKhachangthanthiet";
            this.itKhachangthanthiet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachangthanthiet_ItemClick);
            // 
            // itVetrochoi
            // 
            this.itVetrochoi.BackgroundImage = global::KP_RES.Properties.Resources.trochoi;
            this.itVetrochoi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Vé khu vui chơi";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itVetrochoi.Elements.Add(tileItemElement5);
            this.itVetrochoi.Id = 55;
            this.itVetrochoi.IsLarge = true;
            this.itVetrochoi.Name = "itVetrochoi";
            this.itVetrochoi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVetrochoi_ItemClick);
            // 
            // itCongvan
            // 
            this.itCongvan.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCongvan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itCongvan.Checked = true;
            tileItemElement6.Text = "Công văn";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongvan.Elements.Add(tileItemElement6);
            this.itCongvan.Id = 59;
            this.itCongvan.IsLarge = true;
            this.itCongvan.Name = "itCongvan";
            this.itCongvan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itKhuyenmai);
            this.tileGroup12.Items.Add(this.itVoucher);
            this.tileGroup12.Items.Add(this.itKhachsan);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itKhuyenmai
            // 
            this.itKhuyenmai.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho1;
            this.itKhuyenmai.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Khuyến mãi";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhuyenmai.Elements.Add(tileItemElement7);
            this.itKhuyenmai.Id = 53;
            this.itKhuyenmai.IsLarge = true;
            this.itKhuyenmai.Name = "itKhuyenmai";
            this.itKhuyenmai.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhuyenmai_ItemClick);
            // 
            // itVoucher
            // 
            this.itVoucher.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itVoucher.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Voucher";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itVoucher.Elements.Add(tileItemElement8);
            this.itVoucher.Id = 58;
            this.itVoucher.IsLarge = true;
            this.itVoucher.Name = "itVoucher";
            this.itVoucher.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVoucher_ItemClick);
            // 
            // itKhachsan
            // 
            this.itKhachsan.BackgroundImage = global::KP_RES.Properties.Resources.khachsan28;
            this.itKhachsan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Khách sạn";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachsan.Elements.Add(tileItemElement9);
            this.itKhachsan.Id = 57;
            this.itKhachsan.IsLarge = true;
            this.itKhachsan.Name = "itKhachsan";
            this.itKhachsan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachsan_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itVe);
            this.tileGroup13.Items.Add(this.itNhansu);
            this.tileGroup13.Items.Add(this.itDanhgiaKH);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itVe
            // 
            this.itVe.BackgroundImage = global::KP_RES.Properties.Resources.caidatve;
            this.itVe.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Rạp phim";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itVe.Elements.Add(tileItemElement10);
            this.itVe.Id = 51;
            this.itVe.IsLarge = true;
            this.itVe.Name = "itVe";
            this.itVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVe_ItemClick);
            // 
            // itNhansu
            // 
            this.itNhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Nhân sự";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhansu.Elements.Add(tileItemElement11);
            this.itNhansu.Id = 60;
            this.itNhansu.IsLarge = true;
            this.itNhansu.Name = "itNhansu";
            this.itNhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // itDanhgiaKH
            // 
            this.itDanhgiaKH.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itDanhgiaKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Đánh giá khách hàng";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itDanhgiaKH.Elements.Add(tileItemElement12);
            this.itDanhgiaKH.Id = 61;
            this.itDanhgiaKH.IsLarge = true;
            this.itDanhgiaKH.Name = "itDanhgiaKH";
            this.itDanhgiaKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDanhgiaKH_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Items.Add(this.itTheodoihopdong);
            this.tileGroup14.Items.Add(this.itMakhuyenmai);
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itTheodoihopdong
            // 
            this.itTheodoihopdong.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itTheodoihopdong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Theo dõi hợp đồng";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itTheodoihopdong.Elements.Add(tileItemElement13);
            this.itTheodoihopdong.Id = 62;
            this.itTheodoihopdong.IsLarge = true;
            this.itTheodoihopdong.Name = "itTheodoihopdong";
            this.itTheodoihopdong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTheodoihopdong_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 471);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup11
            // 
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup15
            // 
            this.tileGroup15.Name = "tileGroup15";
            // 
            // itMakhuyenmai
            // 
            this.itMakhuyenmai.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itMakhuyenmai.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Mã khuyến mãi";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itMakhuyenmai.Elements.Add(tileItemElement14);
            this.itMakhuyenmai.Id = 63;
            this.itMakhuyenmai.IsLarge = true;
            this.itMakhuyenmai.Name = "itMakhuyenmai";
            this.itMakhuyenmai.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMakhuyenmai_ItemClick);
            // 
            // Frm_Caidat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 471);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat";
            this.Text = "Cài đặt";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itCaidatchung;
        private DevExpress.XtraEditors.TileItem itNhahang;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itKhachangthanthiet;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itKhuyenmai;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itVetrochoi;
        private DevExpress.XtraEditors.TileItem itKaraoke;
        private DevExpress.XtraEditors.TileItem itKhachsan;
        private DevExpress.XtraEditors.TileItem itVoucher;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itNhansu;
        private DevExpress.XtraEditors.TileItem itDanhgiaKH;
        private DevExpress.XtraEditors.TileItem itCongvan;
        private DevExpress.XtraEditors.TileItem itVe;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itTheodoihopdong;
        private DevExpress.XtraEditors.TileItem itMakhuyenmai;
        private DevExpress.XtraEditors.TileGroup tileGroup15;



    }
}