﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Banhang_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Banhang_Karaoke()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Banhang_Karaoke_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaocaoDSHD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDon_Karaoke>();
            }
        }

        private void itBaocaoBanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoBanhang.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banhang_Karaoke>();
            }
        }

        private void itBaocaoDoanhso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDoanhso.Checked)
            {
                this.Parent.GoTo<Frm_BC_Doanhso_Karaoke>();
            }
        }

        private void itBaocaoKQKD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoKQKD.Checked)
            {
                this.Parent.GoTo<Frm_BC_KQKD_Karaoke>();
            }
        }

        private void itThongkedoanhso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThongkedoanhso.Checked)
            {
                this.Parent.GoTo<Frm_TK_Doanhso_Karaoke>();
            }
        }

        private void itThongkebanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThongkebanhang.Checked)
            {
                this.Parent.GoTo<Frm_TK_Hangbanchay_Karaoke>();
            }
        }

        private void itDoanhthugio_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDoanhthugio.Checked)
            {
                this.Parent.GoTo<Frm_BC_DoanhthuKaraoke>();
            }
        }

        private void itBaocaoDSHD_INLAI_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD_INLAI.Checked)
            {
                this.Parent.GoTo<Frm_BC_InlaiHoadonKaraoke>();
            }
        }

        private void itHuyban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuyban.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyBanKaraoke>();
            }
        }

        private void itHuymon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuymon.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyMonKaraoke>();
            }
        }
        private void itLaylaihoadon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLaylaihoadon.Checked)
            {
                this.Parent.GoTo<Frm_DSLayLaiHDKaraoke>();
            }
        }
        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD);
            cls_KP_RES.RemoveTileItem(itBaocaoBanhang);
            cls_KP_RES.RemoveTileItem(itBaocaoDoanhso);
            cls_KP_RES.RemoveTileItem(itBaocaoKQKD);
            cls_KP_RES.RemoveTileItem(itThongkedoanhso);
            cls_KP_RES.RemoveTileItem(itThongkebanhang);
            cls_KP_RES.RemoveTileItem(itDoanhthugio);
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD_INLAI);
            cls_KP_RES.RemoveTileItem(itHuyban);
            cls_KP_RES.RemoveTileItem(itHuymon);
            cls_KP_RES.RemoveTileItem(itLaylaihoadon);
        }

        private void LoadPermission()
        {
            itBaocaoDSHD.Checked = clsUserManagement.AllowView("28");
            itBaocaoBanhang.Checked = clsUserManagement.AllowView("29");
            itBaocaoDoanhso.Checked = clsUserManagement.AllowView("30");
            itBaocaoKQKD.Checked = clsUserManagement.AllowView("31");
            itThongkedoanhso.Checked = clsUserManagement.AllowView("103");
            itThongkebanhang.Checked = clsUserManagement.AllowView("103");
            itDoanhthugio.Checked = clsUserManagement.AllowView("29"); 
            itBaocaoDSHD_INLAI.Checked = true;
            itHuyban.Checked = true;
            itHuymon.Checked = true;
            itLaylaihoadon.Checked = true;
        }

        
    }
}