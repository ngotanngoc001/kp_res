﻿namespace KP_RES 
{
    partial class Frm_Baocao 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itHanghoa = new DevExpress.XtraEditors.TileItem();
            this.itNhapxuat = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itBanhang = new DevExpress.XtraEditors.TileItem();
            this.itVe = new DevExpress.XtraEditors.TileItem();
            this.itVeTroChoi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itKHTT = new DevExpress.XtraEditors.TileItem();
            this.itCongno = new DevExpress.XtraEditors.TileItem();
            this.itKaraoke = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itNguoidung = new DevExpress.XtraEditors.TileItem();
            this.itBaocaodacthu = new DevExpress.XtraEditors.TileItem();
            this.itVoucher = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itCongvan = new DevExpress.XtraEditors.TileItem();
            this.itNhansu = new DevExpress.XtraEditors.TileItem();
            this.itDanhgiaKH = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itTheodoihopdong = new DevExpress.XtraEditors.TileItem();
            this.itMakhuyenmai = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.itKhachsan = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 61;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 532;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itHanghoa);
            this.tileGroup2.Items.Add(this.itNhapxuat);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itHanghoa
            // 
            this.itHanghoa.BackgroundImage = global::KP_RES.Properties.Resources.inmavach;
            this.itHanghoa.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Hàng hóa";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itHanghoa.Elements.Add(tileItemElement1);
            this.itHanghoa.Id = 44;
            this.itHanghoa.IsLarge = true;
            this.itHanghoa.Name = "itHanghoa";
            this.itHanghoa.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHanghoa_ItemClick);
            // 
            // itNhapxuat
            // 
            this.itNhapxuat.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itNhapxuat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Nhập xuất tồn";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhapxuat.Elements.Add(tileItemElement2);
            this.itNhapxuat.Id = 45;
            this.itNhapxuat.IsLarge = true;
            this.itNhapxuat.Name = "itNhapxuat";
            this.itNhapxuat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhapxuat_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itBanhang);
            this.tileGroup7.Items.Add(this.itVe);
            this.tileGroup7.Items.Add(this.itVeTroChoi);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itBanhang
            // 
            this.itBanhang.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itBanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Báo cáo bán hàng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanhang.Elements.Add(tileItemElement3);
            this.itBanhang.Id = 46;
            this.itBanhang.IsLarge = true;
            this.itBanhang.Name = "itBanhang";
            this.itBanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanhang_ItemClick);
            // 
            // itVe
            // 
            this.itVe.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itVe.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Báo cáo bán vé rạp phim";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itVe.Elements.Add(tileItemElement4);
            this.itVe.Id = 47;
            this.itVe.IsLarge = true;
            this.itVe.Name = "itVe";
            this.itVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVe_ItemClick);
            // 
            // itVeTroChoi
            // 
            this.itVeTroChoi.BackgroundImage = global::KP_RES.Properties.Resources.trochoi;
            this.itVeTroChoi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Báo cáo bán vé khu vui chơi";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itVeTroChoi.Elements.Add(tileItemElement5);
            this.itVeTroChoi.Id = 50;
            this.itVeTroChoi.IsLarge = true;
            this.itVeTroChoi.Name = "itVeTroChoi";
            this.itVeTroChoi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVeTroChoi_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itKHTT);
            this.tileGroup8.Items.Add(this.itCongno);
            this.tileGroup8.Items.Add(this.itKaraoke);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itKHTT
            // 
            this.itKHTT.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Khách hàng thân thiết";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT.Elements.Add(tileItemElement6);
            this.itKHTT.Id = 48;
            this.itKHTT.IsLarge = true;
            this.itKHTT.Name = "itKHTT";
            this.itKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_ItemClick);
            // 
            // itCongno
            // 
            this.itCongno.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itCongno.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Công nợ - Thu chi";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongno.Elements.Add(tileItemElement7);
            this.itCongno.Id = 49;
            this.itCongno.IsLarge = true;
            this.itCongno.Name = "itCongno";
            this.itCongno.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongno_ItemClick);
            // 
            // itKaraoke
            // 
            this.itKaraoke.BackgroundImage = global::KP_RES.Properties.Resources.karaoke3;
            this.itKaraoke.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Báo cáo bán hàng Karaoke";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKaraoke.Elements.Add(tileItemElement8);
            this.itKaraoke.Id = 51;
            this.itKaraoke.IsLarge = true;
            this.itKaraoke.Name = "itKaraoke";
            this.itKaraoke.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKaraoke_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itNguoidung);
            this.tileGroup9.Items.Add(this.itBaocaodacthu);
            this.tileGroup9.Items.Add(this.itVoucher);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itNguoidung
            // 
            this.itNguoidung.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itNguoidung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Người dùng";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itNguoidung.Elements.Add(tileItemElement9);
            this.itNguoidung.Id = 52;
            this.itNguoidung.IsLarge = true;
            this.itNguoidung.Name = "itNguoidung";
            this.itNguoidung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNguoidung_ItemClick);
            // 
            // itBaocaodacthu
            // 
            this.itBaocaodacthu.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itBaocaodacthu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Báo cáo đặc thù";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaodacthu.Elements.Add(tileItemElement10);
            this.itBaocaodacthu.Id = 53;
            this.itBaocaodacthu.IsLarge = true;
            this.itBaocaodacthu.Name = "itBaocaodacthu";
            this.itBaocaodacthu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaodacthu_ItemClick);
            // 
            // itVoucher
            // 
            this.itVoucher.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itVoucher.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Voucher";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itVoucher.Elements.Add(tileItemElement11);
            this.itVoucher.Id = 55;
            this.itVoucher.IsLarge = true;
            this.itVoucher.Name = "itVoucher";
            this.itVoucher.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVoucher_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itCongvan);
            this.tileGroup10.Items.Add(this.itNhansu);
            this.tileGroup10.Items.Add(this.itDanhgiaKH);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itCongvan
            // 
            this.itCongvan.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCongvan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itCongvan.Checked = true;
            tileItemElement12.Text = "Công văn";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongvan.Elements.Add(tileItemElement12);
            this.itCongvan.Id = 54;
            this.itCongvan.IsLarge = true;
            this.itCongvan.Name = "itCongvan";
            this.itCongvan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // itNhansu
            // 
            this.itNhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Nhân sự";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhansu.Elements.Add(tileItemElement13);
            this.itNhansu.Id = 56;
            this.itNhansu.IsLarge = true;
            this.itNhansu.Name = "itNhansu";
            this.itNhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // itDanhgiaKH
            // 
            this.itDanhgiaKH.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itDanhgiaKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Đánh giá khách hàng";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itDanhgiaKH.Elements.Add(tileItemElement14);
            this.itDanhgiaKH.Id = 57;
            this.itDanhgiaKH.IsLarge = true;
            this.itDanhgiaKH.Name = "itDanhgiaKH";
            this.itDanhgiaKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDanhgiaKH_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itTheodoihopdong);
            this.tileGroup11.Items.Add(this.itMakhuyenmai);
            this.tileGroup11.Items.Add(this.itKhachsan);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itTheodoihopdong
            // 
            this.itTheodoihopdong.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itTheodoihopdong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Theo dõi hợp đồng";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itTheodoihopdong.Elements.Add(tileItemElement15);
            this.itTheodoihopdong.Id = 58;
            this.itTheodoihopdong.IsLarge = true;
            this.itTheodoihopdong.Name = "itTheodoihopdong";
            this.itTheodoihopdong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTheodoihopdong_ItemClick);
            // 
            // itMakhuyenmai
            // 
            this.itMakhuyenmai.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itMakhuyenmai.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Mã khuyến mãi";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itMakhuyenmai.Elements.Add(tileItemElement16);
            this.itMakhuyenmai.Id = 59;
            this.itMakhuyenmai.IsLarge = true;
            this.itMakhuyenmai.Name = "itMakhuyenmai";
            this.itMakhuyenmai.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMakhuyenmai_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement18.Text = "Tồn kho";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement18.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement18);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement19.Text = "Tồn kho";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement19.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement19);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // itKhachsan
            // 
            this.itKhachsan.BackgroundImage = global::KP_RES.Properties.Resources.khachsan28;
            this.itKhachsan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement17.Text = "Khách sạn";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachsan.Elements.Add(tileItemElement17);
            this.itKhachsan.Id = 60;
            this.itKhachsan.IsLarge = true;
            this.itKhachsan.Name = "itKhachsan";
            this.itKhachsan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachsan_ItemClick);
            // 
            // Frm_Baocao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao";
            this.Text = "Báo cáo";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itHanghoa;
        private DevExpress.XtraEditors.TileItem itNhapxuat;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itBanhang;
        private DevExpress.XtraEditors.TileItem itVe;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itKHTT;
        private DevExpress.XtraEditors.TileItem itCongno;
        private DevExpress.XtraEditors.TileItem itVeTroChoi;
        private DevExpress.XtraEditors.TileItem itKaraoke;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itNguoidung;
        private DevExpress.XtraEditors.TileItem itBaocaodacthu;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itCongvan;
        private DevExpress.XtraEditors.TileItem itVoucher;
        private DevExpress.XtraEditors.TileItem itNhansu;
        private DevExpress.XtraEditors.TileItem itDanhgiaKH;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itTheodoihopdong;
        private DevExpress.XtraEditors.TileItem itMakhuyenmai;
        private DevExpress.XtraEditors.TileItem itKhachsan;



    }
}