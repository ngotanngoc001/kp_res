﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_Caidat_KHTT : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Caidat_KHTT()
        {
            InitializeComponent();
            this.Text = rm.GetString("khachhangthanthiet", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_KHTT_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT.Checked)
            {
               // this.Parent.GoTo<NS_KH_KhachHangThanThiet>();
            }
        }

        private void itQDKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQDKHTT.Checked)
            {
               // this.Parent.GoTo<Frm_Cacquydinh>();
            }
        }

        private void itThethanhtoannoibo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThethanhtoannoibo.Checked)
            {
                this.Parent.GoTo<NS_KH_THANHTOANNOIBO>();
            }
        }

        private void itKHTT1_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT1.Checked)
            {
                this.Parent.GoTo<Frm_Khachhang>();
            }
        }

        private void itTTTNB1_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTTTNB1.Checked)
            {
                this.Parent.GoTo<Frm_Khachhang>();
            }
        }

        private void itQuydinhvediem_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuydinhvediem.Checked)
            {
                this.Parent.GoTo<Frm_Quydinhdiem>();
            }
        }

        private void itCapdothe_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCapdothe.Checked)
            {
                this.Parent.GoTo<Frm_Capdothe>();
            }
        }

        private void itSole_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSole.Checked)
            {
                this.Parent.GoTo<Frm_CauhinhSole>();
            }
        }

        private void itQuatang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuatang.Checked)
            {
                this.Parent.GoTo<Frm_QuaTang>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKHTT);
            cls_KP_RES.RemoveTileItem(itQDKHTT);
            cls_KP_RES.RemoveTileItem(itThethanhtoannoibo);
            cls_KP_RES.RemoveTileItem(itKHTT1);
            cls_KP_RES.RemoveTileItem(itTTTNB1);
            cls_KP_RES.RemoveTileItem(itQuydinhvediem);
            cls_KP_RES.RemoveTileItem(itCapdothe);
            cls_KP_RES.RemoveTileItem(itSole);
            cls_KP_RES.RemoveTileItem(itQuatang );
        }

        private void LoadPermission()
        {
            itKHTT1.Checked = (clsUserManagement.AllowView("52") && clsUserManagement.AllowView("53") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowView("58") && clsUserManagement.CheckActive("8"));
            itQuydinhvediem.Checked = clsUserManagement.AllowView("53") && clsUserManagement.CheckActive("4");
            itCapdothe.Checked = clsUserManagement.AllowView("53") && clsUserManagement.CheckActive("4");
            itQuatang.Checked = clsUserManagement.AllowView("53") && clsUserManagement.CheckActive("4");
          
            itSole.Checked = false;// clsUserManagement.AllowView("53") && clsUserManagement.CheckActive("4");
            itKHTT.Checked = false;
            itThethanhtoannoibo.Checked = false;
            itTTTNB1.Checked = false;
            itQDKHTT.Checked = false ;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            itKHTT1.Text = rm.GetString("khachhang", culture);
            itQuydinhvediem.Text = rm.GetString("quydinhdiem", culture);
            itQuatang.Text = rm.GetString("quatang", culture);
            itCapdothe.Text = rm.GetString("capdothe", culture);

        }
    }
}