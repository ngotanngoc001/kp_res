﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Caidat_Ve : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Ve()
        {
            InitializeComponent();

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Ve_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itRap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itRap.Checked)
            {
                this.Parent.GoTo<Frm_RapPhong>();
            }
        }

        private void itPhim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhim.Checked)
            {
                this.Parent.GoTo<Frm_Phim>();
            }
        }

        private void itBanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanggia.Checked)
            {
                this.Parent.GoTo<Frm_Lapbanggia>();
            }
        }

        private void itLaplichchieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLaplichchieu.Checked)
            {
                this.Parent.GoTo<Frm_Lichchieu>();
            }
        }

        private void itCaidatShowTime_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidatShowTime.Checked)
            {
                this.Parent.GoTo<Frm_ShowTime >();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itRap);
            cls_KP_RES.RemoveTileItem(itPhim);
            cls_KP_RES.RemoveTileItem(itBanggia);
            cls_KP_RES.RemoveTileItem(itLaplichchieu);
            cls_KP_RES.RemoveTileItem(itCaidatShowTime);
        }

        private void LoadPermission()
        {
            itRap.Checked = clsUserManagement.AllowView("68");
           // itNhaphathanh.Checked = clsUserManagement.AllowView("69");
           // itChatluongphim.Checked = clsUserManagement.AllowView("70");
          //  itTheloaiphim.Checked = clsUserManagement.AllowView("71");
            //itLoaive.Checked = clsUserManagement.AllowView("72");
            //itThoigian .Checked = clsUserManagement.AllowView("73");
            itPhim.Checked = clsUserManagement.AllowView("74");
            //itLoaighe.Checked = clsUserManagement.AllowView("76");
            //itSodoghe.Checked = clsUserManagement.AllowView("77");
            itBanggia.Checked = clsUserManagement.AllowView("78");
            itLaplichchieu.Checked = clsUserManagement.AllowView("79");
            itCaidatShowTime.Checked = clsUserManagement.AllowView("104");
        }
    }
}