﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Voucher_Main : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Voucher_Main()
        {
            InitializeComponent();

            LoadPermission();
            cls_KP_RES.RemoveTileItem(itPhathanhvoucher);
            cls_KP_RES.RemoveTileItem(itLoaivoucher);
        }

        private void Frm_Voucher_Main_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itNhapkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhathanhvoucher.Checked)
            {
                this.Parent.GoTo<Frm_Voucher>();
            }
        }

        private void itXuatkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaivoucher.Checked)
            {
                this.Parent.GoTo<Frm_VoucherStyle>();
            }
        }

        private void LoadPermission()
        {
            itPhathanhvoucher.Checked = clsUserManagement.AllowView("12082015");
            itLoaivoucher.Checked = clsUserManagement.AllowView("12082015");
        }
    }
}