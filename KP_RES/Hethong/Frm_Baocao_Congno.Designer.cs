﻿namespace KP_RES 
{
    partial class Frm_Baocao_Congno 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itDSPT = new DevExpress.XtraEditors.TileItem();
            this.itCongnoNCC = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itDSPC = new DevExpress.XtraEditors.TileItem();
            this.itCongnoKH = new DevExpress.XtraEditors.TileItem();
            this.itDenghichi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itQuytienmat = new DevExpress.XtraEditors.TileItem();
            this.itTongquancongno = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itBieudothuchi = new DevExpress.XtraEditors.TileItem();
            this.itTheodoicongno = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.itUynhiemchi = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 60;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itDSPT);
            this.tileGroup2.Items.Add(this.itCongnoNCC);
            this.tileGroup2.Items.Add(this.itUynhiemchi);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itDSPT
            // 
            this.itDSPT.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itDSPT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách phiếu thu";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDSPT.Elements.Add(tileItemElement1);
            this.itDSPT.Id = 49;
            this.itDSPT.IsLarge = true;
            this.itDSPT.Name = "itDSPT";
            this.itDSPT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDSPT_ItemClick);
            // 
            // itCongnoNCC
            // 
            this.itCongnoNCC.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itCongnoNCC.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Công nợ nhà cung cấp";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongnoNCC.Elements.Add(tileItemElement2);
            this.itCongnoNCC.Id = 52;
            this.itCongnoNCC.IsLarge = true;
            this.itCongnoNCC.Name = "itCongnoNCC";
            this.itCongnoNCC.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongnoNCC_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itDSPC);
            this.tileGroup8.Items.Add(this.itCongnoKH);
            this.tileGroup8.Items.Add(this.itDenghichi);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itDSPC
            // 
            this.itDSPC.BackgroundImage = global::KP_RES.Properties.Resources.xuatkho4;
            this.itDSPC.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Danh sách phiếu chi";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itDSPC.Elements.Add(tileItemElement4);
            this.itDSPC.Id = 50;
            this.itDSPC.IsLarge = true;
            this.itDSPC.Name = "itDSPC";
            this.itDSPC.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDSPC_ItemClick);
            // 
            // itCongnoKH
            // 
            this.itCongnoKH.BackgroundImage = global::KP_RES.Properties.Resources.dangky1;
            this.itCongnoKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Công nợ khách hàng";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongnoKH.Elements.Add(tileItemElement5);
            this.itCongnoKH.Id = 53;
            this.itCongnoKH.IsLarge = true;
            this.itCongnoKH.Name = "itCongnoKH";
            this.itCongnoKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongnoKH_ItemClick);
            // 
            // itDenghichi
            // 
            this.itDenghichi.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itDenghichi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Danh sách phiếu đề nghị chi";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itDenghichi.Elements.Add(tileItemElement6);
            this.itDenghichi.Id = 58;
            this.itDenghichi.IsLarge = true;
            this.itDenghichi.Name = "itDenghichi";
            this.itDenghichi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDenghichi_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itQuytienmat);
            this.tileGroup9.Items.Add(this.itTongquancongno);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itQuytienmat
            // 
            this.itQuytienmat.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itQuytienmat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Báo cáo quỹ tiền mặt";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuytienmat.Elements.Add(tileItemElement7);
            this.itQuytienmat.Id = 56;
            this.itQuytienmat.IsLarge = true;
            this.itQuytienmat.Name = "itQuytienmat";
            this.itQuytienmat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuytienmat_ItemClick);
            // 
            // itTongquancongno
            // 
            this.itTongquancongno.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itTongquancongno.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Tổng quan công nợ";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itTongquancongno.Elements.Add(tileItemElement8);
            this.itTongquancongno.Id = 54;
            this.itTongquancongno.IsLarge = true;
            this.itTongquancongno.Name = "itTongquancongno";
            this.itTongquancongno.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTongquancongno_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itBieudothuchi);
            this.tileGroup10.Items.Add(this.itTheodoicongno);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itBieudothuchi
            // 
            this.itBieudothuchi.BackgroundImage = global::KP_RES.Properties.Resources.baocao1;
            this.itBieudothuchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Thống kê thu chi";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itBieudothuchi.Elements.Add(tileItemElement9);
            this.itBieudothuchi.Id = 57;
            this.itBieudothuchi.IsLarge = true;
            this.itBieudothuchi.Name = "itBieudothuchi";
            this.itBieudothuchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBieudothuchi_ItemClick);
            // 
            // itTheodoicongno
            // 
            this.itTheodoicongno.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itTheodoicongno.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Theo dõi công nợ";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itTheodoicongno.Elements.Add(tileItemElement10);
            this.itTheodoicongno.Id = 55;
            this.itTheodoicongno.IsLarge = true;
            this.itTheodoicongno.Name = "itTheodoicongno";
            this.itTheodoicongno.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTheodoicongno_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Tồn kho";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement11);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Tồn kho";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement12);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // itUynhiemchi
            // 
            this.itUynhiemchi.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhbaocao;
            this.itUynhiemchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Danh sách phiếu ủy nhiệm chi";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itUynhiemchi.Elements.Add(tileItemElement3);
            this.itUynhiemchi.Id = 59;
            this.itUynhiemchi.IsLarge = true;
            this.itUynhiemchi.Name = "itUynhiemchi";
            this.itUynhiemchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itUynhiemchi_ItemClick);
            // 
            // Frm_Baocao_Congno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Congno";
            this.Text = "Báo cáo công nợ - thu chi";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Congno_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itDSPT;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itDSPC;
        private DevExpress.XtraEditors.TileItem itQuytienmat;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itBieudothuchi;
        private DevExpress.XtraEditors.TileItem itCongnoNCC;
        private DevExpress.XtraEditors.TileItem itCongnoKH;
        private DevExpress.XtraEditors.TileItem itTongquancongno;
        private DevExpress.XtraEditors.TileItem itTheodoicongno;
        private DevExpress.XtraEditors.TileItem itDenghichi;
        private DevExpress.XtraEditors.TileItem itUynhiemchi;



    }
}