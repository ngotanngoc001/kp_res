﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Kho : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Kho()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Kho_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCuaHangKhoQuay_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCuaHangKhoQuay.Checked)
            {
                this.Parent.GoTo<Frm_CHKQ>();
            }
        }

        private void itNhomHangHangHoa_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhomHangHangHoa.Checked)
            {
                this.Parent.GoTo<Frm_NhomHanghoa>();
            }
        }

        private void itNhaCungCapKhachHang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhaCungCapKhachHang.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_NCCKH>();
            }
        }

        private void itDonViTinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDonViTinh.Checked)
            {
                this.Parent.GoTo<Frm_Donvitinh>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCuaHangKhoQuay);
            cls_KP_RES.RemoveTileItem(itNhomHangHangHoa);
            cls_KP_RES.RemoveTileItem(itNhaCungCapKhachHang);
            cls_KP_RES.RemoveTileItem(itDonViTinh);
        }

        private void LoadPermission()
        {
            itCuaHangKhoQuay.Checked = true;
            itNhomHangHangHoa.Checked = true;
            itNhaCungCapKhachHang.Checked = true;
            itDonViTinh.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       
    }
}