﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Voucher : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Voucher()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void itBCVoucher_Chitiet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBCVoucher_Chitiet.Checked)
            {
                this.Parent.GoTo<Frm_BC_Voucher>();
            }
        }

        private void itBCVoucher_Tonghop_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBCVoucher_Tonghop.Checked)
            {
                this.Parent.GoTo<Frm_BC_TongHopVoucher>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBCVoucher_Chitiet);
            cls_KP_RES.RemoveTileItem(itBCVoucher_Tonghop);
           
        }


        private void LoadPermission()
        {
            itBCVoucher_Chitiet.Checked = clsUserManagement.AllowView("1208201501");
            itBCVoucher_Tonghop.Checked = clsUserManagement.AllowView("1208201501");
        }



        private void Frm_Baocao_Voucher_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
           
        }

     
    }
}