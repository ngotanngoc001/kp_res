﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_QuanLyCongTy : DevExpress.XtraEditors.XtraForm
    {
        public Frm_QuanLyCongTy()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Nghiệp vụ";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_QuanLyCongTy_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itLichLamViec_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLichLamViec.Checked)
            {
                this.Parent.GoTo<Frm_LichLamViec>();
            }
        }

        private void itTuyendung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTuyendung.Checked)
            {
                this.Parent.GoTo<Frm_Tuyendung>();
            }
        }

        private void itTienluong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTienluong.Checked)
            {
                this.Parent.GoTo<Frm_Luong>();
            }
        }

        private void itNhanvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhanvien.Checked)
            {
                this.Parent.GoTo<Frm_NhanVien>();
            }
        }

        private void itHopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHopdong.Checked)
            {
                this.Parent.GoTo<Frm_HopDongLaoDong>();
            }
        }

        private void itKhenthuong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhenthuong.Checked)
            {
                this.Parent.GoTo<Frm_KhenThuongKyLuat>();
            }
        }

        private void VisibleTileItem()
        {
          
            cls_KP_RES.RemoveTileItem(itLichLamViec);
            cls_KP_RES.RemoveTileItem(itTuyendung);
            cls_KP_RES.RemoveTileItem(itNhanvien);
            cls_KP_RES.RemoveTileItem(itHopdong);
            cls_KP_RES.RemoveTileItem(itKhenthuong);
            cls_KP_RES.RemoveTileItem(itTienluong);
        }

        private void LoadPermission()
        {
            itLichLamViec.Checked = clsUserManagement.AllowView("6051603");
            itTuyendung.Checked = clsUserManagement.AllowView("5051601") || clsUserManagement.AllowView("5051602");
            itNhanvien.Checked = clsUserManagement.AllowView("6051601");
            itHopdong.Checked = clsUserManagement.AllowView("7051601");
            itKhenthuong.Checked = clsUserManagement.AllowView("7051602");
            itTienluong.Checked = clsUserManagement.AllowView("7051603") || clsUserManagement.AllowView("7051604") || clsUserManagement.AllowView("7051605") || clsUserManagement.AllowView("7051606") || clsUserManagement.AllowView("7051607");
        }


       

       

        

     
        

        

        
    }
}