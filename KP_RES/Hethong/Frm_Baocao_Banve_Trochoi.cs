﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Banve_Trochoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Banve_Trochoi()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Banve_Trochoi_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itTrochoi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrochoi.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banvetrochoi>();
            }
        }

        private void itDanhsachveban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDanhsachveban.Checked)
            {
                this.Parent.GoTo<Frm_BC_Veban>();
            }
        }

        private void itDanhsachvehuy_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDanhsachvehuy.Checked)
            {
                this.Parent.GoTo<Frm_BC_Vehuy>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itTrochoi);
            cls_KP_RES.RemoveTileItem(itDanhsachveban);
            cls_KP_RES.RemoveTileItem(itDanhsachvehuy);
        }

        private void LoadPermission()
        {
            itTrochoi.Checked = clsUserManagement.AllowView("64");
            itDanhsachveban.Checked = clsUserManagement.AllowView("65");
            itDanhsachvehuy.Checked = clsUserManagement.AllowView("66");
        }
    }
}