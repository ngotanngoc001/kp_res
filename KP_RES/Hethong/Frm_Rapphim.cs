﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Rapphim : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Rapphim()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Rapphim_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBanverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanverapphim.Checked)
            {
                //check ban hang
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //Gọi form quảng cáo
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Ve frmqc = new Frm_Manhinhhai_Ve();
                    Screen[] sc = Screen.AllScreens;
                    frmqc.Width = sc[1].Bounds.Width;
                    frmqc.Height = sc[1].Bounds.Height;
                    frmqc.Location = new Point(this.Width, 0);
                    frmqc.Show();
                }
                //Vô bán hàng
                Frm_RPBanve frm = new Frm_RPBanve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itDatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDatverapphim.Checked)
            {
                Frm_RPDatve frm = new Frm_RPDatve();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itSoatverapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoatverapphim.Checked)
            {
                cls_ConfigCashier.LoadCauHinh();
                Frm_InveMuaTruoc frm = new Frm_InveMuaTruoc();
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void itHoahong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHoahong.Checked)
            {
                this.Parent.GoTo<Frm_HoaHong>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBanverapphim);
            cls_KP_RES.RemoveTileItem(itDatverapphim);
            cls_KP_RES.RemoveTileItem(itSoatverapphim);
            cls_KP_RES.RemoveTileItem(itHoahong);
        }

        private void LoadPermission()
        {
            itBanverapphim.Checked = clsUserManagement.AllowView("67");
            itDatverapphim.Checked = clsUserManagement.AllowView("80");
            itSoatverapphim.Checked = clsUserManagement.AllowView("67");//99 soat ve

            if (cls_KP_RES.Mode == 2)
            {
                itHoahong.Checked = true;
            }
        }

        
    }
}