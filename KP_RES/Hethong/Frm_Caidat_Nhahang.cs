﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Nhahang : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Nhahang()
        {
            InitializeComponent();
            this.Text = rm.GetString("caidatnhahang", culture);
            tileControl1.Text = rm.GetString("caidatnhahang", culture);
            itKhuvucban.ItemClick += new TileItemClickEventHandler(itKhuvucban_ItemClick);
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Nhahang_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKhuvucban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhuvucban.Checked)
            {
                this.Parent.GoTo<Frm_KhuvucBan>();
            }
        }

        private void itApdungbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia.Checked)
            {
                this.Parent.GoTo<Frm_Apdungbanggia>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKhuvucban);
            cls_KP_RES.RemoveTileItem(itApdungbanggia);
        }

        private void LoadPermission()
        {
            itKhuvucban.Checked = clsUserManagement.AllowView("5");
            itApdungbanggia.Checked = clsUserManagement.AllowView("42");
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            itKhuvucban.Text = rm.GetString("khuvucban", culture);
            itApdungbanggia.Text = rm.GetString("apdungbanggia", culture);
            this.Text = rm.GetString("caidatnhahang", culture);
            tileControl1.Text = rm.GetString("caidatnhahang", culture);
        }

    }
}