﻿namespace KP_RES 
{
    partial class Frm_Caidat_Khachsan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itLoaiphong = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itKhachsanlauphong = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itCaidatchung = new DevExpress.XtraEditors.TileItem();
            this.itLoaiphongbanggia = new DevExpress.XtraEditors.TileItem();
            this.itApdungbanggia = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itApdungbanggia_Hanghoa = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup14);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 56;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 30;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 713);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itLoaiphong);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itLoaiphong
            // 
            this.itLoaiphong.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itLoaiphong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Loại phòng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itLoaiphong.Elements.Add(tileItemElement1);
            this.itLoaiphong.Id = 54;
            this.itLoaiphong.IsLarge = true;
            this.itLoaiphong.Name = "itLoaiphong";
            this.itLoaiphong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLoaiphong_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itKhachsanlauphong);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itKhachsanlauphong
            // 
            this.itKhachsanlauphong.BackgroundImage = global::KP_RES.Properties.Resources.khuvucban1;
            this.itKhachsanlauphong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Lầu - Phòng";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachsanlauphong.Elements.Add(tileItemElement2);
            this.itKhachsanlauphong.Id = 15;
            this.itKhachsanlauphong.IsLarge = true;
            this.itKhachsanlauphong.Name = "itKhachsanlauphong";
            this.itKhachsanlauphong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachsanlauphong_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itCaidatchung);
            this.tileGroup13.Items.Add(this.itLoaiphongbanggia);
            this.tileGroup13.Items.Add(this.itApdungbanggia);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itCaidatchung
            // 
            this.itCaidatchung.BackgroundImage = global::KP_RES.Properties.Resources.thoigian;
            this.itCaidatchung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cài đặt thời gian";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidatchung.Elements.Add(tileItemElement3);
            this.itCaidatchung.Id = 53;
            this.itCaidatchung.IsLarge = true;
            this.itCaidatchung.Name = "itCaidatchung";
            this.itCaidatchung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidatchung_ItemClick);
            // 
            // itLoaiphongbanggia
            // 
            this.itLoaiphongbanggia.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itLoaiphongbanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Lập bảng giá";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itLoaiphongbanggia.Elements.Add(tileItemElement4);
            this.itLoaiphongbanggia.Id = 50;
            this.itLoaiphongbanggia.IsLarge = true;
            this.itLoaiphongbanggia.Name = "itLoaiphongbanggia";
            this.itLoaiphongbanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLoaiphongbanggia_ItemClick);
            // 
            // itApdungbanggia
            // 
            this.itApdungbanggia.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhcongnothuchi;
            this.itApdungbanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Áp dụng bảng giá";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itApdungbanggia.Elements.Add(tileItemElement5);
            this.itApdungbanggia.Id = 52;
            this.itApdungbanggia.IsLarge = true;
            this.itApdungbanggia.Name = "itApdungbanggia";
            this.itApdungbanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itApdungbanggia_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 717);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup14
            // 
            this.tileGroup14.Items.Add(this.itApdungbanggia_Hanghoa);
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itApdungbanggia_Hanghoa
            // 
            this.itApdungbanggia_Hanghoa.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itApdungbanggia_Hanghoa.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Áp dụng bảng giá hàng hóa";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itApdungbanggia_Hanghoa.Elements.Add(tileItemElement6);
            this.itApdungbanggia_Hanghoa.Id = 55;
            this.itApdungbanggia_Hanghoa.IsLarge = true;
            this.itApdungbanggia_Hanghoa.Name = "itApdungbanggia_Hanghoa";
            this.itApdungbanggia_Hanghoa.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itApdungbanggia_Hanghoa_ItemClick);
            // 
            // Frm_Caidat_Khachsan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 717);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Khachsan";
            this.Text = "Cài đặt khách sạn";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Khachsan_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itLoaiphong;
        private DevExpress.XtraEditors.TileItem itKhachsanlauphong;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itCaidatchung;
        private DevExpress.XtraEditors.TileItem itLoaiphongbanggia;
        private DevExpress.XtraEditors.TileItem itApdungbanggia;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itApdungbanggia_Hanghoa;



    }
}