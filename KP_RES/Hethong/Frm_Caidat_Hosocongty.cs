﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Hosocongty : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Hosocongty()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Cài đặt";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Hosocongty_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itLoaihoso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaihoso.Checked)
            {
                this.Parent.GoTo<Frm_LoaiHoSo>();
            }
        }

        private void itHophoso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHophoso.Checked)
            {
                this.Parent.GoTo<Frm_KhoKeHop_HoSo>();
            }
        }

        private void itCoquancap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCoquancap.Checked)
            {
                this.Parent.GoTo<Frm_CoQuanBanHanh>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itLoaihoso);
            cls_KP_RES.RemoveTileItem(itHophoso);
            cls_KP_RES.RemoveTileItem(itCoquancap);
        }

        private void LoadPermission()
        {
            itLoaihoso.Checked = true;
            itHophoso.Checked = true;
            itCoquancap.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

      

        

        

        

    }
}