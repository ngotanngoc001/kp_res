﻿namespace KP_RES 
{
    partial class Frm_Baocao_Banhang 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoDSHD = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoDSHD_HUY = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoDSHD_INLAI = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoBanhang = new DevExpress.XtraEditors.TileItem();
            this.itBc_BH_Combo = new DevExpress.XtraEditors.TileItem();
            this.itBc_CTBH_Combo = new DevExpress.XtraEditors.TileItem();
            this.itBaocao_banhang_online = new DevExpress.XtraEditors.TileItem();
            this.itBC_Banhang_Nhanvien = new DevExpress.XtraEditors.TileItem();
            this.itBC_Banhang_Khachhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoDoanhso = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoKQKD = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itThongkedoanhso = new DevExpress.XtraEditors.TileItem();
            this.itThongkebanhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itSoluongkhach = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itHuyban = new DevExpress.XtraEditors.TileItem();
            this.itHuymon = new DevExpress.XtraEditors.TileItem();
            this.itLaylaihoadon = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itDSHD_Trahang = new DevExpress.XtraEditors.TileItem();
            this.itBaocaotrahang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoDSHD_Chitiet = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 61;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itBaocaoDSHD);
            this.tileGroup7.Items.Add(this.itBaocaoDSHD_HUY);
            this.tileGroup7.Items.Add(this.itBaocaoDSHD_INLAI);
            this.tileGroup7.Items.Add(this.itBaocaoDSHD_Chitiet);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itBaocaoDSHD
            // 
            this.itBaocaoDSHD.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itBaocaoDSHD.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách hóa đơn";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD.Elements.Add(tileItemElement1);
            this.itBaocaoDSHD.Id = 21;
            this.itBaocaoDSHD.IsLarge = true;
            this.itBaocaoDSHD.Name = "itBaocaoDSHD";
            this.itBaocaoDSHD.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_ItemClick);
            // 
            // itBaocaoDSHD_HUY
            // 
            this.itBaocaoDSHD_HUY.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_Huy;
            this.itBaocaoDSHD_HUY.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Danh sách hóa đơn đã hủy";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD_HUY.Elements.Add(tileItemElement2);
            this.itBaocaoDSHD_HUY.Id = 48;
            this.itBaocaoDSHD_HUY.IsLarge = true;
            this.itBaocaoDSHD_HUY.Name = "itBaocaoDSHD_HUY";
            this.itBaocaoDSHD_HUY.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_HUY_ItemClick);
            // 
            // itBaocaoDSHD_INLAI
            // 
            this.itBaocaoDSHD_INLAI.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itBaocaoDSHD_INLAI.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Danh sách hóa đơn in lại";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD_INLAI.Elements.Add(tileItemElement3);
            this.itBaocaoDSHD_INLAI.Id = 49;
            this.itBaocaoDSHD_INLAI.IsLarge = true;
            this.itBaocaoDSHD_INLAI.Name = "itBaocaoDSHD_INLAI";
            this.itBaocaoDSHD_INLAI.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_INLAI_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itBaocaoBanhang);
            this.tileGroup8.Items.Add(this.itBc_BH_Combo);
            this.tileGroup8.Items.Add(this.itBc_CTBH_Combo);
            this.tileGroup8.Items.Add(this.itBaocao_banhang_online);
            this.tileGroup8.Items.Add(this.itBC_Banhang_Nhanvien);
            this.tileGroup8.Items.Add(this.itBC_Banhang_Khachhang);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itBaocaoBanhang
            // 
            this.itBaocaoBanhang.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itBaocaoBanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Báo cáo bán hàng";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoBanhang.Elements.Add(tileItemElement5);
            this.itBaocaoBanhang.Id = 22;
            this.itBaocaoBanhang.IsLarge = true;
            this.itBaocaoBanhang.Name = "itBaocaoBanhang";
            this.itBaocaoBanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoBanhang_ItemClick);
            // 
            // itBc_BH_Combo
            // 
            this.itBc_BH_Combo.BackgroundImage = global::KP_RES.Properties.Resources.danhsachkhct;
            this.itBc_BH_Combo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Báo cáo bán hàng combo";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itBc_BH_Combo.Elements.Add(tileItemElement6);
            this.itBc_BH_Combo.Id = 47;
            this.itBc_BH_Combo.IsLarge = true;
            this.itBc_BH_Combo.Name = "itBc_BH_Combo";
            this.itBc_BH_Combo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBc_BH_Combo_ItemClick);
            // 
            // itBc_CTBH_Combo
            // 
            this.itBc_CTBH_Combo.BackgroundImage = global::KP_RES.Properties.Resources.danhsachkhct;
            this.itBc_CTBH_Combo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Báo cáo chi tiết bán hàng combo";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itBc_CTBH_Combo.Elements.Add(tileItemElement7);
            this.itBc_CTBH_Combo.Id = 50;
            this.itBc_CTBH_Combo.IsLarge = true;
            this.itBc_CTBH_Combo.Name = "itBc_CTBH_Combo";
            this.itBc_CTBH_Combo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBc_CTBH_Combo_ItemClick);
            // 
            // itBaocao_banhang_online
            // 
            this.itBaocao_banhang_online.BackgroundImage = global::KP_RES.Properties.Resources.hangbanonline;
            this.itBaocao_banhang_online.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itBaocao_banhang_online.Checked = true;
            tileItemElement8.Text = "Báo cáo bán hàng Online";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocao_banhang_online.Elements.Add(tileItemElement8);
            this.itBaocao_banhang_online.Id = 51;
            this.itBaocao_banhang_online.IsLarge = true;
            this.itBaocao_banhang_online.Name = "itBaocao_banhang_online";
            this.itBaocao_banhang_online.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocao_banhang_online_ItemClick);
            // 
            // itBC_Banhang_Nhanvien
            // 
            this.itBC_Banhang_Nhanvien.BackgroundImage = global::KP_RES.Properties.Resources.danhsachkhct;
            this.itBC_Banhang_Nhanvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Báo cáo dịch vụ - nhân viên thực hiện";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itBC_Banhang_Nhanvien.Elements.Add(tileItemElement9);
            this.itBC_Banhang_Nhanvien.Id = 55;
            this.itBC_Banhang_Nhanvien.IsLarge = true;
            this.itBC_Banhang_Nhanvien.Name = "itBC_Banhang_Nhanvien";
            this.itBC_Banhang_Nhanvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBC_Banhang_Nhanvien_ItemClick);
            // 
            // itBC_Banhang_Khachhang
            // 
            this.itBC_Banhang_Khachhang.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itBC_Banhang_Khachhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Báo cáo dịch vụ - khách hàng";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itBC_Banhang_Khachhang.Elements.Add(tileItemElement10);
            this.itBC_Banhang_Khachhang.Id = 58;
            this.itBC_Banhang_Khachhang.IsLarge = true;
            this.itBC_Banhang_Khachhang.Name = "itBC_Banhang_Khachhang";
            this.itBC_Banhang_Khachhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBC_Banhang_Khachhang_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBaocaoDoanhso);
            this.tileGroup2.Items.Add(this.itBaocaoKQKD);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBaocaoDoanhso
            // 
            this.itBaocaoDoanhso.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itBaocaoDoanhso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Báo cáo doanh số";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDoanhso.Elements.Add(tileItemElement11);
            this.itBaocaoDoanhso.Id = 23;
            this.itBaocaoDoanhso.IsLarge = true;
            this.itBaocaoDoanhso.Name = "itBaocaoDoanhso";
            this.itBaocaoDoanhso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDoanhso_ItemClick);
            // 
            // itBaocaoKQKD
            // 
            this.itBaocaoKQKD.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itBaocaoKQKD.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Kết quả kinh doanh";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoKQKD.Elements.Add(tileItemElement12);
            this.itBaocaoKQKD.Id = 24;
            this.itBaocaoKQKD.IsLarge = true;
            this.itBaocaoKQKD.Name = "itBaocaoKQKD";
            this.itBaocaoKQKD.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoKQKD_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itThongkedoanhso);
            this.tileGroup9.Items.Add(this.itThongkebanhang);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itThongkedoanhso
            // 
            this.itThongkedoanhso.BackgroundImage = global::KP_RES.Properties.Resources.baocao1;
            this.itThongkedoanhso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Thống kê doanh số";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongkedoanhso.Elements.Add(tileItemElement13);
            this.itThongkedoanhso.Id = 44;
            this.itThongkedoanhso.IsLarge = true;
            this.itThongkedoanhso.Name = "itThongkedoanhso";
            this.itThongkedoanhso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongkedoanhso_ItemClick);
            // 
            // itThongkebanhang
            // 
            this.itThongkebanhang.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            this.itThongkebanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Thống kê bán hàng";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongkebanhang.Elements.Add(tileItemElement14);
            this.itThongkebanhang.Id = 45;
            this.itThongkebanhang.IsLarge = true;
            this.itThongkebanhang.Name = "itThongkebanhang";
            this.itThongkebanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongkebanhang_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itSoluongkhach);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itSoluongkhach
            // 
            this.itSoluongkhach.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itSoluongkhach.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Số lượng khách";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itSoluongkhach.Elements.Add(tileItemElement15);
            this.itSoluongkhach.Id = 52;
            this.itSoluongkhach.IsLarge = true;
            this.itSoluongkhach.Name = "itSoluongkhach";
            this.itSoluongkhach.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSoluongkhach_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itHuyban);
            this.tileGroup11.Items.Add(this.itHuymon);
            this.tileGroup11.Items.Add(this.itLaylaihoadon);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itHuyban
            // 
            this.itHuyban.BackgroundImage = global::KP_RES.Properties.Resources.saoluu;
            this.itHuyban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Hủy bàn";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuyban.Elements.Add(tileItemElement16);
            this.itHuyban.Id = 53;
            this.itHuyban.IsLarge = true;
            this.itHuyban.Name = "itHuyban";
            this.itHuyban.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuyban_ItemClick);
            // 
            // itHuymon
            // 
            this.itHuymon.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itHuymon.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement17.Text = "Hủy món";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuymon.Elements.Add(tileItemElement17);
            this.itHuymon.Id = 54;
            this.itHuymon.IsLarge = true;
            this.itHuymon.Name = "itHuymon";
            this.itHuymon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuymon_ItemClick);
            // 
            // itLaylaihoadon
            // 
            this.itLaylaihoadon.BackgroundImage = global::KP_RES.Properties.Resources.saoluu1;
            this.itLaylaihoadon.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement18.Text = "Lấy lại hóa đơn";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement18.TextLocation = new System.Drawing.Point(0, 0);
            this.itLaylaihoadon.Elements.Add(tileItemElement18);
            this.itLaylaihoadon.Id = 59;
            this.itLaylaihoadon.IsLarge = true;
            this.itLaylaihoadon.Name = "itLaylaihoadon";
            this.itLaylaihoadon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLaylaihoadon_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itDSHD_Trahang);
            this.tileGroup12.Items.Add(this.itBaocaotrahang);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itDSHD_Trahang
            // 
            this.itDSHD_Trahang.BackgroundImage = global::KP_RES.Properties.Resources.saoluu;
            this.itDSHD_Trahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement19.Text = "Danh sách hóa đơn trả hàng";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement19.TextLocation = new System.Drawing.Point(0, 0);
            this.itDSHD_Trahang.Elements.Add(tileItemElement19);
            this.itDSHD_Trahang.Id = 56;
            this.itDSHD_Trahang.IsLarge = true;
            this.itDSHD_Trahang.Name = "itDSHD_Trahang";
            this.itDSHD_Trahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDSHD_Trahang_ItemClick);
            // 
            // itBaocaotrahang
            // 
            this.itBaocaotrahang.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itBaocaotrahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement20.Text = "Báo cáo trả hàng";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement20.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaotrahang.Elements.Add(tileItemElement20);
            this.itBaocaotrahang.Id = 57;
            this.itBaocaotrahang.IsLarge = true;
            this.itBaocaotrahang.Name = "itBaocaotrahang";
            this.itBaocaotrahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaotrahang_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement21.Text = "Tồn kho";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement21.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement21);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement22.Text = "Tồn kho";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement22.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement22);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // itBaocaoDSHD_Chitiet
            // 
            this.itBaocaoDSHD_Chitiet.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itBaocaoDSHD_Chitiet.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Danh sách hóa đơn chi tiết";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHD_Chitiet.Elements.Add(tileItemElement4);
            this.itBaocaoDSHD_Chitiet.Id = 60;
            this.itBaocaoDSHD_Chitiet.IsLarge = true;
            this.itBaocaoDSHD_Chitiet.Name = "itBaocaoDSHD_Chitiet";
            this.itBaocaoDSHD_Chitiet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHD_Chitiet_ItemClick);
            // 
            // Frm_Baocao_Banhang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Banhang";
            this.Text = "Báo cáo bán hàng";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Banhang_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD_HUY;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD_INLAI;
        private DevExpress.XtraEditors.TileItem itBaocaoBanhang;
        private DevExpress.XtraEditors.TileItem itBc_BH_Combo;
        private DevExpress.XtraEditors.TileItem itBaocaoKQKD;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itThongkebanhang;
        private DevExpress.XtraEditors.TileItem itBc_CTBH_Combo;
        private DevExpress.XtraEditors.TileItem itThongkedoanhso;
        private DevExpress.XtraEditors.TileItem itBaocao_banhang_online;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itSoluongkhach;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itHuyban;
        private DevExpress.XtraEditors.TileItem itHuymon;
        private DevExpress.XtraEditors.TileItem itBC_Banhang_Nhanvien;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itDSHD_Trahang;
        private DevExpress.XtraEditors.TileItem itBaocaotrahang;
        private DevExpress.XtraEditors.TileItem itBC_Banhang_Khachhang;
        private DevExpress.XtraEditors.TileItem itLaylaihoadon;
        private DevExpress.XtraEditors.TileItem itBaocaoDoanhso;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHD_Chitiet;



    }
}