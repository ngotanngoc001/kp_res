﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Nhansu : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Nhansu()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Cài đặt";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Nhansu_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itPhongban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongban.Checked)
            {
                this.Parent.GoTo<Frm_PhongBan>();
            }
       
        }

        private void itTrangthainhanvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrangthainhanvien.Checked)
            {
                this.Parent.GoTo<Frm_TrangThaiNhanVien>();
            }
        }

        private void itPhucap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhucap.Checked)
            {
                this.Parent.GoTo<Frm_PhuCap>();
            }
        }

        private void itChucvu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itChucvu.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.Frm_Chucvu4>();
            }
        }

        private void itBaohiem_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaohiem.Checked)
            {
                this.Parent.GoTo<Frm_Baohiem>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itPhongban);
            cls_KP_RES.RemoveTileItem(itTrangthainhanvien);
            cls_KP_RES.RemoveTileItem(itPhucap);
            cls_KP_RES.RemoveTileItem(itChucvu);
            cls_KP_RES.RemoveTileItem(itBaohiem);
        }

        private void LoadPermission()
        {
            itPhongban.Checked = true;
            itTrangthainhanvien.Checked = true;
            itPhucap.Checked = true ;
            itChucvu.Checked = true ;
            itBaohiem.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       

        

       
      

        
        

        

        

    }
}