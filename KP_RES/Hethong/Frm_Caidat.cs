﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat()
        {
            InitializeComponent();
            tileControl1_Paint(null, null);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCaidatchung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidatchung.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Chung>();
            }
        }

        private void itNhahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhahang.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Nhahang>();
            }
        }

        private void itKhachangthanthiet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhachangthanthiet.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_KHTT>();
            }
        }

        private void itVe_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVe.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Ve>();
            }
        }

        private void itVetrochoi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVetrochoi.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Ve_Trochoi>();
            }
        }

        private void itKhuyenmai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhuyenmai.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Khuyenmai>();
            }
        }

        private void itKaraoke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKaraoke.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Karaoke>();
            }
        }

        private void itKhachsan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhachsan.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Khachsan>();
            }
        }

        private void itVoucher_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVoucher.Checked)
            {
                this.Parent.GoTo<Frm_Voucher_Main>();
            }
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_CongVan>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhansu.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Nhansu>();
            }
        }

        private void itDanhgiaKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDanhgiaKH.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_DanhGia>();
            }
        }

        private void itTheodoihopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTheodoihopdong.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Sotheodoi>();
            }
        }

        private void itMakhuyenmai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMakhuyenmai.Checked)
            {
                this.Parent.GoTo<Frm_TheKhuyenMai>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCaidatchung);
            cls_KP_RES.RemoveTileItem(itNhahang);
            cls_KP_RES.RemoveTileItem(itKhachangthanthiet);
            cls_KP_RES.RemoveTileItem(itVe);
            cls_KP_RES.RemoveTileItem(itVetrochoi);
            cls_KP_RES.RemoveTileItem(itKhuyenmai);
            cls_KP_RES.RemoveTileItem(itKaraoke);
            cls_KP_RES.RemoveTileItem(itKhachsan);
            cls_KP_RES.RemoveTileItem(itVoucher);
            cls_KP_RES.RemoveTileItem(itCongvan );
            cls_KP_RES.RemoveTileItem(itNhansu);
            cls_KP_RES.RemoveTileItem(itDanhgiaKH);
            cls_KP_RES.RemoveTileItem(itTheodoihopdong);
            cls_KP_RES.RemoveTileItem(itMakhuyenmai);
        }

        private void LoadPermission()
        {
            itNhansu.Checked = clsUserManagement.AllowView("6051602") && clsUserManagement.CheckActive("17");
            itCongvan.Checked = clsUserManagement.AllowView("24031603") && clsUserManagement.CheckActive("16");
            itCaidatchung.Checked = clsUserManagement.AllowView("40") || clsUserManagement.AllowView("41") || clsUserManagement.AllowView("4") || clsUserManagement.AllowView("6") || clsUserManagement.AllowView("7") || clsUserManagement.AllowView("8") || clsUserManagement.AllowView("9") || clsUserManagement.AllowView("18") || clsUserManagement.AllowView("33") || clsUserManagement.AllowView("100");
            itNhahang.Checked = (clsUserManagement.AllowView("5") || clsUserManagement.AllowView("42")) && (clsUserManagement.CheckActive("5") ) ;
            itKhachangthanthiet.Checked = (clsUserManagement.AllowView("53") || clsUserManagement.AllowView("58")) && (clsUserManagement.CheckActive("4") || clsUserManagement.CheckActive("8"));
            itVe.Checked = (clsUserManagement.AllowView("68") || clsUserManagement.AllowView("69") || clsUserManagement.AllowView("70") || clsUserManagement.AllowView("71") || clsUserManagement.AllowView("72") || clsUserManagement.AllowView("73") || clsUserManagement.AllowView("74") || clsUserManagement.AllowView("75") || clsUserManagement.AllowView("76") || clsUserManagement.AllowView("77") || clsUserManagement.AllowView("78") || clsUserManagement.AllowView("79")) && (clsUserManagement.CheckActive("10"));
            itVetrochoi.Checked = (clsUserManagement.AllowView("60") && clsUserManagement.CheckActive("9"));
            itKhuyenmai.Checked = (clsUserManagement.AllowView("60") || clsUserManagement.AllowView("68") || clsUserManagement.AllowView("69") || clsUserManagement.AllowView("70") || clsUserManagement.AllowView("71") || clsUserManagement.AllowView("72") || clsUserManagement.AllowView("73") || clsUserManagement.AllowView("74") || clsUserManagement.AllowView("75") || clsUserManagement.AllowView("76") || clsUserManagement.AllowView("77") || clsUserManagement.AllowView("78") || clsUserManagement.AllowView("79")) && ( clsUserManagement.CheckActive("11"));
            itKaraoke.Checked = (clsUserManagement.AllowView("5") || clsUserManagement.AllowView("42") || clsUserManagement.AllowView("89")) && (clsUserManagement.CheckActive("6"));
            itKhachsan.Checked = (clsUserManagement.AllowView("0906201501") || clsUserManagement.AllowView("0906201502")) && (clsUserManagement.CheckActive("7"));
            itVoucher.Checked = ((clsUserManagement.AllowView("12082015") ) && clsUserManagement.CheckActive("13"));
            itDanhgiaKH.Checked = (clsUserManagement.AllowView("22071601") && clsUserManagement.CheckActive("220716"));
            itTheodoihopdong.Checked = (clsUserManagement.AllowView("30081601") && clsUserManagement.CheckActive("300816")); ;
            itMakhuyenmai.Checked = cls_KP_RES .Mode ==2 ? true  : false ; 
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            itCaidatchung.Text = rm.GetString("caidatchung", culture);
            itNhahang.Text = rm.GetString("nhahang", culture);
            this.Text = rm.GetString("caidat", culture);
            tileControl1 .Text =  rm.GetString("caidat", culture);
            itKhachangthanthiet.Text = rm.GetString("khtt", culture);
        }

    

      

    }
}