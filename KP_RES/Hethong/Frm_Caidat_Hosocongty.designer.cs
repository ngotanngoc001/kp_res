﻿namespace KP_RES 
{
    partial class Frm_Caidat_Hosocongty 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itHophoso = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itLoaihoso = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itCoquancap = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 54;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itHophoso);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itHophoso
            // 
            this.itHophoso.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itHophoso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Kho - Kệ - Hộp hồ sơ";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itHophoso.Elements.Add(tileItemElement1);
            this.itHophoso.Id = 51;
            this.itHophoso.IsLarge = true;
            this.itHophoso.Name = "itHophoso";
            this.itHophoso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHophoso_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itLoaihoso);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itLoaihoso
            // 
            this.itLoaihoso.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itLoaihoso.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Loại hồ sơ";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itLoaihoso.Elements.Add(tileItemElement2);
            this.itLoaihoso.Id = 50;
            this.itLoaihoso.IsLarge = true;
            this.itLoaihoso.Name = "itLoaihoso";
            this.itLoaihoso.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLoaihoso_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itCoquancap);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itCoquancap
            // 
            this.itCoquancap.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itCoquancap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cơ quan cấp";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCoquancap.Elements.Add(tileItemElement3);
            this.itCoquancap.Id = 53;
            this.itCoquancap.IsLarge = true;
            this.itCoquancap.Name = "itCoquancap";
            this.itCoquancap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCoquancap_ItemClick);
            // 
            // Frm_Caidat_Hosocongty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Hosocongty";
            this.Text = "Hồ sơ công ty";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itLoaihoso;
        private DevExpress.XtraEditors.TileItem itHophoso;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itCoquancap;



    }
}