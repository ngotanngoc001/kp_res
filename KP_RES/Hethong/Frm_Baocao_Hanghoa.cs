﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Hanghoa : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Hanghoa()
        {
            InitializeComponent();
            this.Text = rm.GetString("hanghoa", culture);
            tileControl1.Text = rm.GetString("hanghoa", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void itBaocaoDSHH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHH.Checked)
            {
                this.Parent.GoTo<Frm_DS_Hanghoa>();
            }
        }

        private void itBaocaoBanggiatheokhu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoBanggiatheokhu.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banggiatheokhu>();
            }
        }

        private void itInmavachhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itInmavachhang.Checked)
            {
                this.Parent.GoTo<Frm_Inmavachhanghoa>();
            }
        }

        private void itCanhbaotonkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCanhbaotonkho.Checked)
            {
                this.Parent.GoTo<Frm_Canhbaotonkho >();
            }
        }

        private void itDinhluong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDinhluong.Checked)
            {
                this.Parent.GoTo<Frm_BC_Dinhluong >();
            }
        }

        private void itCombo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCombo.Checked)
            {
                this.Parent.GoTo<Frm_BC_DSCombo>();
            }
        }

        private void itInPLU_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itInPLU.Checked)
            {
                this.Parent.GoTo<Frm_InmaPLU>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocaoDSHH);
            cls_KP_RES.RemoveTileItem(itBaocaoBanggiatheokhu);
            cls_KP_RES.RemoveTileItem(itInmavachhang);
            cls_KP_RES.RemoveTileItem(itCanhbaotonkho);
            cls_KP_RES.RemoveTileItem(itDinhluong);
            cls_KP_RES.RemoveTileItem(itCombo);
            cls_KP_RES.RemoveTileItem(itInPLU);
        }

        private void LoadPermission()
        {
            itBaocaoDSHH.Checked = clsUserManagement.AllowView("48");
            itBaocaoBanggiatheokhu.Checked = clsUserManagement.AllowView("50");
            itInmavachhang.Checked = clsUserManagement.AllowView("56");
            itInPLU.Checked = clsUserManagement.AllowView("56");
            itCanhbaotonkho.Checked = clsUserManagement.AllowView("57");
            itDinhluong.Checked = clsUserManagement.AllowView("48") && clsUserManagement.CheckActive("2");
            itCombo.Checked = clsUserManagement.AllowView("11051501");
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Baocao_Hanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();

          
            itBaocaoDSHH.Text = rm.GetString("danhsachhanghoa", culture);
            itCanhbaotonkho.Text = rm.GetString("canhbaotonkho", culture);
            itBaocaoBanggiatheokhu.Text = rm.GetString("banggiatheokhu", culture);
            itInmavachhang.Text = rm.GetString("inmavachhanghoa", culture);
            itDinhluong.Text = rm.GetString("dinhluong", culture);

            this.Text = rm.GetString("hanghoa", culture);
            tileControl1.Text = rm.GetString("hanghoa", culture);
        }

       
    }
}