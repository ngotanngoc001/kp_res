﻿namespace KP_RES 
{
    partial class Frm_QuanLyCongTy 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itTuyendung = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itNhanvien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itHopdong = new DevExpress.XtraEditors.TileItem();
            this.itLichLamViec = new DevExpress.XtraEditors.TileItem();
            this.itKhenthuong = new DevExpress.XtraEditors.TileItem();
            this.itTienluong = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 53;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itTuyendung);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itTuyendung
            // 
            this.itTuyendung.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itTuyendung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Tuyển dụng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itTuyendung.Elements.Add(tileItemElement1);
            this.itTuyendung.Id = 48;
            this.itTuyendung.IsLarge = true;
            this.itTuyendung.Name = "itTuyendung";
            this.itTuyendung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTuyendung_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itNhanvien);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itNhanvien
            // 
            this.itNhanvien.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhanvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Nhân viên";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhanvien.Elements.Add(tileItemElement2);
            this.itNhanvien.Id = 49;
            this.itNhanvien.IsLarge = true;
            this.itNhanvien.Name = "itNhanvien";
            this.itNhanvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhanvien_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itHopdong);
            this.tileGroup10.Items.Add(this.itLichLamViec);
            this.tileGroup10.Items.Add(this.itKhenthuong);
            this.tileGroup10.Items.Add(this.itTienluong);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itHopdong
            // 
            this.itHopdong.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itHopdong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Hợp đồng lao động";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itHopdong.Elements.Add(tileItemElement3);
            this.itHopdong.Id = 50;
            this.itHopdong.IsLarge = true;
            this.itHopdong.Name = "itHopdong";
            this.itHopdong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHopdong_ItemClick);
            // 
            // itLichLamViec
            // 
            this.itLichLamViec.BackgroundImage = global::KP_RES.Properties.Resources.Caban;
            this.itLichLamViec.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Lịch làm việc";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itLichLamViec.Elements.Add(tileItemElement4);
            this.itLichLamViec.Id = 47;
            this.itLichLamViec.IsLarge = true;
            this.itLichLamViec.Name = "itLichLamViec";
            this.itLichLamViec.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLichLamViec_ItemClick);
            // 
            // itKhenthuong
            // 
            this.itKhenthuong.BackgroundImage = global::KP_RES.Properties.Resources.quydinhkhachhangthanthiet;
            this.itKhenthuong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Khen thưởng - Kỷ luật";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhenthuong.Elements.Add(tileItemElement5);
            this.itKhenthuong.Id = 51;
            this.itKhenthuong.IsLarge = true;
            this.itKhenthuong.Name = "itKhenthuong";
            this.itKhenthuong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhenthuong_ItemClick);
            // 
            // itTienluong
            // 
            this.itTienluong.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itTienluong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itTienluong.Checked = true;
            tileItemElement6.Text = "Tiền lương";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itTienluong.Elements.Add(tileItemElement6);
            this.itTienluong.Id = 52;
            this.itTienluong.IsLarge = true;
            this.itTienluong.Name = "itTienluong";
            this.itTienluong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTienluong_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Tồn kho";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement7);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Tồn kho";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement8);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // Frm_QuanLyCongTy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_QuanLyCongTy";
            this.Text = "Nhân sự";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_QuanLyCongTy_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itTuyendung;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itNhanvien;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itLichLamViec;
        private DevExpress.XtraEditors.TileItem itKhenthuong;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itHopdong;
        private DevExpress.XtraEditors.TileItem itTienluong;



    }
}