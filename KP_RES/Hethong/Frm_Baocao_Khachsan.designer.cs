﻿namespace KP_RES 
{
    partial class Frm_Baocao_Khachsan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itBaoCaoDanhSachHoaDon = new DevExpress.XtraEditors.TileItem();
            this.itBaoCaoDanhSachHoaDonInLai = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itBaoCaoDichVu = new DevExpress.XtraEditors.TileItem();
            this.itBaoCaoDoanhSo = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itKetQuaKinhDoanh = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itThongKeDoanhSo = new DevExpress.XtraEditors.TileItem();
            this.itThongKeBanHang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itHuyPhong = new DevExpress.XtraEditors.TileItem();
            this.itHuyDichVu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.itBaoCaoDanhSachHoaDon1 = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 59;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itBaoCaoDanhSachHoaDon);
            this.tileGroup7.Items.Add(this.itBaoCaoDanhSachHoaDonInLai);
            this.tileGroup7.Items.Add(this.itBaoCaoDanhSachHoaDon1);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itBaoCaoDanhSachHoaDon
            // 
            this.itBaoCaoDanhSachHoaDon.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itBaoCaoDanhSachHoaDon.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách hóa đơn";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCaoDanhSachHoaDon.Elements.Add(tileItemElement1);
            this.itBaoCaoDanhSachHoaDon.Id = 21;
            this.itBaoCaoDanhSachHoaDon.IsLarge = true;
            this.itBaoCaoDanhSachHoaDon.Name = "itBaoCaoDanhSachHoaDon";
            this.itBaoCaoDanhSachHoaDon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCaoDanhSachHoaDon_ItemClick);
            // 
            // itBaoCaoDanhSachHoaDonInLai
            // 
            this.itBaoCaoDanhSachHoaDonInLai.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itBaoCaoDanhSachHoaDonInLai.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Danh sách hóa đơn in lại";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCaoDanhSachHoaDonInLai.Elements.Add(tileItemElement2);
            this.itBaoCaoDanhSachHoaDonInLai.Id = 49;
            this.itBaoCaoDanhSachHoaDonInLai.IsLarge = true;
            this.itBaoCaoDanhSachHoaDonInLai.Name = "itBaoCaoDanhSachHoaDonInLai";
            this.itBaoCaoDanhSachHoaDonInLai.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCaoDanhSachHoaDonInLai_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itBaoCaoDichVu);
            this.tileGroup8.Items.Add(this.itBaoCaoDoanhSo);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itBaoCaoDichVu
            // 
            this.itBaoCaoDichVu.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itBaoCaoDichVu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Báo cáo bán hàng";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCaoDichVu.Elements.Add(tileItemElement4);
            this.itBaoCaoDichVu.Id = 22;
            this.itBaoCaoDichVu.IsLarge = true;
            this.itBaoCaoDichVu.Name = "itBaoCaoDichVu";
            this.itBaoCaoDichVu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCaoDichVu_ItemClick);
            // 
            // itBaoCaoDoanhSo
            // 
            this.itBaoCaoDoanhSo.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itBaoCaoDoanhSo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Báo cáo doanh số";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCaoDoanhSo.Elements.Add(tileItemElement5);
            this.itBaoCaoDoanhSo.Id = 23;
            this.itBaoCaoDoanhSo.IsLarge = true;
            this.itBaoCaoDoanhSo.Name = "itBaoCaoDoanhSo";
            this.itBaoCaoDoanhSo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCaoDoanhSo_ItemClick_1);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itKetQuaKinhDoanh);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itKetQuaKinhDoanh
            // 
            this.itKetQuaKinhDoanh.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itKetQuaKinhDoanh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Kết quả kinh doanh";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itKetQuaKinhDoanh.Elements.Add(tileItemElement6);
            this.itKetQuaKinhDoanh.Id = 24;
            this.itKetQuaKinhDoanh.IsLarge = true;
            this.itKetQuaKinhDoanh.Name = "itKetQuaKinhDoanh";
            this.itKetQuaKinhDoanh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKetQuaKinhDoanh_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itThongKeDoanhSo);
            this.tileGroup11.Items.Add(this.itThongKeBanHang);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itThongKeDoanhSo
            // 
            this.itThongKeDoanhSo.BackgroundImage = global::KP_RES.Properties.Resources.baocao1;
            this.itThongKeDoanhSo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Thống kê doanh số";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongKeDoanhSo.Elements.Add(tileItemElement7);
            this.itThongKeDoanhSo.Id = 44;
            this.itThongKeDoanhSo.IsLarge = true;
            this.itThongKeDoanhSo.Name = "itThongKeDoanhSo";
            this.itThongKeDoanhSo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongKeDoanhSo_ItemClick_1);
            // 
            // itThongKeBanHang
            // 
            this.itThongKeBanHang.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            this.itThongKeBanHang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Thống kê bán hàng";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongKeBanHang.Elements.Add(tileItemElement8);
            this.itThongKeBanHang.Id = 45;
            this.itThongKeBanHang.IsLarge = true;
            this.itThongKeBanHang.Name = "itThongKeBanHang";
            this.itThongKeBanHang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongKeBanHang_ItemClick_1);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itHuyPhong);
            this.tileGroup10.Items.Add(this.itHuyDichVu);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itHuyPhong
            // 
            this.itHuyPhong.BackgroundImage = global::KP_RES.Properties.Resources.saoluu;
            this.itHuyPhong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Hủy phòng";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuyPhong.Elements.Add(tileItemElement9);
            this.itHuyPhong.Id = 53;
            this.itHuyPhong.IsLarge = true;
            this.itHuyPhong.Name = "itHuyPhong";
            this.itHuyPhong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuyPhong_ItemClick);
            // 
            // itHuyDichVu
            // 
            this.itHuyDichVu.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itHuyDichVu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Hủy dịch vụ";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itHuyDichVu.Elements.Add(tileItemElement10);
            this.itHuyDichVu.Id = 54;
            this.itHuyDichVu.IsLarge = true;
            this.itHuyDichVu.Name = "itHuyDichVu";
            this.itHuyDichVu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHuyDichVu_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Tồn kho";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement11);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Tồn kho";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement12);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // itBaoCaoDanhSachHoaDon1
            // 
            this.itBaoCaoDanhSachHoaDon1.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itBaoCaoDanhSachHoaDon1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Danh sách hóa đơn 1";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCaoDanhSachHoaDon1.Elements.Add(tileItemElement3);
            this.itBaoCaoDanhSachHoaDon1.Id = 58;
            this.itBaoCaoDanhSachHoaDon1.IsLarge = true;
            this.itBaoCaoDanhSachHoaDon1.Name = "itBaoCaoDanhSachHoaDon1";
            this.itBaoCaoDanhSachHoaDon1.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCaoDanhSachHoaDon1_ItemClick);
            // 
            // Frm_Baocao_Khachsan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Khachsan";
            this.Text = "Báo cáo khách sạn";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Khachsan_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itBaoCaoDanhSachHoaDon;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBaoCaoDanhSachHoaDonInLai;
        private DevExpress.XtraEditors.TileItem itBaoCaoDichVu;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itBaoCaoDoanhSo;
        private DevExpress.XtraEditors.TileItem itKetQuaKinhDoanh;
        private DevExpress.XtraEditors.TileItem itThongKeDoanhSo;
        private DevExpress.XtraEditors.TileItem itThongKeBanHang;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itHuyPhong;
        private DevExpress.XtraEditors.TileItem itHuyDichVu;
        private DevExpress.XtraEditors.TileItem itBaoCaoDanhSachHoaDon1;



    }
}