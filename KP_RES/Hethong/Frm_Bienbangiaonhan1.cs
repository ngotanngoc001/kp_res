﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Bienbangiaonhan1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Bienbangiaonhan1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Bienbangiaonhan1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNghiepvu.Checked)
            {
                this.Parent.GoTo<Frm_BienBan_GiaoNhan>();
            }
        }


        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_BienBanGiaoNhan>();
            }
        }

       


        private void VisibleTileItem()
        {
           
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itNghiepvu);
        }

        private void LoadPermission()
        {

            itBaocao.Checked = true;
            itNghiepvu.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       
        

       

      

       

        

        

        

        

    }
}