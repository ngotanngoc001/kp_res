﻿namespace KP_RES 
{
    partial class Frm_Cauhinh3 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itGiaodien = new DevExpress.XtraEditors.TileItem();
            this.itNgonngu = new DevExpress.XtraEditors.TileItem();
            this.itMayin = new DevExpress.XtraEditors.TileItem();
            this.itHienthigia = new DevExpress.XtraEditors.TileItem();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itRap = new DevExpress.XtraEditors.TileItem();
            this.itQuay = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itMatkhaubanhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 42;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1354, 668);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itGiaodien);
            this.tileGroup8.Items.Add(this.itNgonngu);
            this.tileGroup8.Items.Add(this.itMayin);
            this.tileGroup8.Items.Add(this.itHienthigia);
            this.tileGroup8.Items.Add(this.itKettien);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itGiaodien
            // 
            this.itGiaodien.BackgroundImage = global::KP_RES.Properties.Resources.giaodien7;
            this.itGiaodien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Giao diện";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiaodien.Elements.Add(tileItemElement1);
            this.itGiaodien.Id = 21;
            this.itGiaodien.IsLarge = true;
            this.itGiaodien.Name = "itGiaodien";
            this.itGiaodien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiaodien_ItemClick);
            // 
            // itNgonngu
            // 
            this.itNgonngu.BackgroundImage = global::KP_RES.Properties.Resources.vietnam;
            this.itNgonngu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Ngôn ngữ";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNgonngu.Elements.Add(tileItemElement2);
            this.itNgonngu.Id = 35;
            this.itNgonngu.IsLarge = true;
            this.itNgonngu.Name = "itNgonngu";
            this.itNgonngu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNgonngu_ItemClick);
            // 
            // itMayin
            // 
            this.itMayin.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhmayin;
            this.itMayin.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Máy in";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itMayin.Elements.Add(tileItemElement3);
            this.itMayin.Id = 12;
            this.itMayin.IsLarge = true;
            this.itMayin.Name = "itMayin";
            this.itMayin.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMayin_ItemClick);
            // 
            // itHienthigia
            // 
            this.itHienthigia.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhhienthigia;
            this.itHienthigia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Màn hình hiển thị giá";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHienthigia.Elements.Add(tileItemElement4);
            this.itHienthigia.Id = 13;
            this.itHienthigia.IsLarge = true;
            this.itHienthigia.Name = "itHienthigia";
            this.itHienthigia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHienthigia_ItemClick);
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhkettien;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Két tiền";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement5);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            this.itKettien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKettien_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itRap);
            this.tileGroup10.Items.Add(this.itQuay);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itRap
            // 
            this.itRap.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            this.itRap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Cấu hình rạp";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itRap.Elements.Add(tileItemElement6);
            this.itRap.Id = 24;
            this.itRap.IsLarge = true;
            this.itRap.Name = "itRap";
            this.itRap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itRap_ItemClick);
            // 
            // itQuay
            // 
            this.itQuay.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            this.itQuay.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Cấu hình quầy";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuay.Elements.Add(tileItemElement7);
            this.itQuay.Id = 16;
            this.itQuay.IsLarge = true;
            this.itQuay.Name = "itQuay";
            this.itQuay.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuay_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itMatkhaubanhang);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itMatkhaubanhang
            // 
            this.itMatkhaubanhang.BackgroundImage = global::KP_RES.Properties.Resources.doimatkhau;
            this.itMatkhaubanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Cấu hình mật khẩu bán hàng";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itMatkhaubanhang.Elements.Add(tileItemElement8);
            this.itMatkhaubanhang.Id = 23;
            this.itMatkhaubanhang.IsLarge = true;
            this.itMatkhaubanhang.Name = "itMatkhaubanhang";
            this.itMatkhaubanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMatkhaubanhang_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Name = "tileGroup14";
            // 
            // tileGroup15
            // 
            this.tileGroup15.Name = "tileGroup15";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1358, 672);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_Cauhinh3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1358, 672);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Cauhinh3";
            this.Text = "Cấu hình";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Cauhinh3_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itMayin;
        private DevExpress.XtraEditors.TileItem itHienthigia;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itQuay;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itGiaodien;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileGroup tileGroup15;
        private DevExpress.XtraEditors.TileItem itNgonngu;
        private DevExpress.XtraEditors.TileItem itRap;
        private DevExpress.XtraEditors.TileItem itMatkhaubanhang;



    }
}