﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Luong : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Luong()
        {
            InitializeComponent();

            LoadPermission();
            cls_KP_RES.RemoveTileItem(itSongaylamviec);
            cls_KP_RES.RemoveTileItem(itChamcong);
            cls_KP_RES.RemoveTileItem(itTanggiamluong);
            cls_KP_RES.RemoveTileItem(itUngluong);
            cls_KP_RES.RemoveTileItem(itLuongthang);
        }

        private void Frm_Luong_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itSongaylamviec_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSongaylamviec.Checked)
            {
                this.Parent.GoTo<Frm_SoNgayLamViec>();
            }
        }

        private void itChamcong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itChamcong.Checked)
            {
                this.Parent.GoTo<Frm_ChamCong>();
            }
        }

        private void itTanggiamluong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTanggiamluong.Checked)
            {
                this.Parent.GoTo<Frm_TangGiamLuong>();
            }
        }

        private void itUngluong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itUngluong.Checked)
            {
                this.Parent.GoTo<Frm_UngLuong>();
            }
        }

        private void itLuongthang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLuongthang.Checked)
            {
                this.Parent.GoTo<Frm_LuongThang>();
            }
        }

        private void LoadPermission()
        {
            itSongaylamviec.Checked = clsUserManagement.AllowView("7051603");
            itChamcong.Checked = clsUserManagement.AllowView("7051604");
            itTanggiamluong.Checked = clsUserManagement.AllowView("7051605");
            itUngluong.Checked = clsUserManagement.AllowView("7051606");
            itLuongthang.Checked = clsUserManagement.AllowView("7051607"); ;
        }


        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            //itSongaylamviec.Text = rm.GetString("nhapkho", culture);
            //itXuatkho.Text = rm.GetString("xuatkho", culture);
            //itKiemke.Text = rm.GetString("kiemke", culture);
        }

        

        
    }
}