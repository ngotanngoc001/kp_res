﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Phanhebep : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Phanhebep()
        {
            InitializeComponent();
            this.Text = rm.GetString("manhinhbep", culture);
            tileControl1.Text = rm.GetString("manhinhbep", culture);
            LoadPermission();
            cls_KP_RES.RemoveTileItem(itManhinhbep);
            cls_KP_RES.RemoveTileItem(itTrangthaimon);
        }

        private void Frm_Phanhebep_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itManhinhbep_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itManhinhbep.Checked)
            {
                this.Parent.GoTo<Frm_Manhinhbep>();
            }
        }

        private void itTrangthaimon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrangthaimon.Checked)
            {
                this.Parent.GoTo<Frm_TrangthaiMon>();
            }
        }


        private void itManhinhbep1_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itManhinhbep1.Checked)
            {
                this.Parent.GoTo<Frm_Manhinhbep1>();
            }
        }

        private void itGoikhach_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGoikhach.Checked)
            {
                this.Parent.GoTo<Frm_Goikhach >();
            }
        }

        private void LoadPermission()
        {
            itManhinhbep.Checked = clsUserManagement.AllowEdit("24") ;
            itManhinhbep1.Checked = clsUserManagement.AllowEdit("24");
            itTrangthaimon.Checked = clsUserManagement.AllowView("24") ;
            itGoikhach.Checked = clsUserManagement.AllowView("24");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            itManhinhbep.Text = rm.GetString("manhinhbep", culture);
            itManhinhbep1.Text = rm.GetString("manhinhbepv1", culture);
            itTrangthaimon.Text = rm.GetString("trangthaimon", culture);
            itGoikhach.Text = rm.GetString("goikhach", culture);
        }

      

    }
}