﻿namespace KP_RES 
{
    partial class Frm_Caidat_KHTT 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itKHTT1 = new DevExpress.XtraEditors.TileItem();
            this.itKHTT = new DevExpress.XtraEditors.TileItem();
            this.itThethanhtoannoibo = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itQuydinhvediem = new DevExpress.XtraEditors.TileItem();
            this.itCapdothe = new DevExpress.XtraEditors.TileItem();
            this.itSole = new DevExpress.XtraEditors.TileItem();
            this.itQDKHTT = new DevExpress.XtraEditors.TileItem();
            this.itQuatang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itTTTNB1 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 55;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 25;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 647);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itKHTT1);
            this.tileGroup10.Items.Add(this.itKHTT);
            this.tileGroup10.Items.Add(this.itThethanhtoannoibo);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itKHTT1
            // 
            this.itKHTT1.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKHTT1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Khách hàng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT1.Elements.Add(tileItemElement1);
            this.itKHTT1.Id = 49;
            this.itKHTT1.IsLarge = true;
            this.itKHTT1.Name = "itKHTT1";
            this.itKHTT1.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT1_ItemClick);
            // 
            // itKHTT
            // 
            this.itKHTT.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Khách hàng thân thiết";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT.Elements.Add(tileItemElement2);
            this.itKHTT.Id = 31;
            this.itKHTT.IsLarge = true;
            this.itKHTT.Name = "itKHTT";
            this.itKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_ItemClick);
            // 
            // itThethanhtoannoibo
            // 
            this.itThethanhtoannoibo.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itThethanhtoannoibo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Thẻ thanh toán nội bộ";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itThethanhtoannoibo.Elements.Add(tileItemElement3);
            this.itThethanhtoannoibo.Id = 34;
            this.itThethanhtoannoibo.IsLarge = true;
            this.itThethanhtoannoibo.Name = "itThethanhtoannoibo";
            this.itThethanhtoannoibo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThethanhtoannoibo_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itQuydinhvediem);
            this.tileGroup9.Items.Add(this.itCapdothe);
            this.tileGroup9.Items.Add(this.itSole);
            this.tileGroup9.Items.Add(this.itQDKHTT);
            this.tileGroup9.Items.Add(this.itQuatang);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itQuydinhvediem
            // 
            this.itQuydinhvediem.BackgroundImage = global::KP_RES.Properties.Resources.quydinhkhachhangthanthiet;
            this.itQuydinhvediem.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Quy định điểm";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuydinhvediem.Elements.Add(tileItemElement4);
            this.itQuydinhvediem.Id = 51;
            this.itQuydinhvediem.IsLarge = true;
            this.itQuydinhvediem.Name = "itQuydinhvediem";
            this.itQuydinhvediem.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuydinhvediem_ItemClick);
            // 
            // itCapdothe
            // 
            this.itCapdothe.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itCapdothe.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Cấp độ thẻ";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itCapdothe.Elements.Add(tileItemElement5);
            this.itCapdothe.Id = 52;
            this.itCapdothe.IsLarge = true;
            this.itCapdothe.Name = "itCapdothe";
            this.itCapdothe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCapdothe_ItemClick);
            // 
            // itSole
            // 
            this.itSole.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itSole.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Số lẻ";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itSole.Elements.Add(tileItemElement6);
            this.itSole.Id = 53;
            this.itSole.IsLarge = true;
            this.itSole.Name = "itSole";
            this.itSole.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSole_ItemClick);
            // 
            // itQDKHTT
            // 
            this.itQDKHTT.BackgroundImage = global::KP_RES.Properties.Resources.quydinhkhachhangthanthiet;
            this.itQDKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Các quy định";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itQDKHTT.Elements.Add(tileItemElement7);
            this.itQDKHTT.Id = 32;
            this.itQDKHTT.IsLarge = true;
            this.itQDKHTT.Name = "itQDKHTT";
            this.itQDKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQDKHTT_ItemClick);
            // 
            // itQuatang
            // 
            this.itQuatang.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itQuatang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Quà tặng";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuatang.Elements.Add(tileItemElement8);
            this.itQuatang.Id = 54;
            this.itQuatang.IsLarge = true;
            this.itQuatang.Name = "itQuatang";
            this.itQuatang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuatang_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itTTTNB1);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itTTTNB1
            // 
            this.itTTTNB1.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itTTTNB1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Thẻ thanh toán nội bộ (v1)";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itTTTNB1.Elements.Add(tileItemElement9);
            this.itTTTNB1.Id = 50;
            this.itTTTNB1.IsLarge = true;
            this.itTTTNB1.Name = "itTTTNB1";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 651);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // Frm_Caidat_KHTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 651);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_KHTT";
            this.Text = "Khách hàng thân thiết - Thẻ nội bộ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_KHTT_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itKHTT;
        private DevExpress.XtraEditors.TileItem itThethanhtoannoibo;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itKHTT1;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itTTTNB1;
        private DevExpress.XtraEditors.TileItem itQuydinhvediem;
        private DevExpress.XtraEditors.TileItem itQDKHTT;
        private DevExpress.XtraEditors.TileItem itCapdothe;
        private DevExpress.XtraEditors.TileItem itSole;
        private DevExpress.XtraEditors.TileItem itQuatang;



    }
}