﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Caidat_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Karaoke()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Karaoke_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKhuvuckaraoke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhuvuckaraoke.Checked)
            {
               this.Parent.GoTo<Frm_KhuvucPhongKaraoke>();
            }
        }

        private void itLapbanggia_Karaoke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLapbanggia_Karaoke.Checked)
            {
               this.Parent.GoTo<Frm_Lapbanggia_Karaoke>();
            }
        }

        private void itApdungbanggia_Karaoke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia_Karaoke.Checked)
            {
               this.Parent.GoTo<Frm_ApdungbanggiaKaraoke>();
            }
        }

        private void itCauhinhblock_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinhblock.Checked)
            {
                this.Parent.GoTo<Frm_SetBlockGioKaraoke>();
            }
        }

        private void itApdungbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia.Checked)
            {
                this.Parent.GoTo<Frm_Apdungbanggiahanghoa_Karaoke>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKhuvuckaraoke);
            cls_KP_RES.RemoveTileItem(itLapbanggia_Karaoke);
            cls_KP_RES.RemoveTileItem(itApdungbanggia_Karaoke);
            cls_KP_RES.RemoveTileItem(itCauhinhblock);
            cls_KP_RES.RemoveTileItem(itApdungbanggia);
        }

        private void LoadPermission()
        {
            itKhuvuckaraoke.Checked = clsUserManagement.AllowView("5");
            itLapbanggia_Karaoke.Checked = clsUserManagement.AllowView("42");
            itApdungbanggia_Karaoke.Checked = clsUserManagement.AllowView("42");
            itCauhinhblock.Checked = clsUserManagement.AllowView("89");
            itApdungbanggia.Checked = clsUserManagement.AllowView("42");
        }

    }
}