﻿namespace KP_RES 
{
    partial class Frm_Baocao_Kho 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoPhieunhap = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoPhieuxuat = new DevExpress.XtraEditors.TileItem();
            this.itBaocaoPhieuKiemKe = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoTonkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itThekho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itNhapkho = new DevExpress.XtraEditors.TileItem();
            this.itXuatkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 47;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBaocaoPhieunhap);
            this.tileGroup2.Items.Add(this.itBaocaoPhieuxuat);
            this.tileGroup2.Items.Add(this.itBaocaoPhieuKiemKe);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBaocaoPhieunhap
            // 
            this.itBaocaoPhieunhap.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itBaocaoPhieunhap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách phiếu nhập";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoPhieunhap.Elements.Add(tileItemElement1);
            this.itBaocaoPhieunhap.Id = 18;
            this.itBaocaoPhieunhap.IsLarge = true;
            this.itBaocaoPhieunhap.Name = "itBaocaoPhieunhap";
            this.itBaocaoPhieunhap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoPhieunhap_ItemClick);
            // 
            // itBaocaoPhieuxuat
            // 
            this.itBaocaoPhieuxuat.BackgroundImage = global::KP_RES.Properties.Resources.xuatkho4;
            this.itBaocaoPhieuxuat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Danh sách phiếu xuất";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoPhieuxuat.Elements.Add(tileItemElement2);
            this.itBaocaoPhieuxuat.Id = 19;
            this.itBaocaoPhieuxuat.IsLarge = true;
            this.itBaocaoPhieuxuat.Name = "itBaocaoPhieuxuat";
            this.itBaocaoPhieuxuat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoPhieuxuat_ItemClick);
            // 
            // itBaocaoPhieuKiemKe
            // 
            this.itBaocaoPhieuKiemKe.BackgroundImage = global::KP_RES.Properties.Resources.kiemke3;
            this.itBaocaoPhieuKiemKe.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Danh sách phiếu kiểm kê";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoPhieuKiemKe.Elements.Add(tileItemElement3);
            this.itBaocaoPhieuKiemKe.Id = 25;
            this.itBaocaoPhieuKiemKe.IsLarge = true;
            this.itBaocaoPhieuKiemKe.Name = "itBaocaoPhieuKiemKe";
            this.itBaocaoPhieuKiemKe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoPhieuKiemKe_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itBaocaoTonkho);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itBaocaoTonkho
            // 
            this.itBaocaoTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itBaocaoTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Tồn kho";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoTonkho.Elements.Add(tileItemElement4);
            this.itBaocaoTonkho.Id = 20;
            this.itBaocaoTonkho.IsLarge = true;
            this.itBaocaoTonkho.Name = "itBaocaoTonkho";
            this.itBaocaoTonkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoTonkho_ItemClick);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itThekho);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itThekho
            // 
            this.itThekho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho4;
            this.itThekho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thẻ kho";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itThekho.Elements.Add(tileItemElement5);
            this.itThekho.Id = 44;
            this.itThekho.IsLarge = true;
            this.itThekho.Name = "itThekho";
            this.itThekho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThekho_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itNhapkho);
            this.tileGroup9.Items.Add(this.itXuatkho);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itNhapkho
            // 
            this.itNhapkho.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon;
            this.itNhapkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Báo cáo nhập kho";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhapkho.Elements.Add(tileItemElement6);
            this.itNhapkho.Id = 45;
            this.itNhapkho.IsLarge = true;
            this.itNhapkho.Name = "itNhapkho";
            this.itNhapkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhapkho_ItemClick);
            // 
            // itXuatkho
            // 
            this.itXuatkho.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_Huy;
            this.itXuatkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Báo cáo xuất kho";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itXuatkho.Elements.Add(tileItemElement7);
            this.itXuatkho.Id = 46;
            this.itXuatkho.IsLarge = true;
            this.itXuatkho.Name = "itXuatkho";
            this.itXuatkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itXuatkho_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Tồn kho";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement8);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Tồn kho";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement9);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // Frm_Baocao_Kho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Kho";
            this.Text = "Báo cáo nhập xuất kho";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Kho_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBaocaoPhieunhap;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itBaocaoPhieuxuat;
        private DevExpress.XtraEditors.TileItem itBaocaoTonkho;
        private DevExpress.XtraEditors.TileItem itBaocaoPhieuKiemKe;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itThekho;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itNhapkho;
        private DevExpress.XtraEditors.TileItem itXuatkho;



    }
}