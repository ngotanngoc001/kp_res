﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Congno : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Congno()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Nghiệp vụ";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Congno_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itLapphieuthu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLapphieuthu.Checked)
            {
                this.Parent.GoTo<Frm_LapPhieuThu>();
            }
        }

        private void itLapphieuchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLapphieuchi.Checked)
            {
                this.Parent.GoTo<Frm_LapPhieuChi>();
            }
        }

        private void itPhieudenghiduyetchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhieudenghiduyetchi.Checked)
            {
                this.Parent.GoTo<Frm_PhieuDeNghiLapPhieuChi>();
            }
        }

        private void itDuyetphieudenghichi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDuyetphieudenghichi.Checked)
            {
                this.Parent.GoTo<Frm_DanhSachPhieuDeNghiChi>();
            }
        }

        private void itThuquy_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThuquy.Checked)
            {
                this.Parent.GoTo<Frm_Thuquyxacnhan>();
            }
        }

        private void itUynhiemchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itUynhiemchi.Checked)
            {
                this.Parent.GoTo<Frm_UyNhiemChi>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itLapphieuthu );
            cls_KP_RES.RemoveTileItem(itLapphieuchi);
            cls_KP_RES.RemoveTileItem(itPhieudenghiduyetchi);
            cls_KP_RES.RemoveTileItem(itDuyetphieudenghichi);
            cls_KP_RES.RemoveTileItem(itThuquy);
            cls_KP_RES.RemoveTileItem(itUynhiemchi);
        }

        private void LoadPermission()
        {
            itLapphieuthu.Checked = clsUserManagement.AllowView("90") ;
            itLapphieuchi.Checked = clsUserManagement.AllowView("91");
            itPhieudenghiduyetchi.Checked = clsUserManagement.AllowView("26071601");
            itDuyetphieudenghichi.Checked = clsUserManagement.AllowView("26071602");
            if (cls_KP_RES.Mode == 1)
            {
                itThuquy.Checked = false;
            }
            else
            {
                itThuquy.Checked = clsUserManagement.AllowView("981601");
            }
            itUynhiemchi.Checked = clsUserManagement.AllowView("1781601"); ;
        }

      

       

       
      
    }
}