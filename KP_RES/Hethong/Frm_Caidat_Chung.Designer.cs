﻿namespace KP_RES 
{
    partial class Frm_Caidat_Chung 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itCHKQ = new DevExpress.XtraEditors.TileItem();
            this.itDonvitinh = new DevExpress.XtraEditors.TileItem();
            this.itBep = new DevExpress.XtraEditors.TileItem();
            this.itNhomHangHoa = new DevExpress.XtraEditors.TileItem();
            this.itNCCKH = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itCabanThietlapcaban = new DevExpress.XtraEditors.TileItem();
            this.itTienteTygia = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itManhinh2 = new DevExpress.XtraEditors.TileItem();
            this.itHinhmenu = new DevExpress.XtraEditors.TileItem();
            this.itCaidatMHMenu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itYeucauthem = new DevExpress.XtraEditors.TileItem();
            this.itDinhluong = new DevExpress.XtraEditors.TileItem();
            this.itCombo = new DevExpress.XtraEditors.TileItem();
            this.itLapbanggia = new DevExpress.XtraEditors.TileItem();
            this.itApdungbanggia_cuahang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itHangbanonline = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.itBuffet = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 60;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 785;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 647);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itCHKQ);
            this.tileGroup2.Items.Add(this.itDonvitinh);
            this.tileGroup2.Items.Add(this.itBep);
            this.tileGroup2.Items.Add(this.itNhomHangHoa);
            this.tileGroup2.Items.Add(this.itNCCKH);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itCHKQ
            // 
            this.itCHKQ.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itCHKQ.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Cửa hàng - Kho - Quầy";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itCHKQ.Elements.Add(tileItemElement1);
            this.itCHKQ.Id = 14;
            this.itCHKQ.IsLarge = true;
            this.itCHKQ.Name = "itCHKQ";
            // 
            // itDonvitinh
            // 
            this.itDonvitinh.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itDonvitinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Đơn vị tính";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDonvitinh.Elements.Add(tileItemElement2);
            this.itDonvitinh.Id = 18;
            this.itDonvitinh.IsLarge = true;
            this.itDonvitinh.Name = "itDonvitinh";
            // 
            // itBep
            // 
            this.itBep.BackgroundImage = global::KP_RES.Properties.Resources.bep3;
            this.itBep.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Bếp";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBep.Elements.Add(tileItemElement3);
            this.itBep.Id = 22;
            this.itBep.IsLarge = true;
            this.itBep.Name = "itBep";
            // 
            // itNhomHangHoa
            // 
            this.itNhomHangHoa.BackgroundImage = global::KP_RES.Properties.Resources.nhomhanghanghoa;
            this.itNhomHangHoa.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Nhóm hàng - Hàng hóa";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhomHangHoa.Elements.Add(tileItemElement4);
            this.itNhomHangHoa.Id = 16;
            this.itNhomHangHoa.IsLarge = true;
            this.itNhomHangHoa.Name = "itNhomHangHoa";
            // 
            // itNCCKH
            // 
            this.itNCCKH.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itNCCKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Nhà cung cấp - Khách hàng";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itNCCKH.Elements.Add(tileItemElement5);
            this.itNCCKH.Id = 13;
            this.itNCCKH.IsLarge = true;
            this.itNCCKH.Name = "itNCCKH";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itCabanThietlapcaban);
            this.tileGroup6.Items.Add(this.itTienteTygia);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itCabanThietlapcaban
            // 
            this.itCabanThietlapcaban.BackgroundImage = global::KP_RES.Properties.Resources.Caban;
            this.itCabanThietlapcaban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Ca bán - Thiết lập ca bán";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itCabanThietlapcaban.Elements.Add(tileItemElement6);
            this.itCabanThietlapcaban.Id = 26;
            this.itCabanThietlapcaban.IsLarge = true;
            this.itCabanThietlapcaban.Name = "itCabanThietlapcaban";
            // 
            // itTienteTygia
            // 
            this.itTienteTygia.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itTienteTygia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Tiền tệ - Tỷ giá";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itTienteTygia.Elements.Add(tileItemElement7);
            this.itTienteTygia.Id = 25;
            this.itTienteTygia.IsLarge = true;
            this.itTienteTygia.Name = "itTienteTygia";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itManhinh2);
            this.tileGroup10.Items.Add(this.itHinhmenu);
            this.tileGroup10.Items.Add(this.itCaidatMHMenu);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itManhinh2
            // 
            this.itManhinh2.BackgroundImage = global::KP_RES.Properties.Resources.manhinh2;
            this.itManhinh2.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Màn hình phụ";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itManhinh2.Elements.Add(tileItemElement8);
            this.itManhinh2.Id = 49;
            this.itManhinh2.IsLarge = true;
            this.itManhinh2.Name = "itManhinh2";
            this.itManhinh2.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itManhinh2_ItemClick);
            // 
            // itHinhmenu
            // 
            this.itHinhmenu.BackgroundImage = global::KP_RES.Properties.Resources.giaodien7;
            this.itHinhmenu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Cài đặt hình Menu (Display)";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itHinhmenu.Elements.Add(tileItemElement9);
            this.itHinhmenu.Id = 50;
            this.itHinhmenu.IsLarge = true;
            this.itHinhmenu.Name = "itHinhmenu";
            this.itHinhmenu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHinhmenu_ItemClick);
            // 
            // itCaidatMHMenu
            // 
            this.itCaidatMHMenu.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep;
            this.itCaidatMHMenu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Cài đặt  Menu Display";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidatMHMenu.Elements.Add(tileItemElement10);
            this.itCaidatMHMenu.Id = 51;
            this.itCaidatMHMenu.IsLarge = true;
            this.itCaidatMHMenu.Name = "itCaidatMHMenu";
            this.itCaidatMHMenu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidatMHMenu_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itYeucauthem);
            this.tileGroup11.Items.Add(this.itDinhluong);
            this.tileGroup11.Items.Add(this.itCombo);
            this.tileGroup11.Items.Add(this.itLapbanggia);
            this.tileGroup11.Items.Add(this.itApdungbanggia_cuahang);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itYeucauthem
            // 
            this.itYeucauthem.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itYeucauthem.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Yêu cầu thêm";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itYeucauthem.Elements.Add(tileItemElement11);
            this.itYeucauthem.Id = 53;
            this.itYeucauthem.IsLarge = true;
            this.itYeucauthem.Name = "itYeucauthem";
            this.itYeucauthem.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itYeucauthem_ItemClick);
            // 
            // itDinhluong
            // 
            this.itDinhluong.BackgroundImage = global::KP_RES.Properties.Resources.dinhluong;
            this.itDinhluong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Định lượng";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itDinhluong.Elements.Add(tileItemElement12);
            this.itDinhluong.Id = 54;
            this.itDinhluong.IsLarge = true;
            this.itDinhluong.Name = "itDinhluong";
            this.itDinhluong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDinhluong_ItemClick);
            // 
            // itCombo
            // 
            this.itCombo.BackgroundImage = global::KP_RES.Properties.Resources.showtime;
            this.itCombo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Set Combo";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itCombo.Elements.Add(tileItemElement13);
            this.itCombo.Id = 55;
            this.itCombo.IsLarge = true;
            this.itCombo.Name = "itCombo";
            this.itCombo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCombo_ItemClick);
            // 
            // itLapbanggia
            // 
            this.itLapbanggia.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itLapbanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Lập bảng giá";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itLapbanggia.Elements.Add(tileItemElement14);
            this.itLapbanggia.Id = 56;
            this.itLapbanggia.IsLarge = true;
            this.itLapbanggia.Name = "itLapbanggia";
            this.itLapbanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLapbanggia_ItemClick);
            // 
            // itApdungbanggia_cuahang
            // 
            this.itApdungbanggia_cuahang.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itApdungbanggia_cuahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Áp dụng bảng giá theo cửa hàng";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itApdungbanggia_cuahang.Elements.Add(tileItemElement15);
            this.itApdungbanggia_cuahang.Id = 57;
            this.itApdungbanggia_cuahang.IsLarge = true;
            this.itApdungbanggia_cuahang.Name = "itApdungbanggia_cuahang";
            this.itApdungbanggia_cuahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itApdungbanggia_cuahang_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itHangbanonline);
            this.tileGroup12.Items.Add(this.itBuffet);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itHangbanonline
            // 
            this.itHangbanonline.BackgroundImage = global::KP_RES.Properties.Resources.hangbanonline;
            this.itHangbanonline.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itHangbanonline.Checked = true;
            tileItemElement16.Text = "Hàng bán Online";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itHangbanonline.Elements.Add(tileItemElement16);
            this.itHangbanonline.Id = 58;
            this.itHangbanonline.IsLarge = true;
            this.itHangbanonline.Name = "itHangbanonline";
            this.itHangbanonline.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHangbanonline_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 651);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // itBuffet
            // 
            this.itBuffet.BackgroundImage = global::KP_RES.Properties.Resources.phongve;
            this.itBuffet.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement17.Text = "Cài đặt buffet";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            this.itBuffet.Elements.Add(tileItemElement17);
            this.itBuffet.Id = 59;
            this.itBuffet.IsLarge = true;
            this.itBuffet.Name = "itBuffet";
            this.itBuffet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBuffet_ItemClick);
            // 
            // Frm_Caidat_Chung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 651);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Chung";
            this.Text = "Cài đặt chung";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Chung_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itCHKQ;
        private DevExpress.XtraEditors.TileItem itCabanThietlapcaban;
        private DevExpress.XtraEditors.TileItem itNhomHangHoa;
        private DevExpress.XtraEditors.TileItem itTienteTygia;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itDonvitinh;
        private DevExpress.XtraEditors.TileItem itBep;
        private DevExpress.XtraEditors.TileItem itNCCKH;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itManhinh2;
        private DevExpress.XtraEditors.TileItem itHinhmenu;
        private DevExpress.XtraEditors.TileItem itCaidatMHMenu;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itYeucauthem;
        private DevExpress.XtraEditors.TileItem itDinhluong;
        private DevExpress.XtraEditors.TileItem itCombo;
        private DevExpress.XtraEditors.TileItem itLapbanggia;
        private DevExpress.XtraEditors.TileItem itApdungbanggia_cuahang;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itHangbanonline;
        private DevExpress.XtraEditors.TileItem itBuffet;



    }
}