﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Quanlycongty1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Quanlycongty1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Quanlycongty1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhansu>();
            }
        }

        private void itCaidat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidat.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Nhansu>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhansu.Checked)
            {
                this.Parent.GoTo<Frm_QuanLyCongTy>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCaidat);
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itNhansu);
        }

        private void LoadPermission()
        {
            itCaidat.Checked = clsUserManagement.AllowView("6051602") && clsUserManagement.CheckActive("17");
            itBaocao.Checked = clsUserManagement.AllowView("21051601") || clsUserManagement.AllowView("21051602") || clsUserManagement.AllowView("21051603") || clsUserManagement.AllowView("21051604") || clsUserManagement.AllowView("21051605") && clsUserManagement.CheckActive("17");
            itNhansu.Checked = clsUserManagement.AllowView("5051601") || clsUserManagement.AllowView("5051602") || clsUserManagement.AllowView("6051601") || clsUserManagement.AllowView("6051603") && clsUserManagement.CheckActive("17");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

 
       
        

       

      

       

        

        

        

        

    }
}