﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Caidat_Ve_Trochoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Ve_Trochoi()
        {
            InitializeComponent();

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Ve_Trochoi_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itTrochoi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrochoi.Checked)
            {
                this.Parent.GoTo<Frm_LoaiTroChoi>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itTrochoi);
        }

        private void LoadPermission()
        {
            itTrochoi.Checked = clsUserManagement.AllowView("60");
        }
    }
}