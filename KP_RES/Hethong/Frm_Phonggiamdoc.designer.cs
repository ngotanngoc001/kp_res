﻿namespace KP_RES 
{
    partial class Frm_Phonggiamdoc 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itBaoCao = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itGiayToKyDuyet = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itGiaiQuyetCongViec = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 63;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itBaoCao);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itBaoCao
            // 
            this.itBaoCao.BackgroundImage = global::KP_RES.Properties.Resources.baocao1;
            this.itBaoCao.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Báo cáo";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoCao.Elements.Add(tileItemElement1);
            this.itBaoCao.Id = 62;
            this.itBaoCao.IsLarge = true;
            this.itBaoCao.Name = "itBaoCao";
            this.itBaoCao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoCao_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itGiayToKyDuyet);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itGiayToKyDuyet
            // 
            tileItemElement2.Text = "Giấy tờ ký duyệt";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiayToKyDuyet.Elements.Add(tileItemElement2);
            this.itGiayToKyDuyet.Id = 60;
            this.itGiayToKyDuyet.IsLarge = true;
            this.itGiayToKyDuyet.Name = "itGiayToKyDuyet";
            this.itGiayToKyDuyet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiayToKyDuyet_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itGiaiQuyetCongViec);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itGiaiQuyetCongViec
            // 
            tileItemElement3.Text = "Giải quyết công việc";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiaiQuyetCongViec.Elements.Add(tileItemElement3);
            this.itGiaiQuyetCongViec.Id = 61;
            this.itGiaiQuyetCongViec.IsLarge = true;
            this.itGiaiQuyetCongViec.Name = "itGiaiQuyetCongViec";
            this.itGiaiQuyetCongViec.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiaiQuyetCongViec_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_Phonggiamdoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Phonggiamdoc";
            this.Text = "Phòng giám đốc";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Phonggiamdoc_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itBaoCao;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itGiayToKyDuyet;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itGiaiQuyetCongViec;
    }
}