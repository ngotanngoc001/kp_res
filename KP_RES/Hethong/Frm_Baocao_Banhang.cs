﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Banhang : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Banhang()
        {
            InitializeComponent();
            this.Text = rm.GetString("banhang", culture);
            tileControl1.Text = rm.GetString("banhang", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Banhang_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaocaoDSHD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDon>();
            }
        }

        private void itBaocaoBanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoBanhang.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banhang>();
            }
        }

        private void itBaocaoDoanhso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDoanhso.Checked)
            {
                this.Parent.GoTo<Frm_BC_Doanhso>();
            }
        }

        private void itBaocaoKQKD_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoKQKD.Checked)
            {
                this.Parent.GoTo<Frm_BC_KQKD>();
            }
        }

        private void itThongkedoanhso_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThongkedoanhso.Checked)
            {
                this.Parent.GoTo<Frm_TK_Doanhso>();
            }
        }

        private void itThongkebanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThongkebanhang.Checked)
            {
                this.Parent.GoTo<Frm_TK_Hangbanchay>();
            }
        }

        private void itBc_BH_Combo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBc_BH_Combo.Checked)
            {
                this.Parent.GoTo<Frm_BC_Combo>();
            }
        }

        private void itBaocaoDSHD_HUY_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD_HUY.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDonCancel>();
            }
        }

        private void itBaocaoDSHD_INLAI_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD_INLAI.Checked)
            {
                this.Parent.GoTo<Frm_BC_InlaiHoadon>();
            }
        }

        private void itBc_CTBH_Combo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBc_CTBH_Combo.Checked)
            {
                this.Parent.GoTo<Frm_BC_Combo_ChiTiet>();
            }
        }

        private void itBaocao_banhang_online_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao_banhang_online.Checked)
            {
                this.Parent.GoTo<Frm_BC_HH_ONLINE>();
            }
        }

        private void itSoluongkhach_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoluongkhach.Checked)
            {
                this.Parent.GoTo<Frm_BC_Soluongkhach>();
            }
        }

        private void itHuyban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuyban.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyBan>();
            }
        }

        private void itHuymon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuymon.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyMon>();
            }
        }

        private void itBC_Banhang_Nhanvien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBC_Banhang_Nhanvien.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banhang1>();
            }
        }

        private void itDSHD_Trahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDSHD_Trahang.Checked)
            {
                this.Parent.GoTo<Frm_DSTraHang>();
            }
        }

        private void itBaocaotrahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaotrahang.Checked)
            {
                this.Parent.GoTo<Frm_BC_Trahang>();
            }
        }

        private void itBC_Banhang_Khachhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBC_Banhang_Khachhang.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banhang2>();
            }
        }

        private void itLaylaihoadon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLaylaihoadon.Checked)
            {
                this.Parent.GoTo<Frm_DSLayLaiHD>();
            }
        }

        private void itBaocaoDSHD_Chitiet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoDSHD_Chitiet.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDon_Chitiet>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD);
            cls_KP_RES.RemoveTileItem(itBaocaoBanhang);
            cls_KP_RES.RemoveTileItem(itBaocaoDoanhso);
            cls_KP_RES.RemoveTileItem(itBaocaoKQKD);
            cls_KP_RES.RemoveTileItem(itThongkedoanhso);
            cls_KP_RES.RemoveTileItem(itThongkebanhang);
            cls_KP_RES.RemoveTileItem(itBc_BH_Combo);
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD_HUY);
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD_INLAI);
            cls_KP_RES.RemoveTileItem(itBc_CTBH_Combo);
            cls_KP_RES.RemoveTileItem(itBaocao_banhang_online);
            cls_KP_RES.RemoveTileItem(itSoluongkhach);
            cls_KP_RES.RemoveTileItem(itHuyban);
            cls_KP_RES.RemoveTileItem(itHuymon);
            cls_KP_RES.RemoveTileItem(itBC_Banhang_Nhanvien);
            cls_KP_RES.RemoveTileItem(itDSHD_Trahang);
            cls_KP_RES.RemoveTileItem(itBaocaotrahang);
            cls_KP_RES.RemoveTileItem(itBC_Banhang_Khachhang);
            cls_KP_RES.RemoveTileItem(itLaylaihoadon);
            cls_KP_RES.RemoveTileItem(itBaocaoDSHD_Chitiet);
        }

        private void LoadPermission()
        {
            itBaocaoDSHD.Checked = clsUserManagement.AllowView("28");
            itBaocaoBanhang.Checked = clsUserManagement.AllowView("29");
            itBaocaoDoanhso.Checked = clsUserManagement.AllowView("30");
            itBaocaoKQKD.Checked = clsUserManagement.AllowView("31");
            itThongkedoanhso.Checked = clsUserManagement.AllowView("103");
            itThongkebanhang.Checked = clsUserManagement.AllowView("103");
            itBc_BH_Combo.Checked = clsUserManagement.AllowView("26041501");
            itBc_CTBH_Combo.Checked = clsUserManagement.AllowView("26041501");
            itBaocaoDSHD_HUY.Checked = clsUserManagement.AllowView("28") && (clsUserManagement.CheckActive("14") || clsUserManagement.CheckActive("220417"));
            itBaocaoDSHD_INLAI.Checked = clsUserManagement.AllowView("28");
            itBaocao_banhang_online.Checked = clsUserManagement.AllowView("14031602");
            itSoluongkhach.Checked = clsUserManagement.AllowView("811201601");
            itHuyban.Checked = clsUserManagement.AllowView("28") && clsUserManagement.CheckActive("5");
            itHuymon.Checked = clsUserManagement.AllowView("28") && clsUserManagement.CheckActive("5");
            itLaylaihoadon.Checked = clsUserManagement.AllowView("28") && clsUserManagement.CheckActive("5");
            itBC_Banhang_Nhanvien.Checked = clsUserManagement.AllowView("29") && cls_KP_RES.Mode ==8;
            itBC_Banhang_Khachhang.Checked = clsUserManagement.AllowView("29") && cls_KP_RES.Mode == 8;

            itDSHD_Trahang.Checked = clsUserManagement.AllowView("19061701") && clsUserManagement.CheckActive("220417");
            itBaocaotrahang.Checked = clsUserManagement.AllowView("19061701") && clsUserManagement.CheckActive("220417");

            itBaocaoDSHD_Chitiet.Checked = clsUserManagement.AllowView("28");
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            
            itBaocaoDSHD.Text = rm.GetString("danhsachhoadon", culture);
            itBaocaoBanhang.Text = rm.GetString("baocaobanhang", culture);
            itBaocaoKQKD.Text = rm.GetString("ketquakinhdoanh", culture);
            itThongkebanhang.Text = rm.GetString("thongkehangbanchay", culture);
            itThongkedoanhso.Text = rm.GetString("thongkedoanhthu1", culture);
            itBaocaoDoanhso.Text = rm.GetString("baocaodoanhso", culture);
            itBc_BH_Combo.Text = rm.GetString("baocaobanhangcombo", culture);
            itBaocaoDSHD_HUY.Text = rm.GetString("danhsachhoadondahuy", culture);
            itBaocaoDSHD_INLAI.Text = rm.GetString("danhsachhoadoninlai", culture);
            itHuyban.Text = rm.GetString("huyban", culture);
            itBc_CTBH_Combo.Text = rm.GetString("bcctbanhangcombo", culture);
            itBaocaoDSHD_Chitiet.Text = rm.GetString("dshdct", culture);

            this.Text = rm.GetString("banhang", culture);
            tileControl1.Text = rm.GetString("banhang", culture);
        }

       
      
      

      

        

        
    }
}