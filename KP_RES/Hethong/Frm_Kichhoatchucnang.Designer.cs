﻿namespace KP_RES 
{
    partial class Frm_Kichhoatchucnang 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itLuu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itKho = new DevExpress.XtraEditors.TileItem();
            this.itDinhluong = new DevExpress.XtraEditors.TileItem();
            this.itManhinhbep = new DevExpress.XtraEditors.TileItem();
            this.itKHTT = new DevExpress.XtraEditors.TileItem();
            this.itThethanhtoannoibo = new DevExpress.XtraEditors.TileItem();
            this.itNhahang = new DevExpress.XtraEditors.TileItem();
            this.itKaraok = new DevExpress.XtraEditors.TileItem();
            this.itKhachsan = new DevExpress.XtraEditors.TileItem();
            this.itTrochoi = new DevExpress.XtraEditors.TileItem();
            this.itRapphim = new DevExpress.XtraEditors.TileItem();
            this.itKhuyenmai = new DevExpress.XtraEditors.TileItem();
            this.itCongno = new DevExpress.XtraEditors.TileItem();
            this.itVoucher = new DevExpress.XtraEditors.TileItem();
            this.itFastfood = new DevExpress.XtraEditors.TileItem();
            this.itMenu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.itCongvan = new DevExpress.XtraEditors.TileItem();
            this.itNhansu = new DevExpress.XtraEditors.TileItem();
            this.itDanhgiaKH = new DevExpress.XtraEditors.TileItem();
            this.itTheodoihopdong = new DevExpress.XtraEditors.TileItem();
            this.itsieuthi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup16 = new DevExpress.XtraEditors.TileGroup();
            this.itTrasua = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 687);
            this.panelControl1.TabIndex = 1;
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup15);
            this.tileControl1.Groups.Add(this.tileGroup16);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 36;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 272;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 683);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itLuu);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itLuu
            // 
            this.itLuu.BackgroundImage = global::KP_RES.Properties.Resources.Saveactive;
            this.itLuu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Lưu";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itLuu.Elements.Add(tileItemElement1);
            this.itLuu.Id = 5;
            this.itLuu.Name = "itLuu";
            this.itLuu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLuu_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itKho);
            this.tileGroup11.Items.Add(this.itDinhluong);
            this.tileGroup11.Items.Add(this.itManhinhbep);
            this.tileGroup11.Items.Add(this.itKHTT);
            this.tileGroup11.Items.Add(this.itThethanhtoannoibo);
            this.tileGroup11.Items.Add(this.itNhahang);
            this.tileGroup11.Items.Add(this.itKaraok);
            this.tileGroup11.Items.Add(this.itKhachsan);
            this.tileGroup11.Items.Add(this.itTrochoi);
            this.tileGroup11.Items.Add(this.itRapphim);
            this.tileGroup11.Items.Add(this.itKhuyenmai);
            this.tileGroup11.Items.Add(this.itCongno);
            this.tileGroup11.Items.Add(this.itVoucher);
            this.tileGroup11.Items.Add(this.itFastfood);
            this.tileGroup11.Items.Add(this.itMenu);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itKho
            // 
            this.itKho.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho2;
            this.itKho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Quản lý kho";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itKho.Elements.Add(tileItemElement2);
            this.itKho.Id = 12;
            this.itKho.IsLarge = true;
            this.itKho.Name = "itKho";
            this.itKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKho_ItemClick);
            // 
            // itDinhluong
            // 
            this.itDinhluong.BackgroundImage = global::KP_RES.Properties.Resources.dinhluong;
            this.itDinhluong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Định lượng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itDinhluong.Elements.Add(tileItemElement3);
            this.itDinhluong.Id = 13;
            this.itDinhluong.IsLarge = true;
            this.itDinhluong.Name = "itDinhluong";
            this.itDinhluong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDinhluong_ItemClick);
            // 
            // itManhinhbep
            // 
            this.itManhinhbep.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep;
            this.itManhinhbep.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Màn hình bếp";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itManhinhbep.Elements.Add(tileItemElement4);
            this.itManhinhbep.Id = 0;
            this.itManhinhbep.IsLarge = true;
            this.itManhinhbep.Name = "itManhinhbep";
            this.itManhinhbep.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itManhinhbep_ItemClick);
            // 
            // itKHTT
            // 
            this.itKHTT.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Khách hàng thân thiết";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT.Elements.Add(tileItemElement5);
            this.itKHTT.Id = 14;
            this.itKHTT.IsLarge = true;
            this.itKHTT.Name = "itKHTT";
            this.itKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_ItemClick);
            // 
            // itThethanhtoannoibo
            // 
            this.itThethanhtoannoibo.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itThethanhtoannoibo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Thẻ thanh toán nội bộ";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itThethanhtoannoibo.Elements.Add(tileItemElement6);
            this.itThethanhtoannoibo.Id = 22;
            this.itThethanhtoannoibo.IsLarge = true;
            this.itThethanhtoannoibo.Name = "itThethanhtoannoibo";
            this.itThethanhtoannoibo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThethanhtoannoibo_ItemClick);
            // 
            // itNhahang
            // 
            this.itNhahang.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            this.itNhahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Nhà hàng - Cà phê";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhahang.Elements.Add(tileItemElement7);
            this.itNhahang.Id = 19;
            this.itNhahang.IsLarge = true;
            this.itNhahang.Name = "itNhahang";
            this.itNhahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhahang_ItemClick);
            // 
            // itKaraok
            // 
            this.itKaraok.BackgroundImage = global::KP_RES.Properties.Resources.banhang3;
            this.itKaraok.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Karaok - Bida";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKaraok.Elements.Add(tileItemElement8);
            this.itKaraok.Id = 20;
            this.itKaraok.IsLarge = true;
            this.itKaraok.Name = "itKaraok";
            this.itKaraok.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKaraok_ItemClick);
            // 
            // itKhachsan
            // 
            this.itKhachsan.BackgroundImage = global::KP_RES.Properties.Resources.banhang5;
            this.itKhachsan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Khách sạn";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachsan.Elements.Add(tileItemElement9);
            this.itKhachsan.Id = 21;
            this.itKhachsan.IsLarge = true;
            this.itKhachsan.Name = "itKhachsan";
            this.itKhachsan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachsan_ItemClick);
            // 
            // itTrochoi
            // 
            this.itTrochoi.BackgroundImage = global::KP_RES.Properties.Resources.banhang6;
            this.itTrochoi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Khu vui chơi";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrochoi.Elements.Add(tileItemElement10);
            this.itTrochoi.Id = 23;
            this.itTrochoi.IsLarge = true;
            this.itTrochoi.Name = "itTrochoi";
            this.itTrochoi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrochoi_ItemClick);
            // 
            // itRapphim
            // 
            this.itRapphim.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            this.itRapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Rạp phim";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itRapphim.Elements.Add(tileItemElement11);
            this.itRapphim.Id = 24;
            this.itRapphim.IsLarge = true;
            this.itRapphim.Name = "itRapphim";
            this.itRapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itRapphim_ItemClick);
            // 
            // itKhuyenmai
            // 
            this.itKhuyenmai.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itKhuyenmai.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Khuyến mãi";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhuyenmai.Elements.Add(tileItemElement12);
            this.itKhuyenmai.Id = 25;
            this.itKhuyenmai.IsLarge = true;
            this.itKhuyenmai.Name = "itKhuyenmai";
            this.itKhuyenmai.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhuyenmai_ItemClick);
            // 
            // itCongno
            // 
            this.itCongno.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itCongno.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Công nợ";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongno.Elements.Add(tileItemElement13);
            this.itCongno.Id = 26;
            this.itCongno.IsLarge = true;
            this.itCongno.Name = "itCongno";
            this.itCongno.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongno_ItemClick);
            // 
            // itVoucher
            // 
            this.itVoucher.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itVoucher.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Voucher";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itVoucher.Elements.Add(tileItemElement14);
            this.itVoucher.Id = 27;
            this.itVoucher.IsLarge = true;
            this.itVoucher.Name = "itVoucher";
            this.itVoucher.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVoucher_ItemClick);
            // 
            // itFastfood
            // 
            this.itFastfood.BackgroundImage = global::KP_RES.Properties.Resources.nhahang28;
            this.itFastfood.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "FastFood";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itFastfood.Elements.Add(tileItemElement15);
            this.itFastfood.Id = 28;
            this.itFastfood.IsLarge = true;
            this.itFastfood.Name = "itFastfood";
            this.itFastfood.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itFastfood_ItemClick);
            // 
            // itMenu
            // 
            this.itMenu.BackgroundImage = global::KP_RES.Properties.Resources.bep3;
            this.itMenu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Menu Display";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itMenu.Elements.Add(tileItemElement16);
            this.itMenu.Id = 29;
            this.itMenu.IsLarge = true;
            this.itMenu.Name = "itMenu";
            this.itMenu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMenu_ItemClick);
            // 
            // tileGroup15
            // 
            this.tileGroup15.Items.Add(this.itCongvan);
            this.tileGroup15.Items.Add(this.itNhansu);
            this.tileGroup15.Items.Add(this.itDanhgiaKH);
            this.tileGroup15.Items.Add(this.itTheodoihopdong);
            this.tileGroup15.Items.Add(this.itsieuthi);
            this.tileGroup15.Name = "tileGroup15";
            // 
            // itCongvan
            // 
            this.itCongvan.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCongvan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement17.Text = "Công văn";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongvan.Elements.Add(tileItemElement17);
            this.itCongvan.Id = 30;
            this.itCongvan.IsLarge = true;
            this.itCongvan.Name = "itCongvan";
            this.itCongvan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // itNhansu
            // 
            this.itNhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement18.Text = "Nhân sự";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement18.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhansu.Elements.Add(tileItemElement18);
            this.itNhansu.Id = 31;
            this.itNhansu.IsLarge = true;
            this.itNhansu.Name = "itNhansu";
            this.itNhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // itDanhgiaKH
            // 
            this.itDanhgiaKH.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itDanhgiaKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement19.Text = "Đánh giá khách hàng";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement19.TextLocation = new System.Drawing.Point(0, 0);
            this.itDanhgiaKH.Elements.Add(tileItemElement19);
            this.itDanhgiaKH.Id = 32;
            this.itDanhgiaKH.IsLarge = true;
            this.itDanhgiaKH.Name = "itDanhgiaKH";
            this.itDanhgiaKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDanhgiaKH_ItemClick);
            // 
            // itTheodoihopdong
            // 
            this.itTheodoihopdong.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itTheodoihopdong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement20.Text = "Theo dõi hợp đồng";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement20.TextLocation = new System.Drawing.Point(0, 0);
            this.itTheodoihopdong.Elements.Add(tileItemElement20);
            this.itTheodoihopdong.Id = 33;
            this.itTheodoihopdong.IsLarge = true;
            this.itTheodoihopdong.Name = "itTheodoihopdong";
            this.itTheodoihopdong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTheodoihopdong_ItemClick);
            // 
            // itsieuthi
            // 
            this.itsieuthi.BackgroundImage = global::KP_RES.Properties.Resources.banhang3;
            this.itsieuthi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement21.Text = "Siêu thị";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement21.TextLocation = new System.Drawing.Point(0, 0);
            this.itsieuthi.Elements.Add(tileItemElement21);
            this.itsieuthi.Id = 34;
            this.itsieuthi.IsLarge = true;
            this.itsieuthi.Name = "itsieuthi";
            this.itsieuthi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itsieuthi_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup13
            // 
            this.tileGroup13.Name = "tileGroup13";
            // 
            // tileGroup14
            // 
            this.tileGroup14.Name = "tileGroup14";
            // 
            // tileItem1
            // 
            this.tileItem1.BackgroundImage = global::KP_RES.Properties.Resources.dangky1;
            this.tileItem1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement23.Text = "Đăng ký sử dụng phần mềm";
            tileItemElement23.TextLocation = new System.Drawing.Point(0, 0);
            this.tileItem1.Elements.Add(tileItemElement23);
            this.tileItem1.Id = 11;
            this.tileItem1.IsLarge = true;
            this.tileItem1.Name = "tileItem1";
            // 
            // tileGroup16
            // 
            this.tileGroup16.Items.Add(this.itTrasua);
            this.tileGroup16.Name = "tileGroup16";
            // 
            // itTrasua
            // 
            this.itTrasua.BackgroundImage = global::KP_RES.Properties.Resources.banhang6;
            this.itTrasua.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement22.Text = "Trà sữa";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement22.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrasua.Elements.Add(tileItemElement22);
            this.itTrasua.Id = 35;
            this.itTrasua.IsLarge = true;
            this.itTrasua.Name = "itTrasua";
            this.itTrasua.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrasua_ItemClick);
            // 
            // Frm_Kichhoatchucnang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 687);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Kichhoatchucnang";
            this.Text = "Kích hoạt chức năng";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Kichhoatchucnang_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itLuu;
        private DevExpress.XtraEditors.TileItem itKho;
        private DevExpress.XtraEditors.TileItem itDinhluong;
        private DevExpress.XtraEditors.TileItem itManhinhbep;
        private DevExpress.XtraEditors.TileItem itKHTT;
        private DevExpress.XtraEditors.TileItem itNhahang;
        private DevExpress.XtraEditors.TileItem itKaraok;
        private DevExpress.XtraEditors.TileItem itKhachsan;
        private DevExpress.XtraEditors.TileItem itTrochoi;
        private DevExpress.XtraEditors.TileItem itRapphim;
        private DevExpress.XtraEditors.TileItem itThethanhtoannoibo;
        private DevExpress.XtraEditors.TileItem itKhuyenmai;
        private DevExpress.XtraEditors.TileItem itCongno;
        private DevExpress.XtraEditors.TileItem itVoucher;
        private DevExpress.XtraEditors.TileItem itFastfood;
        private DevExpress.XtraEditors.TileItem itMenu;
        private DevExpress.XtraEditors.TileGroup tileGroup15;
        private DevExpress.XtraEditors.TileItem itCongvan;
        private DevExpress.XtraEditors.TileItem itNhansu;
        private DevExpress.XtraEditors.TileItem itDanhgiaKH;
        private DevExpress.XtraEditors.TileItem itTheodoihopdong;
        private DevExpress.XtraEditors.TileItem itsieuthi;
        private DevExpress.XtraEditors.TileGroup tileGroup16;
        private DevExpress.XtraEditors.TileItem itTrasua;



    }
}