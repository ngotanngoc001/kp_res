﻿namespace KP_RES 
{
    partial class Frm_Baocao_Hanghoa 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoDSHH = new DevExpress.XtraEditors.TileItem();
            this.itInmavachhang = new DevExpress.XtraEditors.TileItem();
            this.itDinhluong = new DevExpress.XtraEditors.TileItem();
            this.itCombo = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itCanhbaotonkho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocaoBanggiatheokhu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.itTonkho = new DevExpress.XtraEditors.TileItem();
            this.itInPLU = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 47;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 620);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itBaocaoDSHH);
            this.tileGroup8.Items.Add(this.itInmavachhang);
            this.tileGroup8.Items.Add(this.itDinhluong);
            this.tileGroup8.Items.Add(this.itCombo);
            this.tileGroup8.Items.Add(this.itInPLU);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itBaocaoDSHH
            // 
            this.itBaocaoDSHH.BackgroundImage = global::KP_RES.Properties.Resources.nhomhanghanghoa;
            this.itBaocaoDSHH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Danh sách hàng hóa";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoDSHH.Elements.Add(tileItemElement1);
            this.itBaocaoDSHH.Id = 26;
            this.itBaocaoDSHH.IsLarge = true;
            this.itBaocaoDSHH.Name = "itBaocaoDSHH";
            this.itBaocaoDSHH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoDSHH_ItemClick);
            // 
            // itInmavachhang
            // 
            this.itInmavachhang.BackgroundImage = global::KP_RES.Properties.Resources.inmavach;
            this.itInmavachhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "In mã vạch hàng hóa";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itInmavachhang.Elements.Add(tileItemElement2);
            this.itInmavachhang.Id = 31;
            this.itInmavachhang.IsLarge = true;
            this.itInmavachhang.Name = "itInmavachhang";
            this.itInmavachhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itInmavachhang_ItemClick);
            // 
            // itDinhluong
            // 
            this.itDinhluong.BackgroundImage = global::KP_RES.Properties.Resources.dinhluong;
            this.itDinhluong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Định lượng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itDinhluong.Elements.Add(tileItemElement3);
            this.itDinhluong.Id = 44;
            this.itDinhluong.IsLarge = true;
            this.itDinhluong.Name = "itDinhluong";
            this.itDinhluong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDinhluong_ItemClick);
            // 
            // itCombo
            // 
            this.itCombo.BackgroundImage = global::KP_RES.Properties.Resources.showtime;
            this.itCombo.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Combo";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itCombo.Elements.Add(tileItemElement4);
            this.itCombo.Id = 45;
            this.itCombo.IsLarge = true;
            this.itCombo.Name = "itCombo";
            this.itCombo.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCombo_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itCanhbaotonkho);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itCanhbaotonkho
            // 
            this.itCanhbaotonkho.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itCanhbaotonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Cảnh báo tồn kho";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itCanhbaotonkho.Elements.Add(tileItemElement6);
            this.itCanhbaotonkho.Id = 32;
            this.itCanhbaotonkho.IsLarge = true;
            this.itCanhbaotonkho.Name = "itCanhbaotonkho";
            this.itCanhbaotonkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCanhbaotonkho_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBaocaoBanggiatheokhu);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBaocaoBanggiatheokhu
            // 
            this.itBaocaoBanggiatheokhu.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itBaocaoBanggiatheokhu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Bảng giá theo khu";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocaoBanggiatheokhu.Elements.Add(tileItemElement7);
            this.itBaocaoBanggiatheokhu.Id = 27;
            this.itBaocaoBanggiatheokhu.IsLarge = true;
            this.itBaocaoBanggiatheokhu.Name = "itBaocaoBanggiatheokhu";
            this.itBaocaoBanggiatheokhu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocaoBanggiatheokhu_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 624);
            this.panelControl1.TabIndex = 1;
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Tồn kho";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement8);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            // 
            // itTonkho
            // 
            this.itTonkho.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itTonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Tồn kho";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itTonkho.Elements.Add(tileItemElement9);
            this.itTonkho.Id = 17;
            this.itTonkho.IsLarge = true;
            this.itTonkho.Name = "itTonkho";
            // 
            // itInPLU
            // 
            this.itInPLU.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhkettien;
            this.itInPLU.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Xuất mã PLU";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itInPLU.Elements.Add(tileItemElement5);
            this.itInPLU.Id = 46;
            this.itInPLU.IsLarge = true;
            this.itInPLU.Name = "itInPLU";
            this.itInPLU.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itInPLU_ItemClick);
            // 
            // Frm_Baocao_Hanghoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 624);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Baocao_Hanghoa";
            this.Text = "Báo cáo hàng hóa";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Baocao_Hanghoa_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itTonkho;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itBaocaoDSHH;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itInmavachhang;
        private DevExpress.XtraEditors.TileItem itCanhbaotonkho;
        private DevExpress.XtraEditors.TileItem itDinhluong;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBaocaoBanggiatheokhu;
        private DevExpress.XtraEditors.TileItem itCombo;
        private DevExpress.XtraEditors.TileItem itInPLU;



    }
}