﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_License;

namespace KP_RES 
{
    public partial class Frm_Kichhoatchucnang : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Kichhoatchucnang()
        {
            InitializeComponent();
        }

        private void Frm_Kichhoatchucnang_Paint(object sender, PaintEventArgs e)
        {
            LoadActive();
        }


        private void itKho_ItemClick(object sender, TileItemEventArgs e)
        {
            itKho.Checked = !itKho.Checked;
        }

        private void itDinhluong_ItemClick(object sender, TileItemEventArgs e)
        {
            itDinhluong.Checked = !itDinhluong.Checked;
        }

        private void itManhinhbep_ItemClick(object sender, TileItemEventArgs e)
        {
            itManhinhbep.Checked = !itManhinhbep.Checked;
        }

        private void itKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            itKHTT.Checked = !itKHTT.Checked;
        }

        private void itNhahang_ItemClick(object sender, TileItemEventArgs e)
        {
            itNhahang.Checked = !itNhahang.Checked;
        }

        private void itKaraok_ItemClick(object sender, TileItemEventArgs e)
        {
            itKaraok.Checked = !itKaraok.Checked;
        }

        private void itKhachsan_ItemClick(object sender, TileItemEventArgs e)
        {
            itKhachsan.Checked = !itKhachsan.Checked;
        }

        private void itThethanhtoannoibo_ItemClick(object sender, TileItemEventArgs e)
        {
            itThethanhtoannoibo.Checked = !itThethanhtoannoibo.Checked;
        }

        private void itTrochoi_ItemClick(object sender, TileItemEventArgs e)
        {
            itTrochoi.Checked = !itTrochoi.Checked;
        }

        private void itRapphim_ItemClick(object sender, TileItemEventArgs e)
        {
            itRapphim.Checked = !itRapphim.Checked;
        }

        private void itKhuyenmai_ItemClick(object sender, TileItemEventArgs e)
        {
            itKhuyenmai.Checked = !itKhuyenmai.Checked;
        }

        private void itCongno_ItemClick(object sender, TileItemEventArgs e)
        {
            itCongno.Checked = !itCongno.Checked;
        }

        private void itVoucher_ItemClick(object sender, TileItemEventArgs e)
        {
            itVoucher.Checked = !itVoucher.Checked;
        }

        private void itFastfood_ItemClick(object sender, TileItemEventArgs e)
        {
            itFastfood.Checked = !itFastfood.Checked;
        }

        private void itMenu_ItemClick(object sender, TileItemEventArgs e)
        {
            itMenu.Checked = !itMenu.Checked;
        }


        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            itCongvan.Checked = !itCongvan.Checked;
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            itNhansu.Checked = !itNhansu.Checked;
        }

        private void itDanhgiaKH_ItemClick(object sender, TileItemEventArgs e)
        {
            itDanhgiaKH.Checked = !itDanhgiaKH.Checked;
        }

        private void itTheodoihopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            itTheodoihopdong.Checked = !itTheodoihopdong.Checked;
        }

        private void itsieuthi_ItemClick(object sender, TileItemEventArgs e)
        {
            itsieuthi.Checked = !itsieuthi.Checked;
        }

        private void itTrasua_ItemClick(object sender, TileItemEventArgs e)
        {
            itTrasua.Checked = !itTrasua.Checked;
        }

        private void itLuu_ItemClick(object sender, TileItemEventArgs e)
        {
           cls_KP_RES . LoadDefaultLayout();

            bool bRunSQL = false;
            //Phân hệ kho
            bRunSQL = clsUserManagement.SaveActive("1", itKho.Checked);
            //Định lượng
            bRunSQL = clsUserManagement.SaveActive("2", itDinhluong.Checked);
            //Màn hình bếp
            bRunSQL = clsUserManagement.SaveActive("3", itManhinhbep.Checked);
            //Khách hàng thân thiết
            bRunSQL = clsUserManagement.SaveActive("4", itKHTT.Checked);
            //Nhà hàng
            bRunSQL = clsUserManagement.SaveActive("5", itNhahang.Checked);
            //Karaok
            bRunSQL = clsUserManagement.SaveActive("6", itKaraok.Checked);
            //Khách sạn
            bRunSQL = clsUserManagement.SaveActive("7", itKhachsan.Checked);
            //Thẻ thanh toán nội bộ
            bRunSQL = clsUserManagement.SaveActive("8", itThethanhtoannoibo.Checked);
            //Trò chơi
            bRunSQL = clsUserManagement.SaveActive("9", itTrochoi.Checked);
            //Rạp phim
            bRunSQL = clsUserManagement.SaveActive("10", itRapphim.Checked);
            //Khuyến mãi
            bRunSQL = clsUserManagement.SaveActive("11", itKhuyenmai.Checked);
            //Công nợ
            bRunSQL = clsUserManagement.SaveActive("12", itCongno.Checked);
            //Voucher
            bRunSQL = clsUserManagement.SaveActive("13", itVoucher.Checked);
            //Fastfood
            bRunSQL = clsUserManagement.SaveActive("14", itFastfood.Checked);
            //Menu
            bRunSQL = clsUserManagement.SaveActive("15", itMenu.Checked);
            //Công van
            bRunSQL = clsUserManagement.SaveActive("16", itCongvan.Checked);
            //Nhân sự
            bRunSQL = clsUserManagement.SaveActive("17", itNhansu .Checked);
            //Đánh giá khách hàng
            bRunSQL = clsUserManagement.SaveActive("220716", itDanhgiaKH .Checked);
            //Theo dõi hợp đồng
            bRunSQL = clsUserManagement.SaveActive("300816", itTheodoihopdong.Checked);
            //Sieu thị
            bRunSQL = clsUserManagement.SaveActive("220417", itsieuthi.Checked);
            //Trà sữa
            bRunSQL = clsUserManagement.SaveActive("251217", itTrasua.Checked);
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công .Phần mềm sẻ khởi động lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadActive()
        {
            itKho.Checked = clsUserManagement.CheckActive("1");
            itDinhluong.Checked = clsUserManagement.CheckActive("2");
            itManhinhbep.Checked = clsUserManagement.CheckActive("3");
            itKHTT.Checked = clsUserManagement.CheckActive("4");
            itNhahang.Checked = clsUserManagement.CheckActive("5");
            itKaraok.Checked = clsUserManagement.CheckActive("6");
            itKhachsan.Checked = clsUserManagement.CheckActive("7");
            itThethanhtoannoibo.Checked = clsUserManagement.CheckActive("8");
            itTrochoi.Checked = clsUserManagement.CheckActive("9");
            itRapphim.Checked = clsUserManagement.CheckActive("10");
            itKhuyenmai.Checked = clsUserManagement.CheckActive("11");
            itCongno.Checked = clsUserManagement.CheckActive("12");
            itVoucher.Checked = clsUserManagement.CheckActive("13");
            itFastfood.Checked = clsUserManagement.CheckActive("14");
            itMenu.Checked = clsUserManagement.CheckActive("15");
            itCongvan.Checked = clsUserManagement.CheckActive("16");
            itNhansu.Checked = clsUserManagement.CheckActive("17");
            itDanhgiaKH .Checked = clsUserManagement.CheckActive("220716");
            itTheodoihopdong.Checked = clsUserManagement.CheckActive("300816");
            itsieuthi.Checked = clsUserManagement.CheckActive("220417");
            itTrasua.Checked = clsUserManagement.CheckActive("251217");
        }

        

        

       

        

      

       
     
     
       

       

        
    }
}