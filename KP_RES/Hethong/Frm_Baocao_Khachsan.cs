﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Khachsan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Khachsan()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Khachsan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaoCaoDanhSachHoaDon_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDanhSachHoaDon.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDon_Khachsan>();
            }
        }

        private void itBaoCaoDanhSachHoaDonInLai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDanhSachHoaDonInLai.Checked)
            {
                this.Parent.GoTo<Frm_BC_InlaiHoadon_Khachsan>();
            }
        }

        private void itBaoCaoDichVu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDichVu.Checked)
            {
                this.Parent.GoTo<Frm_BC_Banhang_Khachsan>();
            }
        }

        private void itKetQuaKinhDoanh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKetQuaKinhDoanh.Checked)
            {
                this.Parent.GoTo<Frm_BC_KQKD_Khachsan>();
            }
        }

        private void itBaoCaoDoanhSo_ItemClick_1(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDoanhSo.Checked)
            {
                this.Parent.GoTo<Frm_BC_Doanhso_KhachSan>();
            }
        }

        private void itThongKeDoanhSo_ItemClick_1(object sender, TileItemEventArgs e)
        {
            if (itThongKeDoanhSo.Checked)
            {
                this.Parent.GoTo<Frm_TK_Doanhso_KhachSan>();
            }
        }

        private void itThongKeBanHang_ItemClick_1(object sender, TileItemEventArgs e)
        {
            if (itThongKeBanHang.Checked)
            {
                this.Parent.GoTo<Frm_TK_Hangbanchay_KhachSan>();
            }
        }

        private void itHuyPhong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuyPhong.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyPhong>();
            }
        }

        private void itHuyDichVu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuyDichVu.Checked)
            {
                this.Parent.GoTo<Frm_DSHuyDichVu>();
            }
        }

        private void itBaoCaoDanhSachHoaDon1_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDanhSachHoaDon1.Checked)
            {
                this.Parent.GoTo<Frm_DSHoaDon_Khachsan1>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaoCaoDanhSachHoaDon);
            cls_KP_RES.RemoveTileItem(itBaoCaoDichVu);
            cls_KP_RES.RemoveTileItem(itBaoCaoDoanhSo);
            cls_KP_RES.RemoveTileItem(itKetQuaKinhDoanh);
            cls_KP_RES.RemoveTileItem(itThongKeDoanhSo);
            cls_KP_RES.RemoveTileItem(itThongKeBanHang);
            cls_KP_RES.RemoveTileItem(itBaoCaoDanhSachHoaDonInLai);
            cls_KP_RES.RemoveTileItem(itHuyPhong);
            cls_KP_RES.RemoveTileItem(itHuyDichVu);
            cls_KP_RES.RemoveTileItem(itBaoCaoDanhSachHoaDon1);
        }

        private void LoadPermission()
        {
            itBaoCaoDanhSachHoaDon.Checked = clsUserManagement.AllowView("28"); ;
            itBaoCaoDanhSachHoaDonInLai.Checked = clsUserManagement.AllowView("28"); ;
            itBaoCaoDichVu.Checked = clsUserManagement.AllowView("29"); ;
            itBaoCaoDoanhSo.Checked = false;
            itKetQuaKinhDoanh.Checked = clsUserManagement.AllowView("31"); ;
            itThongKeDoanhSo.Checked = clsUserManagement.AllowView("103"); ;
            itThongKeBanHang.Checked = clsUserManagement.AllowView("103"); ;
            itHuyPhong.Checked = clsUserManagement.AllowView("28") && clsUserManagement.CheckActive("7"); 
            itHuyDichVu.Checked = clsUserManagement.AllowView("28") && clsUserManagement.CheckActive("7");
            itBaoCaoDanhSachHoaDon1.Checked = clsUserManagement.AllowView("28"); ;
        }

      
    }
}