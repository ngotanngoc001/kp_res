﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao()
        {
            InitializeComponent();
            this.Text = rm.GetString("baocao", culture);
            tileControl1.Text = rm.GetString("baocao", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itHanghoa_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHanghoa.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Hanghoa>();
            }
        }

        private void itNhapxuat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapxuat.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhapxuat>();
            }
        }

        private void itBanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanhang.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Banhang>();
            }
        }

        private void itVe_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVe.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Banve>();
            }
        }

        private void itKHTT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKHTT.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_KHTT>();
            }
        }

        private void itCongno_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongno.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Congno>();
            }
        }

        private void itVeTroChoi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVeTroChoi.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Banve_Trochoi>();
            }
        }

        private void itKaraoke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKaraoke.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Banhang_Karaoke>();
            }
        }

        private void itNguoidung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNguoidung.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_User>();
            }
        }

        private void itBaocaodacthu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaodacthu.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Datthu>();
            }
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_CongVan>();
            }
        }

        private void itVoucher_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itVoucher.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Voucher>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhansu.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhansu >();
            }
        }

        private void itDanhgiaKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDanhgiaKH.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_DanhGia>();
            }
        }

        private void itTheodoihopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTheodoihopdong.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Sotheodoi>();
            }
        }

        private void itMakhuyenmai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMakhuyenmai.Checked)
            {
                this.Parent.GoTo<Frm_BC_TheKhuyenMai>();
            }
        }

        private void itKhachsan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhachsan.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Khachsan>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itHanghoa );
            cls_KP_RES.RemoveTileItem(itNhapxuat);
            cls_KP_RES.RemoveTileItem(itBanhang);
            cls_KP_RES.RemoveTileItem(itVe);
            cls_KP_RES.RemoveTileItem(itKHTT);
            cls_KP_RES.RemoveTileItem(itCongno);
            cls_KP_RES.RemoveTileItem(itVeTroChoi);
            cls_KP_RES.RemoveTileItem(itKaraoke);
            cls_KP_RES.RemoveTileItem(itNguoidung);
            cls_KP_RES.RemoveTileItem(itBaocaodacthu);
            cls_KP_RES.RemoveTileItem(itCongvan);
            cls_KP_RES.RemoveTileItem(itVoucher );
            cls_KP_RES.RemoveTileItem(itNhansu);
            cls_KP_RES.RemoveTileItem(itDanhgiaKH );
            cls_KP_RES.RemoveTileItem(itTheodoihopdong);
            cls_KP_RES.RemoveTileItem(itMakhuyenmai);
            cls_KP_RES.RemoveTileItem(itKhachsan);
        }

        private void LoadPermission()
        {
            itCongvan.Checked = clsUserManagement.AllowView("24031602") && clsUserManagement.CheckActive("16");
            itHanghoa.Checked = clsUserManagement.AllowView("48") || clsUserManagement.AllowView("50") || clsUserManagement.AllowView("56") || clsUserManagement.AllowView("57");
            itNhapxuat.Checked = (clsUserManagement.AllowView("17") || clsUserManagement.AllowView("26") || clsUserManagement.AllowView("47") || clsUserManagement.AllowView("27")) && (clsUserManagement.CheckActive("1"));
            itBanhang.Checked = (clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31")) && (clsUserManagement.CheckActive("5")||clsUserManagement.CheckActive("14")||clsUserManagement.CheckActive("220417"));
            itKHTT.Checked = (clsUserManagement.AllowView("54") || clsUserManagement.AllowView("55") || clsUserManagement.AllowView("59")) && (clsUserManagement.CheckActive("4") || clsUserManagement.CheckActive("8"));
            itVe.Checked = (clsUserManagement.AllowView("81") || clsUserManagement.AllowView("82") || clsUserManagement.AllowView("83") || clsUserManagement.AllowView("84") || clsUserManagement.AllowView("85") || clsUserManagement.AllowView("86") || clsUserManagement.AllowView("87")) && (clsUserManagement.CheckActive("10") );
            itVeTroChoi.Checked = (clsUserManagement.AllowView("64") || clsUserManagement.AllowView("65") || clsUserManagement.AllowView("66")) && (clsUserManagement.CheckActive("9"));
            itCongno.Checked = (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98")) && clsUserManagement.CheckActive("12");
            itKaraoke.Checked = (clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31") || clsUserManagement.AllowView("103") ) && clsUserManagement.CheckActive("6");
            itNguoidung.Checked = clsUserManagement.AllowView("11051502");
            itBaocaodacthu.Checked = clsUserManagement.AllowView("2108201501");
            itVoucher.Checked = ((clsUserManagement.AllowView("1208201501")) && clsUserManagement.CheckActive("13")); ;
            itNhansu.Checked = (clsUserManagement.AllowView("21051601") || clsUserManagement.AllowView("21051602") || clsUserManagement.AllowView("21051603") || clsUserManagement.AllowView("21051604") || clsUserManagement.AllowView("21051605")) && clsUserManagement.CheckActive("17");
            itDanhgiaKH.Checked = ((clsUserManagement.AllowView("22071603")) && clsUserManagement.CheckActive("220716")); ;
            itTheodoihopdong.Checked = ((clsUserManagement.AllowView("30081603")) && clsUserManagement.CheckActive("300816")); ;
            itMakhuyenmai.Checked = cls_KP_RES.Mode == 2 ? true : false;
            itKhachsan.Checked = (clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31")) && (clsUserManagement.CheckActive("7"));
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
           
            itHanghoa.Text = rm.GetString("hanghoa", culture);
            itBanhang.Text = rm.GetString("banhang", culture);
            itNhapxuat.Text = rm.GetString("nhapxuat", culture);

            this.Text = rm.GetString("baocao", culture);
            tileControl1.Text = rm.GetString("baocao", culture);
            itKHTT.Text = rm.GetString("khtt", culture);
        }

      

        

       
      

      
        

       

        
    }
}