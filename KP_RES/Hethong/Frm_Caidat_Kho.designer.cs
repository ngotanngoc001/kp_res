﻿namespace KP_RES 
{
    partial class Frm_Caidat_Kho 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itCuaHangKhoQuay = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itDonViTinh = new DevExpress.XtraEditors.TileItem();
            this.itNhomHangHangHoa = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itNhaCungCapKhachHang = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 64;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itCuaHangKhoQuay);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itCuaHangKhoQuay
            // 
            this.itCuaHangKhoQuay.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itCuaHangKhoQuay.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Cửa hàng - Kho - Quầy";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itCuaHangKhoQuay.Elements.Add(tileItemElement1);
            this.itCuaHangKhoQuay.Id = 60;
            this.itCuaHangKhoQuay.IsLarge = true;
            this.itCuaHangKhoQuay.Name = "itCuaHangKhoQuay";
            this.itCuaHangKhoQuay.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCuaHangKhoQuay_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itDonViTinh);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itNhomHangHangHoa);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itDonViTinh
            // 
            this.itDonViTinh.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itDonViTinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Đơn vị tính";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDonViTinh.Elements.Add(tileItemElement2);
            this.itDonViTinh.Id = 63;
            this.itDonViTinh.IsLarge = true;
            this.itDonViTinh.Name = "itDonViTinh";
            this.itDonViTinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDonViTinh_ItemClick);
            // 
            // itNhomHangHangHoa
            // 
            this.itNhomHangHangHoa.BackgroundImage = global::KP_RES.Properties.Resources.nhomhanghanghoa;
            this.itNhomHangHangHoa.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Nhóm hàng - Hàng hóa";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhomHangHangHoa.Elements.Add(tileItemElement3);
            this.itNhomHangHangHoa.Id = 61;
            this.itNhomHangHangHoa.IsLarge = true;
            this.itNhomHangHangHoa.Name = "itNhomHangHangHoa";
            this.itNhomHangHangHoa.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhomHangHangHoa_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itNhaCungCapKhachHang);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itNhaCungCapKhachHang
            // 
            this.itNhaCungCapKhachHang.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itNhaCungCapKhachHang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Nhà cung cấp - Khách hàng";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhaCungCapKhachHang.Elements.Add(tileItemElement4);
            this.itNhaCungCapKhachHang.Id = 62;
            this.itNhaCungCapKhachHang.IsLarge = true;
            this.itNhaCungCapKhachHang.Name = "itNhaCungCapKhachHang";
            this.itNhaCungCapKhachHang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhaCungCapKhachHang_ItemClick);
            // 
            // Frm_Caidat_Kho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Kho";
            this.Text = "Cài đặt";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Kho_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itCuaHangKhoQuay;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itDonViTinh;
        private DevExpress.XtraEditors.TileItem itNhomHangHangHoa;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itNhaCungCapKhachHang;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
    }
}