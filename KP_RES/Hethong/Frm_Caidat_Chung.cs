﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_Chung : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Chung()
        {
            InitializeComponent();
            tileControl1_Paint(null, null);
            itNCCKH.ItemClick += new TileItemClickEventHandler(itNCCKH_ItemClick);
            itCHKQ.ItemClick += new TileItemClickEventHandler(itCHKQ_ItemClick);
            itNhomHangHoa.ItemClick += new TileItemClickEventHandler(itNhomHangHoa_ItemClick);
            itDonvitinh.ItemClick += new TileItemClickEventHandler(itDonvitinh_ItemClick);
            itBep.ItemClick += new TileItemClickEventHandler(itBep_ItemClick);
            itTienteTygia.ItemClick += new TileItemClickEventHandler(itTienteTygia_ItemClick);
            itCabanThietlapcaban.ItemClick += new TileItemClickEventHandler(itCabanThietlapcaban_ItemClick);

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Chung_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itNCCKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNCCKH.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_NCCKH>();
            }
        }

        private void itCHKQ_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCHKQ.Checked)
            {
                this.Parent.GoTo<Frm_CHKQ>();
            }
        }

        private void itNhomHangHoa_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhomHangHoa.Checked)
            {
                this.Parent.GoTo<Frm_NhomHanghoa>();
            }
        }

        private void itDonvitinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDonvitinh.Checked)
            {
                this.Parent.GoTo<Frm_Donvitinh>();
            }
        }

        private void itBep_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBep.Checked)
            {
                this.Parent.GoTo<Frm_Bep>();
            }
        }

        private void itTienteTygia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTienteTygia.Checked)
            {
                this.Parent.GoTo<Frm_TienteTygia>();
            }
        }

        private void itCabanThietlapcaban_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCabanThietlapcaban.Checked)
            {
                this.Parent.GoTo<Frm_CabanThietlapcaban>();
            }
        }

        private void itManhinh2_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itManhinh2.Checked)
            {
                this.Parent.GoTo<Frm_Manhinhhai>();
            }
        }

        private void itHinhmenu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHinhmenu.Checked)
            {
                this.Parent.GoTo<Frm_CaiDatHinhMenuDisPlay>();
            }
        }

        private void itCaidatMHMenu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidatMHMenu.Checked)
            {
                this.Parent.GoTo<Frm_MenuDisplay>();
            }
        }

        private void itYeucauthem_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itYeucauthem.Checked)
            {
                this.Parent.GoTo<Frm_Yeucauthem>();
            }
        }

        private void itDinhluong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDinhluong.Checked)
            {
                this.Parent.GoTo<Frm_Dinhluong>();
            }
        }

        private void itCombo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCombo.Checked)
            {
                this.Parent.GoTo<Frm_SetCombo>();
            }
        }

        private void itLapbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLapbanggia.Checked)
            {
                this.Parent.GoTo<Frm_Lapbanggia_Nhahang>();
            }
        }

        private void itApdungbanggia_cuahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia_cuahang.Checked)
            {
                this.Parent.GoTo<Frm_Apdungbanggiacuahang>();
            }
        }

        private void itHangbanonline_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHangbanonline.Checked)
            {
                this.Parent.GoTo<Frm_HangHoaOnline>();
            }
        }

        private void itBuffet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBuffet.Checked)
            {
                this.Parent.GoTo<Frm_Buffet>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCHKQ);
            cls_KP_RES.RemoveTileItem(itNhomHangHoa);
            cls_KP_RES.RemoveTileItem(itDonvitinh);
            cls_KP_RES.RemoveTileItem(itBep);
            cls_KP_RES.RemoveTileItem(itNCCKH);
            cls_KP_RES.RemoveTileItem(itTienteTygia);
            cls_KP_RES.RemoveTileItem(itCabanThietlapcaban);
            cls_KP_RES.RemoveTileItem(itManhinh2);
            cls_KP_RES.RemoveTileItem(itHinhmenu);
            cls_KP_RES.RemoveTileItem(itCaidatMHMenu);
            cls_KP_RES.RemoveTileItem(itYeucauthem);
            cls_KP_RES.RemoveTileItem(itDinhluong);
            cls_KP_RES.RemoveTileItem(itCombo);
            cls_KP_RES.RemoveTileItem(itLapbanggia);
            cls_KP_RES.RemoveTileItem(itApdungbanggia_cuahang);
            cls_KP_RES.RemoveTileItem(itHangbanonline);
            cls_KP_RES.RemoveTileItem(itBuffet );
        }

        private void LoadPermission()
        {
            itCHKQ.Checked = clsUserManagement.AllowView("4");
            itNhomHangHoa.Checked = clsUserManagement.AllowView("6");
            itDonvitinh.Checked = clsUserManagement.AllowView("7");
            itBep.Checked = clsUserManagement.AllowView("8");
            itNCCKH.Checked = clsUserManagement.AllowView("9");
            itTienteTygia.Checked = clsUserManagement.AllowView("18");
            itCabanThietlapcaban.Checked = clsUserManagement.AllowView("33");
            itManhinh2.Checked = clsUserManagement.AllowView("100");
            itHinhmenu.Checked = clsUserManagement.AllowView("6") && clsUserManagement.CheckActive("16");
            itCaidatMHMenu.Checked = clsUserManagement.AllowView("6") && clsUserManagement.CheckActive("16");
            itYeucauthem.Checked = clsUserManagement.AllowView("40");
            itDinhluong.Checked = clsUserManagement.AllowView("41") && clsUserManagement.CheckActive("2");
            itCombo.Checked = clsUserManagement.AllowView("109");
            itLapbanggia.Checked = clsUserManagement.AllowView("42");
            itApdungbanggia_cuahang.Checked = clsUserManagement.AllowView("42");
            itHangbanonline.Checked = clsUserManagement.AllowView("14031601");
            itBuffet.Checked = clsUserManagement.AllowView("29071801");

        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            itCHKQ.Text = rm.GetString("cuahangkhoquay", culture);
            itDonvitinh.Text = rm.GetString("dvt", culture);
            itBep.Text = rm.GetString("bep", culture);
            itNhomHangHoa.Text = rm.GetString("nhomhanghanghoa", culture);
            itNCCKH.Text = rm.GetString("ncckh", culture);
            itCabanThietlapcaban.Text = rm.GetString("cbtlcb", culture);
            itTienteTygia.Text = rm.GetString("tientetygia", culture);
            itYeucauthem.Text = rm.GetString("yeucauthem", culture);
            itDinhluong.Text = rm.GetString("dinhluong", culture);
            itLapbanggia.Text = rm.GetString("lapbanggia", culture);
            this.Text = rm.GetString("caidatchung", culture);
            tileControl1.Text = rm.GetString("caidatchung", culture);
            itApdungbanggia_cuahang.Text = rm.GetString("apdungbanggia", culture);
            itManhinh2.Text = rm.GetString("manhinhphu", culture);
        }

      
        
       
  

     
    }
}