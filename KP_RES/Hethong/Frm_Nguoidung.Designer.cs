﻿namespace KP_RES 
{
    partial class Frm_Nguoidung 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itBophan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itNhanvien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itNguoidung = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itNhomnguoidung = new DevExpress.XtraEditors.TileItem();
            this.itPhanquyen = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup14);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 14;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 371);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itBophan);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itBophan
            // 
            this.itBophan.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itBophan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Phòng ban";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itBophan.Elements.Add(tileItemElement1);
            this.itBophan.Id = 12;
            this.itBophan.IsLarge = true;
            this.itBophan.Name = "itBophan";
            this.itBophan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBophan_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itNhanvien);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itNhanvien
            // 
            this.itNhanvien.BackgroundImage = global::KP_RES.Properties.Resources.nhanvien;
            this.itNhanvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Nhân viên";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhanvien.Elements.Add(tileItemElement2);
            this.itNhanvien.Id = 5;
            this.itNhanvien.IsLarge = true;
            this.itNhanvien.Name = "itNhanvien";
            this.itNhanvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhanvien_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itNguoidung);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itNguoidung
            // 
            this.itNguoidung.BackgroundImage = global::KP_RES.Properties.Resources.phanquyen;
            this.itNguoidung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Tài khoản";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itNguoidung.Elements.Add(tileItemElement3);
            this.itNguoidung.Id = 4;
            this.itNguoidung.IsLarge = true;
            this.itNguoidung.Name = "itNguoidung";
            this.itNguoidung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNguoidung_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Items.Add(this.itNhomnguoidung);
            this.tileGroup14.Items.Add(this.itPhanquyen);
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itNhomnguoidung
            // 
            this.itNhomnguoidung.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itNhomnguoidung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Nhóm quyền";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhomnguoidung.Elements.Add(tileItemElement4);
            this.itNhomnguoidung.Id = 3;
            this.itNhomnguoidung.IsLarge = true;
            this.itNhomnguoidung.Name = "itNhomnguoidung";
            this.itNhomnguoidung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhomnguoidung_ItemClick);
            // 
            // itPhanquyen
            // 
            this.itPhanquyen.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itPhanquyen.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Phân quyền";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhanquyen.Elements.Add(tileItemElement5);
            this.itPhanquyen.Id = 13;
            this.itPhanquyen.IsLarge = true;
            this.itPhanquyen.Name = "itPhanquyen";
            this.itPhanquyen.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhanquyen_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileItem1
            // 
            this.tileItem1.BackgroundImage = global::KP_RES.Properties.Resources.dangky1;
            this.tileItem1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Đăng ký sử dụng phần mềm";
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.tileItem1.Elements.Add(tileItemElement6);
            this.tileItem1.Id = 11;
            this.tileItem1.IsLarge = true;
            this.tileItem1.Name = "tileItem1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 375);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_Nguoidung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 375);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Nguoidung";
            this.Text = "Quản lý người dùng";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Nguoidung_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileItem itBophan;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itNhanvien;
        private DevExpress.XtraEditors.TileItem itPhanquyen;
        private DevExpress.XtraEditors.TileItem itNguoidung;
        private DevExpress.XtraEditors.TileItem itNhomnguoidung;



    }
}