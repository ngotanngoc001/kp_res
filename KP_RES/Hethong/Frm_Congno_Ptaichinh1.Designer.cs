﻿namespace KP_RES 
{
    partial class Frm_Congno_Ptaichinh1 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itLapphieuthu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itLapphieuchi = new DevExpress.XtraEditors.TileItem();
            this.itPhieudenghiduyetchi = new DevExpress.XtraEditors.TileItem();
            this.itDuyetphieudenghichi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itThuquy = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itUynhiemchi = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 62;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 713);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itLapphieuthu);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itLapphieuthu
            // 
            this.itLapphieuthu.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho4;
            this.itLapphieuthu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Lập phiếu thu";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itLapphieuthu.Elements.Add(tileItemElement1);
            this.itLapphieuthu.Id = 54;
            this.itLapphieuthu.IsLarge = true;
            this.itLapphieuthu.Name = "itLapphieuthu";
            this.itLapphieuthu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLapphieuthu_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itLapphieuchi);
            this.tileGroup6.Items.Add(this.itPhieudenghiduyetchi);
            this.tileGroup6.Items.Add(this.itDuyetphieudenghichi);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itLapphieuchi
            // 
            this.itLapphieuchi.BackgroundImage = global::KP_RES.Properties.Resources.xuatkho4;
            this.itLapphieuchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Lập phiếu chi";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itLapphieuchi.Elements.Add(tileItemElement2);
            this.itLapphieuchi.Id = 55;
            this.itLapphieuchi.IsLarge = true;
            this.itLapphieuchi.Name = "itLapphieuchi";
            this.itLapphieuchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLapphieuchi_ItemClick);
            // 
            // itPhieudenghiduyetchi
            // 
            this.itPhieudenghiduyetchi.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itPhieudenghiduyetchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Lập phiếu đề nghị chi";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhieudenghiduyetchi.Elements.Add(tileItemElement3);
            this.itPhieudenghiduyetchi.Id = 58;
            this.itPhieudenghiduyetchi.IsLarge = true;
            this.itPhieudenghiduyetchi.Name = "itPhieudenghiduyetchi";
            this.itPhieudenghiduyetchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhieudenghiduyetchi_ItemClick);
            // 
            // itDuyetphieudenghichi
            // 
            this.itDuyetphieudenghichi.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itDuyetphieudenghichi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Duyệt phiếu đề nghị chi";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itDuyetphieudenghichi.Elements.Add(tileItemElement4);
            this.itDuyetphieudenghichi.Id = 59;
            this.itDuyetphieudenghichi.IsLarge = true;
            this.itDuyetphieudenghichi.Name = "itDuyetphieudenghichi";
            this.itDuyetphieudenghichi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDuyetphieudenghichi_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itThuquy);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itThuquy
            // 
            this.itThuquy.BackgroundImage = global::KP_RES.Properties.Resources.tonkho5;
            this.itThuquy.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thủ quỹ xác nhận";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itThuquy.Elements.Add(tileItemElement5);
            this.itThuquy.Id = 60;
            this.itThuquy.IsLarge = true;
            this.itThuquy.Name = "itThuquy";
            this.itThuquy.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThuquy_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 717);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup11
            // 
            this.tileGroup11.Name = "tileGroup11";
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itUynhiemchi);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itUynhiemchi
            // 
            this.itUynhiemchi.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhbaocao;
            this.itUynhiemchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Lập phiếu ủy nhiệm chi";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itUynhiemchi.Elements.Add(tileItemElement6);
            this.itUynhiemchi.Id = 61;
            this.itUynhiemchi.IsLarge = true;
            this.itUynhiemchi.Name = "itUynhiemchi";
            this.itUynhiemchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itUynhiemchi_ItemClick);
            // 
            // Frm_Congno_Ptaichinh1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 717);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Congno_Ptaichinh1";
            this.Text = "Công nợ thu chi";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Congno_Ptaichinh1_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itLapphieuthu;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itLapphieuchi;
        private DevExpress.XtraEditors.TileItem itPhieudenghiduyetchi;
        private DevExpress.XtraEditors.TileItem itDuyetphieudenghichi;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itThuquy;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itUynhiemchi;



    }
}