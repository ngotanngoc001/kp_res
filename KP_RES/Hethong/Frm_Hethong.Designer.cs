﻿namespace KP_RES 
{
    partial class Frm_Hethong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itDoimatkhau = new DevExpress.XtraEditors.TileItem();
            this.itNguoidung = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itSaoluudulieu = new DevExpress.XtraEditors.TileItem();
            this.itPhuchoidulieu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itDangkysudung = new DevExpress.XtraEditors.TileItem();
            this.itCapnhat = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itKichhoat = new DevExpress.XtraEditors.TileItem();
            this.itDaily = new DevExpress.XtraEditors.TileItem();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.itXoadulieu = new DevExpress.XtraEditors.TileItem();
            this.itDongbodulieu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1204, 435);
            this.panelControl1.TabIndex = 1;
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup15);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 18;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1200, 431);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itDoimatkhau);
            this.tileGroup10.Items.Add(this.itNguoidung);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itDoimatkhau
            // 
            this.itDoimatkhau.BackgroundImage = global::KP_RES.Properties.Resources.doimatkhau;
            this.itDoimatkhau.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Đổi mật khẩu";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDoimatkhau.Elements.Add(tileItemElement1);
            this.itDoimatkhau.Id = 12;
            this.itDoimatkhau.IsLarge = true;
            this.itDoimatkhau.Name = "itDoimatkhau";
            this.itDoimatkhau.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDoimatkhau_ItemClick);
            // 
            // itNguoidung
            // 
            this.itNguoidung.BackgroundImage = global::KP_RES.Properties.Resources.phanquyen;
            this.itNguoidung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Quản lý người dùng";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNguoidung.Elements.Add(tileItemElement2);
            this.itNguoidung.Id = 5;
            this.itNguoidung.IsLarge = true;
            this.itNguoidung.Name = "itNguoidung";
            this.itNguoidung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNguoidung_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itSaoluudulieu);
            this.tileGroup11.Items.Add(this.itPhuchoidulieu);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itSaoluudulieu
            // 
            this.itSaoluudulieu.BackgroundImage = global::KP_RES.Properties.Resources.saoluu1;
            this.itSaoluudulieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Sao lưu dữ liệu";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itSaoluudulieu.Elements.Add(tileItemElement3);
            this.itSaoluudulieu.Id = 3;
            this.itSaoluudulieu.IsLarge = true;
            this.itSaoluudulieu.Name = "itSaoluudulieu";
            this.itSaoluudulieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSaoluudulieu_ItemClick);
            // 
            // itPhuchoidulieu
            // 
            this.itPhuchoidulieu.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itPhuchoidulieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Phục hồi dữ liệu";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhuchoidulieu.Elements.Add(tileItemElement4);
            this.itPhuchoidulieu.Id = 4;
            this.itPhuchoidulieu.IsLarge = true;
            this.itPhuchoidulieu.Name = "itPhuchoidulieu";
            this.itPhuchoidulieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhuchoidulieu_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itDangkysudung);
            this.tileGroup12.Items.Add(this.itCapnhat);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itDangkysudung
            // 
            this.itDangkysudung.BackgroundImage = global::KP_RES.Properties.Resources.dangky1;
            this.itDangkysudung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Đăng ký sử dụng phần mềm";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itDangkysudung.Elements.Add(tileItemElement5);
            this.itDangkysudung.Id = 13;
            this.itDangkysudung.IsLarge = true;
            this.itDangkysudung.Name = "itDangkysudung";
            this.itDangkysudung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDangkysudung_ItemClick);
            // 
            // itCapnhat
            // 
            this.itCapnhat.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho1;
            this.itCapnhat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Cập nhật phần mềm";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itCapnhat.Elements.Add(tileItemElement6);
            this.itCapnhat.Id = 14;
            this.itCapnhat.IsLarge = true;
            this.itCapnhat.Name = "itCapnhat";
            this.itCapnhat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCapnhat_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itKichhoat);
            this.tileGroup13.Items.Add(this.itDaily);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itKichhoat
            // 
            this.itKichhoat.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itKichhoat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Kích hoạt chức năng";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itKichhoat.Elements.Add(tileItemElement7);
            this.itKichhoat.Id = 0;
            this.itKichhoat.IsLarge = true;
            this.itKichhoat.Name = "itKichhoat";
            this.itKichhoat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKichhoat_ItemClick);
            // 
            // itDaily
            // 
            this.itDaily.BackgroundImage = global::KP_RES.Properties.Resources.bep3;
            tileItemElement8.Text = "Đại lý";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itDaily.Elements.Add(tileItemElement8);
            this.itDaily.Id = 15;
            this.itDaily.IsLarge = true;
            this.itDaily.Name = "itDaily";
            this.itDaily.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDaily_ItemClick);
            // 
            // tileGroup15
            // 
            this.tileGroup15.Items.Add(this.itXoadulieu);
            this.tileGroup15.Items.Add(this.itDongbodulieu);
            this.tileGroup15.Name = "tileGroup15";
            // 
            // itXoadulieu
            // 
            this.itXoadulieu.BackgroundImage = global::KP_RES.Properties.Resources.saoluu;
            this.itXoadulieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Xóa dữ liệu bán hàng";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itXoadulieu.Elements.Add(tileItemElement9);
            this.itXoadulieu.Id = 16;
            this.itXoadulieu.IsLarge = true;
            this.itXoadulieu.Name = "itXoadulieu";
            this.itXoadulieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itXoadulieu_ItemClick);
            // 
            // itDongbodulieu
            // 
            this.itDongbodulieu.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itDongbodulieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Đồng bộ dữ liệu";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itDongbodulieu.Elements.Add(tileItemElement10);
            this.itDongbodulieu.Id = 17;
            this.itDongbodulieu.IsLarge = true;
            this.itDongbodulieu.Name = "itDongbodulieu";
            this.itDongbodulieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDongbodulieu_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Name = "tileGroup14";
            // 
            // tileItem1
            // 
            this.tileItem1.BackgroundImage = global::KP_RES.Properties.Resources.dangky1;
            this.tileItem1.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Đăng ký sử dụng phần mềm";
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.tileItem1.Elements.Add(tileItemElement11);
            this.tileItem1.Id = 11;
            this.tileItem1.IsLarge = true;
            this.tileItem1.Name = "tileItem1";
            // 
            // Frm_Hethong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 435);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Hethong";
            this.Text = "Hệ thống";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Hethong_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itDoimatkhau;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itKichhoat;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itNguoidung;
        private DevExpress.XtraEditors.TileItem itSaoluudulieu;
        private DevExpress.XtraEditors.TileItem itPhuchoidulieu;
        private DevExpress.XtraEditors.TileItem itDangkysudung;
        private DevExpress.XtraEditors.TileItem itCapnhat;
        private DevExpress.XtraEditors.TileItem itDaily;
        private DevExpress.XtraEditors.TileGroup tileGroup15;
        private DevExpress.XtraEditors.TileItem itXoadulieu;
        private DevExpress.XtraEditors.TileItem itDongbodulieu;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;



    }
}