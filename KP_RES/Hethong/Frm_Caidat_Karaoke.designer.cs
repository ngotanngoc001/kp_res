﻿namespace KP_RES 
{
    partial class Frm_Caidat_Karaoke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itKhuvuckaraoke = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itApdungbanggia = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itLapbanggia_Karaoke = new DevExpress.XtraEditors.TileItem();
            this.itApdungbanggia_Karaoke = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.itCauhinhblock = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 55;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 713);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itKhuvuckaraoke);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itKhuvuckaraoke
            // 
            this.itKhuvuckaraoke.BackgroundImage = global::KP_RES.Properties.Resources.khuvucban1;
            this.itKhuvuckaraoke.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Khu vực - Phòng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhuvuckaraoke.Elements.Add(tileItemElement1);
            this.itKhuvuckaraoke.Id = 15;
            this.itKhuvuckaraoke.IsLarge = true;
            this.itKhuvuckaraoke.Name = "itKhuvuckaraoke";
            this.itKhuvuckaraoke.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhuvuckaraoke_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itApdungbanggia);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itApdungbanggia
            // 
            this.itApdungbanggia.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itApdungbanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Áp dụng bảng giá hàng hóa";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itApdungbanggia.Elements.Add(tileItemElement2);
            this.itApdungbanggia.Id = 54;
            this.itApdungbanggia.IsLarge = true;
            this.itApdungbanggia.Name = "itApdungbanggia";
            this.itApdungbanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itApdungbanggia_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itCauhinhblock);
            this.tileGroup11.Items.Add(this.itLapbanggia_Karaoke);
            this.tileGroup11.Items.Add(this.itApdungbanggia_Karaoke);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itLapbanggia_Karaoke
            // 
            this.itLapbanggia_Karaoke.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itLapbanggia_Karaoke.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Lập bảng giá giờ";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itLapbanggia_Karaoke.Elements.Add(tileItemElement4);
            this.itLapbanggia_Karaoke.Id = 49;
            this.itLapbanggia_Karaoke.IsLarge = true;
            this.itLapbanggia_Karaoke.Name = "itLapbanggia_Karaoke";
            this.itLapbanggia_Karaoke.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLapbanggia_Karaoke_ItemClick);
            // 
            // itApdungbanggia_Karaoke
            // 
            this.itApdungbanggia_Karaoke.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhcongnothuchi;
            this.itApdungbanggia_Karaoke.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Áp dụng bảng giá giờ";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itApdungbanggia_Karaoke.Elements.Add(tileItemElement5);
            this.itApdungbanggia_Karaoke.Id = 50;
            this.itApdungbanggia_Karaoke.IsLarge = true;
            this.itApdungbanggia_Karaoke.Name = "itApdungbanggia_Karaoke";
            this.itApdungbanggia_Karaoke.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itApdungbanggia_Karaoke_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 717);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // itCauhinhblock
            // 
            this.itCauhinhblock.BackgroundImage = global::KP_RES.Properties.Resources.thoigian;
            this.itCauhinhblock.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Quy đổi thời gian";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhblock.Elements.Add(tileItemElement3);
            this.itCauhinhblock.Id = 53;
            this.itCauhinhblock.IsLarge = true;
            this.itCauhinhblock.Name = "itCauhinhblock";
            this.itCauhinhblock.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhblock_ItemClick);
            // 
            // Frm_Caidat_Karaoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 717);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Karaoke";
            this.Text = "Cài đặt Karaoke";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Karaoke_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itKhuvuckaraoke;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itApdungbanggia;
        private DevExpress.XtraEditors.TileItem itLapbanggia_Karaoke;
        private DevExpress.XtraEditors.TileItem itApdungbanggia_Karaoke;
        private DevExpress.XtraEditors.TileItem itCauhinhblock;



    }
}