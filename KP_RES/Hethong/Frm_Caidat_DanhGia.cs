﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_DanhGia : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_DanhGia()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Cài đặt";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_DanhGia_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itTrangThai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrangThai.Checked)
            {
                this.Parent.GoTo<Frm_DGTrangThai>();
            }
        }

        private void itLyDo_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLyDo.Checked)
            {
                this.Parent.GoTo<Frm_DGLyDo>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itTrangThai);
            cls_KP_RES.RemoveTileItem(itLyDo);
        }

        private void LoadPermission()
        {
            itTrangThai.Checked = true;
            itLyDo.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       

        
        

        

        

    }
}