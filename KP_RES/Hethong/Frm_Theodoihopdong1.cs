﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Theodoihopdong1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Theodoihopdong1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Theodoihopdong1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.Parent.GoTo<Frm_SoTheoDoiHD>();
            }
        }


        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Sotheodoi>();
            }
        }

        private void itCaidat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidat.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Sotheodoi>();
            }
        }


        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCaidat);
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itCongvan);
        }

        private void LoadPermission()
        {
            itCaidat.Checked = clsUserManagement.AllowView("30081601") && clsUserManagement.CheckActive("300816");
            itBaocao.Checked = clsUserManagement.AllowView("30081603") && clsUserManagement.CheckActive("300816");
            itCongvan.Checked = clsUserManagement.AllowView("30081602") && clsUserManagement.CheckActive("300816");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       
        

       

      

       

        

        

        

        

    }
}