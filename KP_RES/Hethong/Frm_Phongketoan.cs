﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Phongketoan : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Phongketoan()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Phongketoan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongnothuchi.Checked)
            {
                this.Parent.GoTo<Frm_Congno1>();
            }
        }

        private void itTheodoihopdong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTheodoihopdong.Checked)
            {
                this.Parent.GoTo<Frm_Theodoihopdong1>();
            }
        }
        

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCongnothuchi);
            cls_KP_RES.RemoveTileItem(itTheodoihopdong);
        }

        private void LoadPermission()
        {
            itCongnothuchi.Checked = (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("981601") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98") || clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
            itTheodoihopdong.Checked = (clsUserManagement.AllowView("30081601") || clsUserManagement.AllowView("30081602") || clsUserManagement.AllowView("30081603")) && clsUserManagement.CheckActive("300816");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

      

      

       
        

       

      

       

        

        

        

        

    }
}