﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Nhapxuat : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Nhapxuat()
        {
            InitializeComponent();
            this.Text = rm.GetString("khonhapxuat", culture);
            tileControl1.Text = rm.GetString("khonhapxuat", culture);
            LoadPermission();
            cls_KP_RES.RemoveTileItem(itNhapkho);
            cls_KP_RES.RemoveTileItem(itXuatkho);
            cls_KP_RES.RemoveTileItem(itKiemke);
        }

        private void Frm_Nhapxuat_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itNhapkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapkho.Checked)
            {
                this.Parent.GoTo<Frm_Nhapkho>();
            }
        }

        private void itXuatkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itXuatkho.Checked)
            {
                this.Parent.GoTo<Frm_Xuatkho>();
            }
        }

        private void itKiemke_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKiemke.Checked)
            {
                this.Parent.GoTo<Frm_Phieukiemke>();
            }
        }

        private void LoadPermission()
        {
            itNhapkho.Checked = clsUserManagement.AllowView("12");
            itXuatkho.Checked = clsUserManagement.AllowView("13");
            itKiemke.Checked = clsUserManagement.AllowView("14");
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            
            itNhapkho.Text = rm.GetString("nhapkho", culture);
            itXuatkho.Text = rm.GetString("xuatkho", culture);
            itKiemke.Text = rm.GetString("kiemke", culture);

            this.Text = rm.GetString("khonhapxuat", culture);
            tileControl1.Text = rm.GetString("khonhapxuat", culture);
        }
    }
}