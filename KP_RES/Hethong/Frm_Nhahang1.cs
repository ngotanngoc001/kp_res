﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Diagnostics;

namespace KP_RES 
{
    public partial class  Frm_Nhahang1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Nhahang1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Nhahang1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongVanDen_ItemClick(object sender, TileItemEventArgs e)
        {
            //if (itCongVanDen.Checked)
            //{
            //    this.Parent.GoTo< Frm_Nhahang1Den>();
            //}
        }

        private void itCongVanDi_ItemClick(object sender, TileItemEventArgs e)
        {
        //    if (itCongVanDi.Checked)
        //    {
        //        this.Parent.GoTo< Frm_Nhahang1Di>();
        //    }
        }

        private void itCaidat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidat.Checked)
            {
                this.Parent.GoTo<Frm_Caidat_Chung>();
            }
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhahang1>();
            }
        }

        private void itKho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKho.Checked)
            {
                this.Parent.GoTo<Frm_Nhapxuat>();
            }
        }

        private void itFastfood_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itFastfood.Checked)
            {
                //check bán hàng
                if (!cls_ConfigCashier.checkBanHang())
                {
                    return;
                }
                //vô bán hàng
                if (cls_KP_RES.CheckScreen())
                {
                    Frm_Manhinhhai_Nhahang frm = new Frm_Manhinhhai_Nhahang();
                    Screen[] sc = Screen.AllScreens;
                    frm.Width = sc[1].Bounds.Width;
                    frm.Height = sc[1].Bounds.Height;
                    frm.Location = new Point(this.Width, 0);
                    frm.Show();

                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.MyGetData = new Frm_Banhang_FastFood.GetString(frm.GetValue);
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                else
                {
                    Frm_Banhang_FastFood frm1 = new Frm_Banhang_FastFood();
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
        }
        
        private void itManhinhbep_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itManhinhbep.Checked)
            {
                this.Parent.GoTo<Frm_Phanhebep>();
            }
        }
        
        private void itMenudisplay_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itMenudisplay.Checked)
            {
                if (File.Exists(Application.StartupPath + "\\MenuDisplay.exe"))
                {
                    foreach (Process proc in Process.GetProcessesByName("MenuDisplay"))
                    {
                        proc.Kill();
                    }
                    Process.Start(Application.StartupPath + "\\MenuDisplay.exe");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCaidat);
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itKho);
            cls_KP_RES.RemoveTileItem(itFastfood);
            cls_KP_RES.RemoveTileItem(itManhinhbep);
            cls_KP_RES.RemoveTileItem(itMenudisplay);
        }

        private void LoadPermission()
        {
            bool bBH = (clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31")) && (clsUserManagement.CheckActive("5") || (clsUserManagement.CheckActive("14")));
            bool bHH = clsUserManagement.AllowView("48") || clsUserManagement.AllowView("50") || clsUserManagement.AllowView("56") || clsUserManagement.AllowView("57");
            bool bNX = (clsUserManagement.AllowView("17") || clsUserManagement.AllowView("26") || clsUserManagement.AllowView("47") || clsUserManagement.AllowView("27")) && (clsUserManagement.CheckActive("1"));
            itCaidat.Checked = clsUserManagement.AllowView("40") || clsUserManagement.AllowView("41") || clsUserManagement.AllowView("4") || clsUserManagement.AllowView("6") || clsUserManagement.AllowView("7") || clsUserManagement.AllowView("8") || clsUserManagement.AllowView("9") || clsUserManagement.AllowView("18") || clsUserManagement.AllowView("33") || clsUserManagement.AllowView("100");
            itBaocao.Checked = bBH ||bHH ||bNX ;
            itKho.Checked = (clsUserManagement.AllowView("12") || clsUserManagement.AllowView("13") || clsUserManagement.AllowView("14")) && clsUserManagement.CheckActive("1");
            itFastfood.Checked = clsUserManagement.AllowView("102") && clsUserManagement.CheckActive("14");
            itManhinhbep.Checked = clsUserManagement.AllowView("24") && clsUserManagement.CheckActive("3");
            itMenudisplay.Checked = clsUserManagement.AllowView("106") && clsUserManagement.CheckActive("15"); ;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

       

        

        

      
        

      
       

        

        

        

        

    }
}