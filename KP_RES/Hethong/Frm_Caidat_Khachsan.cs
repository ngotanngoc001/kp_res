﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Caidat_Khachsan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_Khachsan()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_Khachsan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKhachsanlauphong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKhachsanlauphong.Checked)
            {
               // this.Parent.GoTo<Frm_KLP>();
                this.Parent.GoTo<Frm_Lauphong>();
            }
        }

        private void itLoaiphongbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaiphongbanggia.Checked)
            {
                this.Parent.GoTo<Frm_KS_Lapbanggia>();
            }
        }

        private void itLoaiphong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaiphong.Checked)
            {
                this.Parent.GoTo<Frm_Loaiphong>();
            }
        }

        private void itApdungbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia.Checked)
            {
                this.Parent.GoTo<Frm_ApdungbanggiaKhachsan>();
            }
        }

        private void itCaidatchung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCaidatchung.Checked)
            {
                this.Parent.GoTo<Frm_CauhinhBanggiaKhachsan>();
            }
        }

        private void itApdungbanggia_Hanghoa_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itApdungbanggia_Hanghoa.Checked)
            {
                this.Parent.GoTo<Frm_Apdungbanggiahanghoa_Khachsan>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKhachsanlauphong);
            cls_KP_RES.RemoveTileItem(itLoaiphongbanggia);
            cls_KP_RES.RemoveTileItem(itLoaiphong);
            cls_KP_RES.RemoveTileItem(itApdungbanggia);
            cls_KP_RES.RemoveTileItem(itCaidatchung);
            cls_KP_RES.RemoveTileItem(itApdungbanggia_Hanghoa);
        }

        private void LoadPermission()
        {
            itKhachsanlauphong.Checked = clsUserManagement.AllowView("0906201501");
            itLoaiphong.Checked = clsUserManagement.AllowView("0906201501");
            itCaidatchung.Checked = clsUserManagement.AllowView("0906201502");
            itLoaiphongbanggia.Checked = clsUserManagement.AllowView("0906201502");
            itApdungbanggia.Checked = clsUserManagement.AllowView("0906201502");
            itApdungbanggia_Hanghoa.Checked = clsUserManagement.AllowView("0906201502") && clsUserManagement.AllowDelete("42"); ;
        }

       

    }
}