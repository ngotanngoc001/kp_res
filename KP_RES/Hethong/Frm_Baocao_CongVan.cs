﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_CongVan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_CongVan()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
            
            LoadPermission();
            VisibleTileItem();
        }

        private void itBaoCaoCongVanDen_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoCongVanDen.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_CongVanDen>();
            }
        }

        private void itBaoCaoCongVanDi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoCongVanDi.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_CongVanDi>();
            }
        }

        private void itBaoCaoBienBanGiaoNhan_ItemClick(object sender, TileItemEventArgs e)
        {
            //if (itBaoCaoBienBanGiaoNhan.Checked)
            //{
            //    this.Parent.GoTo<Frm_BaoCao_BienBanGiaoNhan>();
            //}
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaoCaoCongVanDen);
            cls_KP_RES.RemoveTileItem(itBaoCaoCongVanDi);
            //cls_KP_RES.RemoveTileItem(itBaoCaoBienBanGiaoNhan);
        }

        private void LoadPermission()
        {
            itBaoCaoCongVanDen.Checked = true;
            itBaoCaoCongVanDi.Checked = true;
           // itBaoCaoBienBanGiaoNhan.Checked = true;
        }

        

        
    }
}