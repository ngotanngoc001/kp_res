﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Datthu : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Datthu()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Datthu_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itTonghopvetrochoi_nhahang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTonghopvetrochoi_nhahang.Checked)
            {
                this.Parent.GoTo<Frm_BC_Tonghop_Trochoi_Nhahang>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itTonghopvetrochoi_nhahang);
        }

        private void LoadPermission()
        {
            itTonghopvetrochoi_nhahang.Checked = clsUserManagement.AllowView("2108201501");
        }
    }
}