﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_NCCKH : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_NCCKH()
        {
            InitializeComponent();
            this.Text = rm.GetString("ncckh1", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_NCCKH_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itLoaiDL_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaiDL.Checked)
            {
                this.Parent.GoTo<Frm_LoaiDaiLy>();
            }
        }

        private void itCapDL_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCapDL.Checked)
            {
                this.Parent.GoTo<Frm_CapDoDaiLy>();
            }
        }

        private void itNCCKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNCCKH.Checked)
            {
               this.Parent.GoTo<Frm_NCCKH>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itLoaiDL);
            cls_KP_RES.RemoveTileItem(itCapDL);
            cls_KP_RES.RemoveTileItem(itNCCKH);
         
        }

        private void LoadPermission()
        {
            itLoaiDL.Checked = clsUserManagement.AllowView("9");
            itCapDL.Checked = clsUserManagement.AllowView("9");
            itNCCKH.Checked = clsUserManagement.AllowView("9");
        }

        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            this.Text = rm.GetString("ncckh1", culture);
            itLoaiDL.Text = rm.GetString("loaidaily", culture);
            itCapDL.Text = rm.GetString("capdaily", culture);
            itNCCKH.Text = rm.GetString("ncckh", culture);
        }
    }
}