﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Congno_Ptaichinh : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Congno_Ptaichinh()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Congno_Ptaichinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Congno_Ptaichinh>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongno.Checked)
            {
                this.Parent.GoTo<Frm_Congno_Ptaichinh1>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itCongno);
        }

        private void LoadPermission()
        {
            itBaocao.Checked = ( clsUserManagement.AllowView("98")) && clsUserManagement.CheckActive("12");
            itCongno.Checked = (clsUserManagement.AllowView("981601")) && clsUserManagement.CheckActive("12");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

 
       
        

       

      

       

        

        

        

        

    }
}