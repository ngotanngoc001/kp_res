﻿namespace KP_RES 
{
    partial class  Frm_Phonghanhchinh 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itCongvan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itBienbangiaonhan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itHosocongty = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itQuytrinhlamviec = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 61;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itCongvan);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itCongvan
            // 
            this.itCongvan.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCongvan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Công văn";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongvan.Elements.Add(tileItemElement1);
            this.itCongvan.Id = 56;
            this.itCongvan.IsLarge = true;
            this.itCongvan.Name = "itCongvan";
            this.itCongvan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itBienbangiaonhan);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itBienbangiaonhan
            // 
            this.itBienbangiaonhan.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itBienbangiaonhan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Biên bản giao nhận";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itBienbangiaonhan.Elements.Add(tileItemElement2);
            this.itBienbangiaonhan.Id = 59;
            this.itBienbangiaonhan.IsLarge = true;
            this.itBienbangiaonhan.Name = "itBienbangiaonhan";
            this.itBienbangiaonhan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBienbangiaonhan_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itHosocongty);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itHosocongty
            // 
            this.itHosocongty.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itHosocongty.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Tủ lưu trữ hồ sơ";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itHosocongty.Elements.Add(tileItemElement3);
            this.itHosocongty.Id = 57;
            this.itHosocongty.IsLarge = true;
            this.itHosocongty.Name = "itHosocongty";
            this.itHosocongty.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHosocongty_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itQuytrinhlamviec);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itQuytrinhlamviec
            // 
            tileItemElement4.Text = "Quy trình làm việc";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuytrinhlamviec.Elements.Add(tileItemElement4);
            this.itQuytrinhlamviec.Id = 60;
            this.itQuytrinhlamviec.IsLarge = true;
            this.itQuytrinhlamviec.Name = "itQuytrinhlamviec";
            this.itQuytrinhlamviec.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuytrinhlamviec_ItemClick);
            // 
            // Frm_Phonghanhchinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Phonghanhchinh";
            this.Text = "Phòng hành chính";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itCongvan;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itBienbangiaonhan;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itHosocongty;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itQuytrinhlamviec;
    }
}