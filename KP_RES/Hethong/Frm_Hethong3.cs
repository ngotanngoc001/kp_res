﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_License;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace KP_RES 
{
    public partial class Frm_Hethong3 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Hethong3()
        {
            InitializeComponent();

            LoadPermission();
            cls_KP_RES.RemoveTileItem(itDoimatkhau);
       
          
            cls_KP_RES.RemoveTileItem(itDangkysudung);
        
            cls_KP_RES.RemoveTileItem(itCapnhat);
        }

        private void Frm_Hethong3_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itDoimatkhau_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDoimatkhau.Checked)
            {
                this.Parent.GoTo<KP_UserManagement.PF_Doimatkhau3>();
            }
        }

       


        private void itDangkysudung_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDangkysudung.Checked)
            {
                this.Parent.GoTo<KP_License.Frm_Createmachinecode3>();
            }
        }


        private void itCapnhat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCapnhat.Checked)
            {
                Application.Exit();
                if (File.Exists(Application.StartupPath + "\\KP_Update.exe"))
                {
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\KP_Update.exe");
                }
                else
                {
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\UpdateApp.exe");
                }
            }
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        private void LoadPermission()
        {
            itDoimatkhau.Checked = clsUserManagement.AllowView("1");
           
         
            itDangkysudung.Checked = clsUserManagement.AllowView("43");
          
            itCapnhat.Checked = clsUserManagement.AllowView("45");
        }
    }
}