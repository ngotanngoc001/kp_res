﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Banve : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Banve()
        {
            InitializeComponent();
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Banve_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itInbanggia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itInbanggia.Checked)
            {
                this.Parent.GoTo<Frm_BC_Inbanggia>();
            }
        }

        private void itLichchieu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLichchieu.Checked)
            {
                this.Parent.GoTo<Frm_BC_Lichchieu>();
            }
        }

        private void itBanverap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanverap.Checked)
            {
                this.Parent.GoTo<Frm_BC_BVRAP>();
            }
        }

        private void itDatverap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDatverap.Checked)
            {
                this.Parent.GoTo<Frm_BC_DVRAP>();
            }
        }

        private void itHuyverap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHuyverap.Checked)
            {
                this.Parent.GoTo<Frm_BC_HVRAP>();
            }
        }

        private void itDoanhthurap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDoanhthurap.Checked)
            {
                this.Parent.GoTo<Frm_BC_DTVE>();
            }
        }

        private void itBanvephathanh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanvephathanh.Checked)
            {
                this.Parent.GoTo<Frm_BC_Nhaphathanh>();
            }
        }

        private void itBanvetruoc_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanvetruoc.Checked)
            {
                this.Parent.GoTo<Frm_BC_BVRAP_TRUOC>();
            }
        }

        private void itBanveonline_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanveonline.Checked)
            {
                this.Parent.GoTo<Frm_BC_BV_ONLINE>();
            }
        }

        private void itBKCTBV_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBKCTBV.Checked)
            {
                this.Parent.GoTo<Frm_BK_ChiTiet_BanVe>();
            }
        }

        private void itBanvetheoKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanvetheoKH.Checked)
            {
                this.Parent.GoTo<Frm_BC_TongHopBanHang_KH>();
            }
        }

        private void itLichsudoilich_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLichsudoilich.Checked)
            {
                this.Parent.GoTo<Frm_BC_LichSuLichChieu>();
            }
        }

        private void itBanvetheoShow_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBanvetheoShow.Checked)
            {
                //this.Parent.GoTo<Frm_BC_LichSuLichChieu>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itInbanggia);
            cls_KP_RES.RemoveTileItem(itLichchieu);
            cls_KP_RES.RemoveTileItem(itBanverap);
            cls_KP_RES.RemoveTileItem(itDatverap);
            cls_KP_RES.RemoveTileItem(itHuyverap);
            cls_KP_RES.RemoveTileItem(itDoanhthurap);
            cls_KP_RES.RemoveTileItem(itBanvephathanh);
            cls_KP_RES.RemoveTileItem(itBanvetruoc);
            cls_KP_RES.RemoveTileItem(itBanveonline);
            cls_KP_RES.RemoveTileItem(itBKCTBV);
            cls_KP_RES.RemoveTileItem(itBanvetheoKH);
            cls_KP_RES.RemoveTileItem(itBanvetheoShow);
            cls_KP_RES.RemoveTileItem(itBanvetheoNhanvien);
            cls_KP_RES.RemoveTileItem(itLichsudoilich);
        }

        private void LoadPermission()
        {
            itInbanggia.Checked = clsUserManagement.AllowView("81");
            itLichchieu.Checked = clsUserManagement.AllowView("82");
            itBanverap.Checked = clsUserManagement.AllowView("83");
            itDatverap.Checked = clsUserManagement.AllowView("84");
            itHuyverap.Checked = clsUserManagement.AllowView("85");
            itDoanhthurap.Checked = clsUserManagement.AllowView("86");
            itBanvephathanh.Checked = clsUserManagement.AllowView("83");
            itBanvetruoc.Checked = clsUserManagement.AllowView("83");
            itBanveonline.Checked = clsUserManagement.AllowView("83");
            itLichsudoilich.Checked = clsUserManagement.AllowView("82");

            if (cls_KP_RES.Mode == 2)
            {
                itBKCTBV.Checked = true;
                itBanvetheoKH.Checked = true;
                itBanvetheoShow.Checked = true;
                itBanvetheoNhanvien.Checked = true;
            }
        }

       

     
       

       

     

       
    }
}