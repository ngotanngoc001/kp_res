﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Caidat_CongVan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Caidat_CongVan()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Cài đặt";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Caidat_CongVan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCoQuanBanHanh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCoQuanBanHanh.Checked)
            {
                this.Parent.GoTo<Frm_CoQuanBanHanh>();
            }
        }

        private void itLoaiVanBan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itLoaiVanBan.Checked)
            {
                this.Parent.GoTo<Frm_LoaiVanBan>();
            }
        }

        private void itSoVanBan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itSoVanBan.Checked)
            {
                this.Parent.GoTo<Frm_SoVanBan>();
            }
        }

        private void itTrangThai_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrangThai.Checked)
            {
                this.Parent.GoTo<Frm_TrangThai>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCoQuanBanHanh);
            cls_KP_RES.RemoveTileItem(itLoaiVanBan);
            cls_KP_RES.RemoveTileItem(itSoVanBan);
            cls_KP_RES.RemoveTileItem(itTrangThai);
        }

        private void LoadPermission()
        {
            itCoQuanBanHanh.Checked = true;
            itLoaiVanBan.Checked = true;
            itSoVanBan.Checked = true;
            itTrangThai.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

        

        

    }
}