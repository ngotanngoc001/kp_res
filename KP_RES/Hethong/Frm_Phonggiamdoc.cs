﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Phonggiamdoc : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Phonggiamdoc()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Phonggiamdoc_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaoCao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCao.Checked)
            {
                this.Parent.GoTo<Frm_GD_Baocao>();
            }
        }

        private void itGiayToKyDuyet_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiayToKyDuyet.Checked)
            {
                this.Parent.GoTo<Frm_GD_Kyduyet>();
            }
        }

        private void itGiaiQuyetCongViec_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiaiQuyetCongViec.Checked)
            {
                this.Parent.GoTo<Frm_GD_Giaiquyet>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaoCao);
            cls_KP_RES.RemoveTileItem(itGiayToKyDuyet);
            cls_KP_RES.RemoveTileItem(itGiaiQuyetCongViec);
        }

        private void LoadPermission()
        {
            itBaoCao.Checked = true;
            itGiayToKyDuyet.Checked = true;
            itGiaiQuyetCongViec.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        } 

    }
}