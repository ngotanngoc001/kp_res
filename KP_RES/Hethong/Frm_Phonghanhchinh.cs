﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Phonghanhchinh : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Phonghanhchinh()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Phonghanhchinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itCongvan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongvan.Checked)
            {
                this.Parent.GoTo<Frm_Congvan1>();
            }
        }

        private void itBienbangiaonhan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBienbangiaonhan.Checked)
            {
                this.Parent.GoTo<Frm_Bienbangiaonhan1>();
            }
        }

        private void itHosocongty_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHosocongty.Checked)
            {
                this.Parent.GoTo<Frm_Hosocongty1>();
            }
        }

        private void itQuytrinhlamviec_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuytrinhlamviec.Checked)
            {
                this.Parent.GoTo<Frm_HC_Quytrinh>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itCongvan);
            cls_KP_RES.RemoveTileItem(itHosocongty);
            cls_KP_RES.RemoveTileItem(itBienbangiaonhan);
            cls_KP_RES.RemoveTileItem(itQuytrinhlamviec);
        }

        private void LoadPermission()
        {
            itCongvan.Checked = (clsUserManagement.AllowView("24031601") || clsUserManagement.AllowView("24031602") || clsUserManagement.AllowView("24031603")) && clsUserManagement.CheckActive("16");
            itBienbangiaonhan.Checked = true ;
            itHosocongty.Checked = true;
            itQuytrinhlamviec.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

      

        

      

       
        

       

      

       

        

        

        

        

    }
}