﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_Congno1 : DevExpress.XtraEditors.XtraForm
    {
        public  Frm_Congno1()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void  Frm_Congno1_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itBaocao_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocao.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Congno>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongno.Checked)
            {
                this.Parent.GoTo<Frm_Congno>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocao);
            cls_KP_RES.RemoveTileItem(itCongno);
        }

        private void LoadPermission()
        {
            itBaocao.Checked = (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98") || clsUserManagement.AllowView("7081601")) && clsUserManagement.CheckActive("12");
            itCongno.Checked = (clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

 
       
        

       

      

       

        

        

        

        

    }
}