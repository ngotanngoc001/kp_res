﻿namespace KP_RES 
{
    partial class Frm_Caidat_Nhansu 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongban = new DevExpress.XtraEditors.TileItem();
            this.itChucvu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itTrangthainhanvien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itPhucap = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.itBaohiem = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 54;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 30;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itPhongban);
            this.tileGroup2.Items.Add(this.itChucvu);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itPhongban
            // 
            this.itPhongban.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itPhongban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Phòng ban";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongban.Elements.Add(tileItemElement1);
            this.itPhongban.Id = 15;
            this.itPhongban.IsLarge = true;
            this.itPhongban.Name = "itPhongban";
            this.itPhongban.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongban_ItemClick);
            // 
            // itChucvu
            // 
            this.itChucvu.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            this.itChucvu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Chức vụ";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itChucvu.Elements.Add(tileItemElement2);
            this.itChucvu.Id = 52;
            this.itChucvu.IsLarge = true;
            this.itChucvu.Name = "itChucvu";
            this.itChucvu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itChucvu_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itTrangthainhanvien);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itTrangthainhanvien
            // 
            this.itTrangthainhanvien.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itTrangthainhanvien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Trạng thái nhân viên";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrangthainhanvien.Elements.Add(tileItemElement3);
            this.itTrangthainhanvien.Id = 50;
            this.itTrangthainhanvien.IsLarge = true;
            this.itTrangthainhanvien.Name = "itTrangthainhanvien";
            this.itTrangthainhanvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrangthainhanvien_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itPhucap);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itPhucap
            // 
            this.itPhucap.BackgroundImage = global::KP_RES.Properties.Resources.Danhsachoadon_inlai;
            this.itPhucap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Phụ cấp";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhucap.Elements.Add(tileItemElement4);
            this.itPhucap.Id = 51;
            this.itPhucap.IsLarge = true;
            this.itPhucap.Name = "itPhucap";
            this.itPhucap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhucap_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itBaohiem);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // itBaohiem
            // 
            this.itBaohiem.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itBaohiem.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Bảo hiểm";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaohiem.Elements.Add(tileItemElement5);
            this.itBaohiem.Id = 53;
            this.itBaohiem.IsLarge = true;
            this.itBaohiem.Name = "itBaohiem";
            this.itBaohiem.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaohiem_ItemClick);
            // 
            // Frm_Caidat_Nhansu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Nhansu";
            this.Text = "Nhân sự";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itPhongban;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itTrangthainhanvien;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itPhucap;
        private DevExpress.XtraEditors.TileItem itChucvu;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itBaohiem;



    }
}