﻿namespace KP_RES 
{
    partial class Frm_Cauhinh 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Cauhinh));
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.itGiaodien = new DevExpress.XtraEditors.TileItem();
            this.itNgonngu = new DevExpress.XtraEditors.TileItem();
            this.itMayin = new DevExpress.XtraEditors.TileItem();
            this.itHienthigia = new DevExpress.XtraEditors.TileItem();
            this.itKettien = new DevExpress.XtraEditors.TileItem();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.itBaocao = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhkhuvuc = new DevExpress.XtraEditors.TileItem();
            this.itRap = new DevExpress.XtraEditors.TileItem();
            this.itQuay = new DevExpress.XtraEditors.TileItem();
            this.itBep = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itCauhinhgiaThue = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhxuatkho = new DevExpress.XtraEditors.TileItem();
            this.itNgaytinhton = new DevExpress.XtraEditors.TileItem();
            this.itBanggia = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itCauhinhcaban = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhuser = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhthoigianbanve = new DevExpress.XtraEditors.TileItem();
            this.itMatkhaubanhang = new DevExpress.XtraEditors.TileItem();
            this.tileGroup16 = new DevExpress.XtraEditors.TileGroup();
            this.itCauhinhmenu = new DevExpress.XtraEditors.TileItem();
            this.itCanhbaotonkho = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhemail = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhintem = new DevExpress.XtraEditors.TileItem();
            this.tileGroup17 = new DevExpress.XtraEditors.TileGroup();
            this.itKHTT_Sole = new DevExpress.XtraEditors.TileItem();
            this.itKHTT_Tinhdiem = new DevExpress.XtraEditors.TileItem();
            this.itCauhinhtinhgiobida = new DevExpress.XtraEditors.TileItem();
            this.tileGroup18 = new DevExpress.XtraEditors.TileGroup();
            this.itCongnothuchi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.itInTongBill = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup8);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup16);
            this.tileControl1.Groups.Add(this.tileGroup17);
            this.tileControl1.Groups.Add(this.tileGroup18);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 47;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1024, 668);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup8
            // 
            this.tileGroup8.Items.Add(this.itGiaodien);
            this.tileGroup8.Items.Add(this.itNgonngu);
            this.tileGroup8.Items.Add(this.itMayin);
            this.tileGroup8.Items.Add(this.itHienthigia);
            this.tileGroup8.Items.Add(this.itKettien);
            this.tileGroup8.Items.Add(this.itInTongBill);
            this.tileGroup8.Name = "tileGroup8";
            // 
            // itGiaodien
            // 
            this.itGiaodien.BackgroundImage = global::KP_RES.Properties.Resources.giaodien7;
            this.itGiaodien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Giao diện";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiaodien.Elements.Add(tileItemElement1);
            this.itGiaodien.Id = 21;
            this.itGiaodien.IsLarge = true;
            this.itGiaodien.Name = "itGiaodien";
            this.itGiaodien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiaodien_ItemClick);
            // 
            // itNgonngu
            // 
            this.itNgonngu.BackgroundImage = global::KP_RES.Properties.Resources.vietnam;
            this.itNgonngu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Ngôn ngữ";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itNgonngu.Elements.Add(tileItemElement2);
            this.itNgonngu.Id = 35;
            this.itNgonngu.IsLarge = true;
            this.itNgonngu.Name = "itNgonngu";
            this.itNgonngu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNgonngu_ItemClick);
            // 
            // itMayin
            // 
            this.itMayin.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhmayin;
            this.itMayin.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Máy in";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itMayin.Elements.Add(tileItemElement3);
            this.itMayin.Id = 12;
            this.itMayin.IsLarge = true;
            this.itMayin.Name = "itMayin";
            this.itMayin.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMayin_ItemClick);
            // 
            // itHienthigia
            // 
            this.itHienthigia.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhhienthigia;
            this.itHienthigia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Màn hình hiển thị giá";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHienthigia.Elements.Add(tileItemElement4);
            this.itHienthigia.Id = 13;
            this.itHienthigia.IsLarge = true;
            this.itHienthigia.Name = "itHienthigia";
            this.itHienthigia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHienthigia_ItemClick);
            // 
            // itKettien
            // 
            this.itKettien.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhkettien;
            this.itKettien.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Két tiền";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itKettien.Elements.Add(tileItemElement5);
            this.itKettien.Id = 14;
            this.itKettien.IsLarge = true;
            this.itKettien.Name = "itKettien";
            this.itKettien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKettien_ItemClick);
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itBaocao);
            this.tileGroup10.Items.Add(this.itCauhinhkhuvuc);
            this.tileGroup10.Items.Add(this.itRap);
            this.tileGroup10.Items.Add(this.itQuay);
            this.tileGroup10.Items.Add(this.itBep);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // itBaocao
            // 
            this.itBaocao.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itBaocao.BackgroundImage")));
            this.itBaocao.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Cấu hình báo cáo";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocao.Elements.Add(tileItemElement7);
            this.itBaocao.Id = 15;
            this.itBaocao.IsLarge = true;
            this.itBaocao.Name = "itBaocao";
            this.itBaocao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocao_ItemClick);
            // 
            // itCauhinhkhuvuc
            // 
            this.itCauhinhkhuvuc.BackgroundImage = global::KP_RES.Properties.Resources.khuvucban1;
            this.itCauhinhkhuvuc.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement8.Text = "Cấu hình khu vực-nhóm hàng bán";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhkhuvuc.Elements.Add(tileItemElement8);
            this.itCauhinhkhuvuc.Id = 31;
            this.itCauhinhkhuvuc.IsLarge = true;
            this.itCauhinhkhuvuc.Name = "itCauhinhkhuvuc";
            this.itCauhinhkhuvuc.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhkhuvuc_ItemClick);
            // 
            // itRap
            // 
            this.itRap.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            this.itRap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement9.Text = "Cấu hình rạp";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itRap.Elements.Add(tileItemElement9);
            this.itRap.Id = 24;
            this.itRap.IsLarge = true;
            this.itRap.Name = "itRap";
            this.itRap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itRap_ItemClick);
            // 
            // itQuay
            // 
            this.itQuay.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            this.itQuay.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Cấu hình quầy";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuay.Elements.Add(tileItemElement10);
            this.itQuay.Id = 16;
            this.itQuay.IsLarge = true;
            this.itQuay.Name = "itQuay";
            this.itQuay.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuay_ItemClick);
            // 
            // itBep
            // 
            this.itBep.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep;
            this.itBep.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement11.Text = "Cấu hình bếp";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itBep.Elements.Add(tileItemElement11);
            this.itBep.Id = 18;
            this.itBep.IsLarge = true;
            this.itBep.Name = "itBep";
            this.itBep.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBep_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itCauhinhgiaThue);
            this.tileGroup11.Items.Add(this.itCauhinhxuatkho);
            this.tileGroup11.Items.Add(this.itNgaytinhton);
            this.tileGroup11.Items.Add(this.itBanggia);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itCauhinhgiaThue
            // 
            this.itCauhinhgiaThue.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itCauhinhgiaThue.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement12.Text = "Cấu hình đơn giá - thuế";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhgiaThue.Elements.Add(tileItemElement12);
            this.itCauhinhgiaThue.Id = 17;
            this.itCauhinhgiaThue.IsLarge = true;
            this.itCauhinhgiaThue.Name = "itCauhinhgiaThue";
            this.itCauhinhgiaThue.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhgiaThue_ItemClick);
            // 
            // itCauhinhxuatkho
            // 
            this.itCauhinhxuatkho.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhxuatam;
            this.itCauhinhxuatkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Cấu hình xuất kho";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhxuatkho.Elements.Add(tileItemElement13);
            this.itCauhinhxuatkho.Id = 20;
            this.itCauhinhxuatkho.IsLarge = true;
            this.itCauhinhxuatkho.Name = "itCauhinhxuatkho";
            this.itCauhinhxuatkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhxuatkho_ItemClick);
            // 
            // itNgaytinhton
            // 
            this.itNgaytinhton.BackgroundImage = global::KP_RES.Properties.Resources.Caban;
            this.itNgaytinhton.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement14.Text = "Cấu hình ngày tính tồn kho";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itNgaytinhton.Elements.Add(tileItemElement14);
            this.itNgaytinhton.Id = 34;
            this.itNgaytinhton.IsLarge = true;
            this.itNgaytinhton.Name = "itNgaytinhton";
            this.itNgaytinhton.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNgaytinhton_ItemClick);
            // 
            // itBanggia
            // 
            this.itBanggia.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itBanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement15.Text = "Cấu hình bảng giá";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanggia.Elements.Add(tileItemElement15);
            this.itBanggia.Id = 19;
            this.itBanggia.IsLarge = true;
            this.itBanggia.Name = "itBanggia";
            this.itBanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanggia_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itCauhinhcaban);
            this.tileGroup13.Items.Add(this.itCauhinhuser);
            this.tileGroup13.Items.Add(this.itCauhinhthoigianbanve);
            this.tileGroup13.Items.Add(this.itMatkhaubanhang);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itCauhinhcaban
            // 
            this.itCauhinhcaban.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itCauhinhcaban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Cấu hình ca bán";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhcaban.Elements.Add(tileItemElement16);
            this.itCauhinhcaban.Id = 33;
            this.itCauhinhcaban.IsLarge = true;
            this.itCauhinhcaban.Name = "itCauhinhcaban";
            this.itCauhinhcaban.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhcaban_ItemClick);
            // 
            // itCauhinhuser
            // 
            this.itCauhinhuser.BackgroundImage = global::KP_RES.Properties.Resources.phanquyen;
            this.itCauhinhuser.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement17.Text = "Cấu hình kích hoạt user";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhuser.Elements.Add(tileItemElement17);
            this.itCauhinhuser.Id = 36;
            this.itCauhinhuser.IsLarge = true;
            this.itCauhinhuser.Name = "itCauhinhuser";
            this.itCauhinhuser.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhuser_ItemClick);
            // 
            // itCauhinhthoigianbanve
            // 
            this.itCauhinhthoigianbanve.BackgroundImage = global::KP_RES.Properties.Resources.thoigian;
            this.itCauhinhthoigianbanve.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement18.Text = "Cấu hình thời gian ngừng bán vé";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement18.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhthoigianbanve.Elements.Add(tileItemElement18);
            this.itCauhinhthoigianbanve.Id = 26;
            this.itCauhinhthoigianbanve.IsLarge = true;
            this.itCauhinhthoigianbanve.Name = "itCauhinhthoigianbanve";
            this.itCauhinhthoigianbanve.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhthoigianbanve_ItemClick);
            // 
            // itMatkhaubanhang
            // 
            this.itMatkhaubanhang.BackgroundImage = global::KP_RES.Properties.Resources.doimatkhau;
            this.itMatkhaubanhang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement19.Text = "Cấu hình mật khẩu bán hàng";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement19.TextLocation = new System.Drawing.Point(0, 0);
            this.itMatkhaubanhang.Elements.Add(tileItemElement19);
            this.itMatkhaubanhang.Id = 23;
            this.itMatkhaubanhang.IsLarge = true;
            this.itMatkhaubanhang.Name = "itMatkhaubanhang";
            this.itMatkhaubanhang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMatkhaubanhang_ItemClick);
            // 
            // tileGroup16
            // 
            this.tileGroup16.Items.Add(this.itCauhinhmenu);
            this.tileGroup16.Items.Add(this.itCanhbaotonkho);
            this.tileGroup16.Items.Add(this.itCauhinhemail);
            this.tileGroup16.Items.Add(this.itCauhinhintem);
            this.tileGroup16.Name = "tileGroup16";
            // 
            // itCauhinhmenu
            // 
            this.itCauhinhmenu.BackgroundImage = global::KP_RES.Properties.Resources.loaighe;
            this.itCauhinhmenu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement20.Text = "Cấu hình tự khởi động";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement20.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhmenu.Elements.Add(tileItemElement20);
            this.itCauhinhmenu.Id = 27;
            this.itCauhinhmenu.IsLarge = true;
            this.itCauhinhmenu.Name = "itCauhinhmenu";
            this.itCauhinhmenu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhmenu_ItemClick);
            // 
            // itCanhbaotonkho
            // 
            this.itCanhbaotonkho.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itCanhbaotonkho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement21.Text = "Cảnh báo tồn kho";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement21.TextLocation = new System.Drawing.Point(0, 0);
            this.itCanhbaotonkho.Elements.Add(tileItemElement21);
            this.itCanhbaotonkho.Id = 37;
            this.itCanhbaotonkho.IsLarge = true;
            this.itCanhbaotonkho.Name = "itCanhbaotonkho";
            this.itCanhbaotonkho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCanhbaotonkho_ItemClick);
            // 
            // itCauhinhemail
            // 
            this.itCauhinhemail.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCauhinhemail.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement22.Text = "Cấu hình gửi mail";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement22.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhemail.Elements.Add(tileItemElement22);
            this.itCauhinhemail.Id = 22;
            this.itCauhinhemail.IsLarge = true;
            this.itCauhinhemail.Name = "itCauhinhemail";
            this.itCauhinhemail.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhemail_ItemClick);
            // 
            // itCauhinhintem
            // 
            this.itCauhinhintem.BackgroundImage = global::KP_RES.Properties.Resources.inmavach;
            this.itCauhinhintem.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement23.Text = "Cấu hình in tem mã vạch";
            tileItemElement23.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement23.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhintem.Elements.Add(tileItemElement23);
            this.itCauhinhintem.Id = 41;
            this.itCauhinhintem.IsLarge = true;
            this.itCauhinhintem.Name = "itCauhinhintem";
            this.itCauhinhintem.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhintem_ItemClick);
            // 
            // tileGroup17
            // 
            this.tileGroup17.Items.Add(this.itKHTT_Sole);
            this.tileGroup17.Items.Add(this.itKHTT_Tinhdiem);
            this.tileGroup17.Items.Add(this.itCauhinhtinhgiobida);
            this.tileGroup17.Name = "tileGroup17";
            // 
            // itKHTT_Sole
            // 
            this.itKHTT_Sole.BackgroundImage = global::KP_RES.Properties.Resources.quydinhkhachhangthanthiet;
            this.itKHTT_Sole.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement24.Text = "Cấu hình số lẻ KHTT";
            tileItemElement24.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement24.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT_Sole.Elements.Add(tileItemElement24);
            this.itKHTT_Sole.Id = 38;
            this.itKHTT_Sole.IsLarge = true;
            this.itKHTT_Sole.Name = "itKHTT_Sole";
            this.itKHTT_Sole.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_Sole_ItemClick);
            // 
            // itKHTT_Tinhdiem
            // 
            this.itKHTT_Tinhdiem.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itKHTT_Tinhdiem.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement25.Text = "Cấu hình tính điểm KHTT";
            tileItemElement25.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement25.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT_Tinhdiem.Elements.Add(tileItemElement25);
            this.itKHTT_Tinhdiem.Id = 39;
            this.itKHTT_Tinhdiem.IsLarge = true;
            this.itKHTT_Tinhdiem.Name = "itKHTT_Tinhdiem";
            this.itKHTT_Tinhdiem.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_Tinhdiem_ItemClick);
            // 
            // itCauhinhtinhgiobida
            // 
            this.itCauhinhtinhgiobida.BackgroundImage = global::KP_RES.Properties.Resources.dinhmuc;
            this.itCauhinhtinhgiobida.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement26.Text = "Cấu hình tính giờ Bida";
            tileItemElement26.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement26.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinhtinhgiobida.Elements.Add(tileItemElement26);
            this.itCauhinhtinhgiobida.Id = 45;
            this.itCauhinhtinhgiobida.IsLarge = true;
            this.itCauhinhtinhgiobida.Name = "itCauhinhtinhgiobida";
            this.itCauhinhtinhgiobida.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinhtinhgiobida_ItemClick);
            // 
            // tileGroup18
            // 
            this.tileGroup18.Items.Add(this.itCongnothuchi);
            this.tileGroup18.Name = "tileGroup18";
            // 
            // itCongnothuchi
            // 
            this.itCongnothuchi.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhcongnothuchi;
            this.itCongnothuchi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement27.Text = "Cấu hình công nợ - thu chi";
            tileItemElement27.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement27.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongnothuchi.Elements.Add(tileItemElement27);
            this.itCongnothuchi.Id = 40;
            this.itCongnothuchi.IsLarge = true;
            this.itCongnothuchi.Name = "itCongnothuchi";
            this.itCongnothuchi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongnothuchi_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Name = "tileGroup14";
            // 
            // tileGroup15
            // 
            this.tileGroup15.Name = "tileGroup15";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 672);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // itInTongBill
            // 
            this.itInTongBill.BackgroundImage = global::KP_RES.Properties.Resources.cauhinhmayin;
            this.itInTongBill.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "In bill tổng";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itInTongBill.Elements.Add(tileItemElement6);
            this.itInTongBill.Id = 46;
            this.itInTongBill.IsLarge = true;
            this.itInTongBill.Name = "itInTongBill";
            this.itInTongBill.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itInTongBill_ItemClick);
            // 
            // Frm_Cauhinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 672);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Cauhinh";
            this.Text = "Cấu hình";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Cauhinh_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itMayin;
        private DevExpress.XtraEditors.TileItem itHienthigia;
        private DevExpress.XtraEditors.TileItem itKettien;
        private DevExpress.XtraEditors.TileItem itBaocao;
        private DevExpress.XtraEditors.TileItem itQuay;
        private DevExpress.XtraEditors.TileItem itBep;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itCauhinhgiaThue;
        private DevExpress.XtraEditors.TileItem itBanggia;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itGiaodien;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itCauhinhxuatkho;
        private DevExpress.XtraEditors.TileItem itCauhinhthoigianbanve;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileGroup tileGroup15;
        private DevExpress.XtraEditors.TileGroup tileGroup16;
        private DevExpress.XtraEditors.TileItem itNgonngu;
        private DevExpress.XtraEditors.TileItem itCauhinhkhuvuc;
        private DevExpress.XtraEditors.TileItem itRap;
        private DevExpress.XtraEditors.TileItem itNgaytinhton;
        private DevExpress.XtraEditors.TileItem itCauhinhcaban;
        private DevExpress.XtraEditors.TileItem itCauhinhuser;
        private DevExpress.XtraEditors.TileItem itMatkhaubanhang;
        private DevExpress.XtraEditors.TileItem itCauhinhmenu;
        private DevExpress.XtraEditors.TileItem itCanhbaotonkho;
        private DevExpress.XtraEditors.TileItem itCauhinhemail;
        private DevExpress.XtraEditors.TileGroup tileGroup17;
        private DevExpress.XtraEditors.TileItem itKHTT_Sole;
        private DevExpress.XtraEditors.TileItem itKHTT_Tinhdiem;
        private DevExpress.XtraEditors.TileGroup tileGroup18;
        private DevExpress.XtraEditors.TileItem itCongnothuchi;
        private DevExpress.XtraEditors.TileItem itCauhinhintem;
        private DevExpress.XtraEditors.TileItem itCauhinhtinhgiobida;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem itInTongBill;



    }
}