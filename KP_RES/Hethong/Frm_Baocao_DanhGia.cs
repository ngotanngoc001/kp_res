﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_DanhGia : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_DanhGia()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
            
            LoadPermission();
            VisibleTileItem();
        }

        private void itBaoCaoDanhGia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaoCaoDanhGia.Checked)
            {
                this.Parent.GoTo<Frm_BaoCao_DanhGiaKH>();
            }
        }

        private void itBieuDoDanhGia_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBieuDoDanhGia.Checked)
            {
                this.Parent.GoTo<Frm_BieuDo_DanhGiaKH>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaoCaoDanhGia);
            cls_KP_RES.RemoveTileItem(itBieuDoDanhGia);
        }

        private void LoadPermission()
        {
            itBaoCaoDanhGia.Checked = true;
            itBieuDoDanhGia.Checked = true;
        }

        

        

        

        
    }
}