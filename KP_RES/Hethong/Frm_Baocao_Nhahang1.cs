﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Nhahang1 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Nhahang1()
        {
            InitializeComponent();

            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Hanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

      
        private void itHanghoa_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHanghoa.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Hanghoa>();
            }
        }

        private void itNhapxuatton_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapxuatton.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Nhapxuat>();
            }
        }

        private void itBaocaobanhang_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaobanhang.Checked)
            {
                this.Parent.GoTo<Frm_Baocao_Banhang>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocaobanhang);
            cls_KP_RES.RemoveTileItem(itHanghoa);
            cls_KP_RES.RemoveTileItem(itNhapxuatton);
        }

        private void LoadPermission()
        {
            itBaocaobanhang.Checked = (clsUserManagement.AllowView("28") || clsUserManagement.AllowView("29") || clsUserManagement.AllowView("30") || clsUserManagement.AllowView("31")) && (clsUserManagement.CheckActive("5") || (clsUserManagement.CheckActive("14")));
            itHanghoa.Checked = clsUserManagement.AllowView("48") || clsUserManagement.AllowView("50") || clsUserManagement.AllowView("56") || clsUserManagement.AllowView("57");
            itNhapxuatton.Checked = (clsUserManagement.AllowView("17") || clsUserManagement.AllowView("26") || clsUserManagement.AllowView("47") || clsUserManagement.AllowView("27")) && (clsUserManagement.CheckActive("1"));
        }

        
    }
}