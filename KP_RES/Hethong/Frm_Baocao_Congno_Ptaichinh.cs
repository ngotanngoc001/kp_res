﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Baocao_Congno_Ptaichinh : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Congno_Ptaichinh()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_Baocao_Congno_Ptaichinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itDSPT_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDSPT.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoThu>();
            }
        }

        private void itDSPC_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDSPC.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoChi>();
            }
        }

        private void itCongnoNCC_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongnoNCC.Checked)
            {
                this.Parent.GoTo<Frm_CongNoNhaCungCap>();
            }
        }

        private void itCongnoKH_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCongnoKH.Checked)
            {
                this.Parent.GoTo<Frm_CongNoKhachHang>();
            }
        }

        private void itTongquancongno_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTongquancongno.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoTongQuanCongNo>();
            }
        }

        private void itTheodoicongno_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTheodoicongno.Checked)
            {
                this.Parent.GoTo<Frm_TheoDoiCongNo>();
            }
        }

        private void itQuytienmat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itQuytienmat.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoQuyTienMat>();
            }
        }

        private void itBieudothuchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBieudothuchi.Checked)
            {
                this.Parent.GoTo<Frm_BieuDoThuChi>();
            }
        }

        private void itDenghichi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDenghichi.Checked)
            {
                this.Parent.GoTo<Frm_BC_DanhSachPhieuDeNghiChi>();
            }
        }

        private void itUynhiemchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itUynhiemchi.Checked)
            {
                //this.Parent.GoTo<Frm_BC_DanhSachPhieuDeNghiChi>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itDSPT);
            cls_KP_RES.RemoveTileItem(itDSPC);
            cls_KP_RES.RemoveTileItem(itCongnoNCC);
            cls_KP_RES.RemoveTileItem(itCongnoKH);
            cls_KP_RES.RemoveTileItem(itTongquancongno);
            cls_KP_RES.RemoveTileItem(itTheodoicongno);
            cls_KP_RES.RemoveTileItem(itQuytienmat);
            cls_KP_RES.RemoveTileItem(itBieudothuchi);
            cls_KP_RES.RemoveTileItem(itDenghichi);
            cls_KP_RES.RemoveTileItem(itUynhiemchi);
        }

        private void LoadPermission()
        {
            itDSPT.Checked = false;
            itDSPC.Checked = false;
            itCongnoNCC.Checked = false;
            itCongnoKH.Checked = false;
            itTongquancongno.Checked = false;
            itTheodoicongno.Checked = false;
            itQuytienmat.Checked = clsUserManagement.AllowView("98");
            itBieudothuchi.Checked = false;
            itDenghichi.Checked = false;
            itUynhiemchi.Checked = false  ;
        }

      

       

        
    }
}