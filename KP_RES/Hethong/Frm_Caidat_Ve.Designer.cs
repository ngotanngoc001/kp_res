﻿namespace KP_RES 
{
    partial class Frm_Caidat_Ve 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itRap = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup15 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itPhim = new DevExpress.XtraEditors.TileItem();
            this.itBanggia = new DevExpress.XtraEditors.TileItem();
            this.tileGroup16 = new DevExpress.XtraEditors.TileGroup();
            this.itCaidatShowTime = new DevExpress.XtraEditors.TileItem();
            this.itLaplichchieu = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup12);
            this.tileControl1.Groups.Add(this.tileGroup10);
            this.tileControl1.Groups.Add(this.tileGroup15);
            this.tileControl1.Groups.Add(this.tileGroup16);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 51;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Position = 160;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 668);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itRap);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itRap
            // 
            this.itRap.BackgroundImage = global::KP_RES.Properties.Resources.caidat4;
            this.itRap.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Rạp - Phòng";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itRap.Elements.Add(tileItemElement1);
            this.itRap.Id = 37;
            this.itRap.IsLarge = true;
            this.itRap.Name = "itRap";
            this.itRap.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itRap_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Items.Add(this.itPhim);
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Items.Add(this.itBanggia);
            this.tileGroup10.Name = "tileGroup10";
            // 
            // tileGroup15
            // 
            this.tileGroup15.Items.Add(this.itLaplichchieu);
            this.tileGroup15.Name = "tileGroup15";
            // 
            // tileGroup13
            // 
            this.tileGroup13.Name = "tileGroup13";
            // 
            // tileGroup9
            // 
            this.tileGroup9.Name = "tileGroup9";
            // 
            // tileGroup7
            // 
            this.tileGroup7.Name = "tileGroup7";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 672);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup14
            // 
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itPhim
            // 
            this.itPhim.BackgroundImage = global::KP_RES.Properties.Resources.phim;
            this.itPhim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Phim";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhim.Elements.Add(tileItemElement2);
            this.itPhim.Id = 46;
            this.itPhim.IsLarge = true;
            this.itPhim.Name = "itPhim";
            this.itPhim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhim_ItemClick);
            // 
            // itBanggia
            // 
            this.itBanggia.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itBanggia.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Bảng giá";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itBanggia.Elements.Add(tileItemElement3);
            this.itBanggia.Id = 47;
            this.itBanggia.IsLarge = true;
            this.itBanggia.Name = "itBanggia";
            this.itBanggia.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBanggia_ItemClick);
            // 
            // tileGroup16
            // 
            this.tileGroup16.Items.Add(this.itCaidatShowTime);
            this.tileGroup16.Name = "tileGroup16";
            // 
            // itCaidatShowTime
            // 
            this.itCaidatShowTime.BackgroundImage = global::KP_RES.Properties.Resources.showtime;
            this.itCaidatShowTime.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "ShowTime";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidatShowTime.Elements.Add(tileItemElement5);
            this.itCaidatShowTime.Id = 49;
            this.itCaidatShowTime.IsLarge = true;
            this.itCaidatShowTime.Name = "itCaidatShowTime";
            this.itCaidatShowTime.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidatShowTime_ItemClick);
            // 
            // itLaplichchieu
            // 
            this.itLaplichchieu.BackgroundImage = global::KP_RES.Properties.Resources.dangky;
            this.itLaplichchieu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Lịch chiếu";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itLaplichchieu.Elements.Add(tileItemElement4);
            this.itLaplichchieu.Id = 48;
            this.itLaplichchieu.IsLarge = true;
            this.itLaplichchieu.Name = "itLaplichchieu";
            this.itLaplichchieu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itLaplichchieu_ItemClick);
            // 
            // Frm_Caidat_Ve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 672);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Caidat_Ve";
            this.Text = "Cài đặt rạp phim";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Caidat_Ve_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itRap;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup15;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itPhim;
        private DevExpress.XtraEditors.TileItem itBanggia;
        private DevExpress.XtraEditors.TileItem itLaplichchieu;
        private DevExpress.XtraEditors.TileGroup tileGroup16;
        private DevExpress.XtraEditors.TileItem itCaidatShowTime;



    }
}