﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baocao_Kho : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baocao_Kho()
        {
            InitializeComponent();
            this.Text = rm.GetString("nhapxuat", culture);
            tileControl1.Text = rm.GetString("nhapxuat", culture);
            LoadPermission();
            VisibleTileItem();
        }

        private void itBaocaoPhieunhap_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoPhieunhap.Checked)
            {
                this.Parent.GoTo<Frm_DSPhieuNhap>();
            }
        }

        private void itBaocaoPhieuxuat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoPhieuxuat.Checked)
            {
                this.Parent.GoTo<Frm_DSPhieuXuat>();
            }
        }

        private void itBaocaoPhieuKiemKe_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoPhieuKiemKe.Checked)
            {
                this.Parent.GoTo<Frm_DSPhieuKiemke>();
            }
        }

        private void itBaocaoTonkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itBaocaoPhieuxuat.Checked)
            {
                this.Parent.GoTo<Frm_TonKhoNew>();
            }
        }

        private void itThekho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThekho.Checked)
            {
                this.Parent.GoTo<Frm_Thekho>();
            }
        }

        private void itNhapkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapkho.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoHangNhapKho>();
            }
        }

        private void itXuatkho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itXuatkho.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoHangXuatKho>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itBaocaoPhieunhap);
            cls_KP_RES.RemoveTileItem(itBaocaoPhieuxuat);
            cls_KP_RES.RemoveTileItem(itBaocaoPhieuKiemKe);
            cls_KP_RES.RemoveTileItem(itBaocaoTonkho);
            cls_KP_RES.RemoveTileItem(itThekho);
            cls_KP_RES.RemoveTileItem(itNhapkho);
            cls_KP_RES.RemoveTileItem(itXuatkho);
        }


        private void LoadPermission()
        {
            itBaocaoPhieunhap.Checked = true;
            itBaocaoPhieuxuat.Checked = true;
            itBaocaoPhieuKiemKe.Checked = true;
            itBaocaoTonkho.Checked = true;
            itThekho.Checked = true;

            itNhapkho.Checked = true;
            itXuatkho.Checked = true;
        }


        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Baocao_Kho_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();

          
          
            itBaocaoPhieunhap.Text = rm.GetString("danhsachphieunhap", culture);
            itBaocaoPhieuxuat.Text = rm.GetString("danhsachphieuxuat", culture);
            itBaocaoTonkho.Text = rm.GetString("tonkho", culture);
            itThekho.Text = rm.GetString("thekho", culture);
            itBaocaoPhieuKiemKe.Text = rm.GetString("danhsachphieukiemke", culture);

            this.Text = rm.GetString("nhapxuat", culture);
            tileControl1.Text = rm.GetString("nhapxuat", culture);
        }

      
    }
}