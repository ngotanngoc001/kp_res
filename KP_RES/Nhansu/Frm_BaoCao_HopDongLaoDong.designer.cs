﻿namespace KP_RES
{
    partial class Frm_BaoCao_HopDongLaoDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCao_HopDongLaoDong));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.lblTuNgay = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.lblDenNgay = new DevExpress.XtraEditors.LabelControl();
            this.cboData = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHIEULUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHETHIEULUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MUCLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_XAHOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_YTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_THATNGHIEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUCCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 674);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 127);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 510);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 82);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 426);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.dtpTuNgay);
            this.pnChoose.Controls.Add(this.lblTuNgay);
            this.pnChoose.Controls.Add(this.dtpDenNgay);
            this.pnChoose.Controls.Add(this.lblDenNgay);
            this.pnChoose.Controls.Add(this.cboData);
            this.pnChoose.Controls.Add(this.lblTitle);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 80);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(86, 2);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpTuNgay.TabIndex = 18;
            this.dtpTuNgay.Visible = false;
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTuNgay.Location = new System.Drawing.Point(2, 6);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(72, 19);
            this.lblTuNgay.TabIndex = 20;
            this.lblTuNgay.Text = "Từ Ngày :";
            this.lblTuNgay.Visible = false;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(86, 42);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpDenNgay.TabIndex = 19;
            this.dtpDenNgay.Visible = false;
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblDenNgay.Location = new System.Drawing.Point(2, 46);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(80, 19);
            this.lblDenNgay.TabIndex = 21;
            this.lblDenNgay.Text = "Đến Ngày :";
            this.lblDenNgay.Visible = false;
            // 
            // cboData
            // 
            this.cboData.Location = new System.Drawing.Point(5, 42);
            this.cboData.Name = "cboData";
            this.cboData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.Appearance.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboData.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboData.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboData.Properties.DisplayMember = "TEN";
            this.cboData.Properties.DropDownItemHeight = 40;
            this.cboData.Properties.NullText = "";
            this.cboData.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboData.Properties.ShowHeader = false;
            this.cboData.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboData.Properties.ValueMember = "MA";
            this.cboData.Size = new System.Drawing.Size(210, 26);
            this.cboData.TabIndex = 17;
            this.cboData.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTitle.Location = new System.Drawing.Point(3, 8);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(27, 19);
            this.lblTitle.TabIndex = 16;
            this.lblTitle.Text = "Tên";
            this.lblTitle.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 125);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "0";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "Theo nhân viên"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "Theo ngày ký")});
            this.optGroup.Size = new System.Drawing.Size(215, 119);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 637);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatFile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Controls.Add(this.btnDownLoadFile);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1325, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 674);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 317);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 275);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 115);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 195);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 112);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 592);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 237);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatFile
            // 
            this.btnXuatFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatFile.Appearance.Options.UseFont = true;
            this.btnXuatFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatFile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatFile.Location = new System.Drawing.Point(2, 157);
            this.btnXuatFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatFile.Name = "btnXuatFile";
            this.btnXuatFile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatFile.TabIndex = 14;
            this.btnXuatFile.Text = "&Xuất File";
            this.btnXuatFile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 77);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDownLoadFile.Location = new System.Drawing.Point(2, 2);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(35, 75);
            this.btnDownLoadFile.TabIndex = 18;
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(223, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1102, 674);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILECV,
            this.STT,
            this.MA,
            this.TENHOPDONG,
            this.TENNHANVIEN,
            this.THOIHAN,
            this.NGAYKY,
            this.NGAYHIEULUC,
            this.NGAYHETHIEULUC,
            this.MUCLUONG,
            this.BH_XAHOI,
            this.BH_YTE,
            this.BH_THATNGHIEP,
            this.PHUCCAP,
            this.TEN_FILECV,
            this.FILL});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // FILECV
            // 
            this.FILECV.FieldName = "FILECV";
            this.FILECV.Name = "FILECV";
            this.FILECV.OptionsColumn.AllowEdit = false;
            this.FILECV.OptionsColumn.AllowFocus = false;
            this.FILECV.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TENHOPDONG
            // 
            this.TENHOPDONG.AppearanceCell.Options.UseTextOptions = true;
            this.TENHOPDONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHOPDONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENHOPDONG.AppearanceHeader.Options.UseFont = true;
            this.TENHOPDONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHOPDONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHOPDONG.Caption = "Tên hợp đồng";
            this.TENHOPDONG.FieldName = "TENHOPDONG";
            this.TENHOPDONG.Name = "TENHOPDONG";
            this.TENHOPDONG.OptionsColumn.AllowEdit = false;
            this.TENHOPDONG.OptionsColumn.AllowFocus = false;
            this.TENHOPDONG.OptionsColumn.FixedWidth = true;
            this.TENHOPDONG.Visible = true;
            this.TENHOPDONG.VisibleIndex = 2;
            this.TENHOPDONG.Width = 200;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.Caption = "Tên nhân viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 3;
            this.TENNHANVIEN.Width = 150;
            // 
            // THOIHAN
            // 
            this.THOIHAN.AppearanceCell.Options.UseTextOptions = true;
            this.THOIHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIHAN.AppearanceHeader.Options.UseFont = true;
            this.THOIHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIHAN.Caption = "Thời hạn";
            this.THOIHAN.FieldName = "THOIHAN";
            this.THOIHAN.Name = "THOIHAN";
            this.THOIHAN.OptionsColumn.AllowEdit = false;
            this.THOIHAN.OptionsColumn.AllowFocus = false;
            this.THOIHAN.OptionsColumn.FixedWidth = true;
            this.THOIHAN.Visible = true;
            this.THOIHAN.VisibleIndex = 4;
            this.THOIHAN.Width = 100;
            // 
            // NGAYKY
            // 
            this.NGAYKY.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYKY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYKY.AppearanceHeader.Options.UseFont = true;
            this.NGAYKY.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYKY.Caption = "Ngày ký";
            this.NGAYKY.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYKY.FieldName = "NGAYKY";
            this.NGAYKY.Name = "NGAYKY";
            this.NGAYKY.OptionsColumn.AllowEdit = false;
            this.NGAYKY.OptionsColumn.AllowFocus = false;
            this.NGAYKY.OptionsColumn.FixedWidth = true;
            this.NGAYKY.Visible = true;
            this.NGAYKY.VisibleIndex = 5;
            this.NGAYKY.Width = 120;
            // 
            // NGAYHIEULUC
            // 
            this.NGAYHIEULUC.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHIEULUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHIEULUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHIEULUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHIEULUC.AppearanceHeader.Options.UseFont = true;
            this.NGAYHIEULUC.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHIEULUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHIEULUC.Caption = "Ngày hiệu lực";
            this.NGAYHIEULUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYHIEULUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYHIEULUC.FieldName = "NGAYHIEULUC";
            this.NGAYHIEULUC.Name = "NGAYHIEULUC";
            this.NGAYHIEULUC.OptionsColumn.AllowEdit = false;
            this.NGAYHIEULUC.OptionsColumn.AllowFocus = false;
            this.NGAYHIEULUC.OptionsColumn.FixedWidth = true;
            this.NGAYHIEULUC.Visible = true;
            this.NGAYHIEULUC.VisibleIndex = 6;
            this.NGAYHIEULUC.Width = 120;
            // 
            // NGAYHETHIEULUC
            // 
            this.NGAYHETHIEULUC.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHETHIEULUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHIEULUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHETHIEULUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHETHIEULUC.AppearanceHeader.Options.UseFont = true;
            this.NGAYHETHIEULUC.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHETHIEULUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHIEULUC.Caption = "Ngày hết HL";
            this.NGAYHETHIEULUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYHETHIEULUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYHETHIEULUC.FieldName = "NGAYHETHIEULUC";
            this.NGAYHETHIEULUC.Name = "NGAYHETHIEULUC";
            this.NGAYHETHIEULUC.OptionsColumn.AllowEdit = false;
            this.NGAYHETHIEULUC.OptionsColumn.AllowFocus = false;
            this.NGAYHETHIEULUC.OptionsColumn.FixedWidth = true;
            this.NGAYHETHIEULUC.Visible = true;
            this.NGAYHETHIEULUC.VisibleIndex = 7;
            this.NGAYHETHIEULUC.Width = 120;
            // 
            // MUCLUONG
            // 
            this.MUCLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MUCLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MUCLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MUCLUONG.AppearanceHeader.Options.UseFont = true;
            this.MUCLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MUCLUONG.Caption = "Lương cơ bản";
            this.MUCLUONG.DisplayFormat.FormatString = "N0";
            this.MUCLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MUCLUONG.FieldName = "MUCLUONG";
            this.MUCLUONG.Name = "MUCLUONG";
            this.MUCLUONG.OptionsColumn.AllowEdit = false;
            this.MUCLUONG.OptionsColumn.AllowFocus = false;
            this.MUCLUONG.OptionsColumn.FixedWidth = true;
            this.MUCLUONG.Visible = true;
            this.MUCLUONG.VisibleIndex = 8;
            this.MUCLUONG.Width = 150;
            // 
            // BH_XAHOI
            // 
            this.BH_XAHOI.AppearanceCell.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_XAHOI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_XAHOI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_XAHOI.AppearanceHeader.Options.UseFont = true;
            this.BH_XAHOI.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_XAHOI.Caption = "BHXH";
            this.BH_XAHOI.FieldName = "BH_XAHOI";
            this.BH_XAHOI.Name = "BH_XAHOI";
            this.BH_XAHOI.OptionsColumn.AllowEdit = false;
            this.BH_XAHOI.OptionsColumn.AllowFocus = false;
            this.BH_XAHOI.OptionsColumn.FixedWidth = true;
            this.BH_XAHOI.Visible = true;
            this.BH_XAHOI.VisibleIndex = 9;
            this.BH_XAHOI.Width = 100;
            // 
            // BH_YTE
            // 
            this.BH_YTE.AppearanceCell.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_YTE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_YTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_YTE.AppearanceHeader.Options.UseFont = true;
            this.BH_YTE.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_YTE.Caption = "BHYT";
            this.BH_YTE.FieldName = "BH_YTE";
            this.BH_YTE.Name = "BH_YTE";
            this.BH_YTE.OptionsColumn.AllowEdit = false;
            this.BH_YTE.OptionsColumn.AllowFocus = false;
            this.BH_YTE.OptionsColumn.FixedWidth = true;
            this.BH_YTE.Visible = true;
            this.BH_YTE.VisibleIndex = 10;
            this.BH_YTE.Width = 100;
            // 
            // BH_THATNGHIEP
            // 
            this.BH_THATNGHIEP.AppearanceCell.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseFont = true;
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.Caption = "BHTN";
            this.BH_THATNGHIEP.FieldName = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.Name = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.OptionsColumn.AllowEdit = false;
            this.BH_THATNGHIEP.OptionsColumn.AllowFocus = false;
            this.BH_THATNGHIEP.OptionsColumn.FixedWidth = true;
            this.BH_THATNGHIEP.Visible = true;
            this.BH_THATNGHIEP.VisibleIndex = 11;
            this.BH_THATNGHIEP.Width = 100;
            // 
            // PHUCCAP
            // 
            this.PHUCCAP.AppearanceCell.Options.UseTextOptions = true;
            this.PHUCCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUCCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PHUCCAP.AppearanceHeader.Options.UseFont = true;
            this.PHUCCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUCCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUCCAP.Caption = "Phụ cấp";
            this.PHUCCAP.FieldName = "PHUCCAP";
            this.PHUCCAP.Name = "PHUCCAP";
            this.PHUCCAP.OptionsColumn.AllowEdit = false;
            this.PHUCCAP.OptionsColumn.AllowFocus = false;
            this.PHUCCAP.OptionsColumn.FixedWidth = true;
            this.PHUCCAP.Visible = true;
            this.PHUCCAP.VisibleIndex = 12;
            this.PHUCCAP.Width = 150;
            // 
            // TEN_FILECV
            // 
            this.TEN_FILECV.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILECV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILECV.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILECV.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILECV.Caption = "Tên file";
            this.TEN_FILECV.FieldName = "TEN_FILECV";
            this.TEN_FILECV.Name = "TEN_FILECV";
            this.TEN_FILECV.OptionsColumn.AllowEdit = false;
            this.TEN_FILECV.OptionsColumn.AllowFocus = false;
            this.TEN_FILECV.OptionsColumn.FixedWidth = true;
            this.TEN_FILECV.Visible = true;
            this.TEN_FILECV.VisibleIndex = 13;
            this.TEN_FILECV.Width = 100;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 14;
            this.FILL.Width = 20;
            // 
            // Frm_BaoCao_HopDongLaoDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 674);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_BaoCao_HopDongLaoDong";
            this.Text = "Danh sách hợp đồng lao động";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatFile;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.LookUpEdit cboData;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TENHOPDONG;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn THOIHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYKY;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHIEULUC;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHETHIEULUC;
        private DevExpress.XtraGrid.Columns.GridColumn MUCLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn PHUCCAP;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn BH_XAHOI;
        private DevExpress.XtraGrid.Columns.GridColumn BH_YTE;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private DevExpress.XtraEditors.LabelControl lblTuNgay;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private DevExpress.XtraEditors.LabelControl lblDenNgay;
        private DevExpress.XtraGrid.Columns.GridColumn BH_THATNGHIEP;



    }
}