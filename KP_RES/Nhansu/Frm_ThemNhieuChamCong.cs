﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using KP_RES.Class;

namespace KP_RES 
{// 03/08/2017 khang thêm nhiều hợp đồng lao động
    public partial class Frm_ThemNhieuChamCong : DevExpress.XtraEditors.XtraForm
    {
        private Byte[] sByteDataFile;
        private string sThang;
        private string sNam;
        public Frm_ThemNhieuChamCong()
        {
            InitializeComponent();
            cboThoigian.Properties.PopupFormSize = new Size(100, 100);
        }

        private void Frm_ThemNhieuHopDong_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
            LoadPermission();
            LoadCombo();
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("SELECT MA,THANG,NAM,SONGAY, CONVERT(VARCHAR, THANG) +'/' + CONVERT(VARCHAR, NAM) AS THANGNAM FROM DM_SONGAYLAMVIEC ORDER BY NAM DESC ,THANG DESC");
            cboThoigian.Properties.DataSource = myDT.Copy();
            cboThoigian.EditValue = myDT.Rows.Count;

        }
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sThongbao = "";
            if (!CheckInput())
                return;
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                // so sánh đối tượng cột trong gridview
                if (gridView2.GetRowCellValue(i, "CHON").ToString() == "True")
                {
                    SqlParameter[] sParameter = null;
                    String sSPs = "";
                    sSPs = "SP_InsertChamCong";

                    sParameter = new SqlParameter[5];
                    sParameter[0] = new SqlParameter("@THANG", (Object)(sThang));
                    sParameter[1] = new SqlParameter("@NAM", (Object)(sNam));
                    sParameter[2] = new SqlParameter("@NHANVIEN", (Object)gridView2.GetRowCellValue(i, "MA").ToString());
                    sParameter[3] = new SqlParameter("@SONGAY", (Object)txtSoNgay.Text);
                    sParameter[4] = new SqlParameter("@GHICHU", (Object)"");

                    this.Cursor = Cursors.WaitCursor;
                    Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
                    this.Cursor = Cursors.Default;
                    if (sBoolean)
                        sThongbao = "Lưu thành công";
                    else if (!sBoolean)
                        sThongbao = "Lưu không thành công";
                }
            }
            // xuất thông báo
            if (sThongbao == "")
                sThongbao = "Chưa chọn nhân viên";
            clsQLCongTy.MessageInformation(sThongbao, "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        
        private void LoaddataGridView()
        {
            gridView2.Focus();
            string sql = "SELECT MANHANVIEN AS MA,TENNHANVIEN AS TEN, NGAYSINH, CASE WHEN GIOITINH = 1 THEN 'Nam' ELSE N'Nữ' END AS GIOITINH, DIACHI, DIENTHOAI FROM DM_NHANVIEN WHERE SUDUNG = 1 AND MANHANVIEN <> '82' AND MANHANVIEN <> '83'  ORDER BY TEN";
            DataTable myDT = clsMain.ReturnDataTable(sql);
            // thêm 1 cột vào trong datatable
            myDT.Columns.Add("CHON", typeof (bool));
            for (int i = 0; i < myDT.Rows.Count; i++)
            {
                // gán dữ liệu vào cột
                myDT.Rows[i]["CHON"] = 0;
            }
            gridControl2.DataSource = myDT;
        }

        private void LoadPermission()
        {
            btnLuu.Enabled = clsUserManagement.AllowAdd("6051601");
        }

        private Boolean CheckInput()
        {
            if (cboThoigian.EditValue == "" || cboThoigian.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblThoigian.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboThoigian.Focus();
                return false;
            }
            return true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChontatca_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView2.DataRowCount; i++)
            {
                gridView2.SetRowCellValue(i, "CHON", 1);
            }
        }

        private void btnBochontatca_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView2.DataRowCount; i++)
            {
                gridView2.SetRowCellValue(i, "CHON", 0);
            }
        }

        private void cboThoigian_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                txtSoNgay.Text = ((DataRowView)editor.GetSelectedDataRow())["SONGAY"].ToString();
                sThang = ((DataRowView)editor.GetSelectedDataRow())["THANG"].ToString();
                sNam = ((DataRowView)editor.GetSelectedDataRow())["NAM"].ToString();
            }
            catch (Exception)
            {
            }
           
        }



        //private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        //{
        //    string sChon = gridView2.GetFocusedRowCellValue("CHON").ToString();
        //}
    }
}