﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Baohiem : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Baohiem()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Baohiem_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtXH.SelectAll();
        }

        string sma = "";

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            String sSQL = "";
            sSQL += "EXEC SP_UpdateCauHinh " + clsMain.SQLString("BH_XAHOI") + "," + clsMain.SQLString(txtXH.Text) + "\n";
            sSQL += "EXEC SP_UpdateCauHinh " + clsMain.SQLString("BH_YTE") + "," + clsMain.SQLString(txtYT.Text) + "\n";
            sSQL += "EXEC SP_UpdateCauHinh " + clsMain.SQLString("BH_THATNGHIEP") + "," + clsMain.SQLString(txtTN.Text) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                clsQLCongTy.MessageInformation("Lưu thành công", "Thông báo");
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void LoaddataGridView()
        {

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectCauHinh " + clsMain.SQLString("BH_XAHOI"));
            txtXH.Text = myDT.Rows[0][0].ToString();

            myDT = clsMain.ReturnDataTable("EXEC SP_SelectCauHinh " + clsMain.SQLString("BH_YTE"));
            txtYT.Text = myDT.Rows[0][0].ToString();

            myDT = clsMain.ReturnDataTable("EXEC SP_SelectCauHinh " + clsMain.SQLString("BH_THATNGHIEP"));
            txtTN.Text = myDT.Rows[0][0].ToString();
        }

        private void LoadPermission()
        {
            btnLuu.Enabled = true ;
        }

        private Boolean CheckInput()
        {
            if (txtXH.Text == "")
            {
                //DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtXH.Focus();
                return false;
            }
            return true;
        }
    }
}