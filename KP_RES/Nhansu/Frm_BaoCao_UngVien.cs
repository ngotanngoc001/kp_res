﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;

namespace KP_RES
{
    public partial class Frm_BaoCao_UngVien : DevExpress.XtraEditors.XtraForm
    {
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_BaoCao_UngVien()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisibleChoose();
        }

        private void VisibleChoose()
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

            pnChoose.Visible = (optGroup.SelectedIndex == 0) ? false : true;

            if (optGroup.SelectedIndex == 1)
            {
                Visible(true);
                clsQLCongTy.LoadCombo("SP_ComBoTuyenDungA", cboData);
            }
        }

        private void Visible(Boolean sBoolean)
        {
            lblTitle.Visible = cboData.Visible = sBoolean;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";

                if (optGroup.EditValue != "0")
                    sMa = cboData.EditValue.ToString();

                SqlParameter[] sParameter = new SqlParameter[2];
                sParameter[0] = new SqlParameter("@MA", (Object)sMa);
                sParameter[1] = new SqlParameter("@STYLE", (Object)optGroup.EditValue);

                DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectBCUngVien", sParameter);
                grcUngVien.DataSource = myDT;
                grvUngVien.Columns["TUYENDUNG"].GroupIndex = 0;
                grvUngVien.ExpandAllGroups();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvUngVien.Focus();
                grvUngVien.FocusedRowHandle = grvUngVien.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                grvUngVien.Focus();
                grvUngVien.FocusedRowHandle = grvUngVien.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (grvUngVien.FocusedRowHandle == grvUngVien.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                grvUngVien.Focus();
                grvUngVien.FocusedRowHandle = grvUngVien.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (grvUngVien.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                grvUngVien.Focus();
                grvUngVien.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.grvUngVien.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                grcUngVien.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                grcUngVien.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvUngVien.RowCount > 0)
                {
                    DataTable myDT = (DataTable)grcUngVien.DataSource;

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex != 0)
                        Description = "( " + Description + " : " + cboData.Text + " )";
                   
                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;

                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 75;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    clsQLCongTy.MessageInformation("Không có dữ liệu", "Thông báo");
            }
            catch{
            }
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            Boolean sBoolean = (columns.Contains(columnName)) ? true : false;

            return sBoolean;
        }

        private void grvUngVien_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void grvUngVien_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (grvUngVien.RowCount == 0 || grvUngVien.FocusedRowHandle < 0)
                return;

            sNameFile = grvUngVien.GetRowCellValue(grvUngVien.FocusedRowHandle, "TEN_FILECV").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(grvUngVien.GetRowCellValue(grvUngVien.FocusedRowHandle, "FILECV"));
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = "(xlsx,doc)|*.xlsx;*.doc|all files|*.*";
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile);
        }

        

        
       
       


    }
}