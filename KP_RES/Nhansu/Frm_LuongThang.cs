﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_LuongThang : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";

        public Frm_LuongThang()
        {
            InitializeComponent();
        }

        private void Frm_LuongThang_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            String sSPs = "SP_InsertLuongThang";

            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
            sParameter[1] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs(sSPs, sParameter);
            gridControl2.DataSource = myDT;
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            String sSPs = "SP_UpdateLuongThang";

            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
            sParameter[1] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs(sSPs, sParameter);
            gridControl2.DataSource = myDT;
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 1; i < 13; i++)
            {
                myDT.Rows.Add(i.ToString(), "Tháng " + i);
            }
            clsQLCongTy.LoadCombo(myDT, cboThang);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 2016; i < 2027; i++)
            {
                myDT.Rows.Add(i.ToString(), "Năm " + i);
            }
            clsQLCongTy.LoadCombo(myDT, cboNam);
        }

        private void LoadPermission()
        {
            btnXem.Enabled = clsUserManagement.AllowAdd("7051607");
            btnLamMoi.Enabled = clsUserManagement.AllowEdit("7051607");
        }

        private void SetRongControl()
        {
            sMa = "";
            cboThang.EditValue = cboThang.Properties.GetDataSourceValue(cboThang.Properties.ValueMember, DateTime.Now.Month - 1);
            cboNam.EditValue = cboNam.Properties.GetDataSourceValue(cboNam.Properties.ValueMember, DateTime.Now.Year - 2016);
        }

       

    }
}