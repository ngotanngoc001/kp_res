﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using KP_RES.Class;

namespace KP_RES 
{// 03/08/2017 khang thêm nhiều hợp đồng lao động
    public partial class Frm_ThemNhieuHopDong : DevExpress.XtraEditors.XtraForm
    {
        private Byte[] sByteDataFile;
        public Frm_ThemNhieuHopDong()
        {
            InitializeComponent();
        }

        private void Frm_ThemNhieuHopDong_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sThongbao="";
            if (!CheckInput())
                return;

            // duyệt từng dòng trong gridview
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                // so sánh đối tượng cột trong gridview
                if (gridView2.GetRowCellValue(i, "CHON").ToString() == "True")
                {
                    TimeSpan sThoihan = new System.TimeSpan(3650, 0, 0, 0);
                    DateTime sNgayhetHD = DateTime.Now.Add(sThoihan);
                    SqlParameter[] sParameter = null;
                    String sSPs = "";
                    sSPs = "SP_InsertHopDongLaoDong2";

                    sParameter = new SqlParameter[18];
                    sParameter[0] = new SqlParameter("@TENHOPDONG", (Object)"HĐLĐ Tính Lương");
                    sParameter[1] = new SqlParameter("@NGUOILAODONG", (Object)gridView2.GetRowCellValue(i, "MA").ToString());
                    sParameter[2] = new SqlParameter("@NGUOISDLAODONG", (Object)"82");
                    sParameter[3] = new SqlParameter("@THOIHAN", (Object)"Không thời hạn");
                    sParameter[4] = new SqlParameter("@NGAYKY", (Object)"2017-07-01 13:56:49.000");
                    sParameter[5] = new SqlParameter("@NGAYHIEULUC", (Object)"2017-07-01 13:56:49.000");
                    sParameter[6] = new SqlParameter("@NGAYHETHIEULUC", (Object)"2025-07-31 13:56:49.000");
                    sParameter[7] = new SqlParameter("@MUCLUONG", (Object)txtMucLuong.Text);
                    sParameter[8] = new SqlParameter("@LUONGTHUCTE", (Object)txtLuongThucTe.Text);
                    sParameter[9] = new SqlParameter("@HINHTHUC", (Object)"Tiền mặt");
                    sParameter[10] = new SqlParameter("@NGAYTRALUONG", (Object)"Ngày cuối tháng");
                    sParameter[11] = new SqlParameter("@PHUCAP", (Object)"9");
                    sParameter[12] = new SqlParameter("@BH_XAHOI", (Object)"0");
                    sParameter[13] = new SqlParameter("@BH_YTE", (Object)"0");
                    sParameter[14] = new SqlParameter("@BH_THATNGHIEP", (Object)"0");
                    sParameter[15] = new SqlParameter("@THOIGIAN_LV", (Object)"8h/ngày");
                    sParameter[16] = new SqlParameter("@DENBU_HD", (Object)"");
                    sParameter[17] = new SqlParameter("@GHICHU", (Object)"");
                    //sParameter[18] = new SqlParameter("@TEN_FILES", (Object)"");
                    //sParameter[19] = new SqlParameter("@FILES", (Object)"");
                    //sParameter[19].Value = sByteDataFile;

                    Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
                    if (sBoolean)
                        sThongbao = "Lưu thành công";
                    else if (!sBoolean)
                        sThongbao = "Lưu không thành công";
                }
            }
            // xuất thông báo
            if (sThongbao == "")
                sThongbao = "Chưa chọn nhân viên";
            clsQLCongTy.MessageInformation(sThongbao, "Thông báo");
            LoaddataGridView();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(false);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectNhanVien2");
            // thêm 1 cột vào trong datatable
            myDT.Columns.Add("CHON", typeof (bool));
            for (int i = 0; i < myDT.Rows.Count; i++)
            {
                // gán dữ liệu vào cột
                myDT.Rows[i]["CHON"] = 0;
            }
            gridControl2.DataSource = myDT;
        }

        private void LoadPermission()
        {
            btnLuu.Enabled = clsUserManagement.AllowAdd("6051601");
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtMucLuong.Properties.ReadOnly = sBoolean;
            txtLuongThucTe.Properties.ReadOnly = sBoolean;
            
        }

        private void SetRongControl()
        {
            txtMucLuong.Text = "";
            txtLuongThucTe.Text = "";
        }

        private Boolean CheckInput()
        {
            if (txtMucLuong.Text == "")
            {
                XtraMessageBox.Show("Chưa nhập mức lương", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMucLuong.Focus();
                return false;
            }
            else if (txtLuongThucTe.Text == "")
            {
                XtraMessageBox.Show("Chưa nhập lương thực tế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLuongThucTe.Focus();
                return false;
            }
            return true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChontatca_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView2.DataRowCount; i++)
            {
                gridView2.SetRowCellValue(i, "CHON", 1);
            }
        }

        private void btnBochontatca_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView2.DataRowCount; i++)
            {
                gridView2.SetRowCellValue(i, "CHON", 0);
            }
        }

        //private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        //{
        //    string sChon = gridView2.GetFocusedRowCellValue("CHON").ToString();
        //}
    }
}