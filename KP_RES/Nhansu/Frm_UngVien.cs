﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_UngVien : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;
        private Byte[] sByteDataImage;

        public Frm_UngVien()
        {
            InitializeComponent();
        }

        private void Frm_UngVien_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteUngVien " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            if (txtFileCV.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFileCV.Text);

            Byte[] sImage = clsQLCongTy.ReturnByteImage(picHinhAnh);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertUngVien";

                sParameter = new SqlParameter[18];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGAYSINH", (DateTime)dtpNgaySinh.EditValue);
                sParameter[2] = new SqlParameter("@GIOITINH", (Object)cboGioiTinh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@DIACHI", (Object)txtDiaChi.Text);
                sParameter[4] = new SqlParameter("@DIENTHOAI", (Object)txtDienThoai.Text);
                sParameter[5] = new SqlParameter("@EMAIL", (Object)txtEmail.Text);
                sParameter[6] = new SqlParameter("@HOCVAN", (Object)txtHocVan.Text);
                sParameter[7] = new SqlParameter("@NGOAINGU", (Object)txtNgoaiNgu.Text);
                sParameter[8] = new SqlParameter("@TINHOC", (Object)txtTinHoc.Text);
                sParameter[9] = new SqlParameter("@KINHNGHIEM", (Object)txtKinhNghiem.Text);
                sParameter[10] = new SqlParameter("@MUCLUONG", (Object)txtMucLuong.Text);
                sParameter[11] = new SqlParameter("@KYNANG", (Object)txtKyNang.Text);
                sParameter[12] = new SqlParameter("@TUYENDUNG", (Object)cboTuyenDung.EditValue.ToString());
                sParameter[13] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[14] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[14].Value = sImage;
                sParameter[15] = new SqlParameter("@TEN_FILECV", (Object)sNameFile);
                sParameter[16] = new SqlParameter("@FILECV", SqlDbType.VarBinary, int.MaxValue);
                sParameter[16].Value = sByteDataFile;
                sParameter[17] = new SqlParameter("@TRUNGTUYEN", (Object)(chkTrungTuyen.Checked == true ? "1" : "0"));
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateUngVien";

                sParameter = new SqlParameter[19];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGAYSINH", (DateTime)dtpNgaySinh.EditValue);
                sParameter[2] = new SqlParameter("@GIOITINH", (Object)cboGioiTinh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@DIACHI", (Object)txtDiaChi.Text);
                sParameter[4] = new SqlParameter("@DIENTHOAI", (Object)txtDienThoai.Text);
                sParameter[5] = new SqlParameter("@EMAIL", (Object)txtEmail.Text);
                sParameter[6] = new SqlParameter("@HOCVAN", (Object)txtHocVan.Text);
                sParameter[7] = new SqlParameter("@NGOAINGU", (Object)txtNgoaiNgu.Text);
                sParameter[8] = new SqlParameter("@TINHOC", (Object)txtTinHoc.Text);
                sParameter[9] = new SqlParameter("@KINHNGHIEM", (Object)txtKinhNghiem.Text);
                sParameter[10] = new SqlParameter("@MUCLUONG", (Object)txtMucLuong.Text);
                sParameter[11] = new SqlParameter("@KYNANG", (Object)txtKyNang.Text);
                sParameter[12] = new SqlParameter("@TUYENDUNG", (Object)cboTuyenDung.EditValue.ToString());
                sParameter[13] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[14] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[14].Value = sImage;
                sParameter[15] = new SqlParameter("@TEN_FILECV", (Object)sNameFile);
                sParameter[16] = new SqlParameter("@FILECV", SqlDbType.VarBinary, int.MaxValue);
                sParameter[16].Value = sByteDataFile;
                sParameter[17] = new SqlParameter("@TRUNGTUYEN", (Object)(chkTrungTuyen.Checked == true ? "1" : "0"));
                sParameter[18] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtDiaChi.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIACHI").ToString();
            txtDienThoai.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            txtEmail.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            txtHocVan.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HOCVAN").ToString();
            txtNgoaiNgu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGOAINGU").ToString();
            txtTinHoc.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TINHOC").ToString();
            txtKinhNghiem.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KINHNGHIEM").ToString();
            txtMucLuong.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MUCLUONG").ToString();
            txtKyNang.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KYNANG").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILECV").ToString();
            Boolean sGioiTinh = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOITINH_ID").ToString());
            cboGioiTinh.EditValue = sGioiTinh == true ? "1" : "0";
            cboTuyenDung.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TUYENDUNG_ID").ToString());
            cboTrangThai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TRANGTHAI_ID").ToString());
            dtpNgaySinh.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString());
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILECV").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILECV"));
            sByteDataImage = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IMAGE"));
            picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            chkTrungTuyen.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TRUNGTUYEN_ID").ToString());

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectUngVien");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");
            myDT.Rows.Add("1", "Nam");
            myDT.Rows.Add("0", "Nữ");
            clsQLCongTy.LoadCombo(myDT, cboGioiTinh);
                
            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoSatus " + clsMain.SQLString("02"));
            clsQLCongTy.LoadCombo(myDT, cboTrangThai);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoTuyenDung");
            clsQLCongTy.LoadCombo(myDT, cboTuyenDung);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("5051602");
            btnSua.Enabled = clsUserManagement.AllowEdit("5051602");
            btnXoa.Enabled = clsUserManagement.AllowDelete("5051602");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTen.Properties.ReadOnly = sBoolean;
            txtDiaChi.Properties.ReadOnly = sBoolean;
            txtDienThoai.Properties.ReadOnly = sBoolean;
            txtEmail.Properties.ReadOnly = sBoolean;
            txtHocVan.Properties.ReadOnly = sBoolean;
            txtNgoaiNgu.Properties.ReadOnly = sBoolean;
            txtTinHoc.Properties.ReadOnly = sBoolean;
            txtKinhNghiem.Properties.ReadOnly = sBoolean;
            txtMucLuong.Properties.ReadOnly = sBoolean;
            txtKyNang.Properties.ReadOnly = sBoolean;
            cboGioiTinh.Properties.ReadOnly = sBoolean;
            cboTuyenDung.Properties.ReadOnly = sBoolean;
            cboTrangThai.Properties.ReadOnly = sBoolean;
            dtpNgaySinh.Properties.ReadOnly = sBoolean;
            chkTrungTuyen.Properties.ReadOnly = sBoolean;
            btnFileCV.Enabled = !sBoolean;
            picHinhAnh.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            sByteDataImage = new Byte[1];
            txtTen.Text = "";
            txtDiaChi.Text = "";
            txtDienThoai.Text = "";
            txtEmail.Text = "";
            txtHocVan.Text = "";
            txtNgoaiNgu.Text = "";
            txtTinHoc.Text = "";
            txtKinhNghiem.Text = "";
            txtMucLuong.Text = "";
            txtKyNang.Text = "";
            txtTenFile.Text = "";
            txtFileCV.Text = "";
            cboGioiTinh.EditValue = cboGioiTinh.Properties.GetDataSourceValue(cboGioiTinh.Properties.ValueMember, 0);
            cboTuyenDung.EditValue = cboTuyenDung.Properties.GetDataSourceValue(cboTuyenDung.Properties.ValueMember, 0);
            cboTrangThai.EditValue = cboTrangThai.Properties.GetDataSourceValue(cboTrangThai.Properties.ValueMember, 0);
            dtpNgaySinh.EditValue = clsMain.GetServerDate();
            chkTrungTuyen.Checked = false;
            picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (cboTuyenDung.EditValue == "" || cboTuyenDung.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblTuyenDung.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTuyenDung.Focus();
                return false;
            }
            if (cboTrangThai.EditValue == "" || cboTrangThai.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblTrangThai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTrangThai.Focus();
                return false;
            }
            if (txtFileCV.Text != "")
            {
                if (!File.Exists(txtFileCV.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFileCV.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFileCV_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFileCV.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        private void picHinhAnh_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.InitialDirectory = Application.StartupPath + "\\Images";
            sOpenFile.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif";
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Title = "Chọn ảnh";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                picHinhAnh.ImageLocation = sOpenFile.FileName;
                picHinhAnh.BackgroundImage = null;
            }
            else
                picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            
        }
    }
}