﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_UngLuong : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_UngLuong()
        {
            InitializeComponent();
        }

        private void Frm_UngLuong_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtSoKH.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteUngLuong " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtSoKH.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertUngLuong";

                sParameter = new SqlParameter[8];
                sParameter[0] = new SqlParameter("@SOKH", (Object)txtSoKH.Text);
                sParameter[1] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());
                sParameter[3] = new SqlParameter("@NHANVIEN", (Object)cboNhanVien.EditValue.ToString());
                sParameter[4] = new SqlParameter("@SOTIEN", (Object)txtSoTien.Text);
                sParameter[5] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[6] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[7] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[7].Value = sByteDataFile;
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateUngLuong";

                sParameter = new SqlParameter[9];
                sParameter[0] = new SqlParameter("@SOKH", (Object)txtSoKH.Text);
                sParameter[1] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());
                sParameter[3] = new SqlParameter("@NHANVIEN", (Object)cboNhanVien.EditValue.ToString());
                sParameter[4] = new SqlParameter("@SOTIEN", (Object)txtSoTien.Text);
                sParameter[5] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[6] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[7] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[7].Value = sByteDataFile;
                sParameter[8] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtSoKH.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOKH").ToString();
            txtSoTien.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOTIEN").ToString();
            txtGhiChu.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            cboThang.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THANG").ToString();
            cboNam.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAM").ToString();
            cboNhanVien.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NHANVIEN_ID").ToString());
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectUngLuong");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 1; i < 13; i++)
                myDT.Rows.Add(i.ToString(), "Tháng " + i);
            clsQLCongTy.LoadCombo(myDT, cboThang);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 2016; i < 2027; i++)
                myDT.Rows.Add(i.ToString(), "Năm " + i);
            clsQLCongTy.LoadCombo(myDT, cboNam);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoNhanVien");
            clsQLCongTy.LoadCombo(myDT, cboNhanVien);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("7051606");
            btnSua.Enabled = clsUserManagement.AllowEdit("7051606");
            btnXoa.Enabled = clsUserManagement.AllowDelete("7051606");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtSoKH.Properties.ReadOnly = sBoolean;
            txtSoTien.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            cboThang.Properties.ReadOnly = sBoolean;
            cboNhanVien.Properties.ReadOnly = sBoolean;
            cboNam.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtSoKH.Text = "";
            txtSoTien.Text = "";
            txtGhiChu.Text = "";
            txtTenFile.Text = "";
            txtFile.Text = "";
            cboThang.EditValue = cboThang.Properties.GetDataSourceValue(cboThang.Properties.ValueMember, DateTime.Now.Month - 1);
            cboNam.EditValue = cboNam.Properties.GetDataSourceValue(cboNam.Properties.ValueMember, DateTime.Now.Year - 2016);  
            cboNhanVien.EditValue = cboNhanVien.Properties.GetDataSourceValue(cboNhanVien.Properties.ValueMember, 0);
        }

        private Boolean CheckInput()
        {
            if (txtSoKH.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblSoKH.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoKH.Focus();
                return false;
            }
            if (cboThang.EditValue == "" || cboThang.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblThang.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboThang.Focus();
                return false;
            }
            if (cboNam.EditValue == "" || cboNam.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNam.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNam.Focus();
                return false;
            }
            if (cboNhanVien.EditValue == "" || cboNhanVien.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNhanVien.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNhanVien.Focus();
                return false;
            }
            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

       

    }
}