﻿namespace KP_RES 
{
    partial class Frm_ThemNhieuHopDong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GIOITINH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONGBAN_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHUCVU_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOCVAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGOAINGU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TINHOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KINHNGHIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KYNANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONGBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHUCVU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TUYENDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ck_chon = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.btnBochontatca = new DevExpress.XtraEditors.SimpleButton();
            this.btnChontatca = new DevExpress.XtraEditors.SimpleButton();
            this.txtLuongThucTe = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblMucLuong = new DevExpress.XtraEditors.LabelControl();
            this.txtMucLuong = new DevExpress.XtraEditors.TextEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_chon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongThucTe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucLuong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 57);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ck_chon});
            this.gridControl2.Size = new System.Drawing.Size(945, 364);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GIOITINH_ID,
            this.PHONGBAN_ID,
            this.CHUCVU_ID,
            this.TRANGTHAI_ID,
            this.FILECV,
            this.CHON,
            this.STT,
            this.MA,
            this.TEN,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.DIENTHOAI,
            this.EMAIL,
            this.HOCVAN,
            this.NGOAINGU,
            this.TINHOC,
            this.KINHNGHIEM,
            this.KYNANG,
            this.PHONGBAN,
            this.CHUCVU,
            this.TRANGTHAI,
            this.TUYENDUNG,
            this.TEN_FILECV,
            this.gridColumn1,
            this.SUDUNG});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // GIOITINH_ID
            // 
            this.GIOITINH_ID.FieldName = "GIOITINH_ID";
            this.GIOITINH_ID.Name = "GIOITINH_ID";
            this.GIOITINH_ID.OptionsColumn.AllowEdit = false;
            this.GIOITINH_ID.OptionsColumn.AllowFocus = false;
            this.GIOITINH_ID.OptionsColumn.FixedWidth = true;
            // 
            // PHONGBAN_ID
            // 
            this.PHONGBAN_ID.FieldName = "PHONGBAN_ID";
            this.PHONGBAN_ID.Name = "PHONGBAN_ID";
            this.PHONGBAN_ID.OptionsColumn.AllowEdit = false;
            this.PHONGBAN_ID.OptionsColumn.AllowFocus = false;
            this.PHONGBAN_ID.OptionsColumn.FixedWidth = true;
            // 
            // CHUCVU_ID
            // 
            this.CHUCVU_ID.FieldName = "CHUCVU_ID";
            this.CHUCVU_ID.Name = "CHUCVU_ID";
            this.CHUCVU_ID.OptionsColumn.AllowEdit = false;
            this.CHUCVU_ID.OptionsColumn.AllowFocus = false;
            this.CHUCVU_ID.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.FixedWidth = true;
            // 
            // FILECV
            // 
            this.FILECV.FieldName = "FILECV";
            this.FILECV.Name = "FILECV";
            this.FILECV.OptionsColumn.AllowEdit = false;
            this.FILECV.OptionsColumn.AllowFocus = false;
            this.FILECV.OptionsColumn.FixedWidth = true;
            // 
            // CHON
            // 
            this.CHON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHON.AppearanceHeader.Options.UseFont = true;
            this.CHON.AppearanceHeader.Options.UseTextOptions = true;
            this.CHON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHON.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CHON.Caption = "Chọn";
            this.CHON.FieldName = "CHON";
            this.CHON.Name = "CHON";
            this.CHON.OptionsColumn.FixedWidth = true;
            this.CHON.Visible = true;
            this.CHON.VisibleIndex = 0;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 1;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 2;
            this.MA.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Họ và tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 3;
            this.TEN.Width = 250;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 4;
            this.NGAYSINH.Width = 100;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceCell.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 5;
            this.GIOITINH.Width = 85;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DIACHI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 6;
            this.DIACHI.Width = 200;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 7;
            this.DIENTHOAI.Width = 100;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceCell.Options.UseTextOptions = true;
            this.EMAIL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 8;
            this.EMAIL.Width = 150;
            // 
            // HOCVAN
            // 
            this.HOCVAN.AppearanceCell.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HOCVAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HOCVAN.AppearanceHeader.Options.UseFont = true;
            this.HOCVAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOCVAN.Caption = "Trình độ học vấn";
            this.HOCVAN.FieldName = "HOCVAN";
            this.HOCVAN.Name = "HOCVAN";
            this.HOCVAN.OptionsColumn.AllowEdit = false;
            this.HOCVAN.OptionsColumn.AllowFocus = false;
            this.HOCVAN.OptionsColumn.FixedWidth = true;
            this.HOCVAN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.HOCVAN.Visible = true;
            this.HOCVAN.VisibleIndex = 9;
            this.HOCVAN.Width = 200;
            // 
            // NGOAINGU
            // 
            this.NGOAINGU.AppearanceCell.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGOAINGU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGOAINGU.AppearanceHeader.Options.UseFont = true;
            this.NGOAINGU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGOAINGU.Caption = "Ngoại ngữ";
            this.NGOAINGU.FieldName = "NGOAINGU";
            this.NGOAINGU.Name = "NGOAINGU";
            this.NGOAINGU.OptionsColumn.AllowEdit = false;
            this.NGOAINGU.OptionsColumn.AllowFocus = false;
            this.NGOAINGU.OptionsColumn.FixedWidth = true;
            this.NGOAINGU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.NGOAINGU.Visible = true;
            this.NGOAINGU.VisibleIndex = 10;
            this.NGOAINGU.Width = 200;
            // 
            // TINHOC
            // 
            this.TINHOC.AppearanceCell.Options.UseTextOptions = true;
            this.TINHOC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TINHOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TINHOC.AppearanceHeader.Options.UseFont = true;
            this.TINHOC.AppearanceHeader.Options.UseTextOptions = true;
            this.TINHOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TINHOC.Caption = "Tin học";
            this.TINHOC.FieldName = "TINHOC";
            this.TINHOC.Name = "TINHOC";
            this.TINHOC.OptionsColumn.AllowEdit = false;
            this.TINHOC.OptionsColumn.AllowFocus = false;
            this.TINHOC.OptionsColumn.FixedWidth = true;
            this.TINHOC.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TINHOC.Visible = true;
            this.TINHOC.VisibleIndex = 11;
            this.TINHOC.Width = 150;
            // 
            // KINHNGHIEM
            // 
            this.KINHNGHIEM.AppearanceCell.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KINHNGHIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KINHNGHIEM.AppearanceHeader.Options.UseFont = true;
            this.KINHNGHIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KINHNGHIEM.Caption = "Kinh nghiệm";
            this.KINHNGHIEM.FieldName = "KINHNGHIEM";
            this.KINHNGHIEM.Name = "KINHNGHIEM";
            this.KINHNGHIEM.OptionsColumn.AllowEdit = false;
            this.KINHNGHIEM.OptionsColumn.AllowFocus = false;
            this.KINHNGHIEM.OptionsColumn.FixedWidth = true;
            this.KINHNGHIEM.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.KINHNGHIEM.Visible = true;
            this.KINHNGHIEM.VisibleIndex = 12;
            this.KINHNGHIEM.Width = 350;
            // 
            // KYNANG
            // 
            this.KYNANG.AppearanceCell.Options.UseTextOptions = true;
            this.KYNANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KYNANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KYNANG.AppearanceHeader.Options.UseFont = true;
            this.KYNANG.AppearanceHeader.Options.UseTextOptions = true;
            this.KYNANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KYNANG.Caption = "Kỹ năng";
            this.KYNANG.FieldName = "KYNANG";
            this.KYNANG.Name = "KYNANG";
            this.KYNANG.OptionsColumn.AllowEdit = false;
            this.KYNANG.OptionsColumn.AllowFocus = false;
            this.KYNANG.OptionsColumn.FixedWidth = true;
            this.KYNANG.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.KYNANG.Visible = true;
            this.KYNANG.VisibleIndex = 13;
            this.KYNANG.Width = 350;
            // 
            // PHONGBAN
            // 
            this.PHONGBAN.AppearanceCell.Options.UseTextOptions = true;
            this.PHONGBAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHONGBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PHONGBAN.AppearanceHeader.Options.UseFont = true;
            this.PHONGBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.PHONGBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHONGBAN.Caption = "Phòng ban";
            this.PHONGBAN.FieldName = "PHONGBAN";
            this.PHONGBAN.Name = "PHONGBAN";
            this.PHONGBAN.OptionsColumn.AllowEdit = false;
            this.PHONGBAN.OptionsColumn.AllowFocus = false;
            this.PHONGBAN.OptionsColumn.FixedWidth = true;
            this.PHONGBAN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PHONGBAN.Visible = true;
            this.PHONGBAN.VisibleIndex = 14;
            this.PHONGBAN.Width = 150;
            // 
            // CHUCVU
            // 
            this.CHUCVU.AppearanceCell.Options.UseTextOptions = true;
            this.CHUCVU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHUCVU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHUCVU.AppearanceHeader.Options.UseFont = true;
            this.CHUCVU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHUCVU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHUCVU.Caption = "Chức vụ";
            this.CHUCVU.FieldName = "CHUCVU";
            this.CHUCVU.Name = "CHUCVU";
            this.CHUCVU.OptionsColumn.AllowEdit = false;
            this.CHUCVU.OptionsColumn.AllowFocus = false;
            this.CHUCVU.OptionsColumn.FixedWidth = true;
            this.CHUCVU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.CHUCVU.Visible = true;
            this.CHUCVU.VisibleIndex = 15;
            this.CHUCVU.Width = 150;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 16;
            this.TRANGTHAI.Width = 150;
            // 
            // TUYENDUNG
            // 
            this.TUYENDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TUYENDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TUYENDUNG.AppearanceHeader.Options.UseFont = true;
            this.TUYENDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TUYENDUNG.Caption = "Đợt tuyển dụng";
            this.TUYENDUNG.FieldName = "TUYENDUNG";
            this.TUYENDUNG.Name = "TUYENDUNG";
            this.TUYENDUNG.OptionsColumn.AllowEdit = false;
            this.TUYENDUNG.OptionsColumn.AllowFocus = false;
            this.TUYENDUNG.OptionsColumn.FixedWidth = true;
            this.TUYENDUNG.Visible = true;
            this.TUYENDUNG.VisibleIndex = 17;
            this.TUYENDUNG.Width = 250;
            // 
            // TEN_FILECV
            // 
            this.TEN_FILECV.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILECV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILECV.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILECV.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILECV.Caption = "File CV";
            this.TEN_FILECV.FieldName = "TEN_FILECV";
            this.TEN_FILECV.Name = "TEN_FILECV";
            this.TEN_FILECV.OptionsColumn.AllowEdit = false;
            this.TEN_FILECV.OptionsColumn.AllowFocus = false;
            this.TEN_FILECV.OptionsColumn.FixedWidth = true;
            this.TEN_FILECV.Visible = true;
            this.TEN_FILECV.VisibleIndex = 18;
            this.TEN_FILECV.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 19;
            this.gridColumn1.Width = 20;
            // 
            // SUDUNG
            // 
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            // 
            // ck_chon
            // 
            this.ck_chon.AutoHeight = false;
            this.ck_chon.Name = "ck_chon";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(945, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 421);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 63);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 295);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 122);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 51);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 173);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 61);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 234);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 61);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 61);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 61);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 61);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 358);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 61);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 61);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(945, 54);
            this.panelControl1.TabIndex = 5;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.btnBochontatca);
            this.pal_ban.Controls.Add(this.btnChontatca);
            this.pal_ban.Controls.Add(this.txtLuongThucTe);
            this.pal_ban.Controls.Add(this.labelControl1);
            this.pal_ban.Controls.Add(this.lblMucLuong);
            this.pal_ban.Controls.Add(this.txtMucLuong);
            this.pal_ban.Controls.Add(this.btnLuu);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(941, 49);
            this.pal_ban.TabIndex = 0;
            // 
            // btnBochontatca
            // 
            this.btnBochontatca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBochontatca.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBochontatca.Appearance.Options.UseFont = true;
            this.btnBochontatca.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btnBochontatca.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBochontatca.Location = new System.Drawing.Point(699, 7);
            this.btnBochontatca.Margin = new System.Windows.Forms.Padding(4);
            this.btnBochontatca.Name = "btnBochontatca";
            this.btnBochontatca.Size = new System.Drawing.Size(141, 35);
            this.btnBochontatca.TabIndex = 50;
            this.btnBochontatca.Text = "&Bỏ Chọn tất cả";
            this.btnBochontatca.Click += new System.EventHandler(this.btnBochontatca_Click);
            // 
            // btnChontatca
            // 
            this.btnChontatca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChontatca.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChontatca.Appearance.Options.UseFont = true;
            this.btnChontatca.Image = global::KP_RES.Properties.Resources.checked_checkbox_26;
            this.btnChontatca.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChontatca.Location = new System.Drawing.Point(579, 7);
            this.btnChontatca.Margin = new System.Windows.Forms.Padding(4);
            this.btnChontatca.Name = "btnChontatca";
            this.btnChontatca.Size = new System.Drawing.Size(112, 35);
            this.btnChontatca.TabIndex = 49;
            this.btnChontatca.Text = "&Chọn tất cả";
            this.btnChontatca.Click += new System.EventHandler(this.btnChontatca_Click);
            // 
            // txtLuongThucTe
            // 
            this.txtLuongThucTe.EnterMoveNextControl = true;
            this.txtLuongThucTe.Location = new System.Drawing.Point(404, 16);
            this.txtLuongThucTe.Name = "txtLuongThucTe";
            this.txtLuongThucTe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongThucTe.Properties.Appearance.Options.UseFont = true;
            this.txtLuongThucTe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtLuongThucTe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtLuongThucTe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLuongThucTe.Size = new System.Drawing.Size(141, 26);
            this.txtLuongThucTe.TabIndex = 41;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(298, 19);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 19);
            this.labelControl1.TabIndex = 40;
            this.labelControl1.Text = "Lương thực tế";
            // 
            // lblMucLuong
            // 
            this.lblMucLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMucLuong.Location = new System.Drawing.Point(10, 19);
            this.lblMucLuong.Margin = new System.Windows.Forms.Padding(4);
            this.lblMucLuong.Name = "lblMucLuong";
            this.lblMucLuong.Size = new System.Drawing.Size(75, 19);
            this.lblMucLuong.TabIndex = 38;
            this.lblMucLuong.Text = "Mức lương";
            // 
            // txtMucLuong
            // 
            this.txtMucLuong.EnterMoveNextControl = true;
            this.txtMucLuong.Location = new System.Drawing.Point(116, 16);
            this.txtMucLuong.Name = "txtMucLuong";
            this.txtMucLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucLuong.Properties.Appearance.Options.UseFont = true;
            this.txtMucLuong.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMucLuong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMucLuong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMucLuong.Size = new System.Drawing.Size(141, 26);
            this.txtMucLuong.TabIndex = 39;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(848, 7);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(76, 35);
            this.btnLuu.TabIndex = 36;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_ThemNhieuHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 421);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Name = "Frm_ThemNhieuHopDong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm nhiều hợp đồng";
            this.Load += new System.EventHandler(this.Frm_ThemNhieuHopDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_chon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongThucTe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucLuong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn HOCVAN;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn NGOAINGU;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn TINHOC;
        private DevExpress.XtraGrid.Columns.GridColumn FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn CHUCVU_ID;
        private DevExpress.XtraGrid.Columns.GridColumn KINHNGHIEM;
        private DevExpress.XtraGrid.Columns.GridColumn PHONGBAN;
        private DevExpress.XtraGrid.Columns.GridColumn KYNANG;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TUYENDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn PHONGBAN_ID;
        private DevExpress.XtraGrid.Columns.GridColumn CHUCVU;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ck_chon;
        private DevExpress.XtraGrid.Columns.GridColumn CHON;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.SimpleButton btnBochontatca;
        private DevExpress.XtraEditors.SimpleButton btnChontatca;
        private DevExpress.XtraEditors.TextEdit txtLuongThucTe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblMucLuong;
        private DevExpress.XtraEditors.TextEdit txtMucLuong;
        private DevExpress.XtraEditors.SimpleButton btnLuu;




    }
}