﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using KP_RES.Class;

namespace KP_RES 
{
    public partial class Frm_NhanVien : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;
        private Byte[] sByteDataImage;
        private UngVien sUngVien = new UngVien();

        public Frm_NhanVien()
        {
            InitializeComponent();
        }

        private void Frm_NhanVien_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteNhanVien " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            if (txtFileCV.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFileCV.Text);

            Byte[] sImage = clsQLCongTy.ReturnByteImage(picHinhAnh);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertNhanVien";

                sParameter = new SqlParameter[20];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGAYSINH", (DateTime)dtpNgaySinh.EditValue);
                sParameter[2] = new SqlParameter("@GIOITINH", (Object)cboGioiTinh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@DIACHI", (Object)txtDiaChi.Text);
                sParameter[4] = new SqlParameter("@DIENTHOAI", (Object)txtDienThoai.Text);
                sParameter[5] = new SqlParameter("@EMAIL", (Object)txtEmail.Text);
                sParameter[6] = new SqlParameter("@HOCVAN", (Object)txtHocVan.Text);
                sParameter[7] = new SqlParameter("@NGOAINGU", (Object)txtNgoaiNgu.Text);
                sParameter[8] = new SqlParameter("@TINHOC", (Object)txtTinHoc.Text);
                sParameter[9] = new SqlParameter("@KINHNGHIEM", (Object)txtKinhNghiem.Text);
                sParameter[10] = new SqlParameter("@KYNANG", (Object)txtKyNang.Text);
                sParameter[11] = new SqlParameter("@PHONGBAN", (Object)cboPhongBan.EditValue.ToString());
                sParameter[12] = new SqlParameter("@CHUCVU", (Object)cboChucVu.EditValue.ToString());
                sParameter[13] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[14] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[14].Value = sImage;
                sParameter[15] = new SqlParameter("@TEN_FILECV", (Object)sNameFile);
                sParameter[16] = new SqlParameter("@FILECV", SqlDbType.VarBinary, int.MaxValue);
                sParameter[16].Value = sByteDataFile;
                sParameter[17] = new SqlParameter("@UNGVIEN", (Object)sUngVien.pMa);
                sParameter[18] = new SqlParameter("@GHICHU", (Object)"");
                sParameter[19] = new SqlParameter("@SUDUNG", (Object)(chkSuDung.Checked == true ? "1" : "0"));
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateNhanVien";

                sParameter = new SqlParameter[21];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGAYSINH", (DateTime)dtpNgaySinh.EditValue);
                sParameter[2] = new SqlParameter("@GIOITINH", (Object)cboGioiTinh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@DIACHI", (Object)txtDiaChi.Text);
                sParameter[4] = new SqlParameter("@DIENTHOAI", (Object)txtDienThoai.Text);
                sParameter[5] = new SqlParameter("@EMAIL", (Object)txtEmail.Text);
                sParameter[6] = new SqlParameter("@HOCVAN", (Object)txtHocVan.Text);
                sParameter[7] = new SqlParameter("@NGOAINGU", (Object)txtNgoaiNgu.Text);
                sParameter[8] = new SqlParameter("@TINHOC", (Object)txtTinHoc.Text);
                sParameter[9] = new SqlParameter("@KINHNGHIEM", (Object)txtKinhNghiem.Text);
                sParameter[10] = new SqlParameter("@KYNANG", (Object)txtKyNang.Text);
                sParameter[11] = new SqlParameter("@PHONGBAN", (Object)cboPhongBan.EditValue.ToString());
                sParameter[12] = new SqlParameter("@CHUCVU", (Object)cboChucVu.EditValue.ToString());
                sParameter[13] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[14] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[14].Value = sImage;
                sParameter[15] = new SqlParameter("@TEN_FILECV", (Object)sNameFile);
                sParameter[16] = new SqlParameter("@FILECV", SqlDbType.VarBinary, int.MaxValue);
                sParameter[16].Value = sByteDataFile;
                sParameter[17] = new SqlParameter("@MA", (Object)sMa);
                sParameter[18] = new SqlParameter("@UNGVIEN", (Object)sUngVien.pMa);
                sParameter[19] = new SqlParameter("@GHICHU", (Object)"");
                sParameter[20] = new SqlParameter("@SUDUNG", (Object)(chkSuDung.Checked == true ? "1" : "0"));
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean == true)
            {
                Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_UpdateDialogUngVien " + clsMain.SQLString(sUngVien.pMa.ToString()));
                if (bRunSQL)
                {
                    LoaddataGridView();
                    btnThem.Focus();
                }
                else
                    clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtDiaChi.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIACHI").ToString();
            txtDienThoai.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            txtEmail.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            txtHocVan.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HOCVAN").ToString();
            txtNgoaiNgu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGOAINGU").ToString();
            txtTinHoc.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TINHOC").ToString();
            txtKinhNghiem.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KINHNGHIEM").ToString();
            txtKyNang.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KYNANG").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILECV").ToString();
            Boolean sGioiTinh = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOITINH_ID").ToString());
            cboGioiTinh.EditValue = sGioiTinh == true ? "1" : "0";
            cboPhongBan.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "PHONGBAN_ID").ToString());
            cboChucVu.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CHUCVU_ID").ToString());
            cboTrangThai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TRANGTHAI_ID").ToString());
            dtpNgaySinh.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString());
            chkSuDung.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILECV").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILECV"));
            sByteDataImage = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IMAGE"));
            picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectNhanVien");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");
            myDT.Rows.Add("1", "Nam");
            myDT.Rows.Add("0", "Nữ");
            clsQLCongTy.LoadCombo(myDT, cboGioiTinh);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoPhongBan");
            clsQLCongTy.LoadCombo(myDT, cboPhongBan);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoChucVu");
            clsQLCongTy.LoadCombo(myDT, cboChucVu);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoSatus " + clsMain.SQLString("03"));
            clsQLCongTy.LoadCombo(myDT, cboTrangThai);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("6051601");
            btnSua.Enabled = clsUserManagement.AllowEdit("6051601");
            btnXoa.Enabled = clsUserManagement.AllowDelete("6051601");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTen.Properties.ReadOnly = sBoolean;
            txtDiaChi.Properties.ReadOnly = sBoolean;
            txtDienThoai.Properties.ReadOnly = sBoolean;
            txtEmail.Properties.ReadOnly = sBoolean;
            txtHocVan.Properties.ReadOnly = sBoolean;
            txtNgoaiNgu.Properties.ReadOnly = sBoolean;
            txtTinHoc.Properties.ReadOnly = sBoolean;
            txtKinhNghiem.Properties.ReadOnly = sBoolean;
            txtKyNang.Properties.ReadOnly = sBoolean;
            cboGioiTinh.Properties.ReadOnly = sBoolean;
            cboPhongBan.Properties.ReadOnly = sBoolean;
            cboChucVu.Properties.ReadOnly = sBoolean;
            cboTrangThai.Properties.ReadOnly = sBoolean;
            chkSuDung.Properties.ReadOnly = sBoolean;
            dtpNgaySinh.Properties.ReadOnly = sBoolean;
            btnFileCV.Enabled = !sBoolean;
            picHinhAnh.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            sByteDataImage = new Byte[1];
            txtTen.Text = "";
            txtDiaChi.Text = "";
            txtDienThoai.Text = "";
            txtEmail.Text = "";
            txtHocVan.Text = "";
            txtNgoaiNgu.Text = "";
            txtTinHoc.Text = "";
            txtKinhNghiem.Text = "";
            txtKyNang.Text = "";
            txtTenFile.Text = "";
            txtFileCV.Text = "";
            cboGioiTinh.EditValue = cboGioiTinh.Properties.GetDataSourceValue(cboGioiTinh.Properties.ValueMember, 0);
            cboPhongBan.EditValue = cboPhongBan.Properties.GetDataSourceValue(cboPhongBan.Properties.ValueMember, 0);
            cboChucVu.EditValue = cboChucVu.Properties.GetDataSourceValue(cboChucVu.Properties.ValueMember, 0);
            cboTrangThai.EditValue = cboTrangThai.Properties.GetDataSourceValue(cboTrangThai.Properties.ValueMember, 0);
            chkSuDung.Checked = true;
            dtpNgaySinh.EditValue = clsMain.GetServerDate();
            picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            sUngVien = new UngVien();
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (cboPhongBan.EditValue == "" || cboPhongBan.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblPhongBan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPhongBan.Focus();
                return false;
            }
            if (cboChucVu.EditValue == "" || cboChucVu.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblChucVu.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboChucVu.Focus();
                return false;
            }
            if (cboTrangThai.EditValue == "" || cboTrangThai.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblTrangThai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTrangThai.Focus();
                return false;
            }
            if (txtFileCV.Text != "")
            {
                if (!File.Exists(txtFileCV.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFileCV.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFileCV_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFileCV.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        private void picHinhAnh_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.InitialDirectory = Application.StartupPath + "\\Images";
            sOpenFile.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif";
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Title = "Chọn ảnh";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                picHinhAnh.ImageLocation = sOpenFile.FileName;
                picHinhAnh.BackgroundImage = null;
            }
            else
                picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            
        }

        private void btnUngVien_Click(object sender, EventArgs e)
        {
            Frm_DialogUngVien Frm = new Frm_DialogUngVien();
            Frm.ShowDialog();
            sUngVien = Frm.pUngVien;
            LoadUngVien();
            Frm.Dispose();
        }

        private void LoadUngVien()
        {
            txtTen.Text = sUngVien.pTen;
            txtDiaChi.Text = sUngVien.pDiaChi;
            txtDienThoai.Text = sUngVien.pDienThoai;
            txtEmail.Text = sUngVien.pEmail;
            txtHocVan.Text = sUngVien.pHocVan;
            txtNgoaiNgu.Text = sUngVien.pNgoaiNgu;
            txtTinHoc.EditValue = sUngVien.pTinHoc;
            txtKinhNghiem.EditValue = sUngVien.pKinhNghiem;
            txtKyNang.EditValue = sUngVien.pKyNang;
            txtTenFile.Text = sUngVien.pTenFileCV;
            cboGioiTinh.EditValue = sUngVien.pGioiTinh;
            dtpNgaySinh.EditValue = sUngVien.pNgaySinh;
            sNameFile = sUngVien.pTenFileCV;
            sByteDataFile = sUngVien.pFileCV;
            sByteDataImage = sUngVien.pImage;
            picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
        }
    }
}