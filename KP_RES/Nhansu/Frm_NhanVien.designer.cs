﻿namespace KP_RES 
{
    partial class Frm_NhanVien 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.chkSuDung = new DevExpress.XtraEditors.CheckEdit();
            this.btnUngVien = new DevExpress.XtraEditors.SimpleButton();
            this.cboPhongBan = new DevExpress.XtraEditors.LookUpEdit();
            this.cboGioiTinh = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.picHinhAnh = new System.Windows.Forms.PictureBox();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cboChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.txtKyNang = new DevExpress.XtraEditors.TextEdit();
            this.lblChucVu = new DevExpress.XtraEditors.LabelControl();
            this.lblKyNang = new DevExpress.XtraEditors.LabelControl();
            this.txtKinhNghiem = new DevExpress.XtraEditors.TextEdit();
            this.lblPhongBan = new DevExpress.XtraEditors.LabelControl();
            this.lblKinhNghiem = new DevExpress.XtraEditors.LabelControl();
            this.lblNgoaiNgu = new DevExpress.XtraEditors.LabelControl();
            this.txtNgoaiNgu = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.lblEmail = new DevExpress.XtraEditors.LabelControl();
            this.txtHocVan = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtTinHoc = new DevExpress.XtraEditors.TextEdit();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.txtFileCV = new DevExpress.XtraEditors.TextEdit();
            this.btnFileCV = new DevExpress.XtraEditors.SimpleButton();
            this.lblFileCV = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.lblTinHoc = new DevExpress.XtraEditors.LabelControl();
            this.cboTrangThai = new DevExpress.XtraEditors.LookUpEdit();
            this.lblHocVan = new DevExpress.XtraEditors.LabelControl();
            this.lblDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.lblGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.lblDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.txtDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.lblTrangThai = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GIOITINH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONGBAN_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHUCVU_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOCVAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGOAINGU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TINHOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KINHNGHIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KYNANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONGBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHUCVU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TUYENDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPhongBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHinhAnh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyNang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKinhNghiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgoaiNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHocVan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinHoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileCV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaySinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 271);
            this.panelControl1.TabIndex = 0;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.chkSuDung);
            this.pal_ban.Controls.Add(this.btnUngVien);
            this.pal_ban.Controls.Add(this.cboPhongBan);
            this.pal_ban.Controls.Add(this.cboGioiTinh);
            this.pal_ban.Controls.Add(this.panelControl8);
            this.pal_ban.Controls.Add(this.labelControl6);
            this.pal_ban.Controls.Add(this.cboChucVu);
            this.pal_ban.Controls.Add(this.txtKyNang);
            this.pal_ban.Controls.Add(this.lblChucVu);
            this.pal_ban.Controls.Add(this.lblKyNang);
            this.pal_ban.Controls.Add(this.txtKinhNghiem);
            this.pal_ban.Controls.Add(this.lblPhongBan);
            this.pal_ban.Controls.Add(this.lblKinhNghiem);
            this.pal_ban.Controls.Add(this.lblNgoaiNgu);
            this.pal_ban.Controls.Add(this.txtNgoaiNgu);
            this.pal_ban.Controls.Add(this.txtEmail);
            this.pal_ban.Controls.Add(this.lblEmail);
            this.pal_ban.Controls.Add(this.txtHocVan);
            this.pal_ban.Controls.Add(this.txtDiaChi);
            this.pal_ban.Controls.Add(this.txtTinHoc);
            this.pal_ban.Controls.Add(this.txtTenFile);
            this.pal_ban.Controls.Add(this.lblTenFile);
            this.pal_ban.Controls.Add(this.btnDownLoadFile);
            this.pal_ban.Controls.Add(this.txtFileCV);
            this.pal_ban.Controls.Add(this.btnFileCV);
            this.pal_ban.Controls.Add(this.lblFileCV);
            this.pal_ban.Controls.Add(this.dtpNgaySinh);
            this.pal_ban.Controls.Add(this.txtTen);
            this.pal_ban.Controls.Add(this.lblTinHoc);
            this.pal_ban.Controls.Add(this.cboTrangThai);
            this.pal_ban.Controls.Add(this.lblHocVan);
            this.pal_ban.Controls.Add(this.lblDiaChi);
            this.pal_ban.Controls.Add(this.lblGioiTinh);
            this.pal_ban.Controls.Add(this.lblDienThoai);
            this.pal_ban.Controls.Add(this.txtDienThoai);
            this.pal_ban.Controls.Add(this.lblTen);
            this.pal_ban.Controls.Add(this.btnLuu);
            this.pal_ban.Controls.Add(this.btnSua);
            this.pal_ban.Controls.Add(this.btnXoa);
            this.pal_ban.Controls.Add(this.btnThem);
            this.pal_ban.Controls.Add(this.lblNgaySinh);
            this.pal_ban.Controls.Add(this.lblTrangThai);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(1254, 267);
            this.pal_ban.TabIndex = 0;
            // 
            // chkSuDung
            // 
            this.chkSuDung.EditValue = true;
            this.chkSuDung.EnterMoveNextControl = true;
            this.chkSuDung.Location = new System.Drawing.Point(97, 197);
            this.chkSuDung.Name = "chkSuDung";
            this.chkSuDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDung.Properties.Appearance.Options.UseFont = true;
            this.chkSuDung.Properties.Caption = "Sử dụng";
            this.chkSuDung.Size = new System.Drawing.Size(250, 24);
            this.chkSuDung.TabIndex = 35;
            // 
            // btnUngVien
            // 
            this.btnUngVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUngVien.Appearance.Options.UseFont = true;
            this.btnUngVien.Image = global::KP_RES.Properties.Resources.bill_26;
            this.btnUngVien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnUngVien.Location = new System.Drawing.Point(275, 228);
            this.btnUngVien.Margin = new System.Windows.Forms.Padding(4);
            this.btnUngVien.Name = "btnUngVien";
            this.btnUngVien.Size = new System.Drawing.Size(121, 35);
            this.btnUngVien.TabIndex = 40;
            this.btnUngVien.Text = "Ứng viên";
            this.btnUngVien.Click += new System.EventHandler(this.btnUngVien_Click);
            // 
            // cboPhongBan
            // 
            this.cboPhongBan.EnterMoveNextControl = true;
            this.cboPhongBan.Location = new System.Drawing.Point(760, 101);
            this.cboPhongBan.Name = "cboPhongBan";
            this.cboPhongBan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhongBan.Properties.Appearance.Options.UseFont = true;
            this.cboPhongBan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhongBan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPhongBan.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboPhongBan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPhongBan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboPhongBan.Properties.DisplayMember = "TEN";
            this.cboPhongBan.Properties.DropDownItemHeight = 40;
            this.cboPhongBan.Properties.NullText = "";
            this.cboPhongBan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboPhongBan.Properties.ShowHeader = false;
            this.cboPhongBan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboPhongBan.Properties.ValueMember = "MA";
            this.cboPhongBan.Size = new System.Drawing.Size(248, 26);
            this.cboPhongBan.TabIndex = 25;
            // 
            // cboGioiTinh
            // 
            this.cboGioiTinh.EnterMoveNextControl = true;
            this.cboGioiTinh.Location = new System.Drawing.Point(760, 5);
            this.cboGioiTinh.Name = "cboGioiTinh";
            this.cboGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.cboGioiTinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioiTinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboGioiTinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboGioiTinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboGioiTinh.Properties.DisplayMember = "TEN";
            this.cboGioiTinh.Properties.DropDownItemHeight = 40;
            this.cboGioiTinh.Properties.NullText = "";
            this.cboGioiTinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboGioiTinh.Properties.ShowHeader = false;
            this.cboGioiTinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboGioiTinh.Properties.ValueMember = "MA";
            this.cboGioiTinh.Size = new System.Drawing.Size(248, 26);
            this.cboGioiTinh.TabIndex = 5;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.picHinhAnh);
            this.panelControl8.Location = new System.Drawing.Point(1024, 33);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(167, 158);
            this.panelControl8.TabIndex = 13;
            // 
            // picHinhAnh
            // 
            this.picHinhAnh.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picHinhAnh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picHinhAnh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picHinhAnh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHinhAnh.Location = new System.Drawing.Point(2, 2);
            this.picHinhAnh.Name = "picHinhAnh";
            this.picHinhAnh.Size = new System.Drawing.Size(163, 154);
            this.picHinhAnh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHinhAnh.TabIndex = 23;
            this.picHinhAnh.TabStop = false;
            this.picHinhAnh.Click += new System.EventHandler(this.picHinhAnh_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(1024, 8);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(64, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Hình ảnh";
            // 
            // cboChucVu
            // 
            this.cboChucVu.EnterMoveNextControl = true;
            this.cboChucVu.Location = new System.Drawing.Point(760, 133);
            this.cboChucVu.Name = "cboChucVu";
            this.cboChucVu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChucVu.Properties.Appearance.Options.UseFont = true;
            this.cboChucVu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChucVu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboChucVu.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboChucVu.Properties.DisplayMember = "TEN";
            this.cboChucVu.Properties.DropDownItemHeight = 40;
            this.cboChucVu.Properties.NullText = "";
            this.cboChucVu.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboChucVu.Properties.ShowHeader = false;
            this.cboChucVu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboChucVu.Properties.ValueMember = "MA";
            this.cboChucVu.Size = new System.Drawing.Size(248, 26);
            this.cboChucVu.TabIndex = 27;
            // 
            // txtKyNang
            // 
            this.txtKyNang.EnterMoveNextControl = true;
            this.txtKyNang.Location = new System.Drawing.Point(99, 133);
            this.txtKyNang.Name = "txtKyNang";
            this.txtKyNang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyNang.Properties.Appearance.Options.UseFont = true;
            this.txtKyNang.Size = new System.Drawing.Size(578, 26);
            this.txtKyNang.TabIndex = 23;
            // 
            // lblChucVu
            // 
            this.lblChucVu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChucVu.Location = new System.Drawing.Point(681, 136);
            this.lblChucVu.Margin = new System.Windows.Forms.Padding(4);
            this.lblChucVu.Name = "lblChucVu";
            this.lblChucVu.Size = new System.Drawing.Size(58, 19);
            this.lblChucVu.TabIndex = 26;
            this.lblChucVu.Text = "Chức vụ";
            // 
            // lblKyNang
            // 
            this.lblKyNang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKyNang.Location = new System.Drawing.Point(6, 136);
            this.lblKyNang.Margin = new System.Windows.Forms.Padding(4);
            this.lblKyNang.Name = "lblKyNang";
            this.lblKyNang.Size = new System.Drawing.Size(57, 19);
            this.lblKyNang.TabIndex = 22;
            this.lblKyNang.Text = "Kỹ năng";
            // 
            // txtKinhNghiem
            // 
            this.txtKinhNghiem.EnterMoveNextControl = true;
            this.txtKinhNghiem.Location = new System.Drawing.Point(99, 101);
            this.txtKinhNghiem.Name = "txtKinhNghiem";
            this.txtKinhNghiem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKinhNghiem.Properties.Appearance.Options.UseFont = true;
            this.txtKinhNghiem.Size = new System.Drawing.Size(578, 26);
            this.txtKinhNghiem.TabIndex = 21;
            // 
            // lblPhongBan
            // 
            this.lblPhongBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhongBan.Location = new System.Drawing.Point(681, 104);
            this.lblPhongBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhongBan.Name = "lblPhongBan";
            this.lblPhongBan.Size = new System.Drawing.Size(76, 19);
            this.lblPhongBan.TabIndex = 24;
            this.lblPhongBan.Text = "Phòng ban";
            // 
            // lblKinhNghiem
            // 
            this.lblKinhNghiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKinhNghiem.Location = new System.Drawing.Point(6, 104);
            this.lblKinhNghiem.Margin = new System.Windows.Forms.Padding(4);
            this.lblKinhNghiem.Name = "lblKinhNghiem";
            this.lblKinhNghiem.Size = new System.Drawing.Size(89, 19);
            this.lblKinhNghiem.TabIndex = 20;
            this.lblKinhNghiem.Text = "Kinh nghiệm";
            // 
            // lblNgoaiNgu
            // 
            this.lblNgoaiNgu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgoaiNgu.Location = new System.Drawing.Point(351, 72);
            this.lblNgoaiNgu.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgoaiNgu.Name = "lblNgoaiNgu";
            this.lblNgoaiNgu.Size = new System.Drawing.Size(74, 19);
            this.lblNgoaiNgu.TabIndex = 16;
            this.lblNgoaiNgu.Text = "Ngoại ngữ";
            // 
            // txtNgoaiNgu
            // 
            this.txtNgoaiNgu.EnterMoveNextControl = true;
            this.txtNgoaiNgu.Location = new System.Drawing.Point(429, 69);
            this.txtNgoaiNgu.Name = "txtNgoaiNgu";
            this.txtNgoaiNgu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgoaiNgu.Properties.Appearance.Options.UseFont = true;
            this.txtNgoaiNgu.Size = new System.Drawing.Size(248, 26);
            this.txtNgoaiNgu.TabIndex = 17;
            // 
            // txtEmail
            // 
            this.txtEmail.EnterMoveNextControl = true;
            this.txtEmail.Location = new System.Drawing.Point(760, 37);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(248, 26);
            this.txtEmail.TabIndex = 11;
            // 
            // lblEmail
            // 
            this.lblEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(681, 40);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(39, 19);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email";
            // 
            // txtHocVan
            // 
            this.txtHocVan.EnterMoveNextControl = true;
            this.txtHocVan.Location = new System.Drawing.Point(99, 69);
            this.txtHocVan.Name = "txtHocVan";
            this.txtHocVan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHocVan.Properties.Appearance.Options.UseFont = true;
            this.txtHocVan.Size = new System.Drawing.Size(248, 26);
            this.txtHocVan.TabIndex = 15;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.EnterMoveNextControl = true;
            this.txtDiaChi.Location = new System.Drawing.Point(99, 37);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Properties.Appearance.Options.UseFont = true;
            this.txtDiaChi.Size = new System.Drawing.Size(248, 26);
            this.txtDiaChi.TabIndex = 7;
            // 
            // txtTinHoc
            // 
            this.txtTinHoc.EnterMoveNextControl = true;
            this.txtTinHoc.Location = new System.Drawing.Point(760, 69);
            this.txtTinHoc.Name = "txtTinHoc";
            this.txtTinHoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinHoc.Properties.Appearance.Options.UseFont = true;
            this.txtTinHoc.Size = new System.Drawing.Size(248, 26);
            this.txtTinHoc.TabIndex = 19;
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(760, 165);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 34;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(681, 168);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 33;
            this.lblTenFile.Text = "Tên File";
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDownLoadFile.Location = new System.Drawing.Point(587, 228);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(87, 35);
            this.btnDownLoadFile.TabIndex = 41;
            this.btnDownLoadFile.Text = "Tải File";
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // txtFileCV
            // 
            this.txtFileCV.EnterMoveNextControl = true;
            this.txtFileCV.Location = new System.Drawing.Point(429, 165);
            this.txtFileCV.Name = "txtFileCV";
            this.txtFileCV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileCV.Properties.Appearance.Options.UseFont = true;
            this.txtFileCV.Properties.ReadOnly = true;
            this.txtFileCV.Size = new System.Drawing.Size(185, 26);
            this.txtFileCV.TabIndex = 31;
            // 
            // btnFileCV
            // 
            this.btnFileCV.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileCV.Appearance.Options.UseFont = true;
            this.btnFileCV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFileCV.Location = new System.Drawing.Point(621, 166);
            this.btnFileCV.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileCV.Name = "btnFileCV";
            this.btnFileCV.Size = new System.Drawing.Size(56, 25);
            this.btnFileCV.TabIndex = 32;
            this.btnFileCV.Text = "....";
            this.btnFileCV.Click += new System.EventHandler(this.btnFileCV_Click);
            // 
            // lblFileCV
            // 
            this.lblFileCV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileCV.Location = new System.Drawing.Point(351, 168);
            this.lblFileCV.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileCV.Name = "lblFileCV";
            this.lblFileCV.Size = new System.Drawing.Size(49, 19);
            this.lblFileCV.TabIndex = 30;
            this.lblFileCV.Text = "File CV";
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.EditValue = null;
            this.dtpNgaySinh.EnterMoveNextControl = true;
            this.dtpNgaySinh.Location = new System.Drawing.Point(429, 5);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgaySinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaySinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgaySinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgaySinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgaySinh.Size = new System.Drawing.Size(248, 26);
            this.dtpNgaySinh.TabIndex = 3;
            // 
            // txtTen
            // 
            this.txtTen.EnterMoveNextControl = true;
            this.txtTen.Location = new System.Drawing.Point(99, 5);
            this.txtTen.Name = "txtTen";
            this.txtTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Properties.Appearance.Options.UseFont = true;
            this.txtTen.Size = new System.Drawing.Size(248, 26);
            this.txtTen.TabIndex = 1;
            // 
            // lblTinHoc
            // 
            this.lblTinHoc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinHoc.Location = new System.Drawing.Point(681, 72);
            this.lblTinHoc.Margin = new System.Windows.Forms.Padding(4);
            this.lblTinHoc.Name = "lblTinHoc";
            this.lblTinHoc.Size = new System.Drawing.Size(53, 19);
            this.lblTinHoc.TabIndex = 18;
            this.lblTinHoc.Text = "Tin học";
            // 
            // cboTrangThai
            // 
            this.cboTrangThai.EnterMoveNextControl = true;
            this.cboTrangThai.Location = new System.Drawing.Point(99, 165);
            this.cboTrangThai.Name = "cboTrangThai";
            this.cboTrangThai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThai.Properties.Appearance.Options.UseFont = true;
            this.cboTrangThai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTrangThai.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboTrangThai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTrangThai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboTrangThai.Properties.DisplayMember = "TEN";
            this.cboTrangThai.Properties.DropDownItemHeight = 40;
            this.cboTrangThai.Properties.NullText = "";
            this.cboTrangThai.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTrangThai.Properties.ShowHeader = false;
            this.cboTrangThai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTrangThai.Properties.ValueMember = "MA";
            this.cboTrangThai.Size = new System.Drawing.Size(248, 26);
            this.cboTrangThai.TabIndex = 29;
            // 
            // lblHocVan
            // 
            this.lblHocVan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHocVan.Location = new System.Drawing.Point(6, 72);
            this.lblHocVan.Margin = new System.Windows.Forms.Padding(4);
            this.lblHocVan.Name = "lblHocVan";
            this.lblHocVan.Size = new System.Drawing.Size(57, 19);
            this.lblHocVan.TabIndex = 14;
            this.lblHocVan.Text = "Học vấn";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi.Location = new System.Drawing.Point(6, 40);
            this.lblDiaChi.Margin = new System.Windows.Forms.Padding(4);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(48, 19);
            this.lblDiaChi.TabIndex = 6;
            this.lblDiaChi.Text = "Địa chỉ";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioiTinh.Location = new System.Drawing.Point(681, 8);
            this.lblGioiTinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(60, 19);
            this.lblGioiTinh.TabIndex = 4;
            this.lblGioiTinh.Text = "Giới tính";
            // 
            // lblDienThoai
            // 
            this.lblDienThoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDienThoai.Location = new System.Drawing.Point(351, 40);
            this.lblDienThoai.Margin = new System.Windows.Forms.Padding(4);
            this.lblDienThoai.Name = "lblDienThoai";
            this.lblDienThoai.Size = new System.Drawing.Size(72, 19);
            this.lblDienThoai.TabIndex = 8;
            this.lblDienThoai.Text = "Điện thoại";
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.EnterMoveNextControl = true;
            this.txtDienThoai.Location = new System.Drawing.Point(429, 37);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.Properties.Appearance.Options.UseFont = true;
            this.txtDienThoai.Size = new System.Drawing.Size(248, 26);
            this.txtDienThoai.TabIndex = 9;
            // 
            // lblTen
            // 
            this.lblTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Location = new System.Drawing.Point(6, 8);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(27, 19);
            this.lblTen.TabIndex = 0;
            this.lblTen.Text = "Tên";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(99, 228);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 36;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(404, 228);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 38;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(492, 228);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 39;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(187, 228);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 37;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh.Location = new System.Drawing.Point(351, 8);
            this.lblNgaySinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(70, 19);
            this.lblNgaySinh.TabIndex = 2;
            this.lblNgaySinh.Text = "Ngày sinh";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.Location = new System.Drawing.Point(6, 168);
            this.lblTrangThai.Margin = new System.Windows.Forms.Padding(4);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(73, 19);
            this.lblTrangThai.TabIndex = 28;
            this.lblTrangThai.Text = "Trạng thái";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 271);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 380);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GIOITINH_ID,
            this.PHONGBAN_ID,
            this.CHUCVU_ID,
            this.TRANGTHAI_ID,
            this.FILECV,
            this.STT,
            this.MA,
            this.TEN,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.DIENTHOAI,
            this.EMAIL,
            this.HOCVAN,
            this.NGOAINGU,
            this.TINHOC,
            this.KINHNGHIEM,
            this.KYNANG,
            this.PHONGBAN,
            this.CHUCVU,
            this.TRANGTHAI,
            this.TUYENDUNG,
            this.TEN_FILECV,
            this.gridColumn1,
            this.SUDUNG});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // GIOITINH_ID
            // 
            this.GIOITINH_ID.FieldName = "GIOITINH_ID";
            this.GIOITINH_ID.Name = "GIOITINH_ID";
            this.GIOITINH_ID.OptionsColumn.AllowEdit = false;
            this.GIOITINH_ID.OptionsColumn.AllowFocus = false;
            this.GIOITINH_ID.OptionsColumn.FixedWidth = true;
            // 
            // PHONGBAN_ID
            // 
            this.PHONGBAN_ID.FieldName = "PHONGBAN_ID";
            this.PHONGBAN_ID.Name = "PHONGBAN_ID";
            this.PHONGBAN_ID.OptionsColumn.AllowEdit = false;
            this.PHONGBAN_ID.OptionsColumn.AllowFocus = false;
            this.PHONGBAN_ID.OptionsColumn.FixedWidth = true;
            // 
            // CHUCVU_ID
            // 
            this.CHUCVU_ID.FieldName = "CHUCVU_ID";
            this.CHUCVU_ID.Name = "CHUCVU_ID";
            this.CHUCVU_ID.OptionsColumn.AllowEdit = false;
            this.CHUCVU_ID.OptionsColumn.AllowFocus = false;
            this.CHUCVU_ID.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.FixedWidth = true;
            // 
            // FILECV
            // 
            this.FILECV.FieldName = "FILECV";
            this.FILECV.Name = "FILECV";
            this.FILECV.OptionsColumn.AllowEdit = false;
            this.FILECV.OptionsColumn.AllowFocus = false;
            this.FILECV.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Họ và tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 250;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 3;
            this.NGAYSINH.Width = 100;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceCell.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 4;
            this.GIOITINH.Width = 85;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DIACHI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 5;
            this.DIACHI.Width = 200;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 6;
            this.DIENTHOAI.Width = 100;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceCell.Options.UseTextOptions = true;
            this.EMAIL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 7;
            this.EMAIL.Width = 150;
            // 
            // HOCVAN
            // 
            this.HOCVAN.AppearanceCell.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HOCVAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HOCVAN.AppearanceHeader.Options.UseFont = true;
            this.HOCVAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOCVAN.Caption = "Trình độ học vấn";
            this.HOCVAN.FieldName = "HOCVAN";
            this.HOCVAN.Name = "HOCVAN";
            this.HOCVAN.OptionsColumn.AllowEdit = false;
            this.HOCVAN.OptionsColumn.AllowFocus = false;
            this.HOCVAN.OptionsColumn.FixedWidth = true;
            this.HOCVAN.Visible = true;
            this.HOCVAN.VisibleIndex = 8;
            this.HOCVAN.Width = 200;
            // 
            // NGOAINGU
            // 
            this.NGOAINGU.AppearanceCell.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGOAINGU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGOAINGU.AppearanceHeader.Options.UseFont = true;
            this.NGOAINGU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGOAINGU.Caption = "Ngoại ngữ";
            this.NGOAINGU.FieldName = "NGOAINGU";
            this.NGOAINGU.Name = "NGOAINGU";
            this.NGOAINGU.OptionsColumn.AllowEdit = false;
            this.NGOAINGU.OptionsColumn.AllowFocus = false;
            this.NGOAINGU.OptionsColumn.FixedWidth = true;
            this.NGOAINGU.Visible = true;
            this.NGOAINGU.VisibleIndex = 9;
            this.NGOAINGU.Width = 200;
            // 
            // TINHOC
            // 
            this.TINHOC.AppearanceCell.Options.UseTextOptions = true;
            this.TINHOC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TINHOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TINHOC.AppearanceHeader.Options.UseFont = true;
            this.TINHOC.AppearanceHeader.Options.UseTextOptions = true;
            this.TINHOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TINHOC.Caption = "Tin học";
            this.TINHOC.FieldName = "TINHOC";
            this.TINHOC.Name = "TINHOC";
            this.TINHOC.OptionsColumn.AllowEdit = false;
            this.TINHOC.OptionsColumn.AllowFocus = false;
            this.TINHOC.OptionsColumn.FixedWidth = true;
            this.TINHOC.Visible = true;
            this.TINHOC.VisibleIndex = 10;
            this.TINHOC.Width = 150;
            // 
            // KINHNGHIEM
            // 
            this.KINHNGHIEM.AppearanceCell.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KINHNGHIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KINHNGHIEM.AppearanceHeader.Options.UseFont = true;
            this.KINHNGHIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KINHNGHIEM.Caption = "Kinh nghiệm";
            this.KINHNGHIEM.FieldName = "KINHNGHIEM";
            this.KINHNGHIEM.Name = "KINHNGHIEM";
            this.KINHNGHIEM.OptionsColumn.AllowEdit = false;
            this.KINHNGHIEM.OptionsColumn.AllowFocus = false;
            this.KINHNGHIEM.OptionsColumn.FixedWidth = true;
            this.KINHNGHIEM.Visible = true;
            this.KINHNGHIEM.VisibleIndex = 11;
            this.KINHNGHIEM.Width = 350;
            // 
            // KYNANG
            // 
            this.KYNANG.AppearanceCell.Options.UseTextOptions = true;
            this.KYNANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KYNANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KYNANG.AppearanceHeader.Options.UseFont = true;
            this.KYNANG.AppearanceHeader.Options.UseTextOptions = true;
            this.KYNANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KYNANG.Caption = "Kỹ năng";
            this.KYNANG.FieldName = "KYNANG";
            this.KYNANG.Name = "KYNANG";
            this.KYNANG.OptionsColumn.AllowEdit = false;
            this.KYNANG.OptionsColumn.AllowFocus = false;
            this.KYNANG.OptionsColumn.FixedWidth = true;
            this.KYNANG.Visible = true;
            this.KYNANG.VisibleIndex = 12;
            this.KYNANG.Width = 350;
            // 
            // PHONGBAN
            // 
            this.PHONGBAN.AppearanceCell.Options.UseTextOptions = true;
            this.PHONGBAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHONGBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PHONGBAN.AppearanceHeader.Options.UseFont = true;
            this.PHONGBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.PHONGBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHONGBAN.Caption = "Phòng ban";
            this.PHONGBAN.FieldName = "PHONGBAN";
            this.PHONGBAN.Name = "PHONGBAN";
            this.PHONGBAN.OptionsColumn.AllowEdit = false;
            this.PHONGBAN.OptionsColumn.AllowFocus = false;
            this.PHONGBAN.OptionsColumn.FixedWidth = true;
            this.PHONGBAN.Visible = true;
            this.PHONGBAN.VisibleIndex = 13;
            this.PHONGBAN.Width = 150;
            // 
            // CHUCVU
            // 
            this.CHUCVU.AppearanceCell.Options.UseTextOptions = true;
            this.CHUCVU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHUCVU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHUCVU.AppearanceHeader.Options.UseFont = true;
            this.CHUCVU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHUCVU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHUCVU.Caption = "Chức vụ";
            this.CHUCVU.FieldName = "CHUCVU";
            this.CHUCVU.Name = "CHUCVU";
            this.CHUCVU.OptionsColumn.AllowEdit = false;
            this.CHUCVU.OptionsColumn.AllowFocus = false;
            this.CHUCVU.OptionsColumn.FixedWidth = true;
            this.CHUCVU.Visible = true;
            this.CHUCVU.VisibleIndex = 14;
            this.CHUCVU.Width = 150;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 15;
            this.TRANGTHAI.Width = 150;
            // 
            // TUYENDUNG
            // 
            this.TUYENDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TUYENDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TUYENDUNG.AppearanceHeader.Options.UseFont = true;
            this.TUYENDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TUYENDUNG.Caption = "Đợt tuyển dụng";
            this.TUYENDUNG.FieldName = "TUYENDUNG";
            this.TUYENDUNG.Name = "TUYENDUNG";
            this.TUYENDUNG.OptionsColumn.AllowEdit = false;
            this.TUYENDUNG.OptionsColumn.AllowFocus = false;
            this.TUYENDUNG.OptionsColumn.FixedWidth = true;
            this.TUYENDUNG.Visible = true;
            this.TUYENDUNG.VisibleIndex = 16;
            this.TUYENDUNG.Width = 250;
            // 
            // TEN_FILECV
            // 
            this.TEN_FILECV.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILECV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILECV.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILECV.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILECV.Caption = "File CV";
            this.TEN_FILECV.FieldName = "TEN_FILECV";
            this.TEN_FILECV.Name = "TEN_FILECV";
            this.TEN_FILECV.OptionsColumn.AllowEdit = false;
            this.TEN_FILECV.OptionsColumn.AllowFocus = false;
            this.TEN_FILECV.OptionsColumn.FixedWidth = true;
            this.TEN_FILECV.Visible = true;
            this.TEN_FILECV.VisibleIndex = 17;
            this.TEN_FILECV.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 18;
            this.gridColumn1.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 271);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 380);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 63);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 254);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 122);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 10);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 132);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 61);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 193);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 61);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 61);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 61);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 61);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 317);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 61);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 61);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // SUDUNG
            // 
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            // 
            // Frm_NhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_NhanVien";
            this.Text = "Nhân viên";
            this.Load += new System.EventHandler(this.Frm_NhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPhongBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHinhAnh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyNang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKinhNghiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgoaiNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHocVan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinHoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileCV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaySinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn HOCVAN;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn NGOAINGU;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn TINHOC;
        private DevExpress.XtraGrid.Columns.GridColumn FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILECV;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl lblEmail;
        private DevExpress.XtraEditors.TextEdit txtHocVan;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.TextEdit txtTinHoc;
        private DevExpress.XtraEditors.TextEdit txtTenFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.TextEdit txtFileCV;
        private DevExpress.XtraEditors.SimpleButton btnFileCV;
        private DevExpress.XtraEditors.LabelControl lblFileCV;
        private DevExpress.XtraEditors.DateEdit dtpNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.LabelControl lblTinHoc;
        private DevExpress.XtraEditors.LookUpEdit cboTrangThai;
        private DevExpress.XtraEditors.LabelControl lblHocVan;
        private DevExpress.XtraEditors.LabelControl lblDiaChi;
        private DevExpress.XtraEditors.LabelControl lblGioiTinh;
        private DevExpress.XtraEditors.LabelControl lblDienThoai;
        private DevExpress.XtraEditors.TextEdit txtDienThoai;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl lblNgaySinh;
        private DevExpress.XtraEditors.LabelControl lblTrangThai;
        private DevExpress.XtraEditors.LookUpEdit cboChucVu;
        private DevExpress.XtraEditors.TextEdit txtKyNang;
        private DevExpress.XtraEditors.LabelControl lblChucVu;
        private DevExpress.XtraEditors.LabelControl lblKyNang;
        private DevExpress.XtraEditors.TextEdit txtKinhNghiem;
        private DevExpress.XtraEditors.LabelControl lblPhongBan;
        private DevExpress.XtraEditors.LabelControl lblKinhNghiem;
        private DevExpress.XtraEditors.LabelControl lblNgoaiNgu;
        private DevExpress.XtraEditors.TextEdit txtNgoaiNgu;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox picHinhAnh;
        private DevExpress.XtraEditors.LookUpEdit cboGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn CHUCVU_ID;
        private DevExpress.XtraGrid.Columns.GridColumn KINHNGHIEM;
        private DevExpress.XtraGrid.Columns.GridColumn PHONGBAN;
        private DevExpress.XtraGrid.Columns.GridColumn KYNANG;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TUYENDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.LookUpEdit cboPhongBan;
        private DevExpress.XtraEditors.SimpleButton btnUngVien;
        private DevExpress.XtraGrid.Columns.GridColumn PHONGBAN_ID;
        private DevExpress.XtraGrid.Columns.GridColumn CHUCVU;
        private DevExpress.XtraEditors.CheckEdit chkSuDung;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;




    }
}