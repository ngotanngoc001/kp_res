﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;

namespace KP_RES
{
    public partial class Frm_BaoCao_LuongThang : DevExpress.XtraEditors.XtraForm
    {
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_BaoCao_LuongThang()
        {
            InitializeComponent();
            LoadCombo();
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 1; i < 13; i++)
                myDT.Rows.Add(i.ToString(), "Tháng " + i);

            clsQLCongTy.LoadCombo(myDT, cboThang);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 2016; i < 2027; i++)
                myDT.Rows.Add(i.ToString(), "Năm " + i);

            clsQLCongTy.LoadCombo(myDT, cboNam);

            myDT = new DataTable();
            myDT.Columns.Add("MA", typeof(Int32));
            myDT.Columns.Add("TEN");
            myDT.Rows.Add("0", "Tất cả");
            myDT.Merge(clsQLCongTy.ReturnDataTableSPs("SP_ComBoNhanVien"));
            clsQLCongTy.LoadCombo(myDT, cboData);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] sParameter = new SqlParameter[3];
                sParameter[0] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
                sParameter[1] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());
                sParameter[2] = new SqlParameter("@MA", (Object)cboData.EditValue.ToString());

                DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectBCLuongThang", sParameter);
                gridControl1.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataTable myDT = (DataTable)gridControl1.DataSource;

                    String Description = cboThang.Text + " " + cboNam.Text;

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;

                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 79;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    clsQLCongTy.MessageInformation("Không có dữ liệu", "Thông báo");
            }
            catch{
            }
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            Boolean sBoolean = (columns.Contains(columnName)) ? true : false;

            return sBoolean;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void Frm_BaoCao_LuongThang_Load(object sender, EventArgs e)
        {
            cboThang.EditValue = cboThang.Properties.GetDataSourceValue(cboThang.Properties.ValueMember, DateTime.Now.Month - 1);
            cboNam.EditValue = cboNam.Properties.GetDataSourceValue(cboNam.Properties.ValueMember, DateTime.Now.Year - 2016);
            cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
        }

       

        

        
       
       


    }
}