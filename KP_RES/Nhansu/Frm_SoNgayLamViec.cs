﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_SoNgayLamViec : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";

        public Frm_SoNgayLamViec()
        {
            InitializeComponent();
        }

        private void Frm_SoNgayLamViec_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("dvt", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboThang.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteSoNgayLamViec " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            cboThang.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertSoNgayLamViec";

                sParameter = new SqlParameter[3];
                sParameter[0] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
                sParameter[1] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());
                sParameter[2] = new SqlParameter("@SONGAY", (Object)txtSoNgay.Text);
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateSoNgayLamViec";

                sParameter = new SqlParameter[4];
                sParameter[0] = new SqlParameter("@THANG", (Object)cboThang.EditValue.ToString());
                sParameter[1] = new SqlParameter("@NAM", (Object)cboNam.EditValue.ToString());
                sParameter[2] = new SqlParameter("@SONGAY", (Object)txtSoNgay.Text);
                sParameter[3] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;

            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtSoNgay.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SONGAY").ToString();
            cboThang.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THANG").ToString();
            cboNam.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAM").ToString();
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectSoNgayLamViec");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 1; i < 13; i++)
            {
                myDT.Rows.Add(i.ToString(), "Tháng " + i);
            }
            clsQLCongTy.LoadCombo(myDT, cboThang);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");

            for (int i = 2016; i < 2027; i++)
            {
                myDT.Rows.Add(i.ToString(), "Năm " + i);
            }
            clsQLCongTy.LoadCombo(myDT, cboNam);

        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("7051603");
            btnSua.Enabled = clsUserManagement.AllowEdit("7051603");
            btnXoa.Enabled = clsUserManagement.AllowDelete("7051603");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            cboThang.Properties.ReadOnly = sBoolean;
            cboNam.Properties.ReadOnly = sBoolean;
            txtSoNgay.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            txtSoNgay.Text = "";
            cboThang.EditValue = cboThang.Properties.GetDataSourceValue(cboThang.Properties.ValueMember, DateTime.Now.Month - 1);
            cboNam.EditValue = cboNam.Properties.GetDataSourceValue(cboNam.Properties.ValueMember, DateTime.Now.Year - 2016);
        }

        private Boolean CheckInput()
        {
            if (txtSoNgay.Text == "" || txtSoNgay.Text == "0")
            {
                XtraMessageBox.Show("Nhập " + lblSoNgay.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoNgay.Focus();
                return false;
            }
            return true;
        }

        
    }
}