﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_HopDongLaoDong : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_HopDongLaoDong()
        {
            InitializeComponent();
        }

        private void Frm_HopDongLaoDong_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteHopDongLaoDong " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertHopDongLaoDong";

                sParameter = new SqlParameter[20];
                sParameter[0] = new SqlParameter("@TENHOPDONG", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGUOILAODONG", (Object)cboNguoiLaoDong.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NGUOISDLAODONG", (Object)cboNguoiSDLaoDong.EditValue.ToString());
                sParameter[3] = new SqlParameter("@THOIHAN", (Object)txtThoiHan.Text);
                sParameter[4] = new SqlParameter("@NGAYKY", (DateTime)dtpNgayKy.EditValue);
                sParameter[5] = new SqlParameter("@NGAYHIEULUC", (DateTime)dtpNgayHL.EditValue);
                sParameter[6] = new SqlParameter("@NGAYHETHIEULUC", (DateTime)dtpNgayHetHL.EditValue);
                sParameter[7] = new SqlParameter("@MUCLUONG", (Object)txtMucLuong.Text);
                sParameter[8] = new SqlParameter("@LUONGTHUCTE", (Object)txtLuongThucTe.Text);
                sParameter[9] = new SqlParameter("@HINHTHUC", (Object)txtHinhThuc.Text);
                sParameter[10] = new SqlParameter("@NGAYTRALUONG", (Object)txtNgayTraLuong.Text);
                sParameter[11] = new SqlParameter("@PHUCAP", (Object)cboPhuCap.EditValue.ToString());
                sParameter[12] = new SqlParameter("@BH_XAHOI", (Object)(chkBHXaHoi.Checked == true ? "1" : "0"));
                sParameter[13] = new SqlParameter("@BH_YTE", (Object)(chkBHYTe.Checked == true ? "1" : "0"));
                sParameter[14] = new SqlParameter("@BH_THATNGHIEP", (Object)(chkBHThatNghiep.Checked == true ? "1" : "0"));
                sParameter[15] = new SqlParameter("@THOIGIAN_LV", (Object)txtThoiGianLV.Text);
                sParameter[16] = new SqlParameter("@DENBU_HD", (Object)txtDenBuHD.Text);
                sParameter[17] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[18] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[19] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[19].Value = sByteDataFile;
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateHopDongLaoDong";

                sParameter = new SqlParameter[21];
                sParameter[0] = new SqlParameter("@TENHOPDONG", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@NGUOILAODONG", (Object)cboNguoiLaoDong.EditValue.ToString());
                sParameter[2] = new SqlParameter("@NGUOISDLAODONG", (Object)cboNguoiSDLaoDong.EditValue.ToString());
                sParameter[3] = new SqlParameter("@THOIHAN", (Object)txtThoiHan.Text);
                sParameter[4] = new SqlParameter("@NGAYKY", (DateTime)dtpNgayKy.EditValue);
                sParameter[5] = new SqlParameter("@NGAYHIEULUC", (DateTime)dtpNgayHL.EditValue);
                sParameter[6] = new SqlParameter("@NGAYHETHIEULUC", (DateTime)dtpNgayHetHL.EditValue);
                sParameter[7] = new SqlParameter("@MUCLUONG", (Object)txtMucLuong.Text);
                sParameter[8] = new SqlParameter("@LUONGTHUCTE", (Object)txtLuongThucTe.Text);
                sParameter[9] = new SqlParameter("@HINHTHUC", (Object)txtHinhThuc.Text);
                sParameter[10] = new SqlParameter("@NGAYTRALUONG", (Object)txtNgayTraLuong.Text);
                sParameter[11] = new SqlParameter("@PHUCAP", (Object)cboPhuCap.EditValue.ToString());
                sParameter[12] = new SqlParameter("@BH_XAHOI", (Object)(chkBHXaHoi.Checked == true ? "1" : "0"));
                sParameter[13] = new SqlParameter("@BH_YTE", (Object)(chkBHYTe.Checked == true ? "1" : "0"));
                sParameter[14] = new SqlParameter("@BH_THATNGHIEP", (Object)(chkBHThatNghiep.Checked == true ? "1" : "0"));
                sParameter[15] = new SqlParameter("@THOIGIAN_LV", (Object)txtThoiGianLV.Text);
                sParameter[16] = new SqlParameter("@DENBU_HD", (Object)txtDenBuHD.Text);
                sParameter[17] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[18] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[19] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[19].Value = sByteDataFile;
                sParameter[20] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean == true)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TENHOPDONG").ToString();
            txtThoiHan.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THOIHAN").ToString();
            txtMucLuong.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MUCLUONG").ToString();
            txtLuongThucTe.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LUONGTHUCTE").ToString();
            txtHinhThuc.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HINHTHUC").ToString();
            txtNgayTraLuong.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYTRALUONG").ToString();
            txtGhiChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            txtThoiGianLV.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THOIGIAN_LV").ToString();
            chkBHXaHoi.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "BH_XAHOI").ToString());
            chkBHYTe.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "BH_YTE").ToString());
            chkBHThatNghiep.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "BH_THATNGHIEP").ToString());
            txtDenBuHD.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DENBU_HD").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            cboNguoiLaoDong.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOILAODONG_ID").ToString());
            cboNguoiSDLaoDong.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOISDLAODONG_ID").ToString());
            cboPhuCap.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "PHUCAP_ID").ToString());
            dtpNgayKy.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYKY").ToString());
            dtpNgayHL.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYHIEULUC").ToString());
            dtpNgayHetHL.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYHETHIEULUC").ToString());
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectHopDongLaoDong");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoNhanVien");
            clsQLCongTy.LoadCombo(myDT, cboNguoiLaoDong);
            clsQLCongTy.LoadCombo(myDT, cboNguoiSDLaoDong);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoPhuCap");
            clsQLCongTy.LoadCombo(myDT, cboPhuCap);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("7051601");
            btnSua.Enabled = clsUserManagement.AllowEdit("7051601");
            btnXoa.Enabled = clsUserManagement.AllowDelete("7051601");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTen.Properties.ReadOnly = sBoolean;
            txtThoiHan.Properties.ReadOnly = sBoolean;
            txtMucLuong.Properties.ReadOnly = sBoolean;
            txtLuongThucTe.Properties.ReadOnly = sBoolean;
            txtHinhThuc.Properties.ReadOnly = sBoolean;
            txtNgayTraLuong.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            txtThoiGianLV.Properties.ReadOnly = sBoolean;
            chkBHXaHoi.Properties.ReadOnly = sBoolean;
            chkBHYTe.Properties.ReadOnly = sBoolean;
            chkBHThatNghiep.Properties.ReadOnly = sBoolean;
            txtDenBuHD.Properties.ReadOnly = sBoolean;
            cboNguoiLaoDong.Properties.ReadOnly = sBoolean;
            cboNguoiSDLaoDong.Properties.ReadOnly = sBoolean;
            cboPhuCap.Properties.ReadOnly = sBoolean;
            dtpNgayKy.Properties.ReadOnly = sBoolean;
            dtpNgayHL.Properties.ReadOnly = sBoolean;
            dtpNgayHetHL.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtTen.Text = "";
            txtThoiHan.Text = "";
            txtMucLuong.Text = "";
            txtLuongThucTe.Text = "";
            txtHinhThuc.Text = "";
            txtNgayTraLuong.Text = "";
            txtGhiChu.Text = "";
            txtThoiGianLV.Text = "";
            chkBHXaHoi.Checked = false;
            chkBHYTe.Checked = false;
            chkBHThatNghiep.Checked = false;
            txtDenBuHD.Text = "";
            txtTenFile.Text = "";
            txtFile.Text = "";
            cboNguoiLaoDong.EditValue = cboNguoiLaoDong.Properties.GetDataSourceValue(cboNguoiLaoDong.Properties.ValueMember, 0);
            cboNguoiSDLaoDong.EditValue = cboNguoiSDLaoDong.Properties.GetDataSourceValue(cboNguoiSDLaoDong.Properties.ValueMember, 0);
            cboPhuCap.EditValue = cboPhuCap.Properties.GetDataSourceValue(cboPhuCap.Properties.ValueMember, 0);
            dtpNgayKy.EditValue = clsMain.GetServerDate();
            dtpNgayHL.EditValue = clsMain.GetServerDate();
            dtpNgayHetHL.EditValue = clsMain.GetServerDate();
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (cboNguoiLaoDong.EditValue == "" || cboNguoiLaoDong.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNguoiLaoDong.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNguoiLaoDong.Focus();
                return false;
            }
            if (cboNguoiSDLaoDong.EditValue == "" || cboNguoiSDLaoDong.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNguoiSDLaoDong.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNguoiSDLaoDong.Focus();
                return false;
            }
            if (cboPhuCap.EditValue == "" || cboPhuCap.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblPhuCap.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPhuCap.Focus();
                return false;
            }
            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        private void btnThemNhieuHD_Click(object sender, EventArgs e)
        {
            Frm_ThemNhieuHopDong frm = new Frm_ThemNhieuHopDong();
            frm.ShowDialog();
            LoaddataGridView();
        }

    }
}