﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_KhenThuongKyLuat : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_KhenThuongKyLuat()
        {
            InitializeComponent();
        }

        private void Frm_KhenThuongKyLuat_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteKhenThuongKyLuat " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            String sNhanVien = clsQLCongTy.CheckedComboBox(cboNhanVien);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertKhenThuongKyLuat";

                sParameter = new SqlParameter[14];
                sParameter[0] = new SqlParameter("@LOAI", (Object)cboLoai.EditValue.ToString());
                sParameter[1] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[2] = new SqlParameter("@LYDO", (Object)txtLyDo.Text);
                sParameter[3] = new SqlParameter("@NHANVIEN", (Object)sNhanVien);
                sParameter[4] = new SqlParameter("@HINHTHUC", (Object)txtHinhThuc.Text);
                sParameter[5] = new SqlParameter("@GIATRI", (Object)txtGiaTri.Text);
                sParameter[6] = new SqlParameter("@NGAYTHUCHIEN", (DateTime)dtpNgayThucHien.EditValue);
                sParameter[7] = new SqlParameter("@SOQUYETDINH", (Object)txtSoQD.Text);
                sParameter[8] = new SqlParameter("@NGAYBANHANH", (DateTime)dtpNgayThucHien.EditValue);
                sParameter[9] = new SqlParameter("@NGUOIBANHANH", (Object)cboNguoiBanHanh.EditValue.ToString());
                sParameter[10] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[11] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[12] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[13] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[13].Value = sByteDataFile;
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateKhenThuongKyLuat";

                sParameter = new SqlParameter[15];
                sParameter[0] = new SqlParameter("@LOAI", (Object)cboLoai.EditValue.ToString());
                sParameter[1] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[2] = new SqlParameter("@LYDO", (Object)txtLyDo.Text);
                sParameter[3] = new SqlParameter("@NHANVIEN", (Object)sNhanVien);
                sParameter[4] = new SqlParameter("@HINHTHUC", (Object)txtHinhThuc.Text);
                sParameter[5] = new SqlParameter("@GIATRI", (Object)txtGiaTri.Text);
                sParameter[6] = new SqlParameter("@NGAYTHUCHIEN", (DateTime)dtpNgayThucHien.EditValue);
                sParameter[7] = new SqlParameter("@SOQUYETDINH", (Object)txtSoQD.Text);
                sParameter[8] = new SqlParameter("@NGAYBANHANH", (DateTime)dtpNgayThucHien.EditValue);
                sParameter[9] = new SqlParameter("@NGUOIBANHANH", (Object)cboNguoiBanHanh.EditValue.ToString());
                sParameter[10] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
                sParameter[11] = new SqlParameter("@GHICHU", (Object)txtGhiChu.Text);
                sParameter[12] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
                sParameter[13] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[13].Value = sByteDataFile;
                sParameter[14] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtLyDo.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LYDO").ToString();
            txtHinhThuc.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HINHTHUC").ToString();
            txtGiaTri.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI").ToString();
            txtSoQD.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOQUYETDINH").ToString();
            txtGhiChu.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            cboLoai.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAI_ID").ToString();
            cboNguoiBanHanh.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOIBANHANH_ID").ToString());
            cboTrangThai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TRANGTHAI_ID").ToString());
            dtpNgayThucHien.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYTHUCHIEN").ToString());
            dtpNgayBanHanh.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYBANHANH").ToString());
            clsQLCongTy.CheckedComboBox(cboNhanVien, gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NHANVIEN_ID").ToString().Split(','));
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectKhenThuongKyLuat");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoNhanVien");
            clsQLCongTy.LoadCombo(myDT, cboNhanVien);
            clsQLCongTy.LoadCombo(myDT, cboNguoiBanHanh);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");
            myDT.Rows.Add(1.ToString(), "Khen thưởng");
            myDT.Rows.Add(2.ToString(), "Kỷ luật");
            clsQLCongTy.LoadCombo(myDT, cboLoai);

            myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("EXEC SP_ComBoSatus " + clsMain.SQLString("04"));
            clsQLCongTy.LoadCombo(myDT, cboTrangThai);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("7051602");
            btnSua.Enabled = clsUserManagement.AllowEdit("7051602");
            btnXoa.Enabled = clsUserManagement.AllowDelete("7051602");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTen.Properties.ReadOnly = sBoolean;
            txtLyDo.Properties.ReadOnly = sBoolean;
            txtHinhThuc.Properties.ReadOnly = sBoolean;
            txtGiaTri.Properties.ReadOnly = sBoolean;
            txtSoQD.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            cboLoai.Properties.ReadOnly = sBoolean;
            cboNhanVien.Properties.ReadOnly = sBoolean;
            cboNguoiBanHanh.Properties.ReadOnly = sBoolean;
            cboTrangThai.Properties.ReadOnly = sBoolean;
            dtpNgayThucHien.Properties.ReadOnly = sBoolean;
            dtpNgayBanHanh.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtTen.Text = "";
            txtLyDo.Text = "";
            txtHinhThuc.Text = "";
            txtGiaTri.Text = "";
            txtSoQD.Text = "";
            txtGhiChu.Text = "";
            txtTenFile.Text = "";
            txtFile.Text = "";
            dtpNgayThucHien.EditValue = clsMain.GetServerDate();
            dtpNgayBanHanh.EditValue = clsMain.GetServerDate();
            cboLoai.EditValue = cboLoai.Properties.GetDataSourceValue(cboLoai.Properties.ValueMember, 0);
            cboNguoiBanHanh.EditValue = cboNguoiBanHanh.Properties.GetDataSourceValue(cboNguoiBanHanh.Properties.ValueMember, 0);
            cboTrangThai.EditValue = cboTrangThai.Properties.GetDataSourceValue(cboTrangThai.Properties.ValueMember, 0);
            cboNhanVien.CheckAll();
            foreach (CheckedListBoxItem sItem in cboNhanVien.Properties.Items)
                sItem.CheckState = CheckState.Unchecked;
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (cboLoai.EditValue == "" || cboLoai.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblLoai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboLoai.Focus();
                return false;
            }
            if (cboNhanVien.EditValue == "" || cboNhanVien.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNhanVien.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNhanVien.Focus();
                return false;
            }
            if (cboNguoiBanHanh.EditValue == "" || cboNguoiBanHanh.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblNguoiBanHanh.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNguoiBanHanh.Focus();
                return false;
            }
            if (cboTrangThai.EditValue == "" || cboTrangThai.EditValue == null)
            {
                XtraMessageBox.Show("Chọn " + lblTrangThai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTrangThai.Focus();
                return false;
            }
            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        

    }
}