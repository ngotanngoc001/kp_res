﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using KP_RES.Class;

namespace KP_RES 
{
    public partial class Frm_DialogUngVien : DevExpress.XtraEditors.XtraForm
    {
        public UngVien pUngVien = new UngVien();

        public Frm_DialogUngVien()
        {
            InitializeComponent();
        }

        private void Frm_DialogUngVien_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void LoaddataGridView()
        {
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectDialogUngVien");
            gridControl2.DataSource = myDT;
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            pUngVien = new UngVien();

            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            pUngVien.pMa = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString());
            pUngVien.pTen = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            pUngVien.pDiaChi = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIACHI").ToString();
            pUngVien.pDienThoai = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            pUngVien.pEmail = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            pUngVien.pHocVan = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HOCVAN").ToString();
            pUngVien.pNgoaiNgu = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGOAINGU").ToString();
            pUngVien.pTinHoc = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TINHOC").ToString();
            pUngVien.pKinhNghiem = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KINHNGHIEM").ToString();
            pUngVien.pKyNang = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "KYNANG").ToString();
            pUngVien.pTenFileCV = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILECV").ToString();
            Boolean sGioiTinh = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOITINH_ID").ToString());
            pUngVien.pGioiTinh = sGioiTinh == true ? "1" : "0";
            pUngVien.pNgaySinh = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString());
            pUngVien.pFileCV = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILECV"));
            pUngVien.pImage = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IMAGE"));

        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            pUngVien = new UngVien();
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}