﻿namespace KP_RES 
{
    partial class Frm_DialogUngVien 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TUYENDUNG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOCVAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGOAINGU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TINHOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KINHNGHIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KYNANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MUCLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TUYENDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnDong);
            this.panel1.Controls.Add(this.btnLuu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.panel1.Location = new System.Drawing.Point(0, 537);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(994, 38);
            this.panel1.TabIndex = 59;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(881, 1);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 7;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(763, 1);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 6;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.gridControl2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 12F);
            this.panel3.Location = new System.Drawing.Point(0, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(994, 509);
            this.panel3.TabIndex = 60;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(994, 509);
            this.gridControl2.TabIndex = 2;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILECV,
            this.TRANGTHAI_ID,
            this.GIOITINH_ID,
            this.TUYENDUNG_ID,
            this.STT,
            this.MA,
            this.TEN,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.DIENTHOAI,
            this.EMAIL,
            this.HOCVAN,
            this.NGOAINGU,
            this.TINHOC,
            this.KINHNGHIEM,
            this.KYNANG,
            this.MUCLUONG,
            this.TRANGTHAI,
            this.TUYENDUNG,
            this.TEN_FILECV,
            this.gridColumn1});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILECV
            // 
            this.FILECV.FieldName = "FILECV";
            this.FILECV.Name = "FILECV";
            this.FILECV.OptionsColumn.AllowEdit = false;
            this.FILECV.OptionsColumn.AllowFocus = false;
            this.FILECV.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.FixedWidth = true;
            // 
            // GIOITINH_ID
            // 
            this.GIOITINH_ID.FieldName = "GIOITINH_ID";
            this.GIOITINH_ID.Name = "GIOITINH_ID";
            this.GIOITINH_ID.OptionsColumn.AllowEdit = false;
            this.GIOITINH_ID.OptionsColumn.AllowFocus = false;
            this.GIOITINH_ID.OptionsColumn.FixedWidth = true;
            // 
            // TUYENDUNG_ID
            // 
            this.TUYENDUNG_ID.FieldName = "TUYENDUNG_ID";
            this.TUYENDUNG_ID.Name = "TUYENDUNG_ID";
            this.TUYENDUNG_ID.OptionsColumn.AllowEdit = false;
            this.TUYENDUNG_ID.OptionsColumn.AllowFocus = false;
            this.TUYENDUNG_ID.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Họ và tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 250;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 3;
            this.NGAYSINH.Width = 100;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceCell.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 4;
            this.GIOITINH.Width = 85;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DIACHI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 5;
            this.DIACHI.Width = 200;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 6;
            this.DIENTHOAI.Width = 100;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceCell.Options.UseTextOptions = true;
            this.EMAIL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 7;
            this.EMAIL.Width = 150;
            // 
            // HOCVAN
            // 
            this.HOCVAN.AppearanceCell.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HOCVAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HOCVAN.AppearanceHeader.Options.UseFont = true;
            this.HOCVAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOCVAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOCVAN.Caption = "Trình độ học vấn";
            this.HOCVAN.FieldName = "HOCVAN";
            this.HOCVAN.Name = "HOCVAN";
            this.HOCVAN.OptionsColumn.AllowEdit = false;
            this.HOCVAN.OptionsColumn.AllowFocus = false;
            this.HOCVAN.OptionsColumn.FixedWidth = true;
            this.HOCVAN.Visible = true;
            this.HOCVAN.VisibleIndex = 8;
            this.HOCVAN.Width = 200;
            // 
            // NGOAINGU
            // 
            this.NGOAINGU.AppearanceCell.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGOAINGU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGOAINGU.AppearanceHeader.Options.UseFont = true;
            this.NGOAINGU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGOAINGU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGOAINGU.Caption = "Ngoại ngữ";
            this.NGOAINGU.FieldName = "NGOAINGU";
            this.NGOAINGU.Name = "NGOAINGU";
            this.NGOAINGU.OptionsColumn.AllowEdit = false;
            this.NGOAINGU.OptionsColumn.AllowFocus = false;
            this.NGOAINGU.OptionsColumn.FixedWidth = true;
            this.NGOAINGU.Visible = true;
            this.NGOAINGU.VisibleIndex = 9;
            this.NGOAINGU.Width = 200;
            // 
            // TINHOC
            // 
            this.TINHOC.AppearanceCell.Options.UseTextOptions = true;
            this.TINHOC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TINHOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TINHOC.AppearanceHeader.Options.UseFont = true;
            this.TINHOC.AppearanceHeader.Options.UseTextOptions = true;
            this.TINHOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TINHOC.Caption = "Tin học";
            this.TINHOC.FieldName = "TINHOC";
            this.TINHOC.Name = "TINHOC";
            this.TINHOC.OptionsColumn.AllowEdit = false;
            this.TINHOC.OptionsColumn.AllowFocus = false;
            this.TINHOC.OptionsColumn.FixedWidth = true;
            this.TINHOC.Visible = true;
            this.TINHOC.VisibleIndex = 10;
            this.TINHOC.Width = 150;
            // 
            // KINHNGHIEM
            // 
            this.KINHNGHIEM.AppearanceCell.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KINHNGHIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KINHNGHIEM.AppearanceHeader.Options.UseFont = true;
            this.KINHNGHIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.KINHNGHIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KINHNGHIEM.Caption = "Kinh nghiệm";
            this.KINHNGHIEM.FieldName = "KINHNGHIEM";
            this.KINHNGHIEM.Name = "KINHNGHIEM";
            this.KINHNGHIEM.OptionsColumn.AllowEdit = false;
            this.KINHNGHIEM.OptionsColumn.AllowFocus = false;
            this.KINHNGHIEM.OptionsColumn.FixedWidth = true;
            this.KINHNGHIEM.Visible = true;
            this.KINHNGHIEM.VisibleIndex = 11;
            this.KINHNGHIEM.Width = 350;
            // 
            // KYNANG
            // 
            this.KYNANG.AppearanceCell.Options.UseTextOptions = true;
            this.KYNANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KYNANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KYNANG.AppearanceHeader.Options.UseFont = true;
            this.KYNANG.AppearanceHeader.Options.UseTextOptions = true;
            this.KYNANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KYNANG.Caption = "Kỹ năng";
            this.KYNANG.FieldName = "KYNANG";
            this.KYNANG.Name = "KYNANG";
            this.KYNANG.OptionsColumn.AllowEdit = false;
            this.KYNANG.OptionsColumn.AllowFocus = false;
            this.KYNANG.OptionsColumn.FixedWidth = true;
            this.KYNANG.Visible = true;
            this.KYNANG.VisibleIndex = 12;
            this.KYNANG.Width = 350;
            // 
            // MUCLUONG
            // 
            this.MUCLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MUCLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MUCLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MUCLUONG.AppearanceHeader.Options.UseFont = true;
            this.MUCLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MUCLUONG.Caption = "Mức lương";
            this.MUCLUONG.DisplayFormat.FormatString = "N0";
            this.MUCLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MUCLUONG.FieldName = "MUCLUONG";
            this.MUCLUONG.Name = "MUCLUONG";
            this.MUCLUONG.OptionsColumn.AllowEdit = false;
            this.MUCLUONG.OptionsColumn.AllowFocus = false;
            this.MUCLUONG.OptionsColumn.FixedWidth = true;
            this.MUCLUONG.Visible = true;
            this.MUCLUONG.VisibleIndex = 13;
            this.MUCLUONG.Width = 100;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 14;
            this.TRANGTHAI.Width = 150;
            // 
            // TUYENDUNG
            // 
            this.TUYENDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TUYENDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TUYENDUNG.AppearanceHeader.Options.UseFont = true;
            this.TUYENDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.TUYENDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TUYENDUNG.Caption = "Đợt tuyển dụng";
            this.TUYENDUNG.FieldName = "TUYENDUNG";
            this.TUYENDUNG.Name = "TUYENDUNG";
            this.TUYENDUNG.OptionsColumn.AllowEdit = false;
            this.TUYENDUNG.OptionsColumn.AllowFocus = false;
            this.TUYENDUNG.OptionsColumn.FixedWidth = true;
            this.TUYENDUNG.Visible = true;
            this.TUYENDUNG.VisibleIndex = 15;
            this.TUYENDUNG.Width = 250;
            // 
            // TEN_FILECV
            // 
            this.TEN_FILECV.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILECV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILECV.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILECV.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILECV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILECV.Caption = "File CV";
            this.TEN_FILECV.FieldName = "TEN_FILECV";
            this.TEN_FILECV.Name = "TEN_FILECV";
            this.TEN_FILECV.OptionsColumn.AllowEdit = false;
            this.TEN_FILECV.OptionsColumn.AllowFocus = false;
            this.TEN_FILECV.OptionsColumn.FixedWidth = true;
            this.TEN_FILECV.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 16;
            this.gridColumn1.Width = 20;
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl4);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(994, 28);
            this.header.TabIndex = 3;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl4.Controls.Add(this.pictureEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(994, 28);
            this.panelControl4.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(990, 24);
            this.pictureEdit1.TabIndex = 0;
            // 
            // Frm_DialogUngVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 575);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_DialogUngVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ứng viên";
            this.Load += new System.EventHandler(this.Frm_DialogUngVien_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn TUYENDUNG_ID;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn HOCVAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGOAINGU;
        private DevExpress.XtraGrid.Columns.GridColumn TINHOC;
        private DevExpress.XtraGrid.Columns.GridColumn KINHNGHIEM;
        private DevExpress.XtraGrid.Columns.GridColumn MUCLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn KYNANG;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TUYENDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILECV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;




    }
}