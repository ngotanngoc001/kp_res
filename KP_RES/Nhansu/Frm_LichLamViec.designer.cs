﻿namespace KP_RES 
{
    partial class Frm_LichLamViec 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblFont = new DevExpress.XtraEditors.LabelControl();
            this.foFontChu = new DevExpress.XtraEditors.FontEdit();
            this.clrMauChu = new DevExpress.XtraEditors.ColorEdit();
            this.lblMauChu = new DevExpress.XtraEditors.LabelControl();
            this.clrMau = new DevExpress.XtraEditors.ColorEdit();
            this.txtDiaDiem = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayKetThuc = new DevExpress.XtraEditors.DateEdit();
            this.lblNoiDung = new DevExpress.XtraEditors.LabelControl();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayBatDau = new DevExpress.XtraEditors.DateEdit();
            this.txtTieuDe = new DevExpress.XtraEditors.TextEdit();
            this.cboTrangThai = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTrangThai = new DevExpress.XtraEditors.LabelControl();
            this.lblNhanVienThucHien = new DevExpress.XtraEditors.LabelControl();
            this.lblDiaDiem = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayKetThuc = new DevExpress.XtraEditors.LabelControl();
            this.lblMau = new DevExpress.XtraEditors.LabelControl();
            this.lblTieuDe = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNgayBatDau = new DevExpress.XtraEditors.LabelControl();
            this.lblDienGiai = new DevExpress.XtraEditors.LabelControl();
            this.txtDienGiai = new DevExpress.XtraEditors.TextEdit();
            this.cboNhanVienThucHien = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.cboNgay = new DevExpress.XtraEditors.LookUpEdit();
            this.cboTrangThaiLich = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnXem = new DevExpress.XtraEditors.SimpleButton();
            this.cboNhanVienLich = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuoi = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnLich = new DevExpress.XtraEditors.SimpleButton();
            this.pnLuoi = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NV_THUCHIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEUDE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_BATDAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_KETTHUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NVTHUCHIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENGIAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.pnLich = new DevExpress.XtraEditors.PanelControl();
            this.lpnLich = new System.Windows.Forms.FlowLayoutPanel();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.foFontChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clrMauChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clrMau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaDiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKetThuc.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKetThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBatDau.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBatDau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTieuDe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienThucHien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThaiLich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienLich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnLuoi)).BeginInit();
            this.pnLuoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLich)).BeginInit();
            this.pnLich.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblFont);
            this.panelControl1.Controls.Add(this.foFontChu);
            this.panelControl1.Controls.Add(this.clrMauChu);
            this.panelControl1.Controls.Add(this.lblMauChu);
            this.panelControl1.Controls.Add(this.clrMau);
            this.panelControl1.Controls.Add(this.txtDiaDiem);
            this.panelControl1.Controls.Add(this.dtpNgayKetThuc);
            this.panelControl1.Controls.Add(this.lblNoiDung);
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.dtpNgayBatDau);
            this.panelControl1.Controls.Add(this.txtTieuDe);
            this.panelControl1.Controls.Add(this.cboTrangThai);
            this.panelControl1.Controls.Add(this.lblTrangThai);
            this.panelControl1.Controls.Add(this.lblNhanVienThucHien);
            this.panelControl1.Controls.Add(this.lblDiaDiem);
            this.panelControl1.Controls.Add(this.lblNgayKetThuc);
            this.panelControl1.Controls.Add(this.lblMau);
            this.panelControl1.Controls.Add(this.lblTieuDe);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lblNgayBatDau);
            this.panelControl1.Controls.Add(this.lblDienGiai);
            this.panelControl1.Controls.Add(this.txtDienGiai);
            this.panelControl1.Controls.Add(this.cboNhanVienThucHien);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 34);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 178);
            this.panelControl1.TabIndex = 0;
            // 
            // lblFont
            // 
            this.lblFont.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFont.Location = new System.Drawing.Point(709, 72);
            this.lblFont.Margin = new System.Windows.Forms.Padding(4);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(62, 19);
            this.lblFont.TabIndex = 16;
            this.lblFont.Text = "Font chữ";
            // 
            // foFontChu
            // 
            this.foFontChu.Location = new System.Drawing.Point(811, 69);
            this.foFontChu.Name = "foFontChu";
            this.foFontChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.foFontChu.Properties.Appearance.Options.UseFont = true;
            this.foFontChu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.foFontChu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.foFontChu.Properties.AutoComplete = false;
            this.foFontChu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.foFontChu.Properties.DropDownRows = 8;
            this.foFontChu.Properties.PopupFormMinSize = new System.Drawing.Size(285, 50);
            this.foFontChu.Properties.PopupFormSize = new System.Drawing.Size(285, 50);
            this.foFontChu.Size = new System.Drawing.Size(248, 26);
            this.foFontChu.TabIndex = 17;
            // 
            // clrMauChu
            // 
            this.clrMauChu.EditValue = System.Drawing.Color.Empty;
            this.clrMauChu.Location = new System.Drawing.Point(442, 69);
            this.clrMauChu.Name = "clrMauChu";
            this.clrMauChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.clrMauChu.Properties.Appearance.Options.UseFont = true;
            this.clrMauChu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clrMauChu.Size = new System.Drawing.Size(248, 26);
            this.clrMauChu.TabIndex = 15;
            // 
            // lblMauChu
            // 
            this.lblMauChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMauChu.Location = new System.Drawing.Point(341, 72);
            this.lblMauChu.Margin = new System.Windows.Forms.Padding(4);
            this.lblMauChu.Name = "lblMauChu";
            this.lblMauChu.Size = new System.Drawing.Size(60, 19);
            this.lblMauChu.TabIndex = 14;
            this.lblMauChu.Text = "Màu chữ";
            // 
            // clrMau
            // 
            this.clrMau.EditValue = System.Drawing.Color.Empty;
            this.clrMau.Location = new System.Drawing.Point(83, 69);
            this.clrMau.Name = "clrMau";
            this.clrMau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.clrMau.Properties.Appearance.Options.UseFont = true;
            this.clrMau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clrMau.Size = new System.Drawing.Size(248, 26);
            this.clrMau.TabIndex = 13;
            // 
            // txtDiaDiem
            // 
            this.txtDiaDiem.EnterMoveNextControl = true;
            this.txtDiaDiem.Location = new System.Drawing.Point(442, 37);
            this.txtDiaDiem.Name = "txtDiaDiem";
            this.txtDiaDiem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiem.Properties.Appearance.Options.UseFont = true;
            this.txtDiaDiem.Size = new System.Drawing.Size(248, 26);
            this.txtDiaDiem.TabIndex = 9;
            // 
            // dtpNgayKetThuc
            // 
            this.dtpNgayKetThuc.EditValue = null;
            this.dtpNgayKetThuc.EnterMoveNextControl = true;
            this.dtpNgayKetThuc.Location = new System.Drawing.Point(811, 5);
            this.dtpNgayKetThuc.Name = "dtpNgayKetThuc";
            this.dtpNgayKetThuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKetThuc.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayKetThuc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKetThuc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayKetThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayKetThuc.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKetThuc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKetThuc.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKetThuc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKetThuc.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayKetThuc.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayKetThuc.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayKetThuc.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayKetThuc.TabIndex = 5;
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiDung.Location = new System.Drawing.Point(6, 40);
            this.lblNoiDung.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(65, 19);
            this.lblNoiDung.TabIndex = 6;
            this.lblNoiDung.Text = "Nội dung";
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(83, 37);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Properties.AutoHeight = false;
            this.txtNoiDung.Size = new System.Drawing.Size(248, 26);
            this.txtNoiDung.TabIndex = 7;
            // 
            // dtpNgayBatDau
            // 
            this.dtpNgayBatDau.EditValue = null;
            this.dtpNgayBatDau.EnterMoveNextControl = true;
            this.dtpNgayBatDau.Location = new System.Drawing.Point(442, 5);
            this.dtpNgayBatDau.Name = "dtpNgayBatDau";
            this.dtpNgayBatDau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBatDau.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayBatDau.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBatDau.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayBatDau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayBatDau.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBatDau.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBatDau.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBatDau.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBatDau.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayBatDau.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayBatDau.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayBatDau.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayBatDau.TabIndex = 3;
            // 
            // txtTieuDe
            // 
            this.txtTieuDe.EnterMoveNextControl = true;
            this.txtTieuDe.Location = new System.Drawing.Point(83, 5);
            this.txtTieuDe.Name = "txtTieuDe";
            this.txtTieuDe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuDe.Properties.Appearance.Options.UseFont = true;
            this.txtTieuDe.Size = new System.Drawing.Size(248, 26);
            this.txtTieuDe.TabIndex = 1;
            // 
            // cboTrangThai
            // 
            this.cboTrangThai.EnterMoveNextControl = true;
            this.cboTrangThai.Location = new System.Drawing.Point(83, 101);
            this.cboTrangThai.Name = "cboTrangThai";
            this.cboTrangThai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThai.Properties.Appearance.Options.UseFont = true;
            this.cboTrangThai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTrangThai.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboTrangThai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTrangThai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboTrangThai.Properties.DisplayMember = "TEN";
            this.cboTrangThai.Properties.DropDownItemHeight = 40;
            this.cboTrangThai.Properties.NullText = "";
            this.cboTrangThai.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTrangThai.Properties.ShowHeader = false;
            this.cboTrangThai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTrangThai.Properties.ValueMember = "MA";
            this.cboTrangThai.Size = new System.Drawing.Size(248, 26);
            this.cboTrangThai.TabIndex = 19;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.Location = new System.Drawing.Point(6, 104);
            this.lblTrangThai.Margin = new System.Windows.Forms.Padding(4);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(73, 19);
            this.lblTrangThai.TabIndex = 18;
            this.lblTrangThai.Text = "Trạng thái";
            // 
            // lblNhanVienThucHien
            // 
            this.lblNhanVienThucHien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhanVienThucHien.Location = new System.Drawing.Point(709, 40);
            this.lblNhanVienThucHien.Margin = new System.Windows.Forms.Padding(4);
            this.lblNhanVienThucHien.Name = "lblNhanVienThucHien";
            this.lblNhanVienThucHien.Size = new System.Drawing.Size(92, 19);
            this.lblNhanVienThucHien.TabIndex = 10;
            this.lblNhanVienThucHien.Text = "NV thực hiện";
            // 
            // lblDiaDiem
            // 
            this.lblDiaDiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiem.Location = new System.Drawing.Point(341, 40);
            this.lblDiaDiem.Margin = new System.Windows.Forms.Padding(4);
            this.lblDiaDiem.Name = "lblDiaDiem";
            this.lblDiaDiem.Size = new System.Drawing.Size(63, 19);
            this.lblDiaDiem.TabIndex = 8;
            this.lblDiaDiem.Text = "Địa điểm";
            // 
            // lblNgayKetThuc
            // 
            this.lblNgayKetThuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKetThuc.Location = new System.Drawing.Point(709, 8);
            this.lblNgayKetThuc.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayKetThuc.Name = "lblNgayKetThuc";
            this.lblNgayKetThuc.Size = new System.Drawing.Size(57, 19);
            this.lblNgayKetThuc.TabIndex = 4;
            this.lblNgayKetThuc.Text = "Kết thúc";
            // 
            // lblMau
            // 
            this.lblMau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMau.Location = new System.Drawing.Point(6, 72);
            this.lblMau.Margin = new System.Windows.Forms.Padding(4);
            this.lblMau.Name = "lblMau";
            this.lblMau.Size = new System.Drawing.Size(60, 19);
            this.lblMau.TabIndex = 12;
            this.lblMau.Text = "Màu nền";
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTieuDe.Location = new System.Drawing.Point(6, 8);
            this.lblTieuDe.Margin = new System.Windows.Forms.Padding(4);
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Size = new System.Drawing.Size(53, 19);
            this.lblTieuDe.TabIndex = 0;
            this.lblTieuDe.Text = "Tiêu đề";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(83, 134);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 22;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(259, 134);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 24;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(347, 134);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 25;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(171, 134);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 23;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNgayBatDau
            // 
            this.lblNgayBatDau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayBatDau.Location = new System.Drawing.Point(341, 8);
            this.lblNgayBatDau.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayBatDau.Name = "lblNgayBatDau";
            this.lblNgayBatDau.Size = new System.Drawing.Size(53, 19);
            this.lblNgayBatDau.TabIndex = 2;
            this.lblNgayBatDau.Text = "Bắt đầu";
            // 
            // lblDienGiai
            // 
            this.lblDienGiai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDienGiai.Location = new System.Drawing.Point(341, 104);
            this.lblDienGiai.Margin = new System.Windows.Forms.Padding(4);
            this.lblDienGiai.Name = "lblDienGiai";
            this.lblDienGiai.Size = new System.Drawing.Size(62, 19);
            this.lblDienGiai.TabIndex = 20;
            this.lblDienGiai.Text = "Diễn giải";
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.EnterMoveNextControl = true;
            this.txtDienGiai.Location = new System.Drawing.Point(442, 101);
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienGiai.Properties.Appearance.Options.UseFont = true;
            this.txtDienGiai.Size = new System.Drawing.Size(617, 26);
            this.txtDienGiai.TabIndex = 21;
            // 
            // cboNhanVienThucHien
            // 
            this.cboNhanVienThucHien.EditValue = "";
            this.cboNhanVienThucHien.Location = new System.Drawing.Point(811, 37);
            this.cboNhanVienThucHien.Name = "cboNhanVienThucHien";
            this.cboNhanVienThucHien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienThucHien.Properties.Appearance.Options.UseFont = true;
            this.cboNhanVienThucHien.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienThucHien.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanVienThucHien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanVienThucHien.Properties.DisplayMember = "TEN";
            this.cboNhanVienThucHien.Properties.DropDownRows = 10;
            this.cboNhanVienThucHien.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanVienThucHien.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhanVienThucHien.Properties.ValueMember = "MA";
            this.cboNhanVienThucHien.Size = new System.Drawing.Size(248, 26);
            this.cboNhanVienThucHien.TabIndex = 11;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.cboNgay);
            this.panelControl6.Controls.Add(this.cboTrangThaiLich);
            this.panelControl6.Controls.Add(this.labelControl1);
            this.panelControl6.Controls.Add(this.btnXem);
            this.panelControl6.Controls.Add(this.cboNhanVienLich);
            this.panelControl6.Controls.Add(this.labelControl3);
            this.panelControl6.Controls.Add(this.labelControl10);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 212);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1258, 79);
            this.panelControl6.TabIndex = 1;
            // 
            // cboNgay
            // 
            this.cboNgay.EnterMoveNextControl = true;
            this.cboNgay.Location = new System.Drawing.Point(83, 6);
            this.cboNgay.Name = "cboNgay";
            this.cboNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNgay.Properties.Appearance.Options.UseFont = true;
            this.cboNgay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNgay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNgay.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNgay.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNgay.Properties.DisplayMember = "TEN";
            this.cboNgay.Properties.DropDownItemHeight = 40;
            this.cboNgay.Properties.NullText = "";
            this.cboNgay.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNgay.Properties.ShowHeader = false;
            this.cboNgay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNgay.Properties.ValueMember = "MA";
            this.cboNgay.Size = new System.Drawing.Size(248, 26);
            this.cboNgay.TabIndex = 1;
            // 
            // cboTrangThaiLich
            // 
            this.cboTrangThaiLich.EnterMoveNextControl = true;
            this.cboTrangThaiLich.Location = new System.Drawing.Point(811, 6);
            this.cboTrangThaiLich.Name = "cboTrangThaiLich";
            this.cboTrangThaiLich.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThaiLich.Properties.Appearance.Options.UseFont = true;
            this.cboTrangThaiLich.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrangThaiLich.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTrangThaiLich.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboTrangThaiLich.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTrangThaiLich.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboTrangThaiLich.Properties.DisplayMember = "TEN";
            this.cboTrangThaiLich.Properties.DropDownItemHeight = 40;
            this.cboTrangThaiLich.Properties.NullText = "";
            this.cboTrangThaiLich.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTrangThaiLich.Properties.ShowHeader = false;
            this.cboTrangThaiLich.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTrangThaiLich.Properties.ValueMember = "MA";
            this.cboTrangThaiLich.Size = new System.Drawing.Size(248, 26);
            this.cboTrangThaiLich.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(709, 9);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 19);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Trạng thái";
            // 
            // btnXem
            // 
            this.btnXem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXem.Appearance.Options.UseFont = true;
            this.btnXem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnXem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXem.Location = new System.Drawing.Point(83, 39);
            this.btnXem.Margin = new System.Windows.Forms.Padding(4);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(87, 35);
            this.btnXem.TabIndex = 6;
            this.btnXem.Text = "&Xem";
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // cboNhanVienLich
            // 
            this.cboNhanVienLich.EnterMoveNextControl = true;
            this.cboNhanVienLich.Location = new System.Drawing.Point(442, 6);
            this.cboNhanVienLich.Name = "cboNhanVienLich";
            this.cboNhanVienLich.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienLich.Properties.Appearance.Options.UseFont = true;
            this.cboNhanVienLich.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienLich.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanVienLich.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhanVienLich.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanVienLich.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNhanVienLich.Properties.DisplayMember = "TEN";
            this.cboNhanVienLich.Properties.DropDownItemHeight = 40;
            this.cboNhanVienLich.Properties.NullText = "";
            this.cboNhanVienLich.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanVienLich.Properties.ShowHeader = false;
            this.cboNhanVienLich.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhanVienLich.Properties.ValueMember = "MA";
            this.cboNhanVienLich.Size = new System.Drawing.Size(248, 26);
            this.cboNhanVienLich.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(341, 9);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Nhân viên";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(6, 9);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(45, 19);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Tháng";
            // 
            // btnLuoi
            // 
            this.btnLuoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuoi.Appearance.Options.UseFont = true;
            this.btnLuoi.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLuoi.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuoi.Location = new System.Drawing.Point(2, 2);
            this.btnLuoi.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuoi.Name = "btnLuoi";
            this.btnLuoi.Size = new System.Drawing.Size(655, 30);
            this.btnLuoi.TabIndex = 23;
            this.btnLuoi.Text = "Lưới";
            this.btnLuoi.Click += new System.EventHandler(this.btnLuoi_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.btnLich);
            this.panelControl5.Controls.Add(this.btnLuoi);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1258, 34);
            this.panelControl5.TabIndex = 6;
            // 
            // btnLich
            // 
            this.btnLich.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLich.Appearance.Options.UseFont = true;
            this.btnLich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLich.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLich.Location = new System.Drawing.Point(657, 2);
            this.btnLich.Margin = new System.Windows.Forms.Padding(4);
            this.btnLich.Name = "btnLich";
            this.btnLich.Size = new System.Drawing.Size(599, 30);
            this.btnLich.TabIndex = 24;
            this.btnLich.Text = "Lịch";
            this.btnLich.Click += new System.EventHandler(this.btnLich_Click);
            // 
            // pnLuoi
            // 
            this.pnLuoi.Controls.Add(this.gridControl2);
            this.pnLuoi.Controls.Add(this.panelControl2);
            this.pnLuoi.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLuoi.Location = new System.Drawing.Point(0, 291);
            this.pnLuoi.Name = "pnLuoi";
            this.pnLuoi.Size = new System.Drawing.Size(163, 360);
            this.pnLuoi.TabIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(120, 356);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NV_THUCHIEN,
            this.TRANGTHAI_ID,
            this.MAU,
            this.STT,
            this.MA,
            this.TIEUDE,
            this.TIME_BATDAU,
            this.TIME_KETTHUC,
            this.NOIDUNG,
            this.DIADIEM,
            this.NVTHUCHIEN,
            this.TRANGTHAI,
            this.DIENGIAI,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // NV_THUCHIEN
            // 
            this.NV_THUCHIEN.FieldName = "NV_THUCHIEN";
            this.NV_THUCHIEN.Name = "NV_THUCHIEN";
            this.NV_THUCHIEN.OptionsColumn.AllowEdit = false;
            this.NV_THUCHIEN.OptionsColumn.AllowFocus = false;
            this.NV_THUCHIEN.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowMove = false;
            // 
            // MAU
            // 
            this.MAU.AppearanceCell.Options.UseTextOptions = true;
            this.MAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAU.AppearanceHeader.Options.UseFont = true;
            this.MAU.AppearanceHeader.Options.UseTextOptions = true;
            this.MAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAU.Caption = "Màu hiển thị";
            this.MAU.FieldName = "MAU";
            this.MAU.Name = "MAU";
            this.MAU.OptionsColumn.AllowEdit = false;
            this.MAU.OptionsColumn.AllowFocus = false;
            this.MAU.OptionsColumn.FixedWidth = true;
            this.MAU.Width = 120;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TIEUDE
            // 
            this.TIEUDE.AppearanceCell.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEUDE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEUDE.AppearanceHeader.Options.UseFont = true;
            this.TIEUDE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEUDE.Caption = "Tiêu đề";
            this.TIEUDE.FieldName = "TIEUDE";
            this.TIEUDE.Name = "TIEUDE";
            this.TIEUDE.OptionsColumn.AllowEdit = false;
            this.TIEUDE.OptionsColumn.AllowFocus = false;
            this.TIEUDE.OptionsColumn.FixedWidth = true;
            this.TIEUDE.Visible = true;
            this.TIEUDE.VisibleIndex = 2;
            this.TIEUDE.Width = 200;
            // 
            // TIME_BATDAU
            // 
            this.TIME_BATDAU.AppearanceCell.Options.UseTextOptions = true;
            this.TIME_BATDAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_BATDAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_BATDAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIME_BATDAU.AppearanceHeader.Options.UseFont = true;
            this.TIME_BATDAU.AppearanceHeader.Options.UseTextOptions = true;
            this.TIME_BATDAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_BATDAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_BATDAU.Caption = "Ngày bắt đầu";
            this.TIME_BATDAU.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.TIME_BATDAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TIME_BATDAU.FieldName = "TIME_BATDAU";
            this.TIME_BATDAU.Name = "TIME_BATDAU";
            this.TIME_BATDAU.OptionsColumn.AllowEdit = false;
            this.TIME_BATDAU.OptionsColumn.AllowFocus = false;
            this.TIME_BATDAU.OptionsColumn.FixedWidth = true;
            this.TIME_BATDAU.Visible = true;
            this.TIME_BATDAU.VisibleIndex = 3;
            this.TIME_BATDAU.Width = 120;
            // 
            // TIME_KETTHUC
            // 
            this.TIME_KETTHUC.AppearanceCell.Options.UseTextOptions = true;
            this.TIME_KETTHUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_KETTHUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_KETTHUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIME_KETTHUC.AppearanceHeader.Options.UseFont = true;
            this.TIME_KETTHUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TIME_KETTHUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_KETTHUC.Caption = "Ngày kết thúc";
            this.TIME_KETTHUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.TIME_KETTHUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TIME_KETTHUC.FieldName = "TIME_KETTHUC";
            this.TIME_KETTHUC.Name = "TIME_KETTHUC";
            this.TIME_KETTHUC.OptionsColumn.AllowEdit = false;
            this.TIME_KETTHUC.OptionsColumn.AllowFocus = false;
            this.TIME_KETTHUC.OptionsColumn.FixedWidth = true;
            this.TIME_KETTHUC.Visible = true;
            this.TIME_KETTHUC.VisibleIndex = 4;
            this.TIME_KETTHUC.Width = 120;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 5;
            this.NOIDUNG.Width = 350;
            // 
            // DIADIEM
            // 
            this.DIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.DIADIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIADIEM.AppearanceHeader.Options.UseFont = true;
            this.DIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.DIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIADIEM.Caption = "Địa điểm";
            this.DIADIEM.FieldName = "DIADIEM";
            this.DIADIEM.Name = "DIADIEM";
            this.DIADIEM.OptionsColumn.AllowEdit = false;
            this.DIADIEM.OptionsColumn.AllowFocus = false;
            this.DIADIEM.OptionsColumn.FixedWidth = true;
            this.DIADIEM.Visible = true;
            this.DIADIEM.VisibleIndex = 6;
            this.DIADIEM.Width = 200;
            // 
            // NVTHUCHIEN
            // 
            this.NVTHUCHIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NVTHUCHIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NVTHUCHIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NVTHUCHIEN.AppearanceHeader.Options.UseFont = true;
            this.NVTHUCHIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NVTHUCHIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NVTHUCHIEN.Caption = "Nhân viên thực hiện";
            this.NVTHUCHIEN.FieldName = "NVTHUCHIEN";
            this.NVTHUCHIEN.Name = "NVTHUCHIEN";
            this.NVTHUCHIEN.OptionsColumn.AllowEdit = false;
            this.NVTHUCHIEN.OptionsColumn.AllowFocus = false;
            this.NVTHUCHIEN.OptionsColumn.FixedWidth = true;
            this.NVTHUCHIEN.Visible = true;
            this.NVTHUCHIEN.VisibleIndex = 7;
            this.NVTHUCHIEN.Width = 300;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 8;
            this.TRANGTHAI.Width = 150;
            // 
            // DIENGIAI
            // 
            this.DIENGIAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENGIAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENGIAI.AppearanceHeader.Options.UseFont = true;
            this.DIENGIAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENGIAI.Caption = "Diễn giải";
            this.DIENGIAI.FieldName = "DIENGIAI";
            this.DIENGIAI.Name = "DIENGIAI";
            this.DIENGIAI.OptionsColumn.AllowEdit = false;
            this.DIENGIAI.OptionsColumn.AllowFocus = false;
            this.DIENGIAI.OptionsColumn.FixedWidth = true;
            this.DIENGIAI.Visible = true;
            this.DIENGIAI.VisibleIndex = 9;
            this.DIENGIAI.Width = 300;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 10;
            this.FILL.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(122, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 356);
            this.panelControl2.TabIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 192);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 32);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 112);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 274);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // pnLich
            // 
            this.pnLich.Controls.Add(this.lpnLich);
            this.pnLich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLich.Location = new System.Drawing.Point(163, 291);
            this.pnLich.Name = "pnLich";
            this.pnLich.Size = new System.Drawing.Size(1095, 360);
            this.pnLich.TabIndex = 10;
            // 
            // lpnLich
            // 
            this.lpnLich.AutoScroll = true;
            this.lpnLich.BackColor = System.Drawing.Color.Transparent;
            this.lpnLich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lpnLich.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lpnLich.Location = new System.Drawing.Point(2, 2);
            this.lpnLich.Margin = new System.Windows.Forms.Padding(0);
            this.lpnLich.Name = "lpnLich";
            this.lpnLich.Size = new System.Drawing.Size(1091, 356);
            this.lpnLich.TabIndex = 32;
            // 
            // Frm_LichLamViec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.pnLich);
            this.Controls.Add(this.pnLuoi);
            this.Controls.Add(this.panelControl6);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl5);
            this.Name = "Frm_LichLamViec";
            this.Text = "Lịch làm việc";
            this.Load += new System.EventHandler(this.Frm_LichLamViec_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.foFontChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clrMauChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clrMau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaDiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKetThuc.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKetThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBatDau.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBatDau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTieuDe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienThucHien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrangThaiLich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienLich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnLuoi)).EndInit();
            this.pnLuoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLich)).EndInit();
            this.pnLich.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblFont;
        private DevExpress.XtraEditors.FontEdit foFontChu;
        private DevExpress.XtraEditors.ColorEdit clrMauChu;
        private DevExpress.XtraEditors.LabelControl lblMauChu;
        private DevExpress.XtraEditors.ColorEdit clrMau;
        private DevExpress.XtraEditors.TextEdit txtDiaDiem;
        private DevExpress.XtraEditors.DateEdit dtpNgayKetThuc;
        private DevExpress.XtraEditors.LabelControl lblNoiDung;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraEditors.DateEdit dtpNgayBatDau;
        private DevExpress.XtraEditors.TextEdit txtTieuDe;
        private DevExpress.XtraEditors.LookUpEdit cboTrangThai;
        private DevExpress.XtraEditors.LabelControl lblTrangThai;
        private DevExpress.XtraEditors.LabelControl lblNhanVienThucHien;
        private DevExpress.XtraEditors.LabelControl lblDiaDiem;
        private DevExpress.XtraEditors.LabelControl lblNgayKetThuc;
        private DevExpress.XtraEditors.LabelControl lblMau;
        private DevExpress.XtraEditors.LabelControl lblTieuDe;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl lblNgayBatDau;
        private DevExpress.XtraEditors.LabelControl lblDienGiai;
        private DevExpress.XtraEditors.TextEdit txtDienGiai;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboNhanVienThucHien;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnLuoi;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btnLich;
        private DevExpress.XtraEditors.PanelControl pnLuoi;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn NV_THUCHIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn MAU;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TIEUDE;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_BATDAU;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_KETTHUC;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn DIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn NVTHUCHIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENGIAI;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl pnLich;
        private System.Windows.Forms.FlowLayoutPanel lpnLich;
        private DevExpress.XtraEditors.SimpleButton btnXem;
        private DevExpress.XtraEditors.LookUpEdit cboNhanVienLich;
        private DevExpress.XtraEditors.LookUpEdit cboTrangThaiLich;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit cboNgay;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;




    }
}