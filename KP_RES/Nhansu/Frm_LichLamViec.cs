﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using System.Collections;

namespace KP_RES 
{
    public partial class Frm_LichLamViec : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";

        public Frm_LichLamViec()
        {
            InitializeComponent();
        }

        private void Frm_LichLamViec_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();

            btnLuoi.Width = panelControl5.Width / 2;
            btnLuoi_Click(null, null);
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);

            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTieuDe.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "") 
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?",  "Thông báo") == DialogResult.No)
                return;

            SqlParameter[] sParameter = new SqlParameter[1];
            sParameter[0] = new SqlParameter("@MA", (Object)sMa);

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsQLCongTy.ExecuteParameterSPs("SP_DeleteLichLamViec", sParameter);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTieuDe.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            String sNhanVienThucHien = clsQLCongTy.CheckedComboBox(cboNhanVienThucHien);

            SqlParameter[] sParameter = new SqlParameter[20];
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertLichLamViec";

                sParameter = new SqlParameter[12];
                sParameter[0] = new SqlParameter("@TIEUDE", (Object)txtTieuDe.Text);
                sParameter[1] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[2] = new SqlParameter("@TIME_BATDAU", (DateTime)dtpNgayBatDau.EditValue);
                sParameter[3] = new SqlParameter("@TIME_KETTHUC", (DateTime)dtpNgayKetThuc.EditValue);
                sParameter[4] = new SqlParameter("@DIADIEM", (Object)txtDiaDiem.Text);
                sParameter[5] = new SqlParameter("@NV_THUCHIEN", (Object)sNhanVienThucHien);
                sParameter[6] = new SqlParameter("@MAU", (Object)clrMau.Text);
                sParameter[7] = new SqlParameter("@TRANGTHAI_ID", (Object)cboTrangThai.EditValue.ToString());
                sParameter[8] = new SqlParameter("@DIENGIAI", (Object)txtDienGiai.Text);
                sParameter[9] = new SqlParameter("@NGUOITAO", (Object)clsGlobal.gsUserID);
                sParameter[10] = new SqlParameter("@MAUCHU", (Object)clrMauChu.Text);
                sParameter[11] = new SqlParameter("@FONTCHU", (Object)foFontChu.Text);
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateLichLamViec";

                sParameter = new SqlParameter[13];
                sParameter[0] = new SqlParameter("@TIEUDE", (object)txtTieuDe.Text);
                sParameter[1] = new SqlParameter("@NOIDUNG", (object)txtNoiDung.Text);
                sParameter[2] = new SqlParameter("@TIME_BATDAU", (DateTime)dtpNgayBatDau.EditValue);
                sParameter[3] = new SqlParameter("@TIME_KETTHUC", (DateTime)dtpNgayKetThuc.EditValue);
                sParameter[4] = new SqlParameter("@DIADIEM", (object)txtDiaDiem.Text);
                sParameter[5] = new SqlParameter("@NV_THUCHIEN", (object)sNhanVienThucHien);
                sParameter[6] = new SqlParameter("@MAU", (object)clrMau.Text);
                sParameter[7] = new SqlParameter("@TRANGTHAI_ID", (object)cboTrangThai.EditValue.ToString());
                sParameter[8] = new SqlParameter("@DIENGIAI", (object)txtDienGiai.Text);
                sParameter[9] = new SqlParameter("@NGUOISUA", (object)clsGlobal.gsUserID);
                sParameter[10] = new SqlParameter("@MA", (object)sMa);
                sParameter[11] = new SqlParameter("@MAUCHU", (object)clrMauChu.Text);
                sParameter[12] = new SqlParameter("@FONTCHU", (object)foFontChu.Text);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTieuDe.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEUDE").ToString();
            txtDiaDiem.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIADIEM").ToString();
            clrMau.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MAU").ToString();
            clrMauChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MAUCHU").ToString();
            foFontChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FONTCHU").ToString();
            txtNoiDung.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NOIDUNG").ToString();
            txtDienGiai.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENGIAI").ToString();
            cboTrangThai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TRANGTHAI_ID").ToString());
            dtpNgayBatDau.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIME_BATDAU").ToString());
            dtpNgayKetThuc.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIME_KETTHUC").ToString());
            String[] pStr = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NV_THUCHIEN").ToString().Split(',');
            clsQLCongTy.CheckedComboBox(cboNhanVienThucHien, pStr);
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@MA", (Object)sMa);
            sParameter[1] = new SqlParameter("@TYPE", 1);

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectLichLamViec", sParameter);
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            clsQLCongTy.LoadCombo("SP_ComBoNhanVien", cboNhanVienThucHien);
            clsQLCongTy.LoadCombo("SP_ComBoTrangThai", cboTrangThai);

            DataTable myDT = new DataTable();
            myDT.Columns.Add("MA", typeof(Int32));
            myDT.Columns.Add("TEN");
            myDT.Rows.Add("0", "Tất cả");
            myDT.Merge(clsQLCongTy.ReturnDataTableSPs("SP_ComBoTrangThai"));
            clsQLCongTy.LoadCombo(myDT, cboTrangThaiLich);

            myDT = new DataTable();
            myDT.Columns.Add("MA", typeof(Int32));
            myDT.Columns.Add("TEN");
            myDT.Rows.Add("0", "Tất cả");
            myDT.Merge(clsQLCongTy.ReturnDataTableSPs("SP_ComBoNhanVien"));
            clsQLCongTy.LoadCombo(myDT, cboNhanVienLich);

            myDT = new DataTable();
            myDT.Columns.Add("MA");
            myDT.Columns.Add("TEN");
            for (int j = 2016; j <= 2026; j++)
            {
                for (int i = 1; i <= 12; i++)
                {
                    String sThang = i.ToString("00") + "/" + j.ToString("0000");
                    myDT.Rows.Add(sThang, sThang);
                }
            }

            clsQLCongTy.LoadCombo(myDT, cboNgay);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("6051603");
            btnSua.Enabled = clsUserManagement.AllowEdit("6051603");
            btnXoa.Enabled = clsUserManagement.AllowDelete("6051603");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTieuDe.Properties.ReadOnly = sBoolean;
            txtDiaDiem.Properties.ReadOnly = sBoolean;
            txtDienGiai.Properties.ReadOnly = sBoolean;
            txtNoiDung.Properties.ReadOnly = sBoolean;
            clrMau.Properties.ReadOnly = sBoolean;
            clrMauChu.Properties.ReadOnly = sBoolean;
            foFontChu.Properties.ReadOnly = sBoolean;
            dtpNgayBatDau.Properties.ReadOnly = sBoolean;
            dtpNgayKetThuc.Properties.ReadOnly = sBoolean;
            cboNhanVienThucHien.Properties.ReadOnly = sBoolean;
            cboTrangThai.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            txtTieuDe.Text = "";
            txtDiaDiem.Text = "";
            clrMau.Text = "255, 0, 0";
            clrMauChu.Text = "0, 0, 0";
            foFontChu.Text = "Tahoma";
            txtDienGiai.Text = "";
            txtNoiDung.Text = "";
            dtpNgayBatDau.EditValue = clsMain.GetServerDate();
            dtpNgayKetThuc.EditValue = clsMain.GetServerDate();
            cboTrangThai.EditValue = cboTrangThai.Properties.GetDataSourceValue(cboTrangThai.Properties.ValueMember, 0);
            cboNhanVienThucHien.CheckAll();
            clsQLCongTy.UnCheckedComboBox(cboNhanVienThucHien);

            cboNgay.EditValue = DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString("0000");
        }

        private Boolean CheckInput()
        {
            if (txtTieuDe.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTieuDe.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTieuDe.Focus();
                return false;
            }
            if (cboNhanVienThucHien.EditValue == "" || cboNhanVienThucHien.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblNhanVienThucHien.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNhanVienThucHien.Focus();
                return false;
            }
            if (cboTrangThai.EditValue == "" || cboTrangThai.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblTrangThai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTrangThai.Focus();
                return false;
            }
            if (clrMau.Text == "" || clrMau.Text == null)
            {
                XtraMessageBox.Show("Nhập " + lblMau.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clrMau.Focus();
                return false;
            }
            if (clrMauChu.Text == "" || clrMauChu.Text == null)
            {
                XtraMessageBox.Show("Nhập " + lblMauChu.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clrMauChu.Focus();
                return false;
            }
            if (foFontChu.Text == "" || foFontChu.Text == null)
            {
                XtraMessageBox.Show("Nhập " + lblFont.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                foFontChu.Focus();
                return false;
            }
            return true;
        }

        private void btnLuoi_Click(object sender, EventArgs e)
        {
            pnLuoi.Dock = DockStyle.Fill;
            pnLuoi.Visible = true;
            panelControl1.Visible = true;
            pnLich.Visible = false;
            panelControl6.Visible = false;
        }

        private void btnLich_Click(object sender, EventArgs e)
        {
            pnLich.Dock = DockStyle.Fill;
            pnLich.Visible = true;
            panelControl6.Visible = true;
            pnLuoi.Visible = false;
            panelControl1.Visible = false;

            btnXem_Click(null, null);
        }

        private void LoadWorkInMonth(DateTime sDateTime)
        {
            lpnLich.Controls.Clear();

            int pMonth = DateTime.DaysInMonth(sDateTime.Year, sDateTime.Month);

            LoadTitle();
            LoadDayBefore(sDateTime);
            for (int i = 0; i < pMonth; i++)
            {
                DateTime pDateTime = sDateTime.AddDays(i);
                DataTable myDT = LoadWorkInDay(pDateTime);

                FlowLayoutPanel flpDate = new FlowLayoutPanel();
                PanelControl pncDate = new PanelControl();
                Panel pnRow = new Panel();
                LabelControl lblRow = new LabelControl();

                if (String.Format("{0:dd/MM/yyyy}", pDateTime) == String.Format("{0:dd/MM/yyyy}", DateTime.Now))
                    flpDate.BackColor = Color.LightBlue;

                int SWidthLich = lpnLich.Width / 7 - 10;
                int pHeight = SWidthLich / 5 - 7;

                pncDate.Size = (myDT.Rows.Count > 4) ? new Size(SWidthLich - 17, SWidthLich) : new Size(SWidthLich, SWidthLich);

                flpDate.Dock = DockStyle.Fill;
                flpDate.AutoScroll = true;

                pnRow.Size = new Size(pncDate.Width - 10, pHeight);

                lblRow.Dock = DockStyle.Fill;
                lblRow.AutoSizeMode = LabelAutoSizeMode.None;
                lblRow.Text = (pDateTime.Day.ToString("00") == "01") ? (i + 1) + " tháng " + pDateTime.Month : (i + 1).ToString();
                lblRow.Padding = new Padding(pnRow.Width * 5 / 100, 0, 0, 0);
                lblRow.Font = new Font("Tahoma", 11);

                pnRow.Controls.Add(lblRow);
                flpDate.Controls.Add(pnRow);

                for (int j = 0; j < myDT.Rows.Count; j++)
                {
                    pnRow = new Panel();
                    pnRow.Size = new Size(pncDate.Width - 10, pHeight);
                    pnRow.BackColor = ColorTranslator.FromHtml(myDT.Rows[j]["MAU"].ToString());
                    pnRow.Name = myDT.Rows[j]["MA"].ToString();
                    //pnRow.Click += new System.EventHandler(pnRow_Click);

                    lblRow = new LabelControl();
                    lblRow.Dock = DockStyle.Fill;
                    lblRow.AutoSizeMode = LabelAutoSizeMode.None;
                    lblRow.Name = myDT.Rows[j]["MA"].ToString();
                    lblRow.Text = myDT.Rows[j]["TIEUDE"].ToString();
                    lblRow.Padding = new Padding(pnRow.Width * 5 / 100, 0, 0, 0);
                    lblRow.ForeColor = ColorTranslator.FromHtml(myDT.Rows[j]["MAUCHU"].ToString());
                    lblRow.ToolTip = ToolTipCell(myDT.Rows[j]);
                    //lblRow.Click += new System.EventHandler(lblRow_Click);

                    try {
                        lblRow.Font = new Font(myDT.Rows[j]["FONTCHU"].ToString(), 11);
                    }
                    catch (Exception) {
                        lblRow.Font = new Font("Tahoma", 11);
                    }

                    pnRow.Controls.Add(lblRow);
                    flpDate.Controls.Add(pnRow);
                }
                pncDate.Width = SWidthLich;

                pncDate.Controls.Add(flpDate);
                lpnLich.Controls.Add(pncDate);
            }
            LoadDayAfter(sDateTime);
        }

        private void pnRow_Click(object sender, EventArgs e)
        {
            btnLuoi_Click(null, null);

            Panel pnRow = (Panel)sender;
            sMa = pnRow.Name;

            LoadRowCell(sMa);
        }

        private void lblRow_Click(object sender, EventArgs e)
        {
            btnLuoi_Click(null, null);

            LabelControl lblRow = (LabelControl)sender;
            sMa = lblRow.Name;

            LoadRowCell(sMa);
        }

        private void LoadRowCell(String sMa)
        {
            if (sMa == "" || sMa == null)
                return;

            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@MA", (Object)sMa);
            sParameter[1] = new SqlParameter("@TYPE", 2);

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectLichLamViec", sParameter);

            txtTieuDe.Text = myDT.Rows[0]["TIEUDE"].ToString();
            txtDiaDiem.Text = myDT.Rows[0]["DIADIEM"].ToString();
            clrMau.Text = myDT.Rows[0]["MAU"].ToString();
            clrMauChu.Text = myDT.Rows[0]["MAUCHU"].ToString();
            foFontChu.Text = myDT.Rows[0]["FONTCHU"].ToString();
            txtNoiDung.Text = myDT.Rows[0]["NOIDUNG"].ToString();
            txtDienGiai.Text = myDT.Rows[0]["DIENGIAI"].ToString();
            cboTrangThai.EditValue = int.Parse(myDT.Rows[0]["TRANGTHAI_ID"].ToString());
            dtpNgayBatDau.EditValue = DateTime.Parse(myDT.Rows[0]["TIME_BATDAU"].ToString());
            dtpNgayKetThuc.EditValue = DateTime.Parse(myDT.Rows[0]["TIME_KETTHUC"].ToString());
            clsQLCongTy.CheckedComboBox(cboNhanVienThucHien, myDT.Rows[0]["NV_THUCHIEN"].ToString().Split(','));

            KhoaMoControl(true);
        }

        private String ToolTipCell(DataRow sRowCell)
        {
            String sToolTip = "";

            sToolTip += "Tiêu đề : " + sRowCell["TIEUDE"].ToString() + "\n";
            sToolTip += "Bắt đầu : " + String.Format("{0:dd/MM/yyyy}",sRowCell["TIME_BATDAU"]) + "\n";
            sToolTip += "Kết thúc : " + String.Format("{0:dd/MM/yyyy}",sRowCell["TIME_KETTHUC"]) + "\n";
            sToolTip += "Nội dung : " + sRowCell["NOIDUNG"].ToString() + "\n";
            sToolTip += "Địa điểm : " + sRowCell["DIADIEM"].ToString() + "\n";
            sToolTip += "NV thực hiện : " + sRowCell["NHANVIEN"].ToString() + "\n";
            sToolTip += "Trạng thái : " + sRowCell["TRANGTHAI"].ToString() + "\n";
            sToolTip += "Diễn giải : " + sRowCell["DIENGIAI"].ToString();

            return sToolTip;
        }

        private void LoadTitle()
        {
            for (int i = 2; i < 9; i++)
            {
                Panel pnRow = new Panel();
                LabelControl lblRow = new LabelControl();

                int SWidthLich = lpnLich.Width / 7 - 10;
                pnRow.Size = new Size(SWidthLich, SWidthLich / 5 - 7);

                lblRow.Dock = DockStyle.Fill;
                lblRow.AutoSizeMode = LabelAutoSizeMode.None;
                lblRow.Padding = new Padding(pnRow.Width * 5 / 100, 0, 0, 0);
                lblRow.Font = new Font("Tahoma", 11);
                lblRow.Text = (i == 8) ? "Chủ nhật" : "Thứ " + i;

                pnRow.Controls.Add(lblRow);
                lpnLich.Controls.Add(pnRow);
            }
        }

        private void ShowDateHide(int sSoNgay, DateTime sDateTime)
        {
            for (int i = 0; i < sSoNgay; i++)
            {
                int sAddDay = (sDateTime.Day.ToString("00") == "01") ? i - sSoNgay : i + 1;
                String sText = sDateTime.AddDays(sAddDay).Day.ToString();

                PanelControl pncDate = new PanelControl();
                Panel pnRow = new Panel();
                LabelControl lblRow = new LabelControl();

                int SWidthLich = lpnLich.Width / 7 - 10;
                pncDate.Size = new Size(SWidthLich, SWidthLich);

                pnRow.Dock = DockStyle.Top;
                pnRow.Size = new Size(pncDate.Width, SWidthLich / 5 - 7);

                lblRow.Dock = DockStyle.Fill;
                lblRow.AutoSizeMode = LabelAutoSizeMode.None;
                lblRow.Padding = new Padding(pnRow.Width * 5 / 100, 0, 0, 0);
                lblRow.Font = new Font("Tahoma", 11);
                lblRow.Enabled = false;
                lblRow.Text = (sText.ToString() == "1") ? sText + " Tháng " + sDateTime.AddMonths(1).Month : sText;

                pnRow.Controls.Add(lblRow);
                pncDate.Controls.Add(pnRow);
                lpnLich.Controls.Add(pncDate);
            }
        }

        private void LoadDayBefore(DateTime sDateTime)
        {
            int sNumberDay = ((int)sDateTime.DayOfWeek == 0) ? 6 : (int)sDateTime.DayOfWeek - 1;
            ShowDateHide(sNumberDay, sDateTime);
        }

        private void LoadDayAfter(DateTime sDateTime)
        {
            sDateTime = sDateTime.AddMonths(1).AddDays(-1);
            int sNumberDay = ((int)sDateTime.DayOfWeek == 0) ? 6 : (int)sDateTime.DayOfWeek - 1;
            ShowDateHide(6 - sNumberDay, sDateTime);
        }

        private DataTable LoadWorkInDay(DateTime sDateTime)
        {
            SqlParameter[] sParameter = new SqlParameter[3];
            sParameter[0] = new SqlParameter("@DATE", (Object)String.Format("{0:dd/MM/yyyy}", sDateTime));
            sParameter[1] = new SqlParameter("@NHANVIEN", (Object)cboNhanVienLich.EditValue);
            sParameter[2] = new SqlParameter("@TRANGTHAI", (Object)cboTrangThaiLich.EditValue);

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectLichLamViecInDay", sParameter);
            return myDT;
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            splashScreenManager1.ShowWaitForm();
            try
            {
                DateTime sDateTime = DateTime.ParseExact("01/" + cboNgay.EditValue, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                LoadWorkInMonth(sDateTime);
            }
            catch (Exception){

            }
            splashScreenManager1.CloseWaitForm();

        }

        

        

        

       



    }
}