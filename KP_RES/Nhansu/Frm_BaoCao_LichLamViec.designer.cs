﻿namespace KP_RES
{
    partial class Frm_BaoCao_LichLamViec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCao_LichLamViec));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.cboData = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTuNgay = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblDenNgay = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.grdLichLamViec = new DevExpress.XtraGrid.GridControl();
            this.grvLichLamViec = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NV_THUCHIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEUDE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_BATDAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIME_KETTHUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENGIAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLichLamViec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichLamViec)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 674);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 192);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 445);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 80);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 363);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.dtpTuNgay);
            this.pnChoose.Controls.Add(this.cboData);
            this.pnChoose.Controls.Add(this.lblTuNgay);
            this.pnChoose.Controls.Add(this.dtpDenNgay);
            this.pnChoose.Controls.Add(this.lblTitle);
            this.pnChoose.Controls.Add(this.lblDenNgay);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 78);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(87, 4);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpTuNgay.TabIndex = 8;
            this.dtpTuNgay.Visible = false;
            // 
            // cboData
            // 
            this.cboData.Location = new System.Drawing.Point(3, 45);
            this.cboData.Name = "cboData";
            this.cboData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.Appearance.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboData.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboData.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboData.Properties.DisplayMember = "TEN";
            this.cboData.Properties.DropDownItemHeight = 40;
            this.cboData.Properties.NullText = "";
            this.cboData.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboData.Properties.ShowHeader = false;
            this.cboData.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboData.Properties.ValueMember = "MA";
            this.cboData.Size = new System.Drawing.Size(210, 26);
            this.cboData.TabIndex = 17;
            this.cboData.Visible = false;
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTuNgay.Location = new System.Drawing.Point(3, 8);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(72, 19);
            this.lblTuNgay.TabIndex = 10;
            this.lblTuNgay.Text = "Từ Ngày :";
            this.lblTuNgay.Visible = false;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(87, 44);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpDenNgay.TabIndex = 9;
            this.dtpDenNgay.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTitle.Location = new System.Drawing.Point(3, 8);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(84, 19);
            this.lblTitle.TabIndex = 16;
            this.lblTitle.Text = "Tên Cấp Độ";
            this.lblTitle.Visible = false;
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblDenNgay.Location = new System.Drawing.Point(3, 48);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(80, 19);
            this.lblDenNgay.TabIndex = 11;
            this.lblDenNgay.Text = "Đến Ngày :";
            this.lblDenNgay.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 190);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "0";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "Theo nhân viên"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "Theo trạng thái"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "Theo ngày bắt đầu"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "Theo ngày kết thúc")});
            this.optGroup.Size = new System.Drawing.Size(215, 188);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 637);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatFile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1325, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 674);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 350);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 190);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 270);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 187);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 592);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 162);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatFile
            // 
            this.btnXuatFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatFile.Appearance.Options.UseFont = true;
            this.btnXuatFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatFile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatFile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatFile.Name = "btnXuatFile";
            this.btnXuatFile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatFile.TabIndex = 14;
            this.btnXuatFile.Text = "&Xuất File";
            this.btnXuatFile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // grdLichLamViec
            // 
            this.grdLichLamViec.AllowDrop = true;
            this.grdLichLamViec.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grdLichLamViec.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdLichLamViec.Location = new System.Drawing.Point(223, 0);
            this.grdLichLamViec.MainView = this.grvLichLamViec;
            this.grdLichLamViec.Name = "grdLichLamViec";
            this.grdLichLamViec.Size = new System.Drawing.Size(1102, 674);
            this.grdLichLamViec.TabIndex = 9;
            this.grdLichLamViec.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvLichLamViec});
            // 
            // grvLichLamViec
            // 
            this.grvLichLamViec.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grvLichLamViec.Appearance.FooterPanel.Options.UseFont = true;
            this.grvLichLamViec.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvLichLamViec.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvLichLamViec.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grvLichLamViec.Appearance.GroupFooter.Options.UseFont = true;
            this.grvLichLamViec.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grvLichLamViec.Appearance.GroupRow.Options.UseFont = true;
            this.grvLichLamViec.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grvLichLamViec.Appearance.Row.Options.UseFont = true;
            this.grvLichLamViec.ColumnPanelRowHeight = 30;
            this.grvLichLamViec.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NV_THUCHIEN,
            this.TRANGTHAI_ID,
            this.MAU,
            this.STT,
            this.MA,
            this.TIEUDE,
            this.TIME_BATDAU,
            this.TIME_KETTHUC,
            this.NOIDUNG,
            this.DIADIEM,
            this.NHANVIEN,
            this.TRANGTHAI,
            this.DIENGIAI,
            this.FILL});
            this.grvLichLamViec.FooterPanelHeight = 30;
            this.grvLichLamViec.GridControl = this.grdLichLamViec;
            this.grvLichLamViec.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.grvLichLamViec.Name = "grvLichLamViec";
            this.grvLichLamViec.OptionsFind.AlwaysVisible = true;
            this.grvLichLamViec.OptionsFind.ShowCloseButton = false;
            this.grvLichLamViec.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvLichLamViec.OptionsView.EnableAppearanceEvenRow = true;
            this.grvLichLamViec.OptionsView.ShowAutoFilterRow = true;
            this.grvLichLamViec.OptionsView.ShowFooter = true;
            this.grvLichLamViec.OptionsView.ShowIndicator = false;
            this.grvLichLamViec.RowHeight = 30;
            this.grvLichLamViec.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvLichLamViec.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvLichLamViec_CustomDrawCell);
            // 
            // NV_THUCHIEN
            // 
            this.NV_THUCHIEN.FieldName = "NV_THUCHIEN";
            this.NV_THUCHIEN.Name = "NV_THUCHIEN";
            this.NV_THUCHIEN.OptionsColumn.AllowEdit = false;
            this.NV_THUCHIEN.OptionsColumn.AllowFocus = false;
            this.NV_THUCHIEN.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowMove = false;
            // 
            // MAU
            // 
            this.MAU.AppearanceCell.Options.UseTextOptions = true;
            this.MAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAU.AppearanceHeader.Options.UseFont = true;
            this.MAU.AppearanceHeader.Options.UseTextOptions = true;
            this.MAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAU.Caption = "Màu hiển thị";
            this.MAU.FieldName = "MAU";
            this.MAU.Name = "MAU";
            this.MAU.OptionsColumn.AllowEdit = false;
            this.MAU.OptionsColumn.AllowFocus = false;
            this.MAU.OptionsColumn.FixedWidth = true;
            this.MAU.Width = 120;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TIEUDE
            // 
            this.TIEUDE.AppearanceCell.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEUDE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEUDE.AppearanceHeader.Options.UseFont = true;
            this.TIEUDE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEUDE.Caption = "Tiêu đề";
            this.TIEUDE.FieldName = "TIEUDE";
            this.TIEUDE.Name = "TIEUDE";
            this.TIEUDE.OptionsColumn.AllowEdit = false;
            this.TIEUDE.OptionsColumn.AllowFocus = false;
            this.TIEUDE.OptionsColumn.FixedWidth = true;
            this.TIEUDE.Visible = true;
            this.TIEUDE.VisibleIndex = 2;
            this.TIEUDE.Width = 200;
            // 
            // TIME_BATDAU
            // 
            this.TIME_BATDAU.AppearanceCell.Options.UseTextOptions = true;
            this.TIME_BATDAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_BATDAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_BATDAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIME_BATDAU.AppearanceHeader.Options.UseFont = true;
            this.TIME_BATDAU.AppearanceHeader.Options.UseTextOptions = true;
            this.TIME_BATDAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_BATDAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_BATDAU.Caption = "Ngày bắt đầu";
            this.TIME_BATDAU.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.TIME_BATDAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TIME_BATDAU.FieldName = "TIME_BATDAU";
            this.TIME_BATDAU.Name = "TIME_BATDAU";
            this.TIME_BATDAU.OptionsColumn.AllowEdit = false;
            this.TIME_BATDAU.OptionsColumn.AllowFocus = false;
            this.TIME_BATDAU.OptionsColumn.FixedWidth = true;
            this.TIME_BATDAU.Visible = true;
            this.TIME_BATDAU.VisibleIndex = 3;
            this.TIME_BATDAU.Width = 120;
            // 
            // TIME_KETTHUC
            // 
            this.TIME_KETTHUC.AppearanceCell.Options.UseTextOptions = true;
            this.TIME_KETTHUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_KETTHUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIME_KETTHUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIME_KETTHUC.AppearanceHeader.Options.UseFont = true;
            this.TIME_KETTHUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TIME_KETTHUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIME_KETTHUC.Caption = "Ngày kết thúc";
            this.TIME_KETTHUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.TIME_KETTHUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TIME_KETTHUC.FieldName = "TIME_KETTHUC";
            this.TIME_KETTHUC.Name = "TIME_KETTHUC";
            this.TIME_KETTHUC.OptionsColumn.AllowEdit = false;
            this.TIME_KETTHUC.OptionsColumn.AllowFocus = false;
            this.TIME_KETTHUC.OptionsColumn.FixedWidth = true;
            this.TIME_KETTHUC.Visible = true;
            this.TIME_KETTHUC.VisibleIndex = 4;
            this.TIME_KETTHUC.Width = 120;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 5;
            this.NOIDUNG.Width = 350;
            // 
            // DIADIEM
            // 
            this.DIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.DIADIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIADIEM.AppearanceHeader.Options.UseFont = true;
            this.DIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.DIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIADIEM.Caption = "Địa điểm";
            this.DIADIEM.FieldName = "DIADIEM";
            this.DIADIEM.Name = "DIADIEM";
            this.DIADIEM.OptionsColumn.AllowEdit = false;
            this.DIADIEM.OptionsColumn.AllowFocus = false;
            this.DIADIEM.OptionsColumn.FixedWidth = true;
            this.DIADIEM.Visible = true;
            this.DIADIEM.VisibleIndex = 6;
            this.DIADIEM.Width = 130;
            // 
            // NHANVIEN
            // 
            this.NHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.NHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIEN.Caption = "Nhân viên thực hiện";
            this.NHANVIEN.FieldName = "NHANVIEN";
            this.NHANVIEN.Name = "NHANVIEN";
            this.NHANVIEN.OptionsColumn.AllowEdit = false;
            this.NHANVIEN.OptionsColumn.AllowFocus = false;
            this.NHANVIEN.OptionsColumn.FixedWidth = true;
            this.NHANVIEN.Visible = true;
            this.NHANVIEN.VisibleIndex = 7;
            this.NHANVIEN.Width = 300;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 8;
            this.TRANGTHAI.Width = 150;
            // 
            // DIENGIAI
            // 
            this.DIENGIAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENGIAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENGIAI.AppearanceHeader.Options.UseFont = true;
            this.DIENGIAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENGIAI.Caption = "Diễn giải";
            this.DIENGIAI.FieldName = "DIENGIAI";
            this.DIENGIAI.Name = "DIENGIAI";
            this.DIENGIAI.OptionsColumn.AllowEdit = false;
            this.DIENGIAI.OptionsColumn.AllowFocus = false;
            this.DIENGIAI.OptionsColumn.FixedWidth = true;
            this.DIENGIAI.Visible = true;
            this.DIENGIAI.VisibleIndex = 9;
            this.DIENGIAI.Width = 300;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 10;
            this.FILL.Width = 20;
            // 
            // Frm_BaoCao_LichLamViec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 674);
            this.Controls.Add(this.grdLichLamViec);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_BaoCao_LichLamViec";
            this.Text = "Danh sách lịch làm việc";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLichLamViec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvLichLamViec)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatFile;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private DevExpress.XtraEditors.LabelControl lblTuNgay;
        private DevExpress.XtraEditors.LabelControl lblDenNgay;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LookUpEdit cboData;
        private DevExpress.XtraGrid.GridControl grdLichLamViec;
        private DevExpress.XtraGrid.Views.Grid.GridView grvLichLamViec;
        private DevExpress.XtraGrid.Columns.GridColumn NV_THUCHIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn MAU;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TIEUDE;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_BATDAU;
        private DevExpress.XtraGrid.Columns.GridColumn TIME_KETTHUC;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn DIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENGIAI;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;



    }
}