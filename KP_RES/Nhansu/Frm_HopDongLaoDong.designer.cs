﻿namespace KP_RES 
{
    partial class Frm_HopDongLaoDong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.chkBHThatNghiep = new DevExpress.XtraEditors.CheckEdit();
            this.chkBHYTe = new DevExpress.XtraEditors.CheckEdit();
            this.chkBHXaHoi = new DevExpress.XtraEditors.CheckEdit();
            this.txtGhiChu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtLuongThucTe = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtThoiGianLV = new DevExpress.XtraEditors.TextEdit();
            this.txtDenBuHD = new DevExpress.XtraEditors.TextEdit();
            this.cboNguoiSDLaoDong = new DevExpress.XtraEditors.LookUpEdit();
            this.cboNguoiLaoDong = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpNgayHetHL = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgayHL = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgayKy = new DevExpress.XtraEditors.DateEdit();
            this.cboPhuCap = new DevExpress.XtraEditors.LookUpEdit();
            this.lblPhuCap = new DevExpress.XtraEditors.LabelControl();
            this.lblThoiGianLV = new DevExpress.XtraEditors.LabelControl();
            this.txtNgayTraLuong = new DevExpress.XtraEditors.TextEdit();
            this.lblNgayTraLuong = new DevExpress.XtraEditors.LabelControl();
            this.lblMucLuong = new DevExpress.XtraEditors.LabelControl();
            this.txtMucLuong = new DevExpress.XtraEditors.TextEdit();
            this.lblNgayHL = new DevExpress.XtraEditors.LabelControl();
            this.txtThoiHan = new DevExpress.XtraEditors.TextEdit();
            this.txtHinhThuc = new DevExpress.XtraEditors.TextEdit();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.txtFile = new DevExpress.XtraEditors.TextEdit();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.lblFile = new DevExpress.XtraEditors.LabelControl();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.lblHinhThuc = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayHetHL = new DevExpress.XtraEditors.LabelControl();
            this.lblThoiHan = new DevExpress.XtraEditors.LabelControl();
            this.lblNguoiSDLaoDong = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayKy = new DevExpress.XtraEditors.LabelControl();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNguoiLaoDong = new DevExpress.XtraEditors.LabelControl();
            this.lblTrangThai = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOILAODONG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOISDLAODONG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUCAP_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHOPDONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOILAODONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOISDLAODONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHIEULUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHETHIEULUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MUCLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LUONGTHUCTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HINHTHUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTRALUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENPHUCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_XAHOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_YTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_THATNGHIEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIAN_LV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DENBU_HD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemNhieuHD = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHThatNghiep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHYTe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHXaHoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongThucTe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianLV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenBuHD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguoiSDLaoDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguoiLaoDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHL.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHL.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKy.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPhuCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayTraLuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucLuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 274);
            this.panelControl1.TabIndex = 0;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.btnThemNhieuHD);
            this.pal_ban.Controls.Add(this.chkBHThatNghiep);
            this.pal_ban.Controls.Add(this.chkBHYTe);
            this.pal_ban.Controls.Add(this.chkBHXaHoi);
            this.pal_ban.Controls.Add(this.txtGhiChu);
            this.pal_ban.Controls.Add(this.labelControl2);
            this.pal_ban.Controls.Add(this.txtLuongThucTe);
            this.pal_ban.Controls.Add(this.labelControl1);
            this.pal_ban.Controls.Add(this.txtThoiGianLV);
            this.pal_ban.Controls.Add(this.txtDenBuHD);
            this.pal_ban.Controls.Add(this.cboNguoiSDLaoDong);
            this.pal_ban.Controls.Add(this.cboNguoiLaoDong);
            this.pal_ban.Controls.Add(this.dtpNgayHetHL);
            this.pal_ban.Controls.Add(this.dtpNgayHL);
            this.pal_ban.Controls.Add(this.dtpNgayKy);
            this.pal_ban.Controls.Add(this.cboPhuCap);
            this.pal_ban.Controls.Add(this.lblPhuCap);
            this.pal_ban.Controls.Add(this.lblThoiGianLV);
            this.pal_ban.Controls.Add(this.txtNgayTraLuong);
            this.pal_ban.Controls.Add(this.lblNgayTraLuong);
            this.pal_ban.Controls.Add(this.lblMucLuong);
            this.pal_ban.Controls.Add(this.txtMucLuong);
            this.pal_ban.Controls.Add(this.lblNgayHL);
            this.pal_ban.Controls.Add(this.txtThoiHan);
            this.pal_ban.Controls.Add(this.txtHinhThuc);
            this.pal_ban.Controls.Add(this.txtTenFile);
            this.pal_ban.Controls.Add(this.lblTenFile);
            this.pal_ban.Controls.Add(this.btnDownLoadFile);
            this.pal_ban.Controls.Add(this.txtFile);
            this.pal_ban.Controls.Add(this.btnFile);
            this.pal_ban.Controls.Add(this.lblFile);
            this.pal_ban.Controls.Add(this.txtTen);
            this.pal_ban.Controls.Add(this.lblHinhThuc);
            this.pal_ban.Controls.Add(this.lblNgayHetHL);
            this.pal_ban.Controls.Add(this.lblThoiHan);
            this.pal_ban.Controls.Add(this.lblNguoiSDLaoDong);
            this.pal_ban.Controls.Add(this.lblNgayKy);
            this.pal_ban.Controls.Add(this.lblTen);
            this.pal_ban.Controls.Add(this.btnLuu);
            this.pal_ban.Controls.Add(this.btnSua);
            this.pal_ban.Controls.Add(this.btnXoa);
            this.pal_ban.Controls.Add(this.btnThem);
            this.pal_ban.Controls.Add(this.lblNguoiLaoDong);
            this.pal_ban.Controls.Add(this.lblTrangThai);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(1254, 270);
            this.pal_ban.TabIndex = 0;
            // 
            // chkBHThatNghiep
            // 
            this.chkBHThatNghiep.EnterMoveNextControl = true;
            this.chkBHThatNghiep.Location = new System.Drawing.Point(888, 101);
            this.chkBHThatNghiep.Name = "chkBHThatNghiep";
            this.chkBHThatNghiep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBHThatNghiep.Properties.Appearance.Options.UseFont = true;
            this.chkBHThatNghiep.Properties.Caption = "Bảo hiểm thất nghiệp";
            this.chkBHThatNghiep.Size = new System.Drawing.Size(250, 24);
            this.chkBHThatNghiep.TabIndex = 48;
            // 
            // chkBHYTe
            // 
            this.chkBHYTe.EnterMoveNextControl = true;
            this.chkBHYTe.Location = new System.Drawing.Point(496, 101);
            this.chkBHYTe.Name = "chkBHYTe";
            this.chkBHYTe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBHYTe.Properties.Appearance.Options.UseFont = true;
            this.chkBHYTe.Properties.Caption = "Bảo hiểm y tế";
            this.chkBHYTe.Size = new System.Drawing.Size(250, 24);
            this.chkBHYTe.TabIndex = 47;
            // 
            // chkBHXaHoi
            // 
            this.chkBHXaHoi.EnterMoveNextControl = true;
            this.chkBHXaHoi.Location = new System.Drawing.Point(92, 101);
            this.chkBHXaHoi.Name = "chkBHXaHoi";
            this.chkBHXaHoi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBHXaHoi.Properties.Appearance.Options.UseFont = true;
            this.chkBHXaHoi.Properties.Caption = "Bảo hiểm xã hội";
            this.chkBHXaHoi.Size = new System.Drawing.Size(250, 24);
            this.chkBHXaHoi.TabIndex = 46;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.EnterMoveNextControl = true;
            this.txtGhiChu.Location = new System.Drawing.Point(498, 197);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu.Size = new System.Drawing.Size(640, 26);
            this.txtGhiChu.TabIndex = 40;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(355, 197);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 19);
            this.labelControl2.TabIndex = 39;
            this.labelControl2.Text = "Ghi chú";
            // 
            // txtLuongThucTe
            // 
            this.txtLuongThucTe.EnterMoveNextControl = true;
            this.txtLuongThucTe.Location = new System.Drawing.Point(498, 69);
            this.txtLuongThucTe.Name = "txtLuongThucTe";
            this.txtLuongThucTe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongThucTe.Properties.Appearance.Options.UseFont = true;
            this.txtLuongThucTe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtLuongThucTe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtLuongThucTe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLuongThucTe.Size = new System.Drawing.Size(248, 26);
            this.txtLuongThucTe.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(355, 72);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 19);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Lương thực tế";
            // 
            // txtThoiGianLV
            // 
            this.txtThoiGianLV.EnterMoveNextControl = true;
            this.txtThoiGianLV.Location = new System.Drawing.Point(94, 133);
            this.txtThoiGianLV.Name = "txtThoiGianLV";
            this.txtThoiGianLV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianLV.Properties.Appearance.Options.UseFont = true;
            this.txtThoiGianLV.Size = new System.Drawing.Size(248, 26);
            this.txtThoiGianLV.TabIndex = 25;
            // 
            // txtDenBuHD
            // 
            this.txtDenBuHD.EnterMoveNextControl = true;
            this.txtDenBuHD.Location = new System.Drawing.Point(94, 165);
            this.txtDenBuHD.Name = "txtDenBuHD";
            this.txtDenBuHD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDenBuHD.Properties.Appearance.Options.UseFont = true;
            this.txtDenBuHD.Size = new System.Drawing.Size(248, 26);
            this.txtDenBuHD.TabIndex = 31;
            // 
            // cboNguoiSDLaoDong
            // 
            this.cboNguoiSDLaoDong.EnterMoveNextControl = true;
            this.cboNguoiSDLaoDong.Location = new System.Drawing.Point(890, 5);
            this.cboNguoiSDLaoDong.Name = "cboNguoiSDLaoDong";
            this.cboNguoiSDLaoDong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguoiSDLaoDong.Properties.Appearance.Options.UseFont = true;
            this.cboNguoiSDLaoDong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguoiSDLaoDong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNguoiSDLaoDong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNguoiSDLaoDong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNguoiSDLaoDong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNguoiSDLaoDong.Properties.DisplayMember = "TEN";
            this.cboNguoiSDLaoDong.Properties.DropDownItemHeight = 40;
            this.cboNguoiSDLaoDong.Properties.NullText = "";
            this.cboNguoiSDLaoDong.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNguoiSDLaoDong.Properties.ShowHeader = false;
            this.cboNguoiSDLaoDong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNguoiSDLaoDong.Properties.ValueMember = "MA";
            this.cboNguoiSDLaoDong.Size = new System.Drawing.Size(248, 26);
            this.cboNguoiSDLaoDong.TabIndex = 5;
            // 
            // cboNguoiLaoDong
            // 
            this.cboNguoiLaoDong.EnterMoveNextControl = true;
            this.cboNguoiLaoDong.Location = new System.Drawing.Point(498, 5);
            this.cboNguoiLaoDong.Name = "cboNguoiLaoDong";
            this.cboNguoiLaoDong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguoiLaoDong.Properties.Appearance.Options.UseFont = true;
            this.cboNguoiLaoDong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguoiLaoDong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNguoiLaoDong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNguoiLaoDong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNguoiLaoDong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNguoiLaoDong.Properties.DisplayMember = "TEN";
            this.cboNguoiLaoDong.Properties.DropDownItemHeight = 40;
            this.cboNguoiLaoDong.Properties.NullText = "";
            this.cboNguoiLaoDong.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNguoiLaoDong.Properties.ShowHeader = false;
            this.cboNguoiLaoDong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNguoiLaoDong.Properties.ValueMember = "MA";
            this.cboNguoiLaoDong.Size = new System.Drawing.Size(248, 26);
            this.cboNguoiLaoDong.TabIndex = 3;
            // 
            // dtpNgayHetHL
            // 
            this.dtpNgayHetHL.EditValue = null;
            this.dtpNgayHetHL.EnterMoveNextControl = true;
            this.dtpNgayHetHL.Location = new System.Drawing.Point(890, 37);
            this.dtpNgayHetHL.Name = "dtpNgayHetHL";
            this.dtpNgayHetHL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHL.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayHetHL.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHL.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayHetHL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayHetHL.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHetHL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHetHL.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHetHL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHetHL.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayHetHL.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayHetHL.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayHetHL.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayHetHL.TabIndex = 11;
            // 
            // dtpNgayHL
            // 
            this.dtpNgayHL.EditValue = null;
            this.dtpNgayHL.EnterMoveNextControl = true;
            this.dtpNgayHL.Location = new System.Drawing.Point(498, 37);
            this.dtpNgayHL.Name = "dtpNgayHL";
            this.dtpNgayHL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHL.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayHL.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHL.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayHL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayHL.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHL.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHL.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayHL.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayHL.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayHL.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayHL.TabIndex = 9;
            // 
            // dtpNgayKy
            // 
            this.dtpNgayKy.EditValue = null;
            this.dtpNgayKy.EnterMoveNextControl = true;
            this.dtpNgayKy.Location = new System.Drawing.Point(94, 197);
            this.dtpNgayKy.Name = "dtpNgayKy";
            this.dtpNgayKy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKy.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayKy.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayKy.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayKy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayKy.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKy.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayKy.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayKy.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayKy.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayKy.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayKy.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayKy.TabIndex = 38;
            // 
            // cboPhuCap
            // 
            this.cboPhuCap.EnterMoveNextControl = true;
            this.cboPhuCap.Location = new System.Drawing.Point(890, 69);
            this.cboPhuCap.Name = "cboPhuCap";
            this.cboPhuCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhuCap.Properties.Appearance.Options.UseFont = true;
            this.cboPhuCap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhuCap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPhuCap.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboPhuCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPhuCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboPhuCap.Properties.DisplayMember = "TEN";
            this.cboPhuCap.Properties.DropDownItemHeight = 40;
            this.cboPhuCap.Properties.NullText = "";
            this.cboPhuCap.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboPhuCap.Properties.ShowHeader = false;
            this.cboPhuCap.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboPhuCap.Properties.ValueMember = "MA";
            this.cboPhuCap.Size = new System.Drawing.Size(248, 26);
            this.cboPhuCap.TabIndex = 17;
            // 
            // lblPhuCap
            // 
            this.lblPhuCap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuCap.Location = new System.Drawing.Point(760, 72);
            this.lblPhuCap.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhuCap.Name = "lblPhuCap";
            this.lblPhuCap.Size = new System.Drawing.Size(56, 19);
            this.lblPhuCap.TabIndex = 16;
            this.lblPhuCap.Text = "Phụ cấp";
            // 
            // lblThoiGianLV
            // 
            this.lblThoiGianLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiGianLV.Location = new System.Drawing.Point(6, 136);
            this.lblThoiGianLV.Margin = new System.Windows.Forms.Padding(4);
            this.lblThoiGianLV.Name = "lblThoiGianLV";
            this.lblThoiGianLV.Size = new System.Drawing.Size(84, 19);
            this.lblThoiGianLV.TabIndex = 24;
            this.lblThoiGianLV.Text = "TG làm việc";
            // 
            // txtNgayTraLuong
            // 
            this.txtNgayTraLuong.EnterMoveNextControl = true;
            this.txtNgayTraLuong.Location = new System.Drawing.Point(890, 133);
            this.txtNgayTraLuong.Name = "txtNgayTraLuong";
            this.txtNgayTraLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayTraLuong.Properties.Appearance.Options.UseFont = true;
            this.txtNgayTraLuong.Size = new System.Drawing.Size(248, 26);
            this.txtNgayTraLuong.TabIndex = 29;
            // 
            // lblNgayTraLuong
            // 
            this.lblNgayTraLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayTraLuong.Location = new System.Drawing.Point(760, 136);
            this.lblNgayTraLuong.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayTraLuong.Name = "lblNgayTraLuong";
            this.lblNgayTraLuong.Size = new System.Drawing.Size(106, 19);
            this.lblNgayTraLuong.TabIndex = 28;
            this.lblNgayTraLuong.Text = "Ngày trả lương";
            // 
            // lblMucLuong
            // 
            this.lblMucLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMucLuong.Location = new System.Drawing.Point(6, 72);
            this.lblMucLuong.Margin = new System.Windows.Forms.Padding(4);
            this.lblMucLuong.Name = "lblMucLuong";
            this.lblMucLuong.Size = new System.Drawing.Size(75, 19);
            this.lblMucLuong.TabIndex = 12;
            this.lblMucLuong.Text = "Mức lương";
            // 
            // txtMucLuong
            // 
            this.txtMucLuong.EnterMoveNextControl = true;
            this.txtMucLuong.Location = new System.Drawing.Point(94, 69);
            this.txtMucLuong.Name = "txtMucLuong";
            this.txtMucLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucLuong.Properties.Appearance.Options.UseFont = true;
            this.txtMucLuong.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMucLuong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMucLuong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMucLuong.Size = new System.Drawing.Size(248, 26);
            this.txtMucLuong.TabIndex = 13;
            // 
            // lblNgayHL
            // 
            this.lblNgayHL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHL.Location = new System.Drawing.Point(355, 40);
            this.lblNgayHL.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayHL.Name = "lblNgayHL";
            this.lblNgayHL.Size = new System.Drawing.Size(97, 19);
            this.lblNgayHL.TabIndex = 8;
            this.lblNgayHL.Text = "Ngày hiệu lực";
            // 
            // txtThoiHan
            // 
            this.txtThoiHan.EnterMoveNextControl = true;
            this.txtThoiHan.Location = new System.Drawing.Point(94, 37);
            this.txtThoiHan.Name = "txtThoiHan";
            this.txtThoiHan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiHan.Properties.Appearance.Options.UseFont = true;
            this.txtThoiHan.Size = new System.Drawing.Size(248, 26);
            this.txtThoiHan.TabIndex = 7;
            // 
            // txtHinhThuc
            // 
            this.txtHinhThuc.EnterMoveNextControl = true;
            this.txtHinhThuc.Location = new System.Drawing.Point(498, 133);
            this.txtHinhThuc.Name = "txtHinhThuc";
            this.txtHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.txtHinhThuc.Size = new System.Drawing.Size(248, 26);
            this.txtHinhThuc.TabIndex = 27;
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(890, 165);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 36;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(760, 168);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 35;
            this.lblTenFile.Text = "Tên File";
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDownLoadFile.Location = new System.Drawing.Point(453, 230);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(87, 35);
            this.btnDownLoadFile.TabIndex = 45;
            this.btnDownLoadFile.Text = "Tải File";
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // txtFile
            // 
            this.txtFile.EnterMoveNextControl = true;
            this.txtFile.Location = new System.Drawing.Point(498, 165);
            this.txtFile.Name = "txtFile";
            this.txtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Properties.Appearance.Options.UseFont = true;
            this.txtFile.Properties.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(185, 26);
            this.txtFile.TabIndex = 33;
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(690, 166);
            this.btnFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(56, 25);
            this.btnFile.TabIndex = 34;
            this.btnFile.Text = "....";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblFile
            // 
            this.lblFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile.Location = new System.Drawing.Point(355, 168);
            this.lblFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(51, 19);
            this.lblFile.TabIndex = 32;
            this.lblFile.Text = "File HĐ";
            // 
            // txtTen
            // 
            this.txtTen.EnterMoveNextControl = true;
            this.txtTen.Location = new System.Drawing.Point(94, 5);
            this.txtTen.Name = "txtTen";
            this.txtTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Properties.Appearance.Options.UseFont = true;
            this.txtTen.Size = new System.Drawing.Size(248, 26);
            this.txtTen.TabIndex = 1;
            // 
            // lblHinhThuc
            // 
            this.lblHinhThuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHinhThuc.Location = new System.Drawing.Point(355, 136);
            this.lblHinhThuc.Margin = new System.Windows.Forms.Padding(4);
            this.lblHinhThuc.Name = "lblHinhThuc";
            this.lblHinhThuc.Size = new System.Drawing.Size(139, 19);
            this.lblHinhThuc.TabIndex = 26;
            this.lblHinhThuc.Text = "Hình thức trả lương";
            // 
            // lblNgayHetHL
            // 
            this.lblNgayHetHL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHetHL.Location = new System.Drawing.Point(760, 40);
            this.lblNgayHetHL.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayHetHL.Name = "lblNgayHetHL";
            this.lblNgayHetHL.Size = new System.Drawing.Size(124, 19);
            this.lblNgayHetHL.TabIndex = 10;
            this.lblNgayHetHL.Text = "Ngày hết hiệu lực";
            // 
            // lblThoiHan
            // 
            this.lblThoiHan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiHan.Location = new System.Drawing.Point(6, 40);
            this.lblThoiHan.Margin = new System.Windows.Forms.Padding(4);
            this.lblThoiHan.Name = "lblThoiHan";
            this.lblThoiHan.Size = new System.Drawing.Size(63, 19);
            this.lblThoiHan.TabIndex = 6;
            this.lblThoiHan.Text = "Thời hạn";
            // 
            // lblNguoiSDLaoDong
            // 
            this.lblNguoiSDLaoDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiSDLaoDong.Location = new System.Drawing.Point(760, 8);
            this.lblNguoiSDLaoDong.Margin = new System.Windows.Forms.Padding(4);
            this.lblNguoiSDLaoDong.Name = "lblNguoiSDLaoDong";
            this.lblNguoiSDLaoDong.Size = new System.Drawing.Size(92, 19);
            this.lblNguoiSDLaoDong.TabIndex = 4;
            this.lblNguoiSDLaoDong.Text = "Người SD LĐ";
            // 
            // lblNgayKy
            // 
            this.lblNgayKy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKy.Location = new System.Drawing.Point(6, 200);
            this.lblNgayKy.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayKy.Name = "lblNgayKy";
            this.lblNgayKy.Size = new System.Drawing.Size(57, 19);
            this.lblNgayKy.TabIndex = 37;
            this.lblNgayKy.Text = "Ngày ký";
            // 
            // lblTen
            // 
            this.lblTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Location = new System.Drawing.Point(6, 8);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(54, 19);
            this.lblTen.TabIndex = 0;
            this.lblTen.Text = "Tên HĐ";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(94, 230);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 41;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(270, 230);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 43;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(358, 230);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 44;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(182, 230);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 42;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNguoiLaoDong
            // 
            this.lblNguoiLaoDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiLaoDong.Location = new System.Drawing.Point(355, 8);
            this.lblNguoiLaoDong.Margin = new System.Windows.Forms.Padding(4);
            this.lblNguoiLaoDong.Name = "lblNguoiLaoDong";
            this.lblNguoiLaoDong.Size = new System.Drawing.Size(67, 19);
            this.lblNguoiLaoDong.TabIndex = 2;
            this.lblNguoiLaoDong.Text = "Người LĐ";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.Location = new System.Drawing.Point(6, 168);
            this.lblTrangThai.Margin = new System.Windows.Forms.Padding(4);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(78, 19);
            this.lblTrangThai.TabIndex = 30;
            this.lblTrangThai.Text = "Đền bù HĐ";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 274);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 377);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILES,
            this.NGUOILAODONG_ID,
            this.NGUOISDLAODONG_ID,
            this.PHUCAP_ID,
            this.STT,
            this.MA,
            this.TENHOPDONG,
            this.NGUOILAODONG,
            this.NGUOISDLAODONG,
            this.THOIHAN,
            this.NGAYKY,
            this.NGAYHIEULUC,
            this.NGAYHETHIEULUC,
            this.MUCLUONG,
            this.LUONGTHUCTE,
            this.HINHTHUC,
            this.NGAYTRALUONG,
            this.GHICHU,
            this.TIENPHUCAP,
            this.BH_XAHOI,
            this.BH_YTE,
            this.BH_THATNGHIEP,
            this.THOIGIAN_LV,
            this.DENBU_HD,
            this.TEN_FILES,
            this.gridColumn1});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILES
            // 
            this.FILES.FieldName = "FILES";
            this.FILES.Name = "FILES";
            this.FILES.OptionsColumn.AllowEdit = false;
            this.FILES.OptionsColumn.AllowFocus = false;
            this.FILES.OptionsColumn.FixedWidth = true;
            // 
            // NGUOILAODONG_ID
            // 
            this.NGUOILAODONG_ID.FieldName = "NGUOILAODONG_ID";
            this.NGUOILAODONG_ID.Name = "NGUOILAODONG_ID";
            this.NGUOILAODONG_ID.OptionsColumn.AllowEdit = false;
            this.NGUOILAODONG_ID.OptionsColumn.AllowFocus = false;
            this.NGUOILAODONG_ID.OptionsColumn.FixedWidth = true;
            // 
            // NGUOISDLAODONG_ID
            // 
            this.NGUOISDLAODONG_ID.FieldName = "NGUOISDLAODONG_ID";
            this.NGUOISDLAODONG_ID.Name = "NGUOISDLAODONG_ID";
            this.NGUOISDLAODONG_ID.OptionsColumn.AllowEdit = false;
            this.NGUOISDLAODONG_ID.OptionsColumn.AllowFocus = false;
            this.NGUOISDLAODONG_ID.OptionsColumn.FixedWidth = true;
            // 
            // PHUCAP_ID
            // 
            this.PHUCAP_ID.FieldName = "PHUCAP_ID";
            this.PHUCAP_ID.Name = "PHUCAP_ID";
            this.PHUCAP_ID.OptionsColumn.AllowEdit = false;
            this.PHUCAP_ID.OptionsColumn.AllowFocus = false;
            this.PHUCAP_ID.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TENHOPDONG
            // 
            this.TENHOPDONG.AppearanceCell.Options.UseTextOptions = true;
            this.TENHOPDONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHOPDONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENHOPDONG.AppearanceHeader.Options.UseFont = true;
            this.TENHOPDONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHOPDONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHOPDONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHOPDONG.Caption = "Tên hợp đồng";
            this.TENHOPDONG.FieldName = "TENHOPDONG";
            this.TENHOPDONG.Name = "TENHOPDONG";
            this.TENHOPDONG.OptionsColumn.AllowEdit = false;
            this.TENHOPDONG.OptionsColumn.AllowFocus = false;
            this.TENHOPDONG.OptionsColumn.FixedWidth = true;
            this.TENHOPDONG.Visible = true;
            this.TENHOPDONG.VisibleIndex = 2;
            this.TENHOPDONG.Width = 200;
            // 
            // NGUOILAODONG
            // 
            this.NGUOILAODONG.AppearanceCell.Options.UseTextOptions = true;
            this.NGUOILAODONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGUOILAODONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGUOILAODONG.AppearanceHeader.Options.UseFont = true;
            this.NGUOILAODONG.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUOILAODONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUOILAODONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGUOILAODONG.Caption = "Người LĐ";
            this.NGUOILAODONG.FieldName = "NGUOILAODONG";
            this.NGUOILAODONG.Name = "NGUOILAODONG";
            this.NGUOILAODONG.OptionsColumn.AllowEdit = false;
            this.NGUOILAODONG.OptionsColumn.AllowFocus = false;
            this.NGUOILAODONG.OptionsColumn.FixedWidth = true;
            this.NGUOILAODONG.Visible = true;
            this.NGUOILAODONG.VisibleIndex = 3;
            this.NGUOILAODONG.Width = 250;
            // 
            // NGUOISDLAODONG
            // 
            this.NGUOISDLAODONG.AppearanceCell.Options.UseTextOptions = true;
            this.NGUOISDLAODONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGUOISDLAODONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGUOISDLAODONG.AppearanceHeader.Options.UseFont = true;
            this.NGUOISDLAODONG.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUOISDLAODONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUOISDLAODONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGUOISDLAODONG.Caption = "Người SD LĐ";
            this.NGUOISDLAODONG.FieldName = "NGUOISDLAODONG";
            this.NGUOISDLAODONG.Name = "NGUOISDLAODONG";
            this.NGUOISDLAODONG.OptionsColumn.AllowEdit = false;
            this.NGUOISDLAODONG.OptionsColumn.AllowFocus = false;
            this.NGUOISDLAODONG.OptionsColumn.FixedWidth = true;
            this.NGUOISDLAODONG.Visible = true;
            this.NGUOISDLAODONG.VisibleIndex = 4;
            this.NGUOISDLAODONG.Width = 250;
            // 
            // THOIHAN
            // 
            this.THOIHAN.AppearanceCell.Options.UseTextOptions = true;
            this.THOIHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIHAN.AppearanceHeader.Options.UseFont = true;
            this.THOIHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIHAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIHAN.Caption = "Thời hạn";
            this.THOIHAN.FieldName = "THOIHAN";
            this.THOIHAN.Name = "THOIHAN";
            this.THOIHAN.OptionsColumn.AllowEdit = false;
            this.THOIHAN.OptionsColumn.AllowFocus = false;
            this.THOIHAN.OptionsColumn.FixedWidth = true;
            this.THOIHAN.Visible = true;
            this.THOIHAN.VisibleIndex = 5;
            this.THOIHAN.Width = 150;
            // 
            // NGAYKY
            // 
            this.NGAYKY.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYKY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYKY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYKY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYKY.AppearanceHeader.Options.UseFont = true;
            this.NGAYKY.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYKY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYKY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYKY.Caption = "Ngày ký";
            this.NGAYKY.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYKY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYKY.FieldName = "NGAYKY";
            this.NGAYKY.Name = "NGAYKY";
            this.NGAYKY.OptionsColumn.AllowEdit = false;
            this.NGAYKY.OptionsColumn.AllowFocus = false;
            this.NGAYKY.OptionsColumn.FixedWidth = true;
            this.NGAYKY.Visible = true;
            this.NGAYKY.VisibleIndex = 6;
            this.NGAYKY.Width = 100;
            // 
            // NGAYHIEULUC
            // 
            this.NGAYHIEULUC.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHIEULUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHIEULUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHIEULUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHIEULUC.AppearanceHeader.Options.UseFont = true;
            this.NGAYHIEULUC.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHIEULUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHIEULUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHIEULUC.Caption = "Ngày HL";
            this.NGAYHIEULUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYHIEULUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYHIEULUC.FieldName = "NGAYHIEULUC";
            this.NGAYHIEULUC.Name = "NGAYHIEULUC";
            this.NGAYHIEULUC.OptionsColumn.AllowEdit = false;
            this.NGAYHIEULUC.OptionsColumn.AllowFocus = false;
            this.NGAYHIEULUC.OptionsColumn.FixedWidth = true;
            this.NGAYHIEULUC.Visible = true;
            this.NGAYHIEULUC.VisibleIndex = 7;
            this.NGAYHIEULUC.Width = 120;
            // 
            // NGAYHETHIEULUC
            // 
            this.NGAYHETHIEULUC.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHETHIEULUC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHIEULUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHETHIEULUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHETHIEULUC.AppearanceHeader.Options.UseFont = true;
            this.NGAYHETHIEULUC.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHETHIEULUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHIEULUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHETHIEULUC.Caption = "Ngày hết HL";
            this.NGAYHETHIEULUC.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYHETHIEULUC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYHETHIEULUC.FieldName = "NGAYHETHIEULUC";
            this.NGAYHETHIEULUC.Name = "NGAYHETHIEULUC";
            this.NGAYHETHIEULUC.OptionsColumn.AllowEdit = false;
            this.NGAYHETHIEULUC.OptionsColumn.AllowFocus = false;
            this.NGAYHETHIEULUC.OptionsColumn.FixedWidth = true;
            this.NGAYHETHIEULUC.Visible = true;
            this.NGAYHETHIEULUC.VisibleIndex = 8;
            this.NGAYHETHIEULUC.Width = 120;
            // 
            // MUCLUONG
            // 
            this.MUCLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MUCLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MUCLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MUCLUONG.AppearanceHeader.Options.UseFont = true;
            this.MUCLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.MUCLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MUCLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MUCLUONG.Caption = "Mức lương";
            this.MUCLUONG.DisplayFormat.FormatString = "N0";
            this.MUCLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MUCLUONG.FieldName = "MUCLUONG";
            this.MUCLUONG.Name = "MUCLUONG";
            this.MUCLUONG.OptionsColumn.AllowEdit = false;
            this.MUCLUONG.OptionsColumn.AllowFocus = false;
            this.MUCLUONG.OptionsColumn.FixedWidth = true;
            this.MUCLUONG.Visible = true;
            this.MUCLUONG.VisibleIndex = 9;
            this.MUCLUONG.Width = 150;
            // 
            // LUONGTHUCTE
            // 
            this.LUONGTHUCTE.AppearanceCell.Options.UseTextOptions = true;
            this.LUONGTHUCTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LUONGTHUCTE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGTHUCTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LUONGTHUCTE.AppearanceHeader.Options.UseFont = true;
            this.LUONGTHUCTE.AppearanceHeader.Options.UseTextOptions = true;
            this.LUONGTHUCTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LUONGTHUCTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGTHUCTE.Caption = "Lương thực tế";
            this.LUONGTHUCTE.DisplayFormat.FormatString = "N0";
            this.LUONGTHUCTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LUONGTHUCTE.FieldName = "LUONGTHUCTE";
            this.LUONGTHUCTE.Name = "LUONGTHUCTE";
            this.LUONGTHUCTE.OptionsColumn.AllowEdit = false;
            this.LUONGTHUCTE.OptionsColumn.AllowFocus = false;
            this.LUONGTHUCTE.OptionsColumn.FixedWidth = true;
            this.LUONGTHUCTE.Visible = true;
            this.LUONGTHUCTE.VisibleIndex = 10;
            this.LUONGTHUCTE.Width = 150;
            // 
            // HINHTHUC
            // 
            this.HINHTHUC.AppearanceCell.Options.UseTextOptions = true;
            this.HINHTHUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HINHTHUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HINHTHUC.AppearanceHeader.Options.UseFont = true;
            this.HINHTHUC.AppearanceHeader.Options.UseTextOptions = true;
            this.HINHTHUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HINHTHUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HINHTHUC.Caption = "Hình thức";
            this.HINHTHUC.FieldName = "HINHTHUC";
            this.HINHTHUC.Name = "HINHTHUC";
            this.HINHTHUC.OptionsColumn.AllowEdit = false;
            this.HINHTHUC.OptionsColumn.AllowFocus = false;
            this.HINHTHUC.OptionsColumn.FixedWidth = true;
            this.HINHTHUC.Visible = true;
            this.HINHTHUC.VisibleIndex = 11;
            this.HINHTHUC.Width = 150;
            // 
            // NGAYTRALUONG
            // 
            this.NGAYTRALUONG.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTRALUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTRALUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTRALUONG.AppearanceHeader.Options.UseFont = true;
            this.NGAYTRALUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTRALUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTRALUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTRALUONG.Caption = "Ngày trả lương";
            this.NGAYTRALUONG.FieldName = "NGAYTRALUONG";
            this.NGAYTRALUONG.Name = "NGAYTRALUONG";
            this.NGAYTRALUONG.OptionsColumn.AllowEdit = false;
            this.NGAYTRALUONG.OptionsColumn.AllowFocus = false;
            this.NGAYTRALUONG.OptionsColumn.FixedWidth = true;
            this.NGAYTRALUONG.Visible = true;
            this.NGAYTRALUONG.VisibleIndex = 12;
            this.NGAYTRALUONG.Width = 150;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 13;
            this.GHICHU.Width = 200;
            // 
            // TIENPHUCAP
            // 
            this.TIENPHUCAP.AppearanceCell.Options.UseTextOptions = true;
            this.TIENPHUCAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENPHUCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHUCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENPHUCAP.AppearanceHeader.Options.UseFont = true;
            this.TIENPHUCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENPHUCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENPHUCAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHUCAP.Caption = "Phụ cấp";
            this.TIENPHUCAP.DisplayFormat.FormatString = "N0";
            this.TIENPHUCAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIENPHUCAP.FieldName = "TIENPHUCAP";
            this.TIENPHUCAP.Name = "TIENPHUCAP";
            this.TIENPHUCAP.OptionsColumn.AllowEdit = false;
            this.TIENPHUCAP.OptionsColumn.AllowFocus = false;
            this.TIENPHUCAP.OptionsColumn.FixedWidth = true;
            this.TIENPHUCAP.Visible = true;
            this.TIENPHUCAP.VisibleIndex = 14;
            this.TIENPHUCAP.Width = 150;
            // 
            // BH_XAHOI
            // 
            this.BH_XAHOI.AppearanceCell.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_XAHOI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_XAHOI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_XAHOI.AppearanceHeader.Options.UseFont = true;
            this.BH_XAHOI.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_XAHOI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_XAHOI.Caption = "BH Xã hội";
            this.BH_XAHOI.FieldName = "BH_XAHOI";
            this.BH_XAHOI.Name = "BH_XAHOI";
            this.BH_XAHOI.OptionsColumn.AllowEdit = false;
            this.BH_XAHOI.OptionsColumn.AllowFocus = false;
            this.BH_XAHOI.OptionsColumn.FixedWidth = true;
            this.BH_XAHOI.Visible = true;
            this.BH_XAHOI.VisibleIndex = 15;
            this.BH_XAHOI.Width = 150;
            // 
            // BH_YTE
            // 
            this.BH_YTE.AppearanceCell.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_YTE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_YTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_YTE.AppearanceHeader.Options.UseFont = true;
            this.BH_YTE.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_YTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_YTE.Caption = "BH Y tế";
            this.BH_YTE.FieldName = "BH_YTE";
            this.BH_YTE.Name = "BH_YTE";
            this.BH_YTE.OptionsColumn.AllowEdit = false;
            this.BH_YTE.OptionsColumn.AllowFocus = false;
            this.BH_YTE.OptionsColumn.FixedWidth = true;
            this.BH_YTE.Visible = true;
            this.BH_YTE.VisibleIndex = 16;
            this.BH_YTE.Width = 150;
            // 
            // BH_THATNGHIEP
            // 
            this.BH_THATNGHIEP.AppearanceCell.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseFont = true;
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.Caption = "BH Thất nghiệp";
            this.BH_THATNGHIEP.FieldName = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.Name = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.OptionsColumn.AllowEdit = false;
            this.BH_THATNGHIEP.OptionsColumn.AllowFocus = false;
            this.BH_THATNGHIEP.OptionsColumn.FixedWidth = true;
            this.BH_THATNGHIEP.Visible = true;
            this.BH_THATNGHIEP.VisibleIndex = 17;
            this.BH_THATNGHIEP.Width = 150;
            // 
            // THOIGIAN_LV
            // 
            this.THOIGIAN_LV.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIAN_LV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIGIAN_LV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIAN_LV.AppearanceHeader.Options.UseFont = true;
            this.THOIGIAN_LV.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIAN_LV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN_LV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIGIAN_LV.Caption = "Thời gian làm việc";
            this.THOIGIAN_LV.FieldName = "THOIGIAN_LV";
            this.THOIGIAN_LV.Name = "THOIGIAN_LV";
            this.THOIGIAN_LV.OptionsColumn.AllowEdit = false;
            this.THOIGIAN_LV.OptionsColumn.AllowFocus = false;
            this.THOIGIAN_LV.OptionsColumn.FixedWidth = true;
            this.THOIGIAN_LV.Visible = true;
            this.THOIGIAN_LV.VisibleIndex = 18;
            this.THOIGIAN_LV.Width = 200;
            // 
            // DENBU_HD
            // 
            this.DENBU_HD.AppearanceCell.Options.UseTextOptions = true;
            this.DENBU_HD.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DENBU_HD.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DENBU_HD.AppearanceHeader.Options.UseFont = true;
            this.DENBU_HD.AppearanceHeader.Options.UseTextOptions = true;
            this.DENBU_HD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DENBU_HD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DENBU_HD.Caption = "Đền bù hợp đồng";
            this.DENBU_HD.FieldName = "DENBU_HD";
            this.DENBU_HD.Name = "DENBU_HD";
            this.DENBU_HD.OptionsColumn.AllowEdit = false;
            this.DENBU_HD.OptionsColumn.AllowFocus = false;
            this.DENBU_HD.OptionsColumn.FixedWidth = true;
            this.DENBU_HD.Visible = true;
            this.DENBU_HD.VisibleIndex = 19;
            this.DENBU_HD.Width = 250;
            // 
            // TEN_FILES
            // 
            this.TEN_FILES.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILES.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILES.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILES.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILES.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILES.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILES.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILES.Caption = "File";
            this.TEN_FILES.FieldName = "TEN_FILES";
            this.TEN_FILES.Name = "TEN_FILES";
            this.TEN_FILES.OptionsColumn.AllowEdit = false;
            this.TEN_FILES.OptionsColumn.AllowFocus = false;
            this.TEN_FILES.OptionsColumn.FixedWidth = true;
            this.TEN_FILES.Visible = true;
            this.TEN_FILES.VisibleIndex = 20;
            this.TEN_FILES.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 21;
            this.gridColumn1.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 274);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 377);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 62);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 253);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 120);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 13);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 133);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 60);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 193);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 60);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 60);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 60);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 60);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 315);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 60);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 60);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnThemNhieuHD
            // 
            this.btnThemNhieuHD.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemNhieuHD.Appearance.Options.UseFont = true;
            this.btnThemNhieuHD.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThemNhieuHD.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemNhieuHD.Location = new System.Drawing.Point(548, 230);
            this.btnThemNhieuHD.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemNhieuHD.Name = "btnThemNhieuHD";
            this.btnThemNhieuHD.Size = new System.Drawing.Size(156, 35);
            this.btnThemNhieuHD.TabIndex = 49;
            this.btnThemNhieuHD.Text = "&Thêm nhiều HĐ";
            this.btnThemNhieuHD.Click += new System.EventHandler(this.btnThemNhieuHD_Click);
            // 
            // Frm_HopDongLaoDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_HopDongLaoDong";
            this.Text = "Hợp đồng lao động";
            this.Load += new System.EventHandler(this.Frm_HopDongLaoDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHThatNghiep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHYTe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBHXaHoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongThucTe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianLV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenBuHD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguoiSDLaoDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguoiLaoDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHL.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHL.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKy.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPhuCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayTraLuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucLuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOILAODONG;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOISDLAODONG;
        private DevExpress.XtraGrid.Columns.GridColumn THOIHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYKY;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHETHIEULUC;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHIEULUC;
        private DevExpress.XtraGrid.Columns.GridColumn MUCLUONG;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn TENHOPDONG;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn HINHTHUC;
        private DevExpress.XtraGrid.Columns.GridColumn FILES;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILES;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.LabelControl lblNgayHL;
        private DevExpress.XtraEditors.TextEdit txtThoiHan;
        private DevExpress.XtraEditors.TextEdit txtHinhThuc;
        private DevExpress.XtraEditors.TextEdit txtTenFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.TextEdit txtFile;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraEditors.LabelControl lblFile;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.LabelControl lblHinhThuc;
        private DevExpress.XtraEditors.LabelControl lblNgayHetHL;
        private DevExpress.XtraEditors.LabelControl lblThoiHan;
        private DevExpress.XtraEditors.LabelControl lblNguoiSDLaoDong;
        private DevExpress.XtraEditors.LabelControl lblNgayKy;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl lblNguoiLaoDong;
        private DevExpress.XtraEditors.LabelControl lblTrangThai;
        private DevExpress.XtraEditors.LabelControl lblThoiGianLV;
        private DevExpress.XtraEditors.TextEdit txtNgayTraLuong;
        private DevExpress.XtraEditors.LabelControl lblNgayTraLuong;
        private DevExpress.XtraEditors.LabelControl lblMucLuong;
        private DevExpress.XtraEditors.TextEdit txtMucLuong;
        private DevExpress.XtraGrid.Columns.GridColumn PHUCAP_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTRALUONG;
        private DevExpress.XtraGrid.Columns.GridColumn TIENPHUCAP;
        private DevExpress.XtraGrid.Columns.GridColumn BH_XAHOI;
        private DevExpress.XtraGrid.Columns.GridColumn BH_YTE;
        private DevExpress.XtraGrid.Columns.GridColumn BH_THATNGHIEP;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOILAODONG_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOISDLAODONG_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.TextEdit txtDenBuHD;
        private DevExpress.XtraEditors.LookUpEdit cboNguoiSDLaoDong;
        private DevExpress.XtraEditors.LookUpEdit cboNguoiLaoDong;
        private DevExpress.XtraEditors.DateEdit dtpNgayHetHL;
        private DevExpress.XtraEditors.DateEdit dtpNgayHL;
        private DevExpress.XtraEditors.DateEdit dtpNgayKy;
        private DevExpress.XtraEditors.LookUpEdit cboPhuCap;
        private DevExpress.XtraEditors.LabelControl lblPhuCap;
        private DevExpress.XtraEditors.TextEdit txtThoiGianLV;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIAN_LV;
        private DevExpress.XtraGrid.Columns.GridColumn DENBU_HD;
        private DevExpress.XtraEditors.TextEdit txtLuongThucTe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtGhiChu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn LUONGTHUCTE;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.CheckEdit chkBHThatNghiep;
        private DevExpress.XtraEditors.CheckEdit chkBHYTe;
        private DevExpress.XtraEditors.CheckEdit chkBHXaHoi;
        private DevExpress.XtraEditors.SimpleButton btnThemNhieuHD;




    }
}