﻿namespace KP_RES 
{
    partial class Frm_LuongThang 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.cboNam = new DevExpress.XtraEditors.LookUpEdit();
            this.cboThang = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLamMoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnXem = new DevExpress.XtraEditors.SimpleButton();
            this.lblThang = new DevExpress.XtraEditors.LabelControl();
            this.lblLoai = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NHANVIEN_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SONGAYLAMVIEC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LUONGCOBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LUONGTHUCTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTHUNHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCONG_TT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGLUONG_TT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LUONGDONG_BH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_XAHOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_YTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BH_THATNGHIEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTHUNHAP_TT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TAMUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUCLINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 84);
            this.panelControl1.TabIndex = 0;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.cboNam);
            this.pal_ban.Controls.Add(this.cboThang);
            this.pal_ban.Controls.Add(this.btnLamMoi);
            this.pal_ban.Controls.Add(this.btnXem);
            this.pal_ban.Controls.Add(this.lblThang);
            this.pal_ban.Controls.Add(this.lblLoai);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(1254, 80);
            this.pal_ban.TabIndex = 57;
            // 
            // cboNam
            // 
            this.cboNam.EnterMoveNextControl = true;
            this.cboNam.Location = new System.Drawing.Point(335, 5);
            this.cboNam.Name = "cboNam";
            this.cboNam.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNam.Properties.Appearance.Options.UseFont = true;
            this.cboNam.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNam.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNam.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNam.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNam.Properties.DisplayMember = "TEN";
            this.cboNam.Properties.DropDownItemHeight = 40;
            this.cboNam.Properties.NullText = "";
            this.cboNam.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNam.Properties.ShowHeader = false;
            this.cboNam.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNam.Properties.ValueMember = "MA";
            this.cboNam.Size = new System.Drawing.Size(230, 26);
            this.cboNam.TabIndex = 131;
            // 
            // cboThang
            // 
            this.cboThang.EnterMoveNextControl = true;
            this.cboThang.Location = new System.Drawing.Point(58, 5);
            this.cboThang.Name = "cboThang";
            this.cboThang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThang.Properties.Appearance.Options.UseFont = true;
            this.cboThang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboThang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboThang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboThang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboThang.Properties.DisplayMember = "TEN";
            this.cboThang.Properties.DropDownItemHeight = 40;
            this.cboThang.Properties.NullText = "";
            this.cboThang.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboThang.Properties.ShowHeader = false;
            this.cboThang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboThang.Properties.ValueMember = "MA";
            this.cboThang.Size = new System.Drawing.Size(230, 26);
            this.cboThang.TabIndex = 130;
            // 
            // btnLamMoi
            // 
            this.btnLamMoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLamMoi.Appearance.Options.UseFont = true;
            this.btnLamMoi.Image = global::KP_RES.Properties.Resources.file_26;
            this.btnLamMoi.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLamMoi.Location = new System.Drawing.Point(146, 38);
            this.btnLamMoi.Margin = new System.Windows.Forms.Padding(4);
            this.btnLamMoi.Name = "btnLamMoi";
            this.btnLamMoi.Size = new System.Drawing.Size(108, 35);
            this.btnLamMoi.TabIndex = 129;
            this.btnLamMoi.Text = "&Làm mới";
            this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
            // 
            // btnXem
            // 
            this.btnXem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXem.Appearance.Options.UseFont = true;
            this.btnXem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnXem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXem.Location = new System.Drawing.Point(58, 38);
            this.btnXem.Margin = new System.Windows.Forms.Padding(4);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(80, 35);
            this.btnXem.TabIndex = 128;
            this.btnXem.Text = "&Xem";
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // lblThang
            // 
            this.lblThang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThang.Location = new System.Drawing.Point(6, 8);
            this.lblThang.Margin = new System.Windows.Forms.Padding(4);
            this.lblThang.Name = "lblThang";
            this.lblThang.Size = new System.Drawing.Size(45, 19);
            this.lblThang.TabIndex = 57;
            this.lblThang.Text = "Tháng";
            // 
            // lblLoai
            // 
            this.lblLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoai.Location = new System.Drawing.Point(295, 8);
            this.lblLoai.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoai.Name = "lblLoai";
            this.lblLoai.Size = new System.Drawing.Size(33, 19);
            this.lblLoai.TabIndex = 59;
            this.lblLoai.Text = "Năm";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 84);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 567);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NHANVIEN_ID,
            this.STT,
            this.MA,
            this.NHANVIEN,
            this.THANG,
            this.NAM,
            this.SONGAYLAMVIEC,
            this.LUONGCOBAN,
            this.LUONGTHUCTE,
            this.PHUCAP,
            this.TONGTHUNHAP,
            this.NGAYCONG_TT,
            this.TONGLUONG_TT,
            this.LUONGDONG_BH,
            this.BH_XAHOI,
            this.BH_YTE,
            this.BH_THATNGHIEP,
            this.TONGBH,
            this.TONGTHUNHAP_TT,
            this.TAMUNG,
            this.THUCLINH,
            this.gridColumn1});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // NHANVIEN_ID
            // 
            this.NHANVIEN_ID.FieldName = "NHANVIEN_ID";
            this.NHANVIEN_ID.Name = "NHANVIEN_ID";
            this.NHANVIEN_ID.OptionsColumn.AllowEdit = false;
            this.NHANVIEN_ID.OptionsColumn.AllowFocus = false;
            this.NHANVIEN_ID.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // NHANVIEN
            // 
            this.NHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.NHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIEN.Caption = "Nhân viên";
            this.NHANVIEN.FieldName = "NHANVIEN";
            this.NHANVIEN.Name = "NHANVIEN";
            this.NHANVIEN.OptionsColumn.AllowEdit = false;
            this.NHANVIEN.OptionsColumn.AllowFocus = false;
            this.NHANVIEN.OptionsColumn.FixedWidth = true;
            this.NHANVIEN.Visible = true;
            this.NHANVIEN.VisibleIndex = 2;
            this.NHANVIEN.Width = 250;
            // 
            // THANG
            // 
            this.THANG.AppearanceCell.Options.UseTextOptions = true;
            this.THANG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANG.AppearanceHeader.Options.UseFont = true;
            this.THANG.AppearanceHeader.Options.UseTextOptions = true;
            this.THANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANG.Caption = "Tháng";
            this.THANG.DisplayFormat.FormatString = "00";
            this.THANG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THANG.FieldName = "THANG";
            this.THANG.Name = "THANG";
            this.THANG.OptionsColumn.AllowEdit = false;
            this.THANG.OptionsColumn.AllowFocus = false;
            this.THANG.OptionsColumn.FixedWidth = true;
            this.THANG.Visible = true;
            this.THANG.VisibleIndex = 3;
            this.THANG.Width = 80;
            // 
            // NAM
            // 
            this.NAM.AppearanceCell.Options.UseTextOptions = true;
            this.NAM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAM.AppearanceHeader.Options.UseFont = true;
            this.NAM.AppearanceHeader.Options.UseTextOptions = true;
            this.NAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NAM.Caption = "Năm";
            this.NAM.FieldName = "NAM";
            this.NAM.Name = "NAM";
            this.NAM.OptionsColumn.AllowEdit = false;
            this.NAM.OptionsColumn.AllowFocus = false;
            this.NAM.OptionsColumn.FixedWidth = true;
            this.NAM.Visible = true;
            this.NAM.VisibleIndex = 4;
            this.NAM.Width = 80;
            // 
            // SONGAYLAMVIEC
            // 
            this.SONGAYLAMVIEC.AppearanceCell.Options.UseTextOptions = true;
            this.SONGAYLAMVIEC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SONGAYLAMVIEC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SONGAYLAMVIEC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SONGAYLAMVIEC.AppearanceHeader.Options.UseFont = true;
            this.SONGAYLAMVIEC.AppearanceHeader.Options.UseTextOptions = true;
            this.SONGAYLAMVIEC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SONGAYLAMVIEC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SONGAYLAMVIEC.Caption = "Ngày làm việc";
            this.SONGAYLAMVIEC.FieldName = "SONGAYLAMVIEC";
            this.SONGAYLAMVIEC.Name = "SONGAYLAMVIEC";
            this.SONGAYLAMVIEC.OptionsColumn.AllowEdit = false;
            this.SONGAYLAMVIEC.OptionsColumn.AllowFocus = false;
            this.SONGAYLAMVIEC.OptionsColumn.FixedWidth = true;
            this.SONGAYLAMVIEC.Visible = true;
            this.SONGAYLAMVIEC.VisibleIndex = 5;
            this.SONGAYLAMVIEC.Width = 120;
            // 
            // LUONGCOBAN
            // 
            this.LUONGCOBAN.AppearanceCell.Options.UseTextOptions = true;
            this.LUONGCOBAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LUONGCOBAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGCOBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LUONGCOBAN.AppearanceHeader.Options.UseFont = true;
            this.LUONGCOBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.LUONGCOBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LUONGCOBAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGCOBAN.Caption = "Lương cơ bản";
            this.LUONGCOBAN.DisplayFormat.FormatString = "N0";
            this.LUONGCOBAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LUONGCOBAN.FieldName = "LUONGCOBAN";
            this.LUONGCOBAN.Name = "LUONGCOBAN";
            this.LUONGCOBAN.OptionsColumn.AllowEdit = false;
            this.LUONGCOBAN.OptionsColumn.AllowFocus = false;
            this.LUONGCOBAN.OptionsColumn.FixedWidth = true;
            this.LUONGCOBAN.Visible = true;
            this.LUONGCOBAN.VisibleIndex = 6;
            this.LUONGCOBAN.Width = 150;
            // 
            // LUONGTHUCTE
            // 
            this.LUONGTHUCTE.AppearanceCell.Options.UseTextOptions = true;
            this.LUONGTHUCTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LUONGTHUCTE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGTHUCTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LUONGTHUCTE.AppearanceHeader.Options.UseFont = true;
            this.LUONGTHUCTE.AppearanceHeader.Options.UseTextOptions = true;
            this.LUONGTHUCTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LUONGTHUCTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGTHUCTE.Caption = "Lương thực tế";
            this.LUONGTHUCTE.DisplayFormat.FormatString = "N0";
            this.LUONGTHUCTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LUONGTHUCTE.FieldName = "LUONGTHUCTE";
            this.LUONGTHUCTE.Name = "LUONGTHUCTE";
            this.LUONGTHUCTE.OptionsColumn.AllowEdit = false;
            this.LUONGTHUCTE.OptionsColumn.AllowFocus = false;
            this.LUONGTHUCTE.OptionsColumn.FixedWidth = true;
            this.LUONGTHUCTE.Visible = true;
            this.LUONGTHUCTE.VisibleIndex = 7;
            this.LUONGTHUCTE.Width = 150;
            // 
            // PHUCAP
            // 
            this.PHUCAP.AppearanceCell.Options.UseTextOptions = true;
            this.PHUCAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PHUCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PHUCAP.AppearanceHeader.Options.UseFont = true;
            this.PHUCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUCAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUCAP.Caption = "Phụ cấp";
            this.PHUCAP.DisplayFormat.FormatString = "N0";
            this.PHUCAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PHUCAP.FieldName = "PHUCAP";
            this.PHUCAP.Name = "PHUCAP";
            this.PHUCAP.OptionsColumn.AllowEdit = false;
            this.PHUCAP.OptionsColumn.AllowFocus = false;
            this.PHUCAP.OptionsColumn.FixedWidth = true;
            this.PHUCAP.Visible = true;
            this.PHUCAP.VisibleIndex = 8;
            this.PHUCAP.Width = 150;
            // 
            // TONGTHUNHAP
            // 
            this.TONGTHUNHAP.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTHUNHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTHUNHAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTHUNHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTHUNHAP.AppearanceHeader.Options.UseFont = true;
            this.TONGTHUNHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTHUNHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTHUNHAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTHUNHAP.Caption = "Tổng thu nhập";
            this.TONGTHUNHAP.DisplayFormat.FormatString = "N0";
            this.TONGTHUNHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTHUNHAP.FieldName = "TONGTHUNHAP";
            this.TONGTHUNHAP.Name = "TONGTHUNHAP";
            this.TONGTHUNHAP.OptionsColumn.AllowEdit = false;
            this.TONGTHUNHAP.OptionsColumn.AllowFocus = false;
            this.TONGTHUNHAP.OptionsColumn.FixedWidth = true;
            this.TONGTHUNHAP.Visible = true;
            this.TONGTHUNHAP.VisibleIndex = 9;
            this.TONGTHUNHAP.Width = 170;
            // 
            // NGAYCONG_TT
            // 
            this.NGAYCONG_TT.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCONG_TT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NGAYCONG_TT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCONG_TT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYCONG_TT.AppearanceHeader.Options.UseFont = true;
            this.NGAYCONG_TT.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCONG_TT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCONG_TT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCONG_TT.Caption = "Ngày công";
            this.NGAYCONG_TT.FieldName = "NGAYCONG_TT";
            this.NGAYCONG_TT.Name = "NGAYCONG_TT";
            this.NGAYCONG_TT.OptionsColumn.AllowEdit = false;
            this.NGAYCONG_TT.OptionsColumn.AllowFocus = false;
            this.NGAYCONG_TT.OptionsColumn.FixedWidth = true;
            this.NGAYCONG_TT.Visible = true;
            this.NGAYCONG_TT.VisibleIndex = 10;
            this.NGAYCONG_TT.Width = 100;
            // 
            // TONGLUONG_TT
            // 
            this.TONGLUONG_TT.AppearanceCell.Options.UseTextOptions = true;
            this.TONGLUONG_TT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGLUONG_TT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGLUONG_TT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGLUONG_TT.AppearanceHeader.Options.UseFont = true;
            this.TONGLUONG_TT.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGLUONG_TT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGLUONG_TT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGLUONG_TT.Caption = "Tổng lương TT";
            this.TONGLUONG_TT.DisplayFormat.FormatString = "N0";
            this.TONGLUONG_TT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGLUONG_TT.FieldName = "TONGLUONG_TT";
            this.TONGLUONG_TT.Name = "TONGLUONG_TT";
            this.TONGLUONG_TT.OptionsColumn.AllowEdit = false;
            this.TONGLUONG_TT.OptionsColumn.AllowFocus = false;
            this.TONGLUONG_TT.OptionsColumn.FixedWidth = true;
            this.TONGLUONG_TT.Visible = true;
            this.TONGLUONG_TT.VisibleIndex = 11;
            this.TONGLUONG_TT.Width = 150;
            // 
            // LUONGDONG_BH
            // 
            this.LUONGDONG_BH.AppearanceCell.Options.UseTextOptions = true;
            this.LUONGDONG_BH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LUONGDONG_BH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGDONG_BH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LUONGDONG_BH.AppearanceHeader.Options.UseFont = true;
            this.LUONGDONG_BH.AppearanceHeader.Options.UseTextOptions = true;
            this.LUONGDONG_BH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LUONGDONG_BH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LUONGDONG_BH.Caption = "Lương đóng BH";
            this.LUONGDONG_BH.DisplayFormat.FormatString = "N0";
            this.LUONGDONG_BH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LUONGDONG_BH.FieldName = "LUONGDONG_BH";
            this.LUONGDONG_BH.Name = "LUONGDONG_BH";
            this.LUONGDONG_BH.OptionsColumn.AllowEdit = false;
            this.LUONGDONG_BH.OptionsColumn.AllowFocus = false;
            this.LUONGDONG_BH.OptionsColumn.FixedWidth = true;
            this.LUONGDONG_BH.Visible = true;
            this.LUONGDONG_BH.VisibleIndex = 12;
            this.LUONGDONG_BH.Width = 150;
            // 
            // BH_XAHOI
            // 
            this.BH_XAHOI.AppearanceCell.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_XAHOI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_XAHOI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_XAHOI.AppearanceHeader.Options.UseFont = true;
            this.BH_XAHOI.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_XAHOI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_XAHOI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_XAHOI.Caption = "BH Xã hội";
            this.BH_XAHOI.DisplayFormat.FormatString = "N0";
            this.BH_XAHOI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.BH_XAHOI.FieldName = "BH_XAHOI";
            this.BH_XAHOI.Name = "BH_XAHOI";
            this.BH_XAHOI.OptionsColumn.AllowEdit = false;
            this.BH_XAHOI.OptionsColumn.AllowFocus = false;
            this.BH_XAHOI.OptionsColumn.FixedWidth = true;
            this.BH_XAHOI.Visible = true;
            this.BH_XAHOI.VisibleIndex = 13;
            this.BH_XAHOI.Width = 150;
            // 
            // BH_YTE
            // 
            this.BH_YTE.AppearanceCell.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_YTE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_YTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_YTE.AppearanceHeader.Options.UseFont = true;
            this.BH_YTE.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_YTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_YTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_YTE.Caption = "BH Y tế";
            this.BH_YTE.DisplayFormat.FormatString = "N0";
            this.BH_YTE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.BH_YTE.FieldName = "BH_YTE";
            this.BH_YTE.Name = "BH_YTE";
            this.BH_YTE.OptionsColumn.AllowEdit = false;
            this.BH_YTE.OptionsColumn.AllowFocus = false;
            this.BH_YTE.OptionsColumn.FixedWidth = true;
            this.BH_YTE.Visible = true;
            this.BH_YTE.VisibleIndex = 14;
            this.BH_YTE.Width = 150;
            // 
            // BH_THATNGHIEP
            // 
            this.BH_THATNGHIEP.AppearanceCell.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BH_THATNGHIEP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseFont = true;
            this.BH_THATNGHIEP.AppearanceHeader.Options.UseTextOptions = true;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BH_THATNGHIEP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BH_THATNGHIEP.Caption = "BH Thất nghiệp";
            this.BH_THATNGHIEP.DisplayFormat.FormatString = "N0";
            this.BH_THATNGHIEP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.BH_THATNGHIEP.FieldName = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.Name = "BH_THATNGHIEP";
            this.BH_THATNGHIEP.OptionsColumn.AllowEdit = false;
            this.BH_THATNGHIEP.OptionsColumn.AllowFocus = false;
            this.BH_THATNGHIEP.OptionsColumn.FixedWidth = true;
            this.BH_THATNGHIEP.Visible = true;
            this.BH_THATNGHIEP.VisibleIndex = 15;
            this.BH_THATNGHIEP.Width = 150;
            // 
            // TONGBH
            // 
            this.TONGBH.AppearanceCell.Options.UseTextOptions = true;
            this.TONGBH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGBH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGBH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGBH.AppearanceHeader.Options.UseFont = true;
            this.TONGBH.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGBH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGBH.Caption = "Tổng BH";
            this.TONGBH.DisplayFormat.FormatString = "N0";
            this.TONGBH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGBH.FieldName = "TONGBH";
            this.TONGBH.Name = "TONGBH";
            this.TONGBH.OptionsColumn.AllowEdit = false;
            this.TONGBH.OptionsColumn.AllowFocus = false;
            this.TONGBH.OptionsColumn.FixedWidth = true;
            this.TONGBH.Visible = true;
            this.TONGBH.VisibleIndex = 16;
            this.TONGBH.Width = 150;
            // 
            // TONGTHUNHAP_TT
            // 
            this.TONGTHUNHAP_TT.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTHUNHAP_TT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTHUNHAP_TT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTHUNHAP_TT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTHUNHAP_TT.AppearanceHeader.Options.UseFont = true;
            this.TONGTHUNHAP_TT.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTHUNHAP_TT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTHUNHAP_TT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTHUNHAP_TT.Caption = "Tổng thu nhập TT";
            this.TONGTHUNHAP_TT.DisplayFormat.FormatString = "N0";
            this.TONGTHUNHAP_TT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTHUNHAP_TT.FieldName = "TONGTHUNHAP_TT";
            this.TONGTHUNHAP_TT.Name = "TONGTHUNHAP_TT";
            this.TONGTHUNHAP_TT.OptionsColumn.AllowEdit = false;
            this.TONGTHUNHAP_TT.OptionsColumn.AllowFocus = false;
            this.TONGTHUNHAP_TT.OptionsColumn.FixedWidth = true;
            this.TONGTHUNHAP_TT.Visible = true;
            this.TONGTHUNHAP_TT.VisibleIndex = 17;
            this.TONGTHUNHAP_TT.Width = 170;
            // 
            // TAMUNG
            // 
            this.TAMUNG.AppearanceCell.Options.UseTextOptions = true;
            this.TAMUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TAMUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TAMUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TAMUNG.AppearanceHeader.Options.UseFont = true;
            this.TAMUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.TAMUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TAMUNG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TAMUNG.Caption = "Tạm ứng";
            this.TAMUNG.DisplayFormat.FormatString = "N0";
            this.TAMUNG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TAMUNG.FieldName = "TAMUNG";
            this.TAMUNG.Name = "TAMUNG";
            this.TAMUNG.OptionsColumn.AllowEdit = false;
            this.TAMUNG.OptionsColumn.AllowFocus = false;
            this.TAMUNG.OptionsColumn.FixedWidth = true;
            this.TAMUNG.Visible = true;
            this.TAMUNG.VisibleIndex = 18;
            this.TAMUNG.Width = 150;
            // 
            // THUCLINH
            // 
            this.THUCLINH.AppearanceCell.Options.UseTextOptions = true;
            this.THUCLINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THUCLINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUCLINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUCLINH.AppearanceHeader.Options.UseFont = true;
            this.THUCLINH.AppearanceHeader.Options.UseTextOptions = true;
            this.THUCLINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUCLINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUCLINH.Caption = "Thực lĩnh";
            this.THUCLINH.DisplayFormat.FormatString = "N0";
            this.THUCLINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THUCLINH.FieldName = "THUCLINH";
            this.THUCLINH.Name = "THUCLINH";
            this.THUCLINH.OptionsColumn.AllowEdit = false;
            this.THUCLINH.OptionsColumn.AllowFocus = false;
            this.THUCLINH.OptionsColumn.FixedWidth = true;
            this.THUCLINH.Visible = true;
            this.THUCLINH.VisibleIndex = 19;
            this.THUCLINH.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 20;
            this.gridColumn1.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 84);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 567);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 403);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 83);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 243);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 323);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 485);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_LuongThang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_LuongThang";
            this.Text = "Lương tháng";
            this.Load += new System.EventHandler(this.Frm_LuongThang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn THANG;
        private DevExpress.XtraGrid.Columns.GridColumn NAM;
        private DevExpress.XtraGrid.Columns.GridColumn SONGAYLAMVIEC;
        private DevExpress.XtraGrid.Columns.GridColumn LUONGCOBAN;
        private DevExpress.XtraGrid.Columns.GridColumn PHUCAP;
        private DevExpress.XtraGrid.Columns.GridColumn LUONGTHUCTE;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTHUNHAP;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIEN;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCONG_TT;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.LabelControl lblThang;
        private DevExpress.XtraEditors.LabelControl lblLoai;
        private DevExpress.XtraGrid.Columns.GridColumn TONGLUONG_TT;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIEN_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton btnLamMoi;
        private DevExpress.XtraEditors.SimpleButton btnXem;
        private DevExpress.XtraEditors.LookUpEdit cboNam;
        private DevExpress.XtraEditors.LookUpEdit cboThang;
        private DevExpress.XtraGrid.Columns.GridColumn LUONGDONG_BH;
        private DevExpress.XtraGrid.Columns.GridColumn BH_XAHOI;
        private DevExpress.XtraGrid.Columns.GridColumn BH_YTE;
        private DevExpress.XtraGrid.Columns.GridColumn BH_THATNGHIEP;
        private DevExpress.XtraGrid.Columns.GridColumn TONGBH;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTHUNHAP_TT;
        private DevExpress.XtraGrid.Columns.GridColumn TAMUNG;
        private DevExpress.XtraGrid.Columns.GridColumn THUCLINH;




    }
}