﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Cauhinh_Phieuchi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinh_Phieuchi()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='CAUHINHPHIEUCHI'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                opt.Checked = dr["GIATRI"].ToString() == "0" ? true : false;
                opt1.Checked = !opt.Checked;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='CAUHINHPHIEUCHI'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("CAUHINHPHIEUCHI") + ",";
            sSQL += clsMain.SQLBit(opt1.Checked) + ",";
            sSQL += clsMain.SQLStringUnicode("Cấu hình phiếu chi. 1 - phiếu chi phải dc duyệt. 0-Dùng mặc định") + ")" +"\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}