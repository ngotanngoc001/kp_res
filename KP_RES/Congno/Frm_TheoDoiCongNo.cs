﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;
using DevExpress.XtraEditors.Repository;

namespace KP_RES
{
    public partial class Frm_TheoDoiCongNo : DevExpress.XtraEditors.XtraForm
    {
        bool flag = true;//luoi 1
        public Frm_TheoDoiCongNo()
        {
            InitializeComponent();
           // LoadData();
            RepositoryItemMemoEdit memo = new RepositoryItemMemoEdit();
            memo.AutoHeight = true;
            gridView1.Columns[10].ColumnEdit = memo;
            gridView2.Columns[10].ColumnEdit = memo;
        }

        private void LoadData(DateTime date_TuNgay, DateTime date_DenNgay)//load dữ liệu thu tiền theo ngày
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();//lấy dữ liệu phiếu chi
                string sqlThu = string.Format("select (case when MA_HOADON is null or MA_HOADON ='' then CONVERT(VARCHAR,pk_PHIEUKHOID)  else MA_HOADON end) as MAPHIEU from CHITIETTHANHTOAN where THU_CHI='true' and FLAG='true' and CONVERT(VARCHAR,NGAYHENTRA,112) BETWEEN '{0:yyyyMMdd}' AND '{1:yyyyMMdd}'", date_TuNgay, date_DenNgay);//lấy tất cả phiếu xuất kho đúng điều kiện ngày thu
                dt = clsMain.ReturnDataTable(sqlThu);
                string sqlChi = string.Format("select (case when MA_HOADON is null or MA_HOADON ='' then CONVERT(VARCHAR,pk_PHIEUKHOID)  else MA_HOADON end) as MAPHIEU from CHITIETTHANHTOAN where THU_CHI='false' and FLAG='true' and CONVERT(VARCHAR,NGAYHENTRA,112) BETWEEN '{0:yyyyMMdd}' AND '{1:yyyyMMdd}'", date_TuNgay, date_DenNgay);//lấy tất cả phiếu nhập kho đúng điều kiện ngày Chi
                dt1 = clsMain.ReturnDataTable(sqlChi);

                DataTable dtThu = new DataTable();//bảng dữ liệu lưới thu
                DataTable dtChi = new DataTable();//bảng dữ liệu lưới chi
                string sql = "";
                if (dt.Rows.Count > 0)//nếu thu  có dữ liệu
                {
                    for (int i = 0; i < dt.Rows.Count; i++)// duyệt qua từng phiếu
                    {
                        if (dt.Rows[i][0].ToString().Contains("B"))
                        {
                            sql = "select ct.MA_HOADON  as MAPHIEU,dt.TEN as HOTENKH,hd.NGAYTAO as NgayGiaoDich,ct.TONGTIENCANTT as TongTien,ct.TONGTIENCANTT as TongNo from CHITIETTHANHTOAN ct inner join HOADON hd on hd.Ma_HoaDon=ct.MA_HOADON inner join NHACUNGCAP dt on dt.MA=hd.MA_KHACHHANG where  DOTTHANHTOAN='0' and ct.MA_HOADON='" + dt.Rows[i][0].ToString() + "'";// lấy thông tin của 1 phiếu xuất kho
                        }
                        else
                        {
                            sql = "select pk.SOPHIEU as MAPHIEU,dt.TEN as HOTENKH,pk.NGAYTAO as NgayGiaoDich,ct.TONGTIENCANTT as TongTien,ct.TONGTIENCANTT as TongNo from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on pk.MA=ct.PK_PHIEUKHOID inner join NHACUNGCAP dt on dt.MA=pk.NHACUNGCAP where  DOTTHANHTOAN='0' and ct.PK_PHIEUKHOID='" + dt.Rows[i][0].ToString() + "'";// lấy thông tin của 1 phiếu xuất kho
                        }

                        DataTable dtThu1 = clsMain.ReturnDataTable(sql);//gắn vào 1 bản tạm
                        dtThu1.Columns.Add("DaThanhToan", typeof(decimal));
                        dtThu1.Columns.Add("NoConLai", typeof(decimal));
                        dtThu1.Columns.Add("GhiChu", typeof(string));
                        dtThu1.Columns.Add("NGAYHENTRA", typeof(DateTime));
                        string sqlConLai = "";
                        string ngay = "";
                        if (dt.Rows[i][0].ToString().Contains("B"))
                        {
                            sqlConLai = "select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where MA_HOADON='" + dt.Rows[i][0].ToString() + "' order by CTTT_STT desc";//tính số nơ còn lại phải thu
                            dtThu1.Rows[0]["NGAYHENTRA"] = clsMain.ReturnDataTable("select NGAYHENTRA from CHITIETTHANHTOAN where THU_CHI='true' and FLAG='true' and MA_HOADON='" + dt.Rows[i][0].ToString() + "'").Rows[0][0];
                            ngay = "select NGAYTREHEN from CHITIETTHANHTOAN where MA_HOADON='" + dt.Rows[i][0].ToString() + "' and ISTREHEN='true' order by NGAYTREHEN";
                        }
                        else
                        {
                            sqlConLai = "select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dt.Rows[i][0].ToString() + "' order by CTTT_STT desc";//tính số nơ còn lại phải thu
                            dtThu1.Rows[0]["NGAYHENTRA"] = clsMain.ReturnDataTable("select NGAYHENTRA from CHITIETTHANHTOAN where THU_CHI='true' and FLAG='true' and PK_PHIEUKHOID='" + dt.Rows[i][0].ToString() + "'").Rows[0][0];
                            ngay = "select NGAYTREHEN from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dt.Rows[i][0].ToString() + "' and ISTREHEN='true' order by NGAYTREHEN";
                        }

                        dtThu1.Rows[0]["NoConLai"] = decimal.Parse(clsMain.ReturnDataTable(sqlConLai).Rows[0][0].ToString());
                        dtThu1.Rows[0]["DaThanhToan"] = (decimal.Parse(dtThu1.Rows[0]["TongNo"].ToString()) - decimal.Parse(dtThu1.Rows[0]["NoConLai"].ToString()));





                        DataTable dtngay = clsMain.ReturnDataTable(ngay);//lấy tất cả các ngày hẹn trả trễ hẹn
                        if (dtngay.Rows.Count > 0)//nếu có ngày trễ hẹn
                        {
                            for (int j = 0; j < dtngay.Rows.Count; j++)
                            {
                                dtThu1.Rows[0]["GhiChu"] = "Trễ Hẹn Lần " + (j + 1) + " vào Ngày " + string.Format("{0:dd/MM/yyyy}", dtngay.Rows[j][0]) + "\r\n";
                            }
                        }
                        //}
                        if (i == 0)
                        {
                            dtThu = dtThu1;
                        }
                        else
                        {
                            dtThu.Merge(dtThu1);
                        }
                    }
                    gv_DanhSachThu.DataSource = dtThu;
                }
                //chi ncc
                if (dt1.Rows.Count > 0)//nếu có dữ liệu
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)// duyệt qua từng phiếu
                    {
                        if (dt1.Rows[i][0].ToString().Contains("B"))
                        {
                            sqlChi = "select ct.MA_HOADON  as MAPHIEU,dt.TEN as HOTENKH,hd.NGAYTAO as NgayGiaoDich,ct.TONGTIENCANTT as TongTien,ct.TONGTIENCANTT as TongNo from CHITIETTHANHTOAN ct inner join HOADON hd on hd.Ma_HoaDon=ct.MA_HOADON inner join NHACUNGCAP dt on dt.MA=hd.MA_KHACHHANG where  DOTTHANHTOAN='0 and ct.MA_HOADON='" + dt1.Rows[i][0].ToString() + "'";// lấy thông tin của 1 phiếu Nhập kho
                        }
                        else
                        {
                            sqlChi = "select pk.MA as MAPHIEU,dt.TEN as HOTENKH,pk.NGAYTAO as NgayGiaoDich,ct.TONGTIENCANTT as TongTien,ct.TONGTIENCANTT as TongNo from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on pk.MA=ct.PK_PHIEUKHOID inner join NHACUNGCAP dt on dt.MA=pk.NHACUNGCAP where  DOTTHANHTOAN='0' and ct.PK_PHIEUKHOID='" + dt1.Rows[i][0].ToString() + "'";// lấy thông tin của 1 phiếu Nhập kho
                        }

                        DataTable dtChi1 = clsMain.ReturnDataTable(sqlChi);//gắn vào 1 bản tạm
                        dtChi1.Columns.Add("DaThanhToan", typeof(decimal));
                        dtChi1.Columns.Add("NoConLai", typeof(decimal));
                        dtChi1.Columns.Add("GhiChu", typeof(string));
                        dtChi1.Columns.Add("NGAYHENTRA", typeof(DateTime));

                        string sqlConLai = "";
                        string ngay = "";

                        if (dt1.Rows[i][0].ToString().Contains("B"))
                        {
                            sqlConLai = "select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where MA_HOADON='" + dt1.Rows[i][0].ToString() + "' order by CTTT_STT desc";//tính số nơ còn lại phải thu
                            dtChi1.Rows[0]["NGAYHENTRA"] = clsMain.ReturnDataTable("select NGAYHENTRA from CHITIETTHANHTOAN where THU_CHI='false' and FLAG='true' and MA_HOADON='" + dt1.Rows[i][0].ToString() + "'").Rows[0][0];
                            ngay = "select NGAYTREHEN from CHITIETTHANHTOAN where MA_HOADON='" + dt.Rows[i][0].ToString() + "' and ISTREHEN='true' order by NGAYTREHEN";
                        }
                        else
                        {
                            sqlConLai = "select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dt1.Rows[i][0].ToString() + "' order by CTTT_STT desc";//tính số nơ còn lại phải thu
                            dtChi1.Rows[0]["NGAYHENTRA"] = clsMain.ReturnDataTable("select NGAYHENTRA from CHITIETTHANHTOAN where THU_CHI='false' and FLAG='true' and PK_PHIEUKHOID='" + dt1.Rows[i][0].ToString() + "'").Rows[0][0];
                            ngay = "select NGAYTREHEN from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dt1.Rows[i][0].ToString() + "' and ISTREHEN='true' order by NGAYTREHEN";
                        }


                        dtChi1.Rows[0]["NoConLai"] = decimal.Parse(clsMain.ReturnDataTable(sqlConLai).Rows[0][0].ToString());
                        dtChi1.Rows[0]["DaThanhToan"] = (decimal.Parse(dtChi1.Rows[0]["TongNo"].ToString()) - decimal.Parse(dtChi1.Rows[0]["NoConLai"].ToString()));

                        dtChi1.Rows[0]["NGAYHENTRA"] = clsMain.ReturnDataTable("select NGAYHENTRA from CHITIETTHANHTOAN where THU_CHI='false' and FLAG='true' and PK_PHIEUKHOID='" + dt1.Rows[i][0].ToString() + "'").Rows[0][0];


                        DataTable dtngay = clsMain.ReturnDataTable(ngay);
                        if (dtngay.Rows.Count > 0)//nếu có ngày trễ hẹn
                        {
                            for (int j = 0; j < dtngay.Rows.Count; j++)
                            {
                                dtChi1.Rows[0]["GhiChu"] = "Trễ Hẹn Lần " + (j + 1) + " vào Ngày " + string.Format("{0:dd/MM/yyyy}", dtngay.Rows[j][0]) + "\r\n";
                            }
                        }
                        //}
                        if (i == 0)
                        {
                            dtChi = dtChi1;
                        }
                        else
                        {
                            dtChi.Merge(dtChi1);
                        }
                    }
                    gvDanhSachChi.DataSource = dtChi;
                }
                if (dt.Rows.Count > 0 && dt1.Rows.Count > 0)
                {
                    gc_DanhSachThu.Visible = gcDanhSachChi.Visible = true;
                    gcDanhSachChi.Dock = DockStyle.Bottom;
                }
                else
                    if (dt.Rows.Count > 0)
                    {
                        gc_DanhSachThu.Visible = true;
                        gcDanhSachChi.Visible = false;
                    }
                    else
                        if (dt1.Rows.Count > 0)
                        {
                            gc_DanhSachThu.Visible = false;
                            gcDanhSachChi.Visible = true;
                            gcDanhSachChi.Dock = DockStyle.Fill;
                        }
                        else
                        {
                            gv_DanhSachThu.DataSource = null;
                            gvDanhSachChi.DataSource = null;
                            gc_DanhSachThu.Visible = gcDanhSachChi.Visible = true;
                            gcDanhSachChi.Dock = DockStyle.Bottom;
                            throw new Exception("Không có công nợ thu chi nào! ");
                        }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
                

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            if (optGroup.SelectedIndex == 0)
            {
                DateTime tungay = Convert.ToDateTime("01/01/1990");
                DateTime denngay = Convert.ToDateTime("01/01/2990");
                LoadData(tungay, denngay);
            }
            else
                if (optGroup.SelectedIndex == 1)
                {
                    LoadData(date_tungay.DateTime, date_denngay.DateTime);
                }
        }

        
        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!flag)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!flag)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!flag)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!flag)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0 && this.gridView2.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (this.gridView1.RowCount > 0)
                {
                    STT.Visible = false;

                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                        if (saveDialog.ShowDialog() != DialogResult.Cancel)
                        {
                            string exportFilePath = saveDialog.FileName;
                            string fileExtenstion = new FileInfo(exportFilePath).Extension;
                            switch (fileExtenstion)
                            {
                                case ".xls":
                                    gvDanhSachChi.ExportToXls(exportFilePath);
                                    
                                    break;
                                case ".xlsx":
                                    gvDanhSachChi.ExportToXlsx(exportFilePath);
                                   // gv_DanhSachThu.ExportToXls(exportFilePath);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    STT.Visible = true;
                }
                if (this.gridView2.RowCount > 0)
                {
                    STTKH.Visible = false;

                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                        if (saveDialog.ShowDialog() != DialogResult.Cancel)
                        {
                            string exportFilePath = saveDialog.FileName;
                            string fileExtenstion = new FileInfo(exportFilePath).Extension;
                            switch (fileExtenstion)
                            {
                                case ".xls":
                                    //gvDanhSachChi.ExportToXls(exportFilePath);
                                    gv_DanhSachThu.ExportToXls(exportFilePath);
                                    break;
                                case ".xlsx":
                                    //gvDanhSachChi.ExportToXls(exportFilePath);
                                    gv_DanhSachThu.ExportToXlsx(exportFilePath);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    STTKH.Visible = true;
                }
                
               
                //FILL.Visible = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
          
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gv_DanhSachThu.DataSource = null;
            gvDanhSachChi.DataSource = null;
            if (optGroup.SelectedIndex == 0)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }
            if (optGroup.SelectedIndex == 0)
            {
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
            }
            else
                if (optGroup.SelectedIndex == 1)
                {
                    date_tungay.EditValue = date_denngay.EditValue = clsMain.GetServerDate();
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTKH && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gv_DanhSachThu_Click(object sender, EventArgs e)
        {
            flag = true;
        }

        private void gvDanhSachChi_Click(object sender, EventArgs e)
        {
            flag = false;
        }

     
       

        
    
    }
}