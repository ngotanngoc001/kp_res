﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_LapPhieuThu : DevExpress.XtraEditors.XtraForm
    {
        int close = 0;
        bool checkNgay = false;
        public Frm_LapPhieuThu()
        {
            InitializeComponent();
            gl_nguon.EditValue = null;
            LoadData();
            gl_DoiTuong.Focus();
            panel4.Visible = false;
        }

        public Frm_LapPhieuThu(string maNCC, string maPhieuNhap, bool flag)
        {
            InitializeComponent();
            gl_nguon.EditValue = null;
            LoadData();
            if (flag)
            {
                gl_DoiTuong.EditValue = maNCC;
                gl_MaPhieuXuat.EditValue = maPhieuNhap;
            }
            gl_DoiTuong.Focus();
            panel4.Visible = true;
            title.Text = this.Text;
            close = 1;
        }

        private void LoadData()
        {
            string sqlcheck = "select * from CN_LOAI_CONGNO";
            DataTable dtcheck = clsMain.ReturnDataTable(sqlcheck);
            if (dtcheck.Rows.Count == 0)
            {
                string sqlInsert = "insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('01',N'Thu Khách Hàng',N'thu từ nguồn khách hang','01') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('02',N'Thu Khác',N'thu từ nguồn khác','01')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('03',N'Chi Nhà Cung Cấp',N'Chi cho nhà cung cấp','02')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('04',N'Chi Nhân Viên',N'Chi cho nhân viên','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('05',N'Chi Khác',N'Chi cho các dịch vụ khác','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('06',N'Chi Trả Hàng',N'Chi tiền trả hàng','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('07',N'Chi Khách Hàng',N'Chi tiền cho khách hàng','02') ";
                clsMain.ExecuteSQL(sqlInsert);
            }

            DataTable dt = clsMain.ReturnDataTable("select * from CN_LOAI_CONGNO where LoaiThuChi='01' AND SUDUNG=1");
            gl_nguon.Properties.DataSource = dt;
            gl_nguon.EditValue = dt.Rows[0]["MaLoai"].ToString();

            txtMaPHC.Text = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_NS()").Rows[0][0].ToString();
           
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;
               
            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;
               
            }
            dateTMNgayChi.EditValue = dateCKChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
        }

        private void Reset()
        {
            txt_TMTen.Text = txt_TenKH.Text = txtDiaChi.Text = txtLydo.Text = txtKemTheo.Text = txtTienChu.Text = string.Empty;
            txtNo.Text = txtSoTien.Text = txtTongNo.Text = txtTienTra.Text = string.Empty;
            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
            dateNgayHenTra.EditValue = null;
            txtCKTenNganHang.Text = txtSoTaiKhoanChuyen.Text = txtTenNHNhan.Text = txtSoTaiKhoanNhan.Text = txtSoGiaoDich.Text = string.Empty;
            dateCKChi.EditValue = dateTMNgayChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
        }

        private void btn_HuyBo_Click(object sender, EventArgs e)
        {
            Reset();
            //gl_DoiTuong.EditValue = null;
            //gl_MaPhieuXuat.EditValue = null;
            //DateTime ngayhentra=clsMain.GetServerDate().AddDays(3);
            ////NgocSang.CongNoKhachHangHoaDon("48", "217112014B0002", 272000, 72000, ngayhentra);
            ////NgocSang.TruCongNoKhachHangHoaDon("2129102014B0001", 100000);
            
            ////NgocSang.CongCongNoNhaCungCap("25","199",205000,105000,ngayhentra,true);
            ////NgocSang.CongCongNoNhaCungCap("25", "199", 205000, 205000, ngayhentra, true);
            ////NgocSang.CongNoKhachHangPhieuXuat("53", "207", 250000, 40000, ngayhentra, true);
            ////NgocSang.TruCongNoKhachHangPhieuXuat("200", 35000);
            //NgocSang.ThanhToanNoiBo("155", 200000);
        }

        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (gl_nguon.EditValue.ToString() == "01")
                {
                    if (gl_DoiTuong.Text == "")
                    {
                        e.Handled = true;
                        throw new Exception("Mời bạn chọn khách hàng ");
                    }
                    if (gl_MaPhieuXuat.EditValue.ToString() == "")
                    {
                        e.Handled = true;
                        throw new Exception("Mời bạn chọn Phiếu Xuẩt");
                    }
                }
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTien_Leave(object sender, EventArgs e)
        {
            try
            {

                if (txtSoTien.Text != "")
                    txtTienChu.Text =cls_KHTT.ChuyenSo(decimal.Parse(txtSoTien.Text.Replace(",", "")).ToString()) + "đồng";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTien_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {

                if (gl_nguon.EditValue.ToString() == "01" && gl_DoiTuong.Text != "" && gl_MaPhieuXuat.EditValue.ToString() != "")
                {
                    if (txtSoTien.Text == "")
                    {
                        txtTienTra.Text = "";
                        txtTienChu.Text = "";
                    }
                    else
                    {

                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongNo.Text.Replace(",", "")))
                        {
                            txtSoTien.Focus();
                            if (e.KeyValue != 13)
                                throw new Exception("Số tiền Thu không được lớn hơn số tiền nợ là: " + txtTongNo.Text);
                        }
                        txtTienTra.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTien.Text.Replace(",", "")));

                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "Value was either too large or too small for a Decimal.")
                    XtraMessageBox.Show("Số nhập vào quá giới hạn cho phép", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void txtTienTra_EditValueChanged(object sender, EventArgs e)
        {
            if (txtTienTra.Text == "")
            {
                txtNo.Text = "";
            }
            else
            {
                txtNo.Text = string.Format("{0:#,0}", (decimal.Parse(txtTongNo.Text) - decimal.Parse(txtTienTra.Text)));
            }
        }

        private void txtSoTien_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtSoTien.Text != "")
                {
                    if (checkNgay == true && gl_nguon.EditValue.ToString() == "01")
                    {
                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) == decimal.Parse(txtTongNo.Text))
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                        }
                        else
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                        }
                    }
                    txtTienChu.Text =cls_KHTT. ChuyenSo(decimal.Parse(txtSoTien.Text.Replace(",", "")).ToString()) + " đồng";
                    if (gl_nguon.EditValue.ToString() == "01")
                    {
                        txtTienTra.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTien.Text.Replace(",", "")));
                    }
                }
                else
                {
                    txtTienChu.Text = "";
                    txtTienTra.Text = txtNo.Text = string.Empty;
                }
            }
            catch
            {
                XtraMessageBox.Show("Số Nhập vào quá giới hạn cho phép!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            if (DialogResult.No  == (XtraMessageBox.Show("Bạn có chắc muốn tạo phiếu thu này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
            {
                return;
            }
            try
            {
                if (gl_nguon.EditValue.ToString() == "01")
                {
                    if (gl_DoiTuong.Text == "")
                    {
                        gl_DoiTuong.Focus();
                        throw new Exception("Mời Chọn Khách Hàng");
                    }

                    if (gl_MaPhieuXuat.EditValue.ToString() == "")
                    {
                        gl_MaPhieuXuat.Focus();
                        throw new Exception("Mời Chọn Mã Phiếu");
                    }
                    if (txtSoTien.Text == "")
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền Thu");
                    }
                    if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongNo.Text.Replace(",", "")))
                    {
                        txtSoTien.Focus();
                        throw new Exception("Số tiền thu không được lớn hơn số tiền nợ là: " + txtTongNo.Text);
                    }
                    if (dateNgayHenTra.Visible == true)
                    {
                        if (dateNgayHenTra.EditValue == null)
                        {
                            dateNgayHenTra.Focus();
                            throw new Exception("Mời Chọn Ngày Hẹn Trả");
                        }
                        if (dateNgayHenTra.DateTime < KP_UserManagement.clsMain.GetServerDate().Date)
                        {
                            dateNgayHenTra.Focus();

                            throw new Exception("Ngày Hẹn Trả không được nhỏ hơn ngày hiện tại");

                        }
                    }
                }
                else
                {
                    if (txt_TenKH.Text == "")
                    {
                        txt_TenKH.Focus();
                        throw new Exception("Mời Nhập họ tên người nộp tiền");
                    }

                    if (txtSoTien.Text == "")
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền thu");
                    }

                    if (txtLydo.Text == "")
                    {
                        txtLydo.Focus();
                        throw new Exception("Mời Nhập lý do");
                    }
                }

                if (chkTienMat.Checked)
                {
                    if (txt_TMTen.Text == "")
                    {
                        txt_TMTen.Focus();
                        throw new Exception("Mời Nhập Tên Người Nộp Tiền");
                    }
                }

                if (chHTTT.Checked)
                {
                    if (txtSoGiaoDich.Text == "")
                    {
                        txtSoGiaoDich.Focus();
                        throw new Exception("Mời Nhập Số Giao Dịch");
                    }

                }

                decimal tongtien = 0;
               // bool httt = true;
                string sql; string sqlChiTiet = ""; string sql1 = ""; string sql2 = "";
                DataTable dt = clsMain.ReturnDataTable("select top(1) TONGTIEN from CN_CONGNO where SUDUNG='true' order by NGAYLAPPHIEU desc");
                if (dt.Rows.Count > 0)
                {
                    tongtien = decimal.Parse(dt.Rows[0][0].ToString());
                    if (chHTTT.Checked == false)
                    {
                        tongtien = tongtien + decimal.Parse(txtSoTien.Text);
                        //httt = true;
                    }
                    else
                    {
                       // httt = false;
                    }
                }
                else
                {
                    if (chHTTT.Checked == false)
                    {
                        tongtien = decimal.Parse(txtSoTien.Text);
                       // httt = true;
                    }
                    else
                    {
                       // httt = false;
                    }

                }
                string soTaiKhoanChuyen;
                if (txtSoTaiKhoanChuyen.Text == "")
                {
                    soTaiKhoanChuyen = "NULL";
                }
                else
                {
                    soTaiKhoanChuyen = txtSoTaiKhoanChuyen.Text;
                }
                string soTaiKhoanNhan;
                if (txtSoTaiKhoanNhan.Text == "")
                {
                    soTaiKhoanNhan = "NULL";
                }
                else
                {
                    soTaiKhoanNhan = txtSoTaiKhoanNhan.Text;
                }
                string soGiaoDich;
                if (txtSoGiaoDich.Text == "")
                {
                    soGiaoDich = "NULL";
                }
                else
                {
                    soGiaoDich = txtSoGiaoDich.Text;
                }
                if (gl_nguon.EditValue.ToString() == "01")//thu nợ khách hàng
                {
                    //Tính tiền Công Nợ
                    string sSQL = "";
                    sSQL += "Update NHACUNGCAP " + "\n";
                    sSQL += "Set TIEN_CONGNO=ISNULL(TIEN_CONGNO,0) - " + txtSoTien.Text.Replace(",", "") + "\n";
                    sSQL += " Where MA=" + clsMain.SQLString(gl_DoiTuong.EditValue.ToString());
                    clsMain.ExecuteSQL(sSQL);

                    DataTable dtCheckMaPhieu = clsMain.ReturnDataTable("select MA_HOADON from CHITIETTHANHTOAN where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'");

                    if (chkTienMat.Checked)
                    {
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}', @TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, gl_DoiTuong.EditValue, "", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString(), clsUserManagement.ReturnMaNVOfUserLogin(), "01", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "NULL", "", "NULL", "NULL", txt_TMTen.Text, "NULL", "NULL");
                        }
                        else//thu theo hóa đơn
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, gl_DoiTuong.EditValue, gl_MaPhieuXuat.EditValue, "", clsUserManagement.ReturnMaNVOfUserLogin(), "01", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "NULL", "", "NULL", "NULL", txt_TMTen.Text, "NULL", "NULL");
                        }

                    }
                    else
                    {
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, gl_DoiTuong.EditValue, "", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString(), clsUserManagement.ReturnMaNVOfUserLogin(), "01", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", "NULL", "NULL");
                        }
                        else//thu theo hóa đơn
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, gl_DoiTuong.EditValue, gl_MaPhieuXuat.EditValue, "", clsUserManagement.ReturnMaNVOfUserLogin(), "01", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", "NULL", "NULL");
                        }

                    }
                    int dotTT = 0;
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        string dotthanhtoan = clsMain.ReturnDataTable("select ct.DOTTHANHTOAN  from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where pk.SOPHIEU='" + gl_MaPhieuXuat.EditValue + "' and FLAG='true'").Rows[0][0].ToString();
                        if (dotthanhtoan.Length > 1)
                        {
                            dotTT = int.Parse(dotthanhtoan.Substring(0, 2).Replace("-", "")) + 1;
                        }
                        else
                        {
                            dotTT = int.Parse(dotthanhtoan.Substring(0, 1).Replace("-", "")) + 1;
                        }
                    }
                    else//thu theo hóa đơn
                    {
                        string dotthanhtoan = clsMain.ReturnDataTable("select ct.DOTTHANHTOAN  from  CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and FLAG='true'").Rows[0][0].ToString();
                        if (dotthanhtoan.Length > 1)
                        {
                            dotTT = int.Parse(dotthanhtoan.Substring(0, 2).Replace("-", "")) + 1;
                        }
                        else
                        {
                            dotTT = int.Parse(dotthanhtoan.Substring(0, 1).Replace("-", "")) + 1;
                        }
                    }



                    bool _flag = true;
                    if (txtNo.Text == "0")
                        _flag = false;
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        if (clsMain.ReturnDataTable("select top(1) * from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString() + "'").Rows.Count > 0)
                        {
                            string MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString();
                            sql1 = string.Format("update CHITIETTHANHTOAN set FLAG='false' where  PK_PHIEUKHOID='{0}' and flag='true'", MaKho);

                        }
                    }
                    else//thu theo hóa đơn
                    {
                        if (clsMain.ReturnDataTable("select top(1) * from CHITIETTHANHTOAN where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'").Rows.Count > 0)
                        {
                            sql1 = string.Format("update CHITIETTHANHTOAN set FLAG='false' where  MA_HOADON='{0}' and flag='true'", gl_MaPhieuXuat.EditValue);

                        }
                    }
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}','{4}','{5: dd/MM/yyyy HH:mm:ss}','{6}','{7}',", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString(), txtTongNo.Text.Replace(",", ""), txtSoTien.Text.Replace(",", ""), txtNo.Text.Replace(",", ""), dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "true");
                    }
                    else//thu theo hóa đơn
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN (MA_HOADON,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}','{4}','{5: dd/MM/yyyy HH:mm:ss}','{6}','{7}',", gl_MaPhieuXuat.EditValue, txtTongNo.Text.Replace(",", ""), txtSoTien.Text.Replace(",", ""), txtNo.Text.Replace(",", ""), dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "true");
                    }

                    if (dateNgayHenTra.EditValue == null)
                    {
                        string sqlngayhentra = "";
                        string MaKho = "";
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString();
                            sqlngayhentra = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.CTTT_STT desc";
                        }
                        else
                        {
                            sqlngayhentra = "select top(1) ct.NGAYHENTRA from  CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.CTTT_STT desc";
                        }

                        DataTable dtngayhentra = clsMain.ReturnDataTable(sqlngayhentra);
                        string ngayhentra = "";
                        if (dtngayhentra.Rows.Count > 0)
                        {

                            ngayhentra = string.Format("'{0:dd/MM/yyyy}'", dtngayhentra.Rows[0][0]);
                            if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                            {
                                sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN='" + ngayhentra + "',ISTREHEN='false' where MA='" + MaKho + "' and NGAYHENTRA='" + ngayhentra + "'";
                            }
                            else
                            {
                                sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN='" + ngayhentra + "',ISTREHEN='false' where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and NGAYHENTRA='" + ngayhentra + "'";
                            }

                        }
                        else
                        {
                            ngayhentra = "NULL";
                        }
                        if (decimal.Parse(txtNo.Text.Replace(",", "")) == 0)
                        {
                            sqlChiTiet += "NULL,null,'false')";
                        }
                        else
                        {
                            sqlChiTiet += "" + ngayhentra + ",null,'false')";
                        }
                    }
                    else
                    {
                        if (decimal.Parse(txtNo.Text.Replace(",", "")) == 0)
                        {
                            sqlChiTiet += "NULL,null,'false')";
                        }
                        else
                        {
                            //sqlChiTiet += "'" + ngayhentra + "',null,'false')";
                            sqlChiTiet += "'" + string.Format("{0:dd/MM/yyyy}", dateNgayHenTra.EditValue) + "',null,'false')";
                        }

                        string MaKho = "";
                        string sqlngayhentra = "";
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0].ToString();
                            sqlngayhentra = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                        }
                        else
                        {
                            sqlngayhentra = "select top(1) ct.NGAYHENTRA from  CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.Ma_HoaDon where hd.Ma_HoaDon='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                        }

                        DataTable dtngayhentra = clsMain.ReturnDataTable(sqlngayhentra);
                        string ngayhentra = "";
                        if (dtngayhentra.Rows.Count > 0)
                        {
                            ngayhentra = string.Format("'{0:dd/MM/yyyy}'", dtngayhentra.Rows[0][0]);
                        }
                        else
                        {
                            ngayhentra = "NULL";
                        }
                        if (dtngayhentra.Rows.Count > 0)
                        {
                            if (int.Parse(string.Format("{0:yyyyMMdd}", clsMain.ReturnDataTable(sqlngayhentra).Rows[0][0])) < int.Parse(string.Format("{0:yyyyMMdd}", KP_UserManagement.clsMain.GetServerDate())))
                            {
                                if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                                {
                                    sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='true' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + "";
                                }
                                else
                                {
                                    sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='true' where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and NGAYHENTRA=" + ngayhentra + "";
                                }

                            }
                            else
                            {
                                if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                                {
                                    sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='false' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + "";
                                }
                                else
                                {
                                    sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='false' where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and NGAYHENTRA=" + ngayhentra + "";
                                }
                            }
                        }

                    }

                }
                else//thu từ nguồn khác
                {
                    if (chkTienMat.Checked)
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, gl_DoiTuong.EditValue, "", "", clsUserManagement.ReturnMaNVOfUserLogin(), gl_nguon.EditValue.ToString(), txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, "NULL", "NULL");

                    }
                    else
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:dd/MM/yyyy}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC={22}", txtMaPHC.Text, "", gl_DoiTuong.EditValue, "", clsUserManagement.ReturnMaNVOfUserLogin(), gl_nguon.EditValue.ToString(), txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txt_TenKH.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", "NULL", "NULL");

                    }
                }


                if (clsMain.ExecuteSQL(sql))
                {
                    if (sqlChiTiet != "")
                    {

                        if (sql1 != "")
                            clsMain.ReturnDataTable(sql1);
                        if (sql2 != "")
                            clsMain.ReturnDataTable(sql2);
                        clsMain.ExecuteSQL(sqlChiTiet);
                    }
                    try
                    {
                        if (chkTienMat.Checked)
                        {
                            DataTable dtInBaoCao = new DataTable();
                            dtInBaoCao.Columns.Add("TongCong", typeof(string));
                            dtInBaoCao.Columns.Add("SoPhieu", typeof(string));
                            dtInBaoCao.Columns.Add(new DataColumn("Ngay", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("Thang", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("Nam", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("HoTen", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("DiaChi", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("LyDo", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("SoTien", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("VietBangChu", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("KemTheo", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("NguoiLap", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("hotengiaodich", typeof(string)));
                            DataRow row = dtInBaoCao.NewRow();
                            row["SoPhieu"] = txtMaPHC.Text;
                            row["Ngay"] = string.Format("{0:dd}", dateTMNgayChi.EditValue);
                            row["Thang"] = string.Format("{0:MM}", dateTMNgayChi.EditValue);
                            row["Nam"] = string.Format("{0:yyyy}", dateTMNgayChi.EditValue);
                            row["HoTen"] = txt_TenKH.Text;
                            row["DiaChi"] = txtDiaChi.Text;
                            row["LyDo"] = txtLydo.Text;
                            row["SoTien"] = txtSoTien.Text + " VNĐ";
                            row["VietBangChu"] = txtTienChu.Text;
                            row["KemTheo"] = txtKemTheo.Text;
                            string sql21 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                            row["NguoiLap"] = clsMain.ReturnDataTable(sql21).Rows[0][0].ToString();
                            row["hotengiaodich"] = txt_TMTen.Text ;
                            dtInBaoCao.Rows.Add(row);

                            Frm_BCInPhieu frm = new Frm_BCInPhieu();
                            frm.WindowState = FormWindowState.Maximized;
                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            frm.dtSource = dtInBaoCao;
                            frm.Mode = 18;
                            frm.ShowDialog();
                            frm.Dispose();

                            
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    Reset();
                    gl_DoiTuong.EditValue = null;
                    gl_MaPhieuXuat.EditValue = null;
                    txtMaPHC.Text = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_NS()").Rows[0][0].ToString();
                    DataTable dtreset = clsMain.ReturnDataTable("select * from CN_LOAI_CONGNO where LoaiThuChi='01'  AND SUDUNG=1");
                    gl_nguon.Properties.DataSource = dtreset;
                    gl_nguon.EditValue = "-1";
                    gl_nguon.EditValue = dtreset.Rows[0]["MaLoai"].ToString();
                    if (close == 1)
                        this.Close();
                    throw new Exception("Lưu Thành Công");

                }
                else
                    throw new Exception("Lưu Thất Bại");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void chkTienMat_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTienMat.Checked)
            {
                chHTTT.Checked = false;
            }
            else
            {
                chHTTT.Checked = true;
            }
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;
                
            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;
              
            }
        }

        private void chHTTT_CheckedChanged(object sender, EventArgs e)
        {

            if (chHTTT.Checked)
            {
                chkTienMat.Checked = false;
            }
            else
            {
                chkTienMat.Checked = true;
            }
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;
               
            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;
              
            }
        }

        private void txtSoTien_TextChanged(object sender, EventArgs e)
        {
            if (txtSoTien.Text != "")
            {
                txtSoTien.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTien.Text.Replace(",", "")));
            }
        }

        private void txt_TenKH_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (gl_nguon.EditValue.ToString() == "02")
            {
                gl_DoiTuong.EditValue = null;
            }
        }

        private void gl_nguon_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_nguon.EditValue != null)
            {
                if (gl_nguon.EditValue.ToString() == "01")//nếu thu nợ khách hàng
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    gl_MaPhieuXuat.EditValue = null;
                    gbNo.Visible = true;
                    txt_TenKH.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = true;
                    lbDoiTuong.Visible = lbMaPhieuNhap.Visible = gl_DoiTuong.Visible = gl_MaPhieuXuat.Visible = true;
                    DataTable dt = clsMain.ReturnDataTable("(select  distinct dt.MA as DT_DOITUONGID ,dt.TEN as DT_HOTEN from NHACUNGCAP dt inner join KHO_PHIEUKHO pk on dt.MA=pk.NHACUNGCAP inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where THU_CHI='true' and FLAG='true' and ct.SOTIENCONLAI>0 )union (select  distinct dt.Ma as DT_DOITUONGID,dt.TEN as DT_HOTEN from NHACUNGCAP dt inner join HOADON hd on dt.MA=hd.MA_KHACHHANG inner join CHITIETTHANHTOAN ct on hd.MA_HOADON =ct.MA_HOADON where THU_CHI='true' and FLAG='true' and ct.SOTIENCONLAI>0 )order by dt.TEN");//lấy danh đối tượng nợ
                    gl_DoiTuong.Properties.DataSource = dt;
                    
                }
                else//thu khác
                {

                    Reset();
                    gl_DoiTuong.EditValue = null;
                    gl_MaPhieuXuat.EditValue = null;
                    gbNo.Visible = false;
                    txt_TenKH.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = false;

                    lbMaPhieuNhap.Visible = gl_MaPhieuXuat.Visible = false;
                    gl_MaPhieuXuat.EditValue = null;
                    
                    //DataTable dtkh = clsMain.ReturnDataTable("select MA as DT_DOITUONGID,TEN as DT_HOTEN from NHACUNGCAP where LOAI='0' and SUDUNG='true' order by TEN");
                    //gl_DoiTuong.Properties.DataSource = dtkh;

                    lbDoiTuong.Visible = gl_DoiTuong.Visible = false;
                    gl_DoiTuong.EditValue = null;

                    
                }

                dateTMNgayChi.EditValue = dateCKChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
            }
        }

        private void gl_DoiTuong_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_DoiTuong.EditValue != null)
            {
                gl_MaPhieuXuat.EditValue = null;
                Reset();
                if (gl_nguon.EditValue.ToString() == "01")
                {
                    string sql = "(select pk.SOPHIEU as MaPhieu from NHACUNGCAP dt inner join KHO_PHIEUKHO pk on dt.MA=pk.NHACUNGCAP inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID  where ct.SOTIENCONLAI>0 and THU_CHI='true' and FLAG='true' and pk.NHACUNGCAP='" + gl_DoiTuong.EditValue.ToString() + "' )  union  (select hd.Ma_HoaDon as MaPhieu from NHACUNGCAP dt inner join HOADON hd on dt.MA=hd.MA_KHACHHANG inner join CHITIETTHANHTOAN ct on hd.MA_HOADON=ct.MA_HOADON where ct.SOTIENCONLAI>0 and THU_CHI='true' and FLAG='true' and hd.MA_KHACHHANG='" + gl_DoiTuong.EditValue.ToString() + "')  order by MaPhieu";//lấy danh sách phiếu xuất của khách hàng đã chọn
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    gl_MaPhieuXuat.Properties.DataSource = dt;
                }
                else
                    if (gl_nguon.EditValue.ToString() == "02")
                    {
                        if (gl_DoiTuong.EditValue != null)
                        {
                            DataTable dtKH = clsMain.ReturnDataTable("select TEN as DT_HOTEN,DIACHI as DT_DIACHI from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'");
                            txt_TMTen.Text = txt_TenKH.Text = dtKH.Rows[0][0].ToString();
                            txtDiaChi.Text = dtKH.Rows[0][1].ToString();
                        }
                    }
            }
            
        }

        private void gl_MaPhieuXuat_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_MaPhieuXuat.EditValue != null)
            {
                try
                {
                    lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                    dateNgayHenTra.EditValue = null;
                    dateTMNgayChi.EditValue = dateCKChi.EditValue = clsMain.GetServerDate();
                    if (gl_nguon.EditValue.ToString() == "01")//thu khách hàng
                    {
                        DataTable dtCheckMaphieu = clsMain.ReturnDataTable("select SOPHIEU as maphieu from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "' union select MA_HOADON as maphieu from HOADON where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'");
                        if (dtCheckMaphieu.Rows.Count == 0)
                        {
                            Reset();
                        }
                        else
                        {
                            txtLydo.Text = txtKemTheo.Text = txtTienChu.Text = string.Empty;
                            txtNo.Text = txtSoTien.Text = txtTongNo.Text = txtTienTra.Text = string.Empty;
                            txt_TenKH.Text = clsMain.ReturnDataTable("select  TEN from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'").Rows[0][0].ToString();
                            //hiện thị số nợ của khách hàng
                            DataTable dtCheckMaPhieu = clsMain.ReturnDataTable("select MA_HOADON from CHITIETTHANHTOAN where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'");
                            string sql = "";
                            if (dtCheckMaPhieu.Rows.Count == 0)
                            {
                                sql = "select ct.SOTIENCONLAI from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU='" + gl_MaPhieuXuat.EditValue + "' and THU_CHI='true' and FLAG='true'";
                            }
                            else
                            {
                                sql = "select ct.SOTIENCONLAI from CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and THU_CHI='true' and FLAG='true'";
                            }
                            if (gl_nguon.EditValue.ToString() == "01")
                            {
                                txtTongNo.Text = string.Format("{0:#,0}", decimal.Parse(clsMain.ReturnDataTable(sql).Rows[0][0].ToString()));
                            }
                            sql = "select TEN,DIACHI from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "' ";
                            DataTable dt = clsMain.ReturnDataTable(sql);
                            if (dt.Rows.Count > 0)
                            {
                                txt_TenKH.Text = dt.Rows[0][0].ToString();
                                txtDiaChi.Text = dt.Rows[0][1].ToString();
                                txt_TMTen.Text = dt.Rows[0][0].ToString();
                                string ngayxuat = "";
                                if (dtCheckMaPhieu.Rows.Count == 0)
                                {
                                    ngayxuat = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable("select NGAYTAO from KHO_PHIEUKHO where SOPHIEU='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0]);
                                    txtLydo.Text = "Thu tiền mặt cho phiếu xuất số " + gl_MaPhieuXuat.EditValue + ",ngày xuất " + ngayxuat + "";
                                }
                                else
                                {
                                    ngayxuat = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable(" select NGAYTAO from HOADON  where MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'").Rows[0][0]);
                                    txtLydo.Text = "Thu tiền mặt cho hóa đơn số " + gl_MaPhieuXuat.EditValue + ",ngày bán " + ngayxuat + "";
                                }


                            }
                            string sqlCheck = "";
                            if (dtCheckMaPhieu.Rows.Count == 0)
                            {
                                sqlCheck = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                            }
                            else
                            {
                                sqlCheck = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + gl_MaPhieuXuat.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                            }
                            DataTable dtCheck = clsMain.ReturnDataTable(sqlCheck);
                            if (dtCheck.Rows.Count > 0)//nếu có ngày hẹn trả
                            {
                                lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                                dateNgayHenTra.EditValue = dtCheck.Rows[0][0];
                                checkNgay = true;
                            }
                            else//ko có ngày hẹn trả
                            {
                                string sqlCheck1 = "";
                                if (dtCheckMaPhieu.Rows.Count == 0)
                                {
                                    sqlCheck1 = "select top(1) ct.SOTIENCONLAI from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + gl_MaPhieuXuat.EditValue + "' order by ct.CTTT_STT desc";
                                }
                                else
                                {
                                    sqlCheck1 = "select top(1) ct.SOTIENCONLAI from CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + gl_MaPhieuXuat.EditValue + "'  order by ct.CTTT_STT desc";
                                }

                                DataTable dtCheck1 = clsMain.ReturnDataTable(sqlCheck1);
                                if (dtCheck1.Rows[0][0].ToString() != "0.0000")
                                {
                                    lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                                    checkNgay = true;
                                }
                                else
                                {
                                    lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                                    checkNgay = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
           
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}