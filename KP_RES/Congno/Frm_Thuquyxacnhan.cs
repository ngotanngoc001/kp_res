﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;

namespace KP_RES
{
    public partial class Frm_Thuquyxacnhan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Thuquyxacnhan()
        {
            InitializeComponent();
            LoaddataGridView();
        }
       
        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gridView1_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == NGUOIXACNHAN)
            {
                e.Info.DisplayText = (Convert.ToInt64(SOTIEN.SummaryItem.SummaryValue) - Convert.ToInt64(XACNHAN.SummaryItem.SummaryValue)).ToString("N0");
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void LoaddataGridView()
        {
            string sSQL = "";
            sSQL += " Exec sp_CN_CONGNO_DANHSACH_THU_CHI_THUQUYXACNHAN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gcDanhSach.DataSource = dt;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.GetFocusedRowCellValue(MAPCN).ToString().Substring(0, 2) == "PT")
            {
                btnXacnhan.Text = "Xác nhận đã thu đủ";
            }
            else
            {
                btnXacnhan.Text = "Xác nhận đã chi đủ";
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (View.GetRowCellValue(e.RowHandle, View.Columns["MAPCN"]).ToString() .Substring(0, 2) == "PC")
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.Blue;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXacnhan_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "update CN_CONGNO SET XACNHAN=1,NGUOIXACNHAN=" + clsMain.SQLString(clsGlobal.gsUserID) + ",	NGAYXACNHAN=GETDATE()"+"\n";
            sSQL += "Where MAPCN=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MAPCN).ToString()) ;
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Đã xác nhận thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận không thành công. Thử lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnTatcaphieu_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += " Exec sp_CN_CONGNO_DANHSACH_THU_CHI_THUQUYXACNHAN_ALL" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gcDanhSach.DataSource = dt;
        }
       

    
    }
}