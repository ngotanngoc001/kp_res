﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_BaoCaoUyNhiemChi : DevExpress.XtraEditors.XtraForm
    {
        BindingSource bs = new BindingSource();

        public Frm_BaoCaoUyNhiemChi()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gcDanhSach.DataSource = null;

            if (optGroup.SelectedIndex == 0)
                pnChoose.Visible = false;
            else
                pnChoose.Visible = true;

            if (optGroup.SelectedIndex == 1)
            {
                dtpTuNgay.EditValue = dtpDenNgay.EditValue = clsMain.GetServerDate();
                lbl_title.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = dtpDenNgay.Visible = dtpTuNgay.Visible = true;
                gl_NguoiLapPhieu.Visible = gl_LoaiThu.Visible = gl_PhuongThucTT.Visible = gl_NguonChi.Visible = false;
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";
                String sTuNgay = "";
                String sDenNgay = "";

                if (optGroup.EditValue.Equals("1"))
                {
                    sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue);
                    sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue);
                }

                String sSQL = "EXEC SP_SelectBCUyNhiemChi ";
                sSQL += clsMain.SQLString(sMa) + ",";
                sSQL += clsMain.SQLString(sTuNgay) + ",";
                sSQL += clsMain.SQLString(sDenNgay) + ",";
                sSQL += clsMain.SQLString(optGroup.SelectedIndex.ToString());

                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                gcDanhSach.DataSource = myDT;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView3.FocusedRowHandle == gridView3.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView3.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView3.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gcDanhSach.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gcDanhSach.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView3.RowCount > 0)
                {
                    DataTable myDT = ((DataTable)gcDanhSach.DataSource).Copy ();

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex == 1)
                        Description = "( " + Description + " - Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";
                    
                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;
                    myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 89;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch {
            }
        }

        private void bntInLai_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView3.FocusedRowHandle < 0)
                    throw new Exception("Hãy chọn Ủy Nhiệm Chi cần in");

                DataTable myDT = new DataTable();
                myDT.Columns.Add("MA", typeof(String));
                myDT.Columns.Add("NGAY", typeof(String));
                myDT.Columns.Add("DONVI_CHUYEN", typeof(String));
                myDT.Columns.Add("TAIKHOAN_CHUYEN", typeof(String));
                myDT.Columns.Add("CHINHANH_CHUYEN", typeof(String));
                myDT.Columns.Add("DONVI_NHAN", typeof(String));
                myDT.Columns.Add("TAIKHOAN_NHAN", typeof(String));
                myDT.Columns.Add("CMND", typeof(String));
                myDT.Columns.Add("NGAYCAP", typeof(String));
                myDT.Columns.Add("NOICAP", typeof(String));
                myDT.Columns.Add("NGANHANG", typeof(String));
                myDT.Columns.Add("TINH_TP", typeof(String));
                myDT.Columns.Add("NOIDUNG", typeof(String));
                myDT.Columns.Add("TIEN_CHU", typeof(String));
                myDT.Columns.Add("TIEN_SO", typeof(String));
                myDT.Columns.Add("TAIKHOANNO", typeof(String));
                myDT.Columns.Add("TAIKHOANCO", typeof(String));
                DataRow row = myDT.NewRow();
                row["MA"] = gridView3.GetFocusedRowCellValue(MA).ToString();
                row["NGAY"] = String.Format("{0:dd/MM/yyyy}", gridView3.GetFocusedRowCellValue(NGAY));
                row["DONVI_CHUYEN"] = gridView3.GetFocusedRowCellValue(DONVI_CHUYEN);
                row["TAIKHOAN_CHUYEN"] = gridView3.GetFocusedRowCellValue(TAIKHOAN_CHUYEN);
                row["CHINHANH_CHUYEN"] = gridView3.GetFocusedRowCellValue(CHINHANH_CHUYEN);
                row["DONVI_NHAN"] = gridView3.GetFocusedRowCellValue(DONVI_NHAN);
                row["TAIKHOAN_NHAN"] = gridView3.GetFocusedRowCellValue(TAIKHOAN_NHAN);
                row["CMND"] = gridView3.GetFocusedRowCellValue(CMND);
                row["NGAYCAP"] = String.Format("{0:dd/MM/yyyy}", gridView3.GetFocusedRowCellValue(NGAYCAP));
                row["NOICAP"] = gridView3.GetFocusedRowCellValue(NOICAP);
                row["NGANHANG"] = gridView3.GetFocusedRowCellValue(NGANHANG);
                row["TINH_TP"] = gridView3.GetFocusedRowCellValue(TINH_TP);
                row["NOIDUNG"] = gridView3.GetFocusedRowCellValue(NOIDUNG);
                row["TIEN_CHU"] = gridView3.GetFocusedRowCellValue(TIEN_CHU);
                row["TIEN_SO"] = String.Format("{0:N0}",gridView3.GetFocusedRowCellValue(TIEN_SO)) + " VNĐ";
                row["TAIKHOANNO"] = "";
                row["TAIKHOANCO"] = "";
                myDT.Rows.Add(row);

                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = myDT;
                frm.Mode = 88;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

    }
}