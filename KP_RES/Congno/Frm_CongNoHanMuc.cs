﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_CongNoHanMuc : DevExpress.XtraEditors.XtraForm
    {
        string _maKH;
        string _congno;
        string _tiendatcoc;
        public Frm_CongNoHanMuc(string maKH,string tenKH,bool isdatcoc,string congno,string HanThanhToan,string tiendatcoc)
        {
            InitializeComponent();
            _maKH = maKH;
            _congno = congno;
            _tiendatcoc = tiendatcoc;
            txtMaKH.Text = maKH;
            txtTenKhachHang.Text = tenKH;
            txtHanThanhToan.Text = HanThanhToan;
            chkTienDatCoc.Checked = isdatcoc;
            if (chkTienDatCoc.Checked)
            {
                txtTienHanMuc.Enabled = false;
                txtDatCoc.Enabled = true;
                txtDatCoc.Text = _tiendatcoc;
                txtDatCoc.Focus();
                txtDatCoc.SelectAll();
            }
            else
            {
                txtTienHanMuc.Enabled = true;
                txtDatCoc.Enabled = false;
                txtTienHanMuc.Text = _congno;
                txtTienHanMuc.Focus();
                txtTienHanMuc.SelectAll();
            }
        }


        private void bnt_dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            if (chkTienDatCoc.Checked)
            {
                if (txtDatCoc.Text.Trim() == "" || txtDatCoc.Text.Trim() == "0")
                {
                    txtDatCoc.Focus();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bạn chưa nhập số tiền ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else
            {
                if (txtTienHanMuc.Text.Trim() == "" || txtTienHanMuc.Text.Trim() == "0")
                {
                    txtTienHanMuc.Focus();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bạn chưa nhập số tiền ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            
            if (txtHanThanhToan.Text.Trim() == "" || txtHanThanhToan.Text.Trim() == "0")
            {
                txtHanThanhToan.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập thời hạn thanh toán", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                string sSQL = "";
                sSQL += "UPDATE NHACUNGCAP Set" + "\n";
                sSQL += "ISDATCOC=" + clsMain.SQLBit(chkTienDatCoc.Checked) + ",";
                sSQL += "HANTHANHTOAN=" + clsMain.SQLString(txtHanThanhToan.Text.Replace(",", "")) + ",";
                if (chkTienDatCoc.Checked)
                    sSQL += "TIENDATCOC=" + clsMain.SQLString(txtDatCoc.Text.Replace(",", "")) + "\n";
                else
                    sSQL += "HANMUC_CONGNO=" + clsMain.SQLString(txtTienHanMuc.Text.Replace(",", "")) + "\n";
                sSQL += "WHERE MA=" + clsMain.SQLString(txtMaKH.Text);
                if (clsMain.ExecuteSQL(sSQL))
                {
                    XtraMessageBox.Show("Lưu công nợ thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("Lưu công nợ thất bại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void chkTienDatCoc_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTienDatCoc.Checked)
            {
                txtTienHanMuc.Enabled = false;
                txtDatCoc.Enabled = true;
                txtDatCoc.Text = _tiendatcoc;
                txtDatCoc.Focus();
                txtDatCoc.SelectAll();
            }
            else
            {
                txtTienHanMuc.Enabled = true;
                txtDatCoc.Enabled = false;
                txtTienHanMuc.Text = _congno;
                txtTienHanMuc.Focus();
                txtTienHanMuc.SelectAll();
            }
        }
    }
}