﻿namespace KP_RES
{
    partial class PU_THAYDOINGAYHENTRA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.date_NgayHenTra = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bnt_luu = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_dong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.date_NgayHenTra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_NgayHenTra.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // date_NgayHenTra
            // 
            this.date_NgayHenTra.EditValue = null;
            this.date_NgayHenTra.Location = new System.Drawing.Point(179, 55);
            this.date_NgayHenTra.Name = "date_NgayHenTra";
            this.date_NgayHenTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_NgayHenTra.Properties.Appearance.Options.UseFont = true;
            this.date_NgayHenTra.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_NgayHenTra.Properties.AppearanceDropDown.Options.UseFont = true;
            this.date_NgayHenTra.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_NgayHenTra.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.date_NgayHenTra.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_NgayHenTra.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.date_NgayHenTra.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_NgayHenTra.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.date_NgayHenTra.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_NgayHenTra.Properties.AppearanceFocused.Options.UseFont = true;
            this.date_NgayHenTra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_NgayHenTra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_NgayHenTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_NgayHenTra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_NgayHenTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_NgayHenTra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_NgayHenTra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_NgayHenTra.Size = new System.Drawing.Size(194, 26);
            this.date_NgayHenTra.TabIndex = 0;
            this.date_NgayHenTra.EditValueChanged += new System.EventHandler(this.date_NgayHenTra_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(5, 58);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(168, 20);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mời Nhập Ngày Hẹn trả:";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(385, 46);
            this.panel1.TabIndex = 2;
            // 
            // bnt_luu
            // 
            this.bnt_luu.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luu.Appearance.Options.UseFont = true;
            this.bnt_luu.Image = global::KP_RES.Properties.Resources.save_26;
            this.bnt_luu.Location = new System.Drawing.Point(179, 87);
            this.bnt_luu.Name = "bnt_luu";
            this.bnt_luu.Size = new System.Drawing.Size(98, 33);
            this.bnt_luu.TabIndex = 3;
            this.bnt_luu.Text = "Lưu";
            this.bnt_luu.Click += new System.EventHandler(this.bnt_luu_Click);
            // 
            // bnt_dong
            // 
            this.bnt_dong.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_dong.Appearance.Options.UseFont = true;
            this.bnt_dong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.bnt_dong.Location = new System.Drawing.Point(283, 87);
            this.bnt_dong.Name = "bnt_dong";
            this.bnt_dong.Size = new System.Drawing.Size(89, 33);
            this.bnt_dong.TabIndex = 4;
            this.bnt_dong.Text = "Đóng";
            this.bnt_dong.Click += new System.EventHandler(this.bnt_dong_Click);
            // 
            // PU_THAYDOINGAYHENTRA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 124);
            this.Controls.Add(this.bnt_dong);
            this.Controls.Add(this.bnt_luu);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.date_NgayHenTra);
            this.Name = "PU_THAYDOINGAYHENTRA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "THAY ĐỔI NGÀY HẸN TRẢ";
            ((System.ComponentModel.ISupportInitialize)(this.date_NgayHenTra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_NgayHenTra.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit date_NgayHenTra;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton bnt_luu;
        private DevExpress.XtraEditors.SimpleButton bnt_dong;
    }
}