﻿namespace KP_RES 
{
    partial class Frm_LapPhieuChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnLayPhieuDeNghiChi = new DevExpress.XtraEditors.SimpleButton();
            this.btn_HuyBo = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_luu = new DevExpress.XtraEditors.SimpleButton();
            this.pnHTTT = new DevExpress.XtraEditors.GroupControl();
            this.pnBottom = new DevExpress.XtraEditors.PanelControl();
            this.pnChuyenKhoan = new DevExpress.XtraEditors.PanelControl();
            this.txtSoGiaoDich = new DevExpress.XtraEditors.TextEdit();
            this.lbSoGiaoDich = new DevExpress.XtraEditors.LabelControl();
            this.dateCKChi = new DevExpress.XtraEditors.DateEdit();
            this.lbCkChi = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTaiKhoanNhan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenNHNhan = new DevExpress.XtraEditors.TextEdit();
            this.lbtentaiKhoanNhan = new System.Windows.Forms.Label();
            this.txtSoTaiKhoanChuyen = new DevExpress.XtraEditors.TextEdit();
            this.lbSoTaiKhoanChuyen = new DevExpress.XtraEditors.LabelControl();
            this.txtCKTenNganHang = new DevExpress.XtraEditors.TextEdit();
            this.lbCKTenNH = new System.Windows.Forms.Label();
            this.pnTienMat = new DevExpress.XtraEditors.PanelControl();
            this.dateTMNgayChi = new DevExpress.XtraEditors.DateEdit();
            this.lbTMNgayChi = new DevExpress.XtraEditors.LabelControl();
            this.txt_TMTen = new DevExpress.XtraEditors.TextEdit();
            this.lbTMTen = new System.Windows.Forms.Label();
            this.pnHinhthuc = new DevExpress.XtraEditors.PanelControl();
            this.chkTienMat = new DevExpress.XtraEditors.CheckEdit();
            this.chHTTT = new DevExpress.XtraEditors.CheckEdit();
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grMaPhieuNhap = new DevExpress.XtraEditors.GridLookUpEdit();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnDoiTuong = new DevExpress.XtraEditors.PanelControl();
            this.gbNo = new DevExpress.XtraEditors.PanelControl();
            this.txtNo = new DevExpress.XtraEditors.TextEdit();
            this.txtTongNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienTra = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gbChiChi = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtChiConLai = new DevExpress.XtraEditors.TextEdit();
            this.txtTongChi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienChi = new DevExpress.XtraEditors.TextEdit();
            this.gl_NhanVien = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbMaPhieuNhap = new System.Windows.Forms.Label();
            this.gl_DoiTuong = new DevExpress.XtraEditors.GridLookUpEdit();
            this.lbNhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbNCC = new DevExpress.XtraEditors.LabelControl();
            this.gl_nguon = new DevExpress.XtraEditors.GridLookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateNgayHenTra = new DevExpress.XtraEditors.DateEdit();
            this.lbNgayHenTra = new DevExpress.XtraEditors.LabelControl();
            this.txtMaPHC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtKemTheo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienChu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTien = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtLydo = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.pnthongtinphieu = new DevExpress.XtraEditors.GroupControl();
            this.cbNguonChi = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbNguonChi = new System.Windows.Forms.Label();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNCC = new DevExpress.XtraEditors.TextEdit();
            this.lbNCC1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnHTTT)).BeginInit();
            this.pnHTTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).BeginInit();
            this.pnBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChuyenKhoan)).BeginInit();
            this.pnChuyenKhoan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCKChi.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCKChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNHNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCKTenNganHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTienMat)).BeginInit();
            this.pnTienMat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTMNgayChi.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTMNgayChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TMTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHinhthuc)).BeginInit();
            this.pnHinhthuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienMat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chHTTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grMaPhieuNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDoiTuong)).BeginInit();
            this.pnDoiTuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbNo)).BeginInit();
            this.gbNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbChiChi)).BeginInit();
            this.gbChiChi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiConLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_DoiTuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_nguon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayHenTra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayHenTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPHC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemTheo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLydo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnthongtinphieu)).BeginInit();
            this.pnthongtinphieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbNguonChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNCC.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLayPhieuDeNghiChi
            // 
            this.btnLayPhieuDeNghiChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhieuDeNghiChi.Appearance.Options.UseFont = true;
            this.btnLayPhieuDeNghiChi.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnLayPhieuDeNghiChi.Location = new System.Drawing.Point(313, 6);
            this.btnLayPhieuDeNghiChi.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnLayPhieuDeNghiChi.Name = "btnLayPhieuDeNghiChi";
            this.btnLayPhieuDeNghiChi.Size = new System.Drawing.Size(225, 35);
            this.btnLayPhieuDeNghiChi.TabIndex = 2;
            this.btnLayPhieuDeNghiChi.Text = "&3.Lấy Phiếu Đề Nghị Chi";
            this.btnLayPhieuDeNghiChi.Click += new System.EventHandler(this.btnLayPhieuDeNghiChi_Click);
            // 
            // btn_HuyBo
            // 
            this.btn_HuyBo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HuyBo.Appearance.Options.UseFont = true;
            this.btn_HuyBo.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btn_HuyBo.Location = new System.Drawing.Point(223, 6);
            this.btn_HuyBo.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btn_HuyBo.Name = "btn_HuyBo";
            this.btn_HuyBo.Size = new System.Drawing.Size(80, 35);
            this.btn_HuyBo.TabIndex = 1;
            this.btn_HuyBo.Text = "&2.Hủy";
            this.btn_HuyBo.Click += new System.EventHandler(this.btn_HuyBo_Click);
            // 
            // bnt_luu
            // 
            this.bnt_luu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luu.Appearance.Options.UseFont = true;
            this.bnt_luu.Image = global::KP_RES.Properties.Resources.save_26;
            this.bnt_luu.Location = new System.Drawing.Point(133, 6);
            this.bnt_luu.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bnt_luu.Name = "bnt_luu";
            this.bnt_luu.Size = new System.Drawing.Size(80, 35);
            this.bnt_luu.TabIndex = 0;
            this.bnt_luu.Text = "&1.Lưu";
            this.bnt_luu.Click += new System.EventHandler(this.bnt_luu_Click);
            // 
            // pnHTTT
            // 
            this.pnHTTT.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.pnHTTT.AppearanceCaption.Options.UseFont = true;
            this.pnHTTT.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnHTTT.Controls.Add(this.pnBottom);
            this.pnHTTT.Controls.Add(this.pnChuyenKhoan);
            this.pnHTTT.Controls.Add(this.pnTienMat);
            this.pnHTTT.Controls.Add(this.pnHinhthuc);
            this.pnHTTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHTTT.Location = new System.Drawing.Point(0, 504);
            this.pnHTTT.Name = "pnHTTT";
            this.pnHTTT.Size = new System.Drawing.Size(1057, 237);
            this.pnHTTT.TabIndex = 110;
            this.pnHTTT.Text = "Hình thức thanh toán";
            // 
            // pnBottom
            // 
            this.pnBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBottom.Controls.Add(this.btnLayPhieuDeNghiChi);
            this.pnBottom.Controls.Add(this.btn_HuyBo);
            this.pnBottom.Controls.Add(this.bnt_luu);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnBottom.Location = new System.Drawing.Point(2, 193);
            this.pnBottom.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1053, 44);
            this.pnBottom.TabIndex = 1;
            // 
            // pnChuyenKhoan
            // 
            this.pnChuyenKhoan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnChuyenKhoan.Controls.Add(this.txtSoGiaoDich);
            this.pnChuyenKhoan.Controls.Add(this.lbSoGiaoDich);
            this.pnChuyenKhoan.Controls.Add(this.dateCKChi);
            this.pnChuyenKhoan.Controls.Add(this.lbCkChi);
            this.pnChuyenKhoan.Controls.Add(this.txtSoTaiKhoanNhan);
            this.pnChuyenKhoan.Controls.Add(this.labelControl11);
            this.pnChuyenKhoan.Controls.Add(this.txtTenNHNhan);
            this.pnChuyenKhoan.Controls.Add(this.lbtentaiKhoanNhan);
            this.pnChuyenKhoan.Controls.Add(this.txtSoTaiKhoanChuyen);
            this.pnChuyenKhoan.Controls.Add(this.lbSoTaiKhoanChuyen);
            this.pnChuyenKhoan.Controls.Add(this.txtCKTenNganHang);
            this.pnChuyenKhoan.Controls.Add(this.lbCKTenNH);
            this.pnChuyenKhoan.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChuyenKhoan.Location = new System.Drawing.Point(2, 94);
            this.pnChuyenKhoan.Name = "pnChuyenKhoan";
            this.pnChuyenKhoan.Size = new System.Drawing.Size(1053, 99);
            this.pnChuyenKhoan.TabIndex = 4;
            // 
            // txtSoGiaoDich
            // 
            this.txtSoGiaoDich.EnterMoveNextControl = true;
            this.txtSoGiaoDich.Location = new System.Drawing.Point(133, 67);
            this.txtSoGiaoDich.Name = "txtSoGiaoDich";
            this.txtSoGiaoDich.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiaoDich.Properties.Appearance.Options.UseFont = true;
            this.txtSoGiaoDich.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoGiaoDich.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtSoGiaoDich.Properties.DisplayFormat.FormatString = "0,0 vnd";
            this.txtSoGiaoDich.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoGiaoDich.Properties.EditFormat.FormatString = "0,0 vnd";
            this.txtSoGiaoDich.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoGiaoDich.Size = new System.Drawing.Size(248, 26);
            this.txtSoGiaoDich.TabIndex = 9;
            // 
            // lbSoGiaoDich
            // 
            this.lbSoGiaoDich.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoGiaoDich.Location = new System.Drawing.Point(5, 70);
            this.lbSoGiaoDich.Name = "lbSoGiaoDich";
            this.lbSoGiaoDich.Size = new System.Drawing.Size(97, 19);
            this.lbSoGiaoDich.TabIndex = 8;
            this.lbSoGiaoDich.Text = "Số Giao Dịch:";
            // 
            // dateCKChi
            // 
            this.dateCKChi.EditValue = null;
            this.dateCKChi.EnterMoveNextControl = true;
            this.dateCKChi.Location = new System.Drawing.Point(468, 67);
            this.dateCKChi.Name = "dateCKChi";
            this.dateCKChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateCKChi.Properties.Appearance.Options.UseFont = true;
            this.dateCKChi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateCKChi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateCKChi.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateCKChi.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateCKChi.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateCKChi.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateCKChi.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.dateCKChi.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateCKChi.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateCKChi.Properties.AppearanceFocused.Options.UseFont = true;
            this.dateCKChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCKChi.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateCKChi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateCKChi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateCKChi.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateCKChi.Size = new System.Drawing.Size(248, 26);
            this.dateCKChi.TabIndex = 11;
            // 
            // lbCkChi
            // 
            this.lbCkChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCkChi.Location = new System.Drawing.Point(387, 70);
            this.lbCkChi.Name = "lbCkChi";
            this.lbCkChi.Size = new System.Drawing.Size(42, 19);
            this.lbCkChi.TabIndex = 10;
            this.lbCkChi.Text = "Ngày:";
            // 
            // txtSoTaiKhoanNhan
            // 
            this.txtSoTaiKhoanNhan.EnterMoveNextControl = true;
            this.txtSoTaiKhoanNhan.Location = new System.Drawing.Point(468, 35);
            this.txtSoTaiKhoanNhan.Name = "txtSoTaiKhoanNhan";
            this.txtSoTaiKhoanNhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTaiKhoanNhan.Properties.Appearance.Options.UseFont = true;
            this.txtSoTaiKhoanNhan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoTaiKhoanNhan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtSoTaiKhoanNhan.Properties.DisplayFormat.FormatString = "0,0 vnd";
            this.txtSoTaiKhoanNhan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoTaiKhoanNhan.Properties.EditFormat.FormatString = "0,0 vnd";
            this.txtSoTaiKhoanNhan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoTaiKhoanNhan.Size = new System.Drawing.Size(248, 26);
            this.txtSoTaiKhoanNhan.TabIndex = 7;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(387, 38);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(67, 19);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "TK Nhận:";
            // 
            // txtTenNHNhan
            // 
            this.txtTenNHNhan.EnterMoveNextControl = true;
            this.txtTenNHNhan.Location = new System.Drawing.Point(133, 35);
            this.txtTenNHNhan.Name = "txtTenNHNhan";
            this.txtTenNHNhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNHNhan.Properties.Appearance.Options.UseFont = true;
            this.txtTenNHNhan.Size = new System.Drawing.Size(248, 26);
            this.txtTenNHNhan.TabIndex = 5;
            // 
            // lbtentaiKhoanNhan
            // 
            this.lbtentaiKhoanNhan.AutoSize = true;
            this.lbtentaiKhoanNhan.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtentaiKhoanNhan.Location = new System.Drawing.Point(5, 38);
            this.lbtentaiKhoanNhan.Name = "lbtentaiKhoanNhan";
            this.lbtentaiKhoanNhan.Size = new System.Drawing.Size(79, 19);
            this.lbtentaiKhoanNhan.TabIndex = 4;
            this.lbtentaiKhoanNhan.Text = "NH Nhận:";
            // 
            // txtSoTaiKhoanChuyen
            // 
            this.txtSoTaiKhoanChuyen.EnterMoveNextControl = true;
            this.txtSoTaiKhoanChuyen.Location = new System.Drawing.Point(468, 3);
            this.txtSoTaiKhoanChuyen.Name = "txtSoTaiKhoanChuyen";
            this.txtSoTaiKhoanChuyen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTaiKhoanChuyen.Properties.Appearance.Options.UseFont = true;
            this.txtSoTaiKhoanChuyen.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoTaiKhoanChuyen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtSoTaiKhoanChuyen.Properties.DisplayFormat.FormatString = "0,0 vnd";
            this.txtSoTaiKhoanChuyen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoTaiKhoanChuyen.Properties.EditFormat.FormatString = "0,0 vnd";
            this.txtSoTaiKhoanChuyen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSoTaiKhoanChuyen.Properties.Mask.EditMask = "N0";
            this.txtSoTaiKhoanChuyen.Size = new System.Drawing.Size(248, 26);
            this.txtSoTaiKhoanChuyen.TabIndex = 3;
            // 
            // lbSoTaiKhoanChuyen
            // 
            this.lbSoTaiKhoanChuyen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoTaiKhoanChuyen.Location = new System.Drawing.Point(387, 6);
            this.lbSoTaiKhoanChuyen.Name = "lbSoTaiKhoanChuyen";
            this.lbSoTaiKhoanChuyen.Size = new System.Drawing.Size(83, 19);
            this.lbSoTaiKhoanChuyen.TabIndex = 2;
            this.lbSoTaiKhoanChuyen.Text = "TK Chuyển:";
            // 
            // txtCKTenNganHang
            // 
            this.txtCKTenNganHang.EnterMoveNextControl = true;
            this.txtCKTenNganHang.Location = new System.Drawing.Point(133, 3);
            this.txtCKTenNganHang.Name = "txtCKTenNganHang";
            this.txtCKTenNganHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCKTenNganHang.Properties.Appearance.Options.UseFont = true;
            this.txtCKTenNganHang.Size = new System.Drawing.Size(248, 26);
            this.txtCKTenNganHang.TabIndex = 1;
            // 
            // lbCKTenNH
            // 
            this.lbCKTenNH.AutoSize = true;
            this.lbCKTenNH.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCKTenNH.Location = new System.Drawing.Point(5, 6);
            this.lbCKTenNH.Name = "lbCKTenNH";
            this.lbCKTenNH.Size = new System.Drawing.Size(95, 19);
            this.lbCKTenNH.TabIndex = 0;
            this.lbCKTenNH.Text = "NH Chuyển:";
            // 
            // pnTienMat
            // 
            this.pnTienMat.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTienMat.Controls.Add(this.dateTMNgayChi);
            this.pnTienMat.Controls.Add(this.lbTMNgayChi);
            this.pnTienMat.Controls.Add(this.txt_TMTen);
            this.pnTienMat.Controls.Add(this.lbTMTen);
            this.pnTienMat.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTienMat.Location = new System.Drawing.Point(2, 55);
            this.pnTienMat.Name = "pnTienMat";
            this.pnTienMat.Size = new System.Drawing.Size(1053, 39);
            this.pnTienMat.TabIndex = 3;
            // 
            // dateTMNgayChi
            // 
            this.dateTMNgayChi.EditValue = null;
            this.dateTMNgayChi.EnterMoveNextControl = true;
            this.dateTMNgayChi.Location = new System.Drawing.Point(468, 5);
            this.dateTMNgayChi.Name = "dateTMNgayChi";
            this.dateTMNgayChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTMNgayChi.Properties.Appearance.Options.UseFont = true;
            this.dateTMNgayChi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateTMNgayChi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateTMNgayChi.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateTMNgayChi.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateTMNgayChi.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateTMNgayChi.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateTMNgayChi.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.dateTMNgayChi.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateTMNgayChi.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateTMNgayChi.Properties.AppearanceFocused.Options.UseFont = true;
            this.dateTMNgayChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateTMNgayChi.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateTMNgayChi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateTMNgayChi.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateTMNgayChi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateTMNgayChi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateTMNgayChi.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateTMNgayChi.Size = new System.Drawing.Size(248, 26);
            this.dateTMNgayChi.TabIndex = 3;
            // 
            // lbTMNgayChi
            // 
            this.lbTMNgayChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTMNgayChi.Location = new System.Drawing.Point(387, 8);
            this.lbTMNgayChi.Name = "lbTMNgayChi";
            this.lbTMNgayChi.Size = new System.Drawing.Size(42, 19);
            this.lbTMNgayChi.TabIndex = 2;
            this.lbTMNgayChi.Text = "Ngày:";
            // 
            // txt_TMTen
            // 
            this.txt_TMTen.EnterMoveNextControl = true;
            this.txt_TMTen.Location = new System.Drawing.Point(133, 5);
            this.txt_TMTen.Name = "txt_TMTen";
            this.txt_TMTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TMTen.Properties.Appearance.Options.UseFont = true;
            this.txt_TMTen.Size = new System.Drawing.Size(248, 26);
            this.txt_TMTen.TabIndex = 1;
            // 
            // lbTMTen
            // 
            this.lbTMTen.AutoSize = true;
            this.lbTMTen.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTMTen.Location = new System.Drawing.Point(5, 7);
            this.lbTMTen.Name = "lbTMTen";
            this.lbTMTen.Size = new System.Drawing.Size(132, 19);
            this.lbTMTen.TabIndex = 0;
            this.lbTMTen.Text = "Tên Người Nhận:";
            // 
            // pnHinhthuc
            // 
            this.pnHinhthuc.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnHinhthuc.Controls.Add(this.chkTienMat);
            this.pnHinhthuc.Controls.Add(this.chHTTT);
            this.pnHinhthuc.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHinhthuc.Location = new System.Drawing.Point(2, 27);
            this.pnHinhthuc.Name = "pnHinhthuc";
            this.pnHinhthuc.Size = new System.Drawing.Size(1053, 28);
            this.pnHinhthuc.TabIndex = 0;
            // 
            // chkTienMat
            // 
            this.chkTienMat.EditValue = true;
            this.chkTienMat.EnterMoveNextControl = true;
            this.chkTienMat.Location = new System.Drawing.Point(131, 3);
            this.chkTienMat.Name = "chkTienMat";
            this.chkTienMat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTienMat.Properties.Appearance.Options.UseFont = true;
            this.chkTienMat.Properties.Caption = "Tiền Mặt";
            this.chkTienMat.Size = new System.Drawing.Size(106, 24);
            this.chkTienMat.TabIndex = 0;
            this.chkTienMat.CheckedChanged += new System.EventHandler(this.chkTienMat_CheckedChanged);
            // 
            // chHTTT
            // 
            this.chHTTT.EnterMoveNextControl = true;
            this.chHTTT.Location = new System.Drawing.Point(565, 1);
            this.chHTTT.Name = "chHTTT";
            this.chHTTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chHTTT.Properties.Appearance.Options.UseFont = true;
            this.chHTTT.Properties.Caption = "Chuyển Khoản";
            this.chHTTT.Size = new System.Drawing.Size(151, 24);
            this.chHTTT.TabIndex = 1;
            this.chHTTT.CheckedChanged += new System.EventHandler(this.chHTTT_CheckedChanged);
            // 
            // TenLoai
            // 
            this.TenLoai.Name = "TenLoai";
            // 
            // MaLoai
            // 
            this.MaLoai.Name = "MaLoai";
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 25;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn8});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Mã";
            this.gridColumn5.FieldName = "MaPhieu";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 335;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "gridColumn8";
            this.gridColumn8.FieldName = "DT_DOITUONGID";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // MaPhieu
            // 
            this.MaPhieu.Name = "MaPhieu";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridLookUpEdit1View.Appearance.FocusedRow.Options.UseFont = true;
            this.gridLookUpEdit1View.ColumnPanelRowHeight = 30;
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.GroupRowHeight = 30;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.RowHeight = 30;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.Caption = "Mã";
            this.gridColumn3.FieldName = "MaLoai";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.Caption = "Tên";
            this.gridColumn4.FieldName = "TenLoai";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 235;
            // 
            // grMaPhieuNhap
            // 
            this.grMaPhieuNhap.EnterMoveNextControl = true;
            this.grMaPhieuNhap.Location = new System.Drawing.Point(133, 96);
            this.grMaPhieuNhap.Name = "grMaPhieuNhap";
            this.grMaPhieuNhap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grMaPhieuNhap.Properties.Appearance.Options.UseFont = true;
            this.grMaPhieuNhap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grMaPhieuNhap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.grMaPhieuNhap.Properties.AutoComplete = false;
            this.grMaPhieuNhap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.grMaPhieuNhap.Properties.DisplayMember = "MaPhieu";
            this.grMaPhieuNhap.Properties.NullText = "";
            this.grMaPhieuNhap.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.grMaPhieuNhap.Properties.PopupFormSize = new System.Drawing.Size(248, 100);
            this.grMaPhieuNhap.Properties.ValueMember = "MaPhieu";
            this.grMaPhieuNhap.Properties.View = this.gridView2;
            this.grMaPhieuNhap.Size = new System.Drawing.Size(248, 26);
            this.grMaPhieuNhap.TabIndex = 5;
            this.grMaPhieuNhap.EditValueChanged += new System.EventHandler(this.grMaPhieuNhap_EditValueChanged);
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "Mã";
            this.gridColumn1.FieldName = "DT_DOITUONGID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 100;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "Tên";
            this.gridColumn2.FieldName = "DT_HOTEN";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 235;
            // 
            // DT_HOTEN
            // 
            this.DT_HOTEN.Name = "DT_HOTEN";
            // 
            // pnDoiTuong
            // 
            this.pnDoiTuong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnDoiTuong.Controls.Add(this.gbNo);
            this.pnDoiTuong.Controls.Add(this.groupControl1);
            this.pnDoiTuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnDoiTuong.Location = new System.Drawing.Point(0, 50);
            this.pnDoiTuong.Name = "pnDoiTuong";
            this.pnDoiTuong.Size = new System.Drawing.Size(1057, 135);
            this.pnDoiTuong.TabIndex = 105;
            // 
            // gbNo
            // 
            this.gbNo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gbNo.Controls.Add(this.txtNo);
            this.gbNo.Controls.Add(this.txtTongNo);
            this.gbNo.Controls.Add(this.labelControl9);
            this.gbNo.Controls.Add(this.labelControl8);
            this.gbNo.Controls.Add(this.txtTienTra);
            this.gbNo.Controls.Add(this.labelControl10);
            this.gbNo.Location = new System.Drawing.Point(394, 30);
            this.gbNo.Name = "gbNo";
            this.gbNo.Size = new System.Drawing.Size(338, 103);
            this.gbNo.TabIndex = 6;
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(74, 66);
            this.txtNo.Name = "txtNo";
            this.txtNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtNo.Properties.Appearance.Options.UseFont = true;
            this.txtNo.Properties.Appearance.Options.UseForeColor = true;
            this.txtNo.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtNo.Properties.Mask.EditMask = "N0";
            this.txtNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtNo.Properties.ReadOnly = true;
            this.txtNo.Size = new System.Drawing.Size(248, 26);
            this.txtNo.TabIndex = 5;
            // 
            // txtTongNo
            // 
            this.txtTongNo.Location = new System.Drawing.Point(74, 2);
            this.txtTongNo.Name = "txtTongNo";
            this.txtTongNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongNo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtTongNo.Properties.Appearance.Options.UseFont = true;
            this.txtTongNo.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongNo.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongNo.Properties.Mask.EditMask = "N0";
            this.txtTongNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongNo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongNo.Properties.ReadOnly = true;
            this.txtTongNo.Size = new System.Drawing.Size(248, 26);
            this.txtTongNo.TabIndex = 1;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(3, 69);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(59, 19);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "Còn Lại:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(0, 5);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(68, 19);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Tổng Nợ:";
            // 
            // txtTienTra
            // 
            this.txtTienTra.Location = new System.Drawing.Point(74, 34);
            this.txtTienTra.Name = "txtTienTra";
            this.txtTienTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienTra.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtTienTra.Properties.Appearance.Options.UseFont = true;
            this.txtTienTra.Properties.Appearance.Options.UseForeColor = true;
            this.txtTienTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienTra.Properties.Mask.EditMask = "N0";
            this.txtTienTra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienTra.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienTra.Properties.ReadOnly = true;
            this.txtTienTra.Size = new System.Drawing.Size(248, 26);
            this.txtTienTra.TabIndex = 3;
            this.txtTienTra.EditValueChanged += new System.EventHandler(this.txtTienTra_EditValueChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(0, 37);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(66, 19);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Tiền Trả:";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.grMaPhieuNhap);
            this.groupControl1.Controls.Add(this.gbChiChi);
            this.groupControl1.Controls.Add(this.gl_NhanVien);
            this.groupControl1.Controls.Add(this.lbMaPhieuNhap);
            this.groupControl1.Controls.Add(this.gl_DoiTuong);
            this.groupControl1.Controls.Add(this.lbNhanVien);
            this.groupControl1.Controls.Add(this.lbNCC);
            this.groupControl1.Controls.Add(this.gl_nguon);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1053, 131);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Loại phiếu chi";
            // 
            // gbChiChi
            // 
            this.gbChiChi.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gbChiChi.Controls.Add(this.labelControl14);
            this.gbChiChi.Controls.Add(this.txtChiConLai);
            this.gbChiChi.Controls.Add(this.txtTongChi);
            this.gbChiChi.Controls.Add(this.labelControl12);
            this.gbChiChi.Controls.Add(this.labelControl13);
            this.gbChiChi.Controls.Add(this.txtTienChi);
            this.gbChiChi.Location = new System.Drawing.Point(394, 30);
            this.gbChiChi.Name = "gbChiChi";
            this.gbChiChi.Size = new System.Drawing.Size(338, 103);
            this.gbChiChi.TabIndex = 101;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(0, 5);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(71, 19);
            this.labelControl14.TabIndex = 94;
            this.labelControl14.Text = "Tổng Chi:";
            // 
            // txtChiConLai
            // 
            this.txtChiConLai.Location = new System.Drawing.Point(74, 66);
            this.txtChiConLai.Name = "txtChiConLai";
            this.txtChiConLai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiConLai.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtChiConLai.Properties.Appearance.Options.UseFont = true;
            this.txtChiConLai.Properties.Appearance.Options.UseForeColor = true;
            this.txtChiConLai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtChiConLai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtChiConLai.Properties.Mask.EditMask = "N0";
            this.txtChiConLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChiConLai.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtChiConLai.Properties.ReadOnly = true;
            this.txtChiConLai.Size = new System.Drawing.Size(248, 26);
            this.txtChiConLai.TabIndex = 90;
            // 
            // txtTongChi
            // 
            this.txtTongChi.Location = new System.Drawing.Point(74, 2);
            this.txtTongChi.Name = "txtTongChi";
            this.txtTongChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongChi.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtTongChi.Properties.Appearance.Options.UseFont = true;
            this.txtTongChi.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongChi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongChi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongChi.Properties.Mask.EditMask = "N0";
            this.txtTongChi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongChi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTongChi.Properties.ReadOnly = true;
            this.txtTongChi.Size = new System.Drawing.Size(248, 26);
            this.txtTongChi.TabIndex = 88;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(3, 69);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(59, 19);
            this.labelControl12.TabIndex = 98;
            this.labelControl12.Text = "Còn Lại:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(0, 37);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(65, 19);
            this.labelControl13.TabIndex = 96;
            this.labelControl13.Text = "Tiền Chi:";
            // 
            // txtTienChi
            // 
            this.txtTienChi.Location = new System.Drawing.Point(74, 34);
            this.txtTienChi.Name = "txtTienChi";
            this.txtTienChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienChi.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtTienChi.Properties.Appearance.Options.UseFont = true;
            this.txtTienChi.Properties.Appearance.Options.UseForeColor = true;
            this.txtTienChi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienChi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienChi.Properties.Mask.EditMask = "N0";
            this.txtTienChi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienChi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienChi.Properties.ReadOnly = true;
            this.txtTienChi.Size = new System.Drawing.Size(248, 26);
            this.txtTienChi.TabIndex = 89;
            this.txtTienChi.EditValueChanged += new System.EventHandler(this.txtTienChi_EditValueChanged);
            // 
            // gl_NhanVien
            // 
            this.gl_NhanVien.AllowDrop = true;
            this.gl_NhanVien.EnterMoveNextControl = true;
            this.gl_NhanVien.Location = new System.Drawing.Point(133, 64);
            this.gl_NhanVien.Name = "gl_NhanVien";
            this.gl_NhanVien.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.gl_NhanVien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gl_NhanVien.Properties.Appearance.Options.UseFont = true;
            this.gl_NhanVien.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NhanVien.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_NhanVien.Properties.AutoComplete = false;
            this.gl_NhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_NhanVien.Properties.DisplayMember = "TENNHANVIEN";
            this.gl_NhanVien.Properties.NullText = "";
            this.gl_NhanVien.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.gl_NhanVien.Properties.PopupFormSize = new System.Drawing.Size(248, 300);
            this.gl_NhanVien.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gl_NhanVien.Properties.ValueMember = "MANHANVIEN";
            this.gl_NhanVien.Properties.View = this.gridView3;
            this.gl_NhanVien.Size = new System.Drawing.Size(248, 26);
            this.gl_NhanVien.TabIndex = 3;
            this.gl_NhanVien.EditValueChanged += new System.EventHandler(this.gl_NhanVien_EditValueChanged);
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView3.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GroupRowHeight = 30;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.Caption = "Mã";
            this.gridColumn6.FieldName = "MANHANVIEN";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.Caption = "Tên";
            this.gridColumn7.FieldName = "TENNHANVIEN";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 235;
            // 
            // lbMaPhieuNhap
            // 
            this.lbMaPhieuNhap.AutoSize = true;
            this.lbMaPhieuNhap.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaPhieuNhap.Location = new System.Drawing.Point(1, 99);
            this.lbMaPhieuNhap.Name = "lbMaPhieuNhap";
            this.lbMaPhieuNhap.Size = new System.Drawing.Size(79, 19);
            this.lbMaPhieuNhap.TabIndex = 4;
            this.lbMaPhieuNhap.Text = "Mã Phiếu:";
            // 
            // gl_DoiTuong
            // 
            this.gl_DoiTuong.AllowDrop = true;
            this.gl_DoiTuong.EnterMoveNextControl = true;
            this.gl_DoiTuong.Location = new System.Drawing.Point(133, 64);
            this.gl_DoiTuong.Name = "gl_DoiTuong";
            this.gl_DoiTuong.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.gl_DoiTuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gl_DoiTuong.Properties.Appearance.Options.UseFont = true;
            this.gl_DoiTuong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_DoiTuong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_DoiTuong.Properties.AutoComplete = false;
            this.gl_DoiTuong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_DoiTuong.Properties.DisplayMember = "DT_HOTEN";
            this.gl_DoiTuong.Properties.NullText = "";
            this.gl_DoiTuong.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.gl_DoiTuong.Properties.PopupFormSize = new System.Drawing.Size(248, 300);
            this.gl_DoiTuong.Properties.ValueMember = "DT_DOITUONGID";
            this.gl_DoiTuong.Properties.View = this.gridView1;
            this.gl_DoiTuong.Size = new System.Drawing.Size(248, 26);
            this.gl_DoiTuong.TabIndex = 1;
            this.gl_DoiTuong.EditValueChanged += new System.EventHandler(this.gl_DoiTuong_EditValueChanged);
            // 
            // lbNhanVien
            // 
            this.lbNhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhanVien.Location = new System.Drawing.Point(5, 67);
            this.lbNhanVien.Name = "lbNhanVien";
            this.lbNhanVien.Size = new System.Drawing.Size(79, 19);
            this.lbNhanVien.TabIndex = 2;
            this.lbNhanVien.Text = "Nhân Viên:";
            // 
            // lbNCC
            // 
            this.lbNCC.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNCC.Location = new System.Drawing.Point(5, 67);
            this.lbNCC.Name = "lbNCC";
            this.lbNCC.Size = new System.Drawing.Size(108, 19);
            this.lbNCC.TabIndex = 71;
            this.lbNCC.Text = "Nhà Cung Cấp:";
            // 
            // gl_nguon
            // 
            this.gl_nguon.EditValue = "";
            this.gl_nguon.EnterMoveNextControl = true;
            this.gl_nguon.Location = new System.Drawing.Point(133, 32);
            this.gl_nguon.Name = "gl_nguon";
            this.gl_nguon.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gl_nguon.Properties.Appearance.Options.UseFont = true;
            this.gl_nguon.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.gl_nguon.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_nguon.Properties.AutoComplete = false;
            this.gl_nguon.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_nguon.Properties.DisplayMember = "TenLoai";
            this.gl_nguon.Properties.NullText = "";
            this.gl_nguon.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.gl_nguon.Properties.PopupFormSize = new System.Drawing.Size(248, 200);
            this.gl_nguon.Properties.ValueMember = "MaLoai";
            this.gl_nguon.Properties.View = this.gridLookUpEdit1View;
            this.gl_nguon.Size = new System.Drawing.Size(248, 26);
            this.gl_nguon.TabIndex = 1;
            this.gl_nguon.EditValueChanged += new System.EventHandler(this.gl_nguon_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(5, 35);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(107, 19);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Loại Phiếu Chi:";
            // 
            // dateNgayHenTra
            // 
            this.dateNgayHenTra.EditValue = null;
            this.dateNgayHenTra.EnterMoveNextControl = true;
            this.dateNgayHenTra.Location = new System.Drawing.Point(135, 285);
            this.dateNgayHenTra.Name = "dateNgayHenTra";
            this.dateNgayHenTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateNgayHenTra.Properties.Appearance.Options.UseFont = true;
            this.dateNgayHenTra.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateNgayHenTra.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateNgayHenTra.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateNgayHenTra.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateNgayHenTra.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateNgayHenTra.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateNgayHenTra.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateNgayHenTra.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateNgayHenTra.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 13F);
            this.dateNgayHenTra.Properties.AppearanceFocused.Options.UseFont = true;
            this.dateNgayHenTra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNgayHenTra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateNgayHenTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateNgayHenTra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateNgayHenTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateNgayHenTra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateNgayHenTra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateNgayHenTra.Size = new System.Drawing.Size(583, 26);
            this.dateNgayHenTra.TabIndex = 18;
            this.dateNgayHenTra.Visible = false;
            // 
            // lbNgayHenTra
            // 
            this.lbNgayHenTra.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayHenTra.Location = new System.Drawing.Point(5, 288);
            this.lbNgayHenTra.Name = "lbNgayHenTra";
            this.lbNgayHenTra.Size = new System.Drawing.Size(104, 19);
            this.lbNgayHenTra.TabIndex = 17;
            this.lbNgayHenTra.Text = "Ngày Hẹn Trả:";
            this.lbNgayHenTra.Visible = false;
            // 
            // txtMaPHC
            // 
            this.txtMaPHC.Enabled = false;
            this.txtMaPHC.EnterMoveNextControl = true;
            this.txtMaPHC.Location = new System.Drawing.Point(135, 30);
            this.txtMaPHC.Name = "txtMaPHC";
            this.txtMaPHC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPHC.Properties.Appearance.Options.UseFont = true;
            this.txtMaPHC.Properties.DisplayFormat.FormatString = "0,0 vnd";
            this.txtMaPHC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaPHC.Properties.EditFormat.FormatString = "0,0 vnd";
            this.txtMaPHC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaPHC.Size = new System.Drawing.Size(583, 26);
            this.txtMaPHC.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(616, 224);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(102, 19);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Chứng từ gốc.";
            // 
            // txtKemTheo
            // 
            this.txtKemTheo.EnterMoveNextControl = true;
            this.txtKemTheo.Location = new System.Drawing.Point(135, 221);
            this.txtKemTheo.Name = "txtKemTheo";
            this.txtKemTheo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKemTheo.Properties.Appearance.Options.UseFont = true;
            this.txtKemTheo.Size = new System.Drawing.Size(475, 26);
            this.txtKemTheo.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(5, 224);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(78, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Kèm Theo:";
            // 
            // txtTienChu
            // 
            this.txtTienChu.EnterMoveNextControl = true;
            this.txtTienChu.Location = new System.Drawing.Point(135, 189);
            this.txtTienChu.Name = "txtTienChu";
            this.txtTienChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienChu.Properties.Appearance.Options.UseFont = true;
            this.txtTienChu.Properties.ReadOnly = true;
            this.txtTienChu.Size = new System.Drawing.Size(583, 26);
            this.txtTienChu.TabIndex = 11;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(5, 192);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(107, 19);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Viết Bằng Chữ:";
            // 
            // txtSoTien
            // 
            this.txtSoTien.EnterMoveNextControl = true;
            this.txtSoTien.Location = new System.Drawing.Point(135, 157);
            this.txtSoTien.Name = "txtSoTien";
            this.txtSoTien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTien.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtSoTien.Properties.Appearance.Options.UseFont = true;
            this.txtSoTien.Properties.Appearance.Options.UseForeColor = true;
            this.txtSoTien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoTien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtSoTien.Properties.Mask.EditMask = "N0";
            this.txtSoTien.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoTien.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSoTien.Size = new System.Drawing.Size(583, 26);
            this.txtSoTien.TabIndex = 9;
            this.txtSoTien.EditValueChanged += new System.EventHandler(this.txtSoTien_EditValueChanged);
            this.txtSoTien.TextChanged += new System.EventHandler(this.txtSoTien_TextChanged);
            this.txtSoTien.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSoTien_KeyPress);
            this.txtSoTien.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoTien_KeyUp);
            this.txtSoTien.Leave += new System.EventHandler(this.txtSoTien_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã Phiếu :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(5, 160);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(88, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Số Tiền Chi:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(5, 128);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Lý Do:";
            // 
            // txtLydo
            // 
            this.txtLydo.EnterMoveNextControl = true;
            this.txtLydo.Location = new System.Drawing.Point(135, 125);
            this.txtLydo.Name = "txtLydo";
            this.txtLydo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLydo.Properties.Appearance.Options.UseFont = true;
            this.txtLydo.Size = new System.Drawing.Size(583, 26);
            this.txtLydo.TabIndex = 7;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.EnterMoveNextControl = true;
            this.txtDiaChi.Location = new System.Drawing.Point(135, 93);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Properties.Appearance.Options.UseFont = true;
            this.txtDiaChi.Size = new System.Drawing.Size(583, 26);
            this.txtDiaChi.TabIndex = 5;
            // 
            // pnthongtinphieu
            // 
            this.pnthongtinphieu.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnthongtinphieu.AppearanceCaption.Options.UseFont = true;
            this.pnthongtinphieu.Controls.Add(this.cbNguonChi);
            this.pnthongtinphieu.Controls.Add(this.lbNguonChi);
            this.pnthongtinphieu.Controls.Add(this.dateNgayHenTra);
            this.pnthongtinphieu.Controls.Add(this.lbNgayHenTra);
            this.pnthongtinphieu.Controls.Add(this.txtMaPHC);
            this.pnthongtinphieu.Controls.Add(this.labelControl7);
            this.pnthongtinphieu.Controls.Add(this.txtKemTheo);
            this.pnthongtinphieu.Controls.Add(this.labelControl6);
            this.pnthongtinphieu.Controls.Add(this.txtTienChu);
            this.pnthongtinphieu.Controls.Add(this.labelControl5);
            this.pnthongtinphieu.Controls.Add(this.txtSoTien);
            this.pnthongtinphieu.Controls.Add(this.label2);
            this.pnthongtinphieu.Controls.Add(this.labelControl4);
            this.pnthongtinphieu.Controls.Add(this.txtLydo);
            this.pnthongtinphieu.Controls.Add(this.labelControl1);
            this.pnthongtinphieu.Controls.Add(this.txtDiaChi);
            this.pnthongtinphieu.Controls.Add(this.labelControl2);
            this.pnthongtinphieu.Controls.Add(this.txtNCC);
            this.pnthongtinphieu.Controls.Add(this.lbNCC1);
            this.pnthongtinphieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnthongtinphieu.Location = new System.Drawing.Point(0, 185);
            this.pnthongtinphieu.Name = "pnthongtinphieu";
            this.pnthongtinphieu.Size = new System.Drawing.Size(1057, 319);
            this.pnthongtinphieu.TabIndex = 107;
            this.pnthongtinphieu.Text = "Thông tin phiếu chi";
            // 
            // cbNguonChi
            // 
            this.cbNguonChi.EditValue = "Quỹ Tiền Mặt";
            this.cbNguonChi.EnterMoveNextControl = true;
            this.cbNguonChi.Location = new System.Drawing.Point(135, 253);
            this.cbNguonChi.Name = "cbNguonChi";
            this.cbNguonChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbNguonChi.Properties.Appearance.Options.UseFont = true;
            this.cbNguonChi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbNguonChi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbNguonChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNguonChi.Properties.Items.AddRange(new object[] {
            "Quỹ Tiền Mặt",
            "Nguồn Khác"});
            this.cbNguonChi.Size = new System.Drawing.Size(583, 26);
            this.cbNguonChi.TabIndex = 15;
            // 
            // lbNguonChi
            // 
            this.lbNguonChi.AutoSize = true;
            this.lbNguonChi.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNguonChi.Location = new System.Drawing.Point(5, 256);
            this.lbNguonChi.Name = "lbNguonChi";
            this.lbNguonChi.Size = new System.Drawing.Size(90, 19);
            this.lbNguonChi.TabIndex = 14;
            this.lbNguonChi.Text = "Nguồn Chi:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(5, 96);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(57, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Địa Chỉ:";
            // 
            // txtNCC
            // 
            this.txtNCC.EnterMoveNextControl = true;
            this.txtNCC.Location = new System.Drawing.Point(135, 61);
            this.txtNCC.Name = "txtNCC";
            this.txtNCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNCC.Properties.Appearance.Options.UseFont = true;
            this.txtNCC.Size = new System.Drawing.Size(583, 26);
            this.txtNCC.TabIndex = 3;
            this.txtNCC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_TenKH_KeyPress);
            // 
            // lbNCC1
            // 
            this.lbNCC1.AutoSize = true;
            this.lbNCC1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNCC1.Location = new System.Drawing.Point(5, 64);
            this.lbNCC1.Name = "lbNCC1";
            this.lbNCC1.Size = new System.Drawing.Size(42, 19);
            this.lbNCC1.TabIndex = 2;
            this.lbNCC1.Text = "Tên:";
            // 
            // panel4
            // 
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.header);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1057, 50);
            this.panel4.TabIndex = 111;
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl4);
            this.header.Controls.Add(this.panelControl5);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1057, 52);
            this.header.TabIndex = 2;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl4.Controls.Add(this.pictureEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(107, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(950, 52);
            this.panelControl4.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(946, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSize = true;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.title);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl5.Location = new System.Drawing.Point(81, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(26, 52);
            this.panelControl5.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(14, 32);
            this.title.TabIndex = 1;
            this.title.Text = "[]";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // Frm_LapPhieuChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 741);
            this.Controls.Add(this.pnHTTT);
            this.Controls.Add(this.pnthongtinphieu);
            this.Controls.Add(this.pnDoiTuong);
            this.Controls.Add(this.panel4);
            this.Name = "Frm_LapPhieuChi";
            this.Text = "Lập phiếu chi";
            ((System.ComponentModel.ISupportInitialize)(this.pnHTTT)).EndInit();
            this.pnHTTT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).EndInit();
            this.pnBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChuyenKhoan)).EndInit();
            this.pnChuyenKhoan.ResumeLayout(false);
            this.pnChuyenKhoan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCKChi.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCKChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNHNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCKTenNganHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTienMat)).EndInit();
            this.pnTienMat.ResumeLayout(false);
            this.pnTienMat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTMNgayChi.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTMNgayChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TMTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHinhthuc)).EndInit();
            this.pnHinhthuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkTienMat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chHTTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grMaPhieuNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDoiTuong)).EndInit();
            this.pnDoiTuong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbNo)).EndInit();
            this.gbNo.ResumeLayout(false);
            this.gbNo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbChiChi)).EndInit();
            this.gbChiChi.ResumeLayout(false);
            this.gbChiChi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiConLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_DoiTuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_nguon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayHenTra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayHenTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPHC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemTheo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLydo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnthongtinphieu)).EndInit();
            this.pnthongtinphieu.ResumeLayout(false);
            this.pnthongtinphieu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbNguonChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNCC.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton bnt_luu;
        private DevExpress.XtraEditors.GroupControl pnHTTT;
        private DevExpress.XtraEditors.PanelControl pnChuyenKhoan;
        private DevExpress.XtraEditors.TextEdit txtSoGiaoDich;
        private DevExpress.XtraEditors.LabelControl lbSoGiaoDich;
        private DevExpress.XtraEditors.DateEdit dateCKChi;
        private DevExpress.XtraEditors.LabelControl lbCkChi;
        private DevExpress.XtraEditors.TextEdit txtSoTaiKhoanNhan;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtTenNHNhan;
        private System.Windows.Forms.Label lbtentaiKhoanNhan;
        private DevExpress.XtraEditors.TextEdit txtSoTaiKhoanChuyen;
        private DevExpress.XtraEditors.LabelControl lbSoTaiKhoanChuyen;
        private DevExpress.XtraEditors.TextEdit txtCKTenNganHang;
        private System.Windows.Forms.Label lbCKTenNH;
        private DevExpress.XtraEditors.PanelControl pnTienMat;
        private DevExpress.XtraEditors.DateEdit dateTMNgayChi;
        private DevExpress.XtraEditors.LabelControl lbTMNgayChi;
        private DevExpress.XtraEditors.TextEdit txt_TMTen;
        private System.Windows.Forms.Label lbTMTen;
        private DevExpress.XtraEditors.PanelControl pnHinhthuc;
        private DevExpress.XtraEditors.CheckEdit chkTienMat;
        private DevExpress.XtraEditors.CheckEdit chHTTT;
        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn MaLoai;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhieu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit grMaPhieuNhap;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn DT_HOTEN;
        private DevExpress.XtraEditors.PanelControl pnDoiTuong;
        private DevExpress.XtraEditors.GridLookUpEdit gl_DoiTuong;
        private DevExpress.XtraEditors.GridLookUpEdit gl_nguon;
        private DevExpress.XtraEditors.TextEdit txtNo;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtTienTra;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTongNo;
        private DevExpress.XtraEditors.LabelControl lbNCC;
        private System.Windows.Forms.Label lbMaPhieuNhap;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dateNgayHenTra;
        private DevExpress.XtraEditors.LabelControl lbNgayHenTra;
        private DevExpress.XtraEditors.TextEdit txtMaPHC;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtKemTheo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTienChu;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtSoTien;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLydo;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.GroupControl pnthongtinphieu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNCC;
        private System.Windows.Forms.Label lbNCC1;
        private DevExpress.XtraEditors.SimpleButton btn_HuyBo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.GridLookUpEdit gl_NhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.LabelControl lbNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txtChiConLai;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtTienChi;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtTongChi;
        private DevExpress.XtraEditors.ComboBoxEdit cbNguonChi;
        private System.Windows.Forms.Label lbNguonChi;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.SimpleButton btnLayPhieuDeNghiChi;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl gbNo;
        private DevExpress.XtraEditors.PanelControl gbChiChi;
        private DevExpress.XtraEditors.PanelControl pnBottom;





    }
}