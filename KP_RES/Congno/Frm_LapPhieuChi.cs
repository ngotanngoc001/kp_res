﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_LapPhieuChi : DevExpress.XtraEditors.XtraForm
    {
        int close = 0;
        bool checkNgay = false;
        public static string tmp_MAPDNDC = "";
        bool flag_DaLayPhieuDNDC = false;
        public Frm_LapPhieuChi()
        {
            InitializeComponent();
            gl_nguon.EditValue = null;
            LoadData();
            gl_DoiTuong.Focus();
            panel4.Visible = false;
        }
        public Frm_LapPhieuChi(string maNCC, string maPhieuNhap, int flag)//nếu flag=0: lập phiếu chi,flag:1 lập phiếu chi nha cung từ công nợ nhà cung cấp,flag=2; chi trả tiền trả hàng//3 chi khách hàng
        {
            InitializeComponent();
            gl_nguon.EditValue = null;
            LoadData();


            if (flag == 1)
            {
                grMaPhieuNhap.EditValue = maPhieuNhap;

            }
            if (flag == 2)
            {
                gl_nguon.EditValue = "06";
            }
            if (flag == 3)
            {
                gl_nguon.EditValue = "07";

            }
            gl_DoiTuong.EditValue = maNCC;
            grMaPhieuNhap.EditValue = maPhieuNhap;
            gl_DoiTuong.Focus();
            panel4.Visible = true;
            title.Text = this.Text;
            close = 1;
        }

        private void LoadData()
        {
            string sqlcheck = "select * from CN_LOAI_CONGNO";
            DataTable dtcheck = clsMain.ReturnDataTable(sqlcheck);
            if (dtcheck.Rows.Count == 0)
            {
                string sqlInsert = "insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('01',N'Thu Khách Hàng',N'thu từ nguồn khách hang','01') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('02',N'Thu Khác',N'thu từ nguồn khác','01')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('03',N'Chi Nhà Cung Cấp',N'Chi cho nhà cung cấp','02')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('04',N'Chi Nhân Viên',N'Chi cho nhân viên','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('05',N'Chi Khác',N'Chi cho các dịch vụ khác','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('06',N'Chi Trả Hàng',N'Chi tiền trả hàng','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('07',N'Chi Khách Hàng',N'Chi tiền cho khách hàng','02')  ";
                clsMain.ExecuteSQL(sqlInsert);
            }

            DataTable dt = clsMain.ReturnDataTable("select * from CN_LOAI_CONGNO where LoaiThuChi='02'   AND SUDUNG=1");
            gl_nguon.Properties.DataSource = dt;
            gl_nguon.EditValue = dt.Rows[0]["MaLoai"].ToString();

            txtMaPHC.Text = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_CHI_NS()").Rows[0][0].ToString();
            lbNhanVien.Visible = gl_NhanVien.Visible = false;
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;

            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;

            }
            dateTMNgayChi.EditValue = dateCKChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
        }

        private void Reset()
        {
            txt_TMTen.Text = txtNCC.Text = txtDiaChi.Text = txtLydo.Text = txtKemTheo.Text = txtTienChu.Text = string.Empty;
            txtCKTenNganHang.Text = txtSoTaiKhoanChuyen.Text = txtTenNHNhan.Text = txtSoTaiKhoanNhan.Text = txtSoGiaoDich.Text = string.Empty;
            txtNo.Text = "0";
            txtTongNo.Text = "0";
            txtSoTien.Text = "0";
            txtTienTra.Text = "0";
            txtTongChi.Text = "0";
            txtTienChi.Text = "0";
            txtChiConLai.Text = "0";
            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
            dateNgayHenTra.EditValue = null;
            cbNguonChi.SelectedIndex = 0;
            dateCKChi.EditValue = dateTMNgayChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
        }

        private void btn_HuyBo_Click(object sender, EventArgs e)
        {
            KhoaControl(false);
            Reset();
            gl_DoiTuong.EditValue = null;
            gl_NhanVien.EditValue = null;
            grMaPhieuNhap.EditValue = null;
            DateTime ngayhentra = clsMain.GetServerDate().AddDays(3);
            //NgocSang.CongNoKhachHangHoaDon("48", "2129102014B0001", 1439000, 439000, ngayhentra);
        }

        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (gl_nguon.EditValue.ToString() == "03")
                {
                    if (gl_DoiTuong.Text == "")
                    {
                        e.Handled = true;
                        throw new Exception("Mời bạn chọn khách hàng");
                    }
                    if (grMaPhieuNhap.EditValue.ToString() == "")
                    {
                        e.Handled = true;
                        throw new Exception("Mời bạn chọn Phiếu Nhập");
                    }
                }
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTien_Leave(object sender, EventArgs e)
        {
            try
            {

                if (txtSoTien.Text != "")
                    txtTienChu.Text = cls_KHTT.ChuyenSo(decimal.Parse(txtSoTien.Text.Replace(",", "")).ToString()) + "đồng";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSoTien_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (gl_nguon.EditValue.ToString() == "07" && gl_DoiTuong.EditValue.ToString() != "" && grMaPhieuNhap.EditValue.ToString() != "")
                {
                    if (txtSoTien.Text == "")
                    {
                        txtTienChi.Text = "";
                        txtChiConLai.Text = "";
                    }
                    else
                    {

                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongChi.Text.Replace(",", "")))
                        {
                            txtSoTien.Focus();
                            if (e.KeyValue != 13)
                            {
                                throw new Exception("Số tiền Chi không được lớn hơn số tiền được chi là: " + txtTongChi.Text);
                            }
                        }
                        txtTienChi.Text = txtSoTien.Text;

                    }
                }
                if ((gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06") && gl_DoiTuong.EditValue.ToString() != "" && grMaPhieuNhap.EditValue.ToString() != "")
                {
                    if (txtSoTien.Text == "")
                    {
                        txtTienTra.Text = "";
                        txtTienChu.Text = "";
                    }
                    else
                    {

                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongNo.Text.Replace(",", "")))
                        {
                            txtSoTien.Focus();
                            if (e.KeyValue != 13)
                            {
                                throw new Exception("Số tiền Chi không được lớn hơn số tiền nợ là: " + txtTongNo.Text);
                            }
                        }
                        txtTienTra.Text = txtSoTien.Text;

                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtTienTra_EditValueChanged(object sender, EventArgs e)
        {
            if (txtTienTra.Text == "")
            {
                txtNo.Text = "";
            }
            else
            {
                txtNo.Text = string.Format("{0:#,0}", (decimal.Parse(txtTongNo.Text) - decimal.Parse(txtTienTra.Text)));
            }
        }

        private void txtSoTien_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtSoTien.Text != "")
                {
                    if (checkNgay == true && (gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06"))
                    {
                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) == decimal.Parse(txtTongNo.Text))
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                        }
                        else
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                        }
                    }
                    txtTienChu.Text = cls_KHTT.ChuyenSo(decimal.Parse(txtSoTien.Text.Replace(",", "")).ToString()) + " đồng";
                    if (gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06")
                    {
                        txtTienTra.Text = txtSoTien.Text;
                    }
                    if (gl_nguon.EditValue.ToString() == "07")
                    {
                        txtTienChi.Text = txtSoTien.Text;
                    }
                    if (gl_nguon.EditValue.ToString() == "06")
                    {
                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) == decimal.Parse(txtTongNo.Text))
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                        }
                        else
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                        }
                    }

                }
                else
                {
                    txtTienChu.Text = "";
                    txtTienTra.Text = txtNo.Text = string.Empty;
                }
            }
            catch
            {
                XtraMessageBox.Show("Số Nhập vào quá giới hạn cho phép!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            //lấy cấu hình duyệt phiếu chi
            if (tmp_MAPDNDC == "")
            {
                string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='CAUHINHPHIEUCHI' and GIATRI=0";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count == 0)
                {
                    XtraMessageBox .Show ("Phiếu chi cần phải được duyệt trước","Thông báo",MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            //
            if (DialogResult.No == (XtraMessageBox.Show("Bạn có chắc muốn tạo phiếu chi này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
            {
                return;
            }
            try
            {
                if (gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06" || gl_nguon.EditValue.ToString() == "07")
                {
                    if (gl_DoiTuong.Text == "")
                    {
                        gl_DoiTuong.Focus();
                        throw new Exception("Mời Chọn Nhà Cung Cấp");
                    }

                    if (grMaPhieuNhap.EditValue.ToString() == "")
                    {
                        grMaPhieuNhap.Focus();
                        throw new Exception("Mời Chọn Mã Phiếu");
                    }
                    if (txtSoTien.Text == "" || long.Parse(txtSoTien.Text.Replace(",", "")) <= 0)
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền chi");
                    }
                    if (gl_nguon.EditValue.ToString() == "07")
                    {
                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongChi.Text.Replace(",", "")))
                        {
                            txtSoTien.Focus();
                            throw new Exception("Số tiền Chi không được lớn hơn số tiền được chi là: " + txtTongChi.Text);
                        }
                    }
                    else
                    {
                        if (decimal.Parse(txtSoTien.Text.Replace(",", "")) > decimal.Parse(txtTongNo.Text.Replace(",", "")))
                        {
                            txtSoTien.Focus();
                            throw new Exception("Số tiền Chi không được lớn hơn số tiền nợ là: " + txtTongNo.Text);
                        }
                        if (dateNgayHenTra.Visible == true)
                        {
                            if (dateNgayHenTra.EditValue == null)
                            {
                                dateNgayHenTra.Focus();
                                throw new Exception("Mời Chọn Ngày Hẹn Trả");
                            }
                            if (dateNgayHenTra.DateTime < KP_UserManagement.clsMain.GetServerDate().Date)
                            {
                                dateNgayHenTra.Focus();

                                throw new Exception("Ngày Hẹn Trả không được nhỏ ngày hiện tại");

                            }
                        }
                    }

                }
                else if (gl_nguon.EditValue.ToString() == "04")
                {
                    if (gl_NhanVien.Text == "")
                    {
                        gl_NhanVien.Focus();
                        throw new Exception("Mời Chọn Nhân Viên ");
                    }
                    if (txtSoTien.Text == ""||long.Parse (txtSoTien.Text.Replace (",",""))<=0)
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền chi");
                    }
                }
                else if (gl_nguon.EditValue.ToString() == "08" || gl_nguon.EditValue.ToString() == "09" || gl_nguon.EditValue.ToString() == "10")
                {
                    if (txtNCC.Text == "")
                    {
                        txtNCC.Focus();
                        throw new Exception("Mời Nhập họ tên người nộp tiền");
                    }
                    if (txtSoTien.Text == "" || long.Parse(txtSoTien.Text.Replace(",", "")) <= 0)
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền chi");
                    }
                }
                else
                {
                    if (txtNCC.Text == "")
                    {
                        txtNCC.Focus();
                        throw new Exception("Mời Nhập họ tên người nộp tiền");
                    }
                    if (txtSoTien.Text == "" || long.Parse(txtSoTien.Text.Replace(",", "")) <= 0)
                    {
                        txtSoTien.Focus();
                        throw new Exception("Mời Nhập số tiền chi");
                    }
                }

                if (chkTienMat.Checked)
                {
                    if (txt_TMTen.Text == "")
                    {
                        txt_TMTen.Focus();
                        throw new Exception("Mời Nhập Tên Người Nhận Tiền");
                    }

                }
                if (chHTTT.Checked)
                {
                    if (txtSoGiaoDich.Text == "")
                    {
                        txtSoGiaoDich.Focus();
                        throw new Exception("Mời Nhập Số Giao Dịch");
                    }

                }
               

                double tongtien = 0;
                // bool httt = true;
                string sql = ""; string sqlChiTiet = ""; string sql1 = ""; string sql2 = "";
                bool nguonchi;//true là quỹ tiền mặt//false là nguồn khác
                DataTable dt = clsMain.ReturnDataTable("select top(1) TONGTIEN from CN_CONGNO where SUDUNG='true' order by NGAYLAPPHIEU desc");
                if (cbNguonChi.Visible)
                {
                    if (cbNguonChi.SelectedIndex == 0)
                    {
                        nguonchi = true;
                        if (dt.Rows.Count > 0)
                        {
                            tongtien = double.Parse(dt.Rows[0][0].ToString());
                            if (double.Parse(txtSoTien.Text.Replace(",", "")) > tongtien)
                                throw new Exception(string.Format("Vui Lòng Chi ít hơn số tiền quỹ đang có là:{0:0,0} VND", tongtien));
                            if (chHTTT.Checked == false)
                            {
                                tongtien = tongtien - double.Parse(txtSoTien.Text);
                                //httt = true;
                            }
                            else
                            {
                                // httt = false;
                            }
                        }
                        else
                        {
                            throw new Exception("Không Chi được vì Số tiền Quỹ Đang 0 Đồng");
                        }

                    }
                    else
                    {
                        nguonchi = false;
                        if (dt.Rows.Count > 0)
                        {
                            tongtien = double.Parse(dt.Rows[0][0].ToString());
                        }
                        else
                        {
                            tongtien = 0;
                        }
                    }
                }
                else
                {
                    nguonchi = false;
                    if (dt.Rows.Count > 0)
                    {
                        tongtien = double.Parse(dt.Rows[0][0].ToString());
                    }
                    else
                    {
                        tongtien = 0;
                    }
                }
                string soTaiKhoanChuyen;
                if (txtSoTaiKhoanChuyen.Text == "")
                {
                    soTaiKhoanChuyen = "";
                }
                else
                {
                    soTaiKhoanChuyen = txtSoTaiKhoanChuyen.Text;
                }
                string soTaiKhoanNhan;
                if (txtSoTaiKhoanNhan.Text == "")
                {
                    soTaiKhoanNhan = "";
                }
                else
                {
                    soTaiKhoanNhan = txtSoTaiKhoanNhan.Text;
                }
                string soGiaoDich;
                if (txtSoGiaoDich.Text == "")
                {
                    soGiaoDich = "";
                }
                else
                {
                    soGiaoDich = txtSoGiaoDich.Text;
                }

                if (gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06")//chi nhà cung cấp
                {
                    if (grMaPhieuNhap.EditValue.ToString() == "")
                    {
                        grMaPhieuNhap.Focus();
                        throw new Exception("Bạn hãy chọn phiếu nhập kho");
                    }

                    //Tính tiền Công Nợ
                    string sSQL = "";
                    sSQL += "Update NHACUNGCAP " + "\n";
                    sSQL += "Set TIEN_CONGNO=ISNULL(TIEN_CONGNO,0) - " + txtSoTien.Text.Replace(",", "") + "\n";
                    sSQL += " Where MA=" + clsMain.SQLString(gl_DoiTuong.EditValue.ToString());
                    clsMain.ExecuteSQL(sSQL);

                    string maloai;
                    if (gl_nguon.EditValue.ToString() == "03")
                    {
                        maloai = "03";
                    }
                    else
                    {
                        maloai = "06";
                    }
                    if (chkTienMat.Checked)
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString(), clsUserManagement.ReturnMaNVOfUserLogin(), maloai, txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "01", string.Format("{0: yyyyMMdd HH:mm:ss}", dateTMNgayChi.EditValue), "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);
                    }
                    else
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString(), clsUserManagement.ReturnMaNVOfUserLogin(), maloai, txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "02", string.Format("{0: yyyyMMdd HH:mm:ss}", dateCKChi.EditValue), txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);
                    }
                    decimal tienNo;
                    string dotTT;
                    DataTable dtNo = clsMain.ReturnDataTable("select  ct.SOTIENCONLAI,ct.DOTTHANHTOAN,ct.TONGTIENCANTT  from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where pk.SOPHIEU='" + grMaPhieuNhap.EditValue + "' and FLAG='true'");
                    if (dtNo.Rows[0][1].ToString() == "Tạm")
                    {
                        tienNo = decimal.Parse(dtNo.Rows[0][2].ToString());
                        if (txtNo.Text == "0")
                        {
                            dotTT = "0";
                        }
                        else
                        {
                            dotTT = 0.ToString();
                        }
                    }
                    else
                    {
                        if (dtNo.Rows[0][1].ToString().Length > 1)
                        {
                            dotTT = (long.Parse(dtNo.Rows[0][1].ToString().Substring(0, 2).Replace("-", "")) + 1).ToString();
                        }
                        else
                        {
                            dotTT = (long.Parse(dtNo.Rows[0][1].ToString().Substring(0, 1).Replace("-", "")) + 1).ToString();
                        }
                        if (gl_nguon.EditValue.ToString() == "06")
                        {
                            dotTT += "-Chi Tiền trả hàng";
                        }
                        tienNo = decimal.Parse(dtNo.Rows[0][0].ToString());

                    }

                    bool _flag = true;
                    if (Math.Abs(decimal.Parse(txtSoTien.Text.Replace(",", ""))) == Math.Abs(tienNo))
                        _flag = false;
                    if (clsMain.ReturnDataTable("select top(1) * from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString() + "'").Rows.Count > 0)
                    {
                        string MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue.ToString() + "'").Rows[0][0].ToString();
                        sql1 = string.Format("update CHITIETTHANHTOAN set FLAG='false' where  PK_PHIEUKHOID='{0}' and flag='true'", MaKho);
                        clsMain.ReturnDataTable(sql1);
                    }
                    if (gl_nguon.EditValue.ToString() == "06")
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}',N'{4}','{5: dd/MM/yyyy HH:mm:ss}','{6}','{7}',", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString(), tienNo, "-" + txtSoTien.Text.Replace(",", ""), (tienNo + decimal.Parse(txtSoTien.Text)), dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "True");

                    }
                    else
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN(PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}',N'{4}','{5: dd/MM/yyyy HH:mm:ss}','{6}','{7}',", clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString(), tienNo, txtSoTien.Text.Replace(",", ""), (tienNo - decimal.Parse(txtSoTien.Text)), dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "false");
                    }

                    if (dateNgayHenTra.EditValue == null)
                    {
                        string MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                        string sqlngayhentra = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + grMaPhieuNhap.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                        DataTable dtngayhentra = clsMain.ReturnDataTable(sqlngayhentra);
                        string ngayhentra = "";
                        if (dtngayhentra.Rows.Count > 0)
                        {
                            ngayhentra = string.Format("'{0:dd/MM/yyyy}'", dtngayhentra.Rows[0][0]);
                        }
                        else
                        {
                            ngayhentra = "NULL";
                        }
                        sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='false' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + " and DOTTHANHTOAN!=N'Tạm'";
                        if (decimal.Parse(txtNo.Text.Replace(",", "")) == 0)
                        {
                            sqlChiTiet += "NULL,null,'false')";
                        }
                        else
                        {
                            sqlChiTiet += "'" + ngayhentra + "',null,'false')";
                        }
                    }
                    else
                    {
                        if (decimal.Parse(txtNo.Text.Replace(",", "")) == 0)
                        {
                            sqlChiTiet += "NULL,null,'false')";
                        }
                        else
                        {
                            //sqlChiTiet += "'" + ngayhentra + "',null,'false')";
                            sqlChiTiet += "'" + string.Format("{0:dd/MM/yyyy}", dateNgayHenTra.EditValue) + "',null,'false')";
                        }
                        // sqlChiTiet += "'" + string.Format("{0:dd/MM/yyyy}", dateNgayHenTra.EditValue) + "',null,'false')";
                        string MaKho = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                        string sqlngayhentra = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + grMaPhieuNhap.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                        DataTable dtngayhentra = clsMain.ReturnDataTable(sqlngayhentra);
                        string ngayhentra = "";
                        if (dtngayhentra.Rows.Count > 0)
                        {
                            ngayhentra = string.Format("'{0:dd/MM/yyyy}'", dtngayhentra.Rows[0][0]);
                        }
                        else
                        {
                            ngayhentra = "NULL";
                        }
                        if (clsMain.ReturnDataTable(sqlngayhentra).Rows.Count == 0)
                        {
                            sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='false' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + "";
                        }
                        else
                        {

                            if (int.Parse(string.Format("{0:yyyyMMdd}", clsMain.ReturnDataTable(sqlngayhentra).Rows[0][0])) < int.Parse(string.Format("{0:yyyyMMdd}", KP_UserManagement.clsMain.GetServerDate())))
                            {

                                sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='true' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + "";
                            }
                            else
                            {
                                sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=" + ngayhentra + ",ISTREHEN='false' where PK_PHIEUKHOID='" + MaKho + "' and NGAYHENTRA=" + ngayhentra + "";

                            }
                        }
                    }

                }
                else if (gl_nguon.EditValue.ToString() == "04")//chi nhân viên
                {
                    if (chkTienMat.Checked)
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, "", "", "", clsUserManagement.ReturnMaNVOfUserLogin(), "04", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, gl_NhanVien.EditValue, "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);

                    }
                    else
                    {
                        sql = string.Format("exec  sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, "", "", "", clsUserManagement.ReturnMaNVOfUserLogin(), "04", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, gl_NhanVien.EditValue, "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);

                    }
                }
                else if (gl_nguon.EditValue.ToString() == "08" || gl_nguon.EditValue.ToString() == "09" || gl_nguon.EditValue.ToString() == "10")//chi trả lương,chi thưởng,chi cho hợp đồng
                {
                    string maloai;
                    if (gl_nguon.EditValue.ToString() == "08")
                    {
                        maloai = "08";
                    }
                    else if (gl_nguon.EditValue.ToString() == "09")
                    {
                        maloai = "09";
                    }
                    else
                    {
                        maloai = "10";
                    }
                    if (chkTienMat.Checked)
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, "", "", "", clsUserManagement.ReturnMaNVOfUserLogin(), maloai, txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);

                    }
                    else
                    {
                        sql = string.Format("exec  sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, "", "", "", clsUserManagement.ReturnMaNVOfUserLogin(), maloai, txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);

                    }
                }
                else if (gl_nguon.EditValue.ToString() == "07")//chi khách hàng
                {
                    DataTable dtCheckMaPhieu = clsMain.ReturnDataTable("select MA_HOADON from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "'");
                    string makhoID = "";
                    if (grMaPhieuNhap.EditValue.ToString() == "")
                    {
                        grMaPhieuNhap.Focus();
                        throw new Exception("Bạn hãy chọn mã phiếu kho");
                    }


                    if (chkTienMat.Checked)
                    {
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            makhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                            sql = string.Format("exec  sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", makhoID, clsUserManagement.ReturnMaNVOfUserLogin(), "07", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);
                        }
                        else
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, grMaPhieuNhap.EditValue, "", clsUserManagement.ReturnMaNVOfUserLogin(), "07", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);
                        }

                    }
                    else
                    {
                        if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                        {
                            makhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                            sql = string.Format("exec  sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", makhoID, clsUserManagement.ReturnMaNVOfUserLogin(), "07", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);
                        }
                        else
                        {
                            sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, grMaPhieuNhap.EditValue, "", clsUserManagement.ReturnMaNVOfUserLogin(), "07", txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);
                        }

                    }
                    string sqlngayhentra = "";
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        sqlngayhentra = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + grMaPhieuNhap.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                    }
                    else
                    {
                        sqlngayhentra = "select top(1) ct.NGAYHENTRA  from  CHITIETTHANHTOAN ct inner join HOADON hd on ct.MA_HOADON=hd.MA_HOADON where hd.MA_HOADON='" + grMaPhieuNhap.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                    }

                    DataTable dtngayhentra = clsMain.ReturnDataTable(sqlngayhentra);
                    string ngayhentra = "";
                    if (dtngayhentra.Rows.Count > 0)
                    {
                        ngayhentra = string.Format("'{0:dd/MM/yyyy}'", dtngayhentra.Rows[0][0]);
                    }
                    else
                    {
                        ngayhentra = "NULL";
                    }
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        sql1 = "update CHITIETTHANHTOAN set flag='false' where  PK_PHIEUKHOID='" + makhoID + "' and  flag='true'";
                        sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=NULL,ISTREHEN='false' where PK_PHIEUKHOID='" + makhoID + "' and NGAYHENTRA=" + ngayhentra + "";
                    }
                    else
                    {
                        sql1 = "update CHITIETTHANHTOAN set flag='false' where  MA_HOADON='" + grMaPhieuNhap.EditValue + "' and  flag='true'";
                        sql2 = "update CHITIETTHANHTOAN set NGAYHENTRA=NULL,NGAYTREHEN=NULL,ISTREHEN='false' where MA_HOADON='" + grMaPhieuNhap.EditValue + "' and NGAYHENTRA=" + ngayhentra + "";
                    }

                    decimal sotienconlai = 0;
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        sotienconlai = decimal.Parse(clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + makhoID + "'order by CTTT_STT desc").Rows[0][0].ToString());
                    }
                    else
                    {
                        sotienconlai = decimal.Parse(clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "'order by CTTT_STT desc").Rows[0][0].ToString());
                    }

                    decimal tongno = sotienconlai + decimal.Parse(txtSoTien.Text.Replace(",", ""));
                    string dotTT = "0";
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        dotTT = clsMain.ReturnDataTable("select top(1) DOTTHANHTOAN from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + makhoID + "'order by CTTT_STT desc").Rows[0][0].ToString();
                    }
                    else
                    {
                        dotTT = clsMain.ReturnDataTable("select top(1) DOTTHANHTOAN from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "'order by CTTT_STT desc").Rows[0][0].ToString();
                    }

                    //dotTT = (int.Parse(dotTT.Substring(0, 2).Replace("-","")) + 1).ToString();
                    if (dotTT.ToString().Length > 1)
                    {
                        dotTT = (long.Parse(dotTT.ToString().Substring(0, 2).Replace("-", "")) + 1).ToString();
                    }
                    else
                    {
                        dotTT = (long.Parse(dotTT.ToString().Substring(0, 1).Replace("-", "")) + 1).ToString();
                    }
                    dotTT += "-Chi Tiền Khách Hàng";
                    bool FlagXong = true;
                    if (tongno == 0)
                    {
                        FlagXong = false;
                    }
                    if (dtCheckMaPhieu.Rows.Count == 0)//nếu thu theo phiếu xuất
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN(PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}',N'{4}','{5: yyyyMMdd HH:mm:ss}','{6}','{7}',{8},{9},'{10}')", makhoID, sotienconlai, txtSoTien.Text.Replace(",", ""), tongno, dotTT, KP_UserManagement.clsMain.GetServerDate(), FlagXong, "true", ngayhentra, "NULL", "false");
                    }
                    else
                    {
                        sqlChiTiet = string.Format("insert into CHITIETTHANHTOAN(MA_HOADON,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}',N'{4}','{5: yyyyMMdd HH:mm:ss}','{6}','{7}',{8},{9},'{10}')", grMaPhieuNhap.EditValue, sotienconlai, txtSoTien.Text.Replace(",", ""), tongno, dotTT, KP_UserManagement.clsMain.GetServerDate(), FlagXong, "true", ngayhentra, "NULL", "false");
                    }

                }

                else
                {
                    if (chkTienMat.Checked)
                    {
                        sql = string.Format("exec  sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", "", clsUserManagement.ReturnMaNVOfUserLogin(), gl_nguon.EditValue.ToString(), txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "01", dateTMNgayChi.EditValue, "", "", "", "", "", txt_TMTen.Text, nguonchi, tmp_MAPDNDC);
                    }
                    else
                    {
                        sql = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14: yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}',@MAPDNDC='{22}'", txtMaPHC.Text, gl_DoiTuong.EditValue, "", "", clsUserManagement.ReturnMaNVOfUserLogin(), gl_nguon.EditValue.ToString(), txtLydo.Text, txtSoTien.Text.Replace(",", ""), txtKemTheo.Text, tongtien, txtNCC.Text, txtDiaChi.Text, "", "02", dateCKChi.EditValue, txtCKTenNganHang.Text, soTaiKhoanChuyen, txtTenNHNhan.Text, soTaiKhoanNhan, soGiaoDich, "", nguonchi, tmp_MAPDNDC);
                    }
                }



                if (clsMain.ExecuteSQL(sql))//insert vào bản công nợ
                {
                    if (flag_DaLayPhieuDNDC && tmp_MAPDNDC != "")
                    {
                        string sSQL = "Update CN_CONGNO_PHIEUDENGHIDUYETCHI Set DALAY=1 Where MAPDNDC=" + clsMain.SQLString(tmp_MAPDNDC);
                        clsMain.ExecuteSQL(sSQL);
                    }
                    if (sqlChiTiet != "")//insert vào bản chi tiết
                    {
                        if (sql1 != "")// update bảng chittiett nếu flag=true
                            clsMain.ReturnDataTable(sql1);
                        if (sql2 != "")//update bảng chitiet cho ngày hẹn trả
                            clsMain.ReturnDataTable(sql2);

                        clsMain.ExecuteSQL(sqlChiTiet);
                    }

                    try
                    {
                        if (chkTienMat.Checked)
                        {
                            DataTable dtInBaoCao = new DataTable();
                            dtInBaoCao.Columns.Add("TongCong", typeof(string));
                            dtInBaoCao.Columns.Add("SoPhieu", typeof(string));
                            dtInBaoCao.Columns.Add(new DataColumn("Ngay", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("Thang", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("Nam", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("HoTen", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("DiaChi", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("LyDo", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("SoTien", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("VietBangChu", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("KemTheo", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("NguoiLap", typeof(string)));
                            dtInBaoCao.Columns.Add(new DataColumn("hotengiaodich", typeof(string)));
                            DataRow row = dtInBaoCao.NewRow();
                            row["SoPhieu"] = txtMaPHC.Text;
                            row["Ngay"] = string.Format("{0:dd}", dateTMNgayChi.EditValue);
                            row["Thang"] = string.Format("{0:MM}", dateTMNgayChi.EditValue);
                            row["Nam"] = string.Format("{0:yyyy}", dateTMNgayChi.EditValue);
                            row["HoTen"] = txtNCC.Text;
                            row["DiaChi"] = txtDiaChi.Text;
                            row["LyDo"] = txtLydo.Text;
                            row["SoTien"] = txtSoTien.Text + " VNĐ";
                            row["VietBangChu"] = txtTienChu.Text;
                            row["KemTheo"] = txtKemTheo.Text;
                            string sql21 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                            row["NguoiLap"] = clsMain.ReturnDataTable(sql21).Rows[0][0].ToString();
                            row["hotengiaodich"] = txt_TMTen.Text;
                            dtInBaoCao.Rows.Add(row);

                            Frm_BCInPhieu frm = new Frm_BCInPhieu();
                            frm.WindowState = FormWindowState.Maximized;
                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            frm.dtSource = dtInBaoCao;
                            frm.Mode = 19;
                            frm.ShowDialog();
                            frm.Dispose();

                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    KhoaControl(false);
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    txtMaPHC.Text = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_CHI_NS()").Rows[0][0].ToString();
                    DataTable dtreset = clsMain.ReturnDataTable("select * from CN_LOAI_CONGNO where LoaiThuChi='02'   AND SUDUNG=1 ");
                    gl_nguon.Properties.DataSource = dtreset;
                    gl_nguon.EditValue = "-1";
                    gl_nguon.EditValue = dtreset.Rows[0]["MaLoai"].ToString();
                    if (close == 1)
                        this.Close();
                    throw new Exception("Lưu Thành Công");

                }
                else
                    throw new Exception("Lưu Thất Bại");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void chkTienMat_CheckedChanged(object sender, EventArgs e)
        {
            if (chHTTT.Checked)
            {
                lbNguonChi.Visible = cbNguonChi.Visible = false;
            }
            else
            {
                lbNguonChi.Visible = cbNguonChi.Visible = true;
            }
            if (chkTienMat.Checked)
            {
                chHTTT.Checked = false;
            }
            else
            {
                chHTTT.Checked = true;
            }
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;

            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;

            }
        }

        private void chHTTT_CheckedChanged(object sender, EventArgs e)
        {

            if (chHTTT.Checked)
            {
                lbNguonChi.Visible = cbNguonChi.Visible = false;
            }
            else
            {
                lbNguonChi.Visible = cbNguonChi.Visible = true;
            }
            if (chHTTT.Checked)
            {
                chkTienMat.Checked = false;
            }
            else
            {
                chkTienMat.Checked = true;
            }
            if (chkTienMat.Checked)
            {
                pnChuyenKhoan.Visible = false;
                pnTienMat.Visible = true;

            }
            else
            {
                pnChuyenKhoan.Visible = true;
                pnTienMat.Visible = false;

            }
        }


        private void txtSoTien_TextChanged(object sender, EventArgs e)
        {
            if (txtSoTien.Text != "")
            {
                txtSoTien.Text = string.Format("{0:#,0}", decimal.Parse(txtSoTien.Text.Replace(",", "")));
            }
        }

        private void txt_TenKH_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (gl_nguon.EditValue.ToString() == "02")
            {
                gl_DoiTuong.EditValue = null;
            }
        }

        private void gl_nguon_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_nguon.EditValue != null)
            {
                if (gl_nguon.EditValue.ToString() == "03")
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    DataTable dtncc = clsMain.ReturnDataTable("select  distinct dt.MA as DT_DOITUONGID,dt.TEN as DT_HOTEN from NHACUNGCAP dt inner join KHO_PHIEUKHO pk on dt.MA=pk.NHACUNGCAP inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID and THU_CHI='false' and FLAG='true'order by dt.MA");
                    gl_DoiTuong.Properties.DataSource = dtncc;
                    lbMaPhieuNhap.Text = "Mã Phiếu Nhập:";
                    lbNCC.Text = "Nhà Cung Cấp:";
                    //lbNCC1.Text = "Nhà Cung Cấp:";
                    gbNo.Visible = true;
                    gbChiChi.Visible = false;
                    gl_DoiTuong.EditValue = "01";
                    gl_DoiTuong.EditValue = "01";
                    grMaPhieuNhap.Properties.DataSource = null;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = false;
                    lbNCC.Visible = gl_DoiTuong.Visible = true;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = true;
                    lbNhanVien.Visible = gl_NhanVien.Visible = false;

                }
                else if (gl_nguon.EditValue.ToString() == "04")
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    gbNo.Visible = false;
                    gbChiChi.Visible = false;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = true;

                    gl_NhanVien.Properties.DataSource = clsMain.ReturnDataTable("select MANHANVIEN,TENNHANVIEN from DM_NhanVien");
                    lbNCC.Visible = gl_DoiTuong.Visible = false;
                    txtSoTien.Properties.ReadOnly = false;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = false;
                    lbNhanVien.Visible = gl_NhanVien.Visible = true;

                }
                else if (gl_nguon.EditValue.ToString() == "08" || gl_nguon.EditValue.ToString() == "09" || gl_nguon.EditValue.ToString() == "10")
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    gbNo.Visible = false;
                    gbChiChi.Visible = false;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = true;

                    gl_NhanVien.Properties.DataSource = clsMain.ReturnDataTable("select MANHANVIEN,TENNHANVIEN from DM_NhanVien");
                    lbNCC.Visible = gl_DoiTuong.Visible = false;
                    txtSoTien.Properties.ReadOnly = false;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = false;
                    lbNhanVien.Visible = gl_NhanVien.Visible = false;

                }
                else if (gl_nguon.EditValue.ToString() == "06")
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    DataTable dtncc = clsMain.ReturnDataTable("select  distinct  dt.MA as DT_DOITUONGID,dt.TEN as DT_HOTEN from NHACUNGCAP dt inner join KHO_PHIEUKHO pk on dt.MA=pk.NHACUNGCAP  inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where THU_CHI='True' and SOTIENCONLAI < 0 and FLAG='true'  order by dt.MA");
                    lbMaPhieuNhap.Text = "Mã Phiếu Xuất:";
                    lbNCC.Text = "Tên Khách Hàng:";
                    //lbNCC1.Text = "Tên Khách Hàng:";
                    gl_DoiTuong.Properties.DataSource = dtncc;
                    gbNo.Visible = true;
                    gbChiChi.Visible = false;
                    gl_DoiTuong.EditValue = "01";
                    grMaPhieuNhap.Properties.DataSource = null;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = false;
                    lbNCC.Visible = gl_DoiTuong.Visible = true;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = true;
                    lbNhanVien.Visible = gl_NhanVien.Visible = false;

                }
                else if (gl_nguon.EditValue.ToString() == "07")//chi khách hàng
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    DataTable dtncc = clsMain.ReturnDataTable("select  distinct dt.MA as DT_DOITUONGID,dt.TEN as DT_HOTEN from NHACUNGCAP dt inner join KHO_PHIEUKHO pk on dt.MA=pk.NHACUNGCAP inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where THU_CHI='True' and SOTIENCONLAI < TONGTIENCANTT order by dt.MA");
                    lbMaPhieuNhap.Text = "Mã Phiếu Xuất:";
                    lbNCC.Text = "Tên Khách Hàng:";
                    //lbNCC1.Text = "Tên Khách Hàng:";
                    gl_DoiTuong.Properties.DataSource = dtncc;
                    gbNo.Visible = false;
                    gbChiChi.Visible = true;
                    gl_DoiTuong.EditValue = "01";

                    grMaPhieuNhap.Properties.DataSource = null;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = false;
                    lbNCC.Visible = gl_DoiTuong.Visible = true;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = true;
                    lbNhanVien.Visible = gl_NhanVien.Visible = false;

                }
                else//chi khac
                {
                    Reset();
                    gl_DoiTuong.EditValue = null;
                    grMaPhieuNhap.EditValue = null;
                    gl_NhanVien.EditValue = null;
                    gbNo.Visible = false;
                    gbChiChi.Visible = false;
                    txtNCC.Properties.ReadOnly = txtDiaChi.Properties.ReadOnly = false;
                    txtSoTien.Properties.ReadOnly = false;
                    lbNCC.Visible = gl_DoiTuong.Visible = false;
                    lbMaPhieuNhap.Visible = grMaPhieuNhap.Visible = false;
                    lbNhanVien.Visible = gl_NhanVien.Visible = false;

                }
            }
            dateTMNgayChi.EditValue = dateCKChi.EditValue = KP_UserManagement.clsMain.GetServerDate();
        }

        private void gl_DoiTuong_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_DoiTuong.EditValue != null)
            {
                grMaPhieuNhap.EditValue = null;
                Reset();
                if (gl_nguon.EditValue.ToString() == "03")
                {
                    string sql = string.Format("select pk.SOPHIEU as MaPhieu from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where ct.THU_CHI='false' and ct.FLAG='true'and pk.NHACUNGCAP='{0}' order by  pk.SOPHIEU", gl_DoiTuong.EditValue);
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    grMaPhieuNhap.Properties.DataSource = dt;
                }
                else
                    if (gl_nguon.EditValue.ToString() == "06")
                    {
                        string sql = string.Format("select pk.SOPHIEU as MaPhieu from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where ct.THU_CHI='true' and ct.FLAG='true' and SOTIENCONLAI < 0 and pk.NHACUNGCAP='{0}' order by  pk.SOPHIEU", gl_DoiTuong.EditValue);
                        DataTable dt = clsMain.ReturnDataTable(sql);
                        grMaPhieuNhap.Properties.DataSource = dt;
                    }
                    else
                        if (gl_nguon.EditValue.ToString() == "07")
                        {
                            string sql = string.Format("(select distinct pk.SOPHIEU as MaPhieu from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where ct.THU_CHI='true'  and SOTIENCONLAI < TONGTIENCANTT and pk.NHACUNGCAP='{0}')  union (select distinct hd.MA_HOADON as MaPhieu from NHACUNGCAP dt inner join HOADON hd on dt.MA=hd.MA_KHACHHANG inner join CHITIETTHANHTOAN ct on hd.MA_HOADON=ct.MA_HOADON where ct.THU_CHI='true'  and SOTIENCONLAI < TONGTIENCANTT and hd.MA_KHACHHANG='{0}') order by MaPhieu", gl_DoiTuong.EditValue);
                            DataTable dt = clsMain.ReturnDataTable(sql);
                            grMaPhieuNhap.Properties.DataSource = dt;
                        }
            }

        }
        //đây nhé
        private void grMaPhieuNhap_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (grMaPhieuNhap.EditValue != null)
                {
                    if (gl_nguon.EditValue.ToString() == "03")
                    {
                        txtTienTra.Text = "";
                        string sql = string.Format("select DIACHI from NHACUNGCAP where MA='{0}'", gl_DoiTuong.EditValue);
                        txt_TMTen.Text = txtNCC.Text = clsMain.ReturnDataTable("select  TEN from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'").Rows[0][0].ToString();

                        string ngaynhap = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable("select NGAYTAO from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0]);
                        txtLydo.Text = "Chi tiền mặt cho phiếu nhập số " + grMaPhieuNhap.EditValue + ",ngày nhập " + ngaynhap + "";

                        if (clsMain.ReturnDataTable(sql).Rows.Count > 0)//nếu có ngày hẹn trả
                        {

                            txtDiaChi.Text = clsMain.ReturnDataTable(sql).Rows[0][0].ToString();
                        }
                        else
                        {
                            txtDiaChi.Text = "";
                        }

                        //kiểm tra xem nợ có hết hạn hay chưa
                        string sqlCheck = "select top(1) ct.NGAYHENTRA from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + grMaPhieuNhap.EditValue + "' and ct.NGAYHENTRA is not NULL order by ct.NGAYHENTRA desc";
                        DataTable dtCheck = clsMain.ReturnDataTable(sqlCheck);//lấy ngày hẹn trả của 1 phiếu kho mới nhất.
                        if (dtCheck.Rows.Count > 0)
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                            dateNgayHenTra.EditValue = dtCheck.Rows[0][0];
                            checkNgay = true;
                        }
                        else//ko có ngày hẹn trả thì
                        {
                            string sqlCheck1 = "select top(1) ct.SOTIENCONLAI from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA where pk.SOPHIEU ='" + grMaPhieuNhap.EditValue + "' order by ct.NGAYHENTRA desc";
                            DataTable dtCheck1 = clsMain.ReturnDataTable(sqlCheck1);
                            if (dtCheck1.Rows[0][0].ToString() != "0.0000")
                            {
                                lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                                checkNgay = true;
                            }
                            else
                            {
                                lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                                checkNgay = false;
                            }

                        }

                        decimal tienNo;

                        DataTable dtNo = clsMain.ReturnDataTable("select top(1) ct.SOTIENCONLAI,ct.DOTTHANHTOAN,ct.TONGTIENCANTT,ct.SOTIENTHANHTOAN  from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID where pk.SOPHIEU='" + grMaPhieuNhap.EditValue + "' order by ct.CTTT_STT desc");
                        if (dtNo.Rows[0][1].ToString() == "Tạm")
                        {
                            tienNo = decimal.Parse(dtNo.Rows[0][2].ToString());
                            txtTongNo.Text = string.Format("{0:#,0}", tienNo);

                            if (dtNo.Rows[0][3].ToString() != "0.0000")
                            {
                                //lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                                txtSoTien.Text = string.Format("{0:#,0}", (decimal.Parse(dtNo.Rows[0][3].ToString())));
                                txtTienTra.Text = string.Format("{0:#,0}", (decimal.Parse(dtNo.Rows[0][3].ToString())));
                                txtSoTien.Properties.ReadOnly = true;
                                txtNo.Text = string.Format("{0:#,0}", (tienNo - decimal.Parse(txtSoTien.Text.Replace(",", ""))));
                                txtTienChu.Text = cls_KHTT.ChuyenSo(txtSoTien.Text.Replace(",", "")) + "đồng";
                            }
                            else
                            {
                                //lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                                txtSoTien.Properties.ReadOnly = false;
                                txtSoTien.Text = "";
                                txtNo.Text = "";
                                txtTienChu.Text = "";
                            }

                        }
                        else
                        {
                            // lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                            txtSoTien.Properties.ReadOnly = false;
                            tienNo = decimal.Parse(dtNo.Rows[0][0].ToString());
                            txtTongNo.Text = string.Format("{0:#,0}", tienNo);
                            txtSoTien.Text = "";
                            txtNo.Text = "";
                            txtTienChu.Text = "";
                            txtTienTra.Text = "";
                        }
                    }
                    else
                        if (gl_nguon.EditValue.ToString() == "06")
                        {
                            string maKhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                            txt_TMTen.Text = txtNCC.Text = clsMain.ReturnDataTable("select  TEN from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'").Rows[0][0].ToString();
                            txtDiaChi.Text = clsMain.ReturnDataTable("select  DIACHI from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'").Rows[0][0].ToString();
                            string ngaynhap = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable("select NGAYTAO from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0]);
                            txtLydo.Text = "Chi tiền trả hàng  cho phiếu xuất số " + grMaPhieuNhap.EditValue + ",ngày xuất " + ngaynhap + "";
                            txtTongNo.Text = string.Format("{0:#,0}", Math.Abs(decimal.Parse(clsMain.ReturnDataTable("select  SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maKhoID + "' and flag='true'").Rows[0][0].ToString())));
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = true;
                            string ngayhentra = "select  NGAYHENTRA from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maKhoID + "' and flag='true'";
                            DataTable dtngayhentra = clsMain.ReturnDataTable(ngayhentra);
                            if (dtngayhentra.Rows.Count > 0)
                            {
                                dateNgayHenTra.EditValue = dtngayhentra.Rows[0][0];
                            }
                        }
                        else
                        {
                            lbNgayHenTra.Visible = dateNgayHenTra.Visible = false;
                            txt_TMTen.Text = txtNCC.Text = clsMain.ReturnDataTable("select  TEN from NHACUNGCAP where MA='" + gl_DoiTuong.EditValue + "'").Rows[0][0].ToString();
                            string maKhoID = "";
                            string ngaynhap = "";
                            DataTable dtCheckMaPhieu = clsMain.ReturnDataTable("select MA_HOADON from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "'");
                            if (dtCheckMaPhieu.Rows.Count == 0)
                            {
                                maKhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0].ToString();
                                ngaynhap = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable("select NGAYTAO from KHO_PHIEUKHO where SOPHIEU='" + grMaPhieuNhap.EditValue + "'").Rows[0][0]);
                                txtLydo.Text = "Chi tiền  cho phiếu xuất số " + grMaPhieuNhap.EditValue + ",ngày xuất " + ngaynhap + "";
                            }
                            else
                            {
                                ngaynhap = string.Format("{0:dd/MM/yyyy}", clsMain.ReturnDataTable("select NGAYTAO from HOADON where MA_HOADON='" + grMaPhieuNhap.EditValue + "'").Rows[0][0]);
                                txtLydo.Text = "Chi tiền  cho hóa đơn số " + grMaPhieuNhap.EditValue + ",ngày bán " + ngaynhap + "";
                            }

                            decimal tongtien = 0;
                            decimal sotienconlai = 0;
                            if (dtCheckMaPhieu.Rows.Count == 0)
                            {
                                tongtien = decimal.Parse(clsMain.ReturnDataTable("select top(1)TONGTIENCANTT from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maKhoID + "' ").Rows[0][0].ToString());
                                sotienconlai = decimal.Parse(clsMain.ReturnDataTable("select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maKhoID + "' order by CTTT_STT desc ").Rows[0][0].ToString());
                            }
                            else
                            {
                                tongtien = decimal.Parse(clsMain.ReturnDataTable("select top(1)TONGTIENCANTT from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "' ").Rows[0][0].ToString());
                                sotienconlai = decimal.Parse(clsMain.ReturnDataTable("select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where MA_HOADON='" + grMaPhieuNhap.EditValue + "' order by CTTT_STT desc ").Rows[0][0].ToString());
                            }


                            txtTongChi.Text = string.Format("{0:#,0}", (tongtien - sotienconlai));
                        }
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void txtTienChi_EditValueChanged(object sender, EventArgs e)
        {
            if (txtTienChi.Text == "")
            {
                txtChiConLai.Text = "";
            }
            else
            {
                txtChiConLai.Text = string.Format("{0:#,0}", (decimal.Parse(txtTongChi.Text) - decimal.Parse(txtTienChi.Text)));
            }
        }

        private void gl_NhanVien_EditValueChanged(object sender, EventArgs e)
        {
            if (gl_NhanVien.EditValue != null)
            {
                string sql = string.Format("select DIACHI from DM_NhanVien where MANHANVIEN='{0}'", gl_NhanVien.EditValue);
                txtNCC.Text = txt_TMTen.Text = gl_NhanVien.Text;
                //lbNCC1.Text = "Tên Nhân Viên";


                if (clsMain.ReturnDataTable(sql).Rows.Count > 0)
                {

                    txtDiaChi.Text = clsMain.ReturnDataTable(sql).Rows[0][0].ToString();
                }
                else
                {
                    txtDiaChi.Text = "";
                }
            }
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLayPhieuDeNghiChi_Click(object sender, EventArgs e)
        {
            Frm_DanhSachPhieuDeNghiChi frm = new Frm_DanhSachPhieuDeNghiChi(true);
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.ShowDialog();
            frm.Dispose();
            if (tmp_MAPDNDC != "")
            {
                LoadPhieuDeNghiXuatKho(tmp_MAPDNDC);
                flag_DaLayPhieuDNDC = true;
                KhoaControl(true);
            }
        }

        private void LoadPhieuDeNghiXuatKho(string MaPhieu)
        {
            string sSQL = "";
            sSQL += "Select MAPDNDC,MADOITUONG,NGAYLAPPHIEU,TENNHANVIEN,A.MALOAI,TenLoai,LYDO,A.SOTIEN,A.KEMTHEO,PHIEUKHO_ID,A.MANVMUON,A.MATT,A.HOTEN,A.DIACHI,HOTENGIAODICH,NGAYTHANHTOAN,TENNGANHANGCHUYEN,SOTAIKHOANCHUYEN,TENNGANHANGNHAN,SOTAIKHOANNHAN,SOGIAODICH,NGUONCHI" + "\n";
            sSQL += ",(SELECT SUM(THANHTIEN) FROM CHITIETPHIEUDENGHIDUYETCHI F WHERE F.PHIEUDENGHI = "  + clsMain.SQLString(MaPhieu) + " AND F.DUYET=1) AS TONGTIENDUYET" + "\n";
            sSQL += ",(SELECT SUM(SOLUONG) FROM CHITIETPHIEUDENGHIDUYETCHI F WHERE F.PHIEUDENGHI = "  + clsMain.SQLString(MaPhieu) + " AND F.DUYET=1) AS TONGSLDUYET" + "\n";
            sSQL += "From CN_CONGNO_PHIEUDENGHIDUYETCHI A,DM_NHANVIEN B,CN_LOAI_CONGNO C" + "\n";
            sSQL += "WHERE A.MANV=B.MANHANVIEN AND A.MALOAI=C.MaLoai AND A.TRANGTHAI=1" + "\n";
            sSQL += "AND MAPDNDC=" + clsMain.SQLString(MaPhieu);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                gl_nguon.EditValue = dt.Rows[0]["MALOAI"].ToString();
                if (gl_nguon.EditValue.ToString() == "03" || gl_nguon.EditValue.ToString() == "06")
                {
                    gl_DoiTuong.EditValue = dt.Rows[0]["MADOITUONG"].ToString();
                }

                if (dt.Rows[0]["PHIEUKHO_ID"].ToString() != "0")
                {
                    sSQL = "";
                    sSQL = "Select SOPHIEU From KHO_PHIEUKHO Where MA=" + clsMain.SQLString(dt.Rows[0]["PHIEUKHO_ID"].ToString());
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    grMaPhieuNhap.EditValue = dt1.Rows[0]["SOPHIEU"].ToString();

                    sSQL = "";
                    sSQL += "Select TOP 1 SOTIENCONLAI" + "\n";
                    sSQL += "From CHITIETTHANHTOAN" + "\n";
                    sSQL += "Where PK_PHIEUKHOID=" + clsMain.SQLString(dt.Rows[0]["PHIEUKHO_ID"].ToString()) + "\n";
                    sSQL += "Order by NGAYTHANHTOAN desc";
                    dt1 = clsMain.ReturnDataTable(sSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        dt.Columns.Add("SOTIENCONLAI");
                        dt.Rows[0]["SOTIENCONLAI"] = dt1.Rows[0]["SOTIENCONLAI"].ToString();
                        txtTongNo.Text = dt.Rows[0]["SOTIENCONLAI"].ToString();
                    }
                }

                //txtMaPHC.Text = dt.Rows[0]["MAPDNDC"].ToString();
                txtNCC.Text = dt.Rows[0]["HOTEN"].ToString();
                txtDiaChi.Text = dt.Rows[0]["DIACHI"].ToString();
                txtSoTien.Text = dt.Rows[0]["TONGTIENDUYET"].ToString();
                txtLydo.Text = dt.Rows[0]["LYDO"].ToString();
                txtKemTheo.Text = dt.Rows[0]["MAPDNDC"].ToString();
                if (dt.Rows[0]["MATT"].ToString() == "01")
                    cbNguonChi.Text = "Quỹ Tiền Mặt";
                else
                    cbNguonChi.Text = "Nguồn Khác";
                if (dt.Rows[0]["MATT"].ToString() == "01")
                {
                    chkTienMat.Checked = true;
                    chHTTT.Checked = false;
                    txt_TMTen.Text = dt.Rows[0]["HOTENGIAODICH"].ToString();
                    dateTMNgayChi.EditValue = DateTime.Parse(dt.Rows[0]["NGAYTHANHTOAN"].ToString());
                }
                else
                {
                    chkTienMat.Checked = false;
                    chHTTT.Checked = true;
                    txtCKTenNganHang.Text = dt.Rows[0]["TENNGANHANGCHUYEN"].ToString();
                    txtSoTaiKhoanChuyen.Text = dt.Rows[0]["SOTAIKHOANCHUYEN"].ToString();
                    txtTenNHNhan.Text = dt.Rows[0]["TENNGANHANGNHAN"].ToString();
                    txtSoTaiKhoanNhan.Text = dt.Rows[0]["SOTAIKHOANNHAN"].ToString();
                    txtSoGiaoDich.Text = dt.Rows[0]["SOGIAODICH"].ToString();
                    dateCKChi.EditValue = DateTime.Parse(dt.Rows[0]["NGAYTHANHTOAN"].ToString());
                }
            }
        }

        private void KhoaControl(bool flag)
        {
            gl_nguon.Properties.ReadOnly = flag;
            gl_NhanVien.Properties.ReadOnly = flag;
            grMaPhieuNhap.Properties.ReadOnly = flag;
            gl_DoiTuong.Properties.ReadOnly = flag;
            cbNguonChi.Properties.ReadOnly = flag;
            txtNCC.Properties.ReadOnly = flag;
            txtDiaChi.Properties.ReadOnly = flag;
            txtLydo.Properties.ReadOnly = flag;
            txtSoTien.Properties.ReadOnly = flag;
            dateNgayHenTra.Properties.ReadOnly = flag;
            txtKemTheo.Properties.ReadOnly = flag;
            chkTienMat.Properties.ReadOnly = chHTTT.Properties.ReadOnly = flag;
            txt_TMTen.Properties.ReadOnly = flag;
            //dateTMNgayChi.Properties.ReadOnly = flag;
            txtCKTenNganHang.Properties.ReadOnly = flag;
            txtSoTaiKhoanChuyen.Properties.ReadOnly = flag;
            txtTenNHNhan.Properties.ReadOnly = flag;
        }
    }
}