﻿namespace KP_RES
{
    partial class Frm_TheoDoiCongNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TheoDoiCongNo));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.lbNgayTu = new DevExpress.XtraEditors.LabelControl();
            this.lbNgayDen = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.gc_DanhSachThu = new DevExpress.XtraEditors.GroupControl();
            this.gv_DanhSachThu = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaPhieuXuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayHenTra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GhiChuKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDanhSachChi = new DevExpress.XtraEditors.GroupControl();
            this.gvDanhSachChi = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPCN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NhaCungCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYGIAODICHCHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongTienNhap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DaThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoConLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PK_MAPHIEUKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayHenTraNCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ghiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.date_tungay = new DevExpress.XtraEditors.DateEdit();
            this.date_denngay = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc_DanhSachThu)).BeginInit();
            this.gc_DanhSachThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_DanhSachThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSachChi)).BeginInit();
            this.gcDanhSachChi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDanhSachChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 780);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Controls.Add(this.panelControl7);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 741);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 205);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 534);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(78, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(132, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.date_denngay);
            this.pnChoose.Controls.Add(this.date_tungay);
            this.pnChoose.Controls.Add(this.lbNgayTu);
            this.pnChoose.Controls.Add(this.lbNgayDen);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 82);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 123);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // lbNgayTu
            // 
            this.lbNgayTu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayTu.Location = new System.Drawing.Point(5, 7);
            this.lbNgayTu.Name = "lbNgayTu";
            this.lbNgayTu.Size = new System.Drawing.Size(72, 19);
            this.lbNgayTu.TabIndex = 10;
            this.lbNgayTu.Text = "Từ Ngày :";
            this.lbNgayTu.Visible = false;
            // 
            // lbNgayDen
            // 
            this.lbNgayDen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayDen.Location = new System.Drawing.Point(5, 64);
            this.lbNgayDen.Name = "lbNgayDen";
            this.lbNgayDen.Size = new System.Drawing.Size(80, 19);
            this.lbNgayDen.TabIndex = 11;
            this.lbNgayDen.Text = "Đến Ngày :";
            this.lbNgayDen.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(215, 80);
            this.panelControl7.TabIndex = 12;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "TC";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TC", "Tất Cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Ngay", "Theo Ngày Hẹn Trả")});
            this.optGroup.Size = new System.Drawing.Size(211, 78);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 743);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1339, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 780);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 536);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 376);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 456);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 373);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 698);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 82);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 2);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // gc_DanhSachThu
            // 
            this.gc_DanhSachThu.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gc_DanhSachThu.AppearanceCaption.ForeColor = System.Drawing.Color.Blue;
            this.gc_DanhSachThu.AppearanceCaption.Options.UseFont = true;
            this.gc_DanhSachThu.AppearanceCaption.Options.UseForeColor = true;
            this.gc_DanhSachThu.Controls.Add(this.gv_DanhSachThu);
            this.gc_DanhSachThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc_DanhSachThu.Location = new System.Drawing.Point(223, 0);
            this.gc_DanhSachThu.Name = "gc_DanhSachThu";
            this.gc_DanhSachThu.Size = new System.Drawing.Size(1116, 463);
            this.gc_DanhSachThu.TabIndex = 17;
            this.gc_DanhSachThu.Text = "Danh Sách Thu Công Nợ:";
            // 
            // gv_DanhSachThu
            // 
            this.gv_DanhSachThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_DanhSachThu.Location = new System.Drawing.Point(2, 22);
            this.gv_DanhSachThu.MainView = this.gridView2;
            this.gv_DanhSachThu.Name = "gv_DanhSachThu";
            this.gv_DanhSachThu.Size = new System.Drawing.Size(1112, 439);
            this.gv_DanhSachThu.TabIndex = 0;
            this.gv_DanhSachThu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gv_DanhSachThu.Click += new System.EventHandler(this.gv_DanhSachThu_Click);
            // 
            // gridView2
            // 
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTKH,
            this.MaPhieuXuat,
            this.gridColumn8,
            this.NgayGiaoDich,
            this.TongTien,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn11,
            this.gridColumn9,
            this.NgayHenTra,
            this.GhiChuKH});
            this.gridView2.GridControl = this.gv_DanhSachThu;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.RowAutoHeight = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.NgayHenTra, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STTKH
            // 
            this.STTKH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTKH.AppearanceCell.Options.UseFont = true;
            this.STTKH.AppearanceCell.Options.UseTextOptions = true;
            this.STTKH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTKH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTKH.AppearanceHeader.Options.UseFont = true;
            this.STTKH.AppearanceHeader.Options.UseTextOptions = true;
            this.STTKH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTKH.Caption = "STT";
            this.STTKH.FieldName = "STT";
            this.STTKH.Name = "STTKH";
            this.STTKH.OptionsColumn.AllowEdit = false;
            this.STTKH.OptionsColumn.AllowFocus = false;
            this.STTKH.OptionsColumn.FixedWidth = true;
            this.STTKH.Visible = true;
            this.STTKH.VisibleIndex = 0;
            this.STTKH.Width = 39;
            // 
            // MaPhieuXuat
            // 
            this.MaPhieuXuat.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaPhieuXuat.AppearanceCell.Options.UseFont = true;
            this.MaPhieuXuat.AppearanceCell.Options.UseTextOptions = true;
            this.MaPhieuXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaPhieuXuat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaPhieuXuat.AppearanceHeader.Options.UseFont = true;
            this.MaPhieuXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.MaPhieuXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaPhieuXuat.Caption = "Mã Phiếu";
            this.MaPhieuXuat.FieldName = "MAPHIEU";
            this.MaPhieuXuat.Name = "MaPhieuXuat";
            this.MaPhieuXuat.OptionsColumn.AllowEdit = false;
            this.MaPhieuXuat.OptionsColumn.AllowFocus = false;
            this.MaPhieuXuat.OptionsColumn.FixedWidth = true;
            this.MaPhieuXuat.Visible = true;
            this.MaPhieuXuat.VisibleIndex = 1;
            this.MaPhieuXuat.Width = 160;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Khách Hàng";
            this.gridColumn8.FieldName = "HOTENKH";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 400;
            // 
            // NgayGiaoDich
            // 
            this.NgayGiaoDich.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayGiaoDich.AppearanceCell.Options.UseFont = true;
            this.NgayGiaoDich.AppearanceCell.Options.UseTextOptions = true;
            this.NgayGiaoDich.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayGiaoDich.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayGiaoDich.AppearanceHeader.Options.UseFont = true;
            this.NgayGiaoDich.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayGiaoDich.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayGiaoDich.Caption = "Ngày Giao Dịch";
            this.NgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayGiaoDich.FieldName = "NgayGiaoDich";
            this.NgayGiaoDich.Name = "NgayGiaoDich";
            this.NgayGiaoDich.OptionsColumn.AllowEdit = false;
            this.NgayGiaoDich.OptionsColumn.AllowFocus = false;
            this.NgayGiaoDich.OptionsColumn.FixedWidth = true;
            this.NgayGiaoDich.Visible = true;
            this.NgayGiaoDich.VisibleIndex = 3;
            this.NgayGiaoDich.Width = 150;
            // 
            // TongTien
            // 
            this.TongTien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTien.AppearanceCell.Options.UseFont = true;
            this.TongTien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTien.AppearanceHeader.Options.UseFont = true;
            this.TongTien.AppearanceHeader.Options.UseTextOptions = true;
            this.TongTien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TongTien.Caption = "Tổng Tiền Xuất";
            this.TongTien.DisplayFormat.FormatString = "#,0";
            this.TongTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TongTien.FieldName = "TongTien";
            this.TongTien.Name = "TongTien";
            this.TongTien.OptionsColumn.AllowEdit = false;
            this.TongTien.OptionsColumn.AllowFocus = false;
            this.TongTien.OptionsColumn.FixedWidth = true;
            this.TongTien.Visible = true;
            this.TongTien.VisibleIndex = 4;
            this.TongTien.Width = 200;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Tổng Nợ";
            this.gridColumn4.DisplayFormat.FormatString = "#,0";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn4.FieldName = "TongNo";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 200;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Đã Thanh Toán";
            this.gridColumn5.DisplayFormat.FormatString = "#,0";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "DaThanhToan";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            this.gridColumn5.Width = 200;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "gridColumn1";
            this.gridColumn11.FieldName = "PK_MAPHIEUKHO";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Nợ Còn Lại";
            this.gridColumn9.DisplayFormat.FormatString = "#,0";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "NoConLai";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 200;
            // 
            // NgayHenTra
            // 
            this.NgayHenTra.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayHenTra.AppearanceCell.Options.UseFont = true;
            this.NgayHenTra.AppearanceCell.Options.UseTextOptions = true;
            this.NgayHenTra.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHenTra.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayHenTra.AppearanceHeader.Options.UseFont = true;
            this.NgayHenTra.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayHenTra.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHenTra.Caption = "Ngày Hẹn Trả";
            this.NgayHenTra.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayHenTra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayHenTra.FieldName = "NGAYHENTRA";
            this.NgayHenTra.Name = "NgayHenTra";
            this.NgayHenTra.OptionsColumn.AllowEdit = false;
            this.NgayHenTra.OptionsColumn.AllowFocus = false;
            this.NgayHenTra.OptionsColumn.FixedWidth = true;
            this.NgayHenTra.Visible = true;
            this.NgayHenTra.VisibleIndex = 8;
            this.NgayHenTra.Width = 150;
            // 
            // GhiChuKH
            // 
            this.GhiChuKH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GhiChuKH.AppearanceCell.Options.UseFont = true;
            this.GhiChuKH.AppearanceCell.Options.UseTextOptions = true;
            this.GhiChuKH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GhiChuKH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GhiChuKH.AppearanceHeader.Options.UseFont = true;
            this.GhiChuKH.AppearanceHeader.Options.UseTextOptions = true;
            this.GhiChuKH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GhiChuKH.Caption = "Ghi Chú";
            this.GhiChuKH.FieldName = "GhiChu";
            this.GhiChuKH.Name = "GhiChuKH";
            this.GhiChuKH.OptionsColumn.AllowEdit = false;
            this.GhiChuKH.OptionsColumn.AllowFocus = false;
            this.GhiChuKH.OptionsColumn.FixedWidth = true;
            this.GhiChuKH.Visible = true;
            this.GhiChuKH.VisibleIndex = 9;
            this.GhiChuKH.Width = 300;
            // 
            // gcDanhSachChi
            // 
            this.gcDanhSachChi.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcDanhSachChi.AppearanceCaption.ForeColor = System.Drawing.Color.Blue;
            this.gcDanhSachChi.AppearanceCaption.Options.UseFont = true;
            this.gcDanhSachChi.AppearanceCaption.Options.UseForeColor = true;
            this.gcDanhSachChi.Controls.Add(this.gvDanhSachChi);
            this.gcDanhSachChi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gcDanhSachChi.Location = new System.Drawing.Point(223, 463);
            this.gcDanhSachChi.Name = "gcDanhSachChi";
            this.gcDanhSachChi.Size = new System.Drawing.Size(1116, 317);
            this.gcDanhSachChi.TabIndex = 16;
            this.gcDanhSachChi.Text = "Danh Sách Chi Công Nợ";
            // 
            // gvDanhSachChi
            // 
            this.gvDanhSachChi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvDanhSachChi.Location = new System.Drawing.Point(2, 22);
            this.gvDanhSachChi.MainView = this.gridView1;
            this.gvDanhSachChi.Name = "gvDanhSachChi";
            this.gvDanhSachChi.Size = new System.Drawing.Size(1112, 293);
            this.gvDanhSachChi.TabIndex = 0;
            this.gvDanhSachChi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gvDanhSachChi.Click += new System.EventHandler(this.gvDanhSachChi_Click);
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAPCN,
            this.NhaCungCap,
            this.NGAYGIAODICHCHI,
            this.TongTienNhap,
            this.TongNo,
            this.DaThanhToan,
            this.NoConLai,
            this.PK_MAPHIEUKHO,
            this.NgayHenTraNCC,
            this.ghiChu});
            this.gridView1.GridControl = this.gvDanhSachChi;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.NgayHenTraNCC, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell_1);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 40;
            // 
            // MAPCN
            // 
            this.MAPCN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPCN.AppearanceCell.Options.UseFont = true;
            this.MAPCN.AppearanceCell.Options.UseTextOptions = true;
            this.MAPCN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPCN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPCN.AppearanceHeader.Options.UseFont = true;
            this.MAPCN.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPCN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPCN.Caption = "Mã Phiếu";
            this.MAPCN.FieldName = "MAPHIEU";
            this.MAPCN.Name = "MAPCN";
            this.MAPCN.OptionsColumn.AllowEdit = false;
            this.MAPCN.OptionsColumn.AllowFocus = false;
            this.MAPCN.OptionsColumn.FixedWidth = true;
            this.MAPCN.Visible = true;
            this.MAPCN.VisibleIndex = 1;
            this.MAPCN.Width = 160;
            // 
            // NhaCungCap
            // 
            this.NhaCungCap.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NhaCungCap.AppearanceCell.Options.UseFont = true;
            this.NhaCungCap.AppearanceCell.Options.UseTextOptions = true;
            this.NhaCungCap.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NhaCungCap.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NhaCungCap.AppearanceHeader.Options.UseFont = true;
            this.NhaCungCap.AppearanceHeader.Options.UseTextOptions = true;
            this.NhaCungCap.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NhaCungCap.Caption = "Nhà Cung Cấp";
            this.NhaCungCap.FieldName = "HOTENKH";
            this.NhaCungCap.Name = "NhaCungCap";
            this.NhaCungCap.OptionsColumn.AllowEdit = false;
            this.NhaCungCap.OptionsColumn.AllowFocus = false;
            this.NhaCungCap.OptionsColumn.FixedWidth = true;
            this.NhaCungCap.Visible = true;
            this.NhaCungCap.VisibleIndex = 2;
            this.NhaCungCap.Width = 400;
            // 
            // NGAYGIAODICHCHI
            // 
            this.NGAYGIAODICHCHI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYGIAODICHCHI.AppearanceCell.Options.UseFont = true;
            this.NGAYGIAODICHCHI.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYGIAODICHCHI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYGIAODICHCHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYGIAODICHCHI.AppearanceHeader.Options.UseFont = true;
            this.NGAYGIAODICHCHI.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYGIAODICHCHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYGIAODICHCHI.Caption = "Ngày Giao Dịch";
            this.NGAYGIAODICHCHI.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYGIAODICHCHI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYGIAODICHCHI.FieldName = "NgayGiaoDich";
            this.NGAYGIAODICHCHI.Name = "NGAYGIAODICHCHI";
            this.NGAYGIAODICHCHI.OptionsColumn.AllowEdit = false;
            this.NGAYGIAODICHCHI.OptionsColumn.AllowFocus = false;
            this.NGAYGIAODICHCHI.OptionsColumn.FixedWidth = true;
            this.NGAYGIAODICHCHI.Visible = true;
            this.NGAYGIAODICHCHI.VisibleIndex = 3;
            this.NGAYGIAODICHCHI.Width = 150;
            // 
            // TongTienNhap
            // 
            this.TongTienNhap.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTienNhap.AppearanceCell.Options.UseFont = true;
            this.TongTienNhap.AppearanceCell.Options.UseTextOptions = true;
            this.TongTienNhap.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TongTienNhap.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTienNhap.AppearanceHeader.Options.UseFont = true;
            this.TongTienNhap.AppearanceHeader.Options.UseTextOptions = true;
            this.TongTienNhap.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TongTienNhap.Caption = "Tổng Tiền Nhập";
            this.TongTienNhap.DisplayFormat.FormatString = "#,0";
            this.TongTienNhap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TongTienNhap.FieldName = "TongTien";
            this.TongTienNhap.Name = "TongTienNhap";
            this.TongTienNhap.OptionsColumn.AllowEdit = false;
            this.TongTienNhap.OptionsColumn.AllowFocus = false;
            this.TongTienNhap.OptionsColumn.FixedWidth = true;
            this.TongTienNhap.Visible = true;
            this.TongTienNhap.VisibleIndex = 4;
            this.TongTienNhap.Width = 200;
            // 
            // TongNo
            // 
            this.TongNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongNo.AppearanceCell.Options.UseFont = true;
            this.TongNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongNo.AppearanceHeader.Options.UseFont = true;
            this.TongNo.AppearanceHeader.Options.UseTextOptions = true;
            this.TongNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TongNo.Caption = "Tổng Nợ";
            this.TongNo.DisplayFormat.FormatString = "#,0";
            this.TongNo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TongNo.FieldName = "TongNo";
            this.TongNo.Name = "TongNo";
            this.TongNo.OptionsColumn.AllowEdit = false;
            this.TongNo.OptionsColumn.AllowFocus = false;
            this.TongNo.OptionsColumn.FixedWidth = true;
            this.TongNo.Visible = true;
            this.TongNo.VisibleIndex = 5;
            this.TongNo.Width = 200;
            // 
            // DaThanhToan
            // 
            this.DaThanhToan.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DaThanhToan.AppearanceCell.Options.UseFont = true;
            this.DaThanhToan.AppearanceCell.Options.UseTextOptions = true;
            this.DaThanhToan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DaThanhToan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DaThanhToan.AppearanceHeader.Options.UseFont = true;
            this.DaThanhToan.AppearanceHeader.Options.UseTextOptions = true;
            this.DaThanhToan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DaThanhToan.Caption = "Đã Thanh Toán";
            this.DaThanhToan.DisplayFormat.FormatString = "#,0";
            this.DaThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DaThanhToan.FieldName = "DaThanhToan";
            this.DaThanhToan.Name = "DaThanhToan";
            this.DaThanhToan.OptionsColumn.AllowEdit = false;
            this.DaThanhToan.OptionsColumn.AllowFocus = false;
            this.DaThanhToan.OptionsColumn.FixedWidth = true;
            this.DaThanhToan.Visible = true;
            this.DaThanhToan.VisibleIndex = 6;
            this.DaThanhToan.Width = 200;
            // 
            // NoConLai
            // 
            this.NoConLai.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoConLai.AppearanceCell.Options.UseFont = true;
            this.NoConLai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoConLai.AppearanceHeader.Options.UseFont = true;
            this.NoConLai.AppearanceHeader.Options.UseTextOptions = true;
            this.NoConLai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoConLai.Caption = "Nợ Còn Lại";
            this.NoConLai.DisplayFormat.FormatString = "#,0";
            this.NoConLai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NoConLai.FieldName = "NoConLai";
            this.NoConLai.Name = "NoConLai";
            this.NoConLai.OptionsColumn.AllowEdit = false;
            this.NoConLai.OptionsColumn.AllowFocus = false;
            this.NoConLai.OptionsColumn.FixedWidth = true;
            this.NoConLai.Visible = true;
            this.NoConLai.VisibleIndex = 7;
            this.NoConLai.Width = 200;
            // 
            // PK_MAPHIEUKHO
            // 
            this.PK_MAPHIEUKHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PK_MAPHIEUKHO.AppearanceCell.Options.UseFont = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PK_MAPHIEUKHO.AppearanceHeader.Options.UseFont = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PK_MAPHIEUKHO.Caption = "gridColumn1";
            this.PK_MAPHIEUKHO.FieldName = "PK_MAPHIEUKHO";
            this.PK_MAPHIEUKHO.Name = "PK_MAPHIEUKHO";
            this.PK_MAPHIEUKHO.OptionsColumn.AllowEdit = false;
            this.PK_MAPHIEUKHO.OptionsColumn.AllowFocus = false;
            this.PK_MAPHIEUKHO.OptionsColumn.FixedWidth = true;
            // 
            // NgayHenTraNCC
            // 
            this.NgayHenTraNCC.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayHenTraNCC.AppearanceCell.Options.UseFont = true;
            this.NgayHenTraNCC.AppearanceCell.Options.UseTextOptions = true;
            this.NgayHenTraNCC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHenTraNCC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayHenTraNCC.AppearanceHeader.Options.UseFont = true;
            this.NgayHenTraNCC.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayHenTraNCC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayHenTraNCC.Caption = "Ngày Hẹn Trả";
            this.NgayHenTraNCC.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayHenTraNCC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayHenTraNCC.FieldName = "NGAYHENTRA";
            this.NgayHenTraNCC.Name = "NgayHenTraNCC";
            this.NgayHenTraNCC.OptionsColumn.AllowEdit = false;
            this.NgayHenTraNCC.OptionsColumn.AllowFocus = false;
            this.NgayHenTraNCC.OptionsColumn.FixedWidth = true;
            this.NgayHenTraNCC.Visible = true;
            this.NgayHenTraNCC.VisibleIndex = 8;
            this.NgayHenTraNCC.Width = 150;
            // 
            // ghiChu
            // 
            this.ghiChu.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ghiChu.AppearanceCell.Options.UseFont = true;
            this.ghiChu.AppearanceCell.Options.UseTextOptions = true;
            this.ghiChu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ghiChu.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ghiChu.AppearanceHeader.Options.UseFont = true;
            this.ghiChu.AppearanceHeader.Options.UseTextOptions = true;
            this.ghiChu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ghiChu.Caption = "Ghi Chú";
            this.ghiChu.DisplayFormat.FormatString = "0,0 VND";
            this.ghiChu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ghiChu.FieldName = "GhiChu";
            this.ghiChu.MinWidth = 170;
            this.ghiChu.Name = "ghiChu";
            this.ghiChu.OptionsColumn.AllowEdit = false;
            this.ghiChu.OptionsColumn.AllowFocus = false;
            this.ghiChu.OptionsColumn.FixedWidth = true;
            this.ghiChu.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.ghiChu.Visible = true;
            this.ghiChu.VisibleIndex = 9;
            this.ghiChu.Width = 300;
            // 
            // date_tungay
            // 
            this.date_tungay.EditValue = null;
            this.date_tungay.Location = new System.Drawing.Point(5, 32);
            this.date_tungay.Name = "date_tungay";
            this.date_tungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_tungay.Properties.Appearance.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_tungay.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceFocused.Options.UseFont = true;
            this.date_tungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_tungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_tungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_tungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_tungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_tungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_tungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_tungay.Size = new System.Drawing.Size(210, 26);
            this.date_tungay.TabIndex = 21;
            // 
            // date_denngay
            // 
            this.date_denngay.EditValue = null;
            this.date_denngay.Location = new System.Drawing.Point(5, 89);
            this.date_denngay.Name = "date_denngay";
            this.date_denngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_denngay.Properties.Appearance.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_denngay.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceFocused.Options.UseFont = true;
            this.date_denngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_denngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_denngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_denngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_denngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_denngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_denngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_denngay.Size = new System.Drawing.Size(210, 26);
            this.date_denngay.TabIndex = 22;
            // 
            // Frm_TheoDoiCongNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1378, 780);
            this.Controls.Add(this.gc_DanhSachThu);
            this.Controls.Add(this.gcDanhSachChi);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TheoDoiCongNo";
            this.Text = "Báo Cáo Quỹ Tiền Mặt";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc_DanhSachThu)).EndInit();
            this.gc_DanhSachThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv_DanhSachThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSachChi)).EndInit();
            this.gcDanhSachChi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvDanhSachChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.LabelControl lbNgayDen;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl lbNgayTu;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.GroupControl gc_DanhSachThu;
        private DevExpress.XtraGrid.GridControl gv_DanhSachThu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STTKH;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhieuXuat;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn NgayGiaoDich;
        private DevExpress.XtraGrid.Columns.GridColumn TongTien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn NgayHenTra;
        private DevExpress.XtraGrid.Columns.GridColumn GhiChuKH;
        private DevExpress.XtraEditors.GroupControl gcDanhSachChi;
        private DevExpress.XtraGrid.GridControl gvDanhSachChi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MAPCN;
        private DevExpress.XtraGrid.Columns.GridColumn NhaCungCap;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYGIAODICHCHI;
        private DevExpress.XtraGrid.Columns.GridColumn TongTienNhap;
        private DevExpress.XtraGrid.Columns.GridColumn TongNo;
        private DevExpress.XtraGrid.Columns.GridColumn DaThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn NoConLai;
        private DevExpress.XtraGrid.Columns.GridColumn PK_MAPHIEUKHO;
        private DevExpress.XtraGrid.Columns.GridColumn NgayHenTraNCC;
        private DevExpress.XtraGrid.Columns.GridColumn ghiChu;
        private DevExpress.XtraEditors.DateEdit date_tungay;
        private DevExpress.XtraEditors.DateEdit date_denngay;



    }
}