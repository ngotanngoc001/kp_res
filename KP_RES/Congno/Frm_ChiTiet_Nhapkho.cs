﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_ChiTiet_Nhapkho : DevExpress.XtraEditors.XtraForm
    {
        string _MaKhoID;
        public Frm_ChiTiet_Nhapkho(string MaKhoID)
        {
            InitializeComponent();
            _MaKhoID = MaKhoID;
            title.Text = this.Text;
            LoadData(_MaKhoID);
        }

        private void LoadData(string MaKhoID)
        {
            string sql="select pk.SOPHIEU,k.TEN_KHO,pk.GHICHU,pk.NGAYTAO,pt.PT_TEN,nv.TENNHANVIEN,ncc.TEN from KHO_PHIEUKHO pk inner join NHACUNGCAP ncc on pk.NHACUNGCAP=ncc.MA   inner join KHO_PHUONGTHUC pt on pk.PT_MA=pt.PT_MA  inner join DM_NHANVIEN nv on nv.MANHANVIEN=pk.NHANVIEN  inner join KHO k on pk.KHO=k.MA_KHO  where pk.LOAI=1 and pk.MA='"+_MaKhoID+"'";
            DataTable dt = clsMain.ReturnDataTable(sql);
            txtSophieu.Text = dt.Rows[0]["SOPHIEU"].ToString();
            txtKho.Text = dt.Rows[0]["TEN_KHO"].ToString();
            txtGHICHU.Text = dt.Rows[0]["GHICHU"].ToString();
            txtNgay.Text =string.Format("{0:dd/MM/yyyy}", dt.Rows[0]["NGAYTAO"]);
            txtTenPT.Text = dt.Rows[0]["PT_TEN"].ToString();
            txtNVnhap.Text = dt.Rows[0]["TENNHANVIEN"].ToString();
            txtNCC.Text = dt.Rows[0]["TEN"].ToString();

            string sqlNhap = "select hh.TEN_HANGHOA as TEN,dvt.TEN_DONVITINH as DVT,ct.SOLUONG as SL,hh.GIANHAP,(hh.GIANHAP*ct.SOLUONG) as THANHTIENTCK,ct.CHIETKHAU as CK,ct.TIENCHIETKHAU as TIENCK,ct.THANHTIEN,ct.THUE as VAT,ct.TIENTHUE as TIENVAT,ct.TONGCONG from KHO_PHIEUKHO pk inner join CHITIETPHIEUKHO ct on pk.MA=ct.PHIEUKHO inner join HANGHOA hh on ct.HANGHOA=hh.MA_HANGHOA inner join DONVITINH dvt on hh.MA_DONVITINH=dvt.MA_DONVITINH   where pk.LOAI=1 and pk.MA='" + _MaKhoID + "'";
            DataTable dtNhap = clsMain.ReturnDataTable(sqlNhap);
            gvNhapKho.DataSource = dtNhap;
        }
     
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }
       
        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        
       

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}