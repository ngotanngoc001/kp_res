﻿namespace KP_RES 
{
    partial class Frm_ChiTiet_Xuatkho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ChiTiet_Xuatkho));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtKhachHang = new DevExpress.XtraEditors.TextEdit();
            this.txtPhuongThuc = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoXuat = new DevExpress.XtraEditors.TextEdit();
            this.txtNgayXuat = new DevExpress.XtraEditors.TextEdit();
            this.txtNVnhap = new DevExpress.XtraEditors.TextEdit();
            this.txtSophieu = new DevExpress.XtraEditors.TextEdit();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblDoituong = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.gvXuatKho = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GIAXUAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_vat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.TIENVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_giaxuat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.look_ten = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            this.pnCongNo = new System.Windows.Forms.Panel();
            this.pn_ThanhToan = new DevExpress.XtraEditors.PanelControl();
            this.rdCongNo = new DevExpress.XtraEditors.RadioGroup();
            this.pnct_congno = new DevExpress.XtraEditors.PanelControl();
            this.txtConLai = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txt_datra = new DevExpress.XtraEditors.TextEdit();
            this.txt_tongtien = new DevExpress.XtraEditors.TextEdit();
            this.date_ngayhentra = new DevExpress.XtraEditors.DateEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.look_httt = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNgayTra = new DevExpress.XtraEditors.LabelControl();
            this.lb_httt = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuongThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoXuat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayXuat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVnhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvXuatKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_giaxuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.pnCongNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_ThanhToan)).BeginInit();
            this.pn_ThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdCongNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnct_congno)).BeginInit();
            this.pnct_congno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtConLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_datra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_tongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_ngayhentra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_ngayhentra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_httt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtKhachHang);
            this.panelControl1.Controls.Add(this.txtPhuongThuc);
            this.panelControl1.Controls.Add(this.txtKhoXuat);
            this.panelControl1.Controls.Add(this.txtNgayXuat);
            this.panelControl1.Controls.Add(this.txtNVnhap);
            this.panelControl1.Controls.Add(this.txtSophieu);
            this.panelControl1.Controls.Add(this.txtGHICHU);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.lblDoituong);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.lblGHICHU);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 50);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1224, 116);
            this.panelControl1.TabIndex = 0;
            // 
            // txtKhachHang
            // 
            this.txtKhachHang.EnterMoveNextControl = true;
            this.txtKhachHang.Location = new System.Drawing.Point(744, 37);
            this.txtKhachHang.Name = "txtKhachHang";
            this.txtKhachHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhachHang.Properties.Appearance.Options.UseFont = true;
            this.txtKhachHang.Properties.ReadOnly = true;
            this.txtKhachHang.Size = new System.Drawing.Size(248, 26);
            this.txtKhachHang.TabIndex = 17;
            // 
            // txtPhuongThuc
            // 
            this.txtPhuongThuc.EnterMoveNextControl = true;
            this.txtPhuongThuc.Location = new System.Drawing.Point(415, 37);
            this.txtPhuongThuc.Name = "txtPhuongThuc";
            this.txtPhuongThuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuongThuc.Properties.Appearance.Options.UseFont = true;
            this.txtPhuongThuc.Properties.ReadOnly = true;
            this.txtPhuongThuc.Size = new System.Drawing.Size(248, 26);
            this.txtPhuongThuc.TabIndex = 16;
            // 
            // txtKhoXuat
            // 
            this.txtKhoXuat.EnterMoveNextControl = true;
            this.txtKhoXuat.Location = new System.Drawing.Point(70, 37);
            this.txtKhoXuat.Name = "txtKhoXuat";
            this.txtKhoXuat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoXuat.Properties.Appearance.Options.UseFont = true;
            this.txtKhoXuat.Properties.ReadOnly = true;
            this.txtKhoXuat.Size = new System.Drawing.Size(248, 26);
            this.txtKhoXuat.TabIndex = 15;
            // 
            // txtNgayXuat
            // 
            this.txtNgayXuat.EnterMoveNextControl = true;
            this.txtNgayXuat.Location = new System.Drawing.Point(415, 5);
            this.txtNgayXuat.Name = "txtNgayXuat";
            this.txtNgayXuat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayXuat.Properties.Appearance.Options.UseFont = true;
            this.txtNgayXuat.Properties.ReadOnly = true;
            this.txtNgayXuat.Size = new System.Drawing.Size(248, 26);
            this.txtNgayXuat.TabIndex = 14;
            // 
            // txtNVnhap
            // 
            this.txtNVnhap.EnterMoveNextControl = true;
            this.txtNVnhap.Location = new System.Drawing.Point(744, 5);
            this.txtNVnhap.Name = "txtNVnhap";
            this.txtNVnhap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVnhap.Properties.Appearance.Options.UseFont = true;
            this.txtNVnhap.Properties.ReadOnly = true;
            this.txtNVnhap.Size = new System.Drawing.Size(248, 26);
            this.txtNVnhap.TabIndex = 5;
            // 
            // txtSophieu
            // 
            this.txtSophieu.EnterMoveNextControl = true;
            this.txtSophieu.Location = new System.Drawing.Point(70, 5);
            this.txtSophieu.Name = "txtSophieu";
            this.txtSophieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSophieu.Properties.Appearance.Options.UseFont = true;
            this.txtSophieu.Properties.ReadOnly = true;
            this.txtSophieu.Size = new System.Drawing.Size(248, 26);
            this.txtSophieu.TabIndex = 1;
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(70, 69);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Properties.ReadOnly = true;
            this.txtGHICHU.Size = new System.Drawing.Size(922, 26);
            this.txtGHICHU.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 72);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Ghi chú";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(321, 8);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 19);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Ngày";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(666, 8);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Nhân viên";
            // 
            // lblDoituong
            // 
            this.lblDoituong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDoituong.Location = new System.Drawing.Point(666, 40);
            this.lblDoituong.Margin = new System.Windows.Forms.Padding(4);
            this.lblDoituong.Name = "lblDoituong";
            this.lblDoituong.Size = new System.Drawing.Size(60, 19);
            this.lblDoituong.TabIndex = 10;
            this.lblDoituong.Text = "KH_NCC";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(321, 40);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 19);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Phương thức";
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(6, 40);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(62, 19);
            this.lblGHICHU.TabIndex = 6;
            this.lblGHICHU.Text = "Kho xuất";
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(62, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Số phiếu";
            // 
            // gvXuatKho
            // 
            this.gvXuatKho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvXuatKho.Location = new System.Drawing.Point(0, 166);
            this.gvXuatKho.MainView = this.gridView1;
            this.gvXuatKho.Name = "gvXuatKho";
            this.gvXuatKho.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_soluong,
            this.txt_giaxuat,
            this.txt_chietkhau,
            this.txt_vat,
            this.look_ten});
            this.gvXuatKho.Size = new System.Drawing.Size(1185, 386);
            this.gvXuatKho.TabIndex = 1;
            this.gvXuatKho.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA,
            this.TEN,
            this.DVT,
            this.SL,
            this.GIAXUAT,
            this.THANHTIEN,
            this.VAT,
            this.TIENVAT,
            this.TONGCONG,
            this.FILL});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gvXuatKho;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceCell.Options.UseFont = true;
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.Caption = "Mã";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.Width = 80;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Hàng hóa";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 1;
            this.TEN.Width = 486;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 2;
            this.DVT.Width = 90;
            // 
            // SL
            // 
            this.SL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SL.AppearanceCell.Options.UseFont = true;
            this.SL.AppearanceCell.Options.UseTextOptions = true;
            this.SL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.Caption = "SL";
            this.SL.ColumnEdit = this.txt_soluong;
            this.SL.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.SL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowEdit = false;
            this.SL.OptionsColumn.AllowFocus = false;
            this.SL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SL.OptionsColumn.FixedWidth = true;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 3;
            this.SL.Width = 60;
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            // 
            // GIAXUAT
            // 
            this.GIAXUAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIAXUAT.AppearanceCell.Options.UseFont = true;
            this.GIAXUAT.AppearanceCell.Options.UseTextOptions = true;
            this.GIAXUAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIAXUAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIAXUAT.AppearanceHeader.Options.UseFont = true;
            this.GIAXUAT.AppearanceHeader.Options.UseTextOptions = true;
            this.GIAXUAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIAXUAT.Caption = "Giá xuất";
            this.GIAXUAT.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIAXUAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIAXUAT.FieldName = "GIAXUAT";
            this.GIAXUAT.Name = "GIAXUAT";
            this.GIAXUAT.OptionsColumn.AllowEdit = false;
            this.GIAXUAT.OptionsColumn.AllowFocus = false;
            this.GIAXUAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIAXUAT.OptionsColumn.FixedWidth = true;
            this.GIAXUAT.Visible = true;
            this.GIAXUAT.VisibleIndex = 4;
            this.GIAXUAT.Width = 100;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.Caption = "Thành tiền ";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 5;
            this.THANHTIEN.Width = 150;
            // 
            // VAT
            // 
            this.VAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VAT.AppearanceCell.Options.UseFont = true;
            this.VAT.AppearanceCell.Options.UseTextOptions = true;
            this.VAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.VAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VAT.AppearanceHeader.Options.UseFont = true;
            this.VAT.AppearanceHeader.Options.UseTextOptions = true;
            this.VAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VAT.Caption = "VAT";
            this.VAT.ColumnEdit = this.txt_vat;
            this.VAT.DisplayFormat.FormatString = "{0:#,###0}%";
            this.VAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.VAT.FieldName = "VAT";
            this.VAT.Name = "VAT";
            this.VAT.OptionsColumn.AllowEdit = false;
            this.VAT.OptionsColumn.AllowFocus = false;
            this.VAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.VAT.OptionsColumn.FixedWidth = true;
            this.VAT.Visible = true;
            this.VAT.VisibleIndex = 6;
            this.VAT.Width = 60;
            // 
            // txt_vat
            // 
            this.txt_vat.AutoHeight = false;
            this.txt_vat.Name = "txt_vat";
            // 
            // TIENVAT
            // 
            this.TIENVAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENVAT.AppearanceCell.Options.UseFont = true;
            this.TIENVAT.AppearanceCell.Options.UseTextOptions = true;
            this.TIENVAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENVAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENVAT.AppearanceHeader.Options.UseFont = true;
            this.TIENVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENVAT.Caption = "Tiền VAT";
            this.TIENVAT.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENVAT.FieldName = "TIENVAT";
            this.TIENVAT.Name = "TIENVAT";
            this.TIENVAT.OptionsColumn.AllowEdit = false;
            this.TIENVAT.OptionsColumn.AllowFocus = false;
            this.TIENVAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TIENVAT.OptionsColumn.FixedWidth = true;
            this.TIENVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENVAT", "{0:#,###0}")});
            this.TIENVAT.Visible = true;
            this.TIENVAT.VisibleIndex = 7;
            this.TIENVAT.Width = 150;
            // 
            // TONGCONG
            // 
            this.TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceCell.Options.UseFont = true;
            this.TONGCONG.AppearanceCell.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGCONG.Caption = "Tổng cộng";
            this.TONGCONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGCONG.FieldName = "TONGCONG";
            this.TONGCONG.Name = "TONGCONG";
            this.TONGCONG.OptionsColumn.AllowEdit = false;
            this.TONGCONG.OptionsColumn.AllowFocus = false;
            this.TONGCONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TONGCONG.OptionsColumn.FixedWidth = true;
            this.TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0}")});
            this.TONGCONG.Visible = true;
            this.TONGCONG.VisibleIndex = 8;
            this.TONGCONG.Width = 150;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 9;
            this.FILL.Width = 20;
            // 
            // txt_giaxuat
            // 
            this.txt_giaxuat.AutoHeight = false;
            this.txt_giaxuat.Mask.EditMask = "N0";
            this.txt_giaxuat.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_giaxuat.Name = "txt_giaxuat";
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.Name = "txt_chietkhau";
            // 
            // look_ten
            // 
            this.look_ten.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_ten.AppearanceDropDown.Options.UseFont = true;
            this.look_ten.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_ten.AppearanceDropDownHeader.Options.UseFont = true;
            this.look_ten.AutoHeight = false;
            this.look_ten.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_ten.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_ten.DisplayMember = "TEN_HANGHOA";
            this.look_ten.DropDownItemHeight = 40;
            this.look_ten.Name = "look_ten";
            this.look_ten.NullText = "";
            this.look_ten.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_ten.ShowHeader = false;
            this.look_ten.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_ten.ValueMember = "TEN_HANGHOA";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1185, 166);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 465);
            this.panelControl2.TabIndex = 5;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 381);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 61);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 221);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 301);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 383);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.header);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1224, 50);
            this.panel4.TabIndex = 104;
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl5);
            this.header.Controls.Add(this.panelControl6);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1224, 52);
            this.header.TabIndex = 2;
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSize = true;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.pictureEdit1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(107, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1117, 52);
            this.panelControl5.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(1113, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.AutoSize = true;
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl6.Controls.Add(this.title);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl6.Location = new System.Drawing.Point(81, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(26, 52);
            this.panelControl6.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(14, 32);
            this.title.TabIndex = 1;
            this.title.Text = "[]";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // pnCongNo
            // 
            this.pnCongNo.Controls.Add(this.pn_ThanhToan);
            this.pnCongNo.Controls.Add(this.pnct_congno);
            this.pnCongNo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnCongNo.Location = new System.Drawing.Point(0, 552);
            this.pnCongNo.Name = "pnCongNo";
            this.pnCongNo.Size = new System.Drawing.Size(1185, 79);
            this.pnCongNo.TabIndex = 105;
            // 
            // pn_ThanhToan
            // 
            this.pn_ThanhToan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_ThanhToan.Controls.Add(this.rdCongNo);
            this.pn_ThanhToan.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_ThanhToan.Location = new System.Drawing.Point(669, 0);
            this.pn_ThanhToan.Name = "pn_ThanhToan";
            this.pn_ThanhToan.Size = new System.Drawing.Size(152, 79);
            this.pn_ThanhToan.TabIndex = 39;
            this.pn_ThanhToan.Visible = false;
            // 
            // rdCongNo
            // 
            this.rdCongNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdCongNo.EditValue = "";
            this.rdCongNo.Location = new System.Drawing.Point(0, 0);
            this.rdCongNo.Name = "rdCongNo";
            this.rdCongNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.rdCongNo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rdCongNo.Properties.Appearance.Options.UseFont = true;
            this.rdCongNo.Properties.Appearance.Options.UseForeColor = true;
            this.rdCongNo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rdCongNo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CN", "Công Nợ"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CK", "Chuyển Khoản(T/T)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TM", "Tiền Mặt")});
            this.rdCongNo.Size = new System.Drawing.Size(152, 79);
            this.rdCongNo.TabIndex = 9;
            // 
            // pnct_congno
            // 
            this.pnct_congno.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnct_congno.Controls.Add(this.txtConLai);
            this.pnct_congno.Controls.Add(this.labelControl17);
            this.pnct_congno.Controls.Add(this.txt_datra);
            this.pnct_congno.Controls.Add(this.txt_tongtien);
            this.pnct_congno.Controls.Add(this.date_ngayhentra);
            this.pnct_congno.Controls.Add(this.labelControl19);
            this.pnct_congno.Controls.Add(this.labelControl20);
            this.pnct_congno.Controls.Add(this.look_httt);
            this.pnct_congno.Controls.Add(this.lbNgayTra);
            this.pnct_congno.Controls.Add(this.lb_httt);
            this.pnct_congno.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnct_congno.Location = new System.Drawing.Point(821, 0);
            this.pnct_congno.Name = "pnct_congno";
            this.pnct_congno.Size = new System.Drawing.Size(364, 79);
            this.pnct_congno.TabIndex = 38;
            this.pnct_congno.Visible = false;
            // 
            // txtConLai
            // 
            this.txtConLai.EditValue = "0.00";
            this.txtConLai.Enabled = false;
            this.txtConLai.EnterMoveNextControl = true;
            this.txtConLai.Location = new System.Drawing.Point(72, 49);
            this.txtConLai.Name = "txtConLai";
            this.txtConLai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConLai.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtConLai.Properties.Appearance.Options.UseFont = true;
            this.txtConLai.Properties.Appearance.Options.UseForeColor = true;
            this.txtConLai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtConLai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtConLai.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Orange;
            this.txtConLai.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtConLai.Properties.Mask.EditMask = "N0";
            this.txtConLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtConLai.Size = new System.Drawing.Size(125, 20);
            this.txtConLai.TabIndex = 33;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl17.Location = new System.Drawing.Point(7, 53);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(45, 14);
            this.labelControl17.TabIndex = 34;
            this.labelControl17.Text = "Còn Lại";
            // 
            // txt_datra
            // 
            this.txt_datra.EditValue = "0.00";
            this.txt_datra.EnterMoveNextControl = true;
            this.txt_datra.Location = new System.Drawing.Point(72, 27);
            this.txt_datra.Name = "txt_datra";
            this.txt_datra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_datra.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt_datra.Properties.Appearance.Options.UseFont = true;
            this.txt_datra.Properties.Appearance.Options.UseForeColor = true;
            this.txt_datra.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_datra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_datra.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Orange;
            this.txt_datra.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txt_datra.Properties.Mask.EditMask = "N0";
            this.txt_datra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_datra.Size = new System.Drawing.Size(125, 20);
            this.txt_datra.TabIndex = 0;
            // 
            // txt_tongtien
            // 
            this.txt_tongtien.EditValue = "0.00";
            this.txt_tongtien.EnterMoveNextControl = true;
            this.txt_tongtien.Location = new System.Drawing.Point(72, 3);
            this.txt_tongtien.Name = "txt_tongtien";
            this.txt_tongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tongtien.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt_tongtien.Properties.Appearance.Options.UseFont = true;
            this.txt_tongtien.Properties.Appearance.Options.UseForeColor = true;
            this.txt_tongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_tongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_tongtien.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Orange;
            this.txt_tongtien.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txt_tongtien.Properties.ReadOnly = true;
            this.txt_tongtien.Size = new System.Drawing.Size(125, 20);
            this.txt_tongtien.TabIndex = 4;
            // 
            // date_ngayhentra
            // 
            this.date_ngayhentra.EditValue = null;
            this.date_ngayhentra.EnterMoveNextControl = true;
            this.date_ngayhentra.Location = new System.Drawing.Point(257, 50);
            this.date_ngayhentra.Name = "date_ngayhentra";
            this.date_ngayhentra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.date_ngayhentra.Properties.Appearance.Options.UseFont = true;
            this.date_ngayhentra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_ngayhentra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_ngayhentra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_ngayhentra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_ngayhentra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_ngayhentra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_ngayhentra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_ngayhentra.Size = new System.Drawing.Size(107, 20);
            this.date_ngayhentra.TabIndex = 2;
            this.date_ngayhentra.Visible = false;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl19.Location = new System.Drawing.Point(7, 31);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(39, 14);
            this.labelControl19.TabIndex = 32;
            this.labelControl19.Text = "Đã Trả";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl20.Location = new System.Drawing.Point(4, 6);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(60, 14);
            this.labelControl20.TabIndex = 30;
            this.labelControl20.Text = "Tổng Tiền";
            // 
            // look_httt
            // 
            this.look_httt.EnterMoveNextControl = true;
            this.look_httt.Location = new System.Drawing.Point(257, 28);
            this.look_httt.Name = "look_httt";
            this.look_httt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_httt.Properties.Appearance.Options.UseFont = true;
            this.look_httt.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Orange;
            this.look_httt.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.look_httt.Properties.AutoSearchColumnIndex = 1;
            this.look_httt.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_httt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_httt.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_HTT", "Hình Thức Thanh Toán Tiền")});
            this.look_httt.Properties.DisplayMember = "TEN_HTT";
            this.look_httt.Properties.NullText = "";
            this.look_httt.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_httt.Properties.ValueMember = "MAHTT";
            this.look_httt.Size = new System.Drawing.Size(107, 20);
            this.look_httt.TabIndex = 1;
            this.look_httt.Visible = false;
            // 
            // lbNgayTra
            // 
            this.lbNgayTra.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayTra.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lbNgayTra.Location = new System.Drawing.Point(199, 54);
            this.lbNgayTra.Name = "lbNgayTra";
            this.lbNgayTra.Size = new System.Drawing.Size(53, 14);
            this.lbNgayTra.TabIndex = 5;
            this.lbNgayTra.Text = "Ngày Trả";
            this.lbNgayTra.Visible = false;
            // 
            // lb_httt
            // 
            this.lb_httt.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_httt.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lb_httt.Location = new System.Drawing.Point(199, 31);
            this.lb_httt.Name = "lb_httt";
            this.lb_httt.Size = new System.Drawing.Size(34, 14);
            this.lb_httt.TabIndex = 4;
            this.lb_httt.Text = "HTTT ";
            this.lb_httt.Visible = false;
            // 
            // Frm_ChiTiet_Xuatkho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 631);
            this.Controls.Add(this.gvXuatKho);
            this.Controls.Add(this.pnCongNo);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panel4);
            this.Name = "Frm_ChiTiet_Xuatkho";
            this.Text = "Chi Tiết Xuất Kho";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuongThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoXuat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayXuat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVnhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSophieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvXuatKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_giaxuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.pnCongNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_ThanhToan)).EndInit();
            this.pn_ThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdCongNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnct_congno)).EndInit();
            this.pnct_congno.ResumeLayout(false);
            this.pnct_congno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtConLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_datra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_tongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_ngayhentra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_ngayhentra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_httt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.LabelControl lblDoituong;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.GridControl gvXuatKho;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraGrid.Columns.GridColumn GIAXUAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_giaxuat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn VAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_vat;
        private DevExpress.XtraGrid.Columns.GridColumn TIENVAT;
        private DevExpress.XtraGrid.Columns.GridColumn TONGCONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit look_ten;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.TextEdit txtNVnhap;
        private DevExpress.XtraEditors.TextEdit txtSophieu;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.TextEdit txtKhachHang;
        private DevExpress.XtraEditors.TextEdit txtPhuongThuc;
        private DevExpress.XtraEditors.TextEdit txtKhoXuat;
        private DevExpress.XtraEditors.TextEdit txtNgayXuat;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private System.Windows.Forms.Panel pnCongNo;
        private DevExpress.XtraEditors.PanelControl pn_ThanhToan;
        private DevExpress.XtraEditors.RadioGroup rdCongNo;
        private DevExpress.XtraEditors.PanelControl pnct_congno;
        private DevExpress.XtraEditors.TextEdit txtConLai;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txt_datra;
        private DevExpress.XtraEditors.TextEdit txt_tongtien;
        private DevExpress.XtraEditors.DateEdit date_ngayhentra;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LookUpEdit look_httt;
        private DevExpress.XtraEditors.LabelControl lbNgayTra;
        private DevExpress.XtraEditors.LabelControl lb_httt;




    }
}