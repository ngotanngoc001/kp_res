﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_DanhSachPhieuDeNghiChi : DevExpress.XtraEditors.XtraForm
    {
        
        bool flag_layphieu = false;
        public Frm_DanhSachPhieuDeNghiChi()
        {
            InitializeComponent();
            header.Visible = false;
            btnTONGQUAN.Width = btnCHITIET.Width = panelControl7.Width / 2;
            btnTONGQUAN_Click(null, null);
        }
        public Frm_DanhSachPhieuDeNghiChi(bool layphieu)
        {
            InitializeComponent();
            title.Text = this.Text;
            flag_layphieu = layphieu;
            btnTONGQUAN.Width = btnCHITIET.Width = panelControl7.Width / 2;
            btnTONGQUAN_Click(null, null);
            if (flag_layphieu)
            {
                //gcCongNo.Text = "Danh Sách Phiếu Đã Được Duyệt Chi";
                header.Visible = true;
                btnXuatfile.Visible = false;
                repositoryItemButtonEdit1.Buttons[0].Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            }
            else
            {
                //gcCongNo.Text = "Danh Sách Phiếu Đề Nghị Duyệt Chi";
                header.Visible = false ;
            }
        }
        private void LoadData()
        {
            int trangthai = 0;
            if (flag_layphieu)
                trangthai = 1;

            string sSQL = "";
            sSQL += "Select MAPDNDC,NGAYLAPPHIEU,TENNHANVIEN,A.MALOAI,TenLoai,LYDO,A.SOTIEN,A.PHIEUKHO_ID,A.MANVMUON,A.HOTEN" + "\n";
            sSQL += "From CN_CONGNO_PHIEUDENGHIDUYETCHI A,DM_NHANVIEN B,CN_LOAI_CONGNO C" + "\n";
            sSQL += "WHERE A.MANV=B.MANHANVIEN AND A.MALOAI=C.MaLoai AND ISNULL(DALAY,0)=0" + "\n";
            sSQL += "AND A.TRANGTHAI=" + clsMain.SQLString(trangthai.ToString());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gvcShowCongNoNCC.DataSource = dt;
            if (flag_layphieu)
                gridView1.Columns["XemPhieu"].Caption = "Lấy phiếu";
            else
                gridView1.Columns["XemPhieu"].Caption = "Xem chi tiết";
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gvcShowCongNoNCC.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gvcShowCongNoNCC.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }
      
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (e.Column == XemPhieu)
                {
                    btnXemthongtinphieu();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                btnXemthongtinphieu();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnXemthongtinphieu()
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }
            if (flag_layphieu)
            {
                Frm_LapPhieuChi.tmp_MAPDNDC = gridView1.GetFocusedRowCellValue("MAPDNDC").ToString();
                this.Close();
            }
            else
            {
                Frm_DuyetPhieuChi frm = new Frm_DuyetPhieuChi(gridView1.GetFocusedRowCellValue("MAPDNDC").ToString());
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
                LoadData();
            }
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTONGQUAN_Click(object sender, EventArgs e)
        {
            pnlTONGQUAN.Visible = true;
            pnlCHITIET.Visible = false;
            btnTONGQUAN.ForeColor = Color.Red;
            btnCHITIET.ForeColor = ForeColor;

            LoadData();
        }

        private void btnCHITIET_Click(object sender, EventArgs e)
        {
            pnlTONGQUAN.Visible = false;
            pnlCHITIET.Visible = true;
            btnTONGQUAN.ForeColor = ForeColor;
            btnCHITIET.ForeColor = Color.Red;

            LoadDataCTCN();
        }
        private void LoadDataCTCN()
        {
            int trangthai = 0;
            if (flag_layphieu)
                trangthai = 1;

            string sSQL = "";
            sSQL += "Select MAPDNDC,NGAYLAPPHIEU,TENNHANVIEN,A.MALOAI,TenLoai,LYDO,A.SOTIEN,A.PHIEUKHO_ID,A.MANVMUON,A.HOTEN,D.HANGHOA AS MA,TEN_HANGHOA AS TEN, D.TEN_DONVITINH AS DVT,D.SOLUONG AS SL, D.DONGIA AS GIANHAP, D.THANHTIEN,E.MAVACH AS MA_VACH " + "\n";
            sSQL += "From CN_CONGNO_PHIEUDENGHIDUYETCHI A " + "\n";
            sSQL += "INNER JOIN DM_NHANVIEN B ON A.MANV=B.MANHANVIEN " + "\n";
            sSQL += "INNER JOIN CN_LOAI_CONGNO C ON A.MALOAI=C.MaLoai " + "\n";
            sSQL += "INNER JOIN CHITIETPHIEUDENGHIDUYETCHI D ON A.MAPDNDC = D.PHIEUDENGHI " + "\n";
            sSQL += "INNER JOIN HANGHOA E ON D.HANGHOA = E.MA_HANGHOA " + "\n";
            sSQL += "WHERE ISNULL(DALAY,0)=0 " + "\n";
            sSQL += "AND A.TRANGTHAI=" + clsMain.SQLString(trangthai.ToString());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gvcShowCTCongNoNCC.DataSource = dt;
            if (flag_layphieu)
                gridView1.Columns["XemPhieu"].Caption = "Lấy phiếu";
            else
                gridView1.Columns["XemPhieu"].Caption = "Xem chi tiết";
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT2 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (e.Column == XemPhieu2)
                {
                    btnXemthongtinphieu2();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                btnXemthongtinphieu2();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void btnXemthongtinphieu2()
        {
            if (gridView2.FocusedRowHandle < 0)
            {
                return;
            }
            if (flag_layphieu)
            {
                Frm_LapPhieuChi.tmp_MAPDNDC = gridView2.GetFocusedRowCellValue("MAPDNDC").ToString();
                this.Close();
            }
            else
            {
                Frm_DuyetPhieuChi frm = new Frm_DuyetPhieuChi(gridView2.GetFocusedRowCellValue("MAPDNDC").ToString());
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
                LoadData();
                LoadDataCTCN();
            }
        }
    }
}