﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_UyNhiemChi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_UyNhiemChi()
        {
            InitializeComponent();
        }

        private void Frm_UyNhiemChi_Load(object sender, EventArgs e)
        {
            dtpNgay.EditValue = KP_UserManagement.clsMain.GetServerDate();
            dtpNgayCap.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == (XtraMessageBox.Show("Bạn có chắc muốn tạo phiếu ủy nhiệm chi ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
            {
                return;
            }
            try
            {
                if (txtDonViChuyenTien.Text == "")
                {
                    txtDonViChuyenTien.Focus();
                    throw new Exception("Nhập người chuyển tiền");
                }
                if (txtDonViThuHuong.Text == "")
                {
                    txtDonViThuHuong.Focus();
                    throw new Exception("Nhập người nhận");
                }
                if (txtSoTaiKhoanThuHuong.Text == "")
                {
                    txtSoTaiKhoanThuHuong.Focus();
                    throw new Exception("Nhập tài khoản người nhận");
                }
                if (txtNganHang.Text == "")
                {
                    txtNganHang.Focus();
                    throw new Exception("Nhập ngân hàng chuyển tiền");
                }
                if (txtNoiDung.Text == "")
                {
                    txtNoiDung.Focus();
                    throw new Exception("Nhập nội dung chuyển tiền");
                }
                if (txtTienBangChu.Text == "")
                {
                    txtTienBangChu.Focus();
                    throw new Exception("Nhập số tiền bằng chữ");
                }
                if (txtTienBangSo.Text == "" || int.Parse(txtTienBangSo.Text.Replace(",", "")) <= 0)
                {
                    txtTienBangSo.Focus();
                    throw new Exception("Nhập số tiền bằng số");
                }

                String sMa = clsMain.ReturnDataTable("SELECT dbo.NewCodeCongNo_UyNhiemChi()").Rows[0][0].ToString();

                String sSQL = "";
                sSQL += "EXEC SP_InsertUyNhiemChi " + "\n";
                sSQL += clsMain.SQLString(sMa) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:yyyyMMdd}", dtpNgay.EditValue)) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDonViChuyenTien.Text) + ",";
                sSQL += clsMain.SQLString(txtSoTaiKhoanChuyen.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtChiNhanhChuyen.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDonViThuHuong.Text) + ",";
                sSQL += clsMain.SQLString(txtSoTaiKhoanThuHuong.Text) + ",";
                sSQL += clsMain.SQLString(txtSoCMND.Text) + ",";
                sSQL += clsMain.SQLString(dtpNgayCap.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtNoiCap.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtNganHang.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTinhTP.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtNoiDung.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTienBangChu.Text) + ",";
                sSQL += clsMain.SQLString(txtTienBangSo.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID);

                if (clsMain.ExecuteSQL(sSQL))
                {
                    DataTable myDT = new DataTable();
                    myDT.Columns.Add("MA", typeof(String));
                    myDT.Columns.Add("NGAY", typeof(String));
                    myDT.Columns.Add("DONVI_CHUYEN", typeof(String));
                    myDT.Columns.Add("TAIKHOAN_CHUYEN", typeof(String));
                    myDT.Columns.Add("CHINHANH_CHUYEN", typeof(String));
                    myDT.Columns.Add("DONVI_NHAN", typeof(String));
                    myDT.Columns.Add("TAIKHOAN_NHAN", typeof(String));
                    myDT.Columns.Add("CMND", typeof(String));
                    myDT.Columns.Add("NGAYCAP", typeof(String));
                    myDT.Columns.Add("NOICAP", typeof(String));
                    myDT.Columns.Add("NGANHANG", typeof(String));
                    myDT.Columns.Add("TINH_TP", typeof(String));
                    myDT.Columns.Add("NOIDUNG", typeof(String));
                    myDT.Columns.Add("TIEN_CHU", typeof(String));
                    myDT.Columns.Add("TIEN_SO", typeof(String));
                    myDT.Columns.Add("TAIKHOANNO", typeof(String));
                    myDT.Columns.Add("TAIKHOANCO", typeof(String));
                    DataRow row = myDT.NewRow();
                    row["MA"] = sMa;
                    row["NGAY"] = String.Format("{0:dd/MM/yyyy}", dtpNgay.EditValue);
                    row["DONVI_CHUYEN"] = txtDonViChuyenTien.Text;
                    row["TAIKHOAN_CHUYEN"] = txtSoTaiKhoanChuyen.Text;
                    row["CHINHANH_CHUYEN"] = txtChiNhanhChuyen.Text;
                    row["DONVI_NHAN"] = txtDonViThuHuong.Text;
                    row["TAIKHOAN_NHAN"] = txtSoTaiKhoanThuHuong.Text;
                    row["CMND"] = txtSoCMND.Text;
                    row["NGAYCAP"] = dtpNgayCap.Text ;
                    row["NOICAP"] = txtNoiCap.Text;
                    row["NGANHANG"] = txtNganHang.Text;
                    row["TINH_TP"] = txtTinhTP.Text;
                    row["NOIDUNG"] = txtNoiDung.Text;
                    row["TIEN_CHU"] = txtTienBangChu.Text;
                    row["TIEN_SO"] = txtTienBangSo.Text + " VNĐ";
                    row["TAIKHOANNO"] = "";
                    row["TAIKHOANCO"] = "";
                    myDT.Rows.Add(row);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 88;
                    frm.ShowDialog();
                    frm.Dispose();
                    btnHuy_Click(null, null);
                }
                else
                    XtraMessageBox.Show("Lưu không thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtDonViChuyenTien.Text = "";
            txtSoTaiKhoanChuyen.Text = "";
            txtChiNhanhChuyen.Text = "";
            txtDonViThuHuong.Text = "";
            txtSoTaiKhoanThuHuong.Text = "";
            txtSoCMND.Text = "";
            txtNganHang.Text = "";
            txtNoiDung.Text = "";
            txtTienBangChu.Text = "";
            txtTienBangSo.Text = "";
            txtTinhTP.Text = "";
            dtpNgay.EditValue = KP_UserManagement.clsMain.GetServerDate();
            dtpNgayCap.Text  = "";
            txtNoiCap.Text = "";
        }

        private void txtTienBangSo_Leave(object sender, EventArgs e)
        {
            try
            {

                if (txtTienBangSo.Text != "")
                    txtTienBangChu.Text = cls_KHTT.ChuyenSo(decimal.Parse(txtTienBangSo.Text.Replace(",", "")).ToString()) + "đồng";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

       

        
    }
}