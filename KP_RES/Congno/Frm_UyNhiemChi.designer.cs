﻿namespace KP_RES 
{
    partial class Frm_UyNhiemChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnThongTinUyNhiemChi = new DevExpress.XtraEditors.GroupControl();
            this.pnBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTinhTP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgayCap = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgay = new DevExpress.XtraEditors.DateEdit();
            this.txtNganHang = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.txtTienBangSo = new DevExpress.XtraEditors.TextEdit();
            this.txtTienBangChu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtNoiCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbNguonChi = new System.Windows.Forms.Label();
            this.lbNgayHenTra = new DevExpress.XtraEditors.LabelControl();
            this.txtSoCMND = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTaiKhoanThuHuong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtDonViThuHuong = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtChiNhanhChuyen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTaiKhoanChuyen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtDonViChuyenTien = new DevExpress.XtraEditors.TextEdit();
            this.lbNCC1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pnThongTinUyNhiemChi)).BeginInit();
            this.pnThongTinUyNhiemChi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).BeginInit();
            this.pnBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBangSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBangChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanThuHuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonViThuHuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiNhanhChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonViChuyenTien.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // TenLoai
            // 
            this.TenLoai.Name = "TenLoai";
            // 
            // MaLoai
            // 
            this.MaLoai.Name = "MaLoai";
            // 
            // MaPhieu
            // 
            this.MaPhieu.Name = "MaPhieu";
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            // 
            // DT_HOTEN
            // 
            this.DT_HOTEN.Name = "DT_HOTEN";
            // 
            // pnThongTinUyNhiemChi
            // 
            this.pnThongTinUyNhiemChi.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnThongTinUyNhiemChi.AppearanceCaption.Options.UseFont = true;
            this.pnThongTinUyNhiemChi.Controls.Add(this.pnBottom);
            this.pnThongTinUyNhiemChi.Controls.Add(this.panelControl1);
            this.pnThongTinUyNhiemChi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThongTinUyNhiemChi.Location = new System.Drawing.Point(0, 0);
            this.pnThongTinUyNhiemChi.Name = "pnThongTinUyNhiemChi";
            this.pnThongTinUyNhiemChi.Size = new System.Drawing.Size(1057, 513);
            this.pnThongTinUyNhiemChi.TabIndex = 107;
            this.pnThongTinUyNhiemChi.Text = "Thông tin phiếu ủy nhiệm chi";
            // 
            // pnBottom
            // 
            this.pnBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBottom.Controls.Add(this.btnHuy);
            this.pnBottom.Controls.Add(this.btnLuu);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnBottom.Location = new System.Drawing.Point(2, 347);
            this.pnBottom.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1053, 44);
            this.pnBottom.TabIndex = 1;
            // 
            // btnHuy
            // 
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnHuy.Location = new System.Drawing.Point(251, 4);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(80, 35);
            this.btnHuy.TabIndex = 1;
            this.btnHuy.Text = "&2.Hủy";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.Location = new System.Drawing.Point(161, 4);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 0;
            this.btnLuu.Text = "&1.Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.txtTinhTP);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.dtpNgayCap);
            this.panelControl1.Controls.Add(this.dtpNgay);
            this.panelControl1.Controls.Add(this.txtNganHang);
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.txtTienBangSo);
            this.panelControl1.Controls.Add(this.txtTienBangChu);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtNoiCap);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.lbNguonChi);
            this.panelControl1.Controls.Add(this.lbNgayHenTra);
            this.panelControl1.Controls.Add(this.txtSoCMND);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtSoTaiKhoanThuHuong);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtDonViThuHuong);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtChiNhanhChuyen);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtSoTaiKhoanChuyen);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtDonViChuyenTien);
            this.panelControl1.Controls.Add(this.lbNCC1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 27);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1053, 320);
            this.panelControl1.TabIndex = 1;
            // 
            // txtTinhTP
            // 
            this.txtTinhTP.EnterMoveNextControl = true;
            this.txtTinhTP.Location = new System.Drawing.Point(496, 192);
            this.txtTinhTP.Name = "txtTinhTP";
            this.txtTinhTP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinhTP.Properties.Appearance.Options.UseFont = true;
            this.txtTinhTP.Size = new System.Drawing.Size(248, 26);
            this.txtTinhTP.TabIndex = 21;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(413, 200);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(63, 19);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "Tỉnh/TP:";
            // 
            // dtpNgayCap
            // 
            this.dtpNgayCap.EditValue = null;
            this.dtpNgayCap.EnterMoveNextControl = true;
            this.dtpNgayCap.Location = new System.Drawing.Point(161, 161);
            this.dtpNgayCap.Name = "dtpNgayCap";
            this.dtpNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayCap.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgayCap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgayCap.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgayCap.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.dtpNgayCap.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgayCap.Properties.AppearanceFocused.Options.UseFont = true;
            this.dtpNgayCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayCap.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayCap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayCap.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayCap.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayCap.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayCap.TabIndex = 15;
            // 
            // dtpNgay
            // 
            this.dtpNgay.EditValue = null;
            this.dtpNgay.EnterMoveNextControl = true;
            this.dtpNgay.Location = new System.Drawing.Point(161, 6);
            this.dtpNgay.Name = "dtpNgay";
            this.dtpNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.Appearance.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgay.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgay.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.dtpNgay.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpNgay.Properties.AppearanceFocused.Options.UseFont = true;
            this.dtpNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgay.Size = new System.Drawing.Size(248, 26);
            this.dtpNgay.TabIndex = 1;
            // 
            // txtNganHang
            // 
            this.txtNganHang.EnterMoveNextControl = true;
            this.txtNganHang.Location = new System.Drawing.Point(161, 192);
            this.txtNganHang.Name = "txtNganHang";
            this.txtNganHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNganHang.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtNganHang.Properties.Appearance.Options.UseFont = true;
            this.txtNganHang.Properties.Appearance.Options.UseForeColor = true;
            this.txtNganHang.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNganHang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtNganHang.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtNganHang.Size = new System.Drawing.Size(248, 26);
            this.txtNganHang.TabIndex = 19;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(161, 223);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Properties.Appearance.Options.UseForeColor = true;
            this.txtNoiDung.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNoiDung.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtNoiDung.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtNoiDung.Size = new System.Drawing.Size(583, 26);
            this.txtNoiDung.TabIndex = 23;
            // 
            // txtTienBangSo
            // 
            this.txtTienBangSo.EnterMoveNextControl = true;
            this.txtTienBangSo.Location = new System.Drawing.Point(161, 254);
            this.txtTienBangSo.Name = "txtTienBangSo";
            this.txtTienBangSo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienBangSo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtTienBangSo.Properties.Appearance.Options.UseFont = true;
            this.txtTienBangSo.Properties.Appearance.Options.UseForeColor = true;
            this.txtTienBangSo.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienBangSo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtTienBangSo.Properties.Mask.EditMask = "N0";
            this.txtTienBangSo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienBangSo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienBangSo.Size = new System.Drawing.Size(583, 26);
            this.txtTienBangSo.TabIndex = 25;
            this.txtTienBangSo.Leave += new System.EventHandler(this.txtTienBangSo_Leave);
            // 
            // txtTienBangChu
            // 
            this.txtTienBangChu.EnterMoveNextControl = true;
            this.txtTienBangChu.Location = new System.Drawing.Point(161, 285);
            this.txtTienBangChu.Name = "txtTienBangChu";
            this.txtTienBangChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienBangChu.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtTienBangChu.Properties.Appearance.Options.UseFont = true;
            this.txtTienBangChu.Properties.Appearance.Options.UseForeColor = true;
            this.txtTienBangChu.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienBangChu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtTienBangChu.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTienBangChu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienBangChu.Size = new System.Drawing.Size(583, 26);
            this.txtTienBangChu.TabIndex = 27;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(8, 257);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(116, 19);
            this.labelControl9.TabIndex = 24;
            this.labelControl9.Text = "Số tiền bằng số:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(8, 288);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(126, 19);
            this.labelControl8.TabIndex = 26;
            this.labelControl8.Text = "Số tiền bằng chữ:";
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.EnterMoveNextControl = true;
            this.txtNoiCap.Location = new System.Drawing.Point(496, 161);
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiCap.Properties.Appearance.Options.UseFont = true;
            this.txtNoiCap.Size = new System.Drawing.Size(248, 26);
            this.txtNoiCap.TabIndex = 17;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(413, 168);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(59, 19);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Nơi cấp:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(8, 164);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Ngày cấp:";
            // 
            // lbNguonChi
            // 
            this.lbNguonChi.AutoSize = true;
            this.lbNguonChi.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNguonChi.Location = new System.Drawing.Point(8, 195);
            this.lbNguonChi.Name = "lbNguonChi";
            this.lbNguonChi.Size = new System.Drawing.Size(117, 19);
            this.lbNguonChi.TabIndex = 18;
            this.lbNguonChi.Text = "Tại ngân hàng:";
            // 
            // lbNgayHenTra
            // 
            this.lbNgayHenTra.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayHenTra.Location = new System.Drawing.Point(8, 226);
            this.lbNgayHenTra.Name = "lbNgayHenTra";
            this.lbNgayHenTra.Size = new System.Drawing.Size(71, 19);
            this.lbNgayHenTra.TabIndex = 22;
            this.lbNgayHenTra.Text = "Nội dung:";
            // 
            // txtSoCMND
            // 
            this.txtSoCMND.EnterMoveNextControl = true;
            this.txtSoCMND.Location = new System.Drawing.Point(496, 130);
            this.txtSoCMND.Name = "txtSoCMND";
            this.txtSoCMND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCMND.Properties.Appearance.Options.UseFont = true;
            this.txtSoCMND.Size = new System.Drawing.Size(248, 26);
            this.txtSoCMND.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(413, 136);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(73, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Số CMND:";
            // 
            // txtSoTaiKhoanThuHuong
            // 
            this.txtSoTaiKhoanThuHuong.EnterMoveNextControl = true;
            this.txtSoTaiKhoanThuHuong.Location = new System.Drawing.Point(161, 130);
            this.txtSoTaiKhoanThuHuong.Name = "txtSoTaiKhoanThuHuong";
            this.txtSoTaiKhoanThuHuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTaiKhoanThuHuong.Properties.Appearance.Options.UseFont = true;
            this.txtSoTaiKhoanThuHuong.Size = new System.Drawing.Size(248, 26);
            this.txtSoTaiKhoanThuHuong.TabIndex = 11;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(8, 133);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(94, 19);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Số tài khoản:";
            // 
            // txtDonViThuHuong
            // 
            this.txtDonViThuHuong.EnterMoveNextControl = true;
            this.txtDonViThuHuong.Location = new System.Drawing.Point(161, 99);
            this.txtDonViThuHuong.Name = "txtDonViThuHuong";
            this.txtDonViThuHuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViThuHuong.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtDonViThuHuong.Properties.Appearance.Options.UseFont = true;
            this.txtDonViThuHuong.Properties.Appearance.Options.UseForeColor = true;
            this.txtDonViThuHuong.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDonViThuHuong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtDonViThuHuong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDonViThuHuong.Size = new System.Drawing.Size(583, 26);
            this.txtDonViThuHuong.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ngày :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(8, 102);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(131, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Đơn vị thụ hưởng:";
            // 
            // txtChiNhanhChuyen
            // 
            this.txtChiNhanhChuyen.EnterMoveNextControl = true;
            this.txtChiNhanhChuyen.Location = new System.Drawing.Point(496, 68);
            this.txtChiNhanhChuyen.Name = "txtChiNhanhChuyen";
            this.txtChiNhanhChuyen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiNhanhChuyen.Properties.Appearance.Options.UseFont = true;
            this.txtChiNhanhChuyen.Size = new System.Drawing.Size(248, 26);
            this.txtChiNhanhChuyen.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(413, 72);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Chi nhánh:";
            // 
            // txtSoTaiKhoanChuyen
            // 
            this.txtSoTaiKhoanChuyen.EnterMoveNextControl = true;
            this.txtSoTaiKhoanChuyen.Location = new System.Drawing.Point(161, 68);
            this.txtSoTaiKhoanChuyen.Name = "txtSoTaiKhoanChuyen";
            this.txtSoTaiKhoanChuyen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTaiKhoanChuyen.Properties.Appearance.Options.UseFont = true;
            this.txtSoTaiKhoanChuyen.Size = new System.Drawing.Size(248, 26);
            this.txtSoTaiKhoanChuyen.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(8, 71);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(94, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Số tài khoản:";
            // 
            // txtDonViChuyenTien
            // 
            this.txtDonViChuyenTien.EnterMoveNextControl = true;
            this.txtDonViChuyenTien.Location = new System.Drawing.Point(161, 37);
            this.txtDonViChuyenTien.Name = "txtDonViChuyenTien";
            this.txtDonViChuyenTien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViChuyenTien.Properties.Appearance.Options.UseFont = true;
            this.txtDonViChuyenTien.Size = new System.Drawing.Size(583, 26);
            this.txtDonViChuyenTien.TabIndex = 3;
            // 
            // lbNCC1
            // 
            this.lbNCC1.AutoSize = true;
            this.lbNCC1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNCC1.Location = new System.Drawing.Point(8, 40);
            this.lbNCC1.Name = "lbNCC1";
            this.lbNCC1.Size = new System.Drawing.Size(147, 19);
            this.lbNCC1.TabIndex = 2;
            this.lbNCC1.Text = "Đơn vị chuyển tiền:";
            // 
            // Frm_UyNhiemChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 513);
            this.Controls.Add(this.pnThongTinUyNhiemChi);
            this.Name = "Frm_UyNhiemChi";
            this.Text = "Lập phiếu uy nhiệm chi";
            this.Load += new System.EventHandler(this.Frm_UyNhiemChi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnThongTinUyNhiemChi)).EndInit();
            this.pnThongTinUyNhiemChi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBottom)).EndInit();
            this.pnBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBangSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienBangChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanThuHuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonViThuHuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiNhanhChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTaiKhoanChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonViChuyenTien.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn MaLoai;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraGrid.Columns.GridColumn DT_HOTEN;
        private DevExpress.XtraEditors.GroupControl pnThongTinUyNhiemChi;
        private DevExpress.XtraEditors.PanelControl pnBottom;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit dtpNgayCap;
        private DevExpress.XtraEditors.DateEdit dtpNgay;
        private DevExpress.XtraEditors.TextEdit txtNganHang;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraEditors.TextEdit txtTienBangSo;
        private DevExpress.XtraEditors.TextEdit txtTienBangChu;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtNoiCap;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Label lbNguonChi;
        private DevExpress.XtraEditors.LabelControl lbNgayHenTra;
        private DevExpress.XtraEditors.TextEdit txtSoCMND;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtSoTaiKhoanThuHuong;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtDonViThuHuong;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtSoTaiKhoanChuyen;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtDonViChuyenTien;
        private System.Windows.Forms.Label lbNCC1;
        private DevExpress.XtraEditors.TextEdit txtTinhTP;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtChiNhanhChuyen;
        private DevExpress.XtraEditors.LabelControl labelControl1;





    }
}