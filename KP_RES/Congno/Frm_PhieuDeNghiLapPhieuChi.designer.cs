﻿namespace KP_RES 
{
    partial class Frm_PhieuDeNghiLapPhieuChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_PhieuDeNghiLapPhieuChi));
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtMaPHC = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtLydo = new DevExpress.XtraEditors.TextEdit();
            this.pnthongtinphieu = new DevExpress.XtraEditors.GroupControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.cboNhanVien = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_mavach2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_HANGHOA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.look_ten2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_HANGHOA3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GIANHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_gianhap = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_vat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPHC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLydo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnthongtinphieu)).BeginInit();
            this.pnthongtinphieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_mavach2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).BeginInit();
            this.SuspendLayout();
            // 
            // TenLoai
            // 
            this.TenLoai.Name = "TenLoai";
            // 
            // MaLoai
            // 
            this.MaLoai.Name = "MaLoai";
            // 
            // MaPhieu
            // 
            this.MaPhieu.Name = "MaPhieu";
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            // 
            // DT_HOTEN
            // 
            this.DT_HOTEN.Name = "DT_HOTEN";
            // 
            // txtMaPHC
            // 
            this.txtMaPHC.Enabled = false;
            this.txtMaPHC.EnterMoveNextControl = true;
            this.txtMaPHC.Location = new System.Drawing.Point(82, 6);
            this.txtMaPHC.Name = "txtMaPHC";
            this.txtMaPHC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPHC.Properties.Appearance.Options.UseFont = true;
            this.txtMaPHC.Properties.DisplayFormat.FormatString = "0,0 vnd";
            this.txtMaPHC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaPHC.Properties.EditFormat.FormatString = "0,0 vnd";
            this.txtMaPHC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaPHC.Size = new System.Drawing.Size(250, 26);
            this.txtMaPHC.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số phiếu";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(9, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Lý do";
            // 
            // txtLydo
            // 
            this.txtLydo.EnterMoveNextControl = true;
            this.txtLydo.Location = new System.Drawing.Point(82, 38);
            this.txtLydo.Name = "txtLydo";
            this.txtLydo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLydo.Properties.Appearance.Options.UseFont = true;
            this.txtLydo.Size = new System.Drawing.Size(595, 26);
            this.txtLydo.TabIndex = 7;
            // 
            // pnthongtinphieu
            // 
            this.pnthongtinphieu.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnthongtinphieu.AppearanceCaption.Options.UseFont = true;
            this.pnthongtinphieu.Controls.Add(this.btnXoa);
            this.pnthongtinphieu.Controls.Add(this.cboNhanVien);
            this.pnthongtinphieu.Controls.Add(this.labelControl3);
            this.pnthongtinphieu.Controls.Add(this.btnLuu);
            this.pnthongtinphieu.Controls.Add(this.btnThem);
            this.pnthongtinphieu.Controls.Add(this.txtMaPHC);
            this.pnthongtinphieu.Controls.Add(this.label2);
            this.pnthongtinphieu.Controls.Add(this.txtLydo);
            this.pnthongtinphieu.Controls.Add(this.labelControl1);
            this.pnthongtinphieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnthongtinphieu.Location = new System.Drawing.Point(0, 0);
            this.pnthongtinphieu.Name = "pnthongtinphieu";
            this.pnthongtinphieu.ShowCaption = false;
            this.pnthongtinphieu.Size = new System.Drawing.Size(1733, 116);
            this.pnthongtinphieu.TabIndex = 1;
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(258, 71);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(106, 35);
            this.btnXoa.TabIndex = 114;
            this.btnXoa.Text = "&Xóa dòng";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // cboNhanVien
            // 
            this.cboNhanVien.EnterMoveNextControl = true;
            this.cboNhanVien.Location = new System.Drawing.Point(427, 6);
            this.cboNhanVien.Name = "cboNhanVien";
            this.cboNhanVien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVien.Properties.Appearance.Options.UseFont = true;
            this.cboNhanVien.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVien.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanVien.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanVien.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã NCC", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên NCC")});
            this.cboNhanVien.Properties.DisplayMember = "TEN";
            this.cboNhanVien.Properties.DropDownItemHeight = 40;
            this.cboNhanVien.Properties.NullText = "";
            this.cboNhanVien.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanVien.Properties.ShowHeader = false;
            this.cboNhanVien.Properties.ValueMember = "MA";
            this.cboNhanVien.Size = new System.Drawing.Size(250, 26);
            this.cboNhanVien.TabIndex = 22;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(339, 9);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 19);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Nhân viên";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(82, 71);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(170, 71);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1694, 116);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 625);
            this.panelControl2.TabIndex = 112;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(2, 185);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(35, 255);
            this.panelControl1.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 440);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 61);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 501);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 61);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 124);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 61);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 63);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 61);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 562);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 61);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 61);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 116);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_soluong,
            this.txt_gianhap,
            this.txt_chietkhau,
            this.txt_vat,
            this.look_mavach2,
            this.look_ten2});
            this.gridControl1.Size = new System.Drawing.Size(1694, 625);
            this.gridControl1.TabIndex = 113;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_VACH,
            this.TEN,
            this.DVT,
            this.SL,
            this.GIANHAP,
            this.THANHTIEN,
            this.GHICHU,
            this.Fill,
            this.MA});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_VACH
            // 
            this.MA_VACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_VACH.AppearanceCell.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_VACH.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.Caption = "Mã hàng";
            this.MA_VACH.ColumnEdit = this.look_mavach2;
            this.MA_VACH.FieldName = "MA_VACH";
            this.MA_VACH.Name = "MA_VACH";
            this.MA_VACH.OptionsColumn.FixedWidth = true;
            this.MA_VACH.Visible = true;
            this.MA_VACH.VisibleIndex = 1;
            this.MA_VACH.Width = 189;
            // 
            // look_mavach2
            // 
            this.look_mavach2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_mavach2.Appearance.Options.UseFont = true;
            this.look_mavach2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.look_mavach2.AppearanceDropDown.Options.UseFont = true;
            this.look_mavach2.AutoComplete = false;
            this.look_mavach2.AutoHeight = false;
            this.look_mavach2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_mavach2.DisplayMember = "MAVACH";
            this.look_mavach2.Name = "look_mavach2";
            this.look_mavach2.NullText = "";
            this.look_mavach2.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.look_mavach2.PopupFormSize = new System.Drawing.Size(180, 0);
            this.look_mavach2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_mavach2.ValueMember = "MAVACH";
            this.look_mavach2.View = this.repositoryItemGridLookUpEdit2View;
            this.look_mavach2.EditValueChanged += new System.EventHandler(this.look_mavach2_EditValueChanged);
            this.look_mavach2.Leave += new System.EventHandler(this.look_mavach2_Leave);
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_HANGHOA2,
            this.MA_VACH2});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // MA_HANGHOA2
            // 
            this.MA_HANGHOA2.Caption = "Mã hàng hóa";
            this.MA_HANGHOA2.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA2.Name = "MA_HANGHOA2";
            this.MA_HANGHOA2.Width = 192;
            // 
            // MA_VACH2
            // 
            this.MA_VACH2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH2.AppearanceCell.Options.UseFont = true;
            this.MA_VACH2.AppearanceCell.Options.UseTextOptions = true;
            this.MA_VACH2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_VACH2.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH2.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH2.Caption = "Mã hàng";
            this.MA_VACH2.FieldName = "MAVACH";
            this.MA_VACH2.Name = "MA_VACH2";
            this.MA_VACH2.OptionsColumn.FixedWidth = true;
            this.MA_VACH2.Visible = true;
            this.MA_VACH2.VisibleIndex = 0;
            this.MA_VACH2.Width = 180;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Hàng hóa";
            this.TEN.ColumnEdit = this.look_ten2;
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 290;
            // 
            // look_ten2
            // 
            this.look_ten2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_ten2.Appearance.Options.UseFont = true;
            this.look_ten2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_ten2.AppearanceDropDown.Options.UseFont = true;
            this.look_ten2.AutoComplete = false;
            this.look_ten2.AutoHeight = false;
            this.look_ten2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_ten2.DisplayMember = "TEN_HANGHOA";
            this.look_ten2.Name = "look_ten2";
            this.look_ten2.NullText = "";
            this.look_ten2.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.look_ten2.PopupFormSize = new System.Drawing.Size(300, 0);
            this.look_ten2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.look_ten2.ValueMember = "TEN_HANGHOA";
            this.look_ten2.View = this.repositoryItemGridLookUpEdit1View;
            this.look_ten2.EditValueChanged += new System.EventHandler(this.look_ten2_EditValueChanged);
            this.look_ten2.Leave += new System.EventHandler(this.look_ten2_Leave);
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_HANGHOA3,
            this.TEN_HANGHOA2});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // MA_HANGHOA3
            // 
            this.MA_HANGHOA3.Caption = "Mã hàng hóa";
            this.MA_HANGHOA3.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA3.Name = "MA_HANGHOA3";
            // 
            // TEN_HANGHOA2
            // 
            this.TEN_HANGHOA2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA2.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA2.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_HANGHOA2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA2.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA2.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA2.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA2.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA2.Name = "TEN_HANGHOA2";
            this.TEN_HANGHOA2.Visible = true;
            this.TEN_HANGHOA2.VisibleIndex = 0;
            this.TEN_HANGHOA2.Width = 300;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 3;
            this.DVT.Width = 118;
            // 
            // SL
            // 
            this.SL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SL.AppearanceCell.Options.UseFont = true;
            this.SL.AppearanceCell.Options.UseTextOptions = true;
            this.SL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.Caption = "SL";
            this.SL.ColumnEdit = this.txt_soluong;
            this.SL.DisplayFormat.FormatString = "n";
            this.SL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SL.OptionsColumn.FixedWidth = true;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 4;
            this.SL.Width = 100;
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            this.txt_soluong.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_soluong_EditValueChanging);
            this.txt_soluong.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_soluong_KeyPress);
            // 
            // GIANHAP
            // 
            this.GIANHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIANHAP.AppearanceCell.Options.UseFont = true;
            this.GIANHAP.AppearanceCell.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIANHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIANHAP.AppearanceHeader.Options.UseFont = true;
            this.GIANHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIANHAP.Caption = "Đơn giá";
            this.GIANHAP.ColumnEdit = this.txt_gianhap;
            this.GIANHAP.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIANHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIANHAP.FieldName = "GIANHAP";
            this.GIANHAP.Name = "GIANHAP";
            this.GIANHAP.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIANHAP.OptionsColumn.FixedWidth = true;
            this.GIANHAP.Visible = true;
            this.GIANHAP.VisibleIndex = 5;
            this.GIANHAP.Width = 150;
            // 
            // txt_gianhap
            // 
            this.txt_gianhap.AutoHeight = false;
            this.txt_gianhap.Mask.EditMask = "N0";
            this.txt_gianhap.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_gianhap.Name = "txt_gianhap";
            this.txt_gianhap.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txt_gianhap_EditValueChanging);
            this.txt_gianhap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_soluong_KeyPress);
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.Caption = "Thành tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 6;
            this.THANHTIEN.Width = 200;
            // 
            // Fill
            // 
            this.Fill.Name = "Fill";
            this.Fill.OptionsColumn.AllowEdit = false;
            this.Fill.OptionsColumn.AllowFocus = false;
            this.Fill.Visible = true;
            this.Fill.VisibleIndex = 8;
            this.Fill.Width = 305;
            // 
            // MA
            // 
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.AutoHeight = false;
            this.txt_chietkhau.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_chietkhau.Name = "txt_chietkhau";
            // 
            // txt_vat
            // 
            this.txt_vat.AutoHeight = false;
            this.txt_vat.Name = "txt_vat";
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 7;
            this.GHICHU.Width = 290;
            // 
            // Frm_PhieuDeNghiLapPhieuChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1733, 741);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnthongtinphieu);
            this.Name = "Frm_PhieuDeNghiLapPhieuChi";
            this.Text = "Lập phiếu đề nghị chi";
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPHC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLydo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnthongtinphieu)).EndInit();
            this.pnthongtinphieu.ResumeLayout(false);
            this.pnthongtinphieu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_mavach2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_ten2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_vat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn MaLoai;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraGrid.Columns.GridColumn DT_HOTEN;
        private DevExpress.XtraEditors.TextEdit txtMaPHC;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLydo;
        private DevExpress.XtraEditors.GroupControl pnthongtinphieu;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit look_mavach2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA2;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH2;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit look_ten2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA3;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA2;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraGrid.Columns.GridColumn GIANHAP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_gianhap;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_vat;
        private DevExpress.XtraEditors.LookUpEdit cboNhanVien;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH;
        private DevExpress.XtraGrid.Columns.GridColumn Fill;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;





    }
}