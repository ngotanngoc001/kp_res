﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_PhieuDeNghiLapPhieuChi : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();

        public Frm_PhieuDeNghiLapPhieuChi()
        {
            InitializeComponent();
            try
            {
                LoadPermission();
                SetDulieuKhoiTao();
                LoadData();
                KhoaMoControl(false);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        private void LoadData()
        {
            String sqlcheck = "SELECT * FROM CN_LOAI_CONGNO";
            DataTable dtcheck = clsMain.ReturnDataTable(sqlcheck);
            if (dtcheck.Rows.Count == 0)
            {
                String sqlInsert = "insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('01',N'Thu Khách Hàng',N'thu từ nguồn khách hang','01') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('02',N'Thu Khác',N'thu từ nguồn khác','01')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('03',N'Chi Nhà Cung Cấp',N'Chi cho nhà cung cấp','02')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('04',N'Chi Nhân Viên',N'Chi cho nhân viên','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('05',N'Chi Khác',N'Chi cho các dịch vụ khác','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('06',N'Chi Trả Hàng',N'Chi tiền trả hàng','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('07',N'Chi Khách Hàng',N'Chi tiền cho khách hàng','02')  ";
                clsMain.ExecuteSQL(sqlInsert);
            }

            txtMaPHC.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCode_PHIEUDENGHIDUYETCHI]()").Rows[0][0].ToString();
            DataTable myDT = clsMain.ReturnDataTable("SELECT MANHANVIEN AS MA, TENNHANVIEN AS TEN FROM DM_NHANVIEN WHERE SUDUNG = 1 ORDER BY TENNHANVIEN");
            if (myDT.Rows.Count > 0)
            {
                cboNhanVien.Properties.DataSource = myDT;
                cboNhanVien.EditValue = myDT.Rows[0]["MA"].ToString();
            }

            String sSQL = "";
            sSQL += "SELECT MA_HANGHOA, TEN_HANGHOA, THUE, GIANHAP, TEN_DONVITINH, '' As TONHIENTAI, MAVACH ";
            sSQL += "FROM HANGHOA h, DONVITINH d ";
            sSQL += "WHERE h.MA_DONVITINH = d.MA_DONVITINH ";
            sSQL += "ORDER BY TEN_HANGHOA";

            myDT = clsMain.ReturnDataTable(sSQL);
            if (myDT.Rows.Count > 0)
            {
                look_ten2.DataSource = myDT;
                look_mavach2.DataSource = myDT;
            }
        }

        private void SetDulieuKhoiTao()
        {
            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("MA", typeof(string));
            DTSOUCRE.Columns.Add("MA_VACH", typeof(string));
            DTSOUCRE.Columns.Add("TEN", typeof(string));
            DTSOUCRE.Columns.Add("DVT", typeof(string));
            DTSOUCRE.Columns.Add("SL", typeof(float));
            DTSOUCRE.Columns.Add("GIANHAP", typeof(long));
            DTSOUCRE.Columns.Add("THANHTIEN", typeof(long));
            DTSOUCRE.Columns.Add("TONGCONG", typeof(long));
            DTSOUCRE.Columns.Add("GHICHU", typeof(string));
            DTSOUCRE.Rows.Add(null, null, null, null, null, null, null, null,null );
            BS.DataSource = DTSOUCRE;
            gridControl1.DataSource = BS;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("26071601");
            btnLuu.Enabled = btnThem.Enabled;   
        }

        private void KhoaMoControl(bool flag)
        {
            cboNhanVien.Properties.ReadOnly = txtLydo.Properties.ReadOnly = !flag;
            gridView1.OptionsBehavior.Editable = flag;;
            txtMaPHC.Properties.ReadOnly = true;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            txtMaPHC.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCode_PHIEUDENGHIDUYETCHI]()").Rows[0][0].ToString();
            txtLydo.Text = "";
            SetDulieuKhoiTao();
            KhoaMoControl(true);
            cboNhanVien.Focus();
        }

        private void look_ten2_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_PhieuDeNghiLapPhieuChi.BS.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null);
            }
        }

        private void look_ten2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA", ((DataRowView)editor.GetSelectedDataRow())["MA_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_VACH", ((DataRowView)editor.GetSelectedDataRow())["MAVACH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN", ((DataRowView)editor.GetSelectedDataRow())["TEN_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DVT", ((DataRowView)editor.GetSelectedDataRow())["TEN_DONVITINH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL", 1);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIEN", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
            }
            catch
            {
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                gridView1.ActiveEditor.SelectAll();
            }));
        }

        private void txt_soluong_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || float.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txt_soluong.EditValueChanging -= txt_soluong_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["SL"] = 1;
                    dv["THANHTIEN"] = long.Parse(dv["GIANHAP"].ToString());

                    e.NewValue = 1;
                    BS.ResetCurrentItem();
                }
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txt_soluong.EditValueChanging -= txt_soluong_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;
                        float _SOLUONG = float.Parse(e.NewValue.ToString());
                        dv["THANHTIEN"] = long.Parse(dv["GIANHAP"].ToString()) * _SOLUONG;
                        dv["SL"] = e.NewValue;
                        BS.ResetCurrentItem();
                    }
                }
                txt_soluong.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_soluong_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txt_soluong_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    if (gridView1.GetFocusedRowCellValue(MA).Equals(""))
                        e.Handled = true;
                    else
                    {
                        if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar) & e.KeyChar != '-')
                            e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txt_gianhap_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || long.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txt_gianhap.EditValueChanging -= txt_gianhap_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["GIANHAP"] = long.Parse(dv["GIANHAP"].ToString());
                    dv["THANHTIEN"] = long.Parse(dv["GIANHAP"].ToString()) * float.Parse(dv["SL"].ToString());
                    e.NewValue = long.Parse(dv["GIANHAP"].ToString());
                    BS.ResetCurrentItem();
                }
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txt_gianhap.EditValueChanging -= txt_gianhap_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;
                        float _GIANHAP = float.Parse(e.NewValue.ToString());
                        dv["GIANHAP"] = e.NewValue;
                        dv["THANHTIEN"] = long.Parse(dv["GIANHAP"].ToString()) * long.Parse(dv["SL"].ToString());
                        BS.ResetCurrentItem();
                    }
                }
                txt_gianhap.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txt_gianhap_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void look_mavach2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA", ((DataRowView)editor.GetSelectedDataRow())["MA_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_VACH", ((DataRowView)editor.GetSelectedDataRow())["MAVACH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN", ((DataRowView)editor.GetSelectedDataRow())["TEN_HANGHOA"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DVT", ((DataRowView)editor.GetSelectedDataRow())["TEN_DONVITINH"].ToString());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL", 1);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANHTIEN", ((DataRowView)editor.GetSelectedDataRow())["GIANHAP"].ToString());
            }
            catch
            {
            }
        }

        private void look_mavach2_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_PhieuDeNghiLapPhieuChi.BS.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null, null, null, null, null); 
            }
        }

        private long  sTongTien = 0;

        private String ChiTietPhieuChi()
        {
            sTongTien = 0;
            String sSQL = "";
            DataTable myDT = (DataTable)BS.DataSource;
            foreach (DataRow myDR in myDT.Rows)
            {
                if (myDR["MA"].ToString() != "")
                {
                    sTongTien += long.Parse(myDR["THANHTIEN"].ToString());

                    sSQL += "INSERT INTO CHITIETPHIEUDENGHIDUYETCHI(PHIEUDENGHI,HANGHOA,SOLUONG,DONGIA,THANHTIEN,TEN_DONVITINH, DUYET, NHANVIEN,GHICHU)" + "\n";
                    sSQL += "VALUES(";
                    sSQL += clsMain.SQLString(txtMaPHC.Text) + ",";
                    sSQL += clsMain.SQLString(myDR["MA"].ToString()) + ",";
                    sSQL += clsMain.SQLString(myDR["SL"].ToString()) + ",";
                    sSQL += clsMain.SQLString(myDR["GIANHAP"].ToString()) + ",";
                    sSQL += clsMain.SQLString(myDR["THANHTIEN"].ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(myDR["DVT"].ToString()) + ",";
                    sSQL += clsMain.SQLBit(false) + ",";
                    sSQL += clsMain.SQLStringUnicode(cboNhanVien.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(myDR["GHICHU"].ToString()) + ")";
                    sSQL += "\n";
                }
                else
                    sSQL += "\n";
            }
            return sSQL;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                checkinputbeforeLuu();
                String SQL_ChiTietPhieuChi = ChiTietPhieuChi();
                String SQL_PhieuChi = string.Format("EXEC sp_CN_CONGNO_PHIEUDENGHIDUYETCHI_INSERT_NS @MAPDNDC='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}',@TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14:yyyyMMdd HH:mm:ss}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN=N'{16}',@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN=N'{18}',@SOGIAODICH=N'{19}',@HOTENGIAODICH=N'{20}',@NGUONCHI='{21}'", txtMaPHC.Text, "0", "", "", clsUserManagement.ReturnMaNVOfUserLogin(), "05", txtLydo.Text, sTongTien, "", sTongTien, cboNhanVien.Text, "", "", "01", DateTime.Now, "", "", "", "", "", cboNhanVien.Text, "1");
                if (DialogResult.Yes == (XtraMessageBox.Show("Bạn có chắc muốn lập phiếu đề nghị chi này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    clsMain.ExecuteSQL(SQL_PhieuChi);
                    clsMain.ExecuteSQL(SQL_ChiTietPhieuChi);


                    DataTable myDT = clsMain.ReturnDataTable("EXEC SP_INPHIEUDENGHIDUYETCHI " + clsMain.SQLString(txtMaPHC.Text));
                    XtraMessageBox.Show("Thêm phiếu đề nghị chi thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 120;
                    frm.ShowDialog();
                    frm.Dispose();

                    txtMaPHC.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCode_PHIEUDENGHIDUYETCHI]()").Rows[0][0].ToString();
                    txtLydo.Text = "";
                    SetDulieuKhoiTao();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                if (gridView1.GetFocusedRowCellValue(MA).ToString() != string.Empty)
                    gridView1.DeleteSelectedRows();
            }
        }

        private void checkinputbeforeLuu()
        {
            if (txtLydo.Text == "")
                throw new Exception("Chưa nhập lý do");
            if (gridView1.RowCount == 1 && gridView1.GetFocusedRowCellValue(TEN).ToString() == "")
                throw new Exception("Chưa có mặt hàng nào ");
            if (cboNhanVien.EditValue == null)
                throw new Exception("Chưa chọn nhân viên ");
        }
    }
}