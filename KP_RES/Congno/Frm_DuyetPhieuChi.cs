﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_DuyetPhieuChi : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();
        int close = 0;

        string _MaPhieu = "";
        public Frm_DuyetPhieuChi()
        {
            InitializeComponent();
            LoadData();
            panel4.Visible = false;
        }

        public Frm_DuyetPhieuChi(String maphieu)
        {
            InitializeComponent();
            _MaPhieu = maphieu;
            LoadData();
            LoadPhieuDeNghiXuatKho(maphieu);
            KhoaControl();
            panel4.Visible = true;
            title.Text = this.Text;
            close = 1;
        }

        private void KhoaControl()
        {
            txtMaPHC.Properties.ReadOnly = cboNhanVien.Properties.ReadOnly = txtLydo.Properties.ReadOnly = true;
            gridView1.OptionsBehavior.Editable = true ;
        }

        private void LoadPhieuDeNghiXuatKho(String MaPhieu)
        {
            String sSQL = "";
            sSQL += "SELECT A.MAPDNDC, A.LYDO, B.HANGHOA AS MA, B.SOLUONG AS SL, B.DONGIA AS GIANHAP, B.THANHTIEN, B.TEN_DONVITINH AS DVT, DUYET, TEN_HANGHOA AS TEN, C.MAVACH AS MA_VACH, B.NHANVIEN" + "\n";
            sSQL += "FROM CN_CONGNO_PHIEUDENGHIDUYETCHI A " + "\n";
            sSQL += "INNER JOIN CHITIETPHIEUDENGHIDUYETCHI B ON A.MAPDNDC = B.PHIEUDENGHI" + "\n";
            sSQL += "INNER JOIN HANGHOA C ON B.HANGHOA = C.MA_HANGHOA" + "\n";
            sSQL += "WHERE A.TRANGTHAI = 0 " + "\n";
            sSQL += "AND A.MAPDNDC = " + clsMain.SQLString(MaPhieu);
            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            if (myDT.Rows.Count > 0)
            {
                txtMaPHC.Text = myDT.Rows[0]["MAPDNDC"].ToString();
                txtLydo.Text = myDT.Rows[0]["LYDO"].ToString();
                cboNhanVien.EditValue = int.Parse(myDT.Rows[0]["NHANVIEN"].ToString());

                BS.DataSource = myDT;
                gridControl1.DataSource = BS;
            }
        }

        private void LoadData()
        {
            string sqlcheck = "SELECT * FROM CN_LOAI_CONGNO";
            DataTable dtcheck = clsMain.ReturnDataTable(sqlcheck);
            if (dtcheck.Rows.Count == 0)
            {
                string sqlInsert = "insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('01',N'Thu Khách Hàng',N'thu từ nguồn khách hang','01') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('02',N'Thu Khác',N'thu từ nguồn khác','01')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('03',N'Chi Nhà Cung Cấp',N'Chi cho nhà cung cấp','02')  insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('04',N'Chi Nhân Viên',N'Chi cho nhân viên','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('05',N'Chi Khác',N'Chi cho các dịch vụ khác','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('06',N'Chi Trả Hàng',N'Chi tiền trả hàng','02') insert into CN_LOAI_CONGNO(MaLoai,TenLoai,ChuThich,LoaiThuChi) values ('07',N'Chi Khách Hàng',N'Chi tiền cho khách hàng','02')  ";
                clsMain.ExecuteSQL(sqlInsert);
            }

            DataTable myDT = clsMain.ReturnDataTable("SELECT MANHANVIEN AS MA, TENNHANVIEN AS TEN FROM DM_NHANVIEN WHERE SUDUNG = 1 ORDER BY TENNHANVIEN");
            if (myDT.Rows.Count > 0)
            {
                cboNhanVien.Properties.DataSource = myDT;
                cboNhanVien.EditValue = myDT.Rows[0]["MA"].ToString();
            }

            String sSQL = "";
            sSQL += "SELECT MA_HANGHOA, TEN_HANGHOA, THUE, GIANHAP, TEN_DONVITINH, '' As TONHIENTAI, MAVACH ";
            sSQL += "FROM HANGHOA h, DONVITINH d ";
            sSQL += "WHERE h.MA_DONVITINH = d.MA_DONVITINH ";
            sSQL += "ORDER BY TEN_HANGHOA";

            myDT = clsMain.ReturnDataTable(sSQL);
            if (myDT.Rows.Count > 0)
            {
                look_ten2.DataSource = myDT;
                look_mavach2.DataSource = myDT;
            }
        }
       
        private void btn_HuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntDuyet_Click(object sender, EventArgs e)
        {
            Double sTongTien = 0f;
            String sSQL = "";
            sSQL += "UPDATE CN_CONGNO_PHIEUDENGHIDUYETCHI SET" + "\n";
            sSQL += "TRANGTHAI = 1, NGUOIDUYET = " + clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + "\n";
            sSQL += "WHERE MAPDNDC = " + clsMain.SQLString(_MaPhieu);
            if (clsMain.ExecuteSQL(sSQL))
            {
                DataTable myDT = (DataTable)BS.DataSource;
                foreach (DataRow myDR in myDT.Rows)
                {
                    if (Boolean.Parse(myDR["DUYET"].ToString()))
                    {
                        sTongTien += Double.Parse(myDR["THANHTIEN"].ToString());

                        sSQL = "";
                        sSQL += "UPDATE CHITIETPHIEUDENGHIDUYETCHI SET" + "\n";
                        sSQL += "DUYET = 1" + "\n";
                        sSQL += "WHERE PHIEUDENGHI = " + clsMain.SQLString(_MaPhieu) + "\n";
                        sSQL += "AND HANGHOA = " + clsMain.SQLString(myDR["MA"].ToString()) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                    }
                }

                XtraMessageBox.Show("Phiếu đã được duyệt thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                XtraMessageBox.Show("Xảy ra lỗi trong quá trình duyệt phiếu này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnKoDuyet_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "UPDATE CN_CONGNO_PHIEUDENGHIDUYETCHI SET" + "\n";
            sSQL += "TRANGTHAI = 2, NGUOIDUYET = " + clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + "\n";
            sSQL += "WHERE MAPDNDC = " + clsMain.SQLString(_MaPhieu);
            if (clsMain.ExecuteSQL(sSQL))
            {
                XtraMessageBox.Show("Bạn đã không duyệt phiếu này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                XtraMessageBox.Show("Xảy ra lỗi trong quá trình duyệt phiếu này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {

        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }
    }
}