﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;

namespace KP_RES
{
    public partial class Frm_BaoCaoQuyTienMat : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtQuyTienMat;
        public Frm_BaoCaoQuyTienMat()
        {
            InitializeComponent();
           // LoadData();
            btnXacnhan.Visible = clsUserManagement.AllowView("981601");
        }

       
        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gcDanhSach.DataSource = null;
            if (optGroup.SelectedIndex == 0)
            {
                pnChoose.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
                date_tungay.EditValue = date_denngay.EditValue = clsMain.GetServerDate();
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
            }
          
        }
                

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                splashScreenManager1.ShowWaitForm();
                if (optGroup.SelectedIndex == 0)
                {
                    try
                    {
                        clsMain.ExecuteSQL("EXEC SP_TINH_QUY_TIEN_MAT");
                        string sql ="exec sp_CN_CONGNO_DANHSACH_THU_CHI_NS @NgayTu='',@NgayDen=''";
                        dtQuyTienMat = clsMain.ReturnDataTable(sql);

                        if (dtQuyTienMat.Rows.Count == 0)
                        {
                            gcDanhSach.DataSource = dtQuyTienMat;
                            throw new Exception("Không có phiếu thu chi nào");
                        }
                        dtQuyTienMat.Columns.Add("SOTIENTHU", typeof(decimal));
                        dtQuyTienMat.Columns.Add("SOTIENCHI", typeof(decimal));
                        dtQuyTienMat.Columns.Add("DienGiai", typeof(string));
                        dtQuyTienMat.Columns.Add("DauKy", typeof(decimal));
                        dtQuyTienMat.Columns.Add("TuNgay", typeof(string));
                        dtQuyTienMat.Columns.Add("DenNgay", typeof(string));
                        dtQuyTienMat.Columns.Add("Ngay", typeof(string));
                        dtQuyTienMat.Columns.Add("Thang", typeof(string));
                        dtQuyTienMat.Columns.Add("Nam", typeof(string));
                        dtQuyTienMat.Columns.Add("NguoiLap", typeof(string));
                        dtQuyTienMat.Columns.Add("TongThu", typeof(string));
                        dtQuyTienMat.Columns.Add("TongChi", typeof(string));
                        dtQuyTienMat.Columns.Add("TongTon", typeof(string));
                        decimal tongThu = 0;
                        decimal tongChi = 0; ;
                        for (int i = 0; i < dtQuyTienMat.Rows.Count; i++)
                        {
                            if (dtQuyTienMat.Rows[i]["MAPCN"].ToString().Substring(0, 2) == "PT")
                            {
                                dtQuyTienMat.Rows[i]["SOTIENTHU"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                                tongThu += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                            }
                            else
                            {
                                dtQuyTienMat.Rows[i]["SOTIENCHI"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                                tongChi += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                            }
                        }
                        dtQuyTienMat.Rows[0]["TongThu"] = string.Format("{0:0,0}", tongThu);
                        dtQuyTienMat.Rows[0]["TongChi"] = string.Format("{0:0,0}", tongChi);
                        dtQuyTienMat.Rows[0]["TongTon"] = string.Format("{0:0,0}", dtQuyTienMat.Rows[dtQuyTienMat.Rows.Count - 1]["TONGTIEN"]);
                        gcDanhSach.DataSource = dtQuyTienMat;
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                if (optGroup.SelectedIndex == 1)
                {
                    clsMain.ExecuteSQL("EXEC SP_TINH_QUY_TIEN_MAT");
                    string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_THU_CHI_NS @NgayTu='{0:yyyyMMdd}',@NgayDen='{1:yyyyMMdd}'", date_tungay.EditValue, date_denngay.EditValue);
                    dtQuyTienMat = clsMain.ReturnDataTable(sql);
                    if (dtQuyTienMat.Rows.Count == 0)
                    {
                        gcDanhSach.DataSource = dtQuyTienMat;
                        throw new Exception("Không có phiếu thu chi nào");
                    }
                    dtQuyTienMat.Columns.Add("SOTIENTHU", typeof(decimal));
                    dtQuyTienMat.Columns.Add("SOTIENCHI", typeof(decimal));
                    dtQuyTienMat.Columns.Add("DienGiai", typeof(string));
                    dtQuyTienMat.Columns.Add("DauKy", typeof(decimal));
                    dtQuyTienMat.Columns.Add("TuNgay", typeof(string));
                    dtQuyTienMat.Columns.Add("DenNgay", typeof(string));
                    dtQuyTienMat.Columns.Add("Ngay", typeof(string));
                    dtQuyTienMat.Columns.Add("Thang", typeof(string));
                    dtQuyTienMat.Columns.Add("Nam", typeof(string));
                    dtQuyTienMat.Columns.Add("NguoiLap", typeof(string));
                    dtQuyTienMat.Columns.Add("TongThu", typeof(string));
                    dtQuyTienMat.Columns.Add("TongChi", typeof(string));
                    dtQuyTienMat.Columns.Add("TongTon", typeof(string));
                    decimal tongThu = 0;
                    decimal tongChi = 0; ;
                    for (int i = 0; i < dtQuyTienMat.Rows.Count; i++)
                    {
                        if (dtQuyTienMat.Rows[i]["MAPCN"].ToString().Substring(0, 2) == "PT")
                        {
                            dtQuyTienMat.Rows[i]["SOTIENTHU"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                            tongThu += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                        }
                        else
                        {
                            dtQuyTienMat.Rows[i]["SOTIENCHI"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                            tongChi += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                        }
                    }
                    dtQuyTienMat.Rows[0]["TongThu"] = string.Format("{0:0,0}", tongThu);
                    dtQuyTienMat.Rows[0]["TongChi"] = string.Format("{0:0,0}", tongChi);
                    dtQuyTienMat.Rows[0]["TongTon"] = string.Format("{0:0,0}", dtQuyTienMat.Rows[dtQuyTienMat.Rows.Count - 1]["TONGTIEN"]);
                    gcDanhSach.DataSource = dtQuyTienMat;
                }
                if (optGroup.SelectedIndex == 2)
                {
                    clsMain.ExecuteSQL("EXEC SP_TINH_QUY_TIEN_MAT_NGAYTHANHTOAN");
                    string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_THU_CHI_NS_NGAYTHANHTOAN @NgayTu='{0:yyyyMMdd}',@NgayDen='{1:yyyyMMdd}'", date_tungay.EditValue, date_denngay.EditValue);
                    dtQuyTienMat = clsMain.ReturnDataTable(sql);
                    if (dtQuyTienMat.Rows.Count == 0)
                    {
                        gcDanhSach.DataSource = dtQuyTienMat;
                        throw new Exception("Không có phiếu thu chi nào");
                    }
                    dtQuyTienMat.Columns.Add("SOTIENTHU", typeof(decimal));
                    dtQuyTienMat.Columns.Add("SOTIENCHI", typeof(decimal));
                    dtQuyTienMat.Columns.Add("DienGiai", typeof(string));
                    dtQuyTienMat.Columns.Add("DauKy", typeof(decimal));
                    dtQuyTienMat.Columns.Add("TuNgay", typeof(string));
                    dtQuyTienMat.Columns.Add("DenNgay", typeof(string));
                    dtQuyTienMat.Columns.Add("Ngay", typeof(string));
                    dtQuyTienMat.Columns.Add("Thang", typeof(string));
                    dtQuyTienMat.Columns.Add("Nam", typeof(string));
                    dtQuyTienMat.Columns.Add("NguoiLap", typeof(string));
                    dtQuyTienMat.Columns.Add("TongThu", typeof(string));
                    dtQuyTienMat.Columns.Add("TongChi", typeof(string));
                    dtQuyTienMat.Columns.Add("TongTon", typeof(string));
                    decimal tongThu = 0;
                    decimal tongChi = 0; ;
                    for (int i = 0; i < dtQuyTienMat.Rows.Count; i++)
                    {
                        if (dtQuyTienMat.Rows[i]["MAPCN"].ToString().Substring(0, 2) == "PT")
                        {
                            dtQuyTienMat.Rows[i]["SOTIENTHU"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                            tongThu += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                        }
                        else
                        {
                            dtQuyTienMat.Rows[i]["SOTIENCHI"] = dtQuyTienMat.Rows[i]["SOTIEN"];
                            tongChi += decimal.Parse(dtQuyTienMat.Rows[i]["SOTIEN"].ToString());
                        }
                    }
                    dtQuyTienMat.Rows[0]["TongThu"] = string.Format("{0:0,0}", tongThu);
                    dtQuyTienMat.Rows[0]["TongChi"] = string.Format("{0:0,0}", tongChi);
                    dtQuyTienMat.Rows[0]["TongTon"] = string.Format("{0:0,0}", dtQuyTienMat.Rows[dtQuyTienMat.Rows.Count - 1]["TONGTIEN"]);
                    gcDanhSach.DataSource = dtQuyTienMat;
                }
                splashScreenManager1.CloseWaitForm();
            }
            catch(Exception ex)
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        
        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gcDanhSach.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gcDanhSach.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {


                    if (gridView1.RowCount == 0)
                        throw new Exception("Không có dữ liệu ");

                    for (int i = 0; i < dtQuyTienMat.Rows.Count; i++)
                    {
                        string dienGiai;
                        dienGiai = dtQuyTienMat.Rows[i]["TenLoai"].ToString() + "-" + dtQuyTienMat.Rows[i]["HOTEN"].ToString() + "-" + dtQuyTienMat.Rows[i]["LYDO"].ToString();
                        //+"-" + dtQuyTienMat.Rows[i]["MA_HOADONTAM"].ToString() + dtQuyTienMat.Rows[i]["PK_MAPHIEUKHO"].ToString();
                        if (dtQuyTienMat.Rows[i]["SOPHIEU"].ToString().Length ==10)
                        {
                            dienGiai += "- Hóa Đơn Số:" + dtQuyTienMat.Rows[i]["SOPHIEU"].ToString();
                        }
                        else
                            if (dtQuyTienMat.Rows[i]["SOPHIEU"].ToString().Length==15)
                            {
                                dienGiai += "- Phiếu Kho Số:" + dtQuyTienMat.Rows[i]["SOPHIEU"].ToString();
                            }
                        dtQuyTienMat.Rows[i]["DienGiai"] = dienGiai;
                    }

                    dtQuyTienMat.Rows[0]["TuNgay"] = date_tungay.Text;
                    dtQuyTienMat.Rows[0]["DenNgay"] = date_denngay.Text;
                    string sql;

                    if (optGroup.SelectedIndex == 0)
                    {
                        sql = string.Format("select top(1)TONGTIEN from CN_CONGNO where SUDUNG='true' and CONVERT(varchar,NGAYLAPPHIEU,112)<'{0:yyyyMMdd}' order by NGAYLAPPHIEU desc", "01/01/1990");
                    }
                    else if (optGroup.SelectedIndex == 1)
                    {
                        sql = string.Format("select top(1)TONGTIEN from CN_CONGNO where SUDUNG='true' and CONVERT(varchar,NGAYLAPPHIEU,112)<'{0:yyyyMMdd}' order by NGAYLAPPHIEU desc", date_tungay.EditValue);
                    }
                    else
                    {
                        sql = string.Format("select top(1)TONGTIEN1 from CN_CONGNO where SUDUNG='true' and CONVERT(varchar,NGAYTHANHTOAN,112)<'{0:yyyyMMdd}' order by CONVERT(DATE,NGAYTHANHTOAN)desc,NGAYLAPPHIEU desc", date_tungay.EditValue);
                    }
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    if (dt.Rows.Count > 0)
                    {
                        dtQuyTienMat.Rows[0]["DauKy"] = dt.Rows[0][0];
                    }
                    else
                    {
                        dtQuyTienMat.Rows[0]["DauKy"] = "0";
                    }
                    dtQuyTienMat.Rows[0]["Ngay"] = KP_UserManagement.clsMain.GetServerDate().Day;
                    dtQuyTienMat.Rows[0]["Thang"] = KP_UserManagement.clsMain.GetServerDate().Month;
                    dtQuyTienMat.Rows[0]["Nam"] = KP_UserManagement.clsMain.GetServerDate().Year;
                    string sql1 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                    dtQuyTienMat.Rows[0]["NguoiLap"] = clsMain.ReturnDataTable(sql1).Rows[0][0].ToString();


                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex == 1)
                        Description = "( " + Description + " - Từ ngày: " + date_tungay.Text + " - Đến ngày: " + date_denngay.Text + " )";
                    else if (optGroup.SelectedIndex == 2)
                        Description = "( " + Description + " - Từ ngày: " + date_tungay.Text + " - Đến ngày: " + date_denngay.Text + " )";

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;
                    if (cls_KP_RES.ContainColumn("HEARDER", dtQuyTienMat) == false)
                        dtQuyTienMat.Columns.Add(All);


                    //DataTable dt = (DataTable)gcDanhSach.DataSource;
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dtQuyTienMat;
                    frm.Mode = 26;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gridView1_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == TONGTIEN)
            {
                e.Info.DisplayText = (Convert.ToInt64(SOTIENTHU.SummaryItem.SummaryValue) - Convert.ToInt64(SOTIENCHI.SummaryItem.SummaryValue)).ToString("N0");
            }
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.GetFocusedRowCellValue(MAPCN).ToString().Substring(0, 2) == "PT")
            {
                btnXacnhan.Text = "Xác nhận đã thu đủ";
            }
            else
            {
                btnXacnhan.Text = "Xác nhận đã chi đủ";
            }
            if (gridView1.GetFocusedRowCellValue(XACNHAN).ToString() == "True")
            {
                btnXacnhan.Enabled = false;
            }
            else
            {
                btnXacnhan.Enabled = true ;
            }
        }

        private void btnXacnhan_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount < 1)
            {
                return;
            }
            string sSQL = "";
            sSQL += "update CN_CONGNO SET XACNHAN=1,NGUOIXACNHAN=" + clsMain.SQLString(clsGlobal.gsUserID) + ",	NGAYXACNHAN=GETDATE()" + "\n";
            sSQL += "Where MAPCN=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MAPCN).ToString());
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Đã xác nhận thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnTimkiem_Click(null, null);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận không thành công. Thử lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (View.GetRowCellValue(e.RowHandle, View.Columns["XACNHAN"]).ToString() == "True")
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}