﻿namespace KP_RES
{
    partial class PU_CONGNO_NCC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNgay = new System.Windows.Forms.Label();
            this.lbNo = new DevExpress.XtraEditors.LabelControl();
            this.lbSDT = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.lbChiTiet = new DevExpress.XtraEditors.LabelControl();
            this.bnt_Xem = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.bntLapPhieuChi = new DevExpress.XtraEditors.SimpleButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bnt_ThayDoiNgayThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.bntXemChiTiet = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.bnt_XemTruocKhiIn = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbKH = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcChiTiet = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPHIEUNHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayGiaoDich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TienNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoConLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HanThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ChiTietThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHACUNGCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // lbNgay
            // 
            this.lbNgay.AutoSize = true;
            this.lbNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgay.Location = new System.Drawing.Point(66, 48);
            this.lbNgay.Name = "lbNgay";
            this.lbNgay.Size = new System.Drawing.Size(45, 19);
            this.lbNgay.TabIndex = 1;
            this.lbNgay.Text = "Ngày";
            // 
            // lbNo
            // 
            this.lbNo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbNo.Location = new System.Drawing.Point(147, 94);
            this.lbNo.Name = "lbNo";
            this.lbNo.Size = new System.Drawing.Size(45, 19);
            this.lbNo.TabIndex = 7;
            this.lbNo.Text = "Số nợ";
            // 
            // lbSDT
            // 
            this.lbSDT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbSDT.Location = new System.Drawing.Point(147, 68);
            this.lbSDT.Name = "lbSDT";
            this.lbSDT.Size = new System.Drawing.Size(33, 19);
            this.lbSDT.TabIndex = 5;
            this.lbSDT.Text = "SDT";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(30, 95);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(113, 19);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Số Nợ Hiện Tại:";
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbDiaChi.Location = new System.Drawing.Point(147, 42);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Size = new System.Drawing.Size(55, 19);
            this.lbDiaChi.TabIndex = 4;
            this.lbDiaChi.Text = "Địa chỉ";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl3.Controls.Add(this.lbNgay);
            this.panelControl3.Controls.Add(this.lbChiTiet);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 50);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1262, 73);
            this.panelControl3.TabIndex = 101;
            // 
            // lbChiTiet
            // 
            this.lbChiTiet.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lbChiTiet.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbChiTiet.Location = new System.Drawing.Point(66, 24);
            this.lbChiTiet.Name = "lbChiTiet";
            this.lbChiTiet.Size = new System.Drawing.Size(343, 23);
            this.lbChiTiet.TabIndex = 0;
            this.lbChiTiet.Text = "CHI TIẾT CÔNG NỢ NHÀ CUNG CẤP";
            // 
            // bnt_Xem
            // 
            this.bnt_Xem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.bnt_Xem.Appearance.Options.UseFont = true;
            this.bnt_Xem.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_Xem.Location = new System.Drawing.Point(1137, 2);
            this.bnt_Xem.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bnt_Xem.Name = "bnt_Xem";
            this.bnt_Xem.Size = new System.Drawing.Size(123, 41);
            this.bnt_Xem.TabIndex = 3;
            this.bnt_Xem.Text = "&5.Xem Báo Cáo";
            this.bnt_Xem.Click += new System.EventHandler(this.bnt_Xem_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.bntLapPhieuChi);
            this.panelControl2.Controls.Add(this.panel7);
            this.panelControl2.Controls.Add(this.bnt_ThayDoiNgayThanhToan);
            this.panelControl2.Controls.Add(this.panel6);
            this.panelControl2.Controls.Add(this.bntXemChiTiet);
            this.panelControl2.Controls.Add(this.panel3);
            this.panelControl2.Controls.Add(this.bnt_XemTruocKhiIn);
            this.panelControl2.Controls.Add(this.panel2);
            this.panelControl2.Controls.Add(this.bnt_Xem);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 455);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1262, 45);
            this.panelControl2.TabIndex = 99;
            // 
            // bntLapPhieuChi
            // 
            this.bntLapPhieuChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.bntLapPhieuChi.Appearance.Options.UseFont = true;
            this.bntLapPhieuChi.Dock = System.Windows.Forms.DockStyle.Right;
            this.bntLapPhieuChi.Location = new System.Drawing.Point(490, 2);
            this.bntLapPhieuChi.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bntLapPhieuChi.Name = "bntLapPhieuChi";
            this.bntLapPhieuChi.Size = new System.Drawing.Size(135, 41);
            this.bntLapPhieuChi.TabIndex = 4;
            this.bntLapPhieuChi.Text = "&1.Thanh Toán";
            this.bntLapPhieuChi.Click += new System.EventHandler(this.bntLapPhieuChi_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(625, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 41);
            this.panel7.TabIndex = 11;
            // 
            // bnt_ThayDoiNgayThanhToan
            // 
            this.bnt_ThayDoiNgayThanhToan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.bnt_ThayDoiNgayThanhToan.Appearance.Options.UseFont = true;
            this.bnt_ThayDoiNgayThanhToan.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_ThayDoiNgayThanhToan.Location = new System.Drawing.Point(635, 2);
            this.bnt_ThayDoiNgayThanhToan.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bnt_ThayDoiNgayThanhToan.Name = "bnt_ThayDoiNgayThanhToan";
            this.bnt_ThayDoiNgayThanhToan.Size = new System.Drawing.Size(199, 41);
            this.bnt_ThayDoiNgayThanhToan.TabIndex = 6;
            this.bnt_ThayDoiNgayThanhToan.Text = "&2.Thay Đổi Ngày Hẹn Trả";
            this.bnt_ThayDoiNgayThanhToan.Click += new System.EventHandler(this.bnt_ThayDoiNgayThanhToan_Click);
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(834, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 41);
            this.panel6.TabIndex = 9;
            // 
            // bntXemChiTiet
            // 
            this.bntXemChiTiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.bntXemChiTiet.Appearance.Options.UseFont = true;
            this.bntXemChiTiet.Dock = System.Windows.Forms.DockStyle.Right;
            this.bntXemChiTiet.Location = new System.Drawing.Point(844, 2);
            this.bntXemChiTiet.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bntXemChiTiet.Name = "bntXemChiTiet";
            this.bntXemChiTiet.Size = new System.Drawing.Size(123, 41);
            this.bntXemChiTiet.TabIndex = 5;
            this.bntXemChiTiet.Text = "&3.Xem Chi Tiết";
            this.bntXemChiTiet.Click += new System.EventHandler(this.bntXemChiTiet_Click);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(967, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 41);
            this.panel3.TabIndex = 10;
            // 
            // bnt_XemTruocKhiIn
            // 
            this.bnt_XemTruocKhiIn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.bnt_XemTruocKhiIn.Appearance.Options.UseFont = true;
            this.bnt_XemTruocKhiIn.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_XemTruocKhiIn.Location = new System.Drawing.Point(977, 2);
            this.bnt_XemTruocKhiIn.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.bnt_XemTruocKhiIn.Name = "bnt_XemTruocKhiIn";
            this.bnt_XemTruocKhiIn.Size = new System.Drawing.Size(150, 41);
            this.bnt_XemTruocKhiIn.TabIndex = 7;
            this.bnt_XemTruocKhiIn.Text = "&4.Xem Trước Khi In";
            this.bnt_XemTruocKhiIn.Click += new System.EventHandler(this.bnt_XemTruocKhiIn_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1127, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 41);
            this.panel2.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(30, 69);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(106, 19);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Số Điện Thoại:";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl1.Controls.Add(this.lbNo);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.lbSDT);
            this.panelControl1.Controls.Add(this.lbDiaChi);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.lbKH);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 123);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1262, 123);
            this.panelControl1.TabIndex = 98;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(30, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Đia Chỉ:";
            // 
            // lbKH
            // 
            this.lbKH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbKH.Location = new System.Drawing.Point(147, 17);
            this.lbKH.Name = "lbKH";
            this.lbKH.Size = new System.Drawing.Size(96, 19);
            this.lbKH.TabIndex = 1;
            this.lbKH.Text = "Khách Hàng";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(30, 17);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(108, 19);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Nhà Cung Cấp:";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.gcChiTiet);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 246);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1262, 209);
            this.groupControl1.TabIndex = 100;
            this.groupControl1.Text = "Danh Sách Phiếu Nhập Kho";
            // 
            // gcChiTiet
            // 
            this.gcChiTiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcChiTiet.Location = new System.Drawing.Point(2, 24);
            this.gcChiTiet.MainView = this.gridView1;
            this.gcChiTiet.Name = "gcChiTiet";
            this.gcChiTiet.Size = new System.Drawing.Size(1258, 183);
            this.gcChiTiet.TabIndex = 97;
            this.gcChiTiet.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAPHIEUNHAP,
            this.NgayGiaoDich,
            this.TongTien,
            this.TienNo,
            this.ThanhToan,
            this.NoConLai,
            this.HanThanhToan,
            this.ChiTietThanhToan,
            this.NHACUNGCAP});
            this.gridView1.GridControl = this.gcChiTiet;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 45;
            // 
            // MAPHIEUNHAP
            // 
            this.MAPHIEUNHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPHIEUNHAP.AppearanceCell.Options.UseFont = true;
            this.MAPHIEUNHAP.AppearanceCell.Options.UseTextOptions = true;
            this.MAPHIEUNHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPHIEUNHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPHIEUNHAP.AppearanceHeader.Options.UseFont = true;
            this.MAPHIEUNHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPHIEUNHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPHIEUNHAP.Caption = "Mã Phiếu Nhập ";
            this.MAPHIEUNHAP.FieldName = "MAPHIEUNHAP";
            this.MAPHIEUNHAP.Name = "MAPHIEUNHAP";
            this.MAPHIEUNHAP.OptionsColumn.AllowEdit = false;
            this.MAPHIEUNHAP.OptionsColumn.AllowFocus = false;
            this.MAPHIEUNHAP.OptionsColumn.FixedWidth = true;
            this.MAPHIEUNHAP.Visible = true;
            this.MAPHIEUNHAP.VisibleIndex = 1;
            this.MAPHIEUNHAP.Width = 150;
            // 
            // NgayGiaoDich
            // 
            this.NgayGiaoDich.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayGiaoDich.AppearanceCell.Options.UseFont = true;
            this.NgayGiaoDich.AppearanceCell.Options.UseTextOptions = true;
            this.NgayGiaoDich.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayGiaoDich.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NgayGiaoDich.AppearanceHeader.Options.UseFont = true;
            this.NgayGiaoDich.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayGiaoDich.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayGiaoDich.Caption = "Ngày Giao Dịch";
            this.NgayGiaoDich.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayGiaoDich.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayGiaoDich.FieldName = "NGAYTAO";
            this.NgayGiaoDich.Name = "NgayGiaoDich";
            this.NgayGiaoDich.OptionsColumn.AllowEdit = false;
            this.NgayGiaoDich.OptionsColumn.AllowFocus = false;
            this.NgayGiaoDich.OptionsColumn.FixedWidth = true;
            this.NgayGiaoDich.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NGAYTAO", "Tổng Cộng:")});
            this.NgayGiaoDich.Visible = true;
            this.NgayGiaoDich.VisibleIndex = 2;
            this.NgayGiaoDich.Width = 150;
            // 
            // TongTien
            // 
            this.TongTien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTien.AppearanceCell.Options.UseFont = true;
            this.TongTien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TongTien.AppearanceHeader.Options.UseFont = true;
            this.TongTien.AppearanceHeader.Options.UseTextOptions = true;
            this.TongTien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TongTien.Caption = "Tổng Tiền Nhập";
            this.TongTien.DisplayFormat.FormatString = "#,0";
            this.TongTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TongTien.FieldName = "TONGTIEN";
            this.TongTien.Name = "TongTien";
            this.TongTien.OptionsColumn.AllowEdit = false;
            this.TongTien.OptionsColumn.AllowFocus = false;
            this.TongTien.OptionsColumn.FixedWidth = true;
            this.TongTien.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGTIEN", "{0:#,0}")});
            this.TongTien.Visible = true;
            this.TongTien.VisibleIndex = 3;
            this.TongTien.Width = 180;
            // 
            // TienNo
            // 
            this.TienNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TienNo.AppearanceCell.Options.UseFont = true;
            this.TienNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TienNo.AppearanceHeader.Options.UseFont = true;
            this.TienNo.AppearanceHeader.Options.UseTextOptions = true;
            this.TienNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TienNo.Caption = "Tiền Nợ";
            this.TienNo.DisplayFormat.FormatString = "#,0";
            this.TienNo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TienNo.FieldName = "TienNo";
            this.TienNo.Name = "TienNo";
            this.TienNo.OptionsColumn.AllowEdit = false;
            this.TienNo.OptionsColumn.AllowFocus = false;
            this.TienNo.OptionsColumn.FixedWidth = true;
            this.TienNo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TienNo", "{0:#,0}")});
            this.TienNo.Visible = true;
            this.TienNo.VisibleIndex = 4;
            this.TienNo.Width = 180;
            // 
            // ThanhToan
            // 
            this.ThanhToan.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ThanhToan.AppearanceCell.Options.UseFont = true;
            this.ThanhToan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ThanhToan.AppearanceHeader.Options.UseFont = true;
            this.ThanhToan.AppearanceHeader.Options.UseTextOptions = true;
            this.ThanhToan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ThanhToan.Caption = "Đã Thanh Toán";
            this.ThanhToan.DisplayFormat.FormatString = "#,0";
            this.ThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ThanhToan.FieldName = "DaThanhToan";
            this.ThanhToan.Name = "ThanhToan";
            this.ThanhToan.OptionsColumn.AllowEdit = false;
            this.ThanhToan.OptionsColumn.AllowFocus = false;
            this.ThanhToan.OptionsColumn.FixedWidth = true;
            this.ThanhToan.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DaThanhToan", "{0:#,0}")});
            this.ThanhToan.Visible = true;
            this.ThanhToan.VisibleIndex = 5;
            this.ThanhToan.Width = 180;
            // 
            // NoConLai
            // 
            this.NoConLai.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoConLai.AppearanceCell.Options.UseFont = true;
            this.NoConLai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoConLai.AppearanceHeader.Options.UseFont = true;
            this.NoConLai.AppearanceHeader.Options.UseTextOptions = true;
            this.NoConLai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoConLai.Caption = "Nợ Còn Lại";
            this.NoConLai.DisplayFormat.FormatString = "#,0";
            this.NoConLai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NoConLai.FieldName = "NoConLai";
            this.NoConLai.Name = "NoConLai";
            this.NoConLai.OptionsColumn.AllowEdit = false;
            this.NoConLai.OptionsColumn.AllowFocus = false;
            this.NoConLai.OptionsColumn.FixedWidth = true;
            this.NoConLai.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NoConLai", "{0:#,0}")});
            this.NoConLai.Visible = true;
            this.NoConLai.VisibleIndex = 6;
            this.NoConLai.Width = 180;
            // 
            // HanThanhToan
            // 
            this.HanThanhToan.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HanThanhToan.AppearanceCell.Options.UseFont = true;
            this.HanThanhToan.AppearanceCell.Options.UseTextOptions = true;
            this.HanThanhToan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HanThanhToan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HanThanhToan.AppearanceHeader.Options.UseFont = true;
            this.HanThanhToan.AppearanceHeader.Options.UseTextOptions = true;
            this.HanThanhToan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HanThanhToan.Caption = "Hạn Thanh Toán";
            this.HanThanhToan.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.HanThanhToan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.HanThanhToan.FieldName = "HanThanhToan";
            this.HanThanhToan.Name = "HanThanhToan";
            this.HanThanhToan.OptionsColumn.AllowEdit = false;
            this.HanThanhToan.OptionsColumn.AllowFocus = false;
            this.HanThanhToan.OptionsColumn.FixedWidth = true;
            this.HanThanhToan.Visible = true;
            this.HanThanhToan.VisibleIndex = 7;
            this.HanThanhToan.Width = 150;
            // 
            // ChiTietThanhToan
            // 
            this.ChiTietThanhToan.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChiTietThanhToan.AppearanceCell.Options.UseFont = true;
            this.ChiTietThanhToan.AppearanceCell.Options.UseTextOptions = true;
            this.ChiTietThanhToan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChiTietThanhToan.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ChiTietThanhToan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChiTietThanhToan.AppearanceHeader.Options.UseFont = true;
            this.ChiTietThanhToan.AppearanceHeader.Options.UseTextOptions = true;
            this.ChiTietThanhToan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChiTietThanhToan.Caption = "Chi Tiết Thanh Toán";
            this.ChiTietThanhToan.FieldName = "ChiTietThanhToan";
            this.ChiTietThanhToan.MinWidth = 300;
            this.ChiTietThanhToan.Name = "ChiTietThanhToan";
            this.ChiTietThanhToan.OptionsColumn.AllowEdit = false;
            this.ChiTietThanhToan.OptionsColumn.AllowFocus = false;
            this.ChiTietThanhToan.OptionsColumn.FixedWidth = true;
            this.ChiTietThanhToan.Visible = true;
            this.ChiTietThanhToan.VisibleIndex = 8;
            this.ChiTietThanhToan.Width = 500;
            // 
            // NHACUNGCAP
            // 
            this.NHACUNGCAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NHACUNGCAP.AppearanceCell.Options.UseFont = true;
            this.NHACUNGCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NHACUNGCAP.AppearanceHeader.Options.UseFont = true;
            this.NHACUNGCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.NHACUNGCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHACUNGCAP.Caption = "gridColumn1";
            this.NHACUNGCAP.FieldName = "NHACUNGCAP";
            this.NHACUNGCAP.Name = "NHACUNGCAP";
            this.NHACUNGCAP.OptionsColumn.AllowEdit = false;
            this.NHACUNGCAP.OptionsColumn.AllowFocus = false;
            this.NHACUNGCAP.OptionsColumn.FixedWidth = true;
            // 
            // panel4
            // 
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.header);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1262, 50);
            this.panel4.TabIndex = 102;
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl4);
            this.header.Controls.Add(this.panelControl5);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1262, 52);
            this.header.TabIndex = 2;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl4.Controls.Add(this.pictureEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(107, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1155, 52);
            this.panelControl4.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(1151, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSize = true;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.title);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl5.Location = new System.Drawing.Point(81, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(26, 52);
            this.panelControl5.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(14, 32);
            this.title.TabIndex = 1;
            this.title.Text = "[]";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // PU_CONGNO_NCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 500);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panel4);
            this.Name = "PU_CONGNO_NCC";
            this.Text = "CHI TIẾT CÔNG NỢ NHÀ CUNG CẤP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.SizeChanged += new System.EventHandler(this.PU_CONGNO_KHACHHANG_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbNgay;
        private DevExpress.XtraEditors.LabelControl lbNo;
        private DevExpress.XtraEditors.LabelControl lbSDT;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbDiaChi;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl lbChiTiet;
        private DevExpress.XtraEditors.SimpleButton bnt_Xem;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lbKH;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton bntLapPhieuChi;
        private DevExpress.XtraEditors.SimpleButton bntXemChiTiet;
        private DevExpress.XtraEditors.SimpleButton bnt_ThayDoiNgayThanhToan;
        private DevExpress.XtraEditors.SimpleButton bnt_XemTruocKhiIn;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gcChiTiet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MAPHIEUNHAP;
        private DevExpress.XtraGrid.Columns.GridColumn NgayGiaoDich;
        private DevExpress.XtraGrid.Columns.GridColumn TongTien;
        private DevExpress.XtraGrid.Columns.GridColumn TienNo;
        private DevExpress.XtraGrid.Columns.GridColumn ThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn NoConLai;
        private DevExpress.XtraGrid.Columns.GridColumn HanThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn ChiTietThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn NHACUNGCAP;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
    }
}