﻿namespace KP_RES 
{
    partial class Frm_Cauhinh_Phieuchi 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.opt1 = new DevExpress.XtraEditors.CheckEdit();
            this.opt = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.opt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // opt1
            // 
            this.opt1.EnterMoveNextControl = true;
            this.opt1.Location = new System.Drawing.Point(244, 73);
            this.opt1.Name = "opt1";
            this.opt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opt1.Properties.Appearance.Options.UseFont = true;
            this.opt1.Properties.Caption = "Phiếu chi phải được duyệt";
            this.opt1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.opt1.Properties.RadioGroupIndex = 1;
            this.opt1.Size = new System.Drawing.Size(222, 24);
            this.opt1.TabIndex = 6;
            this.opt1.TabStop = false;
            // 
            // opt
            // 
            this.opt.EditValue = true;
            this.opt.EnterMoveNextControl = true;
            this.opt.Location = new System.Drawing.Point(45, 73);
            this.opt.Name = "opt";
            this.opt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opt.Properties.Appearance.Options.UseFont = true;
            this.opt.Properties.Caption = "Mặc định";
            this.opt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.opt.Properties.RadioGroupIndex = 1;
            this.opt.Size = new System.Drawing.Size(193, 24);
            this.opt.TabIndex = 4;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(132, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Cấu hình phiếu chi";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(356, 124);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(238, 124);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_Cauhinh_Phieuchi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 175);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.opt1);
            this.Controls.Add(this.opt);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinh_Phieuchi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình phiếu chi";
            ((System.ComponentModel.ISupportInitialize)(this.opt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit opt;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit opt1;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;




    }
}