﻿namespace KP_RES
{
    partial class Frm_CongNoHanMuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_luu = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bnt_dong = new DevExpress.XtraEditors.SimpleButton();
            this.txtTienHanMuc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaKH = new DevExpress.XtraEditors.TextEdit();
            this.txtTenKhachHang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtHanThanhToan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtDatCoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.chkTienDatCoc = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienHanMuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHanThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatCoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienDatCoc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkTienDatCoc);
            this.panelControl1.Controls.Add(this.txtDatCoc);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtHanThanhToan);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.bnt_luu);
            this.panelControl1.Controls.Add(this.panel2);
            this.panelControl1.Controls.Add(this.bnt_dong);
            this.panelControl1.Controls.Add(this.txtTienHanMuc);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtMaKH);
            this.panelControl1.Controls.Add(this.txtTenKhachHang);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(614, 253);
            this.panelControl1.TabIndex = 0;
            // 
            // bnt_luu
            // 
            this.bnt_luu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luu.Appearance.Options.UseFont = true;
            this.bnt_luu.Image = global::KP_RES.Properties.Resources.save_26;
            this.bnt_luu.Location = new System.Drawing.Point(419, 210);
            this.bnt_luu.Name = "bnt_luu";
            this.bnt_luu.Size = new System.Drawing.Size(80, 35);
            this.bnt_luu.TabIndex = 12;
            this.bnt_luu.Text = "&1.Lưu";
            this.bnt_luu.Click += new System.EventHandler(this.bnt_luu_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(610, 35);
            this.panel2.TabIndex = 0;
            // 
            // bnt_dong
            // 
            this.bnt_dong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_dong.Appearance.Options.UseFont = true;
            this.bnt_dong.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bnt_dong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.bnt_dong.Location = new System.Drawing.Point(509, 210);
            this.bnt_dong.Name = "bnt_dong";
            this.bnt_dong.Size = new System.Drawing.Size(89, 35);
            this.bnt_dong.TabIndex = 14;
            this.bnt_dong.Text = "&2.Đóng";
            this.bnt_dong.Click += new System.EventHandler(this.bnt_dong_Click);
            // 
            // txtTienHanMuc
            // 
            this.txtTienHanMuc.EnterMoveNextControl = true;
            this.txtTienHanMuc.Location = new System.Drawing.Point(157, 105);
            this.txtTienHanMuc.Name = "txtTienHanMuc";
            this.txtTienHanMuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTienHanMuc.Properties.Appearance.Options.UseFont = true;
            this.txtTienHanMuc.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienHanMuc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienHanMuc.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtTienHanMuc.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTienHanMuc.Properties.Mask.EditMask = "N0";
            this.txtTienHanMuc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienHanMuc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTienHanMuc.Size = new System.Drawing.Size(441, 26);
            this.txtTienHanMuc.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(12, 109);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Hạn mức";
            // 
            // txtMaKH
            // 
            this.txtMaKH.Enabled = false;
            this.txtMaKH.EnterMoveNextControl = true;
            this.txtMaKH.Location = new System.Drawing.Point(124, 43);
            this.txtMaKH.Name = "txtMaKH";
            this.txtMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtMaKH.Properties.Appearance.Options.UseFont = true;
            this.txtMaKH.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtMaKH.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtMaKH.Properties.Mask.EditMask = "N0";
            this.txtMaKH.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMaKH.Size = new System.Drawing.Size(10, 26);
            this.txtMaKH.TabIndex = 2;
            this.txtMaKH.Visible = false;
            // 
            // txtTenKhachHang
            // 
            this.txtTenKhachHang.Enabled = false;
            this.txtTenKhachHang.EnterMoveNextControl = true;
            this.txtTenKhachHang.Location = new System.Drawing.Point(157, 43);
            this.txtTenKhachHang.Name = "txtTenKhachHang";
            this.txtTenKhachHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTenKhachHang.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhachHang.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtTenKhachHang.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTenKhachHang.Size = new System.Drawing.Size(441, 26);
            this.txtTenKhachHang.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(12, 46);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(82, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Khách hàng";
            // 
            // txtHanThanhToan
            // 
            this.txtHanThanhToan.EnterMoveNextControl = true;
            this.txtHanThanhToan.Location = new System.Drawing.Point(157, 74);
            this.txtHanThanhToan.Name = "txtHanThanhToan";
            this.txtHanThanhToan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtHanThanhToan.Properties.Appearance.Options.UseFont = true;
            this.txtHanThanhToan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtHanThanhToan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtHanThanhToan.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtHanThanhToan.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtHanThanhToan.Properties.Mask.EditMask = "N0";
            this.txtHanThanhToan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtHanThanhToan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtHanThanhToan.Size = new System.Drawing.Size(441, 26);
            this.txtHanThanhToan.TabIndex = 16;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(12, 76);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(109, 19);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Hạn thanh toán";
            // 
            // txtDatCoc
            // 
            this.txtDatCoc.EnterMoveNextControl = true;
            this.txtDatCoc.Location = new System.Drawing.Point(157, 166);
            this.txtDatCoc.Name = "txtDatCoc";
            this.txtDatCoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtDatCoc.Properties.Appearance.Options.UseFont = true;
            this.txtDatCoc.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDatCoc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDatCoc.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtDatCoc.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtDatCoc.Properties.Mask.EditMask = "N0";
            this.txtDatCoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDatCoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDatCoc.Size = new System.Drawing.Size(441, 26);
            this.txtDatCoc.TabIndex = 18;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Location = new System.Drawing.Point(12, 169);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(52, 19);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "Đặt cọc";
            // 
            // chkTienDatCoc
            // 
            this.chkTienDatCoc.EnterMoveNextControl = true;
            this.chkTienDatCoc.Location = new System.Drawing.Point(155, 137);
            this.chkTienDatCoc.Name = "chkTienDatCoc";
            this.chkTienDatCoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTienDatCoc.Properties.Appearance.Options.UseFont = true;
            this.chkTienDatCoc.Properties.Caption = "Tiền đặt cọc";
            this.chkTienDatCoc.Size = new System.Drawing.Size(130, 24);
            this.chkTienDatCoc.TabIndex = 25;
            this.chkTienDatCoc.CheckedChanged += new System.EventHandler(this.chkTienDatCoc_CheckedChanged);
            // 
            // Frm_CongNoHanMuc
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 253);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_CongNoHanMuc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hạn Mức Công Nợ";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienHanMuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHanThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatCoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienDatCoc.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton bnt_luu;
        private DevExpress.XtraEditors.SimpleButton bnt_dong;
        private DevExpress.XtraEditors.TextEdit txtTienHanMuc;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtMaKH;
        private DevExpress.XtraEditors.TextEdit txtTenKhachHang;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.TextEdit txtHanThanhToan;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtDatCoc;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit chkTienDatCoc;
    }
}