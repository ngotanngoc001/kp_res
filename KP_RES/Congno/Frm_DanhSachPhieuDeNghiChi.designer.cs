﻿namespace KP_RES
{
    partial class Frm_DanhSachPhieuDeNghiChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_DanhSachPhieuDeNghiChi));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            this.pnlCHITIET = new DevExpress.XtraEditors.PanelControl();
            this.gvcShowCTCongNoNCC = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYLAPPHIEU2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPDNDC2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIANHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOTEN2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LYDO2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.XemPhieu2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MALOAI2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlTONGQUAN = new DevExpress.XtraEditors.PanelControl();
            this.gvcShowCongNoNCC = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPDNDC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LYDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.XemPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DT_DOITUONGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoDauKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoCuoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYLAPPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MALOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHIEUKHO_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnCHITIET = new DevExpress.XtraEditors.SimpleButton();
            this.btnTONGQUAN = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCHITIET)).BeginInit();
            this.pnlCHITIET.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowCTCongNoNCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTONGQUAN)).BeginInit();
            this.pnlTONGQUAN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowCongNoNCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1321, 52);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 541);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 297);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 137);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 217);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 134);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 459);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 82);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatfile.Image")));
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 2);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl1);
            this.header.Controls.Add(this.panelControl5);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1360, 52);
            this.header.TabIndex = 9;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(107, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1253, 52);
            this.panelControl1.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(1249, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSize = true;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.title);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl5.Location = new System.Drawing.Point(81, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(26, 52);
            this.panelControl5.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(14, 32);
            this.title.TabIndex = 1;
            this.title.Text = "[]";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            this.pnlHome.Click += new System.EventHandler(this.home_Click);
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = ((System.Drawing.Image)(resources.GetObject("home.Image")));
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // pnlCHITIET
            // 
            this.pnlCHITIET.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnlCHITIET.Appearance.Options.UseBackColor = true;
            this.pnlCHITIET.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlCHITIET.Controls.Add(this.gvcShowCTCongNoNCC);
            this.pnlCHITIET.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCHITIET.Location = new System.Drawing.Point(0, 82);
            this.pnlCHITIET.Name = "pnlCHITIET";
            this.pnlCHITIET.Size = new System.Drawing.Size(1321, 511);
            this.pnlCHITIET.TabIndex = 65;
            // 
            // gvcShowCTCongNoNCC
            // 
            this.gvcShowCTCongNoNCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvcShowCTCongNoNCC.Location = new System.Drawing.Point(0, 0);
            this.gvcShowCTCongNoNCC.MainView = this.gridView2;
            this.gvcShowCTCongNoNCC.Name = "gvcShowCTCongNoNCC";
            this.gvcShowCTCongNoNCC.Size = new System.Drawing.Size(1321, 511);
            this.gvcShowCTCongNoNCC.TabIndex = 2;
            this.gvcShowCTCongNoNCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT2,
            this.NGAYLAPPHIEU2,
            this.MAPDNDC2,
            this.MA_VACH,
            this.TEN,
            this.DVT,
            this.SL,
            this.GIANHAP,
            this.THANHTIEN,
            this.HOTEN2,
            this.LYDO2,
            this.XemPhieu2,
            this.MA2,
            this.MALOAI2,
            this.Fill});
            this.gridView2.GridControl = this.gvcShowCTCongNoNCC;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 40;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            // 
            // STT2
            // 
            this.STT2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT2.AppearanceCell.Options.UseFont = true;
            this.STT2.AppearanceCell.Options.UseTextOptions = true;
            this.STT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT2.AppearanceHeader.Options.UseFont = true;
            this.STT2.AppearanceHeader.Options.UseTextOptions = true;
            this.STT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT2.Caption = "STT";
            this.STT2.Name = "STT2";
            this.STT2.OptionsColumn.AllowEdit = false;
            this.STT2.OptionsColumn.AllowFocus = false;
            this.STT2.OptionsColumn.FixedWidth = true;
            this.STT2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAPDNDC", "")});
            this.STT2.Visible = true;
            this.STT2.VisibleIndex = 0;
            this.STT2.Width = 50;
            // 
            // NGAYLAPPHIEU2
            // 
            this.NGAYLAPPHIEU2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYLAPPHIEU2.AppearanceCell.Options.UseFont = true;
            this.NGAYLAPPHIEU2.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYLAPPHIEU2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYLAPPHIEU2.AppearanceHeader.Options.UseFont = true;
            this.NGAYLAPPHIEU2.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYLAPPHIEU2.Caption = "Ngày lập";
            this.NGAYLAPPHIEU2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYLAPPHIEU2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYLAPPHIEU2.FieldName = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU2.Name = "NGAYLAPPHIEU2";
            this.NGAYLAPPHIEU2.OptionsColumn.AllowEdit = false;
            this.NGAYLAPPHIEU2.OptionsColumn.AllowFocus = false;
            this.NGAYLAPPHIEU2.OptionsColumn.FixedWidth = true;
            this.NGAYLAPPHIEU2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.NGAYLAPPHIEU2.Visible = true;
            this.NGAYLAPPHIEU2.VisibleIndex = 1;
            this.NGAYLAPPHIEU2.Width = 110;
            // 
            // MAPDNDC2
            // 
            this.MAPDNDC2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MAPDNDC2.AppearanceCell.Options.UseFont = true;
            this.MAPDNDC2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAPDNDC2.AppearanceHeader.Options.UseFont = true;
            this.MAPDNDC2.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPDNDC2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPDNDC2.Caption = "Mã phiếu";
            this.MAPDNDC2.FieldName = "MAPDNDC";
            this.MAPDNDC2.Name = "MAPDNDC2";
            this.MAPDNDC2.OptionsColumn.AllowEdit = false;
            this.MAPDNDC2.OptionsColumn.AllowFocus = false;
            this.MAPDNDC2.OptionsColumn.FixedWidth = true;
            this.MAPDNDC2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAPDNDC2.Visible = true;
            this.MAPDNDC2.VisibleIndex = 2;
            this.MAPDNDC2.Width = 150;
            // 
            // MA_VACH
            // 
            this.MA_VACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_VACH.AppearanceCell.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_VACH.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_VACH.Caption = "Mã";
            this.MA_VACH.FieldName = "MA_VACH";
            this.MA_VACH.Name = "MA_VACH";
            this.MA_VACH.OptionsColumn.AllowEdit = false;
            this.MA_VACH.OptionsColumn.AllowFocus = false;
            this.MA_VACH.OptionsColumn.FixedWidth = true;
            this.MA_VACH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MA_VACH.Visible = true;
            this.MA_VACH.VisibleIndex = 3;
            this.MA_VACH.Width = 150;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN.Caption = "Hàng hóa";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 4;
            this.TEN.Width = 200;
            // 
            // DVT
            // 
            this.DVT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DVT.AppearanceCell.Options.UseFont = true;
            this.DVT.AppearanceCell.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVT.AppearanceHeader.Options.UseFont = true;
            this.DVT.AppearanceHeader.Options.UseTextOptions = true;
            this.DVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DVT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DVT.Caption = "ĐVT";
            this.DVT.FieldName = "DVT";
            this.DVT.Name = "DVT";
            this.DVT.OptionsColumn.AllowEdit = false;
            this.DVT.OptionsColumn.AllowFocus = false;
            this.DVT.OptionsColumn.FixedWidth = true;
            this.DVT.Visible = true;
            this.DVT.VisibleIndex = 5;
            this.DVT.Width = 80;
            // 
            // SL
            // 
            this.SL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SL.AppearanceCell.Options.UseFont = true;
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SL.Caption = "SL";
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowEdit = false;
            this.SL.OptionsColumn.AllowFocus = false;
            this.SL.OptionsColumn.FixedWidth = true;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 6;
            this.SL.Width = 50;
            // 
            // GIANHAP
            // 
            this.GIANHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIANHAP.AppearanceCell.Options.UseFont = true;
            this.GIANHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIANHAP.AppearanceHeader.Options.UseFont = true;
            this.GIANHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIANHAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIANHAP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIANHAP.Caption = "Đơn giá";
            this.GIANHAP.DisplayFormat.FormatString = "#,0";
            this.GIANHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIANHAP.FieldName = "GIANHAP";
            this.GIANHAP.Name = "GIANHAP";
            this.GIANHAP.OptionsColumn.AllowEdit = false;
            this.GIANHAP.OptionsColumn.AllowFocus = false;
            this.GIANHAP.OptionsColumn.FixedWidth = true;
            this.GIANHAP.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GIANHAP.Visible = true;
            this.GIANHAP.VisibleIndex = 7;
            this.GIANHAP.Width = 100;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTIEN.Caption = "Thành tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "#,0";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 8;
            this.THANHTIEN.Width = 150;
            // 
            // HOTEN2
            // 
            this.HOTEN2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN2.AppearanceCell.Options.UseFont = true;
            this.HOTEN2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN2.AppearanceHeader.Options.UseFont = true;
            this.HOTEN2.AppearanceHeader.Options.UseTextOptions = true;
            this.HOTEN2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOTEN2.Caption = "Chi cho";
            this.HOTEN2.FieldName = "HOTEN";
            this.HOTEN2.Name = "HOTEN2";
            this.HOTEN2.OptionsColumn.AllowEdit = false;
            this.HOTEN2.OptionsColumn.AllowFocus = false;
            this.HOTEN2.OptionsColumn.FixedWidth = true;
            this.HOTEN2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.HOTEN2.Visible = true;
            this.HOTEN2.VisibleIndex = 9;
            this.HOTEN2.Width = 200;
            // 
            // LYDO2
            // 
            this.LYDO2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LYDO2.AppearanceCell.Options.UseFont = true;
            this.LYDO2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LYDO2.AppearanceHeader.Options.UseFont = true;
            this.LYDO2.AppearanceHeader.Options.UseTextOptions = true;
            this.LYDO2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LYDO2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LYDO2.Caption = "Lý do";
            this.LYDO2.FieldName = "LYDO";
            this.LYDO2.Name = "LYDO2";
            this.LYDO2.OptionsColumn.AllowEdit = false;
            this.LYDO2.OptionsColumn.AllowFocus = false;
            this.LYDO2.OptionsColumn.FixedWidth = true;
            this.LYDO2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.LYDO2.Visible = true;
            this.LYDO2.VisibleIndex = 10;
            this.LYDO2.Width = 280;
            // 
            // XemPhieu2
            // 
            this.XemPhieu2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.XemPhieu2.AppearanceCell.ForeColor = System.Drawing.Color.Blue;
            this.XemPhieu2.AppearanceCell.Options.UseFont = true;
            this.XemPhieu2.AppearanceCell.Options.UseForeColor = true;
            this.XemPhieu2.AppearanceCell.Options.UseTextOptions = true;
            this.XemPhieu2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XemPhieu2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.XemPhieu2.AppearanceHeader.Options.UseFont = true;
            this.XemPhieu2.AppearanceHeader.Options.UseTextOptions = true;
            this.XemPhieu2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XemPhieu2.Caption = "Duyệt phiếu";
            this.XemPhieu2.ColumnEdit = this.repositoryItemButtonEdit1;
            this.XemPhieu2.FieldName = "XemPhieu2";
            this.XemPhieu2.MinWidth = 148;
            this.XemPhieu2.Name = "XemPhieu2";
            this.XemPhieu2.OptionsColumn.AllowEdit = false;
            this.XemPhieu2.OptionsColumn.AllowFocus = false;
            this.XemPhieu2.Visible = true;
            this.XemPhieu2.VisibleIndex = 11;
            this.XemPhieu2.Width = 150;
            // 
            // MA2
            // 
            this.MA2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA2.AppearanceCell.Options.UseFont = true;
            this.MA2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA2.AppearanceHeader.Options.UseFont = true;
            this.MA2.Caption = "gridColumn1";
            this.MA2.FieldName = "MA";
            this.MA2.Name = "MA2";
            this.MA2.OptionsColumn.AllowEdit = false;
            this.MA2.OptionsColumn.AllowFocus = false;
            this.MA2.OptionsColumn.FixedWidth = true;
            // 
            // MALOAI2
            // 
            this.MALOAI2.Caption = "MALOAI";
            this.MALOAI2.FieldName = "MALOAI";
            this.MALOAI2.Name = "MALOAI2";
            // 
            // Fill
            // 
            this.Fill.Name = "Fill";
            this.Fill.Width = 20;
            // 
            // pnlTONGQUAN
            // 
            this.pnlTONGQUAN.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnlTONGQUAN.Appearance.Options.UseBackColor = true;
            this.pnlTONGQUAN.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTONGQUAN.Controls.Add(this.gvcShowCongNoNCC);
            this.pnlTONGQUAN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTONGQUAN.Location = new System.Drawing.Point(0, 82);
            this.pnlTONGQUAN.Name = "pnlTONGQUAN";
            this.pnlTONGQUAN.Size = new System.Drawing.Size(1321, 511);
            this.pnlTONGQUAN.TabIndex = 64;
            // 
            // gvcShowCongNoNCC
            // 
            this.gvcShowCongNoNCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvcShowCongNoNCC.Location = new System.Drawing.Point(0, 0);
            this.gvcShowCongNoNCC.MainView = this.gridView1;
            this.gvcShowCongNoNCC.Name = "gvcShowCongNoNCC";
            this.gvcShowCongNoNCC.Size = new System.Drawing.Size(1321, 511);
            this.gvcShowCongNoNCC.TabIndex = 1;
            this.gvcShowCongNoNCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAPDNDC,
            this.TenLoai,
            this.HOTEN,
            this.LYDO,
            this.SOTIEN,
            this.TENNHANVIEN,
            this.XemPhieu,
            this.DT_DOITUONGID,
            this.NoDauKy,
            this.NoCuoiky,
            this.NGAYLAPPHIEU,
            this.MALOAI,
            this.PHIEUKHO_ID});
            this.gridView1.GridControl = this.gvcShowCongNoNCC;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 40;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell_1);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAPDNDC", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MAPDNDC
            // 
            this.MAPDNDC.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MAPDNDC.AppearanceCell.Options.UseFont = true;
            this.MAPDNDC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAPDNDC.AppearanceHeader.Options.UseFont = true;
            this.MAPDNDC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPDNDC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPDNDC.Caption = "Mã phiếu";
            this.MAPDNDC.FieldName = "MAPDNDC";
            this.MAPDNDC.Name = "MAPDNDC";
            this.MAPDNDC.OptionsColumn.AllowEdit = false;
            this.MAPDNDC.OptionsColumn.AllowFocus = false;
            this.MAPDNDC.OptionsColumn.FixedWidth = true;
            this.MAPDNDC.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAPDNDC.Visible = true;
            this.MAPDNDC.VisibleIndex = 2;
            this.MAPDNDC.Width = 150;
            // 
            // TenLoai
            // 
            this.TenLoai.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TenLoai.AppearanceCell.Options.UseFont = true;
            this.TenLoai.AppearanceCell.Options.UseTextOptions = true;
            this.TenLoai.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TenLoai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TenLoai.AppearanceHeader.Options.UseFont = true;
            this.TenLoai.AppearanceHeader.Options.UseTextOptions = true;
            this.TenLoai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenLoai.Caption = "Loại";
            this.TenLoai.FieldName = "TenLoai";
            this.TenLoai.Name = "TenLoai";
            this.TenLoai.OptionsColumn.AllowEdit = false;
            this.TenLoai.OptionsColumn.AllowFocus = false;
            this.TenLoai.OptionsColumn.FixedWidth = true;
            this.TenLoai.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TenLoai.Visible = true;
            this.TenLoai.VisibleIndex = 3;
            this.TenLoai.Width = 120;
            // 
            // HOTEN
            // 
            this.HOTEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN.AppearanceCell.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN.AppearanceHeader.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOTEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOTEN.Caption = "Chi cho";
            this.HOTEN.FieldName = "HOTEN";
            this.HOTEN.Name = "HOTEN";
            this.HOTEN.OptionsColumn.AllowEdit = false;
            this.HOTEN.OptionsColumn.AllowFocus = false;
            this.HOTEN.OptionsColumn.FixedWidth = true;
            this.HOTEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.HOTEN.Visible = true;
            this.HOTEN.VisibleIndex = 4;
            this.HOTEN.Width = 200;
            // 
            // LYDO
            // 
            this.LYDO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LYDO.AppearanceCell.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LYDO.AppearanceHeader.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Options.UseTextOptions = true;
            this.LYDO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LYDO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LYDO.Caption = "Lý do";
            this.LYDO.FieldName = "LYDO";
            this.LYDO.Name = "LYDO";
            this.LYDO.OptionsColumn.AllowEdit = false;
            this.LYDO.OptionsColumn.AllowFocus = false;
            this.LYDO.OptionsColumn.FixedWidth = true;
            this.LYDO.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.LYDO.Visible = true;
            this.LYDO.VisibleIndex = 5;
            this.LYDO.Width = 280;
            // 
            // SOTIEN
            // 
            this.SOTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOTIEN.AppearanceCell.Options.UseFont = true;
            this.SOTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOTIEN.AppearanceHeader.Options.UseFont = true;
            this.SOTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTIEN.Caption = "Số tiền";
            this.SOTIEN.DisplayFormat.FormatString = "#,0";
            this.SOTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SOTIEN.FieldName = "SOTIEN";
            this.SOTIEN.Name = "SOTIEN";
            this.SOTIEN.OptionsColumn.AllowEdit = false;
            this.SOTIEN.OptionsColumn.AllowFocus = false;
            this.SOTIEN.OptionsColumn.FixedWidth = true;
            this.SOTIEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.SOTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOTIEN", "{0:#,0}")});
            this.SOTIEN.Visible = true;
            this.SOTIEN.VisibleIndex = 6;
            this.SOTIEN.Width = 130;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Người lập";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 7;
            this.TENNHANVIEN.Width = 131;
            // 
            // XemPhieu
            // 
            this.XemPhieu.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.XemPhieu.AppearanceCell.ForeColor = System.Drawing.Color.Blue;
            this.XemPhieu.AppearanceCell.Options.UseFont = true;
            this.XemPhieu.AppearanceCell.Options.UseForeColor = true;
            this.XemPhieu.AppearanceCell.Options.UseTextOptions = true;
            this.XemPhieu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XemPhieu.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.XemPhieu.AppearanceHeader.Options.UseFont = true;
            this.XemPhieu.AppearanceHeader.Options.UseTextOptions = true;
            this.XemPhieu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XemPhieu.Caption = "Duyệt phiếu";
            this.XemPhieu.ColumnEdit = this.repositoryItemButtonEdit1;
            this.XemPhieu.FieldName = "XemPhieu";
            this.XemPhieu.MinWidth = 80;
            this.XemPhieu.Name = "XemPhieu";
            this.XemPhieu.OptionsColumn.AllowEdit = false;
            this.XemPhieu.OptionsColumn.AllowFocus = false;
            this.XemPhieu.Visible = true;
            this.XemPhieu.VisibleIndex = 8;
            this.XemPhieu.Width = 148;
            // 
            // DT_DOITUONGID
            // 
            this.DT_DOITUONGID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DT_DOITUONGID.AppearanceCell.Options.UseFont = true;
            this.DT_DOITUONGID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DT_DOITUONGID.AppearanceHeader.Options.UseFont = true;
            this.DT_DOITUONGID.Caption = "gridColumn1";
            this.DT_DOITUONGID.FieldName = "MA";
            this.DT_DOITUONGID.Name = "DT_DOITUONGID";
            this.DT_DOITUONGID.OptionsColumn.AllowEdit = false;
            this.DT_DOITUONGID.OptionsColumn.AllowFocus = false;
            this.DT_DOITUONGID.OptionsColumn.FixedWidth = true;
            // 
            // NoDauKy
            // 
            this.NoDauKy.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NoDauKy.AppearanceCell.Options.UseFont = true;
            this.NoDauKy.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NoDauKy.AppearanceHeader.Options.UseFont = true;
            this.NoDauKy.AppearanceHeader.Options.UseTextOptions = true;
            this.NoDauKy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoDauKy.Caption = "Nợ Đầu Kỳ";
            this.NoDauKy.FieldName = "NoDauKy";
            this.NoDauKy.Name = "NoDauKy";
            this.NoDauKy.OptionsColumn.AllowEdit = false;
            this.NoDauKy.OptionsColumn.AllowFocus = false;
            this.NoDauKy.OptionsColumn.FixedWidth = true;
            this.NoDauKy.Width = 200;
            // 
            // NoCuoiky
            // 
            this.NoCuoiky.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NoCuoiky.AppearanceCell.Options.UseFont = true;
            this.NoCuoiky.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NoCuoiky.AppearanceHeader.Options.UseFont = true;
            this.NoCuoiky.AppearanceHeader.Options.UseTextOptions = true;
            this.NoCuoiky.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoCuoiky.Caption = "Nợ Cuối Kỳ";
            this.NoCuoiky.FieldName = "NoCuoiky";
            this.NoCuoiky.Name = "NoCuoiky";
            this.NoCuoiky.OptionsColumn.AllowEdit = false;
            this.NoCuoiky.OptionsColumn.AllowFocus = false;
            this.NoCuoiky.OptionsColumn.FixedWidth = true;
            this.NoCuoiky.Width = 200;
            // 
            // NGAYLAPPHIEU
            // 
            this.NGAYLAPPHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYLAPPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYLAPPHIEU.Caption = "Ngày lập";
            this.NGAYLAPPHIEU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYLAPPHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYLAPPHIEU.FieldName = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.Name = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.OptionsColumn.AllowEdit = false;
            this.NGAYLAPPHIEU.OptionsColumn.AllowFocus = false;
            this.NGAYLAPPHIEU.OptionsColumn.FixedWidth = true;
            this.NGAYLAPPHIEU.Visible = true;
            this.NGAYLAPPHIEU.VisibleIndex = 1;
            this.NGAYLAPPHIEU.Width = 110;
            // 
            // MALOAI
            // 
            this.MALOAI.Caption = "MALOAI";
            this.MALOAI.FieldName = "MALOAI";
            this.MALOAI.Name = "MALOAI";
            // 
            // PHIEUKHO_ID
            // 
            this.PHIEUKHO_ID.Caption = "PHIEUKHO_ID";
            this.PHIEUKHO_ID.FieldName = "PHIEUKHO_ID";
            this.PHIEUKHO_ID.Name = "PHIEUKHO_ID";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.Controls.Add(this.btnCHITIET);
            this.panelControl7.Controls.Add(this.btnTONGQUAN);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 52);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(1321, 30);
            this.panelControl7.TabIndex = 10;
            // 
            // btnCHITIET
            // 
            this.btnCHITIET.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCHITIET.Appearance.Options.UseFont = true;
            this.btnCHITIET.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCHITIET.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCHITIET.Location = new System.Drawing.Point(398, 2);
            this.btnCHITIET.Margin = new System.Windows.Forms.Padding(4);
            this.btnCHITIET.Name = "btnCHITIET";
            this.btnCHITIET.Size = new System.Drawing.Size(432, 26);
            this.btnCHITIET.TabIndex = 25;
            this.btnCHITIET.Text = "CHI TIẾT";
            this.btnCHITIET.Click += new System.EventHandler(this.btnCHITIET_Click);
            // 
            // btnTONGQUAN
            // 
            this.btnTONGQUAN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTONGQUAN.Appearance.Options.UseFont = true;
            this.btnTONGQUAN.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTONGQUAN.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTONGQUAN.Location = new System.Drawing.Point(2, 2);
            this.btnTONGQUAN.Margin = new System.Windows.Forms.Padding(4);
            this.btnTONGQUAN.Name = "btnTONGQUAN";
            this.btnTONGQUAN.Size = new System.Drawing.Size(396, 26);
            this.btnTONGQUAN.TabIndex = 21;
            this.btnTONGQUAN.Text = "TỔNG QUAN";
            this.btnTONGQUAN.Click += new System.EventHandler(this.btnTONGQUAN_Click);
            // 
            // Frm_DanhSachPhieuDeNghiChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1360, 593);
            this.Controls.Add(this.pnlCHITIET);
            this.Controls.Add(this.pnlTONGQUAN);
            this.Controls.Add(this.panelControl7);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_DanhSachPhieuDeNghiChi";
            this.Text = "Danh sách phiếu đề nghị chi";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCHITIET)).EndInit();
            this.pnlCHITIET.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowCTCongNoNCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTONGQUAN)).EndInit();
            this.pnlTONGQUAN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowCongNoNCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.SimpleButton btnCHITIET;
        private DevExpress.XtraEditors.SimpleButton btnTONGQUAN;
        private DevExpress.XtraEditors.PanelControl pnlTONGQUAN;
        private DevExpress.XtraGrid.GridControl gvcShowCongNoNCC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MAPDNDC;
        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn HOTEN;
        private DevExpress.XtraGrid.Columns.GridColumn LYDO;
        private DevExpress.XtraGrid.Columns.GridColumn SOTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn XemPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn DT_DOITUONGID;
        private DevExpress.XtraGrid.Columns.GridColumn NoDauKy;
        private DevExpress.XtraGrid.Columns.GridColumn NoCuoiky;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYLAPPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn MALOAI;
        private DevExpress.XtraGrid.Columns.GridColumn PHIEUKHO_ID;
        private DevExpress.XtraEditors.PanelControl pnlCHITIET;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraGrid.GridControl gvcShowCTCongNoNCC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT2;
        private DevExpress.XtraGrid.Columns.GridColumn MAPDNDC2;
        private DevExpress.XtraGrid.Columns.GridColumn HOTEN2;
        private DevExpress.XtraGrid.Columns.GridColumn LYDO2;
        private DevExpress.XtraGrid.Columns.GridColumn XemPhieu2;
        private DevExpress.XtraGrid.Columns.GridColumn MA2;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYLAPPHIEU2;
        private DevExpress.XtraGrid.Columns.GridColumn MALOAI2;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn DVT;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraGrid.Columns.GridColumn GIANHAP;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn Fill;



    }
}