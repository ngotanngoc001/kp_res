﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_CongNoNhaCungCap : DevExpress.XtraEditors.XtraForm
    {
        
        //bool flag = false;
        public Frm_CongNoNhaCungCap()
        {
            InitializeComponent();
           // LoadData();
        }

       
        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvcShowCongNoNCC.DataSource = null;
            if (optGroup.SelectedIndex == 0)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }
            if (optGroup.SelectedIndex == 0)
            {
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
            }
            else
                if (optGroup.SelectedIndex == 1)
                {
                    date_tungay.EditValue = date_denngay.EditValue = clsMain.GetServerDate();
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                }
        }
                

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                if (optGroup.SelectedIndex == 0)
                {
                    string sql = "select dt.MA,dt.TEN,SUM(ct.TONGTIENCANTT)as TongTienNo from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA inner join NHACUNGCAP dt on pk.NHACUNGCAP=dt.MA where ct.DOTTHANHTOAN='0' and ct.THU_CHI='false'  group by dt.MA,dt.TEN order by dt.TEN";
                    DataTable dtNCC = clsMain.ReturnDataTable(sql);//lấy danh sách nợ và tổng tiền nợ của từng ncc
                    dtNCC.Columns.Add("DaThanhToan", typeof(decimal));
                    dtNCC.Columns.Add("NoConLai", typeof(decimal));
                    dtNCC.Columns.Add("GiaoDich", typeof(string));
                    if (dtNCC.Rows.Count > 0)//nếu có nợ nhà cung cấp
                    {
                        for (int i = 0; i < dtNCC.Rows.Count; i++)//duyệt  qua từng nhà cung cấp
                        {
                            sql = "select SUM(ct.SOTIENTHANHTOAN)as DaThanhToan from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA inner join NHACUNGCAP dt on pk.NHACUNGCAP=dt.MA where ct.DOTTHANHTOAN!=N'Tạm'  and ct.THU_CHI='false' and dt.MA='" + dtNCC.Rows[i][0] + "' group by dt.MA,dt.TEN";
                            DataTable dtThanhToan = clsMain.ReturnDataTable(sql);//lấy tổng số tiền đã thanh toán của 1 nhà cung cấp
                            if (dtThanhToan.Rows.Count > 0)//nếu có thanh toán trả nợ
                            {
                                dtNCC.Rows[i]["DaThanhToan"] = dtThanhToan.Rows[0][0];
                            }
                            else//ko có thì gán = 0
                            {
                                dtNCC.Rows[i]["DaThanhToan"] = 0;
                            }
                            dtNCC.Rows[i]["NoConLai"] = decimal.Parse(dtNCC.Rows[i]["TongTienNo"].ToString()) - decimal.Parse(dtNCC.Rows[i]["DaThanhToan"].ToString());
                            dtNCC.Rows[i]["GiaoDich"] = "Xem Giao Dịch";
                        }
                        for (int j = 0; j < dtNCC.Rows.Count; j++)
                        {
                            if (Math.Round(decimal.Parse(dtNCC.Rows[j]["NoConLai"].ToString())) == 0)
                            {
                                dtNCC.Rows.RemoveAt(j);
                                j--;

                            }
                        }
                    }

                    gvcShowCongNoNCC.DataSource = dtNCC;
                }
                else
                    if (optGroup.SelectedIndex == 1)
                    {
                        string sql = "select dt.MA,dt.TEN,SUM(ct.TONGTIENCANTT)as TongTienNo from CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA inner join NHACUNGCAP dt on pk.NHACUNGCAP=dt.MA   where CONVERT(varchar,ct.NGAYTHANHTOAN,112) between '" + string.Format("{0:yyyyMMdd}", date_tungay.EditValue) + "' and '" + string.Format("{0:yyyyMMdd}", date_denngay.EditValue) + "' and ct.DOTTHANHTOAN='0'  and ct.THU_CHI='false'   group by dt.MA,dt.TEN order by dt.TEN";

                        //lấy danh sách nợ ncc trong khoảng thời gian nếu có giao dịch
                        DataTable dtNCC = clsMain.ReturnDataTable(sql);
                        dtNCC.Columns.Add("DaThanhToan", typeof(decimal));
                        dtNCC.Columns.Add("NoConLai", typeof(decimal));
                        dtNCC.Columns.Add("GiaoDich", typeof(string));

                        if (dtNCC.Rows.Count > 0)//nếu có  ncc thì           
                        {
                            for (int i = 0; i < dtNCC.Rows.Count; i++)//duyệt qua từng nhà cung cấp
                            {

                                sql = "select ct.PK_PHIEUKHOID from  CHITIETTHANHTOAN ct inner join KHO_PHIEUKHO pk on ct.PK_PHIEUKHOID=pk.MA inner join NHACUNGCAP dt on pk.NHACUNGCAP=dt.MA where ct.THU_CHI='False' and ct.DOTTHANHTOAN='0' and CONVERT(varchar(20),ct.NGAYTHANHTOAN,112) between '" + string.Format("{0:yyyyMMdd}", date_tungay.EditValue) + "'  and '" + string.Format("{0:yyyyMMdd}", date_denngay.EditValue) + "' and pk.NHACUNGCAP='" + dtNCC.Rows[i][0] + "'";
                                DataTable dtMaPhieu = clsMain.ReturnDataTable(sql);//lấy danh sách phiếu kho khoảng thời gian đó
                                decimal sotienconlai = 0;
                                for (int k = 0; k < dtMaPhieu.Rows.Count; k++)//duyệt qua từng phiếu để lấy số tiền nợ
                                {
                                    sotienconlai += decimal.Parse(clsMain.ReturnDataTable("select top(1)SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dtMaPhieu.Rows[k]["PK_PHIEUKHOID"].ToString() + "' and THU_CHI='false' order by CTTT_STT desc").Rows[0][0].ToString());

                                }
                                dtNCC.Rows[i]["NoConLai"] = sotienconlai;
                                dtNCC.Rows[i]["DaThanhToan"] = decimal.Parse(dtNCC.Rows[i]["TongTienNo"].ToString()) - decimal.Parse(dtNCC.Rows[i]["NoConLai"].ToString());
                                dtNCC.Rows[i]["GiaoDich"] = "Xem Giao Dịch";


                            }
                            for (int j = 0; j < dtNCC.Rows.Count; j++)
                            {
                                if (decimal.Parse(dtNCC.Rows[j]["NoConLai"].ToString()) <= 0)
                                {
                                    dtNCC.Rows.RemoveAt(j);
                                    j--;

                                }
                            }
                            gvcShowCongNoNCC.DataSource = dtNCC;
                        }
                    }
                
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        
        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gvcShowCongNoNCC.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gvcShowCongNoNCC.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0)
                    throw new Exception("Không có dữ liệu");
                DataTable dtIn = (DataTable)gvcShowCongNoNCC.DataSource;
                if (dtIn.Columns.Contains("Ngay") == false)
                {
                    dtIn.Columns.Add("Ngay", typeof(string));
                    dtIn.Columns.Add("Thang", typeof(string));
                    dtIn.Columns.Add("TuNgay", typeof(string));
                    dtIn.Columns.Add("DenNgay", typeof(string));
                    dtIn.Columns.Add("Nam", typeof(string));
                    dtIn.Columns.Add("NguoiLap", typeof(string));
                }
                dtIn.Rows[0]["Ngay"] = KP_UserManagement.clsMain.GetServerDate().Day;
                dtIn.Rows[0]["Thang"] = KP_UserManagement.clsMain.GetServerDate().Month;
                dtIn.Rows[0]["Nam"] = KP_UserManagement.clsMain.GetServerDate().Year;
                if (optGroup.SelectedIndex==1)
                {
                    dtIn.Rows[0]["TuNgay"] = string.Format("{0:dd/MM/yyyy}", date_tungay.DateTime);
                    dtIn.Rows[0]["DenNgay"] = string.Format("{0:dd/MM/yyyy}", date_denngay.DateTime);
                }
                
                string sql1 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                dtIn.Rows[0]["NguoiLap"] = clsMain.ReturnDataTable(sql1).Rows[0][0].ToString();


                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dtIn;
                frm.Mode = 23;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

      
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (e.Column == GiaoDich)
                {
                    bool _flag = true;
                    if (optGroup.SelectedIndex==1)
                        _flag = false;

                    gridView1.Appearance.FocusedCell.ForeColor = Color.Red;
                    PU_CONGNO_NCC frm = new PU_CONGNO_NCC(gridView1.GetFocusedRowCellValue("MA").ToString(), string.Format("{0:#,0}", gridView1.GetFocusedRowCellValue("NoConLai")).ToString(), date_tungay.DateTime, date_denngay.DateTime, _flag);
                    
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.ShowDialog();
                    frm.Dispose();
                    if (frm.ActiveMdiChild == null)
                        btnTimkiem_Click(sender, e);
                }
            }
            catch
            {
            }
        }

    
    }
}