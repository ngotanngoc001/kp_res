﻿namespace KP_RES
{
    partial class Frm_Thuquyxacnhan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnXacnhan = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnTatcaphieu = new DevExpress.XtraEditors.SimpleButton();
            this.gcDanhSach = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPCN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTHANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LYDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYLAPPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.XACNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOIXACNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYXACNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnXacnhan);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnTatcaphieu);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 709);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1370, 42);
            this.panelControl2.TabIndex = 8;
            // 
            // btnXacnhan
            // 
            this.btnXacnhan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXacnhan.Appearance.Options.UseFont = true;
            this.btnXacnhan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXacnhan.Image = global::KP_RES.Properties.Resources.check_book_26;
            this.btnXacnhan.Location = new System.Drawing.Point(651, 2);
            this.btnXacnhan.Margin = new System.Windows.Forms.Padding(0);
            this.btnXacnhan.Name = "btnXacnhan";
            this.btnXacnhan.Size = new System.Drawing.Size(239, 38);
            this.btnXacnhan.TabIndex = 17;
            this.btnXacnhan.Text = "Xác nhận thu đủ";
            this.btnXacnhan.Click += new System.EventHandler(this.btnXacnhan_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.Location = new System.Drawing.Point(890, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(239, 38);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Text = "Cập nhật danh sách phiếu";
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnTatcaphieu
            // 
            this.btnTatcaphieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTatcaphieu.Appearance.Options.UseFont = true;
            this.btnTatcaphieu.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnTatcaphieu.Image = global::KP_RES.Properties.Resources.bill_26;
            this.btnTatcaphieu.Location = new System.Drawing.Point(1129, 2);
            this.btnTatcaphieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnTatcaphieu.Name = "btnTatcaphieu";
            this.btnTatcaphieu.Size = new System.Drawing.Size(239, 38);
            this.btnTatcaphieu.TabIndex = 9;
            this.btnTatcaphieu.Text = "Tất cả phiếu";
            this.btnTatcaphieu.Click += new System.EventHandler(this.btnTatcaphieu_Click);
            // 
            // gcDanhSach
            // 
            this.gcDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDanhSach.Location = new System.Drawing.Point(0, 0);
            this.gcDanhSach.MainView = this.gridView1;
            this.gcDanhSach.Name = "gcDanhSach";
            this.gcDanhSach.Size = new System.Drawing.Size(1370, 709);
            this.gcDanhSach.TabIndex = 1;
            this.gcDanhSach.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAPCN,
            this.TenLoai,
            this.NGAYTHANHTOAN,
            this.HOTEN,
            this.LYDO,
            this.SOTIEN,
            this.TenNV,
            this.NGAYLAPPHIEU,
            this.XACNHAN,
            this.NGUOIXACNHAN,
            this.NGAYXACNHAN,
            this.fill,
            this.SOPHIEU,
            this.DIACHI});
            this.gridView1.GridControl = this.gcDanhSach;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell_1);
            this.gridView1.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridView1_CustomDrawFooterCell);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAPCN", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MAPCN
            // 
            this.MAPCN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPCN.AppearanceCell.Options.UseFont = true;
            this.MAPCN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPCN.AppearanceHeader.Options.UseFont = true;
            this.MAPCN.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPCN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPCN.Caption = "Mã Phiếu";
            this.MAPCN.FieldName = "MAPCN";
            this.MAPCN.Name = "MAPCN";
            this.MAPCN.OptionsColumn.AllowEdit = false;
            this.MAPCN.OptionsColumn.AllowFocus = false;
            this.MAPCN.OptionsColumn.FixedWidth = true;
            this.MAPCN.Visible = true;
            this.MAPCN.VisibleIndex = 1;
            this.MAPCN.Width = 155;
            // 
            // TenLoai
            // 
            this.TenLoai.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenLoai.AppearanceCell.Options.UseFont = true;
            this.TenLoai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenLoai.AppearanceHeader.Options.UseFont = true;
            this.TenLoai.AppearanceHeader.Options.UseTextOptions = true;
            this.TenLoai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenLoai.Caption = "Loại";
            this.TenLoai.FieldName = "TenLoai";
            this.TenLoai.Name = "TenLoai";
            this.TenLoai.OptionsColumn.AllowEdit = false;
            this.TenLoai.OptionsColumn.AllowFocus = false;
            this.TenLoai.OptionsColumn.FixedWidth = true;
            this.TenLoai.Visible = true;
            this.TenLoai.VisibleIndex = 2;
            this.TenLoai.Width = 170;
            // 
            // NGAYTHANHTOAN
            // 
            this.NGAYTHANHTOAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYTHANHTOAN.AppearanceCell.Options.UseFont = true;
            this.NGAYTHANHTOAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTHANHTOAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTHANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTHANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYTHANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTHANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTHANHTOAN.Caption = "Ngày Thanh Toán";
            this.NGAYTHANHTOAN.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYTHANHTOAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTHANHTOAN.FieldName = "NGAYTHANHTOAN";
            this.NGAYTHANHTOAN.Name = "NGAYTHANHTOAN";
            this.NGAYTHANHTOAN.OptionsColumn.AllowEdit = false;
            this.NGAYTHANHTOAN.OptionsColumn.AllowFocus = false;
            this.NGAYTHANHTOAN.OptionsColumn.FixedWidth = true;
            this.NGAYTHANHTOAN.Visible = true;
            this.NGAYTHANHTOAN.VisibleIndex = 3;
            this.NGAYTHANHTOAN.Width = 150;
            // 
            // HOTEN
            // 
            this.HOTEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN.AppearanceCell.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HOTEN.AppearanceHeader.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOTEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOTEN.Caption = "Người Nhận/Gửi";
            this.HOTEN.FieldName = "HOTEN";
            this.HOTEN.Name = "HOTEN";
            this.HOTEN.OptionsColumn.AllowEdit = false;
            this.HOTEN.OptionsColumn.AllowFocus = false;
            this.HOTEN.OptionsColumn.FixedWidth = true;
            this.HOTEN.Visible = true;
            this.HOTEN.VisibleIndex = 4;
            this.HOTEN.Width = 220;
            // 
            // LYDO
            // 
            this.LYDO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LYDO.AppearanceCell.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LYDO.AppearanceHeader.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Options.UseTextOptions = true;
            this.LYDO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LYDO.Caption = "Lý Do";
            this.LYDO.FieldName = "LYDO";
            this.LYDO.Name = "LYDO";
            this.LYDO.OptionsColumn.AllowEdit = false;
            this.LYDO.OptionsColumn.AllowFocus = false;
            this.LYDO.OptionsColumn.FixedWidth = true;
            this.LYDO.Visible = true;
            this.LYDO.VisibleIndex = 5;
            this.LYDO.Width = 300;
            // 
            // SOTIEN
            // 
            this.SOTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOTIEN.AppearanceCell.Options.UseFont = true;
            this.SOTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOTIEN.AppearanceHeader.Options.UseFont = true;
            this.SOTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTIEN.Caption = "Số Tiền";
            this.SOTIEN.DisplayFormat.FormatString = "#,0";
            this.SOTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SOTIEN.FieldName = "SOTIEN";
            this.SOTIEN.Name = "SOTIEN";
            this.SOTIEN.OptionsColumn.AllowEdit = false;
            this.SOTIEN.OptionsColumn.AllowFocus = false;
            this.SOTIEN.OptionsColumn.FixedWidth = true;
            this.SOTIEN.Visible = true;
            this.SOTIEN.VisibleIndex = 6;
            this.SOTIEN.Width = 150;
            // 
            // TenNV
            // 
            this.TenNV.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenNV.AppearanceCell.Options.UseFont = true;
            this.TenNV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenNV.AppearanceHeader.Options.UseFont = true;
            this.TenNV.AppearanceHeader.Options.UseTextOptions = true;
            this.TenNV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenNV.Caption = "Người Lập Phiếu";
            this.TenNV.FieldName = "TENNHANVIEN";
            this.TenNV.Name = "TenNV";
            this.TenNV.OptionsColumn.AllowEdit = false;
            this.TenNV.OptionsColumn.AllowFocus = false;
            this.TenNV.OptionsColumn.FixedWidth = true;
            this.TenNV.Visible = true;
            this.TenNV.VisibleIndex = 7;
            this.TenNV.Width = 220;
            // 
            // NGAYLAPPHIEU
            // 
            this.NGAYLAPPHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.Caption = "Ngày Lập Phiếu";
            this.NGAYLAPPHIEU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYLAPPHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYLAPPHIEU.FieldName = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.Name = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.OptionsColumn.AllowEdit = false;
            this.NGAYLAPPHIEU.OptionsColumn.AllowFocus = false;
            this.NGAYLAPPHIEU.OptionsColumn.FixedWidth = true;
            this.NGAYLAPPHIEU.Visible = true;
            this.NGAYLAPPHIEU.VisibleIndex = 8;
            this.NGAYLAPPHIEU.Width = 138;
            // 
            // XACNHAN
            // 
            this.XACNHAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XACNHAN.AppearanceCell.Options.UseFont = true;
            this.XACNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XACNHAN.AppearanceHeader.Options.UseFont = true;
            this.XACNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.XACNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XACNHAN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.XACNHAN.Caption = "Xác nhận thủ quỹ";
            this.XACNHAN.FieldName = "XACNHAN";
            this.XACNHAN.Name = "XACNHAN";
            this.XACNHAN.OptionsColumn.AllowEdit = false;
            this.XACNHAN.OptionsColumn.AllowFocus = false;
            this.XACNHAN.OptionsColumn.FixedWidth = true;
            this.XACNHAN.Visible = true;
            this.XACNHAN.VisibleIndex = 9;
            this.XACNHAN.Width = 93;
            // 
            // NGUOIXACNHAN
            // 
            this.NGUOIXACNHAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGUOIXACNHAN.AppearanceCell.Options.UseFont = true;
            this.NGUOIXACNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGUOIXACNHAN.AppearanceHeader.Options.UseFont = true;
            this.NGUOIXACNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUOIXACNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUOIXACNHAN.Caption = "Người xác nhận";
            this.NGUOIXACNHAN.FieldName = "NGUOIXACNHAN";
            this.NGUOIXACNHAN.MinWidth = 150;
            this.NGUOIXACNHAN.Name = "NGUOIXACNHAN";
            this.NGUOIXACNHAN.OptionsColumn.AllowEdit = false;
            this.NGUOIXACNHAN.OptionsColumn.AllowFocus = false;
            this.NGUOIXACNHAN.OptionsColumn.FixedWidth = true;
            this.NGUOIXACNHAN.Visible = true;
            this.NGUOIXACNHAN.VisibleIndex = 10;
            this.NGUOIXACNHAN.Width = 150;
            // 
            // NGAYXACNHAN
            // 
            this.NGAYXACNHAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYXACNHAN.AppearanceCell.Options.UseFont = true;
            this.NGAYXACNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYXACNHAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYXACNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYXACNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYXACNHAN.Caption = "Ngày xác nhận";
            this.NGAYXACNHAN.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYXACNHAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYXACNHAN.FieldName = "NGAYXACNHAN";
            this.NGAYXACNHAN.Name = "NGAYXACNHAN";
            this.NGAYXACNHAN.OptionsColumn.AllowEdit = false;
            this.NGAYXACNHAN.OptionsColumn.AllowFocus = false;
            this.NGAYXACNHAN.OptionsColumn.FixedWidth = true;
            this.NGAYXACNHAN.Visible = true;
            this.NGAYXACNHAN.VisibleIndex = 11;
            this.NGAYXACNHAN.Width = 200;
            // 
            // fill
            // 
            this.fill.Name = "fill";
            this.fill.OptionsColumn.AllowEdit = false;
            this.fill.OptionsColumn.AllowFocus = false;
            this.fill.Visible = true;
            this.fill.VisibleIndex = 12;
            this.fill.Width = 20;
            // 
            // SOPHIEU
            // 
            this.SOPHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOPHIEU.AppearanceCell.Options.UseFont = true;
            this.SOPHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SOPHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOPHIEU.AppearanceHeader.Options.UseFont = true;
            this.SOPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHIEU.Caption = "Mã Phiếu";
            this.SOPHIEU.FieldName = "SOPHIEU";
            this.SOPHIEU.Name = "SOPHIEU";
            this.SOPHIEU.OptionsColumn.AllowEdit = false;
            this.SOPHIEU.OptionsColumn.AllowFocus = false;
            this.SOPHIEU.OptionsColumn.FixedWidth = true;
            this.SOPHIEU.Width = 150;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIACHI.AppearanceCell.Options.UseFont = true;
            this.DIACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DIACHI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa Chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Width = 341;
            // 
            // Frm_Thuquyxacnhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 751);
            this.Controls.Add(this.gcDanhSach);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Thuquyxacnhan";
            this.Text = "Thủ quỹ xác nhận";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnTatcaphieu;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gcDanhSach;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn MAPCN;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn TenNV;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYLAPPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn LYDO;
        private DevExpress.XtraGrid.Columns.GridColumn SOTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn HOTEN;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn XACNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOIXACNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn fill;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTHANHTOAN;
        private DevExpress.XtraEditors.SimpleButton btnXacnhan;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYXACNHAN;



    }
}