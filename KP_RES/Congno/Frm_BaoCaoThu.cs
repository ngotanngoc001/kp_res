﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_BaoCaoThu : DevExpress.XtraEditors.XtraForm
    {
        BindingSource bs = new BindingSource();
        public Frm_BaoCaoThu()
        {
            InitializeComponent();
            btnHuy.Enabled = clsUserManagement.AllowDelete("90");
         
        }
        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gcDanhSach.DataSource = null;
            if (optGroup.SelectedIndex == 0)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }
            if (optGroup.SelectedIndex == 0)
            {
                lbl_title.Visible =  false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                gl_NguoiLapPhieu.Visible = gl_LoaiThu.Visible =gl_PhuongThucTT.Visible= false;
                
            }
            else
                if (optGroup.SelectedIndex == 3 || optGroup.SelectedIndex == 5)
                {
                    date_tungay.EditValue = date_denngay.EditValue = clsMain.GetServerDate();
                    lbl_title.Visible = false;
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                    gl_NguoiLapPhieu.Visible = gl_LoaiThu.Visible = gl_PhuongThucTT.Visible = false;
                }
                else
                    {
                        if (optGroup.SelectedIndex == 1)
                        {
                            lbl_title.Text = "Chọn Người Lập Phiếu:";
                            gl_NguoiLapPhieu.Visible = true;
                            gl_NguoiLapPhieu.EditValue = "";
                            gl_LoaiThu.Visible = gl_PhuongThucTT.Visible = false;
                            gl_NguoiLapPhieu.Properties.DataSource = clsMain.ReturnDataTable("select MANHANVIEN,TENNHANVIEN from DM_NHANVIEN");
                           
                        }
                        else
                            if(optGroup.SelectedIndex==2)
                            {
                                lbl_title.Text = "Chọn Loại Phiếu Thu:";
                                gl_LoaiThu.Visible = true;
                                gl_LoaiThu.EditValue = "";
                                gl_NguoiLapPhieu.Visible = gl_PhuongThucTT.Visible = false;
                                gl_LoaiThu.Properties.DataSource = clsMain.ReturnDataTable("select MaLoai,TenLoai from CN_LOAI_CONGNO where LoaiThuChi='01'");
                            }
                            else
                                if (optGroup.SelectedIndex == 4)
                                {
                                    lbl_title.Text = "Chọn HTTT:";
                                    gl_PhuongThucTT.Visible = true;
                                    gl_PhuongThucTT.EditValue = "";
                                    gl_NguoiLapPhieu.Visible = gl_LoaiThu.Visible = false;
                                    DataTable dtPT = clsMain.ReturnDataTable("select * from HINHTHUC_THANHTOAN");
                                    if (dtPT.Rows.Count == 0)
                                    {
                                        clsMain.ExecuteSQL("insert into HINHTHUC_THANHTOAN(MAHTT,TEN_HTT) values ('01',N'Tiền Mặt');insert into HINHTHUC_THANHTOAN(MAHTT,TEN_HTT) values ('02',N'Chuyển Khoản');");
                                    }
                                    dtPT = clsMain.ReturnDataTable("select * from HINHTHUC_THANHTOAN");
                                    gl_PhuongThucTT.Properties.DataSource = dtPT;
                                }
                        lbl_title.Visible = true;
                        lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                        
                    }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                if (optGroup.SelectedIndex == 0)
                {
                    string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS @NgayTu='',@NgayDen='',@NguoiLapPhieu='',@LoaiThu='',@PhuongThuc=''");
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    gcDanhSach.DataSource = dt;
                }
                else
                    if (optGroup.SelectedIndex == 1)
                    {
                        if (gl_NguoiLapPhieu.EditValue.ToString() == "")
                        {
                            throw new Exception("Mời chọn nhân viên lập phiếu");
                        }
                        string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS @NgayTu='',@NgayDen='',@NguoiLapPhieu='" + gl_NguoiLapPhieu.EditValue + "',@LoaiThu='',@PhuongThuc=''");
                        gcDanhSach.DataSource = clsMain.ReturnDataTable(sql);
                    }
                    else
                        if (optGroup.SelectedIndex == 2)
                        {
                            if (gl_LoaiThu.EditValue.ToString() == "")
                            {
                                throw new Exception("Mời chọn loại thu");
                            }
                            string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS @NgayTu='',@NgayDen='',@NguoiLapPhieu='',@LoaiThu='" + gl_LoaiThu.EditValue + "',@PhuongThuc=''");
                            gcDanhSach.DataSource = clsMain.ReturnDataTable(sql);
                        }
                        else
                            if (optGroup.SelectedIndex == 3)
                            {
                                string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS @NgayTu='" + string.Format("{0:yyyyMMdd}", date_tungay.EditValue) + "',@NgayDen='" + string.Format("{0:yyyyMMdd}", date_denngay.EditValue) + "',@NguoiLapPhieu='',@LoaiThu='',@PhuongThuc=''");
                                gcDanhSach.DataSource = clsMain.ReturnDataTable(sql);
                            }
                            else
                                if (optGroup.SelectedIndex == 4)
                                {
                                    if (gl_PhuongThucTT.EditValue.ToString() == "")
                                    {
                                        throw new Exception("Mời chọn hình thức thanh toán");
                                    }
                                   
                                    string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS @NgayTu='',@NgayDen='',@NguoiLapPhieu='',@LoaiThu='',@PhuongThuc='" + gl_PhuongThucTT.EditValue.ToString() + "'");
                                    gcDanhSach.DataSource = clsMain.ReturnDataTable(sql);
                                }
                                else
                                    if (optGroup.SelectedIndex == 5)
                                    {
                                        string sql = string.Format("exec sp_CN_CONGNO_DANHSACH_NS_NGAYTHANHTOAN @NgayTu='" + string.Format("{0:yyyyMMdd}", date_tungay.EditValue) + "',@NgayDen='" + string.Format("{0:yyyyMMdd}", date_denngay.EditValue) + "',@NguoiLapPhieu='',@LoaiThu='',@PhuongThuc=''");
                                        gcDanhSach.DataSource = clsMain.ReturnDataTable(sql);
                                    }
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
            
           
        }

        private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView3.FocusedRowHandle == gridView3.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = gridView3.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView3.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView3.Focus();
                gridView3.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView3.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gcDanhSach.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gcDanhSach.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView3.RowCount > 0)
                {
                    DataTable myDT = ((DataTable)gcDanhSach.DataSource).Copy(); 

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex == 1)
                        Description = "( " + Description + " : " + gl_NguoiLapPhieu.Text + " )";
                    else if (optGroup.SelectedIndex == 2)
                        Description = "( " + Description + " : " + gl_LoaiThu.Text + " )";
                    else if (optGroup.SelectedIndex == 3 || optGroup.SelectedIndex == 5)
                        Description = "( " + Description + " - Từ ngày: " + date_tungay.Text + " - Đến ngày: " + date_denngay.Text + " )";
                    else if (optGroup.SelectedIndex == 4)
                        Description = "( " + Description + " : " + gl_PhuongThucTT.Text + " )";

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;

                    myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 21;
                    frm.ShowDialog();
                    frm.Dispose();

                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void bntInLai_Click(object sender, EventArgs e)
        {
            //gridView3.GetFocusedRowCellValue(DIACHI).ToString());
            try
            {
                if (gridView3.FocusedRowHandle < 0)
                    throw new Exception("Hãy chọn Phiếu thu cần in");
                DataTable dtInBaoCao = new DataTable();
                dtInBaoCao.Columns.Add("TongCong", typeof(string));
                dtInBaoCao.Columns.Add("SoPhieu", typeof(string));
                dtInBaoCao.Columns.Add(new DataColumn("Ngay", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("Thang", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("Nam", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("HoTen", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("DiaChi", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("LyDo", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("SoTien", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("VietBangChu", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("KemTheo", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("NguoiLap", typeof(string)));
                dtInBaoCao.Columns.Add(new DataColumn("hotengiaodich", typeof(string)));
                DataRow row = dtInBaoCao.NewRow();
                row["SoPhieu"] = gridView3.GetFocusedRowCellValue(MAPCN).ToString();
                row["Ngay"] = ((DateTime )gridView3.GetFocusedRowCellValue(NGAYTHANHTOAN)).Day;
                row["Thang"] = ((DateTime)gridView3.GetFocusedRowCellValue(NGAYTHANHTOAN)).Month;
                row["Nam"] = ((DateTime)gridView3.GetFocusedRowCellValue(NGAYTHANHTOAN)).Year;
                row["HoTen"] = gridView3.GetFocusedRowCellValue(HOTEN).ToString();
                row["DiaChi"] = gridView3.GetFocusedRowCellValue(DIACHI).ToString();
                row["LyDo"] = gridView3.GetFocusedRowCellValue(LYDO).ToString();
                row["SoTien"] = string.Format("{0:#,0}", decimal.Parse(gridView3.GetFocusedRowCellValue(SOTIEN).ToString()));
                row["VietBangChu"] = cls_KHTT. ChuyenSo(gridView3.GetFocusedRowCellValue(SOTIEN).ToString().Replace(",", "")) + "đồng";
                row["KemTheo"] = clsMain.ReturnDataTable("select KEMTHEO from CN_CONGNO where MAPCN='" + gridView3.GetFocusedRowCellValue(MAPCN).ToString() + "'").Rows[0][0].ToString();
               // string sql1 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                //row["NguoiLap"] = clsMain.ReturnDataTable(sql1).Rows[0][0].ToString();
                row["NguoiLap"] = gridView3.GetFocusedRowCellValue(TENNHANVIEN).ToString();
                row["hotengiaodich"] = gridView3.GetFocusedRowCellValue(HOTENGIAODICH).ToString();
                dtInBaoCao.Rows.Add(row);
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dtInBaoCao;
                frm.Mode = 18;
                frm.ShowDialog();
                frm.Dispose();


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn hủy phiếu thu này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string sSQL = "INSERT INTO CN_CONGNO_HUY SELECT *  FROM  CN_CONGNO Where MAPCN=" + clsMain.SQLString(gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "MAPCN").ToString()) + "\n"; 
                sSQL += "DELETE FROM  CN_CONGNO Where MAPCN=" + clsMain.SQLString(gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "MAPCN").ToString()) + "\n";

                if (clsMain.ExecuteSQL(sSQL))
                {
                    clsMain.ExecuteSQL("EXEC SP_TINH_QUY_TIEN_MAT");
                    clsMain.ExecuteSQL("EXEC SP_TINH_QUY_TIEN_MAT_NGAYTHANHTOAN");
                    XtraMessageBox.Show("Đã hủy phiếu thu này", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnTimkiem_Click(null, null);
                }
                else
                {
                    XtraMessageBox.Show("Có lổi vui lòng thử lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

    }
}