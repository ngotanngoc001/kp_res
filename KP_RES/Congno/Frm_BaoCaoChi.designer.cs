﻿namespace KP_RES
{
    partial class Frm_BaoCaoChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCaoChi));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.date_denngay = new DevExpress.XtraEditors.DateEdit();
            this.date_tungay = new DevExpress.XtraEditors.DateEdit();
            this.gl_NguonChi = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gl_LoaiThu = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gl_PhuongThucTT = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gl_NguoiLapPhieu = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MANHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbNgayTu = new DevExpress.XtraEditors.LabelControl();
            this.lbl_title = new DevExpress.XtraEditors.LabelControl();
            this.lbNgayDen = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.bntInLai = new DevExpress.XtraEditors.SimpleButton();
            this.gcDanhSach = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPCN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PK_MAPHIEUKHO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAPDNDC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUONCHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LYDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KEMTHEO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PTTHANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOTENGIAODICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTHANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNGANHANGCHUYEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOTAIKHOANCHUYEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNGANHANGNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOTAIKHOANNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOGIAODICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYLAPPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PK_MAPHIEUKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NguonChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_LoaiThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_PhuongThucTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NguoiLapPhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 743);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 221);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 485);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 76);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 407);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimkiem.Image")));
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.date_denngay);
            this.pnChoose.Controls.Add(this.date_tungay);
            this.pnChoose.Controls.Add(this.gl_NguonChi);
            this.pnChoose.Controls.Add(this.gl_LoaiThu);
            this.pnChoose.Controls.Add(this.gl_PhuongThucTT);
            this.pnChoose.Controls.Add(this.gl_NguoiLapPhieu);
            this.pnChoose.Controls.Add(this.lbNgayTu);
            this.pnChoose.Controls.Add(this.lbl_title);
            this.pnChoose.Controls.Add(this.lbNgayDen);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 74);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // date_denngay
            // 
            this.date_denngay.EditValue = null;
            this.date_denngay.Location = new System.Drawing.Point(82, 38);
            this.date_denngay.Name = "date_denngay";
            this.date_denngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_denngay.Properties.Appearance.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_denngay.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.date_denngay.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Properties.AppearanceFocused.Options.UseFont = true;
            this.date_denngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_denngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_denngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_denngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_denngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_denngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_denngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_denngay.Size = new System.Drawing.Size(129, 26);
            this.date_denngay.TabIndex = 22;
            // 
            // date_tungay
            // 
            this.date_tungay.EditValue = null;
            this.date_tungay.Location = new System.Drawing.Point(82, 7);
            this.date_tungay.Name = "date_tungay";
            this.date_tungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_tungay.Properties.Appearance.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.date_tungay.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.date_tungay.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Properties.AppearanceFocused.Options.UseFont = true;
            this.date_tungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date_tungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.date_tungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_tungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.date_tungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date_tungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.date_tungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.date_tungay.Size = new System.Drawing.Size(130, 26);
            this.date_tungay.TabIndex = 21;
            // 
            // gl_NguonChi
            // 
            this.gl_NguonChi.EditValue = "";
            this.gl_NguonChi.Location = new System.Drawing.Point(1, 38);
            this.gl_NguonChi.Name = "gl_NguonChi";
            this.gl_NguonChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguonChi.Properties.Appearance.Options.UseFont = true;
            this.gl_NguonChi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguonChi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_NguonChi.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguonChi.Properties.AppearanceFocused.Options.UseFont = true;
            this.gl_NguonChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_NguonChi.Properties.DisplayMember = "TenNC";
            this.gl_NguonChi.Properties.NullText = "";
            this.gl_NguonChi.Properties.PopupFormSize = new System.Drawing.Size(210, 200);
            this.gl_NguonChi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gl_NguonChi.Properties.ValueMember = "MaNC";
            this.gl_NguonChi.Properties.View = this.gridView4;
            this.gl_NguonChi.Size = new System.Drawing.Size(210, 26);
            this.gl_NguonChi.TabIndex = 20;
            // 
            // gridView4
            // 
            this.gridView4.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView4.ColumnPanelRowHeight = 30;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.GroupRowHeight = 30;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowColumnHeaders = false;
            this.gridView4.OptionsView.ShowDetailButtons = false;
            this.gridView4.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.RowHeight = 30;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Mã NC";
            this.gridColumn5.FieldName = "MaNC";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 55;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Tên Nguồn Chi";
            this.gridColumn6.FieldName = "TenNC";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 155;
            // 
            // gl_LoaiThu
            // 
            this.gl_LoaiThu.EditValue = "";
            this.gl_LoaiThu.Location = new System.Drawing.Point(1, 38);
            this.gl_LoaiThu.Name = "gl_LoaiThu";
            this.gl_LoaiThu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_LoaiThu.Properties.Appearance.Options.UseFont = true;
            this.gl_LoaiThu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_LoaiThu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_LoaiThu.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_LoaiThu.Properties.AppearanceFocused.Options.UseFont = true;
            this.gl_LoaiThu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_LoaiThu.Properties.DisplayMember = "TenLoai";
            this.gl_LoaiThu.Properties.NullText = "";
            this.gl_LoaiThu.Properties.PopupFormSize = new System.Drawing.Size(210, 200);
            this.gl_LoaiThu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gl_LoaiThu.Properties.ValueMember = "MaLoai";
            this.gl_LoaiThu.Properties.View = this.gridView1;
            this.gl_LoaiThu.Size = new System.Drawing.Size(210, 26);
            this.gl_LoaiThu.TabIndex = 18;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Mã NV";
            this.gridColumn1.FieldName = "MaLoai";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 55;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Tên Nhân Viên";
            this.gridColumn2.FieldName = "TenLoai";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 155;
            // 
            // gl_PhuongThucTT
            // 
            this.gl_PhuongThucTT.EditValue = "";
            this.gl_PhuongThucTT.Location = new System.Drawing.Point(0, 39);
            this.gl_PhuongThucTT.Name = "gl_PhuongThucTT";
            this.gl_PhuongThucTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_PhuongThucTT.Properties.Appearance.Options.UseFont = true;
            this.gl_PhuongThucTT.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_PhuongThucTT.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_PhuongThucTT.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_PhuongThucTT.Properties.AppearanceFocused.Options.UseFont = true;
            this.gl_PhuongThucTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_PhuongThucTT.Properties.DisplayMember = "TEN_HTT";
            this.gl_PhuongThucTT.Properties.NullText = "";
            this.gl_PhuongThucTT.Properties.PopupFormSize = new System.Drawing.Size(210, 200);
            this.gl_PhuongThucTT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gl_PhuongThucTT.Properties.ValueMember = "MAHTT";
            this.gl_PhuongThucTT.Properties.View = this.gridView2;
            this.gl_PhuongThucTT.Size = new System.Drawing.Size(210, 26);
            this.gl_PhuongThucTT.TabIndex = 19;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.GroupRowHeight = 30;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowColumnHeaders = false;
            this.gridView2.OptionsView.ShowDetailButtons = false;
            this.gridView2.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Mã NV";
            this.gridColumn3.FieldName = "MAHTT";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 55;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Tên Nhân Viên";
            this.gridColumn4.FieldName = "TEN_HTT";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 155;
            // 
            // gl_NguoiLapPhieu
            // 
            this.gl_NguoiLapPhieu.EditValue = "";
            this.gl_NguoiLapPhieu.Location = new System.Drawing.Point(2, 39);
            this.gl_NguoiLapPhieu.Name = "gl_NguoiLapPhieu";
            this.gl_NguoiLapPhieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguoiLapPhieu.Properties.Appearance.Options.UseFont = true;
            this.gl_NguoiLapPhieu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguoiLapPhieu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.gl_NguoiLapPhieu.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gl_NguoiLapPhieu.Properties.AppearanceFocused.Options.UseFont = true;
            this.gl_NguoiLapPhieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gl_NguoiLapPhieu.Properties.DisplayMember = "TENNHANVIEN";
            this.gl_NguoiLapPhieu.Properties.NullText = "";
            this.gl_NguoiLapPhieu.Properties.PopupFormSize = new System.Drawing.Size(210, 500);
            this.gl_NguoiLapPhieu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gl_NguoiLapPhieu.Properties.ValueMember = "MANHANVIEN";
            this.gl_NguoiLapPhieu.Properties.View = this.gridLookUpEdit1View;
            this.gl_NguoiLapPhieu.Size = new System.Drawing.Size(210, 26);
            this.gl_NguoiLapPhieu.TabIndex = 17;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridLookUpEdit1View.Appearance.FocusedRow.Options.UseFont = true;
            this.gridLookUpEdit1View.ColumnPanelRowHeight = 30;
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MANHANVIEN,
            this.TENNHANVIEN});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.GroupRowHeight = 30;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowColumnHeaders = false;
            this.gridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.RowHeight = 30;
            // 
            // MANHANVIEN
            // 
            this.MANHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MANHANVIEN.AppearanceCell.Options.UseFont = true;
            this.MANHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.MANHANVIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MANHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MANHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.MANHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.MANHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MANHANVIEN.Caption = "Mã NV";
            this.MANHANVIEN.FieldName = "MANHANVIEN";
            this.MANHANVIEN.Name = "MANHANVIEN";
            this.MANHANVIEN.Visible = true;
            this.MANHANVIEN.VisibleIndex = 0;
            this.MANHANVIEN.Width = 55;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Tên Nhân Viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 1;
            this.TENNHANVIEN.Width = 155;
            // 
            // lbNgayTu
            // 
            this.lbNgayTu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayTu.Location = new System.Drawing.Point(7, 10);
            this.lbNgayTu.Name = "lbNgayTu";
            this.lbNgayTu.Size = new System.Drawing.Size(72, 19);
            this.lbNgayTu.TabIndex = 10;
            this.lbNgayTu.Text = "Từ Ngày :";
            this.lbNgayTu.Visible = false;
            // 
            // lbl_title
            // 
            this.lbl_title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbl_title.Location = new System.Drawing.Point(5, 8);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(123, 19);
            this.lbl_title.TabIndex = 16;
            this.lbl_title.Text = "Người Lập Phiếu:";
            this.lbl_title.Visible = false;
            // 
            // lbNgayDen
            // 
            this.lbNgayDen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayDen.Location = new System.Drawing.Point(5, 41);
            this.lbNgayDen.Name = "lbNgayDen";
            this.lbNgayDen.Size = new System.Drawing.Size(80, 19);
            this.lbNgayDen.TabIndex = 11;
            this.lbNgayDen.Text = "Đến Ngày :";
            this.lbNgayDen.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 219);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "TC";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TC", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("NLP", "Theo người lập phiếu"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("LT", "Theo loại phiếu"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Ngay", "Theo ngày lập phiếu"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("PTTT", "Theo hình thức thanh toán"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("NC", "Theo nguồn chi"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("ngaythanhtoan", "Theo ngày thanh toán")});
            this.optGroup.Size = new System.Drawing.Size(215, 214);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = ((System.Drawing.Image)(resources.GetObject("btnThunho.Image")));
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 706);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Controls.Add(this.btnHuy);
            this.panelControl2.Controls.Add(this.bntInLai);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1325, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 743);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 322);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 355);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 227);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 64);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 291);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 64);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 128);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 227);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 64);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 64);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 64);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 677);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 64);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 258);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 64);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatfile.Image")));
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 194);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 64);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = ((System.Drawing.Image)(resources.GetObject("btnXemtruockhiin.Image")));
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 130);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 64);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHuy.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuy.Location = new System.Drawing.Point(2, 66);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(35, 64);
            this.btnHuy.TabIndex = 20;
            this.btnHuy.Text = "&In";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // bntInLai
            // 
            this.bntInLai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntInLai.Appearance.Options.UseFont = true;
            this.bntInLai.Dock = System.Windows.Forms.DockStyle.Top;
            this.bntInLai.Image = ((System.Drawing.Image)(resources.GetObject("bntInLai.Image")));
            this.bntInLai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bntInLai.Location = new System.Drawing.Point(2, 2);
            this.bntInLai.Margin = new System.Windows.Forms.Padding(4);
            this.bntInLai.Name = "bntInLai";
            this.bntInLai.Size = new System.Drawing.Size(35, 64);
            this.bntInLai.TabIndex = 18;
            this.bntInLai.Text = "&In";
            this.bntInLai.Click += new System.EventHandler(this.bntInLai_Click);
            // 
            // gcDanhSach
            // 
            this.gcDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDanhSach.Location = new System.Drawing.Point(223, 0);
            this.gcDanhSach.MainView = this.gridView3;
            this.gcDanhSach.Name = "gcDanhSach";
            this.gcDanhSach.Size = new System.Drawing.Size(1102, 743);
            this.gcDanhSach.TabIndex = 0;
            this.gcDanhSach.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAPCN,
            this.TenLoai,
            this.HOTEN,
            this.PK_MAPHIEUKHO1,
            this.MAPDNDC,
            this.NGUONCHI,
            this.LYDO,
            this.KEMTHEO,
            this.SOTIEN,
            this.PTTHANHTOAN,
            this.HOTENGIAODICH,
            this.NGAYTHANHTOAN,
            this.TENNGANHANGCHUYEN,
            this.SOTAIKHOANCHUYEN,
            this.TENNGANHANGNHAN,
            this.SOTAIKHOANNHAN,
            this.SOGIAODICH,
            this.TenNV,
            this.NGAYLAPPHIEU,
            this.Fill,
            this.PK_MAPHIEUKHO,
            this.DIACHI});
            this.gridView3.GridControl = this.gcDanhSach;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 35;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MAPCN
            // 
            this.MAPCN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MAPCN.AppearanceCell.Options.UseFont = true;
            this.MAPCN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAPCN.AppearanceHeader.Options.UseFont = true;
            this.MAPCN.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPCN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPCN.Caption = "Mã Phiếu Chi";
            this.MAPCN.FieldName = "MAPCN";
            this.MAPCN.Name = "MAPCN";
            this.MAPCN.OptionsColumn.AllowEdit = false;
            this.MAPCN.OptionsColumn.AllowFocus = false;
            this.MAPCN.OptionsColumn.FixedWidth = true;
            this.MAPCN.Visible = true;
            this.MAPCN.VisibleIndex = 1;
            this.MAPCN.Width = 143;
            // 
            // TenLoai
            // 
            this.TenLoai.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TenLoai.AppearanceCell.Options.UseFont = true;
            this.TenLoai.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TenLoai.AppearanceHeader.Options.UseFont = true;
            this.TenLoai.AppearanceHeader.Options.UseTextOptions = true;
            this.TenLoai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenLoai.Caption = "Loại";
            this.TenLoai.FieldName = "TenLoai";
            this.TenLoai.Name = "TenLoai";
            this.TenLoai.OptionsColumn.AllowEdit = false;
            this.TenLoai.OptionsColumn.AllowFocus = false;
            this.TenLoai.OptionsColumn.FixedWidth = true;
            this.TenLoai.Visible = true;
            this.TenLoai.VisibleIndex = 2;
            this.TenLoai.Width = 170;
            // 
            // HOTEN
            // 
            this.HOTEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.HOTEN.AppearanceCell.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HOTEN.AppearanceHeader.Options.UseFont = true;
            this.HOTEN.AppearanceHeader.Options.UseTextOptions = true;
            this.HOTEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOTEN.Caption = "Chi cho";
            this.HOTEN.FieldName = "HOTEN";
            this.HOTEN.Name = "HOTEN";
            this.HOTEN.OptionsColumn.AllowEdit = false;
            this.HOTEN.OptionsColumn.AllowFocus = false;
            this.HOTEN.OptionsColumn.FixedWidth = true;
            this.HOTEN.Visible = true;
            this.HOTEN.VisibleIndex = 3;
            this.HOTEN.Width = 220;
            // 
            // PK_MAPHIEUKHO1
            // 
            this.PK_MAPHIEUKHO1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.PK_MAPHIEUKHO1.AppearanceCell.Options.UseFont = true;
            this.PK_MAPHIEUKHO1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PK_MAPHIEUKHO1.AppearanceHeader.Options.UseFont = true;
            this.PK_MAPHIEUKHO1.AppearanceHeader.Options.UseTextOptions = true;
            this.PK_MAPHIEUKHO1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PK_MAPHIEUKHO1.Caption = "Mã phiếu ";
            this.PK_MAPHIEUKHO1.FieldName = "SOPHIEU";
            this.PK_MAPHIEUKHO1.Name = "PK_MAPHIEUKHO1";
            this.PK_MAPHIEUKHO1.OptionsColumn.AllowEdit = false;
            this.PK_MAPHIEUKHO1.OptionsColumn.AllowFocus = false;
            this.PK_MAPHIEUKHO1.OptionsColumn.FixedWidth = true;
            this.PK_MAPHIEUKHO1.Visible = true;
            this.PK_MAPHIEUKHO1.VisibleIndex = 4;
            this.PK_MAPHIEUKHO1.Width = 149;
            // 
            // MAPDNDC
            // 
            this.MAPDNDC.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MAPDNDC.AppearanceCell.Options.UseFont = true;
            this.MAPDNDC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAPDNDC.AppearanceHeader.Options.UseFont = true;
            this.MAPDNDC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAPDNDC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAPDNDC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAPDNDC.Caption = "Phiếu đề nghị";
            this.MAPDNDC.FieldName = "MAPDNDC";
            this.MAPDNDC.Name = "MAPDNDC";
            this.MAPDNDC.OptionsColumn.AllowEdit = false;
            this.MAPDNDC.OptionsColumn.AllowFocus = false;
            this.MAPDNDC.Visible = true;
            this.MAPDNDC.VisibleIndex = 5;
            this.MAPDNDC.Width = 149;
            // 
            // NGUONCHI
            // 
            this.NGUONCHI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGUONCHI.AppearanceCell.Options.UseFont = true;
            this.NGUONCHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGUONCHI.AppearanceHeader.Options.UseFont = true;
            this.NGUONCHI.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUONCHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUONCHI.Caption = "Nguồn Chi";
            this.NGUONCHI.FieldName = "NGUONCHI";
            this.NGUONCHI.Name = "NGUONCHI";
            this.NGUONCHI.OptionsColumn.AllowEdit = false;
            this.NGUONCHI.OptionsColumn.AllowFocus = false;
            this.NGUONCHI.Visible = true;
            this.NGUONCHI.VisibleIndex = 6;
            this.NGUONCHI.Width = 120;
            // 
            // LYDO
            // 
            this.LYDO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LYDO.AppearanceCell.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LYDO.AppearanceHeader.Options.UseFont = true;
            this.LYDO.AppearanceHeader.Options.UseTextOptions = true;
            this.LYDO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LYDO.Caption = "Lý Do";
            this.LYDO.FieldName = "LYDO";
            this.LYDO.Name = "LYDO";
            this.LYDO.OptionsColumn.AllowEdit = false;
            this.LYDO.OptionsColumn.AllowFocus = false;
            this.LYDO.OptionsColumn.FixedWidth = true;
            this.LYDO.Visible = true;
            this.LYDO.VisibleIndex = 7;
            this.LYDO.Width = 300;
            // 
            // KEMTHEO
            // 
            this.KEMTHEO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.KEMTHEO.AppearanceCell.Options.UseFont = true;
            this.KEMTHEO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KEMTHEO.AppearanceHeader.Options.UseFont = true;
            this.KEMTHEO.AppearanceHeader.Options.UseTextOptions = true;
            this.KEMTHEO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KEMTHEO.Caption = "Chứng từ";
            this.KEMTHEO.FieldName = "KEMTHEO";
            this.KEMTHEO.Name = "KEMTHEO";
            this.KEMTHEO.OptionsColumn.AllowEdit = false;
            this.KEMTHEO.OptionsColumn.AllowFocus = false;
            this.KEMTHEO.OptionsColumn.FixedWidth = true;
            this.KEMTHEO.Visible = true;
            this.KEMTHEO.VisibleIndex = 8;
            this.KEMTHEO.Width = 250;
            // 
            // SOTIEN
            // 
            this.SOTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOTIEN.AppearanceCell.Options.UseFont = true;
            this.SOTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOTIEN.AppearanceHeader.Options.UseFont = true;
            this.SOTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTIEN.Caption = "Số Tiền";
            this.SOTIEN.DisplayFormat.FormatString = "#,0";
            this.SOTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SOTIEN.FieldName = "SOTIEN";
            this.SOTIEN.MinWidth = 100;
            this.SOTIEN.Name = "SOTIEN";
            this.SOTIEN.OptionsColumn.AllowEdit = false;
            this.SOTIEN.OptionsColumn.AllowFocus = false;
            this.SOTIEN.OptionsColumn.FixedWidth = true;
            this.SOTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOTIEN", "{0:#,0}")});
            this.SOTIEN.Visible = true;
            this.SOTIEN.VisibleIndex = 9;
            this.SOTIEN.Width = 200;
            // 
            // PTTHANHTOAN
            // 
            this.PTTHANHTOAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.PTTHANHTOAN.AppearanceCell.Options.UseFont = true;
            this.PTTHANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PTTHANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.PTTHANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.PTTHANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PTTHANHTOAN.Caption = "HTTT";
            this.PTTHANHTOAN.FieldName = "PHUONGTHUCTT";
            this.PTTHANHTOAN.Name = "PTTHANHTOAN";
            this.PTTHANHTOAN.OptionsColumn.AllowEdit = false;
            this.PTTHANHTOAN.OptionsColumn.AllowFocus = false;
            this.PTTHANHTOAN.OptionsColumn.FixedWidth = true;
            this.PTTHANHTOAN.Visible = true;
            this.PTTHANHTOAN.VisibleIndex = 10;
            this.PTTHANHTOAN.Width = 130;
            // 
            // HOTENGIAODICH
            // 
            this.HOTENGIAODICH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.HOTENGIAODICH.AppearanceCell.Options.UseFont = true;
            this.HOTENGIAODICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HOTENGIAODICH.AppearanceHeader.Options.UseFont = true;
            this.HOTENGIAODICH.AppearanceHeader.Options.UseTextOptions = true;
            this.HOTENGIAODICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HOTENGIAODICH.Caption = "Người nhận";
            this.HOTENGIAODICH.FieldName = "HOTENGIAODICH";
            this.HOTENGIAODICH.Name = "HOTENGIAODICH";
            this.HOTENGIAODICH.OptionsColumn.AllowEdit = false;
            this.HOTENGIAODICH.OptionsColumn.AllowFocus = false;
            this.HOTENGIAODICH.OptionsColumn.FixedWidth = true;
            this.HOTENGIAODICH.Visible = true;
            this.HOTENGIAODICH.VisibleIndex = 11;
            this.HOTENGIAODICH.Width = 180;
            // 
            // NGAYTHANHTOAN
            // 
            this.NGAYTHANHTOAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYTHANHTOAN.AppearanceCell.Options.UseFont = true;
            this.NGAYTHANHTOAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTHANHTOAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTHANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTHANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYTHANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTHANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTHANHTOAN.Caption = "Ngày Thanh Toán";
            this.NGAYTHANHTOAN.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYTHANHTOAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTHANHTOAN.FieldName = "NGAYTHANHTOAN";
            this.NGAYTHANHTOAN.Name = "NGAYTHANHTOAN";
            this.NGAYTHANHTOAN.OptionsColumn.AllowEdit = false;
            this.NGAYTHANHTOAN.OptionsColumn.AllowFocus = false;
            this.NGAYTHANHTOAN.OptionsColumn.FixedWidth = true;
            this.NGAYTHANHTOAN.Visible = true;
            this.NGAYTHANHTOAN.VisibleIndex = 12;
            this.NGAYTHANHTOAN.Width = 150;
            // 
            // TENNGANHANGCHUYEN
            // 
            this.TENNGANHANGCHUYEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNGANHANGCHUYEN.AppearanceCell.Options.UseFont = true;
            this.TENNGANHANGCHUYEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNGANHANGCHUYEN.AppearanceHeader.Options.UseFont = true;
            this.TENNGANHANGCHUYEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNGANHANGCHUYEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNGANHANGCHUYEN.Caption = "Ngân Hàng Chuyển";
            this.TENNGANHANGCHUYEN.FieldName = "TENNGANHANGCHUYEN";
            this.TENNGANHANGCHUYEN.Name = "TENNGANHANGCHUYEN";
            this.TENNGANHANGCHUYEN.OptionsColumn.AllowEdit = false;
            this.TENNGANHANGCHUYEN.OptionsColumn.AllowFocus = false;
            this.TENNGANHANGCHUYEN.OptionsColumn.FixedWidth = true;
            this.TENNGANHANGCHUYEN.Visible = true;
            this.TENNGANHANGCHUYEN.VisibleIndex = 13;
            this.TENNGANHANGCHUYEN.Width = 250;
            // 
            // SOTAIKHOANCHUYEN
            // 
            this.SOTAIKHOANCHUYEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOTAIKHOANCHUYEN.AppearanceCell.Options.UseFont = true;
            this.SOTAIKHOANCHUYEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOTAIKHOANCHUYEN.AppearanceHeader.Options.UseFont = true;
            this.SOTAIKHOANCHUYEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTAIKHOANCHUYEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTAIKHOANCHUYEN.Caption = "Tài Khoản Chuyển";
            this.SOTAIKHOANCHUYEN.FieldName = "SOTAIKHOANCHUYEN";
            this.SOTAIKHOANCHUYEN.Name = "SOTAIKHOANCHUYEN";
            this.SOTAIKHOANCHUYEN.OptionsColumn.AllowEdit = false;
            this.SOTAIKHOANCHUYEN.OptionsColumn.AllowFocus = false;
            this.SOTAIKHOANCHUYEN.OptionsColumn.FixedWidth = true;
            this.SOTAIKHOANCHUYEN.Visible = true;
            this.SOTAIKHOANCHUYEN.VisibleIndex = 14;
            this.SOTAIKHOANCHUYEN.Width = 200;
            // 
            // TENNGANHANGNHAN
            // 
            this.TENNGANHANGNHAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNGANHANGNHAN.AppearanceCell.Options.UseFont = true;
            this.TENNGANHANGNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNGANHANGNHAN.AppearanceHeader.Options.UseFont = true;
            this.TENNGANHANGNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNGANHANGNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNGANHANGNHAN.Caption = "Ngân Hàng Nhận";
            this.TENNGANHANGNHAN.FieldName = "TENNGANHANGNHAN";
            this.TENNGANHANGNHAN.Name = "TENNGANHANGNHAN";
            this.TENNGANHANGNHAN.OptionsColumn.AllowEdit = false;
            this.TENNGANHANGNHAN.OptionsColumn.AllowFocus = false;
            this.TENNGANHANGNHAN.OptionsColumn.FixedWidth = true;
            this.TENNGANHANGNHAN.Visible = true;
            this.TENNGANHANGNHAN.VisibleIndex = 15;
            this.TENNGANHANGNHAN.Width = 250;
            // 
            // SOTAIKHOANNHAN
            // 
            this.SOTAIKHOANNHAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOTAIKHOANNHAN.AppearanceCell.Options.UseFont = true;
            this.SOTAIKHOANNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOTAIKHOANNHAN.AppearanceHeader.Options.UseFont = true;
            this.SOTAIKHOANNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTAIKHOANNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTAIKHOANNHAN.Caption = "Tài Khoản Nhận";
            this.SOTAIKHOANNHAN.FieldName = "SOTAIKHOANNHAN";
            this.SOTAIKHOANNHAN.Name = "SOTAIKHOANNHAN";
            this.SOTAIKHOANNHAN.OptionsColumn.AllowEdit = false;
            this.SOTAIKHOANNHAN.OptionsColumn.AllowFocus = false;
            this.SOTAIKHOANNHAN.OptionsColumn.FixedWidth = true;
            this.SOTAIKHOANNHAN.Visible = true;
            this.SOTAIKHOANNHAN.VisibleIndex = 16;
            this.SOTAIKHOANNHAN.Width = 180;
            // 
            // SOGIAODICH
            // 
            this.SOGIAODICH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOGIAODICH.AppearanceCell.Options.UseFont = true;
            this.SOGIAODICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOGIAODICH.AppearanceHeader.Options.UseFont = true;
            this.SOGIAODICH.AppearanceHeader.Options.UseTextOptions = true;
            this.SOGIAODICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOGIAODICH.Caption = "Số Giao Dịch";
            this.SOGIAODICH.FieldName = "SOGIAODICH";
            this.SOGIAODICH.Name = "SOGIAODICH";
            this.SOGIAODICH.OptionsColumn.AllowEdit = false;
            this.SOGIAODICH.OptionsColumn.AllowFocus = false;
            this.SOGIAODICH.OptionsColumn.FixedWidth = true;
            this.SOGIAODICH.Visible = true;
            this.SOGIAODICH.VisibleIndex = 17;
            this.SOGIAODICH.Width = 150;
            // 
            // TenNV
            // 
            this.TenNV.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TenNV.AppearanceCell.Options.UseFont = true;
            this.TenNV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TenNV.AppearanceHeader.Options.UseFont = true;
            this.TenNV.AppearanceHeader.Options.UseTextOptions = true;
            this.TenNV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenNV.Caption = "Người Lập";
            this.TenNV.FieldName = "TENNHANVIEN";
            this.TenNV.Name = "TenNV";
            this.TenNV.OptionsColumn.AllowEdit = false;
            this.TenNV.OptionsColumn.AllowFocus = false;
            this.TenNV.OptionsColumn.FixedWidth = true;
            this.TenNV.Visible = true;
            this.TenNV.VisibleIndex = 18;
            this.TenNV.Width = 220;
            // 
            // NGAYLAPPHIEU
            // 
            this.NGAYLAPPHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseFont = true;
            this.NGAYLAPPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYLAPPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYLAPPHIEU.Caption = "Ngày Lập";
            this.NGAYLAPPHIEU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYLAPPHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYLAPPHIEU.FieldName = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.Name = "NGAYLAPPHIEU";
            this.NGAYLAPPHIEU.OptionsColumn.AllowEdit = false;
            this.NGAYLAPPHIEU.OptionsColumn.AllowFocus = false;
            this.NGAYLAPPHIEU.OptionsColumn.FixedWidth = true;
            this.NGAYLAPPHIEU.Visible = true;
            this.NGAYLAPPHIEU.VisibleIndex = 19;
            this.NGAYLAPPHIEU.Width = 138;
            // 
            // Fill
            // 
            this.Fill.Name = "Fill";
            this.Fill.OptionsColumn.AllowEdit = false;
            this.Fill.OptionsColumn.AllowFocus = false;
            this.Fill.Visible = true;
            this.Fill.VisibleIndex = 20;
            this.Fill.Width = 20;
            // 
            // PK_MAPHIEUKHO
            // 
            this.PK_MAPHIEUKHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.PK_MAPHIEUKHO.AppearanceCell.Options.UseFont = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PK_MAPHIEUKHO.AppearanceHeader.Options.UseFont = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.PK_MAPHIEUKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PK_MAPHIEUKHO.Caption = "gridColumn1";
            this.PK_MAPHIEUKHO.FieldName = "PK_MAPHIEUKHO";
            this.PK_MAPHIEUKHO.Name = "PK_MAPHIEUKHO";
            this.PK_MAPHIEUKHO.OptionsColumn.AllowEdit = false;
            this.PK_MAPHIEUKHO.OptionsColumn.AllowFocus = false;
            this.PK_MAPHIEUKHO.OptionsColumn.FixedWidth = true;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DIACHI.AppearanceCell.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "gridColumn1";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            // 
            // Frm_BaoCaoChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 743);
            this.Controls.Add(this.gcDanhSach);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_BaoCaoChi";
            this.Text = "Danh sách phiếu chi";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_denngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_tungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NguonChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_LoaiThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_PhuongThucTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gl_NguoiLapPhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanhSach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.LabelControl lbNgayTu;
        private DevExpress.XtraEditors.LabelControl lbNgayDen;
        private DevExpress.XtraEditors.LabelControl lbl_title;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.GridLookUpEdit gl_NguoiLapPhieu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn MANHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraEditors.GridLookUpEdit gl_LoaiThu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.GridLookUpEdit gl_PhuongThucTT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.GridControl gcDanhSach;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn PK_MAPHIEUKHO1;
        private DevExpress.XtraGrid.Columns.GridColumn MAPCN;
        private DevExpress.XtraGrid.Columns.GridColumn HOTEN;
        private DevExpress.XtraGrid.Columns.GridColumn TenNV;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYLAPPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn TenLoai;
        private DevExpress.XtraGrid.Columns.GridColumn LYDO;
        private DevExpress.XtraGrid.Columns.GridColumn SOTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn PTTHANHTOAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTHANHTOAN;
        private DevExpress.XtraGrid.Columns.GridColumn PK_MAPHIEUKHO;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn HOTENGIAODICH;
        private DevExpress.XtraGrid.Columns.GridColumn TENNGANHANGCHUYEN;
        private DevExpress.XtraGrid.Columns.GridColumn SOTAIKHOANCHUYEN;
        private DevExpress.XtraGrid.Columns.GridColumn TENNGANHANGNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn SOTAIKHOANNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn SOGIAODICH;
        private DevExpress.XtraEditors.SimpleButton bntInLai;
        private DevExpress.XtraEditors.GridLookUpEdit gl_NguonChi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn NGUONCHI;
        private DevExpress.XtraEditors.DateEdit date_tungay;
        private DevExpress.XtraEditors.DateEdit date_denngay;
        private DevExpress.XtraGrid.Columns.GridColumn MAPDNDC;
        private DevExpress.XtraGrid.Columns.GridColumn Fill;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraGrid.Columns.GridColumn KEMTHEO;



    }
}