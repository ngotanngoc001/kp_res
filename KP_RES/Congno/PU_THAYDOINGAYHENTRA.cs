﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class PU_THAYDOINGAYHENTRA : DevExpress.XtraEditors.XtraForm
    {
        string _maPhieu;
        string __ngayhentra;
        DateTime _ngayhentra;
        public PU_THAYDOINGAYHENTRA(string maphieu, string ngayhentra)
        {
            InitializeComponent();
            date_NgayHenTra.EditValue = null;
            _maPhieu = maphieu;
            __ngayhentra = ngayhentra;
            date_NgayHenTra.EditValue = Convert.ToDateTime(__ngayhentra);
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            try
            {
                if (date_NgayHenTra.EditValue == null)
                {
                    date_NgayHenTra.Focus();
                    throw new Exception("mời nhập ngày hẹn trả!");
                }
                string sqlUpdate = "";
                DataTable dtCheckMaPhieu = clsMain.ReturnDataTable("select MA_HOADON from CHITIETTHANHTOAN where MA_HOADON='" + _maPhieu + "'");
                if (dtCheckMaPhieu.Rows.Count == 0)
                {
                    string sql = "select MA from KHO_PHIEUKHO where SOPHIEU='" + _maPhieu + "'";

                    if (__ngayhentra != "")//nếu ngày có ngày hẹn trả
                    {
                        _ngayhentra = Convert.ToDateTime(__ngayhentra);
                        if (int.Parse(string.Format("{0:yyyyMMdd}", _ngayhentra)) > int.Parse(string.Format("{0:yyyyMMdd}", KP_UserManagement.clsMain.GetServerDate())))//nếu chưa trễ hẹn
                        {
                            sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "',NGAYTREHEN='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "',ISTREHEN='false' where PK_PHIEUKHOID='" + clsMain.ReturnDataTable(sql).Rows[0][0].ToString() + "' and NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "' and DOTTHANHTOAN!=N'Tạm'";
                        }
                        else//trễ hẹn
                        {
                            sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "',NGAYTREHEN='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "',ISTREHEN='True' where PK_PHIEUKHOID='" + clsMain.ReturnDataTable(sql).Rows[0][0].ToString() + "' and NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "' and DOTTHANHTOAN!=N'Tạm'";
                        }
                    }
                    else//else ko có ngày hẹn tra
                    {
                        sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "' where PK_PHIEUKHOID='" + clsMain.ReturnDataTable(sql).Rows[0][0].ToString() + "' and CTTT_STT=(select top(1)CTTT_STT from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + clsMain.ReturnDataTable(sql).Rows[0][0].ToString() + "' order by CTTT_STT desc)";
                    }
                }
                else//với hóa đơn
                {
                    //string sql = "select PK_PHIEUKHOID from KHO_PHIEUKHO where PK_MAPHIEUKHO='" + _maPhieu + "'";

                    if (__ngayhentra != "")//nếu ngày có ngày hẹn trả
                    {
                        _ngayhentra = Convert.ToDateTime(__ngayhentra);
                        if (int.Parse(string.Format("{0:yyyyMMdd}", _ngayhentra)) > int.Parse(string.Format("{0:yyyyMMdd}", KP_UserManagement.clsMain.GetServerDate())))//nếu chưa trễ hẹn
                        {
                            sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "',NGAYTREHEN='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "',ISTREHEN='false' where MA_HOADON='" + _maPhieu + "' and NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "' and DOTTHANHTOAN!=N'Tạm'";
                        }
                        else//trễ hẹn
                        {
                            sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "',NGAYTREHEN='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "',ISTREHEN='True' where MA_HOADON='" + _maPhieu + "' and NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", _ngayhentra) + "' and DOTTHANHTOAN!=N'Tạm'";
                        }
                    }
                    else//else ko có ngày hẹn tra
                    {
                        sqlUpdate = "update CHITIETTHANHTOAN set NGAYHENTRA='" + string.Format("{0:dd/MM/yyyy}", date_NgayHenTra.EditValue) + "' where MA_HOADON='" + _maPhieu + "' and CTTT_STT=(select top(1)CTTT_STT from CHITIETTHANHTOAN MA_HOADON='" + _maPhieu + "' order by CTTT_STT desc)";
                    }
                }
                
                if (clsMain.ExecuteSQL(sqlUpdate) )
                {
                    XtraMessageBox.Show("Cập nhật ngày hẹn trả thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void date_NgayHenTra_EditValueChanged(object sender, EventArgs e)
        {
            if (int.Parse(string.Format("{0:yyyyMMdd}", date_NgayHenTra.DateTime)) < int.Parse(string.Format("{0:yyyyMMdd}", KP_UserManagement.clsMain.GetServerDate())))
            {
                XtraMessageBox.Show("Ngày Hẹn Trả không thể nhỏ hơn hiện tại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                date_NgayHenTra.EditValue = KP_UserManagement.clsMain.GetServerDate().Date;
            }
        }

        private void bnt_dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}