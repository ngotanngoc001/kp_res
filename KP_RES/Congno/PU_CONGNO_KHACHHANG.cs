﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using KP_UserManagement;
using KP_Report;

namespace KP_RES
{
    public partial class PU_CONGNO_KHACHHANG : DevExpress.XtraEditors.XtraForm
    {
        bool flag;
        DateTime _tuNgay, _denNgay;
        string _noConLai, _maNCC;
        public PU_CONGNO_KHACHHANG(string maKH,string noHT,DateTime tuNgay,DateTime denNgay,bool _flag)
        {
            InitializeComponent();
            flag = _flag;
            _tuNgay = tuNgay;
            _denNgay = denNgay;
            _noConLai = noHT;
            _maNCC = maKH;
            if (_flag == false)
            {
                lbNgay.Visible = true;
                lbNgay.Text = "Từ Ngày: " +string.Format("{0:dd/MM/yyyy}", tuNgay) + " Đến Ngày: " + string.Format("{0:dd/MM/yyyy}",denNgay);
            }
            else
            {
                lbNgay.Visible = false;
            }
            LoadData(maKH,noHT,tuNgay,denNgay,_flag);
            gridView1.FocusedRowHandle = 0;
            gridView1.OptionsView.RowAutoHeight = true;
            RepositoryItemMemoEdit memo = new RepositoryItemMemoEdit();
            memo.AutoHeight=true;
            gridView1.Columns["ChiTietThanhToan"].ColumnEdit = memo;
            title.Text = this.Text;
        }
   
        private void LoadData(string maNCC, string noConLai, DateTime tuNgay, DateTime denNgay, bool _flag)
        {
            try
            {
                DataTable dt = clsMain.ReturnDataTable("select TEN,DIACHI,DIENTHOAI from NHACUNGCAP where MA='" + maNCC + "'");//lấy thông tin nhà cung cấp
                lbKH.Text = dt.Rows[0]["TEN"].ToString();
                lbDiaChi.Text = dt.Rows[0]["DIACHI"].ToString();
                lbSDT.Text = dt.Rows[0]["DIENTHOAI"].ToString();

                if (_flag)//Nếu đúng liệt kê tất cả
                {
                    string sql = "(select  NHACUNGCAP  as DT_NCC_KH,pk.MA,pk.SOPHIEU as MAPHIEU,pk.NGAYTAO as NGAYTAO,pk.TONGTIEN as TONGTIEN,pk.TONGTIEN as TienNo from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID  where ct.THU_CHI='true' and NHACUNGCAP='" + maNCC + "' and ct.DOTTHANHTOAN='0')  union (select hd.MA_KHACHHANG as DT_NCC_KH ,ct.PK_PHIEUKHOID,ct.MA_HOADON as MAPHIEU,ct.NGAYTHANHTOAN as NGAYTAO,ct.TONGTIENCANTT as TONGTIEN,ct.TONGTIENCANTT as TienNo from HOADON hd inner join CHITIETTHANHTOAN ct on hd.Ma_HoaDon=ct.MA_HOADON  where ct.THU_CHI='true' and hd.MA_KHACHHANG='" + maNCC + "' and ct.DOTTHANHTOAN='0') order by NGAYTAO";
                    DataTable dtAll = clsMain.ReturnDataTable(sql);//lấy tất cả các phiếu nhập kho(và hóa đơn) của NCC và tổng tiền,tiền nợ của ncc đó
                    dtAll.Columns.Add("DaThanhToan", typeof(decimal));
                    dtAll.Columns.Add("NoConLai", typeof(decimal));
                    dtAll.Columns.Add("ChiTietThanhToan", typeof(string));
                    dtAll.Columns.Add("HanThanhToan", typeof(DateTime));
                    if (dtAll.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtAll.Rows.Count; i++)//duyệt qua từng phiếu nhập kho và hóa đơn
                        {
                            //lấy số tiền nợ còn lại của ncc
                            DataTable dtSoTienConLai = new DataTable();
                            if (dtAll.Rows[i]["MA"].ToString().Trim() != "0")
                            {
                                dtSoTienConLai = clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where THU_CHI='true' and  PK_PHIEUKHOID='" + dtAll.Rows[i]["MA"].ToString() + "' order by CTTT_STT DESC");
                            }
                            else
                            {
                                dtSoTienConLai = clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where THU_CHI='true' and  Ma_HOADON='" + dtAll.Rows[i]["MAPHIEU"].ToString() + "' order by CTTT_STT DESC");
                            }
                            if (dtSoTienConLai.Rows.Count > 0)//nêu có dữ liệu,có nợ
                            {
                                dtAll.Rows[i]["NoConLai"] = dtSoTienConLai.Rows[0][0].ToString();//gắn giá trị cho nợ còn lại
                            }
                            else
                            {
                                dtAll.Rows[i]["NoConLai"] = "0";
                            }

                            dtAll.Rows[i]["DaThanhToan"] = decimal.Parse(dtAll.Rows[i]["TienNo"].ToString()) - decimal.Parse(dtAll.Rows[i]["NoConLai"].ToString());
                            string sql1 = "";
                            if (dtAll.Rows[i]["MA"].ToString().Trim() != "0")
                            {
                                sql1 = "select ct.SOTIENTHANHTOAN,ct.NGAYTHANHTOAN,ct.DOTTHANHTOAN from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on ct.PK_PHIEUKHOID=pk.MA	 where    pk.MA='" + dtAll.Rows[i]["MA"].ToString() + "' and THU_CHI='true'";
                            }
                            else
                            {

                                sql1 = "select ct.SOTIENTHANHTOAN,ct.NGAYTHANHTOAN,ct.DOTTHANHTOAN from HOADON hd inner join CHITIETTHANHTOAN ct on ct.MA_HOADON=hd.Ma_HoaDon	 where   ct.MA_HOADON='" + dtAll.Rows[i]["MAPHIEU"].ToString() + "' and THU_CHI='true'";
                            }

                            DataTable dtChiTiet = clsMain.ReturnDataTable(sql1);//lấy danh sách các thanh toán trả nợ
                            if (dtChiTiet.Rows.Count > 0)
                            {
                                int tang = 1;
                                for (int j = 0; j < dtChiTiet.Rows.Count; j++)
                                {

                                    if (decimal.Parse(dtChiTiet.Rows[j][0].ToString()) == 0)
                                    {
                                        tang = 0;
                                        continue;
                                    }
                                    string[] stChiTetThanhToan = dtChiTiet.Rows[j]["DOTTHANHTOAN"].ToString().Split('-');
                                    int solan = int.Parse(stChiTetThanhToan[0]) + tang;
                                    string gacon = "";//có ghi chú thì gắn ghi chú
                                    if (stChiTetThanhToan.Length > 1)
                                    {
                                        gacon = "- " + stChiTetThanhToan[1];
                                    }
                                    dtAll.Rows[i]["ChiTietThanhToan"] += "Lần: " + solan.ToString() + "   -Ngày:   " + string.Format("{0,10:dd/MM/yyyy}", dtChiTiet.Rows[j][1]) + "  -Số tiền Trả:  " + string.Format("{0,14} ", string.Format("{0:#,0}", decimal.Parse(dtChiTiet.Rows[j][0].ToString()))) + gacon + "\n";
                                }
                            }
                            if (decimal.Parse(dtAll.Rows[i]["NoConLai"].ToString()) != 0)
                            {
                                if (dtAll.Rows[i]["MA"].ToString().Trim() != "0")
                                {
                                    dtAll.Rows[i]["HanThanhToan"] = clsMain.ReturnDataTable("select top 1 NGAYHENTRA from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dtAll.Rows[i]["MA"].ToString() + "'order by CTTT_STT desc").Rows[0][0];
                                }
                                else
                                {
                                    dtAll.Rows[i]["HanThanhToan"] = clsMain.ReturnDataTable("select top 1 NGAYHENTRA from CHITIETTHANHTOAN where MA_HOADON='" + dtAll.Rows[i]["MAPHIEU"].ToString() + "'order by CTTT_STT desc").Rows[0][0];
                                }
                            }

                        }
                    }

                    gcChiTiet.DataSource = dtAll;
                }
                else//nếu liệt kê theo ngày
                {
                    DataTable dtNgay = new DataTable();
                    string check = "(select  pk.NHACUNGCAP as DT_NCC_KH,pk.MA as PK_PHIEUKHOID,pk.SOPHIEU as MAPHIEU,pk.NGAYTAO as NGAYTAO,pk.TONGTIEN as TONGTIEN,pk.TONGTIEN as TienNo from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on pk.MA=ct.PK_PHIEUKHOID  where ct.THU_CHI='true' and pk.NHACUNGCAP='" + maNCC + "' and ct.DOTTHANHTOAN='0' and CONVERT(varchar,ct.NGAYTHANHTOAN,112) between '" + string.Format("{0:yyyyMMdd}", tuNgay) + "' and '" + string.Format("{0:yyyyMMdd}", denNgay) + "')  union (select hd.MA_KHACHHANG as DT_NCC_KH ,ct.PK_PHIEUKHOID,ct.MA_HOADON as MAPHIEU,ct.NGAYTHANHTOAN as NGAYTAO,ct.TONGTIENCANTT as TONGTIEN,ct.TONGTIENCANTT as TienNo from HOADON hd inner join CHITIETTHANHTOAN ct on hd.MA_HOADON=ct.MA_HOADON  where ct.THU_CHI='true' and hd.MA_KHACHHANG='" + maNCC + "' and ct.DOTTHANHTOAN='0' and CONVERT(varchar,ct.NGAYTHANHTOAN,112) between '" + string.Format("{0:yyyyMMdd}", tuNgay) + "' and '" + string.Format("{0:yyyyMMdd}", denNgay) + "') order by NGAYTAO";
                    dtNgay = clsMain.ReturnDataTable(check);//chọn ra phiếu kho và hóa đơn có mua hàng trong khoảng thời gian đó
                    dtNgay.Columns.Add("DaThanhToan", typeof(decimal));
                    dtNgay.Columns.Add("NoConLai", typeof(decimal));
                    dtNgay.Columns.Add("ChiTietThanhToan", typeof(string));
                    dtNgay.Columns.Add("HanThanhToan", typeof(DateTime));
                    if (dtNgay.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNgay.Rows.Count; i++)//duyệt qua từng phiếu nhập kho và hóa đơn
                        {
                            //lấy số tiền nợ còn lại của ncc
                            DataTable dtSoTienConLai = new DataTable();
                            if (dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString().Trim() != "0")
                            {
                                dtSoTienConLai = clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where THU_CHI='true' and  PK_PHIEUKHOID='" + dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString() + "' order by CTTT_STT DESC");
                            }
                            else
                            {
                                dtSoTienConLai = clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where THU_CHI='true' and  Ma_HOADON='" + dtNgay.Rows[i]["MAPHIEU"].ToString() + "' order by CTTT_STT DESC");
                            }
                            if (dtSoTienConLai.Rows.Count > 0)//nêu có dữ liệu,có nợ
                            {
                                dtNgay.Rows[i]["NoConLai"] = dtSoTienConLai.Rows[0][0].ToString();//gắn giá trị cho nợ còn lại
                            }
                            else
                            {
                                dtNgay.Rows[i]["NoConLai"] = "0";
                            }
                            dtNgay.Rows[i]["DaThanhToan"] = decimal.Parse(dtNgay.Rows[i]["TienNo"].ToString()) - decimal.Parse(dtNgay.Rows[i]["NoConLai"].ToString());
                            string sql1 = "";
                            if (dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString().Trim() != "0")
                            {
                                sql1 = "select ct.SOTIENTHANHTOAN,ct.NGAYTHANHTOAN,ct.DOTTHANHTOAN from KHO_PHIEUKHO pk inner join CHITIETTHANHTOAN ct on ct.PK_PHIEUKHOID=pk.MA	 where    pk.MA='" + dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString() + "' and THU_CHI='true'";
                            }
                            else
                            {

                                sql1 = "select ct.SOTIENTHANHTOAN,ct.NGAYTHANHTOAN,ct.DOTTHANHTOAN from HOADON hd inner join CHITIETTHANHTOAN ct on ct.MA_HOADON=hd.MA_HOADON	 where   ct.MA_HOADON='" + dtNgay.Rows[i]["MAPHIEU"].ToString() + "' and THU_CHI='true'";
                            }
                            DataTable dtChiTiet = clsMain.ReturnDataTable(sql1);//lấy danh sách các thanh toán trả nợ

                            if (dtChiTiet.Rows.Count > 0)
                            {
                                int tang = 1;
                                for (int j = 0; j < dtChiTiet.Rows.Count; j++)
                                {

                                    if (decimal.Parse(dtChiTiet.Rows[j][0].ToString()) == 0)
                                    {
                                        tang = 0;
                                        continue;
                                    }
                                    string[] stChiTetThanhToan = dtChiTiet.Rows[j]["DOTTHANHTOAN"].ToString().Split('-');
                                    int solan = int.Parse(stChiTetThanhToan[0]) + tang;
                                    string gacon = "";//có ghi chú thì gắn ghi chú
                                    if (stChiTetThanhToan.Length > 1)
                                    {
                                        gacon = "- " + stChiTetThanhToan[1];
                                    }
                                    dtNgay.Rows[i]["ChiTietThanhToan"] += "Lần: " + solan.ToString() + "   -Ngày:   " + string.Format("{0,10:dd/MM/yyyy}", dtChiTiet.Rows[j][1]) + "  -Số tiền Trả:  " + string.Format("{0,14} ", string.Format("{0:#,0}", decimal.Parse(dtChiTiet.Rows[j][0].ToString()))) + gacon + "\n";
                                }
                                if (decimal.Parse(dtNgay.Rows[i]["NoConLai"].ToString()) != 0)
                                {
                                    if (dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString().Trim() != "0")
                                    {
                                        dtNgay.Rows[i]["HanThanhToan"] = clsMain.ReturnDataTable("select top 1 NGAYHENTRA from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + dtNgay.Rows[i]["PK_PHIEUKHOID"].ToString() + "'order by CTTT_STT desc").Rows[0][0];
                                    }
                                    else
                                    {
                                        dtNgay.Rows[i]["HanThanhToan"] = clsMain.ReturnDataTable("select top 1 NGAYHENTRA from CHITIETTHANHTOAN where MA_HOADON='" + dtNgay.Rows[i]["MAPHIEU"].ToString() + "'order by CTTT_STT desc").Rows[0][0];
                                    }
                                }
                            }
                        }
                        gcChiTiet.DataSource = dtNgay;
                    }

                }
                decimal tongno = 0;
                if (gridView1.RowCount > 0)
                {
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        gridView1.FocusedRowHandle = -1;
                        gridView1.FocusedRowHandle = i;
                        tongno += decimal.Parse(gridView1.GetFocusedRowCellValue("NoConLai").ToString());
                    }
                }
                lbNo.Text = string.Format("{0:#,0}", tongno) + " VND";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

      
        private void PU_CONGNO_KHACHHANG_SizeChanged(object sender, EventArgs e)
        {
            //lấy kích thước màn hình
            
            lbChiTiet.Location = new Point(this.Size.Width / 2 - lbChiTiet.Width / 2, panelControl3.Size.Height / 2 - lbChiTiet.Height / 2);
            lbNgay.Location=new Point(this.Size.Width / 2 - lbNgay.Width / 2, panelControl3.Size.Height / 2 + lbChiTiet.Height / 2);
        }

        private void bnt_In_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0)
                    throw new Exception("Không có dữ liệu");
                DataTable dtIn = (DataTable)gcChiTiet.DataSource;
                if (dtIn.Columns.Contains("Ngay") == false)
                {
                    dtIn.Columns.Add("Ngay", typeof(string));
                    dtIn.Columns.Add("Thang", typeof(string));
                    dtIn.Columns.Add("TuNgay", typeof(string));
                    dtIn.Columns.Add("DenNgay", typeof(string));
                    dtIn.Columns.Add("Nam", typeof(string));
                    dtIn.Columns.Add("NguoiLap", typeof(string));
                    dtIn.Columns.Add("NhaCungCap", typeof(string));
                    dtIn.Columns.Add("SDT", typeof(string));
                    dtIn.Columns.Add("DiaChi", typeof(string));
                    dtIn.Columns.Add("SoNoHienTai", typeof(string));
                }
                dtIn.Rows[0]["Ngay"] = KP_UserManagement.clsMain.GetServerDate().Day;
                dtIn.Rows[0]["Thang"] = KP_UserManagement.clsMain.GetServerDate().Month;
                dtIn.Rows[0]["Nam"] = KP_UserManagement.clsMain.GetServerDate().Year;
                DataTable dt = clsMain.ReturnDataTable("select TEN,DIENTHOAI,DIACHI from NHACUNGCAP where MA='" + _maNCC + "'");//lấy thông tin nhà cung cấp
                dtIn.Rows[0]["NhaCungCap"] = dt.Rows[0]["TEN"].ToString();
                dtIn.Rows[0]["SDT"] = dt.Rows[0]["DIENTHOAI"].ToString();
                dtIn.Rows[0]["DiaChi"] = dt.Rows[0]["DIACHI"].ToString();
                dtIn.Rows[0]["SoNoHienTai"] = _noConLai;
                if (!flag)
                {

                    dtIn.Rows[0]["TuNgay"] = _tuNgay.ToShortDateString();
                    dtIn.Rows[0]["DenNgay"] = _denNgay.ToShortDateString();
                }
                
                string sql1 = string.Format("select TENNHANVIEN from DM_NHANVIEN where MANHANVIEN='{0}'", clsUserManagement.ReturnMaNVOfUserLogin());
                dtIn.Rows[0]["NguoiLap"] = clsMain.ReturnDataTable(sql1).Rows[0][0].ToString();
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dtIn;
                frm.Mode = 24;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bntLapPhieuChi_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Bạn hãy chọn phiếu cần thanh toán");
                }
                else
                {
                    if (decimal.Parse( gridView1.GetFocusedRowCellValue(NoConLai).ToString()) ==0)
                    {
                        throw new Exception("Phiếu  này đã thanh toán xong");
                    }
                    else
                    {
                        int vitri = gridView1.FocusedRowHandle;
                        if (decimal.Parse(gridView1.GetFocusedRowCellValue(NoConLai).ToString()) > 0)
                        {
                            Frm_LapPhieuThu frm = new Frm_LapPhieuThu(gridView1.GetFocusedRowCellValue(DT_NCC_KH).ToString(), gridView1.GetFocusedRowCellValue(MAPHIEU).ToString(), true);
                            frm.WindowState = FormWindowState.Maximized;
                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            frm.ShowDialog();
                            frm.Dispose();
                            if (frm.ActiveMdiChild == null)
                            LoadData(_maNCC, _noConLai, _tuNgay, _denNgay, flag);
                            
                        }
                        else
                        {
                        
                            Frm_LapPhieuChi frm = new Frm_LapPhieuChi(gridView1.GetFocusedRowCellValue(DT_NCC_KH).ToString(), gridView1.GetFocusedRowCellValue(MAPHIEU).ToString(), 2);
                            frm.WindowState = FormWindowState.Maximized;
                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            frm.ShowDialog();
                            frm.Dispose();
                            if (frm.ActiveMdiChild == null)
                                LoadData(_maNCC, _noConLai, _tuNgay, _denNgay, flag);
                        }
                        gridView1.FocusedRowHandle = vitri;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        
        }

        private void bnt_XemChiTiet_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                string maPhieuKhoID = "";
                string maPhieu=gridView1.GetFocusedRowCellValue(MAPHIEU).ToString();
                if(maPhieu.Contains("B"))
                {
                    maPhieuKhoID = maPhieu;
                }
                else
                {
                    maPhieuKhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + maPhieu + "'").Rows[0][0].ToString();
                }
                

                Frm_ChiTiet_Xuatkho frm = new Frm_ChiTiet_Xuatkho(maPhieuKhoID);
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.ShowDialog();
                frm.Dispose();
            }
            else
                XtraMessageBox.Show("Chưa chọn phiếu cần xem", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bnt_dong_Click(object sender, EventArgs e)
        {

        }

        private void bnt_updateNgayThanhToan_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Bạn hãy chọn phiếu  cần thay đổi ngày hẹn trả");
                }
                else
                {
                    if (gridView1.GetFocusedRowCellValue(NoConLai).ToString() == "0.0000")
                    {
                        throw new Exception("Phiếu  này đã thanh toán xong");
                    }
                    else
                    {
                        int vitri = gridView1.FocusedRowHandle;
                       PU_THAYDOINGAYHENTRA frm = new PU_THAYDOINGAYHENTRA(gridView1.GetFocusedRowCellValue(MAPHIEU).ToString(), gridView1.GetFocusedRowCellValue(HanThanhToan).ToString());
                        frm.ShowDialog();
                        if (frm.ActiveMdiChild == null)
                            LoadData(_maNCC, _noConLai, _tuNgay, _denNgay, flag);
                        gridView1.FocusedRowHandle = vitri;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bnt_XemTruocKhiIn_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                DataTable dtPhieuKhoID = clsMain.ReturnDataTable("select MA from KHO_PHIEUKHO where SOPHIEU='" + gridView1.GetFocusedRowCellValue(MAPHIEU).ToString() + "'");
                if (dtPhieuKhoID.Rows.Count > 0)
                {
                    string maPhieuKhoID = dtPhieuKhoID.Rows[0][0].ToString();
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.ma = maPhieuKhoID;
                    frm.Mode = 1;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    Frm_ChiTiet_Xuatkho frm = new Frm_ChiTiet_Xuatkho(gridView1.GetFocusedRowCellValue(MAPHIEU).ToString());
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                
            }
            else
                XtraMessageBox.Show("Chưa chọn phiếu cần xem", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLapphieuchi_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    throw new Exception("Bạn hãy chọn phiếu  cần chi");
                }
                else
                {
                    int vitri = gridView1.FocusedRowHandle;
                    if (decimal.Parse(gridView1.GetFocusedRowCellValue(NoConLai).ToString()) == decimal.Parse(gridView1.GetFocusedRowCellValue(TienNo).ToString()))
                    {
                        throw new Exception("Phiếu  này chưa thu tiền nên không lập phiếu chi được");
                    }
                    else
                    {
                        Frm_LapPhieuChi frm = new Frm_LapPhieuChi(gridView1.GetFocusedRowCellValue(DT_NCC_KH).ToString(), gridView1.GetFocusedRowCellValue(MAPHIEU).ToString(), 3);
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.ShowDialog();
                        frm.Dispose();
                        if (frm.ActiveMdiChild == null)
                            LoadData(_maNCC, _noConLai, _tuNgay, _denNgay, flag);
                         
                    }
                    gridView1.FocusedRowHandle = vitri;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   
    }
}