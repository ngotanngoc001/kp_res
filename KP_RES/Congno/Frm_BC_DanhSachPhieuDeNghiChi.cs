﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_BC_DanhSachPhieuDeNghiChi : DevExpress.XtraEditors.XtraForm
    {
        
        public Frm_BC_DanhSachPhieuDeNghiChi()
        {
            InitializeComponent();
            header.Visible = false;
            
        }
        private void LoadData()
        {
            string sSQL = "";
            if (optGroup.SelectedIndex == 1)
                sSQL += string.Format("EXEC SP_BC_DSPHIEUDENGHIDUYETCHI @NgayTu='{0}',@NgayDen='{1}',@NguoiLapPhieu='{2}',@TRANGTHAI='{3}',@TYPE='{4}'", "", "", gl_NguoiLapPhieu.EditValue.ToString(), "", "1");
            else if(optGroup.SelectedIndex == 2)
                sSQL += string.Format("EXEC SP_BC_DSPHIEUDENGHIDUYETCHI @NgayTu='{0}',@NgayDen='{1}',@NguoiLapPhieu='{2}',@TRANGTHAI='{3}',@TYPE='{4}'", "", "", "", gl_TT.EditValue.ToString(), "2");
            else if (optGroup.SelectedIndex == 3)
                sSQL += string.Format("EXEC SP_BC_DSPHIEUDENGHIDUYETCHI @NgayTu='{0}',@NgayDen='{1}',@NguoiLapPhieu='{2}',@TRANGTHAI='{3}',@TYPE='{4}'", string.Format("{0:yyyyMMdd}", date_tungay.EditValue), string.Format("{0:yyyyMMdd}", date_denngay.EditValue) + " 23:59:59", "", "", "3");
            else
                sSQL += string.Format("EXEC SP_BC_DSPHIEUDENGHIDUYETCHI @NgayTu='{0}',@NgayDen='{1}',@NguoiLapPhieu='{2}',@TRANGTHAI='{3}',@TYPE='{4}'", "", "", "", "", "0");
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TRANGTHAI"].ToString() == "0")
                    dr["TRANGTHAI1"] = "Chưa duyệt";
                else if (dr["TRANGTHAI"].ToString() == "1")
                    dr["TRANGTHAI1"] = "Đã duyệt";
                else
                    dr["TRANGTHAI1"] = "Không duyệt";
            }
            gvcShowCongNoNCC.DataSource = dt;
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                //FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gvcShowCongNoNCC.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gvcShowCongNoNCC.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                //FILL.Visible = true;
            }
        }


        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

      
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {

        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvcShowCongNoNCC.DataSource = null;
            if (optGroup.SelectedIndex == 0)
            {
                pnChoose.Visible = false;
            }
            else
            {
                pnChoose.Visible = true;
            }
            if (optGroup.SelectedIndex == 0)
            {
                lbl_title.Visible = false;
                lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;
                gl_NguoiLapPhieu.Visible = gl_TT.Visible = false;

            }
            else
                if (optGroup.SelectedIndex == 3)
                {
                    date_tungay.EditValue = date_denngay.EditValue = clsMain.GetServerDate();
                    lbl_title.Visible = false;
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = true;
                    gl_NguoiLapPhieu.Visible = gl_TT.Visible = false;
                }
                else
                {
                    if (optGroup.SelectedIndex == 1)
                    {
                        lbl_title.Text = "Chọn Người Lập Phiếu:";
                        gl_NguoiLapPhieu.EditValue = "";
                        gl_NguoiLapPhieu.Visible = true;
                        gl_TT.Visible = false;
                        gl_NguoiLapPhieu.Properties.DataSource = clsMain.ReturnDataTable("select MANHANVIEN,TENNHANVIEN from DM_NHANVIEN");

                    }
                    else
                        if (optGroup.SelectedIndex == 2)
                        {
                            lbl_title.Text = "Chọn Trạng thái:";
                            gl_TT.Visible = true;
                            gl_TT.EditValue = "";
                            gl_NguoiLapPhieu.Visible = false;
                            DataTable dt1 = new DataTable();
                            dt1.Columns.Add("MA");
                            dt1.Columns.Add("TEN");
                            dt1.Rows.Add("0", "Chưa Duyệt");
                            dt1.Rows.Add("1", "Đã Duyệt");
                            dt1.Rows.Add("2", "Không Duyệt");
                            gl_TT.Properties.DataSource = dt1;
                        }
                    lbl_title.Visible = true;
                    lbNgayTu.Visible = lbNgayDen.Visible = date_denngay.Visible = date_tungay.Visible = false;

                }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl6.Width == 223)
            {
                panelControl6.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl10.Visible = false;
            }
            else
            {
                panelControl6.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true;
                panelControl10.Visible = true;
            }
            this.Refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataTable myDT = ((DataTable)gvcShowCongNoNCC.DataSource).Copy();

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else if (optGroup.SelectedIndex == 1)
                        Description = "( " + Description + " : " + gl_NguoiLapPhieu.Text + " )";
                    else if (optGroup.SelectedIndex == 2)
                        Description = "( " + Description + " : " + gl_TT.Text + " )";
                    else if (optGroup.SelectedIndex == 3)
                        Description = "( " + Description + " - Từ ngày: " + date_tungay.Text + " - Đến ngày: " + date_denngay.Text + " )";

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;
                    myDT.Columns.Add(All);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 87;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    DataTable myDT = clsMain.ReturnDataTable("EXEC SP_INPHIEUDENGHIDUYETCHI " + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MAPDNDC).ToString()));
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 120;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa chọn phiếu cần in");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    
    }
}