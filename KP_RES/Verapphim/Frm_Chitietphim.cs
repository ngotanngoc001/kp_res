﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.Metro.Navigation;
using DevExpress.XtraEditors.Controls;

namespace KP_RES 
{
    public partial class Frm_Chitietphim : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Chitietphim()
        {
            InitializeComponent();
            loadCombo();
        }

        public  string sma = "";
     
        public void loadCombo()
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
           
            dt = clsMain.ReturnDataTable("select ID as MA,VNValue as TEN from ReferenceList where category='Country' and status=1");
            dt1.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                dt1.Rows.Add(dr["MA"].ToString(), dr["TEN"].ToString());
            }
            txtCountry.Properties.DataSource = dt1.Copy ();
            txtCountry.EditValue = txtCountry.Properties.GetDataSourceValue(txtCountry.Properties.ValueMember, 0);

            dt = clsMain.ReturnDataTable("select ID as MA,VNValue as TEN from ReferenceList where category='MovieType' and status=1");
            dt1.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                dt1.Rows.Add(dr["MA"].ToString(), dr["TEN"].ToString());
            }
            txtType.Properties.DataSource = dt1.Copy();
            txtType.EditValue = txtType.Properties.GetDataSourceValue(txtType.Properties.ValueMember, 0);

            dt = clsMain.ReturnDataTable("select ID as MA,VNValue as TEN from ReferenceList where category='MovieTechnology' and status=1");
            dt1.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                dt1.Rows.Add(dr["MA"].ToString(), dr["TEN"].ToString());
            }
            txtTechnology.Properties.DataSource = dt1.Copy();
            txtTechnology.EditValue = txtType.Properties.GetDataSourceValue(txtTechnology.Properties.ValueMember, 0);

            dt = clsMain.ReturnDataTable("select ID as MA,VNName as TEN from Cinemas where status=2");
            dt1.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                dt1.Rows.Add(dr["MA"].ToString(), dr["TEN"].ToString());
            }
            txtCinemas.Properties.DataSource = dt1.Copy();
            txtCinemas.EditValue = txtCinemas.Properties.GetDataSourceValue(txtCinemas.Properties.ValueMember, 0);
            dt1.Clear();
            dt1.Rows.Add("0", "Không hiển thị");
            dt1.Rows.Add("1", "Phim sắp chiếu");
            dt1.Rows.Add("2", "Phim đang chiếu");
            dt1.Rows.Add("3", "Suất chiếu đặt biệt");
            txtStatus.Properties.DataSource = dt1.Copy();
            txtStatus.EditValue = txtStatus.Properties.GetDataSourceValue(txtStatus.Properties.ValueMember, 0);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
            //    btnLenit.Enabled = true;
            //    btnLennhieu.Enabled = true;
            //    btnXuongit.Enabled = false;
            //    btnXuongnhieu.Enabled = false;
            //}
            //catch
            //{
            //}
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
            //    btnLenit.Enabled = true;
            //    btnLennhieu.Enabled = true;
            //    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
            //    {
            //        btnXuongit.Enabled = false;
            //        btnXuongnhieu.Enabled = false;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
            //    btnXuongit.Enabled = true;
            //    btnXuongnhieu.Enabled = true;
            //    if (gridView2.FocusedRowHandle == 0)
            //    {
            //        btnLenit.Enabled = false;
            //        btnLennhieu.Enabled = false;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = 0;
            //    btnLenit.Enabled = false;
            //    btnLennhieu.Enabled = false;
            //    btnXuongit.Enabled = true;
            //    btnXuongnhieu.Enabled = true;
            //}
            //catch
            //{
            //}
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Rap())
            {
                return;
            }
            string sSQL = "";
            sSQL += "Update MOVIE Set " + "\n";
            sSQL += "VNName=" + clsMain.SQLStringUnicode(txtVNName.Text) + "," + "\n";
            sSQL += "ENName=" + clsMain.SQLString(txtENName.Text) + "," + "\n";
            sSQL += "Director=" + clsMain.SQLStringUnicode(txtDirector.Text) + "," + "\n";
            sSQL += "Cast=" + clsMain.SQLStringUnicode(txtCast.Text) + "," + "\n";
            sSQL += "Country=" + clsMain.SQLStringUnicode(txtCountry.EditValue.ToString ()) + "," + "\n";
            sSQL += "ReleaseDate=" + clsMain.SQLString(txtReleaseDate.Text) + "," + "\n";
            sSQL += "EndDate=" + clsMain.SQLString(txtEndDate.Text) + "," + "\n";
            sSQL += "VNBriefContent=" + clsMain.SQLStringUnicode(txtVNBriefContent.Text) + "," + "\n";
            sSQL += "ENBriefContent=" + clsMain.SQLString(txtENBriefContent.Text) + "," + "\n";
            sSQL += "VNDescription=" + clsMain.SQLStringUnicode(txtVNDescription.Text) + "," + "\n";
            sSQL += "ENDescription=" + clsMain.SQLString(txtENDescription.Text) + "," + "\n";
            sSQL += "Image=" + clsMain.SQLString(txtImage.Text) + "," + "\n";
            sSQL += "Trailer=" + clsMain.SQLString(txtTrailer.Text) + "," + "\n";
            sSQL += "Rate=" + clsMain.SQLString(txtRatings.Text) + "," + "\n";
            if (txtType.Text != "")
            {
                sSQL += "Type=" + clsMain.SQLString(txtType.EditValue.ToString()) + "," + "\n";
            }
            sSQL += "Cinemas=" + clsMain.SQLStringUnicode(txtCinemas.EditValue.ToString ()) + "," + "\n";// bo theo y anh hai
            sSQL += "Status=" + clsMain.SQLString(txtStatus.EditValue.ToString ()) + "," + "\n";
            sSQL += "Technology=" + clsMain.SQLStringUnicode(txtTechnology.EditValue.ToString ()) + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_PHIM Where MAPHIM=" + clsMain.SQLString(sma) + ")" + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_Rap()
        {
            if (txtVNName.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập thông tin đầy đủ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtVNName.Focus();
                return false;
            }
            return true;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        DataTable dtRap = new DataTable();
        private void LoaddataGridView()
        {
            string sSQL = "";
            sSQL += "Select ID,VNName,ENName,Director,Cast,Country,ReleaseDate,EndDate,VNBriefContent,ENBriefContent,VNDescription,ENDescription,Image,Trailer,Type,Cinemas,Status,Technology,Rate" + "\n";
            sSQL += "From MOVIE" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From dm_phim Where MAPHIM=" + clsMain.SQLString(sma) + ")" + "\n";
            dtRap = clsMain.ReturnDataTable(sSQL);
            txtVNName.Text = dtRap.Rows[0]["VNName"].ToString();
            txtENName.Text = dtRap.Rows[0]["ENName"].ToString();
            txtDirector.Text = dtRap.Rows[0]["Director"].ToString();
            txtCast.Text = dtRap.Rows[0]["Cast"].ToString();
            try
            {
                txtReleaseDate.EditValue = (DateTime)dtRap.Rows[0]["ReleaseDate"];
                txtEndDate.EditValue = (DateTime)dtRap.Rows[0]["EndDate"];
            }
            catch
            {
            }
            txtVNBriefContent.Text = dtRap.Rows[0]["VNBriefContent"].ToString();
            txtENBriefContent.Text = dtRap.Rows[0]["ENBriefContent"].ToString();
            txtVNDescription.Text = dtRap.Rows[0]["VNDescription"].ToString();
            txtENDescription.Text = dtRap.Rows[0]["ENDescription"].ToString();
            txtImage.Text = dtRap.Rows[0]["Image"].ToString();
            txtTrailer.Text = dtRap.Rows[0]["Trailer"].ToString();
            txtCountry.EditValue = dtRap.Rows[0]["Country"].ToString ().ToLower ();
            txtType.EditValue = dtRap.Rows[0]["Type"].ToString().ToLower();
            txtCinemas.EditValue = dtRap.Rows[0]["Cinemas"].ToString().ToLower();
            txtTechnology.EditValue = dtRap.Rows[0]["Technology"].ToString().ToLower();
            txtStatus.EditValue = dtRap.Rows[0]["Status"].ToString().ToLower();
            txtRatings.EditValue = dtRap.Rows[0]["Rate"].ToString();
        }

        private void Frm_Chitietphim_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnNuocSanXuat_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 54;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnTheLoaiPhim_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 55;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnCongNghe_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 56;
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}