﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;

namespace KP_RES
{
    public partial class PF_Huyve : DevExpress.XtraEditors.XtraForm
    {
        string Mave = "";
        DataTable dt = new DataTable();
        DataTable dtVein = new DataTable();

        public PF_Huyve()
        {
            InitializeComponent();
            txtLydo.Text = clsGlobal.gsNameOfUserLogin + " - Hủy vé";
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTenkhach.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            pnlReport.Controls.Clear();
            string sSQL = "";
            sSQL = "";
            sSQL += "Select A.MAVE,A.MALICHCHIEU,A.MALOAIVE,V.TENLOAIVE,A.MABARCODE,A.SOGHE,A.DONGIA,A.TENHANHKHACH,A.CMND,A.DIACHI,A.DIENTHOAI,A.EMAIL,A.NGUOITAO AS NGUOITAOOLD,A.NGAYTAO,C.MALOAIGHE,C.TENLOAIGHE,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.MACHATLUONGPHIM,E.MAPHONG,E.TENPHONG,F.MAPHIM,F.TENPHIM,G.TENCHATLUONGPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
            sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I,DM_LOAIVE V" + "\n";
            sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
            sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
            sSQL += "And A.MALOAIVE = V.MALOAIVE" + "\n";
            sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
            sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
            sSQL += "And A.NGUOITAO = I.UserID" + "\n";
            sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
            sSQL += "And A.TRANGTHAI =" + clsMain.SQLString("1") + "\n";
            sSQL += "And A.CONLAI =" + clsMain.SQLString("0") + "\n";
            sSQL += "And A.MABARCODE in (Select top 1 MABARCODE From VE Where MABARCODE= " + clsMain.SQLString(txtMave.Text) + " Order by NGAYTAO DESC)" + "\n";
            sSQL += "Order by A.NGAYTAO DESC"  + "\n";

            dt = clsMain.ReturnDataTable(sSQL);
            dtVein = dt.Copy();
            if (dt.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng . Hoặc đã hủy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
            }
            else
            {
                Mave = txtMave.Text;
                Frm_Report1 frm = new Frm_Report1();
                if (cls_KP_RES.Mode == 2)
                {
                    frm.ReportName = "rptVerapphimIonah";
                }
                else if (cls_KP_RES.Mode == 11)
                {
                    frm.ReportName = "rptVerapphim_Bill";
                }
                else
                {
                    frm.ReportName = "rptVerapphim";
                }
                frm.DataSource = dt;
                frm.IsPrint = false;
                frm.PrinterName = clsKP_Terminal.Mayinve;
                frm.ExportName = "In ve";
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.TopLevel = false;
                frm.Parent = pnlReport;
                frm.Show();
                txtLydo.Focus();
            }
        }

        private void btnHuyve_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTenkhach.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (txtLydo.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSodienthoai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLydo.Focus();
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận xóa vé này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                string sSQL = "";
                string sMakhachang = clsMain.ReturnDataTable("select MA_KHACHHANG from VETONGHOP Where MABARCODE in (Select top 1 MABARCODE From VE Where MABARCODE= " + clsMain.SQLString(Mave) + " Order by NGAYTAO DESC)").Rows[0]["MA_KHACHHANG"].ToString();
                if (sMakhachang != "" && sMakhachang != "0")
                {
                    if (clsUserManagement.CheckActive("4"))
                    {
                        cls_KHTT.TruDiemKhachHangThanThiet(sMakhachang, decimal.Parse(dt.Rows[0]["DONGIA"].ToString()));//KHTT
                    }
                }
                sSQL = "";
                sSQL += "Update VE " + "\n";
                sSQL += "Set  TRANGTHAI= " + clsMain.SQLString("0") + "," + "\n";
                sSQL += "MABARCODE= " + clsMain.SQLString("") + "," + "\n";
                sSQL += "BIENNHAN= " + clsMain.SQLString("") + "," + "\n";
                sSQL += "TENHANHKHACH= " + clsMain.SQLString("") + "," + "\n";
                sSQL += "DIENTHOAI= " + clsMain.SQLString("") + "," + "\n";
                sSQL += "DONGIA= " + clsMain.SQLString("1") + "," + "\n";
                sSQL += "DATHANHTOAN= " + clsMain.SQLString("0") + "," + "\n";
                sSQL += "CONLAI= " + clsMain.SQLString("1") + "\n";
                //10/10/2015 Cho chinh xac ve
                //sSQL += "Where MABARCODE in (Select top 1 MABARCODE From VE Where MABARCODE= " + clsMain.SQLString(Mave) + " Order by NGAYTAO DESC)" + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(dt.Rows[0]["MAVE"].ToString()) + "\n";
                //25/2/2014 bổ sung ghi vào sổ bán vé tổng hợp để báo cáo tiền cho chính xác
                sSQL += "Insert into VETONGHOP (LOAIVE,MABARCODE,MALICHCHIEU,SOGHE,MAPHONG,MAPHIM,MALOAIGHE,MALOAIVE,DONGIA,MAQUAY,NGUOITAO,NGAYTAO,LYDO,NGUOIBAN,NGAYBAN)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode("Vé hủy") + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MABARCODE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MALICHCHIEU"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["SOGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MAPHONG"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MAPHIM"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MALOAIGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MALOAIVE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["DONGIA"].ToString()) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += clsMain.SQLStringUnicode(txtLydo.Text) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["NGUOITAOOLD"].ToString()) + ",";
                sSQL += clsMain.SQLString(((DateTime)dt.Rows[0]["NGAYTAO"]).ToString("dd/MM/yyyy HH : mm")) + ")";
                //---------
                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                this.Cursor = Cursors.Default;

                if (bRunSQL)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pnlReport.Controls.Clear();
                    Mave = "";
                    dt.Clear();
                    txtMave.Text = "";
                    txtMave.Focus();
                   
                    Frm_Report1 frm = new Frm_Report1();
                    if (cls_KP_RES.Mode == 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    else if (cls_KP_RES.Mode == 11)
                    {
                        frm.ReportName = "rptVerapphim_Bill_Huy";
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(1000);
                        frm.ReportName = "rptVerapphim_Huy";
                    }
                    frm.DataSource = dtVein;
                    frm.IsPrint = true;
                    frm.PrinterName = clsKP_Terminal.Mayinve;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.ExportName = "In ve";
                    frm.ShowDialog(this);
                    frm.Dispose();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Xóa không thành công.Thử lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PF_Huyve_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            txtMave.Text = "";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtMave.Text = txtMave.Text + btn.Text ;
        }
    }
}