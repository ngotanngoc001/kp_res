﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Collections;


namespace KP_RES 
{
    public partial class Frm_RapPhong : DevExpress.XtraEditors.XtraForm
    {
        public Frm_RapPhong()
        {
            InitializeComponent();
            LoadCombo();
            LoaddataGridView_Rap();
            LoaddataGridView_LoaiGhe();
            LoadPermission();
        }

        private void Frm_RapPhong_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView_Rap();
            LoaddataGridView_LoaiGhe();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL = "";
            sSQL += "Select MALOAIGHE As MA,TENLOAIGHE As TEN,MAU" + "\n";
            sSQL += "From DM_LOAIGHE" + "\n";
            sSQL += "Order by TEN" + "\n";
           DataTable  dt = clsMain.ReturnDataTable(sSQL);
            cboLOAIGHE.Properties.DataSource = dt;
            cboLOAIGHE.EditValue = cboLOAIGHE.Properties.GetDataSourceValue(cboLOAIGHE.Properties.ValueMember, 0);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("68");
            btnSua.Enabled = clsUserManagement.AllowEdit("68");
            btnXoa.Enabled = clsUserManagement.AllowDelete("68");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
            btnKhac.Enabled = btnLuu.Enabled;

            btnThem_Phong.Enabled = clsUserManagement.AllowAdd("68");
            btnSua_Phong.Enabled = clsUserManagement.AllowEdit("68");
            btnXoa_Phong.Enabled = clsUserManagement.AllowDelete("68");
            btnLuu_Phong.Enabled = btnThem_Phong.Enabled || btnSua_Phong.Enabled;

            btnThem_Loaighe.Enabled = clsUserManagement.AllowAdd("68");
            btnSua_Loaighe.Enabled = clsUserManagement.AllowEdit("68");
            btnXoa_Loaighe.Enabled = clsUserManagement.AllowDelete("68");
            btnLuu_Loaighe.Enabled = btnThem_Loaighe.Enabled || btnSua_Loaighe.Enabled;
        }

        private void Frm_RapPhong_Load(object sender, EventArgs e)
        {
            try
            {
                cboLOAIGHE.EditValue = cboLOAIGHE.Properties.GetDataSourceValue(cboLOAIGHE.Properties.ValueMember, 0);
            }
            catch
            {
            }
        }

        #region Loại ghế
        string smaLoaighe = "";
        DataTable dtLoaighe = new DataTable();
        private void LoaddataGridView_LoaiGhe()
        {
            LoadCombo();
            LoadCombo_LoaiGhe();
            SetRongControl_LoaiGhe();
            KhoaMoControl_LoaiGhe(true);

            string sSQL = "";
            sSQL += "Select MALOAIGHE As MA,TENLOAIGHE As TEN,GHICHU,SUDUNG,DONVITINH,(Select TEN_DONVITINH From DONVITINH Where MA_DONVITINH=DONVITINH)As TENDVT,MAU" + "\n";
            sSQL += "From DM_LOAIGHE" + "\n";
            sSQL += "Order by TEN" + "\n";
            dtLoaighe = clsMain.ReturnDataTable(sSQL);

            fpnlLoaiGhe.Controls.Clear();
            foreach (DataRow dr in dtLoaighe.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 50;
                btn.Font = fpnlLoaiGhe.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                if (btn.Text.Length > 20)
                {
                    btn.Width = 110 * 2;
                }
                btn.Click += new EventHandler(LoaiGhe_Click);

                btn.Appearance.ForeColor = Color.White;
                btn.Appearance.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAU"].ToString());
                btn.Appearance.BackColor2 = cls_KP_RES.ConvertStringToColor(dr["MAU"].ToString());
                if (btn.Appearance.Options.UseBackColor == false)
                    btn.Appearance.Options.UseBackColor = true;
                btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                if (btn.LookAndFeel.UseDefaultLookAndFeel == true)
                    btn.LookAndFeel.UseDefaultLookAndFeel = false;

                fpnlLoaiGhe.Controls.Add(btn);
            }
        }

        private void LoadCombo_LoaiGhe()
        {
            string sSQL = "";
            DataTable dt = new DataTable();

            sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN" + "\n";
            sSQL += "From DONVITINH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_DONVITINH ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboDonvitinh.Properties.DataSource = dt;
            cboDonvitinh.EditValue = (int)dt.Rows[0][0];
        }

        private void KhoaMoControl_LoaiGhe(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENLOAIGHE.Properties.ReadOnly = true;
                cboDonvitinh.Properties.ReadOnly = true;
                lblMau_Loaighe.Properties.ReadOnly = true;
                txtGHICHULOAIGHE.Properties.ReadOnly = true;
                chkSUDUNGLOAIGHE.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENLOAIGHE.Properties.ReadOnly = false;
                cboDonvitinh.Properties.ReadOnly = false;
                lblMau_Loaighe.Properties.ReadOnly = false;
                txtGHICHULOAIGHE.Properties.ReadOnly = false;
                chkSUDUNGLOAIGHE.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl_LoaiGhe()
        {
            smaLoaighe = "";
            txtTENLOAIGHE.Text = "";
            txtGHICHULOAIGHE.Text = "";
            chkSUDUNGLOAIGHE.Checked = true;
        }

        private void LoaiGhe_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlLoaiGhe.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = Color.White;
                }
            }
            smaLoaighe = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENLOAIGHE.Text = dtLoaighe.Select("MA=" + clsMain.SQLString(smaLoaighe))[0]["TEN"].ToString();
            cboDonvitinh.EditValue = int.Parse(dtLoaighe.Select("MA=" + clsMain.SQLString(smaLoaighe))[0]["DONVITINH"].ToString());
            lblMau_Loaighe.Color = cls_KP_RES.ConvertStringToColor(dtLoaighe.Select("MA=" + clsMain.SQLString(smaLoaighe))[0]["MAU"].ToString());
            txtGHICHULOAIGHE.Text = dtLoaighe.Select("MA=" + clsMain.SQLString(smaLoaighe))[0]["GHICHU"].ToString();
            chkSUDUNGLOAIGHE.Checked = bool.Parse(dtLoaighe.Select("MA=" + clsMain.SQLString(smaLoaighe))[0]["SUDUNG"].ToString());
            KhoaMoControl_LoaiGhe(true);
            
            cboLOAIGHE.EditValue = int.Parse(smaLoaighe);
            lblMau3.Color = cls_KP_RES.ConvertStringToColor(cboLOAIGHE.GetColumnValue("MAU").ToString());

            foreach (Control txt in panel1.Controls)
            {
                if (txt.ForeColor == Color.Black)
                {
                    txt.ForeColor = Color.White;
                    txt.BackColor = btn.Appearance.BackColor ;
                   //txt.Name =smaLoaighe+ txt.Name.Substring( txt.Name.IndexOf("-"),txt.Name.Length -1);
                    txt.Tag = smaLoaighe;
                }
            }
        }

        private void btnThem_Loaighe_Click(object sender, EventArgs e)
        {
            SetRongControl_LoaiGhe();
            KhoaMoControl_LoaiGhe(false);
            txtTENLOAIGHE.Focus();
        }

        private void btnXoa_Loaighe_Click(object sender, EventArgs e)
        {
            if (smaLoaighe == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
     
            sSQL += "Delete From DM_LOAIGHE" + "\n";
            sSQL += "Where MALOAIGHE=" + clsMain.SQLString(smaLoaighe) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_LoaiGhe();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Loaighe_Click(object sender, EventArgs e)
        {
            if (smaLoaighe == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_LoaiGhe(false);
            txtTENLOAIGHE.Focus();
        }

        private void btnLuu_Loaighe_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Loaighe())
            {
                return;
            }
            string sSQL = "";
            if (smaLoaighe == "" && btnThem_Loaighe.Enabled)
            {
                sSQL += "Insert into DM_LOAIGHE (TENLOAIGHE,DONVITINH,MAU,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENLOAIGHE.Text) + ",";
                sSQL += clsMain.SQLString(cboDonvitinh.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(lblMau_Loaighe.Color.Name.ToString()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHULOAIGHE.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGLOAIGHE.Checked) + ")";
            }
            else if ((smaLoaighe != "" && btnSua_Loaighe.Enabled))
            {
                sSQL += "Update DM_LOAIGHE Set " + "\n";
                sSQL += "TENLOAIGHE=" + clsMain.SQLStringUnicode(txtTENLOAIGHE.Text) + "," + "\n";
                sSQL += "DONVITINH=" + clsMain.SQLString(cboDonvitinh.EditValue.ToString()) + "," + "\n";
                sSQL += "MAU=" + clsMain.SQLString(lblMau_Loaighe.Color.Name.ToString()) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHULOAIGHE.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGLOAIGHE.Checked) + "\n";
                sSQL += "Where MALOAIGHE=" + clsMain.SQLString(smaLoaighe) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_LoaiGhe();
                btnThem_Loaighe.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_Loaighe()
        {
            if (txtTENLOAIGHE.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN_LOAIGHE.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENLOAIGHE.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Rạp
        string smaDIADIEM = "";
        DataTable dtDIADIEM = new DataTable();
        private void LoaddataGridView_Rap()
        {
            SetRongControl_Rap();
            KhoaMoControl_Rap(true);

            string sSQL = "";
            sSQL += "Select MADIADIEM As MA,TENDIADIEM As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DM_DIADIEM" + "\n";
            sSQL += "Order by SUDUNG DESC, TENDIADIEM" + "\n";
            dtDIADIEM = clsMain.ReturnDataTable(sSQL);
            pnlDIADIEM.Controls.Clear();
            pnlPHONG.Controls.Clear();
            foreach (DataRow dr in dtDIADIEM.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 50;
                btn.Font = pnlDIADIEM.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "C" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(DIADIEM_Click);
                pnlDIADIEM.Controls.Add(btn);
            }
        }

        private void SetRongControl_Rap()
        {
            smaDIADIEM = "";
            txtTENDIADIEM.Text = "";
            txtGHICHUDIADIEM.Text = "";
            chkSUDUNGDIADIEM.Checked = true;
        }

        private void KhoaMoControl_Rap(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENDIADIEM.Properties.ReadOnly = true;
                txtGHICHUDIADIEM.Properties.ReadOnly = true;
                chkSUDUNGDIADIEM.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENDIADIEM.Properties.ReadOnly = false;
                txtGHICHUDIADIEM.Properties.ReadOnly = false;
                chkSUDUNGDIADIEM.Properties.ReadOnly = false;
            }
        }

        private void DIADIEM_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlDIADIEM.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaDIADIEM = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENDIADIEM.Text = dtDIADIEM.Select("MA=" + clsMain.SQLString(smaDIADIEM))[0]["TEN"].ToString();
            txtGHICHUDIADIEM.Text = dtDIADIEM.Select("MA=" + clsMain.SQLString(smaDIADIEM))[0]["GHICHU"].ToString();
            chkSUDUNGDIADIEM.Checked = bool.Parse(dtDIADIEM.Select("MA=" + clsMain.SQLString(smaDIADIEM))[0]["SUDUNG"].ToString());
          
            pnlPHONG.Controls.Clear();
            LoaddataGridView_Phong(btn.Name.Substring(1, btn.Name.Length - 1));
        }

        private void btnKhac_Click(object sender, EventArgs e)
        {
            if (smaDIADIEM == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            string sSQL = "";
            sSQL += "Select ID" + "\n";
            sSQL += "From cinemas" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_DIADIEM Where MADIADIEM=" + clsMain.SQLString(smaDIADIEM) + ")" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count == 0)
            {
                sSQL = "";
                string ID_SYNC = clsMain.ReturnDataTable("Select NEWID() As ID ").Rows[0][0].ToString();
                sSQL += "Insert into cinemas (ID,VNName)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(ID_SYNC) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTENDIADIEM.Text) + ")" + "\n";
                sSQL += "Update DM_DIADIEM Set " + "\n";
                sSQL += "ID_SYNC=" + clsMain.SQLString(ID_SYNC)  + "\n";
                sSQL += "Where MADIADIEM=" + clsMain.SQLString(smaDIADIEM) + "\n";
                clsMain.ExecuteSQL(sSQL);
            }
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.ma = smaDIADIEM;
            frm.Mode = 37;
            frm.ShowDialog();
            frm.Dispose();
        }
         
        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (smaDIADIEM == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From cinemas" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_DIADIEM Where MADIADIEM=" + clsMain.SQLString(smaDIADIEM)  +")"+ "\n";
            sSQL += "Delete From DM_DIADIEM" + "\n";
            sSQL += "Where MADIADIEM=" + clsMain.SQLString(smaDIADIEM) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView_Rap();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaDIADIEM == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_Rap(false);
            if (smaDIADIEM != "")
            {
                txtTENDIADIEM.Focus();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl_Rap();
            KhoaMoControl_Rap(false);
            txtTENDIADIEM.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Rap())
            {
                return;
            }
            string sSQL = "";
            if (txtTENDIADIEM.Text != "")
            {
                if (smaDIADIEM == "" && btnThem.Enabled)
                {
                   
                    sSQL += "Insert into DM_DIADIEM (TENDIADIEM,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLStringUnicode(txtTENDIADIEM.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHUDIADIEM.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNGDIADIEM.Checked) + ")";
                }
                else if ((smaDIADIEM != "" && btnSua.Enabled))
                {
                    sSQL += "Update DM_DIADIEM Set " + "\n";
                    sSQL += "TENDIADIEM=" + clsMain.SQLStringUnicode(txtTENDIADIEM.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUDIADIEM.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGDIADIEM.Checked) + "\n";
                    sSQL += "Where MADIADIEM=" + clsMain.SQLString(smaDIADIEM) + "\n";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_Rap();
                LoadCombo();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_Rap()
        {
            if (txtTENDIADIEM.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập thông tin đầy đủ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENDIADIEM.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Phòng
        string smaPHONG = "";
        DataTable dtPHONG = new DataTable();
        private void LoaddataGridView_Phong(string maDIADIEM)
        {
            SetRongControl_Phong();
            KhoaMoControl_Phong(true);

            string sSQL = "";
            sSQL += "Select MARAP,MAPHONG As MA,TENPHONG As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DM_PHONG" + "\n";
            sSQL += "Where MARAP=" + clsMain.SQLString(maDIADIEM) + "\n";
            sSQL += "Order by SUDUNG DESC, TENPHONG" + "\n";
            dtPHONG = clsMain.ReturnDataTable(sSQL);
            pnlPHONG.Controls.Clear();
            foreach (DataRow dr in dtPHONG.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 50;
                btn.Font = pnlPHONG.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "K" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(PHONG_Click);
                pnlPHONG.Controls.Add(btn);
            }
        }

        private void SetRongControl_Phong()
        {
            smaPHONG= "";
            txtTENPHONG.Text = "";
            txtGHICHUPHONG.Text = "";
            chkSUDUNGPHONG.Checked = true;
        }

        private void KhoaMoControl_Phong(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENPHONG.Properties.ReadOnly = true;
                txtGHICHUPHONG.Properties.ReadOnly = true;
                chkSUDUNGPHONG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENPHONG.Properties.ReadOnly = false;
                txtGHICHUPHONG.Properties.ReadOnly = false;
                chkSUDUNGPHONG.Properties.ReadOnly = false;
            }
        }

        private void PHONG_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlPHONG.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaPHONG = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENPHONG.Text = dtPHONG.Select("MA=" + clsMain.SQLString(smaPHONG))[0]["TEN"].ToString();
            txtGHICHUPHONG.Text = dtPHONG.Select("MA=" + clsMain.SQLString(smaPHONG))[0]["GHICHU"].ToString();
            chkSUDUNGPHONG.Checked = bool.Parse(dtPHONG.Select("MA=" + clsMain.SQLString(smaPHONG))[0]["SUDUNG"].ToString());

            LoadSoDoGhe();
        }

        private void btnXoa_Phong_Click(object sender, EventArgs e)
        {
            if (smaPHONG == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            string sSQL = "";
            if (clsMain.ReturnDataTable("select a.maghe from VE a , DM_GHE b where a.MAGHE =b.MAGHE and b.MAPHONG =" + clsMain.SQLString(smaPHONG)).Rows.Count > 0)
            {
                sSQL += "Delete From DM_PHONG" + "\n";
                sSQL += "Where MAPHONG=" + clsMain.SQLString(smaPHONG) + "\n";
            }
            else
            {
                sSQL += "Delete From DM_GHE" + "\n";
                sSQL += "Where MAPHONG=" + clsMain.SQLString(smaPHONG) + "\n";
                sSQL += "Delete From DM_PHONG" + "\n";
                sSQL += "Where MAPHONG=" + clsMain.SQLString(smaPHONG) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_Phong(smaDIADIEM);
                panel1.Controls.Clear();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Phong_Click(object sender, EventArgs e)
        {
            if (smaPHONG == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_Phong(false);
            if (smaPHONG != "")
            {
                txtTENPHONG.Focus();
            }
        }

        private void btnThem_Phong_Click(object sender, EventArgs e)
        {
            if (smaDIADIEM == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            SetRongControl_Phong();
            KhoaMoControl_Phong(false);
            txtTENPHONG.Focus();
        }

        private void btnLuu_Phong_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Phong())
            {
                return;
            }
            string sSQL = "";
            if (txtTENPHONG.Text != "")
            {
                if (smaPHONG == "" && btnThem_Phong.Enabled)
                {
                    sSQL += "Insert into DM_PHONG (MARAP,TENPHONG,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLString(smaDIADIEM) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTENPHONG.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHUPHONG.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNGPHONG.Checked) + ")";
                }
                else if ((smaPHONG != "" && btnSua_Phong.Enabled))
                {
                    sSQL += "Update DM_PHONG Set " + "\n";
                    sSQL += "TENPHONG=" + clsMain.SQLStringUnicode(txtTENPHONG.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUPHONG.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGPHONG.Checked) + "\n";
                    sSQL += "Where MAPHONG=" + clsMain.SQLString(smaPHONG) + "\n";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_Phong(smaDIADIEM);
                btnThem_Phong.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_Phong()
        {
            if (smaDIADIEM  == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn rạp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (txtTENPHONG.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập thông tin đầy đủ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENPHONG.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Sơ đồ ghế
        private void LoadSoDoGhe()
        {
            panel1.Visible = false;
            while (panel1.Controls.Count > 0)
            {
                panel1.Controls[0].Dispose();
            }

            string sSQL = "";
            sSQL += "Select A.TENGHE,A.X,A.Y,A.MAPHONG,A.MALOAIGHE,A.W,A.H,A.MAUNEN,B.MAU" + "\n";
            sSQL += "From DM_GHE A , DM_LOAIGHE B " + "\n";
            sSQL += "Where A.MALOAIGHE = B.MALOAIGHE" + "\n";
            sSQL += "And A.MAPHONG =" + clsMain.SQLString(smaPHONG) + "\n";
            sSQL += "And A.SUDUNG =" + clsMain.SQLString("1") + "\n";
            sSQL += "Order by  A.X,A.Y" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            foreach (DataRow dr in dt.Rows)
            {
                int i = 0;
                TextBox txt = new TextBox();
                txt.Name = dr["MALOAIGHE"].ToString() + "-" + i.ToString();
                txt.Tag = dr["MALOAIGHE"].ToString();
                i++;
                txt.Location = new Point(int.Parse(dr["X"].ToString()), int.Parse(dr["Y"].ToString()));

                if (dr["TENGHE"].ToString().Length != 1 && dr["TENGHE"].ToString().Length < 4)
                {
                    txt.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAU"].ToString());
                }
                else
                {
                    txt.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAUNEN"].ToString());
                }
                txt.Width = int.Parse(dr["W"].ToString()); ;
                txt.Height = int.Parse(dr["H"].ToString());
                txt.Text = dr["TENGHE"].ToString();
                txt.CharacterCasing = CharacterCasing.Upper;
                txt.ContextMenuStrip = contextMenuStrip;
                txt.ForeColor = Color.White;
                txt.BorderStyle = BorderStyle.None;
                txt.Cursor = Cursors.Arrow;
                txt.TextAlign = HorizontalAlignment.Center;
                txt.Multiline = true;
                txt.Click += new EventHandler(textBox_Click);
                txt.TextChanged += new EventHandler(txt_TextChanged);
                panel1.Controls.Add(txt);
            }
            panel1.Visible = true;

            cls_PickBox pb = new cls_PickBox();
            foreach (Control c in panel1.Controls)
            {
                pb.WireControl(c);
            }
            txtDay.SelectedIndex = 0;
            txtX.Text = "0";
            txtY.Text = "0";
            txtBatdau.Text = "1";
            txtKhoangcach.Text = "3";

            int iTongsoghe = (int)clsMain.ReturnDataTable("SELECT COUNT (A.MAGHE) FROM DM_GHE A WHERE LEN(A.TENGHE )<4 AND LEN(A.TENGHE )!=1 AND A.SUDUNG =1 AND A.MAPHONG =" + clsMain.SQLString(smaPHONG)).Rows[0][0];
            lblTongghe.Text = "Tổng ghế : " + iTongsoghe.ToString();
            string sLoaighe = "";
            foreach (DataRow dr in dtLoaighe.Rows)
            {
                int iTongsoghe_Loai = (int)clsMain.ReturnDataTable("SELECT COUNT (A.MAGHE) FROM DM_GHE A WHERE LEN(A.TENGHE )<4 AND LEN(A.TENGHE )!=1 AND A.SUDUNG =1 AND A.MAPHONG =" + clsMain.SQLString(smaPHONG) + " AND A.MALOAIGHE =" + clsMain.SQLString(dr["MA"].ToString ())).Rows[0][0];
                if (iTongsoghe_Loai != 0)
                {
                    sLoaighe +=" -"+ dr["TEN"].ToString() + ":" + iTongsoghe_Loai.ToString();
                }
            }
            lblTongghe.Text = lblTongghe.Text + " (" + sLoaighe + ")";
        }

        private void textBox_Click(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)this.ActiveControl;
            if (chkChonghe.Checked)
            {
               
                if ((txt.Parent == panel1) && (txt.ForeColor == Color.White)) //Chọn ghế
                {
                    txt.ForeColor = Color.Black;
                }
                else if ((txt.Parent == panel1) && (txt.ForeColor == Color.Black)) //Trả lại ghế đã chọn
                {
                    txt.ForeColor = Color.White;
                }
            }
            if (chkAutoselect.Checked)
            {
                txt.SelectAll();
            }
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            Control txt = this.ActiveControl;
            if (txt.Name == btnThem_ghe.Name || txt.Name == btnThem1ghe.Name)
            {
                return;
            }
            if (txt.Text.Length <= 1)
            {
                txt.BackColor = Color.Gray;
                txt.ForeColor = Color.White;
            }
            else if (txt.Text.Length >= 4)
            {
                txt.BackColor = Color.Peru;
                txt.ForeColor = Color.White;
            }
            else
            {
                foreach (SimpleButton btn in fpnlLoaiGhe.Controls)
                {
                    if (btn.Name.Substring(1, btn.Name.Length - 1) == txt.Name.Substring(0, txt.Name.IndexOf("-")))
                    {
                        txt.BackColor = btn.Appearance.BackColor;
                    }
                }
                txt.ForeColor = Color.White;
            }

            if (chkAutoRename.Checked )
            {
                foreach (Control btn in panel1.Controls)
                {
                    if (btn.Location.Y == txt.Location.Y)
                    {
                        if (txt.Text != "")
                        {
                            btn.Text = txt.Text.Substring(0, 1) + btn.Text.Substring(1, btn.Text.Length - 1);
                            if (btn.Text.Length <= 1)
                            {
                                btn.BackColor = Color.Gray;
                                btn.ForeColor = Color.White;
                            }
                            else if (txt.Text.Length >= 4)
                            {
                                btn.BackColor = Color.Peru;
                                btn.ForeColor = Color.White;
                            }
                            else
                            {
                                foreach (SimpleButton btn1 in fpnlLoaiGhe.Controls)
                                {
                                    if (btn1.Name.Substring(1, btn1.Name.Length - 1) == btn.Name.Substring(0, txt.Name.IndexOf("-")))
                                    {
                                        btn.BackColor = btn1.Appearance.BackColor;
                                    }
                                }
                                btn.ForeColor = Color.White;
                            }
                        }
                    }
                    if (btn.Location.X == txt.Location.X)
                    {
                        if (txt.Text != "")
                        {
                            btn.Text = btn.Text.Substring(0, 1) + txt.Text.Substring(1, txt.Text.Length - 1);
                            if (btn.Text.Length <= 1)
                            {
                                btn.BackColor = Color.Gray;
                                btn.ForeColor = Color.White;
                            }
                            else if (txt.Text.Length >= 4)
                            {
                                btn.BackColor = Color.Peru;
                                btn.ForeColor = Color.White;
                            }
                            else
                            {
                                foreach (SimpleButton btn1 in fpnlLoaiGhe.Controls)
                                {
                                    if (btn1.Name.Substring(1, btn1.Name.Length - 1) == btn.Name.Substring(0, txt.Name.IndexOf("-")))
                                    {
                                        btn.BackColor = btn1.Appearance.BackColor;
                                    }
                                }
                                btn.ForeColor = Color.White;
                            }
                        }
                    }
                }
            }
        }

        private void mnuXoatacca_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                panel1.Controls.Clear();
            }
        }

        private void mnuXoa_Click(object sender, EventArgs e)
        {
            ArrayList danhsachve = new ArrayList();
            foreach (Control txt in panel1.Controls)
            {
                if (txt.ForeColor == Color.Black)
                {
                    danhsachve.Add(txt.Text);
                }
            }
            foreach (Object obj in danhsachve)
            {
                foreach (Control txt in panel1.Controls)
                {
                    if (txt.Text == obj.ToString() && txt.ForeColor == Color.Black)
                    {
                        panel1.Controls.Remove(txt);
                        break;
                    }
                }
            }
        }

        private void txtSoluongghe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void cboLOAIGHE_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                lblMau3.Color = cls_KP_RES.ConvertStringToColor(cboLOAIGHE.GetColumnValue("MAU").ToString());
            }
            catch
            {
            }
        }

        private void btnNhaptiep_Click(object sender, EventArgs e)
        {
            LoadSoDoGhe();
        }

        private void btnLuuGhe_Click(object sender, EventArgs e)
        {
            panel1.VerticalScroll.Value = 0;
            panel1.HorizontalScroll.Value = 0;
            panel1.VerticalScroll.Value = 0;
            panel1.HorizontalScroll.Value = 0;

            string sSQL = "";
            sSQL += "Update DM_GHE" + "\n";
            sSQL += "Set SUDUNG=" + clsMain.SQLString("0") + "\n";
            sSQL += "Where MAPHONG=" + clsMain.SQLString(smaPHONG) + "\n";
            foreach (Control ctl in panel1.Controls)
            {
                if (ctl.Text != "")
                {
                    sSQL += "Insert into DM_GHE (TENGHE,X,Y,W,H,MAUNEN,MAPHONG,MALOAIGHE)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLStringUnicode(ctl.Text) + ",";
                    sSQL += clsMain.SQLString(ctl.Location.X.ToString()) + ",";
                    sSQL += clsMain.SQLString(ctl.Location.Y.ToString()) + ",";
                    sSQL += clsMain.SQLString(ctl.Width.ToString()) + ",";
                    sSQL += clsMain.SQLString(ctl.Height.ToString()) + ",";
                    sSQL += clsMain.SQLString(ctl.BackColor.Name.ToString()) + ",";
                    sSQL += clsMain.SQLString(smaPHONG) + ",";
                   // sSQL += clsMain.SQLString(ctl.Name.Substring(0, ctl.Name.IndexOf("-"))) + ")";
                    sSQL += clsMain.SQLString(ctl.Tag .ToString ()) + ")";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadSoDoGhe();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThem1ghe_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Ghe())
                return;

            if (panel1.Controls.Count > 0)
            {
                int w =50;
                int h = 50;
                foreach (Control c in panel1.Controls)
                {
                    try
                    {
                        if (c.Text.Substring(0, 1) == "A")
                        {
                            w = c.Width;
                            h = c.Height;
                            break;
                        }
                    }
                    catch
                    {
                    }
                }
                TextBox txt = new TextBox();
                txt.Name = cboLOAIGHE.EditValue.ToString() + "-" + panel1.Controls.Count.ToString();
                txt.Tag = cboLOAIGHE.EditValue.ToString();
                txt.Location = new Point (0,0);
                txt.ContextMenuStrip = contextMenuStrip;
                txt.BackColor = lblMau3.Color;
                txt.ForeColor = Color.White;
                txt.BorderStyle = BorderStyle.None;
                txt.Cursor = Cursors.Arrow;
                txt.TextAlign = HorizontalAlignment.Center;
                txt.Multiline = true;
                txt.Width = w ;
                txt.Height = h;
                txt.CharacterCasing = CharacterCasing.Upper;
                txt.Click += new EventHandler(textBox_Click);
                txt.TextChanged += new EventHandler(txt_TextChanged);
                txt.Text = txtDay.Text + (1).ToString("D2");
                txt.MaxLength = 20;
                panel1.Controls.Add(txt);
                cls_PickBox pb = new cls_PickBox();
                foreach (Control c in panel1.Controls)
                {
                    pb.WireControl(c);
                }
            }
        }

        private void btnThem_ghe_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Ghe())
                return;
            int w =  (800  /(int.Parse(txtSoluongghe.Text)+1))-(int.Parse(txtKhoangcach.Text));
            int h = (700 / (int.Parse(txtSoluongday.Text)+1))-(int.Parse(txtKhoangcach.Text));

            for (int j = 0; j <= int.Parse(txtSoluongday.Text); j++)
            {
                Point P;
                if (txtX.Text != "" && txtY.Text != "")
                {
                    P = new Point(int.Parse(txtX.Text), int.Parse(txtY.Text));
                }
                else if (txtX.Text != "")
                {
                    P = new Point(int.Parse(txtX.Text), 0);
                }
                else if (txtY.Text != "")
                {
                    P = new Point(0, int.Parse(txtY.Text));
                }
                else
                {
                    P = new Point(0, 0);
                }
                for (int i = 0; i <= int.Parse(txtSoluongghe.Text); i++)
                {
                   
                    TextBox txt = new TextBox();
                    txt.Name = cboLOAIGHE.EditValue.ToString() + "-" + i.ToString();
                    txt.Tag = cboLOAIGHE.EditValue.ToString();
                    txt.Location = P;
                    txt.ContextMenuStrip = contextMenuStrip;
                    txt.BackColor = lblMau3.Color;
                    txt.ForeColor = Color.White;
                    txt.BorderStyle = BorderStyle.None;
                    txt.Cursor = Cursors.Arrow;
                    txt.TextAlign = HorizontalAlignment.Center;
                    txt.Multiline = true;
                    txt.Width =w;
                    txt.Height = h;
                    txt.CharacterCasing = CharacterCasing.Upper;
                    txt.Click += new EventHandler(textBox_Click);
                    txt .TextChanged +=new EventHandler(txt_TextChanged);
                    if (j == 0)
                    {
                        txt.Width = w * 4;
                        txt.Location = new System.Drawing.Point((800 - txt.Width) / 2, 0);
                        txt.Text = "SCREEN";
                        txt.BackColor = Color.Peru;
                        txt.MaxLength = 20;
                        panel1.Controls.Add(txt);
                        break;
                    }
                    if (i == 0)
                    {
                        txt.Text = txtDay.Text;
                        txt.BackColor = Color.Gray;
                    }
                    else
                    {
                        txt.Text = txtDay.Text + (i + int.Parse(txtBatdau.Text) - 1).ToString("D2");
                    }
                    txt.MaxLength = 20;
                    panel1.Controls.Add(txt);
                    P.X += txt.Width + int.Parse(txtKhoangcach.Text);
                }
                txtY.Text = (int.Parse(txtY.Text) + h + int.Parse(txtKhoangcach.Text)).ToString();
                txtDay.SelectedIndex = txtDay.SelectedIndex + 1;
            }
            cls_PickBox pb = new cls_PickBox();
            foreach (Control c in panel1.Controls)
            {
                pb.WireControl(c);
            }
            txtDay.SelectedIndex = 0;
            txtX.Text  = "0";
            txtY .Text = "0";
            txtBatdau.Text = "1";
            txtKhoangcach.Text = "3";
        }

        Boolean CheckInput_Ghe()
        {
            if (smaPHONG == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (cboLOAIGHE.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblLOAIGHE.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboLOAIGHE.Focus();
                return false;
            }
            if (txtSoluongday.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSoluongday.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDay.Focus();
                return false;
            }
            if (txtSoluongghe.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSoluongghe.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoluongghe.Focus();
                return false;
            }
            return true;
        }

        private void chkChonghe_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control txt in panel1.Controls)
            {
                if (txt.ForeColor == Color.Black)
                {
                    txt.ForeColor = Color.White;
                }
            }
        }
        #endregion
    }
}