﻿namespace KP_RES 
{
    partial class Frm_Lichchieu 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Lichchieu));
            this.splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            this.pnlMain = new DevExpress.XtraEditors.PanelControl();
            this.pnlListPhong = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pnlThanggio = new DevExpress.XtraEditors.PanelControl();
            this.pnlGio = new DevExpress.XtraEditors.PanelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.btnPhimkosudung = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlRap = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnPre = new DevExpress.XtraEditors.SimpleButton();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgay = new DevExpress.XtraEditors.DateEdit();
            this.lblNgay = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnWeb = new DevExpress.XtraEditors.SimpleButton();
            this.btnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoahet = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.pnlThemlichchieu = new DevExpress.XtraEditors.XtraScrollableControl();
            this.chkFast = new DevExpress.XtraEditors.CheckEdit();
            this.btnAn = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.pnlBanggia = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.pnlChatluongphim = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.pnlGiochieu = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pnlNgaychieu = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pnlPHIM = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pnlPHONG = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuBanggia = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPhim = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThanggio)).BeginInit();
            this.pnlThanggio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGio)).BeginInit();
            this.pnlGio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPhimkosudung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).BeginInit();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.pnlThemlichchieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFast.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlListPhong);
            this.pnlMain.Controls.Add(this.pnlThanggio);
            this.pnlMain.Controls.Add(this.pnlTop);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1020, 137);
            this.pnlMain.TabIndex = 0;
            // 
            // pnlListPhong
            // 
            this.pnlListPhong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlListPhong.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.pnlListPhong.Location = new System.Drawing.Point(2, 83);
            this.pnlListPhong.Name = "pnlListPhong";
            this.pnlListPhong.Size = new System.Drawing.Size(1016, 52);
            this.pnlListPhong.TabIndex = 38;
            // 
            // pnlThanggio
            // 
            this.pnlThanggio.Controls.Add(this.pnlGio);
            this.pnlThanggio.Controls.Add(this.btnPhimkosudung);
            this.pnlThanggio.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlThanggio.Location = new System.Drawing.Point(2, 60);
            this.pnlThanggio.Name = "pnlThanggio";
            this.pnlThanggio.Size = new System.Drawing.Size(1016, 23);
            this.pnlThanggio.TabIndex = 37;
            // 
            // pnlGio
            // 
            this.pnlGio.Controls.Add(this.labelControl27);
            this.pnlGio.Controls.Add(this.labelControl28);
            this.pnlGio.Controls.Add(this.labelControl29);
            this.pnlGio.Controls.Add(this.labelControl30);
            this.pnlGio.Controls.Add(this.labelControl31);
            this.pnlGio.Controls.Add(this.labelControl32);
            this.pnlGio.Controls.Add(this.labelControl33);
            this.pnlGio.Controls.Add(this.labelControl34);
            this.pnlGio.Controls.Add(this.labelControl35);
            this.pnlGio.Controls.Add(this.labelControl36);
            this.pnlGio.Controls.Add(this.labelControl37);
            this.pnlGio.Controls.Add(this.labelControl38);
            this.pnlGio.Controls.Add(this.labelControl39);
            this.pnlGio.Controls.Add(this.labelControl40);
            this.pnlGio.Controls.Add(this.labelControl41);
            this.pnlGio.Controls.Add(this.labelControl42);
            this.pnlGio.Controls.Add(this.labelControl43);
            this.pnlGio.Controls.Add(this.labelControl44);
            this.pnlGio.Controls.Add(this.labelControl45);
            this.pnlGio.Controls.Add(this.labelControl46);
            this.pnlGio.Controls.Add(this.labelControl47);
            this.pnlGio.Controls.Add(this.labelControl48);
            this.pnlGio.Controls.Add(this.labelControl49);
            this.pnlGio.Controls.Add(this.labelControl50);
            this.pnlGio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGio.Location = new System.Drawing.Point(56, 2);
            this.pnlGio.Name = "pnlGio";
            this.pnlGio.Size = new System.Drawing.Size(958, 19);
            this.pnlGio.TabIndex = 36;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(1320, 0);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(18, 19);
            this.labelControl27.TabIndex = 52;
            this.labelControl27.Text = "05";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(1260, 0);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(18, 19);
            this.labelControl28.TabIndex = 51;
            this.labelControl28.Text = "04";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(1200, 0);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(18, 19);
            this.labelControl29.TabIndex = 50;
            this.labelControl29.Text = "03";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(1140, 0);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(18, 19);
            this.labelControl30.TabIndex = 49;
            this.labelControl30.Text = "02";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(1080, 0);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(18, 19);
            this.labelControl31.TabIndex = 48;
            this.labelControl31.Text = "01";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(1020, 0);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(18, 19);
            this.labelControl32.TabIndex = 47;
            this.labelControl32.Text = "00";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(960, 0);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(18, 19);
            this.labelControl33.TabIndex = 46;
            this.labelControl33.Text = "23";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(900, 0);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(18, 19);
            this.labelControl34.TabIndex = 45;
            this.labelControl34.Text = "22";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(840, 0);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(18, 19);
            this.labelControl35.TabIndex = 44;
            this.labelControl35.Text = "21";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(780, 0);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(18, 19);
            this.labelControl36.TabIndex = 43;
            this.labelControl36.Text = "20";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(720, 0);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(18, 19);
            this.labelControl37.TabIndex = 42;
            this.labelControl37.Text = "19";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(660, 0);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(18, 19);
            this.labelControl38.TabIndex = 41;
            this.labelControl38.Text = "18";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(600, 0);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(18, 19);
            this.labelControl39.TabIndex = 40;
            this.labelControl39.Text = "17";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(540, 0);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(18, 19);
            this.labelControl40.TabIndex = 39;
            this.labelControl40.Text = "16";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(480, 0);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(18, 19);
            this.labelControl41.TabIndex = 38;
            this.labelControl41.Text = "15";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(420, 0);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(18, 19);
            this.labelControl42.TabIndex = 37;
            this.labelControl42.Text = "14";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Location = new System.Drawing.Point(360, 0);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(18, 19);
            this.labelControl43.TabIndex = 36;
            this.labelControl43.Text = "13";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Location = new System.Drawing.Point(300, 0);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(18, 19);
            this.labelControl44.TabIndex = 35;
            this.labelControl44.Text = "12";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Location = new System.Drawing.Point(240, 0);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(18, 19);
            this.labelControl45.TabIndex = 34;
            this.labelControl45.Text = "11";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Location = new System.Drawing.Point(180, 0);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(18, 19);
            this.labelControl46.TabIndex = 33;
            this.labelControl46.Text = "10";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Location = new System.Drawing.Point(120, 0);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(18, 19);
            this.labelControl47.TabIndex = 32;
            this.labelControl47.Text = "09";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Location = new System.Drawing.Point(60, 0);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(18, 19);
            this.labelControl48.TabIndex = 31;
            this.labelControl48.Text = "08";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Location = new System.Drawing.Point(0, 0);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(18, 19);
            this.labelControl49.TabIndex = 30;
            this.labelControl49.Text = "07";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(1380, 0);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(18, 19);
            this.labelControl50.TabIndex = 29;
            this.labelControl50.Text = "06";
            // 
            // btnPhimkosudung
            // 
            this.btnPhimkosudung.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btnPhimkosudung.Appearance.Options.UseFont = true;
            this.btnPhimkosudung.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPhimkosudung.Location = new System.Drawing.Point(2, 2);
            this.btnPhimkosudung.Name = "btnPhimkosudung";
            this.btnPhimkosudung.Size = new System.Drawing.Size(54, 19);
            this.btnPhimkosudung.TabIndex = 28;
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.panelControl4);
            this.pnlTop.Controls.Add(this.panelControl3);
            this.pnlTop.Controls.Add(this.panelControl2);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(2, 2);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1016, 58);
            this.pnlTop.TabIndex = 25;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlRap);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(242, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(135, 54);
            this.panelControl4.TabIndex = 34;
            // 
            // pnlRap
            // 
            this.pnlRap.AutoScroll = true;
            this.pnlRap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRap.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlRap.Location = new System.Drawing.Point(2, 2);
            this.pnlRap.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRap.Name = "pnlRap";
            this.pnlRap.Size = new System.Drawing.Size(131, 50);
            this.pnlRap.TabIndex = 35;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.btnPre);
            this.panelControl3.Controls.Add(this.btnNext);
            this.panelControl3.Controls.Add(this.dtpNgay);
            this.panelControl3.Controls.Add(this.lblNgay);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(240, 54);
            this.panelControl3.TabIndex = 33;
            // 
            // btnPre
            // 
            this.btnPre.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPre.Appearance.Options.UseFont = true;
            this.btnPre.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnPre.Location = new System.Drawing.Point(6, 2);
            this.btnPre.Margin = new System.Windows.Forms.Padding(4);
            this.btnPre.Name = "btnPre";
            this.btnPre.Size = new System.Drawing.Size(40, 50);
            this.btnPre.TabIndex = 31;
            this.btnPre.Text = "<";
            this.btnPre.Click += new System.EventHandler(this.btnPre_Click);
            // 
            // btnNext
            // 
            this.btnNext.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Appearance.Options.UseFont = true;
            this.btnNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnNext.Location = new System.Drawing.Point(196, 2);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(40, 50);
            this.btnNext.TabIndex = 30;
            this.btnNext.Text = ">";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // dtpNgay
            // 
            this.dtpNgay.EditValue = null;
            this.dtpNgay.EnterMoveNextControl = true;
            this.dtpNgay.Location = new System.Drawing.Point(50, 24);
            this.dtpNgay.Name = "dtpNgay";
            this.dtpNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.Appearance.Options.UseFont = true;
            this.dtpNgay.Properties.Appearance.Options.UseTextOptions = true;
            this.dtpNgay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dtpNgay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgay.Size = new System.Drawing.Size(142, 26);
            this.dtpNgay.TabIndex = 29;
            this.dtpNgay.EditValueChanged += new System.EventHandler(this.dtpNgay_EditValueChanged);
            // 
            // lblNgay
            // 
            this.lblNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgay.Location = new System.Drawing.Point(77, 2);
            this.lblNgay.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgay.Name = "lblNgay";
            this.lblNgay.Size = new System.Drawing.Size(55, 19);
            this.lblNgay.TabIndex = 28;
            this.lblNgay.Text = "Thứ hai";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnWeb);
            this.panelControl2.Controls.Add(this.btnCopy);
            this.panelControl2.Controls.Add(this.btnXoahet);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnSua);
            this.panelControl2.Controls.Add(this.btnXoa);
            this.panelControl2.Controls.Add(this.btnThem);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(377, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(637, 54);
            this.panelControl2.TabIndex = 31;
            // 
            // btnWeb
            // 
            this.btnWeb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWeb.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWeb.Appearance.Options.UseFont = true;
            this.btnWeb.Image = global::KP_RES.Properties.Resources.ie_26;
            this.btnWeb.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnWeb.Location = new System.Drawing.Point(437, 2);
            this.btnWeb.Margin = new System.Windows.Forms.Padding(4);
            this.btnWeb.Name = "btnWeb";
            this.btnWeb.Size = new System.Drawing.Size(97, 50);
            this.btnWeb.TabIndex = 39;
            this.btnWeb.Text = "Update\r\nWeb";
            this.btnWeb.Click += new System.EventHandler(this.btnWeb_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.Appearance.Options.UseFont = true;
            this.btnCopy.Image = global::KP_RES.Properties.Resources.send_file_26;
            this.btnCopy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCopy.Location = new System.Drawing.Point(336, 2);
            this.btnCopy.Margin = new System.Windows.Forms.Padding(4);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(100, 50);
            this.btnCopy.TabIndex = 38;
            this.btnCopy.Text = "Copy lịch";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnXoahet
            // 
            this.btnXoahet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoahet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoahet.Appearance.Options.UseFont = true;
            this.btnXoahet.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoahet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoahet.Location = new System.Drawing.Point(235, 2);
            this.btnXoahet.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoahet.Name = "btnXoahet";
            this.btnXoahet.Size = new System.Drawing.Size(100, 50);
            this.btnXoahet.TabIndex = 37;
            this.btnXoahet.Text = "Xóa hết";
            this.btnXoahet.Click += new System.EventHandler(this.btnXoahet_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCapnhat.Location = new System.Drawing.Point(535, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(100, 50);
            this.btnCapnhat.TabIndex = 36;
            this.btnCapnhat.Text = "Cập nhật";
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnSua
            // 
            this.btnSua.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(83, 2);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 50);
            this.btnSua.TabIndex = 35;
            this.btnSua.Text = "Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(159, 2);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 50);
            this.btnXoa.TabIndex = 34;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(7, 2);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 50);
            this.btnThem.TabIndex = 31;
            this.btnThem.Text = "Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // pnlThemlichchieu
            // 
            this.pnlThemlichchieu.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.pnlThemlichchieu.Appearance.Options.UseBackColor = true;
            this.pnlThemlichchieu.Controls.Add(this.chkFast);
            this.pnlThemlichchieu.Controls.Add(this.btnAn);
            this.pnlThemlichchieu.Controls.Add(this.btnLuu);
            this.pnlThemlichchieu.Controls.Add(this.chkSUDUNG);
            this.pnlThemlichchieu.Controls.Add(this.panelControl17);
            this.pnlThemlichchieu.Controls.Add(this.panelControl15);
            this.pnlThemlichchieu.Controls.Add(this.panelControl13);
            this.pnlThemlichchieu.Controls.Add(this.panelControl11);
            this.pnlThemlichchieu.Controls.Add(this.panelControl7);
            this.pnlThemlichchieu.Controls.Add(this.panelControl1);
            this.pnlThemlichchieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlThemlichchieu.Font = new System.Drawing.Font("Tahoma", 10F);
            this.pnlThemlichchieu.Location = new System.Drawing.Point(0, 137);
            this.pnlThemlichchieu.Name = "pnlThemlichchieu";
            this.pnlThemlichchieu.Size = new System.Drawing.Size(1020, 320);
            this.pnlThemlichchieu.TabIndex = 39;
            // 
            // chkFast
            // 
            this.chkFast.EditValue = true;
            this.chkFast.EnterMoveNextControl = true;
            this.chkFast.Location = new System.Drawing.Point(152, 284);
            this.chkFast.Name = "chkFast";
            this.chkFast.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFast.Properties.Appearance.Options.UseFont = true;
            this.chkFast.Properties.Caption = "Chỉ lấy 100 phim gần đây";
            this.chkFast.Size = new System.Drawing.Size(284, 24);
            this.chkFast.TabIndex = 36;
            // 
            // btnAn
            // 
            this.btnAn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAn.Appearance.Options.UseFont = true;
            this.btnAn.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnAn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAn.Location = new System.Drawing.Point(933, 284);
            this.btnAn.Margin = new System.Windows.Forms.Padding(4);
            this.btnAn.Name = "btnAn";
            this.btnAn.Size = new System.Drawing.Size(87, 35);
            this.btnAn.TabIndex = 35;
            this.btnAn.Text = "Đóng";
            this.btnAn.Click += new System.EventHandler(this.btnAn_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(845, 284);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 33;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EditValue = true;
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(56, 284);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 32;
            // 
            // panelControl17
            // 
            this.panelControl17.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl17.Appearance.Options.UseBackColor = true;
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Controls.Add(this.pnlBanggia);
            this.panelControl17.Controls.Add(this.panelControl18);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl17.Location = new System.Drawing.Point(0, 216);
            this.panelControl17.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl17.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(1020, 66);
            this.panelControl17.TabIndex = 34;
            // 
            // pnlBanggia
            // 
            this.pnlBanggia.AutoScroll = true;
            this.pnlBanggia.BackColor = System.Drawing.Color.Transparent;
            this.pnlBanggia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBanggia.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlBanggia.Location = new System.Drawing.Point(54, 0);
            this.pnlBanggia.Name = "pnlBanggia";
            this.pnlBanggia.Size = new System.Drawing.Size(966, 66);
            this.pnlBanggia.TabIndex = 31;
            this.pnlBanggia.WrapContents = false;
            // 
            // panelControl18
            // 
            this.panelControl18.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl18.Appearance.Options.UseBackColor = true;
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.labelControl16);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl18.Location = new System.Drawing.Point(0, 0);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(54, 66);
            this.panelControl18.TabIndex = 30;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(3, 2);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(40, 38);
            this.labelControl16.TabIndex = 28;
            this.labelControl16.Text = "Bảng \r\n  giá";
            // 
            // panelControl15
            // 
            this.panelControl15.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl15.Appearance.Options.UseBackColor = true;
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.pnlChatluongphim);
            this.panelControl15.Controls.Add(this.panelControl16);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl15.Location = new System.Drawing.Point(0, 216);
            this.panelControl15.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl15.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(1020, 0);
            this.panelControl15.TabIndex = 31;
            this.panelControl15.Visible = false;
            // 
            // pnlChatluongphim
            // 
            this.pnlChatluongphim.BackColor = System.Drawing.Color.Transparent;
            this.pnlChatluongphim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlChatluongphim.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlChatluongphim.Location = new System.Drawing.Point(54, 0);
            this.pnlChatluongphim.Name = "pnlChatluongphim";
            this.pnlChatluongphim.Size = new System.Drawing.Size(966, 0);
            this.pnlChatluongphim.TabIndex = 31;
            // 
            // panelControl16
            // 
            this.panelControl16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl16.Appearance.Options.UseBackColor = true;
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.labelControl15);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl16.Location = new System.Drawing.Point(0, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(54, 0);
            this.panelControl16.TabIndex = 30;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(3, 4);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(41, 38);
            this.labelControl15.TabIndex = 28;
            this.labelControl15.Text = "Chất \r\nlượng";
            // 
            // panelControl13
            // 
            this.panelControl13.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.panelControl13.Appearance.Options.UseBackColor = true;
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.pnlGiochieu);
            this.panelControl13.Controls.Add(this.panelControl14);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl13.Location = new System.Drawing.Point(0, 166);
            this.panelControl13.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl13.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(1020, 50);
            this.panelControl13.TabIndex = 30;
            // 
            // pnlGiochieu
            // 
            this.pnlGiochieu.BackColor = System.Drawing.Color.Transparent;
            this.pnlGiochieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGiochieu.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlGiochieu.Location = new System.Drawing.Point(54, 0);
            this.pnlGiochieu.Name = "pnlGiochieu";
            this.pnlGiochieu.Size = new System.Drawing.Size(966, 50);
            this.pnlGiochieu.TabIndex = 31;
            // 
            // panelControl14
            // 
            this.panelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl14.Appearance.Options.UseBackColor = true;
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.labelControl14);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(54, 50);
            this.panelControl14.TabIndex = 30;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(3, 4);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(37, 38);
            this.labelControl14.TabIndex = 28;
            this.labelControl14.Text = "Giờ \r\nchiếu";
            // 
            // panelControl11
            // 
            this.panelControl11.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl11.Appearance.Options.UseBackColor = true;
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.pnlNgaychieu);
            this.panelControl11.Controls.Add(this.panelControl12);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl11.Location = new System.Drawing.Point(0, 116);
            this.panelControl11.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl11.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(1020, 50);
            this.panelControl11.TabIndex = 29;
            // 
            // pnlNgaychieu
            // 
            this.pnlNgaychieu.BackColor = System.Drawing.Color.Transparent;
            this.pnlNgaychieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNgaychieu.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlNgaychieu.Location = new System.Drawing.Point(54, 0);
            this.pnlNgaychieu.Name = "pnlNgaychieu";
            this.pnlNgaychieu.Size = new System.Drawing.Size(966, 50);
            this.pnlNgaychieu.TabIndex = 31;
            // 
            // panelControl12
            // 
            this.panelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl12.Appearance.Options.UseBackColor = true;
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.labelControl13);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl12.Location = new System.Drawing.Point(0, 0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(54, 50);
            this.panelControl12.TabIndex = 30;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(3, 2);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 38);
            this.labelControl13.TabIndex = 28;
            this.labelControl13.Text = "Ngày \r\nchiếu";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.pnlPHIM);
            this.panelControl7.Controls.Add(this.panelControl10);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 50);
            this.panelControl7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl7.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(1020, 66);
            this.panelControl7.TabIndex = 28;
            // 
            // pnlPHIM
            // 
            this.pnlPHIM.AutoScroll = true;
            this.pnlPHIM.BackColor = System.Drawing.Color.Transparent;
            this.pnlPHIM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPHIM.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlPHIM.Location = new System.Drawing.Point(54, 0);
            this.pnlPHIM.Name = "pnlPHIM";
            this.pnlPHIM.Size = new System.Drawing.Size(966, 66);
            this.pnlPHIM.TabIndex = 31;
            this.pnlPHIM.WrapContents = false;
            // 
            // panelControl10
            // 
            this.panelControl10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl10.Appearance.Options.UseBackColor = true;
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.labelControl12);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(54, 66);
            this.panelControl10.TabIndex = 30;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(3, 14);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 19);
            this.labelControl12.TabIndex = 28;
            this.labelControl12.Text = "Phim";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.pnlPHONG);
            this.panelControl1.Controls.Add(this.panelControl9);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 50);
            this.panelControl1.TabIndex = 27;
            // 
            // pnlPHONG
            // 
            this.pnlPHONG.BackColor = System.Drawing.Color.Transparent;
            this.pnlPHONG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPHONG.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlPHONG.Location = new System.Drawing.Point(54, 0);
            this.pnlPHONG.Name = "pnlPHONG";
            this.pnlPHONG.Size = new System.Drawing.Size(966, 50);
            this.pnlPHONG.TabIndex = 30;
            // 
            // panelControl9
            // 
            this.panelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl9.Appearance.Options.UseBackColor = true;
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.labelControl11);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl9.Location = new System.Drawing.Point(0, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(54, 50);
            this.panelControl9.TabIndex = 29;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(3, 13);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(45, 19);
            this.labelControl11.TabIndex = 28;
            this.labelControl11.Text = "Phòng";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Font = new System.Drawing.Font("Tahoma", 12F);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBanggia,
            this.mnuPhim});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(140, 52);
            // 
            // mnuBanggia
            // 
            this.mnuBanggia.Image = global::KP_RES.Properties.Resources.bill_26;
            this.mnuBanggia.Name = "mnuBanggia";
            this.mnuBanggia.Size = new System.Drawing.Size(139, 24);
            this.mnuBanggia.Text = "Bảng giá";
            // 
            // mnuPhim
            // 
            this.mnuPhim.Image = global::KP_RES.Properties.Resources.monkey_26;
            this.mnuPhim.Name = "mnuPhim";
            this.mnuPhim.Size = new System.Drawing.Size(139, 24);
            this.mnuPhim.Text = "Phim";
            // 
            // Frm_Lichchieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 457);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlThemlichchieu);
            this.Name = "Frm_Lichchieu";
            this.Text = "Lịch chiếu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Lichchieu_FormClosing);
            this.Shown += new System.EventHandler(this.Frm_Lichchieu_Shown);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Lichchieu_Paint);
            this.Leave += new System.EventHandler(this.Frm_Lichchieu_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlThanggio)).EndInit();
            this.pnlThanggio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGio)).EndInit();
            this.pnlGio.ResumeLayout(false);
            this.pnlGio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPhimkosudung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).EndInit();
            this.pnlTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.pnlThemlichchieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkFast.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlMain;
        private DevExpress.XtraEditors.PanelControl pnlTop;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl pnlThanggio;
        private DevExpress.XtraEditors.PanelControl pnlGio;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.PanelControl btnPhimkosudung;
        private DevExpress.XtraEditors.XtraScrollableControl pnlListPhong;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnPre;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraEditors.DateEdit dtpNgay;
        private DevExpress.XtraEditors.LabelControl lblNgay;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.FlowLayoutPanel pnlRap;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.XtraScrollableControl pnlThemlichchieu;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel pnlPHONG;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private System.Windows.Forms.FlowLayoutPanel pnlPHIM;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private System.Windows.Forms.FlowLayoutPanel pnlNgaychieu;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private System.Windows.Forms.FlowLayoutPanel pnlGiochieu;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private System.Windows.Forms.FlowLayoutPanel pnlChatluongphim;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.SimpleButton btnAn;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private System.Windows.Forms.FlowLayoutPanel pnlBanggia;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SimpleButton btnXoahet;
        private DevExpress.XtraEditors.SimpleButton btnCopy;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuBanggia;
        private System.Windows.Forms.ToolStripMenuItem mnuPhim;
        private DevExpress.XtraEditors.SimpleButton btnWeb;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.CheckEdit chkFast;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2;




    }
}