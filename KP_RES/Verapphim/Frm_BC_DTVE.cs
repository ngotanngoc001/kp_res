﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_DTVE : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_DTVE()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_DTVE_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Rạp
            {
                dt = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1 Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Rạp";
            }
            else if (optGroup.SelectedIndex == 1)//Phòng
            {
                dt = clsMain.ReturnDataTable("select MAPHONG as MA,TENPHONG as TEN from DM_PHONG where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Phòng";
            }
            else if (optGroup.SelectedIndex == 2)//Nhà phát hành
            {
                dt = clsMain.ReturnDataTable("select MANHAPHATHANH as MA,TENNHAPHATHANH as TEN from DM_NHAPHATHANH where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhà phát hành";
            }
            else if (optGroup.SelectedIndex == 3)//Phim
            {
                dt = clsMain.ReturnDataTable("select MAPHIM as MA,TENPHIM as TEN from DM_PHIM where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Phim";
            }
            else if (optGroup.SelectedIndex == 4)//Loại ghế
            {
                dt = clsMain.ReturnDataTable("select MALOAIGHE AS MA,TENLOAIGHE AS TEN from DM_LOAIGHE where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Loại ghế";
            }
            else if (optGroup.SelectedIndex == 5)//Loại vé
            {
                dt = clsMain.ReturnDataTable("select MALOAIVE AS MA,TENLOAIVE AS TEN from DM_LOAIVE where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Loại vé";
            }
            else if (optGroup.SelectedIndex == 6)//Nhân viên
            {
                dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dt;
                dt.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhân viên";
            }
         
            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        int tsg = 0;
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                splashScreenManager1.ShowWaitForm();
                sql += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE, A.TRANGTHAI,A.DONGIA,A.PHUTHU,A.DATHANHTOAN,A.CONLAI,A.NGUOITAO,A.NGAYTAO,B.TENGHE , B.X , B.Y , B.W , B.H , B.MAUNEN ,B.MALOAIGHE,C.TENLOAIGHE,C.MAU,D.NGAYCHIEU ,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.KETTHUC,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.MALOAIVE,I.TENLOAIVE,J.TENDIADIEM,K.TENNHAPHATHANH" + "\n";
                sql += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J,DM_NHAPHATHANH K" + "\n";
                sql += "Where A.MAGHE = B.MAGHE" + "\n";
                sql += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sql += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sql += "And D.MAPHONG = E.MAPHONG" + "\n";
                sql += "And D.MAPHIM = F.MAPHIM" + "\n";
                sql += "And A.NGUOITAO = H.UserID" + "\n";
                sql += "And H.MaNV = G.MANHANVIEN" + "\n";
                sql += "And A.MALOAIVE = I.MALOAIVE" + "\n";
                sql += "And E.MARAP = J.MADIADIEM" + "\n";
                sql += "And F.NSX = K.MANHAPHATHANH" + "\n";

                sql += "And A.TRANGTHAI =" + clsMain.SQLString("1") + "\n";
                sql += "And A.CONLAI =" + clsMain.SQLString("0") + "\n";

                sql += "And D.NGAYCHIEU BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";

                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And J.MADIADIEM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 1)//Phòng
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And E.MAPHONG= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And F.NSX= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 3)//Phim
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And D.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 4)//Loại ghế
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And B.MALOAIGHE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 5)//Loại vé
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And A.MALOAIVE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 6)//Nhân viên
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + ")" + "\n";
                    }
                }

                sql += "Order By J.TENDIADIEM,D.NGAYCHIEU,D.BATDAU,E.TENPHONG,F.TENPHIM,A.NGAYTAO" + "\n";

                dt  = clsMain.ReturnDataTable(sql);

                //Lấy tổng số ghế có thể bán 
                sql = "";
                sql += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE, A.TRANGTHAI,A.DONGIA,A.PHUTHU,A.DATHANHTOAN,A.CONLAI,A.TENHANHKHACH,A.QUOCTICH,A.NAMSINH,A.NOISINH,A.CMND,A.DIACHI,A.DIENTHOAI,A.EMAIL,A.NGUOIDICUNG,A.NGUOITAO,A.NGAYTAO,B.TENGHE , B.X , B.Y , B.W , B.H , B.MAUNEN ,B.MALOAIGHE,C.TENLOAIGHE,C.MAU,D.NGAYCHIEU ,D.BATDAU,D.KETTHUC,E.TENPHONG,F.TENPHIM,J.TENDIADIEM" + "\n";
                sql += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_DIADIEM J" + "\n";
                sql += "Where A.MAGHE = B.MAGHE" + "\n";
                sql += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sql += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sql += "And D.MAPHONG = E.MAPHONG" + "\n";
                sql += "And D.MAPHIM = F.MAPHIM" + "\n";
                sql += "And E.MARAP = J.MADIADIEM" + "\n";

                sql += "And D.NGAYCHIEU BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";

                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And J.MADIADIEM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 1)//Phòng
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And E.MAPHONG= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And F.NSX= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 3)//Phim
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And D.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 4)//Loại ghế
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And B.MALOAIGHE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 5)//Loại vé
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And A.MALOAIVE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    }
                }
                else if (optGroup.SelectedIndex == 6)//Nhân viên
                {
                    if (cboDATA.EditValue.ToString() != "0")
                    {
                        sql += "And A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + ")" + "\n";
                    }
                }

                sql += "And LEN(B.TENGHE)=" + clsMain.SQLString("3") + "\n";

                DataTable dtTSG = clsMain.ReturnDataTable(sql);

                gridControl1.DataSource = dt;

                int vb = 0;
                long tb = 0;
                

                tsg = dtTSG.Rows.Count;
                foreach (DataRow dr in dt.Rows)
                {
                    vb = vb + 1;
                    tb = tb + long.Parse(dr["DONGIA"].ToString());
                }

                lblTongveban.Text = "Tổng vé bán : " + vb.ToString("n0");
                lblTongtienban.Text = "Tổng tiền bán : " + tb.ToString("n0");
                lblTongve.Text = "Tổng số vé : " + tsg.ToString("n0");
                lblTyle.Text = "Tỷ lệ : " + ((double.Parse(vb.ToString()) * 100) / double.Parse(tsg.ToString())).ToString("F1") + "%";

                fpnlGheban.Controls.Clear();
                DataTable dtLoaighe = clsMain.ReturnDataTable("Select MALOAIGHE,TENLOAIGHE From DM_LOAIGHE");
                foreach (DataRow dr in dtLoaighe.Rows)
                {
                    DataRow[] drs = dt.Select("MALOAIGHE=" + clsMain.SQLString(dr["MALOAIGHE"].ToString()));
                    if (drs.Length > 0)
                    {
                        LabelControl lbl = new LabelControl();
                        lbl.Font = btnTimkiem.Font;
                        lbl.Text = dr["TENLOAIGHE"].ToString() + " : " + drs.Length.ToString() + "  ";
                        fpnlGheban.Controls.Add(lbl);
                    }
                }
                fpnlVeban.Controls.Clear();
                DataTable dtLoaive = clsMain.ReturnDataTable("Select MALOAIVE,TENLOAIVE From DM_LOAIVE");
                foreach (DataRow dr in dtLoaive.Rows)
                {
                    DataRow[] drs = dt.Select("MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()));
                    if (drs.Length > 0)
                    {
                        LabelControl lbl = new LabelControl();
                        lbl.Font = btnTimkiem.Font;
                        lbl.Text = dr["TENLOAIVE"].ToString() + " : " + drs.Length.ToString() + "  ";
                        fpnlVeban.Controls.Add(lbl);
                    }
                }
                splashScreenManager1.CloseWaitForm();
            }
            catch (Exception ex)
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    DataColumn colTYPE = new DataColumn();
                    colTYPE.ColumnName = "TYPE";
                    colTYPE.DataType = System.Type.GetType("System.String");

                    DataColumn colNAMETYPE = new DataColumn();
                    colNAMETYPE.ColumnName = "NAMETYPE";
                    colNAMETYPE.DataType = System.Type.GetType("System.String");
                    colNAMETYPE.DefaultValue = cboDATA.Text;

                    if (optGroup.SelectedIndex == 0)//Rạp
                    {
                        colTYPE.DefaultValue = "Rạp";

                    }
                    else if (optGroup.SelectedIndex == 1)//Phòng
                    {
                        colTYPE.DefaultValue = "Phòng";
                    }
                    else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                    {
                        colTYPE.DefaultValue = "Nhà phát hành";
                    }
                    else if (optGroup.SelectedIndex == 3)//Phim
                    {
                        colTYPE.DefaultValue = "Phim";
                    }
                    else if (optGroup.SelectedIndex == 4)//Loại ghế
                    {
                        colTYPE.DefaultValue = "Loại ghế";
                    }
                    else if (optGroup.SelectedIndex == 5)//Loại vé
                    {
                        colTYPE.DefaultValue = "Loại vé";
                    }
                    else if (optGroup.SelectedIndex == 6)//Nhân viên
                    {
                        colTYPE.DefaultValue = "Nhân viên";
                    }

                    DataColumn colTONGSOGHE = new DataColumn();
                    colTONGSOGHE.ColumnName = "TONGSOGHE2";
                    colTONGSOGHE.DataType = System.Type.GetType("System.String");
                    colTONGSOGHE.DefaultValue = tsg;

                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TYPE", dt) == false)
                        dt.Columns.Add(colTYPE);
                    if (ContainColumn("NAMETYPE", dt) == false)
                        dt.Columns.Add(colNAMETYPE);
                    if (ContainColumn("TONGSOGHE2", dt) == false)
                        dt.Columns.Add(colTONGSOGHE);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    if (optGroup1.SelectedIndex == 0)
                    {
                        frm.ma = "0";
                    }
                    else if (optGroup1.SelectedIndex == 1)
                    {
                        frm.ma = "1";
                    }
                    frm.dtSource = dt;
                    frm.Mode = 35;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}