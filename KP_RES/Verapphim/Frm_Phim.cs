﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.Metro.Navigation;

namespace KP_RES 
{
    public partial class Frm_Phim : DevExpress.XtraEditors.XtraForm
    {
        string sma = "";

        public Frm_Phim()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Phim_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtTEN.SelectAll();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From MOVIE" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_PHIM Where MAPHIM=" + clsMain.SQLString(sma) + ")" + "\n";
            sSQL += "Delete From DM_PHIM" + "\n";
            sSQL += "Where MAPHIM=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picLOAITROCHOI.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into DM_PHIM (TENPHIM,DAODIEN,DIENVIEN,THELOAI,NSX,NAM,THOILUONG,NOIDUNG,GHICHU,SUDUNG,HINHANH) Values (@TENPHIM,@DAODIEN,@DIENVIEN,@THELOAI,@NSX,@NAM,@THOILUONG,@NOIDUNG,@GHICHU,@SUDUNG,@HINHANH)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TENPHIM", (object)txtTEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@DAODIEN", (object)txtDAODIEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@DIENVIEN", (object)txtDIENVIEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THELOAI", (object)cboTheloai.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@NSX", (object)cboNSX.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@NAM", (object)txtNAM.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THOILUONG", (object)txtTHOILUONG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@NOIDUNG", (object)txtNOIDUNG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHU.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update DM_PHIM Set TENPHIM=@TENPHIM,DAODIEN=@DAODIEN,DIENVIEN=@DIENVIEN,THELOAI=@THELOAI,NSX=@NSX,NAM=@NAM,THOILUONG=@THOILUONG,NOIDUNG=@NOIDUNG,GHICHU=@GHICHU,SUDUNG=@SUDUNG,HINHANH=@HINHANH " + "\n";
                sSQL += "Where MAPHIM=" + clsMain.SQLString(sma) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TENPHIM", (object)txtTEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@DAODIEN", (object)txtDAODIEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@DIENVIEN", (object)txtDIENVIEN.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THELOAI", (object)cboTheloai.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@NSX", (object)cboNSX.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@NAM", (object)txtNAM.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THOILUONG", (object)txtTHOILUONG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@NOIDUNG", (object)txtNOIDUNG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHU.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }


            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtDAODIEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DAODIEN").ToString();
            txtDIENVIEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENVIEN").ToString();
            cboTheloai.EditValue = int.Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THELOAI").ToString ());
            cboNSX.EditValue = int.Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NSX").ToString ());
            txtNAM.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAM").ToString();
            txtTHOILUONG.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THOILUONG").ToString();
            txtNOIDUNG.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NOIDUNG").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());

            try
            {
                byte[] tam = new byte[((byte[])dtPhim.Select("MA=" + clsMain.SQLString(sma))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtPhim.Select("MA=" + clsMain.SQLString(sma))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picLOAITROCHOI.Image = bm;
                picLOAITROCHOI.BackgroundImage = null;
            }
            catch
            {
                picLOAITROCHOI.Image = null;
                picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
            KhoaMoControl(true);
        }

        private void picLOAITROCHOI_Click(object sender, EventArgs e)
        {
            if (picLOAITROCHOI.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picLOAITROCHOI.ImageLocation = openFileDialog1.FileName;
                        picLOAITROCHOI.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picLOAITROCHOI.Image = null;
                        picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        DataTable dtPhim = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            if (chkFast.Checked)
            {
                sSQL += "Select Top 200 MAPHIM As MA,TENPHIM As TEN,DAODIEN,DIENVIEN,THELOAI,(Select TENTHELOAIPHIM From DM_THELOAIPHIM Where MATHELOAIPHIM=THELOAI)As TENTHELOAI,NSX,(Select TENNHAPHATHANH From DM_NHAPHATHANH Where MANHAPHATHANH=NSX)As TENNSX,NAM,THOILUONG,NOIDUNG,GHICHU,SUDUNG,HINHANH" + "\n";
            }
            else
            {
                sSQL += "Select MAPHIM As MA,TENPHIM As TEN,DAODIEN,DIENVIEN,THELOAI,(Select TENTHELOAIPHIM From DM_THELOAIPHIM Where MATHELOAIPHIM=THELOAI)As TENTHELOAI,NSX,(Select TENNHAPHATHANH From DM_NHAPHATHANH Where MANHAPHATHANH=NSX)As TENNSX,NAM,THOILUONG,NOIDUNG,GHICHU,SUDUNG,HINHANH" + "\n";
            }
            sSQL += "From DM_PHIM" + "\n";
            if (chkFast.Checked)
            {
                sSQL += "Order by MA DESC" + "\n";
            }
            dtPhim = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dtPhim;
            gridView2.ActiveFilterString = "[SUDUNG] = True";

            LoadCombo();
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();

            sSQL = "";
            sSQL += "Select MATHELOAIPHIM as MA,TENTHELOAIPHIM As TEN" + "\n";
            sSQL += "From DM_THELOAIPHIM " + "\n";
            sSQL += "Where SUDUNG=1 " + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboTheloai.Properties.DataSource = dt;
            cboTheloai.EditValue = cboTheloai.Properties.GetDataSourceValue(cboTheloai.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MANHAPHATHANH As MA,TENNHAPHATHANH As TEN" + "\n";
            sSQL += "From DM_NHAPHATHANH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboNSX.Properties.DataSource = dt;
            cboNSX.EditValue = cboNSX.Properties.GetDataSourceValue(cboNSX.Properties.ValueMember, 0);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("74");
            btnSua.Enabled = clsUserManagement.AllowEdit("74");
            btnXoa.Enabled = clsUserManagement.AllowDelete("74");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;

            btnKhac.Enabled = btnLuu.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtDAODIEN.Properties.ReadOnly = true;
                txtDIENVIEN.Properties.ReadOnly = true;
                cboTheloai.Properties.ReadOnly = true;
                cboNSX.Properties.ReadOnly = true;
                txtNAM.Properties.ReadOnly = true;
                txtTHOILUONG.Properties.ReadOnly = true;
                txtNOIDUNG.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                picLOAITROCHOI.Enabled = false;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtDAODIEN.Properties.ReadOnly = false;
                txtDIENVIEN.Properties.ReadOnly = false;
                cboTheloai.Properties.ReadOnly = false;
                cboNSX.Properties.ReadOnly = false;
                txtNAM.Properties.ReadOnly = false;
                txtTHOILUONG.Properties.ReadOnly = false;
                txtNOIDUNG.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                picLOAITROCHOI.Enabled = true ;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtDAODIEN.Text = "";
            txtDIENVIEN.Text = "";
            cboTheloai.EditValue = cboTheloai.Properties.GetDataSourceValue(cboTheloai.Properties.ValueMember, 0);
            cboNSX.EditValue = cboNSX.Properties.GetDataSourceValue(cboNSX.Properties.ValueMember, 0);
            txtNAM.Text = "";
            txtTHOILUONG.Text = "";
            txtNOIDUNG.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
            picLOAITROCHOI.Image = null;
            picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            if (txtTHOILUONG.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTHOILUONG.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTHOILUONG.Focus();
                return false;
            }
            if (picLOAITROCHOI.Image != null && picLOAITROCHOI.ImageLocation!=null )
            {
                FileInfo fileInfo = new FileInfo(picLOAITROCHOI.ImageLocation);
                long size = fileInfo.Length;
                if (size > 200000)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Giới hạn dung lượng hình phải nhỏ hơn 200kb", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    picLOAITROCHOI.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnThongtinkhac_Click(object sender, EventArgs e)
        {
            this.Parent.GoTo<Frm_Thongtinphim>();
        }

        private void btnKhac_Click(object sender, EventArgs e)
        {
            if (sma  == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            string sSQL = "";
            sSQL += "Select ID" + "\n";
            sSQL += "From MOVIE" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_PHIM Where MAPHIM=" + clsMain.SQLString(sma) + ")" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count == 0)
            {
                sSQL = "";
                string ID_SYNC = clsMain.ReturnDataTable("Select NEWID() As ID ").Rows[0][0].ToString();
                sSQL += "Insert into MOVIE (ID,Director,Cast,VNBriefContent,VNDescription,VNName)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(ID_SYNC) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDAODIEN.Text ) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDIENVIEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtNOIDUNG.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ")" + "\n";
                sSQL += "Update DM_PHIM Set " + "\n";
                sSQL += "ID_SYNC=" + clsMain.SQLString(ID_SYNC) + "\n";
                sSQL += "Where MAPHIM=" + clsMain.SQLString(sma) + "\n";
                clsMain.ExecuteSQL(sSQL);
            }
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.ma = sma;
            frm.Mode = 38;
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}