﻿namespace KP_RES 
{
    partial class Frm_Lapbanggia 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Lapbanggia));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.txtGHICHUPP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtGiaTri = new DevExpress.XtraEditors.TextEdit();
            this.btnLuuPP = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaPP = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaPP = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemPP = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNGPP = new DevExpress.XtraEditors.CheckEdit();
            this.lblTENPP = new DevExpress.XtraEditors.LabelControl();
            this.txtTenPhuPhi = new DevExpress.XtraEditors.TextEdit();
            this.pnlPHUPHI = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.chkMacdinhLV = new DevExpress.XtraEditors.CheckEdit();
            this.btnLuuLV = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaLV = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaLV = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemLV = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGLV = new DevExpress.XtraEditors.CheckEdit();
            this.lblGHICHULV = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHULV = new DevExpress.XtraEditors.TextEdit();
            this.lblTENLV = new DevExpress.XtraEditors.LabelControl();
            this.txtTENLV = new DevExpress.XtraEditors.TextEdit();
            this.pnlLOAIVE = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnSaochep = new DevExpress.XtraEditors.SimpleButton();
            this.pnlBANGGIA = new System.Windows.Forms.FlowLayoutPanel();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENBANGGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAIGHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENTHOIGIAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MALOAIGHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MALOAIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MATHOIGIAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuutacca = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cboLOAIVE = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cboLOAIGHE = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLuu1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa1 = new DevExpress.XtraEditors.SimpleButton();
            this.txtGIAVE = new DevExpress.XtraEditors.TextEdit();
            this.btnThem1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblTYGIA = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUPP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGPP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhuPhi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMacdinhLV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIVE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIGHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAVE.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl9);
            this.panelControl1.Controls.Add(this.panelControl8);
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(725, 648);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.txtGHICHUPP);
            this.panelControl9.Controls.Add(this.labelControl12);
            this.panelControl9.Controls.Add(this.txtGiaTri);
            this.panelControl9.Controls.Add(this.btnLuuPP);
            this.panelControl9.Controls.Add(this.btnSuaPP);
            this.panelControl9.Controls.Add(this.btnXoaPP);
            this.panelControl9.Controls.Add(this.btnThemPP);
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.chkSUDUNGPP);
            this.panelControl9.Controls.Add(this.lblTENPP);
            this.panelControl9.Controls.Add(this.txtTenPhuPhi);
            this.panelControl9.Controls.Add(this.pnlPHUPHI);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(362, 313);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(361, 333);
            this.panelControl9.TabIndex = 5;
            // 
            // txtGHICHUPP
            // 
            this.txtGHICHUPP.EditValue = "         ";
            this.txtGHICHUPP.EnterMoveNextControl = true;
            this.txtGHICHUPP.Location = new System.Drawing.Point(102, 67);
            this.txtGHICHUPP.Name = "txtGHICHUPP";
            this.txtGHICHUPP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUPP.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUPP.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHUPP.TabIndex = 32;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(9, 70);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(54, 19);
            this.labelControl12.TabIndex = 31;
            this.labelControl12.Text = "Ghi chú";
            // 
            // txtGiaTri
            // 
            this.txtGiaTri.EnterMoveNextControl = true;
            this.txtGiaTri.Location = new System.Drawing.Point(102, 35);
            this.txtGiaTri.Name = "txtGiaTri";
            this.txtGiaTri.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTri.Properties.Appearance.Options.UseFont = true;
            this.txtGiaTri.Size = new System.Drawing.Size(248, 26);
            this.txtGiaTri.TabIndex = 25;
            // 
            // btnLuuPP
            // 
            this.btnLuuPP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuPP.Appearance.Options.UseFont = true;
            this.btnLuuPP.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuPP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuPP.Location = new System.Drawing.Point(10, 125);
            this.btnLuuPP.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuPP.Name = "btnLuuPP";
            this.btnLuuPP.Size = new System.Drawing.Size(78, 35);
            this.btnLuuPP.TabIndex = 27;
            this.btnLuuPP.Text = "&Lưu";
            this.btnLuuPP.Click += new System.EventHandler(this.btnLuuPP_Click);
            // 
            // btnSuaPP
            // 
            this.btnSuaPP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaPP.Appearance.Options.UseFont = true;
            this.btnSuaPP.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSuaPP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaPP.Location = new System.Drawing.Point(186, 125);
            this.btnSuaPP.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaPP.Name = "btnSuaPP";
            this.btnSuaPP.Size = new System.Drawing.Size(80, 35);
            this.btnSuaPP.TabIndex = 29;
            this.btnSuaPP.Text = "&Sửa";
            this.btnSuaPP.Click += new System.EventHandler(this.btnSuaPP_Click);
            // 
            // btnXoaPP
            // 
            this.btnXoaPP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaPP.Appearance.Options.UseFont = true;
            this.btnXoaPP.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaPP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaPP.Location = new System.Drawing.Point(274, 125);
            this.btnXoaPP.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaPP.Name = "btnXoaPP";
            this.btnXoaPP.Size = new System.Drawing.Size(80, 35);
            this.btnXoaPP.TabIndex = 30;
            this.btnXoaPP.Text = "&Xóa";
            this.btnXoaPP.Click += new System.EventHandler(this.btnXoaPP_Click);
            // 
            // btnThemPP
            // 
            this.btnThemPP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemPP.Appearance.Options.UseFont = true;
            this.btnThemPP.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThemPP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemPP.Location = new System.Drawing.Point(98, 125);
            this.btnThemPP.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemPP.Name = "btnThemPP";
            this.btnThemPP.Size = new System.Drawing.Size(80, 35);
            this.btnThemPP.TabIndex = 28;
            this.btnThemPP.Text = "&Thêm";
            this.btnThemPP.Click += new System.EventHandler(this.btnThemPP_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(9, 38);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(43, 19);
            this.labelControl3.TabIndex = 24;
            this.labelControl3.Text = "Giá trị";
            // 
            // chkSUDUNGPP
            // 
            this.chkSUDUNGPP.EnterMoveNextControl = true;
            this.chkSUDUNGPP.Location = new System.Drawing.Point(100, 97);
            this.chkSUDUNGPP.Name = "chkSUDUNGPP";
            this.chkSUDUNGPP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGPP.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGPP.Properties.Caption = "Sử dụng";
            this.chkSUDUNGPP.Size = new System.Drawing.Size(89, 24);
            this.chkSUDUNGPP.TabIndex = 26;
            // 
            // lblTENPP
            // 
            this.lblTENPP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENPP.Location = new System.Drawing.Point(9, 6);
            this.lblTENPP.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENPP.Name = "lblTENPP";
            this.lblTENPP.Size = new System.Drawing.Size(86, 19);
            this.lblTENPP.TabIndex = 22;
            this.lblTENPP.Text = "Tên phụ phí";
            // 
            // txtTenPhuPhi
            // 
            this.txtTenPhuPhi.EditValue = "         ";
            this.txtTenPhuPhi.EnterMoveNextControl = true;
            this.txtTenPhuPhi.Location = new System.Drawing.Point(102, 3);
            this.txtTenPhuPhi.Name = "txtTenPhuPhi";
            this.txtTenPhuPhi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhuPhi.Properties.Appearance.Options.UseFont = true;
            this.txtTenPhuPhi.Size = new System.Drawing.Size(248, 26);
            this.txtTenPhuPhi.TabIndex = 23;
            // 
            // pnlPHUPHI
            // 
            this.pnlPHUPHI.AutoScroll = true;
            this.pnlPHUPHI.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPHUPHI.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPHUPHI.Location = new System.Drawing.Point(2, 167);
            this.pnlPHUPHI.Name = "pnlPHUPHI";
            this.pnlPHUPHI.Size = new System.Drawing.Size(357, 248);
            this.pnlPHUPHI.TabIndex = 10;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.chkMacdinhLV);
            this.panelControl8.Controls.Add(this.btnLuuLV);
            this.panelControl8.Controls.Add(this.btnSuaLV);
            this.panelControl8.Controls.Add(this.btnXoaLV);
            this.panelControl8.Controls.Add(this.btnThemLV);
            this.panelControl8.Controls.Add(this.chkSUDUNGLV);
            this.panelControl8.Controls.Add(this.lblGHICHULV);
            this.panelControl8.Controls.Add(this.txtGHICHULV);
            this.panelControl8.Controls.Add(this.lblTENLV);
            this.panelControl8.Controls.Add(this.txtTENLV);
            this.panelControl8.Controls.Add(this.pnlLOAIVE);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl8.Location = new System.Drawing.Point(2, 313);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(360, 333);
            this.panelControl8.TabIndex = 1;
            // 
            // chkMacdinhLV
            // 
            this.chkMacdinhLV.EnterMoveNextControl = true;
            this.chkMacdinhLV.Location = new System.Drawing.Point(66, 97);
            this.chkMacdinhLV.Name = "chkMacdinhLV";
            this.chkMacdinhLV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMacdinhLV.Properties.Appearance.Options.UseFont = true;
            this.chkMacdinhLV.Properties.Caption = "Mặc định";
            this.chkMacdinhLV.Size = new System.Drawing.Size(92, 24);
            this.chkMacdinhLV.TabIndex = 16;
            // 
            // btnLuuLV
            // 
            this.btnLuuLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuLV.Appearance.Options.UseFont = true;
            this.btnLuuLV.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuLV.Image")));
            this.btnLuuLV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuLV.Location = new System.Drawing.Point(10, 125);
            this.btnLuuLV.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuLV.Name = "btnLuuLV";
            this.btnLuuLV.Size = new System.Drawing.Size(80, 35);
            this.btnLuuLV.TabIndex = 17;
            this.btnLuuLV.Text = "&Lưu";
            this.btnLuuLV.Click += new System.EventHandler(this.btnLuuLV_Click);
            // 
            // btnSuaLV
            // 
            this.btnSuaLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaLV.Appearance.Options.UseFont = true;
            this.btnSuaLV.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaLV.Image")));
            this.btnSuaLV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaLV.Location = new System.Drawing.Point(186, 125);
            this.btnSuaLV.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaLV.Name = "btnSuaLV";
            this.btnSuaLV.Size = new System.Drawing.Size(80, 35);
            this.btnSuaLV.TabIndex = 19;
            this.btnSuaLV.Text = "&Sửa";
            this.btnSuaLV.Click += new System.EventHandler(this.btnSuaLV_Click);
            // 
            // btnXoaLV
            // 
            this.btnXoaLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLV.Appearance.Options.UseFont = true;
            this.btnXoaLV.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaLV.Image")));
            this.btnXoaLV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaLV.Location = new System.Drawing.Point(274, 125);
            this.btnXoaLV.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaLV.Name = "btnXoaLV";
            this.btnXoaLV.Size = new System.Drawing.Size(80, 35);
            this.btnXoaLV.TabIndex = 20;
            this.btnXoaLV.Text = "&Xóa";
            this.btnXoaLV.Click += new System.EventHandler(this.btnXoaLV_Click);
            // 
            // btnThemLV
            // 
            this.btnThemLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLV.Appearance.Options.UseFont = true;
            this.btnThemLV.Image = ((System.Drawing.Image)(resources.GetObject("btnThemLV.Image")));
            this.btnThemLV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemLV.Location = new System.Drawing.Point(98, 125);
            this.btnThemLV.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemLV.Name = "btnThemLV";
            this.btnThemLV.Size = new System.Drawing.Size(80, 35);
            this.btnThemLV.TabIndex = 18;
            this.btnThemLV.Text = "&Thêm";
            this.btnThemLV.Click += new System.EventHandler(this.btnThemLV_Click);
            // 
            // chkSUDUNGLV
            // 
            this.chkSUDUNGLV.EnterMoveNextControl = true;
            this.chkSUDUNGLV.Location = new System.Drawing.Point(66, 67);
            this.chkSUDUNGLV.Name = "chkSUDUNGLV";
            this.chkSUDUNGLV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGLV.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGLV.Properties.Caption = "Sử dụng";
            this.chkSUDUNGLV.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGLV.TabIndex = 15;
            // 
            // lblGHICHULV
            // 
            this.lblGHICHULV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHULV.Location = new System.Drawing.Point(9, 38);
            this.lblGHICHULV.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHULV.Name = "lblGHICHULV";
            this.lblGHICHULV.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHULV.TabIndex = 13;
            this.lblGHICHULV.Text = "Ghi chú";
            // 
            // txtGHICHULV
            // 
            this.txtGHICHULV.EnterMoveNextControl = true;
            this.txtGHICHULV.Location = new System.Drawing.Point(66, 35);
            this.txtGHICHULV.Name = "txtGHICHULV";
            this.txtGHICHULV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHULV.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHULV.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHULV.TabIndex = 14;
            // 
            // lblTENLV
            // 
            this.lblTENLV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENLV.Location = new System.Drawing.Point(9, 6);
            this.lblTENLV.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENLV.Name = "lblTENLV";
            this.lblTENLV.Size = new System.Drawing.Size(50, 19);
            this.lblTENLV.TabIndex = 11;
            this.lblTENLV.Text = "Loại vé";
            // 
            // txtTENLV
            // 
            this.txtTENLV.EnterMoveNextControl = true;
            this.txtTENLV.Location = new System.Drawing.Point(66, 3);
            this.txtTENLV.Name = "txtTENLV";
            this.txtTENLV.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENLV.Properties.Appearance.Options.UseFont = true;
            this.txtTENLV.Size = new System.Drawing.Size(248, 26);
            this.txtTENLV.TabIndex = 12;
            // 
            // pnlLOAIVE
            // 
            this.pnlLOAIVE.AutoScroll = true;
            this.pnlLOAIVE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlLOAIVE.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlLOAIVE.Location = new System.Drawing.Point(2, 167);
            this.pnlLOAIVE.Name = "pnlLOAIVE";
            this.pnlLOAIVE.Size = new System.Drawing.Size(356, 248);
            this.pnlLOAIVE.TabIndex = 10;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.btnSaochep);
            this.panelControl5.Controls.Add(this.pnlBANGGIA);
            this.panelControl5.Controls.Add(this.txtTEN);
            this.panelControl5.Controls.Add(this.lblTEN);
            this.panelControl5.Controls.Add(this.chkSUDUNG);
            this.panelControl5.Controls.Add(this.btnThem);
            this.panelControl5.Controls.Add(this.btnXoa);
            this.panelControl5.Controls.Add(this.btnSua);
            this.panelControl5.Controls.Add(this.btnLuu);
            this.panelControl5.Controls.Add(this.txtGHICHU);
            this.panelControl5.Controls.Add(this.labelControl1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(721, 311);
            this.panelControl5.TabIndex = 0;
            // 
            // btnSaochep
            // 
            this.btnSaochep.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaochep.Appearance.Options.UseFont = true;
            this.btnSaochep.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnSaochep.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSaochep.Location = new System.Drawing.Point(428, 68);
            this.btnSaochep.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaochep.Name = "btnSaochep";
            this.btnSaochep.Size = new System.Drawing.Size(104, 35);
            this.btnSaochep.TabIndex = 12;
            this.btnSaochep.Text = "&Sao chép";
            this.btnSaochep.Click += new System.EventHandler(this.btnSaochep_Click);
            // 
            // pnlBANGGIA
            // 
            this.pnlBANGGIA.AutoScroll = true;
            this.pnlBANGGIA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBANGGIA.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBANGGIA.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlBANGGIA.Location = new System.Drawing.Point(2, 110);
            this.pnlBANGGIA.Name = "pnlBANGGIA";
            this.pnlBANGGIA.Size = new System.Drawing.Size(717, 199);
            this.pnlBANGGIA.TabIndex = 11;
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(76, 3);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 1;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(8, 6);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(61, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Bảng giá";
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(330, 37);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 4;
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(164, 68);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(340, 68);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = ((System.Drawing.Image)(resources.GetObject("btnSua.Image")));
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(252, 68);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 7;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(76, 68);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(76, 35);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHU.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(8, 38);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Ghi chú";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(578, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 644);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 480);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 160);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 320);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 400);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 562);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.gridControl1);
            this.panelControl7.Controls.Add(this.panelControl6);
            this.panelControl7.Controls.Add(this.panelControl2);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(725, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(619, 648);
            this.panelControl7.TabIndex = 4;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(2, 112);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(576, 534);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.TENBANGGIA,
            this.TENLOAIGHE,
            this.TENLOAIVE,
            this.TENTHOIGIAN,
            this.DONGIA,
            this.gridColumn4,
            this.gridColumn2,
            this.MALOAIGHE,
            this.MALOAIVE,
            this.MATHOIGIAN,
            this.NHANVIEN});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.MALOAIGHE, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TENLOAIGHE, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TENLOAIVE, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STT1.OptionsColumn.AllowMove = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 50;
            // 
            // TENBANGGIA
            // 
            this.TENBANGGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENBANGGIA.AppearanceHeader.Options.UseFont = true;
            this.TENBANGGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.TENBANGGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENBANGGIA.Caption = "Bảng giá";
            this.TENBANGGIA.FieldName = "TENBANGGIA";
            this.TENBANGGIA.Name = "TENBANGGIA";
            this.TENBANGGIA.OptionsColumn.AllowEdit = false;
            this.TENBANGGIA.OptionsColumn.AllowFocus = false;
            this.TENBANGGIA.OptionsColumn.FixedWidth = true;
            this.TENBANGGIA.Visible = true;
            this.TENBANGGIA.VisibleIndex = 1;
            this.TENBANGGIA.Width = 150;
            // 
            // TENLOAIGHE
            // 
            this.TENLOAIGHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAIGHE.AppearanceHeader.Options.UseFont = true;
            this.TENLOAIGHE.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAIGHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAIGHE.Caption = "Loại ghế";
            this.TENLOAIGHE.FieldName = "TENLOAIGHE";
            this.TENLOAIGHE.Name = "TENLOAIGHE";
            this.TENLOAIGHE.OptionsColumn.AllowEdit = false;
            this.TENLOAIGHE.OptionsColumn.AllowFocus = false;
            this.TENLOAIGHE.OptionsColumn.FixedWidth = true;
            this.TENLOAIGHE.Visible = true;
            this.TENLOAIGHE.VisibleIndex = 2;
            this.TENLOAIGHE.Width = 110;
            // 
            // TENLOAIVE
            // 
            this.TENLOAIVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAIVE.AppearanceHeader.Options.UseFont = true;
            this.TENLOAIVE.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAIVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAIVE.Caption = "Loại vé";
            this.TENLOAIVE.FieldName = "TENLOAIVE";
            this.TENLOAIVE.Name = "TENLOAIVE";
            this.TENLOAIVE.OptionsColumn.AllowEdit = false;
            this.TENLOAIVE.OptionsColumn.AllowFocus = false;
            this.TENLOAIVE.OptionsColumn.FixedWidth = true;
            this.TENLOAIVE.Visible = true;
            this.TENLOAIVE.VisibleIndex = 3;
            this.TENLOAIVE.Width = 150;
            // 
            // TENTHOIGIAN
            // 
            this.TENTHOIGIAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENTHOIGIAN.AppearanceHeader.Options.UseFont = true;
            this.TENTHOIGIAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENTHOIGIAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENTHOIGIAN.Caption = "Thời gian";
            this.TENTHOIGIAN.FieldName = "TENTHOIGIAN";
            this.TENTHOIGIAN.Name = "TENTHOIGIAN";
            this.TENTHOIGIAN.OptionsColumn.AllowEdit = false;
            this.TENTHOIGIAN.OptionsColumn.AllowFocus = false;
            this.TENTHOIGIAN.OptionsColumn.FixedWidth = true;
            this.TENTHOIGIAN.Width = 120;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.Caption = "Giá vé";
            this.DONGIA.DisplayFormat.FormatString = "N0";
            this.DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 4;
            this.DONGIA.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.MinWidth = 10;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 15;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "MA";
            this.gridColumn2.DisplayFormat.FormatString = "000";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn2.FieldName = "MA";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Width = 100;
            // 
            // MALOAIGHE
            // 
            this.MALOAIGHE.AppearanceCell.Options.UseTextOptions = true;
            this.MALOAIGHE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MALOAIGHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MALOAIGHE.AppearanceHeader.Options.UseFont = true;
            this.MALOAIGHE.AppearanceHeader.Options.UseTextOptions = true;
            this.MALOAIGHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MALOAIGHE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MALOAIGHE.Caption = "MALOAIGHE";
            this.MALOAIGHE.FieldName = "MALOAIGHE";
            this.MALOAIGHE.Name = "MALOAIGHE";
            this.MALOAIGHE.OptionsColumn.AllowEdit = false;
            this.MALOAIGHE.OptionsColumn.AllowFocus = false;
            this.MALOAIGHE.OptionsColumn.FixedWidth = true;
            this.MALOAIGHE.Width = 100;
            // 
            // MALOAIVE
            // 
            this.MALOAIVE.Caption = "MALOAIVE";
            this.MALOAIVE.FieldName = "MALOAIVE";
            this.MALOAIVE.Name = "MALOAIVE";
            // 
            // MATHOIGIAN
            // 
            this.MATHOIGIAN.Caption = "MATHOIGIAN";
            this.MATHOIGIAN.FieldName = "MATHOIGIAN";
            this.MATHOIGIAN.Name = "MATHOIGIAN";
            // 
            // NHANVIEN
            // 
            this.NHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.NHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIEN.Caption = "Người tạo";
            this.NHANVIEN.FieldName = "NGUOITAO";
            this.NHANVIEN.Name = "NHANVIEN";
            this.NHANVIEN.OptionsColumn.AllowEdit = false;
            this.NHANVIEN.OptionsColumn.AllowFocus = false;
            this.NHANVIEN.OptionsColumn.FixedWidth = true;
            this.NHANVIEN.Width = 100;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.btnLuutacca);
            this.panelControl6.Controls.Add(this.labelControl5);
            this.panelControl6.Controls.Add(this.cboLOAIVE);
            this.panelControl6.Controls.Add(this.labelControl2);
            this.panelControl6.Controls.Add(this.cboLOAIGHE);
            this.panelControl6.Controls.Add(this.btnLuu1);
            this.panelControl6.Controls.Add(this.btnSua1);
            this.panelControl6.Controls.Add(this.btnXoa1);
            this.panelControl6.Controls.Add(this.txtGIAVE);
            this.panelControl6.Controls.Add(this.btnThem1);
            this.panelControl6.Controls.Add(this.lblTYGIA);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(576, 110);
            this.panelControl6.TabIndex = 4;
            // 
            // btnLuutacca
            // 
            this.btnLuutacca.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuutacca.Appearance.Options.UseFont = true;
            this.btnLuutacca.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnLuutacca.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuutacca.Location = new System.Drawing.Point(428, 68);
            this.btnLuutacca.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuutacca.Name = "btnLuutacca";
            this.btnLuutacca.Size = new System.Drawing.Size(121, 35);
            this.btnLuutacca.TabIndex = 14;
            this.btnLuutacca.Text = "&Lưu tất cả";
            this.btnLuutacca.Click += new System.EventHandler(this.btnLuutacca_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(293, 8);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(50, 19);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Loại vé";
            // 
            // cboLOAIVE
            // 
            this.cboLOAIVE.EnterMoveNextControl = true;
            this.cboLOAIVE.Location = new System.Drawing.Point(361, 3);
            this.cboLOAIVE.Name = "cboLOAIVE";
            this.cboLOAIVE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAIVE.Properties.Appearance.Options.UseFont = true;
            this.cboLOAIVE.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAIVE.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLOAIVE.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLOAIVE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLOAIVE.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", 30, "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLOAIVE.Properties.DisplayMember = "TEN";
            this.cboLOAIVE.Properties.DropDownItemHeight = 40;
            this.cboLOAIVE.Properties.NullText = "";
            this.cboLOAIVE.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLOAIVE.Properties.ShowHeader = false;
            this.cboLOAIVE.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLOAIVE.Properties.ValueMember = "MA";
            this.cboLOAIVE.Size = new System.Drawing.Size(210, 26);
            this.cboLOAIVE.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(8, 6);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 19);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Loại ghế";
            // 
            // cboLOAIGHE
            // 
            this.cboLOAIGHE.EnterMoveNextControl = true;
            this.cboLOAIGHE.Location = new System.Drawing.Point(76, 3);
            this.cboLOAIGHE.Name = "cboLOAIGHE";
            this.cboLOAIGHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAIGHE.Properties.Appearance.Options.UseFont = true;
            this.cboLOAIGHE.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAIGHE.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLOAIGHE.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLOAIGHE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLOAIGHE.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", 30, "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLOAIGHE.Properties.DisplayMember = "TEN";
            this.cboLOAIGHE.Properties.DropDownItemHeight = 40;
            this.cboLOAIGHE.Properties.NullText = "";
            this.cboLOAIGHE.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLOAIGHE.Properties.ShowHeader = false;
            this.cboLOAIGHE.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLOAIGHE.Properties.ValueMember = "MA";
            this.cboLOAIGHE.Size = new System.Drawing.Size(210, 26);
            this.cboLOAIGHE.TabIndex = 3;
            // 
            // btnLuu1
            // 
            this.btnLuu1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu1.Appearance.Options.UseFont = true;
            this.btnLuu1.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu1.Image")));
            this.btnLuu1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu1.Location = new System.Drawing.Point(76, 68);
            this.btnLuu1.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu1.Name = "btnLuu1";
            this.btnLuu1.Size = new System.Drawing.Size(80, 35);
            this.btnLuu1.TabIndex = 10;
            this.btnLuu1.Text = "&Lưu";
            this.btnLuu1.Click += new System.EventHandler(this.btnLuu1_Click);
            // 
            // btnSua1
            // 
            this.btnSua1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua1.Appearance.Options.UseFont = true;
            this.btnSua1.Image = ((System.Drawing.Image)(resources.GetObject("btnSua1.Image")));
            this.btnSua1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua1.Location = new System.Drawing.Point(252, 68);
            this.btnSua1.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua1.Name = "btnSua1";
            this.btnSua1.Size = new System.Drawing.Size(80, 35);
            this.btnSua1.TabIndex = 12;
            this.btnSua1.Text = "&Sửa";
            this.btnSua1.Click += new System.EventHandler(this.btnSua1_Click);
            // 
            // btnXoa1
            // 
            this.btnXoa1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa1.Appearance.Options.UseFont = true;
            this.btnXoa1.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa1.Image")));
            this.btnXoa1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa1.Location = new System.Drawing.Point(340, 68);
            this.btnXoa1.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa1.Name = "btnXoa1";
            this.btnXoa1.Size = new System.Drawing.Size(80, 35);
            this.btnXoa1.TabIndex = 13;
            this.btnXoa1.Text = "&Xóa";
            this.btnXoa1.Click += new System.EventHandler(this.btnXoa1_Click);
            // 
            // txtGIAVE
            // 
            this.txtGIAVE.EnterMoveNextControl = true;
            this.txtGIAVE.Location = new System.Drawing.Point(76, 35);
            this.txtGIAVE.Name = "txtGIAVE";
            this.txtGIAVE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIAVE.Properties.Appearance.Options.UseFont = true;
            this.txtGIAVE.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIAVE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIAVE.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIAVE.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAVE.Properties.EditFormat.FormatString = "N0";
            this.txtGIAVE.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAVE.Properties.Mask.EditMask = "N0";
            this.txtGIAVE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIAVE.Size = new System.Drawing.Size(495, 26);
            this.txtGIAVE.TabIndex = 9;
            // 
            // btnThem1
            // 
            this.btnThem1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem1.Appearance.Options.UseFont = true;
            this.btnThem1.Image = ((System.Drawing.Image)(resources.GetObject("btnThem1.Image")));
            this.btnThem1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem1.Location = new System.Drawing.Point(164, 68);
            this.btnThem1.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem1.Name = "btnThem1";
            this.btnThem1.Size = new System.Drawing.Size(80, 35);
            this.btnThem1.TabIndex = 11;
            this.btnThem1.Text = "&Thêm";
            this.btnThem1.Click += new System.EventHandler(this.btnThem1_Click);
            // 
            // lblTYGIA
            // 
            this.lblTYGIA.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTYGIA.Location = new System.Drawing.Point(8, 38);
            this.lblTYGIA.Margin = new System.Windows.Forms.Padding(4);
            this.lblTYGIA.Name = "lblTYGIA";
            this.lblTYGIA.Size = new System.Drawing.Size(44, 19);
            this.lblTYGIA.TabIndex = 8;
            this.lblTYGIA.Text = "Giá vé";
            // 
            // Frm_Lapbanggia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 648);
            this.Controls.Add(this.panelControl7);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Lapbanggia";
            this.Text = "Lập bảng giá";
            this.Load += new System.EventHandler(this.Frm_Lapbanggia_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Lapbanggia_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUPP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGPP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhuPhi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMacdinhLV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIVE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIGHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAVE.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit cboLOAIVE;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit cboLOAIGHE;
        private DevExpress.XtraEditors.SimpleButton btnLuu1;
        private DevExpress.XtraEditors.SimpleButton btnSua1;
        private DevExpress.XtraEditors.SimpleButton btnXoa1;
        private DevExpress.XtraEditors.TextEdit txtGIAVE;
        private DevExpress.XtraEditors.SimpleButton btnThem1;
        private DevExpress.XtraEditors.LabelControl lblTYGIA;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.FlowLayoutPanel pnlLOAIVE;
        private System.Windows.Forms.FlowLayoutPanel pnlBANGGIA;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn TENBANGGIA;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAIGHE;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAIVE;
        private DevExpress.XtraGrid.Columns.GridColumn TENTHOIGIAN;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn MALOAIGHE;
        private DevExpress.XtraGrid.Columns.GridColumn MALOAIVE;
        private DevExpress.XtraGrid.Columns.GridColumn MATHOIGIAN;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIEN;
        private DevExpress.XtraEditors.CheckEdit chkMacdinhLV;
        private DevExpress.XtraEditors.SimpleButton btnLuuLV;
        private DevExpress.XtraEditors.SimpleButton btnSuaLV;
        private DevExpress.XtraEditors.SimpleButton btnXoaLV;
        private DevExpress.XtraEditors.SimpleButton btnThemLV;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGLV;
        private DevExpress.XtraEditors.LabelControl lblGHICHULV;
        private DevExpress.XtraEditors.TextEdit txtGHICHULV;
        private DevExpress.XtraEditors.LabelControl lblTENLV;
        private DevExpress.XtraEditors.TextEdit txtTENLV;
        private DevExpress.XtraEditors.SimpleButton btnSaochep;
        private DevExpress.XtraEditors.SimpleButton btnLuutacca;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.FlowLayoutPanel pnlPHUPHI;
        private DevExpress.XtraEditors.TextEdit txtGHICHUPP;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtGiaTri;
        private DevExpress.XtraEditors.SimpleButton btnLuuPP;
        private DevExpress.XtraEditors.SimpleButton btnSuaPP;
        private DevExpress.XtraEditors.SimpleButton btnXoaPP;
        private DevExpress.XtraEditors.SimpleButton btnThemPP;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGPP;
        private DevExpress.XtraEditors.LabelControl lblTENPP;
        private DevExpress.XtraEditors.TextEdit txtTenPhuPhi;




    }
}