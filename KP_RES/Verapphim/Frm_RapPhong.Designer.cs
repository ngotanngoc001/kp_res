﻿namespace KP_RES 
{
    partial class Frm_RapPhong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_RapPhong));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.lblTongghe = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.chkAutoRename = new DevExpress.XtraEditors.CheckEdit();
            this.btnXoataccaghe = new DevExpress.XtraEditors.SimpleButton();
            this.chkAutoselect = new DevExpress.XtraEditors.CheckEdit();
            this.btnXoaghe = new DevExpress.XtraEditors.SimpleButton();
            this.chkChonghe = new DevExpress.XtraEditors.CheckEdit();
            this.btnLuuGhe = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem1ghe = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.lblSoluongday = new DevExpress.XtraEditors.LabelControl();
            this.txtSoluongday = new DevExpress.XtraEditors.TextEdit();
            this.lblSoluongghe = new DevExpress.XtraEditors.LabelControl();
            this.txtSoluongghe = new DevExpress.XtraEditors.TextEdit();
            this.btnThem_ghe = new DevExpress.XtraEditors.SimpleButton();
            this.lblMau3 = new DevExpress.XtraEditors.ColorEdit();
            this.cboLOAIGHE = new DevExpress.XtraEditors.LookUpEdit();
            this.lblLOAIGHE = new DevExpress.XtraEditors.LabelControl();
            this.txtDay = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtY = new DevExpress.XtraEditors.TextEdit();
            this.txtKhoangcach = new DevExpress.XtraEditors.TextEdit();
            this.txtX = new DevExpress.XtraEditors.TextEdit();
            this.txtBatdau = new DevExpress.XtraEditors.TextEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.fpnlLoaiGhe = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblMau_Loaighe = new DevExpress.XtraEditors.ColorEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.cboDonvitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLuu_Loaighe = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua_Loaighe = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa_Loaighe = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem_Loaighe = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGLOAIGHE = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHULOAIGHE = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN_LOAIGHE = new DevExpress.XtraEditors.LabelControl();
            this.txtTENLOAIGHE = new DevExpress.XtraEditors.TextEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pnlPHONG = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuu_Phong = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua_Phong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa_Phong = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem_Phong = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGPHONG = new DevExpress.XtraEditors.CheckEdit();
            this.txtGHICHUPHONG = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTENPHONG = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlDIADIEM = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnKhac = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHUDIADIEM = new DevExpress.XtraEditors.TextEdit();
            this.chkSUDUNGDIADIEM = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTENDIADIEM = new DevExpress.XtraEditors.TextEdit();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuXoa = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuXoatacca = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoRename.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoselect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChonghe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluongday.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluongghe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMau3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIGHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtY.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoangcach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatdau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMau_Loaighe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLOAIGHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULOAIGHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLOAIGHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGPHONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUPHONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENPHONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUDIADIEM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGDIADIEM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENDIADIEM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1267, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 640);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 156);
            this.panelControl3.TabIndex = 20;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 398);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 18;
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 478);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 16;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 162);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 19;
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 82);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 17;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 558);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1267, 640);
            this.panelControl5.TabIndex = 4;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.panelControl13);
            this.panelControl7.Controls.Add(this.panelControl17);
            this.panelControl7.Controls.Add(this.panelControl8);
            this.panelControl7.Controls.Add(this.panelControl6);
            this.panelControl7.Controls.Add(this.panelControl10);
            this.panelControl7.Controls.Add(this.panelControl4);
            this.panelControl7.Controls.Add(this.panelControl1);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(500, 640);
            this.panelControl7.TabIndex = 24;
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.panelControl9);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl13.Location = new System.Drawing.Point(2, 512);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(496, 126);
            this.panelControl13.TabIndex = 7;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.lblTongghe);
            this.panelControl9.Controls.Add(this.panelControl15);
            this.panelControl9.Controls.Add(this.panelControl14);
            this.panelControl9.Controls.Add(this.txtDay);
            this.panelControl9.Controls.Add(this.txtY);
            this.panelControl9.Controls.Add(this.txtKhoangcach);
            this.panelControl9.Controls.Add(this.txtX);
            this.panelControl9.Controls.Add(this.txtBatdau);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(492, 122);
            this.panelControl9.TabIndex = 1;
            // 
            // lblTongghe
            // 
            this.lblTongghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongghe.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTongghe.Location = new System.Drawing.Point(2, 2);
            this.lblTongghe.Name = "lblTongghe";
            this.lblTongghe.Size = new System.Drawing.Size(155, 19);
            this.lblTongghe.TabIndex = 17;
            this.lblTongghe.Text = "Chưa chọn phòng...";
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.chkAutoRename);
            this.panelControl15.Controls.Add(this.btnXoataccaghe);
            this.panelControl15.Controls.Add(this.chkAutoselect);
            this.panelControl15.Controls.Add(this.btnXoaghe);
            this.panelControl15.Controls.Add(this.chkChonghe);
            this.panelControl15.Controls.Add(this.btnLuuGhe);
            this.panelControl15.Controls.Add(this.btnThem1ghe);
            this.panelControl15.Location = new System.Drawing.Point(220, 29);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(267, 165);
            this.panelControl15.TabIndex = 16;
            // 
            // chkAutoRename
            // 
            this.chkAutoRename.Location = new System.Drawing.Point(5, 65);
            this.chkAutoRename.Name = "chkAutoRename";
            this.chkAutoRename.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkAutoRename.Properties.Appearance.Options.UseFont = true;
            this.chkAutoRename.Properties.Caption = "Tự đổi tên ghế";
            this.chkAutoRename.Size = new System.Drawing.Size(211, 24);
            this.chkAutoRename.TabIndex = 16;
            // 
            // btnXoataccaghe
            // 
            this.btnXoataccaghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnXoataccaghe.Appearance.Options.UseFont = true;
            this.btnXoataccaghe.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoataccaghe.Location = new System.Drawing.Point(125, 132);
            this.btnXoataccaghe.Name = "btnXoataccaghe";
            this.btnXoataccaghe.Size = new System.Drawing.Size(137, 31);
            this.btnXoataccaghe.TabIndex = 15;
            this.btnXoataccaghe.Text = "Xóa tất cả ghế";
            this.btnXoataccaghe.Click += new System.EventHandler(this.mnuXoatacca_Click);
            // 
            // chkAutoselect
            // 
            this.chkAutoselect.Location = new System.Drawing.Point(5, 35);
            this.chkAutoselect.Name = "chkAutoselect";
            this.chkAutoselect.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkAutoselect.Properties.Appearance.Options.UseFont = true;
            this.chkAutoselect.Properties.Caption = "Bôi đen chữ khi chọn";
            this.chkAutoselect.Size = new System.Drawing.Size(200, 24);
            this.chkAutoselect.TabIndex = 12;
            // 
            // btnXoaghe
            // 
            this.btnXoaghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnXoaghe.Appearance.Options.UseFont = true;
            this.btnXoaghe.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaghe.Location = new System.Drawing.Point(125, 95);
            this.btnXoaghe.Name = "btnXoaghe";
            this.btnXoaghe.Size = new System.Drawing.Size(137, 31);
            this.btnXoaghe.TabIndex = 14;
            this.btnXoaghe.Text = "Xóa ghế";
            this.btnXoaghe.Click += new System.EventHandler(this.mnuXoa_Click);
            // 
            // chkChonghe
            // 
            this.chkChonghe.Location = new System.Drawing.Point(5, 5);
            this.chkChonghe.Name = "chkChonghe";
            this.chkChonghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkChonghe.Properties.Appearance.Options.UseFont = true;
            this.chkChonghe.Properties.Caption = "Chọn ghế";
            this.chkChonghe.Size = new System.Drawing.Size(102, 24);
            this.chkChonghe.TabIndex = 11;
            this.chkChonghe.CheckedChanged += new System.EventHandler(this.chkChonghe_CheckedChanged);
            // 
            // btnLuuGhe
            // 
            this.btnLuuGhe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnLuuGhe.Appearance.Options.UseFont = true;
            this.btnLuuGhe.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuGhe.Location = new System.Drawing.Point(7, 132);
            this.btnLuuGhe.Name = "btnLuuGhe";
            this.btnLuuGhe.Size = new System.Drawing.Size(112, 31);
            this.btnLuuGhe.TabIndex = 6;
            this.btnLuuGhe.Text = "Lưu";
            this.btnLuuGhe.Click += new System.EventHandler(this.btnLuuGhe_Click);
            // 
            // btnThem1ghe
            // 
            this.btnThem1ghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnThem1ghe.Appearance.Options.UseFont = true;
            this.btnThem1ghe.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem1ghe.Location = new System.Drawing.Point(7, 95);
            this.btnThem1ghe.Name = "btnThem1ghe";
            this.btnThem1ghe.Size = new System.Drawing.Size(112, 31);
            this.btnThem1ghe.TabIndex = 13;
            this.btnThem1ghe.Text = "Thêm ghế";
            this.btnThem1ghe.Click += new System.EventHandler(this.btnThem1ghe_Click);
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.lblSoluongday);
            this.panelControl14.Controls.Add(this.txtSoluongday);
            this.panelControl14.Controls.Add(this.lblSoluongghe);
            this.panelControl14.Controls.Add(this.txtSoluongghe);
            this.panelControl14.Controls.Add(this.btnThem_ghe);
            this.panelControl14.Controls.Add(this.lblMau3);
            this.panelControl14.Controls.Add(this.cboLOAIGHE);
            this.panelControl14.Controls.Add(this.lblLOAIGHE);
            this.panelControl14.Location = new System.Drawing.Point(0, 27);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(217, 165);
            this.panelControl14.TabIndex = 2;
            // 
            // lblSoluongday
            // 
            this.lblSoluongday.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluongday.Location = new System.Drawing.Point(6, 97);
            this.lblSoluongday.Name = "lblSoluongday";
            this.lblSoluongday.Size = new System.Drawing.Size(136, 19);
            this.lblSoluongday.TabIndex = 7;
            this.lblSoluongday.Text = "Số lượng chiều dọc";
            // 
            // txtSoluongday
            // 
            this.txtSoluongday.EditValue = "10";
            this.txtSoluongday.EnterMoveNextControl = true;
            this.txtSoluongday.Location = new System.Drawing.Point(169, 94);
            this.txtSoluongday.Name = "txtSoluongday";
            this.txtSoluongday.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoluongday.Properties.Appearance.Options.UseFont = true;
            this.txtSoluongday.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoluongday.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSoluongday.Properties.MaxLength = 100;
            this.txtSoluongday.Size = new System.Drawing.Size(43, 26);
            this.txtSoluongday.TabIndex = 8;
            // 
            // lblSoluongghe
            // 
            this.lblSoluongghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluongghe.Location = new System.Drawing.Point(6, 64);
            this.lblSoluongghe.Name = "lblSoluongghe";
            this.lblSoluongghe.Size = new System.Drawing.Size(155, 19);
            this.lblSoluongghe.TabIndex = 5;
            this.lblSoluongghe.Text = "Số lượng chiều ngang";
            // 
            // txtSoluongghe
            // 
            this.txtSoluongghe.EditValue = "10";
            this.txtSoluongghe.EnterMoveNextControl = true;
            this.txtSoluongghe.Location = new System.Drawing.Point(169, 62);
            this.txtSoluongghe.Name = "txtSoluongghe";
            this.txtSoluongghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoluongghe.Properties.Appearance.Options.UseFont = true;
            this.txtSoluongghe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoluongghe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSoluongghe.Properties.MaxLength = 100;
            this.txtSoluongghe.Size = new System.Drawing.Size(43, 26);
            this.txtSoluongghe.TabIndex = 6;
            // 
            // btnThem_ghe
            // 
            this.btnThem_ghe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem_ghe.Appearance.Options.UseFont = true;
            this.btnThem_ghe.Image = global::KP_RES.Properties.Resources.hospital_26;
            this.btnThem_ghe.Location = new System.Drawing.Point(42, 132);
            this.btnThem_ghe.Name = "btnThem_ghe";
            this.btnThem_ghe.Size = new System.Drawing.Size(170, 31);
            this.btnThem_ghe.TabIndex = 4;
            this.btnThem_ghe.Text = "&1.Tạo sơ đồ ghế";
            this.btnThem_ghe.Click += new System.EventHandler(this.btnThem_ghe_Click);
            // 
            // lblMau3
            // 
            this.lblMau3.EditValue = System.Drawing.Color.Empty;
            this.lblMau3.EnterMoveNextControl = true;
            this.lblMau3.Location = new System.Drawing.Point(75, 30);
            this.lblMau3.Name = "lblMau3";
            this.lblMau3.Properties.AllowFocused = false;
            this.lblMau3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMau3.Properties.Appearance.Options.UseFont = true;
            this.lblMau3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lblMau3.Properties.ReadOnly = true;
            this.lblMau3.Size = new System.Drawing.Size(137, 26);
            this.lblMau3.TabIndex = 2;
            // 
            // cboLOAIGHE
            // 
            this.cboLOAIGHE.EnterMoveNextControl = true;
            this.cboLOAIGHE.Location = new System.Drawing.Point(75, 5);
            this.cboLOAIGHE.Name = "cboLOAIGHE";
            this.cboLOAIGHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAIGHE.Properties.Appearance.Options.UseFont = true;
            this.cboLOAIGHE.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLOAIGHE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLOAIGHE.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAU", "MAU", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cboLOAIGHE.Properties.DisplayMember = "TEN";
            this.cboLOAIGHE.Properties.ShowHeader = false;
            this.cboLOAIGHE.Properties.ValueMember = "MA";
            this.cboLOAIGHE.Size = new System.Drawing.Size(137, 26);
            this.cboLOAIGHE.TabIndex = 1;
            this.cboLOAIGHE.EditValueChanged += new System.EventHandler(this.cboLOAIGHE_SelectedValueChanged);
            // 
            // lblLOAIGHE
            // 
            this.lblLOAIGHE.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLOAIGHE.Location = new System.Drawing.Point(6, 8);
            this.lblLOAIGHE.Name = "lblLOAIGHE";
            this.lblLOAIGHE.Size = new System.Drawing.Size(60, 19);
            this.lblLOAIGHE.TabIndex = 0;
            this.lblLOAIGHE.Text = "Loại ghế";
            // 
            // txtDay
            // 
            this.txtDay.EditValue = "0";
            this.txtDay.EnterMoveNextControl = true;
            this.txtDay.Location = new System.Drawing.Point(335, 205);
            this.txtDay.Name = "txtDay";
            this.txtDay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtDay.Properties.Appearance.Options.UseFont = true;
            this.txtDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDay.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDay.Properties.Items.AddRange(new object[] {
            "0",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.txtDay.Properties.MaxLength = 100;
            this.txtDay.Size = new System.Drawing.Size(30, 20);
            this.txtDay.TabIndex = 4;
            this.txtDay.Visible = false;
            // 
            // txtY
            // 
            this.txtY.EditValue = "0";
            this.txtY.EnterMoveNextControl = true;
            this.txtY.Location = new System.Drawing.Point(370, 227);
            this.txtY.Name = "txtY";
            this.txtY.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtY.Properties.Appearance.Options.UseFont = true;
            this.txtY.Properties.Appearance.Options.UseTextOptions = true;
            this.txtY.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtY.Properties.MaxLength = 100;
            this.txtY.Size = new System.Drawing.Size(30, 20);
            this.txtY.TabIndex = 7;
            this.txtY.Visible = false;
            // 
            // txtKhoangcach
            // 
            this.txtKhoangcach.EditValue = "3";
            this.txtKhoangcach.EnterMoveNextControl = true;
            this.txtKhoangcach.Location = new System.Drawing.Point(406, 227);
            this.txtKhoangcach.Name = "txtKhoangcach";
            this.txtKhoangcach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtKhoangcach.Properties.Appearance.Options.UseFont = true;
            this.txtKhoangcach.Properties.Appearance.Options.UseTextOptions = true;
            this.txtKhoangcach.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtKhoangcach.Properties.MaxLength = 100;
            this.txtKhoangcach.Size = new System.Drawing.Size(30, 20);
            this.txtKhoangcach.TabIndex = 10;
            this.txtKhoangcach.Visible = false;
            // 
            // txtX
            // 
            this.txtX.EditValue = "0";
            this.txtX.EnterMoveNextControl = true;
            this.txtX.Location = new System.Drawing.Point(370, 205);
            this.txtX.Name = "txtX";
            this.txtX.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtX.Properties.Appearance.Options.UseFont = true;
            this.txtX.Properties.Appearance.Options.UseTextOptions = true;
            this.txtX.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtX.Properties.MaxLength = 100;
            this.txtX.Size = new System.Drawing.Size(30, 20);
            this.txtX.TabIndex = 5;
            this.txtX.ToolTip = "50";
            this.txtX.Visible = false;
            // 
            // txtBatdau
            // 
            this.txtBatdau.EditValue = "1";
            this.txtBatdau.EnterMoveNextControl = true;
            this.txtBatdau.Location = new System.Drawing.Point(406, 205);
            this.txtBatdau.Name = "txtBatdau";
            this.txtBatdau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtBatdau.Properties.Appearance.Options.UseFont = true;
            this.txtBatdau.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBatdau.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBatdau.Properties.MaxLength = 100;
            this.txtBatdau.Size = new System.Drawing.Size(30, 20);
            this.txtBatdau.TabIndex = 8;
            this.txtBatdau.Visible = false;
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.fpnlLoaiGhe);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl17.Location = new System.Drawing.Point(2, 447);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(496, 65);
            this.panelControl17.TabIndex = 6;
            // 
            // fpnlLoaiGhe
            // 
            this.fpnlLoaiGhe.AutoScroll = true;
            this.fpnlLoaiGhe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fpnlLoaiGhe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlLoaiGhe.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlLoaiGhe.Location = new System.Drawing.Point(2, 2);
            this.fpnlLoaiGhe.Name = "fpnlLoaiGhe";
            this.fpnlLoaiGhe.Size = new System.Drawing.Size(492, 61);
            this.fpnlLoaiGhe.TabIndex = 19;
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.labelControl16);
            this.panelControl8.Controls.Add(this.lblMau_Loaighe);
            this.panelControl8.Controls.Add(this.labelControl17);
            this.panelControl8.Controls.Add(this.cboDonvitinh);
            this.panelControl8.Controls.Add(this.btnLuu_Loaighe);
            this.panelControl8.Controls.Add(this.btnSua_Loaighe);
            this.panelControl8.Controls.Add(this.btnXoa_Loaighe);
            this.panelControl8.Controls.Add(this.btnThem_Loaighe);
            this.panelControl8.Controls.Add(this.chkSUDUNGLOAIGHE);
            this.panelControl8.Controls.Add(this.labelControl18);
            this.panelControl8.Controls.Add(this.txtGHICHULOAIGHE);
            this.panelControl8.Controls.Add(this.lblTEN_LOAIGHE);
            this.panelControl8.Controls.Add(this.txtTENLOAIGHE);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 362);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(496, 85);
            this.panelControl8.TabIndex = 5;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(226, 8);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(56, 19);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "Màu sắc";
            // 
            // lblMau_Loaighe
            // 
            this.lblMau_Loaighe.EditValue = System.Drawing.Color.Empty;
            this.lblMau_Loaighe.EnterMoveNextControl = true;
            this.lblMau_Loaighe.Location = new System.Drawing.Point(292, 5);
            this.lblMau_Loaighe.Name = "lblMau_Loaighe";
            this.lblMau_Loaighe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblMau_Loaighe.Properties.Appearance.Options.UseFont = true;
            this.lblMau_Loaighe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lblMau_Loaighe.Size = new System.Drawing.Size(202, 26);
            this.lblMau_Loaighe.TabIndex = 5;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(421, 70);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(31, 19);
            this.labelControl17.TabIndex = 2;
            this.labelControl17.Text = "ĐVT";
            this.labelControl17.Visible = false;
            // 
            // cboDonvitinh
            // 
            this.cboDonvitinh.EnterMoveNextControl = true;
            this.cboDonvitinh.Location = new System.Drawing.Point(452, 69);
            this.cboDonvitinh.Name = "cboDonvitinh";
            this.cboDonvitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.Appearance.Options.UseFont = true;
            this.cboDonvitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDonvitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDonvitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDonvitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboDonvitinh.Properties.DisplayMember = "TEN";
            this.cboDonvitinh.Properties.DropDownItemHeight = 40;
            this.cboDonvitinh.Properties.NullText = "";
            this.cboDonvitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDonvitinh.Properties.ShowHeader = false;
            this.cboDonvitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDonvitinh.Properties.ValueMember = "MA";
            this.cboDonvitinh.Size = new System.Drawing.Size(39, 26);
            this.cboDonvitinh.TabIndex = 3;
            this.cboDonvitinh.Visible = false;
            // 
            // btnLuu_Loaighe
            // 
            this.btnLuu_Loaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu_Loaighe.Appearance.Options.UseFont = true;
            this.btnLuu_Loaighe.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu_Loaighe.Image")));
            this.btnLuu_Loaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu_Loaighe.Location = new System.Drawing.Point(73, 54);
            this.btnLuu_Loaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu_Loaighe.Name = "btnLuu_Loaighe";
            this.btnLuu_Loaighe.Size = new System.Drawing.Size(80, 31);
            this.btnLuu_Loaighe.TabIndex = 9;
            this.btnLuu_Loaighe.Text = "&Lưu";
            this.btnLuu_Loaighe.Click += new System.EventHandler(this.btnLuu_Loaighe_Click);
            // 
            // btnSua_Loaighe
            // 
            this.btnSua_Loaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua_Loaighe.Appearance.Options.UseFont = true;
            this.btnSua_Loaighe.Image = ((System.Drawing.Image)(resources.GetObject("btnSua_Loaighe.Image")));
            this.btnSua_Loaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua_Loaighe.Location = new System.Drawing.Point(249, 54);
            this.btnSua_Loaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua_Loaighe.Name = "btnSua_Loaighe";
            this.btnSua_Loaighe.Size = new System.Drawing.Size(80, 31);
            this.btnSua_Loaighe.TabIndex = 11;
            this.btnSua_Loaighe.Text = "&Sửa";
            this.btnSua_Loaighe.Click += new System.EventHandler(this.btnSua_Loaighe_Click);
            // 
            // btnXoa_Loaighe
            // 
            this.btnXoa_Loaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa_Loaighe.Appearance.Options.UseFont = true;
            this.btnXoa_Loaighe.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa_Loaighe.Image")));
            this.btnXoa_Loaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa_Loaighe.Location = new System.Drawing.Point(337, 54);
            this.btnXoa_Loaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa_Loaighe.Name = "btnXoa_Loaighe";
            this.btnXoa_Loaighe.Size = new System.Drawing.Size(80, 31);
            this.btnXoa_Loaighe.TabIndex = 12;
            this.btnXoa_Loaighe.Text = "&Xóa";
            this.btnXoa_Loaighe.Click += new System.EventHandler(this.btnXoa_Loaighe_Click);
            // 
            // btnThem_Loaighe
            // 
            this.btnThem_Loaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem_Loaighe.Appearance.Options.UseFont = true;
            this.btnThem_Loaighe.Image = ((System.Drawing.Image)(resources.GetObject("btnThem_Loaighe.Image")));
            this.btnThem_Loaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem_Loaighe.Location = new System.Drawing.Point(161, 54);
            this.btnThem_Loaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem_Loaighe.Name = "btnThem_Loaighe";
            this.btnThem_Loaighe.Size = new System.Drawing.Size(80, 31);
            this.btnThem_Loaighe.TabIndex = 10;
            this.btnThem_Loaighe.Text = "&Thêm";
            this.btnThem_Loaighe.Click += new System.EventHandler(this.btnThem_Loaighe_Click);
            // 
            // chkSUDUNGLOAIGHE
            // 
            this.chkSUDUNGLOAIGHE.EnterMoveNextControl = true;
            this.chkSUDUNGLOAIGHE.Location = new System.Drawing.Point(71, 30);
            this.chkSUDUNGLOAIGHE.Name = "chkSUDUNGLOAIGHE";
            this.chkSUDUNGLOAIGHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGLOAIGHE.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGLOAIGHE.Properties.Caption = "Sử dụng";
            this.chkSUDUNGLOAIGHE.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGLOAIGHE.TabIndex = 8;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(399, 43);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(54, 19);
            this.labelControl18.TabIndex = 6;
            this.labelControl18.Text = "Ghi chú";
            this.labelControl18.Visible = false;
            // 
            // txtGHICHULOAIGHE
            // 
            this.txtGHICHULOAIGHE.EnterMoveNextControl = true;
            this.txtGHICHULOAIGHE.Location = new System.Drawing.Point(452, 37);
            this.txtGHICHULOAIGHE.Name = "txtGHICHULOAIGHE";
            this.txtGHICHULOAIGHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHULOAIGHE.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHULOAIGHE.Size = new System.Drawing.Size(39, 26);
            this.txtGHICHULOAIGHE.TabIndex = 7;
            this.txtGHICHULOAIGHE.Visible = false;
            // 
            // lblTEN_LOAIGHE
            // 
            this.lblTEN_LOAIGHE.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN_LOAIGHE.Location = new System.Drawing.Point(6, 8);
            this.lblTEN_LOAIGHE.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN_LOAIGHE.Name = "lblTEN_LOAIGHE";
            this.lblTEN_LOAIGHE.Size = new System.Drawing.Size(60, 19);
            this.lblTEN_LOAIGHE.TabIndex = 0;
            this.lblTEN_LOAIGHE.Text = "Loại ghế";
            // 
            // txtTENLOAIGHE
            // 
            this.txtTENLOAIGHE.EnterMoveNextControl = true;
            this.txtTENLOAIGHE.Location = new System.Drawing.Point(73, 5);
            this.txtTENLOAIGHE.Name = "txtTENLOAIGHE";
            this.txtTENLOAIGHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENLOAIGHE.Properties.Appearance.Options.UseFont = true;
            this.txtTENLOAIGHE.Size = new System.Drawing.Size(146, 26);
            this.txtTENLOAIGHE.TabIndex = 1;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.pnlPHONG);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 237);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(496, 125);
            this.panelControl6.TabIndex = 4;
            // 
            // pnlPHONG
            // 
            this.pnlPHONG.AutoScroll = true;
            this.pnlPHONG.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPHONG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPHONG.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlPHONG.Location = new System.Drawing.Point(2, 2);
            this.pnlPHONG.Name = "pnlPHONG";
            this.pnlPHONG.Size = new System.Drawing.Size(492, 121);
            this.pnlPHONG.TabIndex = 20;
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.btnLuu_Phong);
            this.panelControl10.Controls.Add(this.btnSua_Phong);
            this.panelControl10.Controls.Add(this.btnXoa_Phong);
            this.panelControl10.Controls.Add(this.btnThem_Phong);
            this.panelControl10.Controls.Add(this.chkSUDUNGPHONG);
            this.panelControl10.Controls.Add(this.txtGHICHUPHONG);
            this.panelControl10.Controls.Add(this.labelControl1);
            this.panelControl10.Controls.Add(this.txtTENPHONG);
            this.panelControl10.Controls.Add(this.labelControl4);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(2, 152);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(496, 85);
            this.panelControl10.TabIndex = 3;
            // 
            // btnLuu_Phong
            // 
            this.btnLuu_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu_Phong.Appearance.Options.UseFont = true;
            this.btnLuu_Phong.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu_Phong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu_Phong.Location = new System.Drawing.Point(63, 54);
            this.btnLuu_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu_Phong.Name = "btnLuu_Phong";
            this.btnLuu_Phong.Size = new System.Drawing.Size(80, 31);
            this.btnLuu_Phong.TabIndex = 23;
            this.btnLuu_Phong.Text = "&Lưu";
            this.btnLuu_Phong.Click += new System.EventHandler(this.btnLuu_Phong_Click);
            // 
            // btnSua_Phong
            // 
            this.btnSua_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua_Phong.Appearance.Options.UseFont = true;
            this.btnSua_Phong.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua_Phong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua_Phong.Location = new System.Drawing.Point(239, 54);
            this.btnSua_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua_Phong.Name = "btnSua_Phong";
            this.btnSua_Phong.Size = new System.Drawing.Size(80, 31);
            this.btnSua_Phong.TabIndex = 25;
            this.btnSua_Phong.Text = "&Sửa";
            this.btnSua_Phong.Click += new System.EventHandler(this.btnSua_Phong_Click);
            // 
            // btnXoa_Phong
            // 
            this.btnXoa_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa_Phong.Appearance.Options.UseFont = true;
            this.btnXoa_Phong.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa_Phong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa_Phong.Location = new System.Drawing.Point(327, 54);
            this.btnXoa_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa_Phong.Name = "btnXoa_Phong";
            this.btnXoa_Phong.Size = new System.Drawing.Size(80, 31);
            this.btnXoa_Phong.TabIndex = 26;
            this.btnXoa_Phong.Text = "&Xóa";
            this.btnXoa_Phong.Click += new System.EventHandler(this.btnXoa_Phong_Click);
            // 
            // btnThem_Phong
            // 
            this.btnThem_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem_Phong.Appearance.Options.UseFont = true;
            this.btnThem_Phong.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem_Phong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem_Phong.Location = new System.Drawing.Point(151, 54);
            this.btnThem_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem_Phong.Name = "btnThem_Phong";
            this.btnThem_Phong.Size = new System.Drawing.Size(80, 31);
            this.btnThem_Phong.TabIndex = 24;
            this.btnThem_Phong.Text = "&Thêm";
            this.btnThem_Phong.Click += new System.EventHandler(this.btnThem_Phong_Click);
            // 
            // chkSUDUNGPHONG
            // 
            this.chkSUDUNGPHONG.EnterMoveNextControl = true;
            this.chkSUDUNGPHONG.Location = new System.Drawing.Point(56, 30);
            this.chkSUDUNGPHONG.Name = "chkSUDUNGPHONG";
            this.chkSUDUNGPHONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGPHONG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGPHONG.Properties.Caption = "Sử dụng";
            this.chkSUDUNGPHONG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGPHONG.TabIndex = 11;
            // 
            // txtGHICHUPHONG
            // 
            this.txtGHICHUPHONG.EnterMoveNextControl = true;
            this.txtGHICHUPHONG.Location = new System.Drawing.Point(280, 4);
            this.txtGHICHUPHONG.Name = "txtGHICHUPHONG";
            this.txtGHICHUPHONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUPHONG.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUPHONG.Size = new System.Drawing.Size(214, 26);
            this.txtGHICHUPHONG.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 7);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(45, 19);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Phòng";
            // 
            // txtTENPHONG
            // 
            this.txtTENPHONG.EnterMoveNextControl = true;
            this.txtTENPHONG.Location = new System.Drawing.Point(58, 4);
            this.txtTENPHONG.Name = "txtTENPHONG";
            this.txtTENPHONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENPHONG.Properties.Appearance.Options.UseFont = true;
            this.txtTENPHONG.Size = new System.Drawing.Size(154, 26);
            this.txtTENPHONG.TabIndex = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(219, 7);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 19);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Ghi chú";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlDIADIEM);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 87);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(496, 65);
            this.panelControl4.TabIndex = 2;
            // 
            // pnlDIADIEM
            // 
            this.pnlDIADIEM.AutoScroll = true;
            this.pnlDIADIEM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlDIADIEM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDIADIEM.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlDIADIEM.Location = new System.Drawing.Point(2, 2);
            this.pnlDIADIEM.Name = "pnlDIADIEM";
            this.pnlDIADIEM.Size = new System.Drawing.Size(492, 61);
            this.pnlDIADIEM.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnKhac);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtGHICHUDIADIEM);
            this.panelControl1.Controls.Add(this.chkSUDUNGDIADIEM);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtTENDIADIEM);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(496, 85);
            this.panelControl1.TabIndex = 1;
            // 
            // btnKhac
            // 
            this.btnKhac.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhac.Appearance.Options.UseFont = true;
            this.btnKhac.Image = global::KP_RES.Properties.Resources.ie_26;
            this.btnKhac.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnKhac.Location = new System.Drawing.Point(392, 54);
            this.btnKhac.Margin = new System.Windows.Forms.Padding(4);
            this.btnKhac.Name = "btnKhac";
            this.btnKhac.Size = new System.Drawing.Size(80, 31);
            this.btnKhac.TabIndex = 23;
            this.btnKhac.Text = "&Web";
            this.btnKhac.Click += new System.EventHandler(this.btnKhac_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(40, 54);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 31);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(216, 54);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 31);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(304, 54);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 31);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(128, 54);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 31);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(219, 7);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Ghi chú";
            // 
            // txtGHICHUDIADIEM
            // 
            this.txtGHICHUDIADIEM.EnterMoveNextControl = true;
            this.txtGHICHUDIADIEM.Location = new System.Drawing.Point(280, 4);
            this.txtGHICHUDIADIEM.Name = "txtGHICHUDIADIEM";
            this.txtGHICHUDIADIEM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUDIADIEM.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUDIADIEM.Size = new System.Drawing.Size(214, 26);
            this.txtGHICHUDIADIEM.TabIndex = 3;
            // 
            // chkSUDUNGDIADIEM
            // 
            this.chkSUDUNGDIADIEM.EnterMoveNextControl = true;
            this.chkSUDUNGDIADIEM.Location = new System.Drawing.Point(38, 30);
            this.chkSUDUNGDIADIEM.Name = "chkSUDUNGDIADIEM";
            this.chkSUDUNGDIADIEM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGDIADIEM.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGDIADIEM.Properties.Caption = "Sử dụng";
            this.chkSUDUNGDIADIEM.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGDIADIEM.TabIndex = 4;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 7);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(27, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Rạp";
            // 
            // txtTENDIADIEM
            // 
            this.txtTENDIADIEM.EnterMoveNextControl = true;
            this.txtTENDIADIEM.Location = new System.Drawing.Point(40, 4);
            this.txtTENDIADIEM.Name = "txtTENDIADIEM";
            this.txtTENDIADIEM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENDIADIEM.Properties.Appearance.Options.UseFont = true;
            this.txtTENDIADIEM.Size = new System.Drawing.Size(172, 26);
            this.txtTENDIADIEM.TabIndex = 1;
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.panelControl12);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl11.Location = new System.Drawing.Point(500, 0);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(767, 640);
            this.panelControl11.TabIndex = 26;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.panel1);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(763, 636);
            this.panelControl12.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(759, 632);
            this.panel1.TabIndex = 2;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuXoa,
            this.mnuXoatacca});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(127, 48);
            // 
            // mnuXoa
            // 
            this.mnuXoa.Image = global::KP_RES.Properties.Resources.multiply_26;
            this.mnuXoa.Name = "mnuXoa";
            this.mnuXoa.Size = new System.Drawing.Size(126, 22);
            this.mnuXoa.Text = "Xóa";
            this.mnuXoa.Click += new System.EventHandler(this.mnuXoa_Click);
            // 
            // mnuXoatacca
            // 
            this.mnuXoatacca.Image = global::KP_RES.Properties.Resources.multiply_26;
            this.mnuXoatacca.Name = "mnuXoatacca";
            this.mnuXoatacca.Size = new System.Drawing.Size(126, 22);
            this.mnuXoatacca.Text = "Xóa tất cả";
            this.mnuXoatacca.Click += new System.EventHandler(this.mnuXoatacca_Click);
            // 
            // Frm_RapPhong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1306, 640);
            this.Controls.Add(this.panelControl11);
            this.Controls.Add(this.panelControl7);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl2);
            this.Name = "Frm_RapPhong";
            this.Text = "Rạp - Phòng - Loại ghế - Sơ đồ ghế";
            this.Load += new System.EventHandler(this.Frm_RapPhong_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_RapPhong_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoRename.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoselect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChonghe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluongday.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoluongghe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMau3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAIGHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtY.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKhoangcach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatdau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMau_Loaighe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLOAIGHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULOAIGHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLOAIGHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGPHONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUPHONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENPHONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUDIADIEM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGDIADIEM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENDIADIEM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.FlowLayoutPanel pnlDIADIEM;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtGHICHUDIADIEM;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGDIADIEM;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTENDIADIEM;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGPHONG;
        private DevExpress.XtraEditors.TextEdit txtGHICHUPHONG;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtTENPHONG;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnLuu_Phong;
        private DevExpress.XtraEditors.SimpleButton btnSua_Phong;
        private DevExpress.XtraEditors.SimpleButton btnXoa_Phong;
        private DevExpress.XtraEditors.SimpleButton btnThem_Phong;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.ColorEdit lblMau_Loaighe;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LookUpEdit cboDonvitinh;
        private DevExpress.XtraEditors.SimpleButton btnLuu_Loaighe;
        private DevExpress.XtraEditors.SimpleButton btnSua_Loaighe;
        private DevExpress.XtraEditors.SimpleButton btnXoa_Loaighe;
        private DevExpress.XtraEditors.SimpleButton btnThem_Loaighe;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGLOAIGHE;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtGHICHULOAIGHE;
        private DevExpress.XtraEditors.LabelControl lblTEN_LOAIGHE;
        private DevExpress.XtraEditors.TextEdit txtTENLOAIGHE;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private System.Windows.Forms.FlowLayoutPanel fpnlLoaiGhe;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuXoa;
        private System.Windows.Forms.ToolStripMenuItem mnuXoatacca;
        private System.Windows.Forms.FlowLayoutPanel pnlPHONG;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.ComboBoxEdit txtDay;
        private DevExpress.XtraEditors.LabelControl lblSoluongghe;
        private DevExpress.XtraEditors.TextEdit txtKhoangcach;
        private DevExpress.XtraEditors.TextEdit txtSoluongghe;
        private DevExpress.XtraEditors.ColorEdit lblMau3;
        private DevExpress.XtraEditors.LookUpEdit cboLOAIGHE;
        private DevExpress.XtraEditors.LabelControl lblLOAIGHE;
        private DevExpress.XtraEditors.TextEdit txtBatdau;
        private DevExpress.XtraEditors.TextEdit txtY;
        private DevExpress.XtraEditors.TextEdit txtX;
        private DevExpress.XtraEditors.SimpleButton btnLuuGhe;
        private DevExpress.XtraEditors.SimpleButton btnThem_ghe;
        private DevExpress.XtraEditors.LabelControl lblSoluongday;
        private DevExpress.XtraEditors.TextEdit txtSoluongday;
        private DevExpress.XtraEditors.CheckEdit chkChonghe;
        private DevExpress.XtraEditors.CheckEdit chkAutoselect;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.SimpleButton btnXoataccaghe;
        private DevExpress.XtraEditors.SimpleButton btnXoaghe;
        private DevExpress.XtraEditors.SimpleButton btnThem1ghe;
        private DevExpress.XtraEditors.CheckEdit chkAutoRename;
        private DevExpress.XtraEditors.SimpleButton btnKhac;
        private DevExpress.XtraEditors.LabelControl lblTongghe;




    }
}