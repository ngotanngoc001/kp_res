﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Collections;

namespace KP_RES 
{
    public partial class Frm_Lichchieu : DevExpress.XtraEditors.XtraForm
    {
        bool bUpdateweb = false;
        public Frm_Lichchieu()
        {
            InitializeComponent();
        }

        private void Frm_Lichchieu_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void Frm_Lichchieu_Shown(object sender, EventArgs e)
        {
            LoadPermission();
            LoaddataGridView();
            pnlThemlichchieu.Height = 0;
            dtpNgay.EditValue = clsGlobal.gdServerDate;
            DateTime dtime = (DateTime)dtpNgay.EditValue;
            lblNgay.Text = cls_KP_RES.GetThuofDateTime(dtime);
            this.ParentForm.FormClosing += new FormClosingEventHandler(Frm_Lichchieu_FormClosing);
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            try
            {
                splashScreenManager2.ShowWaitForm();
                string sMarap_old = smaRap;
                LoaddataGridView();
                bLoadTongghe_Veban = true;
                foreach (SimpleButton btn1 in pnlRap.Controls)
                {
                    if (btn1.Name.Substring(1, btn1.Name.Length - 1).ToString() == sMarap_old)
                    {
                        Rap_Click(btn1, null);
                        break;
                    }
                }
                splashScreenManager2.CloseWaitForm();
            }
            catch
            {
            }
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("79");
            btnSua.Enabled = clsUserManagement.AllowEdit("79");
            btnXoa.Enabled = clsUserManagement.AllowDelete("79");
            btnXoahet.Enabled = clsUserManagement.AllowDelete("79");
            btnCopy.Enabled = clsUserManagement.AllowAdd("79");
        }

        private void LoaddataGridView()
        {
            LoaddataGridView_Rap();
            LoaddataGridView_Phim();
            LoaddataGridView_Giochieu();
            LoaddataGridView_Chatluongphim();
            LoaddataGridView_Banggia();
            //dtpNgay.EditValue = clsGlobal.gdServerDate;
        }

        string smaRap = "";
        DataTable dtRap = new DataTable();
        private void LoaddataGridView_Rap()
        {
            smaRap = "";
            string sSQL = "";
            sSQL += "Select MADIADIEM As MA,TENDIADIEM As TEN" + "\n";
            sSQL += "From DM_DIADIEM" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by TEN" + "\n";
            dtRap = clsMain.ReturnDataTable(sSQL);
            pnlRap.Controls.Clear();
            foreach (DataRow dr in dtRap.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
                btn.Width = 110;
                btn.Height = 50;
                btn.Font = pnlRap.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Name = "R" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Rap_Click);
                pnlRap.Controls.Add(btn);
            }
        }

        private void Rap_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlRap.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnThem.ForeColor;
                }
            }
            smaRap= btn.Name.Substring(1, btn.Name.Length - 1);
            LoadLichChieu(smaRap, dtpNgay.Text);
            LoaddataGridView_Phong(smaRap);
        }

        DataTable dtLichchieu = new DataTable();
        int iscoll = 0;
        bool bLoadTongghe_Veban = false;
        private void LoadLichChieu(string maRap, string ngay)
        {
            iscoll = pnlListPhong.AutoScrollPosition.Y ;
            smaLichchieu = "";
            amalichchieu.Clear();
            pnlListPhong.Controls.Clear();
            if (dtLichchieu.Rows.Count > 0)
            {
                dtLichchieu.Clear();
            }
            string sSQL = "";
            DataTable dtPHONG = new DataTable();
            sSQL += "EXECUTE SP_VE_LICHCHIEU_LOADPHONG " +clsMain.SQLString(maRap) + "\n";
            dtPHONG = clsMain.ReturnDataTable(sSQL);

            DataTable dtLichchieu_Full = new DataTable();
            if (dtPHONG.Rows.Count > 0)
            {
                sSQL = "";
                if (bLoadTongghe_Veban )
                {
                    sSQL += "EXECUTE SP_VE_LICHCHIEU_LOADPHONG_LICHCHIEU " + clsMain.SQLString(ngay) + "," + clsMain.SQLString("0") + "\n";
                    bLoadTongghe_Veban = false;
                }
                else
                {
                    sSQL += "EXECUTE SP_VE_LICHCHIEU_LOADPHONG_LICHCHIEU1 " + clsMain.SQLString(ngay) + "," + clsMain.SQLString("0") + "\n";
                }
                dtLichchieu_Full = clsMain.ReturnDataTable(sSQL);
            }

            foreach (DataRow dr in dtPHONG.Rows)
            {
                SimpleButton pnlSub = new SimpleButton();
                pnlSub.Dock = DockStyle.Left;
                pnlSub.Width = 52;
                pnlSub.Font = pnlRap.Font;
                pnlSub.Text = dr["TEN"].ToString() + "\n" +"(" +dr["SOLUONGGHE"].ToString()+")";

                PanelControl fpnlSub = new PanelControl();
                fpnlSub.Dock = DockStyle.Fill;
                fpnlSub.BorderStyle =DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;

                dtLichchieu_Full.DefaultView.RowFilter = "MAPHONG=" + dr["MA"].ToString();
                DataTable dt = dtLichchieu_Full.DefaultView .ToTable ();

                if (dtLichchieu.Rows.Count == 0)
                {
                    dtLichchieu = dt.Copy();
                    dtLichchieu.Clear();
                }
                foreach (DataRow dr1 in dt.Rows)
                {
                    dtLichchieu.Rows.Add(dr1.ItemArray);
                    int iTongsoghe = int.Parse(dr1["TONGGHE"].ToString());
                    int iTongsoghe_ban = int.Parse(dr1["GHEDABAN"].ToString());

                    SimpleButton btn = new SimpleButton();
                    btn.Font = pnlListPhong.Font;// pnlRap.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.Tag = dr1["MALICHCHIEU"].ToString();
                    if (iTongsoghe == 0)
                    {
                        btn.Text = dr1["TENPHIM"].ToString() + "\n" + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "(" + dr1["TENBANGGIA"].ToString() + ")" ;
                        btn.ToolTip = "Phim : " + dr1["TENPHIM"].ToString() + "\n" + "Giờ chiếu : " + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "Bảng giá : " + dr1["TENBANGGIA"].ToString() ;
                    }
                    else
                    {
                        btn.Text = dr1["TENPHIM"].ToString() + "\n" + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "(" + dr1["TENBANGGIA"].ToString() + ")" + "\n" + iTongsoghe_ban.ToString() + "/" + iTongsoghe.ToString();
                        btn.ToolTip = "Phim : " + dr1["TENPHIM"].ToString() + "\n" + "Giờ chiếu : " + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "Bảng giá : " + dr1["TENBANGGIA"].ToString() + "\n" + "Số ghế bán được  : " + iTongsoghe_ban.ToString() + "/" + iTongsoghe.ToString();
                    }
                    btn.Width = int.Parse(dr1["THOILUONG"].ToString());
                    btn.Height = 96;
                    foreach (LabelControl lblGio in pnlGio.Controls)
                    {
                        if (lblGio.Text == dr1["BATDAU"].ToString().Substring(0, 2))
                        {
                            btn.Location = new Point(lblGio.Location.X + int.Parse(dr1["BATDAU"].ToString().Substring(3, 2)), 0);
                            break;
                        }
                    }
                    if (!bool.Parse(dr1["SUDUNG"].ToString()))
                    {
                        btn.Font = btnPhimkosudung.Appearance.Font;
                    }
                    btn.Click += new EventHandler(btn_Click);
                    btn.ContextMenuStrip = contextMenuStrip;
                    fpnlSub.Controls.Add(btn);
                }

                PanelControl pnl = new PanelControl();
                pnl.Dock = DockStyle.Top;
                pnl.Height = 100;
                pnl.Controls.Add(fpnlSub);
                pnl.Controls.Add(pnlSub);
                pnlListPhong.Controls.Add(pnl);
            }
            pnlListPhong.AutoScrollPosition = new Point(0, Math.Abs(iscoll));
        }
     
        string smaLichchieu = "";
        ArrayList amalichchieu = new ArrayList();
        private void btn_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            //Chon nhieu xuat chieu
            if (Form.ModifierKeys == Keys.Control)
            {
                if (btn.ForeColor == Color.Red)
                {
                    btn.ForeColor = btnThem.ForeColor;
                    amalichchieu.Remove(btn.Tag.ToString());
                }
                else
                {
                    btn.ForeColor = Color.Red;
                    amalichchieu.Add(btn.Tag.ToString());
                }
                if (smaLichchieu != "")
                {
                    amalichchieu.Add(smaLichchieu);
                    smaLichchieu = "";
                }
            }
            else
            {
                amalichchieu.Clear();
                btn.ForeColor = Color.Red;
                foreach (Control ctl in pnlListPhong.Controls)
                {
                    foreach (Control ctl1 in ctl.Controls)
                    {
                        foreach (Control ctl2 in ctl1.Controls)
                        {
                            if (ctl2.Tag.ToString() != btn.Tag.ToString())
                            {
                                ctl2.ForeColor = btnThem.ForeColor;
                            }
                        }
                    }
                }
                smaLichchieu = btn.Tag.ToString();
            }
        }

        private void dtpNgay_EditValueChanged(object sender, EventArgs e)
        {
            LoadLichChieu(smaRap, dtpNgay.Text);
            DateTime dtime = (DateTime)dtpNgay.EditValue;
            lblNgay.Text = cls_KP_RES.GetThuofDateTime(dtime);
            LoaddataGridView_Ngaychieu(dtime);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            dtpNgay.EditValue = ((DateTime)dtpNgay.EditValue).AddDays(1);
        }

        private void btnPre_Click(object sender, EventArgs e)
        {
            dtpNgay.EditValue = ((DateTime)dtpNgay.EditValue).AddDays(-1);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            splashScreenManager2.ShowWaitForm();
            LoaddataGridView_Phong(smaRap);
            LoaddataGridView_Phim();
            LoaddataGridView_Ngaychieu((DateTime)dtpNgay.EditValue);
            LoaddataGridView_Giochieu();
            LoaddataGridView_Chatluongphim();
            LoaddataGridView_Banggia();
            bSua = false;
            pnlThemlichchieu.Height = 320;
            splashScreenManager2.CloseWaitForm();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            if (amalichchieu.Count > 0)
            {
                foreach (object obj in amalichchieu)
                {
                    smaLichchieu = obj.ToString();
                    if (!CheckDelete())
                    {
                        return;
                    }
                }
                foreach (object obj in amalichchieu)
                {
                    sSQL += "Update LICHCHIEU" + "\n";
                    sSQL += "Set  SUDUNG=0,XOA=1" + "\n";
                    sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(obj.ToString()) + "\n";
                    sSQL += LSLichChieu(obj.ToString(), "3");
                }
            }
            else
            {
                if (smaLichchieu == "")//Chưa chọn dòng để xóa
                {
                    XtraMessageBox.Show("Chưa chọn xuất chiếu để xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (!CheckDelete())
                {
                    return;
                }
                sSQL += "Update LICHCHIEU" + "\n";
                sSQL += "Set  SUDUNG=0,XOA=1" + "\n";
                sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(smaLichchieu) + "\n";
                sSQL += LSLichChieu(smaLichchieu, "3");
            }
            
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                //19-05-2017 tu update web khi luu
                //btnWeb_Click(null, null);
                bUpdateweb = true;
                LoadLichChieu(smaRap, dtpNgay.Text);
            }
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        Boolean CheckDelete()
        {
            string sSQL = "";
            sSQL += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
            sSQL += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
            sSQL += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
            sSQL += "And A.TRANGTHAI = " + clsMain.SQLString("1") + "\n";
            sSQL += "Union" + "\n";
            sSQL += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
            sSQL += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
            sSQL += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
            sSQL += "And B.NGAYCHIEU < " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
            if (clsMain.ReturnDataTable(sSQL).Rows.Count != 0)
            {
                XtraMessageBox.Show("Lịch chiếu đã được bán vé.Không thể thay đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaLichchieu == "")//Chưa chọn dòng để sữa
            {
                XtraMessageBox.Show("Chưa chọn xuất chiếu để sữa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!CheckDelete())
                return;

            splashScreenManager2.ShowWaitForm();
            LoaddataGridView_Phong(smaRap);
            LoaddataGridView_Phim();
            LoaddataGridView_Ngaychieu((DateTime)dtpNgay.EditValue);
            LoaddataGridView_Giochieu();
            LoaddataGridView_Chatluongphim();
            LoaddataGridView_Banggia();

            DataRow dr = dtLichchieu.Select("MALICHCHIEU=" + clsMain.SQLString(smaLichchieu))[0];

            foreach (SimpleButton btn in pnlPHONG.Controls)
            {
                if (btn.Name.Substring(1, btn.Name.Length - 1) == dr["MAPHONG"].ToString ())
                {
                    btn.ForeColor =Color .Red ; 
                }
                else 
                {
                     btn.ForeColor =btnCapnhat.ForeColor;
                }
            }
            smaPHONG = dr["MAPHONG"].ToString();

            foreach (SimpleButton btn in pnlPHIM.Controls)
            {
                if (btn.Name.Substring(1, btn.Name.Length - 1) == dr["MAPHIM"].ToString())
                {
                    btn.ForeColor = Color.Red;
                }
                else
                {
                    btn.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaPHIM = dr["MAPHIM"].ToString();
            iThoiluong = int.Parse(dr["THOILUONG"].ToString());

            foreach (SimpleButton btn in pnlChatluongphim.Controls)
            {
                if (btn.Name.Substring(1, btn.Name.Length - 1) == dr["MACHATLUONGPHIM"].ToString())
                {
                    btn.ForeColor = Color.Red;
                }
                else
                {
                    btn.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaChatluongphim = dr["MACHATLUONGPHIM"].ToString();

            foreach (SimpleButton btn in pnlBanggia.Controls)
            {
                if (btn.Name.Substring(1, btn.Name.Length - 1) == dr["MABANGGIA"].ToString())
                {
                    btn.ForeColor = Color.Red;
                }
                else
                {
                    btn.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaBanggia = dr["MABANGGIA"].ToString();

            DateTime dtime = (DateTime)dtpNgay.EditValue;
            foreach (SimpleButton btn in pnlNgaychieu.Controls)
            {
                if (btn.Tag.ToString ()== cls_KP_RES.DateToString_Date(dtime) )
                {
                    btn.ForeColor = Color.Red;
                }
                else
                {
                    btn.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaNgaychieu.Add( cls_KP_RES.DateToString_Date(dtime));

            foreach (PanelControl pnl in pnlGiochieu.Controls)
            {
                foreach (MaskedTextBox btn in pnl.Controls)
                {
                    if (btn.BackColor == Color.LimeGreen)
                    {
                        btn.Text = dr["BATDAU"].ToString();
                        break;
                    }
                }
                break;
            }
            smaGiochieu.Add(dr["BATDAU"].ToString());
            bSua = true;

            chkSUDUNG .Checked = bool .Parse (dr["SUDUNG"].ToString());
            pnlThemlichchieu.Height = 320;
            splashScreenManager2.CloseWaitForm();
        }

        private void btnAn_Click(object sender, EventArgs e)
        {
            pnlThemlichchieu.Height = 0;
            bSua = false;
        }

        string smaPHIM = "";
        int iThoiluong = 0;
        DataTable dtPHIM = new DataTable();
        private void LoaddataGridView_Phim()
        {
            if (pnlPHIM.Visible)
            {
                smaPHIM = "";
                string sSQL = "";
                if (chkFast.Checked)
                {
                    sSQL += "Select Top 100 MAPHIM As MA,TENPHIM As TEN,THOILUONG" + "\n";
                }
                else
                {
                    sSQL += "Select MAPHIM As MA,TENPHIM As TEN,THOILUONG" + "\n";
                }
                sSQL += "From DM_PHIM" + "\n";
                sSQL += "Where SUDUNG=1" + "\n";
                if (chkFast.Checked)
                {
                    sSQL += "Order by MA DESC" + "\n";
                }
                else
                {
                    sSQL += "Order by TEN" + "\n";
                }

                dtPHIM = clsMain.ReturnDataTable(sSQL);
                dtPHIM.DefaultView.Sort = "TEN ASC";
                dtPHIM = dtPHIM.DefaultView.ToTable();

                pnlPHIM.Controls.Clear();
                mnuPhim.DropDownItems.Clear();
               
                foreach (DataRow dr in dtPHIM.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 45;
                    btn.Font = pnlRap.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.Name = "P" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    btn.Tag = dr["THOILUONG"].ToString();
                    btn.Click += new EventHandler(PHIM_Click);
                    pnlPHIM.Controls.Add(btn);
                    mnuPhim.DropDownItems.Add(dr["TEN"].ToString(), null, new EventHandler(PhimMenu_Click));
                }
            }
        }
        private void PHIM_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlPHIM.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaPHIM = btn.Name.Substring(1, btn.Name.Length - 1);
            iThoiluong = int.Parse(btn.Tag.ToString());

            foreach (PanelControl pnl in pnlGiochieu.Controls)
            {
                foreach (MaskedTextBox mtx in pnl.Controls)
                {
                    if (mtx.BackColor == Color.LimeGreen)
                    {
                        string str = mtx.Text ;
                        mtx.Text = "00:00";
                        mtx.Text = str;
                        break;
                    }
                }
            }

        }

        ArrayList smaNgaychieu = new ArrayList();
        DataTable dtNgaychieu = new DataTable();
        private void LoaddataGridView_Ngaychieu(DateTime dti)
        {
            smaNgaychieu.Clear();
            string sSQL = "";
            int i = ( dti-clsGlobal.gdServerDate).Days;
            sSQL += "select getdate() +" + i.ToString() + "+0 as NGAY1,getdate()+" + i.ToString() + "+1 as NGAY2,getdate()+" + i.ToString() + "+ 2 as NGAY3,getdate()+" + i.ToString() + "+3 as NGAY4,getdate()+" + i.ToString() + "+4 as NGAY5,getdate()+" + i.ToString() + "+5 as NGAY6,getdate()+" + i.ToString() + "+6 as NGAY7,getdate()+" + i.ToString() + "+7 as NGAY8,getdate()+" + i.ToString() + "+8 as NGAY9,getdate()+" + i.ToString() + "+9 as NGAY10,getdate()+" + i.ToString() + "+10 as NGAY11" + "\n";
            dtNgaychieu = clsMain.ReturnDataTable(sSQL);
            pnlNgaychieu.Controls.Clear();
            foreach (DataColumn dr in dtNgaychieu.Columns)
            {
                DateTime dtime = (DateTime)dtNgaychieu.Rows[0][dr];
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 45;
                btn.Font = pnlRap.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Name = "N" + dtNgaychieu.Rows[0][dr].ToString();
                btn.Text = cls_KP_RES.GetThuofDateTime(dtime) + "\n" +cls_KP_RES.DateToString_Date(dtime);
                btn.Tag = cls_KP_RES.DateToString_Date(dtime) ;
                btn.Click += new EventHandler(Ngaychieu_Click);
                pnlNgaychieu.Controls.Add(btn);
            }
        }
        private void Ngaychieu_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.ForeColor == Color.Red)
            {
                btn.ForeColor = btnCapnhat.ForeColor;
            }
            else
            {
                btn.ForeColor = Color.Red;
            }
        }

        ArrayList smaGiochieu = new ArrayList();
        DataTable dtGiochieu = new DataTable();
        private void LoaddataGridView_Giochieu()
        {
            pnlGiochieu.Controls.Clear();
            for (int i = 0; i <= 10; i++)
            {
                MaskedTextBox txt = new MaskedTextBox();
                txt.Dock = DockStyle.Top;
                txt.Font = btnCapnhat.Font;
                txt.Name = "G" + i.ToString();
                txt.BackColor = Color.LimeGreen;
                txt.AutoSize = false;
                txt.Width = 110;
                txt.Height = 25;
                txt.Text = "00:00";
                txt.Mask = "00:00";
                txt.ForeColor = btnCapnhat.ForeColor;
                txt.BorderStyle = BorderStyle.None;
                txt.TextAlign = HorizontalAlignment.Center;
                txt.Multiline = true;
                txt.TextChanged += new EventHandler(Giochieu_TextChanged);
                txt.Click += new EventHandler(Giochieu_Click);
                txt.Leave += new EventHandler(Giochieu_LostFocus);
                

                MaskedTextBox txt1 = new MaskedTextBox();
                txt1.Dock = DockStyle.Top;
                txt1.Font = btnCapnhat.Font;
                txt1.Name = "G1" + i.ToString();
                txt1.BackColor = Color.Tomato;
                txt1.AutoSize = false;
                txt1.Width = 110;
                txt1.Height = 20;
                txt1.Text = "00:00";
                txt1.Mask = "00:00";
                txt1.ForeColor = btnCapnhat.ForeColor;
                txt1.BorderStyle = BorderStyle.None;
                txt1.TextAlign = HorizontalAlignment.Center;
                txt1.Multiline = true;

                PanelControl pnlTemp = new PanelControl();
                pnlTemp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                pnlTemp.Width = 110;
                pnlTemp.Height = 45;
                pnlTemp.Controls.Add(txt1);
                pnlTemp.Controls.Add(txt);
                
                pnlGiochieu.Controls.Add(pnlTemp);
            }
        }
        private void Giochieu_TextChanged(object sender, EventArgs e)
        {
            try
            {
                MaskedTextBox btn = (MaskedTextBox)sender;
                DateTime dti = Convert.ToDateTime(btn.Text);
                dti = dti.AddMinutes(double.Parse(iThoiluong.ToString()));

                if (btn.Text != "00:00")
                {
                    btn.ForeColor = Color.Red;
                    foreach (Control ctl in btn.Parent.Controls)
                    {
                        if (ctl.Name != btn.Name)
                        {
                            ctl.Text = dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00");
                        }
                    }
                }
                else
                {
                    btn.ForeColor = btnCapnhat.ForeColor;
                    foreach (Control ctl in btn.Parent.Controls)
                    {
                        if (ctl.Name != btn.Name)
                        {
                            ctl.Text = "00:00";
                        }
                    }
                }
            }
            catch
            {
            }
          
        }

        private void Giochieu_Click(object sender, EventArgs e)
        {
            try
            {
                MaskedTextBox btn = (MaskedTextBox)sender;
                btn.SelectAll();
            }
            catch
            {
            }
        }

        private void Giochieu_LostFocus(object sender, EventArgs e)
        {
            bool bTrue = true;
            MaskedTextBox btn = (MaskedTextBox)sender;
            try
            {
                if (btn.Text.Length != 5)
                {
                    bTrue = false;
                }
                DateTime dti = Convert.ToDateTime(btn.Text);
            }
            catch
            {
                bTrue = false;
            }
            if (!bTrue)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Giờ chiếu không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btn.Focus();
                btn.SelectAll();
            }
        }

        string smaChatluongphim = "";
        DataTable dtChatluongphim = new DataTable();
        private void LoaddataGridView_Chatluongphim()
        {
            if (pnlChatluongphim.Controls.Count < 1)
            {
                smaChatluongphim = "";
                string sSQL = "";
                sSQL += "Select MACHATLUONGPHIM As MA,	TENCHATLUONGPHIM As TEN" + "\n";
                sSQL += "From DM_CHATLUONGPHIM" + "\n";
                sSQL += "Where SUDUNG=1" + "\n";
                sSQL += "Order by TEN" + "\n";
                dtChatluongphim = clsMain.ReturnDataTable(sSQL);
                pnlChatluongphim.Controls.Clear();
                foreach (DataRow dr in dtChatluongphim.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 45;
                    btn.Font = pnlRap.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.Name = "c" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    btn.Click += new EventHandler(Chatluongphim_Click);
                    pnlChatluongphim.Controls.Add(btn);
                }
                if (dtChatluongphim.Rows.Count > 0)
                {
                    smaChatluongphim = dtChatluongphim.Rows[0]["MA"].ToString();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa có chất lượng phim", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void Chatluongphim_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlChatluongphim.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaChatluongphim = btn.Name.Substring(1, btn.Name.Length - 1);
        }

        string smaBanggia = "";
        DataTable dtBanggia = new DataTable();
        private void LoaddataGridView_Banggia()
        {
            if (pnlBanggia.Visible)
            {
                smaBanggia = "";
                string sSQL = "";
                sSQL += "Select MABANGGIA As MA,TENBANGGIA As TEN" + "\n";
                sSQL += "From BANGGIA" + "\n";
                sSQL += "Where  MABANGGIA NOT IN (Select MA_BANGGIA From THIETLAPBANGGIA)" + "\n";
                sSQL += "And SUDUNG=1" + "\n";
                sSQL += "Order by TEN" + "\n";
                dtBanggia = clsMain.ReturnDataTable(sSQL);
                pnlBanggia.Controls.Clear();
                mnuBanggia.DropDownItems.Clear();
                foreach (DataRow dr in dtBanggia.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 45;
                    btn.Font = pnlRap.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.Name = "b" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    btn.Click += new EventHandler(Banggia_Click);
                    pnlBanggia.Controls.Add(btn);
                    mnuBanggia.DropDownItems.Add(dr["TEN"].ToString(), null, new EventHandler(BanggiaMenu_Click));
                }
            }
        }
        private void Banggia_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlBanggia.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaBanggia = btn.Name.Substring(1, btn.Name.Length - 1);
        }

        string smaPHONG = "";
        DataTable dtPHONG = new DataTable();
        private void LoaddataGridView_Phong(string maRap)
        {
            if (pnlPHONG.Visible)
            {
                smaPHONG = "";
                string sSQL = "";
                sSQL += "Select MARAP,MAPHONG As MA,TENPHONG As TEN" + "\n";
                sSQL += "From DM_PHONG" + "\n";
                sSQL += "Where MARAP=" + clsMain.SQLString(maRap) + "\n";
                sSQL += "And SUDUNG=1" + "\n";
                sSQL += "Order by TEN" + "\n";
                dtPHONG = clsMain.ReturnDataTable(sSQL);
                pnlPHONG.Controls.Clear();
                foreach (DataRow dr in dtPHONG.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 45;
                    btn.Font = pnlRap.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.Name = "P" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    btn.Click += new EventHandler(PHONG_Click);
                    pnlPHONG.Controls.Add(btn);
                }
            }
        }
        private void PHONG_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlPHONG.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            smaPHONG = btn.Name.Substring(1, btn.Name.Length - 1);
        }

        bool bSua = false;
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }

            string sSQL = "";
            Boolean bRunSQL = false;

            if (bSua)
            {
                sSQL = "";
                sSQL += "Update LICHCHIEU" + "\n";
                sSQL += "Set  SUDUNG=0,XOA=1" + "\n";
                sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(smaLichchieu) + "\n";
                bRunSQL = clsMain.ExecuteSQL(sSQL);
            }
            if (!CheckGioChieuInput(smaGiochieu, iThoiluong))
            {
                return;
            }
            foreach (object objNgaychieu in smaNgaychieu)
            {
                foreach (object objGiochieu in smaGiochieu)
                {
                    DateTime dti = Convert.ToDateTime(objGiochieu.ToString());
                    dti = dti.AddMinutes(double.Parse(iThoiluong.ToString()));
                    string objKetthuc = dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00");
                    if (!CheckGioChieu(smaPHONG, objNgaychieu.ToString(), objGiochieu.ToString(), objKetthuc.ToString()))
                    {
                        return;
                    }
                }
            }

            foreach (object objNgaychieu in smaNgaychieu)
            {
                foreach (object objGiochieu in smaGiochieu)
                {
                    DateTime dti = Convert.ToDateTime(objGiochieu.ToString());
                    dti = dti.AddMinutes(double.Parse(iThoiluong.ToString()));
                    sSQL = "";
                    sSQL += "Insert into LICHCHIEU (MAPHONG,MAPHIM,MACHATLUONGPHIM,MABANGGIA,NGAYCHIEU,BATDAU,KETTHUC,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLString(smaPHONG) + ",";
                    sSQL += clsMain.SQLString(smaPHIM) + ",";
                    sSQL += clsMain.SQLString(smaChatluongphim) + ",";
                    sSQL += clsMain.SQLString(smaBanggia) + ",";
                    sSQL += clsMain.SQLString(objNgaychieu.ToString()) + ",";
                    sSQL += clsMain.SQLString(objGiochieu.ToString()) + ",";
                    sSQL += clsMain.SQLString(dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00")) + ",";
                    sSQL += clsMain.SQLStringUnicode("") + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")" + "\n";
                    sSQL += TaoVe(smaPHONG);

                    bRunSQL = clsMain.ExecuteSQL(sSQL);
                }
            }

            if (bRunSQL)
            {
                if (bSua)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                //19-05-2017 tu update web khi luu
                //btnWeb_Click(null, null);
                bUpdateweb = true;
                LoadLichChieu(smaRap, dtpNgay.Text);
                LoaddataGridView_Giochieu();
                if (bSua)
                {
                    btnAn_Click(null, null);
                }
            }
            else
            {
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        string TaoVe(string sMaphong)
        {
            string str = "";

            string sSQL = "";
            sSQL += "Select MAGHE From DM_GHE" + "\n";
            sSQL += "Where MAPHONG = " + clsMain.SQLString(sMaphong) + "\n";
            sSQL += "And SUDUNG=" + clsMain.SQLString("1") + "\n";
            DataTable dtGhe = clsMain.ReturnDataTable(sSQL);

            string sMALICHTRINH = "";
            sSQL = "";
            sSQL += "Select IDENT_CURRENT('LICHCHIEU')+1 As MALICHCHIEU " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            sMALICHTRINH = dt.Rows[0]["MALICHCHIEU"].ToString();

            foreach (DataRow dr in dtGhe.Rows)
            {
                str += "Insert into VE (MALICHCHIEU,MAGHE,SOGHE,TRANGTHAI,PHUTHU,DATHANHTOAN,DONGIA,CONLAI)" + "\n";
                str += "Values ( ";
                str += clsMain.SQLString(sMALICHTRINH) + ",";
                str += clsMain.SQLString(dr["MAGHE"].ToString()) + ",";
                str += clsMain.SQLString("") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("1") + ",";
                str += clsMain.SQLString("1") + ")" + "\n";
            }
            if (bSua)
                str += LSLichChieu(sMALICHTRINH, "2");
            else
                str += LSLichChieu(sMALICHTRINH, "1");

            return str;
        }

        private Boolean CheckInput()
        {
            if (smaRap == "")
            {
                XtraMessageBox.Show("Chưa chọn rạp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (smaPHIM == "")
            {
                XtraMessageBox.Show("Chưa chọn phim", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (smaPHONG == "")
            {
                XtraMessageBox.Show("Chưa chọn phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            smaNgaychieu.Clear();
            foreach (SimpleButton btn in pnlNgaychieu.Controls)
            {
                if (btn.ForeColor == Color.Red)
                {
                    smaNgaychieu.Add(btn.Tag);
                }
            }
            if (smaNgaychieu.Count == 0)
            {
                XtraMessageBox.Show("Chưa chọn ngày chiếu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            smaGiochieu.Clear();
            foreach (PanelControl pnl in pnlGiochieu.Controls)
            {
                foreach (MaskedTextBox btn in pnl.Controls)
                {
                    if (btn.ForeColor == Color.Red)
                    {
                        smaGiochieu.Add(btn.Text);
                    }
                }
            }
        
            if (smaGiochieu.Count == 0)
            {
                XtraMessageBox.Show("Chưa chọn giờ chiếu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (smaChatluongphim == "")
            {
                XtraMessageBox.Show("Chưa chọn chất lượng phim", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (smaBanggia == "")
            {
                XtraMessageBox.Show("Chưa chọn bảng giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void btnXoahet_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa tất cả ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            foreach (DataRow dr in dtLichchieu.Rows)
            {
                string sSQL = "";
                sSQL += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                sSQL += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                sSQL += "And A.MALICHCHIEU = " + clsMain.SQLString(dr["MALICHCHIEU"].ToString()) + "\n";
                sSQL += "And A.TRANGTHAI = " + clsMain.SQLString("1") + "\n";
                sSQL += "Union" + "\n";
                sSQL += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                sSQL += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                sSQL += "And A.MALICHCHIEU = " + clsMain.SQLString(dr["MALICHCHIEU"].ToString()) + "\n";
                sSQL += "And B.NGAYCHIEU < " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
                if (clsMain.ReturnDataTable(sSQL).Rows.Count == 0)
                {
                    sSQL = "";
                    sSQL += "Update LICHCHIEU" + "\n";
                    sSQL += "Set  SUDUNG=0,XOA=1" + "\n";
                    sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(dr["MALICHCHIEU"].ToString()) + "\n";
                    sSQL += LSLichChieu(dr["MALICHCHIEU"].ToString(), "3");
                    clsMain.ExecuteSQL(sSQL);
                }
            }
            //19-05-2017 tu update web khi luu
            //btnWeb_Click(null, null);
            bUpdateweb = true;
            LoadLichChieu(smaRap, dtpNgay.Text);
        }

        private Boolean CheckGioChieu(string sPhong, string sNgaychieu,string sBatdau,string sKetthuc)
        {
            string sSQL = "";
            sSQL += "SELECT MALICHCHIEU,MAPHONG ,NGAYCHIEU ,BATDAU,KETTHUC" + "\n";
            sSQL += "FROM LICHCHIEU " + "\n";
            sSQL += "WHERE  MAPHONG =" + clsMain.SQLString(sPhong) + "\n";
            sSQL += "AND NGAYCHIEU =" + clsMain.SQLString(sNgaychieu) + "\n";
            sSQL += "AND (convert(datetime,BATDAU) BETWEEN  convert(datetime," + clsMain.SQLString(sBatdau) + ") AND convert(datetime," + clsMain.SQLString(sKetthuc) + ") OR convert(datetime,KETTHUC) BETWEEN  convert(datetime," + clsMain.SQLString(sBatdau) + ") AND convert(datetime," + clsMain.SQLString(sKetthuc) + ")  OR(BATDAU=" + clsMain.SQLString(sBatdau) + ") OR (KETTHUC=" + clsMain.SQLString(sKetthuc) + "))" + "\n";
            sSQL += "AND SUDUNG =" + clsMain.SQLString("1") + "\n";
            if (clsMain.ReturnDataTable(sSQL).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Giờ chiếu bị trùng . Kiểm tra lại giờ chiếu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private Boolean CheckGioChieuInput(ArrayList aGiochieu,int iLenghPhim)
        {
            aGiochieu.Sort();
            for (int i = 0; i < aGiochieu.Count; i++)
            {
                if (i != aGiochieu.Count - 1)
                {
                    DateTime dti1 = Convert.ToDateTime(aGiochieu[i].ToString());
                    DateTime dti2 = Convert.ToDateTime(aGiochieu[i + 1].ToString());
                    if (dti1.AddMinutes(iLenghPhim) >= dti2)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Giờ chiếu bị trùng . Kiểm tra lại giờ chiếu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }
            return true;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Frm_CopyLich frm = new Frm_CopyLich();
            frm.sMarap = smaRap;
            frm.ShowDialog();
            if (frm.bDongy)
            {
                //19-05-2017 tu update web khi luu
                //btnWeb_Click(null, null);
                bUpdateweb = true;
                LoadLichChieu(smaRap, dtpNgay.Text);
            }
            frm.Dispose();
        }

        private void BanggiaMenu_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem it = (ToolStripDropDownItem)sender ;
            string mabanggia = dtBanggia.Select("TEN=" + clsMain.SQLString(it.Text))[0]["MA"].ToString();
            string sSQL = "";
            if (amalichchieu.Count > 0)//Chon nhieu lich chieu
            {
                foreach (object obj in amalichchieu)
                {
                    string sSQL1 = "";
                    sSQL1 = "";
                    sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                    sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                    sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(obj.ToString()) + "\n";
                    sSQL1 += "And A.TRANGTHAI = " + clsMain.SQLString("1") + "\n";
                    sSQL1 += "Union" + "\n";
                    sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                    sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                    sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(obj.ToString()) + "\n";
                    sSQL1 += "And B.NGAYCHIEU < " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
                    if (clsMain.ReturnDataTable(sSQL1).Rows.Count == 0)
                    {
                        sSQL += "Update LICHCHIEU" + "\n";
                        sSQL += "Set  MABANGGIA=" + clsMain.SQLString(mabanggia) + "\n";
                        sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(obj.ToString()) + "\n";

                        sSQL += LSLichChieu(obj.ToString(), "2") + "\n";
                    }
                }
            }
            else
            {
                string sSQL1 = "";
                sSQL1 = "";
                sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
                sSQL1 += "And A.TRANGTHAI = " + clsMain.SQLString("1") + "\n";
                sSQL1 += "Union" + "\n";
                sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
                sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
                sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
                sSQL1 += "And B.NGAYCHIEU < " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
                if (clsMain.ReturnDataTable(sSQL1).Rows.Count == 0)
                {
                    sSQL += "Update LICHCHIEU" + "\n";
                    sSQL += "Set  MABANGGIA=" + clsMain.SQLString(mabanggia) + "\n";
                    sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(smaLichchieu) + "\n";

                    sSQL += LSLichChieu(smaLichchieu, "2") + "\n";
                }
            }
            bool bRunSQL = clsMain.ExecuteSQL(sSQL);
            if (bRunSQL)
            {
                //19-05-2017 tu update web khi luu
                //btnWeb_Click(null, null);
                bUpdateweb = true;
                LoadLichChieu(smaRap, dtpNgay.Text);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PhimMenu_Click(object sender, EventArgs e)
        {
            if (smaLichchieu == "")//Chưa chọn dòng để sữa
            {
                XtraMessageBox.Show("Chưa chọn xuất chiếu để sữa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            ToolStripDropDownItem it = (ToolStripDropDownItem)sender;
            string maphim = dtPHIM.Select("TEN=" + clsMain.SQLString(it.Text))[0]["MA"].ToString();
            string thoiluong = dtPHIM.Select("TEN=" + clsMain.SQLString(it.Text))[0]["THOILUONG"].ToString();
            string batdau = dtLichchieu.Select("MALICHCHIEU=" + clsMain.SQLString(smaLichchieu))[0]["BATDAU"].ToString();
            DateTime dti = Convert.ToDateTime(batdau.ToString());
            dti = dti.AddMinutes(double.Parse(thoiluong.ToString()));
            string ketthuc = dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00");
            
            string sSQL = "";
            string sSQL1 = "";
            sSQL1 = "";
            sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
            sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
            sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
            sSQL1 += "And A.TRANGTHAI = " + clsMain.SQLString("1") + "\n";
            sSQL1 += "Union" + "\n";
            sSQL1 += "Select A.MAVE,B.MALICHCHIEU From VE A, LICHCHIEU B " + "\n";
            sSQL1 += "Where A.MALICHCHIEU=B.MALICHCHIEU " + "\n";
            sSQL1 += "And A.MALICHCHIEU = " + clsMain.SQLString(smaLichchieu) + "\n";
            sSQL1 += "And B.NGAYCHIEU < " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
            if (clsMain.ReturnDataTable(sSQL1).Rows.Count == 0)
            {
                sSQL += "Update LICHCHIEU" + "\n";
                sSQL += "Set  MAPHIM=" + clsMain.SQLString(maphim) + "," + "\n";
                sSQL += "KETTHUC=" + clsMain.SQLString(ketthuc) + "\n";
                sSQL += "Where MALICHCHIEU=" + clsMain.SQLString(smaLichchieu) + "\n";

                sSQL += LSLichChieu(smaLichchieu, "2") + "\n";
            }
           
            bool bRunSQL = clsMain.ExecuteSQL(sSQL);
            if (bRunSQL)
            {
                //19-05-2017 tu update web khi luu
                //btnWeb_Click(null, null);
                bUpdateweb = true;
                LoadLichChieu(smaRap, dtpNgay.Text);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnWeb_Click(object sender, EventArgs e)
        {
            string sSQLMain = "";
            string sSQL = "";
           
            if (clsMain.ReturnDataTable("Select MAPHIM ,TENPHIM  From DM_PHIM Where ID_SYNC  is null And MAPHIM in (Select MAPHIM From LICHCHIEU  Where ngaychieu>=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate)) + ")").Rows.Count > 0)
            {
                XtraMessageBox.Show("Bạn cần up phim lên web trước khi có thể up lịch chiếu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            clsMain.ExecuteSQL("Delete From  MovieCinemaDetails");

            sSQL = "";
            sSQL = "Select distinct B.MAPHIM,B.TENPHIM,B.ID_SYNC,A.SUDUNG From LICHCHIEU A, DM_PHIM B Where A.MAPHIM=B.MAPHIM AND A.SUDUNG=1 AND A.NGAYCHIEU>=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate));
            DataTable dtPhimLC = clsMain.ReturnDataTable(sSQL);

            foreach (DataRow dr in dtPhimLC.Rows)
            {
                sSQL = "";
                sSQL = "Select distinct C.MADIADIEM,C.TENDIADIEM,C.ID_SYNC From LICHCHIEU A, DM_PHONG B,DM_DIADIEM C Where A.MAPHONG=B.MAPHONG AND B.MARAP=C.MADIADIEM  AND A.SUDUNG=1 AND A.MAPHIM=" + clsMain.SQLString(dr["MAPHIM"].ToString()) + " AND A.NGAYCHIEU>=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate));
                DataTable dtRapLC = clsMain.ReturnDataTable(sSQL);

                foreach (DataRow dr1 in dtRapLC.Rows)
                {
                    string Schedules = "";
                    sSQL = "";
                    sSQL = "Select distinct A.NGAYCHIEU From LICHCHIEU A,DM_PHONG B,DM_DIADIEM C  Where A.MAPHONG=B.MAPHONG AND B.MARAP=C.MADIADIEM  AND A.SUDUNG=1 AND A.MAPHIM=" + clsMain.SQLString(dr["MAPHIM"].ToString()) + " AND C.MADIADIEM=" + clsMain.SQLString(dr1["MADIADIEM"].ToString()) + " AND  A.NGAYCHIEU>=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate));
                    DataTable dtNgay = clsMain.ReturnDataTable(sSQL);

                    for (int i = 0; i < dtNgay.Rows.Count; i++)
                    {
                        DateTime dti = (DateTime)dtNgay.Rows[i]["NGAYCHIEU"];
                        if (i == 0)
                        {
                            Schedules = "[{\"Date\":\"";
                            Schedules += dti.Day.ToString() + "/" + dti.Month.ToString() + "/" + dti.Year.ToString();
                            Schedules += "\",\"Times\":[";

                            sSQL = "";
                            sSQL += "Select distinct A.BATDAU From LICHCHIEU A,DM_PHONG B,DM_DIADIEM C  Where A.MAPHONG=B.MAPHONG AND B.MARAP=C.MADIADIEM  AND A.SUDUNG=1 AND A.MAPHIM=" + clsMain.SQLString(dr["MAPHIM"].ToString()) + " AND C.MADIADIEM=" + clsMain.SQLString(dr1["MADIADIEM"].ToString()) + " AND  A.NGAYCHIEU=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dti));
                            DataTable dtGio = clsMain.ReturnDataTable(sSQL);

                            for (int j = 0; j < dtGio.Rows.Count; j++)
                            {
                                if (j == 0)
                                {
                                    Schedules += "{\"Time\":\"" + dtGio.Rows[j]["BATDAU"].ToString() + "\",\"IsChecked\":true,\"$$hashKey\":\"" + (j + 1).ToString("000") + "\"}";
                                }
                                else
                                {
                                    Schedules += ",{\"Time\":\"" + dtGio.Rows[j]["BATDAU"].ToString() + "\",\"IsChecked\":true,\"$$hashKey\":\"" + (j + 1).ToString("000") + "\"}";
                                }
                            }
                            Schedules += "],\"IsCheckAll\":true,\"$$hashKey\":\"" + (i + 1).ToString("000") + "\"}";
                        }
                        else
                        {
                            Schedules += ",{\"Date\":\"";
                            Schedules += dti.Day.ToString() + "/" + dti.Month.ToString() + "/" + dti.Year.ToString();
                            Schedules += "\",\"Times\":[";

                            sSQL = "";
                            sSQL += "Select distinct BATDAU From LICHCHIEU A,DM_PHONG B,DM_DIADIEM C  Where A.MAPHONG=B.MAPHONG AND B.MARAP=C.MADIADIEM AND A.SUDUNG=1   AND A.MAPHIM=" + clsMain.SQLString(dr["MAPHIM"].ToString()) + " AND C.MADIADIEM=" + clsMain.SQLString(dr1["MADIADIEM"].ToString()) + " AND  A.NGAYCHIEU=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dti));
                            DataTable dtGio = clsMain.ReturnDataTable(sSQL);

                            for (int j = 0; j < dtGio.Rows.Count; j++)
                            {
                                if (j == 0)
                                {
                                    Schedules += "{\"Time\":\"" + dtGio.Rows[j]["BATDAU"].ToString() + "\",\"IsChecked\":true,\"$$hashKey\":\"" + (j + 1).ToString("000") + "\"}";
                                }
                                else
                                {
                                    Schedules += ",{\"Time\":\"" + dtGio.Rows[j]["BATDAU"].ToString() + "\",\"IsChecked\":true,\"$$hashKey\":\"" + (j + 1).ToString("000") + "\"}";
                                }
                            }
                            Schedules += "],\"IsCheckAll\":true,\"$$hashKey\":\"" + (i + 1).ToString("000") + "\"}";
                        }
                    }
                    Schedules += "]";

                    sSQLMain += "\n" + "Insert into MovieCinemaDetails (ID,Movie,Cinema,Schedules)";
                    sSQLMain += "Values ( ";
                    sSQLMain += "newid()" + ",";
                    sSQLMain += clsMain.SQLString(dr["ID_SYNC"].ToString()) + ",";
                    sSQLMain += clsMain.SQLString(dr1["ID_SYNC"].ToString()) + ",";
                    sSQLMain += clsMain.SQLString(Schedules) + ")";
                }
            }
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQLMain);

            clsMain.ExecuteSQL("Delete From  MovieSchedules");
            sSQL = "";
            sSQL +="Select A.MALICHCHIEU,A.NGAYCHIEU ,A.BATDAU ,A.KETTHUC ,A.SUDUNG ,A.XOA  ,B.ID_SYNC AS IDPHIM , D.ID_SYNC AS IDRAP"+"\n";
            sSQL +="From LICHCHIEU A,DM_PHIM B,DM_PHONG C,DM_DIADIEM D"+"\n";
            sSQL +="Where A.MAPHIM=B.MAPHIM AND A.MAPHONG=C.MAPHONG AND C.MARAP=D.MADIADIEM  AND A.SUDUNG=1"+"\n";
            sSQL +="AND A.NGAYCHIEU>=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate));
            DataTable dtLichchieu = clsMain.ReturnDataTable(sSQL);
            sSQLMain = "";
            foreach (DataRow dr1 in dtLichchieu.Rows)
            {
                DateTime dti = (DateTime)dr1["NGAYCHIEU"];
                sSQLMain += "\n" + "Insert into MovieSchedules (ID,Movie,Cinema,Date,Time,Privatekey)";
                sSQLMain += "Values ( ";
                sSQLMain += "newid()" + ",";
                sSQLMain += clsMain.SQLString(dr1["IDPHIM"].ToString()) + ",";
                sSQLMain += clsMain.SQLString(dr1["IDRAP"].ToString()) + ",";
                sSQLMain += clsMain.SQLString(cls_KP_RES.DateToString_Date(dti)) + ",";
                sSQLMain += clsMain.SQLString(dr1["BATDAU"].ToString()) + ",";
                sSQLMain += clsMain.SQLString(dr1["MALICHCHIEU"].ToString()) + ")" + "\n";

                sSQLMain += LSLichChieu(dr1["MALICHCHIEU"].ToString(), "5");
            }
            bRunSQL = clsMain.ExecuteSQL(sSQLMain);

            if (bUpdateweb)
            {
                return;
            }

            if (bRunSQL)
                XtraMessageBox.Show("Update lịch chiếu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private String LSLichChieu(String sMaLichChieu, String sStyle)
        {
            String sSQL = "";
            sSQL += "EXEC SP_InsertLichSuLichChieu ";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
            sSQL += clsMain.SQLString(sMaLichChieu) + ",";
            sSQL += clsMain.SQLString(sStyle) + "\n";

            return sSQL;
        }

        private void Frm_Lichchieu_Leave(object sender, EventArgs e)
        {
            if (bUpdateweb)
            {
                //19-05-2017 tu update web khi luu
                btnWeb_Click(null, null);
                bUpdateweb = false;
            }
        }

        private void Frm_Lichchieu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bUpdateweb)
            {
                //19-05-2017 tu update web khi luu
                btnWeb_Click(null, null);
                bUpdateweb = false;
            }
        }

       
    }
}