﻿namespace KP_RES
{
    partial class PF_Invedattruoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInve = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.txtMave = new DevExpress.XtraEditors.TextEdit();
            this.lblMadatve = new DevExpress.XtraEditors.LabelControl();
            this.btnTim = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyDatve = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.txtSodienthoai = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pnlReport = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnBanve1 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtMave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSodienthoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).BeginInit();
            this.btnBanve1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // btnInve
            // 
            this.btnInve.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInve.Appearance.Options.UseFont = true;
            this.btnInve.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnInve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnInve.Location = new System.Drawing.Point(6, 133);
            this.btnInve.Margin = new System.Windows.Forms.Padding(6);
            this.btnInve.Name = "btnInve";
            this.btnInve.Size = new System.Drawing.Size(93, 60);
            this.btnInve.TabIndex = 5;
            this.btnInve.Text = "In vé";
            this.btnInve.Click += new System.EventHandler(this.btnInve_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(106, 198);
            this.btnDong.Margin = new System.Windows.Forms.Padding(6);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(134, 60);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // txtMave
            // 
            this.txtMave.EnterMoveNextControl = true;
            this.txtMave.Location = new System.Drawing.Point(106, 3);
            this.txtMave.Name = "txtMave";
            this.txtMave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMave.Properties.Appearance.Options.UseFont = true;
            this.txtMave.Size = new System.Drawing.Size(344, 26);
            this.txtMave.TabIndex = 1;
            // 
            // lblMadatve
            // 
            this.lblMadatve.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadatve.Location = new System.Drawing.Point(6, 6);
            this.lblMadatve.Margin = new System.Windows.Forms.Padding(4);
            this.lblMadatve.Name = "lblMadatve";
            this.lblMadatve.Size = new System.Drawing.Size(68, 19);
            this.lblMadatve.TabIndex = 0;
            this.lblMadatve.Text = "Mã đặt vé";
            // 
            // btnTim
            // 
            this.btnTim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTim.Appearance.Options.UseFont = true;
            this.btnTim.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTim.Location = new System.Drawing.Point(6, 67);
            this.btnTim.Margin = new System.Windows.Forms.Padding(6);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(93, 60);
            this.btnTim.TabIndex = 4;
            this.btnTim.Text = "Tìm";
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnHuyDatve);
            this.panelControl2.Controls.Add(this.btnXoa);
            this.panelControl2.Controls.Add(this.btn5);
            this.panelControl2.Controls.Add(this.btn6);
            this.panelControl2.Controls.Add(this.btn7);
            this.panelControl2.Controls.Add(this.btn8);
            this.panelControl2.Controls.Add(this.btn9);
            this.panelControl2.Controls.Add(this.btn0);
            this.panelControl2.Controls.Add(this.btn1);
            this.panelControl2.Controls.Add(this.btn2);
            this.panelControl2.Controls.Add(this.btn3);
            this.panelControl2.Controls.Add(this.btn4);
            this.panelControl2.Controls.Add(this.btnDong);
            this.panelControl2.Controls.Add(this.txtSodienthoai);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.txtMave);
            this.panelControl2.Controls.Add(this.btnTim);
            this.panelControl2.Controls.Add(this.lblMadatve);
            this.panelControl2.Controls.Add(this.btnInve);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(461, 639);
            this.panelControl2.TabIndex = 0;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBanphim.Location = new System.Drawing.Point(246, 198);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(6);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(134, 60);
            this.btnBanphim.TabIndex = 59;
            this.btnBanphim.Text = "Bàn phím";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnHuyDatve
            // 
            this.btnHuyDatve.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyDatve.Appearance.Options.UseFont = true;
            this.btnHuyDatve.Appearance.Options.UseTextOptions = true;
            this.btnHuyDatve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnHuyDatve.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuyDatve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuyDatve.Location = new System.Drawing.Point(6, 198);
            this.btnHuyDatve.Margin = new System.Windows.Forms.Padding(6);
            this.btnHuyDatve.Name = "btnHuyDatve";
            this.btnHuyDatve.Size = new System.Drawing.Size(93, 60);
            this.btnHuyDatve.TabIndex = 58;
            this.btnHuyDatve.Text = "Hủy \r\nđặt vé";
            this.btnHuyDatve.Click += new System.EventHandler(this.btnHuyDatve_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Location = new System.Drawing.Point(386, 198);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(64, 60);
            this.btnXoa.TabIndex = 57;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(106, 133);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(64, 60);
            this.btn5.TabIndex = 56;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(176, 133);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(64, 60);
            this.btn6.TabIndex = 53;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(246, 133);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(64, 60);
            this.btn7.TabIndex = 54;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(316, 133);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(64, 60);
            this.btn8.TabIndex = 55;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(386, 133);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(64, 60);
            this.btn9.TabIndex = 52;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(106, 67);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(64, 60);
            this.btn0.TabIndex = 51;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(176, 67);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(64, 60);
            this.btn1.TabIndex = 48;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(246, 67);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(64, 60);
            this.btn2.TabIndex = 49;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(316, 67);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(64, 60);
            this.btn3.TabIndex = 50;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(386, 67);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(64, 60);
            this.btn4.TabIndex = 47;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn0_Click);
            // 
            // txtSodienthoai
            // 
            this.txtSodienthoai.EnterMoveNextControl = true;
            this.txtSodienthoai.Location = new System.Drawing.Point(106, 35);
            this.txtSodienthoai.Name = "txtSodienthoai";
            this.txtSodienthoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSodienthoai.Properties.Appearance.Options.UseFont = true;
            this.txtSodienthoai.Size = new System.Drawing.Size(344, 26);
            this.txtSodienthoai.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 38);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(93, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Số điện thoại";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.pnlReport);
            this.panelControl3.Controls.Add(this.panelControl2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 43);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1026, 643);
            this.panelControl3.TabIndex = 16;
            // 
            // pnlReport
            // 
            this.pnlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlReport.Location = new System.Drawing.Point(463, 2);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(561, 639);
            this.pnlReport.TabIndex = 17;
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.btnBanve1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1026, 43);
            this.panelControl4.TabIndex = 47;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.pictureEdit2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(820, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(204, 39);
            this.panelControl5.TabIndex = 13;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit2.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit2.Location = new System.Drawing.Point(-179, 2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(381, 35);
            this.pictureEdit2.TabIndex = 2;
            // 
            // btnBanve1
            // 
            this.btnBanve1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnBanve1.Controls.Add(this.title);
            this.btnBanve1.Controls.Add(this.home);
            this.btnBanve1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanve1.Location = new System.Drawing.Point(2, 2);
            this.btnBanve1.Name = "btnBanve1";
            this.btnBanve1.Size = new System.Drawing.Size(818, 39);
            this.btnBanve1.TabIndex = 8;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(49, 11);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(121, 19);
            this.title.TabIndex = 2;
            this.title.Text = "In vé đặt trước";
            // 
            // home
            // 
            this.home.Image = global::KP_RES.Properties.Resources.bill_26;
            this.home.Location = new System.Drawing.Point(10, 4);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            // 
            // PF_Invedattruoc
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1026, 686);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.Name = "PF_Invedattruoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "In vé đặt trước";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PF_Invedattruoc_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.txtMave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSodienthoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).EndInit();
            this.btnBanve1.ResumeLayout(false);
            this.btnBanve1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblMadatve;
        private DevExpress.XtraEditors.SimpleButton btnInve;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.TextEdit txtMave;
        private DevExpress.XtraEditors.SimpleButton btnTim;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl pnlReport;
        private DevExpress.XtraEditors.TextEdit txtSodienthoai;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl btnBanve1;
        private DevExpress.XtraEditors.LabelControl title;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btnHuyDatve;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
    }
}