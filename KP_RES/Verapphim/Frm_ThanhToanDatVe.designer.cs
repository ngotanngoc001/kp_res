﻿namespace KP_RES 
{
    partial class Frm_ThanhToanDatVe 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.txtThanhToan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNgayChieu = new DevExpress.XtraEditors.TextEdit();
            this.txtGioChieu = new DevExpress.XtraEditors.TextEdit();
            this.txtTenPhim = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDaiLy = new DevExpress.XtraEditors.TextEdit();
            this.txtChietKhau = new DevExpress.XtraEditors.TextEdit();
            this.txtTongTien = new DevExpress.XtraEditors.TextEdit();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.lblGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.lblDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.txtMaDaiLy = new DevExpress.XtraEditors.TextEdit();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.lblNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtConlai = new DevExpress.XtraEditors.TextEdit();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayChieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGioChieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDaiLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChietKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDaiLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConlai.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 246);
            this.panelControl1.TabIndex = 0;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.btnDong);
            this.pal_ban.Controls.Add(this.txtConlai);
            this.pal_ban.Controls.Add(this.labelControl5);
            this.pal_ban.Controls.Add(this.txtThanhToan);
            this.pal_ban.Controls.Add(this.labelControl4);
            this.pal_ban.Controls.Add(this.labelControl2);
            this.pal_ban.Controls.Add(this.txtNgayChieu);
            this.pal_ban.Controls.Add(this.txtGioChieu);
            this.pal_ban.Controls.Add(this.txtTenPhim);
            this.pal_ban.Controls.Add(this.txtTenDaiLy);
            this.pal_ban.Controls.Add(this.txtChietKhau);
            this.pal_ban.Controls.Add(this.txtTongTien);
            this.pal_ban.Controls.Add(this.txtBarcode);
            this.pal_ban.Controls.Add(this.labelControl1);
            this.pal_ban.Controls.Add(this.labelControl3);
            this.pal_ban.Controls.Add(this.lblDiaChi);
            this.pal_ban.Controls.Add(this.lblGioiTinh);
            this.pal_ban.Controls.Add(this.lblDienThoai);
            this.pal_ban.Controls.Add(this.txtMaDaiLy);
            this.pal_ban.Controls.Add(this.lblTen);
            this.pal_ban.Controls.Add(this.btnLuu);
            this.pal_ban.Controls.Add(this.lblNgaySinh);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(744, 242);
            this.pal_ban.TabIndex = 0;
            // 
            // txtThanhToan
            // 
            this.txtThanhToan.EnterMoveNextControl = true;
            this.txtThanhToan.Location = new System.Drawing.Point(99, 162);
            this.txtThanhToan.Name = "txtThanhToan";
            this.txtThanhToan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhToan.Properties.Appearance.Options.UseFont = true;
            this.txtThanhToan.Properties.Mask.EditMask = "N0";
            this.txtThanhToan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThanhToan.Size = new System.Drawing.Size(266, 26);
            this.txtThanhToan.TabIndex = 0;
            this.txtThanhToan.EditValueChanged += new System.EventHandler(this.txtThanhToan_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 165);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(81, 19);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Thanh toán";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(372, 46);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 19);
            this.labelControl2.TabIndex = 9;
            this.labelControl2.Text = "Ngày chiếu";
            // 
            // txtNgayChieu
            // 
            this.txtNgayChieu.EnterMoveNextControl = true;
            this.txtNgayChieu.Location = new System.Drawing.Point(460, 43);
            this.txtNgayChieu.Name = "txtNgayChieu";
            this.txtNgayChieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayChieu.Properties.Appearance.Options.UseFont = true;
            this.txtNgayChieu.Properties.ReadOnly = true;
            this.txtNgayChieu.Size = new System.Drawing.Size(265, 26);
            this.txtNgayChieu.TabIndex = 10;
            // 
            // txtGioChieu
            // 
            this.txtGioChieu.EnterMoveNextControl = true;
            this.txtGioChieu.Location = new System.Drawing.Point(460, 75);
            this.txtGioChieu.Name = "txtGioChieu";
            this.txtGioChieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioChieu.Properties.Appearance.Options.UseFont = true;
            this.txtGioChieu.Properties.ReadOnly = true;
            this.txtGioChieu.Size = new System.Drawing.Size(265, 26);
            this.txtGioChieu.TabIndex = 14;
            // 
            // txtTenPhim
            // 
            this.txtTenPhim.EnterMoveNextControl = true;
            this.txtTenPhim.Location = new System.Drawing.Point(460, 11);
            this.txtTenPhim.Name = "txtTenPhim";
            this.txtTenPhim.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhim.Properties.Appearance.Options.UseFont = true;
            this.txtTenPhim.Properties.ReadOnly = true;
            this.txtTenPhim.Size = new System.Drawing.Size(265, 26);
            this.txtTenPhim.TabIndex = 6;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.EnterMoveNextControl = true;
            this.txtTenDaiLy.Location = new System.Drawing.Point(99, 75);
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Properties.Appearance.Options.UseFont = true;
            this.txtTenDaiLy.Properties.ReadOnly = true;
            this.txtTenDaiLy.Size = new System.Drawing.Size(266, 26);
            this.txtTenDaiLy.TabIndex = 12;
            // 
            // txtChietKhau
            // 
            this.txtChietKhau.EnterMoveNextControl = true;
            this.txtChietKhau.Location = new System.Drawing.Point(99, 130);
            this.txtChietKhau.Name = "txtChietKhau";
            this.txtChietKhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChietKhau.Properties.Appearance.Options.UseFont = true;
            this.txtChietKhau.Properties.ReadOnly = true;
            this.txtChietKhau.Size = new System.Drawing.Size(266, 26);
            this.txtChietKhau.TabIndex = 16;
            // 
            // txtTongTien
            // 
            this.txtTongTien.EnterMoveNextControl = true;
            this.txtTongTien.Location = new System.Drawing.Point(460, 130);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.Properties.Appearance.Options.UseFont = true;
            this.txtTongTien.Properties.ReadOnly = true;
            this.txtTongTien.Size = new System.Drawing.Size(265, 26);
            this.txtTongTien.TabIndex = 18;
            // 
            // txtBarcode
            // 
            this.txtBarcode.EnterMoveNextControl = true;
            this.txtBarcode.Location = new System.Drawing.Point(99, 11);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.ReadOnly = true;
            this.txtBarcode.Size = new System.Drawing.Size(266, 26);
            this.txtBarcode.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 78);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(70, 19);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Tên đại lý";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(6, 133);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 19);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Chiết khấu";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi.Location = new System.Drawing.Point(372, 133);
            this.lblDiaChi.Margin = new System.Windows.Forms.Padding(4);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(76, 19);
            this.lblDiaChi.TabIndex = 17;
            this.lblDiaChi.Text = "Thành tiền";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioiTinh.Location = new System.Drawing.Point(372, 78);
            this.lblGioiTinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(66, 19);
            this.lblGioiTinh.TabIndex = 13;
            this.lblGioiTinh.Text = "Giờ chiếu";
            // 
            // lblDienThoai
            // 
            this.lblDienThoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDienThoai.Location = new System.Drawing.Point(6, 46);
            this.lblDienThoai.Margin = new System.Windows.Forms.Padding(4);
            this.lblDienThoai.Name = "lblDienThoai";
            this.lblDienThoai.Size = new System.Drawing.Size(63, 19);
            this.lblDienThoai.TabIndex = 7;
            this.lblDienThoai.Text = "Mã đại lý";
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.EnterMoveNextControl = true;
            this.txtMaDaiLy.Location = new System.Drawing.Point(99, 43);
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Properties.Appearance.Options.UseFont = true;
            this.txtMaDaiLy.Properties.ReadOnly = true;
            this.txtMaDaiLy.Size = new System.Drawing.Size(266, 26);
            this.txtMaDaiLy.TabIndex = 8;
            // 
            // lblTen
            // 
            this.lblTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Location = new System.Drawing.Point(6, 11);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(58, 19);
            this.lblTen.TabIndex = 3;
            this.lblTen.Text = "Mã code";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(557, 195);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 1;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh.Location = new System.Drawing.Point(372, 11);
            this.lblNgaySinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(68, 19);
            this.lblNgaySinh.TabIndex = 5;
            this.lblNgaySinh.Text = "Tên phim";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(372, 165);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 19);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Còn lại";
            // 
            // txtConlai
            // 
            this.txtConlai.EnterMoveNextControl = true;
            this.txtConlai.Location = new System.Drawing.Point(460, 162);
            this.txtConlai.Name = "txtConlai";
            this.txtConlai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConlai.Properties.Appearance.Options.UseFont = true;
            this.txtConlai.Properties.ReadOnly = true;
            this.txtConlai.Size = new System.Drawing.Size(265, 26);
            this.txtConlai.TabIndex = 20;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(645, 195);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(80, 35);
            this.btnDong.TabIndex = 2;
            this.btnDong.Text = "&Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // Frm_ThanhToanDatVe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 246);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_ThanhToanDatVe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thanh toán đặt vé";
            this.Load += new System.EventHandler(this.Frm_ThanhToanDatVe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayChieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGioChieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDaiLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChietKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDaiLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConlai.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.TextEdit txtTongTien;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraEditors.LabelControl lblDiaChi;
        private DevExpress.XtraEditors.LabelControl lblGioiTinh;
        private DevExpress.XtraEditors.LabelControl lblDienThoai;
        private DevExpress.XtraEditors.TextEdit txtMaDaiLy;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.LabelControl lblNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtGioChieu;
        private DevExpress.XtraEditors.TextEdit txtTenPhim;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNgayChieu;
        private DevExpress.XtraEditors.TextEdit txtTenDaiLy;
        private DevExpress.XtraEditors.TextEdit txtChietKhau;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtThanhToan;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtConlai;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnDong;




    }
}