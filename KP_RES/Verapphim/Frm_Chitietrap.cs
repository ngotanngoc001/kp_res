﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.Metro.Navigation;

namespace KP_RES 
{
    public partial class Frm_Chitietrap : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Chitietrap()
        {
            InitializeComponent();
            loadCombo();
        }

        public  string sma = "";

        public void loadCombo()
        {
            DataTable dt = new DataTable();
            dt = clsMain.ReturnDataTable("select ID as MA,VNValue as TEN from ReferenceList where category='Location'");
            txtLocation.Properties.DataSource = dt;
            txtLocation.EditValue = txtLocation.Properties.GetDataSourceValue(txtLocation.Properties.ValueMember, 0);

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
            dt1.Rows.Add("1", "Không hiển thị");
            dt1.Rows.Add("2", "Hiển thị");
            txtStatus.Properties.DataSource = dt1;
            txtStatus.EditValue = txtStatus.Properties.GetDataSourceValue(txtStatus.Properties.ValueMember, 0);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
            //    btnLenit.Enabled = true;
            //    btnLennhieu.Enabled = true;
            //    btnXuongit.Enabled = false;
            //    btnXuongnhieu.Enabled = false;
            //}
            //catch
            //{
            //}
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
            //    btnLenit.Enabled = true;
            //    btnLennhieu.Enabled = true;
            //    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
            //    {
            //        btnXuongit.Enabled = false;
            //        btnXuongnhieu.Enabled = false;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
            //    btnXuongit.Enabled = true;
            //    btnXuongnhieu.Enabled = true;
            //    if (gridView2.FocusedRowHandle == 0)
            //    {
            //        btnLenit.Enabled = false;
            //        btnLennhieu.Enabled = false;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    gridView2.Focus();
            //    gridView2.FocusedRowHandle = 0;
            //    btnLenit.Enabled = false;
            //    btnLennhieu.Enabled = false;
            //    btnXuongit.Enabled = true;
            //    btnXuongnhieu.Enabled = true;
            //}
            //catch
            //{
            //}
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Rap())
            {
                return;
            }
            string sSQL = "";
            sSQL += "Update cinemas Set " + "\n";
            sSQL += "VNName=" + clsMain.SQLStringUnicode(txtVNName.Text) + "," + "\n";
            sSQL += "ENName=" + clsMain.SQLString(txtENName.Text) + "," + "\n";
            if (txtLocation.Text != "")
            {
                sSQL += "Location=" + clsMain.SQLString(txtLocation.EditValue.ToString ()) + "," + "\n";
            }
            sSQL += "Address=" + clsMain.SQLStringUnicode(txtAddress.Text) + "," + "\n";
            sSQL += "GoogleAddress=" + clsMain.SQLString(txtGoogleAddress.Text) + "," + "\n";
            sSQL += "Telephone=" + clsMain.SQLString(txtTelephone.Text) + "," + "\n";
            sSQL += "Fax=" + clsMain.SQLString(txtFax.Text) + "," + "\n";
            sSQL += "DescriptionFor2D=" + clsMain.SQLStringUnicode(txtDescriptionFor2D.Text) + "," + "\n";
            sSQL += "DescriptionFor3D=" + clsMain.SQLStringUnicode(txtDescriptionFor3D.Text) + "," + "\n";
            sSQL += "Status=" + clsMain.SQLString(txtStatus.EditValue.ToString ()) + "," + "\n";
            sSQL += "Image=" + clsMain.SQLString(txtImage.Text)  + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_DIADIEM Where MADIADIEM=" + clsMain.SQLString(sma ) + ")" + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_Rap()
        {
            if (txtVNName.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập thông tin đầy đủ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtVNName.Focus();
                return false;
            }
            return true;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        DataTable dtRap = new DataTable();
        private void LoaddataGridView()
        {
            string sSQL = "";
            sSQL += "Select ID,VNName,ENName,Location,Address,GoogleAddress,Telephone,Fax,DescriptionFor2D,DescriptionFor3D,Status,Image" + "\n";
            sSQL += "From cinemas" + "\n";
            sSQL += "Where ID IN (Select ID_SYNC From DM_DIADIEM Where MADIADIEM=" + clsMain.SQLString(sma ) + ")" + "\n";
            dtRap = clsMain.ReturnDataTable(sSQL);
            txtVNName.Text = dtRap.Rows[0]["VNName"].ToString();
            txtENName.Text = dtRap.Rows[0]["ENName"].ToString();
            txtLocation.EditValue = dtRap.Rows[0]["Location"];
            txtAddress.Text = dtRap.Rows[0]["Address"].ToString();
            txtGoogleAddress.Text = dtRap.Rows[0]["GoogleAddress"].ToString();
            txtTelephone.Text = dtRap.Rows[0]["Telephone"].ToString();
            txtFax.Text = dtRap.Rows[0]["Fax"].ToString();
            txtDescriptionFor2D.Text = dtRap.Rows[0]["DescriptionFor2D"].ToString();
            txtDescriptionFor3D.Text = dtRap.Rows[0]["DescriptionFor3D"].ToString();
            txtStatus.EditValue = dtRap.Rows[0]["Status"].ToString ();
            txtImage.Text = dtRap.Rows[0]["Image"].ToString();
        }

        private void Frm_Chitietrap_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnKhuVuc_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 53;
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}