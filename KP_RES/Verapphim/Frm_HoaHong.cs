﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_HoaHong : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_HoaHong()
        {
            InitializeComponent();
        }

        private void Frm_HoaHong_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnSua.Text = rm.GetString("sua", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtHoaDon.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            String sSPs = "SP_UpdateHoaHong";
            
            SqlParameter[] sParameter = new SqlParameter[6];
            sParameter[0] = new SqlParameter("@TEN_FILES", (Object)sNameFile);
            sParameter[1] = new SqlParameter("@FILE_ANH", SqlDbType.VarBinary, int.MaxValue);
            sParameter[1].Value = sByteDataFile;
            sParameter[2] = new SqlParameter("@DACHI", (Object)(chkDuyetChi.Checked == true ? "1" : "0"));
            sParameter[3] = new SqlParameter("@NGUOISUA", (Object)clsGlobal.gsUserID);
            sParameter[4] = new SqlParameter("@SOPHIEU", (Object)txtSoPhieu.Text);
            sParameter[5] = new SqlParameter("@MA", (Object)sMa);

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnSua.Focus();
            }
            else
                MessageError("Lưu không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA_HOADON").ToString();
            txtHoaDon.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA_HOADON").ToString();
            txtDaiLy.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DAILY").ToString();
            txtTongTien.Text = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TONGTIEN").ToString()).ToString("N0");
            txtChiecKhau.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CHIETKHAU").ToString();
            txtTongTienCK.Text = int.Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TONGTIENCK").ToString()).ToString ("N0");
            txtSoPhieu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOPHIEU").ToString();
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILE_ANH"));
            chkDuyetChi.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DACHI").ToString());

            KhoaMoControl(true);
        }

        private Byte[] ReturnByte(Object sObject)
        {
            Byte[] sByte = new Byte[1];
            try{
                sByte = (Byte[])sObject;
            }
            catch (Exception){
            }

            return sByte;
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectHoaHong");
            gridControl2.DataSource = myDT;
        }

        private void LoadPermission()
        {
            btnSua.Enabled = clsUserManagement.AllowEdit("9");
            btnLuu.Enabled = btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtSoPhieu.Properties.ReadOnly = sBoolean;
            chkDuyetChi.Properties.ReadOnly = sBoolean;
            btnFileCV.Enabled = !sBoolean;

        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtHoaDon.Text = "";
            txtDaiLy.Text = "";
            txtTongTien.Text = "";
            txtChiecKhau.Text = "";
            txtTongTienCK.Text = "";
            txtSoPhieu.Text = "";
            txtTenFile.Text = "";
            txtFile.Text = "";
            chkDuyetChi.Checked = false;
        }

        private Boolean CheckInput()
        {
            if (txtSoPhieu.Text == "")
            {
                MessageInformation("Nhập " + labelControl1.Text, "Thông báo");
                txtSoPhieu.Focus();
                return false;
            }
            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFileCV_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        private Boolean ReadByteFile(String sPathFile, Byte[] sByteFile)
        {
            Boolean sBoolean = true;
            try
            {
                using (FileStream sStream = new FileStream(sPathFile, FileMode.Create, FileAccess.ReadWrite))
                {
                    using (BinaryWriter bw = new BinaryWriter(sStream))
                    {
                        bw.Write(sByteFile);
                        bw.Close();
                    }
                }
            }
            catch (Exception)
            {
                sBoolean = false;
            }

            return sBoolean;
        }

        private void MessageInformation(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private String Filter()
        {
            String Filter = "";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }

    }
}