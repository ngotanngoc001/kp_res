﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Lapbanggia : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Lapbanggia()
        {
            InitializeComponent();
            LoaddataGridViewLV();
            LoaddataGridViewPP();
            LoaddataGridView_BG();
            LoadPermission();
        }

        private void Frm_Lapbanggia_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridViewLV();
            LoaddataGridViewPP();
            LoaddataGridView_BG();
            LoaddataGridView1();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("78");
            btnSua.Enabled = clsUserManagement.AllowEdit("78");
            btnXoa.Enabled = clsUserManagement.AllowDelete("78");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        string smaThoigian = "";
        private void LoadCombo()
        {
            string sSQL = "";
            sSQL = "";
            sSQL += "Select MALOAIGHE As MA,TENLOAIGHE As TEN" + "\n";
            sSQL += "From DM_LOAIGHE" + "\n";
            sSQL += "Where SUDUNG=1 ";
            DataTable  dt = clsMain.ReturnDataTable(sSQL);
            cboLOAIGHE.Properties.DataSource = dt;
            cboLOAIGHE.EditValue = cboLOAIGHE.Properties.GetDataSourceValue(cboLOAIGHE.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MALOAIVE As MA,TENLOAIVE As TEN" + "\n";
            sSQL += "From DM_LOAIVE" + "\n";
            sSQL += "Where SUDUNG=1 ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboLOAIVE.Properties.DataSource = dt;
            cboLOAIVE.EditValue = cboLOAIVE.Properties.GetDataSourceValue(cboLOAIVE.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MATHOIGIAN As MA,TENTHOIGIAN As TEN" + "\n";
            sSQL += "From DM_THOIGIAN" + "\n";
            sSQL += "Where SUDUNG=1 ";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                smaThoigian = dt.Rows[0][0].ToString();
            }
            else
            {
                MessageBox.Show("Dữ liệu mặc định sai.Liên hệ admin");
            }
        }

        private void Frm_Lapbanggia_Load(object sender, EventArgs e)
        {
            cboLOAIGHE.EditValue = cboLOAIGHE.Properties.GetDataSourceValue(cboLOAIGHE.Properties.ValueMember, 0);
            cboLOAIVE.EditValue = cboLOAIVE.Properties.GetDataSourceValue(cboLOAIVE.Properties.ValueMember, 0);
        }

        #region Loại vé
        string smaLV = "";
        DataTable dtLOAIVE = new DataTable();
        private void LoaddataGridViewLV()
        {
            SetRongControlLV();
            KhoaMoControlLV(true);
            string sSQL = "";
            sSQL += "Select MALOAIVE As MA,TENLOAIVE As TEN,GHICHU,SUDUNG,MACDINH" + "\n";
            sSQL += "From DM_LOAIVE" + "\n";
            sSQL += "Order by  MACDINH DESC,TEN" + "\n";
            dtLOAIVE = clsMain.ReturnDataTable(sSQL);
            pnlLOAIVE.Controls.Clear();
            foreach (DataRow dr in dtLOAIVE.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 50;
                btn.Font = pnlLOAIVE.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "C" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(LOAIVE_Click);
                pnlLOAIVE.Controls.Add(btn);
            }
        }

        private void LOAIVE_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlLOAIVE.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaLV = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENLV.Text = dtLOAIVE.Select("MA=" + clsMain.SQLString(smaLV))[0]["TEN"].ToString();
            txtGHICHULV.Text = dtLOAIVE.Select("MA=" + clsMain.SQLString(smaLV))[0]["GHICHU"].ToString();
            chkSUDUNGLV.Checked = bool.Parse(dtLOAIVE.Select("MA=" + clsMain.SQLString(smaLV))[0]["SUDUNG"].ToString());
            chkMacdinhLV.Checked = bool.Parse(dtLOAIVE.Select("MA=" + clsMain.SQLString(smaLV))[0]["MACDINH"].ToString());
            KhoaMoControlLV(true);
        }

        private void btnThemLV_Click(object sender, EventArgs e)
        {
            SetRongControlLV();
            KhoaMoControlLV(false);
            txtTENLV.Focus();
        }

        private void btnXoaLV_Click(object sender, EventArgs e)
        {
            if (smaLV == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DM_LOAIVE" + "\n";
            sSQL += "Where MALOAIVE=" + clsMain.SQLString(smaLV) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridViewLV();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSuaLV_Click(object sender, EventArgs e)
        {
            if (smaLV == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControlLV(false);
            txtTENLV.Focus();
        }

        private void btnLuuLV_Click(object sender, EventArgs e)
        {
            if (!CheckInputLV())
            {
                return;
            }
            string sSQL = "";
            if (smaLV == "" && btnThemLV.Enabled)
            {
                sSQL += "Insert into DM_LOAIVE (TENLOAIVE,GHICHU,SUDUNG,MACDINH)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENLV.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHULV.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGLV.Checked) + ",";
                sSQL += clsMain.SQLBit(chkMacdinhLV.Checked) + ")";
            }
            else if ((smaLV != "" && btnSuaLV.Enabled))
            {
                sSQL += "Update DM_LOAIVE Set " + "\n";
                sSQL += "TENLOAIVE=" + clsMain.SQLStringUnicode(txtTENLV.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHULV.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGLV.Checked) + "," + "\n";
                sSQL += "MACDINH=" + clsMain.SQLBit(chkMacdinhLV.Checked) + "\n";
                sSQL += "Where MALOAIVE=" + clsMain.SQLString(smaLV) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridViewLV();
                btnThemLV.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControlLV(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENLV.Properties.ReadOnly = true;
                txtGHICHULV.Properties.ReadOnly = true;
                chkSUDUNGLV.Properties.ReadOnly = true;
                chkMacdinhLV.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENLV.Properties.ReadOnly = false;
                txtGHICHULV.Properties.ReadOnly = false;
                chkSUDUNGLV.Properties.ReadOnly = false;
                chkMacdinhLV.Properties.ReadOnly = false;
            }
        }

        private void SetRongControlLV()
        {
            smaLV = "";
            txtTENLV.Text = "";
            txtGHICHULV.Text = "";
            chkSUDUNGLV.Checked = true;
            chkMacdinhLV.Checked = false;
        }

        private Boolean CheckInputLV()
        {
            if (txtTENLV.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENLV.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENLV.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Bảng giá
        string smaBANGGIA = "";
        DataTable dtBANGGIA = new DataTable();
        private void LoaddataGridView_BG()
        {
            SetRongControl_BG();
            KhoaMoControl_BG(true);
            LoadCombo();

            string sSQL = "";
            sSQL += "Select MABANGGIA As MA,TENBANGGIA As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From BANGGIA" + "\n";
            sSQL += "Where  MABANGGIA NOT IN (Select MA_BANGGIA From THIETLAPBANGGIA)" + "\n";
            sSQL += "Order by SUDUNG DESC,TEN" + "\n";
            dtBANGGIA = clsMain.ReturnDataTable(sSQL);
            pnlBANGGIA.Controls.Clear();
            foreach (DataRow dr in dtBANGGIA.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 50;
                btn.Font = pnlBANGGIA.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "B" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(BANGGIA_Click);
                pnlBANGGIA.Controls.Add(btn);
            }
            if (dtBANGGIA.Rows.Count > 0)
            {
                BANGGIA_Click(pnlBANGGIA.Controls[0], null);
            }
        }

        private void BANGGIA_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlBANGGIA.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            
            smaBANGGIA = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTEN.Text = dtBANGGIA.Select("MA=" + clsMain.SQLString(smaBANGGIA))[0]["TEN"].ToString();
            txtGHICHU.Text = dtBANGGIA.Select("MA=" + clsMain.SQLString(smaBANGGIA))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtBANGGIA.Select("MA=" + clsMain.SQLString(smaBANGGIA))[0]["SUDUNG"].ToString());
            if (bSaochep)
            {
                Saochepbanggia(smacu, smaBANGGIA);
            }
            KhoaMoControl_BG(true);
            LoaddataGridView1();
        }

        bool bSaochep = false;
        string smacu = "";
        private void btnSaochep_Click(object sender, EventArgs e)
        {
            if (smaBANGGIA == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chọn bảng giá nguồn để sao chép", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chọn bảng giá đích để sao chép", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bSaochep = true;
                smacu = smaBANGGIA;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl_BG();
            KhoaMoControl_BG(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (smaBANGGIA == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From BANGGIA" + "\n";
            sSQL += "Where MABANGGIA=" + clsMain.SQLString(smaBANGGIA) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_BG();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaBANGGIA == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_BG(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput_BG())
            {
                return;
            }
            string sSQL = "";
            if (smaBANGGIA == "" && btnThem.Enabled)
            {
                sSQL += "Insert into BANGGIA (TENBANGGIA,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((smaBANGGIA != "" && btnSua.Enabled))
            {
                sSQL += "Update BANGGIA Set " + "\n";
                sSQL += "TENBANGGIA=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MABANGGIA=" + clsMain.SQLString(smaBANGGIA) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_BG();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControl_BG(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl_BG()
        {
            smaBANGGIA = "";
            txtTEN.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput_BG()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void Saochepbanggia(string manguon, string madich)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn sao chép bảng giá ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                bSaochep = false;
                return;
            }
            string sSQL = "";
            sSQL += "Select  MABANGGIA,MALOAIGHE,MALOAIVE,MATHOIGIAN,DONGIA" + "\n";
            sSQL += "From CHITIETBANGGIA A" + "\n";
            sSQL += "Where MABANGGIA=" + clsMain.SQLString(manguon) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            sSQL = "";

            sSQL += "Delete From CHITIETBANGGIA" + "\n";
            sSQL += "Where MABANGGIA=" + clsMain.SQLString(madich) + "\n";
            foreach (DataRow dr in dt.Rows)
            {
                sSQL += "Insert into CHITIETBANGGIA (MABANGGIA,MALOAIGHE,MALOAIVE,MATHOIGIAN,DONGIA)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(madich) + ",";
                sSQL += clsMain.SQLString(dr["MALOAIGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["MALOAIVE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["MATHOIGIAN"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["DONGIA"].ToString()) + ")";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Sao chép thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Sao chép không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            bSaochep = false;
        }
        #endregion

        #region Chi tiết bảng giá
        string smaChitietbanggia = "";
        int igrid = 1;
        private void LoaddataGridView1()
        {
            SetRongControl1();
            KhoaMoControl1(true);

            string sSQL = "";
            sSQL += "Select A.MACHITIETBANGGIA As MA,A.MABANGGIA,A.MALOAIGHE,A.MALOAIVE,A.MATHOIGIAN,A.DONGIA,B.TENBANGGIA,C.TENLOAIGHE,D.TENLOAIVE, E.TENTHOIGIAN" + "\n";
            sSQL += "From CHITIETBANGGIA A, BANGGIA B, DM_LOAIGHE C , DM_LOAIVE D, DM_THOIGIAN E" + "\n";
            sSQL += "Where A.MABANGGIA=B.MABANGGIA" + "\n";
            sSQL += "And A.MALOAIGHE=C.MALOAIGHE" + "\n";
            sSQL += "And A.MALOAIVE=D.MALOAIVE" + "\n";
            sSQL += "And A.MATHOIGIAN=E.MATHOIGIAN" + "\n";
            sSQL += "And A.MABANGGIA="+ clsMain .SQLString (smaBANGGIA ) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl1.DataSource = dt;
            gridView1.ExpandAllGroups();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 1;
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            smaChitietbanggia = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            cboLOAIGHE.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MALOAIGHE").ToString());
            cboLOAIVE.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MALOAIVE").ToString());
            txtGIAVE.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DONGIA").ToString());
            KhoaMoControl1(true);
        }

        private void btnThem1_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            cboLOAIGHE.Focus();
        }

        private void btnSua1_Click(object sender, EventArgs e)
        {
            if (smaChitietbanggia == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboLOAIGHE.Focus();
        }

        private void btnXoa1_Click(object sender, EventArgs e)
        {
            if (smaChitietbanggia == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From CHITIETBANGGIA" + "\n";
            sSQL += "Where MACHITIETBANGGIA=" + clsMain.SQLString(smaChitietbanggia) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                //LoaddataGridView_BG();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuu1_Click(object sender, EventArgs e)
        {
            if (!CheckInput1())
            {
                return;
            }
            string sSQL = "";
            if (smaChitietbanggia == "" && btnThem1.Enabled)
            {
                sSQL += "Delete From CHITIETBANGGIA" + "\n";
                sSQL += "Where MABANGGIA=" + clsMain.SQLString(smaBANGGIA) + "\n";
                sSQL += "And MALOAIGHE=" + clsMain.SQLString(cboLOAIGHE.EditValue.ToString()) + "\n";
                sSQL += "And MALOAIVE=" + clsMain.SQLString(cboLOAIVE.EditValue.ToString()) + "\n";

                sSQL += "Insert into CHITIETBANGGIA (MABANGGIA,MALOAIGHE,MALOAIVE,MATHOIGIAN,DONGIA)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(smaBANGGIA) + ",";
                sSQL += clsMain.SQLString(cboLOAIGHE.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(cboLOAIVE.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(smaThoigian) + ",";
                sSQL += clsMain.SQLString(txtGIAVE.Text.Replace(",", "")) + ")";
            }
            else if ((smaChitietbanggia != "" && btnSua1.Enabled))
            {
                sSQL += "Update CHITIETBANGGIA Set " + "\n";
                sSQL += "MABANGGIA=" + clsMain.SQLString(smaBANGGIA) + "," + "\n";
                sSQL += "MALOAIGHE=" + clsMain.SQLString(cboLOAIGHE.EditValue.ToString()) + "," + "\n";
                sSQL += "MALOAIVE=" + clsMain.SQLString(cboLOAIVE.EditValue.ToString()) + "," + "\n";
                sSQL += "MATHOIGIAN=" + clsMain.SQLString(smaThoigian) + "," + "\n";
                sSQL += "DONGIA=" + clsMain.SQLString(txtGIAVE.Text.Replace(",", "")) + "\n";
                sSQL += "Where MACHITIETBANGGIA=" + clsMain.SQLString(smaChitietbanggia) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                btnThem1.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuutacca_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            DataTable dt = ((DataView)gridView1.DataSource).ToTable();
            foreach (DataRow dr in dt.Rows)
            {
                sSQL += "Update CHITIETBANGGIA Set " + "\n";
                sSQL += "DONGIA=" + clsMain.SQLString(dr["DONGIA"].ToString()) + "\n";
                sSQL += "Where MACHITIETBANGGIA=" + clsMain.SQLString(dr["MA"].ToString()) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                btnThem1.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                cboLOAIGHE.Properties.ReadOnly = true;
                cboLOAIVE.Properties.ReadOnly = true;
                txtGIAVE.Properties.ReadOnly = true;

            }
            else//mở
            {
                cboLOAIGHE.Properties.ReadOnly = false;
                cboLOAIVE.Properties.ReadOnly = false;
                txtGIAVE.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl1()
        {
            smaChitietbanggia = "";
            txtGIAVE.EditValue = 0;
        }

        private Boolean CheckInput1()
        {
            if (txtGIAVE.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTYGIA.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtGIAVE.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Phụ phí
        string smaPP = "";
        DataTable dtPP = new DataTable();
        private void LoaddataGridViewPP()
        {
            SetRongControlPP();
            KhoaMoControlPP(true);
            string sSQL = "";
            sSQL += "Select MAPHUPHI As MA,TENPHUPHI As TEN,GHICHU,SUDUNG,GIATRI" + "\n";
            sSQL += "From PHUPHI" + "\n";
            sSQL += "Order by TEN" + "\n";
            dtPP = clsMain.ReturnDataTable(sSQL);
            pnlPHUPHI.Controls.Clear();
            foreach (DataRow dr in dtPP.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 50;
                btn.Font = pnlLOAIVE.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "C" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(PHUPHI_Click);
                pnlPHUPHI.Controls.Add(btn);
            }
        }

        private void PHUPHI_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlPHUPHI.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaPP = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTenPhuPhi.Text = dtPP.Select("MA=" + clsMain.SQLString(smaPP))[0]["TEN"].ToString();
            txtGHICHUPP.Text = dtPP.Select("MA=" + clsMain.SQLString(smaPP))[0]["GHICHU"].ToString();
            txtGiaTri .Text = dtPP.Select("MA=" + clsMain.SQLString(smaPP))[0]["GIATRI"].ToString();
            chkSUDUNGPP.Checked = bool.Parse(dtPP.Select("MA=" + clsMain.SQLString(smaPP))[0]["SUDUNG"].ToString());
            KhoaMoControlPP(true);
        }

        private void btnThemPP_Click(object sender, EventArgs e)
        {
            SetRongControlPP();
            KhoaMoControlPP(false);
            txtTenPhuPhi.Focus();
        }

        private void btnXoaPP_Click(object sender, EventArgs e)
        {
            if (smaPP == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From PHUPHI" + "\n";
            sSQL += "Where MAPHUPHI=" + clsMain.SQLString(smaPP) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridViewPP();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSuaPP_Click(object sender, EventArgs e)
        {
            if (smaPP == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControlPP(false);
            txtTenPhuPhi.Focus();
        }

        private void btnLuuPP_Click(object sender, EventArgs e)
        {
            if (!CheckInputPP())
            {
                return;
            }
            string sSQL = "";
            if (smaPP == "" && btnThemPP.Enabled)
            {
                sSQL += "Insert into PHUPHI (TENPHUPHI,GHICHU,SUDUNG,GIATRI)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTenPhuPhi.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHUPP.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGPP.Checked) + ",";
                sSQL += clsMain.SQLString (txtGiaTri .Text .Replace (",","")) + ")";
            }
            else if ((smaPP  != "" && btnSuaPP.Enabled))
            {
                sSQL += "Update PHUPHI Set " + "\n";
                sSQL += "TENPHUPHI=" + clsMain.SQLStringUnicode(txtTenPhuPhi.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUPP.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGPP.Checked) + "," + "\n";
                sSQL += "GIATRI=" + clsMain.SQLString(txtGiaTri.Text.Replace(",", "")) + "\n";
                sSQL += "Where MAPHUPHI=" + clsMain.SQLString(smaPP) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridViewPP();
                btnThemPP.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControlPP(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTenPhuPhi.Properties.ReadOnly = true;
                txtGHICHUPP.Properties.ReadOnly = true;
                chkSUDUNGPP.Properties.ReadOnly = true;
                txtGiaTri .Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTenPhuPhi.Properties.ReadOnly = false;
                txtGHICHUPP.Properties.ReadOnly = false;
                chkSUDUNGPP.Properties.ReadOnly = false;
                txtGiaTri.Properties.ReadOnly = false;
            }
        }

        private void SetRongControlPP()
        {
            smaPP = "";
            txtTenPhuPhi.Text = "";
            txtGHICHUPP.Text = "";
            chkSUDUNGPP.Checked = true;
            txtGiaTri.EditValue  = 0;
        }

        private Boolean CheckInputPP()
        {
            if (txtTenPhuPhi.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENPP.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenPhuPhi.Focus();
                return false;
            }
            return true;
        }
        #endregion
    }
}