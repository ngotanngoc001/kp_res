﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid;

namespace KP_RES 
{
    public partial class Frm_BC_BVRAP : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_BVRAP()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_BVRAP_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
            panelControl8.Height = panelControl1.Height;
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        DataTable dtCombo = new DataTable();
        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            //cboDATA.Properties.Columns.Clear();
            //cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Rạp
            {
                dtCombo = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1 Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Rạp";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 1)//Phòng
            {
                dtCombo = clsMain.ReturnDataTable("select A.MAPHONG as MA,A.TENPHONG+' - '+B.TENDIADIEM as TEN from DM_PHONG A, DM_DIADIEM B where A.MARAP=B.MADIADIEM AND A.SUDUNG=1  Order by B.TENDIADIEM,TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Phòng";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 2)//Nhà phát hành
            {
                dtCombo = clsMain.ReturnDataTable("select MANHAPHATHANH as MA,TENNHAPHATHANH as TEN from DM_NHAPHATHANH where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhà phát hành";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 3)//Phim
            {
                dtCombo = clsMain.ReturnDataTable("select MAPHIM as MA,TENPHIM as TEN from DM_PHIM where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Phim";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 4)//Loại ghế
            {
                dtCombo = clsMain.ReturnDataTable("select MALOAIGHE AS MA,TENLOAIGHE AS TEN from DM_LOAIGHE where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Loại ghế";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 5)//Loại vé
            {
                dtCombo = clsMain.ReturnDataTable("select MALOAIVE AS MA,TENLOAIVE AS TEN from DM_LOAIVE where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Loại vé";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 6)//Nhân viên
            {
                dtCombo = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhân viên";
                LoadChiTietCombo(dtCombo);
            }
         
            //cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void LoadChiTietCombo(DataTable dt)
        {
            cboDATA.Properties.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                cboDATA.Properties.Items.Add(dr["TEN"].ToString());
            }
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            //if (Caption.Length > 1 & dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < Caption.Length; i++)
            //    {
            //        LookUpColumnInfo info = new LookUpColumnInfo();
            //        info.FieldName = FieldName[i].TrimEnd();
            //        info.Caption = Caption[i].TrimEnd();
            //        info.Visible = visible[i];
            //        info.Alignment = DevExpress.Utils.HorzAlignment.Default;
            //        cboDATA.Properties.Columns.Add(info);
            //    }
            //    cboDATA.Properties.BestFitMode = BestFitMode.None;
            //    cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
            //    cboDATA.Properties.DisplayMember = FieldName[1];
            //    cboDATA.Properties.ValueMember = FieldName[0];
            //    if (visible[0] == true)
            //    {
            //        cboDATA.Properties.Columns[0].Width = 100;
            //        cboDATA.Properties.Columns[1].Width = 500;
            //    }
            //    else
            //    {
            //        cboDATA.Properties.Columns[1].Width = 100;
            //        cboDATA.Properties.Columns[2].Width = 500;
            //    }
            //    if (dt.Rows.Count > 0)
            //        cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            //}
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            string sql1 = string.Empty;
            string sql2 = string.Empty;
            if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value || cboDATA.EditValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn đối tượng cần xem", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                splashScreenManager1.ShowWaitForm();
                //25/12 sửa lại cho đúng số tiền
                sql1 += "Select A.LOAIVE As LOAIVE1,A.MABARCODE,A.SOGHE,A.LYDO,A.MAPHONG,A.MAPHIM,A.MALOAIGHE,A.MALOAIVE,A.NGAYTAO,A.DONGIA,C.TENLOAIGHE,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM,K.NGAYCHIEU,SUBSTRING(K.BATDAU,1,5) As BATDAU,K.KETTHUC" + "\n";
                sql1 += "From VETONGHOP A, DM_LOAIGHE C , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J,LICHCHIEU K" + "\n";
                sql1 += "Where A.MALOAIGHE = C.MALOAIGHE" + "\n";
                sql1 += "And A.MAPHONG = E.MAPHONG" + "\n";
                sql1 += "And A.MAPHIM = F.MAPHIM" + "\n";
                sql1 += "And A.NGUOIBAN = H.UserID" + "\n";
                sql1 += "And H.MaNV = G.MANHANVIEN" + "\n";
                sql1 += "And A.MALOAIVE = I.MALOAIVE" + "\n";
                sql1 += "And E.MARAP = J.MADIADIEM" + "\n";
                sql1 += "And A.MALICHCHIEU = K.MALICHCHIEU" + "\n";

                sql1 += "And A.LOAIVE= " + clsMain.SQLStringUnicode("Vé bán") + "\n";

                sql1 += "And A.NGAYTAO BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";

                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And J.MADIADIEM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 1)//Phòng
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And E.MAPHONG= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " E.MAPHONG= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or E.MAPHONG= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And F.NSX= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " F.NSX= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or F.NSX= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 3)//Phim
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And A.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 4)//Loại ghế
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And A.MALOAIGHE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MALOAIGHE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MALOAIGHE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 5)//Loại vé
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And A.MALOAIVE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MALOAIVE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MALOAIVE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 6)//Nhân viên
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql1 += "And A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + ")" + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                                else
                                {
                                    sqlTemp += " Or A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                            }
                        }
                    }
                    sql1 += "And ( " + sqlTemp + ")" + "\n";
                }
                if (optGroup1.SelectedIndex == 3)
                {
                    sql1 += "And K.NGAYCHIEU > " + clsMain.SQLString(dtpDenngay.Text ) + "\n";
                }
                //25/12 sửa lại cho đúng số tiền
                sql2 += "Select A.LOAIVE As LOAIVE1,A.MABARCODE,A.SOGHE,A.LYDO,A.MAPHONG,A.MAPHIM,A.MALOAIGHE,A.MALOAIVE,A.NGAYBAN As NGAYTAO,A.DONGIA,C.TENLOAIGHE,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM,K.NGAYCHIEU,SUBSTRING(K.BATDAU,1,5) As BATDAU,K.KETTHUC" + "\n";
                sql2 += "From VETONGHOP A, DM_LOAIGHE C , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J,LICHCHIEU K" + "\n";
                sql2 += "Where A.MALOAIGHE = C.MALOAIGHE" + "\n";
                sql2 += "And A.MAPHONG = E.MAPHONG" + "\n";
                sql2 += "And A.MAPHIM = F.MAPHIM" + "\n";
                sql2 += "And A.NGUOIBAN = H.UserID" + "\n";
                sql2 += "And H.MaNV = G.MANHANVIEN" + "\n";
                sql2 += "And A.MALOAIVE = I.MALOAIVE" + "\n";
                sql2 += "And E.MARAP = J.MADIADIEM" + "\n";
                sql2 += "And A.MALICHCHIEU = K.MALICHCHIEU" + "\n";

                sql2 += "And A.LOAIVE= " + clsMain.SQLStringUnicode("Vé hủy") + "\n";

                sql2 += "And A.NGAYTAO BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";

                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And J.MADIADIEM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 1)//Phòng
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And E.MAPHONG= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " E.MAPHONG= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or E.MAPHONG= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And F.NSX= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " F.NSX= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or F.NSX= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 3)//Phim
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And A.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 4)//Loại ghế
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And A.MALOAIGHE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MALOAIGHE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MALOAIGHE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 5)//Loại vé
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And A.MALOAIVE= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MALOAIVE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MALOAIVE= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 6)//Nhân viên
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql2 += "And A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + ")" + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                                else
                                {
                                    sqlTemp += " Or A.NGUOITAO in (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                            }
                        }
                    }
                    sql2 += "And ( " + sqlTemp + ")" + "\n";
                }
                if (optGroup1.SelectedIndex == 3)
                {
                    sql2 += "And K.NGAYCHIEU > " + clsMain.SQLString(dtpDenngay.Text) + "\n";
                }

                if (optGroup1.SelectedIndex == 0 || optGroup1.SelectedIndex == 3)
                {
                    sql = sql1;
                    sql += "UNION" + "\n";
                    sql += sql2  + "\n";
                }
                else if (optGroup1.SelectedIndex == 1)
                {
                    sql = sql1;
                }
                else if (optGroup1.SelectedIndex == 2)
                {
                    sql = sql2;
                }
                //Sắp xếp theo đúng thời gian bán
                sql += "Order By A.NGAYTAO" + "\n";

                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.ExpandAllGroups();
                
                if (optGroup1.SelectedIndex != 0)
                {
                    DONGIA.Summary.Clear();
                    GridColumnSummaryItem item = new GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DONGIA", "{0:#,###}");
                    DONGIA.Summary.Add(item);
                }

                int vb = 0;
                int vh = 0;
                long tb = 0;
                int th = 0;
                int vbht = 0;
                long tbht = 0;
                int vhht = 0;
                int thht = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["LOAIVE1"].ToString() == "Vé bán")
                    {
                        vb = vb + 1;
                        tb = tb + long.Parse(dr["DONGIA"].ToString());


                        if (DateTime.ParseExact((cls_KP_RES.DateToString_Date((DateTime)dr["NGAYCHIEU"])), "dd/MM/yyyy", null) <= DateTime.ParseExact(dtpDenngay.Text, "dd/MM/yyyy", null))
                        {
                            vbht = vbht + 1;
                            tbht = tbht + long.Parse(dr["DONGIA"].ToString());
                        }
                    }
                    else
                    {
                        vh = vh + 1;
                        th = th + int.Parse(dr["DONGIA"].ToString());

                        if (DateTime.ParseExact((cls_KP_RES.DateToString_Date((DateTime)dr["NGAYCHIEU"])), "dd/MM/yyyy", null) <= DateTime.ParseExact(dtpDenngay.Text, "dd/MM/yyyy", null))
                        {
                            vhht = vhht + 1;
                            thht = thht + int.Parse(dr["DONGIA"].ToString());
                        }
                    }
                }


                lblTongveban.Text = "Tổng vé bán : " + vb.ToString("n0");
                lblTongtienban.Text = "Tổng tiền bán : " + tb.ToString("n0");
                lblTongvehuy.Text = "Tổng vé hủy : " + vh.ToString("n0");
                lblTongtienhuy.Text = "Tổng tiền hủy : " + th.ToString("n0");
                lblSoducuoi.Text = "Số dư cuối : " + (tb - th).ToString("n0");

                lblVbht.Text = "- Số vé : " + vbht.ToString("n0");
                lblTbht.Text = "- Tổng tiền : " + tbht.ToString("n0");
                lblVbtr.Text = "- Số vé : " + (vb - vbht).ToString("n0");
                lblTbtr.Text = "- Tổng tiền : " + (tb - tbht).ToString("n0");


                lblVhht.Text = "- Số vé : " + vhht.ToString("n0");
                lblThht.Text = "- Tổng tiền : " + thht.ToString("n0");
                lblVhtr.Text = "- Số vé : " + (vh - vhht).ToString("n0");
                lblThtr.Text = "- Tổng tiền : " + (th - thht).ToString("n0");

                lblSoducuoiht.Text = "Hiện tại: " + (tbht - thht).ToString("n0");
                lblSoducuoibtr.Text = "Bán trước: " + ((tb - tbht) - (th - thht)).ToString("n0");

                fpnlGheban.Controls.Clear();
                DataTable dtLoaighe = clsMain.ReturnDataTable("Select MALOAIGHE,TENLOAIGHE From DM_LOAIGHE");
                foreach (DataRow dr in dtLoaighe.Rows)
                {
                   DataRow[] drs = dt.Select("MALOAIGHE=" + clsMain.SQLString(dr["MALOAIGHE"].ToString()) + " And LOAIVE1='Vé bán'");
                   if (drs.Length > 0)
                   {
                       LabelControl lbl = new LabelControl();
                       lbl.Font = fpnlGheban.Font;
                       lbl.Text ="- "+ dr["TENLOAIGHE"].ToString() + " : " + drs.Length.ToString()+"  ";
                       fpnlGheban.Controls.Add(lbl);
                   }
                }
                fpnlVeban.Controls.Clear();
                DataTable dtLoaive = clsMain.ReturnDataTable("Select MALOAIVE,TENLOAIVE From DM_LOAIVE");
                foreach (DataRow dr in dtLoaive.Rows)
                {
                    DataRow[] drs = dt.Select("MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + " And LOAIVE1='Vé bán'");
                    if (drs.Length > 0)
                    {
                        LabelControl lbl = new LabelControl();
                        lbl.Font = fpnlGheban.Font;
                        lbl.Text = "- " + dr["TENLOAIVE"].ToString() + " : " + drs.Length.ToString() + "  ";
                        fpnlVeban.Controls.Add(lbl);
                    }
                }

                fpnlGhehuy.Controls.Clear();
                foreach (DataRow dr in dtLoaighe.Rows)
                {
                    DataRow[] drs = dt.Select("MALOAIGHE=" + clsMain.SQLString(dr["MALOAIGHE"].ToString()) + " And LOAIVE1='Vé hủy'");
                    if (drs.Length > 0)
                    {
                        LabelControl lbl = new LabelControl();
                        lbl.Font = fpnlGheban.Font;
                        lbl.Text = "- " + dr["TENLOAIGHE"].ToString() + " : " + drs.Length.ToString() + "  ";
                        fpnlGhehuy.Controls.Add(lbl);
                    }
                }
                fpnlVehuy.Controls.Clear();
                foreach (DataRow dr in dtLoaive.Rows)
                {
                    DataRow[] drs = dt.Select("MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + " And LOAIVE1='Vé hủy'");
                    if (drs.Length > 0)
                    {
                        LabelControl lbl = new LabelControl();
                        lbl.Font = fpnlGheban.Font;
                        lbl.Text = "- " + dr["TENLOAIVE"].ToString() + " : " + drs.Length.ToString() + "  ";
                        fpnlVehuy.Controls.Add(lbl);
                    }
                }
                splashScreenManager1.CloseWaitForm();
            }
            catch (Exception ex)
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount >0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    DataColumn colTYPE = new DataColumn();
                    colTYPE.ColumnName = "TYPE";
                    colTYPE.DataType = System.Type.GetType("System.String");
                    if (optGroup.SelectedIndex == 0)//Rạp
                    {
                        colTYPE.DefaultValue = "Rạp";
                    }
                    else if (optGroup.SelectedIndex == 1)//Phòng
                    {
                        colTYPE.DefaultValue = "Phòng";
                    }
                    else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                    {
                        colTYPE.DefaultValue = "Nhà phát hành";
                    }
                    else if (optGroup.SelectedIndex == 3)//Phim
                    {
                        colTYPE.DefaultValue = "Phim";
                    }
                    else if (optGroup.SelectedIndex == 4)//Loại ghế
                    {
                        colTYPE.DefaultValue = "Loại ghế";
                    }
                    else if (optGroup.SelectedIndex == 5)//Loại vé
                    {
                        colTYPE.DefaultValue = "Loại vé";
                    }
                    else if (optGroup.SelectedIndex == 6)//Nhân viên
                    {
                        colTYPE.DefaultValue = "Nhân viên";
                    }

                    DataColumn colNAMETYPE = new DataColumn();
                    colNAMETYPE.ColumnName = "NAMETYPE";
                    colNAMETYPE.DataType = System.Type.GetType("System.String");
                    //colNAMETYPE.DefaultValue = cboDATA.Text;
                    bool bAll = true;
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Unchecked)
                        {
                            bAll = false;
                            break;
                        }
                    }
                    if (bAll)
                    {
                        colNAMETYPE.DefaultValue = "Tất cả";
                    }
                    else
                    {
                        colNAMETYPE.DefaultValue = cboDATA.Text;
                    }

                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TYPE", dt) == false)
                        dt.Columns.Add(colTYPE);
                    if (ContainColumn("NAMETYPE", dt) == false)
                        dt.Columns.Add(colNAMETYPE);
                  
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 33;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Không có dữ liệu");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        int iHeightpanelControl8 =0;
        private void btnAnformchung_Click(object sender, EventArgs e)
        {
            if (panelControl8.Height > panelControl12.Height)
            {
                iHeightpanelControl8 = panelControl8.Height;
                panelControl8.Height = panelControl12.Height;
                btnAnformchung.Image = global::KP_RES.Properties.Resources.up11_26;
            }
            else
            {
                panelControl8.Height = iHeightpanelControl8;
                btnAnformchung.Image = global::KP_RES.Properties.Resources.up22_26;
            }
        }
    }
}