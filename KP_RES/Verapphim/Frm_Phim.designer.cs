﻿namespace KP_RES 
{
    partial class Frm_Phim 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Phim));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkFast = new DevExpress.XtraEditors.CheckEdit();
            this.btnKhac = new DevExpress.XtraEditors.SimpleButton();
            this.btnThongtinkhac = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.picLOAITROCHOI = new System.Windows.Forms.PictureBox();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtNOIDUNG = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtNAM = new DevExpress.XtraEditors.TextEdit();
            this.lblTHOILUONG = new DevExpress.XtraEditors.LabelControl();
            this.txtTHOILUONG = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtDIENVIEN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDAODIEN = new DevExpress.XtraEditors.TextEdit();
            this.cboTheloai = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cboNSX = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENTHELOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNSX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOILUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THELOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NSX = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFast.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLOAITROCHOI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOIDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNAM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHOILUONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENVIEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDAODIEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTheloai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNSX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkFast);
            this.panelControl1.Controls.Add(this.btnKhac);
            this.panelControl1.Controls.Add(this.btnThongtinkhac);
            this.panelControl1.Controls.Add(this.panelControl8);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtNOIDUNG);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtNAM);
            this.panelControl1.Controls.Add(this.lblTHOILUONG);
            this.panelControl1.Controls.Add(this.txtTHOILUONG);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtDIENVIEN);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtDAODIEN);
            this.panelControl1.Controls.Add(this.cboTheloai);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cboNSX);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.chkSUDUNG);
            this.panelControl1.Controls.Add(this.lblGHICHU);
            this.panelControl1.Controls.Add(this.txtGHICHU);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtTEN);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1071, 221);
            this.panelControl1.TabIndex = 0;
            // 
            // chkFast
            // 
            this.chkFast.EditValue = true;
            this.chkFast.EnterMoveNextControl = true;
            this.chkFast.Location = new System.Drawing.Point(187, 158);
            this.chkFast.Name = "chkFast";
            this.chkFast.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFast.Properties.Appearance.Options.UseFont = true;
            this.chkFast.Properties.Caption = "Chỉ lấy 200 phim gần đây";
            this.chkFast.Size = new System.Drawing.Size(284, 24);
            this.chkFast.TabIndex = 37;
            // 
            // btnKhac
            // 
            this.btnKhac.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhac.Appearance.Options.UseFont = true;
            this.btnKhac.Image = global::KP_RES.Properties.Resources.ie_26;
            this.btnKhac.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnKhac.Location = new System.Drawing.Point(687, 183);
            this.btnKhac.Margin = new System.Windows.Forms.Padding(4);
            this.btnKhac.Name = "btnKhac";
            this.btnKhac.Size = new System.Drawing.Size(80, 35);
            this.btnKhac.TabIndex = 25;
            this.btnKhac.Text = "&Web";
            this.btnKhac.Click += new System.EventHandler(this.btnKhac_Click);
            // 
            // btnThongtinkhac
            // 
            this.btnThongtinkhac.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThongtinkhac.Appearance.Options.UseFont = true;
            this.btnThongtinkhac.Image = global::KP_RES.Properties.Resources.list_26;
            this.btnThongtinkhac.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThongtinkhac.Location = new System.Drawing.Point(443, 183);
            this.btnThongtinkhac.Margin = new System.Windows.Forms.Padding(4);
            this.btnThongtinkhac.Name = "btnThongtinkhac";
            this.btnThongtinkhac.Size = new System.Drawing.Size(236, 35);
            this.btnThongtinkhac.TabIndex = 24;
            this.btnThongtinkhac.Text = "Các thông tin khác về phim";
            this.btnThongtinkhac.Click += new System.EventHandler(this.btnThongtinkhac_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.picLOAITROCHOI);
            this.panelControl8.Location = new System.Drawing.Point(726, 5);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(169, 154);
            this.panelControl8.TabIndex = 23;
            // 
            // picLOAITROCHOI
            // 
            this.picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picLOAITROCHOI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picLOAITROCHOI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLOAITROCHOI.Location = new System.Drawing.Point(2, 2);
            this.picLOAITROCHOI.Name = "picLOAITROCHOI";
            this.picLOAITROCHOI.Size = new System.Drawing.Size(165, 150);
            this.picLOAITROCHOI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLOAITROCHOI.TabIndex = 23;
            this.picLOAITROCHOI.TabStop = false;
            this.picLOAITROCHOI.Click += new System.EventHandler(this.picLOAITROCHOI_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 104);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 19);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Nội dung";
            // 
            // txtNOIDUNG
            // 
            this.txtNOIDUNG.EnterMoveNextControl = true;
            this.txtNOIDUNG.Location = new System.Drawing.Point(91, 101);
            this.txtNOIDUNG.Name = "txtNOIDUNG";
            this.txtNOIDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNOIDUNG.Properties.Appearance.Options.UseFont = true;
            this.txtNOIDUNG.Size = new System.Drawing.Size(248, 26);
            this.txtNOIDUNG.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(346, 40);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(109, 19);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Năm phát hành";
            // 
            // txtNAM
            // 
            this.txtNAM.EnterMoveNextControl = true;
            this.txtNAM.Location = new System.Drawing.Point(472, 37);
            this.txtNAM.Name = "txtNAM";
            this.txtNAM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNAM.Properties.Appearance.Options.UseFont = true;
            this.txtNAM.Size = new System.Drawing.Size(248, 26);
            this.txtNAM.TabIndex = 11;
            // 
            // lblTHOILUONG
            // 
            this.lblTHOILUONG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTHOILUONG.Location = new System.Drawing.Point(346, 104);
            this.lblTHOILUONG.Margin = new System.Windows.Forms.Padding(4);
            this.lblTHOILUONG.Name = "lblTHOILUONG";
            this.lblTHOILUONG.Size = new System.Drawing.Size(122, 19);
            this.lblTHOILUONG.TabIndex = 14;
            this.lblTHOILUONG.Text = "Thời lượng(phút)";
            // 
            // txtTHOILUONG
            // 
            this.txtTHOILUONG.EnterMoveNextControl = true;
            this.txtTHOILUONG.Location = new System.Drawing.Point(472, 101);
            this.txtTHOILUONG.Name = "txtTHOILUONG";
            this.txtTHOILUONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHOILUONG.Properties.Appearance.Options.UseFont = true;
            this.txtTHOILUONG.Size = new System.Drawing.Size(248, 26);
            this.txtTHOILUONG.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(6, 72);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Diễn viên";
            // 
            // txtDIENVIEN
            // 
            this.txtDIENVIEN.EnterMoveNextControl = true;
            this.txtDIENVIEN.Location = new System.Drawing.Point(91, 69);
            this.txtDIENVIEN.Name = "txtDIENVIEN";
            this.txtDIENVIEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDIENVIEN.Properties.Appearance.Options.UseFont = true;
            this.txtDIENVIEN.Size = new System.Drawing.Size(248, 26);
            this.txtDIENVIEN.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 40);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Đạo diễn";
            // 
            // txtDAODIEN
            // 
            this.txtDAODIEN.EnterMoveNextControl = true;
            this.txtDAODIEN.Location = new System.Drawing.Point(91, 37);
            this.txtDAODIEN.Name = "txtDAODIEN";
            this.txtDAODIEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDAODIEN.Properties.Appearance.Options.UseFont = true;
            this.txtDAODIEN.Size = new System.Drawing.Size(248, 26);
            this.txtDAODIEN.TabIndex = 3;
            // 
            // cboTheloai
            // 
            this.cboTheloai.EnterMoveNextControl = true;
            this.cboTheloai.Location = new System.Drawing.Point(472, 69);
            this.cboTheloai.Name = "cboTheloai";
            this.cboTheloai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTheloai.Properties.Appearance.Options.UseFont = true;
            this.cboTheloai.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboTheloai.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboTheloai.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboTheloai.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboTheloai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTheloai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTheloai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTheloai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA", 40, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "TEN")});
            this.cboTheloai.Properties.DisplayMember = "TEN";
            this.cboTheloai.Properties.DropDownItemHeight = 40;
            this.cboTheloai.Properties.NullText = "";
            this.cboTheloai.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTheloai.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboTheloai.Properties.ShowHeader = false;
            this.cboTheloai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTheloai.Properties.ValueMember = "MA";
            this.cboTheloai.Size = new System.Drawing.Size(248, 26);
            this.cboTheloai.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(346, 72);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(57, 19);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Thể loại";
            // 
            // cboNSX
            // 
            this.cboNSX.EnterMoveNextControl = true;
            this.cboNSX.Location = new System.Drawing.Point(472, 5);
            this.cboNSX.Name = "cboNSX";
            this.cboNSX.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNSX.Properties.Appearance.Options.UseFont = true;
            this.cboNSX.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboNSX.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboNSX.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboNSX.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboNSX.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNSX.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNSX.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNSX.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", 40, "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "TEN")});
            this.cboNSX.Properties.DisplayMember = "TEN";
            this.cboNSX.Properties.DropDownItemHeight = 40;
            this.cboNSX.Properties.NullText = "";
            this.cboNSX.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNSX.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboNSX.Properties.ShowHeader = false;
            this.cboNSX.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNSX.Properties.ValueMember = "MA";
            this.cboNSX.Size = new System.Drawing.Size(248, 26);
            this.cboNSX.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(346, 8);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(104, 19);
            this.labelControl7.TabIndex = 8;
            this.labelControl7.Text = "Nhà phát hành";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(91, 183);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = ((System.Drawing.Image)(resources.GetObject("btnSua.Image")));
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(267, 183);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(355, 183);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(179, 183);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(89, 158);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 18;
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(6, 136);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 16;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(91, 133);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(629, 26);
            this.txtGHICHU.TabIndex = 17;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(27, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Tên";
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(91, 5);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1032, 221);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 427);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 263);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 103);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 183);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 345);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 221);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1032, 427);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA,
            this.TENTHELOAI,
            this.TEN,
            this.GHICHU,
            this.SUDUNG,
            this.FILL,
            this.TENNSX,
            this.NAM,
            this.THOILUONG,
            this.NOIDUNG,
            this.THELOAI,
            this.NSX});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 60;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 100;
            // 
            // TENTHELOAI
            // 
            this.TENTHELOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENTHELOAI.AppearanceHeader.Options.UseFont = true;
            this.TENTHELOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TENTHELOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENTHELOAI.Caption = "Thể loại";
            this.TENTHELOAI.FieldName = "TENTHELOAI";
            this.TENTHELOAI.Name = "TENTHELOAI";
            this.TENTHELOAI.OptionsColumn.AllowEdit = false;
            this.TENTHELOAI.OptionsColumn.AllowFocus = false;
            this.TENTHELOAI.OptionsColumn.FixedWidth = true;
            this.TENTHELOAI.Visible = true;
            this.TENTHELOAI.VisibleIndex = 3;
            this.TENTHELOAI.Width = 120;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 250;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 8;
            this.GHICHU.Width = 250;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 9;
            this.SUDUNG.Width = 100;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 10;
            this.FILL.Width = 20;
            // 
            // TENNSX
            // 
            this.TENNSX.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNSX.AppearanceHeader.Options.UseFont = true;
            this.TENNSX.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNSX.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNSX.Caption = "Nhà phát hành";
            this.TENNSX.FieldName = "TENNSX";
            this.TENNSX.Name = "TENNSX";
            this.TENNSX.OptionsColumn.AllowEdit = false;
            this.TENNSX.OptionsColumn.AllowFocus = false;
            this.TENNSX.OptionsColumn.FixedWidth = true;
            this.TENNSX.Visible = true;
            this.TENNSX.VisibleIndex = 4;
            this.TENNSX.Width = 150;
            // 
            // NAM
            // 
            this.NAM.AppearanceCell.Options.UseTextOptions = true;
            this.NAM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAM.AppearanceHeader.Options.UseFont = true;
            this.NAM.AppearanceHeader.Options.UseTextOptions = true;
            this.NAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAM.Caption = "Năm phát hành";
            this.NAM.FieldName = "NAM";
            this.NAM.Name = "NAM";
            this.NAM.OptionsColumn.AllowEdit = false;
            this.NAM.OptionsColumn.AllowFocus = false;
            this.NAM.OptionsColumn.FixedWidth = true;
            this.NAM.Visible = true;
            this.NAM.VisibleIndex = 5;
            this.NAM.Width = 150;
            // 
            // THOILUONG
            // 
            this.THOILUONG.AppearanceCell.Options.UseTextOptions = true;
            this.THOILUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THOILUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOILUONG.AppearanceHeader.Options.UseFont = true;
            this.THOILUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.THOILUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOILUONG.Caption = "Thời lượng";
            this.THOILUONG.FieldName = "THOILUONG";
            this.THOILUONG.Name = "THOILUONG";
            this.THOILUONG.OptionsColumn.AllowEdit = false;
            this.THOILUONG.OptionsColumn.AllowFocus = false;
            this.THOILUONG.OptionsColumn.FixedWidth = true;
            this.THOILUONG.Visible = true;
            this.THOILUONG.VisibleIndex = 6;
            this.THOILUONG.Width = 100;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 7;
            this.NOIDUNG.Width = 200;
            // 
            // THELOAI
            // 
            this.THELOAI.Caption = "THELOAI";
            this.THELOAI.FieldName = "THELOAI";
            this.THELOAI.Name = "THELOAI";
            // 
            // NSX
            // 
            this.NSX.Caption = "NSX";
            this.NSX.FieldName = "NSX";
            this.NSX.Name = "NSX";
            // 
            // Frm_Phim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 648);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Phim";
            this.Text = "Danh mục phim";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Phim_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFast.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLOAITROCHOI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOIDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNAM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHOILUONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENVIEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDAODIEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTheloai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNSX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtNOIDUNG;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtNAM;
        private DevExpress.XtraEditors.LabelControl lblTHOILUONG;
        private DevExpress.XtraEditors.TextEdit txtTHOILUONG;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtDIENVIEN;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDAODIEN;
        private DevExpress.XtraEditors.LookUpEdit cboTheloai;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit cboNSX;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraGrid.Columns.GridColumn TENTHELOAI;
        private DevExpress.XtraGrid.Columns.GridColumn TENNSX;
        private DevExpress.XtraGrid.Columns.GridColumn NAM;
        private DevExpress.XtraGrid.Columns.GridColumn THOILUONG;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn THELOAI;
        private DevExpress.XtraGrid.Columns.GridColumn NSX;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox picLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnThongtinkhac;
        private DevExpress.XtraEditors.SimpleButton btnKhac;
        private DevExpress.XtraEditors.CheckEdit chkFast;




    }
}