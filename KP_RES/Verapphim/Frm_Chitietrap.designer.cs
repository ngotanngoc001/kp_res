﻿namespace KP_RES 
{
    partial class Frm_Chitietrap 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Chitietrap));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.txtLocation = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtImage = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTelephone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtGoogleAddress = new DevExpress.XtraEditors.TextEdit();
            this.lblTHOILUONG = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtENName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtVNName = new DevExpress.XtraEditors.TextEdit();
            this.txtDescriptionFor2D = new DevExpress.XtraEditors.MemoEdit();
            this.txtDescriptionFor3D = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnKhuVuc = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtENName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVNName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescriptionFor2D.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescriptionFor3D.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnKhuVuc);
            this.panelControl1.Controls.Add(this.txtStatus);
            this.panelControl1.Controls.Add(this.txtLocation);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtImage);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtTelephone);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtGoogleAddress);
            this.panelControl1.Controls.Add(this.lblTHOILUONG);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtAddress);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtENName);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.lblGHICHU);
            this.panelControl1.Controls.Add(this.txtFax);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtVNName);
            this.panelControl1.Controls.Add(this.txtDescriptionFor2D);
            this.panelControl1.Controls.Add(this.txtDescriptionFor3D);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1164, 648);
            this.panelControl1.TabIndex = 0;
            // 
            // txtStatus
            // 
            this.txtStatus.EnterMoveNextControl = true;
            this.txtStatus.Location = new System.Drawing.Point(135, 571);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.Properties.Appearance.Options.UseFont = true;
            this.txtStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtStatus.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.txtStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtStatus.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.txtStatus.Properties.DisplayMember = "TEN";
            this.txtStatus.Properties.DropDownItemHeight = 40;
            this.txtStatus.Properties.NullText = "";
            this.txtStatus.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.txtStatus.Properties.ShowHeader = false;
            this.txtStatus.Properties.ValueMember = "MA";
            this.txtStatus.Size = new System.Drawing.Size(857, 26);
            this.txtStatus.TabIndex = 24;
            // 
            // txtLocation
            // 
            this.txtLocation.EnterMoveNextControl = true;
            this.txtLocation.Location = new System.Drawing.Point(135, 165);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Properties.Appearance.Options.UseFont = true;
            this.txtLocation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtLocation.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.txtLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtLocation.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.txtLocation.Properties.DisplayMember = "TEN";
            this.txtLocation.Properties.DropDownItemHeight = 40;
            this.txtLocation.Properties.NullText = "";
            this.txtLocation.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.txtLocation.Properties.ShowHeader = false;
            this.txtLocation.Properties.ValueMember = "MA";
            this.txtLocation.Size = new System.Drawing.Size(857, 26);
            this.txtLocation.TabIndex = 23;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 574);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 19);
            this.labelControl4.TabIndex = 18;
            this.labelControl4.Text = "Trạng thái";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(6, 606);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(64, 19);
            this.labelControl8.TabIndex = 20;
            this.labelControl8.Text = "Hình ảnh";
            // 
            // txtImage
            // 
            this.txtImage.EnterMoveNextControl = true;
            this.txtImage.Location = new System.Drawing.Point(135, 603);
            this.txtImage.Name = "txtImage";
            this.txtImage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImage.Properties.Appearance.Options.UseFont = true;
            this.txtImage.Size = new System.Drawing.Size(857, 26);
            this.txtImage.TabIndex = 21;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 104);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(72, 19);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Điện thoại";
            // 
            // txtTelephone
            // 
            this.txtTelephone.EnterMoveNextControl = true;
            this.txtTelephone.Location = new System.Drawing.Point(135, 101);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelephone.Properties.Appearance.Options.UseFont = true;
            this.txtTelephone.Size = new System.Drawing.Size(857, 26);
            this.txtTelephone.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(6, 200);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 19);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Bản đồ";
            // 
            // txtGoogleAddress
            // 
            this.txtGoogleAddress.EnterMoveNextControl = true;
            this.txtGoogleAddress.Location = new System.Drawing.Point(135, 197);
            this.txtGoogleAddress.Name = "txtGoogleAddress";
            this.txtGoogleAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoogleAddress.Properties.Appearance.Options.UseFont = true;
            this.txtGoogleAddress.Size = new System.Drawing.Size(857, 26);
            this.txtGoogleAddress.TabIndex = 13;
            // 
            // lblTHOILUONG
            // 
            this.lblTHOILUONG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTHOILUONG.Location = new System.Drawing.Point(6, 402);
            this.lblTHOILUONG.Margin = new System.Windows.Forms.Padding(4);
            this.lblTHOILUONG.Name = "lblTHOILUONG";
            this.lblTHOILUONG.Size = new System.Drawing.Size(90, 19);
            this.lblTHOILUONG.TabIndex = 16;
            this.lblTHOILUONG.Text = "Nội dung 3D";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(6, 72);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Địa chỉ";
            // 
            // txtAddress
            // 
            this.txtAddress.EnterMoveNextControl = true;
            this.txtAddress.Location = new System.Drawing.Point(135, 69);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(857, 26);
            this.txtAddress.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 40);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(59, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Tên(EN)";
            // 
            // txtENName
            // 
            this.txtENName.EnterMoveNextControl = true;
            this.txtENName.Location = new System.Drawing.Point(135, 37);
            this.txtENName.Name = "txtENName";
            this.txtENName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtENName.Properties.Appearance.Options.UseFont = true;
            this.txtENName.Size = new System.Drawing.Size(857, 26);
            this.txtENName.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(6, 232);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 19);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Nội dung 2D";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(6, 168);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 19);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Địa điểm";
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(6, 136);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(24, 19);
            this.lblGHICHU.TabIndex = 8;
            this.lblGHICHU.Text = "Fax";
            // 
            // txtFax
            // 
            this.txtFax.EnterMoveNextControl = true;
            this.txtFax.Location = new System.Drawing.Point(135, 133);
            this.txtFax.Name = "txtFax";
            this.txtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFax.Properties.Appearance.Options.UseFont = true;
            this.txtFax.Size = new System.Drawing.Size(857, 26);
            this.txtFax.TabIndex = 9;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(60, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Tên(VN)";
            // 
            // txtVNName
            // 
            this.txtVNName.EnterMoveNextControl = true;
            this.txtVNName.Location = new System.Drawing.Point(135, 5);
            this.txtVNName.Name = "txtVNName";
            this.txtVNName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNName.Properties.Appearance.Options.UseFont = true;
            this.txtVNName.Size = new System.Drawing.Size(857, 26);
            this.txtVNName.TabIndex = 1;
            // 
            // txtDescriptionFor2D
            // 
            this.txtDescriptionFor2D.Location = new System.Drawing.Point(135, 229);
            this.txtDescriptionFor2D.Name = "txtDescriptionFor2D";
            this.txtDescriptionFor2D.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescriptionFor2D.Properties.Appearance.Options.UseFont = true;
            this.txtDescriptionFor2D.Size = new System.Drawing.Size(857, 164);
            this.txtDescriptionFor2D.TabIndex = 15;
            // 
            // txtDescriptionFor3D
            // 
            this.txtDescriptionFor3D.Location = new System.Drawing.Point(135, 399);
            this.txtDescriptionFor3D.Name = "txtDescriptionFor3D";
            this.txtDescriptionFor3D.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescriptionFor3D.Properties.Appearance.Options.UseFont = true;
            this.txtDescriptionFor3D.Size = new System.Drawing.Size(857, 164);
            this.txtDescriptionFor3D.TabIndex = 17;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1164, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 648);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 484);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 164);
            this.panelControl4.TabIndex = 15;
            // 
            // btnKhuVuc
            // 
            this.btnKhuVuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhuVuc.Appearance.Options.UseFont = true;
            this.btnKhuVuc.Image = global::KP_RES.Properties.Resources.geo_fence_26;
            this.btnKhuVuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnKhuVuc.Location = new System.Drawing.Point(999, 551);
            this.btnKhuVuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnKhuVuc.Name = "btnKhuVuc";
            this.btnKhuVuc.Size = new System.Drawing.Size(150, 35);
            this.btnKhuVuc.TabIndex = 25;
            this.btnKhuVuc.Text = "&Thêm địa điểm";
            this.btnKhuVuc.Click += new System.EventHandler(this.btnKhuVuc_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(999, 594);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(150, 35);
            this.btnLuu.TabIndex = 22;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 324);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 404);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 566);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_Chitietrap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 648);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "Frm_Chitietrap";
            this.Text = "Thông tin rạp (Đưa thông tin lên website)";
            this.Load += new System.EventHandler(this.Frm_Chitietrap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtENName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVNName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescriptionFor2D.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescriptionFor3D.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtVNName;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTelephone;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtGoogleAddress;
        private DevExpress.XtraEditors.LabelControl lblTHOILUONG;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtENName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtImage;
        private DevExpress.XtraEditors.MemoEdit txtDescriptionFor3D;
        private DevExpress.XtraEditors.LookUpEdit txtLocation;
        private DevExpress.XtraEditors.LookUpEdit txtStatus;
        private DevExpress.XtraEditors.MemoEdit txtDescriptionFor2D;
        private DevExpress.XtraEditors.SimpleButton btnKhuVuc;




    }
}