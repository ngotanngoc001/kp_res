﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_ThanhToanDatVe : DevExpress.XtraEditors.XtraForm
    {
        public String sMa = "";

        public Frm_ThanhToanDatVe()
        {
            InitializeComponent();
        }

        private void Frm_ThanhToanDatVe_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectDialogDanhSachDatVe " + clsMain.SQLString(sMa));
            if (myDT.Rows.Count > 0)
            {
                txtBarcode.Text = myDT.Rows[0]["MABARCODE"].ToString();
                txtTenPhim.Text = myDT.Rows[0]["TENPHIM"].ToString();
                txtNgayChieu.Text = String.Format("{0:dd/MM/yyyy}",myDT.Rows[0]["NGAYCHIEU"]);
                txtGioChieu.Text = myDT.Rows[0]["BATDAU"].ToString();
                txtMaDaiLy.Text = myDT.Rows[0]["MADAILY"].ToString();
                txtTenDaiLy.Text = myDT.Rows[0]["TEN"].ToString();
                txtChietKhau.Text = myDT.Rows[0]["CHIETKHAU"].ToString();
                txtTongTien.Text = String.Format("{0:###,##0}",myDT.Rows[0]["THANHTIEN"]);
                txtThanhToan.Text = String.Format("{0:###,##0}", myDT.Rows[0]["THANHTOAN"]);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            String sSPs = "SP_UpdateDanhSachDatVe";
            
            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@BARCODE", (Object)sMa);
            sParameter[1] = new SqlParameter("@THANHTOAN", (Object)txtThanhToan.Text);

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;

            if (sBoolean)
            {
                XtraMessageBox.Show("Đã lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                MessageError("Lưu không thành công", "Thông báo");
        }

        private void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtThanhToan_EditValueChanged(object sender, EventArgs e)
        {
            int iconlai = int.Parse(txtTongTien.Text.Replace(",", "")) - int.Parse(txtThanhToan.Text.Replace(",", ""));
            txtConlai.Text = String.Format("{0:###,##0}", iconlai);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        


    }
}