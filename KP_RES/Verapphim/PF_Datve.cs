﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;

namespace KP_RES
{

    public partial class PF_Datve : DevExpress.XtraEditors.XtraForm
    {
        public DataTable dt;
        public bool bInve = false;
        string ControlName = "";

        public PF_Datve()
        {
            InitializeComponent();
            txtTenkhach.GotFocus += new EventHandler(txtTenkhach_GotFocus);
            txtSodienthoai.GotFocus += new EventHandler(txtTenkhach_GotFocus);
            txtEmail.GotFocus += new EventHandler(txtTenkhach_GotFocus);
            txtChietkhau.GotFocus += new EventHandler(txtTenkhach_GotFocus);
        }

        private void PF_Datve_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void PF_Datve_Load(object sender, EventArgs e)
        {
            gridControl.DataSource = dt;
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
            txtChietkhau.Text = "0";
            LoadCombo();
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA,ID,TEN,LOAI,(Case When LOAI=1 Then N'Nhà cung cấp' Else N'Khách hàng' End) As TENLOAI,";
            sSQL += "ISNULL(HANMUC_CONGNO,0) AS HANMUC_CONGNO,ISNULL(HANTHANHTOAN,0) AS HANTHANHTOAN,";
            sSQL += "ISNULL(TIENDATCOC,0) AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC,";
            sSQL += "DIACHI,DIENTHOAI,FAX,EMAIL,WEBSITE,NGUOILIENHE,GHICHU,SUDUNG,CMND,NGAYSINH,GIOITINH,LOAIDAILY,CAPDO,(SELECT A.TEN FROM LOAIDAILY A WHERE A.MA=LOAIDAILY)AS LOAIDAILY1,(SELECT A.TEN FROM CAPDODAILY A WHERE A.MA=CAPDO)AS CAPDAILY,(SELECT A.CHIETKHAU FROM CAPDODAILY A WHERE A.MA=CAPDO)AS CHIETKHAU" + "\n";
            sSQL += "From NHACUNGCAP" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            cboKhachhang.Properties.DataSource = dt;
            cboKhachhang.EditValue = 1;
        }

        private void txtTenkhach_GotFocus(object sender, System.EventArgs e)
        {
            TextEdit txt = (TextEdit)sender;
            ControlName = txt.Name;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ControlName == txtTenkhach.Name)
            {
                txtTenkhach.Text = "";
            }
            else if (ControlName == txtSodienthoai.Name)
            {
                txtSodienthoai.Text = "";
            }
            else if (ControlName == txtChietkhau.Name)
            {
                txtChietkhau.Text = "";
            }
            else
            {
                txtEmail.Text = "";
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (ControlName == txtTenkhach.Name)
            {
                txtTenkhach.Text = txtTenkhach.Text + btn.Text;
            }
            else if (ControlName == txtSodienthoai.Name)
            {
                txtSodienthoai.Text = txtSodienthoai.Text + btn.Text;
            }
            else if (ControlName == txtChietkhau.Name)
            {
                txtChietkhau.Text = txtChietkhau.Text + btn.Text;
            }
            else
            {
                txtEmail.Text = txtEmail.Text + btn.Text;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTenkhach.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTenkhach.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenkhach.Focus();
                return;
            }
            if (txtSodienthoai.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSodienthoai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSodienthoai.Focus();
                return;
            }

            string MaBarCode = cls_KP_RES.CreateBarcode("BIENNHAN");
            string sSQL = "";
            sSQL += "Insert into BIENNHAN (MABARCODE,TENHANHKHACH,DIACHI,DIENTHOAI,EMAIL,TONGGIAVE,DATHANHTOAN,MADAILY,CHIETKHAU,THANHTIEN,NGUOITAO,TONGSOGHE)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString(MaBarCode) + ",";
            sSQL += clsMain.SQLStringUnicode(txtTenkhach.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("") + ",";
            sSQL += clsMain.SQLString(txtSodienthoai.Text) + ",";
            sSQL += clsMain.SQLString("") + ",";
            sSQL += clsMain.SQLString(txtTongtien.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString("0") + ",";
            sSQL += clsMain.SQLString(cboKhachhang .EditValue .ToString ()) + ",";
            sSQL += clsMain.SQLString(txtChietkhau .Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(txtThanhtien.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(clsUserManagement .ReturnUserLogin ()) + ",";
            sSQL += clsMain.SQLString(dt.Rows.Count.ToString()) + ")" + "\n";
            foreach (DataRow dr in dt.Rows)
            {
                sSQL += "Update VE Set " + "\n";
                sSQL += "TENHANHKHACH=" + clsMain.SQLStringUnicode(txtTenkhach.Text) + "," + "\n";
                sSQL += "DIENTHOAI=" + clsMain.SQLString(txtSodienthoai.Text) + "," + "\n";
                sSQL += "BIENNHAN=" + clsMain.SQLString(MaBarCode) + "," + "\n";
                sSQL += "MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + "," + "\n";
                sSQL += "DONGIA=" + clsMain.SQLString(dr["PHUTHU"].ToString()) + "," + "\n";
                sSQL += "PHUTHU=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                bInve = true;
                btnLuu.Enabled = false;
                sSQL = "";
                sSQL += "Select A.MABARCODE As MAGHE,A.SOGHE,A.DONGIA,A.TENHANHKHACH,A.CMND,A.DIACHI,A.DIENTHOAI,A.EMAIL,A.NGAYTAO,C.MABARCODE As MAPHIEU,C.TONGGIAVE,C.DATHANHTOAN,C.TONGSOGHE,D.NGAYCHIEU,D.BATDAU,E.TENPHONG,F.TENPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
                sSQL += "From VE A , DM_GHE B , BIENNHAN C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN H,SYS_USER I" + "\n";
                sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                sSQL += "And A.BIENNHAN = C.MABARCODE" + "\n";
                sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                sSQL += "And A.BIENNHAN =" + clsMain.SQLString(MaBarCode) + "\n";
                sSQL += "Order by A.SOGHE" + "\n";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);

                Frm_Report1 frm1 = new Frm_Report1();
                frm1.ReportName = "rptBiennhan1";
                frm1.DataSource = dt1;
                frm1.IsPrint = false;
                frm1.PrinterName = clsKP_Terminal.Mayinbill;
                frm1.ExportName = "Bien nhan dat ve";
                frm1.WindowState = FormWindowState.Maximized;
                frm1.Text = "Biên nhận đặt vé";
                frm1.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm1.TopLevel = false;
                frm1.Parent = pnlReport;
                frm1.Show();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtChietkhau_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                int gv = int.Parse(txtTongtien.Text.ToString().Replace(",", ""));
                int ck = int.Parse(txtChietkhau.Text.ToString().Replace(",", ""));
                int tt = 0;
                if (ck <= 100)
                {
                    tt = gv - gv * ck / 100;
                }
                else
                {
                    tt = gv - ck;
                }
                txtTonggiave.Text = gv.ToString("N0");
                txtThanhtien.Text = tt.ToString("N0");
            }
            catch
            {
            }
        }

        private void cboKhachhang_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboKhachhang.EditValue.ToString() == "1")
                {
                    txtTenkhach.Text = "";
                    txtSodienthoai.Text = "";
                    txtEmail.Text = "";
                    txtChietkhau.EditValue = 0;

                    txtTenkhach.Properties.ReadOnly = false;
                    txtSodienthoai.Properties.ReadOnly = false;
                    txtEmail.Properties.ReadOnly = false;
                }
                else
                {
                    DataRowView rowView = (DataRowView)cboKhachhang.GetSelectedDataRow();
                    DataRow row = rowView.Row;
                    txtTenkhach.Text = row["TEN"].ToString();
                    txtSodienthoai.Text = row["DIENTHOAI"].ToString();
                    txtEmail.Text = row["EMAIL"].ToString();
                    txtChietkhau.EditValue = int.Parse(row["CHIETKHAU"].ToString().Replace(",", ""));

                    txtTenkhach.Properties.ReadOnly = true ;
                    txtSodienthoai.Properties.ReadOnly = true;
                    txtEmail.Properties.ReadOnly = true;
                }
            }
            catch
            {
            }
        }
    }
}