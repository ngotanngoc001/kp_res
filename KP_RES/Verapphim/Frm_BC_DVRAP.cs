﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_DVRAP : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_DVRAP()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_DVRAP_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        DataTable dtCombo = new DataTable();
        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            //cboDATA.Properties.Columns.Clear();
            //cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Rạp
            {
                dtCombo = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1 Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Rạp";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 1)//Phim
            {
                dtCombo = clsMain.ReturnDataTable("select MAPHIM as MA,TENPHIM as TEN from DM_PHIM where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Phim";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 2)//Nhân viên
            {
                dtCombo = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dtCombo;
                //dtCombo.Rows.Add("0", "Tất cả");
                //taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhân viên";
                LoadChiTietCombo(dtCombo);
            }
         
            //cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void LoadChiTietCombo(DataTable dt)
        {
            cboDATA.Properties.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                cboDATA.Properties.Items.Add(dr["TEN"].ToString());
            }
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            //if (Caption.Length > 1 & dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < Caption.Length; i++)
            //    {
            //        LookUpColumnInfo info = new LookUpColumnInfo();
            //        info.FieldName = FieldName[i].TrimEnd();
            //        info.Caption = Caption[i].TrimEnd();
            //        info.Visible = visible[i];
            //        info.Alignment = DevExpress.Utils.HorzAlignment.Default;
            //        cboDATA.Properties.Columns.Add(info);
            //    }
            //    cboDATA.Properties.BestFitMode = BestFitMode.None;
            //    cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
            //    cboDATA.Properties.DisplayMember = FieldName[1];
            //    cboDATA.Properties.ValueMember = FieldName[0];
            //    if (visible[0] == true)
            //    {
            //        cboDATA.Properties.Columns[0].Width = 100;
            //        cboDATA.Properties.Columns[1].Width = 500;
            //    }
            //    else
            //    {
            //        cboDATA.Properties.Columns[1].Width = 100;
            //        cboDATA.Properties.Columns[2].Width = 500;
            //    }
            //    if (dt.Rows.Count > 0)
            //        cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            //}
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value || cboDATA.EditValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn đối tượng cần xem", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                splashScreenManager1.ShowWaitForm();
                sql += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE, A.TRANGTHAI,A.DONGIA,A.PHUTHU,A.DATHANHTOAN,A.CONLAI,A.TENHANHKHACH,A.CMND,A.DIACHI,A.DIENTHOAI,A.EMAIL,A.NGUOITAO,A.NGAYTAO,B.TENGHE ,D.NGAYCHIEU ,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.KETTHUC,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM,A.BIENNHAN" + "\n";
                sql += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J" + "\n";
                sql += "Where A.MAGHE = B.MAGHE" + "\n";
                sql += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sql += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sql += "And D.MAPHONG = E.MAPHONG" + "\n";
                sql += "And D.MAPHIM = F.MAPHIM" + "\n";
                sql += "And A.NGUOITAO = H.UserID" + "\n";
                sql += "And H.MaNV = G.MANHANVIEN" + "\n";
                sql += "And A.MALOAIVE = I.MALOAIVE" + "\n";
                sql += "And E.MARAP = J.MADIADIEM" + "\n";
                sql += "And A.BIENNHAN IN(SELECT MABARCODE FROM BIENNHAN WHERE LEN(DIACHI)<1)" + "\n";
                sql += "And A.TRANGTHAI =" + clsMain.SQLString("1") + "\n";
                sql += "And A.CONLAI =" + clsMain.SQLString("1") + "\n";

                sql += "And A.NGAYTAO BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";
                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or J.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 1)//Phim
                {
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " D.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " D.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 2)//Nhân viên
                {
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.NGUOITAO IN (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                                else
                                {
                                    sqlTemp += " Or A.NGUOITAO IN (SELECT UserID FROM SYS_USER WHERE MaNV= " + clsMain.SQLString(dr[0]["MA"].ToString()) + ")" + "\n";
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }

                //Sắp xếp theo đúng thời gian bán
                sql += "Order By A.NGAYTAO" + "\n";

                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                splashScreenManager1.CloseWaitForm();
            }
            catch (Exception ex)
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    DataColumn colTYPE = new DataColumn();
                    colTYPE.ColumnName = "TYPE";
                    colTYPE.DataType = System.Type.GetType("System.String");
                    if (optGroup.SelectedIndex == 0)//Rạp
                    {
                        colTYPE.DefaultValue = "Rạp";

                    }
                    else if (optGroup.SelectedIndex == 1)//Phim
                    {
                        colTYPE.DefaultValue = "Phim";
                    }
                    else if (optGroup.SelectedIndex == 2)//Nhân viên
                    {
                        colTYPE.DefaultValue = "Nhân viên";
                    }

                    DataColumn colNAMETYPE = new DataColumn();
                    colNAMETYPE.ColumnName = "NAMETYPE";
                    colNAMETYPE.DataType = System.Type.GetType("System.String");
                    //colNAMETYPE.DefaultValue = cboDATA.Text;
                    bool bAll = true;
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Unchecked)
                        {
                            bAll = false;
                            break;
                        }
                    }
                    if (bAll)
                    {
                        colNAMETYPE.DefaultValue = "Tất cả";
                    }
                    else
                    {
                        colNAMETYPE.DefaultValue = cboDATA.Text;
                    }

                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TYPE", dt) == false)
                        dt.Columns.Add(colTYPE);
                    if (ContainColumn("NAMETYPE", dt) == false)
                        dt.Columns.Add(colNAMETYPE);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 36;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa chọn phiếu cần in");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}