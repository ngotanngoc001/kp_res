﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CopyLich : DevExpress.XtraEditors.XtraForm
    {
        public string   sMarap="";
        public bool bDongy = false ;
        public Frm_CopyLich()
        {
            InitializeComponent();
        }

        private void Frm_CopyLich_Load(object sender, EventArgs e)
        {
            dtpTuNgay.EditValue = clsGlobal.gdServerDate;
            dtpDenNgay.EditValue = clsGlobal.gdServerDate;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if ((DateTime)dtpTuNgay.EditValue == (DateTime)dtpDenNgay.EditValue)
            {
                XtraMessageBox.Show("Ngày không đúng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

           splashScreenManager1.ShowWaitForm();
           string sSQL = "";
           Boolean bRunSQL = false;
           Boolean bIsRun = false;

           sSQL += "select A.MALICHCHIEU,A.NGAYCHIEU,A.BATDAU,A.KETTHUC,E.MARAP,A.MAPHONG,A.MAPHIM,A.MACHATLUONGPHIM,A.MABANGGIA,A.GHICHU,A.SUDUNG" + "\n";
           sSQL += "from LICHCHIEU A, DM_PHONG E" + "\n";
           sSQL += "where A .MAPHONG = E .MAPHONG " + "\n";
           sSQL += "and E.MARAP =" + clsMain.SQLString(sMarap ) + "\n";
           sSQL += "and A.NGAYCHIEU =" + clsMain.SQLString(dtpTuNgay .Text ) + "\n";
           sSQL += "and A.SUDUNG =" + clsMain.SQLString("1") + "\n";
           sSQL += "ORDER BY A.BATDAU";

           DataTable dt = clsMain.ReturnDataTable(sSQL);
           foreach (DataRow dr in dt.Rows)
           {
               if (CheckLichChieu(dr["MAPHONG"].ToString(), dtpDenNgay.Text, dr["BATDAU"].ToString(),dr["KETTHUC"].ToString()))
               {
                   sSQL = "";
                   sSQL += "Insert into LICHCHIEU (MAPHONG,MAPHIM,MACHATLUONGPHIM,MABANGGIA,NGAYCHIEU,BATDAU,KETTHUC,GHICHU,SUDUNG)" + "\n";
                   sSQL += "Values ( ";
                   sSQL += clsMain.SQLString(dr["MAPHONG"].ToString ()) + ",";
                   sSQL += clsMain.SQLString(dr["MAPHIM"].ToString()) + ",";
                   sSQL += clsMain.SQLString(dr["MACHATLUONGPHIM"].ToString()) + ",";
                   sSQL += clsMain.SQLString(dr["MABANGGIA"].ToString()) + ",";
                   sSQL += clsMain.SQLString(dtpDenNgay.Text ) + ",";
                   sSQL += clsMain.SQLString(dr["BATDAU"].ToString()) + ",";
                   sSQL += clsMain.SQLString(dr["KETTHUC"].ToString()) + ",";
                   sSQL += clsMain.SQLStringUnicode(dr["GHICHU"].ToString()) + ",";
                   sSQL += clsMain.SQLString(dr["SUDUNG"].ToString()) + ")" + "\n";
                   sSQL += TaoVe(dr["MAPHONG"].ToString());

                   bRunSQL = clsMain.ExecuteSQL(sSQL);
                   bIsRun = true;
               }
           }
           splashScreenManager1.CloseWaitForm();

           if ((bIsRun && bRunSQL) ||(!bIsRun))
           {
               DevExpress.XtraEditors.XtraMessageBox.Show("Copy lịch chiếu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
               bDongy = true;
               this.Close();
           }
           else
           {
               DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
          
        }

        string TaoVe(string sMaphong)
        {
            string str = "";

            string sSQL = "";
            sSQL += "Select MAGHE From DM_GHE" + "\n";
            sSQL += "Where MAPHONG = " + clsMain.SQLString(sMaphong) + "\n";
            sSQL += "And SUDUNG=" + clsMain.SQLString("1") + "\n";
            DataTable dtGhe = clsMain.ReturnDataTable(sSQL);

            string sMALICHTRINH = "";
            sSQL = "";
            sSQL += "Select IDENT_CURRENT('LICHCHIEU')+1 As MALICHCHIEU " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            sMALICHTRINH = dt.Rows[0]["MALICHCHIEU"].ToString();

            foreach (DataRow dr in dtGhe.Rows)
            {
                str += "Insert into VE (MALICHCHIEU,MAGHE,SOGHE,TRANGTHAI,PHUTHU,DATHANHTOAN,DONGIA,CONLAI)" + "\n";
                str += "Values ( ";
                str += clsMain.SQLString(sMALICHTRINH) + ",";
                str += clsMain.SQLString(dr["MAGHE"].ToString()) + ",";
                str += clsMain.SQLString("") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("0") + ",";
                str += clsMain.SQLString("1") + ",";
                str += clsMain.SQLString("1") + ")" + "\n";
            }

            str += LSLichChieu(sMALICHTRINH, "4");

            return str;
        }

        private String LSLichChieu(String sMaLichChieu, String sStyle)
        {
            String sSQL = "";
            sSQL += "EXEC SP_InsertLichSuLichChieu ";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
            sSQL += clsMain.SQLString(sMaLichChieu) + ",";
            sSQL += clsMain.SQLString(sStyle) + "\n";

            return sSQL;
        }

        bool CheckLichChieu(string sMaphong, string dNgaychieu, string sBatdau, string sKetthuc)
        {
            string sSQL = "";
            sSQL += "SELECT MALICHCHIEU,MAPHONG ,NGAYCHIEU ,BATDAU,KETTHUC" + "\n";
            sSQL += "FROM LICHCHIEU " + "\n";
            sSQL += "WHERE  MAPHONG =" + clsMain.SQLString(sMaphong) + "\n";
            sSQL += "AND NGAYCHIEU =" + clsMain.SQLString(dNgaychieu) + "\n";
            sSQL += "AND (convert(datetime,BATDAU) BETWEEN  convert(datetime," + clsMain.SQLString(sBatdau) + ") AND convert(datetime," + clsMain.SQLString(sKetthuc) + ") OR convert(datetime,KETTHUC) BETWEEN  convert(datetime," + clsMain.SQLString(sBatdau) + ") AND convert(datetime," + clsMain.SQLString(sKetthuc) + ")  OR(BATDAU=" + clsMain.SQLString(sBatdau) + ") OR (KETTHUC=" + clsMain.SQLString(sKetthuc) + "))" + "\n";
            sSQL += "AND SUDUNG =" + clsMain.SQLString("1") + "\n";
            if (clsMain.ReturnDataTable(sSQL).Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
    }
}