﻿namespace KP_RES 
{
    partial class Frm_BK_ChiTiet_BanVe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BK_ChiTiet_BanVe));
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DOITUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANGDIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYDIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYMUA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HANGVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIENTCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIAMGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HINHTHUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAIDONHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.cboData = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.lb_From = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.lb_To = new DevExpress.XtraEditors.LabelControl();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.gridControl1);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(219, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1039, 672);
            this.panelControl5.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(996, 668);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.DOITUONG,
            this.LOAIKH,
            this.MA_HOADON,
            this.THANGDIEN,
            this.NGAYDIEN,
            this.TEN,
            this.NGAYMUA,
            this.SOVE,
            this.HANGVE,
            this.TONGTIENTCK,
            this.GIAMGIA,
            this.TONGTIEN,
            this.HINHTHUC,
            this.TRANGTHAI,
            this.TRANGTHAIDONHANG,
            this.TENNHANVIEN});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.NGAYMUA, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // DOITUONG
            // 
            this.DOITUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DOITUONG.AppearanceCell.Options.UseFont = true;
            this.DOITUONG.AppearanceCell.Options.UseTextOptions = true;
            this.DOITUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DOITUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DOITUONG.AppearanceHeader.Options.UseFont = true;
            this.DOITUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.DOITUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DOITUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DOITUONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.DOITUONG.Caption = "Đối tượng";
            this.DOITUONG.FieldName = "DOITUONG";
            this.DOITUONG.Name = "DOITUONG";
            this.DOITUONG.OptionsColumn.AllowEdit = false;
            this.DOITUONG.OptionsColumn.AllowFocus = false;
            this.DOITUONG.OptionsColumn.FixedWidth = true;
            this.DOITUONG.Visible = true;
            this.DOITUONG.VisibleIndex = 1;
            this.DOITUONG.Width = 100;
            // 
            // LOAIKH
            // 
            this.LOAIKH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LOAIKH.AppearanceCell.Options.UseFont = true;
            this.LOAIKH.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIKH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIKH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIKH.AppearanceHeader.Options.UseFont = true;
            this.LOAIKH.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIKH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIKH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIKH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LOAIKH.Caption = "Loại KH";
            this.LOAIKH.FieldName = "LOAIKH";
            this.LOAIKH.Name = "LOAIKH";
            this.LOAIKH.OptionsColumn.AllowEdit = false;
            this.LOAIKH.OptionsColumn.AllowFocus = false;
            this.LOAIKH.OptionsColumn.FixedWidth = true;
            this.LOAIKH.Visible = true;
            this.LOAIKH.VisibleIndex = 2;
            this.LOAIKH.Width = 150;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.MA_HOADON.Caption = "Mã đơn hàng";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 3;
            this.MA_HOADON.Width = 150;
            // 
            // THANGDIEN
            // 
            this.THANGDIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANGDIEN.AppearanceCell.Options.UseFont = true;
            this.THANGDIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANGDIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANGDIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANGDIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANGDIEN.AppearanceHeader.Options.UseFont = true;
            this.THANGDIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANGDIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANGDIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANGDIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANGDIEN.Caption = "Tháng diễn";
            this.THANGDIEN.DisplayFormat.FormatString = "00";
            this.THANGDIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANGDIEN.FieldName = "THANGDIEN";
            this.THANGDIEN.Name = "THANGDIEN";
            this.THANGDIEN.OptionsColumn.AllowEdit = false;
            this.THANGDIEN.OptionsColumn.AllowFocus = false;
            this.THANGDIEN.OptionsColumn.FixedWidth = true;
            this.THANGDIEN.Visible = true;
            this.THANGDIEN.VisibleIndex = 4;
            this.THANGDIEN.Width = 60;
            // 
            // NGAYDIEN
            // 
            this.NGAYDIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYDIEN.AppearanceCell.Options.UseFont = true;
            this.NGAYDIEN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYDIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYDIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYDIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYDIEN.AppearanceHeader.Options.UseFont = true;
            this.NGAYDIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYDIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYDIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYDIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYDIEN.Caption = "Ngày diễn";
            this.NGAYDIEN.DisplayFormat.FormatString = "00";
            this.NGAYDIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.NGAYDIEN.FieldName = "NGAYDIEN";
            this.NGAYDIEN.Name = "NGAYDIEN";
            this.NGAYDIEN.OptionsColumn.AllowEdit = false;
            this.NGAYDIEN.OptionsColumn.AllowFocus = false;
            this.NGAYDIEN.OptionsColumn.FixedWidth = true;
            this.NGAYDIEN.Visible = true;
            this.NGAYDIEN.VisibleIndex = 5;
            this.NGAYDIEN.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TEN.Caption = "Tên  khách hàng";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 6;
            this.TEN.Width = 250;
            // 
            // NGAYMUA
            // 
            this.NGAYMUA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYMUA.AppearanceCell.Options.UseFont = true;
            this.NGAYMUA.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYMUA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYMUA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYMUA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYMUA.AppearanceHeader.Options.UseFont = true;
            this.NGAYMUA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYMUA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYMUA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYMUA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYMUA.Caption = "Ngày mua vé";
            this.NGAYMUA.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYMUA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYMUA.FieldName = "NGAYMUA";
            this.NGAYMUA.Name = "NGAYMUA";
            this.NGAYMUA.OptionsColumn.AllowEdit = false;
            this.NGAYMUA.OptionsColumn.AllowFocus = false;
            this.NGAYMUA.OptionsColumn.FixedWidth = true;
            this.NGAYMUA.Visible = true;
            this.NGAYMUA.VisibleIndex = 7;
            this.NGAYMUA.Width = 120;
            // 
            // SOVE
            // 
            this.SOVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOVE.AppearanceCell.Options.UseFont = true;
            this.SOVE.AppearanceCell.Options.UseTextOptions = true;
            this.SOVE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOVE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOVE.AppearanceHeader.Options.UseFont = true;
            this.SOVE.AppearanceHeader.Options.UseTextOptions = true;
            this.SOVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.SOVE.Caption = "Số vé";
            this.SOVE.FieldName = "SOVE";
            this.SOVE.Name = "SOVE";
            this.SOVE.OptionsColumn.AllowEdit = false;
            this.SOVE.OptionsColumn.AllowFocus = false;
            this.SOVE.OptionsColumn.FixedWidth = true;
            this.SOVE.Visible = true;
            this.SOVE.VisibleIndex = 8;
            this.SOVE.Width = 60;
            // 
            // HANGVE
            // 
            this.HANGVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.HANGVE.AppearanceCell.Options.UseFont = true;
            this.HANGVE.AppearanceCell.Options.UseTextOptions = true;
            this.HANGVE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HANGVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HANGVE.AppearanceHeader.Options.UseFont = true;
            this.HANGVE.AppearanceHeader.Options.UseTextOptions = true;
            this.HANGVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HANGVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HANGVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.HANGVE.Caption = "Hạng vé";
            this.HANGVE.FieldName = "HANGVE";
            this.HANGVE.Name = "HANGVE";
            this.HANGVE.OptionsColumn.AllowEdit = false;
            this.HANGVE.OptionsColumn.AllowFocus = false;
            this.HANGVE.OptionsColumn.FixedWidth = true;
            this.HANGVE.Visible = true;
            this.HANGVE.VisibleIndex = 9;
            this.HANGVE.Width = 150;
            // 
            // TONGTIENTCK
            // 
            this.TONGTIENTCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TONGTIENTCK.AppearanceCell.Options.UseFont = true;
            this.TONGTIENTCK.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIENTCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTIENTCK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIENTCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTIENTCK.AppearanceHeader.Options.UseFont = true;
            this.TONGTIENTCK.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIENTCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIENTCK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIENTCK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TONGTIENTCK.Caption = "Đơn giá";
            this.TONGTIENTCK.DisplayFormat.FormatString = "N0";
            this.TONGTIENTCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIENTCK.FieldName = "TONGTIENTCK";
            this.TONGTIENTCK.Name = "TONGTIENTCK";
            this.TONGTIENTCK.OptionsColumn.AllowEdit = false;
            this.TONGTIENTCK.OptionsColumn.AllowFocus = false;
            this.TONGTIENTCK.OptionsColumn.FixedWidth = true;
            this.TONGTIENTCK.Visible = true;
            this.TONGTIENTCK.VisibleIndex = 10;
            this.TONGTIENTCK.Width = 150;
            // 
            // GIAMGIA
            // 
            this.GIAMGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIAMGIA.AppearanceCell.Options.UseFont = true;
            this.GIAMGIA.AppearanceCell.Options.UseTextOptions = true;
            this.GIAMGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIAMGIA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIAMGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIAMGIA.AppearanceHeader.Options.UseFont = true;
            this.GIAMGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.GIAMGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIAMGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIAMGIA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIAMGIA.Caption = "% Chiết khấu";
            this.GIAMGIA.DisplayFormat.FormatString = "N0";
            this.GIAMGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIAMGIA.FieldName = "GIAMGIA";
            this.GIAMGIA.Name = "GIAMGIA";
            this.GIAMGIA.OptionsColumn.AllowEdit = false;
            this.GIAMGIA.OptionsColumn.AllowFocus = false;
            this.GIAMGIA.OptionsColumn.FixedWidth = true;
            this.GIAMGIA.Visible = true;
            this.GIAMGIA.VisibleIndex = 11;
            this.GIAMGIA.Width = 80;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TONGTIEN.AppearanceCell.Options.UseFont = true;
            this.TONGTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TONGTIEN.Caption = "Tổng tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.OptionsColumn.FixedWidth = true;
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 12;
            this.TONGTIEN.Width = 150;
            // 
            // HINHTHUC
            // 
            this.HINHTHUC.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.HINHTHUC.AppearanceCell.Options.UseFont = true;
            this.HINHTHUC.AppearanceCell.Options.UseTextOptions = true;
            this.HINHTHUC.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HINHTHUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HINHTHUC.AppearanceHeader.Options.UseFont = true;
            this.HINHTHUC.AppearanceHeader.Options.UseTextOptions = true;
            this.HINHTHUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HINHTHUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HINHTHUC.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.HINHTHUC.Caption = "Hình thức thanh toán";
            this.HINHTHUC.FieldName = "HINHTHUC";
            this.HINHTHUC.Name = "HINHTHUC";
            this.HINHTHUC.OptionsColumn.AllowEdit = false;
            this.HINHTHUC.OptionsColumn.AllowFocus = false;
            this.HINHTHUC.OptionsColumn.FixedWidth = true;
            this.HINHTHUC.Visible = true;
            this.HINHTHUC.VisibleIndex = 13;
            this.HINHTHUC.Width = 150;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRANGTHAI.AppearanceCell.Options.UseFont = true;
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 14;
            this.TRANGTHAI.Width = 150;
            // 
            // TRANGTHAIDONHANG
            // 
            this.TRANGTHAIDONHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRANGTHAIDONHANG.AppearanceCell.Options.UseFont = true;
            this.TRANGTHAIDONHANG.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAIDONHANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAIDONHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAIDONHANG.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAIDONHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAIDONHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAIDONHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAIDONHANG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TRANGTHAIDONHANG.Caption = "Trạng thái đơn hàng";
            this.TRANGTHAIDONHANG.FieldName = "TRANGTHAIDONHANG";
            this.TRANGTHAIDONHANG.Name = "TRANGTHAIDONHANG";
            this.TRANGTHAIDONHANG.OptionsColumn.AllowEdit = false;
            this.TRANGTHAIDONHANG.OptionsColumn.AllowFocus = false;
            this.TRANGTHAIDONHANG.OptionsColumn.FixedWidth = true;
            this.TRANGTHAIDONHANG.Visible = true;
            this.TRANGTHAIDONHANG.VisibleIndex = 15;
            this.TRANGTHAIDONHANG.Width = 150;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.TENNHANVIEN.Caption = "Nhân viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 16;
            this.TENNHANVIEN.Width = 250;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(998, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 668);
            this.panelControl2.TabIndex = 7;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 424);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 104);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 264);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 344);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 586);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 15;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.panelControl8);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(219, 672);
            this.panelControl1.TabIndex = 6;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl10);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 175);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(215, 460);
            this.panelControl6.TabIndex = 23;
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.btnTimkiem);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(2, 80);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(211, 47);
            this.panelControl10.TabIndex = 23;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.cboData);
            this.pnChoose.Controls.Add(this.lblTitle);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(211, 78);
            this.pnChoose.TabIndex = 22;
            this.pnChoose.Visible = false;
            // 
            // cboData
            // 
            this.cboData.Location = new System.Drawing.Point(6, 33);
            this.cboData.Name = "cboData";
            this.cboData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.Appearance.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboData.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cboData.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.cboData.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboData.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboData.Properties.DisplayMember = "TEN";
            this.cboData.Properties.DropDownItemHeight = 40;
            this.cboData.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.cboData.Properties.NullText = "";
            this.cboData.Properties.ShowHeader = false;
            this.cboData.Properties.ValueMember = "MA";
            this.cboData.Size = new System.Drawing.Size(205, 26);
            this.cboData.TabIndex = 17;
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTitle.Location = new System.Drawing.Point(4, 8);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(82, 19);
            this.lblTitle.TabIndex = 16;
            this.lblTitle.Text = "Khách hàng";
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 129);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(215, 46);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = true;
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Tất cả")});
            this.optGroup.Size = new System.Drawing.Size(211, 44);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.dtpTungay);
            this.panelControl8.Controls.Add(this.lb_From);
            this.panelControl8.Controls.Add(this.dtpDenngay);
            this.panelControl8.Controls.Add(this.lb_To);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 127);
            this.panelControl8.TabIndex = 0;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(6, 32);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(205, 26);
            this.dtpTungay.TabIndex = 1;
            // 
            // lb_From
            // 
            this.lb_From.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_From.Location = new System.Drawing.Point(6, 6);
            this.lb_From.Margin = new System.Windows.Forms.Padding(4);
            this.lb_From.Name = "lb_From";
            this.lb_From.Size = new System.Drawing.Size(59, 19);
            this.lb_From.TabIndex = 0;
            this.lb_From.Text = "Từ ngày";
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(6, 91);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(205, 26);
            this.dtpDenngay.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_To.Location = new System.Drawing.Point(6, 65);
            this.lb_To.Margin = new System.Windows.Forms.Padding(4);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(67, 19);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "Đến ngày";
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 635);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(215, 35);
            this.btnThunho.TabIndex = 5;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // Frm_BK_ChiTiet_BanVe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 672);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_BK_ChiTiet_BanVe";
            this.Text = "Bảng kê chi tiết bán vé";
            this.Load += new System.EventHandler(this.Frm_BK_ChiTiet_BanVe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn THANGDIEN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYDIEN;
        private DevExpress.XtraGrid.Columns.GridColumn DOITUONG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.LabelControl lb_From;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraEditors.LabelControl lb_To;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIKH;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.LookUpEdit cboData;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYMUA;
        private DevExpress.XtraGrid.Columns.GridColumn SOVE;
        private DevExpress.XtraGrid.Columns.GridColumn HANGVE;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIENTCK;
        private DevExpress.XtraGrid.Columns.GridColumn GIAMGIA;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn HINHTHUC;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAIDONHANG;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;




    }
}