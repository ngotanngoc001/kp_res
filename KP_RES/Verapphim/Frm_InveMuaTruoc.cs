﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;
using System.Collections;
using System.Collections.Generic;

namespace KP_RES
{
    public partial class Frm_InveMuaTruoc : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtBN = new DataTable();
        string ControlName = "";
        public ArrayList danhsachvedat = new ArrayList();
        public string sMalichchieu = "";
        public Frm_InveMuaTruoc()
        {
            InitializeComponent();
            txtMave .GotFocus += new EventHandler(txtMave_GotFocus);
            txtSodienthoai.GotFocus += new EventHandler(txtMave_GotFocus);
        }

        private void Frm_InveMuaTruoc_Load(object sender, EventArgs e)
        {
            //palBill.Width = palShow.Width / 2;
        }

        private void Frm_InveMuaTruoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtMave_GotFocus(object sender, System.EventArgs e)
        {
            TextEdit txt = (TextEdit)sender;
            ControlName = txt.Name;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = txtMave.Text + btn.Text;
            }
            else if (ControlName == txtSodienthoai.Name)
            {
                txtSodienthoai.Text = txtSodienthoai.Text + btn.Text;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = "";
            }
            else
            {
                txtSodienthoai.Text = "";
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "" && txtSodienthoai.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            pnlReport.Controls.Clear();
            string sSQL = "";
            sSQL = "";
            sSQL += "Select A.MAVE,A.BIENNHAN,A.MABARCODE,A.SOGHE,A.MALOAIVE,A.DONGIA,A.TENHANHKHACH,A.DIENTHOAI,A.EMAIL,A.NGAYTAO,A.NGUOITAO,C.MABARCODE As MAPHIEU,C.TONGGIAVE,C.DATHANHTOAN,C.TONGSOGHE,D.MALICHCHIEU,D.NGAYCHIEU,D.BATDAU,E.TENPHONG,F.TENPHIM,H.TENNHANVIEN AS NGUOITAO,C.TAIKHOAN,(SELECT TOP 1 MA FROM KHTT WHERE TAIKHOAN=C.TAIKHOAN) AS ID_KHTT" + "\n";
            sSQL += "From VE A , DM_GHE B , BIENNHAN C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN H,SYS_USER I" + "\n";
            sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
            sSQL += "And A.BIENNHAN = C.MABARCODE" + "\n";
            sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
            sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And A.NGUOITAO = I.UserID" + "\n";
            sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
            sSQL += "And D.NGAYCHIEU >= " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
            sSQL += "And C.TRANGTHAI =" + clsMain.SQLString("0") + "\n";
            sSQL += "And C.DATHANHTOAN =" + clsMain.SQLString("1") + "\n";
            sSQL += "And LEN(C.CMND)>0" + "\n";
            if (txtMave.Text != "")
            {
                sSQL += "And A.BIENNHAN =" + clsMain.SQLString(txtMave.Text) + "\n";
            }
            else
            {
                sSQL += "And A.DIENTHOAI =" + clsMain.SQLString(txtSodienthoai.Text) + "\n";
            }
            sSQL += "Order by A.SOGHE" + "\n";
            dtBN = clsMain.ReturnDataTable(sSQL);

            if (dtBN.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng . Hoặc đã hủy . Hoặc đã lấy vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
            }
            else
            {
                //XemBillHoadon();
                txtMave.Text = dtBN.Rows[0]["BIENNHAN"].ToString ();
                Frm_Report1 frm = new Frm_Report1();
                frm.ReportName = "rptBiennhan1";
                frm.DataSource = dtBN;
                frm.IsPrint = false;
                frm.PrinterName = clsKP_Terminal.Mayinbill;
                frm.ExportName = "In ve";
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.TopLevel = false;
                frm.Parent = pnlReport;
                frm.Show();
                btnInve.Focus();
            }
        }

        private void btnInve_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (dtBN.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
                      
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận in vé cho biên nhận này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                string sSQL = "";
                //03-04/2017 Thêm cộng điểm KHTT khi mua online
                if (dtBN.Rows[0]["ID_KHTT"].ToString() != "")
                {
                    sSQL += "Select A.MANHANVIEN,A.TENNHANVIEN" + "\n";
                    sSQL += "From DM_NHANVIEN A,SYS_USER B" + "\n";
                    sSQL += "WHERE A.MANHANVIEN=B.MaNV And UserID=" + clsMain.SQLString(dtBN.Rows[0]["NGUOITAO"].ToString());
                    DataTable dtTemp = clsMain.ReturnDataTable(sSQL);
                    cls_KHTT.CongDiemKhachHangThanThiet_Nhanvien(dtBN.Rows[0]["ID_KHTT"].ToString(), decimal.Parse(dtBN.Rows[0]["TONGGIAVE"].ToString()), dtTemp.Rows[0]["MANHANVIEN"].ToString());
                }
                //
                foreach (DataRow dr in dtBN.Rows)
                {
                    sSQL = "";
                    sSQL += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE,C.MALOAIGHE,C.TENLOAIGHE,A.MALOAIVE,V.TENLOAIVE,A.DONGIA,A.NGUOITAO As NGUOIBAN,A.NGAYTAO,A.MALOAIVE,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.MACHATLUONGPHIM,D.MALICHCHIEU,E.TENPHONG,E.MAPHONG,F.TENPHIM,F.MAPHIM,G.TENCHATLUONGPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
                    sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I,DM_LOAIVE V" + "\n";
                    sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                    sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                    sSQL += "And A.MALOAIVE = V.MALOAIVE" + "\n";
                    sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                    sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                    sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                    sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
                    sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                    sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                    sSQL += "And A.MAVE =" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    //18/12/2015 Vi mua ve online la da thanh toan roi nen tinh luon cho tai khoan ban //dt1.Rows[0]["NGUOITAO"] = clsUserManagement.ReturnNameOfUserLogin();//18/05/2015-bo sung-19/03/2015 Update cho bao cao ban ve khong nhay ten nhan vien ai in ve thi tinh cho nguoi do
                    // 16/5/2015 bổ sung recheck trùng vé trước khi đưa vào vé tổng hợp
                    sSQL = "";
                    sSQL += "Select MAVETONGHOP " + "\n";
                    sSQL += "From VETONGHOP " + "\n";
                    sSQL += "Where MALICHCHIEU = " + clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + "\n";
                    sSQL += "And SOGHE = " + clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + "\n";
                    DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                    if (dt2.Rows.Count % 2 != 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Vé đã xuất.Vui lòng chọn ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        continue;
                    }
                    //21/09/2015 bổ sung sửa lại lổi nhãy mã vé
                    string sMabarcode_Final = cls_KP_RES.CreateBarcode_Final("VE");
                    dt1.Rows[0]["MABARCODE"] = sMabarcode_Final;
                    sSQL = "";
                    sSQL += "Update VE Set " + "\n";
                    sSQL += "MABARCODE=" + clsMain.SQLString(sMabarcode_Final) + "," + "\n";
                    sSQL += "DATHANHTOAN=" + clsMain.SQLString(dr["DONGIA"].ToString()) + "," + "\n";
                    sSQL += "CONLAI=" + clsMain.SQLString("0") + "\n";
                    sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                    sSQL += "Update BIENNHAN Set " + "\n";
                    sSQL += "TRANGTHAI=" + clsMain.SQLString("1") + "\n";
                    sSQL += "Where MABARCODE=" + clsMain.SQLString(dr["BIENNHAN"].ToString()) + "\n";
                    // 25/2/2014 bổ sung ghi vào sổ bán vé tổng hợp để báo cáo tiền cho chính xác
                    sSQL += "Insert into VETONGHOP (LOAIVE,NGUOIBAN,NGAYBAN,MABARCODE,MALICHCHIEU,SOGHE,MAPHONG,MAPHIM,MALOAIGHE,MALOAIVE,DONGIA,MAQUAY,MA_KHACHHANG,NGUOITAO,NGAYTAO)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLStringUnicode("Vé bán") + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["NGUOIBAN"].ToString()) + ",";
                    //18/12/2015  Vi mua ve online la da thanh toan roi nen tinh luon cho tai khoan ban // sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";//19/03/2015 Update cho bao cao ban ve khong nhay ten nhan vien ai in ve thi tinh cho nguoi do
                    sSQL += clsMain.SQLString(((DateTime)dt1.Rows[0]["NGAYTAO"]).ToString("dd/MM/yyyy HH : mm")) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MABARCODE"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHONG"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHIM"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIGHE"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIVE"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["DONGIA"].ToString()) + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                    //25/03/2017 THEM MA KHACH HANG MUA ONLINE
                    sSQL += clsMain.SQLString(dr["ID_KHTT"].ToString()) + ",";
                    //18/12/2015  Vi mua ve online la da thanh toan roi nen tinh luon cho tai khoan ban //sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                    sSQL += clsMain.SQLString(dt1.Rows[0]["NGUOIBAN"].ToString()) + ",";
                    sSQL += "GETDATE()" + ")";

                    this.Cursor = Cursors.WaitCursor;
                    Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                    this.Cursor = Cursors.Default;

                    if (bRunSQL)
                    {
                       
                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_KP_RES.Mode == 2)
                        {
                            System.Threading.Thread.Sleep(1000);
                            frm.ReportName = "rptVerapphimIonah";
                        }
                        else if (cls_KP_RES.Mode == 11)
                        {
                            frm.ReportName = "rptVerapphim_Bill";
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(1000);
                            frm.ReportName = "rptVerapphim";
                        }
                        frm.DataSource = dt1;
                        frm.IsPrint = true;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = clsKP_Terminal.Mayinve;
                        frm.ExportName = "In ve";
                        frm.ShowDialog(this);
                        frm.Dispose();
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé tổng hợp không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                pnlReport.Controls.Clear();
                dtBN.Clear();
                txtMave.Text = "";
                txtSodienthoai.Text = "";
                txtMave.Focus();
                this.Close();
            }
        }
    }
}