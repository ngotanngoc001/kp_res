﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_Inbanggia : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        DataTable dtCombo;
        public Frm_BC_Inbanggia()
        {
            InitializeComponent();
        }

        private void Frm_BC_Inbanggia_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            if (optGroup.SelectedIndex == 0)//Bảng giá
            {
                dtCombo  = clsMain.ReturnDataTable("select MABANGGIA as MA,TENBANGGIA as TEN from BANGGIA where SUDUNG=1 and MABANGGIA NOT IN (Select MA_BANGGIA From THIETLAPBANGGIA) Order by TEN");
                lblData.Text = "Bảng giá";
                LoadChiTietCombo(dtCombo);
            }
        }

        private void LoadChiTietCombo( DataTable dt)
        {
            cboDATA.Properties.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                cboDATA.Properties.Items.Add(dr["TEN"].ToString());
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value || cboDATA.EditValue == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn đối tượng cần xem", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (optGroup.SelectedIndex == 0)//Bảng giá 
                {
                    sql += "Select A.MACHITIETBANGGIA,A.MABANGGIA,A.MALOAIGHE,A.MALOAIVE,A.MATHOIGIAN,A.DONGIA,A.MACDINH,B.TENBANGGIA,B.GHICHU,C.TENLOAIGHE ,D.TENLOAIVE ,E.TENTHOIGIAN" + "\n";
                    sql += "From CHITIETBANGGIA A , BANGGIA B, DM_LOAIGHE C, DM_LOAIVE D,DM_THOIGIAN E" + "\n";
                    sql += "Where A.MABANGGIA = B.MABANGGIA " + "\n";
                    sql += "And A.MALOAIGHE = C.MALOAIGHE " + "\n";
                    sql += "And A.MALOAIVE = D.MALOAIVE " + "\n";
                    sql += "And A.MATHOIGIAN = E.MATHOIGIAN " + "\n";
                    if (optGroup.SelectedIndex == 0)//Bảng giá
                    {
                        string sqlTemp = "";
                        foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                        {
                            if (item.CheckState == CheckState.Checked)
                            {
                                DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                                if (dr.Length > 0)
                                {
                                    if (sqlTemp == "")
                                    {
                                        sqlTemp += " A.MABANGGIA= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                    }
                                    else
                                    {
                                        sqlTemp += " Or A.MABANGGIA= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                    }
                                }
                            }
                        }
                        sql += "And ( " + sqlTemp + ")" + "\n";
                    }
                }
                sql += "Order by TENBANGGIA, TENLOAIGHE,TENLOAIVE" + "\n";
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colNGAY = new DataColumn();
                    colNGAY.ColumnName = "NGAY";
                    colNGAY.DataType = System.Type.GetType("System.DateTime");
                    colNGAY.DefaultValue = clsGlobal.gdServerDate;

                    DataColumn colBANGGIA = new DataColumn();
                    colBANGGIA.ColumnName = "BANGGIA";
                    colBANGGIA.DataType = System.Type.GetType("System.String");
                    bool bAll = true;
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Unchecked)
                        {
                            bAll = false;
                            break;
                        }
                    }
                    if (bAll)
                    {
                        colBANGGIA.DefaultValue = "Tất cả";
                    }
                    else
                    {
                        colBANGGIA.DefaultValue = cboDATA.Text;
                    }
                    if (ContainColumn("NGAY", dt) == false)
                        dt.Columns.Add(colNGAY);
                    if (ContainColumn("BANGGIA", dt) == false)
                        dt.Columns.Add(colBANGGIA);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource =dt;
                    frm.Mode = 31;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    throw new Exception("Chưa chọn phiếu cần in");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}