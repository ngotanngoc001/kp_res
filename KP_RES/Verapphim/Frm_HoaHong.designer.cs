﻿namespace KP_RES 
{
    partial class Frm_HoaHong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoPhieu = new DevExpress.XtraEditors.TextEdit();
            this.txtTongTien = new DevExpress.XtraEditors.TextEdit();
            this.txtDaiLy = new DevExpress.XtraEditors.TextEdit();
            this.chkDuyetChi = new DevExpress.XtraEditors.CheckEdit();
            this.txtChiecKhau = new DevExpress.XtraEditors.TextEdit();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.txtFile = new DevExpress.XtraEditors.TextEdit();
            this.btnFileCV = new DevExpress.XtraEditors.SimpleButton();
            this.lblFileCV = new DevExpress.XtraEditors.LabelControl();
            this.txtHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.lblDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.lblGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.lblDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.txtTongTienCK = new DevExpress.XtraEditors.TextEdit();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.lblNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILECV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TUYENDUNG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DAILY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIENCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOPHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSUA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOISUA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoPhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaiLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDuyetChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiecKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienCK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pal_ban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 177);
            this.panelControl1.TabIndex = 0;
            // 
            // pal_ban
            // 
            this.pal_ban.AutoScroll = true;
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.labelControl1);
            this.pal_ban.Controls.Add(this.txtSoPhieu);
            this.pal_ban.Controls.Add(this.txtTongTien);
            this.pal_ban.Controls.Add(this.txtDaiLy);
            this.pal_ban.Controls.Add(this.chkDuyetChi);
            this.pal_ban.Controls.Add(this.txtChiecKhau);
            this.pal_ban.Controls.Add(this.txtTenFile);
            this.pal_ban.Controls.Add(this.lblTenFile);
            this.pal_ban.Controls.Add(this.btnDownLoadFile);
            this.pal_ban.Controls.Add(this.txtFile);
            this.pal_ban.Controls.Add(this.btnFileCV);
            this.pal_ban.Controls.Add(this.lblFileCV);
            this.pal_ban.Controls.Add(this.txtHoaDon);
            this.pal_ban.Controls.Add(this.lblDiaChi);
            this.pal_ban.Controls.Add(this.lblGioiTinh);
            this.pal_ban.Controls.Add(this.lblDienThoai);
            this.pal_ban.Controls.Add(this.txtTongTienCK);
            this.pal_ban.Controls.Add(this.lblTen);
            this.pal_ban.Controls.Add(this.btnLuu);
            this.pal_ban.Controls.Add(this.btnSua);
            this.pal_ban.Controls.Add(this.lblNgaySinh);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(1254, 173);
            this.pal_ban.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 72);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 19);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Số phiếu";
            // 
            // txtSoPhieu
            // 
            this.txtSoPhieu.EnterMoveNextControl = true;
            this.txtSoPhieu.Location = new System.Drawing.Point(81, 69);
            this.txtSoPhieu.Name = "txtSoPhieu";
            this.txtSoPhieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPhieu.Properties.Appearance.Options.UseFont = true;
            this.txtSoPhieu.Properties.ReadOnly = true;
            this.txtSoPhieu.Size = new System.Drawing.Size(248, 26);
            this.txtSoPhieu.TabIndex = 11;
            // 
            // txtTongTien
            // 
            this.txtTongTien.EnterMoveNextControl = true;
            this.txtTongTien.Location = new System.Drawing.Point(81, 37);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.Properties.Appearance.Options.UseFont = true;
            this.txtTongTien.Properties.ReadOnly = true;
            this.txtTongTien.Size = new System.Drawing.Size(248, 26);
            this.txtTongTien.TabIndex = 5;
            // 
            // txtDaiLy
            // 
            this.txtDaiLy.EnterMoveNextControl = true;
            this.txtDaiLy.Location = new System.Drawing.Point(420, 5);
            this.txtDaiLy.Name = "txtDaiLy";
            this.txtDaiLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDaiLy.Properties.Appearance.Options.UseFont = true;
            this.txtDaiLy.Properties.ReadOnly = true;
            this.txtDaiLy.Size = new System.Drawing.Size(578, 26);
            this.txtDaiLy.TabIndex = 3;
            // 
            // chkDuyetChi
            // 
            this.chkDuyetChi.EnterMoveNextControl = true;
            this.chkDuyetChi.Location = new System.Drawing.Point(79, 101);
            this.chkDuyetChi.Name = "chkDuyetChi";
            this.chkDuyetChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDuyetChi.Properties.Appearance.Options.UseFont = true;
            this.chkDuyetChi.Properties.Caption = "Xác nhận chi hoa hồng";
            this.chkDuyetChi.Size = new System.Drawing.Size(250, 24);
            this.chkDuyetChi.TabIndex = 17;
            // 
            // txtChiecKhau
            // 
            this.txtChiecKhau.EnterMoveNextControl = true;
            this.txtChiecKhau.Location = new System.Drawing.Point(420, 37);
            this.txtChiecKhau.Name = "txtChiecKhau";
            this.txtChiecKhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiecKhau.Properties.Appearance.Options.UseFont = true;
            this.txtChiecKhau.Properties.ReadOnly = true;
            this.txtChiecKhau.Size = new System.Drawing.Size(248, 26);
            this.txtChiecKhau.TabIndex = 7;
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(750, 69);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 16;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(687, 72);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 15;
            this.lblTenFile.Text = "Tên File";
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDownLoadFile.Location = new System.Drawing.Point(257, 132);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(87, 35);
            this.btnDownLoadFile.TabIndex = 20;
            this.btnDownLoadFile.Text = "Tải File";
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // txtFile
            // 
            this.txtFile.EnterMoveNextControl = true;
            this.txtFile.Location = new System.Drawing.Point(420, 69);
            this.txtFile.Name = "txtFile";
            this.txtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Properties.Appearance.Options.UseFont = true;
            this.txtFile.Properties.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(185, 26);
            this.txtFile.TabIndex = 13;
            // 
            // btnFileCV
            // 
            this.btnFileCV.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileCV.Appearance.Options.UseFont = true;
            this.btnFileCV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFileCV.Location = new System.Drawing.Point(612, 70);
            this.btnFileCV.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileCV.Name = "btnFileCV";
            this.btnFileCV.Size = new System.Drawing.Size(56, 25);
            this.btnFileCV.TabIndex = 14;
            this.btnFileCV.Text = "....";
            this.btnFileCV.Click += new System.EventHandler(this.btnFileCV_Click);
            // 
            // lblFileCV
            // 
            this.lblFileCV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileCV.Location = new System.Drawing.Point(336, 72);
            this.lblFileCV.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileCV.Name = "lblFileCV";
            this.lblFileCV.Size = new System.Drawing.Size(24, 19);
            this.lblFileCV.TabIndex = 12;
            this.lblFileCV.Text = "File";
            // 
            // txtHoaDon
            // 
            this.txtHoaDon.EnterMoveNextControl = true;
            this.txtHoaDon.Location = new System.Drawing.Point(81, 5);
            this.txtHoaDon.Name = "txtHoaDon";
            this.txtHoaDon.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoaDon.Properties.Appearance.Options.UseFont = true;
            this.txtHoaDon.Properties.ReadOnly = true;
            this.txtHoaDon.Size = new System.Drawing.Size(248, 26);
            this.txtHoaDon.TabIndex = 1;
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi.Location = new System.Drawing.Point(336, 40);
            this.lblDiaChi.Margin = new System.Windows.Forms.Padding(4);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(75, 19);
            this.lblDiaChi.TabIndex = 6;
            this.lblDiaChi.Text = "Chiết khấu";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioiTinh.Location = new System.Drawing.Point(6, 40);
            this.lblGioiTinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(68, 19);
            this.lblGioiTinh.TabIndex = 4;
            this.lblGioiTinh.Text = "Tổng tiền";
            // 
            // lblDienThoai
            // 
            this.lblDienThoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDienThoai.Location = new System.Drawing.Point(687, 40);
            this.lblDienThoai.Margin = new System.Windows.Forms.Padding(4);
            this.lblDienThoai.Name = "lblDienThoai";
            this.lblDienThoai.Size = new System.Drawing.Size(55, 19);
            this.lblDienThoai.TabIndex = 8;
            this.lblDienThoai.Text = "Tiền CK";
            // 
            // txtTongTienCK
            // 
            this.txtTongTienCK.EnterMoveNextControl = true;
            this.txtTongTienCK.Location = new System.Drawing.Point(750, 37);
            this.txtTongTienCK.Name = "txtTongTienCK";
            this.txtTongTienCK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTienCK.Properties.Appearance.Options.UseFont = true;
            this.txtTongTienCK.Properties.ReadOnly = true;
            this.txtTongTienCK.Size = new System.Drawing.Size(248, 26);
            this.txtTongTienCK.TabIndex = 9;
            // 
            // lblTen
            // 
            this.lblTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Location = new System.Drawing.Point(6, 8);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(60, 19);
            this.lblTen.TabIndex = 0;
            this.lblTen.Text = "Hóa đơn";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(81, 132);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 18;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(169, 132);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 19;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh.Location = new System.Drawing.Point(336, 8);
            this.lblNgaySinh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(40, 19);
            this.lblNgaySinh.TabIndex = 2;
            this.lblNgaySinh.Text = "Đại lý";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 177);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 474);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILECV,
            this.TRANGTHAI_ID,
            this.GIOITINH_ID,
            this.TUYENDUNG_ID,
            this.STT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.DAILY,
            this.TONGTIEN,
            this.CHIETKHAU,
            this.TONGTIENCK,
            this.SOPHIEU,
            this.DACHI,
            this.TEN_FILES,
            this.NGAYSUA,
            this.NGUOISUA,
            this.gridColumn1});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILECV
            // 
            this.FILECV.FieldName = "FILECV";
            this.FILECV.Name = "FILECV";
            this.FILECV.OptionsColumn.AllowEdit = false;
            this.FILECV.OptionsColumn.AllowFocus = false;
            this.FILECV.OptionsColumn.FixedWidth = true;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.FixedWidth = true;
            // 
            // GIOITINH_ID
            // 
            this.GIOITINH_ID.FieldName = "GIOITINH_ID";
            this.GIOITINH_ID.Name = "GIOITINH_ID";
            this.GIOITINH_ID.OptionsColumn.AllowEdit = false;
            this.GIOITINH_ID.OptionsColumn.AllowFocus = false;
            this.GIOITINH_ID.OptionsColumn.FixedWidth = true;
            // 
            // TUYENDUNG_ID
            // 
            this.TUYENDUNG_ID.FieldName = "TUYENDUNG_ID";
            this.TUYENDUNG_ID.Name = "TUYENDUNG_ID";
            this.TUYENDUNG_ID.OptionsColumn.AllowEdit = false;
            this.TUYENDUNG_ID.OptionsColumn.AllowFocus = false;
            this.TUYENDUNG_ID.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_HOADON.Caption = "Hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 150;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 2;
            this.NGAYTAO.Width = 150;
            // 
            // DAILY
            // 
            this.DAILY.AppearanceCell.Options.UseTextOptions = true;
            this.DAILY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DAILY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DAILY.AppearanceHeader.Options.UseFont = true;
            this.DAILY.AppearanceHeader.Options.UseTextOptions = true;
            this.DAILY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DAILY.Caption = "Đại lý";
            this.DAILY.FieldName = "DAILY";
            this.DAILY.Name = "DAILY";
            this.DAILY.OptionsColumn.AllowEdit = false;
            this.DAILY.OptionsColumn.AllowFocus = false;
            this.DAILY.OptionsColumn.FixedWidth = true;
            this.DAILY.Visible = true;
            this.DAILY.VisibleIndex = 3;
            this.DAILY.Width = 250;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.Caption = "Tổng tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.OptionsColumn.FixedWidth = true;
            this.TONGTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGTIEN", "{0:#,###}")});
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 4;
            this.TONGTIEN.Width = 150;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.Caption = "Chiết khấu";
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 5;
            this.CHIETKHAU.Width = 100;
            // 
            // TONGTIENCK
            // 
            this.TONGTIENCK.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIENCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTIENCK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIENCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGTIENCK.AppearanceHeader.Options.UseFont = true;
            this.TONGTIENCK.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIENCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIENCK.Caption = "Tổng tiền CK";
            this.TONGTIENCK.DisplayFormat.FormatString = "N0";
            this.TONGTIENCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIENCK.FieldName = "TONGTIENCK";
            this.TONGTIENCK.Name = "TONGTIENCK";
            this.TONGTIENCK.OptionsColumn.AllowEdit = false;
            this.TONGTIENCK.OptionsColumn.AllowFocus = false;
            this.TONGTIENCK.OptionsColumn.FixedWidth = true;
            this.TONGTIENCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGTIENCK", "{0:#,###}")});
            this.TONGTIENCK.Visible = true;
            this.TONGTIENCK.VisibleIndex = 6;
            this.TONGTIENCK.Width = 150;
            // 
            // SOPHIEU
            // 
            this.SOPHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SOPHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOPHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOPHIEU.AppearanceHeader.Options.UseFont = true;
            this.SOPHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHIEU.Caption = "Số phiếu";
            this.SOPHIEU.FieldName = "SOPHIEU";
            this.SOPHIEU.Name = "SOPHIEU";
            this.SOPHIEU.OptionsColumn.AllowEdit = false;
            this.SOPHIEU.OptionsColumn.AllowFocus = false;
            this.SOPHIEU.OptionsColumn.FixedWidth = true;
            this.SOPHIEU.Visible = true;
            this.SOPHIEU.VisibleIndex = 8;
            this.SOPHIEU.Width = 150;
            // 
            // DACHI
            // 
            this.DACHI.AppearanceCell.Options.UseTextOptions = true;
            this.DACHI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DACHI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DACHI.AppearanceHeader.Options.UseFont = true;
            this.DACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DACHI.Caption = "Chi hoa hồng";
            this.DACHI.FieldName = "DACHI";
            this.DACHI.Name = "DACHI";
            this.DACHI.OptionsColumn.AllowEdit = false;
            this.DACHI.OptionsColumn.AllowFocus = false;
            this.DACHI.OptionsColumn.FixedWidth = true;
            this.DACHI.Visible = true;
            this.DACHI.VisibleIndex = 7;
            this.DACHI.Width = 120;
            // 
            // TEN_FILES
            // 
            this.TEN_FILES.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_FILES.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_FILES.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILES.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILES.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILES.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILES.Caption = "File";
            this.TEN_FILES.FieldName = "TEN_FILES";
            this.TEN_FILES.Name = "TEN_FILES";
            this.TEN_FILES.OptionsColumn.AllowEdit = false;
            this.TEN_FILES.OptionsColumn.AllowFocus = false;
            this.TEN_FILES.OptionsColumn.FixedWidth = true;
            this.TEN_FILES.Visible = true;
            this.TEN_FILES.VisibleIndex = 9;
            this.TEN_FILES.Width = 150;
            // 
            // NGAYSUA
            // 
            this.NGAYSUA.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSUA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSUA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYSUA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSUA.AppearanceHeader.Options.UseFont = true;
            this.NGAYSUA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSUA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSUA.Caption = "Ngày sữa";
            this.NGAYSUA.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYSUA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSUA.FieldName = "NGAYSUA";
            this.NGAYSUA.Name = "NGAYSUA";
            this.NGAYSUA.OptionsColumn.AllowEdit = false;
            this.NGAYSUA.OptionsColumn.AllowFocus = false;
            this.NGAYSUA.OptionsColumn.FixedWidth = true;
            this.NGAYSUA.Visible = true;
            this.NGAYSUA.VisibleIndex = 10;
            this.NGAYSUA.Width = 100;
            // 
            // NGUOISUA
            // 
            this.NGUOISUA.AppearanceCell.Options.UseTextOptions = true;
            this.NGUOISUA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGUOISUA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGUOISUA.AppearanceHeader.Options.UseFont = true;
            this.NGUOISUA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUOISUA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUOISUA.Caption = "Người sữa";
            this.NGUOISUA.FieldName = "NGUOISUA";
            this.NGUOISUA.Name = "NGUOISUA";
            this.NGUOISUA.OptionsColumn.AllowEdit = false;
            this.NGUOISUA.OptionsColumn.AllowFocus = false;
            this.NGUOISUA.OptionsColumn.FixedWidth = true;
            this.NGUOISUA.Visible = true;
            this.NGUOISUA.VisibleIndex = 11;
            this.NGUOISUA.Width = 200;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 12;
            this.gridColumn1.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 177);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 474);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 310);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 150);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 230);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 392);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_HoaHong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_HoaHong";
            this.Text = "Hoa hồng";
            this.Load += new System.EventHandler(this.Frm_HoaHong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            this.pal_ban.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoPhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaiLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDuyetChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiecKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTienCK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIENCK;
        private DevExpress.XtraGrid.Columns.GridColumn DACHI;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSUA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILES;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOISUA;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.Columns.GridColumn DAILY;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn FILECV;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.TextEdit txtChiecKhau;
        private DevExpress.XtraEditors.TextEdit txtTenFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.TextEdit txtFile;
        private DevExpress.XtraEditors.SimpleButton btnFileCV;
        private DevExpress.XtraEditors.LabelControl lblFileCV;
        private DevExpress.XtraEditors.TextEdit txtHoaDon;
        private DevExpress.XtraEditors.LabelControl lblDiaChi;
        private DevExpress.XtraEditors.LabelControl lblGioiTinh;
        private DevExpress.XtraEditors.LabelControl lblDienThoai;
        private DevExpress.XtraEditors.TextEdit txtTongTienCK;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.LabelControl lblNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn TUYENDUNG_ID;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.CheckEdit chkDuyetChi;
        private DevExpress.XtraEditors.TextEdit txtTongTien;
        private DevExpress.XtraEditors.TextEdit txtDaiLy;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSoPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;




    }
}