﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using WMPLib;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Manhinhhai_Ve : Form
    {
        bool[] listDaDownVideo;

        public Frm_Manhinhhai_Ve()
        {
            InitializeComponent();
        }

        private void Frm_Manhinhhai_Ve_Load(object sender, EventArgs e)
        {
            cls_KP_RES.bFormQuangCao = true;
            pnlSodoghe.Visible = false;
            pnlTrailer.Visible = true;
            timer1.Enabled = true;
            timer2.Enabled = true;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                timer2.Enabled = false;
                TaiVideoVe();
                LoadVideo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TaiVideoVe()
        {
            string maQuay = cls_ConfigCashier.idCashier;
            string sqldownvideo = "select h.MAHINH,h.TENHINH from  QUAY q inner join QC_HINHANH_QUAY qh on q.MA_QUAY=qh.MA_QUAY inner join QC_HINHANH h on qh.MAHINH=h.MAHINH inner join QC_QUANGCAO qc on qc.MAQC=h.MAQC where qc.MODE=0 and q.MA_QUAY='" + maQuay + "' ";
            DataTable dtadownvideo = clsMain.ReturnDataTable(sqldownvideo);
            listDaDownVideo = new bool[dtadownvideo.Rows.Count];
            for (int k = 0; k < dtadownvideo.Rows.Count; k++)
            {
                listDaDownVideo[k] = false;//gắn mặc định là chưa down về
            }
            DirectoryInfo directoryVideo = new DirectoryInfo(Application.StartupPath + "\\VideoHaiManHinh");
            FileInfo[] filesVideo = directoryVideo.GetFiles("*.*");//dữ liệu đang nằm trong folder 
            foreach (FileInfo f in filesVideo)
            {
                if (!checkname(dtadownvideo, f.Name))
                {
                    f.Delete();
                }
            }
            for (int i = 0; i < dtadownvideo.Rows.Count; i++)
            {
                if (!listDaDownVideo[i])
                {
                    string sqldadown1 = "select URL from  QC_HINHANH where MAHINH='" + dtadownvideo.Rows[i]["MAHINH"].ToString() + "'";
                    DataTable dtadadown1 = clsMain.ReturnDataTable(sqldadown1);
                    byte[] fileBytes = new byte[((byte[])dtadadown1.Rows[0]["URL"]).LongLength];
                    fileBytes = (byte[])dtadadown1.Rows[0]["URL"];
                    Stream file = File.OpenWrite(Application.StartupPath + "/VideoHaiManHinh/" + dtadownvideo.Rows[i]["TENHINH"].ToString() + "");
                    file.Write(fileBytes, 0, fileBytes.Length);
                    file.Close();
                }
            }
        }

        private bool checkname(DataTable dt, string name)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TENHINH"].ToString() == name)//folder down về đã có file
                {
                    listDaDownVideo[i] = true;
                    return true;
                }
            }
            return false;
        }

        private void LoadVideo()
        {
            try
            {
                if (WindowsMediaPlayer.playlistCollection.getAll().count > 1)
                {
                    WindowsMediaPlayer.playlistCollection.remove(WindowsMediaPlayer.playlistCollection.getAll().Item(0));
                }
                WMPLib.IWMPPlaylist playlist = WindowsMediaPlayer.playlistCollection.newPlaylist("myplaylist");
                WMPLib.IWMPMedia media;
                DirectoryInfo directory = new DirectoryInfo(Application.StartupPath + "\\VideoHaiManHinh");
                FileInfo[] files = directory.GetFiles("*.*");
                if (files.Length > 0)
                {
                    foreach (FileInfo f in files)
                    {
                        media = WindowsMediaPlayer.newMedia(f.FullName);
                        playlist.appendItem(media);
                    }
                    WindowsMediaPlayer.settings.setMode("loop", true);
                    WindowsMediaPlayer.currentPlaylist = playlist;
                    WindowsMediaPlayer.Ctlcontrols.play();
                    WindowsMediaPlayer.uiMode = "none";
                    WindowsMediaPlayer.stretchToFit = true;
                }
            }
            catch
            {
            }
        }

        string phong_old = "0";
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
                if (!cls_KP_RES.bFormQuangCao)
                {
                    this.Close();
                    this.Dispose();
                    return;
                }
                if (!cls_KP_RES.bFormSodoghe)
                {
                    pnlSodoghe.Visible = false;
                    pnlTrailer.Visible = true;
                }
                else
                {
                    pnlTrailer.Visible = false;
                    pnlSodoghe.Visible = true;

                    txtRap.Text = cls_KP_RES.sTenRap;
                    lblThongtinxuatchieu.Text = cls_KP_RES.sThongtinxuatchieu;
                    lblThongtinxuatchieu1.Text = cls_KP_RES.sThongtinxuatchieu1;
                    if (this.Width < 1024)
                    {
                        panelControl1.Visible = false;
                        pnlMain.Visible = false;
                        pnlMain1.Visible = false;
                    }
                    else
                    {
                        LoadDanhSachVe();
                        LoadGiaiThich();
                    }
                    if (phong_old != cls_KP_RES.sTenPhong)
                    {
                        phong_old = cls_KP_RES.sTenPhong;
                        LoadSoDoGhe(true);
                    }
                    else
                    {
                        LoadSoDoGhe(false);
                    }
                }
            }
            catch
            {
            }
            timer1.Enabled = true;
        }

        private void LoadDanhSachVe()
        {
            DataTable dt = cls_KP_RES.dtDanhSachVe;
            gridControl.DataSource = dt;
            gridView.UpdateTotalSummary();
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
        }

        private void LoadGiaiThich()
        {
            if (fpnlGiaithich.Controls.Count > 0)
            {
                return;
            }
            DataTable dt=cls_KP_RES.dtLoaiGhe;
            foreach (DataRow dr in dt.Rows)
            {
                UC_Ghe txt1 = new UC_Ghe();
                txt1.Width = 40;
                txt1.Height = 40;
                txt1.Text = "A01";
                txt1.Tag = "A01";
                txt1.ForeColor = Color.White;
                txt1.BackColor = cls_KP_RES.ConvertStringToColor(dr["mau"].ToString());
                txt1.Font = fpnlGiaithich.Font;
                txt1.BorderStyle = BorderStyle.None;
                Label txt2 = new Label();
                txt2.Width = 100;
                txt2.Height = 40;
                txt2.ForeColor = Color.Black;
                txt2.BackColor = Color.White; ;
                txt2.Text = dr["tenloaighe"].ToString().IndexOf("GHẾ") == -1 ? "GHẾ " + dr["tenloaighe"].ToString() : dr["tenloaighe"].ToString();
                txt2.Font = fpnlGiaithich.Font;
                txt2.BorderStyle = BorderStyle.None;
                txt2.TextAlign = ContentAlignment.MiddleCenter;

                fpnlGiaithich.Controls.Add(txt1);
                fpnlGiaithich.Controls.Add(txt2);
            }

            UC_Ghe txt3 = new UC_Ghe();
            txt3.Width = 40;
            txt3.Height = 40;
            txt3.Text = "A01";
            txt3.Tag = "A01";
            txt3.ForeColor = Color.Silver;
            txt3.BackColor = Color.DarkGray;
            txt3.Font = fpnlGiaithich.Font;
            txt3.BorderStyle = BorderStyle.None;
            Label txt4 = new Label();
            txt4.Width = 100;
            txt4.Height = 40;
            txt4.ForeColor = Color.Black;
            txt4.BackColor = Color.White; ;
            txt4.Text = "GHẾ ĐÃ BÁN";
            txt4.Font = fpnlGiaithich.Font;
            txt4.BorderStyle = BorderStyle.None;
            txt4.TextAlign = ContentAlignment.MiddleCenter;

            fpnlGiaithich.Controls.Add(txt3);
            fpnlGiaithich.Controls.Add(txt4);

            UC_Ghe txt5 = new UC_Ghe();
            txt5.Width = 40;
            txt5.Height = 40;
            txt5.Text = "A01";
            txt5.Tag = "A01";
            txt5.ForeColor = Color.Blue;
            txt5.BackColor = cls_KP_RES.ConvertStringToColor(dt.Rows[0]["mau"].ToString());
            txt5.Font = fpnlGiaithich.Font;
            txt5.BorderStyle = BorderStyle.None;
            Label txt6 = new Label();
            txt6.Width = 100;
            txt6.Height = 40;
            txt6.ForeColor = Color.Black;
            txt6.BackColor = Color.White; ;
            txt6.Text = "GHẾ ĐÃ ĐẶT";
            txt6.Font = fpnlGiaithich.Font;
            txt6.BorderStyle = BorderStyle.None;
            txt6.TextAlign = ContentAlignment.MiddleCenter;

            fpnlGiaithich.Controls.Add(txt5);
            fpnlGiaithich.Controls.Add(txt6);

            UC_Loidi txt7 = new UC_Loidi();
            txt7.Width = 40;
            txt7.Height = 40;
            txt7.Text = "V";
            txt7.Tag = "V";
            txt7.ForeColor = Color.White;
            txt7.Font = fpnlGiaithich.Font;
            txt7.BorderStyle = BorderStyle.None;
            Label txt8 = new Label();
            txt8.Width = 100;
            txt8.Height = 40;
            txt8.ForeColor = Color.Black;
            txt8.BackColor = Color.White; ;
            txt8.Text = "LỐI ĐI";
            txt8.Font = fpnlGiaithich.Font;
            txt8.BorderStyle = BorderStyle.None;
            txt8.TextAlign = ContentAlignment.MiddleCenter;

            fpnlGiaithich.Controls.Add(txt7);
            fpnlGiaithich.Controls.Add(txt8);

            UC_Manhinh txt9 = new UC_Manhinh();
            txt9.Width = 80;
            txt9.Height = 40;
            txt9.Text = "SCREEN";
            txt9.Tag = "SCREEN";
            txt9.ForeColor = Color.White;
            txt9.Font = fpnlGiaithich.Font;
            txt9.BorderStyle = BorderStyle.None;
            Label txt10 = new Label();
            txt10.Width = 80;
            txt10.Height = 40;
            txt10.ForeColor = Color.Black;
            txt10.BackColor = Color.White; ;
            txt10.Text = "MÀN HÌNH";
            txt10.Font = fpnlGiaithich.Font;
            txt10.BorderStyle = BorderStyle.None;
            txt10.TextAlign = ContentAlignment.MiddleCenter;

            fpnlGiaithich.Controls.Add(txt9);
            fpnlGiaithich.Controls.Add(txt10);

            pnlDes.Height = (fpnlGiaithich.Controls.Count / 2) * 40;
        }

        private void LoadSoDoGhe(bool bnew)
        {
            DataTable dtSodoghe = cls_KP_RES.dtSodoghe;
            if (bnew)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Controls.Clear();
                foreach (DataRow dr in dtSodoghe.Rows)
                {
                    Control txt;
                    if (dr["TENGHE"].ToString().Length > 1 && dr["TENGHE"].ToString().Length < 4)
                    {
                        txt = new UC_Ghe();
                    }
                    else if (dr["TENGHE"].ToString().Length <= 1)
                    {
                        txt = new UC_Loidi();
                    }
                    else
                    {
                        txt = new UC_Manhinh();
                    }
                    txt.Name = dr["MAVE"].ToString();
                    txt.Text = dr["TENGHE"].ToString();
                    txt.Tag = dr["TENGHE"].ToString();
                    txt.Location = new Point(int.Parse(dr["X"].ToString()), int.Parse(dr["Y"].ToString()));
                    txt.Width = int.Parse(dr["W"].ToString());
                    txt.Height = int.Parse(dr["H"].ToString());
                    if (txt is UC_Ghe)
                    {
                        txt.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAUSAC"].ToString());
                    }
                    if (Boolean.Parse(dr["TRANGTHAI"].ToString()))
                    {
                        if (dr["CONLAI"].ToString() == "0")
                        {
                            txt.ForeColor = Color.Silver;
                            txt.BackColor = Color.DarkGray;
                        }
                        else
                        {
                            if (dr["TENHANHKHACH"].ToString() == "")
                            {
                                txt.ForeColor = Color.Black;
                            }
                            else
                            {
                                //txt.ForeColor = Color.Blue;
                                //05/04/2017 sửa lại ghế đặt dù thanh toánh ay chưa vẩn ẩn cho khách khỏi chọn
                                txt.ForeColor = Color.Silver;
                                txt.BackColor = Color.DarkGray;
                            }
                        }
                    }
                    else
                    {
                        txt.ForeColor = Color.White;
                    }
                    txt.Font = pnlSodo.Font;

                    pnlSodo.Controls.Add(txt);
                }
                pnlSodo.Visible = true;
            }
            else
            {
                for (int i = 0; i < dtSodoghe.Rows.Count; i++)
                {
                    if (pnlSodo.Controls[i] is UC_Ghe)
                    {
                        pnlSodo.Controls[i].BackColor = cls_KP_RES.ConvertStringToColor(dtSodoghe.Rows[i]["MAUSAC"].ToString());
                        if (Boolean.Parse(dtSodoghe.Rows[i]["TRANGTHAI"].ToString()))
                        {
                            if (dtSodoghe.Rows[i]["CONLAI"].ToString() == "0")
                            {
                                pnlSodo.Controls[i].ForeColor = Color.Silver;
                                pnlSodo.Controls[i].BackColor = Color.DarkGray;
                            }
                            else
                            {
                                if (dtSodoghe.Rows[i]["TENHANHKHACH"].ToString() == "")
                                {
                                    pnlSodo.Controls[i].ForeColor = Color.Black;
                                }
                                else
                                {
                                    //pnlSodo.Controls[i].ForeColor = Color.Blue;
                                    //05/04/2017 sửa lại ghế đặt dù thanh toánh ay chưa vẩn ẩn cho khách khỏi chọn
                                    pnlSodo.Controls[i].ForeColor = Color.Silver;
                                    pnlSodo.Controls[i].BackColor = Color.DarkGray;
                                }
                            }
                        }
                        else
                        {
                            pnlSodo.Controls[i].ForeColor = Color.White;
                        }
                    }
                }
            }
        }
    }
}
