﻿namespace KP_RES 
{
    partial class  Frm_ShowTime 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ShowTime));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pn_Hinh = new DevExpress.XtraEditors.PanelControl();
            this.pn_xemvideo = new DevExpress.XtraEditors.PanelControl();
            this.WindowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.pn_cuon = new DevExpress.XtraEditors.PanelControl();
            this.pal_xuong = new DevExpress.XtraEditors.PanelControl();
            this.lbl_sotrang = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.btn_bochonhinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaHinh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CaiDat_Video = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bnt_Play = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_stop = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CHON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnbochonvideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThemVideo = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CuaHang = new DevExpress.XtraEditors.PanelControl();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.pnQuangCao = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cbHour = new System.Windows.Forms.ComboBox();
            this.cbThongBao = new System.Windows.Forms.ComboBox();
            this.cbTenPhim = new System.Windows.Forms.ComboBox();
            this.cbGhiChu = new System.Windows.Forms.ComboBox();
            this.cbShowTime = new System.Windows.Forms.ComboBox();
            this.chkTextNote = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditLitleTicket = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditNoTicket = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditManyTicket = new DevExpress.XtraEditors.ColorEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditName = new DevExpress.XtraEditors.ColorEdit();
            this.chkRunVideo = new DevExpress.XtraEditors.CheckEdit();
            this.colorEditNote = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditNameFilm = new DevExpress.XtraEditors.ColorEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pbBackGround = new System.Windows.Forms.PictureBox();
            this.fontChu = new DevExpress.XtraEditors.FontEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtLich = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtSLVe = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.memoNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtThoiGianMenu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.mTenRap = new DevExpress.XtraEditors.TextEdit();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefesh = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyBo = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).BeginInit();
            this.pn_Hinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).BeginInit();
            this.pn_xemvideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).BeginInit();
            this.pal_xuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).BeginInit();
            this.pn_CaiDat_Video.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).BeginInit();
            this.pn_CuaHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnQuangCao)).BeginInit();
            this.pnQuangCao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTextNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLitleTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNoTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditManyTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRunVideo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNameFilm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBackGround)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLVe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTenRap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pn_Hinh);
            this.panelControl1.Controls.Add(this.pn_CaiDat_Video);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1271, 634);
            this.panelControl1.TabIndex = 0;
            // 
            // pn_Hinh
            // 
            this.pn_Hinh.Controls.Add(this.pn_xemvideo);
            this.pn_Hinh.Controls.Add(this.pn_cuon);
            this.pn_Hinh.Controls.Add(this.pal_xuong);
            this.pn_Hinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Hinh.Location = new System.Drawing.Point(2, 2);
            this.pn_Hinh.Name = "pn_Hinh";
            this.pn_Hinh.Size = new System.Drawing.Size(1267, 309);
            this.pn_Hinh.TabIndex = 56;
            // 
            // pn_xemvideo
            // 
            this.pn_xemvideo.Controls.Add(this.WindowsMediaPlayer);
            this.pn_xemvideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_xemvideo.Location = new System.Drawing.Point(2, 37);
            this.pn_xemvideo.Name = "pn_xemvideo";
            this.pn_xemvideo.Size = new System.Drawing.Size(1263, 270);
            this.pn_xemvideo.TabIndex = 0;
            this.pn_xemvideo.Visible = false;
            // 
            // WindowsMediaPlayer
            // 
            this.WindowsMediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowsMediaPlayer.Enabled = true;
            this.WindowsMediaPlayer.Location = new System.Drawing.Point(2, 2);
            this.WindowsMediaPlayer.Name = "WindowsMediaPlayer";
            this.WindowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WindowsMediaPlayer.OcxState")));
            this.WindowsMediaPlayer.Size = new System.Drawing.Size(1259, 266);
            this.WindowsMediaPlayer.TabIndex = 2;
            // 
            // pn_cuon
            // 
            this.pn_cuon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_cuon.Location = new System.Drawing.Point(2, 37);
            this.pn_cuon.Name = "pn_cuon";
            this.pn_cuon.Size = new System.Drawing.Size(1263, 270);
            this.pn_cuon.TabIndex = 56;
            // 
            // pal_xuong
            // 
            this.pal_xuong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_xuong.Controls.Add(this.lbl_sotrang);
            this.pal_xuong.Controls.Add(this.bnt_trangxuong);
            this.pal_xuong.Controls.Add(this.bnt_tranglen);
            this.pal_xuong.Controls.Add(this.btn_bochonhinh);
            this.pal_xuong.Controls.Add(this.btnChonTatCaHinh);
            this.pal_xuong.Controls.Add(this.btn_Xoa);
            this.pal_xuong.Controls.Add(this.btn_Them);
            this.pal_xuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_xuong.Location = new System.Drawing.Point(2, 2);
            this.pal_xuong.Name = "pal_xuong";
            this.pal_xuong.Size = new System.Drawing.Size(1263, 35);
            this.pal_xuong.TabIndex = 53;
            // 
            // lbl_sotrang
            // 
            this.lbl_sotrang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrang.Appearance.Options.UseFont = true;
            this.lbl_sotrang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_sotrang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrang.Location = new System.Drawing.Point(774, 0);
            this.lbl_sotrang.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrang.Name = "lbl_sotrang";
            this.lbl_sotrang.Size = new System.Drawing.Size(489, 35);
            this.lbl_sotrang.TabIndex = 49;
            this.lbl_sotrang.Text = "1/5";
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(645, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(129, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(516, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(129, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // btn_bochonhinh
            // 
            this.btn_bochonhinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_bochonhinh.Appearance.Options.UseFont = true;
            this.btn_bochonhinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_bochonhinh.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btn_bochonhinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_bochonhinh.Location = new System.Drawing.Point(387, 0);
            this.btn_bochonhinh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_bochonhinh.Name = "btn_bochonhinh";
            this.btn_bochonhinh.Size = new System.Drawing.Size(129, 35);
            this.btn_bochonhinh.TabIndex = 55;
            this.btn_bochonhinh.Text = "Bỏ chọn";
            this.btn_bochonhinh.Click += new System.EventHandler(this.btn_bochonhinh_Click);
            // 
            // btnChonTatCaHinh
            // 
            this.btnChonTatCaHinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaHinh.Appearance.Options.UseFont = true;
            this.btnChonTatCaHinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaHinh.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaHinh.Image")));
            this.btnChonTatCaHinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaHinh.Location = new System.Drawing.Point(258, 0);
            this.btnChonTatCaHinh.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaHinh.Name = "btnChonTatCaHinh";
            this.btnChonTatCaHinh.Size = new System.Drawing.Size(129, 35);
            this.btnChonTatCaHinh.TabIndex = 57;
            this.btnChonTatCaHinh.Text = "Chọn tất cả";
            this.btnChonTatCaHinh.Click += new System.EventHandler(this.btnChonTatCaHinh_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Xoa.Appearance.Options.UseFont = true;
            this.btn_Xoa.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Xoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_Xoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Xoa.Location = new System.Drawing.Point(129, 0);
            this.btn_Xoa.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(129, 35);
            this.btn_Xoa.TabIndex = 54;
            this.btn_Xoa.Text = "Xóa hình";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Them
            // 
            this.btn_Them.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Them.Appearance.Options.UseFont = true;
            this.btn_Them.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Them.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_Them.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Them.Location = new System.Drawing.Point(0, 0);
            this.btn_Them.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(129, 35);
            this.btn_Them.TabIndex = 51;
            this.btn_Them.Text = "Thêm hình";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // pn_CaiDat_Video
            // 
            this.pn_CaiDat_Video.Controls.Add(this.gridControl2);
            this.pn_CaiDat_Video.Controls.Add(this.panelControl3);
            this.pn_CaiDat_Video.Controls.Add(this.pn_CuaHang);
            this.pn_CaiDat_Video.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_CaiDat_Video.Location = new System.Drawing.Point(2, 311);
            this.pn_CaiDat_Video.Name = "pn_CaiDat_Video";
            this.pn_CaiDat_Video.Size = new System.Drawing.Size(1267, 321);
            this.pn_CaiDat_Video.TabIndex = 58;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.bnt_Play,
            this.btn_stop});
            this.gridControl2.Size = new System.Drawing.Size(551, 278);
            this.gridControl2.TabIndex = 55;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENHINH,
            this.gridColumn10,
            this.gridColumn11,
            this.CHON,
            this.MAHINH});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupFormat = "";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsView.ShowGroupedColumns = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // TENHINH
            // 
            this.TENHINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENHINH.AppearanceHeader.Options.UseFont = true;
            this.TENHINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHINH.Caption = "Tên";
            this.TENHINH.FieldName = "TENHINH";
            this.TENHINH.Name = "TENHINH";
            this.TENHINH.OptionsColumn.AllowEdit = false;
            this.TENHINH.OptionsColumn.AllowFocus = false;
            this.TENHINH.Visible = true;
            this.TENHINH.VisibleIndex = 1;
            this.TENHINH.Width = 199;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Xem ";
            this.gridColumn10.ColumnEdit = this.bnt_Play;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 100;
            // 
            // bnt_Play
            // 
            this.bnt_Play.AutoHeight = false;
            this.bnt_Play.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("bnt_Play.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.bnt_Play.Name = "bnt_Play";
            this.bnt_Play.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.bnt_Play.Click += new System.EventHandler(this.bnt_Play_Click);
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Thoát";
            this.gridColumn11.ColumnEdit = this.btn_stop;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 100;
            // 
            // btn_stop
            // 
            this.btn_stop.AutoHeight = false;
            this.btn_stop.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btn_stop.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // CHON
            // 
            this.CHON.AppearanceCell.Options.UseTextOptions = true;
            this.CHON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHON.AppearanceHeader.Options.UseFont = true;
            this.CHON.AppearanceHeader.Options.UseTextOptions = true;
            this.CHON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.Caption = "Chọn";
            this.CHON.FieldName = "CHON";
            this.CHON.Name = "CHON";
            this.CHON.OptionsColumn.FixedWidth = true;
            this.CHON.Visible = true;
            this.CHON.VisibleIndex = 4;
            this.CHON.Width = 100;
            // 
            // MAHINH
            // 
            this.MAHINH.Caption = "gridColumn1";
            this.MAHINH.FieldName = "MAHINH";
            this.MAHINH.Name = "MAHINH";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.btnbochonvideo);
            this.panelControl3.Controls.Add(this.btnChonTatCaVideo);
            this.panelControl3.Controls.Add(this.btn_XoaVideo);
            this.panelControl3.Controls.Add(this.btn_ThemVideo);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 280);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(551, 39);
            this.panelControl3.TabIndex = 60;
            // 
            // btnbochonvideo
            // 
            this.btnbochonvideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbochonvideo.Appearance.Options.UseFont = true;
            this.btnbochonvideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnbochonvideo.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btnbochonvideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnbochonvideo.Location = new System.Drawing.Point(389, 2);
            this.btnbochonvideo.Margin = new System.Windows.Forms.Padding(0);
            this.btnbochonvideo.Name = "btnbochonvideo";
            this.btnbochonvideo.Size = new System.Drawing.Size(129, 35);
            this.btnbochonvideo.TabIndex = 56;
            this.btnbochonvideo.Text = "Bỏ chọn";
            this.btnbochonvideo.Click += new System.EventHandler(this.btnbochonvideo_Click);
            // 
            // btnChonTatCaVideo
            // 
            this.btnChonTatCaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaVideo.Appearance.Options.UseFont = true;
            this.btnChonTatCaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaVideo.Image")));
            this.btnChonTatCaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaVideo.Location = new System.Drawing.Point(260, 2);
            this.btnChonTatCaVideo.Margin = new System.Windows.Forms.Padding(0);
            this.btnChonTatCaVideo.Name = "btnChonTatCaVideo";
            this.btnChonTatCaVideo.Size = new System.Drawing.Size(129, 35);
            this.btnChonTatCaVideo.TabIndex = 58;
            this.btnChonTatCaVideo.Text = "Chọn tất cả";
            this.btnChonTatCaVideo.Click += new System.EventHandler(this.btnChonTatCaVideo_Click);
            // 
            // btn_XoaVideo
            // 
            this.btn_XoaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_XoaVideo.Appearance.Options.UseFont = true;
            this.btn_XoaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_XoaVideo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_XoaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_XoaVideo.Location = new System.Drawing.Point(131, 2);
            this.btn_XoaVideo.Margin = new System.Windows.Forms.Padding(0);
            this.btn_XoaVideo.Name = "btn_XoaVideo";
            this.btn_XoaVideo.Size = new System.Drawing.Size(129, 35);
            this.btn_XoaVideo.TabIndex = 54;
            this.btn_XoaVideo.Text = "Xóa video";
            this.btn_XoaVideo.Click += new System.EventHandler(this.btn_XoaVideo_Click);
            // 
            // btn_ThemVideo
            // 
            this.btn_ThemVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ThemVideo.Appearance.Options.UseFont = true;
            this.btn_ThemVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThemVideo.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_ThemVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_ThemVideo.Location = new System.Drawing.Point(2, 2);
            this.btn_ThemVideo.Margin = new System.Windows.Forms.Padding(0);
            this.btn_ThemVideo.Name = "btn_ThemVideo";
            this.btn_ThemVideo.Size = new System.Drawing.Size(129, 35);
            this.btn_ThemVideo.TabIndex = 52;
            this.btn_ThemVideo.Text = "Thêm video";
            this.btn_ThemVideo.Click += new System.EventHandler(this.btn_ThemVideo_Click);
            // 
            // pn_CuaHang
            // 
            this.pn_CuaHang.Controls.Add(this.panel_Sandard);
            this.pn_CuaHang.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_CuaHang.Location = new System.Drawing.Point(553, 2);
            this.pn_CuaHang.Name = "pn_CuaHang";
            this.pn_CuaHang.Size = new System.Drawing.Size(712, 317);
            this.pn_CuaHang.TabIndex = 3;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.pnQuangCao);
            this.panel_Sandard.Controls.Add(this.pal_BUTTON);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Sandard.Location = new System.Drawing.Point(2, 2);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(708, 313);
            this.panel_Sandard.TabIndex = 3;
            // 
            // pnQuangCao
            // 
            this.pnQuangCao.Controls.Add(this.labelControl14);
            this.pnQuangCao.Controls.Add(this.labelControl9);
            this.pnQuangCao.Controls.Add(this.labelControl6);
            this.pnQuangCao.Controls.Add(this.labelControl7);
            this.pnQuangCao.Controls.Add(this.cbHour);
            this.pnQuangCao.Controls.Add(this.cbThongBao);
            this.pnQuangCao.Controls.Add(this.cbTenPhim);
            this.pnQuangCao.Controls.Add(this.cbGhiChu);
            this.pnQuangCao.Controls.Add(this.cbShowTime);
            this.pnQuangCao.Controls.Add(this.chkTextNote);
            this.pnQuangCao.Controls.Add(this.labelControl8);
            this.pnQuangCao.Controls.Add(this.labelControl12);
            this.pnQuangCao.Controls.Add(this.labelControl13);
            this.pnQuangCao.Controls.Add(this.colorEditLitleTicket);
            this.pnQuangCao.Controls.Add(this.colorEditNoTicket);
            this.pnQuangCao.Controls.Add(this.colorEditManyTicket);
            this.pnQuangCao.Controls.Add(this.labelControl10);
            this.pnQuangCao.Controls.Add(this.colorEditName);
            this.pnQuangCao.Controls.Add(this.chkRunVideo);
            this.pnQuangCao.Controls.Add(this.colorEditNote);
            this.pnQuangCao.Controls.Add(this.colorEditNameFilm);
            this.pnQuangCao.Controls.Add(this.panelControl4);
            this.pnQuangCao.Controls.Add(this.fontChu);
            this.pnQuangCao.Controls.Add(this.labelControl3);
            this.pnQuangCao.Controls.Add(this.txtLich);
            this.pnQuangCao.Controls.Add(this.labelControl4);
            this.pnQuangCao.Controls.Add(this.txtSLVe);
            this.pnQuangCao.Controls.Add(this.labelControl1);
            this.pnQuangCao.Controls.Add(this.memoNoiDung);
            this.pnQuangCao.Controls.Add(this.labelControl5);
            this.pnQuangCao.Controls.Add(this.txtThoiGianMenu);
            this.pnQuangCao.Controls.Add(this.labelControl2);
            this.pnQuangCao.Controls.Add(this.mTenRap);
            this.pnQuangCao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnQuangCao.Location = new System.Drawing.Point(0, 0);
            this.pnQuangCao.Name = "pnQuangCao";
            this.pnQuangCao.Size = new System.Drawing.Size(708, 278);
            this.pnQuangCao.TabIndex = 17;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(198, 38);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(40, 19);
            this.labelControl14.TabIndex = 71;
            this.labelControl14.Text = "(Ghế)";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(198, 70);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(44, 19);
            this.labelControl9.TabIndex = 70;
            this.labelControl9.Text = "(Phút)";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(198, 102);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(43, 19);
            this.labelControl6.TabIndex = 69;
            this.labelControl6.Text = "(Giây)";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(298, 135);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(66, 19);
            this.labelControl7.TabIndex = 68;
            this.labelControl7.Text = "Giờ chiếu";
            // 
            // cbHour
            // 
            this.cbHour.DropDownHeight = 200;
            this.cbHour.Font = new System.Drawing.Font("Tahoma", 11F);
            this.cbHour.FormattingEnabled = true;
            this.cbHour.IntegralHeight = false;
            this.cbHour.Location = new System.Drawing.Point(379, 132);
            this.cbHour.Name = "cbHour";
            this.cbHour.Size = new System.Drawing.Size(45, 26);
            this.cbHour.TabIndex = 67;
            // 
            // cbThongBao
            // 
            this.cbThongBao.DropDownHeight = 200;
            this.cbThongBao.Font = new System.Drawing.Font("Tahoma", 11F);
            this.cbThongBao.FormattingEnabled = true;
            this.cbThongBao.IntegralHeight = false;
            this.cbThongBao.Location = new System.Drawing.Point(430, 35);
            this.cbThongBao.Name = "cbThongBao";
            this.cbThongBao.Size = new System.Drawing.Size(45, 26);
            this.cbThongBao.TabIndex = 64;
            // 
            // cbTenPhim
            // 
            this.cbTenPhim.DropDownHeight = 200;
            this.cbTenPhim.Font = new System.Drawing.Font("Tahoma", 11F);
            this.cbTenPhim.FormattingEnabled = true;
            this.cbTenPhim.IntegralHeight = false;
            this.cbTenPhim.Location = new System.Drawing.Point(197, 132);
            this.cbTenPhim.Name = "cbTenPhim";
            this.cbTenPhim.Size = new System.Drawing.Size(45, 26);
            this.cbTenPhim.TabIndex = 63;
            // 
            // cbGhiChu
            // 
            this.cbGhiChu.DropDownHeight = 200;
            this.cbGhiChu.Font = new System.Drawing.Font("Tahoma", 11F);
            this.cbGhiChu.FormattingEnabled = true;
            this.cbGhiChu.IntegralHeight = false;
            this.cbGhiChu.Location = new System.Drawing.Point(657, 215);
            this.cbGhiChu.Name = "cbGhiChu";
            this.cbGhiChu.Size = new System.Drawing.Size(45, 26);
            this.cbGhiChu.TabIndex = 62;
            this.cbGhiChu.SelectedIndexChanged += new System.EventHandler(this.cbGhiChu_SelectedIndexChanged);
            // 
            // cbShowTime
            // 
            this.cbShowTime.DropDownHeight = 200;
            this.cbShowTime.Font = new System.Drawing.Font("Tahoma", 11F);
            this.cbShowTime.FormattingEnabled = true;
            this.cbShowTime.IntegralHeight = false;
            this.cbShowTime.ItemHeight = 18;
            this.cbShowTime.Location = new System.Drawing.Point(606, 3);
            this.cbShowTime.Name = "cbShowTime";
            this.cbShowTime.Size = new System.Drawing.Size(45, 26);
            this.cbShowTime.TabIndex = 61;
            this.cbShowTime.SelectedIndexChanged += new System.EventHandler(this.cbShowTime_SelectedIndexChanged);
            // 
            // chkTextNote
            // 
            this.chkTextNote.EditValue = true;
            this.chkTextNote.Location = new System.Drawing.Point(144, 247);
            this.chkTextNote.Name = "chkTextNote";
            this.chkTextNote.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkTextNote.Properties.Appearance.Options.UseFont = true;
            this.chkTextNote.Properties.Caption = "Hiển thị ghi chú";
            this.chkTextNote.Size = new System.Drawing.Size(142, 24);
            this.chkTextNote.TabIndex = 60;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(298, 102);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 19);
            this.labelControl8.TabIndex = 59;
            this.labelControl8.Text = "Hết vé";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(298, 70);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(74, 19);
            this.labelControl12.TabIndex = 58;
            this.labelControl12.Text = "Sắp hết vé";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(298, 38);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(49, 19);
            this.labelControl13.TabIndex = 57;
            this.labelControl13.Text = "Còn vé";
            // 
            // colorEditLitleTicket
            // 
            this.colorEditLitleTicket.EditValue = System.Drawing.Color.Empty;
            this.colorEditLitleTicket.Location = new System.Drawing.Point(379, 67);
            this.colorEditLitleTicket.Name = "colorEditLitleTicket";
            this.colorEditLitleTicket.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditLitleTicket.Properties.Appearance.Options.UseFont = true;
            this.colorEditLitleTicket.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditLitleTicket.Size = new System.Drawing.Size(45, 26);
            this.colorEditLitleTicket.TabIndex = 56;
            // 
            // colorEditNoTicket
            // 
            this.colorEditNoTicket.EditValue = System.Drawing.Color.Empty;
            this.colorEditNoTicket.Location = new System.Drawing.Point(379, 99);
            this.colorEditNoTicket.Name = "colorEditNoTicket";
            this.colorEditNoTicket.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditNoTicket.Properties.Appearance.Options.UseFont = true;
            this.colorEditNoTicket.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditNoTicket.Size = new System.Drawing.Size(45, 26);
            this.colorEditNoTicket.TabIndex = 55;
            // 
            // colorEditManyTicket
            // 
            this.colorEditManyTicket.EditValue = System.Drawing.Color.Empty;
            this.colorEditManyTicket.Location = new System.Drawing.Point(379, 35);
            this.colorEditManyTicket.Name = "colorEditManyTicket";
            this.colorEditManyTicket.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditManyTicket.Properties.Appearance.Options.UseFont = true;
            this.colorEditManyTicket.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditManyTicket.Size = new System.Drawing.Size(45, 26);
            this.colorEditManyTicket.TabIndex = 54;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(3, 136);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(68, 19);
            this.labelControl10.TabIndex = 52;
            this.labelControl10.Text = "Tên phim";
            // 
            // colorEditName
            // 
            this.colorEditName.EditValue = System.Drawing.Color.Empty;
            this.colorEditName.Location = new System.Drawing.Point(146, 132);
            this.colorEditName.Name = "colorEditName";
            this.colorEditName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditName.Properties.Appearance.Options.UseFont = true;
            this.colorEditName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditName.Size = new System.Drawing.Size(45, 26);
            this.colorEditName.TabIndex = 49;
            // 
            // chkRunVideo
            // 
            this.chkRunVideo.EditValue = true;
            this.chkRunVideo.Location = new System.Drawing.Point(292, 247);
            this.chkRunVideo.Name = "chkRunVideo";
            this.chkRunVideo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkRunVideo.Properties.Appearance.Options.UseFont = true;
            this.chkRunVideo.Properties.Caption = "Hiển thị video";
            this.chkRunVideo.Size = new System.Drawing.Size(126, 24);
            this.chkRunVideo.TabIndex = 48;
            // 
            // colorEditNote
            // 
            this.colorEditNote.EditValue = System.Drawing.Color.Empty;
            this.colorEditNote.Location = new System.Drawing.Point(657, 164);
            this.colorEditNote.Name = "colorEditNote";
            this.colorEditNote.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditNote.Properties.Appearance.Options.UseFont = true;
            this.colorEditNote.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditNote.Size = new System.Drawing.Size(45, 26);
            this.colorEditNote.TabIndex = 47;
            this.colorEditNote.EditValueChanged += new System.EventHandler(this.colorEditNote_EditValueChanged);
            // 
            // colorEditNameFilm
            // 
            this.colorEditNameFilm.EditValue = System.Drawing.Color.Empty;
            this.colorEditNameFilm.Location = new System.Drawing.Point(657, 3);
            this.colorEditNameFilm.Name = "colorEditNameFilm";
            this.colorEditNameFilm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.colorEditNameFilm.Properties.Appearance.Options.UseFont = true;
            this.colorEditNameFilm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditNameFilm.Size = new System.Drawing.Size(45, 26);
            this.colorEditNameFilm.TabIndex = 45;
            this.colorEditNameFilm.EditValueChanged += new System.EventHandler(this.colorEditNameFilm_EditValueChanged);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pbBackGround);
            this.panelControl4.Location = new System.Drawing.Point(481, 35);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(221, 123);
            this.panelControl4.TabIndex = 44;
            // 
            // pbBackGround
            // 
            this.pbBackGround.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.pbBackGround.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbBackGround.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbBackGround.ImageLocation = "";
            this.pbBackGround.Location = new System.Drawing.Point(2, 2);
            this.pbBackGround.Name = "pbBackGround";
            this.pbBackGround.Size = new System.Drawing.Size(217, 119);
            this.pbBackGround.TabIndex = 45;
            this.pbBackGround.TabStop = false;
            this.pbBackGround.Click += new System.EventHandler(this.pbBackGround_Click);
            // 
            // fontChu
            // 
            this.fontChu.Location = new System.Drawing.Point(481, 3);
            this.fontChu.Name = "fontChu";
            this.fontChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fontChu.Properties.Appearance.Options.UseFont = true;
            this.fontChu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fontChu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.fontChu.Properties.AutoComplete = false;
            this.fontChu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fontChu.Properties.DropDownRows = 8;
            this.fontChu.Properties.PopupFormMinSize = new System.Drawing.Size(285, 50);
            this.fontChu.Properties.PopupFormSize = new System.Drawing.Size(285, 50);
            this.fontChu.Properties.Popup += new System.EventHandler(this.fontEdit1_Properties_Popup);
            this.fontChu.Size = new System.Drawing.Size(119, 26);
            this.fontChu.TabIndex = 42;
            this.fontChu.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.fontChu_DrawItem);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(3, 5);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(100, 19);
            this.labelControl3.TabIndex = 39;
            this.labelControl3.Text = "Tên showtime";
            // 
            // txtLich
            // 
            this.txtLich.EnterMoveNextControl = true;
            this.txtLich.Location = new System.Drawing.Point(146, 67);
            this.txtLich.Name = "txtLich";
            this.txtLich.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLich.Properties.Appearance.Options.UseFont = true;
            this.txtLich.Properties.Mask.EditMask = "N0";
            this.txtLich.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLich.Size = new System.Drawing.Size(45, 26);
            this.txtLich.TabIndex = 38;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(3, 70);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(119, 19);
            this.labelControl4.TabIndex = 37;
            this.labelControl4.Text = "Thời gian bỏ lịch";
            // 
            // txtSLVe
            // 
            this.txtSLVe.EnterMoveNextControl = true;
            this.txtSLVe.Location = new System.Drawing.Point(146, 35);
            this.txtSLVe.Name = "txtSLVe";
            this.txtSLVe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLVe.Properties.Appearance.Options.UseFont = true;
            this.txtSLVe.Properties.Mask.EditMask = "N0";
            this.txtSLVe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSLVe.Size = new System.Drawing.Size(45, 26);
            this.txtSLVe.TabIndex = 36;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(3, 38);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(126, 19);
            this.labelControl1.TabIndex = 35;
            this.labelControl1.Text = "Số ghế sắp hết vé";
            // 
            // memoNoiDung
            // 
            this.memoNoiDung.Location = new System.Drawing.Point(146, 164);
            this.memoNoiDung.Name = "memoNoiDung";
            this.memoNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoNoiDung.Properties.Appearance.Options.UseFont = true;
            this.memoNoiDung.Size = new System.Drawing.Size(505, 77);
            this.memoNoiDung.TabIndex = 30;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(3, 167);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 19);
            this.labelControl5.TabIndex = 29;
            this.labelControl5.Text = "Ghi chú";
            // 
            // txtThoiGianMenu
            // 
            this.txtThoiGianMenu.EnterMoveNextControl = true;
            this.txtThoiGianMenu.Location = new System.Drawing.Point(146, 99);
            this.txtThoiGianMenu.Name = "txtThoiGianMenu";
            this.txtThoiGianMenu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianMenu.Properties.Appearance.Options.UseFont = true;
            this.txtThoiGianMenu.Properties.Mask.EditMask = "N0";
            this.txtThoiGianMenu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThoiGianMenu.Size = new System.Drawing.Size(45, 26);
            this.txtThoiGianMenu.TabIndex = 26;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(3, 102);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(139, 19);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "Thời gian đổi menu";
            // 
            // mTenRap
            // 
            this.mTenRap.Location = new System.Drawing.Point(146, 3);
            this.mTenRap.Name = "mTenRap";
            this.mTenRap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.mTenRap.Properties.Appearance.Options.UseFont = true;
            this.mTenRap.Size = new System.Drawing.Size(329, 26);
            this.mTenRap.TabIndex = 43;
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btnBanphim);
            this.pal_BUTTON.Controls.Add(this.btnRefesh);
            this.pal_BUTTON.Controls.Add(this.btnHuyBo);
            this.pal_BUTTON.Controls.Add(this.btnEdit);
            this.pal_BUTTON.Controls.Add(this.btnLuu);
            this.pal_BUTTON.Controls.Add(this.panel1);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(0, 278);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(708, 35);
            this.pal_BUTTON.TabIndex = 3;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanphim.Appearance.Options.UseBackColor = true;
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseForeColor = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(622, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(86, 35);
            this.btnBanphim.TabIndex = 32;
            this.btnBanphim.Text = "7";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click_1);
            // 
            // btnRefesh
            // 
            this.btnRefesh.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRefesh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefesh.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnRefesh.Appearance.Options.UseBackColor = true;
            this.btnRefesh.Appearance.Options.UseFont = true;
            this.btnRefesh.Appearance.Options.UseForeColor = true;
            this.btnRefesh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefesh.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnRefesh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRefesh.Location = new System.Drawing.Point(536, 0);
            this.btnRefesh.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefesh.Name = "btnRefesh";
            this.btnRefesh.Size = new System.Drawing.Size(86, 35);
            this.btnRefesh.TabIndex = 33;
            this.btnRefesh.Click += new System.EventHandler(this.btnRefesh_Click_1);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.Appearance.Options.UseFont = true;
            this.btnHuyBo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnHuyBo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuyBo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuyBo.Location = new System.Drawing.Point(407, 0);
            this.btnHuyBo.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(129, 35);
            this.btnHuyBo.TabIndex = 32;
            this.btnHuyBo.Text = "&Hủy";
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEdit.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(278, 0);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(129, 35);
            this.btnEdit.TabIndex = 34;
            this.btnEdit.Text = "&Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(149, 0);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(129, 35);
            this.btnLuu.TabIndex = 31;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(149, 35);
            this.panel1.TabIndex = 35;
            // 
            // Frm_ShowTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 634);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_ShowTime";
            this.Text = "Cài đặt Showtime";
            this.SizeChanged += new System.EventHandler(this.Frm_ShowTime_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).EndInit();
            this.pn_Hinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).EndInit();
            this.pn_xemvideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).EndInit();
            this.pal_xuong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).EndInit();
            this.pn_CaiDat_Video.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).EndInit();
            this.pn_CuaHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnQuangCao)).EndInit();
            this.pnQuangCao.ResumeLayout(false);
            this.pnQuangCao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTextNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLitleTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNoTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditManyTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRunVideo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditNameFilm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbBackGround)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLVe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTenRap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pn_CuaHang;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.PanelControl pal_xuong;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pn_Hinh;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.PanelControl pn_cuon;
        private DevExpress.XtraEditors.PanelControl pn_CaiDat_Video;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btn_XoaVideo;
        private DevExpress.XtraEditors.SimpleButton btn_ThemVideo;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TENHINH;
        private DevExpress.XtraGrid.Columns.GridColumn CHON;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MAHINH;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btn_bochonhinh;
        private DevExpress.XtraEditors.SimpleButton btnbochonvideo;
        private DevExpress.XtraEditors.PanelControl pn_xemvideo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit bnt_Play;
        private AxWMPLib.AxWindowsMediaPlayer WindowsMediaPlayer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_stop;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnRefesh;
        private DevExpress.XtraEditors.PanelControl pnQuangCao;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnHuyBo;
        private DevExpress.XtraEditors.MemoEdit memoNoiDung;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtThoiGianMenu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtSLVe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLich;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaHinh;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaVideo;
        private DevExpress.XtraEditors.FontEdit fontChu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.PictureBox pbBackGround;
        private DevExpress.XtraEditors.ColorEdit colorEditNameFilm;
        private DevExpress.XtraEditors.ColorEdit colorEditNote;
        private DevExpress.XtraEditors.CheckEdit chkRunVideo;
        private DevExpress.XtraEditors.ColorEdit colorEditName;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ColorEdit colorEditLitleTicket;
        private DevExpress.XtraEditors.ColorEdit colorEditNoTicket;
        private DevExpress.XtraEditors.ColorEdit colorEditManyTicket;
        private DevExpress.XtraEditors.CheckEdit chkTextNote;
        private System.Windows.Forms.ComboBox cbTenPhim;
        private System.Windows.Forms.ComboBox cbGhiChu;
        private System.Windows.Forms.ComboBox cbShowTime;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private System.Windows.Forms.ComboBox cbHour;
        private System.Windows.Forms.ComboBox cbThongBao;
        private DevExpress.XtraEditors.TextEdit mTenRap;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.Panel panel1;




    }
}