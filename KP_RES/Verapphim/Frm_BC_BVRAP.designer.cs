﻿namespace KP_RES 
{
    partial class Frm_BC_BVRAP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BC_BVRAP));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.lblData = new DevExpress.XtraEditors.LabelControl();
            this.cboDATA = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MABARCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BATDAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOGHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENPHIM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAIGHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.lblSoducuoibtr = new DevExpress.XtraEditors.LabelControl();
            this.lblSoducuoiht = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lblSoducuoi = new DevExpress.XtraEditors.LabelControl();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblVhtr1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblVhtr = new DevExpress.XtraEditors.LabelControl();
            this.lblThtr = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblVhht = new DevExpress.XtraEditors.LabelControl();
            this.lblThht = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblTongvehuy = new DevExpress.XtraEditors.LabelControl();
            this.lblTongtienhuy = new DevExpress.XtraEditors.LabelControl();
            this.fpnlVehuy = new System.Windows.Forms.FlowLayoutPanel();
            this.fpnlGhehuy = new System.Windows.Forms.FlowLayoutPanel();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblVbtr = new DevExpress.XtraEditors.LabelControl();
            this.lblTbtr = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.fpnlGhebanHT = new System.Windows.Forms.FlowLayoutPanel();
            this.lblVbht = new DevExpress.XtraEditors.LabelControl();
            this.lblTbht = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblTongveban = new DevExpress.XtraEditors.LabelControl();
            this.lblTongtienban = new DevExpress.XtraEditors.LabelControl();
            this.fpnlGheban = new System.Windows.Forms.FlowLayoutPanel();
            this.fpnlVeban = new System.Windows.Forms.FlowLayoutPanel();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnAnformchung = new DevExpress.XtraEditors.SimpleButton();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            this.lblVhtr1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.fpnlGhebanHT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1024, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 668);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(2, 322);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 104);
            this.panelControl4.TabIndex = 16;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 426);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 506);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 242);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 162);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 586);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 82);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 18;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 2);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 0;
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(219, 672);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.optGroup1);
            this.panelControl3.Controls.Add(this.btnTimkiem);
            this.panelControl3.Controls.Add(this.lblData);
            this.panelControl3.Controls.Add(this.cboDATA);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 381);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(215, 254);
            this.panelControl3.TabIndex = 2;
            // 
            // optGroup1
            // 
            this.optGroup1.EditValue = true;
            this.optGroup1.EnterMoveNextControl = true;
            this.optGroup1.Location = new System.Drawing.Point(6, 63);
            this.optGroup1.Name = "optGroup1";
            this.optGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup1.Properties.Appearance.Options.UseFont = true;
            this.optGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Tất cả vé"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Chỉ xem vé bán"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Chỉ xem vé hủy"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Chỉ xem vé bán trước")});
            this.optGroup1.Size = new System.Drawing.Size(211, 132);
            this.optGroup1.TabIndex = 3;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(131, 202);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // lblData
            // 
            this.lblData.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(6, 12);
            this.lblData.Margin = new System.Windows.Forms.Padding(4);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(68, 19);
            this.lblData.TabIndex = 0;
            this.lblData.Text = "Cửa hàng";
            // 
            // cboDATA
            // 
            this.cboDATA.Location = new System.Drawing.Point(6, 35);
            this.cboDATA.Name = "cboDATA";
            this.cboDATA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.Appearance.Options.UseFont = true;
            this.cboDATA.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDATA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDATA.Properties.DropDownRows = 20;
            this.cboDATA.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDATA.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDATA.Size = new System.Drawing.Size(205, 26);
            this.cboDATA.TabIndex = 1;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 129);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(215, 252);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = true;
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Theo rạp"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo phòng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhà phát hành"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo phim"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo loại ghế"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo loại vé"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhân viên")});
            this.optGroup.Size = new System.Drawing.Size(211, 244);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.dtpTungay);
            this.panelControl6.Controls.Add(this.labelControl4);
            this.panelControl6.Controls.Add(this.dtpDenngay);
            this.panelControl6.Controls.Add(this.labelControl1);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(215, 127);
            this.panelControl6.TabIndex = 0;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(6, 32);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(205, 26);
            this.dtpTungay.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 6);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(59, 19);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Từ ngày";
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(6, 91);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(205, 26);
            this.dtpDenngay.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 65);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Đến ngày";
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 635);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(215, 35);
            this.btnThunho.TabIndex = 4;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.gridControl1);
            this.panelControl5.Controls.Add(this.panelControl8);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(219, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1065, 672);
            this.panelControl5.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 702);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1022, 0);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.LOAIVE1,
            this.MABARCODE,
            this.NGAYCHIEU,
            this.BATDAU,
            this.SOGHE,
            this.TENPHONG,
            this.TENPHIM,
            this.TENLOAIGHE,
            this.TENLOAIVE,
            this.TENNHANVIEN,
            this.NGAYTAO,
            this.DONGIA,
            this.FILL});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.LOAIVE1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 60;
            // 
            // LOAIVE1
            // 
            this.LOAIVE1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOAIVE1.AppearanceCell.Options.UseFont = true;
            this.LOAIVE1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOAIVE1.AppearanceHeader.Options.UseFont = true;
            this.LOAIVE1.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIVE1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIVE1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIVE1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LOAIVE1.Caption = "Loại";
            this.LOAIVE1.FieldName = "LOAIVE1";
            this.LOAIVE1.Name = "LOAIVE1";
            this.LOAIVE1.OptionsColumn.AllowEdit = false;
            this.LOAIVE1.OptionsColumn.AllowFocus = false;
            this.LOAIVE1.OptionsColumn.FixedWidth = true;
            this.LOAIVE1.Visible = true;
            this.LOAIVE1.VisibleIndex = 1;
            this.LOAIVE1.Width = 80;
            // 
            // MABARCODE
            // 
            this.MABARCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MABARCODE.AppearanceCell.Options.UseFont = true;
            this.MABARCODE.AppearanceCell.Options.UseTextOptions = true;
            this.MABARCODE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MABARCODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MABARCODE.AppearanceHeader.Options.UseFont = true;
            this.MABARCODE.AppearanceHeader.Options.UseTextOptions = true;
            this.MABARCODE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MABARCODE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MABARCODE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MABARCODE.Caption = "Mã vé";
            this.MABARCODE.FieldName = "MABARCODE";
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.OptionsColumn.AllowEdit = false;
            this.MABARCODE.OptionsColumn.AllowFocus = false;
            this.MABARCODE.OptionsColumn.FixedWidth = true;
            this.MABARCODE.Visible = true;
            this.MABARCODE.VisibleIndex = 1;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCHIEU.AppearanceCell.Options.UseFont = true;
            this.NGAYCHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCHIEU.AppearanceHeader.Options.UseFont = true;
            this.NGAYCHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYCHIEU.Caption = "Ngày";
            this.NGAYCHIEU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYCHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYCHIEU.FieldName = "NGAYCHIEU";
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.OptionsColumn.AllowEdit = false;
            this.NGAYCHIEU.OptionsColumn.AllowFocus = false;
            this.NGAYCHIEU.OptionsColumn.FixedWidth = true;
            this.NGAYCHIEU.Visible = true;
            this.NGAYCHIEU.VisibleIndex = 2;
            this.NGAYCHIEU.Width = 100;
            // 
            // BATDAU
            // 
            this.BATDAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BATDAU.AppearanceCell.Options.UseFont = true;
            this.BATDAU.AppearanceCell.Options.UseTextOptions = true;
            this.BATDAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BATDAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BATDAU.AppearanceHeader.Options.UseFont = true;
            this.BATDAU.AppearanceHeader.Options.UseTextOptions = true;
            this.BATDAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BATDAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BATDAU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.BATDAU.Caption = "Giờ";
            this.BATDAU.FieldName = "BATDAU";
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.OptionsColumn.AllowEdit = false;
            this.BATDAU.OptionsColumn.AllowFocus = false;
            this.BATDAU.OptionsColumn.FixedWidth = true;
            this.BATDAU.Visible = true;
            this.BATDAU.VisibleIndex = 3;
            // 
            // SOGHE
            // 
            this.SOGHE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOGHE.AppearanceCell.Options.UseFont = true;
            this.SOGHE.AppearanceCell.Options.UseTextOptions = true;
            this.SOGHE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOGHE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOGHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOGHE.AppearanceHeader.Options.UseFont = true;
            this.SOGHE.AppearanceHeader.Options.UseTextOptions = true;
            this.SOGHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOGHE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOGHE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOGHE.Caption = "Ghế";
            this.SOGHE.FieldName = "SOGHE";
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.OptionsColumn.AllowEdit = false;
            this.SOGHE.OptionsColumn.AllowFocus = false;
            this.SOGHE.OptionsColumn.FixedWidth = true;
            this.SOGHE.Visible = true;
            this.SOGHE.VisibleIndex = 4;
            // 
            // TENPHONG
            // 
            this.TENPHONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHONG.AppearanceCell.Options.UseFont = true;
            this.TENPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.TENPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHONG.AppearanceHeader.Options.UseFont = true;
            this.TENPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TENPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENPHONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENPHONG.Caption = "Phòng";
            this.TENPHONG.FieldName = "TENPHONG";
            this.TENPHONG.Name = "TENPHONG";
            this.TENPHONG.OptionsColumn.AllowEdit = false;
            this.TENPHONG.OptionsColumn.AllowFocus = false;
            this.TENPHONG.OptionsColumn.FixedWidth = true;
            this.TENPHONG.Visible = true;
            this.TENPHONG.VisibleIndex = 5;
            // 
            // TENPHIM
            // 
            this.TENPHIM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHIM.AppearanceCell.Options.UseFont = true;
            this.TENPHIM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHIM.AppearanceHeader.Options.UseFont = true;
            this.TENPHIM.AppearanceHeader.Options.UseTextOptions = true;
            this.TENPHIM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHIM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENPHIM.Caption = "Phim";
            this.TENPHIM.FieldName = "TENPHIM";
            this.TENPHIM.Name = "TENPHIM";
            this.TENPHIM.OptionsColumn.AllowEdit = false;
            this.TENPHIM.OptionsColumn.AllowFocus = false;
            this.TENPHIM.OptionsColumn.FixedWidth = true;
            this.TENPHIM.Visible = true;
            this.TENPHIM.VisibleIndex = 6;
            this.TENPHIM.Width = 200;
            // 
            // TENLOAIGHE
            // 
            this.TENLOAIGHE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENLOAIGHE.AppearanceCell.Options.UseFont = true;
            this.TENLOAIGHE.AppearanceCell.Options.UseTextOptions = true;
            this.TENLOAIGHE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENLOAIGHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENLOAIGHE.AppearanceHeader.Options.UseFont = true;
            this.TENLOAIGHE.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAIGHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAIGHE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENLOAIGHE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENLOAIGHE.Caption = "Loại ghế";
            this.TENLOAIGHE.FieldName = "TENLOAIGHE";
            this.TENLOAIGHE.Name = "TENLOAIGHE";
            this.TENLOAIGHE.OptionsColumn.AllowEdit = false;
            this.TENLOAIGHE.OptionsColumn.AllowFocus = false;
            this.TENLOAIGHE.OptionsColumn.FixedWidth = true;
            this.TENLOAIGHE.Visible = true;
            this.TENLOAIGHE.VisibleIndex = 7;
            this.TENLOAIGHE.Width = 100;
            // 
            // TENLOAIVE
            // 
            this.TENLOAIVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENLOAIVE.AppearanceCell.Options.UseFont = true;
            this.TENLOAIVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAIVE.AppearanceHeader.Options.UseFont = true;
            this.TENLOAIVE.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAIVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAIVE.Caption = "Loại vé";
            this.TENLOAIVE.FieldName = "TENLOAIVE";
            this.TENLOAIVE.Name = "TENLOAIVE";
            this.TENLOAIVE.OptionsColumn.AllowEdit = false;
            this.TENLOAIVE.OptionsColumn.AllowFocus = false;
            this.TENLOAIVE.OptionsColumn.FixedWidth = true;
            this.TENLOAIVE.Visible = true;
            this.TENLOAIVE.VisibleIndex = 8;
            this.TENLOAIVE.Width = 100;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENNHANVIEN.AppearanceCell.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Nhân viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 9;
            this.TENNHANVIEN.Width = 150;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.NGAYTAO.AppearanceCell.Options.UseFont = true;
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày in";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 10;
            this.NGAYTAO.Width = 150;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DONGIA.AppearanceCell.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.Caption = "Giá vé";
            this.DONGIA.DisplayFormat.FormatString = "N0";
            this.DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowEdit = false;
            this.DONGIA.OptionsColumn.AllowFocus = false;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 11;
            this.DONGIA.Width = 80;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 12;
            this.FILL.Width = 20;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl10);
            this.panelControl8.Controls.Add(this.panelControl11);
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Controls.Add(this.panelControl12);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1022, 700);
            this.panelControl8.TabIndex = 4;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.lblSoducuoibtr);
            this.panelControl10.Controls.Add(this.lblSoducuoiht);
            this.panelControl10.Controls.Add(this.labelControl5);
            this.panelControl10.Controls.Add(this.lblSoducuoi);
            this.panelControl10.Controls.Add(this.shapeContainer3);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl10.Location = new System.Drawing.Point(878, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(142, 661);
            this.panelControl10.TabIndex = 11;
            // 
            // lblSoducuoibtr
            // 
            this.lblSoducuoibtr.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoducuoibtr.Location = new System.Drawing.Point(7, 359);
            this.lblSoducuoibtr.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoducuoibtr.Name = "lblSoducuoibtr";
            this.lblSoducuoibtr.Size = new System.Drawing.Size(90, 19);
            this.lblSoducuoibtr.TabIndex = 14;
            this.lblSoducuoibtr.Text = "Bán trước: ";
            // 
            // lblSoducuoiht
            // 
            this.lblSoducuoiht.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoducuoiht.Location = new System.Drawing.Point(7, 332);
            this.lblSoducuoiht.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoducuoiht.Name = "lblSoducuoiht";
            this.lblSoducuoiht.Size = new System.Drawing.Size(75, 19);
            this.lblSoducuoiht.TabIndex = 13;
            this.lblSoducuoiht.Text = "Hiện tại: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(81, 9);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(99, 19);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "SỐ DƯ CUỐI";
            // 
            // lblSoducuoi
            // 
            this.lblSoducuoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoducuoi.Location = new System.Drawing.Point(7, 77);
            this.lblSoducuoi.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoducuoi.Name = "lblSoducuoi";
            this.lblSoducuoi.Size = new System.Drawing.Size(100, 19);
            this.lblSoducuoi.TabIndex = 9;
            this.lblSoducuoi.Text = "Số dư cuối : ";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(2, 2);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer3.Size = new System.Drawing.Size(138, 657);
            this.shapeContainer3.TabIndex = 12;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = -5;
            this.lineShape2.X2 = 442;
            this.lineShape2.Y1 = 35;
            this.lineShape2.Y2 = 35;
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.labelControl13);
            this.panelControl11.Controls.Add(this.lblVhtr1);
            this.panelControl11.Controls.Add(this.labelControl14);
            this.panelControl11.Controls.Add(this.flowLayoutPanel4);
            this.panelControl11.Controls.Add(this.labelControl10);
            this.panelControl11.Controls.Add(this.labelControl9);
            this.panelControl11.Controls.Add(this.labelControl3);
            this.panelControl11.Controls.Add(this.lblTongvehuy);
            this.panelControl11.Controls.Add(this.lblTongtienhuy);
            this.panelControl11.Controls.Add(this.fpnlVehuy);
            this.panelControl11.Controls.Add(this.fpnlGhehuy);
            this.panelControl11.Controls.Add(this.shapeContainer2);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl11.Location = new System.Drawing.Point(440, 2);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(438, 661);
            this.panelControl11.TabIndex = 11;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(230, 332);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(115, 19);
            this.labelControl13.TabIndex = 16;
            this.labelControl13.Text = "Vé bán trước: ";
            // 
            // lblVhtr1
            // 
            this.lblVhtr1.AutoScroll = true;
            this.lblVhtr1.Controls.Add(this.lblVhtr);
            this.lblVhtr1.Controls.Add(this.lblThtr);
            this.lblVhtr1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.lblVhtr1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhtr1.Location = new System.Drawing.Point(230, 358);
            this.lblVhtr1.Name = "lblVhtr1";
            this.lblVhtr1.Size = new System.Drawing.Size(203, 81);
            this.lblVhtr1.TabIndex = 15;
            // 
            // lblVhtr
            // 
            this.lblVhtr.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhtr.Location = new System.Drawing.Point(4, 4);
            this.lblVhtr.Margin = new System.Windows.Forms.Padding(4);
            this.lblVhtr.Name = "lblVhtr";
            this.lblVhtr.Size = new System.Drawing.Size(0, 16);
            this.lblVhtr.TabIndex = 12;
            // 
            // lblThtr
            // 
            this.lblThtr.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThtr.Location = new System.Drawing.Point(4, 28);
            this.lblThtr.Margin = new System.Windows.Forms.Padding(4);
            this.lblThtr.Name = "lblThtr";
            this.lblThtr.Size = new System.Drawing.Size(0, 16);
            this.lblThtr.TabIndex = 13;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(6, 332);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 19);
            this.labelControl14.TabIndex = 14;
            this.labelControl14.Text = "Vé hiện tại: ";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.AutoScroll = true;
            this.flowLayoutPanel4.Controls.Add(this.lblVhht);
            this.flowLayoutPanel4.Controls.Add(this.lblThht);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel4.Location = new System.Drawing.Point(6, 358);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(203, 81);
            this.flowLayoutPanel4.TabIndex = 13;
            // 
            // lblVhht
            // 
            this.lblVhht.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhht.Location = new System.Drawing.Point(4, 4);
            this.lblVhht.Margin = new System.Windows.Forms.Padding(4);
            this.lblVhht.Name = "lblVhht";
            this.lblVhht.Size = new System.Drawing.Size(0, 16);
            this.lblVhht.TabIndex = 12;
            // 
            // lblThht
            // 
            this.lblThht.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThht.Location = new System.Drawing.Point(4, 28);
            this.lblThht.Margin = new System.Windows.Forms.Padding(4);
            this.lblThht.Name = "lblThht";
            this.lblThht.Size = new System.Drawing.Size(0, 16);
            this.lblThht.TabIndex = 13;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(173, 123);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(69, 19);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "Loại vé: ";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(6, 123);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(80, 19);
            this.labelControl9.TabIndex = 11;
            this.labelControl9.Text = "Loại ghế: ";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(176, 9);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(61, 19);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "VÉ HỦY";
            // 
            // lblTongvehuy
            // 
            this.lblTongvehuy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongvehuy.Location = new System.Drawing.Point(6, 50);
            this.lblTongvehuy.Margin = new System.Windows.Forms.Padding(4);
            this.lblTongvehuy.Name = "lblTongvehuy";
            this.lblTongvehuy.Size = new System.Drawing.Size(115, 19);
            this.lblTongvehuy.TabIndex = 5;
            this.lblTongvehuy.Text = "Tổng vé hủy : ";
            // 
            // lblTongtienhuy
            // 
            this.lblTongtienhuy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongtienhuy.Location = new System.Drawing.Point(6, 77);
            this.lblTongtienhuy.Margin = new System.Windows.Forms.Padding(4);
            this.lblTongtienhuy.Name = "lblTongtienhuy";
            this.lblTongtienhuy.Size = new System.Drawing.Size(128, 19);
            this.lblTongtienhuy.TabIndex = 6;
            this.lblTongtienhuy.Text = "Tổng tiền hủy : ";
            // 
            // fpnlVehuy
            // 
            this.fpnlVehuy.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.fpnlVehuy.Location = new System.Drawing.Point(173, 149);
            this.fpnlVehuy.Name = "fpnlVehuy";
            this.fpnlVehuy.Size = new System.Drawing.Size(260, 176);
            this.fpnlVehuy.TabIndex = 8;
            // 
            // fpnlGhehuy
            // 
            this.fpnlGhehuy.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.fpnlGhehuy.Location = new System.Drawing.Point(6, 149);
            this.fpnlGhehuy.Name = "fpnlGhehuy";
            this.fpnlGhehuy.Size = new System.Drawing.Size(157, 176);
            this.fpnlGhehuy.TabIndex = 7;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(2, 2);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(434, 657);
            this.shapeContainer2.TabIndex = 9;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = -5;
            this.lineShape3.X2 = 442;
            this.lineShape3.Y1 = 35;
            this.lineShape3.Y2 = 35;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl12);
            this.panelControl9.Controls.Add(this.flowLayoutPanel2);
            this.panelControl9.Controls.Add(this.labelControl11);
            this.panelControl9.Controls.Add(this.fpnlGhebanHT);
            this.panelControl9.Controls.Add(this.labelControl8);
            this.panelControl9.Controls.Add(this.labelControl7);
            this.panelControl9.Controls.Add(this.labelControl2);
            this.panelControl9.Controls.Add(this.lblTongveban);
            this.panelControl9.Controls.Add(this.lblTongtienban);
            this.panelControl9.Controls.Add(this.fpnlGheban);
            this.panelControl9.Controls.Add(this.fpnlVeban);
            this.panelControl9.Controls.Add(this.shapeContainer1);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(438, 661);
            this.panelControl9.TabIndex = 10;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(230, 332);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(115, 19);
            this.labelControl12.TabIndex = 12;
            this.labelControl12.Text = "Vé bán trước: ";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoScroll = true;
            this.flowLayoutPanel2.Controls.Add(this.lblVbtr);
            this.flowLayoutPanel2.Controls.Add(this.lblTbtr);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel2.Location = new System.Drawing.Point(230, 357);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(203, 81);
            this.flowLayoutPanel2.TabIndex = 11;
            // 
            // lblVbtr
            // 
            this.lblVbtr.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVbtr.Location = new System.Drawing.Point(4, 4);
            this.lblVbtr.Margin = new System.Windows.Forms.Padding(4);
            this.lblVbtr.Name = "lblVbtr";
            this.lblVbtr.Size = new System.Drawing.Size(0, 16);
            this.lblVbtr.TabIndex = 10;
            // 
            // lblTbtr
            // 
            this.lblTbtr.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTbtr.Location = new System.Drawing.Point(4, 28);
            this.lblTbtr.Margin = new System.Windows.Forms.Padding(4);
            this.lblTbtr.Name = "lblTbtr";
            this.lblTbtr.Size = new System.Drawing.Size(0, 16);
            this.lblTbtr.TabIndex = 11;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(6, 332);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(99, 19);
            this.labelControl11.TabIndex = 10;
            this.labelControl11.Text = "Vé hiện tại: ";
            // 
            // fpnlGhebanHT
            // 
            this.fpnlGhebanHT.AutoScroll = true;
            this.fpnlGhebanHT.Controls.Add(this.lblVbht);
            this.fpnlGhebanHT.Controls.Add(this.lblTbht);
            this.fpnlGhebanHT.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.fpnlGhebanHT.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlGhebanHT.Location = new System.Drawing.Point(6, 358);
            this.fpnlGhebanHT.Name = "fpnlGhebanHT";
            this.fpnlGhebanHT.Size = new System.Drawing.Size(203, 81);
            this.fpnlGhebanHT.TabIndex = 9;
            // 
            // lblVbht
            // 
            this.lblVbht.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVbht.Location = new System.Drawing.Point(4, 4);
            this.lblVbht.Margin = new System.Windows.Forms.Padding(4);
            this.lblVbht.Name = "lblVbht";
            this.lblVbht.Size = new System.Drawing.Size(0, 16);
            this.lblVbht.TabIndex = 8;
            // 
            // lblTbht
            // 
            this.lblTbht.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTbht.Location = new System.Drawing.Point(4, 28);
            this.lblTbht.Margin = new System.Windows.Forms.Padding(4);
            this.lblTbht.Name = "lblTbht";
            this.lblTbht.Size = new System.Drawing.Size(0, 16);
            this.lblTbht.TabIndex = 9;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(173, 123);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(69, 19);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "Loại vé: ";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(6, 123);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(80, 19);
            this.labelControl7.TabIndex = 7;
            this.labelControl7.Text = "Loại ghế: ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(173, 9);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(61, 19);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "VÉ BÁN";
            // 
            // lblTongveban
            // 
            this.lblTongveban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongveban.Location = new System.Drawing.Point(6, 50);
            this.lblTongveban.Margin = new System.Windows.Forms.Padding(4);
            this.lblTongveban.Name = "lblTongveban";
            this.lblTongveban.Size = new System.Drawing.Size(116, 19);
            this.lblTongveban.TabIndex = 1;
            this.lblTongveban.Text = "Tổng vé bán : ";
            // 
            // lblTongtienban
            // 
            this.lblTongtienban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongtienban.Location = new System.Drawing.Point(6, 77);
            this.lblTongtienban.Margin = new System.Windows.Forms.Padding(4);
            this.lblTongtienban.Name = "lblTongtienban";
            this.lblTongtienban.Size = new System.Drawing.Size(129, 19);
            this.lblTongtienban.TabIndex = 2;
            this.lblTongtienban.Text = "Tổng tiền bán : ";
            // 
            // fpnlGheban
            // 
            this.fpnlGheban.AutoScroll = true;
            this.fpnlGheban.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.fpnlGheban.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlGheban.Location = new System.Drawing.Point(6, 149);
            this.fpnlGheban.Name = "fpnlGheban";
            this.fpnlGheban.Size = new System.Drawing.Size(157, 176);
            this.fpnlGheban.TabIndex = 3;
            // 
            // fpnlVeban
            // 
            this.fpnlVeban.AutoScroll = true;
            this.fpnlVeban.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.fpnlVeban.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlVeban.Location = new System.Drawing.Point(173, 149);
            this.fpnlVeban.Name = "fpnlVeban";
            this.fpnlVeban.Size = new System.Drawing.Size(260, 176);
            this.fpnlVeban.TabIndex = 4;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(2, 2);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(434, 657);
            this.shapeContainer1.TabIndex = 5;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = -3;
            this.lineShape1.X2 = 434;
            this.lineShape1.Y1 = 35;
            this.lineShape1.Y2 = 35;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.btnAnformchung);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl12.Location = new System.Drawing.Point(2, 663);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(1018, 35);
            this.panelControl12.TabIndex = 12;
            // 
            // btnAnformchung
            // 
            this.btnAnformchung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnformchung.Appearance.Options.UseFont = true;
            this.btnAnformchung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAnformchung.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnAnformchung.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAnformchung.Location = new System.Drawing.Point(2, 2);
            this.btnAnformchung.Margin = new System.Windows.Forms.Padding(4);
            this.btnAnformchung.Name = "btnAnformchung";
            this.btnAnformchung.Size = new System.Drawing.Size(1014, 31);
            this.btnAnformchung.TabIndex = 14;
            this.btnAnformchung.Click += new System.EventHandler(this.btnAnformchung_Click);
            // 
            // Frm_BC_BVRAP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 672);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_BC_BVRAP";
            this.Text = "Bán vé";
            this.Load += new System.EventHandler(this.Frm_BC_BVRAP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            this.lblVhtr1.ResumeLayout(false);
            this.lblVhtr1.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.fpnlGhebanHT.ResumeLayout(false);
            this.fpnlGhebanHT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn BATDAU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn MABARCODE;
        private DevExpress.XtraGrid.Columns.GridColumn TENPHIM;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAIGHE;
        private DevExpress.XtraGrid.Columns.GridColumn SOGHE;
        private DevExpress.XtraGrid.Columns.GridColumn TENPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVE1;
        private DevExpress.XtraEditors.LabelControl lblData;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.RadioGroup optGroup1;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAIVE;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.FlowLayoutPanel fpnlVeban;
        private System.Windows.Forms.FlowLayoutPanel fpnlGheban;
        private DevExpress.XtraEditors.LabelControl lblTongtienban;
        private DevExpress.XtraEditors.LabelControl lblTongveban;
        private System.Windows.Forms.FlowLayoutPanel fpnlVehuy;
        private System.Windows.Forms.FlowLayoutPanel fpnlGhehuy;
        private DevExpress.XtraEditors.LabelControl lblTongtienhuy;
        private DevExpress.XtraEditors.LabelControl lblTongvehuy;
        private DevExpress.XtraEditors.LabelControl lblSoducuoi;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnAnformchung;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private System.Windows.Forms.FlowLayoutPanel lblVhtr1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private System.Windows.Forms.FlowLayoutPanel fpnlGhebanHT;
        private DevExpress.XtraEditors.LabelControl lblVbht;
        private DevExpress.XtraEditors.LabelControl lblTbht;
        private DevExpress.XtraEditors.LabelControl lblVbtr;
        private DevExpress.XtraEditors.LabelControl lblTbtr;
        private DevExpress.XtraEditors.LabelControl lblVhtr;
        private DevExpress.XtraEditors.LabelControl lblThtr;
        private DevExpress.XtraEditors.LabelControl lblVhht;
        private DevExpress.XtraEditors.LabelControl lblThht;
        private DevExpress.XtraEditors.LabelControl lblSoducuoibtr;
        private DevExpress.XtraEditors.LabelControl lblSoducuoiht;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboDATA;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;




    }
}