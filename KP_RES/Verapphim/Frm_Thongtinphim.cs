﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Thongtinphim : DevExpress.XtraEditors.XtraForm
    {

        string grid = "";
        public Frm_Thongtinphim()
        {
            InitializeComponent();
            LoaddataGridView_Nhaphathanh();
            LoaddataGridView_CLP();
            LoaddataGridView_TLP();
            LoadPermission();
        }

        private void Frm_Nhaphathanh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtTENNPH.SelectAll();
        }

        private void Frm_Nhaphathanh_Load(object sender, EventArgs e)
        {
            pnlDinhdang.Width = pnlNPH.Width = pnlTheloai.Width = (int)(this.Width - panelControl2.Width) / 3;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            if (grid == "gridView2")
            {
                try
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                catch
                {
                }
            }
            if (grid == "gridView3")
            {
                try
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                catch
                {
                }
            }
            if (grid == "gridView1")
            {
                try
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                catch
                {
                }
            }

        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            if (grid == "gridView2")
            {
                try
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
            if (grid == "gridView3")
            {
                try
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView3.FocusedRowHandle == gridView3.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
            if (grid == "gridView1")
            {
                try
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            if (grid == "gridView2")
            {
                try
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
            if (grid == "gridView3")
            {
                try
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView3.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
            if (grid == "gridView1")
            {
                try
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                catch
                {
                }
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            if (grid == "gridView2")
            {
                try
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                catch
                {
                }
            }
            if (grid == "gridView3")
            {
                try
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                catch
                {
                }
            }
            if (grid == "gridView1")
            {
                try
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                catch
                {
                }
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView_Nhaphathanh();
            LoaddataGridView_CLP();
            LoaddataGridView_TLP();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void LoadPermission()
        {
            btnThemNPH.Enabled = clsUserManagement.AllowAdd("74");
            btnSuaNPH.Enabled = clsUserManagement.AllowEdit("74");
            btnXoaNPH.Enabled = clsUserManagement.AllowDelete("74");
            btnLuuNPH.Enabled = btnThemNPH.Enabled || btnSuaNPH.Enabled;

            btnThemCLP.Enabled = clsUserManagement.AllowAdd("74");
            btnSuaCLP.Enabled = clsUserManagement.AllowEdit("74");
            btnXoaCLP.Enabled = clsUserManagement.AllowDelete("74");
            btnLuuCLP.Enabled = btnThemCLP.Enabled || btnSuaCLP.Enabled;

            btnThemTLP.Enabled = clsUserManagement.AllowAdd("74");
            btnSuaTLP.Enabled = clsUserManagement.AllowEdit("74");
            btnXoaTLP.Enabled = clsUserManagement.AllowDelete("74");
            btnLuuTLP.Enabled = btnThemTLP.Enabled || btnSuaTLP.Enabled;
        }

        #region Nhà phát hành
        string smaNhaphathanh = "";

        private void LoaddataGridView_Nhaphathanh()
        {
            SetRongControl_NPH();
            KhoaMoControl_NPH(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MANHAPHATHANH As MA,TENNHAPHATHANH As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DM_NHAPHATHANH" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            grid = "gridView2";
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;
            smaNhaphathanh = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTENNPH.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtGHICHUNPH.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNGNPH.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl_NPH(true);
        }

        private void btnThemNPH_Click(object sender, EventArgs e)
        {
            SetRongControl_NPH();
            KhoaMoControl_NPH(false);
            txtTENNPH.Focus();
        }

        private void btnXoaNPH_Click(object sender, EventArgs e)
        {
            if (smaNhaphathanh == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DM_NHAPHATHANH" + "\n";
            sSQL += "Where MANHAPHATHANH=" + clsMain.SQLString(smaNhaphathanh) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_Nhaphathanh();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSuaNPH_Click(object sender, EventArgs e)
        {
            if (smaNhaphathanh == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_NPH(false);
            txtTENNPH.Focus();
        }

        private void btnLuuNPH_Click(object sender, EventArgs e)
        {
            if (!CheckInput_NPH())
            {
                return;
            }
            string sSQL = "";
            if (smaNhaphathanh == "" && btnThemNPH.Enabled)
            {
                sSQL += "Insert into DM_NHAPHATHANH (TENNHAPHATHANH,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENNPH.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHUNPH.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGNPH.Checked) + ")";
            }
            else if ((smaNhaphathanh != "" && btnSuaNPH.Enabled))
            {
                sSQL += "Update DM_NHAPHATHANH Set " + "\n";
                sSQL += "TENNHAPHATHANH=" + clsMain.SQLStringUnicode(txtTENNPH.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUNPH.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGNPH.Checked) + "\n";
                sSQL += "Where MANHAPHATHANH=" + clsMain.SQLString(smaNhaphathanh) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_Nhaphathanh();
                btnThemNPH.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControl_NPH(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENNPH.Properties.ReadOnly = true;
                txtGHICHUNPH.Properties.ReadOnly = true;
                chkSUDUNGNPH.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENNPH.Properties.ReadOnly = false;
                txtGHICHUNPH.Properties.ReadOnly = false;
                chkSUDUNGNPH.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl_NPH()
        {
            smaNhaphathanh = "";
            txtTENNPH.Text = "";
            txtGHICHUNPH.Text = "";
            chkSUDUNGNPH.Checked = true;
        }

        private Boolean CheckInput_NPH()
        {
            if (txtTENNPH.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENNPH.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Dịnh dạng phim
        string smaChatluongphim = "";
        private void LoaddataGridView_CLP()
        {
            SetRongControl_CLP();
            KhoaMoControl_CLP(true);
            gridView1.Focus();

            string sSQL = "";
            sSQL += "Select MACHATLUONGPHIM As MA,TENCHATLUONGPHIM As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DM_CHATLUONGPHIM" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl1.DataSource = dt;
            gridView1.ActiveFilterString = "[SUDUNG] = True";
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            grid = "gridView1";
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
            smaChatluongphim = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            txtTENCLP.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TEN").ToString();
            txtGHICHUCLP.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNGCLP.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl_CLP(true);
        }

        private void btnThemCLP_Click(object sender, EventArgs e)
        {
            SetRongControl_CLP();
            KhoaMoControl_CLP(false);
            txtTENCLP.Focus();
        }

        private void btnXoaCLP_Click(object sender, EventArgs e)
        {
            if (smaChatluongphim == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DM_CHATLUONGPHIM" + "\n";
            sSQL += "Where MACHATLUONGPHIM=" + clsMain.SQLString(smaChatluongphim) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_CLP();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSuaCLP_Click(object sender, EventArgs e)
        {
            if (smaChatluongphim == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_CLP(false);
            txtTENCLP.Focus();
        }

        private void btnLuuCLP_Click(object sender, EventArgs e)
        {
            if (!CheckInput_CLP())
            {
                return;
            }
            string sSQL = "";
            if (smaChatluongphim == "" && btnThemCLP.Enabled)
            {
                sSQL += "Insert into DM_CHATLUONGPHIM (TENCHATLUONGPHIM,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENCLP.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHUCLP.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGCLP.Checked) + ")";
            }
            else if ((smaChatluongphim != "" && btnSuaCLP.Enabled))
            {
                sSQL += "Update DM_CHATLUONGPHIM Set " + "\n";
                sSQL += "TENCHATLUONGPHIM=" + clsMain.SQLStringUnicode(txtTENCLP.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUCLP.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGCLP.Checked) + "\n";
                sSQL += "Where MACHATLUONGPHIM=" + clsMain.SQLString(smaChatluongphim) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_CLP();
                btnThemCLP.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControl_CLP(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENCLP.Properties.ReadOnly = true;
                txtGHICHUCLP.Properties.ReadOnly = true;
                chkSUDUNGCLP.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENCLP.Properties.ReadOnly = false;
                txtGHICHUCLP.Properties.ReadOnly = false;
                chkSUDUNGCLP.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl_CLP()
        {
            smaChatluongphim = "";
            txtTENCLP.Text = "";
            txtGHICHUCLP.Text = "";
            chkSUDUNGCLP.Checked = true;
        }

        private Boolean CheckInput_CLP()
        {
            if (txtTENCLP.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENCLP.Focus();
                return false;
            }
            return true;
        }
        #endregion

        #region Thể Loại phim
        string sma_TLP = "";
        private void LoaddataGridView_TLP()
        {
            SetRongControl_TLP();
            KhoaMoControl_TLP(true);
            gridView3.Focus();

            string sSQL = "";
            sSQL += "Select MATHELOAIPHIM As MA,TENTHELOAIPHIM As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DM_THELOAIPHIM" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl3.DataSource = dt;
            gridView3.ActiveFilterString = "[SUDUNG] = True";
        }

        private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT2 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView3_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            grid = "gridView3";
            if (gridView3.RowCount == 0 || gridView3.FocusedRowHandle < 0)
                return;
            sma_TLP = gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "MA").ToString();
            txtTENTLP.Text = gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "TEN").ToString();
            txtGHICHUTLP.Text = gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNGTLP.Checked = bool.Parse(gridView3.GetRowCellValue(gridView3.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl_TLP(true);
        }

        private void btnThem_TLP_Click(object sender, EventArgs e)
        {
            SetRongControl_TLP();
            KhoaMoControl_TLP(false);
            txtTENTLP.Focus();
        }

        private void btnXoa_TLP_Click(object sender, EventArgs e)
        {
            if (sma_TLP == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DM_THELOAIPHIM" + "\n";
            sSQL += "Where MATHELOAIPHIM=" + clsMain.SQLString(sma_TLP) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_TLP();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_TLP_Click(object sender, EventArgs e)
        {
            if (sma_TLP == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl_TLP(false);
            txtTENTLP.Focus();
        }

        private void btnLuu_TLP_Click(object sender, EventArgs e)
        {
            if (!CheckInput_TLP())
            {
                return;
            }
            string sSQL = "";
            if (sma_TLP == "" && btnThemTLP.Enabled)
            {
                sSQL += "Insert into DM_THELOAIPHIM (TENTHELOAIPHIM,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENTLP.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHUTLP.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGTLP.Checked) + ")";
            }
            else if ((sma_TLP != "" && btnSuaTLP.Enabled))
            {
                sSQL += "Update DM_THELOAIPHIM Set " + "\n";
                sSQL += "TENTHELOAIPHIM=" + clsMain.SQLStringUnicode(txtTENTLP.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUTLP.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGTLP.Checked) + "\n";
                sSQL += "Where MATHELOAIPHIM=" + clsMain.SQLString(sma_TLP) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView_TLP();
                btnThemTLP.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KhoaMoControl_TLP(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENTLP.Properties.ReadOnly = true;
                txtGHICHUTLP.Properties.ReadOnly = true;
                chkSUDUNGTLP.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENTLP.Properties.ReadOnly = false;
                txtGHICHUTLP.Properties.ReadOnly = false;
                chkSUDUNGTLP.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl_TLP()
        {
            sma_TLP = "";
            txtTENTLP.Text = "";
            txtGHICHUTLP.Text = "";
            chkSUDUNGTLP.Checked = true;
        }

        private Boolean CheckInput_TLP()
        {
            if (txtTENTLP.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENTLP.Focus();
                return false;
            }
            return true;
        }
        #endregion
    }
}