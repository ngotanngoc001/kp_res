﻿namespace KP_RES
{
    partial class Frm_RPBanve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_RPBanve));
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.lblCaban = new DevExpress.XtraEditors.LabelControl();
            this.lblQuay = new DevExpress.XtraEditors.LabelControl();
            this.lblNhanvien = new DevExpress.XtraEditors.LabelControl();
            this.lblThongtinxuatchieu = new DevExpress.XtraEditors.LabelControl();
            this.lblThongtinxuatchieu1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.lblTenRap = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pnlShowTime = new DevExpress.XtraEditors.PanelControl();
            this.pnlPhim = new DevExpress.XtraEditors.PanelControl();
            this.fpnlPhim = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlLenxuong = new DevExpress.XtraEditors.PanelControl();
            this.btnXuong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLen = new DevExpress.XtraEditors.SimpleButton();
            this.pnlNgay = new DevExpress.XtraEditors.PanelControl();
            this.fpnlNgay = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlSodoghe = new DevExpress.XtraEditors.PanelControl();
            this.pnlSodo = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboLoaive = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLenve = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbNgay = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTongtien = new DevExpress.XtraEditors.TextEdit();
            this.txtTongghe = new DevExpress.XtraEditors.TextEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnXoave = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongve = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonnhieughe = new DevExpress.XtraEditors.SimpleButton();
            this.btnKetca = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoket = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonlaighe = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanve = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonSuatChieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvedattruoc = new DevExpress.XtraEditors.SimpleButton();
            this.btnInlaive = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyve = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.timerLoadSoDoGheTuDong = new System.Windows.Forms.Timer(this.components);
            this.timerDongho = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowTime)).BeginInit();
            this.pnlShowTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhim)).BeginInit();
            this.pnlPhim.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLenxuong)).BeginInit();
            this.pnlLenxuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNgay)).BeginInit();
            this.pnlNgay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSodoghe)).BeginInit();
            this.pnlSodoghe.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaive.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Giao diện";
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(798, 2);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 31);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.pictureEdit1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1036, 43);
            this.panelControl2.TabIndex = 44;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.lblCaban);
            this.panelControl4.Controls.Add(this.lblQuay);
            this.panelControl4.Controls.Add(this.lblNhanvien);
            this.panelControl4.Controls.Add(this.lblThongtinxuatchieu);
            this.panelControl4.Controls.Add(this.lblThongtinxuatchieu1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(181, 2);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.panelControl4.Size = new System.Drawing.Size(650, 39);
            this.panelControl4.TabIndex = 53;
            // 
            // lblCaban
            // 
            this.lblCaban.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaban.Location = new System.Drawing.Point(518, 19);
            this.lblCaban.Margin = new System.Windows.Forms.Padding(4);
            this.lblCaban.Name = "lblCaban";
            this.lblCaban.Size = new System.Drawing.Size(53, 16);
            this.lblCaban.TabIndex = 57;
            this.lblCaban.Text = "Ca bán :";
            // 
            // lblQuay
            // 
            this.lblQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuay.Location = new System.Drawing.Point(399, 19);
            this.lblQuay.Margin = new System.Windows.Forms.Padding(4);
            this.lblQuay.Name = "lblQuay";
            this.lblQuay.Size = new System.Drawing.Size(45, 17);
            this.lblQuay.TabIndex = 56;
            this.lblQuay.Text = "Quầy :";
            // 
            // lblNhanvien
            // 
            this.lblNhanvien.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhanvien.Location = new System.Drawing.Point(399, 0);
            this.lblNhanvien.Margin = new System.Windows.Forms.Padding(4);
            this.lblNhanvien.Name = "lblNhanvien";
            this.lblNhanvien.Size = new System.Drawing.Size(72, 16);
            this.lblNhanvien.TabIndex = 55;
            this.lblNhanvien.Text = "Nhân viên :";
            // 
            // lblThongtinxuatchieu
            // 
            this.lblThongtinxuatchieu.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongtinxuatchieu.Location = new System.Drawing.Point(5, 19);
            this.lblThongtinxuatchieu.Margin = new System.Windows.Forms.Padding(4);
            this.lblThongtinxuatchieu.Name = "lblThongtinxuatchieu";
            this.lblThongtinxuatchieu.Size = new System.Drawing.Size(39, 16);
            this.lblThongtinxuatchieu.TabIndex = 50;
            this.lblThongtinxuatchieu.Text = "Phim :";
            // 
            // lblThongtinxuatchieu1
            // 
            this.lblThongtinxuatchieu1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongtinxuatchieu1.Location = new System.Drawing.Point(5, 0);
            this.lblThongtinxuatchieu1.Margin = new System.Windows.Forms.Padding(4);
            this.lblThongtinxuatchieu1.Name = "lblThongtinxuatchieu1";
            this.lblThongtinxuatchieu1.Size = new System.Drawing.Size(49, 16);
            this.lblThongtinxuatchieu1.TabIndex = 51;
            this.lblThongtinxuatchieu1.Text = "Phòng :";
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.lblTenRap);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.panelControl3.Size = new System.Drawing.Size(179, 39);
            this.panelControl3.TabIndex = 52;
            // 
            // lblTenRap
            // 
            this.lblTenRap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenRap.Location = new System.Drawing.Point(6, 6);
            this.lblTenRap.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenRap.Name = "lblTenRap";
            this.lblTenRap.Size = new System.Drawing.Size(169, 25);
            this.lblTenRap.TabIndex = 51;
            this.lblTenRap.Text = "KingPro Cinema";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit1.Location = new System.Drawing.Point(831, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(203, 39);
            this.pictureEdit1.TabIndex = 37;
            // 
            // pnlShowTime
            // 
            this.pnlShowTime.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlShowTime.Controls.Add(this.pnlPhim);
            this.pnlShowTime.Controls.Add(this.pnlLenxuong);
            this.pnlShowTime.Controls.Add(this.pnlNgay);
            this.pnlShowTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShowTime.Location = new System.Drawing.Point(0, 43);
            this.pnlShowTime.Margin = new System.Windows.Forms.Padding(0);
            this.pnlShowTime.Name = "pnlShowTime";
            this.pnlShowTime.Size = new System.Drawing.Size(836, 569);
            this.pnlShowTime.TabIndex = 53;
            // 
            // pnlPhim
            // 
            this.pnlPhim.Controls.Add(this.fpnlPhim);
            this.pnlPhim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPhim.Location = new System.Drawing.Point(0, 94);
            this.pnlPhim.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPhim.Name = "pnlPhim";
            this.pnlPhim.Size = new System.Drawing.Size(836, 475);
            this.pnlPhim.TabIndex = 52;
            // 
            // fpnlPhim
            // 
            this.fpnlPhim.AutoScroll = true;
            this.fpnlPhim.BackColor = System.Drawing.Color.Transparent;
            this.fpnlPhim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlPhim.Location = new System.Drawing.Point(2, 2);
            this.fpnlPhim.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlPhim.Name = "fpnlPhim";
            this.fpnlPhim.Size = new System.Drawing.Size(832, 471);
            this.fpnlPhim.TabIndex = 0;
            // 
            // pnlLenxuong
            // 
            this.pnlLenxuong.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnlLenxuong.Appearance.Options.UseBackColor = true;
            this.pnlLenxuong.Controls.Add(this.btnXuong);
            this.pnlLenxuong.Controls.Add(this.btnLen);
            this.pnlLenxuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLenxuong.Location = new System.Drawing.Point(0, 54);
            this.pnlLenxuong.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLenxuong.Name = "pnlLenxuong";
            this.pnlLenxuong.Size = new System.Drawing.Size(836, 40);
            this.pnlLenxuong.TabIndex = 53;
            // 
            // btnXuong
            // 
            this.btnXuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXuong.Image = global::KP_RES.Properties.Resources.up33_26;
            this.btnXuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuong.Location = new System.Drawing.Point(432, 2);
            this.btnXuong.Name = "btnXuong";
            this.btnXuong.Size = new System.Drawing.Size(402, 36);
            this.btnXuong.TabIndex = 6;
            this.btnXuong.Click += new System.EventHandler(this.btnXuong_Click);
            // 
            // btnLen
            // 
            this.btnLen.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLen.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btnLen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLen.Location = new System.Drawing.Point(2, 2);
            this.btnLen.Name = "btnLen";
            this.btnLen.Size = new System.Drawing.Size(430, 36);
            this.btnLen.TabIndex = 5;
            this.btnLen.Click += new System.EventHandler(this.btnLen_Click);
            // 
            // pnlNgay
            // 
            this.pnlNgay.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnlNgay.Appearance.Options.UseBackColor = true;
            this.pnlNgay.Controls.Add(this.fpnlNgay);
            this.pnlNgay.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNgay.Location = new System.Drawing.Point(0, 0);
            this.pnlNgay.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNgay.Name = "pnlNgay";
            this.pnlNgay.Size = new System.Drawing.Size(836, 54);
            this.pnlNgay.TabIndex = 45;
            // 
            // fpnlNgay
            // 
            this.fpnlNgay.BackColor = System.Drawing.Color.Transparent;
            this.fpnlNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlNgay.Location = new System.Drawing.Point(2, 2);
            this.fpnlNgay.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.fpnlNgay.Name = "fpnlNgay";
            this.fpnlNgay.Size = new System.Drawing.Size(832, 50);
            this.fpnlNgay.TabIndex = 0;
            // 
            // pnlSodoghe
            // 
            this.pnlSodoghe.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlSodoghe.Controls.Add(this.pnlSodo);
            this.pnlSodoghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSodoghe.Location = new System.Drawing.Point(0, 43);
            this.pnlSodoghe.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSodoghe.Name = "pnlSodoghe";
            this.pnlSodoghe.Size = new System.Drawing.Size(836, 569);
            this.pnlSodoghe.TabIndex = 54;
            // 
            // pnlSodo
            // 
            this.pnlSodo.AutoScroll = true;
            this.pnlSodo.BackColor = System.Drawing.Color.Transparent;
            this.pnlSodo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSodo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSodo.Location = new System.Drawing.Point(0, 0);
            this.pnlSodo.Name = "pnlSodo";
            this.pnlSodo.Size = new System.Drawing.Size(836, 569);
            this.pnlSodo.TabIndex = 6;
            // 
            // pnlMain
            // 
            this.pnlMain.AutoScroll = true;
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.gridControl);
            this.pnlMain.Controls.Add(this.panel2);
            this.pnlMain.Controls.Add(this.panel4);
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.panel3);
            this.pnlMain.Controls.Add(this.panelControl1);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMain.Location = new System.Drawing.Point(836, 43);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(200, 569);
            this.pnlMain.TabIndex = 4;
            // 
            // gridControl
            // 
            this.gridControl.AllowDrop = true;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(0, 62);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1});
            this.gridControl.Size = new System.Drawing.Size(198, 152);
            this.gridControl.TabIndex = 45;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridView.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MADIADIEM,
            this.MAVE,
            this.PHUTHU,
            this.MAKHUVUC,
            this.FILL});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.RowHeight = 50;
            this.gridView.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView_CustomDrawCell);
            this.gridView.RowCountChanged += new System.EventHandler(this.gridView_RowCountChanged);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Width = 50;
            // 
            // MADIADIEM
            // 
            this.MADIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MADIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceHeader.Options.UseFont = true;
            this.MADIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MADIADIEM.Caption = "Ghế";
            this.MADIADIEM.FieldName = "SOGHE";
            this.MADIADIEM.Name = "MADIADIEM";
            this.MADIADIEM.OptionsColumn.AllowFocus = false;
            this.MADIADIEM.OptionsColumn.AllowMove = false;
            this.MADIADIEM.OptionsColumn.AllowSize = false;
            this.MADIADIEM.OptionsColumn.FixedWidth = true;
            this.MADIADIEM.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.MADIADIEM.Visible = true;
            this.MADIADIEM.VisibleIndex = 0;
            this.MADIADIEM.Width = 40;
            // 
            // MAVE
            // 
            this.MAVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MAVE.AppearanceHeader.Options.UseFont = true;
            this.MAVE.AppearanceHeader.Options.UseTextOptions = true;
            this.MAVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MAVE.Caption = "Loại vé";
            this.MAVE.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.MAVE.FieldName = "MALOAIVE";
            this.MAVE.Name = "MAVE";
            this.MAVE.OptionsColumn.AllowMove = false;
            this.MAVE.OptionsColumn.AllowSize = false;
            this.MAVE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAVE.Visible = true;
            this.MAVE.VisibleIndex = 1;
            this.MAVE.Width = 98;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", 30, "Loại vé")});
            this.repositoryItemLookUpEdit1.DisplayMember = "TENLOAIVE";
            this.repositoryItemLookUpEdit1.DropDownItemHeight = 40;
            this.repositoryItemLookUpEdit1.DropDownRows = 20;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "Chọn loại vé";
            this.repositoryItemLookUpEdit1.UseParentBackground = true;
            this.repositoryItemLookUpEdit1.ValueMember = "MALOAIVE";
            this.repositoryItemLookUpEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpEdit1_EditValueChanged);
            this.repositoryItemLookUpEdit1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemLookUpEdit1_EditValueChanging);
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PHUTHU.Caption = "Giá vé";
            this.PHUTHU.DisplayFormat.FormatString = "N0";
            this.PHUTHU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.OptionsColumn.AllowMove = false;
            this.PHUTHU.OptionsColumn.AllowSize = false;
            this.PHUTHU.OptionsColumn.FixedWidth = true;
            this.PHUTHU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PHUTHU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PHUTHU", "{0:#,###0}")});
            this.PHUTHU.Visible = true;
            this.PHUTHU.VisibleIndex = 2;
            this.PHUTHU.Width = 60;
            // 
            // MAKHUVUC
            // 
            this.MAKHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHUVUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHUVUC.Caption = "Sử dụng";
            this.MAKHUVUC.FieldName = "MAKHUVUC";
            this.MAKHUVUC.Name = "MAKHUVUC";
            this.MAKHUVUC.OptionsColumn.AllowEdit = false;
            this.MAKHUVUC.OptionsColumn.AllowFocus = false;
            this.MAKHUVUC.OptionsColumn.AllowMove = false;
            this.MAKHUVUC.OptionsColumn.AllowSize = false;
            this.MAKHUVUC.OptionsColumn.FixedWidth = true;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.OptionsColumn.AllowSize = false;
            this.FILL.Width = 20;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cboLoaive);
            this.panel2.Controls.Add(this.btnLenve);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(198, 31);
            this.panel2.TabIndex = 75;
            // 
            // cboLoaive
            // 
            this.cboLoaive.EnterMoveNextControl = true;
            this.cboLoaive.Location = new System.Drawing.Point(96, 0);
            this.cboLoaive.Name = "cboLoaive";
            this.cboLoaive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaive.Properties.Appearance.Options.UseFont = true;
            this.cboLoaive.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaive.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoaive.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoaive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoaive.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", "Loại vé")});
            this.cboLoaive.Properties.DisplayMember = "TENLOAIVE";
            this.cboLoaive.Properties.DropDownItemHeight = 40;
            this.cboLoaive.Properties.NullText = "";
            this.cboLoaive.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoaive.Properties.ShowHeader = false;
            this.cboLoaive.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoaive.Properties.ValueMember = "MALOAIVE";
            this.cboLoaive.Size = new System.Drawing.Size(102, 30);
            this.cboLoaive.TabIndex = 21;
            this.cboLoaive.EditValueChanged += new System.EventHandler(this.cboLoaive_EditValueChanged);
            // 
            // btnLenve
            // 
            this.btnLenve.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLenve.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenve.Location = new System.Drawing.Point(0, 0);
            this.btnLenve.Name = "btnLenve";
            this.btnLenve.Size = new System.Drawing.Size(95, 31);
            this.btnLenve.TabIndex = 7;
            this.btnLenve.Click += new System.EventHandler(this.btnLenve_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbgiay);
            this.panel4.Controls.Add(this.lbNgay);
            this.panel4.Controls.Add(this.lbGio);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 31);
            this.panel4.TabIndex = 77;
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(182, 1);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(14, 16);
            this.lbgiay.TabIndex = 60;
            this.lbgiay.Text = "00";
            // 
            // lbNgay
            // 
            this.lbNgay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNgay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbNgay.Location = new System.Drawing.Point(6, 9);
            this.lbNgay.Name = "lbNgay";
            this.lbNgay.Size = new System.Drawing.Size(39, 19);
            this.lbNgay.TabIndex = 59;
            this.lbNgay.Text = "ngay";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(134, 9);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 61;
            this.lbGio.Text = "00:00";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtTongtien);
            this.panel1.Controls.Add(this.txtTongghe);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 214);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(198, 31);
            this.panel1.TabIndex = 71;
            // 
            // txtTongtien
            // 
            this.txtTongtien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTongtien.EditValue = "0";
            this.txtTongtien.EnterMoveNextControl = true;
            this.txtTongtien.Location = new System.Drawing.Point(40, 0);
            this.txtTongtien.Name = "txtTongtien";
            this.txtTongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongtien.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtTongtien.Properties.Appearance.Options.UseFont = true;
            this.txtTongtien.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongtien.Properties.ReadOnly = true;
            this.txtTongtien.Size = new System.Drawing.Size(158, 26);
            this.txtTongtien.TabIndex = 58;
            // 
            // txtTongghe
            // 
            this.txtTongghe.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtTongghe.EditValue = "0";
            this.txtTongghe.EnterMoveNextControl = true;
            this.txtTongghe.Location = new System.Drawing.Point(0, 0);
            this.txtTongghe.Name = "txtTongghe";
            this.txtTongghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongghe.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtTongghe.Properties.Appearance.Options.UseFont = true;
            this.txtTongghe.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongghe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongghe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTongghe.Properties.DisplayFormat.FormatString = "d";
            this.txtTongghe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTongghe.Properties.EditFormat.FormatString = "d";
            this.txtTongghe.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTongghe.Properties.Mask.EditMask = "d";
            this.txtTongghe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTongghe.Properties.ReadOnly = true;
            this.txtTongghe.Size = new System.Drawing.Size(40, 26);
            this.txtTongghe.TabIndex = 57;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnXoave);
            this.panel3.Controls.Add(this.btnXuongve);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 245);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(198, 31);
            this.panel3.TabIndex = 76;
            // 
            // btnXoave
            // 
            this.btnXoave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXoave.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXoave.Location = new System.Drawing.Point(103, 0);
            this.btnXoave.Name = "btnXoave";
            this.btnXoave.Size = new System.Drawing.Size(95, 31);
            this.btnXoave.TabIndex = 8;
            this.btnXoave.Click += new System.EventHandler(this.btnXoave_Click);
            // 
            // btnXuongve
            // 
            this.btnXuongve.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnXuongve.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongve.Location = new System.Drawing.Point(0, 0);
            this.btnXuongve.Name = "btnXuongve";
            this.btnXuongve.Size = new System.Drawing.Size(95, 31);
            this.btnXuongve.TabIndex = 7;
            this.btnXuongve.Click += new System.EventHandler(this.btnXuongve_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnChonnhieughe);
            this.panelControl1.Controls.Add(this.btnKetca);
            this.panelControl1.Controls.Add(this.btnMoket);
            this.panelControl1.Controls.Add(this.btnChonlaighe);
            this.panelControl1.Controls.Add(this.btnBanve);
            this.panelControl1.Controls.Add(this.btnChonSuatChieu);
            this.panelControl1.Controls.Add(this.btnInvedattruoc);
            this.panelControl1.Controls.Add(this.btnInlaive);
            this.panelControl1.Controls.Add(this.btnHuyve);
            this.panelControl1.Controls.Add(this.btnDong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 276);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(198, 291);
            this.panelControl1.TabIndex = 74;
            // 
            // btnChonnhieughe
            // 
            this.btnChonnhieughe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonnhieughe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonnhieughe.Appearance.Options.UseFont = true;
            this.btnChonnhieughe.Appearance.Options.UseTextOptions = true;
            this.btnChonnhieughe.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonnhieughe.Image = global::KP_RES.Properties.Resources.dust_26;
            this.btnChonnhieughe.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonnhieughe.Location = new System.Drawing.Point(102, 181);
            this.btnChonnhieughe.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonnhieughe.Name = "btnChonnhieughe";
            this.btnChonnhieughe.Size = new System.Drawing.Size(95, 50);
            this.btnChonnhieughe.TabIndex = 76;
            this.btnChonnhieughe.Text = "Chọn nhiều ghế";
            this.btnChonnhieughe.Click += new System.EventHandler(this.btnChonnhieughe_Click);
            // 
            // btnKetca
            // 
            this.btnKetca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetca.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnKetca.Appearance.Options.UseFont = true;
            this.btnKetca.Appearance.Options.UseTextOptions = true;
            this.btnKetca.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKetca.Image = global::KP_RES.Properties.Resources.clock_26;
            this.btnKetca.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnKetca.Location = new System.Drawing.Point(1, 181);
            this.btnKetca.Margin = new System.Windows.Forms.Padding(4);
            this.btnKetca.Name = "btnKetca";
            this.btnKetca.Size = new System.Drawing.Size(95, 50);
            this.btnKetca.TabIndex = 75;
            this.btnKetca.Text = "Kết ca";
            this.btnKetca.Click += new System.EventHandler(this.btnKetca_Click);
            // 
            // btnMoket
            // 
            this.btnMoket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMoket.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMoket.Appearance.Options.UseFont = true;
            this.btnMoket.Appearance.Options.UseTextOptions = true;
            this.btnMoket.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnMoket.Image = global::KP_RES.Properties.Resources.ssd_26;
            this.btnMoket.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMoket.Location = new System.Drawing.Point(1, 239);
            this.btnMoket.Margin = new System.Windows.Forms.Padding(4);
            this.btnMoket.Name = "btnMoket";
            this.btnMoket.Size = new System.Drawing.Size(95, 50);
            this.btnMoket.TabIndex = 74;
            this.btnMoket.Text = "Mở két";
            this.btnMoket.Click += new System.EventHandler(this.btnMoket_Click);
            // 
            // btnChonlaighe
            // 
            this.btnChonlaighe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonlaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonlaighe.Appearance.Options.UseFont = true;
            this.btnChonlaighe.Appearance.Options.UseTextOptions = true;
            this.btnChonlaighe.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonlaighe.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnChonlaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonlaighe.Location = new System.Drawing.Point(102, 7);
            this.btnChonlaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonlaighe.Name = "btnChonlaighe";
            this.btnChonlaighe.Size = new System.Drawing.Size(95, 50);
            this.btnChonlaighe.TabIndex = 73;
            this.btnChonlaighe.Text = "Chọn lại ghế";
            this.btnChonlaighe.Click += new System.EventHandler(this.btnChonlaighe_Click);
            // 
            // btnBanve
            // 
            this.btnBanve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBanve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBanve.Appearance.Options.UseFont = true;
            this.btnBanve.Appearance.Options.UseTextOptions = true;
            this.btnBanve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBanve.Image = global::KP_RES.Properties.Resources.card_in_use_26;
            this.btnBanve.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnBanve.Location = new System.Drawing.Point(1, 65);
            this.btnBanve.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanve.Name = "btnBanve";
            this.btnBanve.Size = new System.Drawing.Size(95, 50);
            this.btnBanve.TabIndex = 42;
            this.btnBanve.Text = "Thanh toán";
            this.btnBanve.Click += new System.EventHandler(this.btnBanve_Click);
            // 
            // btnChonSuatChieu
            // 
            this.btnChonSuatChieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonSuatChieu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonSuatChieu.Appearance.Options.UseFont = true;
            this.btnChonSuatChieu.Appearance.Options.UseTextOptions = true;
            this.btnChonSuatChieu.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonSuatChieu.Image = global::KP_RES.Properties.Resources.backspace_26;
            this.btnChonSuatChieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonSuatChieu.Location = new System.Drawing.Point(1, 7);
            this.btnChonSuatChieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonSuatChieu.Name = "btnChonSuatChieu";
            this.btnChonSuatChieu.Size = new System.Drawing.Size(95, 50);
            this.btnChonSuatChieu.TabIndex = 41;
            this.btnChonSuatChieu.Text = "Chọn phim";
            this.btnChonSuatChieu.Click += new System.EventHandler(this.btnChonSuatChieu_Click);
            // 
            // btnInvedattruoc
            // 
            this.btnInvedattruoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInvedattruoc.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnInvedattruoc.Appearance.Options.UseFont = true;
            this.btnInvedattruoc.Appearance.Options.UseTextOptions = true;
            this.btnInvedattruoc.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInvedattruoc.Image = global::KP_RES.Properties.Resources.bill_26;
            this.btnInvedattruoc.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnInvedattruoc.Location = new System.Drawing.Point(102, 65);
            this.btnInvedattruoc.Margin = new System.Windows.Forms.Padding(4);
            this.btnInvedattruoc.Name = "btnInvedattruoc";
            this.btnInvedattruoc.Size = new System.Drawing.Size(95, 50);
            this.btnInvedattruoc.TabIndex = 70;
            this.btnInvedattruoc.Text = "In vé đặt trước";
            this.btnInvedattruoc.Click += new System.EventHandler(this.btnInvedattruoc_Click);
            // 
            // btnInlaive
            // 
            this.btnInlaive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInlaive.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnInlaive.Appearance.Options.UseFont = true;
            this.btnInlaive.Appearance.Options.UseTextOptions = true;
            this.btnInlaive.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInlaive.Image = global::KP_RES.Properties.Resources.ticket_26;
            this.btnInlaive.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnInlaive.Location = new System.Drawing.Point(102, 123);
            this.btnInlaive.Margin = new System.Windows.Forms.Padding(4);
            this.btnInlaive.Name = "btnInlaive";
            this.btnInlaive.Size = new System.Drawing.Size(95, 50);
            this.btnInlaive.TabIndex = 68;
            this.btnInlaive.Text = "In lại vé";
            this.btnInlaive.Click += new System.EventHandler(this.btnInlaive_Click);
            // 
            // btnHuyve
            // 
            this.btnHuyve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHuyve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnHuyve.Appearance.Options.UseFont = true;
            this.btnHuyve.Appearance.Options.UseTextOptions = true;
            this.btnHuyve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnHuyve.Image = global::KP_RES.Properties.Resources.delete_ticket_26;
            this.btnHuyve.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnHuyve.Location = new System.Drawing.Point(1, 123);
            this.btnHuyve.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyve.Name = "btnHuyve";
            this.btnHuyve.Size = new System.Drawing.Size(95, 50);
            this.btnHuyve.TabIndex = 60;
            this.btnHuyve.Text = "Hủy vé";
            this.btnHuyve.Click += new System.EventHandler(this.btnHuyve_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Appearance.Options.UseTextOptions = true;
            this.btnDong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDong.Image = global::KP_RES.Properties.Resources.shutdown_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDong.Location = new System.Drawing.Point(102, 239);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(95, 50);
            this.btnDong.TabIndex = 72;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "barStaticItem1";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "barStaticItem1";
            this.barStaticItem3.Id = 1;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(653, 164);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(85, 19);
            this.labelControl10.TabIndex = 27;
            this.labelControl10.Text = "Thời gian đi";
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(757, 161);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(518, 26);
            this.textEdit1.TabIndex = 28;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(653, 132);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(27, 19);
            this.labelControl14.TabIndex = 25;
            this.labelControl14.Text = "Tàu";
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(757, 129);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(518, 26);
            this.textEdit5.TabIndex = 26;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(653, 100);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(44, 19);
            this.labelControl15.TabIndex = 23;
            this.labelControl15.Text = "Tuyến";
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(757, 97);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.ReadOnly = true;
            this.textEdit6.Size = new System.Drawing.Size(518, 26);
            this.textEdit6.TabIndex = 24;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(12, 68);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(95, 19);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "Mã biên nhận";
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(128, 66);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(518, 26);
            this.textEdit7.TabIndex = 4;
            // 
            // textEdit8
            // 
            this.textEdit8.EditValue = "0";
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(757, 288);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit8.Properties.Mask.EditMask = "N0";
            this.textEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(518, 26);
            this.textEdit8.TabIndex = 20;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(641, 292);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(49, 19);
            this.labelControl17.TabIndex = 19;
            this.labelControl17.Text = "Còn lại";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.Image")));
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(761, 20);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(123, 40);
            this.simpleButton8.TabIndex = 21;
            this.simpleButton8.Text = "&2.In vé";
            // 
            // memoEdit1
            // 
            this.memoEdit1.EnterMoveNextControl = true;
            this.memoEdit1.Location = new System.Drawing.Point(128, 226);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(518, 122);
            this.memoEdit1.TabIndex = 14;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(9, 231);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(94, 19);
            this.labelControl18.TabIndex = 13;
            this.labelControl18.Text = "Danh sách vé";
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "0";
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(757, 224);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit9.Properties.ReadOnly = true;
            this.textEdit9.Size = new System.Drawing.Size(518, 26);
            this.textEdit9.TabIndex = 16;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(638, 228);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(84, 19);
            this.labelControl19.TabIndex = 15;
            this.labelControl19.Text = "Tổng giá vé";
            // 
            // textEdit10
            // 
            this.textEdit10.EditValue = "0";
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(757, 257);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit10.Properties.Mask.EditMask = "N0";
            this.textEdit10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit10.Properties.ReadOnly = true;
            this.textEdit10.Size = new System.Drawing.Size(518, 26);
            this.textEdit10.TabIndex = 18;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(12, 197);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(39, 19);
            this.labelControl20.TabIndex = 11;
            this.labelControl20.Text = "Email";
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(128, 194);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.ReadOnly = true;
            this.textEdit11.Size = new System.Drawing.Size(518, 26);
            this.textEdit11.TabIndex = 12;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(12, 165);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 19);
            this.labelControl21.TabIndex = 9;
            this.labelControl21.Text = "Điện thoại";
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(128, 162);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.ReadOnly = true;
            this.textEdit12.Size = new System.Drawing.Size(518, 26);
            this.textEdit12.TabIndex = 10;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(12, 133);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 19);
            this.labelControl22.TabIndex = 7;
            this.labelControl22.Text = "Địa chỉ";
            // 
            // textEdit13
            // 
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(128, 130);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.ReadOnly = true;
            this.textEdit13.Size = new System.Drawing.Size(518, 26);
            this.textEdit13.TabIndex = 8;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(638, 260);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(100, 19);
            this.labelControl23.TabIndex = 17;
            this.labelControl23.Text = "Đã thanh toán";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(12, 100);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(27, 19);
            this.labelControl24.TabIndex = 5;
            this.labelControl24.Text = "Tên";
            // 
            // textEdit14
            // 
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(128, 98);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.ReadOnly = true;
            this.textEdit14.Size = new System.Drawing.Size(518, 26);
            this.textEdit14.TabIndex = 6;
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton16.Location = new System.Drawing.Point(653, 20);
            this.simpleButton16.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(100, 40);
            this.simpleButton16.TabIndex = 22;
            this.simpleButton16.Text = "&1.Xem";
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl28.Location = new System.Drawing.Point(0, 0);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(1078, 14);
            this.panelControl28.TabIndex = 0;
            // 
            // textEdit15
            // 
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(128, 20);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit15.Size = new System.Drawing.Size(518, 40);
            this.textEdit15.TabIndex = 2;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(12, 33);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(109, 19);
            this.labelControl25.TabIndex = 1;
            this.labelControl25.Text = "Mã biên nhận";
            // 
            // timerLoadSoDoGheTuDong
            // 
            this.timerLoadSoDoGheTuDong.Enabled = true;
            this.timerLoadSoDoGheTuDong.Interval = 3000;
            this.timerLoadSoDoGheTuDong.Tick += new System.EventHandler(this.timerLoadSoDoGheTuDong_Tick);
            // 
            // timerDongho
            // 
            this.timerDongho.Enabled = true;
            this.timerDongho.Interval = 1000;
            this.timerDongho.Tick += new System.EventHandler(this.timerDongho_Tick);
            // 
            // Frm_RPBanve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 612);
            this.Controls.Add(this.pnlShowTime);
            this.Controls.Add(this.pnlSodoghe);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Frm_RPBanve";
            this.Text = "KingPro Ticket Management Software";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_RPBanve_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_RPBanve_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowTime)).EndInit();
            this.pnlShowTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhim)).EndInit();
            this.pnlPhim.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlLenxuong)).EndInit();
            this.pnlLenxuong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlNgay)).EndInit();
            this.pnlNgay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlSodoghe)).EndInit();
            this.pnlSodoghe.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaive.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl pnlNgay;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl pnlShowTime;
        private DevExpress.XtraEditors.SimpleButton btnChonSuatChieu;
        private DevExpress.XtraEditors.SimpleButton btnBanve;
        private System.Windows.Forms.Panel pnlMain;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MADIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn MAVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.SimpleButton btnHuyve;
        private DevExpress.XtraEditors.SimpleButton btnInlaive;
        private DevExpress.XtraEditors.SimpleButton btnInvedattruoc;
        private System.Windows.Forms.Timer timerLoadSoDoGheTuDong;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txtTongghe;
        private DevExpress.XtraEditors.TextEdit txtTongtien;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnChonlaighe;
        private System.Windows.Forms.FlowLayoutPanel fpnlNgay;
        private DevExpress.XtraEditors.PanelControl pnlPhim;
        private DevExpress.XtraEditors.PanelControl pnlLenxuong;
        private DevExpress.XtraEditors.PanelControl pnlSodoghe;
        private System.Windows.Forms.FlowLayoutPanel fpnlPhim;
        private System.Windows.Forms.Panel pnlSodo;
        private DevExpress.XtraEditors.LabelControl lblThongtinxuatchieu;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblTenRap;
        private DevExpress.XtraEditors.LabelControl lblThongtinxuatchieu1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnMoket;
        private DevExpress.XtraEditors.SimpleButton btnLen;
        private DevExpress.XtraEditors.SimpleButton btnXuong;
        private DevExpress.XtraEditors.SimpleButton btnKetca;
        private DevExpress.XtraEditors.SimpleButton btnChonnhieughe;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnLenve;
        private DevExpress.XtraEditors.SimpleButton btnXuongve;
        private DevExpress.XtraEditors.SimpleButton btnXoave;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LabelControl lblNhanvien;
        private DevExpress.XtraEditors.LabelControl lblCaban;
        private DevExpress.XtraEditors.LabelControl lblQuay;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbNgay;
        private System.Windows.Forms.Timer timerDongho;
        private DevExpress.XtraEditors.LookUpEdit cboLoaive;

    }
}