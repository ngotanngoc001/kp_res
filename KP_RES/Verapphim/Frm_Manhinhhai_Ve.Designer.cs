﻿namespace KP_RES
{
    partial class Frm_Manhinhhai_Ve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Manhinhhai_Ve));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnlTrailer = new System.Windows.Forms.Panel();
            this.WindowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.pnlSodoghe = new System.Windows.Forms.Panel();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlSodo = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtTongtien = new DevExpress.XtraEditors.TextEdit();
            this.txtTongghe = new DevExpress.XtraEditors.TextEdit();
            this.pnlDes = new System.Windows.Forms.Panel();
            this.fpnlGiaithich = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlMain1 = new System.Windows.Forms.Panel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.lblThongtinxuatchieu = new DevExpress.XtraEditors.LabelControl();
            this.lblThongtinxuatchieu1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtRap = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pnlTrailer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).BeginInit();
            this.pnlSodoghe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).BeginInit();
            this.pnlDes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnlTrailer
            // 
            this.pnlTrailer.Controls.Add(this.WindowsMediaPlayer);
            this.pnlTrailer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTrailer.Location = new System.Drawing.Point(0, 0);
            this.pnlTrailer.Name = "pnlTrailer";
            this.pnlTrailer.Size = new System.Drawing.Size(851, 480);
            this.pnlTrailer.TabIndex = 0;
            // 
            // WindowsMediaPlayer
            // 
            this.WindowsMediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowsMediaPlayer.Enabled = true;
            this.WindowsMediaPlayer.Location = new System.Drawing.Point(0, 0);
            this.WindowsMediaPlayer.Name = "WindowsMediaPlayer";
            this.WindowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WindowsMediaPlayer.OcxState")));
            this.WindowsMediaPlayer.Size = new System.Drawing.Size(851, 480);
            this.WindowsMediaPlayer.TabIndex = 2;
            // 
            // pnlSodoghe
            // 
            this.pnlSodoghe.Controls.Add(this.panelControl2);
            this.pnlSodoghe.Controls.Add(this.panelControl1);
            this.pnlSodoghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSodoghe.Location = new System.Drawing.Point(0, 0);
            this.pnlSodoghe.Name = "pnlSodoghe";
            this.pnlSodoghe.Size = new System.Drawing.Size(851, 480);
            this.pnlSodoghe.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panel1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 43);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(851, 437);
            this.panelControl2.TabIndex = 47;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlSodo);
            this.panel1.Controls.Add(this.pnlMain);
            this.panel1.Controls.Add(this.pnlMain1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(851, 437);
            this.panel1.TabIndex = 7;
            // 
            // pnlSodo
            // 
            this.pnlSodo.BackColor = System.Drawing.Color.Transparent;
            this.pnlSodo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSodo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSodo.Location = new System.Drawing.Point(20, 0);
            this.pnlSodo.Name = "pnlSodo";
            this.pnlSodo.Size = new System.Drawing.Size(651, 437);
            this.pnlSodo.TabIndex = 0;
            // 
            // pnlMain
            // 
            this.pnlMain.AutoScroll = true;
            this.pnlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pnlMain.Controls.Add(this.gridControl);
            this.pnlMain.Controls.Add(this.panel3);
            this.pnlMain.Controls.Add(this.pnlDes);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMain.Location = new System.Drawing.Point(671, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(180, 437);
            this.pnlMain.TabIndex = 5;
            // 
            // gridControl
            // 
            this.gridControl.AllowDrop = true;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1});
            this.gridControl.Size = new System.Drawing.Size(180, 119);
            this.gridControl.TabIndex = 45;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridView1});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MADIADIEM,
            this.MAVE,
            this.PHUTHU,
            this.MAKHUVUC,
            this.FILL});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.RowHeight = 50;
            this.gridView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Width = 50;
            // 
            // MADIADIEM
            // 
            this.MADIADIEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceCell.Options.UseFont = true;
            this.MADIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MADIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceHeader.Options.UseFont = true;
            this.MADIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MADIADIEM.Caption = "Ghế";
            this.MADIADIEM.FieldName = "SOGHE";
            this.MADIADIEM.Name = "MADIADIEM";
            this.MADIADIEM.OptionsColumn.AllowFocus = false;
            this.MADIADIEM.OptionsColumn.AllowMove = false;
            this.MADIADIEM.OptionsColumn.AllowSize = false;
            this.MADIADIEM.OptionsColumn.FixedWidth = true;
            this.MADIADIEM.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.MADIADIEM.Visible = true;
            this.MADIADIEM.VisibleIndex = 0;
            this.MADIADIEM.Width = 40;
            // 
            // MAVE
            // 
            this.MAVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MAVE.AppearanceCell.Options.UseFont = true;
            this.MAVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MAVE.AppearanceHeader.Options.UseFont = true;
            this.MAVE.AppearanceHeader.Options.UseTextOptions = true;
            this.MAVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MAVE.Caption = "Loại vé";
            this.MAVE.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.MAVE.FieldName = "MALOAIVE";
            this.MAVE.Name = "MAVE";
            this.MAVE.OptionsColumn.AllowMove = false;
            this.MAVE.OptionsColumn.AllowSize = false;
            this.MAVE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAVE.Width = 98;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", 30, "Loại vé")});
            this.repositoryItemLookUpEdit1.DisplayMember = "TENLOAIVE";
            this.repositoryItemLookUpEdit1.DropDownItemHeight = 40;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "Chọn loại vé";
            this.repositoryItemLookUpEdit1.UseParentBackground = true;
            this.repositoryItemLookUpEdit1.ValueMember = "MALOAIVE";
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceCell.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PHUTHU.Caption = "Giá vé";
            this.PHUTHU.DisplayFormat.FormatString = "N0";
            this.PHUTHU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.OptionsColumn.AllowMove = false;
            this.PHUTHU.OptionsColumn.AllowSize = false;
            this.PHUTHU.OptionsColumn.FixedWidth = true;
            this.PHUTHU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PHUTHU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PHUTHU", "{0:#,###0}")});
            this.PHUTHU.Visible = true;
            this.PHUTHU.VisibleIndex = 1;
            this.PHUTHU.Width = 60;
            // 
            // MAKHUVUC
            // 
            this.MAKHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHUVUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHUVUC.Caption = "Sử dụng";
            this.MAKHUVUC.FieldName = "MAKHUVUC";
            this.MAKHUVUC.Name = "MAKHUVUC";
            this.MAKHUVUC.OptionsColumn.AllowEdit = false;
            this.MAKHUVUC.OptionsColumn.AllowFocus = false;
            this.MAKHUVUC.OptionsColumn.AllowMove = false;
            this.MAKHUVUC.OptionsColumn.AllowSize = false;
            this.MAKHUVUC.OptionsColumn.FixedWidth = true;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.OptionsColumn.AllowSize = false;
            this.FILL.Width = 20;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl;
            this.gridView1.Name = "gridView1";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panel3.Controls.Add(this.txtTongtien);
            this.panel3.Controls.Add(this.txtTongghe);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 119);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(180, 33);
            this.panel3.TabIndex = 71;
            // 
            // txtTongtien
            // 
            this.txtTongtien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTongtien.EditValue = "0";
            this.txtTongtien.EnterMoveNextControl = true;
            this.txtTongtien.Location = new System.Drawing.Point(40, 0);
            this.txtTongtien.Name = "txtTongtien";
            this.txtTongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongtien.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtTongtien.Properties.Appearance.Options.UseFont = true;
            this.txtTongtien.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongtien.Properties.ReadOnly = true;
            this.txtTongtien.Size = new System.Drawing.Size(140, 30);
            this.txtTongtien.TabIndex = 58;
            // 
            // txtTongghe
            // 
            this.txtTongghe.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtTongghe.EditValue = "0";
            this.txtTongghe.EnterMoveNextControl = true;
            this.txtTongghe.Location = new System.Drawing.Point(0, 0);
            this.txtTongghe.Name = "txtTongghe";
            this.txtTongghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongghe.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTongghe.Properties.Appearance.Options.UseFont = true;
            this.txtTongghe.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongghe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongghe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTongghe.Properties.ReadOnly = true;
            this.txtTongghe.Size = new System.Drawing.Size(40, 30);
            this.txtTongghe.TabIndex = 57;
            // 
            // pnlDes
            // 
            this.pnlDes.BackColor = System.Drawing.Color.Transparent;
            this.pnlDes.Controls.Add(this.fpnlGiaithich);
            this.pnlDes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDes.Location = new System.Drawing.Point(0, 152);
            this.pnlDes.Name = "pnlDes";
            this.pnlDes.Size = new System.Drawing.Size(180, 285);
            this.pnlDes.TabIndex = 72;
            // 
            // fpnlGiaithich
            // 
            this.fpnlGiaithich.BackColor = System.Drawing.Color.Transparent;
            this.fpnlGiaithich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlGiaithich.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlGiaithich.Location = new System.Drawing.Point(0, 0);
            this.fpnlGiaithich.Name = "fpnlGiaithich";
            this.fpnlGiaithich.Size = new System.Drawing.Size(180, 285);
            this.fpnlGiaithich.TabIndex = 0;
            // 
            // pnlMain1
            // 
            this.pnlMain1.AutoScroll = true;
            this.pnlMain1.BackColor = System.Drawing.Color.Transparent;
            this.pnlMain1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMain1.Location = new System.Drawing.Point(0, 0);
            this.pnlMain1.Name = "pnlMain1";
            this.pnlMain1.Size = new System.Drawing.Size(20, 437);
            this.pnlMain1.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(851, 43);
            this.panelControl1.TabIndex = 46;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.lblThongtinxuatchieu);
            this.panelControl4.Controls.Add(this.lblThongtinxuatchieu1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(179, 0);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.panelControl4.Size = new System.Drawing.Size(495, 43);
            this.panelControl4.TabIndex = 53;
            // 
            // lblThongtinxuatchieu
            // 
            this.lblThongtinxuatchieu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblThongtinxuatchieu.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongtinxuatchieu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblThongtinxuatchieu.Location = new System.Drawing.Point(23, 19);
            this.lblThongtinxuatchieu.Margin = new System.Windows.Forms.Padding(4);
            this.lblThongtinxuatchieu.Name = "lblThongtinxuatchieu";
            this.lblThongtinxuatchieu.Size = new System.Drawing.Size(39, 16);
            this.lblThongtinxuatchieu.TabIndex = 52;
            this.lblThongtinxuatchieu.Text = "Phim :";
            // 
            // lblThongtinxuatchieu1
            // 
            this.lblThongtinxuatchieu1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblThongtinxuatchieu1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongtinxuatchieu1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblThongtinxuatchieu1.Location = new System.Drawing.Point(23, 0);
            this.lblThongtinxuatchieu1.Margin = new System.Windows.Forms.Padding(4);
            this.lblThongtinxuatchieu1.Name = "lblThongtinxuatchieu1";
            this.lblThongtinxuatchieu1.Size = new System.Drawing.Size(49, 16);
            this.lblThongtinxuatchieu1.TabIndex = 51;
            this.lblThongtinxuatchieu1.Text = "Phòng :";
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.txtRap);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.panelControl3.Size = new System.Drawing.Size(179, 43);
            this.panelControl3.TabIndex = 52;
            // 
            // txtRap
            // 
            this.txtRap.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtRap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRap.Appearance.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtRap.Location = new System.Drawing.Point(6, 6);
            this.txtRap.Margin = new System.Windows.Forms.Padding(4);
            this.txtRap.Name = "txtRap";
            this.txtRap.Size = new System.Drawing.Size(169, 25);
            this.txtRap.TabIndex = 51;
            this.txtRap.Text = "KingPro Cinema";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit1.Location = new System.Drawing.Point(674, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(177, 43);
            this.pictureEdit1.TabIndex = 37;
            // 
            // timer2
            // 
            this.timer2.Interval = 8000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Frm_Manhinhhai_Ve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 480);
            this.Controls.Add(this.pnlSodoghe);
            this.Controls.Add(this.pnlTrailer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Manhinhhai_Ve";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Frm_Manhinhhai_Ve";
            this.Load += new System.EventHandler(this.Frm_Manhinhhai_Ve_Load);
            this.pnlTrailer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).EndInit();
            this.pnlSodoghe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).EndInit();
            this.pnlDes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlTrailer;
        private System.Windows.Forms.Panel pnlSodoghe;
        private AxWMPLib.AxWindowsMediaPlayer WindowsMediaPlayer;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lblThongtinxuatchieu;
        private DevExpress.XtraEditors.LabelControl lblThongtinxuatchieu1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl txtRap;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlSodo;
        private System.Windows.Forms.Panel pnlMain;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MADIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn MAVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.TextEdit txtTongtien;
        private DevExpress.XtraEditors.TextEdit txtTongghe;
        private System.Windows.Forms.Panel pnlDes;
        private System.Windows.Forms.FlowLayoutPanel fpnlGiaithich;
        private System.Windows.Forms.Panel pnlMain1;
        private System.Windows.Forms.Timer timer2;
    }
}