﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace KP_RES 
{
    public partial class PF_Chonnhieughe : DevExpress.XtraEditors.XtraForm
    {
        public int iSoghe = 0;
        public string sGhebatdau = "";
        public PF_Chonnhieughe()
        {
            InitializeComponent();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            iSoghe = int.Parse (txtSoghe.Text.Replace(",", ""));
            sGhebatdau = txtDay.Text;
            this.Close();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckInput()
        {
            txtDay.Text = comboBoxEdit1.Text + comboBoxEdit2.Text;
            if (txtSoghe.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập số ghế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoghe.Focus();
                return false ;
            }
            if (txtDay.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập số ghế bắt đầu","Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDay.Focus();
                return false ;
            }
            return true;
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDay.Text = comboBoxEdit1.Text + comboBoxEdit2.Text;
        }

        private void comboBoxEdit2_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDay.Text = comboBoxEdit1.Text + comboBoxEdit2.Text;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            txtSoghe.Text = "0";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtSoghe.Text = txtSoghe.Text + btn.Text ;
        }

        private void PF_Chonnhieughe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            btnDong_Click(null, null);
        }
    }
}