﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES 
{
    public partial class Frm_BC_Lichchieu : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_Lichchieu()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_Lichchieu_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        DataTable dtCombo = new DataTable();
        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            //cboDATA.Properties.Columns.Clear();
            //cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Rạp
            {
                dtCombo = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1 Order by TEN");
                //dtCombo.Rows.Add("0", "Tất cả");
                //cboDATA.Properties.DataSource = dtCombo;
                //taocolumncholookupedit(new string[] { "Mã", "Tên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Rạp";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 1)//phim
            {
                dtCombo = clsMain.ReturnDataTable("select MAPHIM as MA,TENPHIM as TEN from DM_PHIM where SUDUNG=1  Order by TEN");
                //dtCombo.Rows.Add("0", "Tất cả");
                //cboDATA.Properties.DataSource = dtCombo;
                //taocolumncholookupedit(new string[] { "Mã", "Tên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Phim";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 2)//nhà phát hành
            {
                dtCombo = clsMain.ReturnDataTable("select MANHAPHATHANH as MA,TENNHAPHATHANH as TEN from DM_NHAPHATHANH where SUDUNG=1  Order by TEN");
                //dtCombo.Rows.Add("0", "Tất cả");
                //cboDATA.Properties.DataSource = dtCombo;
                //taocolumncholookupedit(new string[] { "Mã", "Tên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhà phát hành";
                LoadChiTietCombo(dtCombo);
            }

            //cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void LoadChiTietCombo(DataTable dt)
        {
            cboDATA.Properties.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                cboDATA.Properties.Items.Add(dr["TEN"].ToString());
            }
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            //if (Caption.Length > 1 & dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < Caption.Length; i++)
            //    {
            //        LookUpColumnInfo info = new LookUpColumnInfo();
            //        info.FieldName = FieldName[i].TrimEnd();
            //        info.Caption = Caption[i].TrimEnd();
            //        info.Visible = visible[i];
            //        info.Alignment = DevExpress.Utils.HorzAlignment.Default;
            //        cboDATA.Properties.Columns.Add(info);
            //    }
            //    cboDATA.Properties.BestFitMode = BestFitMode.None;
            //    cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
            //    cboDATA.Properties.DisplayMember = FieldName[1];
            //    cboDATA.Properties.ValueMember = FieldName[0];
            //    if (visible[0] == true)
            //    {
            //        cboDATA.Properties.Columns[0].Width = 100;
            //        cboDATA.Properties.Columns[1].Width = 500;
            //    }
            //    else
            //    {
            //        cboDATA.Properties.Columns[1].Width = 100;
            //        cboDATA.Properties.Columns[2].Width = 500;
            //    }
            //    if (dt.Rows.Count > 0)
            //        cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            //}
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value || cboDATA.EditValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn đối tượng cần xem", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (optGroup1.SelectedIndex == 2 && optGroup.SelectedIndex == 0)
            {
                if (dtpDenngay.Text != dtpTungay.Text)
                {
                    XtraMessageBox.Show("Mode xem theo dạng đồ họa chỉ xem được trong 1 ngày", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                splashScreenManager1.ShowWaitForm();
                gridControl1.Visible = false;
                pnlListPhong.Controls.Clear();

                string sqlTemp = "";
                foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                        if (dr.Length > 0)
                        {
                            sqlTemp= dr[0]["MA"].ToString();
                            break;
                        }
                    }
                }

                string sSQL = "";
                DataTable dtPHONG = new DataTable();
                sSQL += "EXECUTE SP_VE_LICHCHIEU_LOADPHONG " + clsMain.SQLString(sqlTemp) + "\n";
                dtPHONG = clsMain.ReturnDataTable(sSQL);

                DataTable dtLichchieu_Full = new DataTable();
                sSQL = "";
                sSQL += "EXECUTE SP_VE_LICHCHIEU_LOADPHONG_LICHCHIEU " + clsMain.SQLString(dtpTungay.Text) + "," + clsMain.SQLString("0") + "\n";
                dtLichchieu_Full = clsMain.ReturnDataTable(sSQL);

                foreach (DataRow dr in dtPHONG.Rows)
                {
                    SimpleButton pnlSub = new SimpleButton();
                    pnlSub.Dock = DockStyle.Left;
                    pnlSub.Width = 52;
                    pnlSub.Font = panelControl8.Appearance .Font;
                    pnlSub.Text = dr["TEN"].ToString();

                    PanelControl fpnlSub = new PanelControl();
                    fpnlSub.Dock = DockStyle.Fill;
                    fpnlSub.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;

                    dtLichchieu_Full.DefaultView.RowFilter = "MAPHONG=" + dr["MA"].ToString();
                    DataTable dt = dtLichchieu_Full.DefaultView.ToTable();

                    foreach (DataRow dr1 in dt.Rows)
                    {
                        int iTongsoghe = int.Parse(dr1["TONGGHE"].ToString());
                        int iTongsoghe_ban = int.Parse(dr1["GHEDABAN"].ToString());

                        SimpleButton btn = new SimpleButton();
                        btn.Font = pnlListPhong.Font;
                        btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn.Tag = dr1["MALICHCHIEU"].ToString();
                        btn.Text = dr1["TENPHIM"].ToString() + "\n" + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "(" + dr1["TENBANGGIA"].ToString() + ")" + "\n" + iTongsoghe_ban.ToString() + "/" + iTongsoghe.ToString();
                        btn.ToolTip = "Phim : " + dr1["TENPHIM"].ToString() + "\n" + "Giờ chiếu : " + dr1["BATDAU"].ToString() + "-" + dr1["KETTHUC"].ToString() + "\n" + "Bảng giá : " + dr1["TENBANGGIA"].ToString() + "\n" + "Số ghế bán được  : " + iTongsoghe_ban.ToString() + "/" + iTongsoghe.ToString();
                        btn.Width = int.Parse(dr1["THOILUONG"].ToString());
                        btn.Height = 96;
                        foreach (LabelControl lblGio in pnlGio.Controls)
                        {
                            if (lblGio.Text == dr1["BATDAU"].ToString().Substring(0, 2))
                            {
                                btn.Location = new Point(lblGio.Location.X + int.Parse(dr1["BATDAU"].ToString().Substring(3, 2)), 0);
                                break;
                            }
                        }
                        if (!bool.Parse(dr1["SUDUNG"].ToString()))
                        {
                            btn.Font = btnPhimkosudung.Appearance.Font;
                        }
                        fpnlSub.Controls.Add(btn);
                    }

                    PanelControl pnl = new PanelControl();
                    pnl.Dock = DockStyle.Top;
                    pnl.Height = 100;
                    pnl.Controls.Add(fpnlSub);
                    pnl.Controls.Add(pnlSub);
                    pnlListPhong.Controls.Add(pnl);
                }
                splashScreenManager1.CloseWaitForm();
            }
            else
            {
                gridControl1.Visible = true ;
            }
            string sql = string.Empty;
            try
            {
                sql += "Select A.MALICHCHIEU,B.TENPHIM,A.NGAYCHIEU,A.BATDAU,A.KETTHUC,A.SUDUNG,C.TENPHONG,D.TENDIADIEM,E.TENCHATLUONGPHIM,F.TENBANGGIA,G.TENNHAPHATHANH" + "\n";
                sql += "From LICHCHIEU A , DM_PHIM B , DM_PHONG C,DM_DIADIEM D,DM_CHATLUONGPHIM E,BANGGIA F,DM_NHAPHATHANH G" + "\n";
                sql += "Where A.MAPHIM = B.MAPHIM" + "\n";
                sql += "And B.NSX = G.MANHAPHATHANH" + "\n";
                sql += "And A.MAPHONG = C.MAPHONG" + "\n";
                sql += "And C.MARAP = D.MADIADIEM" + "\n";
                sql += "And A.MACHATLUONGPHIM = E.MACHATLUONGPHIM" + "\n";
                sql += "And A.MABANGGIA =F.MABANGGIA" + "\n";
                sql += "And A.NGAYCHIEU BETWEEN  " + clsMain.SQLString(dtpTungay.Text.ToString()) + " And " + clsMain.SQLString(dtpDenngay.Text.ToString()) + "\n";
                sql += "And A.XOA<>1  " + "\n";

                if (optGroup.SelectedIndex == 0)//Rạp
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql += "And D.MADIADIEM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " D.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or D.MADIADIEM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 1)//phim
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql += "And A.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }
                else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                {
                    //if (cboDATA.EditValue.ToString() != "0")
                    //{
                    //    sql += "And G.MANHAPHATHANH= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
                    //}
                    string sqlTemp = "";
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Checked)
                        {
                            DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                            if (dr.Length > 0)
                            {
                                if (sqlTemp == "")
                                {
                                    sqlTemp += " G.MANHAPHATHANH= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                                else
                                {
                                    sqlTemp += " Or G.MANHAPHATHANH= " + clsMain.SQLString(dr[0]["MA"].ToString());
                                }
                            }
                        }
                    }
                    sql += "And ( " + sqlTemp + ")" + "\n";
                }
                sql += "Order By D.TENDIADIEM,C.TENPHONG,A.NGAYCHIEU, A.BATDAU" + "\n";

                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.ActiveFilterString = "[SUDUNG] = True";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    DataColumn colTYPE = new DataColumn();
                    colTYPE.ColumnName = "TYPE";
                    colTYPE.DataType = System.Type.GetType("System.String");
                    if (optGroup.SelectedIndex == 0)//Rạp
                    {
                        colTYPE.DefaultValue = "Rạp";
                    }
                    else if (optGroup.SelectedIndex == 1)//phim
                    {
                        colTYPE.DefaultValue = "Phim";
                    }
                    else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                    {
                        colTYPE.DefaultValue = "Nhà phát hành";
                    }

                    DataColumn colNAMETYPE = new DataColumn();
                    colNAMETYPE.ColumnName = "NAMETYPE";
                    colNAMETYPE.DataType = System.Type.GetType("System.String");
                   // colNAMETYPE.DefaultValue = cboDATA.Text;
                    bool bAll = true;
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Unchecked)
                        {
                            bAll = false;
                            break;
                        }
                    }
                    if (bAll)
                    {
                        colNAMETYPE.DefaultValue = "Tất cả";
                    }
                    else
                    {
                        colNAMETYPE.DefaultValue = cboDATA.Text;
                    }

                    Bitmap bmp = new Bitmap(pnlMain.Width, pnlMain.Height);
                    pnlMain.DrawToBitmap(bmp, pnlMain.Bounds);

                    DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
                    colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
                    colIMAGE.AllowDBNull = true;
                    colIMAGE.Caption = "IMAGE";
                    colIMAGE.DefaultValue = cls_KP_RES.ImageToByteArray(bmp);

                   

                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TYPE", dt) == false)
                        dt.Columns.Add(colTYPE);
                    if (ContainColumn("NAMETYPE", dt) == false)
                        dt.Columns.Add(colNAMETYPE);
                    if (ContainColumn("IMAGE", dt) == false)
                        dt.Columns.Add(colIMAGE);

                    if (optGroup1.SelectedIndex == 0)//Rạp
                    {
                        dt.DefaultView.Sort = "NGAYCHIEU ASC,TENPHONG ASC , BATDAU ASC ";
                        dt = dt.DefaultView.ToTable();
                    }
                    else if (optGroup1.SelectedIndex == 1)//phim
                    {
                        dt.DefaultView.Sort = "TENPHIM ASC,NGAYCHIEU ASC , BATDAU ASC ";
                        dt = dt.DefaultView.ToTable();
                    }
                    //Chỉ in phim con đang su dung
                    string sFilter = "SUDUNG=1";
                    dt.DefaultView.RowFilter = sFilter;
                    dt = dt.DefaultView.ToTable();
                   
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    if (optGroup1.SelectedIndex == 0)//Rạp
                    {
                        frm.ma = "0";
                    }
                    else if (optGroup1.SelectedIndex == 1)
                    {
                        frm.ma = "1";
                    }
                    else
                    {
                        frm.ma = "2";
                    }
                    frm.dtSource = dt;
                    frm.Mode = 32;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    throw new Exception("Chưa chọn phiếu cần in");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                btnPhimkosudung.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = true ;
                btnPhimkosudung.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}