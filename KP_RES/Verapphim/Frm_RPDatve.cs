﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Collections;

namespace KP_RES
{
    public partial class Frm_RPDatve : DevExpress.XtraEditors.XtraForm
    {
        
        public Frm_RPDatve()
        {
            InitializeComponent();
            LoadThoigianngungbanve();
        }

        private void Frm_RPDatve_Load(object sender, EventArgs e)
        {
            btnChonSuatChieu_Click(null, null);
            if (cls_KP_RES.Mode != 2)//ionah
            {
                btnThanhToan.Enabled = false;
                MADAILY.Visible = false;
                TEN.Visible = false;
                CHIETKHAU.Visible = false;
                THANHTIEN.Visible = false;
                THANHTOAN.Visible = false;
                CONLAI.Visible = false;
            }
        }

        private void Frm_RPDatve_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnDong_Click(null, null);
            }
        }

        int iThoigianngungbanve = 0;
        private void LoadThoigianngungbanve()
        {
            if (cls_KP_RES.iThoigianngungbanve == -1)
            {
                string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='THOIGIANNGUNGBANVE'";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in dt.Rows)
                {
                    iThoigianngungbanve = int.Parse(dr["GIATRI"].ToString());
                    cls_KP_RES.iThoigianngungbanve = iThoigianngungbanve;
                }
            }
            else
            {
                iThoigianngungbanve = cls_KP_RES.iThoigianngungbanve;
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            btnChonlaighe_Click(null, null);    
            this.Close();
        }

        private void btnChonSuatChieu_Click(object sender, EventArgs e)
        {
            //Ẩn / hiện chổ chọn rạp phim ngày giờ chon nó rộng màn hình
            if (panelControl23.Visible == false)
            {
                panelControl23.Visible = true;
                panelControl27.Visible = true;
                panelControl37.Visible = true;
                panelControl93.Visible = true;
                pnlDanhSachDatVe.Visible = false;
                pnlSodo.Dock = DockStyle.Fill;
            }
            else
            {
                panelControl23.Visible = false;
                panelControl27.Visible = false;
                panelControl37.Visible = false;
                panelControl93.Visible = false;
                pnlDanhSachDatVe.Visible = true;
                pnlDanhSachDatVe.Dock = DockStyle.Fill;
                LoaddataGridView();
            }
            if (panelControl23.Visible)
            {
                if (pnlSodo.Controls.Count > 1)
                {
                    pnlSodo.Visible = false;
                    while (pnlSodo.Controls.Count > 0)
                    {
                        pnlSodo.Controls[0].Dispose();
                    }
                }
            }
            this.Refresh();
        }

        private void btnDSDV_Click(object sender, EventArgs e)
        {
            panelControl23.Visible = false;
            panelControl27.Visible = false;
            panelControl37.Visible = false;
            panelControl93.Visible = false;
            pnlSodo.Visible = false;

            pnlDanhSachDatVe.Visible = true;
            pnlDanhSachDatVe.Dock = DockStyle.Fill;

            LoaddataGridView();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void LoaddataGridView()
        {
            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectDanhSachDatVe");
            gridControl1.DataSource = myDT;
        }

        private String sMaBarcode = "";
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            sMaBarcode = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MABARCODE").ToString();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (sMaBarcode != "")
            {
                Frm_ThanhToanDatVe sFrm = new Frm_ThanhToanDatVe();
                sFrm.sMa = sMaBarcode;
                sFrm.ShowDialog();
                LoaddataGridView();
            }
            
        }

        //Mode : Phim - Rạp  - Ngày - Giờ
        private void lblPhim1_Click(object sender, EventArgs e)
        {
            if (panelControl23.Visible)
            {
                flowLayoutPanel10.Controls.Clear();
                flowLayoutPanel13.Controls.Clear();
                flowLayoutPanel11.Controls.Clear();
                flowLayoutPanel39.Controls.Clear(); 
                
                if (pnlSodo.Controls.Count > 1)
                {
                    pnlSodo.Visible = false;
                    while (pnlSodo.Controls.Count > 0)
                    {
                        pnlSodo.Controls[0].Dispose();
                    }
                    pnlSodo.Visible = true;
                }

                panelControl27.Location = new Point(0, 78);//Phim
                panelControl23.Location = new Point(0, 127);//Rạp
                panelControl37.Location = new Point(0, 176);//Ngày
                panelControl93.Location = new Point(0, 225);//Giờ

                panelControl27.BringToFront();//Phim
                panelControl23.BringToFront();//Rạp
                panelControl37.BringToFront();//Ngày
                panelControl93.BringToFront();//Giờ

                this.Refresh();

                string sSQL = "";
                sSQL += "Select MAPHIM,TENPHIM From DM_PHIM Where MAPHIM IN(" + "\n";
                sSQL += "Select LC.MAPHIM FROM LICHCHIEU LC Where   LC.SUDUNG=1 AND LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + ")" + "\n";
                sSQL += "Order By TENPHIM" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in dt.Rows)
                {
                    SimpleButton btn1 = new SimpleButton();
                    btn1.Name = "P" + dr["MAPHIM"].ToString();
                    btn1.Text = dr["TENPHIM"].ToString();
                    btn1.Font = btnRap333.Font;
                    btn1.Width = 177;
                    btn1.Height = 44;
                    btn1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn1.Click += new EventHandler(btn11_Click);
                    flowLayoutPanel13.Controls.Add(btn1);
                }
                if (dt.Rows.Count <= 5)
                {
                    panelControl27.Height = 49;
                }
                else if (dt.Rows.Count <= 10)
                {
                    panelControl27.Height = 103;
                }
                else if (dt.Rows.Count <= 15)
                {
                    panelControl27.Height = 155;
                }
                else if (dt.Rows.Count <= 20)
                {
                    panelControl27.Height = 207;
                }
                else
                {
                    panelControl27.Height = 260;
                }
            }
        }
      
        private void btn11_Click(object sender, EventArgs e)
        {
            flowLayoutPanel10.Controls.Clear();
            flowLayoutPanel11.Controls.Clear();
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel13.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sPhim = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblPhim.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select MADIADIEM,TENDIADIEM From DM_DIADIEM Where MADIADIEM IN(" + "\n";
                    sSQL += "Select DD.MADIADIEM FROM LICHCHIEU LC, DM_PHONG P,DM_DIADIEM DD Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1  AND P.MARAP = DD.MADIADIEM AND LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + ")" + "\n";
                    sSQL += "Order By TENDIADIEM" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn2 = new SimpleButton();
                        btn2.Name = "D" + dr["MADIADIEM"].ToString().Trim();
                        btn2.Text = dr["TENDIADIEM"].ToString().Trim();
                        btn2.Font = btnRap333.Font;
                        btn2.Width = 177;
                        btn2.Height = 44;
                        btn2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn2.Click += new EventHandler(btn22_Click);
                        flowLayoutPanel10.Controls.Add(btn2);
                    }
                    if (dt.Rows.Count <= 5)
                    {
                        panelControl26.Height = 49;
                    }
                    else if (dt.Rows.Count <= 10)
                    {
                        panelControl26.Height = 103;
                    }
                    else if (dt.Rows.Count <= 15)
                    {
                        panelControl26.Height = 155;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl26.Height = 207;
                    }
                    else
                    {
                        panelControl26.Height = 260;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }
      
        private void btn22_Click(object sender, EventArgs e)
        {
            flowLayoutPanel11.Controls.Clear();
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel10.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sRap = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblRap.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select Distinct LC.NGAYCHIEU FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1 AND LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "AND P.MARAP =" + clsMain.SQLString(sRap) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + "\n";
                    sSQL += "Order By LC.NGAYCHIEU" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn3 = new SimpleButton();
                        btn3.Name = dr["NGAYCHIEU"].ToString();
                        btn3.Text = DateTime.Parse(dr["NGAYCHIEU"].ToString()).Day + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Month + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Year;
                        btn3.Font = btnRap333.Font;
                        btn3.Width = 86;
                        btn3.Height = 44;
                        btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn3.Click += new EventHandler(btn33_Click);
                        flowLayoutPanel11.Controls.Add(btn3);
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }
        
        private void btn33_Click(object sender, EventArgs e)
        {
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel11.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sNgay = btn.Text;
                    btn.Appearance.ForeColor = Color.Red;
                    lblNgay.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select LC.MALICHCHIEU,LC.NGAYCHIEU, LC.BATDAU FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG AND LC.SUDUNG=1 AND LC.NGAYCHIEU =" + clsMain.SQLString(sNgay) + "AND P.MARAP =" + clsMain.SQLString(sRap) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + "\n";
                    sSQL += "Order By LC.BATDAU" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn3 = new SimpleButton();
                        DateTime dttime = DateTime.Parse(dr["BATDAU"].ToString());
                        if (((DateTime)dr["NGAYCHIEU"]).Day == DateTime.Today.Day && dttime.AddMinutes(iThoigianngungbanve) < DateTime.Now)
                        {
                            btn3.Visible = false;
                        }
                        btn3.Name = dr["MALICHCHIEU"].ToString();
                        btn3.Text = dr["BATDAU"].ToString();
                        btn3.Font = btnRap333.Font;
                        btn3.Width = 86;
                        btn3.Height = 44;
                        btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn3.Click += new EventHandler(btn3_Click);
                        flowLayoutPanel39.Controls.Add(btn3);
                    }
                    if (dt.Rows.Count <= 10)
                    {
                        panelControl93.Height = 49;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl93.Height = 103;
                    }
                    else if (dt.Rows.Count <= 30)
                    {
                        panelControl93.Height = 155;
                    }
                    else 
                    {
                        panelControl93.Height = 207;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }

        //Mode : Ngày - Rạp  - Phim - Giờ
        private void lblNgay1_Click(object sender, EventArgs e)
        {
            if (panelControl23.Visible)
            {
                flowLayoutPanel10.Controls.Clear();
                flowLayoutPanel13.Controls.Clear();
                flowLayoutPanel11.Controls.Clear();
                flowLayoutPanel39.Controls.Clear(); 
                if (pnlSodo.Controls.Count > 1)
                {
                    pnlSodo.Visible = false;
                    while (pnlSodo.Controls.Count > 0)
                    {
                        pnlSodo.Controls[0].Dispose();
                    }
                    pnlSodo.Visible = true;
                }

                panelControl37.Location = new Point(0, 78);//Ngày
                panelControl23.Location = new Point(0, 127);//Rạp
                panelControl27.Location = new Point(0, 176);//Phim
                panelControl93.Location = new Point(0, 225);//Giờ

                panelControl37.BringToFront();//Ngày
                panelControl23.BringToFront();//Rạp
                panelControl27.BringToFront();//Phim
                panelControl93.BringToFront();//Giờ

                this.Refresh();

                string sSQL = "";
                sSQL += "Select Distinct LC.NGAYCHIEU FROM LICHCHIEU LC Where  LC.SUDUNG=1 AND  LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
                sSQL += "Order By LC.NGAYCHIEU" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in dt.Rows)
                {
                    SimpleButton btn1 = new SimpleButton();
                    btn1.Name = dr["NGAYCHIEU"].ToString();
                    btn1.Text = DateTime.Parse(dr["NGAYCHIEU"].ToString()).Day + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Month + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Year;
                    btn1.Font = btnRap333.Font;
                    btn1.Width = 86;
                    btn1.Height = 44;
                    btn1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn1.Click += new EventHandler(btn111_Click);
                    flowLayoutPanel11.Controls.Add(btn1);
                }
            }
        }
        
        private void btn111_Click(object sender, EventArgs e)
        {
            flowLayoutPanel10.Controls.Clear();
            flowLayoutPanel13.Controls.Clear();
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel11.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sNgay = btn.Text;
                    btn.Appearance.ForeColor = Color.Red;
                    lblNgay.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select MADIADIEM,TENDIADIEM From DM_DIADIEM Where MADIADIEM IN(" + "\n";
                    sSQL += "Select DD.MADIADIEM FROM LICHCHIEU LC, DM_PHONG P,DM_DIADIEM DD Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1 AND P.MARAP = DD.MADIADIEM AND LC.NGAYCHIEU =" + clsMain.SQLString(sNgay) + ")" + "\n";
                    sSQL += "Order By TENDIADIEM" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn2 = new SimpleButton();
                        btn2.Name = "D" + dr["MADIADIEM"].ToString().Trim();
                        btn2.Text = dr["TENDIADIEM"].ToString().Trim();
                        btn2.Font = btnRap333.Font;
                        btn2.Width = 177;
                        btn2.Height = 44;
                        btn2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn2.Click += new EventHandler(btn222_Click);
                        flowLayoutPanel10.Controls.Add(btn2);
                    }
                    if (dt.Rows.Count <= 5)
                    {
                        panelControl26.Height = 49;
                    }
                    else if (dt.Rows.Count <= 10)
                    {
                        panelControl26.Height = 103;
                    }
                    else if (dt.Rows.Count <= 15)
                    {
                        panelControl26.Height = 155;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl26.Height = 207;
                    }
                    else
                    {
                        panelControl26.Height = 260;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }
        
        private void btn222_Click(object sender, EventArgs e)
        {
            flowLayoutPanel13.Controls.Clear();
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }
            foreach (SimpleButton btn in flowLayoutPanel10.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sRap = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblRap.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select MAPHIM,TENPHIM From DM_PHIM Where MAPHIM IN(" + "\n";
                    sSQL += "Select LC.MAPHIM FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1  AND LC.NGAYCHIEU =" + clsMain.SQLString(sNgay) + "AND P.MARAP =" + clsMain.SQLString(sRap) + ") " + "\n";
                    sSQL += "Order By TENPHIM" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn3 = new SimpleButton();
                        btn3.Name = "P" + dr["MAPHIM"].ToString();
                        btn3.Text = dr["TENPHIM"].ToString();
                        btn3.Font = btnRap333.Font;
                        btn3.Width = 177;
                        btn3.Height = 44;
                        btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn3.Click += new EventHandler(btn333_Click);
                        flowLayoutPanel13.Controls.Add(btn3);
                    }
                    if (dt.Rows.Count <= 5)
                    {
                        panelControl27.Height = 49;
                    }
                    else if (dt.Rows.Count <= 10)
                    {
                        panelControl27.Height = 103;
                    }
                    else if (dt.Rows.Count <= 15)
                    {
                        panelControl27.Height = 155;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl27.Height = 207;
                    }
                    else
                    {
                        panelControl27.Height = 260;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }
        
        private void btn333_Click(object sender, EventArgs e)
        {
            flowLayoutPanel39.Controls.Clear(); 
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel13.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sPhim = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblPhim.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select LC.MALICHCHIEU,LC.NGAYCHIEU, LC.BATDAU FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG AND LC.SUDUNG=1 AND LC.NGAYCHIEU =" + clsMain.SQLString(sNgay) + "AND P.MARAP =" + clsMain.SQLString(sRap) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + "\n";
                    sSQL += "Order By LC.BATDAU" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn3 = new SimpleButton();
                        DateTime dttime = DateTime.Parse(dr["BATDAU"].ToString());
                        if (((DateTime)dr["NGAYCHIEU"]).Day == DateTime.Today.Day && dttime.AddMinutes(iThoigianngungbanve) < DateTime.Now)
                        {
                            btn3.Visible = false;
                        }
                        btn3.Name = dr["MALICHCHIEU"].ToString();
                        btn3.Text = dr["BATDAU"].ToString();
                        btn3.Font = btnRap333.Font;
                        btn3.Width = 86;
                        btn3.Height = 44;
                        btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn3.Click += new EventHandler(btn3_Click);
                        flowLayoutPanel39.Controls.Add(btn3);
                    }
                    if (dt.Rows.Count <= 10)
                    {
                        panelControl93.Height = 49;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl93.Height = 103;
                    }
                    else if (dt.Rows.Count <= 30)
                    {
                        panelControl93.Height = 155;
                    }
                    else
                    {
                        panelControl93.Height = 207;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }

        //Mode : Rạp - Phim  - Ngày - Giờ
        private void lblRap1_Click(object sender, EventArgs e)
        {
            if (panelControl23.Visible)
            {
                flowLayoutPanel10.Controls.Clear();
                flowLayoutPanel13.Controls.Clear();
                flowLayoutPanel11.Controls.Clear();
                flowLayoutPanel39.Controls.Clear();
                
                if (pnlSodo.Controls.Count > 1)
                {
                    pnlSodo.Visible = false;
                    while (pnlSodo.Controls.Count > 0)
                    {
                        pnlSodo.Controls[0].Dispose();
                    }
                    pnlSodo.Visible = true;
                }
                panelControl23.Location = new Point(0, 78);//Rạp
                panelControl27.Location = new Point(0, 127);//Phim
                panelControl37.Location = new Point(0, 176);//Ngày
                panelControl93.Location = new Point(0, 225);//Giờ

                panelControl23.BringToFront();//Rạp
                panelControl27.BringToFront();//Phim
                panelControl37.BringToFront();//Ngày
                panelControl93.BringToFront();//Giờ

                this.Refresh();
                
                string sSQL = "";
                sSQL += "Select MADIADIEM,TENDIADIEM From DM_DIADIEM Where SUDUNG=1 Order By TENDIADIEM" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in dt.Rows)
                {
                    SimpleButton btn3 = new SimpleButton();
                    btn3.Name = "R" + dr["MADIADIEM"].ToString();
                    btn3.Text = dr["TENDIADIEM"].ToString();
                    btn3.Font = btnRap333.Font;
                    btn3.Width = 177;
                    btn3.Height = 44;
                    btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn3.Click += new EventHandler(btn_Click);
                    flowLayoutPanel10.Controls.Add(btn3);
                }
                if (dt.Rows.Count <= 5)
                {
                    panelControl27.Height = 49;
                }
                else if (dt.Rows.Count <= 10)
                {
                    panelControl27.Height = 103;
                }
                else if (dt.Rows.Count <= 15)
                {
                    panelControl27.Height = 155;
                }
                else if (dt.Rows.Count <= 20)
                {
                    panelControl27.Height = 207;
                }
                else
                {
                    panelControl27.Height = 260;
                }
            }
        }
        
        string sRap = "";
        private void btn_Click(object sender, EventArgs e)
        {
            flowLayoutPanel13.Controls.Clear();
            flowLayoutPanel11.Controls.Clear();
            flowLayoutPanel39.Controls.Clear();
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel10.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sRap = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblRap.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select MAPHIM,TENPHIM From DM_PHIM Where MAPHIM IN(" + "\n";
                    sSQL += "Select LC.MAPHIM FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1 AND LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "AND P.MARAP =" + clsMain.SQLString(sRap) + ") " + "\n";
                    sSQL += "Order By TENPHIM" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn1 = new SimpleButton();
                        btn1.Name = "P" + dr["MAPHIM"].ToString();
                        btn1.Text = dr["TENPHIM"].ToString();
                        btn1.Font = btnRap333.Font;
                        btn1.Width = 177;
                        btn1.Height = 44;
                        btn1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn1.Click += new EventHandler(btn1_Click);
                        flowLayoutPanel13.Controls.Add(btn1);
                    }
                    if (dt.Rows.Count <= 5)
                    {
                        panelControl27.Height = 49;
                    }
                    else if (dt.Rows.Count <= 10)
                    {
                        panelControl27.Height = 103;
                    }
                    else if (dt.Rows.Count <= 15)
                    {
                        panelControl27.Height = 155;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl27.Height = 207;
                    }
                    else
                    {
                        panelControl27.Height = 260;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }

        string sPhim = "";
        private void btn1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel11.Controls.Clear();
            flowLayoutPanel39.Controls.Clear(); 
           
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel13.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sPhim = btn.Name.Substring(1, btn.Name.Length - 1);
                    btn.Appearance.ForeColor = Color.Red;
                    lblPhim.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select Distinct LC.NGAYCHIEU FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG  AND LC.SUDUNG=1  AND LC.NGAYCHIEU >=" + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "AND P.MARAP =" + clsMain.SQLString(sRap) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + "\n";
                    sSQL += "Order By LC.NGAYCHIEU" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn2 = new SimpleButton();
                        btn2.Name = dr["NGAYCHIEU"].ToString().Trim();
                        btn2.Text = DateTime.Parse(dr["NGAYCHIEU"].ToString()).Day + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Month + "/" + DateTime.Parse(dr["NGAYCHIEU"].ToString()).Year;
                        btn2.Font = btnRap333.Font;
                        btn2.Width = 86;
                        btn2.Height = 44;
                        btn2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn2.Click += new EventHandler(btn2_Click);
                        flowLayoutPanel11.Controls.Add(btn2);
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }

        string sNgay = "";
        private void btn2_Click(object sender, EventArgs e)
        {
            flowLayoutPanel39.Controls.Clear(); 
         
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel11.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sNgay = btn.Text;
                    btn.Appearance.ForeColor = Color.Red;
                    lblNgay.Text = btn.Text;
                    string sSQL = "";
                    sSQL += "Select LC.MALICHCHIEU,LC.NGAYCHIEU, LC.BATDAU FROM LICHCHIEU LC, DM_PHONG P Where LC.MAPHONG = P.MAPHONG AND LC.SUDUNG=1 AND LC.NGAYCHIEU =" + clsMain.SQLString(sNgay) + "AND P.MARAP =" + clsMain.SQLString(sRap) + "AND LC.MAPHIM =" + clsMain.SQLString(sPhim) + "\n";
                    sSQL += "Order By LC.BATDAU" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        SimpleButton btn3 = new SimpleButton();
                        DateTime dttime = DateTime.Parse(dr["BATDAU"].ToString());
                        if (((DateTime)dr["NGAYCHIEU"]).Day == DateTime.Today.Day && dttime.AddMinutes(iThoigianngungbanve) < DateTime.Now)
                        {
                            btn3.Visible = false;
                        }
                        btn3.Name = dr["MALICHCHIEU"].ToString();
                        btn3.Text = dr["BATDAU"].ToString();
                        btn3.Font = btnRap333.Font;
                        btn3.Width = 86;
                        btn3.Height = 44;
                        btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btn3.Click += new EventHandler(btn3_Click);
                        flowLayoutPanel39.Controls.Add(btn3);
                    }
                    if (dt.Rows.Count <= 10)
                    {
                        panelControl93.Height = 49;
                    }
                    else if (dt.Rows.Count <= 20)
                    {
                        panelControl93.Height = 103;
                    }
                    else if (dt.Rows.Count <= 30)
                    {
                        panelControl93.Height = 155;
                    }
                    else
                    {
                        panelControl93.Height = 207;
                    }
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
        }
        
        string sGio = "";
        string sLichchieu = "";
        private void btn3_Click(object sender, EventArgs e)
        {
            if (pnlSodo.Controls.Count > 1)
            {
                pnlSodo.Visible = false;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Visible = true;
            }

            foreach (SimpleButton btn in flowLayoutPanel39.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    sLichchieu =btn.Name;
                    sGio = btn.Text;
                    btn.Appearance.ForeColor = Color.Red;
                    lblGio.Text = btn.Text;
                }
                else
                {
                    btn.Appearance.ForeColor = Color.Black;
                }
            }
            //Ẩn chổ chọn rạp phim ngày giờ chon nó rộng màn hình
            panelControl23.Visible = false;
            panelControl27.Visible = false;
            panelControl37.Visible = false;
            panelControl93.Visible = false;
            panel12.Visible = true;
            this.Refresh();
            //Load thời gian
            LoadThoigian();
            //Load bảng giá
            LoadBanggia();
            //Load so do ghe
            LoadSoDoGhe(true);
        }

        string MATHOIGIAN;
        private void LoadThoigian()
        {
            if (cls_KP_RES.sMaThoiGian == "")
            {
                string sSQL = "Select  MATHOIGIAN,TENTHOIGIAN,BATDAU,KETTHUC From DM_THOIGIAN" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                MATHOIGIAN = dt.Rows[0]["MATHOIGIAN"].ToString();
                cls_KP_RES.sMaThoiGian = MATHOIGIAN;
            }
            else
            {
                MATHOIGIAN = cls_KP_RES.sMaThoiGian;
            }
        }

        DataTable dtBanggia = new DataTable();
        private void LoadBanggia()
        {
            string sSQL = "Exec SP_LOADBANGGIA " + clsMain.SQLString(sLichchieu);
            dtBanggia = clsMain.ReturnDataTable(sSQL);

            DataTable dt = dtBanggia.DefaultView.ToTable(true, "MALOAIVE", "TENLOAIVE", "MACDINH");
            dt.DefaultView.Sort = "MACDINH DESC,TENLOAIVE ASC";
            dt = dt.DefaultView.ToTable();
            repositoryItemLookUpEdit1.DataSource = dt;

            DataTable dt1 = dt.Copy();
            cboLoaive.Properties.DataSource = dt1;
        }

        private void LoadSoDoGhe(bool bnew)
        {
            string sSQL = "";
            sSQL += "Exec SP_LOADSODOGHE " + clsMain.SQLString(sLichchieu);
            DataTable dtSodoghe = new DataTable();
            dtSodoghe = clsMain.ReturnDataTable(sSQL);

            if (bnew == true)
            {
                lblRap1.Location = lblRap.Location;
                lblPhim1.Location = lblPhim.Location;
                lblNgay1.Location = lblNgay.Location;
                lblGio1.Location = lblGio.Location;

                pnlSodo.Visible = false;
                this.Cursor = Cursors.WaitCursor;
                pnlSodo.Controls.Clear();
                
                foreach (DataRow dr in dtSodoghe.Rows)
                {
                    TextBox txt = new TextBox();
                    txt.Name = dr["MAVE"].ToString();
                    txt.Text = dr["TENGHE"].ToString();
                    txt.Location = new Point(int.Parse(dr["X"].ToString()), int.Parse(dr["Y"].ToString()));
                    txt.Width = int.Parse(dr["W"].ToString());
                    txt.Height = int.Parse(dr["H"].ToString());
                    txt.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAUSAC"].ToString());
                    if (Boolean.Parse(dr["TRANGTHAI"].ToString()))
                    {
                        if (dr["CONLAI"].ToString() == "0")
                        {
                            txt.ForeColor = Color.White;
                            txt.BackColor = Color.DarkGray;
                            txt.Enabled = false;
                        }
                        else
                        {
                            if (dr["TENHANHKHACH"].ToString() == "")
                            {
                                txt.ForeColor = Color.Black;
                            }
                            else
                            {
                                if (dr["DATVE_THANHTOAN"].ToString() == "True")
                                {
                                    txt.ForeColor = Color.White;
                                    txt.BackColor = Color.DarkGray;
                                    txt.Enabled = false;
                                }
                                else
                                {
                                    txt.ForeColor = Color.Blue;
                                }
                            }
                        }
                    }
                    else
                    {
                        txt.ForeColor = Color.White;
                    }
                    txt.BorderStyle = BorderStyle.None;
                    txt.Cursor = Cursors.Hand;
                    txt.TextAlign = HorizontalAlignment.Center;
                    txt.Multiline = true;
                    txt.ReadOnly = true;

                    txt.Click += new EventHandler(textBox_Click);

                    pnlSodo.Controls.Add(txt);
                }
                this.Cursor = Cursors.Default;
                pnlSodo.Visible = true;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    for (int i = 0; i < dtSodoghe.Rows.Count; i++)
                    {
                        if (Boolean.Parse(dtSodoghe.Rows[i]["TRANGTHAI"].ToString()))
                        {
                            if (dtSodoghe.Rows[i]["CONLAI"].ToString() == "0")
                            {
                                pnlSodo.Controls[i].ForeColor = Color.White;
                                pnlSodo.Controls[i].BackColor = Color.DarkGray;
                                pnlSodo.Controls[i].Enabled = false;
                            }
                            else
                            {
                                if (dtSodoghe.Rows[i]["TENHANHKHACH"].ToString() == "")
                                {
                                    pnlSodo.Controls[i].ForeColor = Color.Black;
                                }
                                else
                                {
                                    if (dtSodoghe.Rows[i]["DATVE_THANHTOAN"].ToString() == "True")
                                    {
                                        pnlSodo.Controls[i].ForeColor = Color.White;
                                        pnlSodo.Controls[i].BackColor = Color.DarkGray;
                                        pnlSodo.Controls[i].Enabled = false;
                                    }
                                    else
                                    {
                                        pnlSodo.Controls[i].ForeColor = Color.Blue;
                                    }
                                    pnlSodo.Controls[i].ForeColor = Color.Blue;
                                }
                            }
                        }
                        else
                        {
                            pnlSodo.Controls[i].ForeColor = Color.White;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                this.Cursor = Cursors.Default;
            }
        }

        ArrayList danhsachve2 = new ArrayList();
        private void textBox_Click(object sender, EventArgs e)
        {
            Control txt = this.ActiveControl;
            if (txt.Text.Length > 1 && txt.Text.Length < 4)
            {
                if ((txt.Parent == pnlSodo) && (txt.ForeColor == Color.White)) //Chọn ghế
                {
                    string MaBarCode = cls_KP_RES.CreateBarcode("VE");
                    string sSQL = "Exec SP_ChonGhe " + clsMain.SQLString(int.Parse(txt.Name).ToString()) + " , " + clsMain.SQLString(clsGlobal.gsUserID) + " , " + clsMain.SQLString(txt.Text.ToString()) + " , " + clsMain.SQLString(MaBarCode) + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows[0][0].ToString() == "0")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Ghế đã bán . Chọn lại ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadSoDoGhe(false);
                        return;
                    }
                    else
                    {
                        danhsachve2.Add(txt.Name);
                        LoadSoDoGhe(false);
                    }
                }
                else if ((txt.Parent == pnlSodo) && (txt.ForeColor == Color.Black)) //Trả lại ghế đã chọn
                {
                    //05/04/2017 thêm cho biết ghế do ai chọn
                    string sSQL = "Exec SP_BoChonGhe_New " + clsMain.SQLString(int.Parse(txt.Name).ToString()) + " , " + clsMain.SQLString(clsGlobal.gsUserID) + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows[0][0].ToString() != "1")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Ghế đã được chọn bởi : " + dt.Rows[0][0].ToString() + " . Bạn không bỏ chọn ghế này được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        danhsachve2.Remove(txt.Name);
                        LoadSoDoGhe(false);
                    }
                }
            }
            LoadDanhSachVe();
        }

        private void LoadDanhSachVe()
        {
            DataTable dt = new DataTable();
            gridControl.DataSource = dt;

            if (danhsachve2.Count <= 0)
            {
                txtTongtien.Text = "0";
                txtTongghe.Text = "0";
                return;
            }

            string sSQL = "";
            sSQL += "Select A.MAVE,A.SOGHE, A.PHUTHU,A.TENHANHKHACH AS MALOAIVE,B.MALOAIGHE" + "\n";
            sSQL += "From VE A , DM_GHE B" + "\n";
            sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
            int temp = 0;
            foreach (Object obj in danhsachve2)
            {
                if (temp == 0)
                {
                    sSQL += "And ( A.MAVE= " + clsMain.SQLString(obj.ToString()) + "\n";
                    temp++;
                }
                else
                {
                    sSQL += "Or A.MAVE= " + clsMain.SQLString(obj.ToString()) + "\n";
                }
            }
            sSQL += ")" + "\n";
            sSQL += "Order by SOGHE" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            gridControl.DataSource = dt;

            //Bổ sung tự chọn loại vé
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gridView.SetRowCellValue(i, "MALOAIVE", repositoryItemLookUpEdit1.GetDataSourceValue("MALOAIVE", 0).ToString());
                    string sFilter = "";
                    sFilter += "MALOAIVE=" + clsMain.SQLString(repositoryItemLookUpEdit1.GetDataSourceValue("MALOAIVE", 0).ToString()) + "\n";
                    sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(i, "MALOAIGHE").ToString()) + "\n";
                    sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
                    dtBanggia.DefaultView.RowFilter = sFilter;
                    DataTable dt1 = dtBanggia.DefaultView.ToTable();
                    if (dt1.Rows.Count > 0)
                    {
                        gridView.SetRowCellValue(i, "PHUTHU", dt1.Rows[0]["DONGIA"].ToString());
                    }
                    gridView.UpdateTotalSummary();
                    txtTongtien.Text = PHUTHU.SummaryText;
                    txtTongghe.Text = MADIADIEM.SummaryText;
                }
            }
            catch
            {
            }
        }

        private void repositoryItemLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
            DataRowView row = editor.Properties.GetDataSourceRowByKeyValue(editor.EditValue) as DataRowView;

            string sFilter = "";
            sFilter += "MALOAIVE=" + clsMain.SQLString(row["MALOAIVE"].ToString()) + "\n";
            sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MALOAIGHE").ToString()) + "\n";
            sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
            dtBanggia.DefaultView.RowFilter = sFilter;
            DataTable dt = dtBanggia.DefaultView.ToTable();
            if (dt.Rows.Count > 0)
            {
                gridView.SetRowCellValue(gridView.FocusedRowHandle, "PHUTHU", dt.Rows[0]["DONGIA"].ToString());
            }
            
            gridView.UpdateTotalSummary();
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
        }

        private void cboLoaive_EditValueChanged(object sender, EventArgs e)
        {
            //
            string sMALOAIVE = cboLoaive.EditValue.ToString();
            for (int i = 0; i < gridView.RowCount; i++)
            {
                try
                {
                    string sFilter = "";
                    sFilter += "MALOAIVE=" + clsMain.SQLString(sMALOAIVE) + "\n";
                    sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(i, "MALOAIGHE").ToString()) + "\n";
                    sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
                    dtBanggia.DefaultView.RowFilter = sFilter;
                    DataTable dt = dtBanggia.DefaultView.ToTable();
                    if (dt.Rows.Count > 0)
                    {
                        gridView.SetRowCellValue(i, "PHUTHU", dt.Rows[0]["DONGIA"].ToString());
                        gridView.SetRowCellValue(i, "MALOAIVE", sMALOAIVE);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa có bảng giá" + "\n" + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            gridView.UpdateTotalSummary();
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
            //
        }

        private void btnChonlaighe_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            foreach (string mv in danhsachve2)
            {
                sSQL += "Update VE Set " + "\n";
                sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(mv) + "\n";
            }

            if (sSQL != "")
            {
                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                this.Cursor = Cursors.Default;

                if (bRunSQL)
                {
                    danhsachve2.Clear();
                    LoadSoDoGhe(false);
                    LoadDanhSachVe();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bỏ ghế không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
            }
        }

        private void btnInbiennhandatve_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = (DataView)gridView.DataSource;
                DataTable dt = dv.ToTable();
                string Tonggiave = "0";
                if (dt.Rows.Count == 0)
                {
                    return;
                }
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["MALOAIVE"].ToString() == "")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn loại vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        Tonggiave = (int.Parse(Tonggiave) + int.Parse(dr["PHUTHU"].ToString())).ToString();
                    }
                }

                bool bInve = false;
                PF_Datve frm = new PF_Datve();
                frm.Width = this.Width;
                frm.Height = this.Height;
                frm.dt = dt;
                frm.ShowDialog(this);
                bInve = frm.bInve;
                frm.Dispose();

                if (bInve)
                {
                    danhsachve2.Clear();
                    LoadSoDoGhe(false);
                    dt.Clear();
                    gridControl.DataSource = dt;
                    txtTongtien.Text = "0";
                    txtTongghe.Text = "0";
                }
            }
            catch
            {
            }
        }

        private void btnChonnhieughe_Click(object sender, EventArgs e)
        {
            if (pnlSodo.Controls.Count <= 0 )
            {
                return;
            }
            else
            {
                try
                {
                    int Soghe;
                    string Tenghebatdau;

                    PF_Chonnhieughe frm = new PF_Chonnhieughe();
                    frm.ShowDialog();
                    Soghe = frm.iSoghe;
                    Tenghebatdau = frm.sGhebatdau;
                    frm.Dispose();

                    if (Soghe == 0 || Tenghebatdau == "")
                    {
                        return;
                    }
                    string sSQL = "";
                    sSQL += "Select TOP " + Soghe.ToString() + "B.TENGHE  ,A.MAVE From VE A, DM_GHE B" + "\n";
                    sSQL += "Where a.MAGHE =b.MAGHE " + "\n";
                    sSQL += "And a.MALICHCHIEU =" + clsMain.SQLString(sLichchieu) + "\n";
                    sSQL += "And b.TENGHE >=" + clsMain.SQLString(Tenghebatdau) + "\n";
                    sSQL += "And LEN(B.TENGHE )>1" + "\n";
                    sSQL += "And LEN(B.TENGHE )<4" + "\n";
                    sSQL += "And A.TRANGTHAI =0" + "\n";
                    sSQL += "Order by b.TENGHE asc" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);

                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (TextBox txt in pnlSodo.Controls)
                        {
                            if (txt.Text == dr["TENGHE"].ToString() && txt.Enabled == true)
                            {
                                txt.Focus();
                                textBox_Click(null, null);
                                break;
                            }
                        }
                        btnInbiennhandatve.Focus();
                    }
                }
                catch
                {
                }
            }
        }

        private void btnLenve_Click(object sender, EventArgs e)
        {
            try
            {
                gridView.Focus();
                gridView.FocusedRowHandle = gridView.FocusedRowHandle - 1;
            }
            catch
            {
            }
        }

        private void btnXuongve_Click(object sender, EventArgs e)
        {
            try
            {
                gridView.Focus();
                gridView.FocusedRowHandle = gridView.FocusedRowHandle + 1;
            }
            catch
            {
            }
        }

        private void btnXoave_Click(object sender, EventArgs e)
        {
            string mave = "";
            if (gridView.RowCount > 0)
            {
                mave = gridView.GetFocusedRowCellValue("MAVE").ToString();
            }
            if (mave != "")
            {
                string sSQL = "";
                sSQL += "Update VE Set " + "\n";
                sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(int.Parse(mave).ToString()) + "\n";

                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                this.Cursor = Cursors.Default;
                if (bRunSQL)
                {
                    danhsachve2.Remove(mave);
                    LoadSoDoGhe(false);
                    LoadDanhSachVe();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bỏ ghế không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void timerDongho_Tick(object sender, EventArgs e)
        {
            timerDongho.Enabled = false;
            lbNgay.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lbGio.Text = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00");
            lbgiay.Text = DateTime.Now.Second.ToString("00");
            timerDongho.Enabled = true ;
        }

        private void btnHuydatve_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
            frm1.ShowDialog();
            check = frm1.kt;
            frm1.Dispose();
            if (!check)
            {
                return;
            }
            if (sMaBarcode != "")
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận hủy đặt vé cho biên nhận này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    string sSQL = "";
                    sSQL = "";
                    sSQL += "Select A.MAVE,A.BIENNHAN,A.MABARCODE,A.SOGHE,A.MALOAIVE,A.DONGIA,A.TENHANHKHACH,A.DIENTHOAI,A.EMAIL,A.NGAYTAO,C.MABARCODE As MAPHIEU,C.TONGGIAVE,C.DATHANHTOAN,C.TONGSOGHE,D.MALICHCHIEU,D.NGAYCHIEU,D.BATDAU,E.TENPHONG,F.TENPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
                    sSQL += "From VE A , DM_GHE B , BIENNHAN C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN H,SYS_USER I" + "\n";
                    sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                    sSQL += "And A.BIENNHAN = C.MABARCODE" + "\n";
                    sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                    sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                    sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                    sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                    sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                    sSQL += "And D.NGAYCHIEU >= " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
                    sSQL += "And C.TRANGTHAI =" + clsMain.SQLString("0") + "\n";
                    sSQL += "And C.DATHANHTOAN =" + clsMain.SQLString("0") + "\n";
                    sSQL += "And A.BIENNHAN =" + clsMain.SQLString(sMaBarcode) + "\n";
                    sSQL += "Order by A.SOGHE" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count == 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Mã code không đúng . Hoặc đã hủy . Hoặc đã lấy vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        sSQL = "";
                        foreach (DataRow dr in dt.Rows)
                        {
                            sSQL += "Update VE Set " + "\n";
                            sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                            sSQL += "TENHANHKHACH=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "DIENTHOAI=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "EMAIL=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "BIENNHAN=" + clsMain.SQLString("") + "," + "\n";
                            sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "," + "\n";
                            sSQL += "DONGIA=" + clsMain.SQLString("1") + "," + "\n";
                            sSQL += "PHUTHU=" + clsMain.SQLString("0") + "," + "\n";
                            sSQL += "DATHANHTOAN=" + clsMain.SQLString("0") + "," + "\n";
                            sSQL += "CONLAI=" + clsMain.SQLString("1") + "\n";
                            sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                            sSQL += "Update BIENNHAN Set " + "\n";
                            sSQL += "TRANGTHAI=" + clsMain.SQLString("-1") + "\n";
                            sSQL += "Where MABARCODE=" + clsMain.SQLString(dr["BIENNHAN"].ToString()) + "\n";
                        }
                        bool bRunSQL = clsMain.ExecuteSQL(sSQL);
                        if (bRunSQL)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Đã hủy đặt vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LoaddataGridView();
                        }
                    }
                }
            }
        }
    }
}