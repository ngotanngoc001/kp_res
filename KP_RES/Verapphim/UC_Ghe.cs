﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace KP_RES
{
    public partial class UC_Ghe : UserControl
    {
        public UC_Ghe()
        {
            InitializeComponent();
        }

        private void UC_Ghe_ForeColorChanged(object sender, EventArgs e)
        {
            label1.ForeColor = ((UserControl)sender).ForeColor;
            label1.Text = ((UserControl)sender).Tag.ToString();
        }

        Color color_old;
        private void UC_Ghe_BackColorChanged(object sender, EventArgs e)
        {
            Color color = ((UserControl)sender).BackColor; //Your desired colour
            if (color.Name != "Transparent")
            {
                byte r = color.R; //For Red colour
                byte g = color.G; //For Green colour
                byte b = color.B; //For Blue colour
                Bitmap bmp = new Bitmap(this.BackgroundImage);
                color_old = bmp.GetPixel(10, 10);

                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        Color gotColor = bmp.GetPixel(x, y);
                        if (gotColor == color_old)
                        {
                            gotColor = Color.FromArgb(r, g, b);
                            bmp.SetPixel(x, y, gotColor);
                        }
                    }
                }

                this.BackgroundImage = bmp;
                this.BackColor = Color.Transparent;
            }
        }
    }
}
