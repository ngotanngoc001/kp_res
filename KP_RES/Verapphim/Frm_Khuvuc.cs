﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Khuvuc : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Khuvuc()
        {
            InitializeComponent();
            LoaddataGridView();
           LoadPermission();
        }

        private void Frm_Khuvuc_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtTENvn.SelectAll();
        }

        string sma = "";

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTENvn.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From ReferenceList" + "\n";
            sSQL += "Where Category = 'Location' and ID = " + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTENvn.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into ReferenceList (ID,VNValue,ENValue,Category,OrderBy,Status)" + "\n";
                sSQL += "Values (newid(),";
                sSQL += clsMain.SQLStringUnicode(txtTENvn.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTENen.Text) + ",";
                sSQL += clsMain.SQLString("Location") + ",";
                sSQL += clsMain.SQLString(txtSTT.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update ReferenceList Set " + "\n";
                sSQL += "VNValue=" + clsMain.SQLStringUnicode(txtTENvn.Text) + "," + "\n";
                sSQL += "ENValue=" + clsMain.SQLStringUnicode(txtTENen.Text) + "," + "\n";
                sSQL += "OrderBy=" + clsMain.SQLString(txtSTT.Text) + "," + "\n";
                sSQL += "Status=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where ID=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ID").ToString();
            txtTENvn.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "VNValue").ToString();
            txtTENen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ENValue").ToString();
            txtSTT.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "OrderBy").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "Status").ToString());
            
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "SELECT ID,VNValue, ENValue,OrderBy,CAST (Status as bit) as Status FROM ReferenceList WHERE Category = 'Location'" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[Status] = True";
        }

        private void LoadPermission()
        {
            //btnThem.Enabled = clsUserManagement.AllowAdd("7");
            //btnSua.Enabled = clsUserManagement.AllowEdit("7");
            //btnXoa.Enabled = clsUserManagement.AllowDelete("7");
            //btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENvn.Properties.ReadOnly = true;
                txtTENen.Properties.ReadOnly = true;
                txtSTT.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENvn.Properties.ReadOnly = false;
                txtTENen.Properties.ReadOnly = false;
                txtSTT.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTENvn.Text = "";
            txtTENen.Text = "";
            txtSTT.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTENvn.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENvn.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENvn.Focus();
                return false;
            }
            if (txtTENen.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENen.Focus();
                return false;
            }
            if (txtSTT.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + labelControl1.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSTT.Focus();
                return false;
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void Frm_Khuvuc_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
        }
    }
}