﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using System.Collections;

namespace KP_RES
{
    public partial class PF_Ketca : DevExpress.XtraEditors.XtraForm
    {
        public PF_Ketca()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void PF_Ketca_Shown(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            string sSQL = "";
            sSQL += "Select A.LOAIVE As LOAIVE1,A.MABARCODE,A.SOGHE,A.LYDO,A.MAPHONG,A.MAPHIM,A.MALOAIGHE,A.MALOAIVE,A.NGAYTAO,A.DONGIA,C.TENLOAIGHE,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM,K.NGAYCHIEU,SUBSTRING(K.BATDAU,1,5) As BATDAU,K.KETTHUC" + "\n";
            sSQL += "From VETONGHOP A, DM_LOAIGHE C , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J,LICHCHIEU K" + "\n";
            sSQL += "Where A.MALOAIGHE = C.MALOAIGHE" + "\n";
            sSQL += "And A.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And A.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And A.NGUOIBAN = H.UserID" + "\n";
            sSQL += "And H.MaNV = G.MANHANVIEN" + "\n";
            sSQL += "And A.MALOAIVE = I.MALOAIVE" + "\n";
            sSQL += "And E.MARAP = J.MADIADIEM" + "\n";
            sSQL += "And A.MALICHCHIEU = K.MALICHCHIEU" + "\n";
            sSQL += "And A.LOAIVE= " + clsMain.SQLStringUnicode("Vé bán") + "\n";
            sSQL += "And A.NGAYTAO BETWEEN " + clsMain.SQLString(dtpTungay.Text + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text + " 23 : 59 : 59") + "\n";
            sSQL += "And A.NGUOITAO= " + clsMain.SQLString(clsGlobal.gsUserID) + "\n";
            //Sắp xếp theo đúng thời gian bán
            sSQL += "Order By A.NGAYTAO" + "\n";

            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = dt;

            sSQL = "";
            sSQL += "Select A.LOAIVE As LOAIVE1,A.MABARCODE,A.SOGHE,A.LYDO,A.MAPHONG,A.MAPHIM,A.MALOAIGHE,A.MALOAIVE,A.NGAYTAO,A.DONGIA,C.TENLOAIGHE,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM,K.NGAYCHIEU,SUBSTRING(K.BATDAU,1,5) As BATDAU,K.KETTHUC" + "\n";
            sSQL += "From VETONGHOP A, DM_LOAIGHE C , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J,LICHCHIEU K" + "\n";
            sSQL += "Where A.MALOAIGHE = C.MALOAIGHE" + "\n";
            sSQL += "And A.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And A.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And A.NGUOIBAN = H.UserID" + "\n";
            sSQL += "And H.MaNV = G.MANHANVIEN" + "\n";
            sSQL += "And A.MALOAIVE = I.MALOAIVE" + "\n";
            sSQL += "And E.MARAP = J.MADIADIEM" + "\n";
            sSQL += "And A.MALICHCHIEU = K.MALICHCHIEU" + "\n";
            sSQL += "And A.LOAIVE= " + clsMain.SQLStringUnicode("Vé hủy") + "\n";
            sSQL += "And A.NGAYTAO BETWEEN " + clsMain.SQLString(dtpTungay.Text + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text + " 23 : 59 : 59") + "\n";
            sSQL += "And A.NGUOITAO= " + clsMain.SQLString(clsGlobal.gsUserID) + "\n";
            //Sắp xếp theo đúng thời gian bán
            sSQL += "Order By A.NGAYTAO" + "\n";

            dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;
            txtTientamung.EditValue = int.Parse(clsUserManagement.ReturnTienTamUng());
            txtVeban.EditValue = int.Parse(STT1.SummaryText.Replace(",", ""));
            txtVehuy.EditValue = int.Parse(STT2.SummaryText.Replace(",", ""));
            txtTienban.EditValue = int.Parse(GIAVE1.SummaryText.Replace(",", ""));
            txtTienhuy.EditValue = int.Parse(GIAVE2.SummaryText.Replace(",", ""));
            txtTiennop.EditValue = (int)txtTienban.EditValue - (int)txtTienhuy.EditValue + (int)txtTientamung.EditValue  ;
            txtTienbanve.EditValue = (int)txtTienban.EditValue - (int)txtTienhuy.EditValue;
            txtNhanvien.Text = clsGlobal.gsUserID.ToUpper() + "-" + clsGlobal.gsNameOfUserLogin;
        }

        private void PF_Ketca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnDong_Click(null, null);
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnKetca_Click(object sender, EventArgs e)
        {
            cls_PrinterEndSales printSales = new cls_PrinterEndSales();
            printSales.maCaBan = "";
            printSales.tenCaBan = cls_ConfigCashier.sTenCB;
            printSales.maNV = clsGlobal.gsMaNVOfUserLogin;
            printSales.tenNV = clsGlobal.gsNameOfUserLogin;
            printSales.maQuay = cls_ConfigCashier.idCashier;
            printSales.tenQuay = cls_ConfigCashier.nameCashier;
            printSales.giamgia = double.Parse("0");
            printSales.TienTamUng = double.Parse(txtTientamung.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));
            printSales.tongTienBan = double.Parse(txtTienban.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));
            printSales.tongTienTra = double.Parse(txtTienhuy.Text.Trim(new Char[] { 'V', 'N', 'Đ' })); ;
            printSales.tongTien = double.Parse(txtTiennop.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));

            string sql = string.Format("EXEC SelectKetca_RapPhim @MANHANVIEN='{0}',@TUNGAY='{1}',@DENNGAY='{2}'",
                clsGlobal.gsMaNVOfUserLogin, string.Format("{0: yyyyMMdd}", dtpTungay.EditValue) + " 00:00:00", string.Format("{0: yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59");
            DataTable dtkc = clsMain.ReturnDataTable(sql);
            if (dtkc.Rows.Count > 0)
            {
                printSales.tienmat = double.Parse(dtkc.Rows[0]["MONEYCASH"].ToString()) + double.Parse(txtTientamung.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));
                printSales.tienthe = double.Parse(dtkc.Rows[0]["MONEYVISA"].ToString());
                printSales.voucher = double.Parse(dtkc.Rows[0]["MONEYVOUCHER"].ToString());
                printSales.TTNB = double.Parse(dtkc.Rows[0]["MONEYINNERCARD"].ToString());
            }

            List<cls_PrinterEndSales> endSales = new List<cls_PrinterEndSales>();
            endSales.Add(printSales);
            convert cvrt = new convert();
            DataTable bill = cvrt.ConvertToDataTable(endSales);
            Frm_Report1 frm = new Frm_Report1();
            frm.ReportName = "rpt_ketca";
            frm.DataSource = bill;
            frm.WindowState = FormWindowState.Minimized;
            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
            frm.IsPrint = true;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT2 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnHoantacketca_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn hoàn thành kết ca và đóng tài khoản ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string sSQL = "Select GIATRI From CAUHINH where TEN='KICHHOATUSER'";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() == "1")
                        clsUserManagement.DongTaiKhoan(clsGlobal.gsUserID,false);
                    else
                        clsUserManagement.DongTaiKhoan(clsGlobal.gsUserID,true);
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Đóng tài khoản thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
        }

        
    }
}