﻿namespace KP_RES
{
    partial class Frm_RPDatve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_RPDatve));
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.pnlDanhSachDatVe = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MABARCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENPHIM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BATDAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGSOGHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGGIAVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHANHKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADAILY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CONLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlSodo = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cboLoaive = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLenve = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbNgay = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTongtien = new DevExpress.XtraEditors.TextEdit();
            this.txtTongghe = new DevExpress.XtraEditors.TextEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnXoave = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongve = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnHuydatve = new DevExpress.XtraEditors.SimpleButton();
            this.btnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btnDSDV = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonnhieughe = new DevExpress.XtraEditors.SimpleButton();
            this.btnInbiennhandatve = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonlaighe = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonSuatChieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl93 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl94 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGio333 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl38 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNgay333 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPhim333 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl26 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnRap333 = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl95 = new DevExpress.XtraEditors.PanelControl();
            this.lblGio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.lblNgay = new DevExpress.XtraEditors.LabelControl();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.lblPhim = new DevExpress.XtraEditors.LabelControl();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.lblRap = new DevExpress.XtraEditors.LabelControl();
            this.panelControl39 = new DevExpress.XtraEditors.PanelControl();
            this.lblGio1 = new DevExpress.XtraEditors.LabelControl();
            this.lblNgay1 = new DevExpress.XtraEditors.LabelControl();
            this.lblPhim1 = new DevExpress.XtraEditors.LabelControl();
            this.lblRap1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.timerDongho = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            this.pnlDanhSachDatVe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaive.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl93)).BeginInit();
            this.panelControl93.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl94)).BeginInit();
            this.panelControl94.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).BeginInit();
            this.panelControl38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).BeginInit();
            this.panelControl26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl95)).BeginInit();
            this.panelControl95.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).BeginInit();
            this.panelControl39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Giao diện";
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(798, 2);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 31);
            // 
            // panelControl25
            // 
            this.panelControl25.Controls.Add(this.pnlDanhSachDatVe);
            this.panelControl25.Controls.Add(this.pnlSodo);
            this.panelControl25.Controls.Add(this.panel12);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl25.Location = new System.Drawing.Point(0, 249);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(907, 531);
            this.panelControl25.TabIndex = 53;
            // 
            // pnlDanhSachDatVe
            // 
            this.pnlDanhSachDatVe.AutoScroll = true;
            this.pnlDanhSachDatVe.Controls.Add(this.gridControl1);
            this.pnlDanhSachDatVe.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlDanhSachDatVe.Location = new System.Drawing.Point(252, 2);
            this.pnlDanhSachDatVe.Name = "pnlDanhSachDatVe";
            this.pnlDanhSachDatVe.Size = new System.Drawing.Size(447, 527);
            this.pnlDanhSachDatVe.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(447, 527);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.MABARCODE,
            this.TENPHIM,
            this.NGAYCHIEU,
            this.BATDAU,
            this.TONGSOGHE,
            this.TONGGIAVE,
            this.TENHANHKHACH,
            this.DIENTHOAI,
            this.MADAILY,
            this.TEN,
            this.CHIETKHAU,
            this.THANHTIEN,
            this.THANHTOAN,
            this.CONLAI,
            this.TRANGTHAI,
            this.gridColumn2});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT1.AppearanceCell.Options.UseFont = true;
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STT1.Caption = "STT";
            this.STT1.FieldName = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 60;
            // 
            // MABARCODE
            // 
            this.MABARCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MABARCODE.AppearanceCell.Options.UseFont = true;
            this.MABARCODE.AppearanceCell.Options.UseTextOptions = true;
            this.MABARCODE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MABARCODE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MABARCODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MABARCODE.AppearanceHeader.Options.UseFont = true;
            this.MABARCODE.AppearanceHeader.Options.UseTextOptions = true;
            this.MABARCODE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MABARCODE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MABARCODE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MABARCODE.Caption = "Mã code";
            this.MABARCODE.FieldName = "MABARCODE";
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.OptionsColumn.AllowEdit = false;
            this.MABARCODE.OptionsColumn.AllowFocus = false;
            this.MABARCODE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.MABARCODE.OptionsColumn.FixedWidth = true;
            this.MABARCODE.Visible = true;
            this.MABARCODE.VisibleIndex = 1;
            this.MABARCODE.Width = 100;
            // 
            // TENPHIM
            // 
            this.TENPHIM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHIM.AppearanceCell.Options.UseFont = true;
            this.TENPHIM.AppearanceCell.Options.UseTextOptions = true;
            this.TENPHIM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENPHIM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENPHIM.AppearanceHeader.Options.UseFont = true;
            this.TENPHIM.AppearanceHeader.Options.UseTextOptions = true;
            this.TENPHIM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHIM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENPHIM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENPHIM.Caption = "Tên phim";
            this.TENPHIM.FieldName = "TENPHIM";
            this.TENPHIM.Name = "TENPHIM";
            this.TENPHIM.OptionsColumn.AllowEdit = false;
            this.TENPHIM.OptionsColumn.AllowFocus = false;
            this.TENPHIM.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TENPHIM.OptionsColumn.FixedWidth = true;
            this.TENPHIM.Visible = true;
            this.TENPHIM.VisibleIndex = 2;
            this.TENPHIM.Width = 200;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCHIEU.AppearanceCell.Options.UseFont = true;
            this.NGAYCHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCHIEU.AppearanceHeader.Options.UseFont = true;
            this.NGAYCHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCHIEU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NGAYCHIEU.Caption = "Ngày chiếu";
            this.NGAYCHIEU.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYCHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYCHIEU.FieldName = "NGAYCHIEU";
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.OptionsColumn.AllowEdit = false;
            this.NGAYCHIEU.OptionsColumn.AllowFocus = false;
            this.NGAYCHIEU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.NGAYCHIEU.OptionsColumn.FixedWidth = true;
            this.NGAYCHIEU.Visible = true;
            this.NGAYCHIEU.VisibleIndex = 3;
            this.NGAYCHIEU.Width = 100;
            // 
            // BATDAU
            // 
            this.BATDAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.BATDAU.AppearanceCell.Options.UseFont = true;
            this.BATDAU.AppearanceCell.Options.UseTextOptions = true;
            this.BATDAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BATDAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BATDAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BATDAU.AppearanceHeader.Options.UseFont = true;
            this.BATDAU.AppearanceHeader.Options.UseTextOptions = true;
            this.BATDAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BATDAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BATDAU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.BATDAU.Caption = "Giờ chiếu";
            this.BATDAU.FieldName = "BATDAU";
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.OptionsColumn.AllowEdit = false;
            this.BATDAU.OptionsColumn.AllowFocus = false;
            this.BATDAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BATDAU.OptionsColumn.FixedWidth = true;
            this.BATDAU.Visible = true;
            this.BATDAU.VisibleIndex = 4;
            this.BATDAU.Width = 100;
            // 
            // TONGSOGHE
            // 
            this.TONGSOGHE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGSOGHE.AppearanceCell.Options.UseFont = true;
            this.TONGSOGHE.AppearanceCell.Options.UseTextOptions = true;
            this.TONGSOGHE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGSOGHE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGSOGHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGSOGHE.AppearanceHeader.Options.UseFont = true;
            this.TONGSOGHE.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGSOGHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGSOGHE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGSOGHE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TONGSOGHE.Caption = "Tổng số ghế";
            this.TONGSOGHE.DisplayFormat.FormatString = "N0";
            this.TONGSOGHE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGSOGHE.FieldName = "TONGSOGHE";
            this.TONGSOGHE.Name = "TONGSOGHE";
            this.TONGSOGHE.OptionsColumn.AllowEdit = false;
            this.TONGSOGHE.OptionsColumn.AllowFocus = false;
            this.TONGSOGHE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TONGSOGHE.OptionsColumn.FixedWidth = true;
            this.TONGSOGHE.Visible = true;
            this.TONGSOGHE.VisibleIndex = 5;
            this.TONGSOGHE.Width = 120;
            // 
            // TONGGIAVE
            // 
            this.TONGGIAVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGGIAVE.AppearanceCell.Options.UseFont = true;
            this.TONGGIAVE.AppearanceCell.Options.UseTextOptions = true;
            this.TONGGIAVE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGGIAVE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGGIAVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGGIAVE.AppearanceHeader.Options.UseFont = true;
            this.TONGGIAVE.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGGIAVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGGIAVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGGIAVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TONGGIAVE.Caption = "Tổng giá vé";
            this.TONGGIAVE.DisplayFormat.FormatString = "N0";
            this.TONGGIAVE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGGIAVE.FieldName = "TONGGIAVE";
            this.TONGGIAVE.Name = "TONGGIAVE";
            this.TONGGIAVE.OptionsColumn.AllowEdit = false;
            this.TONGGIAVE.OptionsColumn.AllowFocus = false;
            this.TONGGIAVE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TONGGIAVE.OptionsColumn.FixedWidth = true;
            this.TONGGIAVE.Visible = true;
            this.TONGGIAVE.VisibleIndex = 6;
            this.TONGGIAVE.Width = 130;
            // 
            // TENHANHKHACH
            // 
            this.TENHANHKHACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENHANHKHACH.AppearanceCell.Options.UseFont = true;
            this.TENHANHKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENHANHKHACH.AppearanceHeader.Options.UseFont = true;
            this.TENHANHKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHANHKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHANHKHACH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHANHKHACH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TENHANHKHACH.Caption = "Người đặt";
            this.TENHANHKHACH.FieldName = "TENHANHKHACH";
            this.TENHANHKHACH.Name = "TENHANHKHACH";
            this.TENHANHKHACH.OptionsColumn.AllowEdit = false;
            this.TENHANHKHACH.OptionsColumn.AllowFocus = false;
            this.TENHANHKHACH.OptionsColumn.FixedWidth = true;
            this.TENHANHKHACH.Visible = true;
            this.TENHANHKHACH.VisibleIndex = 7;
            this.TENHANHKHACH.Width = 150;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DIENTHOAI.AppearanceCell.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENTHOAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 8;
            this.DIENTHOAI.Width = 100;
            // 
            // MADAILY
            // 
            this.MADAILY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MADAILY.AppearanceCell.Options.UseFont = true;
            this.MADAILY.AppearanceCell.Options.UseTextOptions = true;
            this.MADAILY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADAILY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MADAILY.AppearanceHeader.Options.UseFont = true;
            this.MADAILY.AppearanceHeader.Options.UseTextOptions = true;
            this.MADAILY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADAILY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADAILY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MADAILY.Caption = "Mã đại lý";
            this.MADAILY.FieldName = "MADAILY";
            this.MADAILY.Name = "MADAILY";
            this.MADAILY.OptionsColumn.AllowEdit = false;
            this.MADAILY.OptionsColumn.AllowFocus = false;
            this.MADAILY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.MADAILY.OptionsColumn.FixedWidth = true;
            this.MADAILY.Visible = true;
            this.MADAILY.VisibleIndex = 9;
            this.MADAILY.Width = 100;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN.Caption = "Tên đại lý";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 10;
            this.TEN.Width = 200;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CHIETKHAU.Caption = "Chiết khấu";
            this.CHIETKHAU.DisplayFormat.FormatString = "N0";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 11;
            this.CHIETKHAU.Width = 120;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTIEN.Caption = "Thành tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "N0";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 12;
            this.THANHTIEN.Width = 130;
            // 
            // THANHTOAN
            // 
            this.THANHTOAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTOAN.AppearanceCell.Options.UseFont = true;
            this.THANHTOAN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTOAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTOAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.THANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTOAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTOAN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.THANHTOAN.Caption = "Thanh toán";
            this.THANHTOAN.DisplayFormat.FormatString = "N0";
            this.THANHTOAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THANHTOAN.FieldName = "THANHTOAN";
            this.THANHTOAN.Name = "THANHTOAN";
            this.THANHTOAN.OptionsColumn.AllowEdit = false;
            this.THANHTOAN.OptionsColumn.AllowFocus = false;
            this.THANHTOAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THANHTOAN.OptionsColumn.FixedWidth = true;
            this.THANHTOAN.Visible = true;
            this.THANHTOAN.VisibleIndex = 13;
            this.THANHTOAN.Width = 130;
            // 
            // CONLAI
            // 
            this.CONLAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.CONLAI.AppearanceCell.Options.UseFont = true;
            this.CONLAI.AppearanceCell.Options.UseTextOptions = true;
            this.CONLAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CONLAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CONLAI.AppearanceHeader.Options.UseFont = true;
            this.CONLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.CONLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CONLAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CONLAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CONLAI.Caption = "Còn lại";
            this.CONLAI.DisplayFormat.FormatString = "N0";
            this.CONLAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CONLAI.FieldName = "CONLAI";
            this.CONLAI.Name = "CONLAI";
            this.CONLAI.OptionsColumn.AllowEdit = false;
            this.CONLAI.OptionsColumn.AllowFocus = false;
            this.CONLAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CONLAI.OptionsColumn.FixedWidth = true;
            this.CONLAI.Visible = true;
            this.CONLAI.VisibleIndex = 14;
            this.CONLAI.Width = 130;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRANGTHAI.AppearanceCell.Options.UseFont = true;
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 15;
            this.TRANGTHAI.Width = 150;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 16;
            this.gridColumn2.Width = 20;
            // 
            // pnlSodo
            // 
            this.pnlSodo.AutoScroll = true;
            this.pnlSodo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSodo.Location = new System.Drawing.Point(2, 2);
            this.pnlSodo.Name = "pnlSodo";
            this.pnlSodo.Size = new System.Drawing.Size(244, 527);
            this.pnlSodo.TabIndex = 5;
            // 
            // panel12
            // 
            this.panel12.AutoScroll = true;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.gridControl);
            this.panel12.Controls.Add(this.panel3);
            this.panel12.Controls.Add(this.panel4);
            this.panel12.Controls.Add(this.panel1);
            this.panel12.Controls.Add(this.panel5);
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(705, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(200, 527);
            this.panel12.TabIndex = 4;
            // 
            // gridControl
            // 
            this.gridControl.AllowDrop = true;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(0, 62);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1});
            this.gridControl.Size = new System.Drawing.Size(198, 120);
            this.gridControl.TabIndex = 45;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MADIADIEM,
            this.MAVE,
            this.PHUTHU,
            this.MAKHUVUC,
            this.FILL});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.RowHeight = 40;
            this.gridView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Width = 50;
            // 
            // MADIADIEM
            // 
            this.MADIADIEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceCell.Options.UseFont = true;
            this.MADIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MADIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceHeader.Options.UseFont = true;
            this.MADIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MADIADIEM.Caption = "Ghế";
            this.MADIADIEM.FieldName = "SOGHE";
            this.MADIADIEM.Name = "MADIADIEM";
            this.MADIADIEM.OptionsColumn.AllowFocus = false;
            this.MADIADIEM.OptionsColumn.AllowMove = false;
            this.MADIADIEM.OptionsColumn.AllowSize = false;
            this.MADIADIEM.OptionsColumn.FixedWidth = true;
            this.MADIADIEM.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.MADIADIEM.Visible = true;
            this.MADIADIEM.VisibleIndex = 0;
            this.MADIADIEM.Width = 50;
            // 
            // MAVE
            // 
            this.MAVE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MAVE.AppearanceCell.Options.UseFont = true;
            this.MAVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MAVE.AppearanceHeader.Options.UseFont = true;
            this.MAVE.AppearanceHeader.Options.UseTextOptions = true;
            this.MAVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAVE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MAVE.Caption = "Loại vé";
            this.MAVE.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.MAVE.FieldName = "MALOAIVE";
            this.MAVE.Name = "MAVE";
            this.MAVE.OptionsColumn.AllowMove = false;
            this.MAVE.OptionsColumn.AllowSize = false;
            this.MAVE.OptionsColumn.FixedWidth = true;
            this.MAVE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAVE.Visible = true;
            this.MAVE.VisibleIndex = 1;
            this.MAVE.Width = 100;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", 30, "Loại vé")});
            this.repositoryItemLookUpEdit1.DisplayMember = "TENLOAIVE";
            this.repositoryItemLookUpEdit1.DropDownItemHeight = 40;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "Chọn loại vé";
            this.repositoryItemLookUpEdit1.ValueMember = "MALOAIVE";
            this.repositoryItemLookUpEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpEdit1_EditValueChanged);
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceCell.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PHUTHU.Caption = "Giá vé";
            this.PHUTHU.DisplayFormat.FormatString = "N0";
            this.PHUTHU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.OptionsColumn.AllowMove = false;
            this.PHUTHU.OptionsColumn.AllowSize = false;
            this.PHUTHU.OptionsColumn.FixedWidth = true;
            this.PHUTHU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PHUTHU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PHUTHU", "{0:#,###0}")});
            this.PHUTHU.Visible = true;
            this.PHUTHU.VisibleIndex = 2;
            this.PHUTHU.Width = 80;
            // 
            // MAKHUVUC
            // 
            this.MAKHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHUVUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHUVUC.Caption = "Sử dụng";
            this.MAKHUVUC.FieldName = "MAKHUVUC";
            this.MAKHUVUC.Name = "MAKHUVUC";
            this.MAKHUVUC.OptionsColumn.AllowEdit = false;
            this.MAKHUVUC.OptionsColumn.AllowFocus = false;
            this.MAKHUVUC.OptionsColumn.AllowMove = false;
            this.MAKHUVUC.OptionsColumn.AllowSize = false;
            this.MAKHUVUC.OptionsColumn.FixedWidth = true;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.OptionsColumn.AllowSize = false;
            this.FILL.Width = 20;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cboLoaive);
            this.panel3.Controls.Add(this.btnLenve);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 31);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(198, 31);
            this.panel3.TabIndex = 78;
            // 
            // cboLoaive
            // 
            this.cboLoaive.EnterMoveNextControl = true;
            this.cboLoaive.Location = new System.Drawing.Point(96, 0);
            this.cboLoaive.Name = "cboLoaive";
            this.cboLoaive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaive.Properties.Appearance.Options.UseFont = true;
            this.cboLoaive.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaive.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoaive.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoaive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoaive.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", "Loại vé")});
            this.cboLoaive.Properties.DisplayMember = "TENLOAIVE";
            this.cboLoaive.Properties.DropDownItemHeight = 40;
            this.cboLoaive.Properties.NullText = "";
            this.cboLoaive.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoaive.Properties.ShowHeader = false;
            this.cboLoaive.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoaive.Properties.ValueMember = "MALOAIVE";
            this.cboLoaive.Size = new System.Drawing.Size(102, 30);
            this.cboLoaive.TabIndex = 20;
            this.cboLoaive.EditValueChanged += new System.EventHandler(this.cboLoaive_EditValueChanged);
            // 
            // btnLenve
            // 
            this.btnLenve.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLenve.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenve.Location = new System.Drawing.Point(0, 0);
            this.btnLenve.Name = "btnLenve";
            this.btnLenve.Size = new System.Drawing.Size(95, 31);
            this.btnLenve.TabIndex = 7;
            this.btnLenve.Click += new System.EventHandler(this.btnLenve_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbgiay);
            this.panel4.Controls.Add(this.lbNgay);
            this.panel4.Controls.Add(this.lbGio);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 31);
            this.panel4.TabIndex = 80;
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(182, 1);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(14, 16);
            this.lbgiay.TabIndex = 60;
            this.lbgiay.Text = "00";
            // 
            // lbNgay
            // 
            this.lbNgay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNgay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbNgay.Location = new System.Drawing.Point(6, 9);
            this.lbNgay.Name = "lbNgay";
            this.lbNgay.Size = new System.Drawing.Size(39, 19);
            this.lbNgay.TabIndex = 59;
            this.lbNgay.Text = "ngay";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(134, 9);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 61;
            this.lbGio.Text = "00:00";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtTongtien);
            this.panel1.Controls.Add(this.txtTongghe);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 182);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(198, 31);
            this.panel1.TabIndex = 71;
            // 
            // txtTongtien
            // 
            this.txtTongtien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTongtien.EditValue = "0";
            this.txtTongtien.EnterMoveNextControl = true;
            this.txtTongtien.Location = new System.Drawing.Point(40, 0);
            this.txtTongtien.Name = "txtTongtien";
            this.txtTongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongtien.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtTongtien.Properties.Appearance.Options.UseFont = true;
            this.txtTongtien.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongtien.Properties.ReadOnly = true;
            this.txtTongtien.Size = new System.Drawing.Size(158, 30);
            this.txtTongtien.TabIndex = 58;
            // 
            // txtTongghe
            // 
            this.txtTongghe.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtTongghe.EditValue = "0";
            this.txtTongghe.EnterMoveNextControl = true;
            this.txtTongghe.Location = new System.Drawing.Point(0, 0);
            this.txtTongghe.Name = "txtTongghe";
            this.txtTongghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongghe.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtTongghe.Properties.Appearance.Options.UseFont = true;
            this.txtTongghe.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongghe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongghe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTongghe.Properties.LookAndFeel.SkinName = "Office 2007 Blue";
            this.txtTongghe.Properties.ReadOnly = true;
            this.txtTongghe.Size = new System.Drawing.Size(40, 30);
            this.txtTongghe.TabIndex = 57;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnXoave);
            this.panel5.Controls.Add(this.btnXuongve);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 213);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(198, 31);
            this.panel5.TabIndex = 79;
            // 
            // btnXoave
            // 
            this.btnXoave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXoave.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXoave.Location = new System.Drawing.Point(103, 0);
            this.btnXoave.Name = "btnXoave";
            this.btnXoave.Size = new System.Drawing.Size(95, 31);
            this.btnXoave.TabIndex = 8;
            this.btnXoave.Click += new System.EventHandler(this.btnXoave_Click);
            // 
            // btnXuongve
            // 
            this.btnXuongve.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnXuongve.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongve.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongve.Location = new System.Drawing.Point(0, 0);
            this.btnXuongve.Name = "btnXuongve";
            this.btnXuongve.Size = new System.Drawing.Size(95, 31);
            this.btnXuongve.TabIndex = 7;
            this.btnXuongve.Click += new System.EventHandler(this.btnXuongve_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.btnHuydatve);
            this.panel2.Controls.Add(this.btnThanhToan);
            this.panel2.Controls.Add(this.btnDSDV);
            this.panel2.Controls.Add(this.btnChonnhieughe);
            this.panel2.Controls.Add(this.btnInbiennhandatve);
            this.panel2.Controls.Add(this.btnChonlaighe);
            this.panel2.Controls.Add(this.btnChonSuatChieu);
            this.panel2.Controls.Add(this.btnDong);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 244);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(198, 281);
            this.panel2.TabIndex = 72;
            // 
            // btnHuydatve
            // 
            this.btnHuydatve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHuydatve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnHuydatve.Appearance.Options.UseFont = true;
            this.btnHuydatve.Appearance.Options.UseTextOptions = true;
            this.btnHuydatve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnHuydatve.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuydatve.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnHuydatve.Location = new System.Drawing.Point(1, 215);
            this.btnHuydatve.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuydatve.Name = "btnHuydatve";
            this.btnHuydatve.Size = new System.Drawing.Size(95, 60);
            this.btnHuydatve.TabIndex = 83;
            this.btnHuydatve.Text = "Hủy đặt vé";
            this.btnHuydatve.Click += new System.EventHandler(this.btnHuydatve_Click);
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnThanhToan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnThanhToan.Appearance.Options.UseFont = true;
            this.btnThanhToan.Appearance.Options.UseTextOptions = true;
            this.btnThanhToan.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnThanhToan.Image = global::KP_RES.Properties.Resources.card_in_use_26;
            this.btnThanhToan.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnThanhToan.Location = new System.Drawing.Point(102, 147);
            this.btnThanhToan.Margin = new System.Windows.Forms.Padding(4);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(95, 60);
            this.btnThanhToan.TabIndex = 82;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnDSDV
            // 
            this.btnDSDV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDSDV.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDSDV.Appearance.Options.UseFont = true;
            this.btnDSDV.Appearance.Options.UseTextOptions = true;
            this.btnDSDV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDSDV.Image = global::KP_RES.Properties.Resources.list_26;
            this.btnDSDV.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDSDV.Location = new System.Drawing.Point(1, 147);
            this.btnDSDV.Margin = new System.Windows.Forms.Padding(4);
            this.btnDSDV.Name = "btnDSDV";
            this.btnDSDV.Size = new System.Drawing.Size(95, 60);
            this.btnDSDV.TabIndex = 81;
            this.btnDSDV.Text = "Danh sách ĐVé";
            this.btnDSDV.Click += new System.EventHandler(this.btnDSDV_Click);
            // 
            // btnChonnhieughe
            // 
            this.btnChonnhieughe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonnhieughe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonnhieughe.Appearance.Options.UseFont = true;
            this.btnChonnhieughe.Appearance.Options.UseTextOptions = true;
            this.btnChonnhieughe.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonnhieughe.Image = global::KP_RES.Properties.Resources.dust_26;
            this.btnChonnhieughe.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonnhieughe.Location = new System.Drawing.Point(102, 80);
            this.btnChonnhieughe.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonnhieughe.Name = "btnChonnhieughe";
            this.btnChonnhieughe.Size = new System.Drawing.Size(95, 60);
            this.btnChonnhieughe.TabIndex = 80;
            this.btnChonnhieughe.Text = "Chọn nhiều ghế";
            this.btnChonnhieughe.Click += new System.EventHandler(this.btnChonnhieughe_Click);
            // 
            // btnInbiennhandatve
            // 
            this.btnInbiennhandatve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInbiennhandatve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnInbiennhandatve.Appearance.Options.UseFont = true;
            this.btnInbiennhandatve.Appearance.Options.UseTextOptions = true;
            this.btnInbiennhandatve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInbiennhandatve.Image = global::KP_RES.Properties.Resources.check_book_26;
            this.btnInbiennhandatve.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnInbiennhandatve.Location = new System.Drawing.Point(1, 80);
            this.btnInbiennhandatve.Margin = new System.Windows.Forms.Padding(4);
            this.btnInbiennhandatve.Name = "btnInbiennhandatve";
            this.btnInbiennhandatve.Size = new System.Drawing.Size(95, 60);
            this.btnInbiennhandatve.TabIndex = 79;
            this.btnInbiennhandatve.Text = "Đặt vé";
            this.btnInbiennhandatve.Click += new System.EventHandler(this.btnInbiennhandatve_Click);
            // 
            // btnChonlaighe
            // 
            this.btnChonlaighe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonlaighe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonlaighe.Appearance.Options.UseFont = true;
            this.btnChonlaighe.Appearance.Options.UseTextOptions = true;
            this.btnChonlaighe.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonlaighe.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnChonlaighe.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonlaighe.Location = new System.Drawing.Point(102, 12);
            this.btnChonlaighe.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonlaighe.Name = "btnChonlaighe";
            this.btnChonlaighe.Size = new System.Drawing.Size(95, 60);
            this.btnChonlaighe.TabIndex = 78;
            this.btnChonlaighe.Text = "Chọn lại ghế";
            this.btnChonlaighe.Click += new System.EventHandler(this.btnChonlaighe_Click);
            // 
            // btnChonSuatChieu
            // 
            this.btnChonSuatChieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonSuatChieu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnChonSuatChieu.Appearance.Options.UseFont = true;
            this.btnChonSuatChieu.Appearance.Options.UseTextOptions = true;
            this.btnChonSuatChieu.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonSuatChieu.Image = global::KP_RES.Properties.Resources.backspace_26;
            this.btnChonSuatChieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnChonSuatChieu.Location = new System.Drawing.Point(1, 12);
            this.btnChonSuatChieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonSuatChieu.Name = "btnChonSuatChieu";
            this.btnChonSuatChieu.Size = new System.Drawing.Size(95, 60);
            this.btnChonSuatChieu.TabIndex = 76;
            this.btnChonSuatChieu.Text = "Chọn phim";
            this.btnChonSuatChieu.Click += new System.EventHandler(this.btnChonSuatChieu_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Appearance.Options.UseTextOptions = true;
            this.btnDong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDong.Image = global::KP_RES.Properties.Resources.shutdown_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDong.Location = new System.Drawing.Point(102, 215);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(95, 60);
            this.btnDong.TabIndex = 77;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // panelControl93
            // 
            this.panelControl93.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl93.Controls.Add(this.panelControl94);
            this.panelControl93.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl93.Location = new System.Drawing.Point(0, 200);
            this.panelControl93.Name = "panelControl93";
            this.panelControl93.Size = new System.Drawing.Size(907, 49);
            this.panelControl93.TabIndex = 52;
            // 
            // panelControl94
            // 
            this.panelControl94.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl94.Controls.Add(this.flowLayoutPanel39);
            this.panelControl94.Controls.Add(this.btnGio333);
            this.panelControl94.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl94.Location = new System.Drawing.Point(0, 0);
            this.panelControl94.Name = "panelControl94";
            this.panelControl94.Size = new System.Drawing.Size(907, 49);
            this.panelControl94.TabIndex = 48;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.Color.DarkGray;
            this.flowLayoutPanel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(80, 0);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(827, 49);
            this.flowLayoutPanel39.TabIndex = 1;
            // 
            // btnGio333
            // 
            this.btnGio333.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGio333.Appearance.Options.UseFont = true;
            this.btnGio333.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnGio333.Image = global::KP_RES.Properties.Resources.timer_32;
            this.btnGio333.Location = new System.Drawing.Point(0, 0);
            this.btnGio333.Name = "btnGio333";
            this.btnGio333.Size = new System.Drawing.Size(80, 49);
            this.btnGio333.TabIndex = 2;
            this.btnGio333.Text = "Giờ";
            // 
            // panelControl37
            // 
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.panelControl38);
            this.panelControl37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl37.Location = new System.Drawing.Point(0, 151);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(907, 49);
            this.panelControl37.TabIndex = 50;
            // 
            // panelControl38
            // 
            this.panelControl38.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl38.Controls.Add(this.flowLayoutPanel11);
            this.panelControl38.Controls.Add(this.btnNgay333);
            this.panelControl38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl38.Location = new System.Drawing.Point(0, 0);
            this.panelControl38.Name = "panelControl38";
            this.panelControl38.Size = new System.Drawing.Size(907, 49);
            this.panelControl38.TabIndex = 48;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BackColor = System.Drawing.SystemColors.Window;
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(80, 0);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(827, 49);
            this.flowLayoutPanel11.TabIndex = 1;
            this.flowLayoutPanel11.WrapContents = false;
            // 
            // btnNgay333
            // 
            this.btnNgay333.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNgay333.Appearance.Options.UseFont = true;
            this.btnNgay333.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNgay333.Image = global::KP_RES.Properties.Resources.overtime_32;
            this.btnNgay333.Location = new System.Drawing.Point(0, 0);
            this.btnNgay333.Name = "btnNgay333";
            this.btnNgay333.Size = new System.Drawing.Size(80, 49);
            this.btnNgay333.TabIndex = 2;
            this.btnNgay333.Text = "Ngày";
            this.btnNgay333.Click += new System.EventHandler(this.lblNgay1_Click);
            // 
            // panelControl27
            // 
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.panelControl30);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl27.Location = new System.Drawing.Point(0, 102);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(907, 49);
            this.panelControl27.TabIndex = 48;
            // 
            // panelControl30
            // 
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.flowLayoutPanel13);
            this.panelControl30.Controls.Add(this.btnPhim333);
            this.panelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl30.Location = new System.Drawing.Point(0, 0);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(907, 49);
            this.panelControl30.TabIndex = 48;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.Color.Gainsboro;
            this.flowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(80, 0);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(827, 49);
            this.flowLayoutPanel13.TabIndex = 1;
            // 
            // btnPhim333
            // 
            this.btnPhim333.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhim333.Appearance.Options.UseFont = true;
            this.btnPhim333.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPhim333.Image = global::KP_RES.Properties.Resources.clapperboard_32;
            this.btnPhim333.Location = new System.Drawing.Point(0, 0);
            this.btnPhim333.Name = "btnPhim333";
            this.btnPhim333.Size = new System.Drawing.Size(80, 49);
            this.btnPhim333.TabIndex = 0;
            this.btnPhim333.Text = "Phim";
            this.btnPhim333.Click += new System.EventHandler(this.lblPhim1_Click);
            // 
            // panelControl23
            // 
            this.panelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl23.Controls.Add(this.panelControl26);
            this.panelControl23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl23.Location = new System.Drawing.Point(0, 53);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(907, 49);
            this.panelControl23.TabIndex = 47;
            // 
            // panelControl26
            // 
            this.panelControl26.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl26.Controls.Add(this.flowLayoutPanel10);
            this.panelControl26.Controls.Add(this.btnRap333);
            this.panelControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl26.Location = new System.Drawing.Point(0, 0);
            this.panelControl26.Name = "panelControl26";
            this.panelControl26.Size = new System.Drawing.Size(907, 49);
            this.panelControl26.TabIndex = 42;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BackColor = System.Drawing.Color.DarkGray;
            this.flowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(80, 0);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(827, 49);
            this.flowLayoutPanel10.TabIndex = 1;
            // 
            // btnRap333
            // 
            this.btnRap333.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRap333.Appearance.Options.UseFont = true;
            this.btnRap333.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRap333.Image = global::KP_RES.Properties.Resources.bungalow_32;
            this.btnRap333.Location = new System.Drawing.Point(0, 0);
            this.btnRap333.Name = "btnRap333";
            this.btnRap333.Size = new System.Drawing.Size(80, 49);
            this.btnRap333.TabIndex = 0;
            this.btnRap333.Text = "Rạp";
            this.btnRap333.Click += new System.EventHandler(this.lblRap1_Click);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "barStaticItem1";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "barStaticItem1";
            this.barStaticItem3.Id = 1;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(653, 164);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(85, 19);
            this.labelControl10.TabIndex = 27;
            this.labelControl10.Text = "Thời gian đi";
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(757, 161);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(518, 26);
            this.textEdit1.TabIndex = 28;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(653, 132);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(27, 19);
            this.labelControl14.TabIndex = 25;
            this.labelControl14.Text = "Tàu";
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(757, 129);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(518, 26);
            this.textEdit5.TabIndex = 26;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(653, 100);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(44, 19);
            this.labelControl15.TabIndex = 23;
            this.labelControl15.Text = "Tuyến";
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(757, 97);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.ReadOnly = true;
            this.textEdit6.Size = new System.Drawing.Size(518, 26);
            this.textEdit6.TabIndex = 24;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(12, 68);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(95, 19);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "Mã biên nhận";
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(128, 66);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(518, 26);
            this.textEdit7.TabIndex = 4;
            // 
            // textEdit8
            // 
            this.textEdit8.EditValue = "0";
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(757, 288);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit8.Properties.Mask.EditMask = "N0";
            this.textEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(518, 26);
            this.textEdit8.TabIndex = 20;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(641, 292);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(49, 19);
            this.labelControl17.TabIndex = 19;
            this.labelControl17.Text = "Còn lại";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.Image")));
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(761, 20);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(123, 40);
            this.simpleButton8.TabIndex = 21;
            this.simpleButton8.Text = "&2.In vé";
            // 
            // memoEdit1
            // 
            this.memoEdit1.EnterMoveNextControl = true;
            this.memoEdit1.Location = new System.Drawing.Point(128, 226);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(518, 122);
            this.memoEdit1.TabIndex = 14;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(9, 231);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(94, 19);
            this.labelControl18.TabIndex = 13;
            this.labelControl18.Text = "Danh sách vé";
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "0";
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(757, 224);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit9.Properties.ReadOnly = true;
            this.textEdit9.Size = new System.Drawing.Size(518, 26);
            this.textEdit9.TabIndex = 16;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(638, 228);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(84, 19);
            this.labelControl19.TabIndex = 15;
            this.labelControl19.Text = "Tổng giá vé";
            // 
            // textEdit10
            // 
            this.textEdit10.EditValue = "0";
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(757, 257);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit10.Properties.Mask.EditMask = "N0";
            this.textEdit10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit10.Properties.ReadOnly = true;
            this.textEdit10.Size = new System.Drawing.Size(518, 26);
            this.textEdit10.TabIndex = 18;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(12, 197);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(39, 19);
            this.labelControl20.TabIndex = 11;
            this.labelControl20.Text = "Email";
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(128, 194);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.ReadOnly = true;
            this.textEdit11.Size = new System.Drawing.Size(518, 26);
            this.textEdit11.TabIndex = 12;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(12, 165);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 19);
            this.labelControl21.TabIndex = 9;
            this.labelControl21.Text = "Điện thoại";
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(128, 162);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.ReadOnly = true;
            this.textEdit12.Size = new System.Drawing.Size(518, 26);
            this.textEdit12.TabIndex = 10;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(12, 133);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 19);
            this.labelControl22.TabIndex = 7;
            this.labelControl22.Text = "Địa chỉ";
            // 
            // textEdit13
            // 
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(128, 130);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.ReadOnly = true;
            this.textEdit13.Size = new System.Drawing.Size(518, 26);
            this.textEdit13.TabIndex = 8;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(638, 260);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(100, 19);
            this.labelControl23.TabIndex = 17;
            this.labelControl23.Text = "Đã thanh toán";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(12, 100);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(27, 19);
            this.labelControl24.TabIndex = 5;
            this.labelControl24.Text = "Tên";
            // 
            // textEdit14
            // 
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(128, 98);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.ReadOnly = true;
            this.textEdit14.Size = new System.Drawing.Size(518, 26);
            this.textEdit14.TabIndex = 6;
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton16.Location = new System.Drawing.Point(653, 20);
            this.simpleButton16.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(100, 40);
            this.simpleButton16.TabIndex = 22;
            this.simpleButton16.Text = "&1.Xem";
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl28.Location = new System.Drawing.Point(0, 0);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(1078, 14);
            this.panelControl28.TabIndex = 0;
            // 
            // textEdit15
            // 
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(128, 20);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit15.Size = new System.Drawing.Size(518, 40);
            this.textEdit15.TabIndex = 2;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(12, 33);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(109, 19);
            this.labelControl25.TabIndex = 1;
            this.labelControl25.Text = "Mã biên nhận";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.pictureEdit1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(907, 53);
            this.panelControl2.TabIndex = 45;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.panelControl22);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(700, 49);
            this.panelControl4.TabIndex = 53;
            // 
            // panelControl22
            // 
            this.panelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl22.Controls.Add(this.panelControl95);
            this.panelControl22.Controls.Add(this.panelControl39);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl22.Location = new System.Drawing.Point(0, 0);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(700, 45);
            this.panelControl22.TabIndex = 46;
            // 
            // panelControl95
            // 
            this.panelControl95.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl95.Appearance.Options.UseBackColor = true;
            this.panelControl95.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl95.Controls.Add(this.lblGio);
            this.panelControl95.Controls.Add(this.labelControl125);
            this.panelControl95.Controls.Add(this.lblNgay);
            this.panelControl95.Controls.Add(this.labelControl123);
            this.panelControl95.Controls.Add(this.lblPhim);
            this.panelControl95.Controls.Add(this.labelControl124);
            this.panelControl95.Controls.Add(this.lblRap);
            this.panelControl95.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl95.Location = new System.Drawing.Point(0, 23);
            this.panelControl95.Name = "panelControl95";
            this.panelControl95.Size = new System.Drawing.Size(700, 22);
            this.panelControl95.TabIndex = 47;
            // 
            // lblGio
            // 
            this.lblGio.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGio.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGio.Location = new System.Drawing.Point(78, 0);
            this.lblGio.Margin = new System.Windows.Forms.Padding(4);
            this.lblGio.Name = "lblGio";
            this.lblGio.Size = new System.Drawing.Size(6, 23);
            this.lblGio.TabIndex = 50;
            this.lblGio.Text = " ";
            // 
            // labelControl125
            // 
            this.labelControl125.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl125.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl125.Location = new System.Drawing.Point(58, 0);
            this.labelControl125.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(20, 19);
            this.labelControl125.TabIndex = 49;
            this.labelControl125.Text = "    ";
            // 
            // lblNgay
            // 
            this.lblNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgay.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblNgay.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblNgay.Location = new System.Drawing.Point(52, 0);
            this.lblNgay.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgay.Name = "lblNgay";
            this.lblNgay.Size = new System.Drawing.Size(6, 23);
            this.lblNgay.TabIndex = 48;
            this.lblNgay.Text = " ";
            // 
            // labelControl123
            // 
            this.labelControl123.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl123.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl123.Location = new System.Drawing.Point(32, 0);
            this.labelControl123.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(20, 19);
            this.labelControl123.TabIndex = 47;
            this.labelControl123.Text = "    ";
            // 
            // lblPhim
            // 
            this.lblPhim.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhim.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPhim.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPhim.Location = new System.Drawing.Point(26, 0);
            this.lblPhim.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhim.Name = "lblPhim";
            this.lblPhim.Size = new System.Drawing.Size(6, 23);
            this.lblPhim.TabIndex = 46;
            this.lblPhim.Text = " ";
            // 
            // labelControl124
            // 
            this.labelControl124.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl124.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl124.Location = new System.Drawing.Point(6, 0);
            this.labelControl124.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(20, 19);
            this.labelControl124.TabIndex = 45;
            this.labelControl124.Text = "    ";
            // 
            // lblRap
            // 
            this.lblRap.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRap.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRap.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRap.Location = new System.Drawing.Point(0, 0);
            this.lblRap.Margin = new System.Windows.Forms.Padding(4);
            this.lblRap.Name = "lblRap";
            this.lblRap.Size = new System.Drawing.Size(6, 23);
            this.lblRap.TabIndex = 44;
            this.lblRap.Text = " ";
            // 
            // panelControl39
            // 
            this.panelControl39.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl39.Appearance.Options.UseBackColor = true;
            this.panelControl39.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl39.Controls.Add(this.lblGio1);
            this.panelControl39.Controls.Add(this.lblNgay1);
            this.panelControl39.Controls.Add(this.lblPhim1);
            this.panelControl39.Controls.Add(this.lblRap1);
            this.panelControl39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl39.Location = new System.Drawing.Point(0, 0);
            this.panelControl39.Name = "panelControl39";
            this.panelControl39.Size = new System.Drawing.Size(700, 23);
            this.panelControl39.TabIndex = 46;
            // 
            // lblGio1
            // 
            this.lblGio1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGio1.Location = new System.Drawing.Point(519, 2);
            this.lblGio1.Margin = new System.Windows.Forms.Padding(4);
            this.lblGio1.Name = "lblGio1";
            this.lblGio1.Size = new System.Drawing.Size(24, 19);
            this.lblGio1.TabIndex = 49;
            this.lblGio1.Text = "Giờ";
            // 
            // lblNgay1
            // 
            this.lblNgay1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgay1.Location = new System.Drawing.Point(347, 2);
            this.lblNgay1.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgay1.Name = "lblNgay1";
            this.lblNgay1.Size = new System.Drawing.Size(36, 19);
            this.lblNgay1.TabIndex = 48;
            this.lblNgay1.Text = "Ngày";
            // 
            // lblPhim1
            // 
            this.lblPhim1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhim1.Location = new System.Drawing.Point(175, 2);
            this.lblPhim1.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhim1.Name = "lblPhim1";
            this.lblPhim1.Size = new System.Drawing.Size(36, 19);
            this.lblPhim1.TabIndex = 47;
            this.lblPhim1.Text = "Phim";
            // 
            // lblRap1
            // 
            this.lblRap1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRap1.Location = new System.Drawing.Point(2, 2);
            this.lblRap1.Margin = new System.Windows.Forms.Padding(4);
            this.lblRap1.Name = "lblRap1";
            this.lblRap1.Size = new System.Drawing.Size(27, 19);
            this.lblRap1.TabIndex = 46;
            this.lblRap1.Text = "Rạp";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.panelControl3.Size = new System.Drawing.Size(0, 49);
            this.panelControl3.TabIndex = 52;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit1.Location = new System.Drawing.Point(702, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(203, 49);
            this.pictureEdit1.TabIndex = 37;
            // 
            // timerDongho
            // 
            this.timerDongho.Enabled = true;
            this.timerDongho.Interval = 1000;
            this.timerDongho.Tick += new System.EventHandler(this.timerDongho_Tick);
            // 
            // Frm_RPDatve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 780);
            this.Controls.Add(this.panelControl25);
            this.Controls.Add(this.panelControl93);
            this.Controls.Add(this.panelControl37);
            this.Controls.Add(this.panelControl27);
            this.Controls.Add(this.panelControl23);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Frm_RPDatve";
            this.Text = "KingPro Ticket Management Software";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_RPDatve_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_RPDatve_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.pnlDanhSachDatVe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaive.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl93)).EndInit();
            this.panelControl93.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl94)).EndInit();
            this.panelControl94.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).EndInit();
            this.panelControl38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).EndInit();
            this.panelControl26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl95)).EndInit();
            this.panelControl95.ResumeLayout(false);
            this.panelControl95.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).EndInit();
            this.panelControl39.ResumeLayout(false);
            this.panelControl39.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.PanelControl panelControl26;
        private DevExpress.XtraEditors.SimpleButton btnRap333;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.SimpleButton btnPhim333;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraEditors.PanelControl panelControl38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.SimpleButton btnNgay333;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl93;
        private DevExpress.XtraEditors.PanelControl panelControl94;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private DevExpress.XtraEditors.SimpleButton btnGio333;
        private System.Windows.Forms.Panel pnlSodo;
        private System.Windows.Forms.Panel panel12;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MADIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn MAVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txtTongghe;
        private DevExpress.XtraEditors.TextEdit txtTongtien;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnInbiennhandatve;
        private DevExpress.XtraEditors.SimpleButton btnChonlaighe;
        private DevExpress.XtraEditors.SimpleButton btnChonSuatChieu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton btnChonnhieughe;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl95;
        private DevExpress.XtraEditors.LabelControl lblGio;
        private DevExpress.XtraEditors.LabelControl labelControl125;
        private DevExpress.XtraEditors.LabelControl lblNgay;
        private DevExpress.XtraEditors.LabelControl labelControl123;
        private DevExpress.XtraEditors.LabelControl lblPhim;
        private DevExpress.XtraEditors.LabelControl labelControl124;
        private DevExpress.XtraEditors.LabelControl lblRap;
        private DevExpress.XtraEditors.PanelControl panelControl39;
        private DevExpress.XtraEditors.LabelControl lblGio1;
        private DevExpress.XtraEditors.LabelControl lblNgay1;
        private DevExpress.XtraEditors.LabelControl lblPhim1;
        private DevExpress.XtraEditors.LabelControl lblRap1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.SimpleButton btnLenve;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LabelControl lbNgay;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.SimpleButton btnXoave;
        private DevExpress.XtraEditors.SimpleButton btnXuongve;
        private System.Windows.Forms.Timer timerDongho;
        private DevExpress.XtraEditors.LookUpEdit cboLoaive;
        private DevExpress.XtraEditors.SimpleButton btnDSDV;
        private System.Windows.Forms.Panel pnlDanhSachDatVe;
        private DevExpress.XtraEditors.SimpleButton btnThanhToan;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn MABARCODE;
        private DevExpress.XtraGrid.Columns.GridColumn TENPHIM;
        private DevExpress.XtraGrid.Columns.GridColumn TONGSOGHE;
        private DevExpress.XtraGrid.Columns.GridColumn TONGGIAVE;
        private DevExpress.XtraGrid.Columns.GridColumn MADAILY;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn BATDAU;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTOAN;
        private DevExpress.XtraGrid.Columns.GridColumn CONLAI;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn TENHANHKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraEditors.SimpleButton btnHuydatve;

    }
}