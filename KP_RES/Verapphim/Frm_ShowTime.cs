﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.XtraEditors.Popup;
using DevExpress.Utils.Win;
using System.Reflection;

namespace KP_RES 
{
    public partial class  Frm_ShowTime : DevExpress.XtraEditors.XtraForm
    {
        string []smaHinh;
        string[] smavideo;
        int trang = 1;
        DataTable dtHinh = new DataTable();
        int _tonghinhtrentrang;
        int _sohinhtheochieudai;
        int sotrang = 1;
        bool[] chooseList;
        string[] FileName;
        string[] FileNameHave;
        string[] FileNameHaveVideo;
        string[] FilePath;
        int soluonghinh;
        bool delete = true;
        bool play = true;

        public  Frm_ShowTime()
        {
            InitializeComponent();
            LoadCombo();
            LockControlImage1();
            LoadControlImage1();
            LoadVideo(false);
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
        }

        private void LoadData()
        {
            try
            {
                int width = 0;
                int height = 0;
                int widthScreen = Screen.PrimaryScreen.WorkingArea.Width;
                int heightScreen = Screen.PrimaryScreen.WorkingArea.Height;
                width = pn_cuon.Width;
                height = pn_cuon.Height;
                soluonghinh = int.Parse(clsMain.ReturnDataTable("select count(h.MAQC) from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=1 and LOAI=0 ").Rows[0][0].ToString());
                FileNameHave = new string[soluonghinh];
                chooseList = new bool[soluonghinh];
                smaHinh = new string[soluonghinh];
                for (int i = 0; i < soluonghinh; i++)
                {
                    chooseList[i] = false;
                    smaHinh[i] = "";
                }
                pn_cuon.Controls.Clear();
                int sohinhtheochieudai = width / 100;
                _sohinhtheochieudai = sohinhtheochieudai;
                int sohinhtheochieurong = height / 160;
                int tonghinhtrentrang = sohinhtheochieudai * sohinhtheochieurong;
                sotrang = soluonghinh / tonghinhtrentrang;
                if (soluonghinh % tonghinhtrentrang != 0)
                    sotrang++;
                _tonghinhtrentrang = tonghinhtrentrang;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load dữ liệu ảnh,hiển thị
      
        private void LoadImage(int tonghinhtrentrang, int trang)
        {
            try
            {
                pn_cuon.Controls.Clear();
                int tang = 1;
                int x =  5, y =  5;
                string sqlhinh = "exec sp_CHONHINHANH_NHAHANG_NS " + (tonghinhtrentrang * (trang - 1)) + "," + (tonghinhtrentrang * trang);
                dtHinh = clsMain.ReturnDataTable(sqlhinh);
                foreach (DataRow dr in dtHinh.Rows)
                {
                    FileNameHave[int.Parse(dr["STTHINH"].ToString())-1] = dr["TENHINH"].ToString();
                    Panel pn_Group = new Panel();
                    Panel pn_Hinh = new Panel();
                    Panel pn_Ten = new Panel();
                    Panel pn_Chon = new Panel();
                    CheckEdit checkChon = new CheckEdit();
                    LabelControl lbTen = new LabelControl();
                    PictureBox btn = new PictureBox();
                    pn_Group.BorderStyle = BorderStyle.FixedSingle;
                    checkChon.Font = btn.Font = btn_Xoa.Font;
                    lbTen.Font = new System.Drawing.Font("Tahoma", 10);
                    checkChon.Enabled = btn.Enabled = true;
                    btn.SizeMode = PictureBoxSizeMode.StretchImage;
                    btn.BorderStyle = BorderStyle.FixedSingle;
                    btn.Dock = DockStyle.Fill;
                    checkChon.Dock = DockStyle.Fill;
                    lbTen.Dock = DockStyle.Fill;
                    pn_Hinh.Dock = DockStyle.Fill;
                    pn_Ten.Dock = DockStyle.Bottom;
                    pn_Chon.Dock = DockStyle.Bottom;
                   // pn_Hinh.Click += new System.EventHandler(pnHinh_Click);// 
                    checkChon.Text = "Chọn";
                    lbTen.Name= checkChon.Name = btn.Name = dr["STTHINH"].ToString();
                    lbTen.Tag= checkChon.Tag = btn.Tag = dr["MAHINH"].ToString();
                    pn_Group.Name = "G" + dr["STTHINH"].ToString();
                    pn_Hinh.Name = "H" + dr["STTHINH"].ToString();
                    pn_Ten.Name = "T" + dr["STTHINH"].ToString();
                    pn_Chon.Name = "C" + dr["STTHINH"].ToString();
                    checkChon.Checked = chooseList[int.Parse(dr["STTHINH"].ToString())-1];
                    lbTen.Text = dr["TENHINH"].ToString();

                     pn_Chon.Size = new Size(90, 23);
                     pn_Ten.Size = new Size(90, 20);
                    pn_Group.Size = new Size(90, 150);
                    pn_Group.Location = new Point(x, y);
                    if (tang < _sohinhtheochieudai)
                    {
                        x += 100;
                        tang++;
                    }
                    else
                    {
                        x =  5;
                        y += 160;
                        tang = 1;
                    }

                    try
                    {
                        byte[] tam = new byte[((byte[])dr["URL"]).LongLength];
                        tam = (byte[])dr["URL"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btn.Image = bm;
                    }
                    catch
                    {
                    }
                    btn.Click += new System.EventHandler(bnt_hinh_Click);
                    checkChon.CheckedChanged += new System.EventHandler(checkChon_CheckedChanged);
                    lbTen.Click += new System.EventHandler(bnt_hinh_Click);
                    //checkChon.Click += new System.EventHandler(bnt_hinh_Click);
                    //btn.MouseDown += new System.Windows.Forms.MouseEventHandler(btban_MouseDown);
                    //btn.MouseMove += new System.Windows.Forms.MouseEventHandler(btban_MouseMove);
                    //btn.MouseUp += new System.Windows.Forms.MouseEventHandler(btban_MouseUp);
                    pn_Hinh.Controls.Add(btn);
                    pn_Ten.Controls.Add(lbTen);
                    pn_Chon.Controls.Add(checkChon);

                    pn_Group.Controls.Add(pn_Hinh);
                    pn_Group.Controls.Add(pn_Ten);
                    pn_Group.Controls.Add(pn_Chon);
                    pn_cuon.Controls.Add(pn_Group);
                  
                }
                lbl_sotrang.Text = trang + "/" + sotrang;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadCombo()
        {
            for (int i = 1; i < 100; i++)
            {
                cbThongBao.Items.Add(i);
                cbTenPhim.Items.Add(i);
                cbShowTime.Items.Add(i);
                cbHour.Items.Add(i);
                cbGhiChu.Items.Add(i);
            }
        }

        private void LoadVideo(bool choose)
        {
            string sql = "select MAHINH,TENHINH,cast('"+choose+"' as bit) as CHON from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=1 and LOAI=1";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl2.DataSource = dt;
            FileNameHaveVideo = new string[dt.Rows.Count];
            smavideo = new string[dt.Rows.Count];
            for(int i=0;i<dt.Rows.Count;i++)
            {
                FileNameHaveVideo[i]=dt.Rows[i]["TENHINH"].ToString();
                if (!choose)
                {
                    smavideo[i] = "";
                }
                else
                {
                    smavideo[i] = dt.Rows[i]["MAHINH"].ToString();
                }
            }
        }

        private bool checktenhinh(string name)
        {
            if (FileNameHave.Length != 0)
            {
                for (int i = 0; i < FileNameHave.Length; i++)
                {
                    if (name == FileNameHave[i])
                        return false;

                }
            }
            return true;
        }//kiểm tra 1 tên đã có trong danh sách hình chưa

        private bool checktenvideo(string name)
        {
            if (FileNameHaveVideo.Length != 0)
            {
                for (int i = 0; i < FileNameHaveVideo.Length; i++)
                {
                    if (name == FileNameHaveVideo[i])
                        return false;

                }
            }
            return true;
        }//kiểm tra 1 tên đã có trong danh sách video chưa

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
            LoadVideo(false);
        }//refresh lại dữ liêu

        private void bnt_tranglen_Click(object sender, EventArgs e)
        {
            if (trang > 1)
            {
                trang--;
                LoadImage(_tonghinhtrentrang,trang);
            }
        }//click vào nút lên

        private void bnt_trangxuong_Click(object sender, EventArgs e)
        {
            if (trang < sotrang)
            {
                trang++;
                LoadImage(_tonghinhtrentrang, trang);
            }
        }//click vào nút xuống

        private void bnt_hinh_Click(object sender, EventArgs e)
        {
            
            Control btn = (Control)sender;
            //int i=1;
            chooseList[int.Parse(btn.Name) - 1] = !chooseList[int.Parse(btn.Name) - 1];
            foreach (Panel pn in pn_cuon.Controls)
            {
                if (pn.Name == ("G" + btn.Name))
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name == ("C" + btn.Name))
                        {
                            foreach (CheckEdit chk in pn1.Controls)
                            {
                                chk.Checked = chooseList[int.Parse(btn.Name) - 1];
                                //break;
                            }
                            break;
                        }

                    }
                    break;
                }

            }


           
        }//sự kiện click vào hình

        private void pn_cuon_SizeChanged(object sender, EventArgs e)
        {
            LoadData();
            LoadImage(_tonghinhtrentrang, 1);
            trang = 1;
        }//xử ly sự kiện pannel chứa ảnh thay đổi kích thước

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }//click bàn phím
       
        private void btn_Them_Click(object sender, EventArgs e)
        {
            string smaQC = "";
            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = false;
                smaHinh[i] = "";
            }
            DataTable dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=1");
            if (dt.Rows.Count > 0)
            {

                smaQC = dt.Rows[0][0].ToString();
            }
            else
            {
                string sqlinsert = "insert QC_QUANGCAO(RUNTIMEIMAGE,TEXTCHU,RUNTIMETEXT,MODE,SOLUONGVE,RUNMENU,TIMEANLICH) values('5','','2','1','5','5','30')";
                clsMain.ExecuteSQL(sqlinsert);
                dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=1");
                smaQC = dt.Rows[0][0].ToString();
            }
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
            openFileDialog1.Filter = "Image Files|*.JPG;*.bmp;*.png;*.gif|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = true;

            openFileDialog1.Title = "Chọn ảnh";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                FileName = openFileDialog1.SafeFileNames;

                FilePath = openFileDialog1.FileNames;
                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i <FileName.Length ; i++)
                {
                    //picter.ImageLocation = FilePath[i];
                    
                    if (!checktenhinh(FileName[i]))
                    {
                        XtraMessageBox.Show("Hình "+FileName[i].ToUpper()+" đã có trong danh sách ảnh", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        continue;
                    }
                    try
                    {


                        Image img = Image.FromFile(FilePath[i]);
                   
                        Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img, typeof(Byte[]));
                        SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                        SqlCommand SqlComMain = null;
                        string sql = "";
                        int stt = 1;
                        DataTable dtSTT = clsMain.ReturnDataTable("select top(1) h.STT from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=1 and LOAI=0 order by STT desc");
                        if (dtSTT.Rows.Count > 0)
                        {
                            stt = int.Parse(dtSTT.Rows[0][0].ToString()) + 1;
                        }
                        sql += "INSERT INTO QC_HINHANH( TENHINH,URL,MAQC,SUDUNG,GHICHU,STT,LOAI) values ";
                        sql += "(@TENHINH,@URL,@MAQC,@SUDUNG,@GHICHU,@STT,@LOAI)" + "\n";
                        SqlCommand SqlCom = new SqlCommand(sql, CN);
                        SqlCom.Parameters.Add(new SqlParameter("@TENHINH", (object)FileName[i]));
                        SqlCom.Parameters.Add(new SqlParameter("@URL", (object)imageData));
                        SqlCom.Parameters.Add(new SqlParameter("@MAQC", (object)smaQC));
                        SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)true));
                        SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                        SqlCom.Parameters.Add(new SqlParameter("@STT", (object)stt));
                        SqlCom.Parameters.Add(new SqlParameter("@LOAI", (object)0));
                        SqlComMain = SqlCom;
                        CN.Open();
                        SqlComMain.ExecuteNonQuery();
                        CN.Close();
                    }
                    catch
                    {
                        continue;
                    }
                   
                }
                this.Cursor = Cursors.Default;
                LoadData();
                 trang = sotrang;
                LoadImage(_tonghinhtrentrang, sotrang);
                
            }
               
        }//thêm ảnh vào danh sách ảnh

        private bool checkChoose()//hàm kiểm tra xem có hình nào được chọn không
        {
            for (int i = 0; i < soluonghinh; i++)
            {
                if (smaHinh[i] != "")
                    return true;
            }
            return false;
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            try
            {
                string chuoi = "";
                if (!checkChoose())
                    throw new Exception("Bạn Chưa chọn hình cần xóa");
                for (int i = 0; i < soluonghinh; i++)
                {
                    if (smaHinh[i] != "")
                        chuoi += smaHinh[i]+",";
                }
                if (chuoi != "")
                    chuoi = chuoi.Remove(chuoi.Length-1);
                string sql = "delete from QC_HINHANH where MAHINH IN (" + chuoi + ")";
               
                if (clsMain.ExecuteSQL(sql))
                {
                    
                    LoadData();
                    if (trang > sotrang)
                    {
                        LoadImage(_tonghinhtrentrang, sotrang);
                        trang = sotrang;
                    }
                    else
                    {
                        LoadImage(_tonghinhtrentrang, trang);
                    }
                  
                   
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//xóa ảnh khỏi danh sách

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }//stt lưới

        private void btn_ThemVideo_Click(object sender, EventArgs e)
        {
            try
            {
                string smaQC = "";

                DataTable dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=1");
                if (dt.Rows.Count > 0)
                {

                    smaQC = dt.Rows[0][0].ToString();
                }
                else
                {
                    string sqlinsert = "insert QC_QUANGCAO(RUNTIMEIMAGE,TEXTCHU,RUNTIMETEXT,MODE,SOLUONGVE,RUNMENU,TIMEANLICH) values('5','','2','1','5','5','30')";
                    clsMain.ExecuteSQL(sqlinsert);
                    dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=1");
                    smaQC = dt.Rows[0][0].ToString();
                }

                OpenFileDialog opnFileDlg = new OpenFileDialog();
                
                opnFileDlg.Filter = "(mp3,wav,mp4,mov,wmv,mpg,avi,3gp,flv)|*.mp3;*.wav;*.mp4;*.3gp;*.avi;*.mov;*.flv;*.wmv;*.mpg|all files|*.*";
                opnFileDlg.FilterIndex = 1;
                opnFileDlg.RestoreDirectory = true;
                opnFileDlg.Multiselect = false;

                opnFileDlg.Title = "Chọn video";
                if (opnFileDlg.ShowDialog() == DialogResult.OK)
                {
                    FileName = opnFileDlg.SafeFileNames;
                    FilePath = opnFileDlg.FileNames;
                    this.Cursor = Cursors.WaitCursor;

                    for (int i = 0; i <= FileName.Length - 1; i++)
                    {
                        //picter.ImageLocation = FilePath[i];
                        if(!checktenvideo(FileName[i]))
                        {
                            throw new Exception("video " + FileName[i] .ToUpper()+ " đã có trong dữ liệu,bạn không cần thêm nữa");
                        }
                        FileStream stream = File.OpenRead(FilePath[i]);
                        if (stream.Length > 210000000)
                            throw new Exception("kích thước video không được lớn hơn 200MB,vui lòng chọn video khác");
                        byte[] fileBytes = new byte[stream.Length];
                        
                        stream.Read(fileBytes, 0, fileBytes.Length);
                        stream.Close();


                        SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                        SqlCommand SqlComMain = null;
                        string sql = "";
                        int stt = 1;
                        DataTable dtSTT = clsMain.ReturnDataTable("select top(1) h.STT from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=1 and LOAI=1 order by STT desc");
                        if (dtSTT.Rows.Count > 0)
                        {
                            stt = int.Parse(dtSTT.Rows[0][0].ToString()) + 1;
                        }
                        sql += "INSERT INTO QC_HINHANH( TENHINH,URL,MAQC,SUDUNG,GHICHU,STT,LOAI) values ";
                        sql += "(@TENHINH,@URL,@MAQC,@SUDUNG,@GHICHU,@STT,@LOAI)" + "\n";
                        SqlCommand SqlCom = new SqlCommand(sql, CN);
                        SqlCom.Parameters.Add(new SqlParameter("@TENHINH", (object)FileName[i]));
                        SqlCom.Parameters.Add(new SqlParameter("@URL", (object)fileBytes));
                        SqlCom.Parameters.Add(new SqlParameter("@MAQC", (object)smaQC));
                        SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)true));
                        SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                        SqlCom.Parameters.Add(new SqlParameter("@STT", (object)stt));
                        SqlCom.Parameters.Add(new SqlParameter("@LOAI", (object)1));
                        SqlComMain = SqlCom;
                        CN.Open();
                        SqlComMain.ExecuteNonQuery();
                        CN.Close();
                    }
                    LoadVideo(false);
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//thêm video vào danh sách

        private bool checkchon()
        {
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                if (bool.Parse(gridView2.GetFocusedRowCellValue("CHON").ToString()) == true)
                {
                    return true;
                }
            }
            return false;
        }//kiểm tra xem có video nào được chọn hay chưa

        private void setronglistvideo()
        {
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                smavideo[i] = "";
            }
        } //bỏ chọn video

        private void listchonvideo()
        {
            
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                if (bool.Parse(gridView2.GetFocusedRowCellValue("CHON").ToString()) == true)
                {
                    smavideo[i]= gridView2.GetFocusedRowCellValue("MAHINH").ToString();
                }
            }
          
        }//lấy tất cả mã video đã dc chọn

        private void btn_XoaVideo_Click(object sender, EventArgs e)
        {
            try
            {
                string videoIDlist = "";
                if (gridView2.RowCount == 0)
                {
                    return;
                }
                if (!checkchon())
                {
                    throw new Exception("Bạn chưa chọn video cần xóa");
                }
                this.Cursor = Cursors.WaitCursor;
                listchonvideo();
                for (int i = 0; i < smavideo.Length; i++)
                {
                    if (smavideo[i] != "")
                        videoIDlist += smavideo[i]+",";
                }
                if (videoIDlist != "")
                {
                    videoIDlist = videoIDlist.Remove(videoIDlist.Length - 1);
                    clsMain.ExecuteSQL("delete from QC_HINHANH where LOAI=1 and MAHINH in("+videoIDlist+")");
                    LoadVideo(false);
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }//xóa video

        private bool checkTonTai(string maquay, string mahinhvieo)
        {
            DataTable dtcheck = clsMain.ReturnDataTable("select MA_QUAY from QC_HINHANH_QUAY where  MA_QUAY='"+maquay+"' and MAHINH='"+mahinhvieo+"'");
            if (dtcheck.Rows.Count > 0)
                return true;
            return false;
        }//hàm kiểm tra xem hình anh ,video đã có trong quầy này chưa,true là có false là chưa              

        private void LoadALLMAHINH()
        {
            DataTable dtMH = clsMain.ReturnDataTable("Select MAHINH from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=1 and h.LOAI=0");
            for (int i = 0; i < dtMH.Rows.Count; i++)
            {
                smaHinh[i] = dtMH.Rows[i][0].ToString();
            }
        }//lấy tất cả mã hình

        private void btn_bochonhinh_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = false;
                smaHinh[i] = "";

            }
            LoadImage(_tonghinhtrentrang, trang);
            
        }//bỏ chọn hình

        private void btnbochonvideo_Click(object sender, EventArgs e)
        {
            LoadVideo(false);
           
        }//bỏ chọn video
      
        private void bnt_Play_Click(object sender, EventArgs e)
        {
            try
            {
                pn_cuon.Visible = false;
                //pn_xemvideo.Visible = !pn_xemvideo.Visible;

                if (play)
                {
                    play = false;
                    this.Cursor = Cursors.WaitCursor;
                    DataTable dt = clsMain.ReturnDataTable("Select TENHINH,URL from QC_HINHANH  where LOAI=1 and MAHINH='" + gridView2.GetFocusedRowCellValue("MAHINH").ToString() + "'");

                    //Begins the process of writing the byte array back to a file
                    byte[] fileBytes = new byte[((byte[])dt.Rows[0]["URL"]).LongLength];
                    fileBytes = (byte[])dt.Rows[0]["URL"];
                    Stream file = File.OpenWrite(Application.StartupPath + "/QCXEM/" + dt.Rows[0]["TENHINH"].ToString());
                    file.Write(fileBytes, 0, fileBytes.Length);
                    file.Close();
                    WindowsMediaPlayer.URL = Application.StartupPath + "/QCXEM/" + dt.Rows[0]["TENHINH"].ToString();
                    WindowsMediaPlayer.Ctlcontrols.play();
                    play = true;
                    this.Cursor = Cursors.Default;
                }

                pn_xemvideo.Visible = true;
            }
            catch
            {
                this.Cursor = Cursors.Default;
                play = true;
            }
            
        }
        
        private void btn_stop_Click(object sender, EventArgs e)
        {
            if (pn_xemvideo.Visible && delete==true)
            {
                delete = false;
                this.Cursor = Cursors.WaitCursor; 
                WindowsMediaPlayer.Ctlcontrols.stop();
                WindowsMediaPlayer.close();
                DirectoryInfo folder = new DirectoryInfo(Application.StartupPath + "/QCXEM");
                FileInfo []files = folder.GetFiles("*.*");
                foreach (FileInfo f in files)
                {

                   f.Delete();

                }
                this.Cursor = Cursors.Default;
                delete = true;
            }
            pn_xemvideo.Visible = false;
            pn_cuon.Visible = true;
            
        }

        private void checkChon_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit btn = (CheckEdit)sender;
            chooseList[int.Parse(btn.Name) - 1] = btn.Checked;
            if (chooseList[int.Parse(btn.Name) - 1])
                smaHinh[int.Parse(btn.Name) - 1] = btn.Tag.ToString();
            else
                smaHinh[int.Parse(btn.Name) - 1] = "";
        }

        private void btnRefesh_Click_1(object sender, EventArgs e)
        {
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
            LoadVideo(false);
            LoadControlImage1();
         
        }

        private void btnBanphim_Click_1(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void Frm_ShowTime_SizeChanged(object sender, EventArgs e)
        {
            //if (pn_cuon.Height <= 0)
            //    return;
            //LoadData();
            //LoadImage(_tonghinhtrentrang, trang);
        }

        private void LockControlImage1()
        {
            txtSLVe.Properties.ReadOnly = true;
            mTenRap.Properties.ReadOnly = true;
            fontChu.Properties.ReadOnly = true;
            colorEditNameFilm.Properties.ReadOnly = true;
            colorEditName.Properties.ReadOnly = true;
            colorEditNote.Properties.ReadOnly = true;
            colorEditManyTicket.Properties.ReadOnly = true;
            colorEditLitleTicket.Properties.ReadOnly = true;
            colorEditNoTicket.Properties.ReadOnly = true;
            chkRunVideo.Properties.ReadOnly = true;
            chkTextNote.Properties.ReadOnly = true;
            txtThoiGianMenu.Properties.ReadOnly = true;
            memoNoiDung.Properties.ReadOnly = true;
            txtLich.Properties.ReadOnly = true;
            pbBackGround.Enabled = false;
            cbGhiChu.Enabled = cbHour.Enabled = cbShowTime.Enabled = cbTenPhim.Enabled = cbThongBao.Enabled = false;
        }

        private void UnlockControlImage1()
        {
            txtSLVe.Properties.ReadOnly = false;
            mTenRap.Properties.ReadOnly = false;
            fontChu.Properties.ReadOnly = false;
            txtThoiGianMenu.Properties.ReadOnly = false;
            memoNoiDung.Properties.ReadOnly = false;
            colorEditNameFilm.Properties.ReadOnly = false;
            colorEditName.Properties.ReadOnly = false;
            colorEditNote.Properties.ReadOnly = false;
            colorEditManyTicket.Properties.ReadOnly = false;
            colorEditLitleTicket.Properties.ReadOnly = false;
            colorEditNoTicket.Properties.ReadOnly = false;
            chkRunVideo.Properties.ReadOnly = false;
            chkTextNote.Properties.ReadOnly = false;
            txtLich.Properties.ReadOnly = false;
            pbBackGround.Enabled = true;
            cbGhiChu.Enabled = cbHour.Enabled = cbShowTime.Enabled = cbTenPhim.Enabled = cbThongBao.Enabled = true;
        }

        private void ClearControlImage1()
        {
            txtSLVe.Text="";
            mTenRap.Text = "";
            fontChu.Text = "";
            colorEditNameFilm.Text = "";
            colorEditName.Text = "";
            colorEditNote.Text = "";
            colorEditManyTicket.Text = "";
            colorEditLitleTicket.Text = "";
            colorEditNoTicket.Text = "";
            chkRunVideo.Checked = true;
            chkTextNote.Checked = true;
            txtThoiGianMenu.Text = "";
            memoNoiDung.Text = "";
            txtLich.Text = "";
            pbBackGround.BackgroundImage = KP_RES.Properties.Resources.slr_camera_26;
            pbBackGround.BackgroundImageLayout = ImageLayout.Center;
            cbGhiChu.SelectedIndex = cbHour.SelectedIndex = cbShowTime.SelectedIndex = cbTenPhim.SelectedIndex = cbThongBao.SelectedIndex = 0;
        }

        private void LoadControlImage1()
        {
            DataTable dt = clsMain.ReturnDataTable("select * from QC_QUANGCAO where MODE=1");
            if (dt.Rows.Count > 0)
            {
                cbThongBao.Text = dt.Rows[0]["SIZESHOWHOUR"].ToString();
                cbTenPhim.Text = dt.Rows[0]["SIZEFILMNAME"].ToString();
                cbShowTime.Text = dt.Rows[0]["SIZEHEADER"].ToString();
                cbHour.Text = dt.Rows[0]["SIZEHOUR"].ToString();
                cbGhiChu.Text = dt.Rows[0]["SIZENOTE"].ToString();
                txtSLVe.Text = dt.Rows[0]["SOLUONGVE"].ToString();
                mTenRap.Text = dt.Rows[0]["TENRAP"].ToString();
                fontChu.Text = dt.Rows[0]["FONTTEXTCHU"].ToString();
                colorEditNameFilm.Text = dt.Rows[0]["COLORNAMEFILM"].ToString();
                colorEditName.Text = dt.Rows[0]["COLORNAME"].ToString();
                colorEditNote.Text = dt.Rows[0]["COLORNOTE"].ToString();
                colorEditManyTicket.Text = dt.Rows[0]["COLORManyTicket"].ToString();
                colorEditLitleTicket.Text = dt.Rows[0]["COLORLitleTicket"].ToString();
                colorEditNoTicket.Text = dt.Rows[0]["COLORNoTicket"].ToString();
                chkRunVideo.Checked =bool.Parse( dt.Rows[0]["ISRUNVIDEO"].ToString());
                chkTextNote.Checked = bool.Parse(dt.Rows[0]["IsRunTextNote"].ToString());
                txtThoiGianMenu.Text = dt.Rows[0]["RUNMENU"].ToString();
                memoNoiDung.Text = dt.Rows[0]["TEXTCHU"].ToString();
                txtLich.Text = dt.Rows[0]["TIMEANLICH"].ToString();
                try
                {
                    mTenRap.Font = new System.Drawing.Font(fontChu.Text, 12);
                    memoNoiDung.Font = new System.Drawing.Font("Tahoma", 12);
                    byte[] tam = new byte[((byte[])dt.Rows[0]["HINHNEN"]).LongLength];
                    tam = (byte[])dt.Rows[0]["HINHNEN"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    pbBackGround.BackgroundImage = bm;
                    pbBackGround.BackgroundImageLayout = ImageLayout.Stretch;
                }
                catch
                {
                    pbBackGround.BackgroundImage = KP_RES.Properties.Resources.slr_camera_26;
                    pbBackGround.BackgroundImageLayout = ImageLayout.Center;
                }
            }
        }

        private void CheckControlImage1()
        {
            if (txtSLVe.Text == "")
            {
                txtSLVe.Focus();
                throw new Exception("Bạn chưa nhập số lượng vé để tính sắp hết vé");
            }
            if (int.Parse(  txtSLVe.Text) == 0)
            {
                txtSLVe.Focus();
                throw new Exception("Số lượng vé để tính sắp hết vé phải khác 0");
            }
            if (txtThoiGianMenu.Text == "")
            {
                txtThoiGianMenu.Focus();
                throw new Exception("Bạn chưa nhập thời gian để thay đổi danh sách phim");
            }
           
            if (txtLich.Text == "")
            {
                txtLich.Focus();
                throw new Exception("Bạn chưa nhập thời gian để bỏ lịch chiếu");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            UnlockControlImage1();
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if(pbBackGround.Enabled)
                ClearControlImage1();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (pbBackGround.Enabled)
                {
                    string sql = "";
                    CheckControlImage1();
                    DataTable dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=1");
                    if (dt.Rows.Count > 0)
                    {
                        sql = "update QC_QUANGCAO set SOLUONGVE='" + txtSLVe.Text + "',RUNMENU='" + txtThoiGianMenu.Text + "',TEXTCHU=N'" + memoNoiDung.Text + "',TIMEANLICH='" + txtLich.Text + "',FONTTEXTCHU='" + fontChu.Text + "',TENRAP=N'" + mTenRap.Text + "',COLORNAMEFILM='" + colorEditNameFilm.Text + "',COLORNOTE='" + colorEditNote.Text + "',ISRUNVIDEO='" + chkRunVideo.Checked + "',COLORNAME='" + colorEditName.Text + "',ColorManyTicket='" + colorEditManyTicket.Text + "',ColorLitleTicket='" + colorEditLitleTicket.Text + "',ColorNoTicket='" + colorEditNoTicket.Text + "',IsRunTextNote='" + chkTextNote.Checked + "',SIZENOTE=" + cbGhiChu.Text + ",SIZEHOUR=" + cbHour.Text + ",SIZEHEADER=" + cbShowTime.Text + ",SIZEFILMNAME=" + cbTenPhim.Text + ",SIZESHOWHOUR="+cbThongBao.Text+" where MODE=1";
                        if (clsMain.ExecuteSQL(sql))
                        {
                           
                            string sql1 = "";                            
                            
                            if (pbBackGround.BackgroundImageLayout == ImageLayout.Center)
                            {
                                sql1 = "Update QC_QUANGCAO set HINHNEN=NULL where MODE=1 ";
                                clsMain.ExecuteSQL(sql);
                            }
                            else
                            {
                                Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(pbBackGround.BackgroundImage, typeof(Byte[]));
                                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                                SqlCommand SqlComMain = null;                                
                                sql1 = "Update QC_QUANGCAO set HINHNEN=@HINHNEN where MODE=1 ";
                                SqlCommand SqlCom = new SqlCommand(sql1, CN);
                                SqlCom.Parameters.Add(new SqlParameter("@HINHNEN", (object)imageData));

                                SqlComMain = SqlCom;
                                CN.Open();
                                SqlComMain.ExecuteNonQuery();
                                LockControlImage1();
                                CN.Close();

                            }
                            
                            throw new Exception("Cập Nhật Thành Công");

                        }
                        else
                        {
                            throw new Exception("Lưu Thất Bại");
                        }

                    }
                    else
                    {
                        sql = "insert QC_QUANGCAO(SOLUONGVE,RUNMENU,TEXTCHU,MODE,TIMEANLICH,FONTTEXTCHU,TENRAP,COLORNAMEFILM,COLORNOTE,ISRUNVIDEO,COLORNAME,ColorManyTicket,ColorLitleTicket,ColorNoTicket,IsRunTextNote,SIZENOTE,SIZEHOUR,SIZEHEADER,SIZEFILMNAME,SIZESHOWHOUR) values('" + txtSLVe.Text + "','" + txtThoiGianMenu.Text + "',N'" + memoNoiDung.Text + "','1','" + txtLich.Text + ",'" + fontChu.Text + "','" + mTenRap.Text + "','" + colorEditNameFilm.Color + "','" + colorEditNote.Color + "','" + chkRunVideo.Checked + "','" + colorEditName.Text + "','" + colorEditManyTicket.Text + "','" + colorEditLitleTicket.Text + "','" + colorEditNoTicket.Text + "','" + chkTextNote.Checked + "',"+cbGhiChu.Text+","+cbHour.Text+","+cbShowTime.Text+","+cbTenPhim.Text+","+cbThongBao.Text+")";
                        if (clsMain.ExecuteSQL(sql))
                        {                            
                            string sql1 = "";
                            if (pbBackGround.BackgroundImageLayout == ImageLayout.Center)
                            {
                                sql1 = "Update QC_QUANGCAO set HINHNEN=NULL where MODE=1 ";
                            }
                            else
                            {
                                Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(pbBackGround.BackgroundImage, typeof(Byte[]));
                                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                                SqlCommand SqlComMain = null;
                                SqlCommand SqlCom = new SqlCommand(sql1, CN);
                                //SqlCom.CommandType = CommandType.StoredProcedure;
                                sql1 = "Update QC_QUANGCAO set HINHNEN=@HINHNEN where MODE=1 ";
                                SqlCom.Parameters.Add(new SqlParameter("@HINHNEN", (object)imageData));
                                SqlComMain = SqlCom;
                                CN.Open();
                                SqlComMain.ExecuteNonQuery();
                                LockControlImage1();
                                CN.Close();
                            }
                           
                            LockControlImage1();

                            throw new Exception("Lưu Thành Công");
                        }
                        else
                        {
                            throw new Exception("Lưu Thất Bại");
                        }
                    }
                }
                
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message,"Thông Báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void btnChonTatCaHinh_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = true;
                    
            }
            LoadALLMAHINH();
            LoadImage(_tonghinhtrentrang, trang);
        }

        private void btnChonTatCaVideo_Click(object sender, EventArgs e)
        {
            LoadVideo(true);
        }

        private void fontChu_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            try
            {
                mTenRap.Font = new System.Drawing.Font(fontChu.Text, 12);
            }
            catch
            {
            }
        }

        private void fontEdit1_Properties_Popup(object sender, EventArgs e)
        {
            PopupListBoxForm f = (sender as IPopupControl).PopupWindow as PopupListBoxForm;
            f.ListBox.MouseMove += new MouseEventHandler(ListBox_MouseMove); 
        }

        private void ListBox_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < (sender as PopupListBox).ItemCount; i++)
            {
                if ((sender as PopupListBox).GetItemRectangle(i).Contains(new Point(e.X, e.Y)))
                {
                    (sender as PopupListBox).SelectedIndex = i;
                    MethodInfo mi = typeof(PopupBaseEdit).GetMethod("AcceptPopupValue", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                    mi.Invoke((sender as PopupListBox).OwnerEdit, new object[] { (sender as PopupListBox).Text });
                    break;
                }
            }
        }

        private void pbBackGround_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
            openFileDialog1.Filter = "Image Files|*.JPG;*.bmp;*.png;*.gif|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = false;
            openFileDialog1.Title = "Chọn Hình Nền";
            string sBackGroundName = "";
            string sBackGroundPath = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                sBackGroundName = openFileDialog1.SafeFileName;
                sBackGroundPath = openFileDialog1.FileName;                
                Image img = Image.FromFile(sBackGroundPath);
                pbBackGround.BackgroundImage = img;
                pbBackGround.BackgroundImageLayout = ImageLayout.Stretch;
                
            }
        }

        private void colorEditNameFilm_EditValueChanged(object sender, EventArgs e)
        {
            //mTenRap.ForeColor = colorEditNameFilm.Color;
        }

        private void colorEditNote_EditValueChanged(object sender, EventArgs e)
        {
            //memoNoiDung.ForeColor = colorEditNote.Color;
        }

        private void cbShowTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            mTenRap.Font = new System.Drawing.Font(fontChu.Text,12);
        }

        private void cbGhiChu_SelectedIndexChanged(object sender, EventArgs e)
        {
            memoNoiDung.Font = new System.Drawing.Font("Tahoma", 12);
        }
    }
}