﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;
using System.Collections;

namespace KP_RES
{
    public partial class PF_Invedattruoc : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        string ControlName = "";
        public ArrayList danhsachvedat = new ArrayList();
        public string sMalichchieu = "";
        public PF_Invedattruoc()
        {
            InitializeComponent();
            txtMave .GotFocus += new EventHandler(txtMave_GotFocus);
            txtSodienthoai.GotFocus += new EventHandler(txtMave_GotFocus);
        }

        private void PF_Invedattruoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtMave_GotFocus(object sender, System.EventArgs e)
        {
            TextEdit txt = (TextEdit)sender;
            ControlName = txt.Name;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = txtMave.Text + btn.Text;
            }
            else if (ControlName == txtSodienthoai.Name)
            {
                txtSodienthoai.Text = txtSodienthoai.Text + btn.Text;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = "";
            }
            else
            {
                txtSodienthoai.Text = "";
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "" && txtSodienthoai.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            pnlReport.Controls.Clear();
            string sSQL = "";
            sSQL = "";
            sSQL += "Select A.MAVE,A.BIENNHAN,A.MABARCODE,A.SOGHE,A.MALOAIVE,A.DONGIA,A.TENHANHKHACH,A.DIENTHOAI,A.EMAIL,A.NGAYTAO,C.MABARCODE As MAPHIEU,C.TONGGIAVE,C.DATHANHTOAN,C.TONGSOGHE,D.MALICHCHIEU,D.NGAYCHIEU,D.BATDAU,E.TENPHONG,F.TENPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
            sSQL += "From VE A , DM_GHE B , BIENNHAN C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN H,SYS_USER I" + "\n";
            sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
            sSQL += "And A.BIENNHAN = C.MABARCODE" + "\n";
            sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
            sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And A.NGUOITAO = I.UserID" + "\n";
            sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
            sSQL += "And D.NGAYCHIEU >= " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
            sSQL += "And C.TRANGTHAI =" + clsMain.SQLString("0") + "\n";
            sSQL += "And C.DATHANHTOAN =" + clsMain.SQLString("0") + "\n";
            if (txtMave.Text != "")
            {
                sSQL += "And A.BIENNHAN =" + clsMain.SQLString(txtMave.Text) + "\n";
            }
            else
            {
                sSQL += "And A.DIENTHOAI =" + clsMain.SQLString(txtSodienthoai.Text) + "\n";
            }
            sSQL += "Order by A.SOGHE" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng . Hoặc đã hủy . Hoặc đã lấy vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
            }
            else
            {
                txtMave .Text = dt.Rows[0]["BIENNHAN"].ToString ();
                Frm_Report1 frm = new Frm_Report1();
                frm.ReportName = "rptBiennhan1";
                frm.DataSource = dt;
                frm.IsPrint = false;
                frm.PrinterName = clsKP_Terminal.Mayinbill;
                frm.ExportName = "In ve";
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.TopLevel = false;
                frm.Parent = pnlReport;
                frm.Show();
                btnInve.Focus();
            }
        }

        private void btnInve_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
                      
            //bool bInve = false;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận in vé cho biên nhận này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Windows.Forms.DialogResult kq = DevExpress.XtraEditors.XtraMessageBox.Show("Chọn chế độ in vé ?" + "\n" + "Yes : In vé ngay" + "\n" + "No : Muốn thay đổi lại loại vé,mua thêm ghế...", "Thông báo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton .Button3 );
                if ( kq== System.Windows.Forms.DialogResult.Yes)
                {
                    clsKP_Terminal.OpenCashDrawer(true);
                    string sSQL;
                    foreach (DataRow dr in dt.Rows)
                    {
                        sSQL = "";
                        sSQL += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE,C.MALOAIGHE,C.TENLOAIGHE,A.MALOAIVE,V.TENLOAIVE,A.DONGIA,A.NGUOITAO As NGUOIBAN,A.NGAYTAO,A.MALOAIVE,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.MACHATLUONGPHIM,D.MALICHCHIEU,E.TENPHONG,E.MAPHONG,F.TENPHIM,F.MAPHIM,G.TENCHATLUONGPHIM,H.TENNHANVIEN AS NGUOITAO" + "\n";
                        sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I,DM_LOAIVE V" + "\n";
                        sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                        sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                        sSQL += "And A.MALOAIVE = V.MALOAIVE" + "\n";
                        sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                        sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                        sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                        sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
                        sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                        sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                        sSQL += "And A.MAVE =" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                        DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                        dt1.Rows[0]["NGUOITAO"] = clsUserManagement.ReturnNameOfUserLogin();//18/05/2015-bo sung-19/03/2015 Update cho bao cao ban ve khong nhay ten nhan vien ai in ve thi tinh cho nguoi do
                        // 16/5/2015 bổ sung recheck trùng vé trước khi đưa vào vé tổng hợp
                        sSQL = "";
                        sSQL += "Select MAVETONGHOP " + "\n";
                        sSQL += "From VETONGHOP " + "\n";
                        sSQL += "Where MALICHCHIEU = " + clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + "\n";
                        sSQL += "And SOGHE = " + clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + "\n";
                        DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                        if (dt2.Rows.Count % 2 != 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Vé đã xuất.Vui lòng chọn ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            continue;
                        }
                        //21/09/2015 bổ sung sửa lại lổi nhãy mã vé
                        string sMabarcode_Final = cls_KP_RES.CreateBarcode_Final("VE");
                        dt1.Rows[0]["MABARCODE"] = sMabarcode_Final;
                        sSQL = "";
                        sSQL += "Update VE Set " + "\n";
                        sSQL += "MABARCODE=" + clsMain.SQLString(sMabarcode_Final) + "," + "\n";
                        sSQL += "DATHANHTOAN=" + clsMain.SQLString(dr["DONGIA"].ToString()) + "," + "\n";
                        sSQL += "CONLAI=" + clsMain.SQLString("0") + "\n";
                        sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                        sSQL += "Update BIENNHAN Set " + "\n";
                        sSQL += "TRANGTHAI=" + clsMain.SQLString("1") + "\n";
                        sSQL += "Where MABARCODE=" + clsMain.SQLString(dr["BIENNHAN"].ToString()) + "\n";
                        // 25/2/2014 bổ sung ghi vào sổ bán vé tổng hợp để báo cáo tiền cho chính xác
                        sSQL += "Insert into VETONGHOP (LOAIVE,NGUOIBAN,NGAYBAN,MABARCODE,MALICHCHIEU,SOGHE,MAPHONG,MAPHIM,MALOAIGHE,MALOAIVE,DONGIA,MAQUAY,NGUOITAO,NGAYTAO)" + "\n";
                        sSQL += "Values ( ";
                        sSQL += clsMain.SQLStringUnicode("Vé bán") + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";//19/03/2015 Update cho bao cao ban ve khong nhay ten nhan vien ai in ve thi tinh cho nguoi do
                        sSQL += clsMain.SQLString(((DateTime)dt1.Rows[0]["NGAYTAO"]).ToString("dd/MM/yyyy HH : mm")) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MABARCODE"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHONG"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHIM"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIGHE"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIVE"].ToString()) + ",";
                        sSQL += clsMain.SQLString(dt1.Rows[0]["DONGIA"].ToString()) + ",";
                        sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                        sSQL += "GETDATE()" + ")";

                        this.Cursor = Cursors.WaitCursor;
                        Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                        this.Cursor = Cursors.Default;

                        if (bRunSQL)
                        {
                           
                            Frm_Report1 frm = new Frm_Report1();
                            if (cls_KP_RES.Mode == 2)
                            {
                                System.Threading.Thread.Sleep(1000);
                                frm.ReportName = "rptVerapphimIonah";
                            }
                            else if (cls_KP_RES.Mode == 11)
                            {
                                frm.ReportName = "rptVerapphim_Bill";
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(1000);
                                frm.ReportName = "rptVerapphim";
                            }
                            frm.DataSource = dt1;
                            frm.IsPrint = true;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = clsKP_Terminal.Mayinve;
                            frm.ExportName = "In ve";
                            frm.ShowDialog(this);
                            frm.Dispose();
                            //bInve = true;
                        }
                        else
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé tổng hợp không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else if (kq == System.Windows.Forms.DialogResult.No)
                {
                    if (dt.Rows[0]["MALICHCHIEU"].ToString() != sMalichchieu)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Phải chọn sơ đồ ghế của suất chiếu đã đặt vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnDong.Focus();
                        return;
                    }
                    string sSQL;
                    sSQL = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        danhsachvedat.Add(dr["MAVE"].ToString());
                        sSQL += "Update VE Set " + "\n";
                        sSQL += "NGUOITAO=" + clsMain.SQLString(clsGlobal .gsUserID ) + "\n";
                        sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                    }
                    sSQL += "Update BIENNHAN Set " + "\n";
                    sSQL += "TRANGTHAI=" + clsMain.SQLString("1") + "\n";
                    sSQL += "Where MABARCODE=" + clsMain.SQLString(dt.Rows[0]["BIENNHAN"].ToString()) + "\n";
                    this.Cursor = Cursors.WaitCursor;
                    Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                    this.Cursor = Cursors.Default;

                    if (bRunSQL)
                    {
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé tổng hợp không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    return;
                }
                pnlReport.Controls.Clear();
                dt.Clear();
                txtMave.Text = "";
                txtSodienthoai.Text = "";
                txtMave.Focus();
                //if (bInve)
                //{
                //    clsKP_Terminal.OpenCashDrawer(true);
                //}
                this.Close();
            }
        }

        private void btnHuyDatve_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận hủy đặt vé cho biên nhận này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                string sSQL="";
                foreach (DataRow dr in dt.Rows)
                {
                    sSQL += "Update VE Set " + "\n";
                    sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                    sSQL += "TENHANHKHACH=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "DIENTHOAI=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "EMAIL=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "BIENNHAN=" + clsMain.SQLString("") + "," + "\n";
                    sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "," + "\n";
                    sSQL += "DONGIA=" + clsMain.SQLString("1") + "," + "\n";
                    sSQL += "PHUTHU=" + clsMain.SQLString("0") + "," + "\n";
                    sSQL += "DATHANHTOAN=" + clsMain.SQLString("0") + "," + "\n";
                    sSQL += "CONLAI=" + clsMain.SQLString("1") + "\n";
                    sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                    sSQL += "Update BIENNHAN Set " + "\n";
                    sSQL += "TRANGTHAI=" + clsMain.SQLString("-1") + "\n";
                    sSQL += "Where MABARCODE=" + clsMain.SQLString(dr["BIENNHAN"].ToString()) + "\n";
                }
                bool bRunSQL = clsMain.ExecuteSQL(sSQL);
                if (bRunSQL)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Đã hủy đặt vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pnlReport.Controls.Clear();
                    dt.Clear();
                    txtMave.Text = "";
                    txtSodienthoai.Text = "";
                    txtMave.Focus();
                }
            }
        }
    }
}