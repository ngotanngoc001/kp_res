﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Report;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Payment_Rapphim : DevExpress.XtraEditors.XtraForm
    {
        TextEdit txtInput=new TextEdit();//txt get input
        public bool bInve = false;
        decimal _itongtien = 0;
        bool bShift = true;//true is upperText,false is lower text
        bool bText = false;//true is permit input,false is not permit
        bool bPrintBill = true;//true print bill.false is not print bill
        bool bVisaCard = true; //true is not payment visa,false were payment visa
        bool bVoucher = true; //true is not payment Voucher,false were payment Voucher
        string idDaiLy = ""; //ID đại lý
        string idVoucher = "";//ID Voucher
        string idKHTT = "";//ID khách hàng thân thiết
        decimal dChietKhau = 0;//chiet khấu cho đại lý
        decimal dMoneyVisa = 0;//total payment = visa
        decimal dMoneyVoucher = 0;//total payment = voucher
        decimal dMoneyInnerCard = 0;//total payment = inner card
        decimal dMoneyCash = 0;//total payment = Cash
        public string soThe = "0";
        public string ngayThe = "0";
        decimal _dMoneygetFirst = 0;
        decimal _dMoneyHour = 0;
        decimal per_Discount = 0;
        decimal per_Extra = 0;
        DataTable dtGridviewProduct=new DataTable();//data list product sale 
        bool flag_GiamTT_DaiLy = true;
        public bool CN_Flag = false;
        public string CN_MaKH = "";
        decimal CN_SoTienTra = 0;
        DateTime CN_ngayHenTra = DateTime.Now;

        public Payment_Rapphim(decimal iTongtien, DataTable dtGridview)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
            _itongtien = iTongtien;
            dtGridviewProduct.Columns.Add("TENLOAIVE");
            DataTable dtTemp = clsMain.ReturnDataTable("Select MALOAIVE,TENLOAIVE From DM_LOAIVE");
            for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
            {
                string sSQL = " MALOAIVE=" + clsMain.SQLString(dtGridviewProduct.Rows[i]["MALOAIVE"].ToString());
                DataTable dt1 = dtTemp.Copy();
                dt1.DefaultView.RowFilter = sSQL;
                dt1 = dt1.DefaultView.ToTable();
                dtGridviewProduct.Rows[i]["TENLOAIVE"] = dt1.Rows[0]["TENLOAIVE"].ToString();
            }
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            LoadForiegnCurrency();
            LoadDataPrepare();
            LoadKeyboardMoney();
            LoadKeyboardNumber();
            LoadKeyboardText();
        }

        private void Payment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                bPrintBill = true;
                PrintBill();

            }
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (e.KeyCode == Keys.Delete)
            {

                if (iLenght > 2)
                {
                    if (iPosition == 0)
                    {
                        if (txtInput.Text[1] == ',')
                            txtInput.Text = txtInput.Text.Remove(0, 2);
                        else
                            txtInput.Text = txtInput.Text.Remove(0, 1);

                        txtInput.SelectionStart = 0;
                        e.Handled = true;
                    }
                }
            }
            else if (e.KeyCode == Keys.Back)
            {

                if (iPosition == 1)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                }
                else if (iPosition == 2)
                {
                    if (txtInput.Text[1] == ',')
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 2, 2);
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
        }

        private void LoadForiegnCurrency()
        {
            DataTable dtForiegnCurrency = clsMain.ReturnDataTable("exec GETTYGIA");
            gcForeignCurrency.DataSource = dtForiegnCurrency;
            if (dtForiegnCurrency.Rows.Count > 0)
            {
                lbKindMoney.Text = dtForiegnCurrency.Rows[0]["MA"].ToString();
                lbExchangeRate.Text = dtForiegnCurrency.Rows[0]["TYGIA"].ToString();
            }

            string sSQL = "";
            sSQL += "Select MAPHUPHI,TENPHUPHI,GIATRI,GHICHU,SUDUNG" + "\n";
            sSQL += "From PHUPHI" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by TENPHUPHI" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            grdPhuthu.DataSource = dt;
        }

        private void LoadDataPrepare()
        {
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
            gcListProduct.DataSource = dtGridviewProduct;
            decimal fTotalMoney = 0;
            if (dtGridviewProduct.Rows.Count > 0)
            {
                for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                {
                    fTotalMoney += decimal.Parse(dtGridviewProduct.Rows[i]["PHUTHU"].ToString());
                }
            }
            if (dtGridviewProduct.Rows.Count >= 7)
            {
                pnUpDownGridview.Visible = true;
            }
            lbTheMoney.Tag = fTotalMoney;
            lbTheMoney.Text = string.Format("{0:#,###0}", fTotalMoney);
            lbHourMoney.Tag = _dMoneyHour;
            lbHourMoney.Text = string.Format("{0:#,###0}", _dMoneyHour);
            lbMoneyGetFirst.Tag = _dMoneygetFirst;
            lbMoneyGetFirst.Text = string.Format("{0:#,###0}", _dMoneygetFirst);
            lbReplyCustomer.Text = "0";

            pnlForeignCurrency.Size = new Size((pnlBottomPayment.Width / 100) * 35, 294);
            pnlPhiPhuVu.Visible = pnlTTTNB.Visible = pnlKHTT.Visible = pnlChietKhau.Visible = pnlVoucher.Visible = false;
            pnlTienKhachTra.Visible = true;
            int iWidth = pnlMoneyReply.Width + pnlMoneyReply.Location.X - lbVNDReply.Width - pnlKeyboardAndShowForeignCurrency.Location.X;
            txtInnerCustomer.Size = txtDearCustomer.Size = txtInputMoney.Size = txtServiceMoney.Size = txtInputDiscount.Size = txtInputVoucher.Size = new Size(iWidth, 32);
            txtInnerCustomer.Location = txtDearCustomer.Location = txtInputMoney.Location = txtServiceMoney.Location = txtInputDiscount.Location = txtInputVoucher.Location = new Point(pnlKeyboardAndShowForeignCurrency.Location.X - pnlChietKhau.Location.X, 7);

            pnlKeyboardMoney.BringToFront();
            pnlShowInforPayment.BringToFront();
            txtInput = txtInputMoney;
            txtInput.KeyPress += new KeyPressEventHandler(txtInput_KeyPress);
            txtInput.Focus();
            txtInput.Text = lbTotalMoneyAll.Text;
            txtInput.SelectAll();
        }

        private void LoadKeyboardMoney()
        {
            int iWidth = (pnlKeyboardMoney.Width - 5) / 5;
            int iSurplusWidth = (pnlKeyboardMoney.Width - 5) % 5;
            int iHeight = (pnlKeyboardMoney.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardMoney.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnMoveLeftMoney.Size = btnMoveRightMoney.Size = btnDeleteMoney.Size = btnTranslateMoney.Size = btnEcsMoney.Size = btnOkMoney.Size = btn500.Size = btn1000.Size = btn2000.Size = btn5000.Size = btn10000.Size = btn20000.Size = btn50000.Size = btn100000.Size = btn200000.Size = btn500000.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentMoney.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            //set location for button
            //dòng 1
            btn500.Location = new Point(iAlignWidth, iAlignHeight);
            btn1000.Location = new Point(btn500.Location.X + iWidth, btn500.Location.Y);
            btn2000.Location = new Point(btn1000.Location.X + iWidth, btn500.Location.Y);
            btn5000.Location = new Point(btn2000.Location.X + iWidth, btn500.Location.Y);
            btn10000.Location = new Point(btn5000.Location.X + iWidth, btn500.Location.Y);
            //dòng 2
            btn20000.Location = new Point(iAlignWidth, btn500.Location.Y + iHeight);
            btn50000.Location = new Point(btn20000.Location.X + iWidth, btn20000.Location.Y);
            btn100000.Location = new Point(btn50000.Location.X + iWidth, btn20000.Location.Y);
            btn200000.Location = new Point(btn100000.Location.X + iWidth, btn20000.Location.Y);
            btn500000.Location = new Point(btn200000.Location.X + iWidth, btn20000.Location.Y);
            //dòng 3
            btnMoveLeftMoney.Location = new Point(iAlignWidth, btn20000.Location.Y + iHeight);
            btnMoveRightMoney.Location = new Point(btnMoveLeftMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnDeleteMoney.Location = new Point(btnMoveRightMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnPaymentMoney.Location = new Point(btnDeleteMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            //dòng 4
            btnTranslateMoney.Location = new Point(iAlignWidth, btnMoveLeftMoney.Location.Y + iHeight);
            btnEcsMoney.Location = new Point(btnTranslateMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btnOkMoney.Location = new Point(btnEcsMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
        }

        private void LoadKeyboardNumber()
        {
            int iWidth = (pnlKeyboardNumber.Width - 5) / 6;
            int iSurplusWidth = (pnlKeyboardNumber.Width - 5) % 6;
            int iHeight = (pnlKeyboardNumber.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardNumber.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnDeleteNumber.Size = btnMoveLeftNumber.Size = btnMoveRightNumber.Size = btn0.Size = btn00.Size = btn000.Size = btn1.Size = btn2.Size = btn3.Size = btn4.Size = btn5.Size = btn6.Size = btn7.Size = btn8.Size = btn9.Size = btnTranslateNumber.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentNumber.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            btnEscNumber.Size = new Size(iWidth - 5, iHeight * 2 - 5);
            btnOkNumber.Size = new Size(iWidth * 2 - 5, iHeight - 5);
            //set location for button
            //dòng 1
            btn7.Location = new Point(iAlignWidth, iAlignHeight);
            btn8.Location = new Point(btn7.Location.X + iWidth, btn7.Location.Y);
            btn9.Location = new Point(btn8.Location.X + iWidth, btn7.Location.Y);
            btnMoveLeftNumber.Location = new Point(btn9.Location.X + iWidth, btn7.Location.Y);
            btnMoveRightNumber.Location = new Point(btnMoveLeftNumber.Location.X + iWidth, btn7.Location.Y);
            btnDeleteNumber.Location = new Point(btnMoveRightNumber.Location.X + iWidth, btn7.Location.Y);
            //dòng 2
            btn4.Location = new Point(iAlignWidth, btn7.Location.Y + iHeight);
            btn5.Location = new Point(btn4.Location.X + iWidth, btn4.Location.Y);
            btn6.Location = new Point(btn5.Location.X + iWidth, btn4.Location.Y);
            btnEscNumber.Location = new Point(btn6.Location.X + iWidth, btn4.Location.Y);
            btnOkNumber.Location = new Point(btnEscNumber.Location.X + iWidth, btn4.Location.Y);
            //dòng 3
            btn1.Location = new Point(iAlignWidth, btn4.Location.Y + iHeight);
            btn2.Location = new Point(btn1.Location.X + iWidth, btn1.Location.Y);
            btn3.Location = new Point(btn2.Location.X + iWidth, btn1.Location.Y);
            btnPaymentNumber.Location = new Point(btnOkNumber.Location.X, btn1.Location.Y);
            //dòng 4
            btnTranslateNumber.Location = new Point(iAlignWidth, btn1.Location.Y + iHeight);
            btn0.Location = new Point(btnTranslateNumber.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn00.Location = new Point(btn0.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn000.Location = new Point(btn00.Location.X + iWidth, btnTranslateMoney.Location.Y);
        }

        private void LoadKeyboardText()
        {
            int iWidth = (pnlKeyBoardText.Width - 5) / 10;
            int iSurplusWidth = (pnlKeyBoardText.Width - 5) % 10;
            int iHeight = (pnlKeyBoardText.Height - 5) / 5;
            int iSurplusHeight = (pnlKeyBoardText.Height - 5) % 5;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnQ.Size = btnW.Size = btnE.Size = btnR.Size = btnT.Size = btnY.Size = btnU.Size = btnI.Size = btnO.Size = btnP.Size = btnA.Size = btnS.Size = btnD.Size = btnF.Size = btnG.Size = btnH.Size = btnJ.Size = btnK.Size = btnL.Size = btnZ.Size = btnX.Size = btnC.Size = btnV.Size = btnB.Size = btnN.Size = btnM.Size = btnMoveLeftText.Size = btnMoveRigthText.Size = new Size(iWidth - 5, iHeight - 5);
            btnTranslateText.Size = new Size(iWidth * 2 - 5, iHeight - 5);
            btnShift.Size = btnEscText.Size = btnDeleteText.Size = new Size(iWidth * 3 - 5, iHeight - 5);
            btnPaymentText.Size = new Size(iWidth * 3 - 5, iHeight * 2 - 5);
            btnOkText.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);

            //set location for button
            //dòng 1
            btnQ.Location = new Point(iAlignWidth, iAlignHeight);
            btnW.Location = new Point(btnQ.Location.X + iWidth, btnQ.Location.Y);
            btnE.Location = new Point(btnW.Location.X + iWidth, btnQ.Location.Y);
            btnR.Location = new Point(btnE.Location.X + iWidth, btnQ.Location.Y);
            btnT.Location = new Point(btnR.Location.X + iWidth, btnQ.Location.Y);
            btnY.Location = new Point(btnT.Location.X + iWidth, btnQ.Location.Y);
            btnU.Location = new Point(btnY.Location.X + iWidth, btnQ.Location.Y);
            btnI.Location = new Point(btnU.Location.X + iWidth, btnQ.Location.Y);
            btnO.Location = new Point(btnI.Location.X + iWidth, btnQ.Location.Y);
            btnP.Location = new Point(btnO.Location.X + iWidth, btnQ.Location.Y);
            //dòng 2
            btnA.Location = new Point(iAlignWidth + iWidth / 2, btnQ.Location.Y + iHeight);
            btnS.Location = new Point(btnA.Location.X + iWidth, btnA.Location.Y);
            btnD.Location = new Point(btnS.Location.X + iWidth, btnA.Location.Y);
            btnF.Location = new Point(btnD.Location.X + iWidth, btnA.Location.Y);
            btnG.Location = new Point(btnF.Location.X + iWidth, btnA.Location.Y);
            btnH.Location = new Point(btnG.Location.X + iWidth, btnA.Location.Y);
            btnJ.Location = new Point(btnH.Location.X + iWidth, btnA.Location.Y);
            btnK.Location = new Point(btnJ.Location.X + iWidth, btnA.Location.Y);
            btnL.Location = new Point(btnK.Location.X + iWidth, btnA.Location.Y);
            //dòng 3
            btnZ.Location = new Point(iAlignWidth, btnA.Location.Y + iHeight);
            btnX.Location = new Point(btnZ.Location.X + iWidth, btnZ.Location.Y);
            btnC.Location = new Point(btnX.Location.X + iWidth, btnZ.Location.Y);
            btnV.Location = new Point(btnC.Location.X + iWidth, btnZ.Location.Y);
            btnB.Location = new Point(btnV.Location.X + iWidth, btnZ.Location.Y);
            btnN.Location = new Point(btnB.Location.X + iWidth, btnZ.Location.Y);
            btnM.Location = new Point(btnN.Location.X + iWidth, btnZ.Location.Y);
            btnShift.Location = new Point(btnM.Location.X + iWidth, btnZ.Location.Y);
            //dòng 4
            btnMoveLeftText.Location = new Point(iAlignWidth, btnZ.Location.Y + iHeight);
            btnMoveRigthText.Location = new Point(btnMoveLeftText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnDeleteText.Location = new Point(btnMoveRigthText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnOkText.Location = new Point(btnDeleteText.Location.X + iWidth * 3, btnMoveLeftText.Location.Y);
            btnPaymentText.Location = new Point(btnOkText.Location.X + iWidth * 2, btnMoveLeftText.Location.Y);
            //dòng 5
            btnTranslateText.Location = new Point(iAlignWidth, btnMoveLeftText.Location.Y + iHeight);
            btnEscText.Location = new Point(btnTranslateText.Location.X + iWidth * 2, btnTranslateText.Location.Y);
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtInput.Text.Length > 15)
            {
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void gvListForiegnCurrency_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gvListForiegnCurrency_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbKindMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbcurrencyService.Text = lbcurrencyGetMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbExchangeRate.Text = gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString();
        }

        private void btnUpForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle > 0)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle - 1;
            }
        }

        private void btnDownForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle < gvListForiegnCurrency.RowCount - 1)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle + 1;
            }
        }

        private void grvPhuthu_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT12 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        bool bPhuphi = false;
        private void grvPhuthu_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (bPhuphi)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Cảnh báo đã chọn phụ phí rồi.Bạn muốn tiếp tục tính thêm phụ phí ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            else
            {
                bPhuphi = true;
            }
            int iTongtien = 0;
            int iPhuthu = int.Parse(grvPhuthu.GetFocusedRowCellValue("GIATRI").ToString());
            foreach (DataRow dr in dtGridviewProduct.Rows)
            {
                dr["PHUTHU"] = int.Parse(dr["PHUTHU"].ToString()) + iPhuthu;
            }
            foreach (DataRow dr in dtGridviewProduct.Rows)
            {
                iTongtien = iTongtien + int.Parse(dr["PHUTHU"].ToString());
            }
            lbTheMoney.Tag = iTongtien;
            lbTheMoney.Text = string.Format("{0:#,###0}", iTongtien);
            txtInput = txtInputMoney;
            txtInput.Text = lbTotalMoneyAll.Text;
            txtInput.Focus();
            txtInput.SelectAll();
            cls_KP_RES.dtDanhSachVe = dtGridviewProduct;
        }

        private void btnUpPhuthu_Click(object sender, EventArgs e)
        {
            if (grvPhuthu.FocusedRowHandle > 0)
            {
                grvPhuthu.FocusedRowHandle = grvPhuthu.FocusedRowHandle - 1;
            }
        }

        private void btnDownPhuthu_Click(object sender, EventArgs e)
        {
            if (grvPhuthu.FocusedRowHandle < grvPhuthu.RowCount - 1)
            {
                grvPhuthu.FocusedRowHandle = grvPhuthu.FocusedRowHandle + 1;
            }
        }

        private void gvListProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnFirstGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = 0;
        }

        private void btnLastGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = gvListProduct.RowCount - 1;
        }

        private void btnUpGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle > 0)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle--;
            }
        }

        private void btnDownGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle < gvListProduct.RowCount - 1)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle++;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
            timer1.Enabled = true;
        }

        #region Text Click
        private void btnA_Click(object sender, EventArgs e)
        {
            InputKeyboardText("A");
        }

        private void btnQ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Q");
        }

        private void btnW_Click(object sender, EventArgs e)
        {
            InputKeyboardText("W");
        }

        private void btnE_Click(object sender, EventArgs e)
        {
            InputKeyboardText("E");
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            InputKeyboardText("R");
        }

        private void btnT_Click(object sender, EventArgs e)
        {
            InputKeyboardText("T");
        }

        private void btnY_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Y");
        }

        private void btnU_Click(object sender, EventArgs e)
        {
            InputKeyboardText("U");
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            InputKeyboardText("I");
        }

        private void btnO_Click(object sender, EventArgs e)
        {
            InputKeyboardText("O");
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            InputKeyboardText("P");
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            InputKeyboardText("S");
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            InputKeyboardText("D");
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            InputKeyboardText("F");
        }

        private void btnG_Click(object sender, EventArgs e)
        {
            InputKeyboardText("G");
        }

        private void btnH_Click(object sender, EventArgs e)
        {
            InputKeyboardText("H");
        }

        private void btnJ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("J");
        }

        private void btnK_Click(object sender, EventArgs e)
        {
            InputKeyboardText("K");
        }

        private void btnL_Click(object sender, EventArgs e)
        {
            InputKeyboardText("L");
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            InputKeyboardText("M");
        }

        private void btnX_Click(object sender, EventArgs e)
        {
            InputKeyboardText("X");
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            InputKeyboardText("C");
        }

        private void btnV_Click(object sender, EventArgs e)
        {
            InputKeyboardText("V");
        }

        private void btnB_Click(object sender, EventArgs e)
        {
            InputKeyboardText("B");
        }

        private void btnN_Click(object sender, EventArgs e)
        {
            InputKeyboardText("N");
        }

        private void btnZ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Z");
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            bShift = !bShift;
            if (bShift)
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            }
            else
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            }
        }

        private void btnTranslateText_Click(object sender, EventArgs e)
        {
            pnlKeyboardMoney.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnMoveRigthText_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnMoveLeftText_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnDeleteText_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnEscText_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnOkText_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnPaymentText_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();

        }

        private void MoveToLeft()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtInput.SelectionStart = 0;
            }
        }

        private void MoveToRight()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition < txtInput.Text.Length - 1)
            {
                if (txtInput.Text[iPosition + 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtInput.SelectionStart = txtInput.Text.Length;
            }
        }

        private void DeleteTextBox()
        {
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 2, 1);

                    txtInput.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtInput.Text.Length == 1)
                    {
                        txtInput.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
            else if (iPosition == 1)
            {
                if (iLenght >= 2)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                    if (txtInput.Text == "0")
                        txtInput.SelectionStart = 1;
                }
                else
                {
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                }
            }
        }

        private void OkTextBox()
        {
            if (txtInput.Name == "txtInputMoney")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                {
                    if (float.Parse(lbTotalMoneyAll.Text) <= 0)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbFirst.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbFirstMoney.Tag = MoneyInputAfterChange;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlFirst.Visible = true;
                    }
                    else
                        return;
                }
                else if (!pnlFirst.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbFirst.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbFirstMoney.Tag = MoneyInputAfterChange;
                    lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlFirst.Visible = true;
                }
                else
                    if (!pnlSecond.Visible)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbSecond.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbSecondMoney.Tag = MoneyInputAfterChange;
                        lbSecondMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlSecond.Visible = true;
                    }
                    else
                        if (!pnlThird.Visible)
                        {
                            decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                            lbThird.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                            lbThirdMoney.Tag = MoneyInputAfterChange;
                            lbThirdMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                            txtInput.Text = "0";
                            txtInput.SelectionStart = 1;
                            pnlThird.Visible = true;
                        }
                        else
                        {
                            XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ResetInputMoney();
                        }
            }
            else
                if (txtInput.Name == "txtInputDiscount")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) <= 100)
                    {
                        lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtInputDiscount.Text.Replace(",", ""))) / 100;
                        lbDiscountPayment.Text = txtInputDiscount.Text;
                        lbDiscount.Text = "        %";
                        per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                    }
                    else
                    {
                        if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) > decimal.Parse(lbTotalMoneyPayment.Tag.ToString()))
                        {
                            XtraMessageBox.Show("Lớn hơn số tiền khách trả !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtInput.Text = "0";
                            return;
                        }
                        lbDiscountPayment.Tag = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                        lbDiscountPayment.Text = string.Format("{0:#,###0}", decimal.Parse(lbDiscountPayment.Tag.ToString()));
                        lbDiscount.Text = "   VND";
                        per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", "")) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                    }

                    txtInput.Text = "0";
                    InputGetMoneyFocus();
                    txtInputMoney.Text = lbTotalMoneyAll.Text;
                }
                else if (txtInput.Name == "txtServiceMoney")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    lbServiceMoney.Tag = decimal.Parse(txtServiceMoney.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbServiceMoney.Text = string.Format("{0:#,###0}", decimal.Parse(lbServiceMoney.Tag.ToString()));
                    txtInput.Text = "0";
                    per_Extra = decimal.Parse(lbServiceMoney.Tag.ToString()) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                    InputGetMoneyFocus();
                }
                else if (txtInput.Name == "txtDearCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        lbIDDearCustomerShow.Text = lbMark.Text = lbNameDearCustomer.Text = "";
                        pnlDearCustomerShow.Visible = false;
                        return;
                    }
                    if (cls_KHTT.CheckKhachHangThanThiet(txtInput.Text))
                    {
                        lbIDDearCustomerShow.Text = txtInput.Text;
                        idKHTT = txtInput.Text;
                        DataTable dtCustomer = cls_KHTT.ReturnKhachHangThanThiet(lbIDDearCustomerShow.Text);
                        lbNameDearCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMark.Text = dtCustomer.Rows[0]["SoDiem"].ToString();
                        pnlDearCustomerShow.Visible = true;
                        InputGetMoneyFocus();
                        txtInputMoney.Text = lbTotalMoneyAll.Text;
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không đúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtMaDaiLy")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    if (cls_KHTT.CheckDaiLy(txtInput.Text))
                    {
                        lbIDDearCustomerShow.Text = txtInput.Text;

                        //idKHTT = txtInput.Text;
                        DataTable dtCustomer = cls_KHTT.ReturnDaiLy(lbIDDearCustomerShow.Text);
                        lbNameDearCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        dChietKhau = Convert.ToDecimal(dtCustomer.Rows[0]["CHIETKHAU"].ToString());
                        idDaiLy = dtCustomer.Rows[0]["MA"].ToString();
                        if (chkGIAMTRUCTIEP.Checked)
                        {
                            lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * dChietKhau) / 100;
                            lbDiscountPayment.Text = dChietKhau.ToString();
                            lbDiscount.Text = "        %";
                            per_Discount = dChietKhau;
                            flag_GiamTT_DaiLy = true;
                        }
                        else
                        {
                            flag_GiamTT_DaiLy = false;
                        }
                        lbMark.Text = "";
                        pnlDearCustomerShow.Visible = true;
                        pnlDaiLy.Visible = false;
                        InputGetMoneyFocus();
                        txtInputMoney.Text = lbTotalMoneyAll.Text;
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã Đại lý không đúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtInnerCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    if (cls_KHTT.CheckThanhToanNoiBo(txtInput.Text))
                    {

                        dMoneyInnerCard = cls_KHTT.ReturnMoneyMustPay(txtInput.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", "")));
                        if (dMoneyInnerCard == 0)
                        {
                            return;
                        }
                        if (lbIdInnerCustomerShow.Text != "")
                            return;
                        lbIdInnerCustomerShow.Text = txtInput.Text;
                        DataTable dtCustomer = clsMain.ReturnDataTable("select khttnb.SoDuTaiKhoan,ncc.TEN from khachhang_thanhtoan_noibo khttnb inner join NHACUNGCAP ncc on khttnb.Ma_KHTT=ncc.MA where MAKHTTNB='" + lbIdInnerCustomerShow.Text + "'");
                        lbNameInnerCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMoneySurplus.Text = string.Format("{0:#,###0}", decimal.Parse(dtCustomer.Rows[0]["SoDuTaiKhoan"].ToString())) + " VND";
                        pnlInnerCustomerShow.Visible = true;
                        if (!pnlFirst.Visible)
                        {

                            lbFirst.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                            lbFirstMoney.Tag = dMoneyInnerCard;
                            lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                            pnlFirst.Visible = true;
                        }
                        else
                            if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                                lbSecondMoney.Tag = dMoneyInnerCard;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                                pnlSecond.Visible = true;

                            }
                            else
                                if (!pnlThird.Visible)
                                {
                                    lbThird.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                                    lbThirdMoney.Tag = dMoneyInnerCard;
                                    lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                                    pnlThird.Visible = true;
                                }
                                else
                                {
                                    XtraMessageBox.Show("Tiền trả không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ResetInputMoney();
                                }
                        InputGetMoneyFocus();
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không dúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtInputVoucher")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 1)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 2)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher này đã sử dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 3)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher chưa được Active", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 4)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher này đã hết hạn sử dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 0)
                    {
                        string sSQL = "";
                        sSQL += "Select GIATRI_PHANTRAM,GIATRI_TIEN From VOUCHER_DETAIL Where MA =" + clsMain.SQLString(txtInput.Text);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (int.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString()) != 0)
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", "")) * decimal.Parse(lbTotalMoneyAll.Tag.ToString()) / 100;
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else
                                if (!pnlSecond.Visible)
                                {
                                    lbSecond.Text = "Voucher: Giảm " + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                    lbSecondMoney.Tag = dMoneyVoucher;
                                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                    txtInput.Text = "0";
                                    txtInput.SelectionStart = 1;
                                    pnlSecond.Visible = true;
                                }
                                else
                                    if (!pnlThird.Visible)
                                    {
                                        lbThird.Text = "Voucher: Giảm " + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                        lbThirdMoney.Tag = dMoneyVoucher;
                                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                        txtInput.Text = "0";
                                        txtInput.SelectionStart = 1;
                                        pnlThird.Visible = true;
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ResetInputMoney();
                                    }
                            //txtInput.Text = "0";
                            InputGetMoneyFocus();

                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                        else
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString().Replace(",", ""));
                            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else
                                if (!pnlSecond.Visible)
                                {
                                    lbSecond.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                    lbSecondMoney.Tag = dMoneyVoucher;
                                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                    txtInput.Text = "0";
                                    txtInput.SelectionStart = 1;
                                    pnlSecond.Visible = true;
                                }
                                else
                                    if (!pnlThird.Visible)
                                    {
                                        lbThirdMoney.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                        lbThirdMoney.Tag = dMoneyVoucher;
                                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                        txtInput.Text = "0";
                                        txtInput.SelectionStart = 1;
                                        pnlThird.Visible = true;
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ResetInputMoney();
                                    }
                            //txtInput.Text = "0";
                            InputGetMoneyFocus();

                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                    }
                }
        }

        private void EscapeTextbox()
        {
            if (txtInput.Name == "txtInputMoney")
            {
                txtInputMoney.Text = "0";
                txtInputMoney.Focus();
                txtInputMoney.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtInputDiscount")
            {
                txtInputDiscount.Text = "0";
                txtInputDiscount.Focus();
                txtInputDiscount.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtInputVoucher")
            {
                txtInputVoucher.Text = "0";
                txtInputVoucher.Focus();
                txtInputVoucher.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtDearCustomer")
            {
                txtDearCustomer.Text = "";
                txtDearCustomer.Focus();
                txtDearCustomer.SelectionStart = 0;
            }
            else if (txtInput.Name == "txtInnerCustomer")
            {
                txtInnerCustomer.Text = "";
                txtInnerCustomer.Focus();
                txtInnerCustomer.SelectionStart = 0;
            }
        }

        private void PrintBill()
        {
            if (pnlTienKhachTra.Visible)
            {
                if (!pnlFirst.Visible)
                {
                    OkTextBox();
                }
            }
            if (decimal.Parse(lbReplyCustomer.Tag.ToString()) < 0)
            {
                XtraMessageBox.Show("Chưa thanh toán hết tiền,yêu cầu nhập số tiền thanh toán", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - dMoneyVoucher;
                dMoneyVisa = 0;
                dMoneyInnerCard = 0;
            }
            else if ((dMoneyVoucher + dMoneyVisa) > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa);
                dMoneyInnerCard = 0;
            }
            else
            {
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa + dMoneyInnerCard);
            }
            if (lbIdInnerCustomerShow.Text != "")
            {
                cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
            }
            if (lbIDDearCustomerShow.Text != "")
            {
                cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
            }
            clsKP_Terminal.OpenCashDrawer(true);
            PrintAndSaveBill();
            this.Close();
        }

        private void PrintAndSaveBill()
        {
            string sSQL = "";
            string idBill = GetNewCodeBill();
            if (idDaiLy != "")
            {
                if (!flag_GiamTT_DaiLy)
                {
                    sSQL = "EXEC SP_Insert_HOAHONG_DAILY ";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(idDaiLy) + ",";
                    sSQL += clsMain.SQLString(lbTotalMoneyAll.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(dChietKhau.ToString()) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLBit(false);
                    clsMain.ExecuteSQL(sSQL);
                }
            }
            else if (idDaiLy == "")
            {
                sSQL = "";
                sSQL += "Select A.MA,A.TEN,B.CHIETKHAU" + "\n";
                sSQL += "from NHACUNGCAP A,CAPDODAILY B" + "\n";
                sSQL += "where A.CAPDO = B.MA" + "\n";
                sSQL += "and ID='NK00000000'";
                DataTable dtkh = clsMain.ReturnDataTable(sSQL);
                idDaiLy = dtkh.Rows[0]["MA"].ToString();
            }

            sSQL = "";
            sSQL += "Insert Into HOADONVE(MA_HOADON,MA_KHACHHANG,MANHANVIEN,NGAYTAO,TONGTIENTCK,TONGTIEN,GIAMGIA,TIEN_TRAKHACH, TIEN_KHACHTRA,";
            sSQL += "MA_CUAHANG,MA_KHO,MA_QUAY,SOTHETT,NGAYTHETT,TIENTRATHETT,MONEYVISA,MONEYINNERCARD,MONEYVOUCHER,MONEYCASH,ISPAYMENT)" + "\n";
            sSQL += "Values(";
            sSQL += clsMain.SQLString(idBill) + ",";
            if (CN_Flag)
                sSQL += clsMain.SQLString(CN_MaKH) + ",";
            else
                sSQL += clsMain.SQLString(idDaiLy) + ",";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
            sSQL += clsMain.SQLString(string.Format("{0:yyyyMMdd HH:mm:ss}", DateTime.Now)) + ",";
            sSQL += clsMain.SQLString(_itongtien.ToString()) + ",";
            sSQL += clsMain.SQLString(lbTotalMoneyAll.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(lbDiscountPayment.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(lbReplyCustomer.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(lbGetCustomer.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
            sSQL += clsMain.SQLString(soThe) + ",";
            sSQL += clsMain.SQLString(ngayThe) + ",";
            sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
            sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
            sSQL += clsMain.SQLString(dMoneyInnerCard.ToString()) + ",";
            sSQL += clsMain.SQLString(dMoneyVoucher.ToString()) + ",";
            sSQL += clsMain.SQLString(dMoneyCash.ToString()) + ",";
            sSQL += clsMain.SQLString("1") + ")";
            clsMain.ExecuteSQL(sSQL);
            
            foreach (DataRow dr in dtGridviewProduct.Rows)
            {
                sSQL = "";
                sSQL += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE,C.MALOAIGHE,C.TENLOAIGHE,A.MALOAIVE,(Select TENLOAIVE From DM_LOAIVE Where MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + ") As TENLOAIVE,A.DONGIA,D.MALICHCHIEU,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,E.MAPHONG,E.TENPHONG,F.MAPHIM,F.TENPHIM,D.MACHATLUONGPHIM,G.TENCHATLUONGPHIM,A.NGUOITAO As NGUOIBAN,H.TENNHANVIEN AS NGUOITAO,A.NGAYTAO" + "\n";
                sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I" + "\n";
                sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
                sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                sSQL += "And A.MAVE =" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                dt1.Rows[0]["MALOAIVE"] = dr["MALOAIVE"];
                dt1.Rows[0]["DONGIA"] = dr["PHUTHU"];
                //16/5/2015 bổ sung recheck trùng vé trước khi đưa vào vé tổng hợp
                sSQL = "";
                sSQL += "Select MAVETONGHOP " + "\n";
                sSQL += "From VETONGHOP " + "\n";
                sSQL += "Where MALICHCHIEU = " + clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + "\n";
                sSQL += "And SOGHE = " + clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + "\n";
                DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                if (dt2.Rows.Count % 2 != 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Vé đã xuất.Vui lòng chọn ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    continue;
                }
                //21/09/2015 bổ sung sửa lại lổi nhãy mã vé
                string sMabarcode_Final = cls_KP_RES.CreateBarcode_Final("VE");
                dt1.Rows[0]["MABARCODE"] = sMabarcode_Final;
                sSQL = "";
                sSQL += "Update VE Set " + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString(sMabarcode_Final) + "," + "\n";
                sSQL += "MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + "," + "\n";
                sSQL += "DONGIA=" + clsMain.SQLString(dr["PHUTHU"].ToString()) + "," + "\n";
                sSQL += "PHUTHU=" + clsMain.SQLString("0") + "," + "\n";
                sSQL += "DATHANHTOAN=" + clsMain.SQLString(dr["PHUTHU"].ToString()) + "," + "\n";
                sSQL += "CONLAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                //25/2/2014 bổ sung ghi vào sổ bán vé tổng hợp để báo cáo tiền cho chính xác
                sSQL += "Insert into VETONGHOP (MA_HOADON,LOAIVE,NGUOIBAN,NGAYBAN,MABARCODE,MALICHCHIEU,SOGHE,MAPHONG,MAPHIM,MALOAIGHE,MALOAIVE,DONGIA,MAQUAY,MA_KHACHHANG,NGUOITAO,NGAYTAO)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(idBill) + ",";
                sSQL += clsMain.SQLStringUnicode("Vé bán") + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["NGUOIBAN"].ToString()) + ",";
                sSQL += clsMain.SQLString(((DateTime)dt1.Rows[0]["NGAYTAO"]).ToString("dd/MM/yyyy HH : mm")) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MABARCODE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHONG"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHIM"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIVE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["DONGIA"].ToString()) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                sSQL += clsMain.SQLString(idKHTT) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                sSQL += "GETDATE()" + ")";

                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                if (bRunSQL)
                {
                    if (bPrintBill)
                    {
                        
                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_KP_RES.Mode == 2)
                        {
                            System.Threading.Thread.Sleep(1000);
                            frm.ReportName = "rptVerapphimIonah";
                        }
                        else if (cls_KP_RES.Mode == 11)
                        {
                            frm.ReportName = "rptVerapphim_Bill";
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(1000);
                            frm.ReportName = "rptVerapphim";
                        }
                        frm.DataSource = dt1;
                        frm.IsPrint = true;
                        frm.PrinterName = clsKP_Terminal.Mayinve;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.ExportName = "In ve";
                        frm.ShowDialog(this);
                        frm.Dispose();
                    }
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé tổng hợp không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            //Công nợ hóa đơn khách  hàng
            if (CN_Flag)
            {
                cls_KHTT.CongNoKhachHangHoaDon(CN_MaKH, idBill, decimal.Parse(lbTotalMoneyAll.Text), decimal.Parse(lbTotalMoneyAll.Text) - CN_SoTienTra, CN_ngayHenTra);
            }
            //Voucher
            if (idVoucher != "")
            {
                sSQL = "";
                sSQL += "Update VOUCHER_DETAIL" + "\n";
                sSQL += "Set STATUS=1,NGAY_SUDUNG=GETDATE(),HOADON=" + clsMain.SQLString(idBill) + "\n";
                sSQL += "Where MA=" + clsMain.SQLString(idVoucher);
                clsMain.ExecuteSQL(sSQL);
            }
            bInve = true;
        }

        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDonVe] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private void ResetInputMoney()
        {
            bVoucher = bVisaCard = true;
            lbThirdMoney.Tag = lbSecondMoney.Tag = lbFirstMoney.Tag = 0;
            lbFirst.Text = lbFirstMoney.Text = lbSecond.Text = lbSecondMoney.Text = lbThird.Text = lbThirdMoney.Text = string.Empty;
            pnlFirst.Visible = pnlSecond.Visible = pnlThird.Visible = false;
            lbNameInnerCustomer.Text = lbIdInnerCustomerShow.Text = lbMoneySurplus.Text = "";
            pnlInnerCustomerShow.Visible = false;
            txtInputMoney.Text = "0";
            txtInputMoney.SelectionStart = 1;
            txtDearCustomer.Text = txtInnerCustomer.Text = "";
            idVoucher = "";
            idKHTT = "";
            dMoneyInnerCard = 0;
            dMoneyVisa = 0;
            dMoneyVoucher = 0;
            dMoneyCash = 0;
        }

        private void InputGetMoneyFocus()
        {
            bText = false;
            txtInput = txtInputMoney;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = true;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
        }

        private void InputKeyboardText(string sInput)
        {
            if (bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (bShift)
                    sInput = sInput.ToUpper();
                else
                    sInput = sInput.ToLower();
                int iPosition = txtInput.SelectionStart;
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
                txtInput.SelectionStart = iPosition + 1;
            }

        }
        #endregion

        #region money click
        private void btn500_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500);
        }

        private void btn1000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(1000);
        }

        private void btn2000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(2000);
        }

        private void btn5000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(5000);
        }

        private void btn10000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(10000);
        }

        private void btn20000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(20000);
        }

        private void btn50000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(50000);
        }

        private void btn100000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(100000);
        }

        private void btn200000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(200000);
        }

        private void btn500000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500000);
        }

        private void btnTranslateMoney_Click(object sender, EventArgs e)
        {
            pnlKeyboardNumber.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnMoveLeftMoney_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightMoney_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteMoney_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkMoney_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEcsMoney_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnPaymentMoney_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
        }

        private void PlusInputMoney(int iMoney)
        {
            if (!bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (txtInput.SelectedText == txtInput.Text)
                {
                    txtInput.Text = "0";
                }
                decimal iTotal = decimal.Parse(txtInput.Text.Replace(",", "")) + iMoney;
                txtInput.Text = iTotal.ToString();
                txtInput.SelectionStart = txtInput.Text.Length;
            }
        }
        #endregion

        #region number click
        private void btn000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btnTranslateNumber_Click(object sender, EventArgs e)
        {
            pnlKeyBoardText.BringToFront();
            txtInput.SelectionStart = txtInput.Text.Length;
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnMoveLeftNumber_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteNumber_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkNumber_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEscNumber_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnPaymentNumber_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
        }

        private void InputKeyboardNumber(string sInput)
        {
            if (txtInput.Text.Length >= 15)
            {
                int iPositon = txtInput.SelectionStart;
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPositon;
                return;
            }

            if (txtInput.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtInput.Text.Length;
            iPosition = txtInput.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.Text == "0")
            {
                txtInput.Text = sInput;
            }
            else
            {
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtInput.Text.Length - iLenght == 1)
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
            }
            else if (sInput.Length == 2)
            {
                if (txtInput.Text.Length - iLenght == 2)
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 3;
                }
            }
            else if (sInput.Length == 3)
            {
                if (txtInput.Text.Length - iLenght == 3)
                {
                    txtInput.SelectionStart = iPosition + 3;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 4;
                }
            }
        }
        #endregion

        private void lbTotalMoneyAll_TextChanged(object sender, EventArgs e)
        {
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbTotalMoneyPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbDiscountPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyService_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyGetFirst_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void UpdateTotalMoneyPayment()
        {
            decimal fTotal = decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) + decimal.Parse(lbServiceMoney.Tag.ToString()) - decimal.Parse(lbDiscountPayment.Tag.ToString()) - decimal.Parse(lbMoneyGetFirst.Tag.ToString());
            lbTotalMoneyAll.Tag = fTotal;
            lbTotalMoneyAll.Text = string.Format("{0:#,###0}", fTotal);
        }

        private void lbFirstMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", lbFirstMoney.Tag);

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbSecondMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())));

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbThirdMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbTheMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbHourMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void UpdateTotalGridview()
        {
            decimal fTotal = decimal.Parse(lbTheMoney.Tag.ToString()) + decimal.Parse(lbHourMoney.Tag.ToString());
            lbTotalMoney.Tag = fTotal;
            lbTotalMoney.Text = string.Format("{0:#,###0}", fTotal);
        }

        private void lbTotalMoney_TextChanged(object sender, EventArgs e)
        {
            lbTotalMoneyPayment.Tag = lbTotalMoney.Tag;
            lbTotalMoneyPayment.Text = lbTotalMoney.Text;
        }

        private void txtServiceMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInnerCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtDearCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputVoucher_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtMaDaiLy_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void btnCancelPayment_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnRetypePayment_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            CN_Flag = false;
            txtInput = txtInputMoney;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = true;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlDaiLy.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
            ResetInputMoney();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            txtInput = txtInputDiscount;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = true;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlDaiLy.Visible = false;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void btnServiceMoney_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;

            txtInput = txtServiceMoney;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlDaiLy.Visible = false;
            pnlPhiPhuVu.Visible = true;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void btnNotPrintBill_Click(object sender, EventArgs e)
        {
            bPrintBill = false;
            PrintBill();
        }

        private void btnMakhuyenmai_Click(object sender, EventArgs e)
        {

        }

        private void btnVoucher_Click(object sender, EventArgs e)
        {
            if (bVoucher)
            {
                pnlShowInforPayment.BringToFront();
                bText = true;

                txtInput = txtInputVoucher;
                pnlVoucher.Visible = true;
                pnlChietKhau.Visible = false;
                pnlTienKhachTra.Visible = false;
                pnlKHTT.Visible = false;
                pnlTTTNB.Visible = false;
                pnlPhiPhuVu.Visible = false;
                pnlDaiLy.Visible = false;
                txtInput.Focus();
                txtInput.SelectionStart = 1;
            }
        }

        private void btnCardInternalPayment_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("8"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thanh toán nội bộ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            decimal dMustGetMoney = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", ""));
            if (dMustGetMoney <= 0)
                return;

            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtInnerCustomer;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = true;
            pnlPhiPhuVu.Visible = false;
            pnlDaiLy.Visible = false;
            txtInput.Text = lbIdInnerCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnCardImtimateCustomer_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("4"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thân thiết", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtDearCustomer;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = true;
            pnlTTTNB.Visible = false;
            pnlDaiLy.Visible = false;
            pnlPhiPhuVu.Visible = false;
            txtInput.Text = lbIDDearCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnDaiLy_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtMaDaiLy;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlDaiLy.Visible = true;
            pnlPhiPhuVu.Visible = false;
            chkGIAMTRUCTIEP.Left = txtMaDaiLy.Left + txtMaDaiLy.Width + 5;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnCongNo_Click(object sender, EventArgs e)
        {
            if (CN_Flag)
            {
                if (pnlFirst.Visible && lbFirst.Text.Contains("Công nợ:"))
                {
                    lbFirstMoney.Tag = 0;
                    lbFirstMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlFirst.Visible = false;
                }
                else if (pnlSecond.Visible && lbSecond.Text.Contains("Công nợ:"))
                {
                    lbSecondMoney.Tag = 0;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlSecond.Visible = false;
                }
                else if (pnlThird.Visible && lbThird.Text.Contains("Công nợ:"))
                {
                    lbThirdMoney.Tag = 0;
                    lbThirdMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlThird.Visible = false;
                }

                pnlCongNo.BringToFront();
                txtCNMaKH.Text = CN_MaKH;
                txtCongNo_Tien.Enabled = true;
                txtCongNo_Tien.Text = (decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text)).ToString();
                dtpNgayHenTra.EditValue = CN_ngayHenTra;
            }
            else
            {
                if (decimal.Parse(lbTotalMoneyAll.Text) <= decimal.Parse(lbGetCustomer.Text))
                {
                    XtraMessageBox.Show("Số tiền trả cho hóa đơn đã đủ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                pnlCongNo.BringToFront();
                ResetControlValue(pnlCongNo);
                txtCongNo_Tien.Enabled = false;
                dtpNgayHenTra.EditValue = DateTime.Now;
            }
            palCongNo_Noidung.Left = pnlCongNo.Width / 2 - palCongNo_Noidung.Width / 2;
        }

        private void btnCongNoSearch_Click(object sender, EventArgs e)
        {
            if (txtCNMaKH.Text != "" || txtCNSoDT.Text != "")
            {
                string sSQL = "";
                sSQL += "Select A.MA,A.ID,A.TEN,HANMUC_CONGNO,B.CHIETKHAU" + "\n";
                sSQL += "From NHACUNGCAP A,CAPDODAILY B" + "\n";
                sSQL += "Where A.CAPDO=B.MA AND (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
                sSQL += "AND (ID=" + clsMain.SQLString(txtCNMaKH.Text.Trim());
                sSQL += "OR DIENTHOAI=" + clsMain.SQLString(txtCNSoDT.Text.Trim()) + ")";
                DataTable dtCN = clsMain.ReturnDataTable(sSQL);
                if (dtCN.Rows.Count > 0)
                {
                    decimal chietkhau = decimal.Parse(lbTotalMoneyAll.Text) * decimal.Parse(dtCN.Rows[0]["CHIETKHAU"].ToString()) / 100;
                    txtCongNo_Tien.Enabled = true;
                    txtCongNo_Tien.Text = (decimal.Parse(lbTotalMoneyAll.Text) - chietkhau - decimal.Parse(lbGetCustomer.Text)).ToString();
                    lbIDDearCustomerShow.Text = txtCNMaKH.Text.Trim();
                    lbNameDearCustomer.Text = dtCN.Rows[0]["TEN"].ToString();
                    lbMark.Text = "";
                    pnlDearCustomerShow.Visible = true;

                    txtCongNo_Tien.Focus();
                    txtCongNo_Tien.SelectAll();
                }
                else
                    txtCongNo_Tien.Enabled = false;
            }
        }

        private void btnHuyCongNo_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            CN_Flag = false;
            ResetControlValue(pnlCongNo);
            pnlDearCustomerShow.Visible = false;
            InputGetMoneyFocus();
        }

        private void btnCongNoXacNhan_Click(object sender, EventArgs e)
        {
            try
            {
                if (DateTime.Compare(DateTime.Parse(DateTime.Now.ToShortDateString()), DateTime.Parse(DateTime.Parse(dtpNgayHenTra.EditValue.ToString()).ToShortDateString())) > 0)
                {
                    XtraMessageBox.Show("Ngày hẹn trả không được nhỏ hơn ngày hiện tại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (decimal.Parse(lbTotalMoneyAll.Text) <= decimal.Parse(lbGetCustomer.Text))
                {
                    XtraMessageBox.Show("Số tiền trả cho hóa đơn đã đủ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtCongNo_Tien.Text == "0")
                {
                    XtraMessageBox.Show("Chưa nhập số tiền", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string sSQL = "";
                sSQL += "Select A.MA,A.ID,A.TEN,ISNULL(HANMUC_CONGNO,0)AS HANMUC_CONGNO,ISNULL(TIEN_CONGNO,0) AS TIEN_CONGNO,";
                sSQL += "ISNULL(TIENDATCOC,0)AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC,B.CHIETKHAU" + "\n";
                sSQL += "From NHACUNGCAP A,CAPDODAILY B" + "\n";
                sSQL += "Where A.CAPDO=B.MA AND (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
                sSQL += "AND (A.ID=" + clsMain.SQLString(txtCNMaKH.Text.Trim());
                sSQL += "OR DIENTHOAI=" + clsMain.SQLString(txtCNSoDT.Text.Trim()) + ")";
                DataTable dtCN = clsMain.ReturnDataTable(sSQL);
                if (dtCN.Rows.Count > 0)
                {
                    if (!checkHanThanhToan(txtCNMaKH.Text.Trim(), txtCNSoDT.Text.Trim()))
                        return;


                    //xét điều kiện theo hạn mức công nợ
                    if (!bool.Parse(dtCN.Rows[0]["ISDATCOC"].ToString()))
                    {
                        if (float.Parse(dtCN.Rows[0]["HANMUC_CONGNO"].ToString()) >= (float.Parse(dtCN.Rows[0]["TIEN_CONGNO"].ToString()) + float.Parse(txtCongNo_Tien.Text)))
                        {
                            dChietKhau = Convert.ToDecimal(dtCN.Rows[0]["CHIETKHAU"].ToString());
                            lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * dChietKhau) / 100;
                            lbDiscountPayment.Text = dChietKhau.ToString();
                            lbDiscount.Text = "        %";
                            per_Discount = dChietKhau;
                            lbMark.Text = "";
                            pnlDearCustomerShow.Visible = true;
                            InputGetMoneyFocus();
                            txtInputMoney.Text = lbTotalMoneyAll.Text;

                            CN_Flag = true;
                            CN_MaKH = dtCN.Rows[0]["MA"].ToString();
                            if (decimal.Parse(txtCongNo_Tien.Text) > decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text))
                                CN_SoTienTra = decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text);
                            else
                                CN_SoTienTra = decimal.Parse(txtCongNo_Tien.Text);
                            CN_ngayHenTra = DateTime.Parse(dtpNgayHenTra.EditValue.ToString());
                            pnlShowInforPayment.BringToFront();

                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbFirstMoney.Tag = CN_SoTienTra;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlFirst.Visible = true;


                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbSecondMoney.Tag = CN_SoTienTra;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbThirdMoney.Tag = CN_SoTienTra;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show("Tiền trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                        }
                        else
                        {
                            XtraMessageBox.Show("Đã vượt hạn mức công nợ cho phép", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else  //xét điều kiện theo tiền đặt cộc
                    {
                        if (float.Parse(dtCN.Rows[0]["TIENDATCOC"].ToString()) >= (float.Parse(dtCN.Rows[0]["TIEN_CONGNO"].ToString()) + float.Parse(txtCongNo_Tien.Text)))
                        {
                            dChietKhau = Convert.ToDecimal(dtCN.Rows[0]["CHIETKHAU"].ToString());
                            lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * dChietKhau) / 100;
                            lbDiscountPayment.Text = dChietKhau.ToString();
                            lbDiscount.Text = "        %";
                            per_Discount = dChietKhau;
                            lbMark.Text = "";
                            pnlDearCustomerShow.Visible = true;
                            InputGetMoneyFocus();
                            txtInputMoney.Text = lbTotalMoneyAll.Text;

                            CN_Flag = true;
                            CN_MaKH = dtCN.Rows[0]["MA"].ToString();
                            if (decimal.Parse(txtCongNo_Tien.Text) > decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text))
                                CN_SoTienTra = decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text);
                            else
                                CN_SoTienTra = decimal.Parse(txtCongNo_Tien.Text);
                            CN_ngayHenTra = DateTime.Parse(dtpNgayHenTra.EditValue.ToString());
                            pnlShowInforPayment.BringToFront();

                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbFirstMoney.Tag = CN_SoTienTra;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbSecondMoney.Tag = CN_SoTienTra;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbThirdMoney.Tag = CN_SoTienTra;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show("Tiền trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                        }
                        else
                        {
                            XtraMessageBox.Show("Đã vượt quá số tiền đặt cọc", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("Không hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool checkHanThanhToan(string _id, string _sdt)
        {
            string sSQL = "";
            sSQL += "Select MA,ID,TEN,ISNULL(HANMUC_CONGNO,0)AS HANMUC_CONGNO,ISNULL(TIEN_CONGNO,0) AS TIEN_CONGNO,";
            sSQL += "ISNULL(TIENDATCOC,0)AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC,ISNULL(HANTHANHTOAN,0) as HANTHANHTOAN" + "\n";
            sSQL += "From NHACUNGCAP" + "\n";
            sSQL += "Where (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
            sSQL += "AND (ID=" + clsMain.SQLString(_id);
            sSQL += "OR DIENTHOAI=" + clsMain.SQLString(_sdt) + ")";
            DataTable dtCN = clsMain.ReturnDataTable(sSQL);
            if (dtCN.Rows.Count > 0)
            {
                sSQL = "";
                sSQL += "Select DISTINCT TOP(1) NGAYLAPPHIEU " + "\n";
                sSQL += "FROM CN_CONGNO A,CHITIETTHANHTOAN B" + "\n";
                sSQL += "WHERE A.MA_HOADON=B.MA_HOADON AND SOTIENCONLAI>0" + "\n";
                sSQL += "AND MADOITUONG=" + clsMain.SQLString(dtCN.Rows[0]["MA"].ToString()) + "\n";
                sSQL += "ORDER BY NGAYLAPPHIEU ASC";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                if (dt1.Rows.Count > 0)
                {
                    if ((DateTime.Now - DateTime.Parse(dt1.Rows[0]["NGAYLAPPHIEU"].ToString())).TotalDays + 1 > double.Parse(dtCN.Rows[0]["HANTHANHTOAN"].ToString()))
                    {
                        XtraMessageBox.Show("Khách hàng đã quá hạn thanh toán", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                XtraMessageBox.Show("Không hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public static void ResetControlValue(Control parent)
        {

            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.TextEdit":
                            ((TextEdit)c).Text = "";
                            break;
                        case "DevExpress.XtraEditors.DateEdit":
                            ((DateEdit)c).EditValue = clsMain.GetServerDate();
                            break;
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;
                        case "DevExpress.XtraEditors.LookupEdit":
                            ((LookUpEdit)(c)).EditValue = ((LookUpEdit)c).Properties.GetDataSourceValue(((LookUpEdit)c).Properties.ValueMember, 0);
                            break;
                        case "DevExpress.XtraEditors.GridLookUpEdit":
                            ((GridLookUpEdit)(c)).EditValue = ((GridLookUpEdit)c).Properties.GetKeyValue(0);
                            break;
                        default:
                            ResetControlValue(c);
                            break;
                    }
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        private void btnVisa_Click(object sender, EventArgs e)
        {
            txtIdCardVisa.GotFocus += new EventHandler(txtIdCardVisa_GotFocus);
            txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtNameVisa.GotFocus += new EventHandler(txtNameVisa_GotFocus);
            txtAddress.GotFocus += new EventHandler(txtAddress_GotFocus);
            txtCwCvc.GotFocus += new EventHandler(txtCwCvc_GotFocus);
            txtCity.GotFocus += new EventHandler(txtCity_GotFocus);
            txtIdCodeInput.GotFocus += new EventHandler(txtIdCodeInput_GotFocus);
            txtMonth.GotFocus += new EventHandler(txtMonth_GotFocus);
            txtYear.GotFocus += new EventHandler(txtYear_GotFocus);
            txtCountry.GotFocus += new EventHandler(txtCountry_GotFocus);
            if (bVisaCard)
            {
                pnlVisa.BringToFront();
                ResetControlValue(pnlVisa);
                if (txtIdCodeCreate.Text == "")
                {
                    txtIdCodeCreate.Text = CreateIdCode();
                }
            }
        }

        private void bntCancelVisa_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
        }

        private void btnRefeshIdCode_Click(object sender, EventArgs e)
        {

            txtIdCodeCreate.Text = CreateIdCode();
        }

        private bool CheckInputVisa()
        {
            if (txtIdCardVisa.Text == "")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Thẻ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIdCardVisa.Focus();
                return false;
            }
            if (txtMoneyVisa.Text == "0")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Tiền ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMoneyVisa.Focus();
                return false;
            }
            return true;

        }

        private void btnPaymentVisa_Click(object sender, EventArgs e)
        {
            if (!CheckInputVisa())
            {
                return;
            }
            if (txtIdCodeInput.Text != "")
            {
                if (txtIdCodeInput.Text != txtIdCodeCreate.Text)
                {
                    XtraMessageBox.Show("Mã An Toàn Không Đúng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIdCodeInput.Text = "";
                    txtIdCodeInput.Focus();
                    return;
                }
            }

            dMoneyVisa = decimal.Parse(txtMoneyVisa.Text.Replace(",", ""));

            if (!pnlFirst.Visible)
            {

                lbFirst.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                lbFirstMoney.Tag = dMoneyVisa;
                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                pnlFirst.Visible = true;
            }
            else
                if (!pnlSecond.Visible)
                {
                    lbSecond.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                    lbSecondMoney.Tag = dMoneyVisa;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                    pnlSecond.Visible = true;

                }
                else
                    if (!pnlThird.Visible)
                    {
                        lbThird.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                        lbThirdMoney.Tag = dMoneyVisa;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }
            soThe = txtIdCardVisa.Text;
            ngayThe = txtMonth.Text + " - " + txtYear.Text;

            InputGetMoneyFocus();
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
            bVisaCard = false;
        }

        private void txtIdCardVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtIdCardVisa;
        }

        private void txtMoneyVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMoneyVisa;
        }

        private void txtMonth_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMonth;
        }

        private void txtYear_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtYear;
        }

        private void txtNameVisa_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtNameVisa;
        }

        private void txtAddress_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtAddress;
        }

        private void txtCwCvc_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCwCvc;
        }

        private void txtCity_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCity;
        }

        private void txtIdCodeInput_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtIdCodeInput;
        }

        private void txtCountry_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCountry;
        }

        private string CreateIdCode()
        {
            string sRandom = "";
            int[] sRandomArr = new int[70];
            int iCount = 0;
            for (int i = 48; i <= 57; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 65; i <= 90; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 97; i <= 122; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            Random rd = new Random();
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);

            return sRandom;

        }
    }
}