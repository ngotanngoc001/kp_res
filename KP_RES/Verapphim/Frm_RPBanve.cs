﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Terminal;

namespace KP_RES
{
    public partial class Frm_RPBanve : DevExpress.XtraEditors.XtraForm
    {
        public Frm_RPBanve()
        {
            InitializeComponent();
            LoadPermission();
            LoadRap();
            LoadThoigianngungbanve();
            LoadNgay();
        }

        private void Frm_RPBanve_Load(object sender, EventArgs e)
        {
            lblQuay.Text = "Quầy : " + cls_ConfigCashier.nameCashier;
            lblCaban.Text = "Ca bán : " + cls_ConfigCashier.sTenCB;
            lblNhanvien.Text = "Nhân viên : " + clsGlobal.gsNameOfUserLogin;
            btnLen.Width = pnlLenxuong.Width / 2;
            btnChonSuatChieu_Click(null, null);
        }

        private void Frm_RPBanve_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnDong_Click(null, null);
            }
        }

        private void LoadPermission()
        {
            btnBanve.Enabled = clsUserManagement.AllowAdd("67");
            btnInvedattruoc.Enabled = clsUserManagement.AllowAdd("67");
            btnInlaive.Enabled = clsUserManagement.AllowEdit("67");
            btnHuyve.Enabled = clsUserManagement.AllowDelete("67");
        }

        int iThoigianngungbanve = 0;
        private void LoadThoigianngungbanve()
        {
            if (cls_KP_RES.iThoigianngungbanve == -1)
            {
                string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='THOIGIANNGUNGBANVE'";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in dt.Rows)
                {
                    iThoigianngungbanve = int.Parse(dr["GIATRI"].ToString());
                    cls_KP_RES.iThoigianngungbanve = iThoigianngungbanve;
                }
            }
            else
            {
                iThoigianngungbanve = cls_KP_RES.iThoigianngungbanve;
            }
        }

        string sMaRap = "";
        private void LoadRap()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            if (File.Exists(Application.StartupPath + "\\Rap.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\Rap.xml");
                    dt = ds.Tables[0];
                    sMaRap = dt.Rows[0]["MADIADIEM"].ToString();
                    lblTenRap.Text = dt.Rows[0]["TENDIADIEM"].ToString();
                }
                catch
                {
                }
            }
            if (sMaRap == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa cấu hình rạp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void LoadNgay()
        {
            while (fpnlNgay.Controls.Count > 0)
            {
                fpnlNgay.Controls[0].Dispose();
            }
            fpnlNgay.Controls.Clear();
            string sSQL = "";
            sSQL += "Select Distinct A.NGAYCHIEU FROM LICHCHIEU A,DM_PHONG B" + "\n";
            sSQL += " Where A.MAPHONG=B.MAPHONG " + "\n";
            sSQL += " And B.MARAP= " + clsMain.SQLString(sMaRap) + "\n";
            sSQL += " And A.NGAYCHIEU >= " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + " Order by A.NGAYCHIEU" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width > 1024)
            {
                pnlNgay.Height = (dt.Rows.Count > 10) ? 110 : 54;
            }
            else
            {
                pnlNgay.Height = (dt.Rows.Count > 7) ? 110 : 54;
            }
            foreach (DataRow dr in dt.Rows)
            {
                DateTime dtime = (DateTime)dr["NGAYCHIEU"];
                SimpleButton btn = new SimpleButton();
                btn.Width = 109;
                btn.Height = 50;
                btn.Margin = new System.Windows.Forms.Padding(0, 0, 6, 6);
                btn.Font = btnDong.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Name = dtime.Day.ToString("00");
                btn.Text = cls_KP_RES.GetThuofDateTime(dtime) + "\n" +cls_KP_RES.DateToString_Date(dtime) ;
                btn.Tag = cls_KP_RES.DateToString_Date(dtime);
                btn.Click += new EventHandler(Ngay_Click);
                fpnlNgay.Controls.Add(btn);
            }
        }

        string sNgay = "";
        private void Ngay_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            foreach (SimpleButton btn1 in fpnlNgay.Controls)
            {
                if (btn1.Name == btn.Name)
                {
                   cls_KP_RES . SetBackgroudOnClick(btn1);
                }
                else
                {
                    cls_KP_RES.SetDefautlBackgroud(btn1);
                }
            }
            sNgay = btn.Tag.ToString();
            LoadPhim();
        }

        string sMaLichchieu = "";
        int iIndexPhim = 0;
        DataTable dtPhim = new DataTable();
        private void LoadPhim()
        {
            while (fpnlPhim.Controls.Count > 0)
            {
                fpnlPhim.Controls[0].Dispose();
            }
            fpnlPhim.Controls.Clear();
            
            string sSQL = "Exec SP_LOADCHITIETPHIM " + clsMain.SQLString(sMaRap) + " , " + clsMain.SQLString(sNgay) + " , " + clsMain.SQLString("0");
            DataTable dtLichchieu_Full = clsMain.ReturnDataTable(sSQL);
            
            dtPhim = dtLichchieu_Full.DefaultView.ToTable(true, "MAPHIM", "TENPHIM");
            dtPhim.DefaultView.Sort = "TENPHIM ASC";
            dtPhim = dtPhim.DefaultView.ToTable();
            
            for(int i=iIndexPhim ; i<iIndexPhim +6;i++)
            {
                if (i > dtPhim.Rows.Count-1)
                {
                    return;
                }
                PanelControl pnl = new PanelControl();
                pnl.Width = 400;
                pnl.Height = 200;

                SimpleButton btn = new SimpleButton();
                btn.Width = 140;
                btn.Height = 196;
                btn.Location = new Point(2,2);
                btn.Font = btnDong.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = dtPhim.Rows [i]["MAPHIM"].ToString();
                btn.Text = dtPhim.Rows[i]["TENPHIM"].ToString();
                try
                {
                    byte[] tam = new byte[((byte[])dtLichchieu_Full.Rows[dtLichchieu_Full .Rows .IndexOf ( dtLichchieu_Full.Select("MAPHIM=" + dtPhim.Rows[i]["MAPHIM"].ToString())[0])]["HINHANH"]).LongLength];
                    tam = (byte[])dtLichchieu_Full.Rows[dtLichchieu_Full.Rows.IndexOf(dtLichchieu_Full.Select("MAPHIM=" + dtPhim.Rows[i]["MAPHIM"].ToString())[0])]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    Image poster = cls_KP_RES.resizeImage(bm, new Size(138, 160));
                    btn.Image = poster;
                }
                catch
                {
                    Image poster = cls_KP_RES.resizeImage(global::KP_RES.Properties.Resources.filmdefault, new Size(138, 160));
                    btn.Image = poster;
                }

                FlowLayoutPanel fpnl = new FlowLayoutPanel();
                fpnl.Width = 255;
                fpnl.Height = 196;
                fpnl.Location = new Point(142,2);
                fpnl.FlowDirection = FlowDirection.LeftToRight;
                fpnl.AutoScroll = true;
                dtLichchieu_Full.DefaultView.RowFilter = "MAPHIM=" + clsMain.SQLString(btn.Name);
                DataTable dtLichchieu = dtLichchieu_Full.DefaultView.ToTable();
                foreach (DataRow drLichchieu in dtLichchieu.Rows)
                {
                    SimpleButton btnLichchieu = new SimpleButton();
                    btnLichchieu.Width = 73;
                    btnLichchieu.Height = 43;
                    btnLichchieu.Font = btnDong.Font;
                    btnLichchieu.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnLichchieu.Name = drLichchieu["MALICHCHIEU"].ToString();
                    btnLichchieu.Text = drLichchieu["BATDAU"].ToString().Substring (0,5) ;
                    List<string> Thongtinxuatchieu = new List<string>();
                    Thongtinxuatchieu.Add(drLichchieu["TENPHIM"].ToString());
                    Thongtinxuatchieu.Add(drLichchieu["TENPHONG"].ToString());
                    Thongtinxuatchieu.Add(cls_KP_RES.GetThuofDateTime(((DateTime)drLichchieu["NGAYCHIEU"])) + " - " + (cls_KP_RES.DateToString_Date((DateTime)drLichchieu["NGAYCHIEU"])));
                    Thongtinxuatchieu.Add(drLichchieu["BATDAU"].ToString());
                    btnLichchieu.Tag = Thongtinxuatchieu;
                    DateTime dttime = DateTime.Parse(drLichchieu["BATDAU"].ToString());
                    if (((DateTime)drLichchieu["NGAYCHIEU"]).Day == DateTime.Today.Day && dttime.AddMinutes(iThoigianngungbanve) < DateTime.Now)
                    {
                        btnLichchieu.Visible = false;
                    }
                    btnLichchieu.VisibleChanged += new EventHandler(btnLichchieu_VisibleChanged);
                    btnLichchieu.Click += new EventHandler(Gio_Click);
                    fpnl.Controls.Add(btnLichchieu);
                }
                pnl.Controls.Add(fpnl);
                pnl.Controls.Add(btn);
                fpnlPhim.Controls.Add(pnl);
            }
        }

        private void btnLichchieu_VisibleChanged(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.Name == sMaLichchieu)
            {
                cls_KP_RES.SetBackgroudOnClick(btn);
            }
            else
            {
                cls_KP_RES.SetDefautlBackgroud(btn);
            }
        }

        private void Gio_Click(object sender, EventArgs e)
        {
            string sLichchieuOld = sMaLichchieu;
            SimpleButton btn = (SimpleButton)sender;
            sMaLichchieu = btn.Name;
            LoadThongtinxuatchieu(btn.Tag);
            LoadThoigian();
            LoadBanggia();
            if (sLichchieuOld == sMaLichchieu)
            {
                LoadSoDoGhe(false);
            }
            else
            {
                LoadSoDoGhe(true);
            }
            pnlShowTime.Visible = false;
            pnlSodoghe.Visible = true;
        }

        private void LoadThongtinxuatchieu(object obj)
        {
             List<string> Thongtinxuatchieu = (List<string> )obj;
             lblThongtinxuatchieu.Text = "Phim : " + Thongtinxuatchieu[0].ToString();
             lblThongtinxuatchieu1.Text = "Phòng : " + Thongtinxuatchieu[1].ToString() + "    " + Thongtinxuatchieu[2].ToString() + " - " + Thongtinxuatchieu[3].ToString().ToString().Substring(0, 5);
             cls_KP_RES.sTenRap = lblTenRap.Text;
             cls_KP_RES.sThongtinxuatchieu = lblThongtinxuatchieu.Text;
             cls_KP_RES.sThongtinxuatchieu1 = lblThongtinxuatchieu1.Text;
             cls_KP_RES.sTenPhong = Thongtinxuatchieu[1].ToString();
        }

        string MATHOIGIAN;
        private void LoadThoigian()
        {
            if (cls_KP_RES.sMaThoiGian == "")
            {
                string sSQL = "Select  MATHOIGIAN,TENTHOIGIAN,BATDAU,KETTHUC From DM_THOIGIAN" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                MATHOIGIAN = dt.Rows[0]["MATHOIGIAN"].ToString();
                cls_KP_RES.sMaThoiGian = MATHOIGIAN;
            }
            else
            {
                MATHOIGIAN = cls_KP_RES.sMaThoiGian;
            }
        }

        DataTable dtBanggia = new DataTable();
        private void LoadBanggia()
        {
            string sSQL = "Exec SP_LOADBANGGIA " + clsMain.SQLString(sMaLichchieu);
            dtBanggia = clsMain.ReturnDataTable(sSQL);

            DataTable dt = dtBanggia.DefaultView.ToTable(true, "MALOAIVE", "TENLOAIVE", "MACDINH");
            dt.DefaultView.Sort = "MACDINH DESC,TENLOAIVE ASC";
            dt = dt.DefaultView.ToTable();
            repositoryItemLookUpEdit1.DataSource = dt;


            DataTable dt1 = dt.Copy();
            cboLoaive.Properties.DataSource = dt1;

            cls_KP_RES.dtLoaiGhe = dtBanggia.DefaultView.ToTable(true, "MALOAIGHE", "TENLOAIGHE","MAU");
            cls_KP_RES.dtLoaiGhe.DefaultView.Sort = "TENLOAIGHE ASC";
            cls_KP_RES.dtLoaiGhe = cls_KP_RES.dtLoaiGhe.DefaultView.ToTable();
        }

        private void LoadSoDoGhe(bool bnew)
        {
            timerLoadSoDoGheTuDong.Enabled = false;
            string sSQL = "Exec SP_LOADSODOGHE " + clsMain.SQLString(sMaLichchieu);
            DataTable dtSodoghe = clsMain.ReturnDataTable(sSQL);
            if (bnew == true)
            {
                pnlSodo.Visible = false;
                this.Cursor = Cursors.WaitCursor;
                while (pnlSodo.Controls.Count > 0)
                {
                    pnlSodo.Controls[0].Dispose();
                }
                pnlSodo.Controls.Clear();

                foreach (DataRow dr in dtSodoghe.Rows)
                {
                    TextBox txt = new TextBox();
                    txt.Name = dr["MAVE"].ToString();
                    txt.Text = dr["TENGHE"].ToString();
                    txt.Location = new Point(int.Parse(dr["X"].ToString()), int.Parse(dr["Y"].ToString()));
                    txt.Width = int.Parse(dr["W"].ToString());
                    txt.Height = int.Parse(dr["H"].ToString());
                    txt.BackColor = cls_KP_RES.ConvertStringToColor(dr["MAUSAC"].ToString());
                    if (Boolean.Parse(dr["TRANGTHAI"].ToString()))
                    {
                        if (dr["CONLAI"].ToString() == "0")
                        {
                            txt.ForeColor = Color.White;
                            txt.BackColor = Color.DarkGray;
                            txt.Enabled = false;
                        }
                        else
                        {
                            if (dr["TENHANHKHACH"].ToString() == "")
                            {
                                txt.ForeColor = Color.Black;
                            }
                            else
                            {
                                if (dr["DATVE_THANHTOAN"].ToString() == "True")
                                {
                                    txt.ForeColor = Color.White;
                                    txt.BackColor = Color.DarkGray;
                                    txt.Enabled = false;
                                }
                                else
                                {
                                    txt.ForeColor = Color.Blue;
                                }
                            }
                        }
                    }
                    else
                    {
                        txt.ForeColor = Color.White;
                    }
                    txt.BorderStyle = BorderStyle.None;
                    txt.Cursor = Cursors.Hand;
                    txt.TextAlign = HorizontalAlignment.Center;
                    txt.Multiline = true;
                    txt.ReadOnly = true;
                   
                    txt.Click += new EventHandler(textBox_Click);

                    pnlSodo.Controls.Add(txt);
                }
                this.Cursor = Cursors.Default;
                pnlSodo.Visible = true;
            }
            else
            {
                for (int i = 0; i < dtSodoghe.Rows.Count; i++)
                {
                    if (Boolean.Parse(dtSodoghe.Rows[i]["TRANGTHAI"].ToString()))
                    {
                        if (dtSodoghe.Rows[i]["CONLAI"].ToString() == "0")
                        {
                            pnlSodo.Controls[i].ForeColor = Color.White;
                            pnlSodo.Controls[i].BackColor = Color.DarkGray;
                            pnlSodo.Controls[i].Enabled = false;
                        }
                        else
                        {
                            if (dtSodoghe.Rows[i]["TENHANHKHACH"].ToString() == "")
                            {
                                pnlSodo.Controls[i].ForeColor = Color.Black;
                            }
                            else
                            {
                                if (dtSodoghe.Rows[i]["DATVE_THANHTOAN"].ToString() == "True")
                                {
                                    pnlSodo.Controls[i].ForeColor = Color.White;
                                    pnlSodo.Controls[i].BackColor = Color.DarkGray;
                                    pnlSodo.Controls[i].Enabled = false;
                                }
                                else
                                {
                                    pnlSodo.Controls[i].ForeColor = Color.Blue;
                                }
                                pnlSodo.Controls[i].ForeColor = Color.Blue;
                            }
                        }
                    }
                    else
                    {
                        pnlSodo.Controls[i].ForeColor = Color.White;
                    }
                }
            }
            cls_KP_RES.bFormSodoghe = true;
            cls_KP_RES.dtSodoghe = dtSodoghe;
            cls_KP_RES.dtDanhSachVe = gridView.DataSource == null ? null : ((DataView)gridView.DataSource).ToTable();
            timerLoadSoDoGheTuDong.Enabled = true;
        }

        ArrayList danhsachve = new ArrayList();
        private void textBox_Click(object sender, EventArgs e)
        {
            Control txt = this.ActiveControl;
            if (txt.Text.Length > 1 && txt.Text.Length < 4)
            {
                if ((txt.Parent == pnlSodo) && (txt.ForeColor == Color.White)) //Chọn ghế
                {
                    string MaBarCode = cls_KP_RES.CreateBarcode("VE");
                    string sSQL = "Exec SP_ChonGhe " + clsMain.SQLString(int.Parse(txt.Name).ToString()) + " , " + clsMain.SQLString(clsGlobal.gsUserID) + " , " + clsMain.SQLString(txt.Text.ToString()) + " , " + clsMain.SQLString(MaBarCode) + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows[0][0].ToString() == "0")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Ghế đã bán . Chọn lại ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadSoDoGhe(false);
                        return;
                    }
                    else
                    {
                        danhsachve.Add(txt.Name);
                        LoadSoDoGhe(false);
                    }
                }
                else if ((txt.Parent == pnlSodo) && (txt.ForeColor == Color.Black)) //Trả lại ghế đã chọn
                {
                    //05/04/2017 thêm cho biết ghế do ai chọn
                    string sSQL = "Exec SP_BoChonGhe_New " + clsMain.SQLString(int.Parse(txt.Name).ToString()) + " , " + clsMain.SQLString(clsGlobal.gsUserID) + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows[0][0].ToString() != "1")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Ghế đã được chọn bởi : " + dt.Rows[0][0].ToString() + " . Bạn không bỏ chọn ghế này được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        danhsachve.Remove(txt.Name);
                        LoadSoDoGhe(false);
                    }
                }
            }
            LoadDanhSachVe();
        }

        private void LoadDanhSachVe()
        {
            DataTable dt = new DataTable();
            gridControl.DataSource = dt;
            if (danhsachve.Count <= 0)
            {
                txtTongtien.Text = "0";
                txtTongghe.Text = "0";
                return;
            }
            int temp = 0;
            string Maves = "";
            foreach (Object obj in danhsachve)
            {
                if (temp == 0)
                {
                    Maves += "And ( A.MAVE= " + clsMain.SQLString(obj.ToString()) + " ";
                    temp++;
                }
                else
                {
                    Maves += "Or A.MAVE= " + clsMain.SQLString(obj.ToString()) + " ";
                }
            }
            Maves += ")" + "\n";
            string sSQL = "Exec SP_LoadDanhSachVe " + clsMain.SQLString(Maves) + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            gridControl.DataSource = dt;

            //Bổ sung tự chọn loại vé
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool bRunGia = false;
                    for (int j = 0; j < dtDanhsachve_Suatkhac.Rows.Count; j++)
                    {
                        if (dt.Rows[i]["MAVE"].ToString() == dtDanhsachve_Suatkhac.Rows[j]["MAVE"].ToString())
                        {
                            gridView.SetRowCellValue(i, "PHUTHU", dtDanhsachve_Suatkhac.Rows[j]["PHUTHU"].ToString());
                            gridView.SetRowCellValue(i, "MALOAIVE", dtDanhsachve_Suatkhac.Rows[j]["MALOAIVE"].ToString());
                            bRunGia = true;
                            break;
                        }
                    }
                    if (!bRunGia)
                    {
                        gridView.SetRowCellValue(i, "MALOAIVE", repositoryItemLookUpEdit1.GetDataSourceValue("MALOAIVE", 0).ToString());
                        string sFilter = "";
                        sFilter += "MALOAIVE=" + clsMain.SQLString(repositoryItemLookUpEdit1.GetDataSourceValue("MALOAIVE", 0).ToString()) + "\n";
                        sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(i, "MALOAIGHE").ToString()) + "\n";
                        sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
                        dtBanggia.DefaultView.RowFilter = sFilter;
                        DataTable dt1 = dtBanggia.DefaultView.ToTable();
                        if (dt1.Rows.Count > 0)
                        {
                            gridView.SetRowCellValue(i, "PHUTHU", dt1.Rows[0]["DONGIA"].ToString());
                        }
                    }
                    gridView.UpdateTotalSummary();
                    txtTongtien.Text = PHUTHU.SummaryText;
                    txtTongghe.Text = MADIADIEM.SummaryText;
                }
            }
            catch
            {
            }
            //--------------
        }

        private void repositoryItemLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
            DataRowView row = editor.Properties.GetDataSourceRowByKeyValue(editor.EditValue) as DataRowView;

            string sFilter = "";
            sFilter += "MALOAIVE=" + clsMain.SQLString(row["MALOAIVE"].ToString()) + "\n";
            sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MALOAIGHE").ToString()) + "\n";
            sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
            dtBanggia.DefaultView.RowFilter = sFilter;
            DataTable dt = dtBanggia.DefaultView.ToTable();
            if (dt.Rows.Count > 0)
            {
                gridView.SetRowCellValue(gridView.FocusedRowHandle, "PHUTHU", dt.Rows[0]["DONGIA"].ToString());
            }
            gridView.UpdateTotalSummary();
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
        }

        private void repositoryItemLookUpEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            for (int j = 0; j < dtDanhsachve_Suatkhac.Rows.Count; j++)
            {
                if (gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAVE").ToString() == dtDanhsachve_Suatkhac.Rows[j]["MAVE"].ToString())
                {
                    e.Cancel = true;
                    break;
                }
            }
        }

        private void cboLoaive_EditValueChanged(object sender, EventArgs e)
        {
            //
            string sMALOAIVE = cboLoaive.EditValue.ToString();
            for (int i = 0; i < gridView.RowCount; i++)
            {
                try
                {
                    string sFilter = "";
                    sFilter += "MALOAIVE=" + clsMain.SQLString(sMALOAIVE) + "\n";
                    sFilter += "And MALOAIGHE=" + clsMain.SQLString(gridView.GetRowCellValue(i, "MALOAIGHE").ToString()) + "\n";
                    sFilter += "And MATHOIGIAN=" + clsMain.SQLString(MATHOIGIAN) + "\n";
                    dtBanggia.DefaultView.RowFilter = sFilter;
                    DataTable dt = dtBanggia.DefaultView.ToTable();
                    if (dt.Rows.Count > 0)
                    {
                        gridView.SetRowCellValue(i, "PHUTHU", dt.Rows[0]["DONGIA"].ToString());
                        gridView.SetRowCellValue(i, "MALOAIVE", sMALOAIVE);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa có bảng giá" + "\n" + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            gridView.UpdateTotalSummary();
            txtTongtien.Text = PHUTHU.SummaryText;
            txtTongghe.Text = MADIADIEM.SummaryText;
            //
        }

        DataTable dtDanhsachve_Suatkhac = new DataTable();
        private void btnChonSuatChieu_Click(object sender, EventArgs e)
        {
            cls_KP_RES.bFormSodoghe = false;
            if (gridView.RowCount > 0)
            {
                dtDanhsachve_Suatkhac = ((DataView)gridView.DataSource).ToTable();
            }
            try
            {
                if (fpnlPhim.Controls.Count <= 0)
                {
                    Ngay_Click(fpnlNgay.Controls[0], null);
                }
            }
            catch
            {
            }
            pnlShowTime.Visible = true;
            pnlSodoghe.Visible = false;
        }

        private void btnLen_Click(object sender, EventArgs e)
        {
            if (iIndexPhim > 6)
            {
                iIndexPhim = iIndexPhim - 6;
            }
            else
            {
                iIndexPhim = 0;
            }
            LoadPhim();
        }

        private void btnXuong_Click(object sender, EventArgs e)
        {
            if (iIndexPhim < dtPhim.Rows.Count && fpnlPhim.Controls.Count == 6)
            {
                iIndexPhim = iIndexPhim + 6;
            }
            LoadPhim();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            if (danhsachve.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn bỏ chọn ghế và thoát ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            btnChonlaighe_Click(null, null);
            cls_KP_RES.bFormQuangCao = false;
            this.Close();
        }

        private void btnMoket_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
            frm.ShowDialog();
            check = frm.kt;
            frm.Dispose();
            if (!check)
            {
                return;
            }
            clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetca_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
            frm1.ShowDialog();
            check = frm1.kt;
            frm1.Dispose();
            if (!check)
            {
                return;
            }
            PF_Ketca frm = new PF_Ketca();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void btnChonlaighe_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            foreach (string mv in danhsachve)
            {
                sSQL += "Update VE Set " + "\n";
                sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(mv) + "\n";
            }
            if (sSQL != "")
            {
                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                this.Cursor = Cursors.Default;
                if (bRunSQL)
                {
                    danhsachve.Clear();
                    dtDanhsachve_Suatkhac.Clear();
                    LoadSoDoGhe(false);
                    LoadDanhSachVe();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bỏ ghế không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnBanve_Click(object sender, EventArgs e)
        {
            if (gridView.RowCount <= 0)
            {
                return;
            }
            DataView dv = (DataView)gridView.DataSource;
            DataTable dt = dv.ToTable();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["MALOAIVE"].ToString() == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn loại vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            bool bInve = false;
            Payment_Rapphim frm = new Payment_Rapphim(int.Parse(txtTongtien.Text.Replace(",", "")), dt);
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog(this);
            bInve = frm.bInve;
            frm.Dispose();

            if (bInve)
            {
                danhsachve.Clear();
                dtDanhsachve_Suatkhac.Clear();
                LoadSoDoGhe(false);
                dt.Clear();
                gridControl.DataSource = dt;
                txtTongtien.Text = "0";
                txtTongghe.Text = "0";
            }
        }

        private void btnInvedattruoc_Click(object sender, EventArgs e)
        {
            ArrayList danhsachvedat = new ArrayList();
            PF_Invedattruoc frm = new PF_Invedattruoc();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.sMalichchieu = sMaLichchieu;
            frm.ShowDialog(this);
            danhsachvedat = frm.danhsachvedat;
            frm.Dispose();
            foreach (string obj in danhsachvedat )
            {
                danhsachve.Add(obj.ToString ());
            }
            if (pnlSodo.Controls.Count > 0 && pnlSodoghe.Visible == true)
            {
                LoadSoDoGhe(false);
            }
            LoadDanhSachVe();
        }

        private void btnInlaive_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
            frm1.ShowDialog();
            check = frm1.kt;
            frm1.Dispose();
            if (!check)
            {
                return;
            }
            string MaLICHCHIEU = sMaLichchieu;
            if (MaLICHCHIEU != "")
            {
                PF_Inlaive frm = new PF_Inlaive();
                frm.Width = this.Width;
                frm.Height = this.Height;
                frm.MaLICHCHIEU = MaLICHCHIEU;
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }

        private void btnHuyve_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
            frm1.ShowDialog();
            check = frm1.kt;
            frm1.Dispose();
            if (!check)
            {
                return;
            }
            PF_Huyve frm = new PF_Huyve();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog(this);
            frm.Dispose();
            if (pnlSodo.Controls.Count > 0 && pnlSodoghe.Visible == true)
            {
                LoadSoDoGhe(false);
            }
        }

        private void btnChonnhieughe_Click(object sender, EventArgs e)
        {
            if (pnlSodo.Controls.Count <= 0 || pnlSodoghe.Visible == false)
            {
                return;
            }
            else
            {
                try
                {
                    int Soghe;
                    string Tenghebatdau;

                    PF_Chonnhieughe frm = new PF_Chonnhieughe();
                    frm.ShowDialog();
                    Soghe = frm.iSoghe;
                    Tenghebatdau = frm.sGhebatdau;
                    frm.Dispose();

                    if (Soghe == 0 || Tenghebatdau == "")
                    {
                        return;
                    }
                    string sSQL = "";
                    sSQL += "Select TOP " + Soghe.ToString() + "B.TENGHE  ,A.MAVE From VE A, DM_GHE B" + "\n";
                    sSQL += "Where a.MAGHE =b.MAGHE " + "\n";
                    sSQL += "And a.MALICHCHIEU =" + clsMain.SQLString(sMaLichchieu) + "\n";
                    sSQL += "And b.TENGHE >=" + clsMain.SQLString(Tenghebatdau) + "\n";
                    sSQL += "And LEN(B.TENGHE )>1" + "\n";
                    sSQL += "And LEN(B.TENGHE )<4" + "\n";
                    sSQL += "And A.TRANGTHAI =0" + "\n";
                    sSQL += "Order by b.TENGHE asc" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sSQL);

                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (TextBox txt in pnlSodo.Controls)
                        {
                            if (txt.Text == dr["TENGHE"].ToString() && txt.Enabled == true)
                            {
                                txt.Focus();
                                textBox_Click(null, null);
                                break;
                            }
                        }
                        btnBanve.Focus();
                    }
                }
                catch
                {
                }
            }
        }

        private void btnLenve_Click(object sender, EventArgs e)
        {
            try
            {
                gridView.Focus();
                gridView.FocusedRowHandle = gridView.FocusedRowHandle - 1;
            }
            catch
            {
            }
        }

        private void btnXuongve_Click(object sender, EventArgs e)
        {
            try
            {
                gridView.Focus();
                gridView.FocusedRowHandle = gridView.FocusedRowHandle + 1;
            }
            catch
            {
            }
        }

        private void btnXoave_Click(object sender, EventArgs e)
        {
            string mave = "";
            if (gridView.RowCount > 0)
            {
                mave = gridView.GetFocusedRowCellValue("MAVE").ToString();
            }
            if (mave != "")
            {
                string sSQL = "";
                sSQL += "Update VE Set " + "\n";
                sSQL += "SOGHE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGUOITAO=" + clsMain.SQLString("") + "," + "\n";
                sSQL += "NGAYTAO= GETDATE()" + "," + "\n";
                sSQL += "TRANGTHAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(int.Parse(mave).ToString()) + "\n";

                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                this.Cursor = Cursors.Default;
                if (bRunSQL)
                {
                    danhsachve.Remove(mave);
                    LoadSoDoGhe(false);
                    LoadDanhSachVe();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Bỏ ghế không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        int iTimenotuse = 0;
        int iXmouse = 0;
        private void timerLoadSoDoGheTuDong_Tick(object sender, EventArgs e)
        {
            if (pnlSodo.Controls.Count > 0 && pnlSodoghe.Visible==true && cls_KP_RES.bThanhtoan ==false )
            {
                timerLoadSoDoGheTuDong.Enabled = false;
                LoadSoDoGhe(false);

                //05/04/2017 thêm vô để giảm tải cho server
                int iXmouseNew = System.Windows.Forms.Control.MousePosition.X;
                if (iXmouseNew != iXmouse)
                {
                    iTimenotuse = 0;
                    iXmouse = iXmouseNew;
                }
                if (iTimenotuse == 100)
                {
                    btnChonlaighe_Click(null, null);
                    btnChonSuatChieu_Click(null, null);
                }
                else
                {
                    iTimenotuse++;
                }
                //

                timerLoadSoDoGheTuDong.Enabled = true;
            }
            
        }

        private void timerDongho_Tick(object sender, EventArgs e)
        {
            timerDongho.Enabled = false;
            lbNgay.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lbGio.Text = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00");
            lbgiay.Text = DateTime.Now.Second.ToString("00");
            timerDongho.Enabled = true;
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            cls_KP_RES.dtDanhSachVe = ((DataView)gridView.DataSource).ToTable();
        }

        private void gridView_RowCountChanged(object sender, EventArgs e)
        {
            cls_KP_RES.dtDanhSachVe = ((DataView)gridView.DataSource).ToTable();
        }
    }
}