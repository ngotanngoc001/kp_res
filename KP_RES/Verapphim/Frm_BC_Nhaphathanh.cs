﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Columns;
using System.Collections;

namespace KP_RES 
{
    public partial class Frm_BC_Nhaphathanh : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt=new DataTable() ;
        public Frm_BC_Nhaphathanh()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_BC_Nhaphathanh_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
            panelControl7.Visible = false;
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        DataTable dtCombo = new DataTable();
        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            //cboDATA.Properties.Columns.Clear();
            //cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//Rạp
            {
                dtCombo = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1 Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Rạp";
            }
            else if (optGroup.SelectedIndex == 1)//Phòng
            {
                dtCombo = clsMain.ReturnDataTable("select MAPHONG as MA,TENPHONG as TEN from DM_PHONG where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Phòng";
            }
            else if (optGroup.SelectedIndex == 2)//Nhà phát hành
            {
                dtCombo = clsMain.ReturnDataTable("select MANHAPHATHANH as MA,TENNHAPHATHANH as TEN from DM_NHAPHATHANH where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhà phát hành";
            }
            else if (optGroup.SelectedIndex == 3)//Phim
            {
                dtCombo = clsMain.ReturnDataTable("select MAPHIM as MA,TENPHIM as TEN from DM_PHIM where SUDUNG=1  Order by TEN");
                //cboDATA.Properties.DataSource = dt;
                //dt.Rows.Add("0", "Tất cả");
                ////taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Phim";
                LoadChiTietCombo(dtCombo);
            }
            else if (optGroup.SelectedIndex == 4)//Loại ghế
            {
                dtCombo = clsMain.ReturnDataTable("select MALOAIGHE AS MA,TENLOAIGHE AS TEN from DM_LOAIGHE where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Loại ghế";
            }
            else if (optGroup.SelectedIndex == 5)//Loại vé
            {
                dtCombo = clsMain.ReturnDataTable("select MALOAIVE AS MA,TENLOAIVE AS TEN from DM_LOAIVE where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhà cung cấp" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Loại vé";
            }
            else if (optGroup.SelectedIndex == 6)//Nhân viên
            {
                dtCombo = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1  Order by TEN");
                cboDATA.Properties.DataSource = dtCombo;
                dtCombo.Rows.Add("0", "Tất cả");
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dtCombo);
                lblData.Text = "Nhân viên";
            }
         
            //cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void LoadChiTietCombo(DataTable dt)
        {
            cboDATA.Properties.Items.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                cboDATA.Properties.Items.Add(dr["TEN"].ToString());
            }
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            //if (Caption.Length > 1 & dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < Caption.Length; i++)
            //    {
            //        LookUpColumnInfo info = new LookUpColumnInfo();
            //        info.FieldName = FieldName[i].TrimEnd();
            //        info.Caption = Caption[i].TrimEnd();
            //        info.Visible = visible[i];
            //        info.Alignment = DevExpress.Utils.HorzAlignment.Default;
            //        cboDATA.Properties.Columns.Add(info);
            //    }
            //    cboDATA.Properties.BestFitMode = BestFitMode.None;
            //    cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
            //    cboDATA.Properties.DisplayMember = FieldName[1];
            //    cboDATA.Properties.ValueMember = FieldName[0];
            //    if (visible[0] == true)
            //    {
            //        cboDATA.Properties.Columns[0].Width = 100;
            //        cboDATA.Properties.Columns[1].Width = 500;
            //    }
            //    else
            //    {
            //        cboDATA.Properties.Columns[1].Width = 100;
            //        cboDATA.Properties.Columns[2].Width = 500;
            //    }
            //    if (dt.Rows.Count > 0)
            //        cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            //}
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            splashScreenManager1.ShowWaitForm();
            dt = new DataTable();
            string sql = string.Empty;
            if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value || cboDATA.EditValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn đối tượng cần xem", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //Tao bang du lieu
            DataColumn MALICHCHIEU = new DataColumn();
            MALICHCHIEU.ColumnName = "MALICHCHIEU";
            MALICHCHIEU.DataType = System.Type.GetType("System.String");
            MALICHCHIEU.DefaultValue = "0";

            DataColumn NGAY = new DataColumn();
            NGAY.ColumnName = "NGAY";
            NGAY.DataType = System.Type.GetType("System.String");
            NGAY.DefaultValue = "0";

            DataColumn PHIM = new DataColumn();
            PHIM.ColumnName = "PHIM";
            PHIM.DataType = System.Type.GetType("System.String");
            PHIM.DefaultValue = "0";

            DataColumn XUATCHIEU = new DataColumn();
            XUATCHIEU.ColumnName = "XUATCHIEU";
            XUATCHIEU.DataType = System.Type.GetType("System.String");
            XUATCHIEU.DefaultValue = "0";

            DataColumn PHONG = new DataColumn();
            PHONG.ColumnName = "PHONG";
            PHONG.DataType = System.Type.GetType("System.String");
            PHONG.DefaultValue = "0";

            DataColumn TONGVE = new DataColumn();
            TONGVE.ColumnName = "TONGVE";
            TONGVE.DataType = System.Type.GetType("System.String");
            TONGVE.DefaultValue = "0";

            DataColumn VEMIENPHI = new DataColumn();
            VEMIENPHI.ColumnName = "0";
            VEMIENPHI.DataType = System.Type.GetType("System.String");
            VEMIENPHI.DefaultValue = "0";

            DataColumn TONGTIEN = new DataColumn();
            TONGTIEN.ColumnName = "TONGTIEN";
            TONGTIEN.DataType = System.Type.GetType("System.Int64");
            TONGTIEN.DefaultValue = "0";

            dt.Columns.Add(MALICHCHIEU);
            dt.Columns.Add(NGAY);
            dt.Columns.Add(PHIM);
            dt.Columns.Add(XUATCHIEU);
            dt.Columns.Add(PHONG);
            dt.Columns.Add(TONGVE);
            dt.Columns.Add(VEMIENPHI);
           
            sql ="";
            sql +="select distinct A.DONGIA  from VETONGHOP A,LICHCHIEU B  "+ "\n";
            sql +="where A.MALICHCHIEU=B.MALICHCHIEU AND B.NGAYCHIEU BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";
            string sqlTemp = "";
            foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                    if (dr.Length > 0)
                    {
                        if (sqlTemp == "")
                        {
                            sqlTemp += " A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                        }
                        else
                        {
                            sqlTemp += " Or A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                        }
                    }
                }
            }
            sql += "And ( " + sqlTemp + ")" + "\n";
            sql += "Order by A.DONGIA" + "\n";
            DataTable dttemp1 = clsMain.ReturnDataTable(sql );
            if (dttemp1.Rows.Count == 0)
            {
                dttemp1 = clsMain.ReturnDataTable("select distinct DONGIA  from CHITIETBANGGIA order by DONGIA");
            }

            foreach (DataRow dr in dttemp1.Rows)
            {
                if (dr["DONGIA"].ToString()!="0")
                {
                    DataColumn DONGIA = new DataColumn();
                    DONGIA.ColumnName = dr["DONGIA"].ToString();
                    DONGIA.DataType = System.Type.GetType("System.String");
                    DONGIA.DefaultValue = "0";
                    dt.Columns.Add(DONGIA);
                }
            }

            dt.Columns.Add(TONGTIEN);
            //xong tao bang du lieu
            //add dong
            sql = "";
            sql += "select A.MALICHCHIEU,A.MAPHIM ,B.TENPHIM,A.NGAYCHIEU, A.BATDAU,C.TENPHONG AS PHONG" + "\n";
            sql += "from LICHCHIEU A,DM_PHIM B,DM_PHONG C" + "\n";
            sql += "where A.SUDUNG=1 " + "\n";
            sql += "and A.MAPHONG  =C.MAPHONG " + "\n";
            sql += "and A.MAPHIM  =B.MAPHIM " + "\n";
            sql += "and A.NGAYCHIEU BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + "\n";
            //if (cboDATA.EditValue.ToString() != "0")
            //{
            //    sql += "And A.MAPHIM= " + clsMain.SQLString(cboDATA.EditValue.ToString()) + "\n";
            //}
            sqlTemp = "";
            foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    DataRow[] dr = dtCombo.Select("TEN=" + clsMain.SQLString(item.Value.ToString()));
                    if (dr.Length > 0)
                    {
                        if (sqlTemp == "")
                        {
                            sqlTemp += " A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                        }
                        else
                        {
                            sqlTemp += " Or A.MAPHIM= " + clsMain.SQLString(dr[0]["MA"].ToString());
                        }
                    }
                }
            }
            sql += "And ( " + sqlTemp + ")" + "\n";
            sql += "Order by NGAYCHIEU,TENPHIM,BATDAU" + "\n";

            dttemp1 = clsMain.ReturnDataTable(sql);
            foreach (DataRow dr in dttemp1.Rows)
            {
                dt.Rows.Add(dr["MALICHCHIEU"].ToString(), String.Format("{0:dd/MM/yyyy}", dr["NGAYCHIEU"]), dr["TENPHIM"].ToString(), dr["BATDAU"].ToString(), dr["PHONG"].ToString());
            }
            //xong add dong
            //add so lieu
            sql = "";
            sql += "select MAVETONGHOP,MABARCODE,MALICHCHIEU,MAPHONG,MAPHIM,LOAIVE,SOGHE,DONGIA" + "\n";
            sql += "from VETONGHOP " + "\n";
            sql += "where MALICHCHIEU IN (select MALICHCHIEU from LICHCHIEU where NGAYCHIEU BETWEEN " + clsMain.SQLString(dtpTungay.Text.ToString() + " 00 : 00 : 00") + " And " + clsMain.SQLString(dtpDenngay.Text.ToString() + " 23 : 59 : 59") + " )" + "\n";
            DataTable dtVefull = clsMain.ReturnDataTable(sql);
            
            dtVefull .DefaultView.RowFilter="LOAIVE='Vé bán'";
            DataTable dtVeban = dtVefull .DefaultView.ToTable()   ;
           
            dtVefull .DefaultView.RowFilter="LOAIVE='Vé hủy'";
            DataTable dtVehuy = dtVefull .DefaultView.ToTable()   ;

            foreach (DataRow dr in dtVehuy.Rows)
            {
                DataRow [] drs=dtVeban.Select("MALICHCHIEU=" + clsMain.SQLString(dr["MALICHCHIEU"].ToString()) + " And MABARCODE=" + clsMain.SQLString(dr["MABARCODE"].ToString()) + " And SOGHE=" + clsMain.SQLString(dr["SOGHE"].ToString()) + " And DONGIA=" + clsMain.SQLString(dr["DONGIA"].ToString()));
                if (drs.Length > 0)
                {
                    dtVeban.Rows.Remove(drs[0]);
                }
            }

            dtVefull = dtVeban;

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn  dc in dt.Columns)
                {
                    if (dc.ColumnName == "MALICHCHIEU" || dc.ColumnName == "NGAY" || dc.ColumnName == "PHIM" || dc.ColumnName == "XUATCHIEU" || dc.ColumnName == "PHONG")
                    {
                        continue;
                    }
                    else if (dc.ColumnName == "TONGVE")
                    {
                        dr[dc]=dtVefull.Select("MALICHCHIEU= " + clsMain.SQLString(dr["MALICHCHIEU"].ToString())).Length;
                    }
                    else if (dc.ColumnName == "TONGTIEN")
                    {
                        int iTongtienTemp = 0;
                        foreach (DataRow drTemp in dtVefull.Select("MALICHCHIEU= " + clsMain.SQLString(dr["MALICHCHIEU"].ToString())))
                        {
                            iTongtienTemp = iTongtienTemp + int.Parse (drTemp["DONGIA"].ToString ());
                        }
                        dr[dc] = iTongtienTemp;
                    }
                    else
                    {
                        dr[dc] = dtVefull.Select("MALICHCHIEU= " + clsMain.SQLString(dr["MALICHCHIEU"].ToString()) + "AND DONGIA=" + clsMain.SQLString(dc.ColumnName)).Length;
                    }
                }
            }
            //xong add so lieu
            gridView1.Columns.Clear();
            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName != "MALICHCHIEU")
                {
                    GridColumn col = new GridColumn();
                    col.OptionsColumn.AllowEdit = false;
                    col.Visible = true;
                    col.FieldName = dc.ColumnName;
                    gridView1.Columns.Add(col);
                    col.VisibleIndex = dt.Columns.IndexOf(dc.ColumnName);
                    col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    col.AppearanceHeader.TextOptions.WordWrap= DevExpress.Utils.WordWrap.Wrap;
                    col.AppearanceCell.Font = btnTimkiem.Appearance.Font;
                    if (dc.ColumnName == "NGAY")
                    {
                        dc.Caption = "Ngày";
                        col.Caption = "Ngày";
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    }
                    else if (dc.ColumnName == "PHIM")
                    {
                        dc.Caption = "Phim";
                        col.Caption = "Phim";
                    }
                    else if (dc.ColumnName == "XUATCHIEU")
                    {
                        dc.Caption = "Xuất chiếu";
                        col.Caption = "Xuất chiếu";
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
                        col.SummaryItem.DisplayFormat = "{0:n0}";
                    }
                    else if (dc.ColumnName == "PHONG")
                    {
                        dc.Caption = "Phòng";
                        col.Caption = "Phòng";
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    }
                    else if (dc.ColumnName == "TONGVE")
                    {
                        dc.Caption = "Tổng số vé";
                        col.Caption = "Tổng số vé";
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        col.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                        col.SummaryItem.DisplayFormat = "{0:n0}";
                        
                    }
                    else if (dc.ColumnName == "TONGTIEN")
                    {
                        dc.Caption = "Thành tiền";
                        col.Caption = "Thành tiền";
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        col.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                        col.DisplayFormat.FormatString = "N0";
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                        col.SummaryItem.DisplayFormat = "{0:n0}";
                    }
                    else
                    {
                        int itemp = int.Parse(dc.ColumnName);
                        if (itemp == 0)
                        {
                            dc.Caption = "Giá vé \n" + itemp.ToString("N0")+" (Miễn phí)";
                            col.Caption = "Giá vé \n" + itemp.ToString("N0") + " (Miễn phí)";
                        }
                        else
                        {
                            dc.Caption = "Giá vé \n" + itemp.ToString("N0");
                            col.Caption = "Giá vé \n" + itemp.ToString("N0");
                        }
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        col.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                        col.SummaryItem.DisplayFormat = "{0:n0}";
                    }
                   
                }
            }
            gridControl1.DataSource = dt;
            gridView1.Columns["PHIM"].GroupIndex = 0;
            gridView1.ExpandAllGroups();
            splashScreenManager1.CloseWaitForm();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;

                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    DataColumn colTYPE = new DataColumn();
                    colTYPE.ColumnName = "TYPE";
                    colTYPE.DataType = System.Type.GetType("System.String");
                    if (optGroup.SelectedIndex == 0)//Rạp
                    {
                        colTYPE.DefaultValue = "Rạp";

                    }
                    else if (optGroup.SelectedIndex == 1)//Phòng
                    {
                        colTYPE.DefaultValue = "Phòng";
                    }
                    else if (optGroup.SelectedIndex == 2)//Nhà phát hành
                    {
                        colTYPE.DefaultValue = "Nhà phát hành";
                    }
                    else if (optGroup.SelectedIndex == 3)//Phim
                    {
                        colTYPE.DefaultValue = "Phim";
                    }
                    else if (optGroup.SelectedIndex == 4)//Loại ghế
                    {
                        colTYPE.DefaultValue = "Loại ghế";
                    }
                    else if (optGroup.SelectedIndex == 5)//Loại vé
                    {
                        colTYPE.DefaultValue = "Loại vé";
                    }
                    else if (optGroup.SelectedIndex == 6)//Nhân viên
                    {
                        colTYPE.DefaultValue = "Nhân viên";
                    }

                    DataColumn colNAMETYPE = new DataColumn();
                    colNAMETYPE.ColumnName = "NAMETYPE";
                    colNAMETYPE.DataType = System.Type.GetType("System.String");
                    //colNAMETYPE.DefaultValue = cboDATA.Text;
                    bool bAll = true;
                    foreach (CheckedListBoxItem item in cboDATA.Properties.Items)
                    {
                        if (item.CheckState == CheckState.Unchecked)
                        {
                            bAll = false;
                            break;
                        }
                    }
                    if (bAll)
                    {
                        colNAMETYPE.DefaultValue  = "Tất cả";
                    }
                    else
                    {
                        colNAMETYPE.DefaultValue = cboDATA.Text;
                    }

                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    if (ContainColumn("TYPE", dt) == false)
                        dt.Columns.Add(colTYPE);
                    if (ContainColumn("NAMETYPE", dt) == false)
                        dt.Columns.Add(colNAMETYPE);
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ColumnName == "NGAY")
                        {
                            dc.Namespace = "Tổng cộng";
                        }
                        else if (dc.ColumnName == "XUATCHIEU")
                        {
                            dc.Namespace = gridView1.Columns[dc.ColumnName].SummaryText;
                        }
                        else if (dc.ColumnName == "TONGVE")
                        {
                            dc.Namespace = gridView1.Columns[dc.ColumnName].SummaryText ;
                        }
                        else if (dc.ColumnName == "TONGTIEN")
                        {
                            dc.Namespace = gridView1.Columns[dc.ColumnName].SummaryText;
                        }
                        else
                        {
                            try
                            {
                                int.Parse(dc.ColumnName);
                                dc.Namespace = gridView1.Columns[dc.ColumnName].SummaryText;
                            }
                            catch
                            {
                            }
                        }
                     
                    }
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt.Copy ();
                    frm.Mode = 40;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl7.Visible = false;
                panelControl6.Visible = false;
                panelControl3.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl7.Visible = false;
                panelControl6.Visible = true;
                panelControl3.Visible = true;
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}