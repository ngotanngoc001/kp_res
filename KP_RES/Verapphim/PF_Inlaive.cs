﻿using System;
using System.Data;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;

namespace KP_RES
{
    public partial class PF_Inlaive : DevExpress.XtraEditors.XtraForm
    {
        public string  MaLICHCHIEU;

        public PF_Inlaive()
        {
            InitializeComponent();
        }

        private void PF_Inlaive_Shown(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE, A.TRANGTHAI,A.DONGIA,A.PHUTHU,A.DATHANHTOAN,A.CONLAI,A.NGUOITAO,A.NGAYTAO,B.TENGHE , B.X , B.Y , B.W , B.H , B.MAUNEN ,B.MALOAIGHE,C.TENLOAIGHE,C.MAU,D.NGAYCHIEU ,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.KETTHUC,E.TENPHONG,F.TENPHIM,G.TENNHANVIEN,I.TENLOAIVE,J.TENDIADIEM" + "\n";
            sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_NHANVIEN G,SYS_USER H,DM_LOAIVE I,DM_DIADIEM J" + "\n";
            sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
            sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
            sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
            sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
            sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
            sSQL += "And A.NGUOITAO = H.UserID" + "\n";
            sSQL += "And H.MaNV = G.MANHANVIEN" + "\n";
            sSQL += "And A.MALOAIVE = I.MALOAIVE" + "\n";
            sSQL += "And E.MARAP = J.MADIADIEM" + "\n";
            sSQL += "And A.TRANGTHAI =" + clsMain.SQLString("1") + "\n";
            sSQL += "And A.CONLAI =" + clsMain.SQLString("0") + "\n";
            sSQL += "And A.MALICHCHIEU=" + clsMain.SQLString(MaLICHCHIEU) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = dt;

        }

        private void PF_Inlaive_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnDong_Click(null, null);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == INVE)
            {
                int iInlaive = 1;
                DataTable dtTemp = clsMain.ReturnDataTable("Select Count( MAVE) As I_MAVE From INLAIVE Where MAVE=" + clsMain.SQLString(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MAVE").ToString()));
                iInlaive = iInlaive + int.Parse(dtTemp.Rows[0][0].ToString());
                clsMain.ExecuteSQL("Insert  INLAIVE (MAVE,LANIN,NHANVIEN) Values (" + clsMain.SQLString(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MAVE").ToString())+","+clsMain .SQLString (iInlaive .ToString ())+","+clsMain .SQLString (clsUserManagement .ReturnUserLogin ()) + ")");

                string sSQL = "";
                sSQL = "";
                sSQL += "Select A.MABARCODE,A.SOGHE,A.DONGIA,A.NGUOITAO As NGUOIBAN,A.NGAYTAO,A.MALOAIVE,V.TENLOAIVE,C.TENLOAIGHE,C.MALOAIGHE,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,D.MACHATLUONGPHIM,D.MALICHCHIEU,E.TENPHONG,E.MAPHONG,F.TENPHIM,F.MAPHIM,G.TENCHATLUONGPHIM,H.TENNHANVIEN AS NGUOITAO ," + iInlaive.ToString() + " As LANIN" + "\n";
                sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I,DM_LOAIVE V" + "\n";
                sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sSQL += "And A.MALOAIVE = V.MALOAIVE" + "\n";
                sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
                sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                sSQL += "And A.MAVE =" + clsMain.SQLString(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MAVE").ToString()) + "\n";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);

                
                Frm_Report1 frm = new Frm_Report1();
                if (cls_KP_RES.Mode == 2)
                {
                    System.Threading.Thread.Sleep(1000);
                    frm.ReportName = "rptVerapphimIonah";
                }
                else if (cls_KP_RES.Mode == 11)
                {
                    frm.ReportName = "rptVerapphim_Bill_Inlai";
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    frm.ReportName = "rptVerapphim_Inlai";
                }
                frm.DataSource = dt1;
                frm.IsPrint = true;
                frm.WindowState = FormWindowState.Minimized;
                frm.PrinterName = clsKP_Terminal.Mayinve;
                frm.ExportName = "In ve";
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }
    }
}