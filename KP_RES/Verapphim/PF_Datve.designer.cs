﻿namespace KP_RES
{
    partial class PF_Datve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panel12 = new System.Windows.Forms.Panel();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADIADIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTongtien = new DevExpress.XtraEditors.TextEdit();
            this.txtTongghe = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cboKhachhang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtThanhtien = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtChietkhau = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTonggiave = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton22 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.txtTenkhach = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.txtSodienthoai = new DevExpress.XtraEditors.TextEdit();
            this.lblSodienthoai = new DevExpress.XtraEditors.LabelControl();
            this.lblTenkhach = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pnlReport = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnBanve1 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhachhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChietkhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonggiave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenkhach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSodienthoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).BeginInit();
            this.btnBanve1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(8, 196);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(6);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(93, 69);
            this.btnLuu.TabIndex = 14;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(8, 356);
            this.btnDong.Margin = new System.Windows.Forms.Padding(6);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(93, 69);
            this.btnDong.TabIndex = 16;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl6);
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(592, 721);
            this.panelControl2.TabIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panel12);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 434);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(588, 285);
            this.panelControl6.TabIndex = 16;
            // 
            // panel12
            // 
            this.panel12.AutoScroll = true;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.gridControl);
            this.panel12.Controls.Add(this.panel1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(2, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(584, 281);
            this.panel12.TabIndex = 15;
            // 
            // gridControl
            // 
            this.gridControl.AllowDrop = true;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1});
            this.gridControl.Size = new System.Drawing.Size(582, 248);
            this.gridControl.TabIndex = 45;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MADIADIEM,
            this.PHUTHU,
            this.MAKHUVUC,
            this.FILL});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.RowHeight = 40;
            this.gridView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Width = 50;
            // 
            // MADIADIEM
            // 
            this.MADIADIEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceCell.Options.UseFont = true;
            this.MADIADIEM.AppearanceCell.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MADIADIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MADIADIEM.AppearanceHeader.Options.UseFont = true;
            this.MADIADIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.MADIADIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MADIADIEM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MADIADIEM.Caption = "Ghế";
            this.MADIADIEM.FieldName = "SOGHE";
            this.MADIADIEM.Name = "MADIADIEM";
            this.MADIADIEM.OptionsColumn.AllowFocus = false;
            this.MADIADIEM.OptionsColumn.AllowMove = false;
            this.MADIADIEM.OptionsColumn.AllowSize = false;
            this.MADIADIEM.OptionsColumn.FixedWidth = true;
            this.MADIADIEM.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.MADIADIEM.Visible = true;
            this.MADIADIEM.VisibleIndex = 0;
            this.MADIADIEM.Width = 50;
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceCell.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PHUTHU.Caption = "Giá vé";
            this.PHUTHU.DisplayFormat.FormatString = "N0";
            this.PHUTHU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.OptionsColumn.AllowMove = false;
            this.PHUTHU.OptionsColumn.AllowSize = false;
            this.PHUTHU.OptionsColumn.FixedWidth = true;
            this.PHUTHU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.PHUTHU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PHUTHU", "{0:#,###0}")});
            this.PHUTHU.Visible = true;
            this.PHUTHU.VisibleIndex = 1;
            this.PHUTHU.Width = 80;
            // 
            // MAKHUVUC
            // 
            this.MAKHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHUVUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHUVUC.Caption = "Sử dụng";
            this.MAKHUVUC.FieldName = "MAKHUVUC";
            this.MAKHUVUC.Name = "MAKHUVUC";
            this.MAKHUVUC.OptionsColumn.AllowEdit = false;
            this.MAKHUVUC.OptionsColumn.AllowFocus = false;
            this.MAKHUVUC.OptionsColumn.AllowMove = false;
            this.MAKHUVUC.OptionsColumn.AllowSize = false;
            this.MAKHUVUC.OptionsColumn.FixedWidth = true;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.OptionsColumn.AllowSize = false;
            this.FILL.Width = 20;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", 30, "Loại vé")});
            this.repositoryItemLookUpEdit1.DisplayMember = "TENLOAIVE";
            this.repositoryItemLookUpEdit1.DropDownItemHeight = 40;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "Chọn loại vé";
            this.repositoryItemLookUpEdit1.ValueMember = "MALOAIVE";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtTongtien);
            this.panel1.Controls.Add(this.txtTongghe);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 248);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(582, 31);
            this.panel1.TabIndex = 71;
            // 
            // txtTongtien
            // 
            this.txtTongtien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTongtien.EditValue = "0";
            this.txtTongtien.EnterMoveNextControl = true;
            this.txtTongtien.Location = new System.Drawing.Point(40, 0);
            this.txtTongtien.Name = "txtTongtien";
            this.txtTongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongtien.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtTongtien.Properties.Appearance.Options.UseFont = true;
            this.txtTongtien.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongtien.Properties.ReadOnly = true;
            this.txtTongtien.Size = new System.Drawing.Size(542, 30);
            this.txtTongtien.TabIndex = 58;
            // 
            // txtTongghe
            // 
            this.txtTongghe.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtTongghe.EditValue = "0";
            this.txtTongghe.EnterMoveNextControl = true;
            this.txtTongghe.Location = new System.Drawing.Point(0, 0);
            this.txtTongghe.Name = "txtTongghe";
            this.txtTongghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongghe.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTongghe.Properties.Appearance.Options.UseFont = true;
            this.txtTongghe.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongghe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongghe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTongghe.Properties.LookAndFeel.SkinName = "Office 2007 Blue";
            this.txtTongghe.Size = new System.Drawing.Size(40, 30);
            this.txtTongghe.TabIndex = 57;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cboKhachhang);
            this.panelControl1.Controls.Add(this.txtThanhtien);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtChietkhau);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtTonggiave);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.simpleButton26);
            this.panelControl1.Controls.Add(this.simpleButton27);
            this.panelControl1.Controls.Add(this.simpleButton21);
            this.panelControl1.Controls.Add(this.simpleButton22);
            this.panelControl1.Controls.Add(this.simpleButton23);
            this.panelControl1.Controls.Add(this.simpleButton24);
            this.panelControl1.Controls.Add(this.simpleButton25);
            this.panelControl1.Controls.Add(this.simpleButton16);
            this.panelControl1.Controls.Add(this.simpleButton17);
            this.panelControl1.Controls.Add(this.simpleButton18);
            this.panelControl1.Controls.Add(this.simpleButton19);
            this.panelControl1.Controls.Add(this.simpleButton20);
            this.panelControl1.Controls.Add(this.simpleButton11);
            this.panelControl1.Controls.Add(this.simpleButton12);
            this.panelControl1.Controls.Add(this.simpleButton13);
            this.panelControl1.Controls.Add(this.simpleButton14);
            this.panelControl1.Controls.Add(this.simpleButton15);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.simpleButton6);
            this.panelControl1.Controls.Add(this.simpleButton7);
            this.panelControl1.Controls.Add(this.simpleButton8);
            this.panelControl1.Controls.Add(this.simpleButton9);
            this.panelControl1.Controls.Add(this.simpleButton10);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btn5);
            this.panelControl1.Controls.Add(this.btn6);
            this.panelControl1.Controls.Add(this.btn7);
            this.panelControl1.Controls.Add(this.btn8);
            this.panelControl1.Controls.Add(this.btn9);
            this.panelControl1.Controls.Add(this.btn0);
            this.panelControl1.Controls.Add(this.btn1);
            this.panelControl1.Controls.Add(this.btn2);
            this.panelControl1.Controls.Add(this.btn3);
            this.panelControl1.Controls.Add(this.btn4);
            this.panelControl1.Controls.Add(this.txtTenkhach);
            this.panelControl1.Controls.Add(this.btnDong);
            this.panelControl1.Controls.Add(this.txtEmail);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.btnBanphim);
            this.panelControl1.Controls.Add(this.txtSodienthoai);
            this.panelControl1.Controls.Add(this.lblSodienthoai);
            this.panelControl1.Controls.Add(this.lblTenkhach);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(588, 432);
            this.panelControl1.TabIndex = 0;
            // 
            // cboKhachhang
            // 
            this.cboKhachhang.EnterMoveNextControl = true;
            this.cboKhachhang.Location = new System.Drawing.Point(108, 69);
            this.cboKhachhang.Name = "cboKhachhang";
            this.cboKhachhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhachhang.Properties.Appearance.Options.UseFont = true;
            this.cboKhachhang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhachhang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKhachhang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKhachhang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKhachhang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Khách hàng")});
            this.cboKhachhang.Properties.DisplayMember = "TEN";
            this.cboKhachhang.Properties.DropDownItemHeight = 40;
            this.cboKhachhang.Properties.NullText = "";
            this.cboKhachhang.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKhachhang.Properties.ShowHeader = false;
            this.cboKhachhang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKhachhang.Properties.ValueMember = "MA";
            this.cboKhachhang.Size = new System.Drawing.Size(472, 26);
            this.cboKhachhang.TabIndex = 7;
            this.cboKhachhang.EditValueChanged += new System.EventHandler(this.cboKhachhang_EditValueChanged);
            // 
            // txtThanhtien
            // 
            this.txtThanhtien.EnterMoveNextControl = true;
            this.txtThanhtien.Location = new System.Drawing.Point(108, 165);
            this.txtThanhtien.Name = "txtThanhtien";
            this.txtThanhtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhtien.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtThanhtien.Properties.Appearance.Options.UseFont = true;
            this.txtThanhtien.Properties.Appearance.Options.UseForeColor = true;
            this.txtThanhtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThanhtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThanhtien.Properties.ReadOnly = true;
            this.txtThanhtien.Size = new System.Drawing.Size(472, 26);
            this.txtThanhtien.TabIndex = 13;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(7, 168);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(76, 19);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Thành tiền";
            // 
            // txtChietkhau
            // 
            this.txtChietkhau.EnterMoveNextControl = true;
            this.txtChietkhau.Location = new System.Drawing.Point(108, 133);
            this.txtChietkhau.Name = "txtChietkhau";
            this.txtChietkhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChietkhau.Properties.Appearance.Options.UseFont = true;
            this.txtChietkhau.Properties.Appearance.Options.UseTextOptions = true;
            this.txtChietkhau.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtChietkhau.Properties.Mask.EditMask = "N0";
            this.txtChietkhau.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChietkhau.Size = new System.Drawing.Size(472, 26);
            this.txtChietkhau.TabIndex = 11;
            this.txtChietkhau.EditValueChanged += new System.EventHandler(this.txtChietkhau_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(7, 136);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(75, 19);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Chiết khấu";
            // 
            // txtTonggiave
            // 
            this.txtTonggiave.EnterMoveNextControl = true;
            this.txtTonggiave.Location = new System.Drawing.Point(108, 101);
            this.txtTonggiave.Name = "txtTonggiave";
            this.txtTonggiave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTonggiave.Properties.Appearance.Options.UseFont = true;
            this.txtTonggiave.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTonggiave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTonggiave.Properties.ReadOnly = true;
            this.txtTonggiave.Size = new System.Drawing.Size(472, 26);
            this.txtTonggiave.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(7, 104);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 19);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Tổng giá vé";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(7, 72);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(82, 19);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Khách hàng";
            // 
            // simpleButton26
            // 
            this.simpleButton26.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton26.Appearance.Options.UseFont = true;
            this.simpleButton26.Location = new System.Drawing.Point(137, 339);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(40, 40);
            this.simpleButton26.TabIndex = 98;
            this.simpleButton26.Text = "Z";
            this.simpleButton26.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton27
            // 
            this.simpleButton27.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton27.Appearance.Options.UseFont = true;
            this.simpleButton27.Location = new System.Drawing.Point(210, 385);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(239, 40);
            this.simpleButton27.TabIndex = 95;
            this.simpleButton27.Text = " ";
            this.simpleButton27.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton21
            // 
            this.simpleButton21.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton21.Appearance.Options.UseFont = true;
            this.simpleButton21.Location = new System.Drawing.Point(396, 243);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(40, 40);
            this.simpleButton21.TabIndex = 93;
            this.simpleButton21.Text = "U";
            this.simpleButton21.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton22
            // 
            this.simpleButton22.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton22.Appearance.Options.UseFont = true;
            this.simpleButton22.Location = new System.Drawing.Point(281, 339);
            this.simpleButton22.Name = "simpleButton22";
            this.simpleButton22.Size = new System.Drawing.Size(40, 40);
            this.simpleButton22.TabIndex = 90;
            this.simpleButton22.Text = "V";
            this.simpleButton22.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton23
            // 
            this.simpleButton23.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton23.Appearance.Options.UseFont = true;
            this.simpleButton23.Location = new System.Drawing.Point(156, 243);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(40, 40);
            this.simpleButton23.TabIndex = 91;
            this.simpleButton23.Text = "W";
            this.simpleButton23.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton24
            // 
            this.simpleButton24.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton24.Appearance.Options.UseFont = true;
            this.simpleButton24.Location = new System.Drawing.Point(185, 339);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(40, 40);
            this.simpleButton24.TabIndex = 92;
            this.simpleButton24.Text = "X";
            this.simpleButton24.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton25
            // 
            this.simpleButton25.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton25.Appearance.Options.UseFont = true;
            this.simpleButton25.Location = new System.Drawing.Point(348, 243);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(40, 40);
            this.simpleButton25.TabIndex = 89;
            this.simpleButton25.Text = "Y";
            this.simpleButton25.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.Location = new System.Drawing.Point(540, 243);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(40, 40);
            this.simpleButton16.TabIndex = 88;
            this.simpleButton16.Text = "P";
            this.simpleButton16.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton17
            // 
            this.simpleButton17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton17.Appearance.Options.UseFont = true;
            this.simpleButton17.Location = new System.Drawing.Point(108, 243);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(40, 40);
            this.simpleButton17.TabIndex = 85;
            this.simpleButton17.Text = "Q";
            this.simpleButton17.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.Location = new System.Drawing.Point(252, 243);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(40, 40);
            this.simpleButton18.TabIndex = 86;
            this.simpleButton18.Text = "R";
            this.simpleButton18.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.Location = new System.Drawing.Point(168, 292);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(40, 40);
            this.simpleButton19.TabIndex = 87;
            this.simpleButton19.Text = "S";
            this.simpleButton19.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton20
            // 
            this.simpleButton20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton20.Appearance.Options.UseFont = true;
            this.simpleButton20.Location = new System.Drawing.Point(300, 243);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(40, 40);
            this.simpleButton20.TabIndex = 84;
            this.simpleButton20.Text = "T";
            this.simpleButton20.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.Location = new System.Drawing.Point(456, 292);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(40, 40);
            this.simpleButton11.TabIndex = 83;
            this.simpleButton11.Text = "K";
            this.simpleButton11.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.Location = new System.Drawing.Point(504, 292);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(40, 40);
            this.simpleButton12.TabIndex = 80;
            this.simpleButton12.Text = "L";
            this.simpleButton12.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.Location = new System.Drawing.Point(425, 339);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(40, 40);
            this.simpleButton13.TabIndex = 81;
            this.simpleButton13.Text = "M";
            this.simpleButton13.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton14
            // 
            this.simpleButton14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.Location = new System.Drawing.Point(377, 339);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(40, 40);
            this.simpleButton14.TabIndex = 82;
            this.simpleButton14.Text = "N";
            this.simpleButton14.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.Location = new System.Drawing.Point(492, 243);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(40, 40);
            this.simpleButton15.TabIndex = 79;
            this.simpleButton15.Text = "O";
            this.simpleButton15.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(264, 292);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(40, 40);
            this.simpleButton1.TabIndex = 78;
            this.simpleButton1.Text = "F";
            this.simpleButton1.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(312, 292);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(40, 40);
            this.simpleButton2.TabIndex = 75;
            this.simpleButton2.Text = "G";
            this.simpleButton2.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(360, 292);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(40, 40);
            this.simpleButton3.TabIndex = 76;
            this.simpleButton3.Text = "H";
            this.simpleButton3.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Location = new System.Drawing.Point(444, 243);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(40, 40);
            this.simpleButton4.TabIndex = 77;
            this.simpleButton4.Text = "I";
            this.simpleButton4.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Location = new System.Drawing.Point(408, 292);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(40, 40);
            this.simpleButton5.TabIndex = 74;
            this.simpleButton5.Text = "J";
            this.simpleButton5.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Location = new System.Drawing.Point(120, 292);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(40, 40);
            this.simpleButton6.TabIndex = 73;
            this.simpleButton6.Text = "A";
            this.simpleButton6.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Location = new System.Drawing.Point(329, 339);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(40, 40);
            this.simpleButton7.TabIndex = 70;
            this.simpleButton7.Text = "B";
            this.simpleButton7.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Location = new System.Drawing.Point(233, 339);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(40, 40);
            this.simpleButton8.TabIndex = 71;
            this.simpleButton8.Text = "C";
            this.simpleButton8.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Location = new System.Drawing.Point(216, 292);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(40, 40);
            this.simpleButton9.TabIndex = 72;
            this.simpleButton9.Text = "D";
            this.simpleButton9.Click += new System.EventHandler(this.btn0_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.Location = new System.Drawing.Point(204, 243);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(40, 40);
            this.simpleButton10.TabIndex = 69;
            this.simpleButton10.Text = "E";
            this.simpleButton10.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Location = new System.Drawing.Point(473, 339);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(40, 40);
            this.btnXoa.TabIndex = 68;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(300, 196);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(40, 40);
            this.btn5.TabIndex = 67;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(348, 196);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(40, 40);
            this.btn6.TabIndex = 64;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(396, 196);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(40, 40);
            this.btn7.TabIndex = 65;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(444, 196);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(40, 40);
            this.btn8.TabIndex = 66;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(492, 196);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(40, 40);
            this.btn9.TabIndex = 63;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(540, 196);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(40, 40);
            this.btn0.TabIndex = 62;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(108, 196);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(40, 40);
            this.btn1.TabIndex = 59;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(156, 196);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(40, 40);
            this.btn2.TabIndex = 60;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(204, 196);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(40, 40);
            this.btn3.TabIndex = 61;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(252, 196);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(40, 40);
            this.btn4.TabIndex = 58;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn0_Click);
            // 
            // txtTenkhach
            // 
            this.txtTenkhach.EnterMoveNextControl = true;
            this.txtTenkhach.Location = new System.Drawing.Point(108, 5);
            this.txtTenkhach.Name = "txtTenkhach";
            this.txtTenkhach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenkhach.Properties.Appearance.Options.UseFont = true;
            this.txtTenkhach.Size = new System.Drawing.Size(232, 26);
            this.txtTenkhach.TabIndex = 1;
            // 
            // txtEmail
            // 
            this.txtEmail.EnterMoveNextControl = true;
            this.txtEmail.Location = new System.Drawing.Point(108, 37);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(472, 26);
            this.txtEmail.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(7, 40);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 19);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Email";
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseTextOptions = true;
            this.btnBanphim.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBanphim.Location = new System.Drawing.Point(8, 278);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(6);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(93, 69);
            this.btnBanphim.TabIndex = 15;
            this.btnBanphim.Text = "Bàn phím";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // txtSodienthoai
            // 
            this.txtSodienthoai.EnterMoveNextControl = true;
            this.txtSodienthoai.Location = new System.Drawing.Point(425, 5);
            this.txtSodienthoai.Name = "txtSodienthoai";
            this.txtSodienthoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSodienthoai.Properties.Appearance.Options.UseFont = true;
            this.txtSodienthoai.Size = new System.Drawing.Size(155, 26);
            this.txtSodienthoai.TabIndex = 3;
            // 
            // lblSodienthoai
            // 
            this.lblSodienthoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodienthoai.Location = new System.Drawing.Point(346, 8);
            this.lblSodienthoai.Margin = new System.Windows.Forms.Padding(4);
            this.lblSodienthoai.Name = "lblSodienthoai";
            this.lblSodienthoai.Size = new System.Drawing.Size(72, 19);
            this.lblSodienthoai.TabIndex = 2;
            this.lblSodienthoai.Text = "Điện thoại";
            // 
            // lblTenkhach
            // 
            this.lblTenkhach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenkhach.Location = new System.Drawing.Point(7, 8);
            this.lblTenkhach.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenkhach.Name = "lblTenkhach";
            this.lblTenkhach.Size = new System.Drawing.Size(27, 19);
            this.lblTenkhach.TabIndex = 0;
            this.lblTenkhach.Text = "Tên";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.pnlReport);
            this.panelControl3.Controls.Add(this.panelControl2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 43);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1026, 725);
            this.panelControl3.TabIndex = 16;
            // 
            // pnlReport
            // 
            this.pnlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlReport.Location = new System.Drawing.Point(594, 2);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(430, 721);
            this.pnlReport.TabIndex = 17;
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.btnBanve1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1026, 43);
            this.panelControl4.TabIndex = 48;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.pictureEdit2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(820, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(204, 39);
            this.panelControl5.TabIndex = 13;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit2.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit2.Location = new System.Drawing.Point(-179, 2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(381, 35);
            this.pictureEdit2.TabIndex = 2;
            // 
            // btnBanve1
            // 
            this.btnBanve1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnBanve1.Controls.Add(this.title);
            this.btnBanve1.Controls.Add(this.home);
            this.btnBanve1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanve1.Location = new System.Drawing.Point(2, 2);
            this.btnBanve1.Name = "btnBanve1";
            this.btnBanve1.Size = new System.Drawing.Size(818, 39);
            this.btnBanve1.TabIndex = 8;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(49, 11);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(53, 19);
            this.title.TabIndex = 2;
            this.title.Text = "Đặt vé";
            // 
            // home
            // 
            this.home.Image = global::KP_RES.Properties.Resources.check_book_26;
            this.home.Location = new System.Drawing.Point(10, 4);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            // 
            // PF_Datve
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1026, 768);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.Name = "PF_Datve";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đặt vé";
            this.Load += new System.EventHandler(this.PF_Datve_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PF_Datve_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongghe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhachhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChietkhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonggiave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenkhach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSodienthoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).EndInit();
            this.btnBanve1.ResumeLayout(false);
            this.btnBanve1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl pnlReport;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl btnBanve1;
        private DevExpress.XtraEditors.LabelControl title;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSodienthoai;
        private DevExpress.XtraEditors.TextEdit txtTenkhach;
        private DevExpress.XtraEditors.LabelControl lblTenkhach;
        private DevExpress.XtraEditors.LabelControl lblSodienthoai;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txtTongtien;
        private DevExpress.XtraEditors.TextEdit txtTongghe;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MADIADIEM;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton simpleButton26;
        private DevExpress.XtraEditors.SimpleButton simpleButton27;
        private DevExpress.XtraEditors.SimpleButton simpleButton21;
        private DevExpress.XtraEditors.SimpleButton simpleButton22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.TextEdit txtThanhtien;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtChietkhau;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtTonggiave;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit cboKhachhang;
    }
}