﻿namespace KP_RES 
{
    partial class Frm_Main2 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame1 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame2 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main2));
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.timerGiaiphongbonho = new System.Windows.Forms.Timer(this.components);
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itThoat = new DevExpress.XtraEditors.TileItem();
            this.itThunho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itQuocthanh = new DevExpress.XtraEditors.TileItem();
            this.itQuocthanh2016 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itDalat = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itHaibatrung = new DevExpress.XtraEditors.TileItem();
            this.SuspendLayout();
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // timerGiaiphongbonho
            // 
            this.timerGiaiphongbonho.Enabled = true;
            this.timerGiaiphongbonho.Interval = 120000;
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Name = "tileGroup10";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_1366x768_5;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(0, 0);
            this.tileControl1.MaxId = 30;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RowCount = 4;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(984, 562);
            this.tileControl1.TabIndex = 3;
            this.tileControl1.Text = "Trang chủ";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itThoat);
            this.tileGroup2.Items.Add(this.itThunho);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itThoat
            // 
            this.itThoat.BackgroundImage = global::KP_RES.Properties.Resources.dong2;
            this.itThoat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itThoat.Checked = true;
            tileItemElement1.Text = "Thoát";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itThoat.Elements.Add(tileItemElement1);
            this.itThoat.Id = 0;
            this.itThoat.Name = "itThoat";
            this.itThoat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThoat_ItemClick);
            // 
            // itThunho
            // 
            this.itThunho.BackgroundImage = global::KP_RES.Properties.Resources.thunho;
            this.itThunho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itThunho.Checked = true;
            tileItemElement2.Text = "Thu nhỏ";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itThunho.Elements.Add(tileItemElement2);
            this.itThunho.Id = 1;
            this.itThunho.Name = "itThunho";
            this.itThunho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThunho_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itQuocthanh);
            this.tileGroup3.Items.Add(this.itQuocthanh2016);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itQuocthanh
            // 
            this.itQuocthanh.BackgroundImage = global::KP_RES.Properties.Resources.cuahang;
            this.itQuocthanh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cinestar Quốc Thanh";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuocthanh.Elements.Add(tileItemElement3);
            this.itQuocthanh.Id = 28;
            this.itQuocthanh.IsLarge = true;
            this.itQuocthanh.Name = "itQuocthanh";
            this.itQuocthanh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuocthanh_ItemClick);
            // 
            // itQuocthanh2016
            // 
            this.itQuocthanh2016.BackgroundImage = global::KP_RES.Properties.Resources.cuahang3;
            tileItemElement4.Text = "Cinestar Quốc Thanh 2016";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuocthanh2016.Elements.Add(tileItemElement4);
            this.itQuocthanh2016.Id = 29;
            this.itQuocthanh2016.IsLarge = true;
            this.itQuocthanh2016.Name = "itQuocthanh2016";
            this.itQuocthanh2016.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuocthanh2016_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itDalat);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itDalat
            // 
            this.itDalat.BackgroundImage = global::KP_RES.Properties.Resources.cuahang1;
            this.itDalat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Cinestar Đà Lạt";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itDalat.Elements.Add(tileItemElement5);
            tileItemFrame1.AnimateBackgroundImage = true;
            tileItemFrame1.AnimateImage = true;
            tileItemFrame1.AnimateText = true;
            tileItemFrame1.BackgroundImage = global::KP_RES.Properties.Resources.cuahang1;
            tileItemElement6.Text = "Cinestar Đà Lạt";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame1.Elements.Add(tileItemElement6);
            tileItemFrame1.UseBackgroundImage = true;
            tileItemFrame1.UseImage = true;
            tileItemFrame1.UseText = true;
            this.itDalat.Frames.Add(tileItemFrame1);
            this.itDalat.Id = 19;
            this.itDalat.IsLarge = true;
            this.itDalat.Name = "itDalat";
            this.itDalat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDalat_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itHaibatrung);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itHaibatrung
            // 
            this.itHaibatrung.BackgroundImage = global::KP_RES.Properties.Resources.cuahang2;
            this.itHaibatrung.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Cinestar Hai Bà Trưng";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itHaibatrung.Elements.Add(tileItemElement7);
            this.itHaibatrung.FrameAnimationInterval = 16000;
            tileItemFrame2.AnimateBackgroundImage = true;
            tileItemFrame2.AnimateImage = true;
            tileItemFrame2.AnimateText = true;
            tileItemFrame2.BackgroundImage = global::KP_RES.Properties.Resources.cuahang2;
            tileItemElement8.Text = "Cinestar Hai Bà Trưng";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame2.Elements.Add(tileItemElement8);
            tileItemFrame2.UseBackgroundImage = true;
            tileItemFrame2.UseImage = true;
            tileItemFrame2.UseText = true;
            this.itHaibatrung.Frames.Add(tileItemFrame2);
            this.itHaibatrung.Id = 14;
            this.itHaibatrung.IsLarge = true;
            this.itHaibatrung.Name = "itHaibatrung";
            this.itHaibatrung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHaibatrung_ItemClick);
            // 
            // Frm_Main2
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.ControlBox = false;
            this.Controls.Add(this.tileControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Main2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KingPos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main2_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main2_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Main2_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itThoat;
        private DevExpress.XtraEditors.TileItem itThunho;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private System.Windows.Forms.Timer timerGiaiphongbonho;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itDalat;
        private DevExpress.XtraEditors.TileItem itHaibatrung;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itQuocthanh;
        private DevExpress.XtraEditors.TileItem itQuocthanh2016;
    }
}