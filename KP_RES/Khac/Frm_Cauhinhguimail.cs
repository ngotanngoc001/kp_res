﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Cauhinhguimail : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinhguimail()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sql = "select MA,Email as TEN,SUDUNG from LISTMAIL";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl2.DataSource = dt;

            sql = "select MA AS MARP,TenReport,SUDUNG AS SUDUNGRP,Report from LISTREPORT";
            dt = clsMain.ReturnDataTable(sql);
            gridControl1.DataSource = dt;

            sql = "select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 Order by TEN";
            dt = clsMain.ReturnDataTable(sql);
            cboCuahang.Properties.DataSource = dt;

            sql = "select EMAIL_GUI,MATKHAU,CHUDE,NOIDUNG,THU2,THU3,THU4,THU5,THU6,THU7,CHUNHAT,TUAN,THANG,THOIGIAN,TAPTIN,HOST,PORT,MA_CUAHANG from CHITIETMAIL";
            dt = clsMain.ReturnDataTable(sql);
            if (dt.Rows.Count > 0)
            {
                txtEmailGui.Text = dt.Rows[0]["EMAIL_GUI"].ToString();
                txtMatkhau.Text = dt.Rows[0]["MATKHAU"].ToString();
                txtObject.Text = dt.Rows[0]["CHUDE"].ToString();
                ritNoidung.Text = dt.Rows[0]["NOIDUNG"].ToString();
                cbbHost.Text = dt.Rows[0]["HOST"].ToString();
                txtPort.Text = dt.Rows[0]["PORT"].ToString();
                chk_2.Checked = bool.Parse(dt.Rows[0]["THU2"].ToString());
                chk_3.Checked = bool.Parse(dt.Rows[0]["THU3"].ToString());
                chk_4.Checked = bool.Parse(dt.Rows[0]["THU4"].ToString());
                chk_5.Checked = bool.Parse(dt.Rows[0]["THU5"].ToString());
                chk_6.Checked = bool.Parse(dt.Rows[0]["THU6"].ToString());
                chk_7.Checked = bool.Parse(dt.Rows[0]["THU7"].ToString());
                chk_CN.Checked = bool.Parse(dt.Rows[0]["CHUNHAT"].ToString());
                chk_Tuan.Checked = bool.Parse(dt.Rows[0]["TUAN"].ToString());
                chk_Thang.Checked = bool.Parse(dt.Rows[0]["THANG"].ToString());
                cboCuahang.EditValue = int.Parse(dt.Rows[0]["MA_CUAHANG"].ToString());
                timeSent.EditValue = dt.Rows[0]["THOIGIAN"].ToString();
                if (dt.Rows[0]["TAPTIN"].ToString() == "E")
                {
                    optExcel.Checked = true;
                    optPDF.Checked = false;
                }
                else
                {
                    optExcel.Checked = false;
                    optPDF.Checked = true;
                }
            }
        }

        private bool CheckInput()
        {
            bool kq = true;
            if (txtEmailGui.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập Email gửi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmailGui.Focus();
                kq = false;
            }
            else if (txtObject.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập chủ đề Mail", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtObject.Focus();
                kq = false;
            }
            else if (cbbHost.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập Host", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cbbHost.Focus();
                kq = false;
            }
            else if (txtPort.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập Port", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPort.Focus();
                kq = false;
            }
            else if (cboCuahang.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn cửa hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboCuahang.Focus();
                kq = false;
            }
            else if (gridView2.RowCount > 0)
            {
                int n = gridView2.RowCount;
                bool flag = false;
                for (int i = 0; i < n; i++)
                {
                    if (bool.Parse(gridView2.GetRowCellValue(i, SUDUNG).ToString()))
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn Email nhận nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    kq = false;
                }
            }
            else
            {
                if (cls_KP_RES.isValidEmail(txtEmailGui.Text.Trim()))
                    kq = true;
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Email không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmailGui.Focus();
                    kq = false;
                }
            }
            return kq;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sql = string.Empty;
            int n = gridView2.RowCount;
            for (int i = 0; i < n; i++)
            {
                sql += "update LISTMAIL set SUDUNG='" + bool.Parse(gridView2.GetRowCellValue(i, SUDUNG).ToString()) + "' where MA='" + gridView2.GetRowCellValue(i, MA).ToString() + "'\n";
            }
            int m = gridView1.RowCount;
            for (int j = 0; j < m; j++)
            {
                sql += "\n update LISTREPORT set SUDUNG='" + bool.Parse(gridView1.GetRowCellValue(j, SUDUNGRP).ToString()) + "' where MA='" + gridView1.GetRowCellValue(j, MARP).ToString() + "'\n";
            }
            //string[] hour = cbbHour.Text.Split(':');
            string fi = string.Empty;
            if (optExcel.Checked)
                fi = "E";
            else
                fi = "P";
            sql += "\n update CHITIETMAIL set EMAIL_GUI=" + clsMain.SQLString(txtEmailGui.Text.Trim()) + ",MATKHAU="+ clsMain.SQLString(txtMatkhau.Text.Trim()) +",CHUDE=N"
                + clsMain.SQLString(txtObject.Text.Trim()) + ",NOIDUNG=N"+clsMain.SQLString(ritNoidung.Text.Trim())+",TAPTIN="+ clsMain.SQLString(fi) + ",THOIGIAN='"
                + string.Format("{0:HH:mm:ss}", timeSent.EditValue) + "',THU2=" + clsMain.SQLBit(chk_2.Checked) + ",THU3=" + clsMain.SQLBit(chk_3.Checked) + ",THU4=" + clsMain.SQLBit(chk_4.Checked) + ",THU5="
                + clsMain.SQLBit(chk_5.Checked) + ",THU6=" + clsMain.SQLBit(chk_6.Checked) + ",THU7=" + clsMain.SQLBit(chk_7.Checked) + ",CHUNHAT="
                + clsMain.SQLBit(chk_CN.Checked) + ",TUAN=" + clsMain.SQLBit(chk_Tuan.Checked) + ",THANG=" + clsMain.SQLBit(chk_Thang.Checked) + ",HOST=" 
                + clsMain.SQLString(cbbHost.Text.Trim()) + ",PORT=" + txtPort.Text + ",MA_CUAHANG=" + cboCuahang.EditValue + " where MA=1";
            if (clsMain.ExecuteSQL(sql))
            {
                XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);     
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThemEmail_Click(object sender, EventArgs e)
        {
            Frm_Email frm = new Frm_Email();
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
            frm.Dispose();
            LoadOption();
        }

        private void txtPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }
}