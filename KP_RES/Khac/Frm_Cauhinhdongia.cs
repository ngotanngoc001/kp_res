﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_Cauhinhdongia : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Cauhinhdongia()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhdongiathue", culture);
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='GIAMUATRUOCTHUE' Or TEN='GIABANTRUOCTHUE' ";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TEN"].ToString() == "GIAMUATRUOCTHUE")
                {
                    optGIAMUA.Checked = dr["GIATRI"].ToString() == "1" ? true : false;
                    optGIAMUA1.Checked = !optGIAMUA.Checked;
                }
                if (dr["TEN"].ToString() == "GIABANTRUOCTHUE")
                {
                    optGIABAN.Checked = dr["GIATRI"].ToString() == "1" ? true : false;
                    optGIABAN1.Checked = !optGIABAN.Checked;
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='GIAMUATRUOCTHUE'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("GIAMUATRUOCTHUE") + ",";
            sSQL += clsMain.SQLBit(optGIAMUA.Checked) + ",";
            sSQL += clsMain.SQLStringUnicode("Cấu hình giá mua. 1 - Giá mua trước thuế. 0-Giá mua sau thuế") + ")" +"\n";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='GIABANTRUOCTHUE'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("GIABANTRUOCTHUE") + ",";
            sSQL += clsMain.SQLBit(optGIABAN.Checked) + ",";
            sSQL += clsMain.SQLStringUnicode("Cấu hình giá bán. 1 - Giá bán trước thuế. 0-Giá bán sau thuế") + ")"+"\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string _thongbao = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";
        private void Frm_Cauhinhdongia_Load(object sender, EventArgs e)
        {
            lblTEN.Text = rm.GetString("cauhinhgiamua",culture);
            optGIAMUA.Text = rm.GetString("giamuatruocthue",culture);
            optGIAMUA1.Text = rm.GetString("giamuasauthue",culture);
            labelControl1.Text = rm.GetString("cauhinhgiaban",culture);
            optGIABAN.Text = rm.GetString("giabantruocthue",culture);
            optGIABAN1.Text = rm.GetString("giabansauthue",culture);
            btnLuu.Text = rm.GetString("luu",culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);

        }
    }
}