﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using System.Resources;
using System.Globalization;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Giaodien : DevExpress.XtraEditors.XtraForm 
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Giaodien()
        {
            InitializeComponent();
            this.Text = rm.GetString("giaodien",culture);
        }

        private void Frm_Giaodien_Load(object sender, EventArgs e) 
        {
            SkinHelper.InitSkinGallery(galleryControl1,true ,true );
            galleryControl1.Gallery.ImageSize = new Size(65, 65);
            chkAuto.Checked = cls_KP_RES.bAutoSkin;
            chkAuto.Text = rm.GetString("giaodientudong",culture);
            btnMacdinh.Text = rm.GetString("macdinh",culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _dacapnhatvegdmacdinh = rm.GetString("dacapnhatvegdmacdinh", culture);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string _dacapnhatvegdmacdinh = "";
        string _thongbao = "";
        private void btnMacdinh_Click(object sender, EventArgs e)
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            cls_KP_RES.LoadDefaultLayout();
            XtraMessageBox.Show(_dacapnhatvegdmacdinh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnDong.Focus();
        }

        private void chkAuto_CheckedChanged(object sender, EventArgs e)
        {
            cls_KP_RES.bAutoSkin = chkAuto.Checked;
        }
    }
}