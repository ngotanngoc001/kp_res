﻿
    partial class Frm_ConfigInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ConfigInventory));
            this.panel2 = new System.Windows.Forms.Panel();
            this.chk_NoWarning = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Warning = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt_Show = new DevExpress.XtraEditors.TextEdit();
            this.txt_Hide = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chk_NoWarning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Warning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Show.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Hide.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(407, 35);
            this.panel2.TabIndex = 1;
            // 
            // chk_NoWarning
            // 
            this.chk_NoWarning.EnterMoveNextControl = true;
            this.chk_NoWarning.Location = new System.Drawing.Point(19, 162);
            this.chk_NoWarning.Name = "chk_NoWarning";
            this.chk_NoWarning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_NoWarning.Properties.Appearance.Options.UseFont = true;
            this.chk_NoWarning.Properties.Caption = "Không cảnh báo";
            this.chk_NoWarning.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk_NoWarning.Properties.RadioGroupIndex = 1;
            this.chk_NoWarning.Size = new System.Drawing.Size(740, 24);
            this.chk_NoWarning.TabIndex = 8;
            this.chk_NoWarning.TabStop = false;
            // 
            // chk_Warning
            // 
            this.chk_Warning.EditValue = true;
            this.chk_Warning.EnterMoveNextControl = true;
            this.chk_Warning.Location = new System.Drawing.Point(19, 62);
            this.chk_Warning.Name = "chk_Warning";
            this.chk_Warning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Warning.Properties.Appearance.Options.UseFont = true;
            this.chk_Warning.Properties.Caption = "Cảnh báo";
            this.chk_Warning.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk_Warning.Properties.RadioGroupIndex = 1;
            this.chk_Warning.Size = new System.Drawing.Size(438, 24);
            this.chk_Warning.TabIndex = 7;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(5, 40);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(126, 19);
            this.lblTEN.TabIndex = 9;
            this.lblTEN.Text = "Cảnh báo tồn kho";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(49, 93);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(102, 19);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Thời gian hiện";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(49, 126);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(89, 19);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Thời gian ẩn";
            // 
            // txt_Show
            // 
            this.txt_Show.Location = new System.Drawing.Point(167, 90);
            this.txt_Show.Name = "txt_Show";
            this.txt_Show.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Show.Properties.Appearance.Options.UseFont = true;
            this.txt_Show.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_Show.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_Show.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_Show.Properties.MaxLength = 1000;
            this.txt_Show.Size = new System.Drawing.Size(178, 26);
            this.txt_Show.TabIndex = 13;
            // 
            // txt_Hide
            // 
            this.txt_Hide.EditValue = "";
            this.txt_Hide.Location = new System.Drawing.Point(167, 123);
            this.txt_Hide.Name = "txt_Hide";
            this.txt_Hide.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Hide.Properties.Appearance.Options.UseFont = true;
            this.txt_Hide.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_Hide.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_Hide.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_Hide.Properties.MaxLength = 1000;
            this.txt_Hide.Size = new System.Drawing.Size(178, 26);
            this.txt_Hide.TabIndex = 14;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(352, 93);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 19);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "(Giây)";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(352, 126);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 19);
            this.labelControl4.TabIndex = 18;
            this.labelControl4.Text = "(Giây)";
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(285, 193);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 20;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(167, 193);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_ConfigInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 233);
            this.ControlBox = false;
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txt_Hide);
            this.Controls.Add(this.txt_Show);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.chk_NoWarning);
            this.Controls.Add(this.chk_Warning);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(800, 300);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ConfigInventory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cảnh Báo Tồn Kho";
            this.Load += new System.EventHandler(this.Frm_ConfigInventory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chk_NoWarning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Warning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Show.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Hide.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.CheckEdit chk_NoWarning;
        private DevExpress.XtraEditors.CheckEdit chk_Warning;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt_Show;
        private DevExpress.XtraEditors.TextEdit txt_Hide;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnLuu;

    }

