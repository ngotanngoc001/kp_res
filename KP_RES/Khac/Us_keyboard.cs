﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace KP_UserManagement
{
    public partial class Us_Keyboard : UserControl
    {
        /// <summary>
        /// keyboard
        /// </summary>
        public Us_Keyboard()
        {
            InitializeComponent();          
        }

        private TextEdit _textbox;
        private int indexshift = 0;  

        /// <summary>
        /// Gán giá trị
        /// </summary>
        /// <param name="edit"></param>
        public void SetTextbox(TextEdit edit)
        {
            try
            {
                _textbox = edit;
            }
            catch
            {
            }
        }

        #region bàn phím số
      

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            try
            {
                _textbox.Text = "";
                _textbox.Focus();
            }
            catch
            {
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (select != 0)
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = select - 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = _textbox.Text.Length;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("1");
                        break;
                    case 1:
                        Click_buttonNumber("!");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("2");
                        break;
                    case 1:
                        Click_buttonNumber("@");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("3");
                        break;
                    case 1:
                        Click_buttonNumber("#");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("4");
                        break;
                    case 1:
                        Click_buttonNumber("$");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("5");
                        break;
                    case 1:
                        Click_buttonNumber("%");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("6");
                        break;
                    case 1:
                        Click_buttonNumber("^");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("7");
                        break;
                    case 1:
                        Click_buttonNumber("&");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("8");
                        break;
                    case 1:
                        Click_buttonNumber("*");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("9");
                        break;
                    case 1:
                        Click_buttonNumber("(");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("0");
                        break;
                    case 1:
                        Click_buttonNumber(")");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_enter_Click(object sender, EventArgs e)
        {
            try
            {
                _textbox.Focus();
                SendKeys.Send("{ENTER}");
            }
            catch
            {
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (select != 0)
                {
                    if (_textbox.SelectionLength < 1)
                    {
                        _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                        _textbox.Focus();
                        _textbox.SelectionStart = select - 1;
                        _textbox.SelectionLength = 0;
                    }
                    else
                    {
                        int length = _textbox.SelectionLength;
                        _textbox.Text = _textbox.Text.Remove(select, length);
                        _textbox.Focus();
                        _textbox.SelectionStart = select;
                        _textbox.SelectionLength = 0;
                    }
                }
                else
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = 0;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        private void btn_A_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("a");
                        break;
                    case 1:
                        Click_buttonNumber("A");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_B_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("b");
                        break;
                    case 1:
                        Click_buttonNumber("B");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_C_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("c");
                        break;
                    case 1:
                        Click_buttonNumber("C");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_D_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("d");
                        break;
                    case 1:
                        Click_buttonNumber("D");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_E_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("e");
                        break;
                    case 1:
                        Click_buttonNumber("E");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_F_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("f");
                        break;
                    case 1:
                        Click_buttonNumber("F");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_G_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("g");
                        break;
                    case 1:
                        Click_buttonNumber("G");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_H_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("h");
                        break;
                    case 1:
                        Click_buttonNumber("H");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_I_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("i");
                        break;
                    case 1:
                        Click_buttonNumber("I");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_J_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("j");
                        break;
                    case 1:
                        Click_buttonNumber("J");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_K_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("k");
                        break;
                    case 1:
                        Click_buttonNumber("K");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_L_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("l");
                        break;
                    case 1:
                        Click_buttonNumber("L");
                        break;
                }
            }
            catch 
            {
            }
        }

        private void btn_M_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("m");
                        break;
                    case 1:
                        Click_buttonNumber("M");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_N_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("n");
                        break;
                    case 1:
                        Click_buttonNumber("N");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_O_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("o");
                        break;
                    case 1:
                        Click_buttonNumber("O");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_P_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("p");
                        break;
                    case 1:
                        Click_buttonNumber("P");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Q_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("q");
                        break;
                    case 1:
                        Click_buttonNumber("Q");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_R_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("r");
                        break;
                    case 1:
                        Click_buttonNumber("R");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_S_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("s");
                        break;
                    case 1:
                        Click_buttonNumber("S");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_T_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("t");
                        break;
                    case 1:
                        Click_buttonNumber("T");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_U_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("u");
                        break;
                    case 1:
                        Click_buttonNumber("U");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_V_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("v");
                        break;
                    case 1:
                        Click_buttonNumber("V");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_W_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("w");
                        break;
                    case 1:
                        Click_buttonNumber("W");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Y_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("y");
                        break;
                    case 1:
                        Click_buttonNumber("Y");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Z_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("z");
                        break;
                    case 1:
                        Click_buttonNumber("Z");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_SLeft_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("\\");
                        break;
                    case 1:
                        Click_buttonNumber("|");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber(".");
                        break;
                    case 1:
                        Click_buttonNumber(">");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_SRight_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("/");
                        break;
                    case 1:
                        Click_buttonNumber("?");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_X_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("x");
                        break;
                    case 1:
                        Click_buttonNumber("X");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Cross_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("-");
                        break;
                    case 1:
                        Click_buttonNumber("_");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Leftcurlybrackets_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("[");
                        break;
                    case 1:
                        Click_buttonNumber("{");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Rightcurlybrackets_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("]");
                        break;
                    case 1:
                        Click_buttonNumber("}");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_colon_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber(";");
                        break;
                    case 1:
                        Click_buttonNumber(":");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_quotationmarks_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("'");
                        break;
                    case 1:
                        Click_buttonNumber("\"");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Space_Click(object sender, EventArgs e)
        {
            try
            {
                Click_buttonNumber(" ");
            }
            catch
            {
            }
        }

        private void btn_Comma_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber(",");
                        break;
                    case 1:
                        Click_buttonNumber("<");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Shift_Click(object sender, EventArgs e)
        {
            try
            {
                if (indexshift == 0)
                    indexshift = 1;
                else if (indexshift == 1)
                    indexshift = 0;
                if (indexshift == 1)
                {
                    btn_Shift.ForeColor = Color.Maroon;
                }
                else
                {
                    btn_Shift.ForeColor = Color.DarkOliveGreen;
                }
            }
            catch
            {
            }
        }

        private void Click_buttonNumber(string number)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (_textbox.SelectionLength < 1)
                {
                    _textbox.Text = _textbox.Text.Insert(_textbox.SelectionStart, number);
                    _textbox.Focus();
                    _textbox.SelectionStart = select + 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    int length = _textbox.SelectionLength;
                    _textbox.Text = _textbox.Text.Remove(select, length);
                    _textbox.Text = _textbox.Text.Insert(select, number);
                    _textbox.Focus();
                    _textbox.SelectionStart = select + 1;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        #endregion
    }
}
