﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_Cauhinhcaban : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Cauhinhcaban()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhcaban", culture);
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select GIATRI From CAUHINH where TEN='CAUHINHCABAN'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() == "1")
                    optCheckFull.Checked = true;
                else
                    optCheckPart.Checked = true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "UPDATE CAUHINH SET GIATRI=" + clsMain.SQLBit(optCheckFull.Checked) + " WHERE TEN='CAUHINHCABAN'";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string _thongbao = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";
        private void Frm_Cauhinhcaban_Load(object sender, EventArgs e)
        {
            lblTEN.Text = rm.GetString("caban",culture);
            optCheckFull.Text = rm.GetString("kiemtradaydu",culture);
            optCheckPart.Text = rm.GetString("chikiemtraquayvanhanvien",culture);
            btnLuu.Text = rm.GetString("luu",culture);
            btnDong.Text = rm.GetString("dong", culture);

            _thongbao = rm.GetString("thongbao", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);	
        }
    }
}