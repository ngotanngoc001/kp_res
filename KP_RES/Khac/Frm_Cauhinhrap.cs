﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Cauhinhrap : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinhrap()
        {
            InitializeComponent();
            loadCombo();
        }

        public void loadCombo()
        {
            string ma = string.Empty;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt = clsMain.ReturnDataTable("select MADIADIEM as MA,TENDIADIEM as TEN from DM_DIADIEM where SUDUNG=1");
            cboRAP.Properties.DataSource = dt;
            cboRAP.EditValue = cboRAP.Properties.GetDataSourceValue(cboRAP.Properties.ValueMember, 0);

            if (File.Exists(Application.StartupPath + "\\Rap.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\Rap.xml");
                    dt = ds.Tables[0];
                    ma = dt.Rows[0]["MADIADIEM"].ToString();
                    cboRAP.EditValue = int.Parse(ma);

                    txtMa.Text = dt.Rows[0]["MADIADIEM"].ToString();
                    txtTen.Text = dt.Rows[0]["TENDIADIEM"].ToString();
                }
                catch
                {
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("MADIADIEM");
                dt.Columns.Add("TENDIADIEM");
                dt.Rows.Add(cboRAP.EditValue.ToString(),cboRAP.Text );
                ds.Tables.Add(dt);
                if (File.Exists(Application.StartupPath + "\\Rap.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Rap.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds.WriteXml(Application.StartupPath + "\\Rap.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds.WriteXml(Application.StartupPath + "\\Rap.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Rap.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }

                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboRAP_EditValueChanged(object sender, EventArgs e)
        {
            txtMa.Text = cboRAP.EditValue.ToString();
            txtTen.Text = cboRAP.Text;
        }

        private void cboRAP_Leave(object sender, EventArgs e)
        {
            txtMa.Text = cboRAP.EditValue.ToString();
            txtTen.Text = cboRAP.Text;
        }
    }
}