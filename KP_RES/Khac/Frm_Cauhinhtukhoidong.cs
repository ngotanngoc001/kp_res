﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using Microsoft.Win32;

namespace KP_RES 
{
    public partial class Frm_Cauhinhtukhoidong : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinhtukhoidong()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\", true);//Khóa dành cho khởi động cùng Windows
            optMenu.Checked = false ;
            optKHTT.Checked = false;
            optShowTime.Checked = false;
            optManhinhbep.Checked = false;
            foreach (string Programs in rk.GetValueNames())
            {
                if (Programs == "MenuDisplay")
                {
                    optMenu.Checked = true;
                }
                if (Programs == "CoopMart")
                {
                    optKHTT.Checked = true;
                }
                if (Programs == "MenuTicket")
                {
                    optShowTime.Checked = true;
                }
                if (Programs == "DisplayKitchen")
                {
                    optManhinhbep.Checked = true;
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\", true);//Khóa dành cho khởi động cùng Windows
                rk.DeleteValue("CoopMart", false); //Xóa khóa trong Registry
                rk.DeleteValue("MenuDisplay", false); //Xóa khóa trong Registry
                rk.DeleteValue("MenuTicket", false); //Xóa khóa trong Registry
                rk.DeleteValue("DisplayKitchen", false); //Xóa khóa trong Registry
                if (optMenu.Checked)
                {
                    string path = Application.StartupPath + "\\MenuDisplay.exe";  //Tên của ứng dụng đang chạy
                    rk.SetValue("MenuDisplay", path); //Ghi vào Registry
                }
                if (optKHTT.Checked)
                {
                    string path = Application.StartupPath + "\\KP_KHTT.exe";  //Tên của ứng dụng đang chạy
                    rk.SetValue("CoopMart", path); //Ghi vào Registry
                }
                if (optShowTime.Checked)
                {
                    string path = Application.StartupPath + "\\MenuTicket.exe";  //Tên của ứng dụng đang chạy
                    rk.SetValue("MenuTicket", path); //Ghi vào Registry
                }
                if (optManhinhbep.Checked)
                {
                    string path = Application.StartupPath + "\\DisplayKitchen.exe";  //Tên của ứng dụng đang chạy
                    rk.SetValue("DisplayKitchen", path); //Ghi vào Registry
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}