﻿namespace KP_RES
{
    partial class Frm_XacNhan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChapNhan = new DevExpress.XtraEditors.SimpleButton();
            this.btnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.txtMatKhauCu = new DevExpress.XtraEditors.TextEdit();
            this.txtPassNew = new DevExpress.XtraEditors.TextEdit();
            this.txtReWritPassNew = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhauCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReWritPassNew.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnChapNhan
            // 
            this.btnChapNhan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnChapNhan.Appearance.Options.UseFont = true;
            this.btnChapNhan.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnChapNhan.Location = new System.Drawing.Point(233, 143);
            this.btnChapNhan.Name = "btnChapNhan";
            this.btnChapNhan.Size = new System.Drawing.Size(101, 35);
            this.btnChapNhan.TabIndex = 7;
            this.btnChapNhan.Text = "&1.Đồng ý";
            this.btnChapNhan.Click += new System.EventHandler(this.btnChapNhan_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnThoat.Appearance.Options.UseFont = true;
            this.btnThoat.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnThoat.Location = new System.Drawing.Point(340, 143);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(88, 35);
            this.btnThoat.TabIndex = 8;
            this.btnThoat.Text = "&2.Đóng";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // txtMatKhauCu
            // 
            this.txtMatKhauCu.EnterMoveNextControl = true;
            this.txtMatKhauCu.Location = new System.Drawing.Point(173, 47);
            this.txtMatKhauCu.Name = "txtMatKhauCu";
            this.txtMatKhauCu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMatKhauCu.Properties.Appearance.Options.UseFont = true;
            this.txtMatKhauCu.Properties.PasswordChar = '*';
            this.txtMatKhauCu.Size = new System.Drawing.Size(255, 26);
            this.txtMatKhauCu.TabIndex = 2;
            // 
            // txtPassNew
            // 
            this.txtPassNew.EnterMoveNextControl = true;
            this.txtPassNew.Location = new System.Drawing.Point(173, 79);
            this.txtPassNew.Name = "txtPassNew";
            this.txtPassNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtPassNew.Properties.Appearance.Options.UseFont = true;
            this.txtPassNew.Properties.PasswordChar = '*';
            this.txtPassNew.Size = new System.Drawing.Size(255, 26);
            this.txtPassNew.TabIndex = 4;
            // 
            // txtReWritPassNew
            // 
            this.txtReWritPassNew.EnterMoveNextControl = true;
            this.txtReWritPassNew.Location = new System.Drawing.Point(173, 111);
            this.txtReWritPassNew.Name = "txtReWritPassNew";
            this.txtReWritPassNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtReWritPassNew.Properties.Appearance.Options.UseFont = true;
            this.txtReWritPassNew.Properties.PasswordChar = '*';
            this.txtReWritPassNew.Size = new System.Drawing.Size(255, 26);
            this.txtReWritPassNew.TabIndex = 6;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(12, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(109, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mã xác nhận cũ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(12, 82);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(120, 19);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Mã xác nhận mới";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(12, 114);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(153, 19);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Nhập lại mã xác nhận";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(442, 35);
            this.panel2.TabIndex = 0;
            // 
            // Frm_XacNhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 186);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtReWritPassNew);
            this.Controls.Add(this.txtPassNew);
            this.Controls.Add(this.txtMatKhauCu);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnChapNhan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_XacNhan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thay đổi mã xác nhận";
            this.Load += new System.EventHandler(this.Frm_XacNhan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhauCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReWritPassNew.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnChapNhan;
        private DevExpress.XtraEditors.SimpleButton btnThoat;
        private DevExpress.XtraEditors.TextEdit txtMatKhauCu;
        private DevExpress.XtraEditors.TextEdit txtPassNew;
        private DevExpress.XtraEditors.TextEdit txtReWritPassNew;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel panel2;
    }
}