﻿namespace KP_RES
{
    partial class Frm_Cauhinhtinhgiobida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.optCheckNotallow = new DevExpress.XtraEditors.CheckEdit();
            this.optCheckAllow = new DevExpress.XtraEditors.CheckEdit();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.optCheckNotallow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optCheckAllow.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(481, 35);
            this.panel2.TabIndex = 23;
            // 
            // optCheckNotallow
            // 
            this.optCheckNotallow.EnterMoveNextControl = true;
            this.optCheckNotallow.Location = new System.Drawing.Point(222, 83);
            this.optCheckNotallow.Name = "optCheckNotallow";
            this.optCheckNotallow.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optCheckNotallow.Properties.Appearance.Options.UseFont = true;
            this.optCheckNotallow.Properties.Caption = "Không cho phép chỉnh giờ";
            this.optCheckNotallow.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optCheckNotallow.Properties.RadioGroupIndex = 1;
            this.optCheckNotallow.Size = new System.Drawing.Size(218, 24);
            this.optCheckNotallow.TabIndex = 22;
            this.optCheckNotallow.TabStop = false;
            // 
            // optCheckAllow
            // 
            this.optCheckAllow.EditValue = true;
            this.optCheckAllow.EnterMoveNextControl = true;
            this.optCheckAllow.Location = new System.Drawing.Point(34, 83);
            this.optCheckAllow.Name = "optCheckAllow";
            this.optCheckAllow.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optCheckAllow.Properties.Appearance.Options.UseFont = true;
            this.optCheckAllow.Properties.Caption = "Cho phép chỉnh giờ";
            this.optCheckAllow.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optCheckAllow.Properties.RadioGroupIndex = 1;
            this.optCheckAllow.Size = new System.Drawing.Size(182, 24);
            this.optCheckAllow.TabIndex = 19;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(354, 123);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 21;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(236, 123);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 20;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_Cauhinhtinhgiobida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 173);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.optCheckNotallow);
            this.Controls.Add(this.optCheckAllow);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhtinhgiobida";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Cauhinhtinhgiobida";
            this.Load += new System.EventHandler(this.Frm_Cauhinhtinhgiobida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.optCheckNotallow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optCheckAllow.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.CheckEdit optCheckNotallow;
        private DevExpress.XtraEditors.CheckEdit optCheckAllow;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
    }
}