﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Cauhinhtonkho : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);


        public Frm_Cauhinhtonkho()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhngaytinhton", culture);
            dtpNgaytonkho.EditValue = clsGlobal.gdServerDate;
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select GIATRI From CAUHINH where TEN='CAUHINHTONKHO'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                dtpNgaytonkho.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='CAUHINHTONKHO'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("CAUHINHTONKHO") + ",";
            sSQL += clsMain.SQLString(string.Format("{0: yyyy/MM/dd}",dtpNgaytonkho.EditValue)) + ",";
            sSQL += clsMain.SQLStringUnicode("Cấu hình ngày tính tồn kho") + ")" + "\n";


            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string _thongbao = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";
        private void Frm_Cauhinhtonkho_Load(object sender, EventArgs e)
        {
            lblngaytonkho.Text = rm.GetString("ngaytinhtonkho",culture);
            btnLuu.Text = rm.GetString("luu",culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);

        }
    }
}