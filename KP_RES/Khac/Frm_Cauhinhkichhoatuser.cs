﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Cauhinhkichhoatuser : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinhkichhoatuser()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select GIATRI From CAUHINH where TEN='KICHHOATUSER'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() == "1")
                    optDUOCPHEP.Checked = true;
                else
                    optKHONGDUOCPHEP.Checked = true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "UPDATE CAUHINH SET GIATRI=" + clsMain.SQLBit(optDUOCPHEP.Checked) + " WHERE TEN='KICHHOATUSER'";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}