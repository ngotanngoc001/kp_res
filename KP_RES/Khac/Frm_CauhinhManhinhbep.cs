﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CauhinhManhinhbep : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhManhinhbep()
        {
            InitializeComponent();
            loadCombo();
            Load_MucTG();
        }

        public void loadCombo()
        {
            string ma = string.Empty;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt = clsMain.ReturnDataTable("select MA_BEP,TEN_BEP from DM_BEP where SUDUNG=1");
            cboBEP.Properties.DataSource = dt;
            cboBEP.EditValue = cboBEP.Properties.GetDataSourceValue(cboBEP.Properties.ValueMember, 0);

            if (File.Exists(Application.StartupPath + "\\BepSD.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\BepSD.xml");
                    dt = ds.Tables[0];
                    ma = dt.Rows[0]["MA_BEP"].ToString();
                    cboBEP.EditValue = int.Parse(ma);
                }
                catch
                {
                }
            }
        }

        private void Load_MucTG()
        {
            DataTable dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER1'");
            if (dt.Rows.Count > 0)
                txtMucTG1.Text = dt.Rows[0][0].ToString();
            dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER2'");
            if (dt.Rows.Count > 0)
                txtMucTG2.Text = dt.Rows[0][0].ToString();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("MA_BEP");
                dt.Rows.Add(cboBEP.EditValue.ToString());
                ds.Tables.Add(dt);
                if (File.Exists(Application.StartupPath + "\\BepSD.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\BepSD.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds.WriteXml(Application.StartupPath + "\\BepSD.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds.WriteXml(Application.StartupPath + "\\BepSD.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\BepSD.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
            }
            string sSQL = "";
            sSQL += "UPDATE CAUHINH SET GIATRI='" + txtMucTG1.Text + "' where TEN='TIMEORDER1'\n";
            sSQL += "UPDATE CAUHINH SET GIATRI='" + txtMucTG2.Text + "' where TEN='TIMEORDER2'\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMucTG1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private bool CheckInput()
        {
            if (int.Parse(txtMucTG1.Text) <= int.Parse(txtMucTG2.Text))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Thời gian chờ 1 phải nhỏ hơn thời gian chờ 2", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}