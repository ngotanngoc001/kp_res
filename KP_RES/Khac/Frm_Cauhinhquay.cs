﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES
{
    public partial class Frm_Cauhinhquay : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        string _MaCuaHang = null;
        //string _tencuahang = null;
       
        public Frm_Cauhinhquay()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhquay", culture);
            Getstalls();
            SetDuLieuCauHinh();            
        }
        string _thongbao = "";
        string _phaichondaydutt = "";
        string _dahuyttcauhinhcu = "";
        string _daluucauhinh = "";

        private void SetDuLieuCauHinh()
        {
            if (cls_ConfigCashier.idShop == "")
            {
               cls_ConfigCashier . LoadCauHinh();
            } 
            if (cls_ConfigCashier.idShop != "")
            {
                look_CuaHang.Properties.NullText = cls_ConfigCashier.nameShop;
                look_QuayHang.Properties.NullText = cls_ConfigCashier.nameCashier;
                look_KhoHang.Properties.NullText = cls_ConfigCashier.nameWarehouse;
            }
            look_KhoHang.Enabled = false;
            look_CuaHang.Enabled = false;
            look_QuayHang.Enabled = false;
            btn_cauhinh.Enabled = false;
        }
        private void btn_cauhinh_Click(object sender, EventArgs e)
        {
            try
            {
                if (look_CuaHang.EditValue == null || look_QuayHang.EditValue == null)
                {
                    XtraMessageBox.Show(_phaichondaydutt, _thongbao, MessageBoxButtons.OK);
                    return;
                }
                if (_MaCuaHang == null)
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "/systeminfo.txt");
                    StreamWriter strw = file.CreateText();
                    string line = look_CuaHang.EditValue.ToString() + "," + look_CuaHang.Text + "," + look_QuayHang.EditValue.ToString() + "," + look_QuayHang.Text + "," + look_KhoHang.EditValue.ToString() + "," + look_KhoHang.Text;
                    strw.WriteLine(line);
                    strw.Close();
                    
                    _MaCuaHang = look_CuaHang.EditValue.ToString();
                    look_CuaHang.Properties.ReadOnly = true;
                    throw new Exception(_daluucauhinh);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Application.Restart();
        }

        private void Getstalls()
        {
            string sql = "select * from CUAHANG where SUDUNG = 1";          
            DataTable dt = new DataTable();
            dt =clsMain.ReturnDataTable(sql);
            look_CuaHang.Properties.DataSource = dt;
            look_CuaHang.Properties.DisplayMember = "TEN_CUAHANG";
            look_CuaHang.Properties.ValueMember = "MA_CUAHANG";
            look_CuaHang.EditValue = null;
        }
        private void LoadKho(string macuahang) 
        {
            string sql = "select * from KHO  where MA_CUAHANG = '" + macuahang + "'";
            DataTable dt = new DataTable();
            dt = clsMain.ReturnDataTable(sql);

            look_KhoHang.Properties.DataSource = dt;
            look_KhoHang.Properties.DisplayMember = "TEN_KHO";
            look_KhoHang.Properties.ValueMember = "MA_KHO";
            look_KhoHang.EditValue = null;  
        }
        private void LoadQuayHang(string maKho) 
        {
            string sql = "select * from QUAY q inner join KHO k on q.MA_KHO = k.MA_KHO where k.MA_KHO = '" + maKho + "'";
            DataTable dt = new DataTable();
            dt = clsMain.ReturnDataTable(sql);

            look_QuayHang.Properties.DataSource = dt;
            look_QuayHang.Properties.DisplayMember = "TEN_QUAY";
            look_QuayHang.Properties.ValueMember = "MA_QUAY";
            look_QuayHang.EditValue = null;    
        }

        private void btn_thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void look_QuayHang_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //int ma=int.Parse(look_QuayHang.EditValue.ToString());
            }
            catch {
                return;
            }
            //string sql = "select * from KHO k inner join Quay q on k.MA_KHO = q.MA_KHO where q	.MA_QUAY =" + int.Parse(look_QuayHang.EditValue.ToString())+ "";           
            //DataTable dt = clsMain.ReturnDataTable(sql);

            //look_KhoHang.Text = dt.Rows[0]["TEN_KHO"].ToString();
            //look_KhoHang.Tag = dt.Rows[0]["MA_KHO"].ToString();
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_cauhinh.Enabled = true;
            look_KhoHang.Enabled = true;
            look_CuaHang.Enabled = true;
            look_QuayHang.Enabled = true;
            look_CuaHang.Properties.NullText = "";
            look_QuayHang.Properties.NullText = "";
            look_KhoHang.Properties.NullText = "";
            // hủy cài đặt quầy hàng cũ
            try
            {
                // lưu thông tin về server              
                FileInfo file = new FileInfo(Application.StartupPath + "/systeminfo.txt");
                file.Delete();
                _MaCuaHang = null;              
                        
                look_CuaHang.EditValue = null;
                look_CuaHang.Properties.ReadOnly = false;

                look_KhoHang.EditValue = null;
                look_KhoHang.Properties.ReadOnly = false;

                look_QuayHang.EditValue = null;
                look_QuayHang.Properties.ReadOnly = false;
               
                throw new Exception(_dahuyttcauhinhcu);            
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void look_CuaHang_EditValueChanged(object sender, EventArgs e)
        {
            if (look_CuaHang.EditValue != null)
            {
                string macuahang = look_CuaHang.EditValue.ToString();
                LoadKho(macuahang);
            }
        }

        private void look_KhoHang_EditValueChanged(object sender, EventArgs e)
        {
            if (look_KhoHang.EditValue != null)
            {
                string maKho = look_KhoHang.EditValue.ToString();
                LoadQuayHang(maKho);
            }
        }

        private void Frm_Cauhinhquay_Load(object sender, EventArgs e)
        {
            labelControl2.Text = rm.GetString("cuahang",culture);
            labelControl4.Text = rm.GetString("khohang",culture);
            labelControl5.Text = rm.GetString("quayhang", culture);

            btn_cauhinh.Text = rm.GetString("dongy", culture);
            btn_Reset.Text = rm.GetString("thietlaplai", culture);
            btn_thoat.Text = rm.GetString("dong", culture);

            _thongbao = rm.GetString("thongbao", culture);
            _daluucauhinh=rm.GetString("daluucauhinh", culture);
            _dahuyttcauhinhcu = rm.GetString("dahuyttcauhinhcu", culture);
            _phaichondaydutt = rm.GetString("phaichondaydutt", culture);
        }   
    }
}