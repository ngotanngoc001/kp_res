﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CauhinhThoigianngungbanve : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhThoigianngungbanve()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='THOIGIANNGUNGBANVE'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                dtp1.EditValue = int.Parse(dr["GIATRI"].ToString());
            }

            sSQL = "Select TEN,GIATRI From CAUHINH where TEN='THOIGIANHUYDATVE'";
            dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                txtThoigianhuydatve.EditValue = int.Parse(dr["GIATRI"].ToString());
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            sSQL += "Update CAUHINH" + "\n";
            sSQL += "Set GIATRI=" + clsMain.SQLString(dtp1.Text)  + "\n";
            sSQL += "Where TEN='THOIGIANNGUNGBANVE'" + "\n";

            sSQL += "Update CAUHINH" + "\n";
            sSQL += "Set GIATRI=" + clsMain.SQLString(txtThoigianhuydatve.Text) + "\n";
            sSQL += "Where TEN='THOIGIANHUYDATVE'" + "\n";
      
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckInput()
        {
            if (dtp1.Text =="")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập thời gian", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtp1.Focus();
                return false;
            }
            if (txtThoigianhuydatve.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập thời gian", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtThoigianhuydatve.Focus();
                return false;
            }
            else
            {
                return true;
            }
            
        }
    }
}