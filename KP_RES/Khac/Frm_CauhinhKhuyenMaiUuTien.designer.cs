﻿namespace KP_RES 
{
    partial class Frm_CauhinhKhuyenMaiUuTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.optTangHang = new DevExpress.XtraEditors.CheckEdit();
            this.optGiamGia = new DevExpress.XtraEditors.CheckEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.optTangHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGiamGia.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // optTangHang
            // 
            this.optTangHang.EnterMoveNextControl = true;
            this.optTangHang.Location = new System.Drawing.Point(291, 73);
            this.optTangHang.Name = "optTangHang";
            this.optTangHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTangHang.Properties.Appearance.Options.UseFont = true;
            this.optTangHang.Properties.Caption = "Mua hàng tặng hàng";
            this.optTangHang.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optTangHang.Properties.RadioGroupIndex = 1;
            this.optTangHang.Size = new System.Drawing.Size(171, 24);
            this.optTangHang.TabIndex = 6;
            this.optTangHang.TabStop = false;
            // 
            // optGiamGia
            // 
            this.optGiamGia.EditValue = true;
            this.optGiamGia.EnterMoveNextControl = true;
            this.optGiamGia.Location = new System.Drawing.Point(55, 73);
            this.optGiamGia.Name = "optGiamGia";
            this.optGiamGia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGiamGia.Properties.Appearance.Options.UseFont = true;
            this.optGiamGia.Properties.Caption = "Giảm giá theo nhóm hàng";
            this.optGiamGia.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optGiamGia.Properties.RadioGroupIndex = 1;
            this.optGiamGia.Size = new System.Drawing.Size(214, 24);
            this.optGiamGia.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(356, 113);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(238, 113);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(52, 19);
            this.lblTEN.TabIndex = 8;
            this.lblTEN.Text = "Ưu tiên";
            // 
            // Frm_CauhinhKhuyenMaiUuTien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 165);
            this.ControlBox = false;
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.optTangHang);
            this.Controls.Add(this.optGiamGia);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_CauhinhKhuyenMaiUuTien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình khuyến mãi ưu tiên";
            ((System.ComponentModel.ISupportInitialize)(this.optTangHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGiamGia.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit optGiamGia;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit optTangHang;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl lblTEN;




    }
}