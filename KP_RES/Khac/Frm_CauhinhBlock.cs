﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CauhinhBlock : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhBlock()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC' OR TEN='THOIGIAN1' OR TEN='THOIGIAN2' OR TEN='THOIGIAN3'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TEN"].ToString() == "APDUNGBANGGIATHEOKHUVUC")
                {
                    optAPDUNGBANGGIATHEOKHUVUC.Checked = dr["GIATRI"].ToString() == "1" ? true : false;
                    optAPDUNGBANGGIATHEOKHUVUC1.Checked = !optAPDUNGBANGGIATHEOKHUVUC.Checked;
                }
                if (dr["TEN"].ToString() == "THOIGIAN1")
                {
                    dtp1.EditValue = DateTime.Parse ( dr["GIATRI"].ToString ());
                }
                if (dr["TEN"].ToString() == "THOIGIAN2")
                {
                    dtp2.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
                }
                if (dr["TEN"].ToString() == "THOIGIAN3")
                {
                    dtp3.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='APDUNGBANGGIATHEOKHUVUC'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("APDUNGBANGGIATHEOKHUVUC") + ",";
            sSQL += clsMain.SQLBit(optAPDUNGBANGGIATHEOKHUVUC.Checked) + ",";
            sSQL += clsMain.SQLStringUnicode("Áp dụng bảng giá theo khu vực. 1 - Có áp dụng bảng giá theo khu vực. 0 - Không áp dụng,dùng bảng giá mặc định") + ")" +"\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN1'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN1") + ",";
            sSQL += clsMain.SQLString(dtp1.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 1") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN2'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN2") + ",";
            sSQL += clsMain.SQLString(dtp2.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 2") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN3'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN3") + ",";
            sSQL += clsMain.SQLString(dtp3.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 3") + ")" + "\n";

            sSQL += "Update THIETLAPBANGGIA" + "\n";
            sSQL += "Set THOIGIAN1=" + clsMain.SQLString(dtp1.Text) +","+ "\n";
            sSQL += "THOIGIAN2=" + clsMain.SQLString(dtp2.Text) + "," + "\n";
            sSQL += "THOIGIAN3=" + clsMain.SQLString(dtp3.Text) + "\n";
      
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckInput()
        {
            if ((DateTime)dtp1.EditValue < (DateTime)dtp2.EditValue && (DateTime)dtp2.EditValue < (DateTime)dtp3.EditValue)
            {
                return true;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Thời gian không đúng" + "\n" + "Thời gian 1 < Thời gian 2 < Thời gian 3", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            
        }
    }
}