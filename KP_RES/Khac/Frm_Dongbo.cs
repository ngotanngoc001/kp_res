﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES
{
    public partial class Frm_Dongbo : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Dongbo()
        {
            InitializeComponent();
        }

        private void btnDanhmuc_Click(object sender, EventArgs e)
        {
            splashScreenManager1.ShowWaitForm();

            string sNhomhang = "";
            sNhomhang = cls_Huyvietnam.getNhomhang(cls_ConfigCashier.sMenuID);
            string sHanghoa = "";
            sHanghoa = cls_Huyvietnam.getHanghoa(cls_ConfigCashier.sMenuID);
            string sYeucauthem = "";
            sYeucauthem = cls_Huyvietnam.getYeucauthem(cls_ConfigCashier.sStore_ID);
            string sBill = "";
           // sBill = cls_Huyvietnam.postBILL(cls_ConfigCashier.sStore_ID);

            splashScreenManager1.CloseWaitForm();

            XtraMessageBox.Show("Đồng bộ dữ liệu thành công" + "\n" + sNhomhang + sHanghoa +sYeucauthem + sBill, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnBill_Click(object sender, EventArgs e)
        {
            splashScreenManager1.ShowWaitForm();

            string sNhomhang = "";
           // sNhomhang = cls_Huyvietnam.getNhomhang(cls_ConfigCashier.sMenuID);
            string sHanghoa = "";
            //sHanghoa = cls_Huyvietnam.getHanghoa(cls_ConfigCashier.sMenuID);
            string sBill = "";
            sBill = cls_Huyvietnam.postBILL(cls_ConfigCashier.sStore_ID);

            splashScreenManager1.CloseWaitForm();

            XtraMessageBox.Show("Đồng bộ dữ liệu thành công" + "\n" + sNhomhang + sHanghoa + sBill, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnTacca_Click(object sender, EventArgs e)
        {
            splashScreenManager1.ShowWaitForm();

            string sNhomhang = "";
            sNhomhang = cls_Huyvietnam.getNhomhang(cls_ConfigCashier.sMenuID);
            string sHanghoa = "";
            sHanghoa = cls_Huyvietnam.getHanghoa(cls_ConfigCashier.sMenuID);
            string sYeucauthem = "";
            sYeucauthem = cls_Huyvietnam.getYeucauthem(cls_ConfigCashier.sStore_ID);
            string sBill = "";
            sBill = cls_Huyvietnam.postBILL(cls_ConfigCashier.sStore_ID);

            splashScreenManager1.CloseWaitForm();

            XtraMessageBox.Show("Đồng bộ dữ liệu thành công" + "\n" + sNhomhang + sHanghoa +sYeucauthem + sBill, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

        

       
    }
}