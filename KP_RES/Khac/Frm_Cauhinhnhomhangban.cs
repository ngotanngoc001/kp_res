﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Cauhinhnhomhangban : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Cauhinhnhomhangban()
        {
            InitializeComponent();
            LoadPermission();
            LoaddataGridView1();
        }

        private void LoadPermission()
        {
            btnLuu.Enabled = clsUserManagement.AllowAdd("13041501");
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView1();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void LoaddataGridView1()
        {
            string sSQL = "";
            sSQL += "SELECT CUAHANG.MA_CUAHANG, CUAHANG.TEN_CUAHANG, QUAY.MA_QUAY, QUAY.TEN_QUAY, NHOMHANG.MA_NHOMHANG, NHOMHANG.TEN_NHOMHANG, CAST(0 as bit)  AS SUDUNG" + "\n";
            sSQL += "FROM   CUAHANG CROSS JOIN  QUAY CROSS JOIN NHOMHANG" + "\n";
            DataTable dtFull = clsMain.ReturnDataTable(sSQL);

            DataTable dtDaluu = clsMain.ReturnDataTable("SELECT MAQUAY , MANHOM FROM CAUHINHKHUVUC WHERE MODE=2");

            foreach (DataRow dr in dtFull.Rows)
            {
                if (dtDaluu.Select("MAQUAY=" + clsMain.SQLString(dr["MA_QUAY"].ToString()) + "And MANHOM=" + clsMain.SQLString(dr["MA_NHOMHANG"].ToString())).Length > 0)
                {
                    dr["SUDUNG"] = 1;
                }
            }
            gridControl1.DataSource = dtFull;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            DataTable dt = ((DataView)gridView1.DataSource ).ToTable();

            string sSQL = "";
            sSQL +="Delete From CAUHINHKHUVUC Where MODE=2" + "\n";
            foreach (DataRow dr in dt.Rows)
            {
                if ((bool)dr["SUDUNG"])
                {
                    sSQL += "Insert into CAUHINHKHUVUC (MAQUAY,MANHOM,MODE)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLString(dr["MA_QUAY"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["MA_NHOMHANG"].ToString()) + ",";
                    sSQL += clsMain.SQLString("2") + ")"+ "\n";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoaddataGridView1();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                gridView1.SetRowCellValue(i, SUDUNG, chkAll.Checked);
            } 
        }
    }
}