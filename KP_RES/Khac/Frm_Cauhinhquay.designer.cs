﻿namespace KP_RES
{
    partial class Frm_Cauhinhquay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Cauhinhquay));
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btn_cauhinh = new DevExpress.XtraEditors.SimpleButton();
            this.look_CuaHang = new DevExpress.XtraEditors.LookUpEdit();
            this.btn_thoat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Reset = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.look_QuayHang = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.look_KhoHang = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.look_CuaHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_QuayHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_KhoHang.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(12, 50);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Cửa Hàng";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(12, 82);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(69, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Kho Hàng";
            // 
            // btn_cauhinh
            // 
            this.btn_cauhinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_cauhinh.Appearance.Options.UseFont = true;
            this.btn_cauhinh.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btn_cauhinh.Location = new System.Drawing.Point(100, 143);
            this.btn_cauhinh.Name = "btn_cauhinh";
            this.btn_cauhinh.Size = new System.Drawing.Size(101, 35);
            this.btn_cauhinh.TabIndex = 10;
            this.btn_cauhinh.Text = "&1.Đồng ý";
            this.btn_cauhinh.Click += new System.EventHandler(this.btn_cauhinh_Click);
            // 
            // look_CuaHang
            // 
            this.look_CuaHang.Location = new System.Drawing.Point(100, 47);
            this.look_CuaHang.Name = "look_CuaHang";
            this.look_CuaHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_CuaHang.Properties.Appearance.Options.UseFont = true;
            this.look_CuaHang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_CuaHang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.look_CuaHang.Properties.AutoSearchColumnIndex = 1;
            this.look_CuaHang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_CuaHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_CuaHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_CUAHANG", "Mã C.Hàng"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "Cửa Hàng")});
            this.look_CuaHang.Properties.DisplayMember = "TEN_CUAHANG";
            this.look_CuaHang.Properties.DropDownItemHeight = 40;
            this.look_CuaHang.Properties.NullText = "(Chọn cửa hàng)";
            this.look_CuaHang.Properties.ReadOnly = true;
            this.look_CuaHang.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_CuaHang.Properties.ShowHeader = false;
            this.look_CuaHang.Properties.ValueMember = "MA_CUAHANG";
            this.look_CuaHang.Size = new System.Drawing.Size(343, 26);
            this.look_CuaHang.TabIndex = 5;
            this.look_CuaHang.EditValueChanged += new System.EventHandler(this.look_CuaHang_EditValueChanged);
            // 
            // btn_thoat
            // 
            this.btn_thoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_thoat.Appearance.Options.UseFont = true;
            this.btn_thoat.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btn_thoat.Location = new System.Drawing.Point(355, 143);
            this.btn_thoat.Name = "btn_thoat";
            this.btn_thoat.Size = new System.Drawing.Size(88, 35);
            this.btn_thoat.TabIndex = 11;
            this.btn_thoat.Text = "&3.Đóng";
            this.btn_thoat.Click += new System.EventHandler(this.btn_thoat_Click);
            // 
            // btn_Reset
            // 
            this.btn_Reset.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_Reset.Appearance.Options.UseFont = true;
            this.btn_Reset.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btn_Reset.Location = new System.Drawing.Point(207, 143);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(142, 35);
            this.btn_Reset.TabIndex = 2;
            this.btn_Reset.Text = "&2.Thiết Lập Lại";
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(12, 114);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(79, 19);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Quầy Hàng";
            // 
            // look_QuayHang
            // 
            this.look_QuayHang.Location = new System.Drawing.Point(100, 111);
            this.look_QuayHang.Name = "look_QuayHang";
            this.look_QuayHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_QuayHang.Properties.Appearance.Options.UseFont = true;
            this.look_QuayHang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_QuayHang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.look_QuayHang.Properties.AutoSearchColumnIndex = 1;
            this.look_QuayHang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_QuayHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_QuayHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_QUAY", "Mã Quầy"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_QUAY", "Quầy Hàng")});
            this.look_QuayHang.Properties.DisplayMember = "TEN_QUAY";
            this.look_QuayHang.Properties.DropDownItemHeight = 40;
            this.look_QuayHang.Properties.NullText = "(Chọn quầy hàng)";
            this.look_QuayHang.Properties.ReadOnly = true;
            this.look_QuayHang.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_QuayHang.Properties.ShowHeader = false;
            this.look_QuayHang.Properties.ValueMember = "MA_QUAY";
            this.look_QuayHang.Size = new System.Drawing.Size(343, 26);
            this.look_QuayHang.TabIndex = 5;
            this.look_QuayHang.EditValueChanged += new System.EventHandler(this.look_QuayHang_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl6.Location = new System.Drawing.Point(178, 16);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(214, 14);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Thiết Lập Cấu Hình Quày Bán Hàng";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(452, 35);
            this.panel2.TabIndex = 14;
            // 
            // look_KhoHang
            // 
            this.look_KhoHang.Location = new System.Drawing.Point(100, 79);
            this.look_KhoHang.Name = "look_KhoHang";
            this.look_KhoHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_KhoHang.Properties.Appearance.Options.UseFont = true;
            this.look_KhoHang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.look_KhoHang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.look_KhoHang.Properties.AutoSearchColumnIndex = 1;
            this.look_KhoHang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.look_KhoHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.look_KhoHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_KHO", "Mã Kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_KHO", "Tên Kho")});
            this.look_KhoHang.Properties.DisplayMember = "TEN_QUAY";
            this.look_KhoHang.Properties.DropDownItemHeight = 40;
            this.look_KhoHang.Properties.NullText = "(Chọn kho hàng)";
            this.look_KhoHang.Properties.ReadOnly = true;
            this.look_KhoHang.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.look_KhoHang.Properties.ShowHeader = false;
            this.look_KhoHang.Properties.ValueMember = "MA_QUAY";
            this.look_KhoHang.Size = new System.Drawing.Size(343, 26);
            this.look_KhoHang.TabIndex = 15;
            this.look_KhoHang.EditValueChanged += new System.EventHandler(this.look_KhoHang_EditValueChanged);
            // 
            // Frm_Cauhinhquay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 185);
            this.ControlBox = false;
            this.Controls.Add(this.look_KhoHang);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.btn_thoat);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btn_cauhinh);
            this.Controls.Add(this.look_QuayHang);
            this.Controls.Add(this.look_CuaHang);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_Cauhinhquay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu Hình Quầy Bán";
            this.Load += new System.EventHandler(this.Frm_Cauhinhquay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.look_CuaHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_QuayHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.look_KhoHang.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btn_cauhinh;
        private DevExpress.XtraEditors.LookUpEdit look_CuaHang;
        private DevExpress.XtraEditors.SimpleButton btn_thoat;
        private DevExpress.XtraEditors.SimpleButton btn_Reset;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit look_QuayHang;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LookUpEdit look_KhoHang;

    }
}