﻿namespace KP_RES
{
    partial class Frm_Dongbo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDanhmuc = new DevExpress.XtraEditors.SimpleButton();
            this.btnBill = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnTacca = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            this.SuspendLayout();
            // 
            // btnDanhmuc
            // 
            this.btnDanhmuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnDanhmuc.Appearance.Options.UseFont = true;
            this.btnDanhmuc.Appearance.Options.UseTextOptions = true;
            this.btnDanhmuc.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDanhmuc.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnDanhmuc.Location = new System.Drawing.Point(12, 53);
            this.btnDanhmuc.Name = "btnDanhmuc";
            this.btnDanhmuc.Size = new System.Drawing.Size(125, 73);
            this.btnDanhmuc.TabIndex = 7;
            this.btnDanhmuc.Text = "&1.Đồng bộ danh mục";
            this.btnDanhmuc.Click += new System.EventHandler(this.btnDanhmuc_Click);
            // 
            // btnBill
            // 
            this.btnBill.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnBill.Appearance.Options.UseFont = true;
            this.btnBill.Appearance.Options.UseTextOptions = true;
            this.btnBill.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBill.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnBill.Location = new System.Drawing.Point(143, 53);
            this.btnBill.Name = "btnBill";
            this.btnBill.Size = new System.Drawing.Size(108, 73);
            this.btnBill.TabIndex = 8;
            this.btnBill.Text = "&2.Đồng bộ bill";
            this.btnBill.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(491, 35);
            this.panel2.TabIndex = 0;
            // 
            // btnTacca
            // 
            this.btnTacca.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnTacca.Appearance.Options.UseFont = true;
            this.btnTacca.Appearance.Options.UseTextOptions = true;
            this.btnTacca.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTacca.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnTacca.Location = new System.Drawing.Point(257, 53);
            this.btnTacca.Name = "btnTacca";
            this.btnTacca.Size = new System.Drawing.Size(115, 73);
            this.btnTacca.TabIndex = 9;
            this.btnTacca.Text = "&3.Đồng bộ tấc cả";
            this.btnTacca.Click += new System.EventHandler(this.btnTacca_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Appearance.Options.UseTextOptions = true;
            this.btnDong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDong.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnDong.Location = new System.Drawing.Point(378, 53);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(101, 73);
            this.btnDong.TabIndex = 10;
            this.btnDong.Text = "&4.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // Frm_Dongbo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 145);
            this.ControlBox = false;
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnTacca);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnBill);
            this.Controls.Add(this.btnDanhmuc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Dongbo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đồng bộ dữ liệu";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDanhmuc;
        private DevExpress.XtraEditors.SimpleButton btnBill;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnTacca;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}