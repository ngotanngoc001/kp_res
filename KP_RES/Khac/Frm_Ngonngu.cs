﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using KP_Terminal;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Ngonngu : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);


        public Frm_Ngonngu()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhngonngu", culture);
        }

        private void Frm_Nhapxuat_Paint(object sender, PaintEventArgs e)
        {
        }

        private void itTiengViet_ItemClick(object sender, TileItemEventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("MA_NN");
                dt.Rows.Add("vi-VN");
                ds.Tables.Add(dt);
                if (File.Exists(Application.StartupPath + "\\ngonngu.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds.WriteXml(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds.WriteXml(Application.StartupPath + "\\ngonngu.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
            }
            Application.Restart();
        }

        private void itTiengAnh_ItemClick(object sender, TileItemEventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("MA_NN");
                dt.Rows.Add("en-US");
                ds.Tables.Add(dt);
                if (File.Exists(Application.StartupPath + "\\ngonngu.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Normal;
                    ds.WriteXml(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    ds.WriteXml(Application.StartupPath + "\\ngonngu.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\ngonngu.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
            }
            Application.Restart();
        }
      
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Ngonngu_Load(object sender, EventArgs e)
        {
            btnDong.Text = rm.GetString("dong", culture);
            itTiengViet.Text = rm.GetString("tiengviet", culture);
            itTiengAnh.Text = rm.GetString("tienganh", culture);
        }
    }
}