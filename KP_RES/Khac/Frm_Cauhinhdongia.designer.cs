﻿namespace KP_RES 
{
    partial class Frm_Cauhinhdongia 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.optGIAMUA1 = new DevExpress.XtraEditors.CheckEdit();
            this.optGIAMUA = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.optGIABAN1 = new DevExpress.XtraEditors.CheckEdit();
            this.optGIABAN = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.optGIAMUA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIAMUA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIABAN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIABAN.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // optGIAMUA1
            // 
            this.optGIAMUA1.EnterMoveNextControl = true;
            this.optGIAMUA1.Location = new System.Drawing.Point(244, 73);
            this.optGIAMUA1.Name = "optGIAMUA1";
            this.optGIAMUA1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGIAMUA1.Properties.Appearance.Options.UseFont = true;
            this.optGIAMUA1.Properties.Caption = "Giá mua sau thuế";
            this.optGIAMUA1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optGIAMUA1.Properties.RadioGroupIndex = 1;
            this.optGIAMUA1.Size = new System.Drawing.Size(193, 24);
            this.optGIAMUA1.TabIndex = 6;
            this.optGIAMUA1.TabStop = false;
            // 
            // optGIAMUA
            // 
            this.optGIAMUA.EditValue = true;
            this.optGIAMUA.EnterMoveNextControl = true;
            this.optGIAMUA.Location = new System.Drawing.Point(45, 73);
            this.optGIAMUA.Name = "optGIAMUA";
            this.optGIAMUA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGIAMUA.Properties.Appearance.Options.UseFont = true;
            this.optGIAMUA.Properties.Caption = "Giá mua trước thuế";
            this.optGIAMUA.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optGIAMUA.Properties.RadioGroupIndex = 1;
            this.optGIAMUA.Size = new System.Drawing.Size(193, 24);
            this.optGIAMUA.TabIndex = 4;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(125, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Cấu hình giá mua";
            // 
            // optGIABAN1
            // 
            this.optGIABAN1.EnterMoveNextControl = true;
            this.optGIABAN1.Location = new System.Drawing.Point(244, 145);
            this.optGIABAN1.Name = "optGIABAN1";
            this.optGIABAN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGIABAN1.Properties.Appearance.Options.UseFont = true;
            this.optGIABAN1.Properties.Caption = "Giá bán sau thuế";
            this.optGIABAN1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optGIABAN1.Properties.RadioGroupIndex = 2;
            this.optGIABAN1.Size = new System.Drawing.Size(193, 24);
            this.optGIABAN1.TabIndex = 6;
            this.optGIABAN1.TabStop = false;
            // 
            // optGIABAN
            // 
            this.optGIABAN.EditValue = true;
            this.optGIABAN.EnterMoveNextControl = true;
            this.optGIABAN.Location = new System.Drawing.Point(45, 145);
            this.optGIABAN.Name = "optGIABAN";
            this.optGIABAN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGIABAN.Properties.Appearance.Options.UseFont = true;
            this.optGIABAN.Properties.Caption = "Giá bán trước thuế";
            this.optGIABAN.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optGIABAN.Properties.RadioGroupIndex = 2;
            this.optGIABAN.Size = new System.Drawing.Size(193, 24);
            this.optGIABAN.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(13, 116);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Cấu hình giá bán";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(356, 176);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(238, 176);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_Cauhinhdongia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 221);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.optGIAMUA1);
            this.Controls.Add(this.optGIABAN1);
            this.Controls.Add(this.optGIAMUA);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.optGIABAN);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhdongia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình đơn giá - thuế";
            this.Load += new System.EventHandler(this.Frm_Cauhinhdongia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.optGIAMUA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIAMUA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIABAN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optGIABAN.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit optGIAMUA;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit optGIAMUA1;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.CheckEdit optGIABAN1;
        private DevExpress.XtraEditors.CheckEdit optGIABAN;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel2;




    }
}