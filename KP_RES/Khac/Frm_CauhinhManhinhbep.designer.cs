﻿namespace KP_RES 
{
    partial class Frm_CauhinhManhinhbep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cboBEP = new DevExpress.XtraEditors.LookUpEdit();
            this.lblMucTG = new DevExpress.XtraEditors.LabelControl();
            this.txtMucTG2 = new DevExpress.XtraEditors.TextEdit();
            this.txtMucTG1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cboBEP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucTG2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucTG1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(468, 35);
            this.panel2.TabIndex = 0;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(300, 141);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 10;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(182, 141);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 9;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(13, 47);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Bếp";
            // 
            // cboBEP
            // 
            this.cboBEP.EnterMoveNextControl = true;
            this.cboBEP.Location = new System.Drawing.Point(131, 44);
            this.cboBEP.Name = "cboBEP";
            this.cboBEP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBEP.Properties.Appearance.Options.UseFont = true;
            this.cboBEP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBEP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBEP.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboBEP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBEP.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_BEP", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_BEP", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboBEP.Properties.DisplayMember = "TEN_BEP";
            this.cboBEP.Properties.DropDownItemHeight = 40;
            this.cboBEP.Properties.NullText = "";
            this.cboBEP.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboBEP.Properties.ShowHeader = false;
            this.cboBEP.Properties.ValueMember = "MA_BEP";
            this.cboBEP.Size = new System.Drawing.Size(279, 26);
            this.cboBEP.TabIndex = 2;
            // 
            // lblMucTG
            // 
            this.lblMucTG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMucTG.Location = new System.Drawing.Point(13, 79);
            this.lblMucTG.Margin = new System.Windows.Forms.Padding(4);
            this.lblMucTG.Name = "lblMucTG";
            this.lblMucTG.Size = new System.Drawing.Size(111, 19);
            this.lblMucTG.TabIndex = 3;
            this.lblMucTG.Text = "Thời gian chờ 1";
            // 
            // txtMucTG2
            // 
            this.txtMucTG2.EnterMoveNextControl = true;
            this.txtMucTG2.Location = new System.Drawing.Point(131, 108);
            this.txtMucTG2.Name = "txtMucTG2";
            this.txtMucTG2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucTG2.Properties.Appearance.Options.UseFont = true;
            this.txtMucTG2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMucTG2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMucTG2.Properties.MaxLength = 3;
            this.txtMucTG2.Size = new System.Drawing.Size(279, 26);
            this.txtMucTG2.TabIndex = 7;
            this.txtMucTG2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMucTG1_KeyPress);
            // 
            // txtMucTG1
            // 
            this.txtMucTG1.EnterMoveNextControl = true;
            this.txtMucTG1.Location = new System.Drawing.Point(131, 76);
            this.txtMucTG1.Name = "txtMucTG1";
            this.txtMucTG1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucTG1.Properties.Appearance.Options.UseFont = true;
            this.txtMucTG1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMucTG1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMucTG1.Properties.MaxLength = 3;
            this.txtMucTG1.Size = new System.Drawing.Size(279, 26);
            this.txtMucTG1.TabIndex = 4;
            this.txtMucTG1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMucTG1_KeyPress);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(13, 111);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(111, 19);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Thời gian chờ 2";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(417, 79);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 19);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "(Phút)";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(417, 111);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(44, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "(Phút)";
            // 
            // Frm_CauhinhManhinhbep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 183);
            this.ControlBox = false;
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lblMucTG);
            this.Controls.Add(this.txtMucTG2);
            this.Controls.Add(this.txtMucTG1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.cboBEP);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_CauhinhManhinhbep";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình màn hình bếp";
            ((System.ComponentModel.ISupportInitialize)(this.cboBEP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucTG2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMucTG1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit cboBEP;
        private DevExpress.XtraEditors.LabelControl lblMucTG;
        private DevExpress.XtraEditors.TextEdit txtMucTG2;
        private DevExpress.XtraEditors.TextEdit txtMucTG1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;




    }
}