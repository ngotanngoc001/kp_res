﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES
{
    public partial class Frm_Cauhinhbanggia : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Cauhinhbanggia()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhbanggia", culture);
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select TEN,GIATRI From CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC' OR TEN='THOIGIAN1' OR TEN='THOIGIAN2' OR TEN='THOIGIAN3'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TEN"].ToString() == "APDUNGBANGGIATHEOKHUVUC")
                {
                    if (dr["GIATRI"].ToString() == "0")
                    {
                        optAPDUNGBANGGIATHEOKHUVUC1.Checked = true;
                    }
                    else if (dr["GIATRI"].ToString() == "1")
                    {
                        optAPDUNGBANGGIATHEOKHUVUC.Checked = true;
                    }
                    else
                    {
                        optAPDUNGBANGGIATHEOKHUVUC2.Checked = true;
                    }
                }
                if (dr["TEN"].ToString() == "THOIGIAN1")
                {
                    dtp1.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
                }
                if (dr["TEN"].ToString() == "THOIGIAN2")
                {
                    dtp2.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
                }
                if (dr["TEN"].ToString() == "THOIGIAN3")
                {
                    dtp3.EditValue = DateTime.Parse(dr["GIATRI"].ToString());
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }

            String PriceList = "";

            if (optAPDUNGBANGGIATHEOKHUVUC.Checked == true) //áp dụng bảng giá theo khu vực
            {
                PriceList = "1";
            }
            else if (optAPDUNGBANGGIATHEOKHUVUC1.Checked == true) //áp dụng bảng giá mặc định
            {
                PriceList = "0";
            }
            else //áp dụng bảng giá theo cửa hàng
            {
                PriceList = "2";
            }


            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='APDUNGBANGGIATHEOKHUVUC'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("APDUNGBANGGIATHEOKHUVUC") + ",";
            sSQL += clsMain.SQLString(PriceList) + ",";
            sSQL += clsMain.SQLStringUnicode("Áp dụng bảng giá theo khu vực. 1 - Có áp dụng bảng giá theo khu vực. 0 - Không áp dụng,dùng bảng giá mặc định. 2 - Dùng bảng giá theo cửa hàng") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN1'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN1") + ",";
            sSQL += clsMain.SQLString(dtp1.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 1") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN2'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN2") + ",";
            sSQL += clsMain.SQLString(dtp2.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 2") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='THOIGIAN3'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("THOIGIAN3") + ",";
            sSQL += clsMain.SQLString(dtp3.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Dùng cho bảng giá theo khu vực-Mức thời gian 3") + ")" + "\n";

            sSQL += "Update THIETLAPBANGGIA" + "\n";
            sSQL += "Set THOIGIAN1=" + clsMain.SQLString(dtp1.Text) + "," + "\n";
            sSQL += "THOIGIAN2=" + clsMain.SQLString(dtp2.Text) + "," + "\n";
            sSQL += "THOIGIAN3=" + clsMain.SQLString(dtp3.Text) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckInput()
        {
            if ((DateTime)dtp1.EditValue < (DateTime)dtp2.EditValue && (DateTime)dtp2.EditValue < (DateTime)dtp3.EditValue)
            {
                return true;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_thoigiankodung + "\n" + _tg1tg2tg3, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

        }
        string _thongbao = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";
        string _thoigiankodung = "";
        string _tg1tg2tg3 = "";
        private void Frm_Cauhinhbanggia_Load(object sender, EventArgs e)
        {
            lblTEN.Text = rm.GetString("cauhinhbanggia", culture);
            optAPDUNGBANGGIATHEOKHUVUC1.Text = rm.GetString("dungbanggiamacdinh", culture);
            optAPDUNGBANGGIATHEOKHUVUC.Text = rm.GetString("dungbanggiatheokhuvuc", culture);
            optAPDUNGBANGGIATHEOKHUVUC2.Text = rm.GetString("dungbanggiatheocuahang", culture);
            labelControl6.Text = rm.GetString("thoigian1", culture);
            labelControl1.Text = rm.GetString("thoigian2", culture);
            labelControl2.Text = rm.GetString("thoigian3", culture);


            _thongbao = rm.GetString("thongbao", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _thoigiankodung = rm.GetString("thoigiankodung", culture);
            _tg1tg2tg3 = rm.GetString("tg1tg2tg3", culture);
        }
    }
}