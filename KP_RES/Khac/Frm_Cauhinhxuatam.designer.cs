﻿namespace KP_RES 
{
    partial class Frm_Cauhinhxuatam 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.optKHONGDUOCPHEP = new DevExpress.XtraEditors.CheckEdit();
            this.optDUOCPHEP = new DevExpress.XtraEditors.CheckEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.optKHONGDUOCPHEP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optDUOCPHEP.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // optKHONGDUOCPHEP
            // 
            this.optKHONGDUOCPHEP.EnterMoveNextControl = true;
            this.optKHONGDUOCPHEP.Location = new System.Drawing.Point(244, 73);
            this.optKHONGDUOCPHEP.Name = "optKHONGDUOCPHEP";
            this.optKHONGDUOCPHEP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optKHONGDUOCPHEP.Properties.Appearance.Options.UseFont = true;
            this.optKHONGDUOCPHEP.Properties.Caption = "Không được phép xuất âm";
            this.optKHONGDUOCPHEP.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optKHONGDUOCPHEP.Properties.RadioGroupIndex = 1;
            this.optKHONGDUOCPHEP.Size = new System.Drawing.Size(216, 24);
            this.optKHONGDUOCPHEP.TabIndex = 6;
            this.optKHONGDUOCPHEP.TabStop = false;
            // 
            // optDUOCPHEP
            // 
            this.optDUOCPHEP.EditValue = true;
            this.optDUOCPHEP.EnterMoveNextControl = true;
            this.optDUOCPHEP.Location = new System.Drawing.Point(45, 73);
            this.optDUOCPHEP.Name = "optDUOCPHEP";
            this.optDUOCPHEP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optDUOCPHEP.Properties.Appearance.Options.UseFont = true;
            this.optDUOCPHEP.Properties.Caption = "Được phép xuất âm";
            this.optDUOCPHEP.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optDUOCPHEP.Properties.RadioGroupIndex = 1;
            this.optDUOCPHEP.Size = new System.Drawing.Size(182, 24);
            this.optDUOCPHEP.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(356, 113);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(238, 113);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(62, 19);
            this.lblTEN.TabIndex = 8;
            this.lblTEN.Text = "Xuất kho";
            // 
            // Frm_Cauhinhxuatam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 165);
            this.ControlBox = false;
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.optKHONGDUOCPHEP);
            this.Controls.Add(this.optDUOCPHEP);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhxuatam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình xuất kho";
            this.Load += new System.EventHandler(this.Frm_Cauhinhxuatam_Load);
            ((System.ComponentModel.ISupportInitialize)(this.optKHONGDUOCPHEP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optDUOCPHEP.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit optDUOCPHEP;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit optKHONGDUOCPHEP;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl lblTEN;




    }
}