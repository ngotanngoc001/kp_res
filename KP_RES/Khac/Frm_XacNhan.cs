﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Resources;
using System.Globalization;

namespace KP_RES
{
    public partial class Frm_XacNhan : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_XacNhan()
        {
            InitializeComponent();
            this.Text = rm.GetString("matkhaubanhang", culture);
        }

        private bool checkPassCu()
        {
            bool kt = false;
            string sql = "select * from CAUHINH where TEN='Kingproconfig' and GIATRI ='"+txtMatKhauCu.Text +"'";
            DataTable dtPass = clsMain.ReturnDataTable(sql);
            if (dtPass.Rows.Count > 0)
            {
                return true;
            }
            else
                return kt;
        }

        private void btnChapNhan_Click(object sender, EventArgs e)
        {
            if (txtMatKhauCu.Text == "" || txtPassNew.Text == "" || txtReWritPassNew.Text == "")
            {
                XtraMessageBox.Show(_chuanhapdutt,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (checkPassCu() == false)
            {
                XtraMessageBox.Show(_matkhaucukcx, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (txtPassNew.Text != txtReWritPassNew.Text)
            {
                XtraMessageBox.Show(_matkhaumoikokhop, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string changePass = "Update CAUHINH set GIATRI='" + txtPassNew.Text + "' where TEN='Kingproconfig'";
            if (clsMain.ExecuteSQL(changePass))
            {
                XtraMessageBox.Show(_doimathanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMatKhauCu.Text = "";
                txtPassNew.Text = "";
                txtReWritPassNew.Text = "";
                btnThoat.Focus();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string _thongbao = "";
        string _chuanhapdutt = "";
        string _matkhaucukcx = "";
        string _matkhaumoikokhop = "";
        string _doimathanhcong = "";
        private void Frm_XacNhan_Load(object sender, EventArgs e)
        {
            labelControl1.Text = rm.GetString("maxacnhancu",culture);
            labelControl2.Text = rm.GetString("maxacnhanmoi", culture);
            labelControl3.Text = rm.GetString("nhaplaimaxacnhan",culture);
            btnChapNhan.Text = rm.GetString("dongy",culture);
            btnThoat.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _chuanhapdutt = rm.GetString("chuanhapdutt", culture);
            _matkhaucukcx = rm.GetString("matkhaucukcx", culture);
            _matkhaumoikokhop = rm.GetString("matkhaumoikokhop", culture);
            _doimathanhcong = rm.GetString("doimathanhcong", culture);
        }
    }
}