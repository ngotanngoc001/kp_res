﻿namespace KP_RES 
{
    partial class Frm_Giaodien 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.galleryControl1 = new DevExpress.XtraBars.Ribbon.GalleryControl();
            this.galleryControlClient1 = new DevExpress.XtraBars.Ribbon.GalleryControlClient();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMacdinh = new DevExpress.XtraEditors.SimpleButton();
            this.chkAuto = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryControl1)).BeginInit();
            this.galleryControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuto.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // galleryControl1
            // 
            this.galleryControl1.Controls.Add(this.galleryControlClient1);
            this.galleryControl1.DesignGalleryGroupIndex = 0;
            this.galleryControl1.DesignGalleryItemIndex = 0;
            this.galleryControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.galleryControl1.Location = new System.Drawing.Point(0, 35);
            this.galleryControl1.Name = "galleryControl1";
            this.galleryControl1.Size = new System.Drawing.Size(969, 536);
            this.galleryControl1.TabIndex = 2;
            this.galleryControl1.Text = "galleryControl1";
            // 
            // galleryControlClient1
            // 
            this.galleryControlClient1.GalleryControl = this.galleryControl1;
            this.galleryControlClient1.Location = new System.Drawing.Point(2, 2);
            this.galleryControlClient1.Size = new System.Drawing.Size(948, 532);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(858, 578);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_700x30;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(969, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnMacdinh
            // 
            this.btnMacdinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMacdinh.Appearance.Options.UseFont = true;
            this.btnMacdinh.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnMacdinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnMacdinh.Location = new System.Drawing.Point(740, 578);
            this.btnMacdinh.Margin = new System.Windows.Forms.Padding(4);
            this.btnMacdinh.Name = "btnMacdinh";
            this.btnMacdinh.Size = new System.Drawing.Size(110, 35);
            this.btnMacdinh.TabIndex = 8;
            this.btnMacdinh.Text = "&1.Mặc định";
            this.btnMacdinh.Click += new System.EventHandler(this.btnMacdinh_Click);
            // 
            // chkAuto
            // 
            this.chkAuto.Location = new System.Drawing.Point(581, 583);
            this.chkAuto.Name = "chkAuto";
            this.chkAuto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkAuto.Properties.Appearance.Options.UseFont = true;
            this.chkAuto.Properties.Caption = "Giao diện tự động";
            this.chkAuto.Size = new System.Drawing.Size(152, 24);
            this.chkAuto.TabIndex = 9;
            this.chkAuto.CheckedChanged += new System.EventHandler(this.chkAuto_CheckedChanged);
            // 
            // Frm_Giaodien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 617);
            this.ControlBox = false;
            this.Controls.Add(this.chkAuto);
            this.Controls.Add(this.btnMacdinh);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.galleryControl1);
            this.Controls.Add(this.panel2);
            this.Name = "Frm_Giaodien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Giao diện";
            this.Load += new System.EventHandler(this.Frm_Giaodien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.galleryControl1)).EndInit();
            this.galleryControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkAuto.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.GalleryControl galleryControl1;
        private DevExpress.XtraBars.Ribbon.GalleryControlClient galleryControlClient1;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnMacdinh;
        private DevExpress.XtraEditors.CheckEdit chkAuto;



    }
}