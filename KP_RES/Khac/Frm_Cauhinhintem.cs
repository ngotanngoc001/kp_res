﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Cauhinhintem : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);


        public Frm_Cauhinhintem()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhintem", culture);
            String sSQL = "SELECT * FROM CAUHINH WHERE TEN = 'PMIT_LABELMATRIX' AND GIATRI = 1";
            DataTable dtpmintem = clsMain.ReturnDataTable(sSQL);
            if (dtpmintem.Rows.Count > 0)
                ck_labelmatrix.Checked = true;
            else
                ck_bardrawer.Checked = true;

            DataSet myDS = new DataSet();
            DataTable myDT = new DataTable();
            if (File.Exists(Application.StartupPath + "\\MAVACH.xml"))
            {
                try
                {
                    myDS.ReadXml(Application.StartupPath + "\\MAVACH.xml");
                    myDT = myDS.Tables[0];
                    lblFileMaVach.Text = myDT.Rows[0]["FILE_MAVACH"].ToString();
                }
                catch
                {

                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            String sSQL = "";
            DataTable dtpmintem = clsMain.ReturnDataTable("SELECT * FROM CAUHINH WHERE TEN = 'PMIT_LABELMATRIX'");
            if (dtpmintem.Rows.Count > 0)
                sSQL = "UPDATE CAUHINH SET GIATRI = " + clsMain.SQLBit(ck_labelmatrix.Checked) + " WHERE TEN = 'PMIT_LABELMATRIX'";
            else
                sSQL = "INSERT INTO CAUHINH(TEN, GIATRI, GHICHU) VALUES ('PMIT_LABELMATRIX'," + clsMain.SQLBit(ck_labelmatrix.Checked) + ",N'Phần mềm in tem Label Matrix')";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                OpenFileDialog sOpenFile = new OpenFileDialog();
                sOpenFile.Filter = Filter();
                sOpenFile.FilterIndex = 1;
                sOpenFile.RestoreDirectory = true;
                sOpenFile.Multiselect = false;
                sOpenFile.Title = "Chọn file";
                if (sOpenFile.ShowDialog() == DialogResult.OK)
                {
                    String FileName = sOpenFile.FileName;

                    dt.Columns.Add("FILE_MAVACH");
                    dt.Rows.Add(sOpenFile.FileName);
                    ds.Tables.Add(dt);

                    if (File.Exists(Application.StartupPath + "\\MAVACH.xml"))
                    {
                        FileInfo file = new FileInfo(Application.StartupPath + "\\MAVACH.xml");
                        file.Attributes = FileAttributes.Normal;
                        ds.WriteXml(Application.StartupPath + "\\MAVACH.xml");
                        file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                    }
                    else
                    {
                        ds.WriteXml(Application.StartupPath + "\\MAVACH.xml");
                        FileInfo file = new FileInfo(Application.StartupPath + "\\MAVACH.xml");
                        file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                    }

                    lblFileMaVach.Text = sOpenFile.FileName;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        public static String Filter()
        {
            String Filter = "";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }
        string _thongbao = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";
        private void Frm_Cauhinhintem_Load(object sender, EventArgs e)
        {
            labelControl6.Text = rm.GetString("tuychonpmintem",culture);
            btnLuu.Text = rm.GetString("dongy", culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
_luuthanhcong = rm.GetString("luuthanhcong", culture);
_luukothanhcong = rm.GetString("luukothanhcong", culture);	
        }

    }
}