﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CauhinhKhuyenMaiUuTien : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhKhuyenMaiUuTien()
        {
            InitializeComponent();
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select GIATRI From CAUHINH where TEN='KHUYENMAIUUTIEN'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() == "1")
                    optTangHang.Checked = true;
                else
                    optGiamGia.Checked = true;
            }
            else
            {
                sSQL  = "";
                sSQL += "Insert Into CAUHINH(TEN,GIATRI,GHICHU)" + "\n";
                sSQL += "Values(";
                sSQL += "'KHUYENMAIUUTIEN'" + ",";
                sSQL += "'1'" + ",";
                sSQL += "N'Cấu hình khuyến mãi ưu tiên 1:Mua hàng tặng hàng, 2:Giảm giá theo nhóm hàng'" + ")";
                clsMain.ExecuteSQL(sSQL);
                LoadOption();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string kq = "";
            if(optTangHang.Checked)
                kq = "1";
            else if(optGiamGia.Checked)
                kq = "2";
            string sSQL = "UPDATE CAUHINH SET GIATRI=" + clsMain.SQLString(kq) + " WHERE TEN='KHUYENMAIUUTIEN'";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}