﻿namespace KP_RES 
{
    partial class Frm_CauhinhKhuvuc_Nhomhang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.cboCUAHANG = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.btnSaochep = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.pnlQuay = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlKhuvuc = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.pnlNhomhang = new System.Windows.Forms.FlowLayoutPanel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.chkALL_Nhomhang = new DevExpress.XtraEditors.CheckEdit();
            this.chkALL_Khuvuc = new DevExpress.XtraEditors.CheckEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkALL_Nhomhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkALL_Khuvuc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(2, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(68, 19);
            this.lblTEN.TabIndex = 8;
            this.lblTEN.Text = "Cửa hàng";
            // 
            // cboCUAHANG
            // 
            this.cboCUAHANG.EnterMoveNextControl = true;
            this.cboCUAHANG.Location = new System.Drawing.Point(77, 6);
            this.cboCUAHANG.Name = "cboCUAHANG";
            this.cboCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.cboCUAHANG.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCUAHANG.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCUAHANG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCUAHANG.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboCUAHANG.Properties.DisplayMember = "TEN";
            this.cboCUAHANG.Properties.DropDownItemHeight = 40;
            this.cboCUAHANG.Properties.NullText = "";
            this.cboCUAHANG.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCUAHANG.Properties.ShowHeader = false;
            this.cboCUAHANG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCUAHANG.Properties.ValueMember = "MA";
            this.cboCUAHANG.Size = new System.Drawing.Size(293, 26);
            this.cboCUAHANG.TabIndex = 10;
            this.cboCUAHANG.EditValueChanged += new System.EventHandler(this.cboCUAHANG_EditValueChanged);
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.lblTEN);
            this.panelControl17.Controls.Add(this.cboCUAHANG);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl17.Location = new System.Drawing.Point(0, 35);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(982, 35);
            this.panelControl17.TabIndex = 56;
            // 
            // btnSaochep
            // 
            this.btnSaochep.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaochep.Appearance.Options.UseFont = true;
            this.btnSaochep.Image = global::KP_RES.Properties.Resources.file_26;
            this.btnSaochep.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSaochep.Location = new System.Drawing.Point(612, 3);
            this.btnSaochep.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaochep.Name = "btnSaochep";
            this.btnSaochep.Size = new System.Drawing.Size(116, 35);
            this.btnSaochep.TabIndex = 11;
            this.btnSaochep.Text = "&1.Sao chép";
            this.btnSaochep.Click += new System.EventHandler(this.btnSaochep_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(736, 3);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(116, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&2.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(860, 3);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(116, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&3.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // pnlQuay
            // 
            this.pnlQuay.AutoScroll = true;
            this.pnlQuay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlQuay.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlQuay.Location = new System.Drawing.Point(2, 2);
            this.pnlQuay.Name = "pnlQuay";
            this.pnlQuay.Size = new System.Drawing.Size(978, 71);
            this.pnlQuay.TabIndex = 57;
            // 
            // pnlKhuvuc
            // 
            this.pnlKhuvuc.AutoScroll = true;
            this.pnlKhuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKhuvuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlKhuvuc.Location = new System.Drawing.Point(2, 24);
            this.pnlKhuvuc.Name = "pnlKhuvuc";
            this.pnlKhuvuc.Size = new System.Drawing.Size(334, 345);
            this.pnlKhuvuc.TabIndex = 58;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pnlQuay);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 70);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(982, 75);
            this.panelControl1.TabIndex = 57;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Controls.Add(this.groupControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 145);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(982, 375);
            this.panelControl2.TabIndex = 58;
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl2.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.groupControl2.Controls.Add(this.pnlNhomhang);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(340, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(640, 371);
            this.groupControl2.TabIndex = 60;
            this.groupControl2.Text = "Nhóm Hàng";
            // 
            // pnlNhomhang
            // 
            this.pnlNhomhang.AutoScroll = true;
            this.pnlNhomhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNhomhang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlNhomhang.Location = new System.Drawing.Point(2, 24);
            this.pnlNhomhang.Name = "pnlNhomhang";
            this.pnlNhomhang.Size = new System.Drawing.Size(636, 345);
            this.pnlNhomhang.TabIndex = 58;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.groupControl1.Controls.Add(this.pnlKhuvuc);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(338, 371);
            this.groupControl1.TabIndex = 59;
            this.groupControl1.Text = "Khu Vực";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.btnSaochep);
            this.panelControl3.Controls.Add(this.chkALL_Nhomhang);
            this.panelControl3.Controls.Add(this.chkALL_Khuvuc);
            this.panelControl3.Controls.Add(this.btnLuu);
            this.panelControl3.Controls.Add(this.btnDong);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(0, 520);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(982, 40);
            this.panelControl3.TabIndex = 57;
            // 
            // chkALL_Nhomhang
            // 
            this.chkALL_Nhomhang.Enabled = false;
            this.chkALL_Nhomhang.EnterMoveNextControl = true;
            this.chkALL_Nhomhang.Location = new System.Drawing.Point(338, 2);
            this.chkALL_Nhomhang.Name = "chkALL_Nhomhang";
            this.chkALL_Nhomhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkALL_Nhomhang.Properties.Appearance.Options.UseFont = true;
            this.chkALL_Nhomhang.Properties.Caption = "Chọn tất cả nhóm hàng";
            this.chkALL_Nhomhang.Size = new System.Drawing.Size(201, 24);
            this.chkALL_Nhomhang.TabIndex = 13;
            this.chkALL_Nhomhang.CheckedChanged += new System.EventHandler(this.chkALL_Nhomhang_CheckedChanged);
            // 
            // chkALL_Khuvuc
            // 
            this.chkALL_Khuvuc.Enabled = false;
            this.chkALL_Khuvuc.EnterMoveNextControl = true;
            this.chkALL_Khuvuc.Location = new System.Drawing.Point(2, 2);
            this.chkALL_Khuvuc.Name = "chkALL_Khuvuc";
            this.chkALL_Khuvuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkALL_Khuvuc.Properties.Appearance.Options.UseFont = true;
            this.chkALL_Khuvuc.Properties.Caption = "Chọn tất cả khu vực";
            this.chkALL_Khuvuc.Size = new System.Drawing.Size(180, 24);
            this.chkALL_Khuvuc.TabIndex = 12;
            this.chkALL_Khuvuc.CheckedChanged += new System.EventHandler(this.chkALL_Khuvuc_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_700x30;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(982, 35);
            this.panel2.TabIndex = 7;
            // 
            // Frm_CauhinhKhuvuc_Nhomhang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 560);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl17);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_CauhinhKhuvuc_Nhomhang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình Khu vực - Nhóm hàng bán";
            this.Load += new System.EventHandler(this.Frm_CauhinhKhuvuc_Nhomhang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            this.panelControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkALL_Nhomhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkALL_Khuvuc.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.LookUpEdit cboCUAHANG;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private System.Windows.Forms.FlowLayoutPanel pnlQuay;
        private System.Windows.Forms.FlowLayoutPanel pnlKhuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.FlowLayoutPanel pnlNhomhang;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.CheckEdit chkALL_Nhomhang;
        private DevExpress.XtraEditors.CheckEdit chkALL_Khuvuc;
        private DevExpress.XtraEditors.SimpleButton btnSaochep;




    }
}