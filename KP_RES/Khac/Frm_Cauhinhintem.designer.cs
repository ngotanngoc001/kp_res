﻿namespace KP_RES 
{
    partial class Frm_Cauhinhintem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.pn_phanmemintem = new DevExpress.XtraEditors.PanelControl();
            this.lblFileMaVach = new DevExpress.XtraEditors.LabelControl();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.ck_labelmatrix = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.ck_bardrawer = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_phanmemintem)).BeginInit();
            this.pn_phanmemintem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck_labelmatrix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_bardrawer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(456, 35);
            this.panel2.TabIndex = 0;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(333, 132);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 10;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(215, 132);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 9;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // pn_phanmemintem
            // 
            this.pn_phanmemintem.Controls.Add(this.lblFileMaVach);
            this.pn_phanmemintem.Controls.Add(this.btnFile);
            this.pn_phanmemintem.Controls.Add(this.ck_labelmatrix);
            this.pn_phanmemintem.Controls.Add(this.labelControl6);
            this.pn_phanmemintem.Controls.Add(this.ck_bardrawer);
            this.pn_phanmemintem.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_phanmemintem.Location = new System.Drawing.Point(0, 35);
            this.pn_phanmemintem.Name = "pn_phanmemintem";
            this.pn_phanmemintem.Size = new System.Drawing.Size(456, 90);
            this.pn_phanmemintem.TabIndex = 16;
            // 
            // lblFileMaVach
            // 
            this.lblFileMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileMaVach.Location = new System.Drawing.Point(12, 57);
            this.lblFileMaVach.Name = "lblFileMaVach";
            this.lblFileMaVach.Size = new System.Drawing.Size(0, 14);
            this.lblFileMaVach.TabIndex = 39;
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.Location = new System.Drawing.Point(354, 52);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(87, 27);
            this.btnFile.TabIndex = 38;
            this.btnFile.Text = "Browser";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // ck_labelmatrix
            // 
            this.ck_labelmatrix.EditValue = true;
            this.ck_labelmatrix.Location = new System.Drawing.Point(44, 26);
            this.ck_labelmatrix.Name = "ck_labelmatrix";
            this.ck_labelmatrix.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.ck_labelmatrix.Properties.Appearance.Options.UseFont = true;
            this.ck_labelmatrix.Properties.Caption = "Label Matrix";
            this.ck_labelmatrix.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_labelmatrix.Properties.RadioGroupIndex = 4;
            this.ck_labelmatrix.Size = new System.Drawing.Size(179, 19);
            this.ck_labelmatrix.TabIndex = 37;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl6.Location = new System.Drawing.Point(12, 7);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(227, 14);
            this.labelControl6.TabIndex = 1;
            this.labelControl6.Text = "Tùy Chọn Phần Mềm In Tem Mã Vạch";
            // 
            // ck_bardrawer
            // 
            this.ck_bardrawer.Location = new System.Drawing.Point(262, 27);
            this.ck_bardrawer.Name = "ck_bardrawer";
            this.ck_bardrawer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.ck_bardrawer.Properties.Appearance.Options.UseFont = true;
            this.ck_bardrawer.Properties.Caption = "BarDrawer";
            this.ck_bardrawer.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_bardrawer.Properties.RadioGroupIndex = 4;
            this.ck_bardrawer.Size = new System.Drawing.Size(91, 19);
            this.ck_bardrawer.TabIndex = 36;
            this.ck_bardrawer.TabStop = false;
            // 
            // Frm_Cauhinhintem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 173);
            this.ControlBox = false;
            this.Controls.Add(this.pn_phanmemintem);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhintem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình in tem";
            this.Load += new System.EventHandler(this.Frm_Cauhinhintem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pn_phanmemintem)).EndInit();
            this.pn_phanmemintem.ResumeLayout(false);
            this.pn_phanmemintem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck_labelmatrix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_bardrawer.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.PanelControl pn_phanmemintem;
        private DevExpress.XtraEditors.LabelControl lblFileMaVach;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraEditors.CheckEdit ck_labelmatrix;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit ck_bardrawer;




    }
}