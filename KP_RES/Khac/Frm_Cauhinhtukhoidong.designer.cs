﻿namespace KP_RES 
{
    partial class Frm_Cauhinhtukhoidong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.optKHTT = new DevExpress.XtraEditors.CheckEdit();
            this.optShowTime = new DevExpress.XtraEditors.CheckEdit();
            this.optMenu = new DevExpress.XtraEditors.CheckEdit();
            this.optManhinhbep = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.optKHTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optShowTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optManhinhbep.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(217, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Các chương trình tự khởi động";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(356, 192);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(238, 192);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // optKHTT
            // 
            this.optKHTT.Location = new System.Drawing.Point(47, 103);
            this.optKHTT.Name = "optKHTT";
            this.optKHTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.optKHTT.Properties.Appearance.Options.UseFont = true;
            this.optKHTT.Properties.Caption = "Tra cứu khách hàng thân thiết";
            this.optKHTT.Size = new System.Drawing.Size(303, 24);
            this.optKHTT.TabIndex = 8;
            // 
            // optShowTime
            // 
            this.optShowTime.Location = new System.Drawing.Point(47, 133);
            this.optShowTime.Name = "optShowTime";
            this.optShowTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.optShowTime.Properties.Appearance.Options.UseFont = true;
            this.optShowTime.Properties.Caption = "ShowTime";
            this.optShowTime.Size = new System.Drawing.Size(303, 24);
            this.optShowTime.TabIndex = 9;
            // 
            // optMenu
            // 
            this.optMenu.Location = new System.Drawing.Point(47, 73);
            this.optMenu.Name = "optMenu";
            this.optMenu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.optMenu.Properties.Appearance.Options.UseFont = true;
            this.optMenu.Properties.Caption = "Menu điện tử";
            this.optMenu.Size = new System.Drawing.Size(303, 24);
            this.optMenu.TabIndex = 10;
            // 
            // optManhinhbep
            // 
            this.optManhinhbep.Location = new System.Drawing.Point(47, 163);
            this.optManhinhbep.Name = "optManhinhbep";
            this.optManhinhbep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.optManhinhbep.Properties.Appearance.Options.UseFont = true;
            this.optManhinhbep.Properties.Caption = "Màn hình bếp";
            this.optManhinhbep.Size = new System.Drawing.Size(303, 24);
            this.optManhinhbep.TabIndex = 11;
            // 
            // Frm_Cauhinhtukhoidong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 235);
            this.ControlBox = false;
            this.Controls.Add(this.optManhinhbep);
            this.Controls.Add(this.optMenu);
            this.Controls.Add(this.optShowTime);
            this.Controls.Add(this.optKHTT);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhtukhoidong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình tự khởi động";
            ((System.ComponentModel.ISupportInitialize)(this.optKHTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optShowTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optManhinhbep.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.CheckEdit optKHTT;
        private DevExpress.XtraEditors.CheckEdit optShowTime;
        private DevExpress.XtraEditors.CheckEdit optMenu;
        private DevExpress.XtraEditors.CheckEdit optManhinhbep;




    }
}