﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_ConfigFuntion : DevExpress.XtraEditors.XtraForm
    {
        public bool kt = false;
        public static TextEdit _textbox;

        public Frm_ConfigFuntion()
        {
            InitializeComponent();
        }

       

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            _textbox.Text = "";
            _textbox.Focus();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {

            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                if (_textbox.SelectionLength < 1)
                {
                    _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                    _textbox.Focus();
                    _textbox.SelectionStart = select - 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    int length = _textbox.SelectionLength;
                    _textbox.Text = _textbox.Text.Remove(select, length);
                    _textbox.Focus();
                    _textbox.SelectionStart = select;
                    _textbox.SelectionLength = 0;
                }
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = 0;
                _textbox.SelectionLength = 0;
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                _textbox.Focus();
                _textbox.SelectionStart = select - 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = _textbox.Text.Length;
                _textbox.SelectionLength = 0;
            }
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{Enter}");
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            Click_buttonNumber(btn.Text );
        }

        private void txtConfig_Click(object sender, EventArgs e)
        {
            SetTextbox(txtConfig);
        }

        private void txtConfig_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtConfig.Text == "")
                {
                    XtraMessageBox.Show(_chuadienttxacnhan, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string sql = "Select TEN From CAUHINH Where TEN='Kingproconfig' And GIATRI=" + clsMain.SQLString(txtConfig.Text);
                DataTable dt = clsMain.ReturnDataTable(sql);
                if (dt.Rows.Count > 0)
                    kt = true;
                if (kt == true)
                    this.Close();
                else
                    XtraMessageBox.Show(_maxnkcx, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void SetTextbox(TextEdit edit)
        {
            _textbox = edit;
        }

        private void Click_buttonNumber(string number)
        {
            int select = _textbox.SelectionStart;
            if (_textbox.SelectionLength < 1)
            {
                _textbox.Text = _textbox.Text.Insert(_textbox.SelectionStart, number);
                _textbox.Focus();
                _textbox.SelectionStart = select + 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                int length = _textbox.SelectionLength;
                _textbox.Text = _textbox.Text.Remove(select, length);
                _textbox.Text = _textbox.Text.Insert(select, number);
                _textbox.Focus();
                _textbox.SelectionStart = select + 1;
                _textbox.SelectionLength = 0;
            }
        }

        string _xacnhan = "";
        string _thongbao = "";
        string _chuadienttxacnhan = "";
        string _maxnkcx = "";
        private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            SetTextbox(txtConfig);
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            _maxnkcx = rm.GetString("maxnkcx", culture);
            _chuadienttxacnhan = rm.GetString("chuadienttxacnhan", culture);
            _xacnhan = rm.GetString("xacnhan", culture);
            _thongbao = rm.GetString("thongbao", culture);
            lb_matkhau.Text = rm.GetString("matkhauxacnhan", culture);
            btn_00.Text = rm.GetString("thoat", culture);
            this.Text = _xacnhan;
        }
    }
}