﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
using KP_UserManagement;
using System.Runtime.InteropServices;
using System.Net;

namespace KP_RES
{
    public partial class Frm_Trogiup : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Trogiup()
        {
            InitializeComponent();
        }

        private void Frm_Lienhe_Load(object sender, EventArgs e)
        {
            string sSQL = "SELECT GIATRI FROM CAUHINH WHERE TEN='DAILY'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            string URL = "" + dt.Rows[0]["GIATRI"] + "";
            this.webBrowser1.Navigate(URL);
            
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if (RemoteFileExists(txtDiachi.Text))
            {
                this.webBrowser1.Navigate(txtDiachi.Text);
            }
            else
            {
                this.webBrowser1.Navigate("http://www.google.com.vn/search?q=" +txtDiachi.Text);
            }
            txtDiachi.Text = "";
        }

        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
    }
}