﻿namespace KP_RES
{
    partial class Frm_ConfigFuntion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ConfigFuntion));
            this.txtConfig = new DevExpress.XtraEditors.TextEdit();
            this.lb_matkhau = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Payments = new DevExpress.XtraEditors.SimpleButton();
            this.btn_00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_back = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtConfig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtConfig
            // 
            this.txtConfig.Location = new System.Drawing.Point(12, 43);
            this.txtConfig.Name = "txtConfig";
            this.txtConfig.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtConfig.Properties.Appearance.Options.UseFont = true;
            this.txtConfig.Properties.PasswordChar = '*';
            this.txtConfig.Properties.UseSystemPasswordChar = true;
            this.txtConfig.Size = new System.Drawing.Size(368, 30);
            this.txtConfig.TabIndex = 1;
            this.txtConfig.Click += new System.EventHandler(this.txtConfig_Click);
            this.txtConfig.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConfig_KeyDown);
            // 
            // lb_matkhau
            // 
            this.lb_matkhau.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_matkhau.Location = new System.Drawing.Point(12, 24);
            this.lb_matkhau.Name = "lb_matkhau";
            this.lb_matkhau.Size = new System.Drawing.Size(107, 13);
            this.lb_matkhau.TabIndex = 3;
            this.lb_matkhau.Text = "Mật Khẩu Xác Nhận";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnBanphim);
            this.panelControl1.Controls.Add(this.btn_Payments);
            this.panelControl1.Controls.Add(this.btn_00);
            this.panelControl1.Controls.Add(this.btn_0);
            this.panelControl1.Controls.Add(this.btn_delete);
            this.panelControl1.Controls.Add(this.btn_6);
            this.panelControl1.Controls.Add(this.btn_9);
            this.panelControl1.Controls.Add(this.btn_8);
            this.panelControl1.Controls.Add(this.btn_7);
            this.panelControl1.Controls.Add(this.btn_back);
            this.panelControl1.Controls.Add(this.btn_5);
            this.panelControl1.Controls.Add(this.btn_4);
            this.panelControl1.Controls.Add(this.btn_3);
            this.panelControl1.Controls.Add(this.btn_2);
            this.panelControl1.Controls.Add(this.btn_1);
            this.panelControl1.Controls.Add(this.btn_ESC);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 94);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(396, 280);
            this.panelControl1.TabIndex = 4;
            // 
            // btnBanphim
            // 
            this.btnBanphim.AllowFocus = false;
            this.btnBanphim.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(243, 79);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(58, 55);
            this.btnBanphim.TabIndex = 66;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btn_Payments
            // 
            this.btn_Payments.AllowFocus = false;
            this.btn_Payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Payments.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payments.Appearance.Options.UseFont = true;
            this.btn_Payments.Location = new System.Drawing.Point(243, 149);
            this.btn_Payments.Name = "btn_Payments";
            this.btn_Payments.Size = new System.Drawing.Size(137, 125);
            this.btn_Payments.TabIndex = 65;
            this.btn_Payments.Text = "ENTER";
            this.btn_Payments.Click += new System.EventHandler(this.btn_Payments_Click);
            // 
            // btn_00
            // 
            this.btn_00.AllowFocus = false;
            this.btn_00.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_00.Appearance.Options.UseFont = true;
            this.btn_00.Location = new System.Drawing.Point(89, 219);
            this.btn_00.Name = "btn_00";
            this.btn_00.Size = new System.Drawing.Size(135, 55);
            this.btn_00.TabIndex = 62;
            this.btn_00.Text = "Thoát";
            this.btn_00.Click += new System.EventHandler(this.btn_00_Click);
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Location = new System.Drawing.Point(12, 219);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(58, 55);
            this.btn_0.TabIndex = 60;
            this.btn_0.Text = "0";
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Location = new System.Drawing.Point(322, 9);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(58, 55);
            this.btn_delete.TabIndex = 61;
            this.btn_delete.Text = "Del";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Location = new System.Drawing.Point(166, 79);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(58, 55);
            this.btn_6.TabIndex = 56;
            this.btn_6.Text = "6";
            this.btn_6.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Location = new System.Drawing.Point(166, 9);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(58, 55);
            this.btn_9.TabIndex = 59;
            this.btn_9.Text = "9";
            this.btn_9.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Location = new System.Drawing.Point(89, 9);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(58, 55);
            this.btn_8.TabIndex = 58;
            this.btn_8.Text = "8";
            this.btn_8.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Location = new System.Drawing.Point(12, 9);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(58, 55);
            this.btn_7.TabIndex = 57;
            this.btn_7.Text = "7";
            this.btn_7.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_back
            // 
            this.btn_back.AllowFocus = false;
            this.btn_back.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.Appearance.Options.UseFont = true;
            this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
            this.btn_back.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_back.Location = new System.Drawing.Point(322, 79);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(58, 55);
            this.btn_back.TabIndex = 55;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Location = new System.Drawing.Point(89, 79);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(58, 55);
            this.btn_5.TabIndex = 54;
            this.btn_5.Text = "5";
            this.btn_5.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Location = new System.Drawing.Point(12, 79);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(58, 55);
            this.btn_4.TabIndex = 53;
            this.btn_4.Text = "4";
            this.btn_4.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Location = new System.Drawing.Point(166, 149);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(58, 55);
            this.btn_3.TabIndex = 52;
            this.btn_3.Text = "3";
            this.btn_3.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Location = new System.Drawing.Point(89, 149);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(58, 55);
            this.btn_2.TabIndex = 51;
            this.btn_2.Text = "2";
            this.btn_2.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_1
            // 
            this.btn_1.AllowFocus = false;
            this.btn_1.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btn_1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.Options.UseBorderColor = true;
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Location = new System.Drawing.Point(12, 149);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(58, 55);
            this.btn_1.TabIndex = 50;
            this.btn_1.Text = "1";
            this.btn_1.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Location = new System.Drawing.Point(243, 9);
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(58, 55);
            this.btn_ESC.TabIndex = 49;
            this.btn_ESC.Text = "ESC";
            this.btn_ESC.Click += new System.EventHandler(this.btn_ESC_Click);
            // 
            // Frm_ConfigFuntion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 374);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.lb_matkhau);
            this.Controls.Add(this.txtConfig);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ConfigFuntion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xác nhận";
            this.Load += new System.EventHandler(this.Frm_ConfigFuntion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtConfig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtConfig;
        private DevExpress.XtraEditors.LabelControl lb_matkhau;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_Payments;
        private DevExpress.XtraEditors.SimpleButton btn_00;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btn_back;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
    }
}