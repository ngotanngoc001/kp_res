﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_CauhinhKhuvuc_Nhomhang : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);


        public Frm_CauhinhKhuvuc_Nhomhang()
        {
            InitializeComponent();
            this.Text = rm.GetString("cauhinhkhuvuc-nhomhang", culture);
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "";
            sSQL += "Select MA_CUAHANG As MA,TEN_CUAHANG As TEN" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by  TEN_CUAHANG" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboCUAHANG.Properties.DataSource = dt;
            cboCUAHANG.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());

            LoadQuay(dt.Rows[0]["MA"].ToString());
            LoadKhuVuc(dt.Rows[0]["MA"].ToString());
            LoadNhomHang();
        }

        private void LoadQuay(string macuahang)
        {
            string sSQL = "";
            sSQL += "Select MA_QUAY As MA,TEN_QUAY As TEN,A.GHICHU,A.SUDUNG" + "\n";
            sSQL += "From QUAY A,KHO B" + "\n";
            sSQL += "Where A.MA_KHO=B.MA_KHO AND A.SUDUNG=1" + "\n";
            sSQL += "AND B.MA_CUAHANG=" + clsMain.SQLString(macuahang) + "\n";
            sSQL += "Order by A.SUDUNG DESC,TEN_QUAY" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            pnlQuay.Controls.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                Panel pal_Group = new Panel();
                Panel pal_bnt = new Panel();
                Panel pal_chk = new Panel();
                CheckEdit chk = new CheckEdit();
                SimpleButton btn = new SimpleButton();

                btn.Font = pnlQuay.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.card_in_use_26;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();

                chk.Name = "chk" + dr["MA"].ToString();
                chk.Text = dr["TEN"].ToString();
                chk.Properties.ReadOnly = true;

                pal_Group.Name = "G" + dr["MA"].ToString();
                pal_bnt.Name = "B" + dr["MA"].ToString();
                pal_chk.Name = "C" + dr["MA"].ToString();

                pal_Group.Width = 145;
                pal_Group.Height = 60;

                pal_chk.Width = 20;

                pal_chk.Dock = DockStyle.Right;
                pal_bnt.Dock = DockStyle.Fill;
                btn.Dock = DockStyle.Fill;
                chk.Dock = DockStyle.Fill;

                btn.Click += new EventHandler(Quay_Click);

                pal_chk.Controls.Add(chk);
                pal_bnt.Controls.Add(btn);
                pal_Group.Controls.Add(pal_bnt);
                pal_Group.Controls.Add(pal_chk);

                pnlQuay.Controls.Add(pal_Group);
            }
        }


        private void LoadKhuVuc(string macuahang)
        {
            string sSQL = "";
            sSQL += "Select MA_KHUVUC As MA,TEN_KHUVUC As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From KHUVUC" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "AND MA_CUAHANG=" + clsMain.SQLString(macuahang) + "\n";
            sSQL += "Order by SUDUNG DESC,TEN_KHUVUC" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            pnlKhuvuc.Controls.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                Panel pal_kv_Group = new Panel();
                Panel pal_kv_bnt = new Panel();
                Panel pal_kv_chk = new Panel();

                CheckEdit chk = new CheckEdit();
                SimpleButton btn = new SimpleButton();

                btn.Font = pnlKhuvuc.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.control_panel_26;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Khuvuc_Click);

                chk.Name = "chk" + dr["MA"].ToString();
                chk.Text = dr["TEN"].ToString();
                chk.Properties.ReadOnly = true;

                pal_kv_Group.Name = "G" + dr["MA"].ToString();
                pal_kv_bnt.Name = "B" + dr["MA"].ToString();
                pal_kv_chk.Name = "C" + dr["MA"].ToString();

                pal_kv_Group.Width = 145;
                pal_kv_Group.Height = 60;

                pal_kv_chk.Width = 20;

                pal_kv_chk.Dock = DockStyle.Right;
                pal_kv_bnt.Dock = DockStyle.Fill;
                btn.Dock = DockStyle.Fill;
                chk.Dock = DockStyle.Fill;

                pal_kv_chk.Controls.Add(chk);
                pal_kv_bnt.Controls.Add(btn);
                pal_kv_Group.Controls.Add(pal_kv_bnt);
                pal_kv_Group.Controls.Add(pal_kv_chk);

                pnlKhuvuc.Controls.Add(pal_kv_Group);
            }
        }

        private void LoadNhomHang()
        {
            string sSQL = "";
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by SUDUNG DESC,TEN_NHOMHANG" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            pnlNhomhang.Controls.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                Panel pal_nh_Group = new Panel();
                Panel pal_nh_bnt = new Panel();
                Panel pal_nh_chk = new Panel();

                CheckEdit chk = new CheckEdit();

                SimpleButton btn = new SimpleButton();
                btn.Font = pnlNhomhang.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Nhomhang_Click);

                chk.Name = "chk" + dr["MA"].ToString();
                chk.Text = dr["TEN"].ToString();
                chk.Properties.ReadOnly = true;

                pal_nh_Group.Name = "G" + dr["MA"].ToString();
                pal_nh_bnt.Name = "B" + dr["MA"].ToString();
                pal_nh_chk.Name = "C" + dr["MA"].ToString();

                pal_nh_Group.Width = 145;
                pal_nh_Group.Height = 60;

                pal_nh_chk.Width = 20;

                pal_nh_chk.Dock = DockStyle.Right;
                pal_nh_bnt.Dock = DockStyle.Fill;
                btn.Dock = DockStyle.Fill;
                chk.Dock = DockStyle.Fill;

                pal_nh_chk.Controls.Add(chk);
                pal_nh_bnt.Controls.Add(btn);
                pal_nh_Group.Controls.Add(pal_nh_bnt);
                pal_nh_Group.Controls.Add(pal_nh_chk);

                pnlNhomhang.Controls.Add(pal_nh_Group);
            }
        }

        private void Quay_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            SetBackgroudOnClick(btn);
            foreach (Panel pn1 in pnlQuay.Controls)
            {
                if (pn1.Name == "G" + btn.Name)
                {
                    foreach (Panel pn2 in pn1.Controls)
                    {
                        if (pn2.Name == "C" + btn.Name)
                        {
                            foreach (CheckEdit chk in pn2.Controls)
                            {
                                chk.Checked = true;
                            }
                        }
                    }
                }
                else
                {
                    foreach (Panel pn2 in pn1.Controls)
                    {
                        if (pn2.Name.Substring(0, 1) == "C" && pn2.Name.Substring(1) != btn.Name)
                        {
                            foreach (CheckEdit chk in pn2.Controls)
                            {
                                chk.Checked = false;
                            }
                        }

                        if (pn2.Name.Substring(0, 1) == "B" && pn2.Name.Substring(1) != btn.Name)
                        {
                            foreach (SimpleButton btn1 in pn2.Controls)
                            {
                                SetDefautlBackgroud(btn1);
                            }
                        }
                    }
                }
            }

            string sSQL = "";
            if (btnSaochep.Appearance.BackColor == btnLuu.Appearance.BackColor)
            {
                Set_Defautl(pnlKhuvuc,true);

                sSQL += "Select MANHOM as MA" + "\n";
                sSQL += "From CAUHINHKHUVUC" + "\n";
                sSQL += "Where MODE=1" + "\n";
                sSQL += "AND MAQUAY=" + clsMain.SQLString(btn.Name);
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    int n = dt.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        Set_OnClick(pnlKhuvuc, dt.Rows[i]["MA"].ToString());
                    }
                }

                Set_Defautl(pnlNhomhang,true);

                sSQL = "";
                sSQL += "Select MANHOM as MA" + "\n";
                sSQL += "From CAUHINHKHUVUC" + "\n";
                sSQL += "Where MODE=2" + "\n";
                sSQL += "AND MAQUAY=" + clsMain.SQLString(btn.Name);
                dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    int n = dt.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        Set_OnClick(pnlNhomhang, dt.Rows[i]["MA"].ToString());
                    }
                }

                chkALL_Khuvuc.Enabled = true;
                chkALL_Nhomhang.Enabled = true;
            }
            else
            {
                if (btn.Name == Ma_quay_coppy)
                {
                    return;
                }
                else
                {
                    if (DialogResult.Yes == (XtraMessageBox.Show("Thực hiện sao chép từ quầy " + Ten_quay_coppy + " sang quầy " + btn.Text + " ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                    {
                        sSQL += "Select MANHOM,MODE" + "\n";
                        sSQL += "From CAUHINHKHUVUC" + "\n";
                        sSQL += "Where MAQUAY=" + clsMain.SQLString(Ma_quay_coppy);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);

                        if (dt.Rows.Count > 0)
                        {
                            int n = dt.Rows.Count;

                            sSQL = "";
                            sSQL += "Delete CAUHINHKHUVUC Where MAQUAY=" + clsMain.SQLString(btn.Name) + "\n";
                            for (int i = 0; i < n; i++)
                            {
                                sSQL += "Insert into CAUHINHKHUVUC(MAQUAY,MANHOM,MODE)" + "\n";
                                sSQL += "Values(";
                                sSQL += clsMain.SQLString(btn.Name) + ",";
                                sSQL += clsMain.SQLString(dt.Rows[i]["MANHOM"].ToString()) + ",";
                                sSQL += clsMain.SQLString(dt.Rows[i]["MODE"].ToString()) + ")" + "\n";
                            }

                            if (clsMain.ExecuteSQL(sSQL))
                            {
                                SetDefautlBackgroud(btnSaochep);
                                XtraMessageBox.Show("Sao chép thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                SetDefautlBackgroud(btnSaochep);
                                XtraMessageBox.Show("Sao chép thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            SetDefautlBackgroud(btnSaochep);
                            XtraMessageBox.Show("Quầy " + Ma_quay_coppy + " chưa có dữ liệu cấu hình", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnSaochep);
                        Set_Defautl(pnlKhuvuc,true);

                        sSQL += "Select MANHOM as MA" + "\n";
                        sSQL += "From CAUHINHKHUVUC" + "\n";
                        sSQL += "Where MODE=1" + "\n";
                        sSQL += "AND MAQUAY=" + clsMain.SQLString(btn.Name);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int n = dt.Rows.Count;
                            for (int i = 0; i < n; i++)
                            {
                                Set_OnClick(pnlKhuvuc, dt.Rows[i]["MA"].ToString());
                            }
                            //foreach (SimpleButton btn1 in pnlKhuvuc.Controls)
                            //{
                            //    for (int i = 0; i < n; i++)
                            //    {
                            //        if (btn1.Name == dt.Rows[i]["MA"].ToString())
                            //        {
                            //            SetBackgroudOnClick(btn1);
                            //        }
                            //    }
                            //}
                        }
                        else
                        {
                            Set_Defautl(pnlKhuvuc,true);
                        }

                        Set_Defautl(pnlNhomhang,true);

                        sSQL = "";
                        sSQL += "Select MANHOM as MA" + "\n";
                        sSQL += "From CAUHINHKHUVUC" + "\n";
                        sSQL += "Where MODE=2" + "\n";
                        sSQL += "AND MAQUAY=" + clsMain.SQLString(btn.Name);
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int n = dt.Rows.Count;
                            for (int i = 0; i < n; i++)
                            {
                                Set_OnClick(pnlNhomhang, dt.Rows[i]["MA"].ToString());
                            }
                        }
                        else
                        {
                            Set_Defautl(pnlNhomhang,true);
                        }

                        chkALL_Khuvuc.Enabled = true;
                        chkALL_Nhomhang.Enabled = true;
                    }
                }
            }
        }

        private void Khuvuc_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            foreach (Panel pn in pnlKhuvuc.Controls)
            {
                if (pn.Name == "G" + btn.Name)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn1.Controls)
                            {
                                if (btn.Appearance.BackColor == btnLuu.Appearance.BackColor)
                                {
                                    chk1.Checked = true;
                                    SetBackgroudOnClick(btn);
                                }
                                else
                                {
                                    chk1.Checked = false;
                                    SetDefautlBackgroud(btn);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Nhomhang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            foreach (Panel pn in pnlNhomhang.Controls)
            {
                if (pn.Name == "G" + btn.Name)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn1.Controls)
                            {
                                if (btn.Appearance.BackColor == btnLuu.Appearance.BackColor)
                                {
                                    chk1.Checked = true;
                                    SetBackgroudOnClick(btn);
                                }
                                else
                                {
                                    chk1.Checked = false;
                                    SetDefautlBackgroud(btn);
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool checkinput()
        {
            bool flag = false;
            foreach (Panel pn in pnlQuay.Controls)
            {
                foreach (Panel pn1 in pn.Controls)
                {
                    if (pn1.Name.Substring(0, 1) == "B")
                    {
                        foreach (SimpleButton btn1 in pn1.Controls)
                        {
                            if (btn1.Appearance.BackColor != btnLuu.Appearance.BackColor)
                            {
                                flag = true;
                                break;
                            }
                        }
                    }
                }
            }
            return flag;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            string ma_quay = "";
            if (checkinput())
            {
                foreach (Panel pn in pnlQuay.Controls)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn1.Controls)
                            {
                                if (btn1.Appearance.BackColor != btnLuu.Appearance.BackColor)
                                {
                                    ma_quay = btn1.Name;
                                    break;
                                }
                            }
                        }
                    }
                }

                string sSQL = "";
                sSQL += "Delete CAUHINHKHUVUC Where MAQUAY=" + clsMain.SQLString(ma_quay) + "\n";
                foreach (Panel pn in pnlKhuvuc.Controls)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn1.Controls)
                            {
                                if (btn1.Appearance.BackColor != btnLuu.Appearance.BackColor)
                                {
                                    sSQL += "Insert Into CAUHINHKHUVUC(MAQUAY,MANHOM,MODE)" + "\n";
                                    sSQL += "Values(";
                                    sSQL += clsMain.SQLString(ma_quay) + ",";
                                    sSQL += clsMain.SQLString(btn1.Name) + ",";
                                    sSQL += clsMain.SQLString(1.ToString()) + ")" + "\n";
                                }                  
                            }
                        }
                    }
                }


                foreach (Panel pn in pnlNhomhang.Controls)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn1.Controls)
                            {
                                if (btn1.Appearance.BackColor != btnLuu.Appearance.BackColor)
                                {
                                    sSQL += "Insert Into CAUHINHKHUVUC(MAQUAY,MANHOM,MODE)" + "\n";
                                    sSQL += "Values(";
                                    sSQL += clsMain.SQLString(ma_quay) + ",";
                                    sSQL += clsMain.SQLString(btn1.Name) + ",";
                                    sSQL += clsMain.SQLString(2.ToString()) + ")" + "\n";
                                }            
                            }
                        }
                    }
                }

                SetDefautlBackgroud(btnSaochep);
                if (clsMain.ExecuteSQL(sSQL))
                {
                    XtraMessageBox.Show(_daluucauhinh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                XtraMessageBox.Show(_chuachonquay, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        string _thongbao = "";
        string _luukothanhcong = "";
        string _daluucauhinh = "";
        string _chuachonquay = "";
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkALL_Khuvuc_CheckedChanged(object sender, EventArgs e)
        {
            if (chkALL_Khuvuc.Checked)
            {
                Set_Defautl(pnlKhuvuc, false);
            }
            else
            {
                Set_Defautl(pnlKhuvuc, true);
            }
        }

        private void chkALL_Nhomhang_CheckedChanged(object sender, EventArgs e)
        {
            if (chkALL_Nhomhang.Checked)
            {
                Set_Defautl(pnlNhomhang,false);
            }
            else
            {
                Set_Defautl(pnlNhomhang,true);
            }
        }

        private void cboCUAHANG_EditValueChanged(object sender, EventArgs e)
        {
            LoadQuay(cboCUAHANG.EditValue.ToString());
            LoadKhuVuc(cboCUAHANG.EditValue.ToString());
            LoadNhomHang();

            chkALL_Khuvuc.Enabled = false;
            chkALL_Nhomhang.Enabled = false;
            SetDefautlBackgroud(btnSaochep);
        }

        string Ma_quay_coppy = "";
        string Ten_quay_coppy = "";
        private void btnSaochep_Click(object sender, EventArgs e)
        {
            bool flag = false;
            foreach (Panel pn in pnlQuay.Controls)
            {
                foreach (Panel pn1 in pn.Controls)
                {
                    if (pn1.Name.Substring(0, 1) == "B")
                    {
                        foreach (SimpleButton btn in pn1.Controls)
                        {
                            if (btn.Appearance.BackColor != btnLuu.Appearance.BackColor)
                            {
                                Ma_quay_coppy = btn.Name;
                                Ten_quay_coppy = btn.Text;
                                flag = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (flag)
            {
                if (btnSaochep.Appearance.BackColor == btnLuu.Appearance.BackColor)
                    SetBackgroudOnClick(btnSaochep);
                else
                    SetDefautlBackgroud(btnSaochep);
            }
            else
            {
                XtraMessageBox.Show("Chưa chọn Quầy để sao chép", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.Empty;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void Set_Defautl(Panel pn,bool flag)
        {
            if (flag)//hiện mặc định ko chọn
            {
                foreach (Panel pn1 in pn.Controls)
                {
                    foreach (Panel pn2 in pn1.Controls)
                    {
                        if (pn2.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn2.Controls)
                            {
                                chk1.Checked = false;
                            }
                        }
                        if (pn2.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn2.Controls)
                            {
                                SetDefautlBackgroud(btn1);
                            }
                        }
                    }
                }
            }
            else//chọn tất cả
            {
                foreach (Panel pn1 in pn.Controls)
                {
                    foreach (Panel pn2 in pn1.Controls)
                    {
                        if (pn2.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn2.Controls)
                            {
                                chk1.Checked = true;
                            }
                        }
                        if (pn2.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn2.Controls)
                            {
                                SetBackgroudOnClick(btn1);
                            }
                        }
                    }
                }
            }
        }
        private void Set_OnClick(Panel pn, string ma)
        {
            foreach (Panel pn1 in pn.Controls)
            {
                if (pn1.Name == "G" + ma)
                {
                    foreach (Panel pn2 in pn1.Controls)
                    {
                        if (pn2.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn2.Controls)
                            {
                                chk1.Checked = true;
                            }
                        }
                        if (pn2.Name.Substring(0, 1) == "B")
                        {
                            foreach (SimpleButton btn1 in pn2.Controls)
                            {
                                SetBackgroudOnClick(btn1);
                            }
                        }
                    }
                }
            }
        }

        private void Frm_CauhinhKhuvuc_Nhomhang_Load(object sender, EventArgs e)
        {
            lblTEN.Text = rm.GetString("cuahang",culture);
            groupControl1.Text = rm.GetString("khuvuc",culture);
            groupControl2.Text = rm.GetString("nhomhang",culture);
            chkALL_Khuvuc.Text = rm.GetString("chontatcakhuvuc",culture);
            chkALL_Nhomhang.Text = rm.GetString("chontatcanhomhang",culture);
            btnSaochep.Text = rm.GetString("saochep",culture);
            btnLuu.Text = rm.GetString("dongy",culture);
            btnDong.Text = rm.GetString("dong", culture);

            _thongbao = rm.GetString("thongbao", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _daluucauhinh = rm.GetString("daluucauhinh", culture);
            _chuachonquay = rm.GetString("chuachonquay", culture);
        }
    }
}