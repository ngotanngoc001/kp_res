﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_Daily : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";

        public Frm_Daily()
        {
            InitializeComponent();
            LoaddataGridView();
            txtwebsite.Enabled = false;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;
            txtwebsite.Enabled = true;
            txtwebsite.Focus();
            btnLuu.Enabled = true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            txtwebsite.Enabled = false;
            String sSPs = "";

            if (sMa != "" && btnSua.Enabled)
                sSPs = "UPDATE CAUHINH SET GIATRI='" + txtwebsite.Text + "' WHERE MA='" + sMa + "'";
            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsMain.ExecuteSQL(sSPs);
            this.Cursor = Cursors.Default;

            if (sBoolean)
            {
                LoaddataGridView();
                //btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtwebsite.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI").ToString();
        }

        private void LoaddataGridView()
        {
            gridView2.Focus();
            DataTable myDT = clsMain.ReturnDataTable("SELECT MA,TEN,GIATRI, GHICHU FROM CAUHINH WHERE TEN='DAILY'");
            gridControl2.DataSource = myDT;
            //gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private Boolean CheckInput()
        {
            if (txtwebsite.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtwebsite.Focus();
                return false;
            }
            return true;
        }

        private void Frm_Daily_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            lblTen.Text = rm.GetString("ten", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnSua.Text = rm.GetString("sua", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
        }

        
    }
}