﻿namespace KP_RES 
{
    partial class Frm_Cauhinhbanggia 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.optAPDUNGBANGGIATHEOKHUVUC1 = new DevExpress.XtraEditors.CheckEdit();
            this.optAPDUNGBANGGIATHEOKHUVUC = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.dtp1 = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dtp2 = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dtp3 = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.optAPDUNGBANGGIATHEOKHUVUC2 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // optAPDUNGBANGGIATHEOKHUVUC1
            // 
            this.optAPDUNGBANGGIATHEOKHUVUC1.EnterMoveNextControl = true;
            this.optAPDUNGBANGGIATHEOKHUVUC1.Location = new System.Drawing.Point(45, 73);
            this.optAPDUNGBANGGIATHEOKHUVUC1.Name = "optAPDUNGBANGGIATHEOKHUVUC1";
            this.optAPDUNGBANGGIATHEOKHUVUC1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAPDUNGBANGGIATHEOKHUVUC1.Properties.Appearance.Options.UseFont = true;
            this.optAPDUNGBANGGIATHEOKHUVUC1.Properties.Caption = "Dùng giá bán mặc định";
            this.optAPDUNGBANGGIATHEOKHUVUC1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optAPDUNGBANGGIATHEOKHUVUC1.Properties.RadioGroupIndex = 1;
            this.optAPDUNGBANGGIATHEOKHUVUC1.Size = new System.Drawing.Size(354, 24);
            this.optAPDUNGBANGGIATHEOKHUVUC1.TabIndex = 2;
            this.optAPDUNGBANGGIATHEOKHUVUC1.TabStop = false;
            // 
            // optAPDUNGBANGGIATHEOKHUVUC
            // 
            this.optAPDUNGBANGGIATHEOKHUVUC.EditValue = true;
            this.optAPDUNGBANGGIATHEOKHUVUC.EnterMoveNextControl = true;
            this.optAPDUNGBANGGIATHEOKHUVUC.Location = new System.Drawing.Point(45, 103);
            this.optAPDUNGBANGGIATHEOKHUVUC.Name = "optAPDUNGBANGGIATHEOKHUVUC";
            this.optAPDUNGBANGGIATHEOKHUVUC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAPDUNGBANGGIATHEOKHUVUC.Properties.Appearance.Options.UseFont = true;
            this.optAPDUNGBANGGIATHEOKHUVUC.Properties.Caption = "Dùng giá bán theo khu vực";
            this.optAPDUNGBANGGIATHEOKHUVUC.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optAPDUNGBANGGIATHEOKHUVUC.Properties.RadioGroupIndex = 1;
            this.optAPDUNGBANGGIATHEOKHUVUC.Size = new System.Drawing.Size(354, 24);
            this.optAPDUNGBANGGIATHEOKHUVUC.TabIndex = 3;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(129, 19);
            this.lblTEN.TabIndex = 1;
            this.lblTEN.Text = "Cấu hình bảng giá";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(419, 35);
            this.panel2.TabIndex = 0;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(289, 266);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 11;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(171, 266);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 10;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // dtp1
            // 
            this.dtp1.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.dtp1.EnterMoveNextControl = true;
            this.dtp1.Location = new System.Drawing.Point(151, 169);
            this.dtp1.Name = "dtp1";
            this.dtp1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp1.Properties.Appearance.Options.UseFont = true;
            this.dtp1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp1.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.dtp1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp1.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.dtp1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp1.Properties.Mask.EditMask = "HH:mm:ss";
            this.dtp1.Size = new System.Drawing.Size(248, 26);
            this.dtp1.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(63, 172);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(81, 19);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "Thời gian 1";
            // 
            // dtp2
            // 
            this.dtp2.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.dtp2.EnterMoveNextControl = true;
            this.dtp2.Location = new System.Drawing.Point(151, 201);
            this.dtp2.Name = "dtp2";
            this.dtp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp2.Properties.Appearance.Options.UseFont = true;
            this.dtp2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp2.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.dtp2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp2.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.dtp2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp2.Properties.Mask.EditMask = "HH:mm:ss";
            this.dtp2.Size = new System.Drawing.Size(248, 26);
            this.dtp2.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(63, 204);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(81, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Thời gian 2";
            // 
            // dtp3
            // 
            this.dtp3.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.dtp3.EnterMoveNextControl = true;
            this.dtp3.Location = new System.Drawing.Point(151, 233);
            this.dtp3.Name = "dtp3";
            this.dtp3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp3.Properties.Appearance.Options.UseFont = true;
            this.dtp3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp3.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.dtp3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp3.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.dtp3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp3.Properties.Mask.EditMask = "HH:mm:ss";
            this.dtp3.Size = new System.Drawing.Size(248, 26);
            this.dtp3.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(63, 236);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(81, 19);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Thời gian 3";
            // 
            // optAPDUNGBANGGIATHEOKHUVUC2
            // 
            this.optAPDUNGBANGGIATHEOKHUVUC2.EnterMoveNextControl = true;
            this.optAPDUNGBANGGIATHEOKHUVUC2.Location = new System.Drawing.Point(45, 134);
            this.optAPDUNGBANGGIATHEOKHUVUC2.Name = "optAPDUNGBANGGIATHEOKHUVUC2";
            this.optAPDUNGBANGGIATHEOKHUVUC2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAPDUNGBANGGIATHEOKHUVUC2.Properties.Appearance.Options.UseFont = true;
            this.optAPDUNGBANGGIATHEOKHUVUC2.Properties.Caption = "Dùng giá bán theo cửa hàng";
            this.optAPDUNGBANGGIATHEOKHUVUC2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optAPDUNGBANGGIATHEOKHUVUC2.Properties.RadioGroupIndex = 1;
            this.optAPDUNGBANGGIATHEOKHUVUC2.Size = new System.Drawing.Size(354, 24);
            this.optAPDUNGBANGGIATHEOKHUVUC2.TabIndex = 12;
            this.optAPDUNGBANGGIATHEOKHUVUC2.TabStop = false;
            // 
            // Frm_Cauhinhbanggia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 309);
            this.ControlBox = false;
            this.Controls.Add(this.optAPDUNGBANGGIATHEOKHUVUC2);
            this.Controls.Add(this.dtp3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.dtp2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dtp1);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.optAPDUNGBANGGIATHEOKHUVUC1);
            this.Controls.Add(this.optAPDUNGBANGGIATHEOKHUVUC);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhbanggia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình bảng giá";
            this.Load += new System.EventHandler(this.Frm_Cauhinhbanggia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAPDUNGBANGGIATHEOKHUVUC2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit optAPDUNGBANGGIATHEOKHUVUC;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.CheckEdit optAPDUNGBANGGIATHEOKHUVUC1;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.TimeEdit dtp1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TimeEdit dtp2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TimeEdit dtp3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit optAPDUNGBANGGIATHEOKHUVUC2;




    }
}