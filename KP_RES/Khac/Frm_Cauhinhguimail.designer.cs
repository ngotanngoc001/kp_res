﻿namespace KP_RES 
{
    partial class Frm_Cauhinhguimail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtObject = new DevExpress.XtraEditors.TextEdit();
            this.txtEmailGui = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnThemEmail = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEmail = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MARP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNGRP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkRP = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Report = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtMatkhau = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.optPDF = new DevExpress.XtraEditors.CheckEdit();
            this.optExcel = new DevExpress.XtraEditors.CheckEdit();
            this.txtPort = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cbbHost = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.cboCuahang = new DevExpress.XtraEditors.LookUpEdit();
            this.chk_Tuan = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Thang = new DevExpress.XtraEditors.CheckEdit();
            this.chk_3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_CN = new DevExpress.XtraEditors.CheckEdit();
            this.chk_7 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_6 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_2 = new DevExpress.XtraEditors.CheckEdit();
            this.timeSent = new DevExpress.XtraEditors.TimeEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.ritNoidung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtObject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailGui.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatkhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optExcel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Tuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Thang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeSent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ritNoidung.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(10, 6);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(157, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Danh sách Email nhận";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(10, 6);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(125, 19);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Danh sách Report";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(10, 6);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Lịch gửi";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(10, 128);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(65, 19);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Nội dung";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(10, 35);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(67, 19);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Email gửi";
            // 
            // txtObject
            // 
            this.txtObject.EnterMoveNextControl = true;
            this.txtObject.Location = new System.Drawing.Point(85, 96);
            this.txtObject.Name = "txtObject";
            this.txtObject.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObject.Properties.Appearance.Options.UseFont = true;
            this.txtObject.Properties.Appearance.Options.UseTextOptions = true;
            this.txtObject.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtObject.Properties.MaxLength = 100;
            this.txtObject.Size = new System.Drawing.Size(267, 26);
            this.txtObject.TabIndex = 6;
            // 
            // txtEmailGui
            // 
            this.txtEmailGui.EnterMoveNextControl = true;
            this.txtEmailGui.Location = new System.Drawing.Point(85, 32);
            this.txtEmailGui.Name = "txtEmailGui";
            this.txtEmailGui.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailGui.Properties.Appearance.Options.UseFont = true;
            this.txtEmailGui.Properties.Appearance.Options.UseTextOptions = true;
            this.txtEmailGui.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtEmailGui.Properties.MaxLength = 100;
            this.txtEmailGui.Size = new System.Drawing.Size(267, 26);
            this.txtEmailGui.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(10, 98);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(50, 19);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Chủ đề";
            // 
            // btnThemEmail
            // 
            this.btnThemEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemEmail.Appearance.Options.UseFont = true;
            this.btnThemEmail.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThemEmail.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemEmail.Location = new System.Drawing.Point(111, 321);
            this.btnThemEmail.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemEmail.Name = "btnThemEmail";
            this.btnThemEmail.Size = new System.Drawing.Size(123, 35);
            this.btnThemEmail.TabIndex = 2;
            this.btnThemEmail.Text = "&Thêm Email";
            this.btnThemEmail.Click += new System.EventHandler(this.btnThemEmail_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_700x30;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1016, 35);
            this.panel2.TabIndex = 0;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(899, 422);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(781, 422);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(10, 32);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.checkEmail});
            this.gridControl2.Size = new System.Drawing.Size(224, 265);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA,
            this.TEN,
            this.SUDUNG});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowColumnHeaders = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 40;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Width = 100;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Email";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 0;
            this.TEN.Width = 236;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.ColumnEdit = this.checkEmail;
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 1;
            this.SUDUNG.Width = 60;
            // 
            // checkEmail
            // 
            this.checkEmail.AutoHeight = false;
            this.checkEmail.Name = "checkEmail";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            gridLevelNode2.RelationName = "Level1";
            gridLevelNode3.RelationName = "Level2";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2,
            gridLevelNode3});
            this.gridControl1.Location = new System.Drawing.Point(10, 32);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.checkRP});
            this.gridControl1.Size = new System.Drawing.Size(224, 265);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MARP,
            this.TenReport,
            this.SUDUNGRP,
            this.Report});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 40;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TenReport, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // MARP
            // 
            this.MARP.AppearanceCell.Options.UseTextOptions = true;
            this.MARP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MARP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MARP.AppearanceHeader.Options.UseFont = true;
            this.MARP.AppearanceHeader.Options.UseTextOptions = true;
            this.MARP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MARP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MARP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MARP.Caption = "Mã";
            this.MARP.DisplayFormat.FormatString = "000";
            this.MARP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MARP.FieldName = "MARP";
            this.MARP.Name = "MARP";
            this.MARP.OptionsColumn.AllowEdit = false;
            this.MARP.OptionsColumn.AllowFocus = false;
            this.MARP.OptionsColumn.FixedWidth = true;
            this.MARP.Width = 60;
            // 
            // TenReport
            // 
            this.TenReport.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenReport.AppearanceHeader.Options.UseFont = true;
            this.TenReport.AppearanceHeader.Options.UseTextOptions = true;
            this.TenReport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenReport.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TenReport.Caption = "Tên báo cáo";
            this.TenReport.FieldName = "TenReport";
            this.TenReport.Name = "TenReport";
            this.TenReport.OptionsColumn.AllowEdit = false;
            this.TenReport.OptionsColumn.AllowFocus = false;
            this.TenReport.OptionsColumn.FixedWidth = true;
            this.TenReport.Visible = true;
            this.TenReport.VisibleIndex = 0;
            this.TenReport.Width = 216;
            // 
            // SUDUNGRP
            // 
            this.SUDUNGRP.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNGRP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNGRP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNGRP.AppearanceHeader.Options.UseFont = true;
            this.SUDUNGRP.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNGRP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNGRP.Caption = "Sử dụng";
            this.SUDUNGRP.ColumnEdit = this.checkRP;
            this.SUDUNGRP.FieldName = "SUDUNGRP";
            this.SUDUNGRP.Name = "SUDUNGRP";
            this.SUDUNGRP.OptionsColumn.FixedWidth = true;
            this.SUDUNGRP.Visible = true;
            this.SUDUNGRP.VisibleIndex = 1;
            this.SUDUNGRP.Width = 60;
            // 
            // checkRP
            // 
            this.checkRP.AutoHeight = false;
            this.checkRP.Name = "checkRP";
            // 
            // Report
            // 
            this.Report.Caption = "Report";
            this.Report.FieldName = "Report";
            this.Report.Name = "Report";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(10, 67);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(64, 19);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Mật khẩu";
            // 
            // txtMatkhau
            // 
            this.txtMatkhau.EnterMoveNextControl = true;
            this.txtMatkhau.Location = new System.Drawing.Point(85, 64);
            this.txtMatkhau.Name = "txtMatkhau";
            this.txtMatkhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatkhau.Properties.Appearance.Options.UseFont = true;
            this.txtMatkhau.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMatkhau.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMatkhau.Properties.MaxLength = 30;
            this.txtMatkhau.Properties.UseSystemPasswordChar = true;
            this.txtMatkhau.Size = new System.Drawing.Size(267, 26);
            this.txtMatkhau.TabIndex = 4;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(10, 239);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(63, 19);
            this.labelControl8.TabIndex = 9;
            this.labelControl8.Text = "Dạng file";
            // 
            // optPDF
            // 
            this.optPDF.EnterMoveNextControl = true;
            this.optPDF.Location = new System.Drawing.Point(165, 238);
            this.optPDF.Name = "optPDF";
            this.optPDF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optPDF.Properties.Appearance.Options.UseFont = true;
            this.optPDF.Properties.Caption = "PDF";
            this.optPDF.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optPDF.Properties.RadioGroupIndex = 1;
            this.optPDF.Size = new System.Drawing.Size(96, 24);
            this.optPDF.TabIndex = 11;
            this.optPDF.TabStop = false;
            // 
            // optExcel
            // 
            this.optExcel.EditValue = true;
            this.optExcel.EnterMoveNextControl = true;
            this.optExcel.Location = new System.Drawing.Point(83, 238);
            this.optExcel.Name = "optExcel";
            this.optExcel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optExcel.Properties.Appearance.Options.UseFont = true;
            this.optExcel.Properties.Caption = "Excel";
            this.optExcel.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.optExcel.Properties.RadioGroupIndex = 1;
            this.optExcel.Size = new System.Drawing.Size(75, 24);
            this.optExcel.TabIndex = 10;
            // 
            // txtPort
            // 
            this.txtPort.EnterMoveNextControl = true;
            this.txtPort.Location = new System.Drawing.Point(85, 330);
            this.txtPort.Name = "txtPort";
            this.txtPort.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Properties.Appearance.Options.UseFont = true;
            this.txtPort.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPort.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPort.Properties.MaxLength = 4;
            this.txtPort.Size = new System.Drawing.Size(267, 26);
            this.txtPort.TabIndex = 16;
            this.txtPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPort_KeyPress);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(10, 334);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(29, 19);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "Port";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(10, 300);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(32, 19);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "Host";
            // 
            // cbbHost
            // 
            this.cbbHost.Location = new System.Drawing.Point(85, 297);
            this.cbbHost.Name = "cbbHost";
            this.cbbHost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHost.Properties.Appearance.Options.UseFont = true;
            this.cbbHost.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHost.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbbHost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbHost.Properties.DropDownItemHeight = 40;
            this.cbbHost.Properties.Items.AddRange(new object[] {
            "smtp.gmail.com",
            "smtp.mail.yahoo.com",
            "smtp.live.com",
            "smtp.mail.com"});
            this.cbbHost.Properties.MaxLength = 50;
            this.cbbHost.Size = new System.Drawing.Size(267, 26);
            this.cbbHost.TabIndex = 14;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(10, 304);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(68, 19);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "Cửa hàng";
            // 
            // cboCuahang
            // 
            this.cboCuahang.EnterMoveNextControl = true;
            this.cboCuahang.Location = new System.Drawing.Point(10, 330);
            this.cboCuahang.Name = "cboCuahang";
            this.cboCuahang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.Appearance.Options.UseFont = true;
            this.cboCuahang.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboCuahang.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboCuahang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCuahang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCuahang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "Tên")});
            this.cboCuahang.Properties.DisplayMember = "TEN";
            this.cboCuahang.Properties.DropDownItemHeight = 40;
            this.cboCuahang.Properties.NullText = "";
            this.cboCuahang.Properties.ShowHeader = false;
            this.cboCuahang.Properties.ValueMember = "MA";
            this.cboCuahang.Size = new System.Drawing.Size(224, 26);
            this.cboCuahang.TabIndex = 3;
            // 
            // chk_Tuan
            // 
            this.chk_Tuan.Location = new System.Drawing.Point(8, 235);
            this.chk_Tuan.Name = "chk_Tuan";
            this.chk_Tuan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Tuan.Properties.Appearance.Options.UseFont = true;
            this.chk_Tuan.Properties.Caption = "Hàng tuần";
            this.chk_Tuan.Size = new System.Drawing.Size(111, 24);
            this.chk_Tuan.TabIndex = 8;
            // 
            // chk_Thang
            // 
            this.chk_Thang.Location = new System.Drawing.Point(8, 264);
            this.chk_Thang.Name = "chk_Thang";
            this.chk_Thang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Thang.Properties.Appearance.Options.UseFont = true;
            this.chk_Thang.Properties.Caption = "Hàng tháng";
            this.chk_Thang.Size = new System.Drawing.Size(111, 24);
            this.chk_Thang.TabIndex = 9;
            // 
            // chk_3
            // 
            this.chk_3.Location = new System.Drawing.Point(8, 61);
            this.chk_3.Name = "chk_3";
            this.chk_3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_3.Properties.Appearance.Options.UseFont = true;
            this.chk_3.Properties.Caption = "Thứ 3";
            this.chk_3.Size = new System.Drawing.Size(96, 24);
            this.chk_3.TabIndex = 2;
            // 
            // chk_4
            // 
            this.chk_4.Location = new System.Drawing.Point(8, 90);
            this.chk_4.Name = "chk_4";
            this.chk_4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_4.Properties.Appearance.Options.UseFont = true;
            this.chk_4.Properties.Caption = "Thứ 4";
            this.chk_4.Size = new System.Drawing.Size(96, 24);
            this.chk_4.TabIndex = 3;
            // 
            // chk_5
            // 
            this.chk_5.Location = new System.Drawing.Point(8, 119);
            this.chk_5.Name = "chk_5";
            this.chk_5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_5.Properties.Appearance.Options.UseFont = true;
            this.chk_5.Properties.Caption = "Thứ 5";
            this.chk_5.Size = new System.Drawing.Size(96, 24);
            this.chk_5.TabIndex = 4;
            // 
            // chk_CN
            // 
            this.chk_CN.Location = new System.Drawing.Point(8, 206);
            this.chk_CN.Name = "chk_CN";
            this.chk_CN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_CN.Properties.Appearance.Options.UseFont = true;
            this.chk_CN.Properties.Caption = "Chủ nhật";
            this.chk_CN.Size = new System.Drawing.Size(96, 24);
            this.chk_CN.TabIndex = 7;
            // 
            // chk_7
            // 
            this.chk_7.Location = new System.Drawing.Point(8, 177);
            this.chk_7.Name = "chk_7";
            this.chk_7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_7.Properties.Appearance.Options.UseFont = true;
            this.chk_7.Properties.Caption = "Thứ 7";
            this.chk_7.Size = new System.Drawing.Size(96, 24);
            this.chk_7.TabIndex = 6;
            // 
            // chk_6
            // 
            this.chk_6.Location = new System.Drawing.Point(8, 148);
            this.chk_6.Name = "chk_6";
            this.chk_6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_6.Properties.Appearance.Options.UseFont = true;
            this.chk_6.Properties.Caption = "Thứ 6";
            this.chk_6.Size = new System.Drawing.Size(96, 24);
            this.chk_6.TabIndex = 5;
            // 
            // chk_2
            // 
            this.chk_2.Location = new System.Drawing.Point(8, 32);
            this.chk_2.Name = "chk_2";
            this.chk_2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_2.Properties.Appearance.Options.UseFont = true;
            this.chk_2.Properties.Caption = "Thứ 2";
            this.chk_2.Size = new System.Drawing.Size(96, 24);
            this.chk_2.TabIndex = 1;
            // 
            // timeSent
            // 
            this.timeSent.EditValue = new System.DateTime(2014, 10, 6, 0, 0, 0, 0);
            this.timeSent.Location = new System.Drawing.Point(10, 330);
            this.timeSent.Name = "timeSent";
            this.timeSent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSent.Properties.Appearance.Options.UseFont = true;
            this.timeSent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeSent.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeSent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeSent.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeSent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeSent.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeSent.Size = new System.Drawing.Size(119, 26);
            this.timeSent.TabIndex = 11;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.gridControl2);
            this.panelControl1.Controls.Add(this.btnThemEmail);
            this.panelControl1.Location = new System.Drawing.Point(6, 45);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(242, 373);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Controls.Add(this.cboCuahang);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Location = new System.Drawing.Point(254, 45);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(242, 373);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl6);
            this.panelControl3.Controls.Add(this.chk_Tuan);
            this.panelControl3.Controls.Add(this.timeSent);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.chk_Thang);
            this.panelControl3.Controls.Add(this.chk_2);
            this.panelControl3.Controls.Add(this.chk_3);
            this.panelControl3.Controls.Add(this.chk_6);
            this.panelControl3.Controls.Add(this.chk_4);
            this.panelControl3.Controls.Add(this.chk_7);
            this.panelControl3.Controls.Add(this.chk_5);
            this.panelControl3.Controls.Add(this.chk_CN);
            this.panelControl3.Location = new System.Drawing.Point(502, 45);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(137, 373);
            this.panelControl3.TabIndex = 3;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(10, 304);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(52, 19);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Giờ gửi";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.ritNoidung);
            this.panelControl4.Controls.Add(this.labelControl13);
            this.panelControl4.Controls.Add(this.txtPort);
            this.panelControl4.Controls.Add(this.labelControl10);
            this.panelControl4.Controls.Add(this.labelControl12);
            this.panelControl4.Controls.Add(this.txtEmailGui);
            this.panelControl4.Controls.Add(this.labelControl9);
            this.panelControl4.Controls.Add(this.labelControl3);
            this.panelControl4.Controls.Add(this.cbbHost);
            this.panelControl4.Controls.Add(this.txtMatkhau);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.optPDF);
            this.panelControl4.Controls.Add(this.txtObject);
            this.panelControl4.Controls.Add(this.optExcel);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Location = new System.Drawing.Point(645, 45);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(364, 373);
            this.panelControl4.TabIndex = 4;
            // 
            // ritNoidung
            // 
            this.ritNoidung.Location = new System.Drawing.Point(85, 128);
            this.ritNoidung.Name = "ritNoidung";
            this.ritNoidung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ritNoidung.Properties.Appearance.Options.UseFont = true;
            this.ritNoidung.Size = new System.Drawing.Size(267, 105);
            this.ritNoidung.TabIndex = 8;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(10, 271);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(105, 19);
            this.labelControl13.TabIndex = 12;
            this.labelControl13.Text = "Cấu hình Smtp";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(10, 6);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(135, 19);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Cấu hình Email gửi";
            // 
            // Frm_Cauhinhguimail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 470);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Cauhinhguimail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình gửi mail báo cáo";
            ((System.ComponentModel.ISupportInitialize)(this.txtObject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailGui.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatkhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optExcel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Tuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Thang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeSent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ritNoidung.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtObject;
        private DevExpress.XtraEditors.TextEdit txtEmailGui;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnThemEmail;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn MARP;
        private DevExpress.XtraGrid.Columns.GridColumn TenReport;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNGRP;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkEmail;
        private DevExpress.XtraGrid.Columns.GridColumn Report;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkRP;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtMatkhau;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit optPDF;
        private DevExpress.XtraEditors.CheckEdit optExcel;
        private DevExpress.XtraEditors.TextEdit txtPort;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit cbbHost;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LookUpEdit cboCuahang;
        private DevExpress.XtraEditors.CheckEdit chk_3;
        private DevExpress.XtraEditors.CheckEdit chk_4;
        private DevExpress.XtraEditors.CheckEdit chk_5;
        private DevExpress.XtraEditors.CheckEdit chk_CN;
        private DevExpress.XtraEditors.CheckEdit chk_7;
        private DevExpress.XtraEditors.CheckEdit chk_6;
        private DevExpress.XtraEditors.CheckEdit chk_2;
        private DevExpress.XtraEditors.TimeEdit timeSent;
        private DevExpress.XtraEditors.CheckEdit chk_Tuan;
        private DevExpress.XtraEditors.CheckEdit chk_Thang;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit ritNoidung;




    }
}