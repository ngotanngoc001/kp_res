﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_DanhSachBienBan : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_DanhSachBienBan()
        {
            InitializeComponent();
        }

        private void Frm_DanhSachBienBan_Load(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "DELETE FROM BIENBAN_GIAONHAN" + "\n";
            sSQL += "WHERE SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";

            sSQL += "DELETE FROM CHITIET_BIENBAN_GN" + "\n";
            sSQL += "WHERE SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            PF_BienBanGiaoNhan frm = new PF_BienBanGiaoNhan();
            frm.iFormState = 2;
            frm.sMa = sMa;
            frm.ShowDialog(this);
            LoaddataGridView();
            frm.Dispose();

            sMa = "";
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            PF_BienBanGiaoNhan frm = new PF_BienBanGiaoNhan();
            frm.iFormState = 1;
            frm.sMa = sMa;
            frm.ShowDialog(this);
            frm.Dispose();

            sMa = "";
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            String sSQL = "";
            sSQL += "SELECT A.MA,A.SO_KYHIEU,A.NGAYBANHANH,A.BENGIAO,A.CV_BENGIAO,A.BENNHAN,A.CV_BENNHAN,A.NGAYNHAN,A.NOIDUNG,A.FILES,A.TEN_FILES,A.NGAYTAO,A.NGUOITAO,A.NGAYSUA,A.NGUOISUA,B.TEN,B.COQUANCAP,B.SOHIEU,B.BANCHINH,B.BANPHU,B.NGAYCAP,B.NGAYHETHAN" + "\n";
            sSQL += "FROM BIENBAN_GIAONHAN A" + "\n";
            sSQL += "INNER JOIN CHITIET_BIENBAN_GN B ON A.SO_KYHIEU = B.SO_KYHIEU" + "\n";
            sSQL += "WHERE A.SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";
            DataTable myDT = clsMain.ReturnDataTable(sSQL);

            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.dtSource = myDT;
            frm.Mode = 81;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SO_KYHIEU").ToString();
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));
        }

        private void LoaddataGridView()
        {
            String sSQL = "";
            sSQL += "SELECT A.MA,A.SO_KYHIEU,A.NGAYBANHANH,A.BENGIAO,A.CV_BENGIAO,A.BENNHAN,A.CV_BENNHAN,A.NGAYNHAN," + "\n";
            sSQL += "A.NOIDUNG,A.FILES,A.TEN_FILES,A.NGAYTAO,A.NGUOITAO,A.NGAYSUA,A.NGUOISUA" + "\n";
            sSQL += "FROM BIENBAN_GIAONHAN A" + "\n";

            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = myDT;
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        

        


    }
}