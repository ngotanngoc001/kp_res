﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;

namespace KP_RES
{
    public partial class Frm_BaoCao_BienBanGiaoNhan : DevExpress.XtraEditors.XtraForm
    {
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_BaoCao_BienBanGiaoNhan()
        {
            InitializeComponent();
            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Báo cáo";
            }
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisibleChoose();
        }

        private void VisibleChoose()
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

            pnChoose.Visible = (optGroup.SelectedIndex == 0) ? false : true;

            if (optGroup.SelectedIndex == 1)
            {
                Visible(false);
            }
            else if (optGroup.SelectedIndex == 2)
            {
                Visible(false);
            }
        }

        private void Visible(Boolean sBoolean)
        {
            lblTitle.Visible = cboData.Visible = sBoolean;
            lblTuNgay.Visible = lblDenNgay.Visible = dtpDenNgay.Visible = dtpTuNgay.Visible = !sBoolean;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";
                String sTuNgay = "";
                String sDenNgay = "";

                if (optGroup.EditValue.Equals("1") || optGroup.EditValue.Equals("2"))
                {
                    sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.Value);
                    sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.Value);
                }

                SqlParameter[] sParameter = new SqlParameter[4];
                sParameter[0] = new SqlParameter("@MA", (Object)sMa);
                sParameter[1] = new SqlParameter("@TUNGAY", (Object)sTuNgay);
                sParameter[2] = new SqlParameter("@DENNGAY", (Object)sDenNgay);
                sParameter[3] = new SqlParameter("@STYLE", (Object)optGroup.EditValue);

                DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectBCBienBanGiaoNhan", sParameter);
                gridControl2.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView2.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridView2.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridView2.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.RowCount > 0)
                {
                    DataView myDV = (DataView)gridView2.DataSource;
                    DataTable myDT = myDV.ToTable();

                    String Description = "";
                    Description = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                        Description = "( " + Description + " )";
                    else
                        Description = "( " + Description + " Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = Description;

                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 82;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    clsQLCongTy.MessageInformation("Không có dữ liệu", "Thông báo");
            }
            catch{
            }
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            Boolean sBoolean = (columns.Contains(columnName)) ? true : false;

            return sBoolean;
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            SaveFileDialog sSaveFile = new SaveFileDialog();
            sSaveFile.Filter = clsQLCongTy.Filter();
            sSaveFile.FilterIndex = 1;
            sSaveFile.RestoreDirectory = true;
            sSaveFile.Title = "Chọn file lưu";
            sSaveFile.FileName = sNameFile;
            if (sSaveFile.ShowDialog() == DialogResult.OK)
                if (clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                    clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                else
                    clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SO_KYHIEU").ToString();
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));
        }

        string sMa = "";
        private void btnXemchitiet_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            PF_BienBanGiaoNhan frm = new PF_BienBanGiaoNhan();
            frm.iFormState = 1;
            frm.sMa = sMa;
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void btnInphieu_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            String sSQL = "";
            sSQL += "SELECT A.MA,A.SO_KYHIEU,A.NGAYBANHANH,A.BENGIAO,A.CV_BENGIAO,A.BENNHAN,A.CV_BENNHAN,A.NGAYNHAN,A.NOIDUNG,A.FILES,A.TEN_FILES,A.NGAYTAO,A.NGUOITAO,A.NGAYSUA,A.NGUOISUA,B.TEN,B.COQUANCAP,B.SOHIEU,B.BANCHINH,B.BANPHU,B.NGAYCAP,B.NGAYHETHAN" + "\n";
            sSQL += "FROM BIENBAN_GIAONHAN A" + "\n";
            sSQL += "INNER JOIN CHITIET_BIENBAN_GN B ON A.SO_KYHIEU = B.SO_KYHIEU" + "\n";
            sSQL += "WHERE A.SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";
            DataTable myDT = clsMain.ReturnDataTable(sSQL);

            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.dtSource = myDT;
            frm.Mode = 81;
            frm.ShowDialog();
            frm.Dispose();
        }

       
       


    }
}