﻿namespace KP_RES 
{
    partial class Frm_DanhSachBienBan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnXem = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnIn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SO_KYHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYBANHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BENGIAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CV_BENGIAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BENNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CV_BENNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYDEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BPXULY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENGIAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnXem);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnIn);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 701);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1362, 40);
            this.panelControl1.TabIndex = 6;
            // 
            // btnXem
            // 
            this.btnXem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXem.Appearance.Options.UseFont = true;
            this.btnXem.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnXem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXem.Location = new System.Drawing.Point(795, 0);
            this.btnXem.Margin = new System.Windows.Forms.Padding(4);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(113, 40);
            this.btnXem.TabIndex = 41;
            this.btnXem.Text = "&Xem";
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(908, 0);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(113, 40);
            this.btnSua.TabIndex = 40;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(1021, 0);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(113, 40);
            this.btnXoa.TabIndex = 38;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnIn
            // 
            this.btnIn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIn.Appearance.Options.UseFont = true;
            this.btnIn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnIn.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnIn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnIn.Location = new System.Drawing.Point(1134, 0);
            this.btnIn.Margin = new System.Windows.Forms.Padding(4);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(113, 40);
            this.btnIn.TabIndex = 42;
            this.btnIn.Text = "&In";
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton1.Image = global::KP_RES.Properties.Resources.down_26;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton1.Location = new System.Drawing.Point(1247, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(115, 40);
            this.simpleButton1.TabIndex = 35;
            this.simpleButton1.Text = "&Tải File";
            this.simpleButton1.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1323, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 701);
            this.panelControl2.TabIndex = 7;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 537);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 217);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 377);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 457);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 619);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILES,
            this.STT,
            this.MA,
            this.SO_KYHIEU,
            this.NGAYBANHANH,
            this.BENGIAO,
            this.CV_BENGIAO,
            this.BENNHAN,
            this.CV_BENNHAN,
            this.NGAYNHAN,
            this.NOIDUNG,
            this.TEN_FILES,
            this.FILL,
            this.NGAYDEN,
            this.BPXULY,
            this.NHANVIENXULY,
            this.TRANGTHAI,
            this.DIENGIAI});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILES
            // 
            this.FILES.FieldName = "FILES";
            this.FILES.Name = "FILES";
            this.FILES.OptionsColumn.AllowEdit = false;
            this.FILES.OptionsColumn.AllowFocus = false;
            this.FILES.OptionsColumn.FixedWidth = true;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // SO_KYHIEU
            // 
            this.SO_KYHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SO_KYHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SO_KYHIEU.AppearanceHeader.Options.UseFont = true;
            this.SO_KYHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SO_KYHIEU.Caption = "Số / Ký hiệu";
            this.SO_KYHIEU.FieldName = "SO_KYHIEU";
            this.SO_KYHIEU.Name = "SO_KYHIEU";
            this.SO_KYHIEU.OptionsColumn.AllowEdit = false;
            this.SO_KYHIEU.OptionsColumn.AllowFocus = false;
            this.SO_KYHIEU.OptionsColumn.FixedWidth = true;
            this.SO_KYHIEU.Visible = true;
            this.SO_KYHIEU.VisibleIndex = 2;
            this.SO_KYHIEU.Width = 150;
            // 
            // NGAYBANHANH
            // 
            this.NGAYBANHANH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYBANHANH.AppearanceHeader.Options.UseFont = true;
            this.NGAYBANHANH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.Caption = "Ngày BH";
            this.NGAYBANHANH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYBANHANH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYBANHANH.FieldName = "NGAYBANHANH";
            this.NGAYBANHANH.Name = "NGAYBANHANH";
            this.NGAYBANHANH.OptionsColumn.AllowEdit = false;
            this.NGAYBANHANH.OptionsColumn.AllowFocus = false;
            this.NGAYBANHANH.OptionsColumn.FixedWidth = true;
            this.NGAYBANHANH.Visible = true;
            this.NGAYBANHANH.VisibleIndex = 3;
            this.NGAYBANHANH.Width = 100;
            // 
            // BENGIAO
            // 
            this.BENGIAO.AppearanceCell.Options.UseTextOptions = true;
            this.BENGIAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BENGIAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BENGIAO.AppearanceHeader.Options.UseFont = true;
            this.BENGIAO.AppearanceHeader.Options.UseTextOptions = true;
            this.BENGIAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BENGIAO.Caption = "Bên giao";
            this.BENGIAO.FieldName = "BENGIAO";
            this.BENGIAO.Name = "BENGIAO";
            this.BENGIAO.OptionsColumn.AllowEdit = false;
            this.BENGIAO.OptionsColumn.AllowFocus = false;
            this.BENGIAO.OptionsColumn.FixedWidth = true;
            this.BENGIAO.Visible = true;
            this.BENGIAO.VisibleIndex = 4;
            this.BENGIAO.Width = 200;
            // 
            // CV_BENGIAO
            // 
            this.CV_BENGIAO.AppearanceCell.Options.UseTextOptions = true;
            this.CV_BENGIAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CV_BENGIAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CV_BENGIAO.AppearanceHeader.Options.UseFont = true;
            this.CV_BENGIAO.AppearanceHeader.Options.UseTextOptions = true;
            this.CV_BENGIAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CV_BENGIAO.Caption = "Chức vụ";
            this.CV_BENGIAO.FieldName = "CV_BENGIAO";
            this.CV_BENGIAO.Name = "CV_BENGIAO";
            this.CV_BENGIAO.OptionsColumn.AllowEdit = false;
            this.CV_BENGIAO.OptionsColumn.AllowFocus = false;
            this.CV_BENGIAO.OptionsColumn.FixedWidth = true;
            this.CV_BENGIAO.Visible = true;
            this.CV_BENGIAO.VisibleIndex = 5;
            this.CV_BENGIAO.Width = 150;
            // 
            // BENNHAN
            // 
            this.BENNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.BENNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BENNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BENNHAN.AppearanceHeader.Options.UseFont = true;
            this.BENNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.BENNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BENNHAN.Caption = "Bên nhận";
            this.BENNHAN.FieldName = "BENNHAN";
            this.BENNHAN.Name = "BENNHAN";
            this.BENNHAN.OptionsColumn.AllowEdit = false;
            this.BENNHAN.OptionsColumn.AllowFocus = false;
            this.BENNHAN.OptionsColumn.FixedWidth = true;
            this.BENNHAN.Visible = true;
            this.BENNHAN.VisibleIndex = 6;
            this.BENNHAN.Width = 200;
            // 
            // CV_BENNHAN
            // 
            this.CV_BENNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.CV_BENNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CV_BENNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CV_BENNHAN.AppearanceHeader.Options.UseFont = true;
            this.CV_BENNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.CV_BENNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CV_BENNHAN.Caption = "Chức vụ";
            this.CV_BENNHAN.FieldName = "CV_BENNHAN";
            this.CV_BENNHAN.Name = "CV_BENNHAN";
            this.CV_BENNHAN.OptionsColumn.AllowEdit = false;
            this.CV_BENNHAN.OptionsColumn.AllowFocus = false;
            this.CV_BENNHAN.OptionsColumn.FixedWidth = true;
            this.CV_BENNHAN.Visible = true;
            this.CV_BENNHAN.VisibleIndex = 7;
            this.CV_BENNHAN.Width = 150;
            // 
            // NGAYNHAN
            // 
            this.NGAYNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYNHAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYNHAN.Caption = "Ngày nhận";
            this.NGAYNHAN.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYNHAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYNHAN.FieldName = "NGAYNHAN";
            this.NGAYNHAN.Name = "NGAYNHAN";
            this.NGAYNHAN.OptionsColumn.AllowEdit = false;
            this.NGAYNHAN.OptionsColumn.AllowFocus = false;
            this.NGAYNHAN.OptionsColumn.FixedWidth = true;
            this.NGAYNHAN.Visible = true;
            this.NGAYNHAN.VisibleIndex = 8;
            this.NGAYNHAN.Width = 100;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 9;
            this.NOIDUNG.Width = 300;
            // 
            // TEN_FILES
            // 
            this.TEN_FILES.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_FILES.AppearanceHeader.Options.UseFont = true;
            this.TEN_FILES.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_FILES.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_FILES.Caption = "Tên file";
            this.TEN_FILES.FieldName = "TEN_FILES";
            this.TEN_FILES.Name = "TEN_FILES";
            this.TEN_FILES.OptionsColumn.AllowEdit = false;
            this.TEN_FILES.OptionsColumn.AllowFocus = false;
            this.TEN_FILES.OptionsColumn.FixedWidth = true;
            this.TEN_FILES.Visible = true;
            this.TEN_FILES.VisibleIndex = 10;
            this.TEN_FILES.Width = 100;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 11;
            this.FILL.Width = 20;
            // 
            // NGAYDEN
            // 
            this.NGAYDEN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYDEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYDEN.AppearanceHeader.Options.UseFont = true;
            this.NGAYDEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYDEN.Caption = "Ngày đến";
            this.NGAYDEN.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYDEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYDEN.FieldName = "NGAYDEN";
            this.NGAYDEN.Name = "NGAYDEN";
            this.NGAYDEN.OptionsColumn.AllowEdit = false;
            this.NGAYDEN.OptionsColumn.AllowFocus = false;
            this.NGAYDEN.OptionsColumn.FixedWidth = true;
            this.NGAYDEN.Width = 100;
            // 
            // BPXULY
            // 
            this.BPXULY.AppearanceCell.Options.UseTextOptions = true;
            this.BPXULY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BPXULY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BPXULY.AppearanceHeader.Options.UseFont = true;
            this.BPXULY.AppearanceHeader.Options.UseTextOptions = true;
            this.BPXULY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BPXULY.Caption = "BP Xử lý";
            this.BPXULY.FieldName = "BPXULY";
            this.BPXULY.Name = "BPXULY";
            this.BPXULY.OptionsColumn.AllowEdit = false;
            this.BPXULY.OptionsColumn.AllowFocus = false;
            this.BPXULY.OptionsColumn.FixedWidth = true;
            this.BPXULY.Width = 150;
            // 
            // NHANVIENXULY
            // 
            this.NHANVIENXULY.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIENXULY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIENXULY.AppearanceHeader.Options.UseFont = true;
            this.NHANVIENXULY.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIENXULY.Caption = "NV Xử lý";
            this.NHANVIENXULY.FieldName = "NHANVIENXULY";
            this.NHANVIENXULY.Name = "NHANVIENXULY";
            this.NHANVIENXULY.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY.OptionsColumn.FixedWidth = true;
            this.NHANVIENXULY.Width = 150;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Width = 150;
            // 
            // DIENGIAI
            // 
            this.DIENGIAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENGIAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENGIAI.AppearanceHeader.Options.UseFont = true;
            this.DIENGIAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENGIAI.Caption = "Diễn giải";
            this.DIENGIAI.FieldName = "DIENGIAI";
            this.DIENGIAI.Name = "DIENGIAI";
            this.DIENGIAI.OptionsColumn.AllowEdit = false;
            this.DIENGIAI.OptionsColumn.AllowFocus = false;
            this.DIENGIAI.OptionsColumn.FixedWidth = true;
            this.DIENGIAI.Width = 300;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1323, 701);
            this.gridControl2.TabIndex = 8;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // Frm_DanhSachBienBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_DanhSachBienBan";
            this.Text = "Danh sách BBGN";
            this.Load += new System.EventHandler(this.Frm_DanhSachBienBan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnXem;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn FILES;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn SO_KYHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYBANHANH;
        private DevExpress.XtraGrid.Columns.GridColumn BENGIAO;
        private DevExpress.XtraGrid.Columns.GridColumn CV_BENGIAO;
        private DevExpress.XtraGrid.Columns.GridColumn BENNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn CV_BENNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_FILES;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYDEN;
        private DevExpress.XtraGrid.Columns.GridColumn BPXULY;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENGIAI;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraEditors.SimpleButton btnIn;





    }
}