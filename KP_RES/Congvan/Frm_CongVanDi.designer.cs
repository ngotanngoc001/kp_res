﻿namespace KP_RES 
{
    partial class Frm_CongVanDi 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.cboCoQuanNhan = new DevExpress.XtraEditors.LookUpEdit();
            this.lblCoQuanNhan = new DevExpress.XtraEditors.LabelControl();
            this.txtFile = new DevExpress.XtraEditors.TextEdit();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.lblFileVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblNoiDung = new DevExpress.XtraEditors.LabelControl();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayBanHanh = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgayGui = new DevExpress.XtraEditors.DateEdit();
            this.txtSoKyHieu = new DevExpress.XtraEditors.TextEdit();
            this.cboNhanVienXuLy = new DevExpress.XtraEditors.LookUpEdit();
            this.lblNhanVienXuLy = new DevExpress.XtraEditors.LabelControl();
            this.cboCoQuanBanHanh = new DevExpress.XtraEditors.LookUpEdit();
            this.cboSoVanBan = new DevExpress.XtraEditors.LookUpEdit();
            this.lblSoVanBan = new DevExpress.XtraEditors.LabelControl();
            this.cboLoaiVanBan = new DevExpress.XtraEditors.LookUpEdit();
            this.lblLoaiVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblCoQuanBanHanh = new DevExpress.XtraEditors.LabelControl();
            this.lblSoGui = new DevExpress.XtraEditors.LabelControl();
            this.txtSoGui = new DevExpress.XtraEditors.TextEdit();
            this.lblSoKyHieu = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNgayBanHanh = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayGui = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILEVANBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQBH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQNHAN_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SO_KYHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYBANHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SODEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYDEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAME_FILE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayGui.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayGui.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoKyHieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienXuLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanBanHanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSoVanBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiVanBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGui.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblTenFile);
            this.panelControl1.Controls.Add(this.txtTenFile);
            this.panelControl1.Controls.Add(this.btnDownLoadFile);
            this.panelControl1.Controls.Add(this.cboCoQuanNhan);
            this.panelControl1.Controls.Add(this.lblCoQuanNhan);
            this.panelControl1.Controls.Add(this.txtFile);
            this.panelControl1.Controls.Add(this.btnFile);
            this.panelControl1.Controls.Add(this.lblFileVanBan);
            this.panelControl1.Controls.Add(this.lblNoiDung);
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.dtpNgayBanHanh);
            this.panelControl1.Controls.Add(this.dtpNgayGui);
            this.panelControl1.Controls.Add(this.txtSoKyHieu);
            this.panelControl1.Controls.Add(this.cboNhanVienXuLy);
            this.panelControl1.Controls.Add(this.lblNhanVienXuLy);
            this.panelControl1.Controls.Add(this.cboCoQuanBanHanh);
            this.panelControl1.Controls.Add(this.cboSoVanBan);
            this.panelControl1.Controls.Add(this.lblSoVanBan);
            this.panelControl1.Controls.Add(this.cboLoaiVanBan);
            this.panelControl1.Controls.Add(this.lblLoaiVanBan);
            this.panelControl1.Controls.Add(this.lblCoQuanBanHanh);
            this.panelControl1.Controls.Add(this.lblSoGui);
            this.panelControl1.Controls.Add(this.txtSoGui);
            this.panelControl1.Controls.Add(this.lblSoKyHieu);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lblNgayBanHanh);
            this.panelControl1.Controls.Add(this.lblNgayGui);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 175);
            this.panelControl1.TabIndex = 0;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(341, 104);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 21;
            this.lblTenFile.Text = "Tên File";
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(418, 101);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.AutoHeight = false;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 22;
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDownLoadFile.Location = new System.Drawing.Point(442, 134);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(87, 35);
            this.btnDownLoadFile.TabIndex = 29;
            this.btnDownLoadFile.Text = "Tải File";
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // cboCoQuanNhan
            // 
            this.cboCoQuanNhan.EnterMoveNextControl = true;
            this.cboCoQuanNhan.Location = new System.Drawing.Point(787, 37);
            this.cboCoQuanNhan.Name = "cboCoQuanNhan";
            this.cboCoQuanNhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanNhan.Properties.Appearance.Options.UseFont = true;
            this.cboCoQuanNhan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanNhan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCoQuanNhan.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCoQuanNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCoQuanNhan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboCoQuanNhan.Properties.DisplayMember = "TEN";
            this.cboCoQuanNhan.Properties.DropDownItemHeight = 40;
            this.cboCoQuanNhan.Properties.NullText = "";
            this.cboCoQuanNhan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCoQuanNhan.Properties.ShowHeader = false;
            this.cboCoQuanNhan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCoQuanNhan.Properties.ValueMember = "MA";
            this.cboCoQuanNhan.Size = new System.Drawing.Size(248, 26);
            this.cboCoQuanNhan.TabIndex = 11;
            // 
            // lblCoQuanNhan
            // 
            this.lblCoQuanNhan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanNhan.Location = new System.Drawing.Point(685, 40);
            this.lblCoQuanNhan.Margin = new System.Windows.Forms.Padding(4);
            this.lblCoQuanNhan.Name = "lblCoQuanNhan";
            this.lblCoQuanNhan.Size = new System.Drawing.Size(62, 19);
            this.lblCoQuanNhan.TabIndex = 10;
            this.lblCoQuanNhan.Text = "CQ nhận";
            // 
            // txtFile
            // 
            this.txtFile.EnterMoveNextControl = true;
            this.txtFile.Location = new System.Drawing.Point(83, 101);
            this.txtFile.Name = "txtFile";
            this.txtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Properties.Appearance.Options.UseFont = true;
            this.txtFile.Properties.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(185, 26);
            this.txtFile.TabIndex = 19;
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(275, 102);
            this.btnFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(56, 25);
            this.btnFile.TabIndex = 20;
            this.btnFile.Text = "....";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblFileVanBan
            // 
            this.lblFileVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileVanBan.Location = new System.Drawing.Point(6, 104);
            this.lblFileVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileVanBan.Name = "lblFileVanBan";
            this.lblFileVanBan.Size = new System.Drawing.Size(48, 19);
            this.lblFileVanBan.TabIndex = 18;
            this.lblFileVanBan.Text = "File VB";
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiDung.Location = new System.Drawing.Point(341, 40);
            this.lblNoiDung.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(65, 19);
            this.lblNoiDung.TabIndex = 8;
            this.lblNoiDung.Text = "Nội dung";
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(418, 37);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Size = new System.Drawing.Size(248, 26);
            this.txtNoiDung.TabIndex = 9;
            // 
            // dtpNgayBanHanh
            // 
            this.dtpNgayBanHanh.EditValue = null;
            this.dtpNgayBanHanh.EnterMoveNextControl = true;
            this.dtpNgayBanHanh.Location = new System.Drawing.Point(418, 5);
            this.dtpNgayBanHanh.Name = "dtpNgayBanHanh";
            this.dtpNgayBanHanh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBanHanh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayBanHanh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBanHanh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayBanHanh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayBanHanh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBanHanh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBanHanh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayBanHanh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayBanHanh.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayBanHanh.TabIndex = 3;
            // 
            // dtpNgayGui
            // 
            this.dtpNgayGui.EditValue = null;
            this.dtpNgayGui.EnterMoveNextControl = true;
            this.dtpNgayGui.Location = new System.Drawing.Point(83, 69);
            this.dtpNgayGui.Name = "dtpNgayGui";
            this.dtpNgayGui.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayGui.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayGui.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayGui.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayGui.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayGui.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayGui.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayGui.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayGui.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayGui.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayGui.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayGui.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayGui.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayGui.TabIndex = 13;
            // 
            // txtSoKyHieu
            // 
            this.txtSoKyHieu.EnterMoveNextControl = true;
            this.txtSoKyHieu.Location = new System.Drawing.Point(83, 5);
            this.txtSoKyHieu.Name = "txtSoKyHieu";
            this.txtSoKyHieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKyHieu.Properties.Appearance.Options.UseFont = true;
            this.txtSoKyHieu.Size = new System.Drawing.Size(248, 26);
            this.txtSoKyHieu.TabIndex = 1;
            // 
            // cboNhanVienXuLy
            // 
            this.cboNhanVienXuLy.EnterMoveNextControl = true;
            this.cboNhanVienXuLy.Location = new System.Drawing.Point(787, 101);
            this.cboNhanVienXuLy.Name = "cboNhanVienXuLy";
            this.cboNhanVienXuLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienXuLy.Properties.Appearance.Options.UseFont = true;
            this.cboNhanVienXuLy.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanVienXuLy.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanVienXuLy.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhanVienXuLy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanVienXuLy.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboNhanVienXuLy.Properties.DisplayMember = "TEN";
            this.cboNhanVienXuLy.Properties.DropDownItemHeight = 40;
            this.cboNhanVienXuLy.Properties.NullText = "";
            this.cboNhanVienXuLy.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanVienXuLy.Properties.ShowHeader = false;
            this.cboNhanVienXuLy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhanVienXuLy.Properties.ValueMember = "MA";
            this.cboNhanVienXuLy.Size = new System.Drawing.Size(248, 26);
            this.cboNhanVienXuLy.TabIndex = 24;
            // 
            // lblNhanVienXuLy
            // 
            this.lblNhanVienXuLy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhanVienXuLy.Location = new System.Drawing.Point(685, 104);
            this.lblNhanVienXuLy.Margin = new System.Windows.Forms.Padding(4);
            this.lblNhanVienXuLy.Name = "lblNhanVienXuLy";
            this.lblNhanVienXuLy.Size = new System.Drawing.Size(61, 19);
            this.lblNhanVienXuLy.TabIndex = 23;
            this.lblNhanVienXuLy.Text = "NV xử lý";
            // 
            // cboCoQuanBanHanh
            // 
            this.cboCoQuanBanHanh.EnterMoveNextControl = true;
            this.cboCoQuanBanHanh.Location = new System.Drawing.Point(787, 5);
            this.cboCoQuanBanHanh.Name = "cboCoQuanBanHanh";
            this.cboCoQuanBanHanh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanBanHanh.Properties.Appearance.Options.UseFont = true;
            this.cboCoQuanBanHanh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanBanHanh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCoQuanBanHanh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCoQuanBanHanh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCoQuanBanHanh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboCoQuanBanHanh.Properties.DisplayMember = "TEN";
            this.cboCoQuanBanHanh.Properties.DropDownItemHeight = 40;
            this.cboCoQuanBanHanh.Properties.NullText = "";
            this.cboCoQuanBanHanh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCoQuanBanHanh.Properties.ShowHeader = false;
            this.cboCoQuanBanHanh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCoQuanBanHanh.Properties.ValueMember = "MA";
            this.cboCoQuanBanHanh.Size = new System.Drawing.Size(248, 26);
            this.cboCoQuanBanHanh.TabIndex = 5;
            // 
            // cboSoVanBan
            // 
            this.cboSoVanBan.EnterMoveNextControl = true;
            this.cboSoVanBan.Location = new System.Drawing.Point(418, 69);
            this.cboSoVanBan.Name = "cboSoVanBan";
            this.cboSoVanBan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSoVanBan.Properties.Appearance.Options.UseFont = true;
            this.cboSoVanBan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSoVanBan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboSoVanBan.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboSoVanBan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboSoVanBan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboSoVanBan.Properties.DisplayMember = "TEN";
            this.cboSoVanBan.Properties.DropDownItemHeight = 40;
            this.cboSoVanBan.Properties.NullText = "";
            this.cboSoVanBan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboSoVanBan.Properties.ShowHeader = false;
            this.cboSoVanBan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboSoVanBan.Properties.ValueMember = "MA";
            this.cboSoVanBan.Size = new System.Drawing.Size(248, 26);
            this.cboSoVanBan.TabIndex = 15;
            // 
            // lblSoVanBan
            // 
            this.lblSoVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanBan.Location = new System.Drawing.Point(341, 72);
            this.lblSoVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoVanBan.Name = "lblSoVanBan";
            this.lblSoVanBan.Size = new System.Drawing.Size(42, 19);
            this.lblSoVanBan.TabIndex = 14;
            this.lblSoVanBan.Text = "Sổ VB";
            // 
            // cboLoaiVanBan
            // 
            this.cboLoaiVanBan.EnterMoveNextControl = true;
            this.cboLoaiVanBan.Location = new System.Drawing.Point(83, 37);
            this.cboLoaiVanBan.Name = "cboLoaiVanBan";
            this.cboLoaiVanBan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiVanBan.Properties.Appearance.Options.UseFont = true;
            this.cboLoaiVanBan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiVanBan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoaiVanBan.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoaiVanBan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoaiVanBan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboLoaiVanBan.Properties.DisplayMember = "TEN";
            this.cboLoaiVanBan.Properties.DropDownItemHeight = 40;
            this.cboLoaiVanBan.Properties.NullText = "";
            this.cboLoaiVanBan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoaiVanBan.Properties.ShowHeader = false;
            this.cboLoaiVanBan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoaiVanBan.Properties.ValueMember = "MA";
            this.cboLoaiVanBan.Size = new System.Drawing.Size(248, 26);
            this.cboLoaiVanBan.TabIndex = 7;
            // 
            // lblLoaiVanBan
            // 
            this.lblLoaiVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiVanBan.Location = new System.Drawing.Point(6, 40);
            this.lblLoaiVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoaiVanBan.Name = "lblLoaiVanBan";
            this.lblLoaiVanBan.Size = new System.Drawing.Size(53, 19);
            this.lblLoaiVanBan.TabIndex = 6;
            this.lblLoaiVanBan.Text = "Loại VB";
            // 
            // lblCoQuanBanHanh
            // 
            this.lblCoQuanBanHanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanBanHanh.Location = new System.Drawing.Point(685, 8);
            this.lblCoQuanBanHanh.Margin = new System.Windows.Forms.Padding(4);
            this.lblCoQuanBanHanh.Name = "lblCoQuanBanHanh";
            this.lblCoQuanBanHanh.Size = new System.Drawing.Size(93, 19);
            this.lblCoQuanBanHanh.TabIndex = 4;
            this.lblCoQuanBanHanh.Text = "CQ ban hành";
            // 
            // lblSoGui
            // 
            this.lblSoGui.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGui.Location = new System.Drawing.Point(685, 72);
            this.lblSoGui.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoGui.Name = "lblSoGui";
            this.lblSoGui.Size = new System.Drawing.Size(46, 19);
            this.lblSoGui.TabIndex = 16;
            this.lblSoGui.Text = "Số gửi";
            // 
            // txtSoGui
            // 
            this.txtSoGui.EnterMoveNextControl = true;
            this.txtSoGui.Location = new System.Drawing.Point(787, 69);
            this.txtSoGui.Name = "txtSoGui";
            this.txtSoGui.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGui.Properties.Appearance.Options.UseFont = true;
            this.txtSoGui.Size = new System.Drawing.Size(248, 26);
            this.txtSoGui.TabIndex = 17;
            // 
            // lblSoKyHieu
            // 
            this.lblSoKyHieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoKyHieu.Location = new System.Drawing.Point(6, 8);
            this.lblSoKyHieu.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoKyHieu.Name = "lblSoKyHieu";
            this.lblSoKyHieu.Size = new System.Drawing.Size(54, 19);
            this.lblSoKyHieu.TabIndex = 0;
            this.lblSoKyHieu.Text = "Số / KH";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(83, 134);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 25;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(259, 134);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 27;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(347, 134);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 28;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(171, 134);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 26;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNgayBanHanh
            // 
            this.lblNgayBanHanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayBanHanh.Location = new System.Drawing.Point(341, 8);
            this.lblNgayBanHanh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayBanHanh.Name = "lblNgayBanHanh";
            this.lblNgayBanHanh.Size = new System.Drawing.Size(61, 19);
            this.lblNgayBanHanh.TabIndex = 2;
            this.lblNgayBanHanh.Text = "Ngày BH";
            // 
            // lblNgayGui
            // 
            this.lblNgayGui.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayGui.Location = new System.Drawing.Point(6, 72);
            this.lblNgayGui.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayGui.Name = "lblNgayGui";
            this.lblNgayGui.Size = new System.Drawing.Size(64, 19);
            this.lblNgayGui.TabIndex = 12;
            this.lblNgayGui.Text = "Ngày gửi";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 175);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 476);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILEVANBAN,
            this.CQBH_ID,
            this.LOAIVB_ID,
            this.SOVB_ID,
            this.CQNHAN_ID,
            this.NHANVIENXULY_ID,
            this.STT,
            this.MA,
            this.SO_KYHIEU,
            this.NGAYBANHANH,
            this.CQBH,
            this.LOAIVB,
            this.NOIDUNG,
            this.SOVB,
            this.SODEN,
            this.NGAYDEN,
            this.CQNHAN,
            this.NHANVIENXULY,
            this.NAME_FILE,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILEVANBAN
            // 
            this.FILEVANBAN.FieldName = "FILEVANBAN";
            this.FILEVANBAN.Name = "FILEVANBAN";
            this.FILEVANBAN.OptionsColumn.AllowEdit = false;
            this.FILEVANBAN.OptionsColumn.AllowFocus = false;
            this.FILEVANBAN.OptionsColumn.FixedWidth = true;
            // 
            // CQBH_ID
            // 
            this.CQBH_ID.FieldName = "CQBH_ID";
            this.CQBH_ID.Name = "CQBH_ID";
            this.CQBH_ID.OptionsColumn.AllowEdit = false;
            this.CQBH_ID.OptionsColumn.AllowFocus = false;
            this.CQBH_ID.OptionsColumn.AllowMove = false;
            // 
            // LOAIVB_ID
            // 
            this.LOAIVB_ID.FieldName = "LOAIVB_ID";
            this.LOAIVB_ID.Name = "LOAIVB_ID";
            this.LOAIVB_ID.OptionsColumn.AllowEdit = false;
            this.LOAIVB_ID.OptionsColumn.AllowFocus = false;
            this.LOAIVB_ID.OptionsColumn.AllowMove = false;
            // 
            // SOVB_ID
            // 
            this.SOVB_ID.FieldName = "SOVB_ID";
            this.SOVB_ID.Name = "SOVB_ID";
            this.SOVB_ID.OptionsColumn.AllowEdit = false;
            this.SOVB_ID.OptionsColumn.AllowFocus = false;
            this.SOVB_ID.OptionsColumn.AllowMove = false;
            // 
            // CQNHAN_ID
            // 
            this.CQNHAN_ID.FieldName = "CQNHAN_ID";
            this.CQNHAN_ID.Name = "CQNHAN_ID";
            this.CQNHAN_ID.OptionsColumn.AllowEdit = false;
            this.CQNHAN_ID.OptionsColumn.AllowFocus = false;
            this.CQNHAN_ID.OptionsColumn.FixedWidth = true;
            // 
            // NHANVIENXULY_ID
            // 
            this.NHANVIENXULY_ID.FieldName = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.Name = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // SO_KYHIEU
            // 
            this.SO_KYHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SO_KYHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SO_KYHIEU.AppearanceHeader.Options.UseFont = true;
            this.SO_KYHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SO_KYHIEU.Caption = "Số / Ký hiệu";
            this.SO_KYHIEU.FieldName = "SO_KYHIEU";
            this.SO_KYHIEU.Name = "SO_KYHIEU";
            this.SO_KYHIEU.OptionsColumn.AllowEdit = false;
            this.SO_KYHIEU.OptionsColumn.AllowFocus = false;
            this.SO_KYHIEU.OptionsColumn.FixedWidth = true;
            this.SO_KYHIEU.Visible = true;
            this.SO_KYHIEU.VisibleIndex = 2;
            this.SO_KYHIEU.Width = 150;
            // 
            // NGAYBANHANH
            // 
            this.NGAYBANHANH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYBANHANH.AppearanceHeader.Options.UseFont = true;
            this.NGAYBANHANH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.Caption = "Ngày BH";
            this.NGAYBANHANH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYBANHANH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYBANHANH.FieldName = "NGAYBANHANH";
            this.NGAYBANHANH.Name = "NGAYBANHANH";
            this.NGAYBANHANH.OptionsColumn.AllowEdit = false;
            this.NGAYBANHANH.OptionsColumn.AllowFocus = false;
            this.NGAYBANHANH.OptionsColumn.FixedWidth = true;
            this.NGAYBANHANH.Visible = true;
            this.NGAYBANHANH.VisibleIndex = 3;
            this.NGAYBANHANH.Width = 100;
            // 
            // CQBH
            // 
            this.CQBH.AppearanceCell.Options.UseTextOptions = true;
            this.CQBH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CQBH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CQBH.AppearanceHeader.Options.UseFont = true;
            this.CQBH.AppearanceHeader.Options.UseTextOptions = true;
            this.CQBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CQBH.Caption = "CQ ban hành";
            this.CQBH.FieldName = "CQBH";
            this.CQBH.Name = "CQBH";
            this.CQBH.OptionsColumn.AllowEdit = false;
            this.CQBH.OptionsColumn.AllowFocus = false;
            this.CQBH.OptionsColumn.FixedWidth = true;
            this.CQBH.Visible = true;
            this.CQBH.VisibleIndex = 4;
            this.CQBH.Width = 250;
            // 
            // LOAIVB
            // 
            this.LOAIVB.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIVB.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIVB.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIVB.AppearanceHeader.Options.UseFont = true;
            this.LOAIVB.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIVB.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIVB.Caption = "Loại văn bản";
            this.LOAIVB.FieldName = "LOAIVB";
            this.LOAIVB.Name = "LOAIVB";
            this.LOAIVB.OptionsColumn.AllowEdit = false;
            this.LOAIVB.OptionsColumn.AllowFocus = false;
            this.LOAIVB.OptionsColumn.FixedWidth = true;
            this.LOAIVB.Visible = true;
            this.LOAIVB.VisibleIndex = 5;
            this.LOAIVB.Width = 150;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 6;
            this.NOIDUNG.Width = 300;
            // 
            // SOVB
            // 
            this.SOVB.AppearanceCell.Options.UseTextOptions = true;
            this.SOVB.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOVB.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOVB.AppearanceHeader.Options.UseFont = true;
            this.SOVB.AppearanceHeader.Options.UseTextOptions = true;
            this.SOVB.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOVB.Caption = "Sổ văn bản";
            this.SOVB.FieldName = "SOVB";
            this.SOVB.Name = "SOVB";
            this.SOVB.OptionsColumn.AllowEdit = false;
            this.SOVB.OptionsColumn.AllowFocus = false;
            this.SOVB.OptionsColumn.FixedWidth = true;
            this.SOVB.Visible = true;
            this.SOVB.VisibleIndex = 7;
            this.SOVB.Width = 150;
            // 
            // SODEN
            // 
            this.SODEN.AppearanceCell.Options.UseTextOptions = true;
            this.SODEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SODEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SODEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SODEN.AppearanceHeader.Options.UseFont = true;
            this.SODEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SODEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SODEN.Caption = "Số gửi";
            this.SODEN.FieldName = "SODEN";
            this.SODEN.Name = "SODEN";
            this.SODEN.OptionsColumn.AllowEdit = false;
            this.SODEN.OptionsColumn.AllowFocus = false;
            this.SODEN.OptionsColumn.FixedWidth = true;
            this.SODEN.Visible = true;
            this.SODEN.VisibleIndex = 8;
            this.SODEN.Width = 70;
            // 
            // NGAYDEN
            // 
            this.NGAYDEN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYDEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYDEN.AppearanceHeader.Options.UseFont = true;
            this.NGAYDEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYDEN.Caption = "Ngày gửi";
            this.NGAYDEN.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYDEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYDEN.FieldName = "NGAYDEN";
            this.NGAYDEN.Name = "NGAYDEN";
            this.NGAYDEN.OptionsColumn.AllowEdit = false;
            this.NGAYDEN.OptionsColumn.AllowFocus = false;
            this.NGAYDEN.OptionsColumn.FixedWidth = true;
            this.NGAYDEN.Visible = true;
            this.NGAYDEN.VisibleIndex = 9;
            this.NGAYDEN.Width = 100;
            // 
            // CQNHAN
            // 
            this.CQNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CQNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CQNHAN.AppearanceHeader.Options.UseFont = true;
            this.CQNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CQNHAN.Caption = "CQ nhận";
            this.CQNHAN.FieldName = "CQNHAN";
            this.CQNHAN.Name = "CQNHAN";
            this.CQNHAN.OptionsColumn.AllowEdit = false;
            this.CQNHAN.OptionsColumn.AllowFocus = false;
            this.CQNHAN.OptionsColumn.FixedWidth = true;
            this.CQNHAN.Visible = true;
            this.CQNHAN.VisibleIndex = 11;
            this.CQNHAN.Width = 150;
            // 
            // NHANVIENXULY
            // 
            this.NHANVIENXULY.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIENXULY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIENXULY.AppearanceHeader.Options.UseFont = true;
            this.NHANVIENXULY.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIENXULY.Caption = "NV Xử lý";
            this.NHANVIENXULY.FieldName = "NHANVIENXULY";
            this.NHANVIENXULY.Name = "NHANVIENXULY";
            this.NHANVIENXULY.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY.OptionsColumn.FixedWidth = true;
            this.NHANVIENXULY.Visible = true;
            this.NHANVIENXULY.VisibleIndex = 10;
            this.NHANVIENXULY.Width = 200;
            // 
            // NAME_FILE
            // 
            this.NAME_FILE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAME_FILE.AppearanceHeader.Options.UseFont = true;
            this.NAME_FILE.AppearanceHeader.Options.UseTextOptions = true;
            this.NAME_FILE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAME_FILE.Caption = "Tên file";
            this.NAME_FILE.FieldName = "NAME_FILE";
            this.NAME_FILE.Name = "NAME_FILE";
            this.NAME_FILE.OptionsColumn.AllowEdit = false;
            this.NAME_FILE.OptionsColumn.AllowFocus = false;
            this.NAME_FILE.OptionsColumn.FixedWidth = true;
            this.NAME_FILE.Visible = true;
            this.NAME_FILE.VisibleIndex = 12;
            this.NAME_FILE.Width = 100;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 13;
            this.FILL.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 175);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 476);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 312);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 152);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 232);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 394);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_CongVanDi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_CongVanDi";
            this.Text = "Công văn đi";
            this.Load += new System.EventHandler(this.Frm_CongVanDi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayGui.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayGui.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoKyHieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanVienXuLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanBanHanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSoVanBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiVanBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGui.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYBANHANH;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYDEN;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.LabelControl lblNgayBanHanh;
        private DevExpress.XtraEditors.LabelControl lblNgayGui;
        private DevExpress.XtraGrid.Columns.GridColumn CQBH;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVB;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn SOVB;
        private DevExpress.XtraGrid.Columns.GridColumn SODEN;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn SO_KYHIEU;
        private DevExpress.XtraEditors.LabelControl lblSoKyHieu;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboLoaiVanBan;
        private DevExpress.XtraEditors.LabelControl lblLoaiVanBan;
        private DevExpress.XtraEditors.LabelControl lblCoQuanBanHanh;
        private DevExpress.XtraEditors.LabelControl lblSoGui;
        private DevExpress.XtraEditors.TextEdit txtSoGui;
        private DevExpress.XtraEditors.LookUpEdit cboSoVanBan;
        private DevExpress.XtraEditors.LabelControl lblSoVanBan;
        private DevExpress.XtraEditors.LookUpEdit cboNhanVienXuLy;
        private DevExpress.XtraEditors.LabelControl lblNhanVienXuLy;
        private DevExpress.XtraEditors.LookUpEdit cboCoQuanBanHanh;
        private DevExpress.XtraEditors.DateEdit dtpNgayBanHanh;
        private DevExpress.XtraEditors.DateEdit dtpNgayGui;
        private DevExpress.XtraEditors.TextEdit txtSoKyHieu;
        private DevExpress.XtraEditors.LabelControl lblNoiDung;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraEditors.LabelControl lblFileVanBan;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraGrid.Columns.GridColumn CQBH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn SOVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY;
        private DevExpress.XtraGrid.Columns.GridColumn CQNHAN;
        private DevExpress.XtraEditors.TextEdit txtFile;
        private DevExpress.XtraGrid.Columns.GridColumn FILEVANBAN;
        private DevExpress.XtraGrid.Columns.GridColumn NAME_FILE;
        private DevExpress.XtraEditors.LookUpEdit cboCoQuanNhan;
        private DevExpress.XtraEditors.LabelControl lblCoQuanNhan;
        private DevExpress.XtraGrid.Columns.GridColumn CQNHAN_ID;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.TextEdit txtTenFile;




    }
}