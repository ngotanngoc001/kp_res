﻿namespace KP_RES 
{
    partial class Frm_BienBan_GiaoNhan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itNhapBienBan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itDanhSachBienBan = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 56;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itNhapBienBan);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itNhapBienBan
            // 
            this.itNhapBienBan.BackgroundImage = global::KP_RES.Properties.Resources.phuchoi;
            this.itNhapBienBan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Nhập biên bản";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhapBienBan.Elements.Add(tileItemElement1);
            this.itNhapBienBan.Id = 15;
            this.itNhapBienBan.IsLarge = true;
            this.itNhapBienBan.Name = "itNhapBienBan";
            this.itNhapBienBan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhapBienBan_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itDanhSachBienBan);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itDanhSachBienBan
            // 
            this.itDanhSachBienBan.BackgroundImage = global::KP_RES.Properties.Resources.thethanhtoan;
            this.itDanhSachBienBan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Danh sách biên bản";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDanhSachBienBan.Elements.Add(tileItemElement2);
            this.itDanhSachBienBan.Id = 53;
            this.itDanhSachBienBan.IsLarge = true;
            this.itDanhSachBienBan.Name = "itDanhSachBienBan";
            this.itDanhSachBienBan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDanhSachBienBan_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_BienBan_GiaoNhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_BienBan_GiaoNhan";
            this.Text = "Biên bản giao nhận";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_BienBan_GiaoNhan_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.PanelControl panelControl1;

        private DevExpress.XtraEditors.TileItem itNhapBienBan;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itDanhSachBienBan;
    }
}