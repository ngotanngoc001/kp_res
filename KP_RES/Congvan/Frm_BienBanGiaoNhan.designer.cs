﻿namespace KP_RES 
{
    partial class Frm_BienBanGiaoNhan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BienBanGiaoNhan));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayBanHanh = new DevExpress.XtraEditors.DateEdit();
            this.txtChucVuBN = new DevExpress.XtraEditors.TextEdit();
            this.txtBenNhan = new DevExpress.XtraEditors.TextEdit();
            this.txtChucVuBG = new DevExpress.XtraEditors.TextEdit();
            this.txtBenGiao = new DevExpress.XtraEditors.TextEdit();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.txtFile = new DevExpress.XtraEditors.TextEdit();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.lblFileVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblThucHien = new DevExpress.XtraEditors.LabelControl();
            this.lblNoiDung = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgayNhan = new DevExpress.XtraEditors.DateEdit();
            this.txtSoKyHieu = new DevExpress.XtraEditors.TextEdit();
            this.lblSoVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblLoaiVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblCoQuanBanHanh = new DevExpress.XtraEditors.LabelControl();
            this.lblSoKyHieu = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayBanHanh = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayDen = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILEVANBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BOPHANXYLY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQBH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COQUANCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BANCHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BANPHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHETHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.XOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENGIAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAME_FILE = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVuBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVuBG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayNhan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoKyHieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.dtpNgayBanHanh);
            this.panelControl1.Controls.Add(this.txtChucVuBN);
            this.panelControl1.Controls.Add(this.txtBenNhan);
            this.panelControl1.Controls.Add(this.txtChucVuBG);
            this.panelControl1.Controls.Add(this.txtBenGiao);
            this.panelControl1.Controls.Add(this.txtTenFile);
            this.panelControl1.Controls.Add(this.lblTenFile);
            this.panelControl1.Controls.Add(this.txtFile);
            this.panelControl1.Controls.Add(this.btnFile);
            this.panelControl1.Controls.Add(this.lblFileVanBan);
            this.panelControl1.Controls.Add(this.lblThucHien);
            this.panelControl1.Controls.Add(this.lblNoiDung);
            this.panelControl1.Controls.Add(this.dtpNgayNhan);
            this.panelControl1.Controls.Add(this.txtSoKyHieu);
            this.panelControl1.Controls.Add(this.lblSoVanBan);
            this.panelControl1.Controls.Add(this.lblLoaiVanBan);
            this.panelControl1.Controls.Add(this.lblCoQuanBanHanh);
            this.panelControl1.Controls.Add(this.lblSoKyHieu);
            this.panelControl1.Controls.Add(this.lblNgayBanHanh);
            this.panelControl1.Controls.Add(this.lblNgayDen);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 136);
            this.panelControl1.TabIndex = 0;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(83, 101);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Size = new System.Drawing.Size(903, 26);
            this.txtNoiDung.TabIndex = 20;
            // 
            // dtpNgayBanHanh
            // 
            this.dtpNgayBanHanh.EditValue = null;
            this.dtpNgayBanHanh.EnterMoveNextControl = true;
            this.dtpNgayBanHanh.Location = new System.Drawing.Point(83, 37);
            this.dtpNgayBanHanh.Name = "dtpNgayBanHanh";
            this.dtpNgayBanHanh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBanHanh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayBanHanh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBanHanh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayBanHanh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayBanHanh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBanHanh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayBanHanh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayBanHanh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayBanHanh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayBanHanh.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayBanHanh.TabIndex = 3;
            // 
            // txtChucVuBN
            // 
            this.txtChucVuBN.EnterMoveNextControl = true;
            this.txtChucVuBN.Location = new System.Drawing.Point(738, 37);
            this.txtChucVuBN.Name = "txtChucVuBN";
            this.txtChucVuBN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVuBN.Properties.Appearance.Options.UseFont = true;
            this.txtChucVuBN.Size = new System.Drawing.Size(248, 26);
            this.txtChucVuBN.TabIndex = 13;
            // 
            // txtBenNhan
            // 
            this.txtBenNhan.EnterMoveNextControl = true;
            this.txtBenNhan.Location = new System.Drawing.Point(418, 37);
            this.txtBenNhan.Name = "txtBenNhan";
            this.txtBenNhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBenNhan.Properties.Appearance.Options.UseFont = true;
            this.txtBenNhan.Size = new System.Drawing.Size(248, 26);
            this.txtBenNhan.TabIndex = 11;
            // 
            // txtChucVuBG
            // 
            this.txtChucVuBG.EnterMoveNextControl = true;
            this.txtChucVuBG.Location = new System.Drawing.Point(738, 5);
            this.txtChucVuBG.Name = "txtChucVuBG";
            this.txtChucVuBG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVuBG.Properties.Appearance.Options.UseFont = true;
            this.txtChucVuBG.Size = new System.Drawing.Size(248, 26);
            this.txtChucVuBG.TabIndex = 9;
            // 
            // txtBenGiao
            // 
            this.txtBenGiao.EnterMoveNextControl = true;
            this.txtBenGiao.Location = new System.Drawing.Point(418, 5);
            this.txtBenGiao.Name = "txtBenGiao";
            this.txtBenGiao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBenGiao.Properties.Appearance.Options.UseFont = true;
            this.txtBenGiao.Size = new System.Drawing.Size(248, 26);
            this.txtBenGiao.TabIndex = 7;
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(738, 69);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 18;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(673, 72);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 17;
            this.lblTenFile.Text = "Tên File";
            // 
            // txtFile
            // 
            this.txtFile.EnterMoveNextControl = true;
            this.txtFile.Location = new System.Drawing.Point(418, 69);
            this.txtFile.Name = "txtFile";
            this.txtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Properties.Appearance.Options.UseFont = true;
            this.txtFile.Properties.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(185, 26);
            this.txtFile.TabIndex = 15;
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(610, 70);
            this.btnFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(56, 25);
            this.btnFile.TabIndex = 16;
            this.btnFile.Text = "....";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblFileVanBan
            // 
            this.lblFileVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileVanBan.Location = new System.Drawing.Point(338, 72);
            this.lblFileVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileVanBan.Name = "lblFileVanBan";
            this.lblFileVanBan.Size = new System.Drawing.Size(48, 19);
            this.lblFileVanBan.TabIndex = 14;
            this.lblFileVanBan.Text = "File VB";
            // 
            // lblThucHien
            // 
            this.lblThucHien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThucHien.Location = new System.Drawing.Point(673, 40);
            this.lblThucHien.Margin = new System.Windows.Forms.Padding(4);
            this.lblThucHien.Name = "lblThucHien";
            this.lblThucHien.Size = new System.Drawing.Size(58, 19);
            this.lblThucHien.TabIndex = 12;
            this.lblThucHien.Text = "Chức vụ";
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiDung.Location = new System.Drawing.Point(338, 40);
            this.lblNoiDung.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(66, 19);
            this.lblNoiDung.TabIndex = 10;
            this.lblNoiDung.Text = "Bên nhận";
            // 
            // dtpNgayNhan
            // 
            this.dtpNgayNhan.EditValue = null;
            this.dtpNgayNhan.EnterMoveNextControl = true;
            this.dtpNgayNhan.Location = new System.Drawing.Point(83, 69);
            this.dtpNgayNhan.Name = "dtpNgayNhan";
            this.dtpNgayNhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayNhan.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayNhan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayNhan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayNhan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayNhan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayNhan.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayNhan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayNhan.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayNhan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayNhan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayNhan.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayNhan.TabIndex = 5;
            // 
            // txtSoKyHieu
            // 
            this.txtSoKyHieu.EnterMoveNextControl = true;
            this.txtSoKyHieu.Location = new System.Drawing.Point(83, 5);
            this.txtSoKyHieu.Name = "txtSoKyHieu";
            this.txtSoKyHieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKyHieu.Properties.Appearance.Options.UseFont = true;
            this.txtSoKyHieu.Size = new System.Drawing.Size(248, 26);
            this.txtSoKyHieu.TabIndex = 1;
            // 
            // lblSoVanBan
            // 
            this.lblSoVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanBan.Location = new System.Drawing.Point(6, 104);
            this.lblSoVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoVanBan.Name = "lblSoVanBan";
            this.lblSoVanBan.Size = new System.Drawing.Size(65, 19);
            this.lblSoVanBan.TabIndex = 19;
            this.lblSoVanBan.Text = "Nội dung";
            // 
            // lblLoaiVanBan
            // 
            this.lblLoaiVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiVanBan.Location = new System.Drawing.Point(673, 8);
            this.lblLoaiVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoaiVanBan.Name = "lblLoaiVanBan";
            this.lblLoaiVanBan.Size = new System.Drawing.Size(58, 19);
            this.lblLoaiVanBan.TabIndex = 8;
            this.lblLoaiVanBan.Text = "Chức vụ";
            // 
            // lblCoQuanBanHanh
            // 
            this.lblCoQuanBanHanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanBanHanh.Location = new System.Drawing.Point(338, 12);
            this.lblCoQuanBanHanh.Margin = new System.Windows.Forms.Padding(4);
            this.lblCoQuanBanHanh.Name = "lblCoQuanBanHanh";
            this.lblCoQuanBanHanh.Size = new System.Drawing.Size(61, 19);
            this.lblCoQuanBanHanh.TabIndex = 6;
            this.lblCoQuanBanHanh.Text = "Bên giao";
            // 
            // lblSoKyHieu
            // 
            this.lblSoKyHieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoKyHieu.Location = new System.Drawing.Point(6, 8);
            this.lblSoKyHieu.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoKyHieu.Name = "lblSoKyHieu";
            this.lblSoKyHieu.Size = new System.Drawing.Size(54, 19);
            this.lblSoKyHieu.TabIndex = 0;
            this.lblSoKyHieu.Text = "Số / KH";
            // 
            // lblNgayBanHanh
            // 
            this.lblNgayBanHanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayBanHanh.Location = new System.Drawing.Point(6, 40);
            this.lblNgayBanHanh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayBanHanh.Name = "lblNgayBanHanh";
            this.lblNgayBanHanh.Size = new System.Drawing.Size(61, 19);
            this.lblNgayBanHanh.TabIndex = 2;
            this.lblNgayBanHanh.Text = "Ngày BH";
            // 
            // lblNgayDen
            // 
            this.lblNgayDen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDen.Location = new System.Drawing.Point(6, 72);
            this.lblNgayDen.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayDen.Name = "lblNgayDen";
            this.lblNgayDen.Size = new System.Drawing.Size(76, 19);
            this.lblNgayDen.TabIndex = 4;
            this.lblNgayDen.Text = "Ngày nhận";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnLuu);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 611);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1258, 40);
            this.panelControl2.TabIndex = 2;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(1143, 2);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(113, 36);
            this.btnLuu.TabIndex = 36;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 136);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1258, 475);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILEVANBAN,
            this.BOPHANXYLY,
            this.CQBH_ID,
            this.LOAIVB_ID,
            this.SOVB_ID,
            this.NHANVIENXULY_ID,
            this.TRANGTHAI_ID,
            this.STT,
            this.TEN,
            this.COQUANCAP,
            this.SOHIEU,
            this.BANCHINH,
            this.BANPHU,
            this.NGAYCAP,
            this.NGAYHETHAN,
            this.XOA,
            this.FILL,
            this.TRANGTHAI,
            this.DIENGIAI,
            this.NAME_FILE});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanged);
            // 
            // FILEVANBAN
            // 
            this.FILEVANBAN.FieldName = "FILEVANBAN";
            this.FILEVANBAN.Name = "FILEVANBAN";
            this.FILEVANBAN.OptionsColumn.AllowEdit = false;
            this.FILEVANBAN.OptionsColumn.AllowFocus = false;
            this.FILEVANBAN.OptionsColumn.FixedWidth = true;
            // 
            // BOPHANXYLY
            // 
            this.BOPHANXYLY.FieldName = "BOPHANXYLY";
            this.BOPHANXYLY.Name = "BOPHANXYLY";
            this.BOPHANXYLY.OptionsColumn.AllowEdit = false;
            this.BOPHANXYLY.OptionsColumn.AllowFocus = false;
            this.BOPHANXYLY.OptionsColumn.FixedWidth = true;
            // 
            // CQBH_ID
            // 
            this.CQBH_ID.FieldName = "CQBH_ID";
            this.CQBH_ID.Name = "CQBH_ID";
            this.CQBH_ID.OptionsColumn.AllowEdit = false;
            this.CQBH_ID.OptionsColumn.AllowFocus = false;
            this.CQBH_ID.OptionsColumn.AllowMove = false;
            // 
            // LOAIVB_ID
            // 
            this.LOAIVB_ID.FieldName = "LOAIVB_ID";
            this.LOAIVB_ID.Name = "LOAIVB_ID";
            this.LOAIVB_ID.OptionsColumn.AllowEdit = false;
            this.LOAIVB_ID.OptionsColumn.AllowFocus = false;
            this.LOAIVB_ID.OptionsColumn.AllowMove = false;
            // 
            // SOVB_ID
            // 
            this.SOVB_ID.FieldName = "SOVB_ID";
            this.SOVB_ID.Name = "SOVB_ID";
            this.SOVB_ID.OptionsColumn.AllowEdit = false;
            this.SOVB_ID.OptionsColumn.AllowFocus = false;
            this.SOVB_ID.OptionsColumn.AllowMove = false;
            // 
            // NHANVIENXULY_ID
            // 
            this.NHANVIENXULY_ID.FieldName = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.Name = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowMove = false;
            // 
            // TRANGTHAI_ID
            // 
            this.TRANGTHAI_ID.FieldName = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.Name = "TRANGTHAI_ID";
            this.TRANGTHAI_ID.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI_ID.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Tên giấy tờ";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 1;
            this.TEN.Width = 200;
            // 
            // COQUANCAP
            // 
            this.COQUANCAP.AppearanceCell.Options.UseTextOptions = true;
            this.COQUANCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.COQUANCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.COQUANCAP.AppearanceHeader.Options.UseFont = true;
            this.COQUANCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.COQUANCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.COQUANCAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.COQUANCAP.Caption = "Cơ quan cấp";
            this.COQUANCAP.FieldName = "COQUANCAP";
            this.COQUANCAP.Name = "COQUANCAP";
            this.COQUANCAP.OptionsColumn.FixedWidth = true;
            this.COQUANCAP.Visible = true;
            this.COQUANCAP.VisibleIndex = 2;
            this.COQUANCAP.Width = 200;
            // 
            // SOHIEU
            // 
            this.SOHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SOHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOHIEU.AppearanceHeader.Options.UseFont = true;
            this.SOHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SOHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOHIEU.Caption = "Số hiệu";
            this.SOHIEU.FieldName = "SOHIEU";
            this.SOHIEU.Name = "SOHIEU";
            this.SOHIEU.OptionsColumn.FixedWidth = true;
            this.SOHIEU.Visible = true;
            this.SOHIEU.VisibleIndex = 3;
            this.SOHIEU.Width = 150;
            // 
            // BANCHINH
            // 
            this.BANCHINH.AppearanceCell.Options.UseTextOptions = true;
            this.BANCHINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BANCHINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BANCHINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BANCHINH.AppearanceHeader.Options.UseFont = true;
            this.BANCHINH.AppearanceHeader.Options.UseTextOptions = true;
            this.BANCHINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BANCHINH.Caption = "Số bản chính";
            this.BANCHINH.FieldName = "BANCHINH";
            this.BANCHINH.Name = "BANCHINH";
            this.BANCHINH.OptionsColumn.FixedWidth = true;
            this.BANCHINH.Visible = true;
            this.BANCHINH.VisibleIndex = 4;
            this.BANCHINH.Width = 120;
            // 
            // BANPHU
            // 
            this.BANPHU.AppearanceCell.Options.UseTextOptions = true;
            this.BANPHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BANPHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BANPHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BANPHU.AppearanceHeader.Options.UseFont = true;
            this.BANPHU.AppearanceHeader.Options.UseTextOptions = true;
            this.BANPHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BANPHU.Caption = "Số bản phụ";
            this.BANPHU.FieldName = "BANPHU";
            this.BANPHU.Name = "BANPHU";
            this.BANPHU.OptionsColumn.FixedWidth = true;
            this.BANPHU.Visible = true;
            this.BANPHU.VisibleIndex = 5;
            this.BANPHU.Width = 120;
            // 
            // NGAYCAP
            // 
            this.NGAYCAP.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYCAP.AppearanceHeader.Options.UseFont = true;
            this.NGAYCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCAP.Caption = "Ngày cấp";
            this.NGAYCAP.FieldName = "NGAYCAP";
            this.NGAYCAP.Name = "NGAYCAP";
            this.NGAYCAP.OptionsColumn.FixedWidth = true;
            this.NGAYCAP.Visible = true;
            this.NGAYCAP.VisibleIndex = 6;
            this.NGAYCAP.Width = 150;
            // 
            // NGAYHETHAN
            // 
            this.NGAYHETHAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHETHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHETHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHETHAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYHETHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHETHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHAN.Caption = "Ngày hết hạn";
            this.NGAYHETHAN.FieldName = "NGAYHETHAN";
            this.NGAYHETHAN.Name = "NGAYHETHAN";
            this.NGAYHETHAN.OptionsColumn.FixedWidth = true;
            this.NGAYHETHAN.Visible = true;
            this.NGAYHETHAN.VisibleIndex = 7;
            this.NGAYHETHAN.Width = 150;
            // 
            // XOA
            // 
            this.XOA.AppearanceCell.Options.UseTextOptions = true;
            this.XOA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.XOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.XOA.AppearanceHeader.Options.UseFont = true;
            this.XOA.AppearanceHeader.Options.UseTextOptions = true;
            this.XOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XOA.Caption = "Xóa";
            this.XOA.ColumnEdit = this.repositoryItemButtonEdit1;
            this.XOA.FieldName = "NHANVIENXULY";
            this.XOA.Name = "XOA";
            this.XOA.OptionsColumn.AllowEdit = false;
            this.XOA.OptionsColumn.AllowFocus = false;
            this.XOA.OptionsColumn.FixedWidth = true;
            this.XOA.Visible = true;
            this.XOA.VisibleIndex = 8;
            this.XOA.Width = 80;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 9;
            this.FILL.Width = 36;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Width = 150;
            // 
            // DIENGIAI
            // 
            this.DIENGIAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DIENGIAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENGIAI.AppearanceHeader.Options.UseFont = true;
            this.DIENGIAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENGIAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENGIAI.Caption = "Diễn giải";
            this.DIENGIAI.FieldName = "DIENGIAI";
            this.DIENGIAI.Name = "DIENGIAI";
            this.DIENGIAI.OptionsColumn.AllowEdit = false;
            this.DIENGIAI.OptionsColumn.AllowFocus = false;
            this.DIENGIAI.OptionsColumn.FixedWidth = true;
            this.DIENGIAI.Width = 300;
            // 
            // NAME_FILE
            // 
            this.NAME_FILE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAME_FILE.AppearanceHeader.Options.UseFont = true;
            this.NAME_FILE.AppearanceHeader.Options.UseTextOptions = true;
            this.NAME_FILE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAME_FILE.Caption = "Tên file";
            this.NAME_FILE.FieldName = "NAME_FILE";
            this.NAME_FILE.Name = "NAME_FILE";
            this.NAME_FILE.OptionsColumn.AllowEdit = false;
            this.NAME_FILE.OptionsColumn.AllowFocus = false;
            this.NAME_FILE.OptionsColumn.FixedWidth = true;
            this.NAME_FILE.Width = 100;
            // 
            // Frm_BienBanGiaoNhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_BienBanGiaoNhan";
            this.Text = "Thêm mới BBGN";
            this.Load += new System.EventHandler(this.Frm_BienBanGiaoNhan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayBanHanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVuBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVuBG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBenGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayNhan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoKyHieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblNgayBanHanh;
        private DevExpress.XtraEditors.LabelControl lblNgayDen;
        private DevExpress.XtraEditors.LabelControl lblSoKyHieu;
        private DevExpress.XtraEditors.LabelControl lblLoaiVanBan;
        private DevExpress.XtraEditors.LabelControl lblCoQuanBanHanh;
        private DevExpress.XtraEditors.LabelControl lblSoVanBan;
        private DevExpress.XtraEditors.DateEdit dtpNgayNhan;
        private DevExpress.XtraEditors.TextEdit txtSoKyHieu;
        private DevExpress.XtraEditors.LabelControl lblThucHien;
        private DevExpress.XtraEditors.LabelControl lblNoiDung;
        private DevExpress.XtraEditors.LabelControl lblFileVanBan;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraEditors.TextEdit txtFile;
        private DevExpress.XtraEditors.TextEdit txtTenFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.DateEdit dtpNgayBanHanh;
        private DevExpress.XtraEditors.TextEdit txtChucVuBN;
        private DevExpress.XtraEditors.TextEdit txtBenNhan;
        private DevExpress.XtraEditors.TextEdit txtChucVuBG;
        private DevExpress.XtraEditors.TextEdit txtBenGiao;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn FILEVANBAN;
        private DevExpress.XtraGrid.Columns.GridColumn BOPHANXYLY;
        private DevExpress.XtraGrid.Columns.GridColumn CQBH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn SOVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY_ID;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn COQUANCAP;
        private DevExpress.XtraGrid.Columns.GridColumn SOHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn BANCHINH;
        private DevExpress.XtraGrid.Columns.GridColumn BANPHU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCAP;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHETHAN;
        private DevExpress.XtraGrid.Columns.GridColumn XOA;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENGIAI;
        private DevExpress.XtraGrid.Columns.GridColumn NAME_FILE;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;




    }
}