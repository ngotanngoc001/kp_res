﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_CongVanDi : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_CongVanDi()
        {
            InitializeComponent();
        }

        private void Frm_CongVanDi_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtSoKyHieu.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            SqlParameter[] sParameter = new SqlParameter[2];
            sParameter[0] = new SqlParameter("@MA", (Object)sMa);
            sParameter[1] = new SqlParameter("@TYPE", 2);

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsQLCongTy.ExecuteParameterSPs("SP_DeleteCongVan", sParameter);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtSoKyHieu.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertCongVan";

                sParameter = new SqlParameter[18];
                sParameter[0] = new SqlParameter("@SO_KYHIEU", (Object)txtSoKyHieu.Text);
                sParameter[1] = new SqlParameter("@NGAYBANHANH", (DateTime)dtpNgayBanHanh.EditValue);
                sParameter[2] = new SqlParameter("@CQBH_ID", (Object)cboCoQuanBanHanh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@LOAIVB_ID", (Object)cboLoaiVanBan.EditValue.ToString());
                sParameter[4] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[5] = new SqlParameter("@NGAYTHUCHIEN", (Object)"");
                sParameter[6] = new SqlParameter("@SOVB_ID", (Object)cboSoVanBan.EditValue.ToString());
                sParameter[7] = new SqlParameter("@SODEN", (Object)txtSoGui.Text);
                sParameter[8] = new SqlParameter("@NGAYDEN", (DateTime)dtpNgayGui.EditValue);
                sParameter[9] = new SqlParameter("@BOPHANXYLY", (Object)"");
                sParameter[10] = new SqlParameter("@NHANVIENXULY_ID", (Object)cboNhanVienXuLy.EditValue.ToString());
                sParameter[11] = new SqlParameter("@TRANGTHAI_ID", (Object)"");
                sParameter[12] = new SqlParameter("@DIENGIAI", (Object)"");
                sParameter[13] = new SqlParameter("@NGUOITAO", (Object)clsGlobal.gsUserID);
                sParameter[14] = new SqlParameter("@NAME_FILE", (Object)sNameFile);
                sParameter[15] = new SqlParameter("@FILEVANBAN", SqlDbType.VarBinary, int.MaxValue);
                sParameter[15].Value = sByteDataFile;
                sParameter[16] = new SqlParameter("@CQNHAN_ID", (Object)cboCoQuanNhan.EditValue.ToString());
                sParameter[17] = new SqlParameter("@TYPE", 2);
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateCongVan";

                sParameter = new SqlParameter[19];
                sParameter[0] = new SqlParameter("@SO_KYHIEU", (Object)txtSoKyHieu.Text);
                sParameter[1] = new SqlParameter("@NGAYBANHANH", (DateTime)dtpNgayBanHanh.EditValue);
                sParameter[2] = new SqlParameter("@CQBH_ID", (Object)cboCoQuanBanHanh.EditValue.ToString());
                sParameter[3] = new SqlParameter("@LOAIVB_ID", (Object)cboLoaiVanBan.EditValue.ToString());
                sParameter[4] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[5] = new SqlParameter("@NGAYTHUCHIEN", (Object)"");
                sParameter[6] = new SqlParameter("@SOVB_ID", (Object)cboSoVanBan.EditValue.ToString());
                sParameter[7] = new SqlParameter("@SODEN", (Object)txtSoGui.Text);
                sParameter[8] = new SqlParameter("@NGAYDEN", (DateTime)dtpNgayGui.EditValue);
                sParameter[9] = new SqlParameter("@BOPHANXYLY", (Object)"");
                sParameter[10] = new SqlParameter("@NHANVIENXULY_ID", (Object)cboNhanVienXuLy.EditValue.ToString());
                sParameter[11] = new SqlParameter("@TRANGTHAI_ID", (Object)"");
                sParameter[12] = new SqlParameter("@DIENGIAI", (Object)"");
                sParameter[13] = new SqlParameter("@NGUOISUA", (Object)clsGlobal.gsUserID);
                sParameter[14] = new SqlParameter("@MA", (Object)sMa);
                sParameter[15] = new SqlParameter("@NAME_FILE", (Object)sNameFile);
                sParameter[16] = new SqlParameter("@FILEVANBAN", SqlDbType.VarBinary, int.MaxValue);
                sParameter[16].Value = sByteDataFile;
                sParameter[17] = new SqlParameter("@CQNHAN_ID", (Object)cboCoQuanNhan.EditValue.ToString());
                sParameter[18] = new SqlParameter("@TYPE", 2);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtSoKyHieu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SO_KYHIEU").ToString();
            txtSoGui.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SODEN").ToString();
            txtNoiDung.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NOIDUNG").ToString();
            cboCoQuanBanHanh.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CQBH_ID").ToString());
            cboLoaiVanBan.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAIVB_ID").ToString());
            cboSoVanBan.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOVB_ID").ToString());
            cboNhanVienXuLy.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NHANVIENXULY_ID").ToString());
            cboCoQuanNhan.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CQNHAN_ID").ToString());
            dtpNgayBanHanh.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYBANHANH").ToString());
            dtpNgayGui.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYDEN").ToString());
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAME_FILE").ToString();
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAME_FILE").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILEVANBAN"));

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            SqlParameter[] sParameter = new SqlParameter[1];
            sParameter[0] = new SqlParameter("@TYPE", 2);

            DataTable myDT = clsQLCongTy.ReturnDataTableSPs("SP_SelectCongVan", sParameter);
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoCoQuan");
            clsQLCongTy.LoadCombo(myDT, cboCoQuanBanHanh);
            clsQLCongTy.LoadCombo(myDT, cboCoQuanNhan);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoLoaiVanBan");
            clsQLCongTy.LoadCombo(myDT, cboLoaiVanBan);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoSoVanBan");
            clsQLCongTy.LoadCombo(myDT, cboSoVanBan);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoNhanVien");
            clsQLCongTy.LoadCombo(myDT, cboNhanVienXuLy);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("24031601");
            btnSua.Enabled = clsUserManagement.AllowEdit("24031601");
            btnXoa.Enabled = clsUserManagement.AllowDelete("24031601");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtSoKyHieu.Properties.ReadOnly = sBoolean;
            dtpNgayBanHanh.Properties.ReadOnly = sBoolean;
            cboCoQuanBanHanh.Properties.ReadOnly = sBoolean;
            cboLoaiVanBan.Properties.ReadOnly = sBoolean;
            cboSoVanBan.Properties.ReadOnly = sBoolean;
            txtSoGui.Properties.ReadOnly = sBoolean;
            dtpNgayGui.Properties.ReadOnly = sBoolean;
            cboCoQuanNhan.Properties.ReadOnly = sBoolean;
            cboNhanVienXuLy.Properties.ReadOnly = sBoolean;
            txtNoiDung.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtTenFile.Text = "";
            txtFile.Text = "";
            txtSoGui.Text = "";
            txtNoiDung.Text = "";
            txtSoKyHieu.Text = "";
            dtpNgayBanHanh.EditValue = clsMain.GetServerDate();
            dtpNgayGui.EditValue = clsMain.GetServerDate();
            cboCoQuanBanHanh.EditValue = cboCoQuanBanHanh.Properties.GetDataSourceValue(cboCoQuanBanHanh.Properties.ValueMember, 0);
            cboLoaiVanBan.EditValue = cboLoaiVanBan.Properties.GetDataSourceValue(cboLoaiVanBan.Properties.ValueMember, 0);
            cboSoVanBan.EditValue = cboSoVanBan.Properties.GetDataSourceValue(cboSoVanBan.Properties.ValueMember, 0);
            cboNhanVienXuLy.EditValue = cboNhanVienXuLy.Properties.GetDataSourceValue(cboNhanVienXuLy.Properties.ValueMember, 0);
            cboCoQuanNhan.EditValue = cboCoQuanNhan.Properties.GetDataSourceValue(cboCoQuanNhan.Properties.ValueMember, 0);
            
        }

        private Boolean CheckInput()
        {
            if (txtSoKyHieu.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblSoKyHieu.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoKyHieu.Focus();
                return false;
            }
            if (cboCoQuanBanHanh.EditValue == "" || cboCoQuanBanHanh.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblCoQuanBanHanh.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboCoQuanBanHanh.Focus();
                return false;
            }
            if (cboLoaiVanBan.EditValue == "" || cboLoaiVanBan.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblLoaiVanBan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboLoaiVanBan.Focus();
                return false;
            }
            if (cboSoVanBan.EditValue == "" || cboSoVanBan.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblSoVanBan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboSoVanBan.Focus();
                return false;
            }
            if (cboCoQuanNhan.EditValue == "" || cboCoQuanNhan.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblCoQuanNhan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboCoQuanNhan.Focus();
                return false;
            }
            if (cboNhanVienXuLy.EditValue == "" || cboNhanVienXuLy.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblNhanVienXuLy.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNhanVienXuLy.Focus();
                return false;
            }
            if (txtTenFile.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblFileVanBan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenFile.Focus();
                return false;
            }
            if (txtFile.Text != "")
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog sOpenFile = new OpenFileDialog())
            {
                sOpenFile.Filter = clsQLCongTy.Filter();
                sOpenFile.FilterIndex = 1;
                sOpenFile.RestoreDirectory = true;
                sOpenFile.Multiselect = false;
                sOpenFile.Title = "Chọn File";
                if (sOpenFile.ShowDialog() == DialogResult.OK)
                {
                    txtFile.Text = sOpenFile.FileName;
                    sNameFile = sOpenFile.SafeFileName;
                    txtTenFile.Text = sOpenFile.SafeFileName;
                }
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            using (SaveFileDialog sSaveFile = new SaveFileDialog())
            {
                sSaveFile.Filter = clsQLCongTy.Filter();
                sSaveFile.FilterIndex = 1;
                sSaveFile.RestoreDirectory = true;
                sSaveFile.Title = "Chọn File";
                sSaveFile.FileName = sNameFile;
                if (sSaveFile.ShowDialog() == DialogResult.OK)
                    if(clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                        clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                    else
                        clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
            }
        }

       

    }
}