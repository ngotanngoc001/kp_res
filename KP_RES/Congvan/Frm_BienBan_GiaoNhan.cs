﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BienBan_GiaoNhan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_BienBan_GiaoNhan()
        {
            InitializeComponent();
            
            LoadPermission();
            VisibleTileItem();

            if (cls_KP_RES.Mode == 1)
            {
                this.Text = "Nghiệp vụ";
            }
        }

        private void Frm_BienBan_GiaoNhan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itNhapBienBan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapBienBan.Checked)
            {
                this.Parent.GoTo<Frm_BienBanGiaoNhan>();
            }
        }

        private void itDanhSachBienBan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itNhapBienBan.Checked)
            {
                this.Parent.GoTo<Frm_DanhSachBienBan>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itNhapBienBan);
            cls_KP_RES.RemoveTileItem(itDanhSachBienBan);
        }

        private void LoadPermission()
        {
            itNhapBienBan.Checked = true;
            itDanhSachBienBan.Checked = true;
        }

    }
}