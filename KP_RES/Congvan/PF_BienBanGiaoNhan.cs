﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;
using System.IO;

namespace KP_RES
{
    public partial class PF_BienBanGiaoNhan : DevExpress.XtraEditors.XtraForm
    {
        public int iFormState;
        public String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public PF_BienBanGiaoNhan()
        {
            InitializeComponent();
        }

        private void PF_BienBanGiaoNhan_Load(object sender, EventArgs e)
        {
            switch (iFormState)
            {
                //Xem
                case 1:
                    KhoaMoControl(true);
                    LoadData();
                    LoadGridView();
                    btnLuu.Visible = false;
                    btnDong.Focus();
                    break;
                //Sửa
                case 2:
                    KhoaMoControl(false);
                    LoadData();
                    LoadGridView();
                    txtBenGiao.Focus();
                    break;
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PF_BienBanGiaoNhan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void LoadData()
        {
            String sSQL = "";
            sSQL += "SELECT MA,SO_KYHIEU,NGAYBANHANH,BENGIAO,CV_BENGIAO,BENNHAN,CV_BENNHAN,NGAYNHAN," + "\n";
            sSQL += "NOIDUNG,FILES,TEN_FILES,NGAYTAO,NGUOITAO,NGAYSUA,NGUOISUA" + "\n";
            sSQL += "FROM BIENBAN_GIAONHAN" + "\n";
            sSQL += "WHERE SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";

            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            if (myDT.Rows.Count > 0)
            {
                txtSoKyHieu.Text = myDT.Rows[0]["SO_KYHIEU"].ToString();
                txtBenGiao.Text = myDT.Rows[0]["BENGIAO"].ToString();
                txtChucVuBG.Text = myDT.Rows[0]["CV_BENGIAO"].ToString();
                txtBenNhan.Text = myDT.Rows[0]["BENNHAN"].ToString();
                txtChucVuBN.Text = myDT.Rows[0]["CV_BENNHAN"].ToString();
                txtTenFile.Text = myDT.Rows[0]["TEN_FILES"].ToString();
                txtNoiDung.Text = myDT.Rows[0]["NOIDUNG"].ToString();
                dtpNgayBanHanh.EditValue = DateTime.Parse(myDT.Rows[0]["NGAYBANHANH"].ToString());
                dtpNgayNhan.EditValue = DateTime.Parse(myDT.Rows[0]["NGAYNHAN"].ToString());
                sNameFile = myDT.Rows[0]["TEN_FILES"].ToString();
                sByteDataFile = clsQLCongTy.ReturnByte(myDT.Rows[0]["FILES"]);
            }
        }

        private void LoadGridView()
        {
            String sSQL = "";
            sSQL += "SELECT TEN,COQUANCAP,SOHIEU,BANCHINH,BANPHU,NGAYCAP,NGAYHETHAN" + "\n";
            sSQL += "FROM CHITIET_BIENBAN_GN" + "\n";
            sSQL += "WHERE SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";

            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            myDT.Rows.Add("", "", "", "0", "0", "", "");
            gridControl2.DataSource = myDT;
        }


        private void btnLuu_Click(object sender, EventArgs e)
        {
            int sRow = 0;
            if (!CheckInput())
                return;

            DataView myDV = (DataView)gridView2.DataSource;
            DataTable myDT = myDV.ToTable();

            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            String sSQL = "";
            sSQL += "EXEC SP_UpdateBienBanGiaoNhan ";
            sSQL += clsMain.SQLString(txtSoKyHieu.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLString(dtpNgayBanHanh.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtBenGiao.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtChucVuBG.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtBenNhan.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtChucVuBN.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLString(dtpNgayNhan.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtNoiDung.Text.Trim()) + "," + "\n";
            sSQL += "0x" + BitConverter.ToString(sByteDataFile).Replace("-", "").ToLower() + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtTenFile.Text.Trim()) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(clsGlobal.gsUserID) + "\n";

            sSQL += "DELETE FROM CHITIET_BIENBAN_GN" + "\n";
            sSQL += "WHERE SO_KYHIEU = " + clsMain.SQLString(sMa) + "\n";

            foreach (DataRow myDR in myDT.Rows)
            {
                if (myDR["TEN"].ToString() != "")
                {
                    sSQL += "EXEC SP_InsertChiTietBienBanGN ";
                    sSQL += clsMain.SQLString(txtSoKyHieu.Text) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["TEN"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["COQUANCAP"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["SOHIEU"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLString(myDR["BANCHINH"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLString(myDR["BANPHU"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["NGAYCAP"].ToString().Trim()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["NGAYHETHAN"].ToString().Trim()) + "\n";

                    sRow = 1;
                }
            }

            if (sRow == 0)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (sBoolean)
            {
                clsQLCongTy.MessageInformation("Lưu thành công", "Thông báo");
                this.Close();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            dtpNgayBanHanh.Properties.ReadOnly = sBoolean;
            txtBenGiao.Properties.ReadOnly = sBoolean;
            txtChucVuBG.Properties.ReadOnly = sBoolean;
            txtBenNhan.Properties.ReadOnly = sBoolean;
            txtChucVuBN.Properties.ReadOnly = sBoolean;
            dtpNgayNhan.Properties.ReadOnly = sBoolean;
            txtNoiDung.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private Boolean CheckInput()
        {
            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == XOA)
            {
                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString() != "")
                {
                    if (gridView2.RowCount == 1)
                    {
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "TEN", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "COQUANCAP", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "SOHIEU", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "BANCHINH", "0");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "BANPHU", "0");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "NGAYCAP", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "NGAYHETHAN", "");
                    }
                    else
                    {
                        gridView2.DeleteSelectedRows();
                        gridView2.UpdateTotalSummary();
                    }
                }
            }
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == TEN)
            {
                gridView2.UpdateTotalSummary();
                DataView myDV = (DataView)gridView2.DataSource;
                DataTable myDT = myDV.ToTable();
                if (myDT.Rows.Count == 1)
                {
                    if (myDT.Rows[0]["TEN"].ToString() != "")
                    {
                        myDT.Rows.Add("", "", "", "0", "0", "", "");
                        gridControl2.DataSource = myDT.Copy();
                    }
                }
                else
                {
                    if (myDT.Rows[myDT.Rows.Count - 1]["TEN"].ToString() != "")
                    {
                        myDT.Rows.Add("", "", "", "0", "0", "", "");
                        gridControl2.DataSource = myDT.Copy();
                    }
                }
            }
        }

       

    }
}