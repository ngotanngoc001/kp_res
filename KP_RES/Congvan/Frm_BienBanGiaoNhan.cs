﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_BienBanGiaoNhan : DevExpress.XtraEditors.XtraForm
    {
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_BienBanGiaoNhan()
        {
            InitializeComponent();
        }

        private void Frm_BienBanGiaoNhan_Load(object sender, EventArgs e)
        {
            dtpNgayBanHanh.EditValue = clsMain.GetServerDate();
            dtpNgayNhan.EditValue = clsMain.GetServerDate();

            SetRongControl();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int sRow = 0;
            if (!CheckInput())
                return;

            DataView myDV = (DataView)gridView2.DataSource;
            DataTable myDT = myDV.ToTable();

            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            String sSQL = "";
            sSQL += "EXEC SP_InsertBienBanGiaoNhan ";
            sSQL += clsMain.SQLString(txtSoKyHieu.Text) + "," + "\n";
            sSQL += clsMain.SQLString(dtpNgayBanHanh.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtBenGiao.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtChucVuBG.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtBenNhan.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtChucVuBN.Text) + "," + "\n";
            sSQL += clsMain.SQLString(dtpNgayNhan.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtNoiDung.Text) + "," + "\n";
            sSQL += "0x" +  BitConverter.ToString(sByteDataFile).Replace("-", "").ToLower() + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(txtTenFile.Text) + "," + "\n";
            sSQL += clsMain.SQLStringUnicode(clsGlobal.gsUserID) + "\n";

            foreach (DataRow myDR in myDT.Rows)
            {
                if (myDR["TEN"].ToString() != "")
                {
                    sSQL += "EXEC SP_InsertChiTietBienBanGN ";
                    sSQL += clsMain.SQLString(txtSoKyHieu.Text) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["TEN"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["COQUANCAP"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["SOHIEU"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLString(myDR["BANCHINH"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLString(myDR["BANPHU"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["NGAYCAP"].ToString()) + "," + "\n";
                    sSQL += clsMain.SQLStringUnicode(myDR["NGAYHETHAN"].ToString()) + "\n";

                    sRow = 1;
                }
            }

            if (sRow == 0)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (sBoolean)
            {
                sSQL = "";
                sSQL += "SELECT A.MA,A.SO_KYHIEU,A.NGAYBANHANH,A.BENGIAO,A.CV_BENGIAO,A.BENNHAN,A.CV_BENNHAN,A.NGAYNHAN,A.NOIDUNG,A.FILES,A.TEN_FILES,A.NGAYTAO,A.NGUOITAO,A.NGAYSUA,A.NGUOISUA,B.TEN,B.COQUANCAP,B.SOHIEU,B.BANCHINH,B.BANPHU,B.NGAYCAP,B.NGAYHETHAN" + "\n";
                sSQL += "FROM BIENBAN_GIAONHAN A" + "\n";
                sSQL += "INNER JOIN CHITIET_BIENBAN_GN B ON A.SO_KYHIEU = B.SO_KYHIEU" + "\n";
                sSQL += "WHERE A.SO_KYHIEU = " + clsMain.SQLString(txtSoKyHieu.Text) + "\n";
                DataTable myDT1 = clsMain.ReturnDataTable(sSQL);

                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = myDT1;
                frm.Mode = 81;
                frm.ShowDialog();
                frm.Dispose();

                SetRongControl();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void SetRongControl()
        {
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtTenFile.Text = "";
            txtFile.Text = "";

            txtSoKyHieu.Text = "";
            txtBenGiao.Text = "";
            txtChucVuBG.Text = "";
            txtBenNhan.Text = "";
            txtChucVuBN.Text = "";
            txtNoiDung.Text = "";
            dtpNgayBanHanh.EditValue = clsMain.GetServerDate();
            dtpNgayNhan.EditValue = clsMain.GetServerDate();

            DataTable myDT = new DataTable();
            myDT.Columns.Add("TEN");
            myDT.Columns.Add("COQUANCAP");
            myDT.Columns.Add("SOHIEU");
            myDT.Columns.Add("BANCHINH");
            myDT.Columns.Add("BANPHU");
            myDT.Columns.Add("NGAYCAP");
            myDT.Columns.Add("NGAYHETHAN");
            myDT.Rows.Add("", "", "", "0", "0", "", "");
            gridControl2.DataSource = myDT;
        }

        private Boolean CheckInput()
        {
            if (txtSoKyHieu.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblSoKyHieu.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoKyHieu.Focus();
                return false;
            }

            if (clsMain.ReturnDataTable("SELECT SO_KYHIEU FROM BIENBAN_GIAONHAN WHERE SO_KYHIEU = " + clsMain.SQLString(txtSoKyHieu.Text)).Rows.Count > 0)
            {
                XtraMessageBox.Show("Số ký hiệu đã tồn tại. Vui lòng nhập lại ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoKyHieu.Focus();
                return false;
            }


            if (txtFile.Text != "")
            {
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = clsQLCongTy.Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = sOpenFile.FileName;
                sNameFile = sOpenFile.SafeFileName;
                txtTenFile.Text = sOpenFile.SafeFileName;
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == XOA)
            {
                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString() != "")
                {
                    if (gridView2.RowCount == 1)
                    {
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "TEN", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "COQUANCAP", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "SOHIEU", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "BANCHINH", "0");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "BANPHU", "0");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "NGAYCAP", "");
                        gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "NGAYHETHAN", "");
                    }
                    else
                    {
                        gridView2.DeleteSelectedRows();
                        gridView2.UpdateTotalSummary();
                    }
                }
            }
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == TEN)
            {
                gridView2.UpdateTotalSummary();
                DataView myDV = (DataView)gridView2.DataSource;
                DataTable myDT = myDV.ToTable();
                if (myDT.Rows.Count == 1)
                {
                    if (myDT.Rows[0]["TEN"].ToString() != "")
                    {
                        myDT.Rows.Add("", "", "", "0", "0", "", "");
                        gridControl2.DataSource = myDT.Copy();
                    }
                }
                else
                {
                    if (myDT.Rows[myDT.Rows.Count - 1]["TEN"].ToString() != "")
                    {
                        myDT.Rows.Add("", "", "", "0", "0", "", "");
                        gridControl2.DataSource = myDT.Copy();
                    }
                }
            }
        }


    }
}