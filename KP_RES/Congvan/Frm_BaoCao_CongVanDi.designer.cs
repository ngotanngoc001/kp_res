﻿namespace KP_RES
{
    partial class Frm_BaoCao_CongVanDi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCao_CongVanDi));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.cboData = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTuNgay = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblDenNgay = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.grcCongVanDi = new DevExpress.XtraGrid.GridControl();
            this.grvCongVanDi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILEVANBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQBH_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQNHAN_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SO_KYHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYBANHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIVB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOVB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SODEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYDEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAME_FILE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcCongVanDi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvCongVanDi)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 674);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 244);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 393);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 80);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 311);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.dtpTuNgay);
            this.pnChoose.Controls.Add(this.cboData);
            this.pnChoose.Controls.Add(this.lblTuNgay);
            this.pnChoose.Controls.Add(this.dtpDenNgay);
            this.pnChoose.Controls.Add(this.lblTitle);
            this.pnChoose.Controls.Add(this.lblDenNgay);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 78);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(87, 4);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpTuNgay.TabIndex = 8;
            this.dtpTuNgay.Visible = false;
            // 
            // cboData
            // 
            this.cboData.Location = new System.Drawing.Point(3, 45);
            this.cboData.Name = "cboData";
            this.cboData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.Appearance.Options.UseFont = true;
            this.cboData.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboData.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboData.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboData.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboData.Properties.DisplayMember = "TEN";
            this.cboData.Properties.DropDownItemHeight = 40;
            this.cboData.Properties.NullText = "";
            this.cboData.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboData.Properties.ShowHeader = false;
            this.cboData.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboData.Properties.ValueMember = "MA";
            this.cboData.Size = new System.Drawing.Size(210, 26);
            this.cboData.TabIndex = 17;
            this.cboData.Visible = false;
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTuNgay.Location = new System.Drawing.Point(3, 8);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(72, 19);
            this.lblTuNgay.TabIndex = 10;
            this.lblTuNgay.Text = "Từ Ngày :";
            this.lblTuNgay.Visible = false;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(87, 44);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(126, 27);
            this.dtpDenNgay.TabIndex = 9;
            this.dtpDenNgay.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTitle.Location = new System.Drawing.Point(3, 8);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(84, 19);
            this.lblTitle.TabIndex = 16;
            this.lblTitle.Text = "Tên Cấp Độ";
            this.lblTitle.Visible = false;
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblDenNgay.Location = new System.Drawing.Point(3, 48);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(80, 19);
            this.lblDenNgay.TabIndex = 11;
            this.lblDenNgay.Text = "Đến Ngày :";
            this.lblDenNgay.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 242);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "0";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "Theo cơ quan ban hành"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "Theo loại văn bản"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "Theo sổ văn bản"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "Theo nhân viên xử lý"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "Theo cơ quan nhận"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "Theo ngày gửi")});
            this.optGroup.Size = new System.Drawing.Size(215, 239);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 637);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatFile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Controls.Add(this.btnDownLoadFile);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1325, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 674);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 317);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 275);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 115);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 195);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 112);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 592);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 237);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatFile
            // 
            this.btnXuatFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatFile.Appearance.Options.UseFont = true;
            this.btnXuatFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatFile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatFile.Location = new System.Drawing.Point(2, 157);
            this.btnXuatFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatFile.Name = "btnXuatFile";
            this.btnXuatFile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatFile.TabIndex = 14;
            this.btnXuatFile.Text = "&Xuất File";
            this.btnXuatFile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 77);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDownLoadFile.Location = new System.Drawing.Point(2, 2);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(35, 75);
            this.btnDownLoadFile.TabIndex = 18;
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // grcCongVanDi
            // 
            this.grcCongVanDi.AllowDrop = true;
            this.grcCongVanDi.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grcCongVanDi.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grcCongVanDi.Location = new System.Drawing.Point(223, 0);
            this.grcCongVanDi.MainView = this.grvCongVanDi;
            this.grcCongVanDi.Name = "grcCongVanDi";
            this.grcCongVanDi.Size = new System.Drawing.Size(1102, 674);
            this.grcCongVanDi.TabIndex = 9;
            this.grcCongVanDi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvCongVanDi});
            // 
            // grvCongVanDi
            // 
            this.grvCongVanDi.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grvCongVanDi.Appearance.FooterPanel.Options.UseFont = true;
            this.grvCongVanDi.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvCongVanDi.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvCongVanDi.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.grvCongVanDi.Appearance.GroupFooter.Options.UseFont = true;
            this.grvCongVanDi.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grvCongVanDi.Appearance.GroupRow.Options.UseFont = true;
            this.grvCongVanDi.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.grvCongVanDi.Appearance.Row.Options.UseFont = true;
            this.grvCongVanDi.ColumnPanelRowHeight = 30;
            this.grvCongVanDi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILEVANBAN,
            this.CQBH_ID,
            this.LOAIVB_ID,
            this.SOVB_ID,
            this.CQNHAN_ID,
            this.NHANVIENXULY_ID,
            this.STT,
            this.MA,
            this.SO_KYHIEU,
            this.NGAYBANHANH,
            this.CQBH,
            this.LOAIVB,
            this.NOIDUNG,
            this.SOVB,
            this.SODEN,
            this.NGAYDEN,
            this.NHANVIENXULY,
            this.CQNHAN,
            this.NAME_FILE,
            this.FILL});
            this.grvCongVanDi.FooterPanelHeight = 30;
            this.grvCongVanDi.GridControl = this.grcCongVanDi;
            this.grvCongVanDi.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.grvCongVanDi.Name = "grvCongVanDi";
            this.grvCongVanDi.OptionsFind.AlwaysVisible = true;
            this.grvCongVanDi.OptionsFind.ShowCloseButton = false;
            this.grvCongVanDi.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvCongVanDi.OptionsView.EnableAppearanceEvenRow = true;
            this.grvCongVanDi.OptionsView.ShowAutoFilterRow = true;
            this.grvCongVanDi.OptionsView.ShowFooter = true;
            this.grvCongVanDi.OptionsView.ShowIndicator = false;
            this.grvCongVanDi.RowHeight = 30;
            this.grvCongVanDi.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvCongVanDi.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.MA, DevExpress.Data.ColumnSortOrder.Descending)});
            this.grvCongVanDi.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvCongVanDi_RowCellClick);
            this.grvCongVanDi.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvCongVanDi_CustomDrawCell);
            // 
            // FILEVANBAN
            // 
            this.FILEVANBAN.FieldName = "FILEVANBAN";
            this.FILEVANBAN.Name = "FILEVANBAN";
            this.FILEVANBAN.OptionsColumn.AllowEdit = false;
            this.FILEVANBAN.OptionsColumn.AllowFocus = false;
            this.FILEVANBAN.OptionsColumn.FixedWidth = true;
            // 
            // CQBH_ID
            // 
            this.CQBH_ID.FieldName = "CQBH_ID";
            this.CQBH_ID.Name = "CQBH_ID";
            this.CQBH_ID.OptionsColumn.AllowEdit = false;
            this.CQBH_ID.OptionsColumn.AllowFocus = false;
            this.CQBH_ID.OptionsColumn.AllowMove = false;
            // 
            // LOAIVB_ID
            // 
            this.LOAIVB_ID.FieldName = "LOAIVB_ID";
            this.LOAIVB_ID.Name = "LOAIVB_ID";
            this.LOAIVB_ID.OptionsColumn.AllowEdit = false;
            this.LOAIVB_ID.OptionsColumn.AllowFocus = false;
            this.LOAIVB_ID.OptionsColumn.AllowMove = false;
            // 
            // SOVB_ID
            // 
            this.SOVB_ID.FieldName = "SOVB_ID";
            this.SOVB_ID.Name = "SOVB_ID";
            this.SOVB_ID.OptionsColumn.AllowEdit = false;
            this.SOVB_ID.OptionsColumn.AllowFocus = false;
            this.SOVB_ID.OptionsColumn.AllowMove = false;
            // 
            // CQNHAN_ID
            // 
            this.CQNHAN_ID.FieldName = "CQNHAN_ID";
            this.CQNHAN_ID.Name = "CQNHAN_ID";
            this.CQNHAN_ID.OptionsColumn.AllowEdit = false;
            this.CQNHAN_ID.OptionsColumn.AllowFocus = false;
            this.CQNHAN_ID.OptionsColumn.FixedWidth = true;
            // 
            // NHANVIENXULY_ID
            // 
            this.NHANVIENXULY_ID.FieldName = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.Name = "NHANVIENXULY_ID";
            this.NHANVIENXULY_ID.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY_ID.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // SO_KYHIEU
            // 
            this.SO_KYHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SO_KYHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SO_KYHIEU.AppearanceHeader.Options.UseFont = true;
            this.SO_KYHIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.SO_KYHIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SO_KYHIEU.Caption = "Số / Ký hiệu";
            this.SO_KYHIEU.FieldName = "SO_KYHIEU";
            this.SO_KYHIEU.Name = "SO_KYHIEU";
            this.SO_KYHIEU.OptionsColumn.AllowEdit = false;
            this.SO_KYHIEU.OptionsColumn.AllowFocus = false;
            this.SO_KYHIEU.OptionsColumn.FixedWidth = true;
            this.SO_KYHIEU.Visible = true;
            this.SO_KYHIEU.VisibleIndex = 2;
            this.SO_KYHIEU.Width = 150;
            // 
            // NGAYBANHANH
            // 
            this.NGAYBANHANH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYBANHANH.AppearanceHeader.Options.UseFont = true;
            this.NGAYBANHANH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYBANHANH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBANHANH.Caption = "Ngày BH";
            this.NGAYBANHANH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYBANHANH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYBANHANH.FieldName = "NGAYBANHANH";
            this.NGAYBANHANH.Name = "NGAYBANHANH";
            this.NGAYBANHANH.OptionsColumn.AllowEdit = false;
            this.NGAYBANHANH.OptionsColumn.AllowFocus = false;
            this.NGAYBANHANH.OptionsColumn.FixedWidth = true;
            this.NGAYBANHANH.Visible = true;
            this.NGAYBANHANH.VisibleIndex = 3;
            this.NGAYBANHANH.Width = 100;
            // 
            // CQBH
            // 
            this.CQBH.AppearanceCell.Options.UseTextOptions = true;
            this.CQBH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CQBH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CQBH.AppearanceHeader.Options.UseFont = true;
            this.CQBH.AppearanceHeader.Options.UseTextOptions = true;
            this.CQBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CQBH.Caption = "CQ ban hành";
            this.CQBH.FieldName = "CQBH";
            this.CQBH.Name = "CQBH";
            this.CQBH.OptionsColumn.AllowEdit = false;
            this.CQBH.OptionsColumn.AllowFocus = false;
            this.CQBH.OptionsColumn.FixedWidth = true;
            this.CQBH.Visible = true;
            this.CQBH.VisibleIndex = 4;
            this.CQBH.Width = 230;
            // 
            // LOAIVB
            // 
            this.LOAIVB.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIVB.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIVB.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIVB.AppearanceHeader.Options.UseFont = true;
            this.LOAIVB.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIVB.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIVB.Caption = "Loại văn bản";
            this.LOAIVB.FieldName = "LOAIVB";
            this.LOAIVB.Name = "LOAIVB";
            this.LOAIVB.OptionsColumn.AllowEdit = false;
            this.LOAIVB.OptionsColumn.AllowFocus = false;
            this.LOAIVB.OptionsColumn.FixedWidth = true;
            this.LOAIVB.Visible = true;
            this.LOAIVB.VisibleIndex = 5;
            this.LOAIVB.Width = 150;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 6;
            this.NOIDUNG.Width = 300;
            // 
            // SOVB
            // 
            this.SOVB.AppearanceCell.Options.UseTextOptions = true;
            this.SOVB.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOVB.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOVB.AppearanceHeader.Options.UseFont = true;
            this.SOVB.AppearanceHeader.Options.UseTextOptions = true;
            this.SOVB.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOVB.Caption = "Sổ văn bản";
            this.SOVB.FieldName = "SOVB";
            this.SOVB.Name = "SOVB";
            this.SOVB.OptionsColumn.AllowEdit = false;
            this.SOVB.OptionsColumn.AllowFocus = false;
            this.SOVB.OptionsColumn.FixedWidth = true;
            this.SOVB.Visible = true;
            this.SOVB.VisibleIndex = 7;
            this.SOVB.Width = 150;
            // 
            // SODEN
            // 
            this.SODEN.AppearanceCell.Options.UseTextOptions = true;
            this.SODEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SODEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SODEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SODEN.AppearanceHeader.Options.UseFont = true;
            this.SODEN.AppearanceHeader.Options.UseTextOptions = true;
            this.SODEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SODEN.Caption = "Số gửi";
            this.SODEN.FieldName = "SODEN";
            this.SODEN.Name = "SODEN";
            this.SODEN.OptionsColumn.AllowEdit = false;
            this.SODEN.OptionsColumn.AllowFocus = false;
            this.SODEN.OptionsColumn.FixedWidth = true;
            this.SODEN.Visible = true;
            this.SODEN.VisibleIndex = 8;
            this.SODEN.Width = 80;
            // 
            // NGAYDEN
            // 
            this.NGAYDEN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYDEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYDEN.AppearanceHeader.Options.UseFont = true;
            this.NGAYDEN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYDEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYDEN.Caption = "Ngày gửi";
            this.NGAYDEN.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYDEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYDEN.FieldName = "NGAYDEN";
            this.NGAYDEN.Name = "NGAYDEN";
            this.NGAYDEN.OptionsColumn.AllowEdit = false;
            this.NGAYDEN.OptionsColumn.AllowFocus = false;
            this.NGAYDEN.OptionsColumn.FixedWidth = true;
            this.NGAYDEN.Visible = true;
            this.NGAYDEN.VisibleIndex = 9;
            this.NGAYDEN.Width = 120;
            // 
            // NHANVIENXULY
            // 
            this.NHANVIENXULY.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIENXULY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIENXULY.AppearanceHeader.Options.UseFont = true;
            this.NHANVIENXULY.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIENXULY.Caption = "NV Xử lý";
            this.NHANVIENXULY.FieldName = "NHANVIENXULY";
            this.NHANVIENXULY.Name = "NHANVIENXULY";
            this.NHANVIENXULY.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY.OptionsColumn.FixedWidth = true;
            this.NHANVIENXULY.Visible = true;
            this.NHANVIENXULY.VisibleIndex = 10;
            this.NHANVIENXULY.Width = 150;
            // 
            // CQNHAN
            // 
            this.CQNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CQNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CQNHAN.AppearanceHeader.Options.UseFont = true;
            this.CQNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CQNHAN.Caption = "CQ nhận";
            this.CQNHAN.FieldName = "CQNHAN";
            this.CQNHAN.Name = "CQNHAN";
            this.CQNHAN.OptionsColumn.AllowEdit = false;
            this.CQNHAN.OptionsColumn.AllowFocus = false;
            this.CQNHAN.OptionsColumn.FixedWidth = true;
            this.CQNHAN.Visible = true;
            this.CQNHAN.VisibleIndex = 11;
            this.CQNHAN.Width = 200;
            // 
            // NAME_FILE
            // 
            this.NAME_FILE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAME_FILE.AppearanceHeader.Options.UseFont = true;
            this.NAME_FILE.AppearanceHeader.Options.UseTextOptions = true;
            this.NAME_FILE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAME_FILE.Caption = "Tên file";
            this.NAME_FILE.FieldName = "NAME_FILE";
            this.NAME_FILE.Name = "NAME_FILE";
            this.NAME_FILE.OptionsColumn.AllowEdit = false;
            this.NAME_FILE.OptionsColumn.AllowFocus = false;
            this.NAME_FILE.OptionsColumn.FixedWidth = true;
            this.NAME_FILE.Visible = true;
            this.NAME_FILE.VisibleIndex = 12;
            this.NAME_FILE.Width = 100;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 13;
            this.FILL.Width = 20;
            // 
            // Frm_BaoCao_CongVanDi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 674);
            this.Controls.Add(this.grcCongVanDi);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_BaoCao_CongVanDi";
            this.Text = "Danh sách công văn đi";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcCongVanDi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvCongVanDi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatFile;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private DevExpress.XtraEditors.LabelControl lblTuNgay;
        private DevExpress.XtraEditors.LabelControl lblDenNgay;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.LookUpEdit cboData;
        private DevExpress.XtraGrid.GridControl grcCongVanDi;
        private DevExpress.XtraGrid.Views.Grid.GridView grvCongVanDi;
        private DevExpress.XtraGrid.Columns.GridColumn FILEVANBAN;
        private DevExpress.XtraGrid.Columns.GridColumn CQBH_ID;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn SOVB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn CQNHAN_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY_ID;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn SO_KYHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYBANHANH;
        private DevExpress.XtraGrid.Columns.GridColumn CQBH;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIVB;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn SOVB;
        private DevExpress.XtraGrid.Columns.GridColumn SODEN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYDEN;
        private DevExpress.XtraGrid.Columns.GridColumn CQNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY;
        private DevExpress.XtraGrid.Columns.GridColumn NAME_FILE;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;



    }
}