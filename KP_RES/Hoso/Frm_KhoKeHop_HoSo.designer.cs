﻿namespace KP_RES 
{
    partial class Frm_KhoKeHop_HoSo 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_KhoKeHop_HoSo));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtSucChua = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cboKho = new DevExpress.XtraEditors.LookUpEdit();
            this.cboKe = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSuDungHop = new DevExpress.XtraEditors.CheckEdit();
            this.chkSuDungKe = new DevExpress.XtraEditors.CheckEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lbKho2 = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChuHop = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu2 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenKe = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenHop = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChuKho = new DevExpress.XtraEditors.TextEdit();
            this.lbQuay = new DevExpress.XtraEditors.LabelControl();
            this.lbKho = new DevExpress.XtraEditors.LabelControl();
            this.chkSuDungKho = new DevExpress.XtraEditors.CheckEdit();
            this.lblTenCH = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChuKe = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTenKho = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pnlHop = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pnlKe = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlKho = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSucChua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungHop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuHop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuKe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtSucChua);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cboKho);
            this.panelControl1.Controls.Add(this.cboKe);
            this.panelControl1.Controls.Add(this.chkSuDungHop);
            this.panelControl1.Controls.Add(this.chkSuDungKe);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lbKho2);
            this.panelControl1.Controls.Add(this.txtGhiChuHop);
            this.panelControl1.Controls.Add(this.lbGhiChu2);
            this.panelControl1.Controls.Add(this.txtTenKe);
            this.panelControl1.Controls.Add(this.lbGhiChu3);
            this.panelControl1.Controls.Add(this.txtTenHop);
            this.panelControl1.Controls.Add(this.lbGhiChu);
            this.panelControl1.Controls.Add(this.txtGhiChuKho);
            this.panelControl1.Controls.Add(this.lbQuay);
            this.panelControl1.Controls.Add(this.lbKho);
            this.panelControl1.Controls.Add(this.chkSuDungKho);
            this.panelControl1.Controls.Add(this.lblTenCH);
            this.panelControl1.Controls.Add(this.txtGhiChuKe);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtTenKho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 174);
            this.panelControl1.TabIndex = 0;
            // 
            // txtSucChua
            // 
            this.txtSucChua.EnterMoveNextControl = true;
            this.txtSucChua.Location = new System.Drawing.Point(749, 69);
            this.txtSucChua.Name = "txtSucChua";
            this.txtSucChua.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSucChua.Properties.Appearance.Options.UseFont = true;
            this.txtSucChua.Size = new System.Drawing.Size(248, 26);
            this.txtSucChua.TabIndex = 25;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(668, 72);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 19);
            this.labelControl1.TabIndex = 24;
            this.labelControl1.Text = "Sức chứa";
            // 
            // cboKho
            // 
            this.cboKho.EnterMoveNextControl = true;
            this.cboKho.Location = new System.Drawing.Point(414, 5);
            this.cboKho.Name = "cboKho";
            this.cboKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.Appearance.Options.UseFont = true;
            this.cboKho.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKho.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKho.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboKho.Properties.DisplayMember = "TEN";
            this.cboKho.Properties.DropDownItemHeight = 40;
            this.cboKho.Properties.NullText = "";
            this.cboKho.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKho.Properties.ShowHeader = false;
            this.cboKho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKho.Properties.ValueMember = "MA";
            this.cboKho.Size = new System.Drawing.Size(248, 26);
            this.cboKho.TabIndex = 6;
            // 
            // cboKe
            // 
            this.cboKe.EnterMoveNextControl = true;
            this.cboKe.Location = new System.Drawing.Point(749, 5);
            this.cboKe.Name = "cboKe";
            this.cboKe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKe.Properties.Appearance.Options.UseFont = true;
            this.cboKe.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKe.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKe.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboKe.Properties.DisplayMember = "TEN";
            this.cboKe.Properties.DropDownItemHeight = 40;
            this.cboKe.Properties.NullText = "";
            this.cboKe.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKe.Properties.ShowHeader = false;
            this.cboKe.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKe.Properties.ValueMember = "MA";
            this.cboKe.Size = new System.Drawing.Size(248, 26);
            this.cboKe.TabIndex = 13;
            // 
            // chkSuDungHop
            // 
            this.chkSuDungHop.EnterMoveNextControl = true;
            this.chkSuDungHop.Location = new System.Drawing.Point(747, 133);
            this.chkSuDungHop.Name = "chkSuDungHop";
            this.chkSuDungHop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDungHop.Properties.Appearance.Options.UseFont = true;
            this.chkSuDungHop.Properties.Caption = "Sử dụng";
            this.chkSuDungHop.Size = new System.Drawing.Size(92, 24);
            this.chkSuDungHop.TabIndex = 18;
            // 
            // chkSuDungKe
            // 
            this.chkSuDungKe.EnterMoveNextControl = true;
            this.chkSuDungKe.Location = new System.Drawing.Point(414, 101);
            this.chkSuDungKe.Name = "chkSuDungKe";
            this.chkSuDungKe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDungKe.Properties.Appearance.Options.UseFont = true;
            this.chkSuDungKe.Properties.Caption = "Sử dụng";
            this.chkSuDungKe.Size = new System.Drawing.Size(92, 24);
            this.chkSuDungKe.TabIndex = 11;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(77, 132);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(253, 132);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(341, 132);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(165, 132);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lbKho2
            // 
            this.lbKho2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKho2.Location = new System.Drawing.Point(668, 8);
            this.lbKho2.Margin = new System.Windows.Forms.Padding(4);
            this.lbKho2.Name = "lbKho2";
            this.lbKho2.Size = new System.Drawing.Size(61, 19);
            this.lbKho2.TabIndex = 12;
            this.lbKho2.Text = "Kệ hồ sơ";
            // 
            // txtGhiChuHop
            // 
            this.txtGhiChuHop.EnterMoveNextControl = true;
            this.txtGhiChuHop.Location = new System.Drawing.Point(749, 101);
            this.txtGhiChuHop.Name = "txtGhiChuHop";
            this.txtGhiChuHop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuHop.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChuHop.Size = new System.Drawing.Size(248, 26);
            this.txtGhiChuHop.TabIndex = 17;
            // 
            // lbGhiChu2
            // 
            this.lbGhiChu2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu2.Location = new System.Drawing.Point(332, 72);
            this.lbGhiChu2.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu2.Name = "lbGhiChu2";
            this.lbGhiChu2.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu2.TabIndex = 9;
            this.lbGhiChu2.Text = "Ghi chú";
            // 
            // txtTenKe
            // 
            this.txtTenKe.EnterMoveNextControl = true;
            this.txtTenKe.Location = new System.Drawing.Point(414, 37);
            this.txtTenKe.Name = "txtTenKe";
            this.txtTenKe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKe.Properties.Appearance.Options.UseFont = true;
            this.txtTenKe.Size = new System.Drawing.Size(248, 26);
            this.txtTenKe.TabIndex = 8;
            // 
            // lbGhiChu3
            // 
            this.lbGhiChu3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu3.Location = new System.Drawing.Point(668, 104);
            this.lbGhiChu3.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu3.Name = "lbGhiChu3";
            this.lbGhiChu3.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu3.TabIndex = 16;
            this.lbGhiChu3.Text = "Ghi chú";
            // 
            // txtTenHop
            // 
            this.txtTenHop.EnterMoveNextControl = true;
            this.txtTenHop.Location = new System.Drawing.Point(749, 37);
            this.txtTenHop.Name = "txtTenHop";
            this.txtTenHop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHop.Properties.Appearance.Options.UseFont = true;
            this.txtTenHop.Size = new System.Drawing.Size(248, 26);
            this.txtTenHop.TabIndex = 15;
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(5, 72);
            this.lbGhiChu.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu.TabIndex = 2;
            this.lbGhiChu.Text = "Ghi chú";
            // 
            // txtGhiChuKho
            // 
            this.txtGhiChuKho.EnterMoveNextControl = true;
            this.txtGhiChuKho.Location = new System.Drawing.Point(77, 69);
            this.txtGhiChuKho.Name = "txtGhiChuKho";
            this.txtGhiChuKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuKho.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChuKho.Size = new System.Drawing.Size(248, 26);
            this.txtGhiChuKho.TabIndex = 3;
            // 
            // lbQuay
            // 
            this.lbQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbQuay.Location = new System.Drawing.Point(668, 40);
            this.lbQuay.Margin = new System.Windows.Forms.Padding(4);
            this.lbQuay.Name = "lbQuay";
            this.lbQuay.Size = new System.Drawing.Size(73, 19);
            this.lbQuay.TabIndex = 14;
            this.lbQuay.Text = "Hộp hồ sơ";
            // 
            // lbKho
            // 
            this.lbKho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKho.Location = new System.Drawing.Point(332, 40);
            this.lbKho.Margin = new System.Windows.Forms.Padding(4);
            this.lbKho.Name = "lbKho";
            this.lbKho.Size = new System.Drawing.Size(61, 19);
            this.lbKho.TabIndex = 7;
            this.lbKho.Text = "Kệ hồ sơ";
            // 
            // chkSuDungKho
            // 
            this.chkSuDungKho.EnterMoveNextControl = true;
            this.chkSuDungKho.Location = new System.Drawing.Point(77, 101);
            this.chkSuDungKho.Name = "chkSuDungKho";
            this.chkSuDungKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDungKho.Properties.Appearance.Options.UseFont = true;
            this.chkSuDungKho.Properties.Caption = "Sử dụng";
            this.chkSuDungKho.Size = new System.Drawing.Size(92, 24);
            this.chkSuDungKho.TabIndex = 4;
            // 
            // lblTenCH
            // 
            this.lblTenCH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCH.Location = new System.Drawing.Point(332, 8);
            this.lblTenCH.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenCH.Name = "lblTenCH";
            this.lblTenCH.Size = new System.Drawing.Size(71, 19);
            this.lblTenCH.TabIndex = 5;
            this.lblTenCH.Text = "Kho hồ sơ";
            // 
            // txtGhiChuKe
            // 
            this.txtGhiChuKe.EnterMoveNextControl = true;
            this.txtGhiChuKe.Location = new System.Drawing.Point(414, 69);
            this.txtGhiChuKe.Name = "txtGhiChuKe";
            this.txtGhiChuKe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuKe.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChuKe.Size = new System.Drawing.Size(248, 26);
            this.txtGhiChuKe.TabIndex = 10;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(5, 40);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(71, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Kho hồ sơ";
            // 
            // txtTenKho
            // 
            this.txtTenKho.EnterMoveNextControl = true;
            this.txtTenKho.Location = new System.Drawing.Point(77, 37);
            this.txtTenKho.Name = "txtTenKho";
            this.txtTenKho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKho.Properties.Appearance.Options.UseFont = true;
            this.txtTenKho.Size = new System.Drawing.Size(248, 26);
            this.txtTenKho.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(981, 174);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 537);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 53);
            this.panelControl3.TabIndex = 20;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 295);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 18;
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 375);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 16;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 162);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 19;
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 82);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 17;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 455);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.panelControl7);
            this.panelControl5.Controls.Add(this.panelControl6);
            this.panelControl5.Controls.Add(this.panelControl4);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 174);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(981, 537);
            this.panelControl5.TabIndex = 4;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.pnlHop);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(2, 288);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(977, 247);
            this.panelControl7.TabIndex = 0;
            // 
            // pnlHop
            // 
            this.pnlHop.AutoScroll = true;
            this.pnlHop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlHop.Location = new System.Drawing.Point(2, 2);
            this.pnlHop.Name = "pnlHop";
            this.pnlHop.Padding = new System.Windows.Forms.Padding(336, 0, 0, 0);
            this.pnlHop.Size = new System.Drawing.Size(973, 243);
            this.pnlHop.TabIndex = 2;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.pnlKe);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 145);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(977, 143);
            this.panelControl6.TabIndex = 0;
            // 
            // pnlKe
            // 
            this.pnlKe.AutoScroll = true;
            this.pnlKe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKe.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlKe.Location = new System.Drawing.Point(2, 2);
            this.pnlKe.Name = "pnlKe";
            this.pnlKe.Padding = new System.Windows.Forms.Padding(168, 0, 0, 0);
            this.pnlKe.Size = new System.Drawing.Size(973, 139);
            this.pnlKe.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlKho);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(977, 143);
            this.panelControl4.TabIndex = 0;
            // 
            // pnlKho
            // 
            this.pnlKho.AutoScroll = true;
            this.pnlKho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKho.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlKho.Location = new System.Drawing.Point(2, 2);
            this.pnlKho.Name = "pnlKho";
            this.pnlKho.Size = new System.Drawing.Size(973, 139);
            this.pnlKho.TabIndex = 0;
            // 
            // Frm_KhoKeHop_HoSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 711);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_KhoKeHop_HoSo";
            this.Text = "Kho - Kệ - Hộp Hồ Sơ";
            this.Load += new System.EventHandler(this.Frm_KhoKeHop_HoSo_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_KhoKeHop_HoSo_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSucChua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungHop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuHop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDungKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuKe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSuDungKho;
        private DevExpress.XtraEditors.LabelControl lblTenCH;
        private DevExpress.XtraEditors.TextEdit txtGhiChuKe;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTenKho;
        private DevExpress.XtraEditors.LabelControl lbKho2;
        private DevExpress.XtraEditors.TextEdit txtGhiChuHop;
        private DevExpress.XtraEditors.LabelControl lbGhiChu2;
        private DevExpress.XtraEditors.TextEdit txtTenKe;
        private DevExpress.XtraEditors.LabelControl lbGhiChu3;
        private DevExpress.XtraEditors.TextEdit txtTenHop;
        private DevExpress.XtraEditors.LabelControl lbGhiChu;
        private DevExpress.XtraEditors.TextEdit txtGhiChuKho;
        private DevExpress.XtraEditors.LabelControl lbQuay;
        private DevExpress.XtraEditors.LabelControl lbKho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.CheckEdit chkSuDungHop;
        private DevExpress.XtraEditors.CheckEdit chkSuDungKe;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LookUpEdit cboKho;
        private DevExpress.XtraEditors.LookUpEdit cboKe;
        private System.Windows.Forms.FlowLayoutPanel pnlHop;
        private System.Windows.Forms.FlowLayoutPanel pnlKe;
        private System.Windows.Forms.FlowLayoutPanel pnlKho;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.TextEdit txtSucChua;
        private DevExpress.XtraEditors.LabelControl labelControl1;




    }
}