﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_HoSo : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private String sNameFile = "";
        private Byte[] sByteDataFile;

        public Frm_HoSo()
        {
            InitializeComponent();
        }

        private void Frm_HoSo_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTieuDe.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("EXEC SP_DeleteHoSo " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTieuDe.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            if (txtFile.Text != "")
                sByteDataFile = clsQLCongTy.ReturnByteFile(txtFile.Text);

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertHoSo";

                sParameter = new SqlParameter[11];
                sParameter[0] = new SqlParameter("@TIEUDE", (Object)txtTieuDe.Text);
                sParameter[1] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[2] = new SqlParameter("@SOLUONGTO", (Object)txtSLTo.Text);
                sParameter[3] = new SqlParameter("@NGAYCAP", (DateTime)dtpNgayCap.EditValue);
                sParameter[4] = new SqlParameter("@NGAYHETHAN", (DateTime)dtpNgayHetHan.EditValue);
                sParameter[5] = new SqlParameter("@COQUANCAP", (Object)cboCoQuanCap.EditValue.ToString());
                sParameter[6] = new SqlParameter("@LOAIHOSO", (Object)cboLoaiHoSo.EditValue.ToString());
                sParameter[7] = new SqlParameter("@HOPHOSO", (Object)cboHopHoSo.EditValue.ToString());
                sParameter[8] = new SqlParameter("@NGUOITAO", (Object)clsGlobal.gsUserID);
                sParameter[9] = new SqlParameter("@NAME_FILES", (Object)sNameFile);
                sParameter[10] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[10].Value = sByteDataFile;
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateHoSo";

                sParameter = new SqlParameter[12];
                sParameter[0] = new SqlParameter("@TIEUDE", (Object)txtTieuDe.Text);
                sParameter[1] = new SqlParameter("@NOIDUNG", (Object)txtNoiDung.Text);
                sParameter[2] = new SqlParameter("@SOLUONGTO", (Object)txtSLTo.Text);
                sParameter[3] = new SqlParameter("@NGAYCAP", (DateTime)dtpNgayCap.EditValue);
                sParameter[4] = new SqlParameter("@NGAYHETHAN", (DateTime)dtpNgayHetHan.EditValue);
                sParameter[5] = new SqlParameter("@COQUANCAP", (Object)cboCoQuanCap.EditValue.ToString());
                sParameter[6] = new SqlParameter("@LOAIHOSO", (Object)cboLoaiHoSo.EditValue.ToString());
                sParameter[7] = new SqlParameter("@HOPHOSO", (Object)cboHopHoSo.EditValue.ToString());
                sParameter[8] = new SqlParameter("@NGUOISUA", (Object)clsGlobal.gsUserID);
                sParameter[9] = new SqlParameter("@NAME_FILES", (Object)sNameFile);
                sParameter[10] = new SqlParameter("@FILES", SqlDbType.VarBinary, int.MaxValue);
                sParameter[10].Value = sByteDataFile;
                sParameter[11] = new SqlParameter("@MA", (Object)sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTieuDe.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIEUDE").ToString();
            txtNoiDung.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NOIDUNG").ToString();
            txtSLTo.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOLUONGTO").ToString();
            cboCoQuanCap.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "COQUANCAP").ToString());
            cboLoaiHoSo.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAIHOSO").ToString());
            cboHopHoSo.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HOPHOSO").ToString());
            dtpNgayCap.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYCAP").ToString());
            dtpNgayHetHan.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYHETHAN").ToString());
            txtTenFile.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAME_FILES").ToString();
            sNameFile = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NAME_FILES").ToString();
            sByteDataFile = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FILES"));

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_SelectHoSo");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoCoQuan");
            clsQLCongTy.LoadCombo(myDT, cboCoQuanCap);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoLoaiHoSo");
            clsQLCongTy.LoadCombo(myDT, cboLoaiHoSo);

            myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs("SP_ComBoHopHoSo");
            clsQLCongTy.LoadCombo(myDT, cboHopHoSo);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("24031601");
            btnSua.Enabled = clsUserManagement.AllowEdit("24031601");
            btnXoa.Enabled = clsUserManagement.AllowDelete("24031601");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTieuDe.Properties.ReadOnly = sBoolean;
            txtNoiDung.Properties.ReadOnly = sBoolean;
            txtSLTo.Properties.ReadOnly = sBoolean;
            dtpNgayCap.Properties.ReadOnly = sBoolean;
            dtpNgayHetHan.Properties.ReadOnly = sBoolean;
            cboCoQuanCap.Properties.ReadOnly = sBoolean;
            cboLoaiHoSo.Properties.ReadOnly = sBoolean;
            cboHopHoSo.Properties.ReadOnly = sBoolean;
            btnFile.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sNameFile = "";
            sByteDataFile = new Byte[1];
            txtTenFile.Text = "";
            txtFile.Text = "";
            txtTieuDe.Text = "";
            txtNoiDung.Text = "";
            txtSLTo.Text = "";
            dtpNgayCap.EditValue = clsMain.GetServerDate();
            dtpNgayHetHan.EditValue = clsMain.GetServerDate();
            cboCoQuanCap.EditValue = cboCoQuanCap.Properties.GetDataSourceValue(cboCoQuanCap.Properties.ValueMember, 0);
            cboLoaiHoSo.EditValue = cboLoaiHoSo.Properties.GetDataSourceValue(cboLoaiHoSo.Properties.ValueMember, 0);
            cboHopHoSo.EditValue = cboHopHoSo.Properties.GetDataSourceValue(cboHopHoSo.Properties.ValueMember, 0);
        }

        private Boolean CheckInput()
        {
            if (txtTieuDe.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTieuDe.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTieuDe.Focus();
                return false;
            }
            if (cboCoQuanCap.EditValue == "" || cboCoQuanCap.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblCoQuanCap.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboCoQuanCap.Focus();
                return false;
            }
            if (cboLoaiHoSo.EditValue == "" || cboLoaiHoSo.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblLoaiHoSo.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboLoaiHoSo.Focus();
                return false;
            }
            if (cboHopHoSo.EditValue == "" || cboHopHoSo.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + lblHopHoSo.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHopHoSo.Focus();
                return false;
            }
            if (txtTenFile.EditValue == "")
            {
                XtraMessageBox.Show("Nhập " + lblFileVanBan.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenFile.Focus();
                return false;
            }
            if (txtFile.Text != "")
                if (!File.Exists(txtFile.Text))
                {
                    clsQLCongTy.MessageInformation("File không hợp lệ", "Thông báo");
                    txtFile.Focus();
                    return false;
                }

            return true;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog sOpenFile = new OpenFileDialog())
            {
                sOpenFile.Filter = clsQLCongTy.Filter();
                sOpenFile.FilterIndex = 1;
                sOpenFile.RestoreDirectory = true;
                sOpenFile.Multiselect = false;
                sOpenFile.Title = "Chọn File";
                if (sOpenFile.ShowDialog() == DialogResult.OK)
                {
                    txtFile.Text = sOpenFile.FileName;
                    sNameFile = sOpenFile.SafeFileName;
                    txtTenFile.Text = sOpenFile.SafeFileName;
                }
            }
        }

        private void btnDownLoadFile_Click(object sender, EventArgs e)
        {
            if (sNameFile == "" || sNameFile == null)
                return;
            using (SaveFileDialog sSaveFile = new SaveFileDialog())
            {
                sSaveFile.Filter = clsQLCongTy.Filter();
                sSaveFile.FilterIndex = 1;
                sSaveFile.RestoreDirectory = true;
                sSaveFile.Title = "Chọn File";
                sSaveFile.FileName = sNameFile;
                if (sSaveFile.ShowDialog() == DialogResult.OK)
                    if(clsQLCongTy.ReadByteFile(sSaveFile.FileName, sByteDataFile))
                        clsQLCongTy.MessageInformation("DownLoad File thành công", "Thông báo");
                    else
                        clsQLCongTy.MessageInformation("DownLoad File không thành công", "Thông báo");
            }
        }
    }
}