﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_KhoKeHop_HoSo : DevExpress.XtraEditors.XtraForm
    {
        public Frm_KhoKeHop_HoSo()
        {
            InitializeComponent();
            LoadCombo();
            LoadDataKhoHoSo();
            LoadPermission();
        }

        string sMaKho = "";
        string sMaKe = "";
        string sMaHop = "";

        private void Frm_KhoKeHop_HoSo_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoadDataKhoHoSo();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTenKho.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMaKho == "" && sMaKe == "" && sMaHop == "")//Chưa chọn dòng để xóa
                return;

            if (DevExpress.XtraEditors.XtraMessageBox.Show( _xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "DELETE FROM DM_KHOHOSO" + "\n";
            sSQL += "WHERE MA = " + clsMain.SQLString(sMaKho) + "\n";
            sSQL += "DELETE FROM DM_KEHOSO" + "\n";
            sSQL += "WHERE MA = " + clsMain.SQLString(sMaKe) + "\n";
            sSQL += "DELETE FROM DM_HOPHOSO" + "\n";
            sSQL += "WHERE MA = " + clsMain.SQLString(sMaHop) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoadDataKhoHoSo();
            }
            else
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMaKho == "" && sMaKe == "" && sMaHop == "")//Chưa chọn dòng để sữa
                return;

            KhoaMoControl(false);
            if (sMaKho != "")
            {
                txtTenKho.Focus();
            }
            else if (sMaKe != "")
            {
                cboKho.Focus();
            }
            else if (sMaHop != "")
            {
                cboKe.Focus();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            String sSQL = "";
            if (txtTenKho.Text != "")
            {
                if (sMaKho == "" && btnThem.Enabled)
                {
                    sSQL += "INSERT INTO DM_KHOHOSO(TEN, GHICHU, SUDUNG)" + "\n";
                    sSQL += "VALUES ( ";
                    sSQL += clsMain.SQLStringUnicode(txtTenKho.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChuKho.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSuDungKho.Checked) + ")";
                }
                else if ((sMaKho != "" && btnSua.Enabled))
                {
                    sSQL += "UPDATE DM_KHOHOSO SET " + "\n";
                    sSQL += "TEN = " + clsMain.SQLStringUnicode(txtTenKho.Text) + "," + "\n";
                    sSQL += "GHICHU = " + clsMain.SQLStringUnicode(txtGhiChuKho.Text) + "," + "\n";
                    sSQL += "SUDUNG = " + clsMain.SQLBit(chkSuDungKho.Checked) + "\n";
                    sSQL += "WHERE MA = " + clsMain.SQLString(sMaKho) + "\n";
                }
            }
            if (txtTenKe.Text != "")
            {
                if (sMaKe == "" && btnThem.Enabled)
                {
                    sSQL += "INSERT INTO DM_KEHOSO(KHOHOSO, TEN, GHICHU, SUDUNG)" + "\n";
                    sSQL += "VALUES ( ";
                    sSQL += clsMain.SQLString(cboKho.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTenKe.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChuKe.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSuDungKe.Checked) + ")";
                }
                else if ((sMaKe != "" && btnSua.Enabled))
                {
                    sSQL += "UPDATE DM_KEHOSO SET " + "\n";
                    sSQL += "KHOHOSO = " + clsMain.SQLString(cboKho.EditValue.ToString()) + "," + "\n";
                    sSQL += "TEN = " + clsMain.SQLStringUnicode(txtTenKe.Text) + "," + "\n";
                    sSQL += "GHICHU = " + clsMain.SQLStringUnicode(txtGhiChuKe.Text) + "," + "\n";
                    sSQL += "SUDUNG = " + clsMain.SQLBit(chkSuDungKe.Checked) + "\n";
                    sSQL += "WHERE MA = " + clsMain.SQLString(sMaKe) + "\n";
                }
            }
            if (txtTenHop.Text != "")
            {
                if (sMaHop == "" && btnThem.Enabled)
                {
                    sSQL += "INSERT INTO DM_HOPHOSO (KEHOSO, TEN, SUCCHUA, GHICHU, SUDUNG)" + "\n";
                    sSQL += "VALUES ( ";
                    sSQL += clsMain.SQLString(cboKe.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTenHop.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtSucChua.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChuHop.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSuDungHop.Checked) + ")";
                }
                else if ((sMaHop != "" && btnSua.Enabled))
                {
                    sSQL += "UPDATE DM_HOPHOSO SET " + "\n";
                    sSQL += "KEHOSO = " + clsMain.SQLString(cboKe.EditValue.ToString()) + "," + "\n";
                    sSQL += "TEN = " + clsMain.SQLStringUnicode(txtTenHop.Text) + "," + "\n";
                    sSQL += "SUCCHUA = " + clsMain.SQLStringUnicode(txtSucChua.Text) + "," + "\n";
                    sSQL += "GHICHU = " + clsMain.SQLStringUnicode(txtGhiChuHop.Text) + "," + "\n";
                    sSQL += "SUDUNG = " + clsMain.SQLBit(chkSuDungHop.Checked) + "\n";
                    sSQL += "WHERE MA = " + clsMain.SQLString(sMaHop) + "\n";
                }
            }


            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadDataKhoHoSo();
                LoadCombo();
                btnThem.Focus();
            }
            else
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void KhoHoSo_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlKho.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            pnlKe.Controls.Clear();
            pnlHop.Controls.Clear();
            pnlKe.Padding = new Padding(btn .Location .X-3 , 0, 0, 0);
           
            LoadDataKeHoSo(btn.Name.Substring(1, btn.Name.Length - 1));

            sMaKho = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTenKho.Text = myDT_Kho.Select("MA = " + clsMain.SQLString(sMaKho))[0]["TEN"].ToString();
            txtGhiChuKho.Text = myDT_Kho.Select("MA = " + clsMain.SQLString(sMaKho))[0]["GHICHU"].ToString();
            chkSuDungKho.Checked = Boolean.Parse(myDT_Kho.Select("MA = " + clsMain.SQLString(sMaKho))[0]["SUDUNG"].ToString());

            sMaKe = "";
            sMaHop = "";
            cboKho.EditValue = cboKho.Properties.GetDataSourceValue(cboKho.Properties.ValueMember, 0);
            cboKe.EditValue = cboKe.Properties.GetDataSourceValue(cboKe.Properties.ValueMember, 0);
            txtTenKe.Text = "";
            txtTenHop.Text = "";
            txtGhiChuHop.Text = "";
            txtGhiChuKe.Text = "";
            chkSuDungKe.Checked = true;
            chkSuDungHop.Checked = true;

        }

        private void KeHoSo_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlKe.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            pnlHop.Controls.Clear();
            pnlHop.Padding = new Padding(btn.Location.X - 3, 0, 0, 0);

            LoadDataHopHoSo(btn.Name.Substring(1, btn.Name.Length - 1));

            sMaKe = btn.Name.Substring(1, btn.Name.Length - 1);
            cboKho.EditValue = int.Parse(myDT_Ke.Select("MA = " + clsMain.SQLString(sMaKe))[0]["MA"].ToString());
            txtTenKe.Text = myDT_Ke.Select("MA = " + clsMain.SQLString(sMaKe))[0]["TEN"].ToString();
            txtGhiChuKe.Text = myDT_Ke.Select("MA = " + clsMain.SQLString(sMaKe))[0]["GHICHU"].ToString();
            chkSuDungKe.Checked = bool.Parse(myDT_Ke.Select("MA = " + clsMain.SQLString(sMaKe))[0]["SUDUNG"].ToString());

            sMaKho = "";
            sMaHop = "";
            txtTenKho.Text = "";
            txtGhiChuKho.Text = "";
            chkSuDungKho.Checked = true;
            cboKe.EditValue = cboKe.Properties.GetDataSourceValue(cboKe.Properties.ValueMember, 0);
            txtTenHop.Text = "";
            txtGhiChuHop.Text = "";
            txtSucChua.Text = "";
            chkSuDungHop.Checked = true;
        }

        private void HopHoSo_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlHop.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
         
            sMaHop = btn.Name.Substring(1, btn.Name.Length - 1);
            cboKe.EditValue = int.Parse(myDT_Hop.Select("MA = " + clsMain.SQLString(sMaHop))[0]["MA"].ToString());
            txtTenHop.Text = myDT_Hop.Select("MA = " + clsMain.SQLString(sMaHop))[0]["TEN"].ToString();
            txtSucChua.Text = myDT_Hop.Select("MA = " + clsMain.SQLString(sMaHop))[0]["SUCCHUA"].ToString();
            txtGhiChuHop.Text = myDT_Hop.Select("MA = " + clsMain.SQLString(sMaHop))[0]["GHICHU"].ToString();
            chkSuDungHop.Checked = bool.Parse(myDT_Hop.Select("MA = " + clsMain.SQLString(sMaHop))[0]["SUDUNG"].ToString());

            sMaKho = "";
            sMaKe = "";
            txtTenKho.Text = "";
            txtGhiChuKho.Text = "";
            chkSuDungKho.Checked = true;
            cboKho.EditValue = cboKho.Properties.GetDataSourceValue(cboKho.Properties.ValueMember, 0);
            txtTenKe.Text = "";
            txtGhiChuKe.Text = "";
            chkSuDungKe.Checked = true;
        }

        DataTable myDT_Kho = new DataTable();
        private void LoadDataKhoHoSo()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "SELECT MA, TEN, GHICHU, SUDUNG" + "\n";
            sSQL += "From DM_KHOHOSO" + "\n";
            sSQL += "ORDER BY SUDUNG DESC, TEN" + "\n";
            myDT_Kho = clsMain.ReturnDataTable(sSQL);

            pnlKho.Controls.Clear();
            pnlKe.Controls.Clear();
            pnlHop.Controls.Clear();
            foreach (DataRow dr in myDT_Kho.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlKho.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                //btn.Image = global::KP_RES.Properties.Resources.geo_fence_32;
                btn.Name = "C" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(KhoHoSo_Click);
                pnlKho.Controls.Add(btn);
            }
        }

        DataTable myDT_Ke = new DataTable();
        private void LoadDataKeHoSo(String MaKho)
        {
            string sSQL = "";
            sSQL += "SELECT MA, KHOHOSO, TEN, GHICHU, SUDUNG" + "\n";
            sSQL += "FROM DM_KEHOSO" + "\n";
            sSQL += "WHERE KHOHOSO = " + clsMain.SQLString(MaKho) + "\n";
            sSQL += "ORDER BY SUDUNG DESC, TEN" + "\n";
            myDT_Ke = clsMain.ReturnDataTable(sSQL);
            pnlKe.Controls.Clear();
            foreach (DataRow dr in myDT_Ke.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlKe.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                //btn.Image = global::KP_RES.Properties.Resources.tent_32;
                btn.Name = "K" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(KeHoSo_Click);
                pnlKe.Controls.Add(btn);
            }
        }

        DataTable myDT_Hop = new DataTable();
        private void LoadDataHopHoSo(String MaKe)
        {
            string sSQL = "";
            sSQL += "SELECT MA, KEHOSO, TEN, SUCCHUA, GHICHU, SUDUNG" + "\n";
            sSQL += "FROM DM_HOPHOSO" + "\n";
            sSQL += "WHERE KEHOSO = " + clsMain.SQLString(MaKe) + "\n";
            sSQL += "ORDER BY SUDUNG DESC, TEN" + "\n";
            myDT_Hop = clsMain.ReturnDataTable(sSQL);
            pnlHop.Controls.Clear();
            foreach (DataRow dr in myDT_Hop.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlHop.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                //btn.Image = global::KP_RES.Properties.Resources.calculator_32;
                btn.Name = "Q" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(HopHoSo_Click);
                pnlHop.Controls.Add(btn);
            }
        }

        private void LoadCombo()
        {
            String sSQL = "";
            sSQL += "SELECT MA, TEN" + "\n";
            sSQL += "FROM DM_KHOHOSO" + "\n";
            sSQL += "WHERE SUDUNG = 1" + "\n";
            sSQL += "ORDER BY TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboKho.Properties.DataSource = dt;
            cboKho.EditValue = cboKho.Properties.GetDataSourceValue(cboKho.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "SELECT MA, TEN " + "\n";
            sSQL += "FROM DM_KEHOSO" + "\n";
            sSQL += "WHERE SUDUNG = 1" + "\n";
            sSQL += "ORDER BY TEN" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboKe.Properties.DataSource = dt;
            cboKe.EditValue = cboKe.Properties.GetDataSourceValue(cboKe.Properties.ValueMember, 0);
        }

        private void LoadPermission()
        {
            //btnThem.Enabled = clsUserManagement.AllowAdd("4");
            //btnSua.Enabled = clsUserManagement.AllowEdit("4");
            //btnXoa.Enabled = clsUserManagement.AllowDelete("4");
            //btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTenKho.Properties.ReadOnly = sBoolean;
            txtGhiChuKho.Properties.ReadOnly = sBoolean;
            chkSuDungKho.Properties.ReadOnly = sBoolean;
            cboKho.Properties.ReadOnly = sBoolean;
            txtTenKe.Properties.ReadOnly = sBoolean;
            txtGhiChuKe.Properties.ReadOnly = sBoolean;
            chkSuDungKe.Properties.ReadOnly = sBoolean;
            cboKe.Properties.ReadOnly = sBoolean;
            txtTenHop.Properties.ReadOnly = sBoolean;
            txtGhiChuHop.Properties.ReadOnly = sBoolean;
            txtSucChua.Properties.ReadOnly = sBoolean;
            chkSuDungHop.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMaKho = "";
            sMaKe = "";
            sMaHop = "";
            txtTenKho.Text = "";
            txtGhiChuKho.Text = "";
            chkSuDungKho.Checked = true;
            cboKho.EditValue = cboKho.Properties.GetDataSourceValue(cboKho.Properties.ValueMember, 0);
            txtTenKe.Text = "";
            txtGhiChuKe.Text = "";
            chkSuDungKe.Checked = true;
            cboKe.EditValue = cboKe.Properties.GetDataSourceValue(cboKe.Properties.ValueMember, 0);
            txtTenHop.Text = "";
            txtGhiChuHop.Text = "";
            txtSucChua.Text = "";
            chkSuDungHop.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTenKho.Text == "" && txtTenKe.Text == "" && txtTenHop.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_chuanhapttdaydu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenKho.Focus();
                return false;
            }
            return true;
        }

        string _chuanhapttdaydu = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_KhoKeHop_HoSo_Load(object sender, EventArgs e)
        {
            this.Text = rm.GetString("cuahangkhoquay", culture);
            
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            lbGhiChu2.Text = rm.GetString("ghichu", culture);
            lbGhiChu3.Text = rm.GetString("ghichu", culture);
            chkSuDungKho.Text = rm.GetString("sudung", culture);
            chkSuDungKe.Text = rm.GetString("sudung", culture);
            chkSuDungHop.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _chuanhapttdaydu = rm.GetString("chuanhapttdaydu", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

        }
    }
}