﻿namespace KP_RES 
{
    partial class Frm_HoSo 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cboLoaiHoSo = new DevExpress.XtraEditors.LookUpEdit();
            this.cboHopHoSo = new DevExpress.XtraEditors.LookUpEdit();
            this.cboCoQuanCap = new DevExpress.XtraEditors.LookUpEdit();
            this.dtpNgayHetHan = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgayCap = new DevExpress.XtraEditors.DateEdit();
            this.txtNoiDung = new DevExpress.XtraEditors.TextEdit();
            this.txtTieuDe = new DevExpress.XtraEditors.TextEdit();
            this.txtSLTo = new DevExpress.XtraEditors.TextEdit();
            this.lblTenFile = new DevExpress.XtraEditors.LabelControl();
            this.txtTenFile = new DevExpress.XtraEditors.TextEdit();
            this.btnDownLoadFile = new DevExpress.XtraEditors.SimpleButton();
            this.lblCoQuanCap = new DevExpress.XtraEditors.LabelControl();
            this.txtFile = new DevExpress.XtraEditors.TextEdit();
            this.btnFile = new DevExpress.XtraEditors.SimpleButton();
            this.lblFileVanBan = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayHetHan = new DevExpress.XtraEditors.LabelControl();
            this.lblHopHoSo = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayCap = new DevExpress.XtraEditors.LabelControl();
            this.lblSoLuongTo = new DevExpress.XtraEditors.LabelControl();
            this.lblTieuDe = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lblNoiDung = new DevExpress.XtraEditors.LabelControl();
            this.lblLoaiHoSo = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COQUANCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIHOSO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HOPHOSO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIEUDE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOIDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONGTO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYHETHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENCOQUAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAME_FILES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAI_HS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKHO_HS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKE_HS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHOP_HS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CQNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NHANVIENXULY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiHoSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHopHoSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTieuDe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cboLoaiHoSo);
            this.panelControl1.Controls.Add(this.cboHopHoSo);
            this.panelControl1.Controls.Add(this.cboCoQuanCap);
            this.panelControl1.Controls.Add(this.dtpNgayHetHan);
            this.panelControl1.Controls.Add(this.dtpNgayCap);
            this.panelControl1.Controls.Add(this.txtNoiDung);
            this.panelControl1.Controls.Add(this.txtTieuDe);
            this.panelControl1.Controls.Add(this.txtSLTo);
            this.panelControl1.Controls.Add(this.lblTenFile);
            this.panelControl1.Controls.Add(this.txtTenFile);
            this.panelControl1.Controls.Add(this.btnDownLoadFile);
            this.panelControl1.Controls.Add(this.lblCoQuanCap);
            this.panelControl1.Controls.Add(this.txtFile);
            this.panelControl1.Controls.Add(this.btnFile);
            this.panelControl1.Controls.Add(this.lblFileVanBan);
            this.panelControl1.Controls.Add(this.lblNgayHetHan);
            this.panelControl1.Controls.Add(this.lblHopHoSo);
            this.panelControl1.Controls.Add(this.lblNgayCap);
            this.panelControl1.Controls.Add(this.lblSoLuongTo);
            this.panelControl1.Controls.Add(this.lblTieuDe);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lblNoiDung);
            this.panelControl1.Controls.Add(this.lblLoaiHoSo);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1258, 175);
            this.panelControl1.TabIndex = 0;
            // 
            // cboLoaiHoSo
            // 
            this.cboLoaiHoSo.EnterMoveNextControl = true;
            this.cboLoaiHoSo.Location = new System.Drawing.Point(83, 69);
            this.cboLoaiHoSo.Name = "cboLoaiHoSo";
            this.cboLoaiHoSo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiHoSo.Properties.Appearance.Options.UseFont = true;
            this.cboLoaiHoSo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiHoSo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoaiHoSo.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoaiHoSo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoaiHoSo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboLoaiHoSo.Properties.DisplayMember = "TEN";
            this.cboLoaiHoSo.Properties.DropDownItemHeight = 40;
            this.cboLoaiHoSo.Properties.NullText = "";
            this.cboLoaiHoSo.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoaiHoSo.Properties.ShowHeader = false;
            this.cboLoaiHoSo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoaiHoSo.Properties.ValueMember = "MA";
            this.cboLoaiHoSo.Size = new System.Drawing.Size(248, 26);
            this.cboLoaiHoSo.TabIndex = 13;
            // 
            // cboHopHoSo
            // 
            this.cboHopHoSo.EnterMoveNextControl = true;
            this.cboHopHoSo.Location = new System.Drawing.Point(440, 69);
            this.cboHopHoSo.Name = "cboHopHoSo";
            this.cboHopHoSo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHopHoSo.Properties.Appearance.Options.UseFont = true;
            this.cboHopHoSo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHopHoSo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboHopHoSo.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboHopHoSo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboHopHoSo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KHO", "KHO"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KE", "KE"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboHopHoSo.Properties.DisplayMember = "TEN";
            this.cboHopHoSo.Properties.DropDownItemHeight = 40;
            this.cboHopHoSo.Properties.NullText = "";
            this.cboHopHoSo.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboHopHoSo.Properties.ShowHeader = false;
            this.cboHopHoSo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboHopHoSo.Properties.ValueMember = "MA";
            this.cboHopHoSo.Size = new System.Drawing.Size(248, 26);
            this.cboHopHoSo.TabIndex = 15;
            // 
            // cboCoQuanCap
            // 
            this.cboCoQuanCap.EnterMoveNextControl = true;
            this.cboCoQuanCap.Location = new System.Drawing.Point(783, 37);
            this.cboCoQuanCap.Name = "cboCoQuanCap";
            this.cboCoQuanCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanCap.Properties.Appearance.Options.UseFont = true;
            this.cboCoQuanCap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCoQuanCap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCoQuanCap.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCoQuanCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCoQuanCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboCoQuanCap.Properties.DisplayMember = "TEN";
            this.cboCoQuanCap.Properties.DropDownItemHeight = 40;
            this.cboCoQuanCap.Properties.NullText = "";
            this.cboCoQuanCap.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCoQuanCap.Properties.ShowHeader = false;
            this.cboCoQuanCap.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCoQuanCap.Properties.ValueMember = "MA";
            this.cboCoQuanCap.Size = new System.Drawing.Size(248, 26);
            this.cboCoQuanCap.TabIndex = 11;
            // 
            // dtpNgayHetHan
            // 
            this.dtpNgayHetHan.EditValue = null;
            this.dtpNgayHetHan.EnterMoveNextControl = true;
            this.dtpNgayHetHan.Location = new System.Drawing.Point(440, 37);
            this.dtpNgayHetHan.Name = "dtpNgayHetHan";
            this.dtpNgayHetHan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHan.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayHetHan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayHetHan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayHetHan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHetHan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHetHan.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHetHan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHetHan.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayHetHan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayHetHan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayHetHan.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayHetHan.TabIndex = 9;
            // 
            // dtpNgayCap
            // 
            this.dtpNgayCap.EditValue = null;
            this.dtpNgayCap.EnterMoveNextControl = true;
            this.dtpNgayCap.Location = new System.Drawing.Point(83, 37);
            this.dtpNgayCap.Name = "dtpNgayCap";
            this.dtpNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayCap.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayCap.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayCap.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayCap.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayCap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayCap.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayCap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayCap.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayCap.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayCap.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayCap.Size = new System.Drawing.Size(248, 26);
            this.dtpNgayCap.TabIndex = 7;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.EnterMoveNextControl = true;
            this.txtNoiDung.Location = new System.Drawing.Point(440, 5);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtNoiDung.Size = new System.Drawing.Size(248, 26);
            this.txtNoiDung.TabIndex = 3;
            // 
            // txtTieuDe
            // 
            this.txtTieuDe.EnterMoveNextControl = true;
            this.txtTieuDe.Location = new System.Drawing.Point(83, 5);
            this.txtTieuDe.Name = "txtTieuDe";
            this.txtTieuDe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuDe.Properties.Appearance.Options.UseFont = true;
            this.txtTieuDe.Size = new System.Drawing.Size(248, 26);
            this.txtTieuDe.TabIndex = 1;
            // 
            // txtSLTo
            // 
            this.txtSLTo.EnterMoveNextControl = true;
            this.txtSLTo.Location = new System.Drawing.Point(783, 5);
            this.txtSLTo.Name = "txtSLTo";
            this.txtSLTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLTo.Properties.Appearance.Options.UseFont = true;
            this.txtSLTo.Size = new System.Drawing.Size(248, 26);
            this.txtSLTo.TabIndex = 5;
            // 
            // lblTenFile
            // 
            this.lblTenFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenFile.Location = new System.Drawing.Point(341, 104);
            this.lblTenFile.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenFile.Name = "lblTenFile";
            this.lblTenFile.Size = new System.Drawing.Size(56, 19);
            this.lblTenFile.TabIndex = 19;
            this.lblTenFile.Text = "Tên File";
            // 
            // txtTenFile
            // 
            this.txtTenFile.EnterMoveNextControl = true;
            this.txtTenFile.Location = new System.Drawing.Point(440, 101);
            this.txtTenFile.Name = "txtTenFile";
            this.txtTenFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenFile.Properties.Appearance.Options.UseFont = true;
            this.txtTenFile.Properties.AutoHeight = false;
            this.txtTenFile.Properties.ReadOnly = true;
            this.txtTenFile.Size = new System.Drawing.Size(248, 26);
            this.txtTenFile.TabIndex = 20;
            // 
            // btnDownLoadFile
            // 
            this.btnDownLoadFile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownLoadFile.Appearance.Options.UseFont = true;
            this.btnDownLoadFile.Image = global::KP_RES.Properties.Resources.down_26;
            this.btnDownLoadFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDownLoadFile.Location = new System.Drawing.Point(442, 134);
            this.btnDownLoadFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownLoadFile.Name = "btnDownLoadFile";
            this.btnDownLoadFile.Size = new System.Drawing.Size(87, 35);
            this.btnDownLoadFile.TabIndex = 25;
            this.btnDownLoadFile.Text = "Tải File";
            this.btnDownLoadFile.Click += new System.EventHandler(this.btnDownLoadFile_Click);
            // 
            // lblCoQuanCap
            // 
            this.lblCoQuanCap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanCap.Location = new System.Drawing.Point(692, 40);
            this.lblCoQuanCap.Margin = new System.Windows.Forms.Padding(4);
            this.lblCoQuanCap.Name = "lblCoQuanCap";
            this.lblCoQuanCap.Size = new System.Drawing.Size(88, 19);
            this.lblCoQuanCap.TabIndex = 10;
            this.lblCoQuanCap.Text = "Cơ quan cấp";
            // 
            // txtFile
            // 
            this.txtFile.EnterMoveNextControl = true;
            this.txtFile.Location = new System.Drawing.Point(83, 101);
            this.txtFile.Name = "txtFile";
            this.txtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Properties.Appearance.Options.UseFont = true;
            this.txtFile.Properties.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(185, 26);
            this.txtFile.TabIndex = 17;
            // 
            // btnFile
            // 
            this.btnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile.Appearance.Options.UseFont = true;
            this.btnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(275, 102);
            this.btnFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(56, 25);
            this.btnFile.TabIndex = 18;
            this.btnFile.Text = "....";
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblFileVanBan
            // 
            this.lblFileVanBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileVanBan.Location = new System.Drawing.Point(6, 104);
            this.lblFileVanBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileVanBan.Name = "lblFileVanBan";
            this.lblFileVanBan.Size = new System.Drawing.Size(24, 19);
            this.lblFileVanBan.TabIndex = 16;
            this.lblFileVanBan.Text = "File";
            // 
            // lblNgayHetHan
            // 
            this.lblNgayHetHan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHetHan.Location = new System.Drawing.Point(341, 40);
            this.lblNgayHetHan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayHetHan.Name = "lblNgayHetHan";
            this.lblNgayHetHan.Size = new System.Drawing.Size(94, 19);
            this.lblNgayHetHan.TabIndex = 8;
            this.lblNgayHetHan.Text = "Ngày hết hạn";
            // 
            // lblHopHoSo
            // 
            this.lblHopHoSo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopHoSo.Location = new System.Drawing.Point(341, 72);
            this.lblHopHoSo.Margin = new System.Windows.Forms.Padding(4);
            this.lblHopHoSo.Name = "lblHopHoSo";
            this.lblHopHoSo.Size = new System.Drawing.Size(73, 19);
            this.lblHopHoSo.TabIndex = 14;
            this.lblHopHoSo.Text = "Hộp hồ sơ";
            // 
            // lblNgayCap
            // 
            this.lblNgayCap.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayCap.Location = new System.Drawing.Point(6, 40);
            this.lblNgayCap.Margin = new System.Windows.Forms.Padding(4);
            this.lblNgayCap.Name = "lblNgayCap";
            this.lblNgayCap.Size = new System.Drawing.Size(65, 19);
            this.lblNgayCap.TabIndex = 6;
            this.lblNgayCap.Text = "Ngày cấp";
            // 
            // lblSoLuongTo
            // 
            this.lblSoLuongTo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongTo.Location = new System.Drawing.Point(692, 8);
            this.lblSoLuongTo.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoLuongTo.Name = "lblSoLuongTo";
            this.lblSoLuongTo.Size = new System.Drawing.Size(83, 19);
            this.lblSoLuongTo.TabIndex = 4;
            this.lblSoLuongTo.Text = "Số lượng tờ";
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTieuDe.Location = new System.Drawing.Point(6, 8);
            this.lblTieuDe.Margin = new System.Windows.Forms.Padding(4);
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Size = new System.Drawing.Size(53, 19);
            this.lblTieuDe.TabIndex = 0;
            this.lblTieuDe.Text = "Tiêu đề";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(83, 134);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 21;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(259, 134);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 23;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(347, 134);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 24;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(171, 134);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 22;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiDung.Location = new System.Drawing.Point(341, 8);
            this.lblNoiDung.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(65, 19);
            this.lblNoiDung.TabIndex = 2;
            this.lblNoiDung.Text = "Nội dung";
            // 
            // lblLoaiHoSo
            // 
            this.lblLoaiHoSo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiHoSo.Location = new System.Drawing.Point(6, 72);
            this.lblLoaiHoSo.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoaiHoSo.Name = "lblLoaiHoSo";
            this.lblLoaiHoSo.Size = new System.Drawing.Size(73, 19);
            this.lblLoaiHoSo.TabIndex = 12;
            this.lblLoaiHoSo.Text = "Loại hồ sơ";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 175);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1219, 476);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FILES,
            this.COQUANCAP,
            this.LOAIHOSO,
            this.HOPHOSO,
            this.STT,
            this.MA,
            this.TIEUDE,
            this.NOIDUNG,
            this.SOLUONGTO,
            this.NGAYCAP,
            this.NGAYHETHAN,
            this.TENCOQUAN,
            this.NAME_FILES,
            this.TENLOAI_HS,
            this.TENKHO_HS,
            this.TENKE_HS,
            this.TENHOP_HS,
            this.FILL,
            this.CQNHAN,
            this.NHANVIENXULY});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // FILES
            // 
            this.FILES.FieldName = "FILES";
            this.FILES.Name = "FILES";
            this.FILES.OptionsColumn.AllowEdit = false;
            this.FILES.OptionsColumn.AllowFocus = false;
            this.FILES.OptionsColumn.FixedWidth = true;
            // 
            // COQUANCAP
            // 
            this.COQUANCAP.FieldName = "COQUANCAP";
            this.COQUANCAP.Name = "COQUANCAP";
            this.COQUANCAP.OptionsColumn.AllowEdit = false;
            this.COQUANCAP.OptionsColumn.AllowFocus = false;
            this.COQUANCAP.OptionsColumn.AllowMove = false;
            // 
            // LOAIHOSO
            // 
            this.LOAIHOSO.FieldName = "LOAIHOSO";
            this.LOAIHOSO.Name = "LOAIHOSO";
            this.LOAIHOSO.OptionsColumn.AllowEdit = false;
            this.LOAIHOSO.OptionsColumn.AllowFocus = false;
            this.LOAIHOSO.OptionsColumn.AllowMove = false;
            // 
            // HOPHOSO
            // 
            this.HOPHOSO.FieldName = "HOPHOSO";
            this.HOPHOSO.Name = "HOPHOSO";
            this.HOPHOSO.OptionsColumn.AllowEdit = false;
            this.HOPHOSO.OptionsColumn.AllowFocus = false;
            this.HOPHOSO.OptionsColumn.AllowMove = false;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 60;
            // 
            // TIEUDE
            // 
            this.TIEUDE.AppearanceCell.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIEUDE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIEUDE.AppearanceHeader.Options.UseFont = true;
            this.TIEUDE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIEUDE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIEUDE.Caption = "Tiêu đề";
            this.TIEUDE.FieldName = "TIEUDE";
            this.TIEUDE.Name = "TIEUDE";
            this.TIEUDE.OptionsColumn.AllowEdit = false;
            this.TIEUDE.OptionsColumn.AllowFocus = false;
            this.TIEUDE.OptionsColumn.FixedWidth = true;
            this.TIEUDE.Visible = true;
            this.TIEUDE.VisibleIndex = 2;
            this.TIEUDE.Width = 200;
            // 
            // NOIDUNG
            // 
            this.NOIDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NOIDUNG.AppearanceHeader.Options.UseFont = true;
            this.NOIDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.NOIDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NOIDUNG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NOIDUNG.Caption = "Nội dung";
            this.NOIDUNG.FieldName = "NOIDUNG";
            this.NOIDUNG.Name = "NOIDUNG";
            this.NOIDUNG.OptionsColumn.AllowEdit = false;
            this.NOIDUNG.OptionsColumn.AllowFocus = false;
            this.NOIDUNG.OptionsColumn.FixedWidth = true;
            this.NOIDUNG.Visible = true;
            this.NOIDUNG.VisibleIndex = 3;
            this.NOIDUNG.Width = 200;
            // 
            // SOLUONGTO
            // 
            this.SOLUONGTO.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONGTO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONGTO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONGTO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONGTO.AppearanceHeader.Options.UseFont = true;
            this.SOLUONGTO.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONGTO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONGTO.Caption = "SL tờ";
            this.SOLUONGTO.FieldName = "SOLUONGTO";
            this.SOLUONGTO.Name = "SOLUONGTO";
            this.SOLUONGTO.OptionsColumn.AllowEdit = false;
            this.SOLUONGTO.OptionsColumn.AllowFocus = false;
            this.SOLUONGTO.OptionsColumn.FixedWidth = true;
            this.SOLUONGTO.Visible = true;
            this.SOLUONGTO.VisibleIndex = 4;
            this.SOLUONGTO.Width = 80;
            // 
            // NGAYCAP
            // 
            this.NGAYCAP.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYCAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYCAP.AppearanceHeader.Options.UseFont = true;
            this.NGAYCAP.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCAP.Caption = "Ngày cấp";
            this.NGAYCAP.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYCAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYCAP.FieldName = "NGAYCAP";
            this.NGAYCAP.Name = "NGAYCAP";
            this.NGAYCAP.OptionsColumn.AllowEdit = false;
            this.NGAYCAP.OptionsColumn.AllowFocus = false;
            this.NGAYCAP.OptionsColumn.FixedWidth = true;
            this.NGAYCAP.Visible = true;
            this.NGAYCAP.VisibleIndex = 5;
            this.NGAYCAP.Width = 120;
            // 
            // NGAYHETHAN
            // 
            this.NGAYHETHAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYHETHAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYHETHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYHETHAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYHETHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYHETHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYHETHAN.Caption = "Ngày hết hạn";
            this.NGAYHETHAN.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAYHETHAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYHETHAN.FieldName = "NGAYHETHAN";
            this.NGAYHETHAN.Name = "NGAYHETHAN";
            this.NGAYHETHAN.OptionsColumn.AllowEdit = false;
            this.NGAYHETHAN.OptionsColumn.AllowFocus = false;
            this.NGAYHETHAN.OptionsColumn.FixedWidth = true;
            this.NGAYHETHAN.Visible = true;
            this.NGAYHETHAN.VisibleIndex = 6;
            this.NGAYHETHAN.Width = 120;
            // 
            // TENCOQUAN
            // 
            this.TENCOQUAN.AppearanceCell.Options.UseTextOptions = true;
            this.TENCOQUAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENCOQUAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENCOQUAN.AppearanceHeader.Options.UseFont = true;
            this.TENCOQUAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENCOQUAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENCOQUAN.Caption = "Cơ quan cấp";
            this.TENCOQUAN.FieldName = "TENCOQUAN";
            this.TENCOQUAN.Name = "TENCOQUAN";
            this.TENCOQUAN.OptionsColumn.AllowEdit = false;
            this.TENCOQUAN.OptionsColumn.AllowFocus = false;
            this.TENCOQUAN.OptionsColumn.FixedWidth = true;
            this.TENCOQUAN.Visible = true;
            this.TENCOQUAN.VisibleIndex = 7;
            this.TENCOQUAN.Width = 200;
            // 
            // NAME_FILES
            // 
            this.NAME_FILES.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NAME_FILES.AppearanceHeader.Options.UseFont = true;
            this.NAME_FILES.AppearanceHeader.Options.UseTextOptions = true;
            this.NAME_FILES.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NAME_FILES.Caption = "Tên file";
            this.NAME_FILES.FieldName = "NAME_FILES";
            this.NAME_FILES.Name = "NAME_FILES";
            this.NAME_FILES.OptionsColumn.AllowEdit = false;
            this.NAME_FILES.OptionsColumn.AllowFocus = false;
            this.NAME_FILES.OptionsColumn.FixedWidth = true;
            this.NAME_FILES.Visible = true;
            this.NAME_FILES.VisibleIndex = 8;
            this.NAME_FILES.Width = 100;
            // 
            // TENLOAI_HS
            // 
            this.TENLOAI_HS.AppearanceCell.Options.UseTextOptions = true;
            this.TENLOAI_HS.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENLOAI_HS.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAI_HS.AppearanceHeader.Options.UseFont = true;
            this.TENLOAI_HS.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAI_HS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAI_HS.Caption = "Loại hồ sơ";
            this.TENLOAI_HS.FieldName = "TENLOAI_HS";
            this.TENLOAI_HS.Name = "TENLOAI_HS";
            this.TENLOAI_HS.OptionsColumn.AllowEdit = false;
            this.TENLOAI_HS.OptionsColumn.AllowFocus = false;
            this.TENLOAI_HS.OptionsColumn.FixedWidth = true;
            this.TENLOAI_HS.Visible = true;
            this.TENLOAI_HS.VisibleIndex = 9;
            this.TENLOAI_HS.Width = 200;
            // 
            // TENKHO_HS
            // 
            this.TENKHO_HS.AppearanceCell.Options.UseTextOptions = true;
            this.TENKHO_HS.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENKHO_HS.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENKHO_HS.AppearanceHeader.Options.UseFont = true;
            this.TENKHO_HS.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKHO_HS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKHO_HS.Caption = "Kho hồ sơ";
            this.TENKHO_HS.FieldName = "TENKHO_HS";
            this.TENKHO_HS.Name = "TENKHO_HS";
            this.TENKHO_HS.OptionsColumn.AllowEdit = false;
            this.TENKHO_HS.OptionsColumn.AllowFocus = false;
            this.TENKHO_HS.OptionsColumn.FixedWidth = true;
            this.TENKHO_HS.Visible = true;
            this.TENKHO_HS.VisibleIndex = 10;
            this.TENKHO_HS.Width = 200;
            // 
            // TENKE_HS
            // 
            this.TENKE_HS.AppearanceCell.Options.UseTextOptions = true;
            this.TENKE_HS.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENKE_HS.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENKE_HS.AppearanceHeader.Options.UseFont = true;
            this.TENKE_HS.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKE_HS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKE_HS.Caption = "Kệ hồ sơ";
            this.TENKE_HS.FieldName = "TENKE_HS";
            this.TENKE_HS.Name = "TENKE_HS";
            this.TENKE_HS.OptionsColumn.AllowEdit = false;
            this.TENKE_HS.OptionsColumn.AllowFocus = false;
            this.TENKE_HS.OptionsColumn.FixedWidth = true;
            this.TENKE_HS.Visible = true;
            this.TENKE_HS.VisibleIndex = 11;
            this.TENKE_HS.Width = 150;
            // 
            // TENHOP_HS
            // 
            this.TENHOP_HS.AppearanceCell.Options.UseTextOptions = true;
            this.TENHOP_HS.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHOP_HS.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENHOP_HS.AppearanceHeader.Options.UseFont = true;
            this.TENHOP_HS.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHOP_HS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHOP_HS.Caption = "Hộp hồ sơ";
            this.TENHOP_HS.FieldName = "TENHOP_HS";
            this.TENHOP_HS.Name = "TENHOP_HS";
            this.TENHOP_HS.OptionsColumn.AllowEdit = false;
            this.TENHOP_HS.OptionsColumn.AllowFocus = false;
            this.TENHOP_HS.OptionsColumn.FixedWidth = true;
            this.TENHOP_HS.Visible = true;
            this.TENHOP_HS.VisibleIndex = 12;
            this.TENHOP_HS.Width = 150;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 13;
            this.FILL.Width = 20;
            // 
            // CQNHAN
            // 
            this.CQNHAN.AppearanceCell.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CQNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CQNHAN.AppearanceHeader.Options.UseFont = true;
            this.CQNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.CQNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CQNHAN.Caption = "CQ nhận";
            this.CQNHAN.FieldName = "CQNHAN";
            this.CQNHAN.Name = "CQNHAN";
            this.CQNHAN.OptionsColumn.AllowEdit = false;
            this.CQNHAN.OptionsColumn.AllowFocus = false;
            this.CQNHAN.OptionsColumn.FixedWidth = true;
            this.CQNHAN.Width = 150;
            // 
            // NHANVIENXULY
            // 
            this.NHANVIENXULY.AppearanceCell.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NHANVIENXULY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NHANVIENXULY.AppearanceHeader.Options.UseFont = true;
            this.NHANVIENXULY.AppearanceHeader.Options.UseTextOptions = true;
            this.NHANVIENXULY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NHANVIENXULY.Caption = "NV Xử lý";
            this.NHANVIENXULY.FieldName = "NHANVIENXULY";
            this.NHANVIENXULY.Name = "NHANVIENXULY";
            this.NHANVIENXULY.OptionsColumn.AllowEdit = false;
            this.NHANVIENXULY.OptionsColumn.AllowFocus = false;
            this.NHANVIENXULY.OptionsColumn.FixedWidth = true;
            this.NHANVIENXULY.Width = 200;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1219, 175);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 476);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 312);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 152);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 232);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 394);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_HoSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_HoSo";
            this.Text = "Hồ sơ";
            this.Load += new System.EventHandler(this.Frm_HoSo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiHoSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHopHoSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCoQuanCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHetHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTieuDe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn NOIDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TENHOP_HS;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.LabelControl lblNoiDung;
        private DevExpress.XtraEditors.LabelControl lblLoaiHoSo;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONGTO;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCAP;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYHETHAN;
        private DevExpress.XtraGrid.Columns.GridColumn TENCOQUAN;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAI_HS;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn TIEUDE;
        private DevExpress.XtraEditors.LabelControl lblTieuDe;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lblNgayCap;
        private DevExpress.XtraEditors.LabelControl lblSoLuongTo;
        private DevExpress.XtraEditors.LabelControl lblHopHoSo;
        private DevExpress.XtraEditors.LabelControl lblNgayHetHan;
        private DevExpress.XtraEditors.LabelControl lblFileVanBan;
        private DevExpress.XtraEditors.SimpleButton btnFile;
        private DevExpress.XtraGrid.Columns.GridColumn COQUANCAP;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIHOSO;
        private DevExpress.XtraGrid.Columns.GridColumn HOPHOSO;
        private DevExpress.XtraGrid.Columns.GridColumn NHANVIENXULY;
        private DevExpress.XtraGrid.Columns.GridColumn CQNHAN;
        private DevExpress.XtraEditors.TextEdit txtFile;
        private DevExpress.XtraGrid.Columns.GridColumn FILES;
        private DevExpress.XtraGrid.Columns.GridColumn NAME_FILES;
        private DevExpress.XtraEditors.LabelControl lblCoQuanCap;
        private DevExpress.XtraEditors.SimpleButton btnDownLoadFile;
        private DevExpress.XtraEditors.LabelControl lblTenFile;
        private DevExpress.XtraEditors.TextEdit txtTenFile;
        private DevExpress.XtraEditors.DateEdit dtpNgayHetHan;
        private DevExpress.XtraEditors.DateEdit dtpNgayCap;
        private DevExpress.XtraEditors.TextEdit txtNoiDung;
        private DevExpress.XtraEditors.TextEdit txtTieuDe;
        private DevExpress.XtraEditors.TextEdit txtSLTo;
        private DevExpress.XtraEditors.LookUpEdit cboLoaiHoSo;
        private DevExpress.XtraEditors.LookUpEdit cboHopHoSo;
        private DevExpress.XtraEditors.LookUpEdit cboCoQuanCap;
        private DevExpress.XtraGrid.Columns.GridColumn TENKHO_HS;
        private DevExpress.XtraGrid.Columns.GridColumn TENKE_HS;




    }
}