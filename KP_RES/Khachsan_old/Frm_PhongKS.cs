﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraTab;
using DevExpress.Metro.Navigation;

namespace KP_RES 
{
    public partial class Frm_PhongKS_test : DevExpress.XtraEditors.XtraForm
    {
        public Frm_PhongKS_test()
        {
            InitializeComponent();
        }

        private void Frm_PhongKS_Load(object sender, EventArgs e)
        {
            timer1.Start();
            palphong.Left = palTop.Width / 2 - palphong.Width / 2;                                   //canh giữa panel hiển thị phòng ở trên cùng
            palbttNhomphong.Width = tabNhomphong.Width - 20;
            palbttNhomphong.Left = 10;                                                               //canh giữa panel button tab Nhóm phòng
            palBttBooking.Left = tabBooking.Width / 2 - palBttBooking.Width / 2;                     //canh giữa panel button tab Booking
            palbtt_thongtinphong.Left = tabThongtinphong.Width / 2 - palbtt_thongtinphong.Width / 2; //canh giữa panel button tab Thông tin phòng
            palbtt_Letan.Left = tabLetan.Width / 2 - palbtt_Letan.Width / 2;                         //canh giữa panel button tab Lễ tân
            palbtt_Thungan.Left = TabThungan.Width / 2 - palbtt_Thungan.Width / 2;                   //canh giữa panel button tab Thu ngân
            palbtt_Nghiepvu.Left = tabNghiepvu.Width / 2 - palbtt_Nghiepvu.Width / 2;                //canh giữa panel button tab Nghiệp vụ
            LoaddataGridView();
            Loadsophong();
        }

        private void Loadsophong()
        {
            try
            {
                string sql = "select T.MA_TT,T.TEN_TT,count(MA_PHONG) as SoPhong from PHONG P RIGHT JOIN TINHTRANG_PHONG T ON P.MA_TT=T.MA_TT group by T.MA_TT,T.TEN_TT";
                DataTable dt = clsMain.ReturnDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    txtCokhach.Text = dt.Rows[0]["SoPhong"].ToString();
                    txtTrongsach.Text = dt.Rows[1]["SoPhong"].ToString();
                    txtTrongban.Text = dt.Rows[2]["SoPhong"].ToString();
                    txtBansach.Text = dt.Rows[3]["SoPhong"].ToString();
                    txtBanban.Text = dt.Rows[4]["SoPhong"].ToString();
                    txtDangsua.Text = dt.Rows[5]["SoPhong"].ToString();
                    txtDadat.Text = dt.Rows[6]["SoPhong"].ToString();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool addpageinTabpage(string name)
        {
            foreach (XtraTabPage tb in this.Tabcontrol.TabPages)
            {
                if (tb.Name == name)
                {
                    Tabcontrol.SelectedTabPage = tb;
                    return true;
                }
            }
            return false;
        }

        private void bttTTPTTG_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_TTP_TTPTTG"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Thông tin phòng theo thời gian";
                tab.Name = "Frm_TTP_TTPTTG";
                KHACHSAN.Frm_TTP_TTPTTG frm = new KHACHSAN.Frm_TTP_TTPTTG();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttKNCUP_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_TTP_KNCUP"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Khả năng cung ứng phòng";
                tab.Name = "Frm_TTP_KNCUP";
                KHACHSAN.Frm_TTP_KNCUP frm = new KHACHSAN.Frm_TTP_KNCUP();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttTKP_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_TTP_TKP"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Thống kê phòng";
                tab.Name = "Frm_TTP_TKP";
                KHACHSAN.Frm_TTP_TKP frm = new KHACHSAN.Frm_TTP_TKP();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttSDPHT_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_TTP_SoDoPHT"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Sơ đồ phòng hiện tại";
                tab.Name = "Frm_TTP_SoDoPHT";
                KHACHSAN.Frm_TTP_SoDoPHT frm = new KHACHSAN.Frm_TTP_SoDoPHT();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttBook_DK_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Booking_Dangky"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Đăng ký";
                tab.Name = "Frm_Booking_Dangky";
                KHACHSAN.Frm_Booking_Dangky frm = new KHACHSAN.Frm_Booking_Dangky();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttNhom_PDon_Click(object sender, EventArgs e)
        {
            //this.Tabcontrol.SelectedTabPage = TabSub_NhomPhong;
        }

        private void bttBook_DS_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Booking_DS"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Danh sách Booking";
                tab.Name = "Frm_Booking_DS";
                KHACHSAN.Frm_Booking_DS frm = new KHACHSAN.Frm_Booking_DS();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttBook_LS_luutru_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Booking_LSluutru"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Lịch sử lưu trú";
                tab.Name = "Frm_Booking_LSluutru";
                KHACHSAN.Frm_Booking_LSluutru frm = new KHACHSAN.Frm_Booking_LSluutru();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void btt_Book_DaiLy_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Booking_Daily"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Đại lý";
                tab.Name = "Frm_Booking_Daily";
                KHACHSAN.Frm_Booking_Daily frm = new KHACHSAN.Frm_Booking_Daily();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bttChuyenghep_phong_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Booking_Chuyenghepphong"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Chuyển Ghép Phòng";
                tab.Name = "Frm_Booking_Chuyenghepphong";
                KHACHSAN.Frm_Booking_Chuyenghepphong frm = new KHACHSAN.Frm_Booking_Chuyenghepphong();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttLetan_DSKhachDoan_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Letan_DSKhachDoan"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Danh Sách Khách Đoàn";
                tab.Name = "Frm_Letan_DSKhachDoan";
                KHACHSAN.Frm_Letan_DSKhachDoan frm = new KHACHSAN.Frm_Letan_DSKhachDoan();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttLetan_QLTamTru_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Letan_QLTamTru"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Quản Lý Tạm Trú";
                tab.Name = "Frm_Letan_QLTamTru";
                KHACHSAN.Frm_Letan_QLTamTru frm = new KHACHSAN.Frm_Letan_QLTamTru();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string hour, minute, second,day;
            day = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            LabNgay.Text = day;
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
        }

        private void bttLetan_Nguoidikem_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Letan_Nguoidikem"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Người đi kèm";
                tab.Name = "Frm_Letan_Nguoidikem";
                KHACHSAN.Frm_Letan_Nguoidikem frm = new KHACHSAN.Frm_Letan_Nguoidikem();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttLetan_Vesinhphong_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Letan_Vesinhphong"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Vệ sinh phòng";
                tab.Name = "Frm_Letan_Vesinhphong";
                KHACHSAN.Frm_Letan_Vesinhphong frm = new KHACHSAN.Frm_Letan_Vesinhphong();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttThungan_Nhahang_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Thungan_Dichvunhahang"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Dịch vụ nhà hàng";
                tab.Name = "Frm_Thungan_Dichvunhahang";
                KHACHSAN.Frm_Thungan_Dichvunhahang frm = new KHACHSAN.Frm_Thungan_Dichvunhahang();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                //this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttThungan_Spa_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Thungan_DichvuSpa"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Dịch vụ Spa";
                tab.Name = "Frm_Thungan_DichvuSpa";
                KHACHSAN.Frm_Thungan_DichvuSpa frm = new KHACHSAN.Frm_Thungan_DichvuSpa();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttThungan_Hoboi_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Thungan_DichvuHoBoi"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Dịch vụ Hồ Bơi";
                tab.Name = "Frm_Thungan_DichvuHoBoi";
                KHACHSAN.Frm_Thungan_DichvuHoBoi frm = new KHACHSAN.Frm_Thungan_DichvuHoBoi();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttThungan_Chothue_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Thungan_DichvuChoThue"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Dịch vụ Cho Thuê";
                tab.Name = "Frm_Thungan_DichvuChoThue";
                KHACHSAN.Frm_Thungan_DichvuChoThue frm = new KHACHSAN.Frm_Thungan_DichvuChoThue();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        private void bttNghiepvu_BCDoanhthu_Click(object sender, EventArgs e)
        {
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Nghiepvu_Baocaodoanhthu"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Báo cáo Doanh thu";
                tab.Name = "Frm_Nghiepvu_Baocaodoanhthu";
                KHACHSAN.Frm_Nghiepvu_Baocaodoanhthu frm = new KHACHSAN.Frm_Nghiepvu_Baocaodoanhthu();
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }

        DataTable dtKhuvuc = new DataTable();
        private void LoaddataGridView()
        {
            //SetRongControl();
            //KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select MA_NHOM As MA,TEN_NHOM As TEN,HINHANH" + "\n";
            sSQL += "From NHOMPHONG" + "\n";
            sSQL += "Order by SUDUNG DESC,TEN_NHOM" + "\n";
            dtKhuvuc = clsMain.ReturnDataTable(sSQL);
            palNhom.Controls.Clear();
            foreach (DataRow dr in dtKhuvuc.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 108;
                btn.Height = 60;
                btn.Font = palNhom.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Click += new EventHandler(btKhuvuc_Click);
                palNhom.Controls.Add(btn);
            }
            if (palNhom.VerticalScroll.Visible)
            {
                palbttNhomphong.Top = 5;
                palbttNhomphong.Height = 83;
            }
            else
            {
                palbttNhomphong.Top = 10;
                palbttNhomphong.Height = 67;
            }
        }

        private void bttTrai_Click(object sender, EventArgs e)
        {
            try
            {
                palNhom.AutoScrollPosition = new Point(palNhom.HorizontalScroll.Value - palNhom.HorizontalScroll.Maximum / 15, 0);
            }
            catch
            {
            }
        }

        private void bnt_Phai_Click(object sender, EventArgs e)
        {
            try
            {
                palNhom.AutoScrollPosition = new Point(palNhom.HorizontalScroll.Value + palNhom.HorizontalScroll.Maximum / 15, 0);
            }
            catch
            {
            }
        }
        public string smaNhom = string.Empty;
        private void btKhuvuc_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in palNhom.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = Color.Empty;
                }
            }

            smaNhom = btn.Tag.ToString();
            Tabcontrol.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            if (!addpageinTabpage("Frm_Nhomphong"))
            {
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Nhóm phòng";
                tab.Name = "Frm_Nhomphong";
                KHACHSAN.Frm_Nhomphong frm = new KHACHSAN.Frm_Nhomphong(smaNhom);
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;

                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
            else
            {
                for (int i = Tabcontrol.TabPages.Count - 1; i >= 0; i--)
                {
                    if (Tabcontrol.TabPages[i].Text == "Frm_Nhomphong")
                        Tabcontrol.TabPages[i].Dispose();
                }
                XtraTabPage tab = new XtraTabPage();
                tab.Text = "Nhóm phòng";
                tab.Name = "Frm_Nhomphong";
                KHACHSAN.Frm_Nhomphong frm = new KHACHSAN.Frm_Nhomphong(smaNhom);
                frm.Dock = DockStyle.Fill;
                frm.TopLevel = false;
                tab.Controls.Add(frm);
                frm.Show();
                this.Tabcontrol.TabPages.Add(tab);
                this.Tabcontrol.SelectedTabPage = tab;
            }
        }
    }
}