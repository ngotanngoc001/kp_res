﻿namespace KP_RES 
{
    partial class Frm_PhongKS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_PhongKS));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            this.palTop = new DevExpress.XtraEditors.PanelControl();
            this.palphong = new DevExpress.XtraEditors.PanelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.XtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.tabNhomphong = new DevExpress.XtraTab.XtraTabPage();
            this.palbttNhomphong = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.bttNhom_PDon = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.tabThongtinphong = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.cbbNhom = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbKhuvuc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bttTTPTTG = new DevExpress.XtraEditors.SimpleButton();
            this.bttTKP = new DevExpress.XtraEditors.SimpleButton();
            this.bttKNCUP = new DevExpress.XtraEditors.SimpleButton();
            this.bttSDPHT = new DevExpress.XtraEditors.SimpleButton();
            this.tabBooking = new DevExpress.XtraTab.XtraTabPage();
            this.palBttBooking = new DevExpress.XtraEditors.PanelControl();
            this.btt_Book_DaiLy = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_DS = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_LS_luutru = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_DK = new DevExpress.XtraEditors.SimpleButton();
            this.tabLetan = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton30 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton31 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton33 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton34 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton28 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton29 = new DevExpress.XtraEditors.SimpleButton();
            this.TabThungan = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_Thungan = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton40 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton32 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton35 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton36 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton37 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton38 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton39 = new DevExpress.XtraEditors.SimpleButton();
            this.tabNghiepvu = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_Nghiepvu = new DevExpress.XtraEditors.PanelControl();
            this.bttThoat = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton42 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton43 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton44 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton45 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton47 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_luuban = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.Tabcontrol = new DevExpress.XtraTab.XtraTabControl();
            this.Tabsub_nhomphong = new DevExpress.XtraTab.XtraTabPage();
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.richTextBox11 = new System.Windows.Forms.RichTextBox();
            this.richTextBox12 = new System.Windows.Forms.RichTextBox();
            this.richTextBox13 = new System.Windows.Forms.RichTextBox();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabsub_TTP_theotg = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEMP1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEMP2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEMP3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEMP4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEMP5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.Tabsub_TTP_khanangcungungphong = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TONGSOPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton46 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton48 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton49 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton50 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton51 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton52 = new DevExpress.XtraEditors.SimpleButton();
            this.Tabsub_TTP_thongkephong = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit50 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit52 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit51 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit54 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit55 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit49 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.Tabsub_TTP_sodophonghientai = new DevExpress.XtraTab.XtraTabPage();
            this.TabsubBooking_Dangky = new DevExpress.XtraTab.XtraTabPage();
            this.palBookDangky = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit77 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit78 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit79 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit76 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit73 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit75 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit53 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit59 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit66 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit67 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit72 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIPHONG1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONG1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYVISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUOCTICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit68 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit71 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit65 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit69 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit70 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit74 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.dtp_ngayden = new DevExpress.XtraEditors.DateEdit();
            this.dtp_ngaydi = new DevExpress.XtraEditors.DateEdit();
            this.textEdit64 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit63 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit62 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit61 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit60 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit58 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit57 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit56 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.Tabsub_Booking_DSBooking = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl26 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.Tabsub_Booking_LSLuutru = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton53 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton54 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton55 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton56 = new DevExpress.XtraEditors.SimpleButton();
            this.palBook_LSluutru_Top = new DevExpress.XtraEditors.PanelControl();
            this.pal_Book_LSluutru_Search = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton57 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit82 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit80 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit81 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.Tabsub_Booking_Daily = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl32 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit96 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit93 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit94 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit95 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit87 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit91 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit92 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit84 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit85 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit83 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit86 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit88 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit89 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit90 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.palTop)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palphong)).BeginInit();
            this.palphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl2)).BeginInit();
            this.XtraTabControl2.SuspendLayout();
            this.tabNhomphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbttNhomphong)).BeginInit();
            this.palbttNhomphong.SuspendLayout();
            this.tabThongtinphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbKhuvuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.tabBooking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palBttBooking)).BeginInit();
            this.palBttBooking.SuspendLayout();
            this.tabLetan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            this.TabThungan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Thungan)).BeginInit();
            this.palbtt_Thungan.SuspendLayout();
            this.tabNghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Nghiepvu)).BeginInit();
            this.palbtt_Nghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tabcontrol)).BeginInit();
            this.Tabcontrol.SuspendLayout();
            this.Tabsub_nhomphong.SuspendLayout();
            this.tabsub_TTP_theotg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.Tabsub_TTP_khanangcungungphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.Tabsub_TTP_thongkephong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            this.TabsubBooking_Dangky.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palBookDangky)).BeginInit();
            this.palBookDangky.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).BeginInit();
            this.Tabsub_Booking_DSBooking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).BeginInit();
            this.panelControl26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.Tabsub_Booking_LSLuutru.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palBook_LSluutru_Top)).BeginInit();
            this.palBook_LSluutru_Top.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_Book_LSluutru_Search)).BeginInit();
            this.pal_Book_LSluutru_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit81.Properties)).BeginInit();
            this.Tabsub_Booking_Daily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).BeginInit();
            this.panelControl32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit90.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.palphong);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(1020, 48);
            this.palTop.TabIndex = 0;
            // 
            // palphong
            // 
            this.palphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palphong.Controls.Add(this.textEdit6);
            this.palphong.Controls.Add(this.textEdit5);
            this.palphong.Controls.Add(this.textEdit4);
            this.palphong.Controls.Add(this.textEdit3);
            this.palphong.Controls.Add(this.textEdit2);
            this.palphong.Controls.Add(this.textEdit1);
            this.palphong.Controls.Add(this.txtTEN);
            this.palphong.Controls.Add(this.pictureEdit7);
            this.palphong.Controls.Add(this.pictureEdit1);
            this.palphong.Controls.Add(this.pictureEdit6);
            this.palphong.Controls.Add(this.pictureEdit2);
            this.palphong.Controls.Add(this.pictureEdit5);
            this.palphong.Controls.Add(this.pictureEdit3);
            this.palphong.Controls.Add(this.pictureEdit4);
            this.palphong.Location = new System.Drawing.Point(7, 3);
            this.palphong.Name = "palphong";
            this.palphong.Size = new System.Drawing.Size(1006, 41);
            this.palphong.TabIndex = 0;
            // 
            // textEdit6
            // 
            this.textEdit6.EditValue = "20";
            this.textEdit6.Enabled = false;
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(964, 8);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit6.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit6.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit6.Size = new System.Drawing.Size(27, 26);
            this.textEdit6.TabIndex = 18;
            // 
            // textEdit5
            // 
            this.textEdit5.EditValue = "23";
            this.textEdit5.Enabled = false;
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(820, 8);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit5.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit5.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit5.Size = new System.Drawing.Size(27, 26);
            this.textEdit5.TabIndex = 17;
            // 
            // textEdit4
            // 
            this.textEdit4.EditValue = "15";
            this.textEdit4.Enabled = false;
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(676, 8);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit4.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit4.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit4.Size = new System.Drawing.Size(27, 26);
            this.textEdit4.TabIndex = 16;
            // 
            // textEdit3
            // 
            this.textEdit3.EditValue = "7";
            this.textEdit3.Enabled = false;
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(532, 8);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit3.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit3.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit3.Size = new System.Drawing.Size(27, 26);
            this.textEdit3.TabIndex = 15;
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "30";
            this.textEdit2.Enabled = false;
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(244, 8);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit2.Size = new System.Drawing.Size(27, 26);
            this.textEdit2.TabIndex = 14;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "0";
            this.textEdit1.Enabled = false;
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(388, 8);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Size = new System.Drawing.Size(27, 26);
            this.textEdit1.TabIndex = 13;
            // 
            // txtTEN
            // 
            this.txtTEN.EditValue = "10";
            this.txtTEN.Enabled = false;
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(99, 8);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtTEN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTEN.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtTEN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTEN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTEN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTEN.Size = new System.Drawing.Size(27, 26);
            this.txtTEN.TabIndex = 4;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Enabled = false;
            this.pictureEdit7.Location = new System.Drawing.Point(869, 3);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(123)))), ((int)(((byte)(79)))));
            this.pictureEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.NullText = "P.Đã Sạch";
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit7.TabIndex = 12;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Enabled = false;
            this.pictureEdit1.Location = new System.Drawing.Point(5, 3);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.pictureEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.NullText = "P.Có Khách";
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit1.TabIndex = 0;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Enabled = false;
            this.pictureEdit6.Location = new System.Drawing.Point(725, 3);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(64)))), ((int)(((byte)(54)))));
            this.pictureEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit6.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit6.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.NullText = "P.Đang Sửa";
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit6.TabIndex = 11;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Enabled = false;
            this.pictureEdit2.Location = new System.Drawing.Point(149, 3);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(239)))));
            this.pictureEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit2.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.NullText = "P.Trống Sạch";
            this.pictureEdit2.Properties.ReadOnly = true;
            this.pictureEdit2.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit2.TabIndex = 7;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Enabled = false;
            this.pictureEdit5.Location = new System.Drawing.Point(581, 3);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(35)))));
            this.pictureEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.NullText = "P.Bận Bẩn";
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit5.TabIndex = 10;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Enabled = false;
            this.pictureEdit3.Location = new System.Drawing.Point(293, 3);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(165)))), ((int)(((byte)(80)))));
            this.pictureEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit3.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.NullText = "P.Trống Bẩn";
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit3.TabIndex = 8;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Enabled = false;
            this.pictureEdit4.Location = new System.Drawing.Point(437, 3);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(147)))), ((int)(((byte)(29)))));
            this.pictureEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit4.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.NullText = "P.Bận Sạch";
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit4.TabIndex = 9;
            // 
            // XtraTabControl2
            // 
            this.XtraTabControl2.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XtraTabControl2.AppearancePage.Header.Options.UseFont = true;
            this.XtraTabControl2.AppearancePage.Header.Options.UseTextOptions = true;
            this.XtraTabControl2.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XtraTabControl2.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.XtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XtraTabControl2.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.XtraTabControl2.Location = new System.Drawing.Point(2, 2);
            this.XtraTabControl2.Name = "XtraTabControl2";
            this.XtraTabControl2.SelectedTabPage = this.tabNhomphong;
            this.XtraTabControl2.Size = new System.Drawing.Size(836, 134);
            this.XtraTabControl2.TabIndex = 1;
            this.XtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabNhomphong,
            this.tabThongtinphong,
            this.tabBooking,
            this.tabLetan,
            this.TabThungan,
            this.tabNghiepvu});
            // 
            // tabNhomphong
            // 
            this.tabNhomphong.Controls.Add(this.palbttNhomphong);
            this.tabNhomphong.Name = "tabNhomphong";
            this.tabNhomphong.Size = new System.Drawing.Size(830, 100);
            this.tabNhomphong.Text = "Nhóm Phòng";
            // 
            // palbttNhomphong
            // 
            this.palbttNhomphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbttNhomphong.Controls.Add(this.simpleButton10);
            this.palbttNhomphong.Controls.Add(this.simpleButton9);
            this.palbttNhomphong.Controls.Add(this.simpleButton8);
            this.palbttNhomphong.Controls.Add(this.simpleButton7);
            this.palbttNhomphong.Controls.Add(this.simpleButton6);
            this.palbttNhomphong.Controls.Add(this.bttNhom_PDon);
            this.palbttNhomphong.Controls.Add(this.simpleButton4);
            this.palbttNhomphong.Controls.Add(this.bnt_Len);
            this.palbttNhomphong.Location = new System.Drawing.Point(6, 21);
            this.palbttNhomphong.Name = "palbttNhomphong";
            this.palbttNhomphong.Size = new System.Drawing.Size(822, 58);
            this.palbttNhomphong.TabIndex = 13;
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseBackColor = true;
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton10.Location = new System.Drawing.Point(677, 0);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(108, 58);
            this.simpleButton10.TabIndex = 34;
            this.simpleButton10.Text = "Tầng 2";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseBackColor = true;
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton9.Location = new System.Drawing.Point(549, 0);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(108, 58);
            this.simpleButton9.TabIndex = 33;
            this.simpleButton9.Text = "Tầng 1";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton8.Location = new System.Drawing.Point(421, 0);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(108, 58);
            this.simpleButton8.TabIndex = 32;
            this.simpleButton8.Text = "Phòng Delux";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton7.Location = new System.Drawing.Point(293, 0);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(108, 58);
            this.simpleButton7.TabIndex = 31;
            this.simpleButton7.Text = "Phòng VIP";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton6.Location = new System.Drawing.Point(165, 0);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(108, 58);
            this.simpleButton6.TabIndex = 30;
            this.simpleButton6.Text = "Phòng Đôi";
            // 
            // bttNhom_PDon
            // 
            this.bttNhom_PDon.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttNhom_PDon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttNhom_PDon.Appearance.Options.UseBackColor = true;
            this.bttNhom_PDon.Appearance.Options.UseFont = true;
            this.bttNhom_PDon.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttNhom_PDon.Location = new System.Drawing.Point(37, 0);
            this.bttNhom_PDon.Margin = new System.Windows.Forms.Padding(4);
            this.bttNhom_PDon.Name = "bttNhom_PDon";
            this.bttNhom_PDon.Size = new System.Drawing.Size(108, 58);
            this.bttNhom_PDon.TabIndex = 29;
            this.bttNhom_PDon.Text = "Phòng Đơn";
            this.bttNhom_PDon.Click += new System.EventHandler(this.bttNhom_PDon_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Dock = System.Windows.Forms.DockStyle.Left;
            this.simpleButton4.Image = global::KP_RES.Properties.Resources.up44_26;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(0, 0);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(29, 58);
            this.simpleButton4.TabIndex = 28;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up33_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(793, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(29, 58);
            this.bnt_Len.TabIndex = 27;
            // 
            // tabThongtinphong
            // 
            this.tabThongtinphong.Controls.Add(this.panelControl7);
            this.tabThongtinphong.Controls.Add(this.panelControl6);
            this.tabThongtinphong.Name = "tabThongtinphong";
            this.tabThongtinphong.Size = new System.Drawing.Size(830, 100);
            this.tabThongtinphong.Text = "Thông Tin Phòng";
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.dtpTungay);
            this.panelControl7.Controls.Add(this.dtpDenngay);
            this.panelControl7.Controls.Add(this.cbbNhom);
            this.panelControl7.Controls.Add(this.cbbKhuvuc);
            this.panelControl7.Controls.Add(this.simpleButton13);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl7.Location = new System.Drawing.Point(476, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(354, 100);
            this.panelControl7.TabIndex = 35;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(139, 13);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(141, 26);
            this.dtpTungay.TabIndex = 38;
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(139, 60);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(141, 26);
            this.dtpDenngay.TabIndex = 39;
            // 
            // cbbNhom
            // 
            this.cbbNhom.EnterMoveNextControl = true;
            this.cbbNhom.Location = new System.Drawing.Point(5, 60);
            this.cbbNhom.Name = "cbbNhom";
            this.cbbNhom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cbbNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNhom.Properties.Appearance.Options.UseFont = true;
            this.cbbNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbbNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbbNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNhom.Properties.DropDownItemHeight = 40;
            this.cbbNhom.Properties.Items.AddRange(new object[] {
            "Phòng thường",
            "Phòng Vip",
            "Phòng đơn",
            "Phòng đôi"});
            this.cbbNhom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbNhom.Size = new System.Drawing.Size(128, 26);
            this.cbbNhom.TabIndex = 37;
            // 
            // cbbKhuvuc
            // 
            this.cbbKhuvuc.EnterMoveNextControl = true;
            this.cbbKhuvuc.Location = new System.Drawing.Point(5, 13);
            this.cbbKhuvuc.Name = "cbbKhuvuc";
            this.cbbKhuvuc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cbbKhuvuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbKhuvuc.Properties.Appearance.Options.UseFont = true;
            this.cbbKhuvuc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbbKhuvuc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbbKhuvuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbKhuvuc.Properties.DropDownItemHeight = 40;
            this.cbbKhuvuc.Properties.Items.AddRange(new object[] {
            "Tầng trệt",
            "Tầng 1",
            "Tầng 2",
            "Tầng 3"});
            this.cbbKhuvuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbKhuvuc.Size = new System.Drawing.Size(128, 26);
            this.cbbKhuvuc.TabIndex = 36;
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseBackColor = true;
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.Image = global::KP_RES.Properties.Resources.search_26;
            this.simpleButton13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton13.Location = new System.Drawing.Point(287, 12);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(61, 74);
            this.simpleButton13.TabIndex = 35;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bttTTPTTG);
            this.panelControl6.Controls.Add(this.bttTKP);
            this.panelControl6.Controls.Add(this.bttKNCUP);
            this.panelControl6.Controls.Add(this.bttSDPHT);
            this.panelControl6.Location = new System.Drawing.Point(5, 12);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(467, 76);
            this.panelControl6.TabIndex = 14;
            // 
            // bttTTPTTG
            // 
            this.bttTTPTTG.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttTTPTTG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttTTPTTG.Appearance.Options.UseBackColor = true;
            this.bttTTPTTG.Appearance.Options.UseFont = true;
            this.bttTTPTTG.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttTTPTTG.Location = new System.Drawing.Point(111, 9);
            this.bttTTPTTG.Margin = new System.Windows.Forms.Padding(4);
            this.bttTTPTTG.Name = "bttTTPTTG";
            this.bttTTPTTG.Size = new System.Drawing.Size(132, 58);
            this.bttTTPTTG.TabIndex = 31;
            this.bttTTPTTG.Text = "Thông Tin Phòng\r\nTheo Thời Gian";
            this.bttTTPTTG.Click += new System.EventHandler(this.bttTTPTTG_Click);
            // 
            // bttTKP
            // 
            this.bttTKP.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttTKP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttTKP.Appearance.Options.UseBackColor = true;
            this.bttTKP.Appearance.Options.UseFont = true;
            this.bttTKP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttTKP.Location = new System.Drawing.Point(379, 9);
            this.bttTKP.Margin = new System.Windows.Forms.Padding(4);
            this.bttTKP.Name = "bttTKP";
            this.bttTKP.Size = new System.Drawing.Size(86, 58);
            this.bttTKP.TabIndex = 34;
            this.bttTKP.Text = "Thống Kê\r\nPhòng";
            this.bttTKP.Click += new System.EventHandler(this.bttTKP_Click);
            // 
            // bttKNCUP
            // 
            this.bttKNCUP.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttKNCUP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttKNCUP.Appearance.Options.UseBackColor = true;
            this.bttKNCUP.Appearance.Options.UseFont = true;
            this.bttKNCUP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttKNCUP.Location = new System.Drawing.Point(252, 9);
            this.bttKNCUP.Margin = new System.Windows.Forms.Padding(4);
            this.bttKNCUP.Name = "bttKNCUP";
            this.bttKNCUP.Size = new System.Drawing.Size(118, 58);
            this.bttKNCUP.TabIndex = 33;
            this.bttKNCUP.Text = "Khả Năng Cung\r\nỨng Phòng";
            this.bttKNCUP.Click += new System.EventHandler(this.bttKNCUP_Click);
            // 
            // bttSDPHT
            // 
            this.bttSDPHT.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttSDPHT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttSDPHT.Appearance.Options.UseBackColor = true;
            this.bttSDPHT.Appearance.Options.UseFont = true;
            this.bttSDPHT.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttSDPHT.Location = new System.Drawing.Point(2, 9);
            this.bttSDPHT.Margin = new System.Windows.Forms.Padding(4);
            this.bttSDPHT.Name = "bttSDPHT";
            this.bttSDPHT.Size = new System.Drawing.Size(100, 58);
            this.bttSDPHT.TabIndex = 29;
            this.bttSDPHT.Text = "Sơ Đồ Phòng\r\nHiện Tại";
            this.bttSDPHT.Click += new System.EventHandler(this.bttSDPHT_Click);
            // 
            // tabBooking
            // 
            this.tabBooking.Controls.Add(this.palBttBooking);
            this.tabBooking.Name = "tabBooking";
            this.tabBooking.Size = new System.Drawing.Size(830, 100);
            this.tabBooking.Text = "Booking";
            // 
            // palBttBooking
            // 
            this.palBttBooking.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palBttBooking.Controls.Add(this.btt_Book_DaiLy);
            this.palBttBooking.Controls.Add(this.simpleButton15);
            this.palBttBooking.Controls.Add(this.bttBook_DS);
            this.palBttBooking.Controls.Add(this.bttBook_LS_luutru);
            this.palBttBooking.Controls.Add(this.simpleButton19);
            this.palBttBooking.Controls.Add(this.simpleButton20);
            this.palBttBooking.Controls.Add(this.bttBook_DK);
            this.palBttBooking.Location = new System.Drawing.Point(4, 22);
            this.palBttBooking.Name = "palBttBooking";
            this.palBttBooking.Size = new System.Drawing.Size(822, 58);
            this.palBttBooking.TabIndex = 14;
            // 
            // btt_Book_DaiLy
            // 
            this.btt_Book_DaiLy.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btt_Book_DaiLy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btt_Book_DaiLy.Appearance.Options.UseBackColor = true;
            this.btt_Book_DaiLy.Appearance.Options.UseFont = true;
            this.btt_Book_DaiLy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btt_Book_DaiLy.Location = new System.Drawing.Point(709, 0);
            this.btt_Book_DaiLy.Margin = new System.Windows.Forms.Padding(4);
            this.btt_Book_DaiLy.Name = "btt_Book_DaiLy";
            this.btt_Book_DaiLy.Size = new System.Drawing.Size(108, 58);
            this.btt_Book_DaiLy.TabIndex = 35;
            this.btt_Book_DaiLy.Text = "Đại Lý";
            this.btt_Book_DaiLy.Click += new System.EventHandler(this.btt_Book_DaiLy_Click);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton15.Appearance.Options.UseBackColor = true;
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton15.Location = new System.Drawing.Point(584, 0);
            this.simpleButton15.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(108, 58);
            this.simpleButton15.TabIndex = 34;
            this.simpleButton15.Text = "Xem\r\nChi Tiết";
            // 
            // bttBook_DS
            // 
            this.bttBook_DS.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_DS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_DS.Appearance.Options.UseBackColor = true;
            this.bttBook_DS.Appearance.Options.UseFont = true;
            this.bttBook_DS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_DS.Location = new System.Drawing.Point(465, 0);
            this.bttBook_DS.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_DS.Name = "bttBook_DS";
            this.bttBook_DS.Size = new System.Drawing.Size(102, 58);
            this.bttBook_DS.TabIndex = 33;
            this.bttBook_DS.Text = "Danh Sách\r\nBooking";
            this.bttBook_DS.Click += new System.EventHandler(this.bttBook_DS_Click);
            // 
            // bttBook_LS_luutru
            // 
            this.bttBook_LS_luutru.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_LS_luutru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_LS_luutru.Appearance.Options.UseBackColor = true;
            this.bttBook_LS_luutru.Appearance.Options.UseFont = true;
            this.bttBook_LS_luutru.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_LS_luutru.Location = new System.Drawing.Point(343, 0);
            this.bttBook_LS_luutru.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_LS_luutru.Name = "bttBook_LS_luutru";
            this.bttBook_LS_luutru.Size = new System.Drawing.Size(105, 58);
            this.bttBook_LS_luutru.TabIndex = 32;
            this.bttBook_LS_luutru.Text = "Lịch Sử\r\nLưu Trú";
            this.bttBook_LS_luutru.Click += new System.EventHandler(this.bttBook_LS_luutru_Click);
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton19.Appearance.Options.UseBackColor = true;
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton19.Location = new System.Drawing.Point(227, 0);
            this.simpleButton19.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(99, 58);
            this.simpleButton19.TabIndex = 31;
            this.simpleButton19.Text = "Sửa\r\nĐăng Ký";
            // 
            // simpleButton20
            // 
            this.simpleButton20.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton20.Appearance.Options.UseBackColor = true;
            this.simpleButton20.Appearance.Options.UseFont = true;
            this.simpleButton20.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton20.Location = new System.Drawing.Point(111, 0);
            this.simpleButton20.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(99, 58);
            this.simpleButton20.TabIndex = 30;
            this.simpleButton20.Text = "Hủy\r\nĐăng Ký";
            // 
            // bttBook_DK
            // 
            this.bttBook_DK.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_DK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_DK.Appearance.Options.UseBackColor = true;
            this.bttBook_DK.Appearance.Options.UseFont = true;
            this.bttBook_DK.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_DK.Location = new System.Drawing.Point(2, 0);
            this.bttBook_DK.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_DK.Name = "bttBook_DK";
            this.bttBook_DK.Size = new System.Drawing.Size(92, 58);
            this.bttBook_DK.TabIndex = 29;
            this.bttBook_DK.Text = "Đăng Ký";
            this.bttBook_DK.Click += new System.EventHandler(this.bttBook_DK_Click);
            // 
            // tabLetan
            // 
            this.tabLetan.Controls.Add(this.panelControl9);
            this.tabLetan.Controls.Add(this.panelControl8);
            this.tabLetan.Name = "tabLetan";
            this.tabLetan.Size = new System.Drawing.Size(830, 100);
            this.tabLetan.Text = "Lễ Tân";
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.simpleButton30);
            this.panelControl9.Controls.Add(this.simpleButton31);
            this.panelControl9.Controls.Add(this.simpleButton33);
            this.panelControl9.Controls.Add(this.simpleButton34);
            this.panelControl9.Location = new System.Drawing.Point(3, 1);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(190, 97);
            this.panelControl9.TabIndex = 36;
            // 
            // simpleButton30
            // 
            this.simpleButton30.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton30.Appearance.Options.UseBackColor = true;
            this.simpleButton30.Appearance.Options.UseFont = true;
            this.simpleButton30.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton30.Location = new System.Drawing.Point(99, 48);
            this.simpleButton30.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton30.Name = "simpleButton30";
            this.simpleButton30.Size = new System.Drawing.Size(87, 45);
            this.simpleButton30.TabIndex = 34;
            this.simpleButton30.Text = "Hủy\r\nCheck Out";
            // 
            // simpleButton31
            // 
            this.simpleButton31.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton31.Appearance.Options.UseBackColor = true;
            this.simpleButton31.Appearance.Options.UseFont = true;
            this.simpleButton31.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton31.Location = new System.Drawing.Point(4, 49);
            this.simpleButton31.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton31.Name = "simpleButton31";
            this.simpleButton31.Size = new System.Drawing.Size(87, 45);
            this.simpleButton31.TabIndex = 33;
            this.simpleButton31.Text = "Hủy\r\nCheck In";
            // 
            // simpleButton33
            // 
            this.simpleButton33.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton33.Appearance.Options.UseBackColor = true;
            this.simpleButton33.Appearance.Options.UseFont = true;
            this.simpleButton33.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton33.Location = new System.Drawing.Point(99, 2);
            this.simpleButton33.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton33.Name = "simpleButton33";
            this.simpleButton33.Size = new System.Drawing.Size(87, 41);
            this.simpleButton33.TabIndex = 30;
            this.simpleButton33.Text = "Check Out";
            // 
            // simpleButton34
            // 
            this.simpleButton34.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton34.Appearance.Options.UseBackColor = true;
            this.simpleButton34.Appearance.Options.UseFont = true;
            this.simpleButton34.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton34.Location = new System.Drawing.Point(4, 2);
            this.simpleButton34.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton34.Name = "simpleButton34";
            this.simpleButton34.Size = new System.Drawing.Size(87, 41);
            this.simpleButton34.TabIndex = 29;
            this.simpleButton34.Text = "Check In";
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.simpleButton23);
            this.panelControl8.Controls.Add(this.simpleButton24);
            this.panelControl8.Controls.Add(this.simpleButton25);
            this.panelControl8.Controls.Add(this.simpleButton26);
            this.panelControl8.Controls.Add(this.simpleButton28);
            this.panelControl8.Controls.Add(this.simpleButton29);
            this.panelControl8.Location = new System.Drawing.Point(199, 9);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(619, 77);
            this.panelControl8.TabIndex = 15;
            // 
            // simpleButton23
            // 
            this.simpleButton23.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton23.Appearance.Options.UseBackColor = true;
            this.simpleButton23.Appearance.Options.UseFont = true;
            this.simpleButton23.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton23.Location = new System.Drawing.Point(520, 10);
            this.simpleButton23.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(91, 58);
            this.simpleButton23.TabIndex = 35;
            this.simpleButton23.Text = "Vệ Sinh\r\nPhòng";
            // 
            // simpleButton24
            // 
            this.simpleButton24.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton24.Appearance.Options.UseBackColor = true;
            this.simpleButton24.Appearance.Options.UseFont = true;
            this.simpleButton24.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton24.Location = new System.Drawing.Point(416, 10);
            this.simpleButton24.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(96, 58);
            this.simpleButton24.TabIndex = 34;
            this.simpleButton24.Text = "Người\r\nĐi Kèm";
            // 
            // simpleButton25
            // 
            this.simpleButton25.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton25.Appearance.Options.UseBackColor = true;
            this.simpleButton25.Appearance.Options.UseFont = true;
            this.simpleButton25.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton25.Location = new System.Drawing.Point(320, 10);
            this.simpleButton25.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(88, 58);
            this.simpleButton25.TabIndex = 33;
            this.simpleButton25.Text = "Quản Lý\r\nTạm Trú";
            // 
            // simpleButton26
            // 
            this.simpleButton26.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton26.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton26.Appearance.Options.UseBackColor = true;
            this.simpleButton26.Appearance.Options.UseFont = true;
            this.simpleButton26.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton26.Location = new System.Drawing.Point(213, 10);
            this.simpleButton26.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(99, 58);
            this.simpleButton26.TabIndex = 32;
            this.simpleButton26.Text = "DS Khách\r\nĐoàn";
            // 
            // simpleButton28
            // 
            this.simpleButton28.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton28.Appearance.Options.UseBackColor = true;
            this.simpleButton28.Appearance.Options.UseFont = true;
            this.simpleButton28.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton28.Location = new System.Drawing.Point(99, 10);
            this.simpleButton28.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton28.Name = "simpleButton28";
            this.simpleButton28.Size = new System.Drawing.Size(108, 58);
            this.simpleButton28.TabIndex = 30;
            this.simpleButton28.Text = "Chuyển/Ghép\r\nPhòng";
            // 
            // simpleButton29
            // 
            this.simpleButton29.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton29.Appearance.Options.UseBackColor = true;
            this.simpleButton29.Appearance.Options.UseFont = true;
            this.simpleButton29.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton29.Location = new System.Drawing.Point(5, 10);
            this.simpleButton29.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton29.Name = "simpleButton29";
            this.simpleButton29.Size = new System.Drawing.Size(85, 58);
            this.simpleButton29.TabIndex = 29;
            this.simpleButton29.Text = "Walk In";
            // 
            // TabThungan
            // 
            this.TabThungan.Controls.Add(this.palbtt_Thungan);
            this.TabThungan.Name = "TabThungan";
            this.TabThungan.Size = new System.Drawing.Size(830, 100);
            this.TabThungan.Text = "Thu Ngân";
            // 
            // palbtt_Thungan
            // 
            this.palbtt_Thungan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_Thungan.Controls.Add(this.simpleButton40);
            this.palbtt_Thungan.Controls.Add(this.simpleButton27);
            this.palbtt_Thungan.Controls.Add(this.simpleButton32);
            this.palbtt_Thungan.Controls.Add(this.simpleButton35);
            this.palbtt_Thungan.Controls.Add(this.simpleButton36);
            this.palbtt_Thungan.Controls.Add(this.simpleButton37);
            this.palbtt_Thungan.Controls.Add(this.simpleButton38);
            this.palbtt_Thungan.Controls.Add(this.simpleButton39);
            this.palbtt_Thungan.Location = new System.Drawing.Point(5, 11);
            this.palbtt_Thungan.Name = "palbtt_Thungan";
            this.palbtt_Thungan.Size = new System.Drawing.Size(822, 71);
            this.palbtt_Thungan.TabIndex = 15;
            // 
            // simpleButton40
            // 
            this.simpleButton40.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton40.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton40.Appearance.Options.UseBackColor = true;
            this.simpleButton40.Appearance.Options.UseFont = true;
            this.simpleButton40.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton40.Location = new System.Drawing.Point(730, 6);
            this.simpleButton40.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton40.Name = "simpleButton40";
            this.simpleButton40.Size = new System.Drawing.Size(83, 58);
            this.simpleButton40.TabIndex = 36;
            this.simpleButton40.Text = "Hủy\r\nDịch Vụ";
            // 
            // simpleButton27
            // 
            this.simpleButton27.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton27.Appearance.Options.UseBackColor = true;
            this.simpleButton27.Appearance.Options.UseFont = true;
            this.simpleButton27.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton27.Location = new System.Drawing.Point(621, 6);
            this.simpleButton27.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(97, 58);
            this.simpleButton27.TabIndex = 35;
            this.simpleButton27.Text = "Tổng Đài\r\nĐiện Thoại";
            // 
            // simpleButton32
            // 
            this.simpleButton32.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton32.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton32.Appearance.Options.UseBackColor = true;
            this.simpleButton32.Appearance.Options.UseFont = true;
            this.simpleButton32.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton32.Location = new System.Drawing.Point(525, 6);
            this.simpleButton32.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton32.Name = "simpleButton32";
            this.simpleButton32.Size = new System.Drawing.Size(84, 58);
            this.simpleButton32.TabIndex = 34;
            this.simpleButton32.Text = "Dịch Vụ\r\nKhác";
            // 
            // simpleButton35
            // 
            this.simpleButton35.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton35.Appearance.Options.UseBackColor = true;
            this.simpleButton35.Appearance.Options.UseFont = true;
            this.simpleButton35.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton35.Location = new System.Drawing.Point(417, 6);
            this.simpleButton35.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton35.Name = "simpleButton35";
            this.simpleButton35.Size = new System.Drawing.Size(96, 58);
            this.simpleButton35.TabIndex = 33;
            this.simpleButton35.Text = "Dịch Vụ\r\nCho Thuê";
            // 
            // simpleButton36
            // 
            this.simpleButton36.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton36.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton36.Appearance.Options.UseBackColor = true;
            this.simpleButton36.Appearance.Options.UseFont = true;
            this.simpleButton36.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton36.Location = new System.Drawing.Point(314, 6);
            this.simpleButton36.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton36.Name = "simpleButton36";
            this.simpleButton36.Size = new System.Drawing.Size(91, 58);
            this.simpleButton36.TabIndex = 32;
            this.simpleButton36.Text = "Dịch Vụ\r\nHồ Bơi";
            // 
            // simpleButton37
            // 
            this.simpleButton37.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton37.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton37.Appearance.Options.UseBackColor = true;
            this.simpleButton37.Appearance.Options.UseFont = true;
            this.simpleButton37.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton37.Location = new System.Drawing.Point(218, 6);
            this.simpleButton37.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton37.Name = "simpleButton37";
            this.simpleButton37.Size = new System.Drawing.Size(84, 58);
            this.simpleButton37.TabIndex = 31;
            this.simpleButton37.Text = "Dịch Vụ\r\nBida";
            // 
            // simpleButton38
            // 
            this.simpleButton38.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton38.Appearance.Options.UseBackColor = true;
            this.simpleButton38.Appearance.Options.UseFont = true;
            this.simpleButton38.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton38.Location = new System.Drawing.Point(116, 6);
            this.simpleButton38.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton38.Name = "simpleButton38";
            this.simpleButton38.Size = new System.Drawing.Size(90, 58);
            this.simpleButton38.TabIndex = 30;
            this.simpleButton38.Text = "Dịch Vụ\r\nSPA";
            // 
            // simpleButton39
            // 
            this.simpleButton39.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton39.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton39.Appearance.Options.UseBackColor = true;
            this.simpleButton39.Appearance.Options.UseFont = true;
            this.simpleButton39.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton39.Location = new System.Drawing.Point(6, 6);
            this.simpleButton39.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton39.Name = "simpleButton39";
            this.simpleButton39.Size = new System.Drawing.Size(98, 58);
            this.simpleButton39.TabIndex = 29;
            this.simpleButton39.Text = "Dịch Vụ\r\nNhà Hàng";
            // 
            // tabNghiepvu
            // 
            this.tabNghiepvu.Controls.Add(this.palbtt_Nghiepvu);
            this.tabNghiepvu.Name = "tabNghiepvu";
            this.tabNghiepvu.Size = new System.Drawing.Size(830, 100);
            this.tabNghiepvu.Text = "Nghiệp vụ";
            // 
            // palbtt_Nghiepvu
            // 
            this.palbtt_Nghiepvu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_Nghiepvu.Controls.Add(this.bttThoat);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton42);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton43);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton44);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton45);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton47);
            this.palbtt_Nghiepvu.Location = new System.Drawing.Point(4, 21);
            this.palbtt_Nghiepvu.Name = "palbtt_Nghiepvu";
            this.palbtt_Nghiepvu.Size = new System.Drawing.Size(822, 58);
            this.palbtt_Nghiepvu.TabIndex = 15;
            // 
            // bttThoat
            // 
            this.bttThoat.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThoat.Appearance.Options.UseBackColor = true;
            this.bttThoat.Appearance.Options.UseFont = true;
            this.bttThoat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThoat.Location = new System.Drawing.Point(699, 1);
            this.bttThoat.Margin = new System.Windows.Forms.Padding(4);
            this.bttThoat.Name = "bttThoat";
            this.bttThoat.Size = new System.Drawing.Size(118, 57);
            this.bttThoat.TabIndex = 35;
            this.bttThoat.Text = "Thoát";
            this.bttThoat.Click += new System.EventHandler(this.bttThoat_Click);
            // 
            // simpleButton42
            // 
            this.simpleButton42.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton42.Appearance.Options.UseBackColor = true;
            this.simpleButton42.Appearance.Options.UseFont = true;
            this.simpleButton42.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton42.Location = new System.Drawing.Point(553, 0);
            this.simpleButton42.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton42.Name = "simpleButton42";
            this.simpleButton42.Size = new System.Drawing.Size(127, 58);
            this.simpleButton42.TabIndex = 34;
            this.simpleButton42.Text = "Trợ Giúp";
            // 
            // simpleButton43
            // 
            this.simpleButton43.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton43.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton43.Appearance.Options.UseBackColor = true;
            this.simpleButton43.Appearance.Options.UseFont = true;
            this.simpleButton43.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton43.Location = new System.Drawing.Point(416, 0);
            this.simpleButton43.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton43.Name = "simpleButton43";
            this.simpleButton43.Size = new System.Drawing.Size(118, 58);
            this.simpleButton43.TabIndex = 33;
            this.simpleButton43.Text = "Mở Cash";
            // 
            // simpleButton44
            // 
            this.simpleButton44.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton44.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton44.Appearance.Options.UseBackColor = true;
            this.simpleButton44.Appearance.Options.UseFont = true;
            this.simpleButton44.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton44.Location = new System.Drawing.Point(274, 1);
            this.simpleButton44.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton44.Name = "simpleButton44";
            this.simpleButton44.Size = new System.Drawing.Size(123, 58);
            this.simpleButton44.TabIndex = 32;
            this.simpleButton44.Text = "Kết Ca";
            // 
            // simpleButton45
            // 
            this.simpleButton45.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton45.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton45.Appearance.Options.UseBackColor = true;
            this.simpleButton45.Appearance.Options.UseFont = true;
            this.simpleButton45.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton45.Location = new System.Drawing.Point(134, 0);
            this.simpleButton45.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton45.Name = "simpleButton45";
            this.simpleButton45.Size = new System.Drawing.Size(121, 58);
            this.simpleButton45.TabIndex = 31;
            this.simpleButton45.Text = "Báo Cáo\r\nDoanh Thu";
            // 
            // simpleButton47
            // 
            this.simpleButton47.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton47.Appearance.Options.UseBackColor = true;
            this.simpleButton47.Appearance.Options.UseFont = true;
            this.simpleButton47.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton47.Location = new System.Drawing.Point(4, 0);
            this.simpleButton47.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton47.Name = "simpleButton47";
            this.simpleButton47.Size = new System.Drawing.Size(111, 58);
            this.simpleButton47.TabIndex = 29;
            this.simpleButton47.Text = "In Lại\r\nHóa Đơn";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.XtraTabControl2);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 553);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1020, 138);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.panelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(838, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(180, 134);
            this.panelControl4.TabIndex = 2;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.simpleButton3);
            this.panelControl5.Controls.Add(this.simpleButton2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(2, 67);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(176, 65);
            this.panelControl5.TabIndex = 4;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton3.Location = new System.Drawing.Point(0, 0);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(88, 65);
            this.simpleButton3.TabIndex = 12;
            this.simpleButton3.Text = "Xóa";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton2.Location = new System.Drawing.Point(88, 0);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(88, 65);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "Lưu";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.bnt_luuban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(176, 65);
            this.panelControl1.TabIndex = 3;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton1.Location = new System.Drawing.Point(0, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(88, 65);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Thêm";
            // 
            // bnt_luuban
            // 
            this.bnt_luuban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luuban.Appearance.Options.UseFont = true;
            this.bnt_luuban.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_luuban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_luuban.Location = new System.Drawing.Point(88, 0);
            this.bnt_luuban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_luuban.Name = "bnt_luuban";
            this.bnt_luuban.Size = new System.Drawing.Size(88, 65);
            this.bnt_luuban.TabIndex = 10;
            this.bnt_luuban.Text = "Sửa";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.Tabcontrol);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 48);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1020, 505);
            this.panelControl3.TabIndex = 1;
            // 
            // Tabcontrol
            // 
            this.Tabcontrol.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tabcontrol.AppearancePage.Header.Options.UseFont = true;
            this.Tabcontrol.AppearancePage.Header.Options.UseTextOptions = true;
            this.Tabcontrol.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tabcontrol.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Tabcontrol.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeaderAndOnMouseHover;
            this.Tabcontrol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabcontrol.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.False;
            this.Tabcontrol.Location = new System.Drawing.Point(2, 2);
            this.Tabcontrol.Name = "Tabcontrol";
            this.Tabcontrol.SelectedTabPage = this.Tabsub_nhomphong;
            this.Tabcontrol.Size = new System.Drawing.Size(1016, 501);
            this.Tabcontrol.TabIndex = 2;
            this.Tabcontrol.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tabsub_nhomphong,
            this.tabsub_TTP_theotg,
            this.Tabsub_TTP_khanangcungungphong,
            this.Tabsub_TTP_thongkephong,
            this.Tabsub_TTP_sodophonghientai,
            this.TabsubBooking_Dangky,
            this.Tabsub_Booking_DSBooking,
            this.Tabsub_Booking_LSLuutru,
            this.Tabsub_Booking_Daily});
            // 
            // Tabsub_nhomphong
            // 
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox10);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox11);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox12);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox13);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox8);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox7);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox6);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox5);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox4);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox3);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox2);
            this.Tabsub_nhomphong.Controls.Add(this.richTextBox1);
            this.Tabsub_nhomphong.Name = "Tabsub_nhomphong";
            this.Tabsub_nhomphong.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_nhomphong.Text = "Nhóm Phòng";
            // 
            // richTextBox10
            // 
            this.richTextBox10.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox10.Location = new System.Drawing.Point(713, 313);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.ReadOnly = true;
            this.richTextBox10.Size = new System.Drawing.Size(212, 125);
            this.richTextBox10.TabIndex = 11;
            this.richTextBox10.Text = "Phòng 403 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox11
            // 
            this.richTextBox11.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox11.Location = new System.Drawing.Point(485, 313);
            this.richTextBox11.Name = "richTextBox11";
            this.richTextBox11.ReadOnly = true;
            this.richTextBox11.Size = new System.Drawing.Size(212, 125);
            this.richTextBox11.TabIndex = 10;
            this.richTextBox11.Text = "Phòng 303 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox12
            // 
            this.richTextBox12.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox12.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox12.Location = new System.Drawing.Point(248, 313);
            this.richTextBox12.Name = "richTextBox12";
            this.richTextBox12.ReadOnly = true;
            this.richTextBox12.Size = new System.Drawing.Size(212, 125);
            this.richTextBox12.TabIndex = 9;
            this.richTextBox12.Text = "Phòng 203 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox13
            // 
            this.richTextBox13.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox13.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox13.Location = new System.Drawing.Point(18, 313);
            this.richTextBox13.Name = "richTextBox13";
            this.richTextBox13.ReadOnly = true;
            this.richTextBox13.Size = new System.Drawing.Size(212, 125);
            this.richTextBox13.TabIndex = 8;
            this.richTextBox13.Text = "Phòng 103 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox8
            // 
            this.richTextBox8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox8.Location = new System.Drawing.Point(713, 164);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.ReadOnly = true;
            this.richTextBox8.Size = new System.Drawing.Size(212, 125);
            this.richTextBox8.TabIndex = 7;
            this.richTextBox8.Text = "Phòng 402 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox7
            // 
            this.richTextBox7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox7.Location = new System.Drawing.Point(485, 164);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.ReadOnly = true;
            this.richTextBox7.Size = new System.Drawing.Size(212, 125);
            this.richTextBox7.TabIndex = 6;
            this.richTextBox7.Text = "Phòng 302 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.Location = new System.Drawing.Point(248, 164);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.ReadOnly = true;
            this.richTextBox6.Size = new System.Drawing.Size(212, 125);
            this.richTextBox6.TabIndex = 5;
            this.richTextBox6.Text = "Phòng 202 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(18, 164);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(212, 125);
            this.richTextBox5.TabIndex = 4;
            this.richTextBox5.Text = "Phòng 102 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.Location = new System.Drawing.Point(713, 15);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(212, 125);
            this.richTextBox4.TabIndex = 3;
            this.richTextBox4.Text = "Phòng 401 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(485, 15);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(212, 125);
            this.richTextBox3.TabIndex = 2;
            this.richTextBox3.Text = "Phòng 301 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(248, 15);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(212, 125);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "Phòng 201 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(18, 15);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(212, 125);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // tabsub_TTP_theotg
            // 
            this.tabsub_TTP_theotg.Controls.Add(this.gridControl2);
            this.tabsub_TTP_theotg.Controls.Add(this.panelControl12);
            this.tabsub_TTP_theotg.Name = "tabsub_TTP_theotg";
            this.tabsub_TTP_theotg.Size = new System.Drawing.Size(1010, 467);
            this.tabsub_TTP_theotg.Text = "TTP_theoTG";
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(971, 467);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOPHONG,
            this.LOAIPHONG,
            this.TEMP1,
            this.TEMP2,
            this.TEMP3,
            this.TEMP4,
            this.TEMP5,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // SOPHONG
            // 
            this.SOPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOPHONG.AppearanceHeader.Options.UseFont = true;
            this.SOPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOPHONG.Caption = "Số phòng";
            this.SOPHONG.FieldName = "SOPHONG";
            this.SOPHONG.Name = "SOPHONG";
            this.SOPHONG.OptionsColumn.AllowEdit = false;
            this.SOPHONG.OptionsColumn.AllowFocus = false;
            this.SOPHONG.OptionsColumn.AllowMove = false;
            this.SOPHONG.OptionsColumn.FixedWidth = true;
            this.SOPHONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.SOPHONG.Visible = true;
            this.SOPHONG.VisibleIndex = 0;
            this.SOPHONG.Width = 99;
            // 
            // LOAIPHONG
            // 
            this.LOAIPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOAIPHONG.AppearanceHeader.Options.UseFont = true;
            this.LOAIPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIPHONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LOAIPHONG.Caption = "Loại phòng";
            this.LOAIPHONG.FieldName = "LOAIPHONG";
            this.LOAIPHONG.Name = "LOAIPHONG";
            this.LOAIPHONG.OptionsColumn.AllowEdit = false;
            this.LOAIPHONG.OptionsColumn.AllowFocus = false;
            this.LOAIPHONG.OptionsColumn.FixedWidth = true;
            this.LOAIPHONG.Visible = true;
            this.LOAIPHONG.VisibleIndex = 1;
            this.LOAIPHONG.Width = 101;
            // 
            // TEMP1
            // 
            this.TEMP1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEMP1.AppearanceHeader.Options.UseFont = true;
            this.TEMP1.AppearanceHeader.Options.UseTextOptions = true;
            this.TEMP1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEMP1.Caption = "TEMP1";
            this.TEMP1.FieldName = "TEMP1";
            this.TEMP1.Name = "TEMP1";
            this.TEMP1.OptionsColumn.AllowEdit = false;
            this.TEMP1.OptionsColumn.AllowFocus = false;
            this.TEMP1.OptionsColumn.FixedWidth = true;
            this.TEMP1.Visible = true;
            this.TEMP1.VisibleIndex = 2;
            this.TEMP1.Width = 120;
            // 
            // TEMP2
            // 
            this.TEMP2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEMP2.AppearanceHeader.Options.UseFont = true;
            this.TEMP2.AppearanceHeader.Options.UseTextOptions = true;
            this.TEMP2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP2.Caption = "TEMP2";
            this.TEMP2.FieldName = "TEMP2";
            this.TEMP2.Name = "TEMP2";
            this.TEMP2.OptionsColumn.AllowEdit = false;
            this.TEMP2.OptionsColumn.AllowFocus = false;
            this.TEMP2.OptionsColumn.FixedWidth = true;
            this.TEMP2.Visible = true;
            this.TEMP2.VisibleIndex = 3;
            this.TEMP2.Width = 113;
            // 
            // TEMP3
            // 
            this.TEMP3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEMP3.AppearanceHeader.Options.UseFont = true;
            this.TEMP3.AppearanceHeader.Options.UseTextOptions = true;
            this.TEMP3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP3.Caption = "TEMP3";
            this.TEMP3.FieldName = "TEMP3";
            this.TEMP3.Name = "TEMP3";
            this.TEMP3.OptionsColumn.AllowEdit = false;
            this.TEMP3.OptionsColumn.AllowFocus = false;
            this.TEMP3.OptionsColumn.FixedWidth = true;
            this.TEMP3.Visible = true;
            this.TEMP3.VisibleIndex = 4;
            this.TEMP3.Width = 148;
            // 
            // TEMP4
            // 
            this.TEMP4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEMP4.AppearanceHeader.Options.UseFont = true;
            this.TEMP4.AppearanceHeader.Options.UseTextOptions = true;
            this.TEMP4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP4.Caption = "TEMP4";
            this.TEMP4.FieldName = "TEMP4";
            this.TEMP4.Name = "TEMP4";
            this.TEMP4.OptionsColumn.AllowEdit = false;
            this.TEMP4.OptionsColumn.AllowFocus = false;
            this.TEMP4.OptionsColumn.FixedWidth = true;
            this.TEMP4.Visible = true;
            this.TEMP4.VisibleIndex = 5;
            this.TEMP4.Width = 108;
            // 
            // TEMP5
            // 
            this.TEMP5.AppearanceCell.Options.UseTextOptions = true;
            this.TEMP5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEMP5.AppearanceHeader.Options.UseFont = true;
            this.TEMP5.AppearanceHeader.Options.UseTextOptions = true;
            this.TEMP5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEMP5.Caption = "TEMP5";
            this.TEMP5.FieldName = "TEMP5";
            this.TEMP5.Name = "TEMP5";
            this.TEMP5.OptionsColumn.AllowEdit = false;
            this.TEMP5.OptionsColumn.AllowFocus = false;
            this.TEMP5.OptionsColumn.FixedWidth = true;
            this.TEMP5.Visible = true;
            this.TEMP5.VisibleIndex = 6;
            this.TEMP5.Width = 100;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 7;
            this.FILL.Width = 180;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.panelControl13);
            this.panelControl12.Controls.Add(this.btnBanphim);
            this.panelControl12.Controls.Add(this.btnCapnhat);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl12.Location = new System.Drawing.Point(971, 0);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(39, 467);
            this.panelControl12.TabIndex = 4;
            // 
            // panelControl13
            // 
            this.panelControl13.AutoSize = true;
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.panelControl14);
            this.panelControl13.Controls.Add(this.btnXuongit);
            this.panelControl13.Controls.Add(this.btnXuongnhieu);
            this.panelControl13.Controls.Add(this.btnLenit);
            this.panelControl13.Controls.Add(this.btnLennhieu);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl13.Location = new System.Drawing.Point(2, 82);
            this.panelControl13.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(35, 303);
            this.panelControl13.TabIndex = 14;
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(0, 160);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(35, -17);
            this.panelControl14.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 143);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 223);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 385);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            // 
            // Tabsub_TTP_khanangcungungphong
            // 
            this.Tabsub_TTP_khanangcungungphong.Controls.Add(this.gridControl1);
            this.Tabsub_TTP_khanangcungungphong.Controls.Add(this.panelControl15);
            this.Tabsub_TTP_khanangcungungphong.Name = "Tabsub_TTP_khanangcungungphong";
            this.Tabsub_TTP_khanangcungungphong.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_TTP_khanangcungungphong.Text = "TTP_kncup";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(971, 467);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TONGSOPHONG,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn1});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // TONGSOPHONG
            // 
            this.TONGSOPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.TONGSOPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGSOPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGSOPHONG.AppearanceHeader.Options.UseFont = true;
            this.TONGSOPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGSOPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGSOPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGSOPHONG.Caption = "Tổng số phòng";
            this.TONGSOPHONG.FieldName = "TONGSOPHONG";
            this.TONGSOPHONG.Name = "TONGSOPHONG";
            this.TONGSOPHONG.OptionsColumn.AllowEdit = false;
            this.TONGSOPHONG.OptionsColumn.AllowFocus = false;
            this.TONGSOPHONG.OptionsColumn.AllowMove = false;
            this.TONGSOPHONG.OptionsColumn.FixedWidth = true;
            this.TONGSOPHONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.TONGSOPHONG.Visible = true;
            this.TONGSOPHONG.VisibleIndex = 0;
            this.TONGSOPHONG.Width = 136;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Loại phòng";
            this.gridColumn3.FieldName = "LOAIPHONG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 101;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "TEMP1";
            this.gridColumn4.FieldName = "TEMP1";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            this.gridColumn4.Width = 120;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "TEMP2";
            this.gridColumn5.FieldName = "TEMP2";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            this.gridColumn5.Width = 113;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "TEMP3";
            this.gridColumn6.FieldName = "TEMP3";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.FixedWidth = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            this.gridColumn6.Width = 148;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "TEMP4";
            this.gridColumn7.FieldName = "TEMP4";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            this.gridColumn7.Width = 108;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "TEMP5";
            this.gridColumn8.FieldName = "TEMP5";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 182;
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.panelControl16);
            this.panelControl15.Controls.Add(this.simpleButton51);
            this.panelControl15.Controls.Add(this.simpleButton52);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl15.Location = new System.Drawing.Point(971, 0);
            this.panelControl15.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(39, 467);
            this.panelControl15.TabIndex = 6;
            // 
            // panelControl16
            // 
            this.panelControl16.AutoSize = true;
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl17);
            this.panelControl16.Controls.Add(this.simpleButton46);
            this.panelControl16.Controls.Add(this.simpleButton48);
            this.panelControl16.Controls.Add(this.simpleButton49);
            this.panelControl16.Controls.Add(this.simpleButton50);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(2, 82);
            this.panelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(35, 303);
            this.panelControl16.TabIndex = 14;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 160);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(35, -17);
            this.panelControl17.TabIndex = 15;
            // 
            // simpleButton46
            // 
            this.simpleButton46.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton46.Appearance.Options.UseFont = true;
            this.simpleButton46.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton46.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton46.Image")));
            this.simpleButton46.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton46.Location = new System.Drawing.Point(0, 143);
            this.simpleButton46.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton46.Name = "simpleButton46";
            this.simpleButton46.Size = new System.Drawing.Size(35, 80);
            this.simpleButton46.TabIndex = 12;
            // 
            // simpleButton48
            // 
            this.simpleButton48.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton48.Appearance.Options.UseFont = true;
            this.simpleButton48.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton48.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton48.Image")));
            this.simpleButton48.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton48.Location = new System.Drawing.Point(0, 223);
            this.simpleButton48.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton48.Name = "simpleButton48";
            this.simpleButton48.Size = new System.Drawing.Size(35, 80);
            this.simpleButton48.TabIndex = 1;
            // 
            // simpleButton49
            // 
            this.simpleButton49.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton49.Appearance.Options.UseFont = true;
            this.simpleButton49.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton49.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton49.Image")));
            this.simpleButton49.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton49.Location = new System.Drawing.Point(0, 80);
            this.simpleButton49.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton49.Name = "simpleButton49";
            this.simpleButton49.Size = new System.Drawing.Size(35, 80);
            this.simpleButton49.TabIndex = 13;
            // 
            // simpleButton50
            // 
            this.simpleButton50.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton50.Appearance.Options.UseFont = true;
            this.simpleButton50.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton50.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton50.Image")));
            this.simpleButton50.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton50.Location = new System.Drawing.Point(0, 0);
            this.simpleButton50.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton50.Name = "simpleButton50";
            this.simpleButton50.Size = new System.Drawing.Size(35, 80);
            this.simpleButton50.TabIndex = 11;
            // 
            // simpleButton51
            // 
            this.simpleButton51.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton51.Appearance.Options.UseFont = true;
            this.simpleButton51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton51.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton51.Image")));
            this.simpleButton51.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton51.Location = new System.Drawing.Point(2, 385);
            this.simpleButton51.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton51.Name = "simpleButton51";
            this.simpleButton51.Size = new System.Drawing.Size(35, 80);
            this.simpleButton51.TabIndex = 9;
            // 
            // simpleButton52
            // 
            this.simpleButton52.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton52.Appearance.Options.UseFont = true;
            this.simpleButton52.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton52.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton52.Image")));
            this.simpleButton52.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton52.Location = new System.Drawing.Point(2, 2);
            this.simpleButton52.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton52.Name = "simpleButton52";
            this.simpleButton52.Size = new System.Drawing.Size(35, 80);
            this.simpleButton52.TabIndex = 0;
            // 
            // Tabsub_TTP_thongkephong
            // 
            this.Tabsub_TTP_thongkephong.Controls.Add(this.panelControl22);
            this.Tabsub_TTP_thongkephong.Controls.Add(this.panelControl21);
            this.Tabsub_TTP_thongkephong.Controls.Add(this.panelControl20);
            this.Tabsub_TTP_thongkephong.Controls.Add(this.panelControl19);
            this.Tabsub_TTP_thongkephong.Controls.Add(this.panelControl18);
            this.Tabsub_TTP_thongkephong.Name = "Tabsub_TTP_thongkephong";
            this.Tabsub_TTP_thongkephong.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_TTP_thongkephong.Text = "TTP_tkp";
            // 
            // panelControl22
            // 
            this.panelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl22.Controls.Add(this.textEdit33);
            this.panelControl22.Controls.Add(this.textEdit48);
            this.panelControl22.Controls.Add(this.textEdit50);
            this.panelControl22.Controls.Add(this.textEdit52);
            this.panelControl22.Controls.Add(this.labelControl21);
            this.panelControl22.Controls.Add(this.textEdit47);
            this.panelControl22.Controls.Add(this.labelControl22);
            this.panelControl22.Controls.Add(this.textEdit51);
            this.panelControl22.Controls.Add(this.labelControl24);
            this.panelControl22.Controls.Add(this.textEdit54);
            this.panelControl22.Controls.Add(this.textEdit55);
            this.panelControl22.Controls.Add(this.labelControl25);
            this.panelControl22.Location = new System.Drawing.Point(712, 10);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(299, 190);
            this.panelControl22.TabIndex = 63;
            // 
            // textEdit33
            // 
            this.textEdit33.EditValue = "10 %";
            this.textEdit33.Enabled = false;
            this.textEdit33.EnterMoveNextControl = true;
            this.textEdit33.Location = new System.Drawing.Point(227, 99);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.Appearance.Options.UseFont = true;
            this.textEdit33.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit33.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit33.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit33.Size = new System.Drawing.Size(54, 26);
            this.textEdit33.TabIndex = 71;
            // 
            // textEdit48
            // 
            this.textEdit48.EditValue = "10 %";
            this.textEdit48.Enabled = false;
            this.textEdit48.EnterMoveNextControl = true;
            this.textEdit48.Location = new System.Drawing.Point(227, 144);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.Appearance.Options.UseFont = true;
            this.textEdit48.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit48.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit48.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit48.Size = new System.Drawing.Size(54, 26);
            this.textEdit48.TabIndex = 70;
            // 
            // textEdit50
            // 
            this.textEdit50.EditValue = "10 %";
            this.textEdit50.Enabled = false;
            this.textEdit50.EnterMoveNextControl = true;
            this.textEdit50.Location = new System.Drawing.Point(229, 50);
            this.textEdit50.Name = "textEdit50";
            this.textEdit50.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit50.Properties.Appearance.Options.UseFont = true;
            this.textEdit50.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit50.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit50.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit50.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit50.Size = new System.Drawing.Size(54, 26);
            this.textEdit50.TabIndex = 69;
            // 
            // textEdit52
            // 
            this.textEdit52.EditValue = "10 %";
            this.textEdit52.Enabled = false;
            this.textEdit52.EnterMoveNextControl = true;
            this.textEdit52.Location = new System.Drawing.Point(227, 5);
            this.textEdit52.Name = "textEdit52";
            this.textEdit52.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit52.Properties.Appearance.Options.UseFont = true;
            this.textEdit52.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit52.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit52.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit52.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit52.Size = new System.Drawing.Size(54, 26);
            this.textEdit52.TabIndex = 68;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(4, 8);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(84, 19);
            this.labelControl21.TabIndex = 48;
            this.labelControl21.Text = "Khách Đoàn";
            // 
            // textEdit47
            // 
            this.textEdit47.EditValue = "10";
            this.textEdit47.Enabled = false;
            this.textEdit47.EnterMoveNextControl = true;
            this.textEdit47.Location = new System.Drawing.Point(158, 5);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.Appearance.Options.UseFont = true;
            this.textEdit47.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit47.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit47.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit47.Size = new System.Drawing.Size(54, 26);
            this.textEdit47.TabIndex = 49;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(4, 53);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(108, 19);
            this.labelControl22.TabIndex = 51;
            this.labelControl22.Text = "Khách Vãng Lai";
            // 
            // textEdit51
            // 
            this.textEdit51.EditValue = "10";
            this.textEdit51.Enabled = false;
            this.textEdit51.EnterMoveNextControl = true;
            this.textEdit51.Location = new System.Drawing.Point(158, 50);
            this.textEdit51.Name = "textEdit51";
            this.textEdit51.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit51.Properties.Appearance.Options.UseFont = true;
            this.textEdit51.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit51.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit51.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit51.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit51.Size = new System.Drawing.Size(54, 26);
            this.textEdit51.TabIndex = 52;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(4, 102);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(68, 19);
            this.labelControl24.TabIndex = 54;
            this.labelControl24.Text = "Khách Nữ";
            // 
            // textEdit54
            // 
            this.textEdit54.EditValue = "10";
            this.textEdit54.Enabled = false;
            this.textEdit54.EnterMoveNextControl = true;
            this.textEdit54.Location = new System.Drawing.Point(158, 144);
            this.textEdit54.Name = "textEdit54";
            this.textEdit54.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit54.Properties.Appearance.Options.UseFont = true;
            this.textEdit54.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit54.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit54.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit54.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit54.Size = new System.Drawing.Size(54, 26);
            this.textEdit54.TabIndex = 58;
            // 
            // textEdit55
            // 
            this.textEdit55.EditValue = "10";
            this.textEdit55.Enabled = false;
            this.textEdit55.EnterMoveNextControl = true;
            this.textEdit55.Location = new System.Drawing.Point(158, 99);
            this.textEdit55.Name = "textEdit55";
            this.textEdit55.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit55.Properties.Appearance.Options.UseFont = true;
            this.textEdit55.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit55.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit55.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit55.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit55.Size = new System.Drawing.Size(54, 26);
            this.textEdit55.TabIndex = 55;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(8, 147);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(80, 19);
            this.labelControl25.TabIndex = 57;
            this.labelControl25.Text = "Khách Nam";
            // 
            // panelControl21
            // 
            this.panelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl21.Controls.Add(this.textEdit38);
            this.panelControl21.Controls.Add(this.labelControl16);
            this.panelControl21.Controls.Add(this.textEdit39);
            this.panelControl21.Controls.Add(this.textEdit37);
            this.panelControl21.Controls.Add(this.textEdit42);
            this.panelControl21.Controls.Add(this.labelControl17);
            this.panelControl21.Controls.Add(this.textEdit43);
            this.panelControl21.Controls.Add(this.textEdit40);
            this.panelControl21.Controls.Add(this.textEdit46);
            this.panelControl21.Controls.Add(this.textEdit41);
            this.panelControl21.Controls.Add(this.labelControl18);
            this.panelControl21.Controls.Add(this.labelControl19);
            this.panelControl21.Controls.Add(this.textEdit44);
            this.panelControl21.Controls.Add(this.textEdit45);
            this.panelControl21.Controls.Add(this.labelControl20);
            this.panelControl21.Location = new System.Drawing.Point(349, 242);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(299, 222);
            this.panelControl21.TabIndex = 63;
            // 
            // textEdit38
            // 
            this.textEdit38.EditValue = "10 %";
            this.textEdit38.Enabled = false;
            this.textEdit38.EnterMoveNextControl = true;
            this.textEdit38.Location = new System.Drawing.Point(227, 99);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.Appearance.Options.UseFont = true;
            this.textEdit38.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit38.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit38.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit38.Size = new System.Drawing.Size(54, 26);
            this.textEdit38.TabIndex = 72;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(4, 8);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(126, 19);
            this.labelControl16.TabIndex = 48;
            this.labelControl16.Text = "Khách Về Dự Kiến";
            // 
            // textEdit39
            // 
            this.textEdit39.EditValue = "10 %";
            this.textEdit39.Enabled = false;
            this.textEdit39.EnterMoveNextControl = true;
            this.textEdit39.Location = new System.Drawing.Point(227, 144);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.Appearance.Options.UseFont = true;
            this.textEdit39.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit39.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit39.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit39.Size = new System.Drawing.Size(54, 26);
            this.textEdit39.TabIndex = 71;
            // 
            // textEdit37
            // 
            this.textEdit37.EditValue = "10";
            this.textEdit37.Enabled = false;
            this.textEdit37.EnterMoveNextControl = true;
            this.textEdit37.Location = new System.Drawing.Point(158, 5);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.Appearance.Options.UseFont = true;
            this.textEdit37.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit37.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit37.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit37.Size = new System.Drawing.Size(54, 26);
            this.textEdit37.TabIndex = 49;
            // 
            // textEdit42
            // 
            this.textEdit42.EditValue = "10 %";
            this.textEdit42.Enabled = false;
            this.textEdit42.EnterMoveNextControl = true;
            this.textEdit42.Location = new System.Drawing.Point(227, 189);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.Appearance.Options.UseFont = true;
            this.textEdit42.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit42.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit42.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit42.Size = new System.Drawing.Size(54, 26);
            this.textEdit42.TabIndex = 70;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(4, 53);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(74, 19);
            this.labelControl17.TabIndex = 51;
            this.labelControl17.Text = "Người Lớn";
            // 
            // textEdit43
            // 
            this.textEdit43.EditValue = "10 %";
            this.textEdit43.Enabled = false;
            this.textEdit43.EnterMoveNextControl = true;
            this.textEdit43.Location = new System.Drawing.Point(227, 50);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.Appearance.Options.UseFont = true;
            this.textEdit43.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit43.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit43.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit43.Size = new System.Drawing.Size(54, 26);
            this.textEdit43.TabIndex = 69;
            // 
            // textEdit40
            // 
            this.textEdit40.EditValue = "10";
            this.textEdit40.Enabled = false;
            this.textEdit40.EnterMoveNextControl = true;
            this.textEdit40.Location = new System.Drawing.Point(158, 189);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.Appearance.Options.UseFont = true;
            this.textEdit40.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit40.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit40.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit40.Size = new System.Drawing.Size(54, 26);
            this.textEdit40.TabIndex = 61;
            // 
            // textEdit46
            // 
            this.textEdit46.EditValue = "10 %";
            this.textEdit46.Enabled = false;
            this.textEdit46.EnterMoveNextControl = true;
            this.textEdit46.Location = new System.Drawing.Point(227, 5);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.Appearance.Options.UseFont = true;
            this.textEdit46.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit46.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit46.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit46.Size = new System.Drawing.Size(54, 26);
            this.textEdit46.TabIndex = 68;
            // 
            // textEdit41
            // 
            this.textEdit41.EditValue = "10";
            this.textEdit41.Enabled = false;
            this.textEdit41.EnterMoveNextControl = true;
            this.textEdit41.Location = new System.Drawing.Point(158, 50);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.Appearance.Options.UseFont = true;
            this.textEdit41.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit41.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit41.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit41.Size = new System.Drawing.Size(54, 26);
            this.textEdit41.TabIndex = 52;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(4, 192);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(81, 19);
            this.labelControl18.TabIndex = 60;
            this.labelControl18.Text = "Nước ngoài";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(4, 102);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(52, 19);
            this.labelControl19.TabIndex = 54;
            this.labelControl19.Text = "Trẻ Em";
            // 
            // textEdit44
            // 
            this.textEdit44.EditValue = "10";
            this.textEdit44.Enabled = false;
            this.textEdit44.EnterMoveNextControl = true;
            this.textEdit44.Location = new System.Drawing.Point(158, 144);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.Appearance.Options.UseFont = true;
            this.textEdit44.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit44.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit44.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit44.Size = new System.Drawing.Size(54, 26);
            this.textEdit44.TabIndex = 58;
            // 
            // textEdit45
            // 
            this.textEdit45.EditValue = "10";
            this.textEdit45.Enabled = false;
            this.textEdit45.EnterMoveNextControl = true;
            this.textEdit45.Location = new System.Drawing.Point(158, 99);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.Appearance.Options.UseFont = true;
            this.textEdit45.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit45.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit45.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit45.Size = new System.Drawing.Size(54, 26);
            this.textEdit45.TabIndex = 55;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(5, 147);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(65, 19);
            this.labelControl20.TabIndex = 57;
            this.labelControl20.Text = "Việt Nam";
            // 
            // panelControl20
            // 
            this.panelControl20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl20.Controls.Add(this.textEdit31);
            this.panelControl20.Controls.Add(this.textEdit29);
            this.panelControl20.Controls.Add(this.textEdit27);
            this.panelControl20.Controls.Add(this.textEdit49);
            this.panelControl20.Controls.Add(this.textEdit35);
            this.panelControl20.Controls.Add(this.labelControl15);
            this.panelControl20.Controls.Add(this.textEdit36);
            this.panelControl20.Controls.Add(this.labelControl14);
            this.panelControl20.Controls.Add(this.textEdit28);
            this.panelControl20.Controls.Add(this.textEdit34);
            this.panelControl20.Controls.Add(this.labelControl11);
            this.panelControl20.Controls.Add(this.labelControl13);
            this.panelControl20.Controls.Add(this.textEdit30);
            this.panelControl20.Controls.Add(this.textEdit32);
            this.panelControl20.Controls.Add(this.labelControl12);
            this.panelControl20.Location = new System.Drawing.Point(349, 10);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(299, 224);
            this.panelControl20.TabIndex = 20;
            // 
            // textEdit31
            // 
            this.textEdit31.EditValue = "10 %";
            this.textEdit31.Enabled = false;
            this.textEdit31.EnterMoveNextControl = true;
            this.textEdit31.Location = new System.Drawing.Point(227, 99);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.Appearance.Options.UseFont = true;
            this.textEdit31.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit31.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit31.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit31.Size = new System.Drawing.Size(54, 26);
            this.textEdit31.TabIndex = 67;
            // 
            // textEdit29
            // 
            this.textEdit29.EditValue = "10 %";
            this.textEdit29.Enabled = false;
            this.textEdit29.EnterMoveNextControl = true;
            this.textEdit29.Location = new System.Drawing.Point(227, 144);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.Appearance.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit29.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit29.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit29.Size = new System.Drawing.Size(54, 26);
            this.textEdit29.TabIndex = 66;
            // 
            // textEdit27
            // 
            this.textEdit27.EditValue = "10 %";
            this.textEdit27.Enabled = false;
            this.textEdit27.EnterMoveNextControl = true;
            this.textEdit27.Location = new System.Drawing.Point(228, 189);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.Appearance.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit27.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit27.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit27.Size = new System.Drawing.Size(54, 26);
            this.textEdit27.TabIndex = 65;
            // 
            // textEdit49
            // 
            this.textEdit49.EditValue = "10 %";
            this.textEdit49.Enabled = false;
            this.textEdit49.EnterMoveNextControl = true;
            this.textEdit49.Location = new System.Drawing.Point(228, 50);
            this.textEdit49.Name = "textEdit49";
            this.textEdit49.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit49.Properties.Appearance.Options.UseFont = true;
            this.textEdit49.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit49.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit49.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit49.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit49.Size = new System.Drawing.Size(54, 26);
            this.textEdit49.TabIndex = 64;
            // 
            // textEdit35
            // 
            this.textEdit35.EditValue = "10 %";
            this.textEdit35.Enabled = false;
            this.textEdit35.EnterMoveNextControl = true;
            this.textEdit35.Location = new System.Drawing.Point(228, 5);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.Appearance.Options.UseFont = true;
            this.textEdit35.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit35.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit35.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit35.Size = new System.Drawing.Size(54, 26);
            this.textEdit35.TabIndex = 63;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(4, 8);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(75, 19);
            this.labelControl15.TabIndex = 48;
            this.labelControl15.Text = "Phòng VIP";
            // 
            // textEdit36
            // 
            this.textEdit36.EditValue = "10";
            this.textEdit36.Enabled = false;
            this.textEdit36.EnterMoveNextControl = true;
            this.textEdit36.Location = new System.Drawing.Point(158, 5);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.Appearance.Options.UseFont = true;
            this.textEdit36.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit36.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit36.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit36.Size = new System.Drawing.Size(54, 26);
            this.textEdit36.TabIndex = 49;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(4, 53);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(146, 19);
            this.labelControl14.TabIndex = 51;
            this.labelControl14.Text = "Khách Đang Lưu Trú";
            // 
            // textEdit28
            // 
            this.textEdit28.EditValue = "10";
            this.textEdit28.Enabled = false;
            this.textEdit28.EnterMoveNextControl = true;
            this.textEdit28.Location = new System.Drawing.Point(158, 189);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.Appearance.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit28.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit28.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit28.Size = new System.Drawing.Size(54, 26);
            this.textEdit28.TabIndex = 61;
            // 
            // textEdit34
            // 
            this.textEdit34.EditValue = "10";
            this.textEdit34.Enabled = false;
            this.textEdit34.EnterMoveNextControl = true;
            this.textEdit34.Location = new System.Drawing.Point(158, 50);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.Appearance.Options.UseFont = true;
            this.textEdit34.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit34.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit34.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit34.Size = new System.Drawing.Size(54, 26);
            this.textEdit34.TabIndex = 52;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(5, 192);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(136, 19);
            this.labelControl11.TabIndex = 60;
            this.labelControl11.Text = "Khách Đến Dự Kiến";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(4, 102);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(75, 19);
            this.labelControl13.TabIndex = 54;
            this.labelControl13.Text = "Khách Đến";
            // 
            // textEdit30
            // 
            this.textEdit30.EditValue = "10";
            this.textEdit30.Enabled = false;
            this.textEdit30.EnterMoveNextControl = true;
            this.textEdit30.Location = new System.Drawing.Point(158, 144);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.Appearance.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit30.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit30.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit30.Size = new System.Drawing.Size(54, 26);
            this.textEdit30.TabIndex = 58;
            // 
            // textEdit32
            // 
            this.textEdit32.EditValue = "10";
            this.textEdit32.Enabled = false;
            this.textEdit32.EnterMoveNextControl = true;
            this.textEdit32.Location = new System.Drawing.Point(158, 99);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.Appearance.Options.UseFont = true;
            this.textEdit32.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit32.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit32.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit32.Size = new System.Drawing.Size(54, 26);
            this.textEdit32.TabIndex = 55;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(4, 147);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(65, 19);
            this.labelControl12.TabIndex = 57;
            this.labelControl12.Text = "Khách Về";
            // 
            // panelControl19
            // 
            this.panelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl19.Controls.Add(this.labelControl10);
            this.panelControl19.Controls.Add(this.textEdit26);
            this.panelControl19.Controls.Add(this.textEdit25);
            this.panelControl19.Controls.Add(this.labelControl9);
            this.panelControl19.Controls.Add(this.textEdit24);
            this.panelControl19.Controls.Add(this.textEdit23);
            this.panelControl19.Controls.Add(this.labelControl8);
            this.panelControl19.Controls.Add(this.textEdit22);
            this.panelControl19.Controls.Add(this.textEdit21);
            this.panelControl19.Controls.Add(this.labelControl7);
            this.panelControl19.Controls.Add(this.textEdit20);
            this.panelControl19.Controls.Add(this.textEdit19);
            this.panelControl19.Controls.Add(this.labelControl6);
            this.panelControl19.Controls.Add(this.textEdit18);
            this.panelControl19.Controls.Add(this.textEdit17);
            this.panelControl19.Location = new System.Drawing.Point(9, 242);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(276, 222);
            this.panelControl19.TabIndex = 33;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(6, 8);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(107, 19);
            this.labelControl10.TabIndex = 33;
            this.labelControl10.Text = "Phòng bận bẩn";
            // 
            // textEdit26
            // 
            this.textEdit26.EditValue = "15";
            this.textEdit26.Enabled = false;
            this.textEdit26.EnterMoveNextControl = true;
            this.textEdit26.Location = new System.Drawing.Point(138, 5);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.Appearance.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit26.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit26.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit26.Size = new System.Drawing.Size(54, 26);
            this.textEdit26.TabIndex = 34;
            // 
            // textEdit25
            // 
            this.textEdit25.EditValue = "15 %";
            this.textEdit25.Enabled = false;
            this.textEdit25.EnterMoveNextControl = true;
            this.textEdit25.Location = new System.Drawing.Point(208, 5);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.Appearance.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit25.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit25.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit25.Size = new System.Drawing.Size(54, 26);
            this.textEdit25.TabIndex = 35;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(4, 53);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(115, 19);
            this.labelControl9.TabIndex = 36;
            this.labelControl9.Text = "Phòng đang sửa";
            // 
            // textEdit24
            // 
            this.textEdit24.EditValue = "23";
            this.textEdit24.Enabled = false;
            this.textEdit24.EnterMoveNextControl = true;
            this.textEdit24.Location = new System.Drawing.Point(138, 50);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.Appearance.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit24.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit24.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit24.Size = new System.Drawing.Size(54, 26);
            this.textEdit24.TabIndex = 37;
            // 
            // textEdit23
            // 
            this.textEdit23.EditValue = "23 %";
            this.textEdit23.Enabled = false;
            this.textEdit23.EnterMoveNextControl = true;
            this.textEdit23.Location = new System.Drawing.Point(208, 50);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.Appearance.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit23.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit23.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit23.Size = new System.Drawing.Size(54, 26);
            this.textEdit23.TabIndex = 38;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(6, 102);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(74, 19);
            this.labelControl8.TabIndex = 39;
            this.labelControl8.Text = "Phòng Đặt";
            // 
            // textEdit22
            // 
            this.textEdit22.EditValue = "30";
            this.textEdit22.Enabled = false;
            this.textEdit22.EnterMoveNextControl = true;
            this.textEdit22.Location = new System.Drawing.Point(138, 99);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit22.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit22.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit22.Size = new System.Drawing.Size(54, 26);
            this.textEdit22.TabIndex = 40;
            // 
            // textEdit21
            // 
            this.textEdit21.EditValue = "30 %";
            this.textEdit21.Enabled = false;
            this.textEdit21.EnterMoveNextControl = true;
            this.textEdit21.Location = new System.Drawing.Point(208, 99);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit21.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit21.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit21.Size = new System.Drawing.Size(54, 26);
            this.textEdit21.TabIndex = 41;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(4, 147);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(79, 19);
            this.labelControl7.TabIndex = 42;
            this.labelControl7.Text = "Phòng Đơn";
            // 
            // textEdit20
            // 
            this.textEdit20.EditValue = "70";
            this.textEdit20.Enabled = false;
            this.textEdit20.EnterMoveNextControl = true;
            this.textEdit20.Location = new System.Drawing.Point(138, 144);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit20.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit20.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit20.Size = new System.Drawing.Size(54, 26);
            this.textEdit20.TabIndex = 43;
            // 
            // textEdit19
            // 
            this.textEdit19.EditValue = "70 %";
            this.textEdit19.Enabled = false;
            this.textEdit19.EnterMoveNextControl = true;
            this.textEdit19.Location = new System.Drawing.Point(208, 144);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit19.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit19.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit19.Size = new System.Drawing.Size(54, 26);
            this.textEdit19.TabIndex = 44;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 192);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 19);
            this.labelControl6.TabIndex = 45;
            this.labelControl6.Text = "Phòng Đôi";
            // 
            // textEdit18
            // 
            this.textEdit18.EditValue = "30";
            this.textEdit18.Enabled = false;
            this.textEdit18.EnterMoveNextControl = true;
            this.textEdit18.Location = new System.Drawing.Point(138, 189);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit18.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit18.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit18.Size = new System.Drawing.Size(54, 26);
            this.textEdit18.TabIndex = 46;
            // 
            // textEdit17
            // 
            this.textEdit17.EditValue = "30 %";
            this.textEdit17.Enabled = false;
            this.textEdit17.EnterMoveNextControl = true;
            this.textEdit17.Location = new System.Drawing.Point(208, 189);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit17.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit17.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit17.Size = new System.Drawing.Size(54, 26);
            this.textEdit17.TabIndex = 47;
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.labelControl5);
            this.panelControl18.Controls.Add(this.textEdit7);
            this.panelControl18.Controls.Add(this.textEdit8);
            this.panelControl18.Controls.Add(this.labelControl1);
            this.panelControl18.Controls.Add(this.textEdit10);
            this.panelControl18.Controls.Add(this.textEdit9);
            this.panelControl18.Controls.Add(this.labelControl2);
            this.panelControl18.Controls.Add(this.textEdit12);
            this.panelControl18.Controls.Add(this.textEdit11);
            this.panelControl18.Controls.Add(this.labelControl3);
            this.panelControl18.Controls.Add(this.textEdit14);
            this.panelControl18.Controls.Add(this.textEdit13);
            this.panelControl18.Controls.Add(this.labelControl4);
            this.panelControl18.Controls.Add(this.textEdit16);
            this.panelControl18.Controls.Add(this.textEdit15);
            this.panelControl18.Location = new System.Drawing.Point(9, 10);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(276, 224);
            this.panelControl18.TabIndex = 19;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(4, 8);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(108, 19);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Tổng số phòng";
            // 
            // textEdit7
            // 
            this.textEdit7.EditValue = "100";
            this.textEdit7.Enabled = false;
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(138, 5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit7.Size = new System.Drawing.Size(54, 26);
            this.textEdit7.TabIndex = 19;
            // 
            // textEdit8
            // 
            this.textEdit8.EditValue = "100 %";
            this.textEdit8.Enabled = false;
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(208, 5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit8.Size = new System.Drawing.Size(54, 26);
            this.textEdit8.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(4, 53);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(112, 19);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "Phòng có khách";
            // 
            // textEdit10
            // 
            this.textEdit10.EditValue = "50";
            this.textEdit10.Enabled = false;
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(138, 50);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Size = new System.Drawing.Size(54, 26);
            this.textEdit10.TabIndex = 22;
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "50 %";
            this.textEdit9.Enabled = false;
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(208, 50);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Size = new System.Drawing.Size(54, 26);
            this.textEdit9.TabIndex = 23;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(4, 102);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(124, 19);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Phòng trống sạch";
            // 
            // textEdit12
            // 
            this.textEdit12.EditValue = "10";
            this.textEdit12.Enabled = false;
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(138, 99);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Size = new System.Drawing.Size(54, 26);
            this.textEdit12.TabIndex = 25;
            // 
            // textEdit11
            // 
            this.textEdit11.EditValue = "10 %";
            this.textEdit11.Enabled = false;
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(208, 99);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Size = new System.Drawing.Size(54, 26);
            this.textEdit11.TabIndex = 26;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(4, 147);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(119, 19);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "Phòng trống bẩn";
            // 
            // textEdit14
            // 
            this.textEdit14.EditValue = "0";
            this.textEdit14.Enabled = false;
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(138, 144);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit14.Size = new System.Drawing.Size(54, 26);
            this.textEdit14.TabIndex = 28;
            // 
            // textEdit13
            // 
            this.textEdit13.EditValue = "0 %";
            this.textEdit13.Enabled = false;
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(208, 144);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit13.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit13.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit13.Size = new System.Drawing.Size(54, 26);
            this.textEdit13.TabIndex = 29;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(4, 192);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(112, 19);
            this.labelControl4.TabIndex = 30;
            this.labelControl4.Text = "Phòng bận sạch";
            // 
            // textEdit16
            // 
            this.textEdit16.EditValue = "7";
            this.textEdit16.Enabled = false;
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(138, 189);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit16.Size = new System.Drawing.Size(54, 26);
            this.textEdit16.TabIndex = 31;
            // 
            // textEdit15
            // 
            this.textEdit15.EditValue = "7 %";
            this.textEdit15.Enabled = false;
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(208, 189);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Size = new System.Drawing.Size(54, 26);
            this.textEdit15.TabIndex = 32;
            // 
            // Tabsub_TTP_sodophonghientai
            // 
            this.Tabsub_TTP_sodophonghientai.Name = "Tabsub_TTP_sodophonghientai";
            this.Tabsub_TTP_sodophonghientai.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_TTP_sodophonghientai.Text = "TTP_sdpht";
            // 
            // TabsubBooking_Dangky
            // 
            this.TabsubBooking_Dangky.Controls.Add(this.palBookDangky);
            this.TabsubBooking_Dangky.Name = "TabsubBooking_Dangky";
            this.TabsubBooking_Dangky.Size = new System.Drawing.Size(1010, 467);
            this.TabsubBooking_Dangky.Text = "Booking_Dangky";
            // 
            // palBookDangky
            // 
            this.palBookDangky.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palBookDangky.Controls.Add(this.xtraTabControl1);
            this.palBookDangky.Controls.Add(this.panelControl25);
            this.palBookDangky.Controls.Add(this.panelControl24);
            this.palBookDangky.Controls.Add(this.panelControl23);
            this.palBookDangky.Location = new System.Drawing.Point(3, 3);
            this.palBookDangky.Name = "palBookDangky";
            this.palBookDangky.Size = new System.Drawing.Size(1007, 461);
            this.palBookDangky.TabIndex = 62;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.xtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Location = new System.Drawing.Point(413, 65);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(596, 400);
            this.xtraTabControl1.TabIndex = 51;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.richTextBox9);
            this.xtraTabPage1.Controls.Add(this.labelControl52);
            this.xtraTabPage1.Controls.Add(this.textEdit77);
            this.xtraTabPage1.Controls.Add(this.textEdit78);
            this.xtraTabPage1.Controls.Add(this.textEdit79);
            this.xtraTabPage1.Controls.Add(this.labelControl44);
            this.xtraTabPage1.Controls.Add(this.labelControl45);
            this.xtraTabPage1.Controls.Add(this.labelControl51);
            this.xtraTabPage1.Controls.Add(this.textEdit76);
            this.xtraTabPage1.Controls.Add(this.labelControl37);
            this.xtraTabPage1.Controls.Add(this.textEdit73);
            this.xtraTabPage1.Controls.Add(this.textEdit75);
            this.xtraTabPage1.Controls.Add(this.textEdit53);
            this.xtraTabPage1.Controls.Add(this.labelControl36);
            this.xtraTabPage1.Controls.Add(this.textEdit59);
            this.xtraTabPage1.Controls.Add(this.labelControl43);
            this.xtraTabPage1.Controls.Add(this.textEdit66);
            this.xtraTabPage1.Controls.Add(this.textEdit67);
            this.xtraTabPage1.Controls.Add(this.textEdit72);
            this.xtraTabPage1.Controls.Add(this.labelControl46);
            this.xtraTabPage1.Controls.Add(this.labelControl47);
            this.xtraTabPage1.Controls.Add(this.labelControl48);
            this.xtraTabPage1.Controls.Add(this.labelControl49);
            this.xtraTabPage1.Controls.Add(this.labelControl50);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(590, 366);
            this.xtraTabPage1.Text = "Thông Tin Khách";
            // 
            // richTextBox9
            // 
            this.richTextBox9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox9.Location = new System.Drawing.Point(393, 145);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(187, 214);
            this.richTextBox9.TabIndex = 73;
            this.richTextBox9.Text = "";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Location = new System.Drawing.Point(312, 150);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(54, 19);
            this.labelControl52.TabIndex = 72;
            this.labelControl52.Text = "Ghi chú";
            // 
            // textEdit77
            // 
            this.textEdit77.EditValue = "";
            this.textEdit77.EnterMoveNextControl = true;
            this.textEdit77.Location = new System.Drawing.Point(393, 9);
            this.textEdit77.Name = "textEdit77";
            this.textEdit77.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit77.Properties.Appearance.Options.UseFont = true;
            this.textEdit77.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit77.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit77.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit77.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit77.Size = new System.Drawing.Size(187, 26);
            this.textEdit77.TabIndex = 71;
            // 
            // textEdit78
            // 
            this.textEdit78.EditValue = "";
            this.textEdit78.EnterMoveNextControl = true;
            this.textEdit78.Location = new System.Drawing.Point(393, 101);
            this.textEdit78.Name = "textEdit78";
            this.textEdit78.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit78.Properties.Appearance.Options.UseFont = true;
            this.textEdit78.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit78.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit78.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit78.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit78.Size = new System.Drawing.Size(187, 26);
            this.textEdit78.TabIndex = 70;
            // 
            // textEdit79
            // 
            this.textEdit79.EditValue = "";
            this.textEdit79.EnterMoveNextControl = true;
            this.textEdit79.Location = new System.Drawing.Point(393, 55);
            this.textEdit79.Name = "textEdit79";
            this.textEdit79.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit79.Properties.Appearance.Options.UseFont = true;
            this.textEdit79.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit79.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit79.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit79.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit79.Size = new System.Drawing.Size(187, 26);
            this.textEdit79.TabIndex = 69;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Location = new System.Drawing.Point(311, 12);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(69, 19);
            this.labelControl44.TabIndex = 66;
            this.labelControl44.Text = "Mã Đại Lý";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Location = new System.Drawing.Point(311, 58);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(76, 19);
            this.labelControl45.TabIndex = 67;
            this.labelControl45.Text = "Tên Đại Lý";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Location = new System.Drawing.Point(312, 104);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(36, 19);
            this.labelControl51.TabIndex = 68;
            this.labelControl51.Text = "Ngày";
            // 
            // textEdit76
            // 
            this.textEdit76.EditValue = "";
            this.textEdit76.EnterMoveNextControl = true;
            this.textEdit76.Location = new System.Drawing.Point(116, 331);
            this.textEdit76.Name = "textEdit76";
            this.textEdit76.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit76.Properties.Appearance.Options.UseFont = true;
            this.textEdit76.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit76.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit76.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit76.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit76.Size = new System.Drawing.Size(185, 26);
            this.textEdit76.TabIndex = 65;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(5, 334);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(51, 19);
            this.labelControl37.TabIndex = 64;
            this.labelControl37.Text = "Địa Chỉ";
            // 
            // textEdit73
            // 
            this.textEdit73.EditValue = "";
            this.textEdit73.EnterMoveNextControl = true;
            this.textEdit73.Location = new System.Drawing.Point(115, 55);
            this.textEdit73.Name = "textEdit73";
            this.textEdit73.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit73.Properties.Appearance.Options.UseFont = true;
            this.textEdit73.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit73.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit73.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit73.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit73.Size = new System.Drawing.Size(185, 26);
            this.textEdit73.TabIndex = 63;
            // 
            // textEdit75
            // 
            this.textEdit75.EditValue = "";
            this.textEdit75.EnterMoveNextControl = true;
            this.textEdit75.Location = new System.Drawing.Point(115, 9);
            this.textEdit75.Name = "textEdit75";
            this.textEdit75.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit75.Properties.Appearance.Options.UseFont = true;
            this.textEdit75.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit75.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit75.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit75.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit75.Size = new System.Drawing.Size(185, 26);
            this.textEdit75.TabIndex = 62;
            // 
            // textEdit53
            // 
            this.textEdit53.EditValue = "";
            this.textEdit53.EnterMoveNextControl = true;
            this.textEdit53.Location = new System.Drawing.Point(116, 285);
            this.textEdit53.Name = "textEdit53";
            this.textEdit53.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit53.Properties.Appearance.Options.UseFont = true;
            this.textEdit53.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit53.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit53.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit53.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit53.Size = new System.Drawing.Size(185, 26);
            this.textEdit53.TabIndex = 61;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(5, 288);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(39, 19);
            this.labelControl36.TabIndex = 60;
            this.labelControl36.Text = "Email";
            // 
            // textEdit59
            // 
            this.textEdit59.EditValue = "";
            this.textEdit59.EnterMoveNextControl = true;
            this.textEdit59.Location = new System.Drawing.Point(116, 239);
            this.textEdit59.Name = "textEdit59";
            this.textEdit59.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit59.Properties.Appearance.Options.UseFont = true;
            this.textEdit59.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit59.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit59.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit59.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit59.Size = new System.Drawing.Size(185, 26);
            this.textEdit59.TabIndex = 58;
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Location = new System.Drawing.Point(5, 242);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(72, 19);
            this.labelControl43.TabIndex = 57;
            this.labelControl43.Text = "Quốc Tịch";
            // 
            // textEdit66
            // 
            this.textEdit66.EditValue = "";
            this.textEdit66.EnterMoveNextControl = true;
            this.textEdit66.Location = new System.Drawing.Point(116, 193);
            this.textEdit66.Name = "textEdit66";
            this.textEdit66.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit66.Properties.Appearance.Options.UseFont = true;
            this.textEdit66.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit66.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit66.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit66.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit66.Size = new System.Drawing.Size(185, 26);
            this.textEdit66.TabIndex = 54;
            // 
            // textEdit67
            // 
            this.textEdit67.EditValue = "";
            this.textEdit67.EnterMoveNextControl = true;
            this.textEdit67.Location = new System.Drawing.Point(116, 147);
            this.textEdit67.Name = "textEdit67";
            this.textEdit67.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit67.Properties.Appearance.Options.UseFont = true;
            this.textEdit67.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit67.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit67.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit67.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit67.Size = new System.Drawing.Size(185, 26);
            this.textEdit67.TabIndex = 53;
            // 
            // textEdit72
            // 
            this.textEdit72.EditValue = "";
            this.textEdit72.EnterMoveNextControl = true;
            this.textEdit72.Location = new System.Drawing.Point(116, 101);
            this.textEdit72.Name = "textEdit72";
            this.textEdit72.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit72.Properties.Appearance.Options.UseFont = true;
            this.textEdit72.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit72.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit72.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit72.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit72.Size = new System.Drawing.Size(185, 26);
            this.textEdit72.TabIndex = 52;
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Location = new System.Drawing.Point(5, 12);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(103, 19);
            this.labelControl46.TabIndex = 47;
            this.labelControl46.Text = "Đoàn/Công Ty";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Location = new System.Drawing.Point(5, 58);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(83, 19);
            this.labelControl47.TabIndex = 48;
            this.labelControl47.Text = "Người Book";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Location = new System.Drawing.Point(4, 104);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(77, 19);
            this.labelControl48.TabIndex = 49;
            this.labelControl48.Text = "Điện Thoại";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Location = new System.Drawing.Point(5, 150);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(98, 19);
            this.labelControl49.TabIndex = 50;
            this.labelControl49.Text = "CMT/Passport";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(4, 196);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(29, 19);
            this.labelControl50.TabIndex = 51;
            this.labelControl50.Text = "Visa";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(590, 366);
            this.xtraTabPage3.Text = "Chi Tiết Phòng";
            // 
            // gridControl3
            // 
            this.gridControl3.AllowDrop = true;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(590, 366);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.LOAIPHONG1,
            this.PHONG1,
            this.SLKHACH,
            this.TENKHACH,
            this.CMT,
            this.NGAYCMT,
            this.VISA,
            this.NGAYVISA,
            this.QUOCTICH});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 59;
            // 
            // LOAIPHONG1
            // 
            this.LOAIPHONG1.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIPHONG1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIPHONG1.AppearanceHeader.Options.UseFont = true;
            this.LOAIPHONG1.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LOAIPHONG1.Caption = "Loại phòng";
            this.LOAIPHONG1.FieldName = "LOAIPHONG1";
            this.LOAIPHONG1.Name = "LOAIPHONG1";
            this.LOAIPHONG1.OptionsColumn.AllowEdit = false;
            this.LOAIPHONG1.OptionsColumn.AllowFocus = false;
            this.LOAIPHONG1.OptionsColumn.FixedWidth = true;
            this.LOAIPHONG1.Visible = true;
            this.LOAIPHONG1.VisibleIndex = 1;
            this.LOAIPHONG1.Width = 114;
            // 
            // PHONG1
            // 
            this.PHONG1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PHONG1.AppearanceHeader.Options.UseFont = true;
            this.PHONG1.AppearanceHeader.Options.UseTextOptions = true;
            this.PHONG1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHONG1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHONG1.Caption = "Phòng";
            this.PHONG1.FieldName = "PHONG1";
            this.PHONG1.Name = "PHONG1";
            this.PHONG1.OptionsColumn.AllowEdit = false;
            this.PHONG1.OptionsColumn.AllowFocus = false;
            this.PHONG1.OptionsColumn.FixedWidth = true;
            this.PHONG1.Visible = true;
            this.PHONG1.VisibleIndex = 2;
            this.PHONG1.Width = 91;
            // 
            // SLKHACH
            // 
            this.SLKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SLKHACH.AppearanceHeader.Options.UseFont = true;
            this.SLKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.SLKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLKHACH.Caption = "SL Khách";
            this.SLKHACH.FieldName = "SLKHACH";
            this.SLKHACH.Name = "SLKHACH";
            this.SLKHACH.OptionsColumn.AllowEdit = false;
            this.SLKHACH.OptionsColumn.AllowFocus = false;
            this.SLKHACH.OptionsColumn.FixedWidth = true;
            this.SLKHACH.Visible = true;
            this.SLKHACH.VisibleIndex = 3;
            this.SLKHACH.Width = 86;
            // 
            // TENKHACH
            // 
            this.TENKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENKHACH.AppearanceHeader.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKHACH.Caption = "Tên Khách";
            this.TENKHACH.FieldName = "TENKHACH";
            this.TENKHACH.Name = "TENKHACH";
            this.TENKHACH.OptionsColumn.AllowEdit = false;
            this.TENKHACH.OptionsColumn.AllowFocus = false;
            this.TENKHACH.OptionsColumn.FixedWidth = true;
            this.TENKHACH.Visible = true;
            this.TENKHACH.VisibleIndex = 4;
            this.TENKHACH.Width = 168;
            // 
            // CMT
            // 
            this.CMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CMT.AppearanceHeader.Options.UseFont = true;
            this.CMT.AppearanceHeader.Options.UseTextOptions = true;
            this.CMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMT.Caption = "CMT/Passport";
            this.CMT.FieldName = "CMT";
            this.CMT.Name = "CMT";
            this.CMT.OptionsColumn.AllowEdit = false;
            this.CMT.OptionsColumn.AllowFocus = false;
            this.CMT.OptionsColumn.FixedWidth = true;
            this.CMT.Visible = true;
            this.CMT.VisibleIndex = 5;
            this.CMT.Width = 173;
            // 
            // NGAYCMT
            // 
            this.NGAYCMT.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYCMT.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMT.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.Caption = "Ngày CMT/Passport";
            this.NGAYCMT.FieldName = "NGAYCMT";
            this.NGAYCMT.Name = "NGAYCMT";
            this.NGAYCMT.OptionsColumn.AllowEdit = false;
            this.NGAYCMT.OptionsColumn.AllowFocus = false;
            this.NGAYCMT.OptionsColumn.FixedWidth = true;
            this.NGAYCMT.Visible = true;
            this.NGAYCMT.VisibleIndex = 6;
            this.NGAYCMT.Width = 172;
            // 
            // VISA
            // 
            this.VISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.VISA.AppearanceCell.Options.UseFont = true;
            this.VISA.AppearanceCell.Options.UseTextOptions = true;
            this.VISA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.VISA.AppearanceHeader.Options.UseFont = true;
            this.VISA.AppearanceHeader.Options.UseTextOptions = true;
            this.VISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.Caption = "Visa";
            this.VISA.FieldName = "VISA";
            this.VISA.Name = "VISA";
            this.VISA.OptionsColumn.AllowEdit = false;
            this.VISA.OptionsColumn.AllowFocus = false;
            this.VISA.OptionsColumn.AllowMove = false;
            this.VISA.Visible = true;
            this.VISA.VisibleIndex = 7;
            this.VISA.Width = 129;
            // 
            // NGAYVISA
            // 
            this.NGAYVISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYVISA.AppearanceCell.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYVISA.AppearanceHeader.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYVISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYVISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYVISA.Caption = "Ngày Visa";
            this.NGAYVISA.FieldName = "NGAYVISA";
            this.NGAYVISA.Name = "NGAYVISA";
            this.NGAYVISA.Visible = true;
            this.NGAYVISA.VisibleIndex = 8;
            this.NGAYVISA.Width = 110;
            // 
            // QUOCTICH
            // 
            this.QUOCTICH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QUOCTICH.AppearanceCell.Options.UseFont = true;
            this.QUOCTICH.AppearanceCell.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QUOCTICH.AppearanceHeader.Options.UseFont = true;
            this.QUOCTICH.AppearanceHeader.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.Caption = "Quốc Tịch";
            this.QUOCTICH.FieldName = "QUOCTICH";
            this.QUOCTICH.Name = "QUOCTICH";
            this.QUOCTICH.Visible = true;
            this.QUOCTICH.VisibleIndex = 9;
            this.QUOCTICH.Width = 120;
            // 
            // panelControl25
            // 
            this.panelControl25.Controls.Add(this.textEdit68);
            this.panelControl25.Controls.Add(this.labelControl38);
            this.panelControl25.Controls.Add(this.textEdit71);
            this.panelControl25.Controls.Add(this.labelControl42);
            this.panelControl25.Location = new System.Drawing.Point(413, 4);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(591, 43);
            this.panelControl25.TabIndex = 50;
            // 
            // textEdit68
            // 
            this.textEdit68.EditValue = "";
            this.textEdit68.EnterMoveNextControl = true;
            this.textEdit68.Location = new System.Drawing.Point(457, 8);
            this.textEdit68.Name = "textEdit68";
            this.textEdit68.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit68.Properties.Appearance.Options.UseFont = true;
            this.textEdit68.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit68.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit68.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit68.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit68.Size = new System.Drawing.Size(124, 26);
            this.textEdit68.TabIndex = 34;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(4, 11);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(103, 19);
            this.labelControl38.TabIndex = 1;
            this.labelControl38.Text = "Tên Khách Đặt";
            // 
            // textEdit71
            // 
            this.textEdit71.EditValue = "";
            this.textEdit71.EnterMoveNextControl = true;
            this.textEdit71.Location = new System.Drawing.Point(114, 8);
            this.textEdit71.Name = "textEdit71";
            this.textEdit71.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit71.Properties.Appearance.Options.UseFont = true;
            this.textEdit71.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit71.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit71.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit71.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit71.Size = new System.Drawing.Size(249, 26);
            this.textEdit71.TabIndex = 19;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(370, 11);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(80, 19);
            this.labelControl42.TabIndex = 21;
            this.labelControl42.Text = "Số Booking";
            // 
            // panelControl24
            // 
            this.panelControl24.Controls.Add(this.textEdit65);
            this.panelControl24.Controls.Add(this.labelControl35);
            this.panelControl24.Controls.Add(this.textEdit69);
            this.panelControl24.Controls.Add(this.labelControl39);
            this.panelControl24.Controls.Add(this.textEdit70);
            this.panelControl24.Controls.Add(this.labelControl40);
            this.panelControl24.Controls.Add(this.textEdit74);
            this.panelControl24.Controls.Add(this.labelControl41);
            this.panelControl24.Location = new System.Drawing.Point(5, 338);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(398, 122);
            this.panelControl24.TabIndex = 49;
            // 
            // textEdit65
            // 
            this.textEdit65.EditValue = "";
            this.textEdit65.EnterMoveNextControl = true;
            this.textEdit65.Location = new System.Drawing.Point(94, 89);
            this.textEdit65.Name = "textEdit65";
            this.textEdit65.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit65.Properties.Appearance.Options.UseFont = true;
            this.textEdit65.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit65.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit65.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit65.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit65.Size = new System.Drawing.Size(108, 26);
            this.textEdit65.TabIndex = 38;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(6, 92);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(53, 19);
            this.labelControl35.TabIndex = 37;
            this.labelControl35.Text = "Còn Lại";
            // 
            // textEdit69
            // 
            this.textEdit69.EditValue = "";
            this.textEdit69.EnterMoveNextControl = true;
            this.textEdit69.Location = new System.Drawing.Point(290, 39);
            this.textEdit69.Name = "textEdit69";
            this.textEdit69.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit69.Properties.Appearance.Options.UseFont = true;
            this.textEdit69.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit69.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit69.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit69.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit69.Size = new System.Drawing.Size(102, 40);
            this.textEdit69.TabIndex = 36;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(208, 51);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(79, 19);
            this.labelControl39.TabIndex = 35;
            this.labelControl39.Text = "Tổng Cộng";
            // 
            // textEdit70
            // 
            this.textEdit70.EditValue = "";
            this.textEdit70.EnterMoveNextControl = true;
            this.textEdit70.Location = new System.Drawing.Point(94, 48);
            this.textEdit70.Name = "textEdit70";
            this.textEdit70.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit70.Properties.Appearance.Options.UseFont = true;
            this.textEdit70.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit70.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit70.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit70.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit70.Size = new System.Drawing.Size(108, 26);
            this.textEdit70.TabIndex = 34;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(6, 10);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(81, 19);
            this.labelControl40.TabIndex = 1;
            this.labelControl40.Text = "Tiền Phòng";
            // 
            // textEdit74
            // 
            this.textEdit74.EditValue = "";
            this.textEdit74.EnterMoveNextControl = true;
            this.textEdit74.Location = new System.Drawing.Point(94, 7);
            this.textEdit74.Name = "textEdit74";
            this.textEdit74.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit74.Properties.Appearance.Options.UseFont = true;
            this.textEdit74.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit74.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit74.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit74.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit74.Size = new System.Drawing.Size(108, 26);
            this.textEdit74.TabIndex = 19;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(6, 49);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(86, 19);
            this.labelControl41.TabIndex = 21;
            this.labelControl41.Text = "Thanh Toán";
            // 
            // panelControl23
            // 
            this.panelControl23.Controls.Add(this.dtp_ngayden);
            this.panelControl23.Controls.Add(this.dtp_ngaydi);
            this.panelControl23.Controls.Add(this.textEdit64);
            this.panelControl23.Controls.Add(this.labelControl34);
            this.panelControl23.Controls.Add(this.textEdit63);
            this.panelControl23.Controls.Add(this.textEdit62);
            this.panelControl23.Controls.Add(this.labelControl33);
            this.panelControl23.Controls.Add(this.textEdit61);
            this.panelControl23.Controls.Add(this.labelControl32);
            this.panelControl23.Controls.Add(this.labelControl31);
            this.panelControl23.Controls.Add(this.textEdit60);
            this.panelControl23.Controls.Add(this.labelControl30);
            this.panelControl23.Controls.Add(this.textEdit58);
            this.panelControl23.Controls.Add(this.textEdit57);
            this.panelControl23.Controls.Add(this.textEdit56);
            this.panelControl23.Controls.Add(this.labelControl23);
            this.panelControl23.Controls.Add(this.labelControl26);
            this.panelControl23.Controls.Add(this.labelControl27);
            this.panelControl23.Controls.Add(this.labelControl28);
            this.panelControl23.Controls.Add(this.labelControl29);
            this.panelControl23.Location = new System.Drawing.Point(5, 4);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(398, 328);
            this.panelControl23.TabIndex = 48;
            // 
            // dtp_ngayden
            // 
            this.dtp_ngayden.EditValue = null;
            this.dtp_ngayden.EnterMoveNextControl = true;
            this.dtp_ngayden.Location = new System.Drawing.Point(94, 5);
            this.dtp_ngayden.Name = "dtp_ngayden";
            this.dtp_ngayden.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngayden.Properties.Appearance.Options.UseFont = true;
            this.dtp_ngayden.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngayden.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtp_ngayden.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_ngayden.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngayden.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngayden.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp_ngayden.Size = new System.Drawing.Size(108, 26);
            this.dtp_ngayden.TabIndex = 45;
            // 
            // dtp_ngaydi
            // 
            this.dtp_ngaydi.EditValue = null;
            this.dtp_ngaydi.EnterMoveNextControl = true;
            this.dtp_ngaydi.Location = new System.Drawing.Point(94, 50);
            this.dtp_ngaydi.Name = "dtp_ngaydi";
            this.dtp_ngaydi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaydi.Properties.Appearance.Options.UseFont = true;
            this.dtp_ngaydi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaydi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtp_ngaydi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_ngaydi.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngaydi.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngaydi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp_ngaydi.Size = new System.Drawing.Size(108, 26);
            this.dtp_ngaydi.TabIndex = 46;
            // 
            // textEdit64
            // 
            this.textEdit64.EditValue = "";
            this.textEdit64.EnterMoveNextControl = true;
            this.textEdit64.Location = new System.Drawing.Point(94, 275);
            this.textEdit64.Name = "textEdit64";
            this.textEdit64.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit64.Properties.Appearance.Options.UseFont = true;
            this.textEdit64.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit64.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit64.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit64.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit64.Size = new System.Drawing.Size(298, 26);
            this.textEdit64.TabIndex = 44;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(6, 278);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(57, 19);
            this.labelControl34.TabIndex = 43;
            this.labelControl34.Text = "Ghi Chú";
            // 
            // textEdit63
            // 
            this.textEdit63.EditValue = "";
            this.textEdit63.EnterMoveNextControl = true;
            this.textEdit63.Location = new System.Drawing.Point(280, 200);
            this.textEdit63.Name = "textEdit63";
            this.textEdit63.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit63.Properties.Appearance.Options.UseFont = true;
            this.textEdit63.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit63.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit63.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit63.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit63.Size = new System.Drawing.Size(112, 40);
            this.textEdit63.TabIndex = 42;
            // 
            // textEdit62
            // 
            this.textEdit62.EditValue = "";
            this.textEdit62.EnterMoveNextControl = true;
            this.textEdit62.Location = new System.Drawing.Point(280, 110);
            this.textEdit62.Name = "textEdit62";
            this.textEdit62.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit62.Properties.Appearance.Options.UseFont = true;
            this.textEdit62.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit62.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit62.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit62.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit62.Size = new System.Drawing.Size(112, 40);
            this.textEdit62.TabIndex = 41;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(212, 200);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(60, 38);
            this.labelControl33.TabIndex = 40;
            this.labelControl33.Text = "Tổng Số\r\n  Khách";
            // 
            // textEdit61
            // 
            this.textEdit61.EditValue = "";
            this.textEdit61.EnterMoveNextControl = true;
            this.textEdit61.Location = new System.Drawing.Point(94, 230);
            this.textEdit61.Name = "textEdit61";
            this.textEdit61.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit61.Properties.Appearance.Options.UseFont = true;
            this.textEdit61.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit61.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit61.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit61.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit61.Size = new System.Drawing.Size(108, 26);
            this.textEdit61.TabIndex = 39;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(6, 233);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(52, 19);
            this.labelControl32.TabIndex = 38;
            this.labelControl32.Text = "Trẻ Em";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(212, 110);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(60, 38);
            this.labelControl31.TabIndex = 37;
            this.labelControl31.Text = "Tổng Số\r\n  Phòng";
            // 
            // textEdit60
            // 
            this.textEdit60.EditValue = "";
            this.textEdit60.EnterMoveNextControl = true;
            this.textEdit60.Location = new System.Drawing.Point(280, 20);
            this.textEdit60.Name = "textEdit60";
            this.textEdit60.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit60.Properties.Appearance.Options.UseFont = true;
            this.textEdit60.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit60.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit60.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit60.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit60.Size = new System.Drawing.Size(112, 40);
            this.textEdit60.TabIndex = 36;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(212, 19);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(57, 38);
            this.labelControl30.TabIndex = 35;
            this.labelControl30.Text = "Số ngày\r\n lưu trú";
            // 
            // textEdit58
            // 
            this.textEdit58.EditValue = "";
            this.textEdit58.EnterMoveNextControl = true;
            this.textEdit58.Location = new System.Drawing.Point(94, 185);
            this.textEdit58.Name = "textEdit58";
            this.textEdit58.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit58.Properties.Appearance.Options.UseFont = true;
            this.textEdit58.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit58.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit58.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit58.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit58.Size = new System.Drawing.Size(108, 26);
            this.textEdit58.TabIndex = 33;
            // 
            // textEdit57
            // 
            this.textEdit57.EditValue = "";
            this.textEdit57.EnterMoveNextControl = true;
            this.textEdit57.Location = new System.Drawing.Point(94, 140);
            this.textEdit57.Name = "textEdit57";
            this.textEdit57.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit57.Properties.Appearance.Options.UseFont = true;
            this.textEdit57.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit57.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit57.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit57.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit57.Size = new System.Drawing.Size(108, 26);
            this.textEdit57.TabIndex = 32;
            // 
            // textEdit56
            // 
            this.textEdit56.EditValue = "";
            this.textEdit56.EnterMoveNextControl = true;
            this.textEdit56.Location = new System.Drawing.Point(94, 95);
            this.textEdit56.Name = "textEdit56";
            this.textEdit56.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit56.Properties.Appearance.Options.UseFont = true;
            this.textEdit56.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit56.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit56.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit56.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit56.Size = new System.Drawing.Size(108, 26);
            this.textEdit56.TabIndex = 31;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(6, 8);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(67, 19);
            this.labelControl23.TabIndex = 1;
            this.labelControl23.Text = "Ngày đến";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Location = new System.Drawing.Point(6, 53);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(54, 19);
            this.labelControl26.TabIndex = 21;
            this.labelControl26.Text = "Ngày đi";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(6, 98);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(79, 19);
            this.labelControl27.TabIndex = 24;
            this.labelControl27.Text = "Loại Phòng";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(6, 143);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(68, 19);
            this.labelControl28.TabIndex = 27;
            this.labelControl28.Text = "Số Phòng";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(6, 188);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(74, 19);
            this.labelControl29.TabIndex = 30;
            this.labelControl29.Text = "Người Lớn";
            // 
            // Tabsub_Booking_DSBooking
            // 
            this.Tabsub_Booking_DSBooking.Controls.Add(this.gridControl4);
            this.Tabsub_Booking_DSBooking.Controls.Add(this.panelControl26);
            this.Tabsub_Booking_DSBooking.Name = "Tabsub_Booking_DSBooking";
            this.Tabsub_Booking_DSBooking.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_Booking_DSBooking.Text = "Booking_DS";
            // 
            // gridControl4
            // 
            this.gridControl4.AllowDrop = true;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode4.RelationName = "Level1";
            this.gridControl4.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode4});
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(971, 467);
            this.gridControl4.TabIndex = 7;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView4.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView4.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView4.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView4.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView4.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView4.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView4.Appearance.GroupRow.Options.UseFont = true;
            this.gridView4.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView4.Appearance.Row.Options.UseFont = true;
            this.gridView4.ColumnPanelRowHeight = 30;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31});
            this.gridView4.FooterPanelHeight = 30;
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.RowHeight = 30;
            this.gridView4.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "STT";
            this.gridColumn2.FieldName = "TONGSOPHONG";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 49;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Số Booking";
            this.gridColumn9.FieldName = "LOAIPHONG";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.Caption = "Tên KH";
            this.gridColumn10.FieldName = "TEMP1";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 163;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Số ĐT";
            this.gridColumn11.FieldName = "TEMP2";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 113;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn12.AppearanceCell.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Loại phòng";
            this.gridColumn12.FieldName = "TEMP3";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.FixedWidth = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            this.gridColumn12.Width = 121;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn13.AppearanceCell.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Số phòng";
            this.gridColumn13.FieldName = "TEMP4";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.FixedWidth = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 87;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn14.AppearanceCell.Options.UseFont = true;
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Số khách";
            this.gridColumn14.FieldName = "TEMP5";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.FixedWidth = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 6;
            this.gridColumn14.Width = 85;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn15.AppearanceCell.Options.UseFont = true;
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.Caption = "Ngày đến";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.AllowMove = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 7;
            this.gridColumn15.Width = 107;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn24.AppearanceCell.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn24.AppearanceHeader.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn24.Caption = "Ngày đi";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 8;
            this.gridColumn24.Width = 85;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn25.AppearanceCell.Options.UseFont = true;
            this.gridColumn25.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn25.AppearanceHeader.Options.UseFont = true;
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn25.Caption = "Số Ngày Lưu Trú";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 9;
            this.gridColumn25.Width = 149;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn26.AppearanceCell.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn26.AppearanceHeader.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn26.Caption = "CMT/Passport";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 10;
            this.gridColumn26.Width = 132;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn27.AppearanceCell.Options.UseFont = true;
            this.gridColumn27.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn27.AppearanceHeader.Options.UseFont = true;
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn27.Caption = "Ngày CMT/Passport";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 11;
            this.gridColumn27.Width = 180;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn28.AppearanceCell.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn28.AppearanceHeader.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn28.Caption = "Visa";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 12;
            this.gridColumn28.Width = 121;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn29.AppearanceCell.Options.UseFont = true;
            this.gridColumn29.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn29.AppearanceHeader.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn29.Caption = "Ngày Visa";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 13;
            this.gridColumn29.Width = 103;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn30.AppearanceCell.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn30.AppearanceHeader.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn30.Caption = "Quốc Tịch";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 14;
            this.gridColumn30.Width = 117;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn31.AppearanceCell.Options.UseFont = true;
            this.gridColumn31.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn31.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn31.AppearanceHeader.Options.UseFont = true;
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn31.Caption = "Tổng Tiền";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 15;
            this.gridColumn31.Width = 148;
            // 
            // panelControl26
            // 
            this.panelControl26.Controls.Add(this.panelControl27);
            this.panelControl26.Controls.Add(this.simpleButton16);
            this.panelControl26.Controls.Add(this.simpleButton21);
            this.panelControl26.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl26.Location = new System.Drawing.Point(971, 0);
            this.panelControl26.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl26.Name = "panelControl26";
            this.panelControl26.Size = new System.Drawing.Size(39, 467);
            this.panelControl26.TabIndex = 8;
            // 
            // panelControl27
            // 
            this.panelControl27.AutoSize = true;
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.panelControl28);
            this.panelControl27.Controls.Add(this.simpleButton5);
            this.panelControl27.Controls.Add(this.simpleButton11);
            this.panelControl27.Controls.Add(this.simpleButton12);
            this.panelControl27.Controls.Add(this.simpleButton14);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl27.Location = new System.Drawing.Point(2, 82);
            this.panelControl27.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(35, 303);
            this.panelControl27.TabIndex = 14;
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl28.Location = new System.Drawing.Point(0, 160);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(35, -17);
            this.panelControl28.TabIndex = 15;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton5.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.Image")));
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton5.Location = new System.Drawing.Point(0, 143);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(35, 80);
            this.simpleButton5.TabIndex = 12;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton11.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton11.Image")));
            this.simpleButton11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton11.Location = new System.Drawing.Point(0, 223);
            this.simpleButton11.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(35, 80);
            this.simpleButton11.TabIndex = 1;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton12.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton12.Image")));
            this.simpleButton12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton12.Location = new System.Drawing.Point(0, 80);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(35, 80);
            this.simpleButton12.TabIndex = 13;
            // 
            // simpleButton14
            // 
            this.simpleButton14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton14.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton14.Image")));
            this.simpleButton14.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton14.Location = new System.Drawing.Point(0, 0);
            this.simpleButton14.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(35, 80);
            this.simpleButton14.TabIndex = 11;
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton16.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton16.Image")));
            this.simpleButton16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton16.Location = new System.Drawing.Point(2, 385);
            this.simpleButton16.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(35, 80);
            this.simpleButton16.TabIndex = 9;
            // 
            // simpleButton21
            // 
            this.simpleButton21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton21.Appearance.Options.UseFont = true;
            this.simpleButton21.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton21.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton21.Image")));
            this.simpleButton21.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton21.Location = new System.Drawing.Point(2, 2);
            this.simpleButton21.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(35, 80);
            this.simpleButton21.TabIndex = 0;
            // 
            // Tabsub_Booking_LSLuutru
            // 
            this.Tabsub_Booking_LSLuutru.Controls.Add(this.panelControl33);
            this.Tabsub_Booking_LSLuutru.Controls.Add(this.palBook_LSluutru_Top);
            this.Tabsub_Booking_LSLuutru.Name = "Tabsub_Booking_LSLuutru";
            this.Tabsub_Booking_LSLuutru.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_Booking_LSLuutru.Text = "Booking_LSLuutru";
            // 
            // panelControl33
            // 
            this.panelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl33.Controls.Add(this.gridControl5);
            this.panelControl33.Controls.Add(this.panelControl29);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl33.Location = new System.Drawing.Point(0, 54);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(1010, 413);
            this.panelControl33.TabIndex = 22;
            // 
            // gridControl5
            // 
            this.gridControl5.AllowDrop = true;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode5.RelationName = "Level1";
            this.gridControl5.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode5});
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(971, 413);
            this.gridControl5.TabIndex = 11;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView5.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView5.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView5.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView5.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView5.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.GroupRow.Options.UseFont = true;
            this.gridView5.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.Row.Options.UseFont = true;
            this.gridView5.ColumnPanelRowHeight = 30;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn40,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39});
            this.gridView5.FooterPanelHeight = 30;
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.RowHeight = 30;
            this.gridView5.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn16.AppearanceCell.Options.UseFont = true;
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn16.Caption = "STT";
            this.gridColumn16.FieldName = "TONGSOPHONG";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.AllowMove = false;
            this.gridColumn16.OptionsColumn.FixedWidth = true;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 49;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn17.AppearanceCell.Options.UseFont = true;
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Số Booking";
            this.gridColumn17.FieldName = "LOAIPHONG";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.FixedWidth = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            this.gridColumn17.Width = 101;
            // 
            // gridColumn40
            // 
            this.gridColumn40.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn40.AppearanceCell.Options.UseFont = true;
            this.gridColumn40.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn40.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn40.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn40.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn40.AppearanceHeader.Options.UseFont = true;
            this.gridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn40.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn40.Caption = "Ngày";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 2;
            this.gridColumn40.Width = 127;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn19.AppearanceCell.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "Số ĐT";
            this.gridColumn19.FieldName = "TEMP2";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.FixedWidth = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            this.gridColumn19.Width = 113;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn20.AppearanceCell.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Loại phòng";
            this.gridColumn20.FieldName = "TEMP3";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.FixedWidth = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            this.gridColumn20.Width = 121;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn21.AppearanceCell.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn21.AppearanceHeader.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "Số phòng";
            this.gridColumn21.FieldName = "TEMP4";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.FixedWidth = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            this.gridColumn21.Width = 87;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn22.AppearanceCell.Options.UseFont = true;
            this.gridColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "Số khách";
            this.gridColumn22.FieldName = "TEMP5";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.FixedWidth = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 6;
            this.gridColumn22.Width = 85;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn23.AppearanceCell.Options.UseFont = true;
            this.gridColumn23.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn23.AppearanceHeader.Options.UseFont = true;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.Caption = "Ngày đến";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.AllowMove = false;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 7;
            this.gridColumn23.Width = 107;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn32.AppearanceCell.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn32.AppearanceHeader.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn32.Caption = "Ngày đi";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 8;
            this.gridColumn32.Width = 99;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn33.AppearanceCell.Options.UseFont = true;
            this.gridColumn33.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn33.AppearanceHeader.Options.UseFont = true;
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.Caption = "Số Ngày Lưu Trú";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 9;
            this.gridColumn33.Width = 152;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn34.AppearanceCell.Options.UseFont = true;
            this.gridColumn34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn34.AppearanceHeader.Options.UseFont = true;
            this.gridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn34.Caption = "CMT/Passport";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 10;
            this.gridColumn34.Width = 132;
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn35.AppearanceCell.Options.UseFont = true;
            this.gridColumn35.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn35.AppearanceHeader.Options.UseFont = true;
            this.gridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.Caption = "Ngày CMT/Passport";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 11;
            this.gridColumn35.Width = 180;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn36.AppearanceCell.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn36.AppearanceHeader.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn36.Caption = "Visa";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 12;
            this.gridColumn36.Width = 121;
            // 
            // gridColumn37
            // 
            this.gridColumn37.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn37.AppearanceCell.Options.UseFont = true;
            this.gridColumn37.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn37.AppearanceHeader.Options.UseFont = true;
            this.gridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.Caption = "Ngày Visa";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 13;
            this.gridColumn37.Width = 103;
            // 
            // gridColumn38
            // 
            this.gridColumn38.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn38.AppearanceCell.Options.UseFont = true;
            this.gridColumn38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn38.AppearanceHeader.Options.UseFont = true;
            this.gridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn38.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn38.Caption = "Quốc Tịch";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 14;
            this.gridColumn38.Width = 117;
            // 
            // gridColumn39
            // 
            this.gridColumn39.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn39.AppearanceCell.Options.UseFont = true;
            this.gridColumn39.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn39.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn39.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn39.AppearanceHeader.Options.UseFont = true;
            this.gridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn39.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn39.Caption = "Tổng Tiền";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 15;
            this.gridColumn39.Width = 148;
            // 
            // panelControl29
            // 
            this.panelControl29.Controls.Add(this.panelControl30);
            this.panelControl29.Controls.Add(this.simpleButton55);
            this.panelControl29.Controls.Add(this.simpleButton56);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl29.Location = new System.Drawing.Point(971, 0);
            this.panelControl29.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(39, 413);
            this.panelControl29.TabIndex = 12;
            // 
            // panelControl30
            // 
            this.panelControl30.AutoSize = true;
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.panelControl31);
            this.panelControl30.Controls.Add(this.simpleButton17);
            this.panelControl30.Controls.Add(this.simpleButton18);
            this.panelControl30.Controls.Add(this.simpleButton53);
            this.panelControl30.Controls.Add(this.simpleButton54);
            this.panelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl30.Location = new System.Drawing.Point(2, 82);
            this.panelControl30.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(35, 249);
            this.panelControl30.TabIndex = 14;
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl31.Location = new System.Drawing.Point(0, 160);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(35, -71);
            this.panelControl31.TabIndex = 15;
            // 
            // simpleButton17
            // 
            this.simpleButton17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton17.Appearance.Options.UseFont = true;
            this.simpleButton17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton17.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton17.Image")));
            this.simpleButton17.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton17.Location = new System.Drawing.Point(0, 89);
            this.simpleButton17.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(35, 80);
            this.simpleButton17.TabIndex = 12;
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton18.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton18.Image")));
            this.simpleButton18.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton18.Location = new System.Drawing.Point(0, 169);
            this.simpleButton18.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(35, 80);
            this.simpleButton18.TabIndex = 1;
            // 
            // simpleButton53
            // 
            this.simpleButton53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton53.Appearance.Options.UseFont = true;
            this.simpleButton53.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton53.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton53.Image")));
            this.simpleButton53.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton53.Location = new System.Drawing.Point(0, 80);
            this.simpleButton53.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton53.Name = "simpleButton53";
            this.simpleButton53.Size = new System.Drawing.Size(35, 80);
            this.simpleButton53.TabIndex = 13;
            // 
            // simpleButton54
            // 
            this.simpleButton54.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton54.Appearance.Options.UseFont = true;
            this.simpleButton54.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton54.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton54.Image")));
            this.simpleButton54.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton54.Location = new System.Drawing.Point(0, 0);
            this.simpleButton54.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton54.Name = "simpleButton54";
            this.simpleButton54.Size = new System.Drawing.Size(35, 80);
            this.simpleButton54.TabIndex = 11;
            // 
            // simpleButton55
            // 
            this.simpleButton55.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton55.Appearance.Options.UseFont = true;
            this.simpleButton55.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton55.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton55.Image")));
            this.simpleButton55.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton55.Location = new System.Drawing.Point(2, 331);
            this.simpleButton55.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton55.Name = "simpleButton55";
            this.simpleButton55.Size = new System.Drawing.Size(35, 80);
            this.simpleButton55.TabIndex = 9;
            // 
            // simpleButton56
            // 
            this.simpleButton56.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton56.Appearance.Options.UseFont = true;
            this.simpleButton56.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton56.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton56.Image")));
            this.simpleButton56.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton56.Location = new System.Drawing.Point(2, 2);
            this.simpleButton56.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton56.Name = "simpleButton56";
            this.simpleButton56.Size = new System.Drawing.Size(35, 80);
            this.simpleButton56.TabIndex = 0;
            // 
            // palBook_LSluutru_Top
            // 
            this.palBook_LSluutru_Top.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palBook_LSluutru_Top.Controls.Add(this.pal_Book_LSluutru_Search);
            this.palBook_LSluutru_Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.palBook_LSluutru_Top.Location = new System.Drawing.Point(0, 0);
            this.palBook_LSluutru_Top.Name = "palBook_LSluutru_Top";
            this.palBook_LSluutru_Top.Size = new System.Drawing.Size(1010, 54);
            this.palBook_LSluutru_Top.TabIndex = 21;
            // 
            // pal_Book_LSluutru_Search
            // 
            this.pal_Book_LSluutru_Search.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_Book_LSluutru_Search.Controls.Add(this.simpleButton57);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit82);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl55);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit80);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl53);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit81);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl54);
            this.pal_Book_LSluutru_Search.Location = new System.Drawing.Point(23, 6);
            this.pal_Book_LSluutru_Search.Name = "pal_Book_LSluutru_Search";
            this.pal_Book_LSluutru_Search.Size = new System.Drawing.Size(970, 43);
            this.pal_Book_LSluutru_Search.TabIndex = 47;
            // 
            // simpleButton57
            // 
            this.simpleButton57.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton57.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton57.Appearance.Options.UseBackColor = true;
            this.simpleButton57.Appearance.Options.UseFont = true;
            this.simpleButton57.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton57.Location = new System.Drawing.Point(839, 3);
            this.simpleButton57.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton57.Name = "simpleButton57";
            this.simpleButton57.Size = new System.Drawing.Size(108, 38);
            this.simpleButton57.TabIndex = 37;
            this.simpleButton57.Text = "Tìm kiếm";
            // 
            // textEdit82
            // 
            this.textEdit82.EditValue = "";
            this.textEdit82.EnterMoveNextControl = true;
            this.textEdit82.Location = new System.Drawing.Point(673, 8);
            this.textEdit82.Name = "textEdit82";
            this.textEdit82.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit82.Properties.Appearance.Options.UseFont = true;
            this.textEdit82.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit82.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit82.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit82.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit82.Size = new System.Drawing.Size(144, 26);
            this.textEdit82.TabIndex = 36;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Location = new System.Drawing.Point(573, 11);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(93, 19);
            this.labelControl55.TabIndex = 35;
            this.labelControl55.Text = "Số điện thoại";
            // 
            // textEdit80
            // 
            this.textEdit80.EditValue = "";
            this.textEdit80.EnterMoveNextControl = true;
            this.textEdit80.Location = new System.Drawing.Point(355, 8);
            this.textEdit80.Name = "textEdit80";
            this.textEdit80.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.Appearance.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit80.Size = new System.Drawing.Size(211, 26);
            this.textEdit80.TabIndex = 34;
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(4, 11);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(109, 19);
            this.labelControl53.TabIndex = 1;
            this.labelControl53.Text = "Mã Khách Hàng";
            // 
            // textEdit81
            // 
            this.textEdit81.EditValue = "";
            this.textEdit81.EnterMoveNextControl = true;
            this.textEdit81.Location = new System.Drawing.Point(120, 8);
            this.textEdit81.Name = "textEdit81";
            this.textEdit81.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit81.Properties.Appearance.Options.UseFont = true;
            this.textEdit81.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit81.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit81.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit81.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit81.Size = new System.Drawing.Size(103, 26);
            this.textEdit81.TabIndex = 19;
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(230, 11);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(116, 19);
            this.labelControl54.TabIndex = 21;
            this.labelControl54.Text = "Tên Khách Hàng";
            // 
            // Tabsub_Booking_Daily
            // 
            this.Tabsub_Booking_Daily.Controls.Add(this.panelControl32);
            this.Tabsub_Booking_Daily.Name = "Tabsub_Booking_Daily";
            this.Tabsub_Booking_Daily.Size = new System.Drawing.Size(1010, 467);
            this.Tabsub_Booking_Daily.Text = "Booking_Daily";
            // 
            // panelControl32
            // 
            this.panelControl32.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl32.Controls.Add(this.textEdit96);
            this.panelControl32.Controls.Add(this.labelControl69);
            this.panelControl32.Controls.Add(this.textEdit93);
            this.panelControl32.Controls.Add(this.labelControl66);
            this.panelControl32.Controls.Add(this.textEdit94);
            this.panelControl32.Controls.Add(this.labelControl67);
            this.panelControl32.Controls.Add(this.textEdit95);
            this.panelControl32.Controls.Add(this.labelControl68);
            this.panelControl32.Controls.Add(this.textEdit87);
            this.panelControl32.Controls.Add(this.labelControl57);
            this.panelControl32.Controls.Add(this.textEdit91);
            this.panelControl32.Controls.Add(this.labelControl59);
            this.panelControl32.Controls.Add(this.textEdit92);
            this.panelControl32.Controls.Add(this.labelControl60);
            this.panelControl32.Controls.Add(this.textEdit84);
            this.panelControl32.Controls.Add(this.textEdit85);
            this.panelControl32.Controls.Add(this.textEdit83);
            this.panelControl32.Controls.Add(this.labelControl56);
            this.panelControl32.Controls.Add(this.textEdit86);
            this.panelControl32.Controls.Add(this.labelControl58);
            this.panelControl32.Controls.Add(this.textEdit88);
            this.panelControl32.Controls.Add(this.textEdit89);
            this.panelControl32.Controls.Add(this.textEdit90);
            this.panelControl32.Controls.Add(this.labelControl61);
            this.panelControl32.Controls.Add(this.labelControl62);
            this.panelControl32.Controls.Add(this.labelControl63);
            this.panelControl32.Controls.Add(this.labelControl64);
            this.panelControl32.Controls.Add(this.labelControl65);
            this.panelControl32.Location = new System.Drawing.Point(13, 42);
            this.panelControl32.Name = "panelControl32";
            this.panelControl32.Size = new System.Drawing.Size(986, 354);
            this.panelControl32.TabIndex = 21;
            // 
            // textEdit96
            // 
            this.textEdit96.EditValue = "";
            this.textEdit96.EnterMoveNextControl = true;
            this.textEdit96.Location = new System.Drawing.Point(705, 294);
            this.textEdit96.Name = "textEdit96";
            this.textEdit96.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit96.Properties.Appearance.Options.UseFont = true;
            this.textEdit96.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit96.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit96.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit96.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit96.Size = new System.Drawing.Size(232, 26);
            this.textEdit96.TabIndex = 60;
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl69.Location = new System.Drawing.Point(521, 295);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(150, 19);
            this.labelControl69.TabIndex = 59;
            this.labelControl69.Text = "Phần trăm chiết khấu";
            // 
            // textEdit93
            // 
            this.textEdit93.EditValue = "";
            this.textEdit93.EnterMoveNextControl = true;
            this.textEdit93.Location = new System.Drawing.Point(705, 249);
            this.textEdit93.Name = "textEdit93";
            this.textEdit93.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit93.Properties.Appearance.Options.UseFont = true;
            this.textEdit93.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit93.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit93.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit93.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit93.Size = new System.Drawing.Size(232, 26);
            this.textEdit93.TabIndex = 58;
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl66.Location = new System.Drawing.Point(521, 250);
            this.labelControl66.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(149, 19);
            this.labelControl66.TabIndex = 57;
            this.labelControl66.Text = "Chính sách hoa hồng";
            // 
            // textEdit94
            // 
            this.textEdit94.EditValue = "";
            this.textEdit94.EnterMoveNextControl = true;
            this.textEdit94.Location = new System.Drawing.Point(705, 204);
            this.textEdit94.Name = "textEdit94";
            this.textEdit94.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit94.Properties.Appearance.Options.UseFont = true;
            this.textEdit94.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit94.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit94.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit94.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit94.Size = new System.Drawing.Size(232, 26);
            this.textEdit94.TabIndex = 56;
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl67.Location = new System.Drawing.Point(521, 205);
            this.labelControl67.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(170, 19);
            this.labelControl67.TabIndex = 55;
            this.labelControl67.Text = "Điện thoại người liên hệ";
            // 
            // textEdit95
            // 
            this.textEdit95.EditValue = "";
            this.textEdit95.EnterMoveNextControl = true;
            this.textEdit95.Location = new System.Drawing.Point(705, 159);
            this.textEdit95.Name = "textEdit95";
            this.textEdit95.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit95.Properties.Appearance.Options.UseFont = true;
            this.textEdit95.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit95.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit95.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit95.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit95.Size = new System.Drawing.Size(232, 26);
            this.textEdit95.TabIndex = 54;
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl68.Location = new System.Drawing.Point(521, 160);
            this.labelControl68.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(95, 19);
            this.labelControl68.TabIndex = 53;
            this.labelControl68.Text = "Người liên hệ";
            // 
            // textEdit87
            // 
            this.textEdit87.EditValue = "";
            this.textEdit87.EnterMoveNextControl = true;
            this.textEdit87.Location = new System.Drawing.Point(705, 114);
            this.textEdit87.Name = "textEdit87";
            this.textEdit87.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit87.Properties.Appearance.Options.UseFont = true;
            this.textEdit87.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit87.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit87.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit87.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit87.Size = new System.Drawing.Size(232, 26);
            this.textEdit87.TabIndex = 52;
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(521, 115);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(39, 19);
            this.labelControl57.TabIndex = 51;
            this.labelControl57.Text = "Email";
            // 
            // textEdit91
            // 
            this.textEdit91.EditValue = "";
            this.textEdit91.EnterMoveNextControl = true;
            this.textEdit91.Location = new System.Drawing.Point(705, 69);
            this.textEdit91.Name = "textEdit91";
            this.textEdit91.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit91.Properties.Appearance.Options.UseFont = true;
            this.textEdit91.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit91.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit91.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit91.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit91.Size = new System.Drawing.Size(232, 26);
            this.textEdit91.TabIndex = 50;
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Location = new System.Drawing.Point(521, 70);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(24, 19);
            this.labelControl59.TabIndex = 49;
            this.labelControl59.Text = "Fax";
            // 
            // textEdit92
            // 
            this.textEdit92.EditValue = "";
            this.textEdit92.EnterMoveNextControl = true;
            this.textEdit92.Location = new System.Drawing.Point(705, 24);
            this.textEdit92.Name = "textEdit92";
            this.textEdit92.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit92.Properties.Appearance.Options.UseFont = true;
            this.textEdit92.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit92.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit92.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit92.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit92.Size = new System.Drawing.Size(232, 26);
            this.textEdit92.TabIndex = 48;
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Location = new System.Drawing.Point(521, 25);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(72, 19);
            this.labelControl60.TabIndex = 47;
            this.labelControl60.Text = "Điện thoại";
            // 
            // textEdit84
            // 
            this.textEdit84.EditValue = "";
            this.textEdit84.EnterMoveNextControl = true;
            this.textEdit84.Location = new System.Drawing.Point(154, 69);
            this.textEdit84.Name = "textEdit84";
            this.textEdit84.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit84.Properties.Appearance.Options.UseFont = true;
            this.textEdit84.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit84.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit84.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit84.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit84.Size = new System.Drawing.Size(232, 26);
            this.textEdit84.TabIndex = 46;
            // 
            // textEdit85
            // 
            this.textEdit85.EditValue = "";
            this.textEdit85.EnterMoveNextControl = true;
            this.textEdit85.Location = new System.Drawing.Point(154, 24);
            this.textEdit85.Name = "textEdit85";
            this.textEdit85.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit85.Properties.Appearance.Options.UseFont = true;
            this.textEdit85.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit85.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit85.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit85.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit85.Size = new System.Drawing.Size(232, 26);
            this.textEdit85.TabIndex = 45;
            // 
            // textEdit83
            // 
            this.textEdit83.EditValue = "";
            this.textEdit83.EnterMoveNextControl = true;
            this.textEdit83.Location = new System.Drawing.Point(154, 294);
            this.textEdit83.Name = "textEdit83";
            this.textEdit83.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit83.Properties.Appearance.Options.UseFont = true;
            this.textEdit83.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit83.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit83.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit83.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit83.Size = new System.Drawing.Size(232, 26);
            this.textEdit83.TabIndex = 44;
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Location = new System.Drawing.Point(17, 295);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(65, 19);
            this.labelControl56.TabIndex = 43;
            this.labelControl56.Text = "Quốc Gia";
            // 
            // textEdit86
            // 
            this.textEdit86.EditValue = "";
            this.textEdit86.EnterMoveNextControl = true;
            this.textEdit86.Location = new System.Drawing.Point(154, 249);
            this.textEdit86.Name = "textEdit86";
            this.textEdit86.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit86.Properties.Appearance.Options.UseFont = true;
            this.textEdit86.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit86.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit86.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit86.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit86.Size = new System.Drawing.Size(232, 26);
            this.textEdit86.TabIndex = 39;
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Location = new System.Drawing.Point(17, 250);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(125, 19);
            this.labelControl58.TabIndex = 38;
            this.labelControl58.Text = "Thành phố / Tỉnh";
            // 
            // textEdit88
            // 
            this.textEdit88.EditValue = "";
            this.textEdit88.EnterMoveNextControl = true;
            this.textEdit88.Location = new System.Drawing.Point(154, 204);
            this.textEdit88.Name = "textEdit88";
            this.textEdit88.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit88.Properties.Appearance.Options.UseFont = true;
            this.textEdit88.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit88.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit88.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit88.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit88.Size = new System.Drawing.Size(232, 26);
            this.textEdit88.TabIndex = 33;
            // 
            // textEdit89
            // 
            this.textEdit89.EditValue = "";
            this.textEdit89.EnterMoveNextControl = true;
            this.textEdit89.Location = new System.Drawing.Point(154, 159);
            this.textEdit89.Name = "textEdit89";
            this.textEdit89.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit89.Properties.Appearance.Options.UseFont = true;
            this.textEdit89.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit89.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit89.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit89.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit89.Size = new System.Drawing.Size(232, 26);
            this.textEdit89.TabIndex = 32;
            // 
            // textEdit90
            // 
            this.textEdit90.EditValue = "";
            this.textEdit90.EnterMoveNextControl = true;
            this.textEdit90.Location = new System.Drawing.Point(154, 114);
            this.textEdit90.Name = "textEdit90";
            this.textEdit90.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit90.Properties.Appearance.Options.UseFont = true;
            this.textEdit90.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit90.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit90.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit90.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit90.Size = new System.Drawing.Size(232, 26);
            this.textEdit90.TabIndex = 31;
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Location = new System.Drawing.Point(17, 25);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(63, 19);
            this.labelControl61.TabIndex = 1;
            this.labelControl61.Text = "Mã đại lý";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Location = new System.Drawing.Point(17, 70);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(70, 19);
            this.labelControl62.TabIndex = 21;
            this.labelControl62.Text = "Tên đại lý";
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Location = new System.Drawing.Point(17, 115);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(31, 19);
            this.labelControl63.TabIndex = 24;
            this.labelControl63.Text = "MST";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Location = new System.Drawing.Point(17, 160);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(48, 19);
            this.labelControl64.TabIndex = 27;
            this.labelControl64.Text = "Địa chỉ";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl65.Location = new System.Drawing.Point(17, 205);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(97, 19);
            this.labelControl65.TabIndex = 30;
            this.labelControl65.Text = "Quận / huyện";
            // 
            // Frm_PhongKS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 691);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.palTop);
            this.Name = "Frm_PhongKS";
            this.Text = "Khách sạn";
            this.Load += new System.EventHandler(this.Frm_PhongKS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.palTop)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palphong)).EndInit();
            this.palphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl2)).EndInit();
            this.XtraTabControl2.ResumeLayout(false);
            this.tabNhomphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbttNhomphong)).EndInit();
            this.palbttNhomphong.ResumeLayout(false);
            this.tabThongtinphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbKhuvuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.tabBooking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palBttBooking)).EndInit();
            this.palBttBooking.ResumeLayout(false);
            this.tabLetan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.TabThungan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Thungan)).EndInit();
            this.palbtt_Thungan.ResumeLayout(false);
            this.tabNghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Nghiepvu)).EndInit();
            this.palbtt_Nghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Tabcontrol)).EndInit();
            this.Tabcontrol.ResumeLayout(false);
            this.Tabsub_nhomphong.ResumeLayout(false);
            this.tabsub_TTP_theotg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.Tabsub_TTP_khanangcungungphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.Tabsub_TTP_thongkephong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            this.panelControl22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            this.panelControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            this.panelControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            this.TabsubBooking_Dangky.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palBookDangky)).EndInit();
            this.palBookDangky.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.panelControl25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            this.panelControl24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).EndInit();
            this.Tabsub_Booking_DSBooking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).EndInit();
            this.panelControl26.ResumeLayout(false);
            this.panelControl26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.Tabsub_Booking_LSLuutru.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palBook_LSluutru_Top)).EndInit();
            this.palBook_LSluutru_Top.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_Book_LSluutru_Search)).EndInit();
            this.pal_Book_LSluutru_Search.ResumeLayout(false);
            this.pal_Book_LSluutru_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit81.Properties)).EndInit();
            this.Tabsub_Booking_Daily.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).EndInit();
            this.panelControl32.ResumeLayout(false);
            this.panelControl32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit90.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl palTop;
        private DevExpress.XtraTab.XtraTabControl XtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage tabNhomphong;
        private DevExpress.XtraTab.XtraTabPage tabThongtinphong;
        private DevExpress.XtraTab.XtraTabPage tabBooking;
        private DevExpress.XtraTab.XtraTabPage tabLetan;
        private DevExpress.XtraTab.XtraTabPage TabThungan;
        private DevExpress.XtraTab.XtraTabPage tabNghiepvu;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl palphong;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl palbttNhomphong;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton bnt_luuban;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton bttNhom_PDon;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bttTTPTTG;
        private DevExpress.XtraEditors.SimpleButton bttTKP;
        private DevExpress.XtraEditors.SimpleButton bttKNCUP;
        private DevExpress.XtraEditors.SimpleButton bttSDPHT;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.ComboBoxEdit cbbNhom;
        private DevExpress.XtraEditors.ComboBoxEdit cbbKhuvuc;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraEditors.PanelControl palBttBooking;
        private DevExpress.XtraEditors.SimpleButton btt_Book_DaiLy;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton bttBook_DS;
        private DevExpress.XtraEditors.SimpleButton bttBook_LS_luutru;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton bttBook_DK;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton simpleButton30;
        private DevExpress.XtraEditors.SimpleButton simpleButton31;
        private DevExpress.XtraEditors.SimpleButton simpleButton33;
        private DevExpress.XtraEditors.SimpleButton simpleButton34;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton26;
        private DevExpress.XtraEditors.SimpleButton simpleButton28;
        private DevExpress.XtraEditors.SimpleButton simpleButton29;
        private DevExpress.XtraEditors.PanelControl palbtt_Thungan;
        private DevExpress.XtraEditors.SimpleButton simpleButton40;
        private DevExpress.XtraEditors.SimpleButton simpleButton27;
        private DevExpress.XtraEditors.SimpleButton simpleButton32;
        private DevExpress.XtraEditors.SimpleButton simpleButton35;
        private DevExpress.XtraEditors.SimpleButton simpleButton36;
        private DevExpress.XtraEditors.SimpleButton simpleButton37;
        private DevExpress.XtraEditors.SimpleButton simpleButton38;
        private DevExpress.XtraEditors.SimpleButton simpleButton39;
        private DevExpress.XtraEditors.PanelControl palbtt_Nghiepvu;
        private DevExpress.XtraEditors.SimpleButton bttThoat;
        private DevExpress.XtraEditors.SimpleButton simpleButton42;
        private DevExpress.XtraEditors.SimpleButton simpleButton43;
        private DevExpress.XtraEditors.SimpleButton simpleButton44;
        private DevExpress.XtraEditors.SimpleButton simpleButton45;
        private DevExpress.XtraEditors.SimpleButton simpleButton47;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraTab.XtraTabControl Tabcontrol;
        private DevExpress.XtraTab.XtraTabPage Tabsub_nhomphong;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraTab.XtraTabPage tabsub_TTP_theotg;
        private DevExpress.XtraTab.XtraTabPage Tabsub_TTP_khanangcungungphong;
        private DevExpress.XtraTab.XtraTabPage Tabsub_TTP_thongkephong;
        private DevExpress.XtraTab.XtraTabPage Tabsub_TTP_sodophonghientai;
        private DevExpress.XtraTab.XtraTabPage TabsubBooking_Dangky;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn TEMP1;
        private DevExpress.XtraGrid.Columns.GridColumn TEMP2;
        private DevExpress.XtraGrid.Columns.GridColumn TEMP3;
        private DevExpress.XtraGrid.Columns.GridColumn TEMP4;
        private DevExpress.XtraGrid.Columns.GridColumn TEMP5;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.SimpleButton simpleButton46;
        private DevExpress.XtraEditors.SimpleButton simpleButton48;
        private DevExpress.XtraEditors.SimpleButton simpleButton49;
        private DevExpress.XtraEditors.SimpleButton simpleButton50;
        private DevExpress.XtraEditors.SimpleButton simpleButton51;
        private DevExpress.XtraEditors.SimpleButton simpleButton52;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn TONGSOPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.TextEdit textEdit28;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit textEdit30;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEdit32;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit textEdit34;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit36;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit21;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private DevExpress.XtraEditors.TextEdit textEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.TextEdit textEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit47;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit51;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit54;
        private DevExpress.XtraEditors.TextEdit textEdit55;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit textEdit40;
        private DevExpress.XtraEditors.TextEdit textEdit41;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit44;
        private DevExpress.XtraEditors.TextEdit textEdit45;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.TextEdit textEdit33;
        private DevExpress.XtraEditors.TextEdit textEdit48;
        private DevExpress.XtraEditors.TextEdit textEdit50;
        private DevExpress.XtraEditors.TextEdit textEdit52;
        private DevExpress.XtraEditors.TextEdit textEdit38;
        private DevExpress.XtraEditors.TextEdit textEdit39;
        private DevExpress.XtraEditors.TextEdit textEdit42;
        private DevExpress.XtraEditors.TextEdit textEdit43;
        private DevExpress.XtraEditors.TextEdit textEdit46;
        private DevExpress.XtraEditors.TextEdit textEdit31;
        private DevExpress.XtraEditors.TextEdit textEdit29;
        private DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.TextEdit textEdit49;
        private DevExpress.XtraEditors.TextEdit textEdit35;
        private DevExpress.XtraTab.XtraTabPage Tabsub_Booking_DSBooking;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraEditors.PanelControl panelControl26;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton21;
        private DevExpress.XtraTab.XtraTabPage Tabsub_Booking_LSLuutru;
        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton53;
        private DevExpress.XtraEditors.SimpleButton simpleButton54;
        private DevExpress.XtraEditors.SimpleButton simpleButton55;
        private DevExpress.XtraEditors.SimpleButton simpleButton56;
        private DevExpress.XtraEditors.PanelControl palBook_LSluutru_Top;
        private DevExpress.XtraEditors.PanelControl pal_Book_LSluutru_Search;
        private DevExpress.XtraEditors.TextEdit textEdit80;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit textEdit81;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraTab.XtraTabPage Tabsub_Booking_Daily;
        private DevExpress.XtraEditors.SimpleButton simpleButton57;
        private DevExpress.XtraEditors.TextEdit textEdit82;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.RichTextBox richTextBox11;
        private System.Windows.Forms.RichTextBox richTextBox12;
        private System.Windows.Forms.RichTextBox richTextBox13;
        private DevExpress.XtraEditors.PanelControl panelControl32;
        private DevExpress.XtraEditors.TextEdit textEdit96;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.TextEdit textEdit93;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.TextEdit textEdit94;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.TextEdit textEdit95;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.TextEdit textEdit87;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.TextEdit textEdit91;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.TextEdit textEdit92;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.TextEdit textEdit84;
        private DevExpress.XtraEditors.TextEdit textEdit85;
        private DevExpress.XtraEditors.TextEdit textEdit83;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.TextEdit textEdit86;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.TextEdit textEdit88;
        private DevExpress.XtraEditors.TextEdit textEdit89;
        private DevExpress.XtraEditors.TextEdit textEdit90;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.PanelControl palBookDangky;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.TextEdit textEdit77;
        private DevExpress.XtraEditors.TextEdit textEdit78;
        private DevExpress.XtraEditors.TextEdit textEdit79;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.TextEdit textEdit76;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit textEdit73;
        private DevExpress.XtraEditors.TextEdit textEdit75;
        private DevExpress.XtraEditors.TextEdit textEdit53;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit textEdit59;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit textEdit66;
        private DevExpress.XtraEditors.TextEdit textEdit67;
        private DevExpress.XtraEditors.TextEdit textEdit72;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIPHONG1;
        private DevExpress.XtraGrid.Columns.GridColumn PHONG1;
        private DevExpress.XtraGrid.Columns.GridColumn SLKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn TENKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn CMT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMT;
        private DevExpress.XtraGrid.Columns.GridColumn VISA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYVISA;
        private DevExpress.XtraGrid.Columns.GridColumn QUOCTICH;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.TextEdit textEdit68;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit textEdit71;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit65;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit textEdit69;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit textEdit70;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit textEdit74;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.DateEdit dtp_ngayden;
        private DevExpress.XtraEditors.DateEdit dtp_ngaydi;
        private DevExpress.XtraEditors.TextEdit textEdit64;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit textEdit63;
        private DevExpress.XtraEditors.TextEdit textEdit62;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit textEdit61;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit textEdit60;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit textEdit58;
        private DevExpress.XtraEditors.TextEdit textEdit57;
        private DevExpress.XtraEditors.TextEdit textEdit56;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;




    }
}