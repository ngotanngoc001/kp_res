﻿namespace KP_RES 
{
    partial class Frm_KLP 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_KLP));
            this.Frame = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlPhong = new DevExpress.XtraEditors.PanelControl();
            this.Frame_Phong_2 = new DevExpress.XtraEditors.PanelControl();
            this.pal_Phong = new System.Windows.Forms.Panel();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.btnPhongXuong = new DevExpress.XtraEditors.SimpleButton();
            this.lblPageX = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.btnPhongLen = new DevExpress.XtraEditors.SimpleButton();
            this.lblPageL = new DevExpress.XtraEditors.SimpleButton();
            this.pnlKhachsan = new DevExpress.XtraEditors.PanelControl();
            this.pal_KS = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnKSXuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.btnKSLen = new DevExpress.XtraEditors.SimpleButton();
            this.pnlLau = new DevExpress.XtraEditors.PanelControl();
            this.pal_Lau = new System.Windows.Forms.FlowLayoutPanel();
            this.btnLauLen = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.btnLauXuong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.Frame_Right = new DevExpress.XtraEditors.PanelControl();
            this.tab_Option = new DevExpress.XtraTab.XtraTabControl();
            this.tab_Phong = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.btn_PhongRigh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PhongUp = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PhongDown = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PhongLeft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuuSoDoPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnDefault = new DevExpress.XtraEditors.SimpleButton();
            this.Frame_ThemPhong = new DevExpress.XtraEditors.PanelControl();
            this.cboLoai_Phong1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Frame_Img_Phong1 = new DevExpress.XtraEditors.PanelControl();
            this.img_Phong1 = new System.Windows.Forms.PictureBox();
            this.btnLuuThemNhanh = new DevExpress.XtraEditors.SimpleButton();
            this.lblKichThuoc_Phong1 = new DevExpress.XtraEditors.LabelControl();
            this.cboKichThuoc_Phong1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblLoai_Phong1 = new DevExpress.XtraEditors.LabelControl();
            this.lblSo_Phong = new DevExpress.XtraEditors.LabelControl();
            this.txtSo_Phong = new DevExpress.XtraEditors.TextEdit();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.txtTen_Phong = new DevExpress.XtraEditors.TextEdit();
            this.cboLoai_Phong = new DevExpress.XtraEditors.LookUpEdit();
            this.btnSuaPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemNhieuPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemPhong = new DevExpress.XtraEditors.SimpleButton();
            this.Frame_Img_Phong = new DevExpress.XtraEditors.PanelControl();
            this.img_Phong = new System.Windows.Forms.PictureBox();
            this.lblLau_Phong = new DevExpress.XtraEditors.LabelControl();
            this.cboLau_Phong = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSuDung_Phong = new DevExpress.XtraEditors.CheckEdit();
            this.lblKichThuoc_Phong = new DevExpress.XtraEditors.LabelControl();
            this.cboKichThuoc_Phong = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblLoai_Phong = new DevExpress.XtraEditors.LabelControl();
            this.lblTen_Phong = new DevExpress.XtraEditors.LabelControl();
            this.tab_Lau = new DevExpress.XtraTab.XtraTabPage();
            this.panelLau = new DevExpress.XtraEditors.PanelControl();
            this.Frame_Img_Lau = new DevExpress.XtraEditors.PanelControl();
            this.img_Lau = new System.Windows.Forms.PictureBox();
            this.cboKhachSan_Lau = new DevExpress.XtraEditors.LookUpEdit();
            this.btnSuaLau = new DevExpress.XtraEditors.SimpleButton();
            this.lblTen_Lau = new DevExpress.XtraEditors.LabelControl();
            this.chkSuDung_Lau = new DevExpress.XtraEditors.CheckEdit();
            this.txtGhiChu_Lau = new DevExpress.XtraEditors.TextEdit();
            this.btnThemLau = new DevExpress.XtraEditors.SimpleButton();
            this.txtTen_Lau = new DevExpress.XtraEditors.TextEdit();
            this.lblKS_Lau = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuLau = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaLau = new DevExpress.XtraEditors.SimpleButton();
            this.lblGhiChu_Lau = new DevExpress.XtraEditors.LabelControl();
            this.tab_KhachSan = new DevExpress.XtraTab.XtraTabPage();
            this.pal_KhachSan = new DevExpress.XtraEditors.PanelControl();
            this.btnSuaKS = new DevExpress.XtraEditors.SimpleButton();
            this.chkSuDung_KS = new DevExpress.XtraEditors.CheckEdit();
            this.txtGhiChu_KS = new DevExpress.XtraEditors.TextEdit();
            this.btnThemKS = new DevExpress.XtraEditors.SimpleButton();
            this.txtTen_KS = new DevExpress.XtraEditors.TextEdit();
            this.lblTenKS = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuKS = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaKS = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonKhachSan = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonLau = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonPhong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanPhim = new DevExpress.XtraEditors.SimpleButton();
            this.timer_Up = new System.Windows.Forms.Timer(this.components);
            this.timer_Down = new System.Windows.Forms.Timer(this.components);
            this.timer_Left = new System.Windows.Forms.Timer(this.components);
            this.timer_Righ = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame)).BeginInit();
            this.Frame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhong)).BeginInit();
            this.pnlPhong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Phong_2)).BeginInit();
            this.Frame_Phong_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKhachsan)).BeginInit();
            this.pnlKhachsan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLau)).BeginInit();
            this.pnlLau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Right)).BeginInit();
            this.Frame_Right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).BeginInit();
            this.tab_Option.SuspendLayout();
            this.tab_Phong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_ThemPhong)).BeginInit();
            this.Frame_ThemPhong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai_Phong1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Phong1)).BeginInit();
            this.Frame_Img_Phong1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Phong1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKichThuoc_Phong1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSo_Phong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_Phong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai_Phong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Phong)).BeginInit();
            this.Frame_Img_Phong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Phong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLau_Phong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_Phong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKichThuoc_Phong.Properties)).BeginInit();
            this.tab_Lau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLau)).BeginInit();
            this.panelLau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Lau)).BeginInit();
            this.Frame_Img_Lau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Lau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhachSan_Lau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_Lau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu_Lau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_Lau.Properties)).BeginInit();
            this.tab_KhachSan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_KhachSan)).BeginInit();
            this.pal_KhachSan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_KS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu_KS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_KS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame
            // 
            this.Frame.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Frame.Controls.Add(this.panelControl4);
            this.Frame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame.Location = new System.Drawing.Point(0, 0);
            this.Frame.Name = "Frame";
            this.Frame.Size = new System.Drawing.Size(1020, 615);
            this.Frame.TabIndex = 0;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.pnlPhong);
            this.panelControl4.Controls.Add(this.pnlLau);
            this.panelControl4.Controls.Add(this.pnlKhachsan);
            this.panelControl4.Controls.Add(this.Frame_Right);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1020, 615);
            this.panelControl4.TabIndex = 1;
            // 
            // pnlPhong
            // 
            this.pnlPhong.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlPhong.Appearance.Options.UseBackColor = true;
            this.pnlPhong.Controls.Add(this.Frame_Phong_2);
            this.pnlPhong.Controls.Add(this.panelControl6);
            this.pnlPhong.Controls.Add(this.pal_sotrang);
            this.pnlPhong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPhong.Location = new System.Drawing.Point(0, 0);
            this.pnlPhong.Name = "pnlPhong";
            this.pnlPhong.Size = new System.Drawing.Size(400, 615);
            this.pnlPhong.TabIndex = 1;
            // 
            // Frame_Phong_2
            // 
            this.Frame_Phong_2.Controls.Add(this.pal_Phong);
            this.Frame_Phong_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame_Phong_2.Location = new System.Drawing.Point(2, 37);
            this.Frame_Phong_2.Name = "Frame_Phong_2";
            this.Frame_Phong_2.Size = new System.Drawing.Size(396, 541);
            this.Frame_Phong_2.TabIndex = 56;
            // 
            // pal_Phong
            // 
            this.pal_Phong.BackColor = System.Drawing.Color.Transparent;
            this.pal_Phong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_Phong.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_Phong.Location = new System.Drawing.Point(2, 2);
            this.pal_Phong.Name = "pal_Phong";
            this.pal_Phong.Size = new System.Drawing.Size(392, 537);
            this.pal_Phong.TabIndex = 55;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.btnPhongXuong);
            this.panelControl6.Controls.Add(this.lblPageX);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 578);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(396, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // btnPhongXuong
            // 
            this.btnPhongXuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhongXuong.Appearance.Options.UseFont = true;
            this.btnPhongXuong.Appearance.Options.UseTextOptions = true;
            this.btnPhongXuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnPhongXuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPhongXuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnPhongXuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPhongXuong.Location = new System.Drawing.Point(78, 0);
            this.btnPhongXuong.Margin = new System.Windows.Forms.Padding(4);
            this.btnPhongXuong.Name = "btnPhongXuong";
            this.btnPhongXuong.Size = new System.Drawing.Size(318, 35);
            this.btnPhongXuong.TabIndex = 50;
            this.btnPhongXuong.Text = "Khu vực";
            this.btnPhongXuong.Click += new System.EventHandler(this.btnPhongXuong_Click);
            // 
            // lblPageX
            // 
            this.lblPageX.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageX.Appearance.Options.UseFont = true;
            this.lblPageX.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPageX.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lblPageX.Location = new System.Drawing.Point(0, 0);
            this.lblPageX.Margin = new System.Windows.Forms.Padding(4);
            this.lblPageX.Name = "lblPageX";
            this.lblPageX.Size = new System.Drawing.Size(78, 35);
            this.lblPageX.TabIndex = 49;
            this.lblPageX.Text = "1/5";
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.btnPhongLen);
            this.pal_sotrang.Controls.Add(this.lblPageL);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(396, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // btnPhongLen
            // 
            this.btnPhongLen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhongLen.Appearance.Options.UseFont = true;
            this.btnPhongLen.Appearance.Options.UseTextOptions = true;
            this.btnPhongLen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnPhongLen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPhongLen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnPhongLen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPhongLen.Location = new System.Drawing.Point(78, 0);
            this.btnPhongLen.Margin = new System.Windows.Forms.Padding(4);
            this.btnPhongLen.Name = "btnPhongLen";
            this.btnPhongLen.Size = new System.Drawing.Size(318, 35);
            this.btnPhongLen.TabIndex = 50;
            this.btnPhongLen.Text = "Khu vực";
            this.btnPhongLen.Click += new System.EventHandler(this.btnPhongLen_Click);
            // 
            // lblPageL
            // 
            this.lblPageL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageL.Appearance.Options.UseFont = true;
            this.lblPageL.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPageL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lblPageL.Location = new System.Drawing.Point(0, 0);
            this.lblPageL.Margin = new System.Windows.Forms.Padding(4);
            this.lblPageL.Name = "lblPageL";
            this.lblPageL.Size = new System.Drawing.Size(78, 35);
            this.lblPageL.TabIndex = 49;
            this.lblPageL.Text = "1/5";
            // 
            // pnlKhachsan
            // 
            this.pnlKhachsan.Controls.Add(this.pal_KS);
            this.pnlKhachsan.Controls.Add(this.panelControl1);
            this.pnlKhachsan.Controls.Add(this.pal_len);
            this.pnlKhachsan.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlKhachsan.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnlKhachsan.Location = new System.Drawing.Point(535, 0);
            this.pnlKhachsan.Name = "pnlKhachsan";
            this.pnlKhachsan.Size = new System.Drawing.Size(135, 615);
            this.pnlKhachsan.TabIndex = 5;
            // 
            // pal_KS
            // 
            this.pal_KS.AutoScroll = true;
            this.pal_KS.BackColor = System.Drawing.Color.Transparent;
            this.pal_KS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_KS.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_KS.Location = new System.Drawing.Point(2, 37);
            this.pal_KS.Margin = new System.Windows.Forms.Padding(0);
            this.pal_KS.Name = "pal_KS";
            this.pal_KS.Size = new System.Drawing.Size(131, 541);
            this.pal_KS.TabIndex = 29;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnKSXuong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(2, 578);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(131, 35);
            this.panelControl1.TabIndex = 30;
            // 
            // btnKSXuong
            // 
            this.btnKSXuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btnKSXuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKSXuong.Appearance.Options.UseBackColor = true;
            this.btnKSXuong.Appearance.Options.UseFont = true;
            this.btnKSXuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnKSXuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnKSXuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnKSXuong.Location = new System.Drawing.Point(0, 0);
            this.btnKSXuong.Margin = new System.Windows.Forms.Padding(4);
            this.btnKSXuong.Name = "btnKSXuong";
            this.btnKSXuong.Size = new System.Drawing.Size(131, 35);
            this.btnKSXuong.TabIndex = 2;
            this.btnKSXuong.Click += new System.EventHandler(this.btnKSXuong_Click);
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.btnKSLen);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // btnKSLen
            // 
            this.btnKSLen.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btnKSLen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKSLen.Appearance.Options.UseBackColor = true;
            this.btnKSLen.Appearance.Options.UseFont = true;
            this.btnKSLen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKSLen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnKSLen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnKSLen.Location = new System.Drawing.Point(0, 0);
            this.btnKSLen.Margin = new System.Windows.Forms.Padding(4);
            this.btnKSLen.Name = "btnKSLen";
            this.btnKSLen.Size = new System.Drawing.Size(131, 35);
            this.btnKSLen.TabIndex = 26;
            this.btnKSLen.Click += new System.EventHandler(this.btnKSLen_Click);
            // 
            // pnlLau
            // 
            this.pnlLau.Controls.Add(this.pal_Lau);
            this.pnlLau.Controls.Add(this.btnLauLen);
            this.pnlLau.Controls.Add(this.panelControl15);
            this.pnlLau.Controls.Add(this.panelControl7);
            this.pnlLau.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLau.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnlLau.Location = new System.Drawing.Point(400, 0);
            this.pnlLau.Name = "pnlLau";
            this.pnlLau.Size = new System.Drawing.Size(135, 615);
            this.pnlLau.TabIndex = 2;
            // 
            // pal_Lau
            // 
            this.pal_Lau.AutoScroll = true;
            this.pal_Lau.BackColor = System.Drawing.Color.Transparent;
            this.pal_Lau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_Lau.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_Lau.Location = new System.Drawing.Point(2, 37);
            this.pal_Lau.Margin = new System.Windows.Forms.Padding(0);
            this.pal_Lau.Name = "pal_Lau";
            this.pal_Lau.Size = new System.Drawing.Size(131, 541);
            this.pal_Lau.TabIndex = 29;
            // 
            // btnLauLen
            // 
            this.btnLauLen.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btnLauLen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLauLen.Appearance.Options.UseBackColor = true;
            this.btnLauLen.Appearance.Options.UseFont = true;
            this.btnLauLen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLauLen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLauLen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLauLen.Location = new System.Drawing.Point(2, 2);
            this.btnLauLen.Margin = new System.Windows.Forms.Padding(4);
            this.btnLauLen.Name = "btnLauLen";
            this.btnLauLen.Size = new System.Drawing.Size(131, 35);
            this.btnLauLen.TabIndex = 26;
            this.btnLauLen.Click += new System.EventHandler(this.btnLauLen_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.btnLauXuong);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl15.Location = new System.Drawing.Point(2, 578);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(131, 35);
            this.panelControl15.TabIndex = 28;
            // 
            // btnLauXuong
            // 
            this.btnLauXuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btnLauXuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLauXuong.Appearance.Options.UseBackColor = true;
            this.btnLauXuong.Appearance.Options.UseFont = true;
            this.btnLauXuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLauXuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnLauXuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLauXuong.Location = new System.Drawing.Point(0, 0);
            this.btnLauXuong.Margin = new System.Windows.Forms.Padding(4);
            this.btnLauXuong.Name = "btnLauXuong";
            this.btnLauXuong.Size = new System.Drawing.Size(131, 35);
            this.btnLauXuong.TabIndex = 2;
            this.btnLauXuong.Click += new System.EventHandler(this.btnLauXuong_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(915, 291);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(77, 168);
            this.panelControl7.TabIndex = 1;
            // 
            // Frame_Right
            // 
            this.Frame_Right.Controls.Add(this.tab_Option);
            this.Frame_Right.Controls.Add(this.panelControl12);
            this.Frame_Right.Controls.Add(this.pal_BUTTON);
            this.Frame_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Frame_Right.Location = new System.Drawing.Point(670, 0);
            this.Frame_Right.Name = "Frame_Right";
            this.Frame_Right.Size = new System.Drawing.Size(350, 615);
            this.Frame_Right.TabIndex = 3;
            // 
            // tab_Option
            // 
            this.tab_Option.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_Option.Location = new System.Drawing.Point(2, 37);
            this.tab_Option.Name = "tab_Option";
            this.tab_Option.SelectedTabPage = this.tab_Phong;
            this.tab_Option.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tab_Option.Size = new System.Drawing.Size(346, 541);
            this.tab_Option.TabIndex = 0;
            this.tab_Option.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab_Lau,
            this.tab_Phong,
            this.tab_KhachSan});
            // 
            // tab_Phong
            // 
            this.tab_Phong.Controls.Add(this.panelControl2);
            this.tab_Phong.Name = "tab_Phong";
            this.tab_Phong.Size = new System.Drawing.Size(340, 535);
            this.tab_Phong.Text = "xtraTabPage2";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panelControl17);
            this.panelControl2.Controls.Add(this.panelControl13);
            this.panelControl2.Controls.Add(this.panelControl14);
            this.panelControl2.Controls.Add(this.Frame_ThemPhong);
            this.panelControl2.Controls.Add(this.panel_Sandard);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(340, 535);
            this.panelControl2.TabIndex = 0;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 432);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(340, 20);
            this.panelControl17.TabIndex = 55;
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.btn_PhongRigh);
            this.panelControl13.Controls.Add(this.btn_PhongUp);
            this.panelControl13.Controls.Add(this.btn_PhongDown);
            this.panelControl13.Controls.Add(this.btn_PhongLeft);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl13.Location = new System.Drawing.Point(0, 452);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(340, 83);
            this.panelControl13.TabIndex = 36;
            // 
            // btn_PhongRigh
            // 
            this.btn_PhongRigh.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btn_PhongRigh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PhongRigh.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_PhongRigh.Appearance.Options.UseBackColor = true;
            this.btn_PhongRigh.Appearance.Options.UseFont = true;
            this.btn_PhongRigh.Appearance.Options.UseForeColor = true;
            this.btn_PhongRigh.Image = global::KP_RES.Properties.Resources.right_26;
            this.btn_PhongRigh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_PhongRigh.Location = new System.Drawing.Point(230, 44);
            this.btn_PhongRigh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_PhongRigh.Name = "btn_PhongRigh";
            this.btn_PhongRigh.Size = new System.Drawing.Size(110, 35);
            this.btn_PhongRigh.TabIndex = 34;
            this.btn_PhongRigh.Click += new System.EventHandler(this.btn_PhongRigh_Click);
            this.btn_PhongRigh.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_PhongRigh_MouseDown);
            this.btn_PhongRigh.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_PhongRigh_MouseUp);
            // 
            // btn_PhongUp
            // 
            this.btn_PhongUp.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btn_PhongUp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PhongUp.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_PhongUp.Appearance.Options.UseBackColor = true;
            this.btn_PhongUp.Appearance.Options.UseFont = true;
            this.btn_PhongUp.Appearance.Options.UseForeColor = true;
            this.btn_PhongUp.Image = global::KP_RES.Properties.Resources.up_26;
            this.btn_PhongUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_PhongUp.Location = new System.Drawing.Point(117, 5);
            this.btn_PhongUp.Margin = new System.Windows.Forms.Padding(4);
            this.btn_PhongUp.Name = "btn_PhongUp";
            this.btn_PhongUp.Size = new System.Drawing.Size(110, 35);
            this.btn_PhongUp.TabIndex = 32;
            this.btn_PhongUp.Click += new System.EventHandler(this.btn_PhongUp_Click);
            this.btn_PhongUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_PhongUp_MouseDown);
            this.btn_PhongUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_PhongUp_MouseUp);
            // 
            // btn_PhongDown
            // 
            this.btn_PhongDown.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btn_PhongDown.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PhongDown.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_PhongDown.Appearance.Options.UseBackColor = true;
            this.btn_PhongDown.Appearance.Options.UseFont = true;
            this.btn_PhongDown.Appearance.Options.UseForeColor = true;
            this.btn_PhongDown.Image = global::KP_RES.Properties.Resources.down_26;
            this.btn_PhongDown.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_PhongDown.Location = new System.Drawing.Point(117, 44);
            this.btn_PhongDown.Margin = new System.Windows.Forms.Padding(4);
            this.btn_PhongDown.Name = "btn_PhongDown";
            this.btn_PhongDown.Size = new System.Drawing.Size(110, 35);
            this.btn_PhongDown.TabIndex = 35;
            this.btn_PhongDown.Click += new System.EventHandler(this.btn_PhongDown_Click);
            this.btn_PhongDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_PhongDown_MouseDown);
            this.btn_PhongDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_PhongDown_MouseUp);
            // 
            // btn_PhongLeft
            // 
            this.btn_PhongLeft.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btn_PhongLeft.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PhongLeft.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_PhongLeft.Appearance.Options.UseBackColor = true;
            this.btn_PhongLeft.Appearance.Options.UseFont = true;
            this.btn_PhongLeft.Appearance.Options.UseForeColor = true;
            this.btn_PhongLeft.Image = global::KP_RES.Properties.Resources.left_26;
            this.btn_PhongLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_PhongLeft.Location = new System.Drawing.Point(3, 44);
            this.btn_PhongLeft.Margin = new System.Windows.Forms.Padding(4);
            this.btn_PhongLeft.Name = "btn_PhongLeft";
            this.btn_PhongLeft.Size = new System.Drawing.Size(110, 35);
            this.btn_PhongLeft.TabIndex = 33;
            this.btn_PhongLeft.Click += new System.EventHandler(this.btn_PhongLeft_Click);
            this.btn_PhongLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_PhongLeft_MouseDown);
            this.btn_PhongLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_PhongLeft_MouseUp);
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.btnLuuSoDoPhong);
            this.panelControl14.Controls.Add(this.btnDefault);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl14.Location = new System.Drawing.Point(0, 387);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(340, 45);
            this.panelControl14.TabIndex = 54;
            // 
            // btnLuuSoDoPhong
            // 
            this.btnLuuSoDoPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuSoDoPhong.Appearance.Options.UseFont = true;
            this.btnLuuSoDoPhong.Image = global::KP_RES.Properties.Resources.display_26;
            this.btnLuuSoDoPhong.Location = new System.Drawing.Point(6, 7);
            this.btnLuuSoDoPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuSoDoPhong.Name = "btnLuuSoDoPhong";
            this.btnLuuSoDoPhong.Size = new System.Drawing.Size(163, 35);
            this.btnLuuSoDoPhong.TabIndex = 54;
            this.btnLuuSoDoPhong.Text = "Lưu sơ đồ phòng";
            this.btnLuuSoDoPhong.Click += new System.EventHandler(this.btnLuuSoDoPhong_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDefault.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefault.Appearance.Options.UseBackColor = true;
            this.btnDefault.Appearance.Options.UseFont = true;
            this.btnDefault.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btnDefault.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDefault.Location = new System.Drawing.Point(172, 7);
            this.btnDefault.Margin = new System.Windows.Forms.Padding(4);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(163, 35);
            this.btnDefault.TabIndex = 39;
            this.btnDefault.Text = "Sắp xếp mặc định";
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // Frame_ThemPhong
            // 
            this.Frame_ThemPhong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Frame_ThemPhong.Controls.Add(this.cboLoai_Phong1);
            this.Frame_ThemPhong.Controls.Add(this.Frame_Img_Phong1);
            this.Frame_ThemPhong.Controls.Add(this.btnLuuThemNhanh);
            this.Frame_ThemPhong.Controls.Add(this.lblKichThuoc_Phong1);
            this.Frame_ThemPhong.Controls.Add(this.cboKichThuoc_Phong1);
            this.Frame_ThemPhong.Controls.Add(this.lblLoai_Phong1);
            this.Frame_ThemPhong.Controls.Add(this.lblSo_Phong);
            this.Frame_ThemPhong.Controls.Add(this.txtSo_Phong);
            this.Frame_ThemPhong.Dock = System.Windows.Forms.DockStyle.Top;
            this.Frame_ThemPhong.Location = new System.Drawing.Point(0, 246);
            this.Frame_ThemPhong.Name = "Frame_ThemPhong";
            this.Frame_ThemPhong.Size = new System.Drawing.Size(340, 141);
            this.Frame_ThemPhong.TabIndex = 53;
            this.Frame_ThemPhong.Visible = false;
            // 
            // cboLoai_Phong1
            // 
            this.cboLoai_Phong1.EnterMoveNextControl = true;
            this.cboLoai_Phong1.Location = new System.Drawing.Point(92, 4);
            this.cboLoai_Phong1.Name = "cboLoai_Phong1";
            this.cboLoai_Phong1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai_Phong1.Properties.Appearance.Options.UseFont = true;
            this.cboLoai_Phong1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai_Phong1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoai_Phong1.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoai_Phong1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai_Phong1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLoai_Phong1.Properties.DisplayMember = "NAME";
            this.cboLoai_Phong1.Properties.DropDownItemHeight = 40;
            this.cboLoai_Phong1.Properties.NullText = "";
            this.cboLoai_Phong1.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoai_Phong1.Properties.ShowHeader = false;
            this.cboLoai_Phong1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoai_Phong1.Properties.ValueMember = "ID";
            this.cboLoai_Phong1.Size = new System.Drawing.Size(242, 26);
            this.cboLoai_Phong1.TabIndex = 42;
            // 
            // Frame_Img_Phong1
            // 
            this.Frame_Img_Phong1.Controls.Add(this.img_Phong1);
            this.Frame_Img_Phong1.Location = new System.Drawing.Point(216, 36);
            this.Frame_Img_Phong1.Name = "Frame_Img_Phong1";
            this.Frame_Img_Phong1.Size = new System.Drawing.Size(118, 90);
            this.Frame_Img_Phong1.TabIndex = 39;
            // 
            // img_Phong1
            // 
            this.img_Phong1.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.img_Phong1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.img_Phong1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.img_Phong1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_Phong1.Location = new System.Drawing.Point(2, 2);
            this.img_Phong1.Name = "img_Phong1";
            this.img_Phong1.Size = new System.Drawing.Size(114, 86);
            this.img_Phong1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img_Phong1.TabIndex = 23;
            this.img_Phong1.TabStop = false;
            this.img_Phong1.Click += new System.EventHandler(this.img_Phong1_Click);
            // 
            // btnLuuThemNhanh
            // 
            this.btnLuuThemNhanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuThemNhanh.Appearance.Options.UseFont = true;
            this.btnLuuThemNhanh.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuThemNhanh.Image")));
            this.btnLuuThemNhanh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuThemNhanh.Location = new System.Drawing.Point(92, 100);
            this.btnLuuThemNhanh.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuThemNhanh.Name = "btnLuuThemNhanh";
            this.btnLuuThemNhanh.Size = new System.Drawing.Size(81, 35);
            this.btnLuuThemNhanh.TabIndex = 38;
            this.btnLuuThemNhanh.Text = "&Lưu";
            this.btnLuuThemNhanh.Click += new System.EventHandler(this.btnLuuThemNhanh_Click);
            // 
            // lblKichThuoc_Phong1
            // 
            this.lblKichThuoc_Phong1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKichThuoc_Phong1.Location = new System.Drawing.Point(6, 70);
            this.lblKichThuoc_Phong1.Margin = new System.Windows.Forms.Padding(4);
            this.lblKichThuoc_Phong1.Name = "lblKichThuoc_Phong1";
            this.lblKichThuoc_Phong1.Size = new System.Drawing.Size(74, 19);
            this.lblKichThuoc_Phong1.TabIndex = 36;
            this.lblKichThuoc_Phong1.Text = "Kích thước";
            // 
            // cboKichThuoc_Phong1
            // 
            this.cboKichThuoc_Phong1.Location = new System.Drawing.Point(92, 67);
            this.cboKichThuoc_Phong1.Name = "cboKichThuoc_Phong1";
            this.cboKichThuoc_Phong1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cboKichThuoc_Phong1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKichThuoc_Phong1.Properties.Appearance.Options.UseFont = true;
            this.cboKichThuoc_Phong1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKichThuoc_Phong1.Properties.Items.AddRange(new object[] {
            "Kích thước 1",
            "Kích thước 2",
            "Kích thước 3",
            "Kích thước 4"});
            this.cboKichThuoc_Phong1.Properties.ReadOnly = true;
            this.cboKichThuoc_Phong1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboKichThuoc_Phong1.Size = new System.Drawing.Size(120, 26);
            this.cboKichThuoc_Phong1.TabIndex = 37;
            // 
            // lblLoai_Phong1
            // 
            this.lblLoai_Phong1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoai_Phong1.Location = new System.Drawing.Point(6, 6);
            this.lblLoai_Phong1.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoai_Phong1.Name = "lblLoai_Phong1";
            this.lblLoai_Phong1.Size = new System.Drawing.Size(79, 19);
            this.lblLoai_Phong1.TabIndex = 22;
            this.lblLoai_Phong1.Text = "Loại phòng";
            // 
            // lblSo_Phong
            // 
            this.lblSo_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSo_Phong.Location = new System.Drawing.Point(6, 38);
            this.lblSo_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.lblSo_Phong.Name = "lblSo_Phong";
            this.lblSo_Phong.Size = new System.Drawing.Size(68, 19);
            this.lblSo_Phong.TabIndex = 24;
            this.lblSo_Phong.Text = "Số phòng";
            // 
            // txtSo_Phong
            // 
            this.txtSo_Phong.EnterMoveNextControl = true;
            this.txtSo_Phong.Location = new System.Drawing.Point(92, 35);
            this.txtSo_Phong.Name = "txtSo_Phong";
            this.txtSo_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSo_Phong.Properties.Appearance.Options.UseFont = true;
            this.txtSo_Phong.Properties.MaxLength = 2;
            this.txtSo_Phong.Size = new System.Drawing.Size(120, 26);
            this.txtSo_Phong.TabIndex = 23;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.txtTen_Phong);
            this.panel_Sandard.Controls.Add(this.cboLoai_Phong);
            this.panel_Sandard.Controls.Add(this.btnSuaPhong);
            this.panel_Sandard.Controls.Add(this.btnXoaPhong);
            this.panel_Sandard.Controls.Add(this.btnLuuPhong);
            this.panel_Sandard.Controls.Add(this.btnThemNhieuPhong);
            this.panel_Sandard.Controls.Add(this.btnThemPhong);
            this.panel_Sandard.Controls.Add(this.Frame_Img_Phong);
            this.panel_Sandard.Controls.Add(this.lblLau_Phong);
            this.panel_Sandard.Controls.Add(this.cboLau_Phong);
            this.panel_Sandard.Controls.Add(this.chkSuDung_Phong);
            this.panel_Sandard.Controls.Add(this.lblKichThuoc_Phong);
            this.panel_Sandard.Controls.Add(this.cboKichThuoc_Phong);
            this.panel_Sandard.Controls.Add(this.lblLoai_Phong);
            this.panel_Sandard.Controls.Add(this.lblTen_Phong);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Sandard.Location = new System.Drawing.Point(0, 0);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(340, 246);
            this.panel_Sandard.TabIndex = 0;
            // 
            // txtTen_Phong
            // 
            this.txtTen_Phong.EnterMoveNextControl = true;
            this.txtTen_Phong.Location = new System.Drawing.Point(90, 70);
            this.txtTen_Phong.Name = "txtTen_Phong";
            this.txtTen_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen_Phong.Properties.Appearance.Options.UseFont = true;
            this.txtTen_Phong.Size = new System.Drawing.Size(123, 26);
            this.txtTen_Phong.TabIndex = 42;
            // 
            // cboLoai_Phong
            // 
            this.cboLoai_Phong.EnterMoveNextControl = true;
            this.cboLoai_Phong.Location = new System.Drawing.Point(90, 38);
            this.cboLoai_Phong.Name = "cboLoai_Phong";
            this.cboLoai_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai_Phong.Properties.Appearance.Options.UseFont = true;
            this.cboLoai_Phong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai_Phong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoai_Phong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoai_Phong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai_Phong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLoai_Phong.Properties.DisplayMember = "NAME";
            this.cboLoai_Phong.Properties.DropDownItemHeight = 40;
            this.cboLoai_Phong.Properties.NullText = "";
            this.cboLoai_Phong.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoai_Phong.Properties.ShowHeader = false;
            this.cboLoai_Phong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoai_Phong.Properties.ValueMember = "ID";
            this.cboLoai_Phong.Size = new System.Drawing.Size(244, 26);
            this.cboLoai_Phong.TabIndex = 41;
            // 
            // btnSuaPhong
            // 
            this.btnSuaPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaPhong.Appearance.Options.UseFont = true;
            this.btnSuaPhong.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaPhong.Image")));
            this.btnSuaPhong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaPhong.Location = new System.Drawing.Point(172, 165);
            this.btnSuaPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaPhong.Name = "btnSuaPhong";
            this.btnSuaPhong.Size = new System.Drawing.Size(80, 35);
            this.btnSuaPhong.TabIndex = 11;
            this.btnSuaPhong.Text = "&Sửa";
            this.btnSuaPhong.Click += new System.EventHandler(this.btnSuaPhong_Click);
            // 
            // btnXoaPhong
            // 
            this.btnXoaPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaPhong.Appearance.Options.UseFont = true;
            this.btnXoaPhong.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaPhong.Image")));
            this.btnXoaPhong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaPhong.Location = new System.Drawing.Point(255, 165);
            this.btnXoaPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaPhong.Name = "btnXoaPhong";
            this.btnXoaPhong.Size = new System.Drawing.Size(80, 35);
            this.btnXoaPhong.TabIndex = 12;
            this.btnXoaPhong.Text = "&Xóa";
            this.btnXoaPhong.Click += new System.EventHandler(this.btnXoaPhong_Click);
            // 
            // btnLuuPhong
            // 
            this.btnLuuPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuPhong.Appearance.Options.UseFont = true;
            this.btnLuuPhong.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuPhong.Image")));
            this.btnLuuPhong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuPhong.Location = new System.Drawing.Point(6, 165);
            this.btnLuuPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuPhong.Name = "btnLuuPhong";
            this.btnLuuPhong.Size = new System.Drawing.Size(80, 35);
            this.btnLuuPhong.TabIndex = 9;
            this.btnLuuPhong.Text = "&Lưu";
            this.btnLuuPhong.Click += new System.EventHandler(this.btnLuuPhong_Click);
            // 
            // btnThemNhieuPhong
            // 
            this.btnThemNhieuPhong.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnThemNhieuPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemNhieuPhong.Appearance.Options.UseBackColor = true;
            this.btnThemNhieuPhong.Appearance.Options.UseFont = true;
            this.btnThemNhieuPhong.Image = global::KP_RES.Properties.Resources.add_list_26;
            this.btnThemNhieuPhong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemNhieuPhong.Location = new System.Drawing.Point(6, 208);
            this.btnThemNhieuPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemNhieuPhong.Name = "btnThemNhieuPhong";
            this.btnThemNhieuPhong.Size = new System.Drawing.Size(329, 35);
            this.btnThemNhieuPhong.TabIndex = 40;
            this.btnThemNhieuPhong.Text = "Thêm nhiều phòng";
            this.btnThemNhieuPhong.Click += new System.EventHandler(this.btnThemNhieuPhong_Click);
            // 
            // btnThemPhong
            // 
            this.btnThemPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemPhong.Appearance.Options.UseFont = true;
            this.btnThemPhong.Image = ((System.Drawing.Image)(resources.GetObject("btnThemPhong.Image")));
            this.btnThemPhong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemPhong.Location = new System.Drawing.Point(89, 165);
            this.btnThemPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemPhong.Name = "btnThemPhong";
            this.btnThemPhong.Size = new System.Drawing.Size(80, 35);
            this.btnThemPhong.TabIndex = 10;
            this.btnThemPhong.Text = "&Thêm";
            this.btnThemPhong.Click += new System.EventHandler(this.btnThemPhong_Click);
            // 
            // Frame_Img_Phong
            // 
            this.Frame_Img_Phong.Controls.Add(this.img_Phong);
            this.Frame_Img_Phong.Location = new System.Drawing.Point(216, 70);
            this.Frame_Img_Phong.Name = "Frame_Img_Phong";
            this.Frame_Img_Phong.Size = new System.Drawing.Size(118, 90);
            this.Frame_Img_Phong.TabIndex = 13;
            // 
            // img_Phong
            // 
            this.img_Phong.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.img_Phong.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.img_Phong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.img_Phong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_Phong.Location = new System.Drawing.Point(2, 2);
            this.img_Phong.Name = "img_Phong";
            this.img_Phong.Size = new System.Drawing.Size(114, 86);
            this.img_Phong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img_Phong.TabIndex = 23;
            this.img_Phong.TabStop = false;
            this.img_Phong.Click += new System.EventHandler(this.img_Phong_Click);
            // 
            // lblLau_Phong
            // 
            this.lblLau_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLau_Phong.Location = new System.Drawing.Point(6, 9);
            this.lblLau_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.lblLau_Phong.Name = "lblLau_Phong";
            this.lblLau_Phong.Size = new System.Drawing.Size(25, 19);
            this.lblLau_Phong.TabIndex = 0;
            this.lblLau_Phong.Text = "Lầu";
            // 
            // cboLau_Phong
            // 
            this.cboLau_Phong.EnterMoveNextControl = true;
            this.cboLau_Phong.Location = new System.Drawing.Point(90, 6);
            this.cboLau_Phong.Name = "cboLau_Phong";
            this.cboLau_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLau_Phong.Properties.Appearance.Options.UseFont = true;
            this.cboLau_Phong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLau_Phong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLau_Phong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLau_Phong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLau_Phong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLau_Phong.Properties.DisplayMember = "NAME";
            this.cboLau_Phong.Properties.DropDownItemHeight = 40;
            this.cboLau_Phong.Properties.NullText = "";
            this.cboLau_Phong.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLau_Phong.Properties.ShowHeader = false;
            this.cboLau_Phong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLau_Phong.Properties.ValueMember = "ID";
            this.cboLau_Phong.Size = new System.Drawing.Size(244, 26);
            this.cboLau_Phong.TabIndex = 1;
            // 
            // chkSuDung_Phong
            // 
            this.chkSuDung_Phong.EditValue = true;
            this.chkSuDung_Phong.EnterMoveNextControl = true;
            this.chkSuDung_Phong.Location = new System.Drawing.Point(89, 134);
            this.chkSuDung_Phong.Name = "chkSuDung_Phong";
            this.chkSuDung_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDung_Phong.Properties.Appearance.Options.UseFont = true;
            this.chkSuDung_Phong.Properties.Caption = "Sử dụng";
            this.chkSuDung_Phong.Size = new System.Drawing.Size(104, 24);
            this.chkSuDung_Phong.TabIndex = 8;
            // 
            // lblKichThuoc_Phong
            // 
            this.lblKichThuoc_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKichThuoc_Phong.Location = new System.Drawing.Point(6, 105);
            this.lblKichThuoc_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.lblKichThuoc_Phong.Name = "lblKichThuoc_Phong";
            this.lblKichThuoc_Phong.Size = new System.Drawing.Size(74, 19);
            this.lblKichThuoc_Phong.TabIndex = 6;
            this.lblKichThuoc_Phong.Text = "Kích thước";
            // 
            // cboKichThuoc_Phong
            // 
            this.cboKichThuoc_Phong.EnterMoveNextControl = true;
            this.cboKichThuoc_Phong.Location = new System.Drawing.Point(90, 102);
            this.cboKichThuoc_Phong.Name = "cboKichThuoc_Phong";
            this.cboKichThuoc_Phong.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cboKichThuoc_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKichThuoc_Phong.Properties.Appearance.Options.UseFont = true;
            this.cboKichThuoc_Phong.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboKichThuoc_Phong.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKichThuoc_Phong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKichThuoc_Phong.Properties.DropDownItemHeight = 40;
            this.cboKichThuoc_Phong.Properties.Items.AddRange(new object[] {
            "Kích thước 1",
            "Kích thước 2",
            "Kích thước 3",
            "Kích thước 4"});
            this.cboKichThuoc_Phong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboKichThuoc_Phong.Size = new System.Drawing.Size(122, 26);
            this.cboKichThuoc_Phong.TabIndex = 7;
            // 
            // lblLoai_Phong
            // 
            this.lblLoai_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoai_Phong.Location = new System.Drawing.Point(6, 41);
            this.lblLoai_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.lblLoai_Phong.Name = "lblLoai_Phong";
            this.lblLoai_Phong.Size = new System.Drawing.Size(79, 19);
            this.lblLoai_Phong.TabIndex = 2;
            this.lblLoai_Phong.Text = "Loại phòng";
            // 
            // lblTen_Phong
            // 
            this.lblTen_Phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen_Phong.Location = new System.Drawing.Point(6, 73);
            this.lblTen_Phong.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen_Phong.Name = "lblTen_Phong";
            this.lblTen_Phong.Size = new System.Drawing.Size(77, 19);
            this.lblTen_Phong.TabIndex = 4;
            this.lblTen_Phong.Text = "Tên phòng";
            // 
            // tab_Lau
            // 
            this.tab_Lau.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.tab_Lau.Appearance.PageClient.Options.UseBackColor = true;
            this.tab_Lau.Controls.Add(this.panelLau);
            this.tab_Lau.Name = "tab_Lau";
            this.tab_Lau.Size = new System.Drawing.Size(340, 535);
            this.tab_Lau.Text = "xtraTabPage1";
            // 
            // panelLau
            // 
            this.panelLau.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelLau.Appearance.Options.UseBackColor = true;
            this.panelLau.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelLau.Controls.Add(this.Frame_Img_Lau);
            this.panelLau.Controls.Add(this.cboKhachSan_Lau);
            this.panelLau.Controls.Add(this.btnSuaLau);
            this.panelLau.Controls.Add(this.lblTen_Lau);
            this.panelLau.Controls.Add(this.chkSuDung_Lau);
            this.panelLau.Controls.Add(this.txtGhiChu_Lau);
            this.panelLau.Controls.Add(this.btnThemLau);
            this.panelLau.Controls.Add(this.txtTen_Lau);
            this.panelLau.Controls.Add(this.lblKS_Lau);
            this.panelLau.Controls.Add(this.btnLuuLau);
            this.panelLau.Controls.Add(this.btnXoaLau);
            this.panelLau.Controls.Add(this.lblGhiChu_Lau);
            this.panelLau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLau.Location = new System.Drawing.Point(0, 0);
            this.panelLau.Name = "panelLau";
            this.panelLau.Size = new System.Drawing.Size(340, 535);
            this.panelLau.TabIndex = 15;
            // 
            // Frame_Img_Lau
            // 
            this.Frame_Img_Lau.Controls.Add(this.img_Lau);
            this.Frame_Img_Lau.Location = new System.Drawing.Point(217, 38);
            this.Frame_Img_Lau.Name = "Frame_Img_Lau";
            this.Frame_Img_Lau.Size = new System.Drawing.Size(118, 90);
            this.Frame_Img_Lau.TabIndex = 15;
            // 
            // img_Lau
            // 
            this.img_Lau.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.img_Lau.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.img_Lau.Cursor = System.Windows.Forms.Cursors.Hand;
            this.img_Lau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_Lau.Location = new System.Drawing.Point(2, 2);
            this.img_Lau.Name = "img_Lau";
            this.img_Lau.Size = new System.Drawing.Size(114, 86);
            this.img_Lau.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img_Lau.TabIndex = 23;
            this.img_Lau.TabStop = false;
            this.img_Lau.Click += new System.EventHandler(this.img_Lau_Click);
            // 
            // cboKhachSan_Lau
            // 
            this.cboKhachSan_Lau.EnterMoveNextControl = true;
            this.cboKhachSan_Lau.Location = new System.Drawing.Point(90, 6);
            this.cboKhachSan_Lau.Name = "cboKhachSan_Lau";
            this.cboKhachSan_Lau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhachSan_Lau.Properties.Appearance.Options.UseFont = true;
            this.cboKhachSan_Lau.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhachSan_Lau.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKhachSan_Lau.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKhachSan_Lau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKhachSan_Lau.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboKhachSan_Lau.Properties.DisplayMember = "NAME";
            this.cboKhachSan_Lau.Properties.DropDownItemHeight = 40;
            this.cboKhachSan_Lau.Properties.NullText = "";
            this.cboKhachSan_Lau.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKhachSan_Lau.Properties.ShowHeader = false;
            this.cboKhachSan_Lau.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKhachSan_Lau.Properties.ValueMember = "ID";
            this.cboKhachSan_Lau.Size = new System.Drawing.Size(243, 26);
            this.cboKhachSan_Lau.TabIndex = 1;
            // 
            // btnSuaLau
            // 
            this.btnSuaLau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaLau.Appearance.Options.UseFont = true;
            this.btnSuaLau.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaLau.Image")));
            this.btnSuaLau.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaLau.Location = new System.Drawing.Point(173, 135);
            this.btnSuaLau.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaLau.Name = "btnSuaLau";
            this.btnSuaLau.Size = new System.Drawing.Size(80, 35);
            this.btnSuaLau.TabIndex = 11;
            this.btnSuaLau.Text = "&Sửa";
            this.btnSuaLau.Click += new System.EventHandler(this.btnSuaLau_Click);
            // 
            // lblTen_Lau
            // 
            this.lblTen_Lau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen_Lau.Location = new System.Drawing.Point(7, 41);
            this.lblTen_Lau.Margin = new System.Windows.Forms.Padding(4);
            this.lblTen_Lau.Name = "lblTen_Lau";
            this.lblTen_Lau.Size = new System.Drawing.Size(27, 19);
            this.lblTen_Lau.TabIndex = 2;
            this.lblTen_Lau.Text = "Tên";
            // 
            // chkSuDung_Lau
            // 
            this.chkSuDung_Lau.EditValue = true;
            this.chkSuDung_Lau.EnterMoveNextControl = true;
            this.chkSuDung_Lau.Location = new System.Drawing.Point(89, 103);
            this.chkSuDung_Lau.Name = "chkSuDung_Lau";
            this.chkSuDung_Lau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDung_Lau.Properties.Appearance.Options.UseFont = true;
            this.chkSuDung_Lau.Properties.Caption = "Sử dụng";
            this.chkSuDung_Lau.Size = new System.Drawing.Size(99, 24);
            this.chkSuDung_Lau.TabIndex = 8;
            // 
            // txtGhiChu_Lau
            // 
            this.txtGhiChu_Lau.EnterMoveNextControl = true;
            this.txtGhiChu_Lau.Location = new System.Drawing.Point(90, 71);
            this.txtGhiChu_Lau.Name = "txtGhiChu_Lau";
            this.txtGhiChu_Lau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu_Lau.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu_Lau.Size = new System.Drawing.Size(123, 26);
            this.txtGhiChu_Lau.TabIndex = 7;
            // 
            // btnThemLau
            // 
            this.btnThemLau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLau.Appearance.Options.UseFont = true;
            this.btnThemLau.Image = ((System.Drawing.Image)(resources.GetObject("btnThemLau.Image")));
            this.btnThemLau.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemLau.Location = new System.Drawing.Point(90, 135);
            this.btnThemLau.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemLau.Name = "btnThemLau";
            this.btnThemLau.Size = new System.Drawing.Size(80, 35);
            this.btnThemLau.TabIndex = 10;
            this.btnThemLau.Text = "&Thêm";
            this.btnThemLau.Click += new System.EventHandler(this.btnThemLau_Click);
            // 
            // txtTen_Lau
            // 
            this.txtTen_Lau.EnterMoveNextControl = true;
            this.txtTen_Lau.Location = new System.Drawing.Point(90, 38);
            this.txtTen_Lau.Name = "txtTen_Lau";
            this.txtTen_Lau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen_Lau.Properties.Appearance.Options.UseFont = true;
            this.txtTen_Lau.Size = new System.Drawing.Size(123, 26);
            this.txtTen_Lau.TabIndex = 3;
            // 
            // lblKS_Lau
            // 
            this.lblKS_Lau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKS_Lau.Location = new System.Drawing.Point(7, 9);
            this.lblKS_Lau.Margin = new System.Windows.Forms.Padding(4);
            this.lblKS_Lau.Name = "lblKS_Lau";
            this.lblKS_Lau.Size = new System.Drawing.Size(71, 19);
            this.lblKS_Lau.TabIndex = 0;
            this.lblKS_Lau.Text = "Khách sạn";
            // 
            // btnLuuLau
            // 
            this.btnLuuLau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuLau.Appearance.Options.UseFont = true;
            this.btnLuuLau.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuLau.Image")));
            this.btnLuuLau.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuLau.Location = new System.Drawing.Point(7, 135);
            this.btnLuuLau.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuLau.Name = "btnLuuLau";
            this.btnLuuLau.Size = new System.Drawing.Size(80, 35);
            this.btnLuuLau.TabIndex = 9;
            this.btnLuuLau.Text = "&Lưu";
            this.btnLuuLau.Click += new System.EventHandler(this.btnLuuLau_Click);
            // 
            // btnXoaLau
            // 
            this.btnXoaLau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLau.Appearance.Options.UseFont = true;
            this.btnXoaLau.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaLau.Image")));
            this.btnXoaLau.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaLau.Location = new System.Drawing.Point(256, 135);
            this.btnXoaLau.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaLau.Name = "btnXoaLau";
            this.btnXoaLau.Size = new System.Drawing.Size(80, 35);
            this.btnXoaLau.TabIndex = 12;
            this.btnXoaLau.Text = "&Xóa";
            this.btnXoaLau.Click += new System.EventHandler(this.btnXoaLau_Click);
            // 
            // lblGhiChu_Lau
            // 
            this.lblGhiChu_Lau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu_Lau.Location = new System.Drawing.Point(7, 74);
            this.lblGhiChu_Lau.Margin = new System.Windows.Forms.Padding(4);
            this.lblGhiChu_Lau.Name = "lblGhiChu_Lau";
            this.lblGhiChu_Lau.Size = new System.Drawing.Size(54, 19);
            this.lblGhiChu_Lau.TabIndex = 6;
            this.lblGhiChu_Lau.Text = "Ghi chú";
            // 
            // tab_KhachSan
            // 
            this.tab_KhachSan.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.tab_KhachSan.Appearance.PageClient.Options.UseBackColor = true;
            this.tab_KhachSan.Controls.Add(this.pal_KhachSan);
            this.tab_KhachSan.Name = "tab_KhachSan";
            this.tab_KhachSan.Size = new System.Drawing.Size(340, 535);
            this.tab_KhachSan.Text = "xtraTabPage1";
            // 
            // pal_KhachSan
            // 
            this.pal_KhachSan.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.pal_KhachSan.Appearance.Options.UseBackColor = true;
            this.pal_KhachSan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_KhachSan.Controls.Add(this.btnSuaKS);
            this.pal_KhachSan.Controls.Add(this.chkSuDung_KS);
            this.pal_KhachSan.Controls.Add(this.txtGhiChu_KS);
            this.pal_KhachSan.Controls.Add(this.btnThemKS);
            this.pal_KhachSan.Controls.Add(this.txtTen_KS);
            this.pal_KhachSan.Controls.Add(this.lblTenKS);
            this.pal_KhachSan.Controls.Add(this.btnLuuKS);
            this.pal_KhachSan.Controls.Add(this.btnXoaKS);
            this.pal_KhachSan.Controls.Add(this.labelControl9);
            this.pal_KhachSan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_KhachSan.Location = new System.Drawing.Point(0, 0);
            this.pal_KhachSan.Name = "pal_KhachSan";
            this.pal_KhachSan.Size = new System.Drawing.Size(340, 535);
            this.pal_KhachSan.TabIndex = 15;
            // 
            // btnSuaKS
            // 
            this.btnSuaKS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaKS.Appearance.Options.UseFont = true;
            this.btnSuaKS.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaKS.Image")));
            this.btnSuaKS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaKS.Location = new System.Drawing.Point(173, 135);
            this.btnSuaKS.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaKS.Name = "btnSuaKS";
            this.btnSuaKS.Size = new System.Drawing.Size(80, 35);
            this.btnSuaKS.TabIndex = 11;
            this.btnSuaKS.Text = "&Sửa";
            this.btnSuaKS.Click += new System.EventHandler(this.btnSuaKS_Click);
            // 
            // chkSuDung_KS
            // 
            this.chkSuDung_KS.EditValue = true;
            this.chkSuDung_KS.EnterMoveNextControl = true;
            this.chkSuDung_KS.Location = new System.Drawing.Point(90, 93);
            this.chkSuDung_KS.Name = "chkSuDung_KS";
            this.chkSuDung_KS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuDung_KS.Properties.Appearance.Options.UseFont = true;
            this.chkSuDung_KS.Properties.Caption = "Sử dụng";
            this.chkSuDung_KS.Size = new System.Drawing.Size(99, 24);
            this.chkSuDung_KS.TabIndex = 8;
            // 
            // txtGhiChu_KS
            // 
            this.txtGhiChu_KS.EnterMoveNextControl = true;
            this.txtGhiChu_KS.Location = new System.Drawing.Point(90, 54);
            this.txtGhiChu_KS.Name = "txtGhiChu_KS";
            this.txtGhiChu_KS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu_KS.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu_KS.Size = new System.Drawing.Size(243, 26);
            this.txtGhiChu_KS.TabIndex = 7;
            // 
            // btnThemKS
            // 
            this.btnThemKS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemKS.Appearance.Options.UseFont = true;
            this.btnThemKS.Image = ((System.Drawing.Image)(resources.GetObject("btnThemKS.Image")));
            this.btnThemKS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemKS.Location = new System.Drawing.Point(90, 135);
            this.btnThemKS.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemKS.Name = "btnThemKS";
            this.btnThemKS.Size = new System.Drawing.Size(80, 35);
            this.btnThemKS.TabIndex = 10;
            this.btnThemKS.Text = "&Thêm";
            this.btnThemKS.Click += new System.EventHandler(this.btnThemKS_Click);
            // 
            // txtTen_KS
            // 
            this.txtTen_KS.EnterMoveNextControl = true;
            this.txtTen_KS.Location = new System.Drawing.Point(90, 13);
            this.txtTen_KS.Name = "txtTen_KS";
            this.txtTen_KS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen_KS.Properties.Appearance.Options.UseFont = true;
            this.txtTen_KS.Size = new System.Drawing.Size(243, 26);
            this.txtTen_KS.TabIndex = 3;
            // 
            // lblTenKS
            // 
            this.lblTenKS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenKS.Location = new System.Drawing.Point(7, 16);
            this.lblTenKS.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenKS.Name = "lblTenKS";
            this.lblTenKS.Size = new System.Drawing.Size(50, 19);
            this.lblTenKS.TabIndex = 0;
            this.lblTenKS.Text = "Tên KS";
            // 
            // btnLuuKS
            // 
            this.btnLuuKS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuKS.Appearance.Options.UseFont = true;
            this.btnLuuKS.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuKS.Image")));
            this.btnLuuKS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuKS.Location = new System.Drawing.Point(7, 135);
            this.btnLuuKS.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuKS.Name = "btnLuuKS";
            this.btnLuuKS.Size = new System.Drawing.Size(80, 35);
            this.btnLuuKS.TabIndex = 9;
            this.btnLuuKS.Text = "&Lưu";
            this.btnLuuKS.Click += new System.EventHandler(this.btnLuuKS_Click);
            // 
            // btnXoaKS
            // 
            this.btnXoaKS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaKS.Appearance.Options.UseFont = true;
            this.btnXoaKS.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaKS.Image")));
            this.btnXoaKS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaKS.Location = new System.Drawing.Point(256, 135);
            this.btnXoaKS.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaKS.Name = "btnXoaKS";
            this.btnXoaKS.Size = new System.Drawing.Size(80, 35);
            this.btnXoaKS.TabIndex = 12;
            this.btnXoaKS.Text = "&Xóa";
            this.btnXoaKS.Click += new System.EventHandler(this.btnXoaKS_Click);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(7, 57);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(54, 19);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "Ghi chú";
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnChonKhachSan);
            this.panelControl12.Controls.Add(this.btnChonLau);
            this.panelControl12.Controls.Add(this.btnChonPhong);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(346, 35);
            this.panelControl12.TabIndex = 19;
            // 
            // btnChonKhachSan
            // 
            this.btnChonKhachSan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonKhachSan.Appearance.Options.UseFont = true;
            this.btnChonKhachSan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonKhachSan.Image = global::KP_RES.Properties.Resources.control_panel_26;
            this.btnChonKhachSan.Location = new System.Drawing.Point(0, 0);
            this.btnChonKhachSan.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonKhachSan.Name = "btnChonKhachSan";
            this.btnChonKhachSan.Size = new System.Drawing.Size(115, 35);
            this.btnChonKhachSan.TabIndex = 11;
            this.btnChonKhachSan.Text = "Khách sạn";
            this.btnChonKhachSan.Click += new System.EventHandler(this.btnChonKhachSan_Click);
            // 
            // btnChonLau
            // 
            this.btnChonLau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonLau.Appearance.Options.UseFont = true;
            this.btnChonLau.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonLau.Image = global::KP_RES.Properties.Resources.control_panel_26;
            this.btnChonLau.Location = new System.Drawing.Point(116, 0);
            this.btnChonLau.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonLau.Name = "btnChonLau";
            this.btnChonLau.Size = new System.Drawing.Size(115, 35);
            this.btnChonLau.TabIndex = 1;
            this.btnChonLau.Text = "Lầu";
            this.btnChonLau.Click += new System.EventHandler(this.btnChonLau_Click);
            // 
            // btnChonPhong
            // 
            this.btnChonPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonPhong.Appearance.Options.UseFont = true;
            this.btnChonPhong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonPhong.Image = global::KP_RES.Properties.Resources.large_icons_26;
            this.btnChonPhong.Location = new System.Drawing.Point(231, 0);
            this.btnChonPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonPhong.Name = "btnChonPhong";
            this.btnChonPhong.Size = new System.Drawing.Size(115, 35);
            this.btnChonPhong.TabIndex = 10;
            this.btnChonPhong.Text = "Phòng";
            this.btnChonPhong.Click += new System.EventHandler(this.btnChonPhong_Click);
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btnCapNhat);
            this.pal_BUTTON.Controls.Add(this.btnBanPhim);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(2, 578);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(346, 35);
            this.pal_BUTTON.TabIndex = 2;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapNhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapNhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapNhat.Location = new System.Drawing.Point(0, 0);
            this.btnCapNhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(173, 35);
            this.btnCapNhat.TabIndex = 33;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnBanPhim
            // 
            this.btnBanPhim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanPhim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanPhim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanPhim.Appearance.Options.UseBackColor = true;
            this.btnBanPhim.Appearance.Options.UseFont = true;
            this.btnBanPhim.Appearance.Options.UseForeColor = true;
            this.btnBanPhim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanPhim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanPhim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanPhim.Location = new System.Drawing.Point(173, 0);
            this.btnBanPhim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanPhim.Name = "btnBanPhim";
            this.btnBanPhim.Size = new System.Drawing.Size(173, 35);
            this.btnBanPhim.TabIndex = 32;
            this.btnBanPhim.Click += new System.EventHandler(this.btnBanPhim_Click);
            // 
            // timer_Up
            // 
            this.timer_Up.Tick += new System.EventHandler(this.timer_Up_Tick);
            // 
            // timer_Down
            // 
            this.timer_Down.Tick += new System.EventHandler(this.timer_Down_Tick);
            // 
            // timer_Left
            // 
            this.timer_Left.Tick += new System.EventHandler(this.timer_Left_Tick);
            // 
            // timer_Righ
            // 
            this.timer_Righ.Tick += new System.EventHandler(this.timer_Righ_Tick);
            // 
            // Frm_KLP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 615);
            this.Controls.Add(this.Frame);
            this.Name = "Frm_KLP";
            this.Text = "Khách sạn - Lầu - Phòng";
            this.Load += new System.EventHandler(this.Frm_KLP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame)).EndInit();
            this.Frame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhong)).EndInit();
            this.pnlPhong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Phong_2)).EndInit();
            this.Frame_Phong_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKhachsan)).EndInit();
            this.pnlKhachsan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlLau)).EndInit();
            this.pnlLau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Right)).EndInit();
            this.Frame_Right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).EndInit();
            this.tab_Option.ResumeLayout(false);
            this.tab_Phong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame_ThemPhong)).EndInit();
            this.Frame_ThemPhong.ResumeLayout(false);
            this.Frame_ThemPhong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai_Phong1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Phong1)).EndInit();
            this.Frame_Img_Phong1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img_Phong1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKichThuoc_Phong1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSo_Phong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            this.panel_Sandard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_Phong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai_Phong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Phong)).EndInit();
            this.Frame_Img_Phong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img_Phong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLau_Phong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_Phong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKichThuoc_Phong.Properties)).EndInit();
            this.tab_Lau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelLau)).EndInit();
            this.panelLau.ResumeLayout(false);
            this.panelLau.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame_Img_Lau)).EndInit();
            this.Frame_Img_Lau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img_Lau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhachSan_Lau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_Lau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu_Lau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_Lau.Properties)).EndInit();
            this.tab_KhachSan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_KhachSan)).EndInit();
            this.pal_KhachSan.ResumeLayout(false);
            this.pal_KhachSan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuDung_KS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu_KS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen_KS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl Frame;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl pnlLau;
        private System.Windows.Forms.FlowLayoutPanel pal_KS;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.SimpleButton btnKSLen;
        private DevExpress.XtraEditors.PanelControl Frame_Right;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnChonLau;
        private DevExpress.XtraEditors.SimpleButton btnChonPhong;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnBanPhim;
        private System.Windows.Forms.Timer timer_Up;
        private System.Windows.Forms.Timer timer_Down;
        private System.Windows.Forms.Timer timer_Left;
        private System.Windows.Forms.Timer timer_Righ;
        private DevExpress.XtraEditors.SimpleButton btnChonKhachSan;
        private DevExpress.XtraEditors.PanelControl pnlPhong;
        private DevExpress.XtraEditors.PanelControl Frame_Phong_2;
        private System.Windows.Forms.Panel pal_Phong;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnPhongXuong;
        private DevExpress.XtraEditors.SimpleButton lblPageX;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton btnPhongLen;
        private DevExpress.XtraEditors.SimpleButton lblPageL;
        private System.Windows.Forms.FlowLayoutPanel pal_Lau;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.SimpleButton btnLauXuong;
        private DevExpress.XtraEditors.SimpleButton btnLauLen;
        private DevExpress.XtraTab.XtraTabControl tab_Option;
        private DevExpress.XtraTab.XtraTabPage tab_Phong;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.SimpleButton btn_PhongRigh;
        private DevExpress.XtraEditors.SimpleButton btn_PhongUp;
        private DevExpress.XtraEditors.SimpleButton btn_PhongDown;
        private DevExpress.XtraEditors.SimpleButton btn_PhongLeft;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btnLuuSoDoPhong;
        private DevExpress.XtraEditors.SimpleButton btnDefault;
        private DevExpress.XtraEditors.PanelControl Frame_ThemPhong;
        private DevExpress.XtraEditors.PanelControl Frame_Img_Phong1;
        private System.Windows.Forms.PictureBox img_Phong1;
        private DevExpress.XtraEditors.SimpleButton btnLuuThemNhanh;
        private DevExpress.XtraEditors.LabelControl lblKichThuoc_Phong1;
        private DevExpress.XtraEditors.ComboBoxEdit cboKichThuoc_Phong1;
        private DevExpress.XtraEditors.LabelControl lblLoai_Phong1;
        private DevExpress.XtraEditors.LabelControl lblSo_Phong;
        private DevExpress.XtraEditors.TextEdit txtSo_Phong;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.LookUpEdit cboLoai_Phong;
        private DevExpress.XtraEditors.SimpleButton btnSuaPhong;
        private DevExpress.XtraEditors.SimpleButton btnXoaPhong;
        private DevExpress.XtraEditors.SimpleButton btnLuuPhong;
        private DevExpress.XtraEditors.SimpleButton btnThemNhieuPhong;
        private DevExpress.XtraEditors.SimpleButton btnThemPhong;
        private DevExpress.XtraEditors.PanelControl Frame_Img_Phong;
        private System.Windows.Forms.PictureBox img_Phong;
        private DevExpress.XtraEditors.LabelControl lblLau_Phong;
        private DevExpress.XtraEditors.LookUpEdit cboLau_Phong;
        private DevExpress.XtraEditors.CheckEdit chkSuDung_Phong;
        private DevExpress.XtraEditors.LabelControl lblKichThuoc_Phong;
        private DevExpress.XtraEditors.ComboBoxEdit cboKichThuoc_Phong;
        private DevExpress.XtraEditors.LabelControl lblLoai_Phong;
        private DevExpress.XtraEditors.LabelControl lblTen_Phong;
        private DevExpress.XtraTab.XtraTabPage tab_Lau;
        private DevExpress.XtraEditors.PanelControl panelLau;
        private DevExpress.XtraEditors.PanelControl Frame_Img_Lau;
        private System.Windows.Forms.PictureBox img_Lau;
        private DevExpress.XtraEditors.LookUpEdit cboKhachSan_Lau;
        private DevExpress.XtraEditors.SimpleButton btnSuaLau;
        private DevExpress.XtraEditors.LabelControl lblTen_Lau;
        private DevExpress.XtraEditors.CheckEdit chkSuDung_Lau;
        private DevExpress.XtraEditors.TextEdit txtGhiChu_Lau;
        private DevExpress.XtraEditors.SimpleButton btnThemLau;
        private DevExpress.XtraEditors.TextEdit txtTen_Lau;
        private DevExpress.XtraEditors.LabelControl lblKS_Lau;
        private DevExpress.XtraEditors.SimpleButton btnLuuLau;
        private DevExpress.XtraEditors.SimpleButton btnXoaLau;
        private DevExpress.XtraEditors.LabelControl lblGhiChu_Lau;
        private DevExpress.XtraTab.XtraTabPage tab_KhachSan;
        private DevExpress.XtraEditors.PanelControl pal_KhachSan;
        private DevExpress.XtraEditors.SimpleButton btnSuaKS;
        private DevExpress.XtraEditors.CheckEdit chkSuDung_KS;
        private DevExpress.XtraEditors.TextEdit txtGhiChu_KS;
        private DevExpress.XtraEditors.SimpleButton btnThemKS;
        private DevExpress.XtraEditors.TextEdit txtTen_KS;
        private DevExpress.XtraEditors.LabelControl lblTenKS;
        private DevExpress.XtraEditors.SimpleButton btnLuuKS;
        private DevExpress.XtraEditors.SimpleButton btnXoaKS;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LookUpEdit cboLoai_Phong1;
        private DevExpress.XtraEditors.TextEdit txtTen_Phong;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnKSXuong;
        private DevExpress.XtraEditors.PanelControl pnlKhachsan;




    }
}