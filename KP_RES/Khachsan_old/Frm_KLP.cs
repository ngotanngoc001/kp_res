﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace KP_RES 
{
    public partial class Frm_KLP : DevExpress.XtraEditors.XtraForm
    {
        String sMaKS = "";
        String sMaPhong = "";
        String sMaLau = "";
        String sPage = "1";
        private Point MouseDownLocation;
        public Frm_KLP()
        {
            InitializeComponent();
            LoadCombo();
            LoadDataKS();
            LoadPermission();
        }

        private void Frm_KLP_Load(object sender, EventArgs e)
        {
            btnChonKhachSan_Click(null, null);
            if (pal_KS.Controls.Count > 0)
            {
                btKS_Click(pal_KS.Controls[0], null);
            }
            if (pal_Lau.Controls.Count > 0)
            {
                btLau_Click(pal_Lau.Controls[0], null);
            }
        }

        private void LoadPermission()
        {
            
        }

        private void LoadCombo()
        {
            String sSQL = "";
            sSQL = "SELECT MAKHACHSAN AS ID,TENKHACHSAN AS NAME FROM KS_KHACHSAN WHERE SUDUNG=1 ORDER BY NAME";
            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            cboKhachSan_Lau.Properties.DataSource = myDT;
            cboKhachSan_Lau.EditValue = cboKhachSan_Lau.Properties.GetDataSourceValue(cboKhachSan_Lau.Properties.ValueMember, 0);

            sSQL = "SELECT MALAU AS ID,TENLAU AS NAME FROM KS_LAU WHERE SUDUNG=1 ORDER BY NAME";
            myDT = clsMain.ReturnDataTable(sSQL);
            cboLau_Phong.Properties.DataSource = myDT;
            cboLau_Phong.EditValue = cboLau_Phong.Properties.GetDataSourceValue(cboLau_Phong.Properties.ValueMember, 0);


            sSQL = "SELECT MALOAIPHONG AS ID,TENLOAIPHONG AS NAME FROM KS_LOAIPHONG WHERE SUDUNG=1 ORDER BY NAME";
            myDT = clsMain.ReturnDataTable(sSQL);
            cboLoai_Phong.Properties.DataSource = myDT;
            cboLoai_Phong.EditValue = cboLoai_Phong.Properties.GetDataSourceValue(cboLoai_Phong.Properties.ValueMember, 0);

            sSQL = "SELECT MALOAIPHONG AS ID,TENLOAIPHONG AS NAME FROM KS_LOAIPHONG WHERE SUDUNG=1 ORDER BY NAME";
            myDT = clsMain.ReturnDataTable(sSQL);
            cboLoai_Phong1.Properties.DataSource = myDT;
            cboLoai_Phong1.EditValue = cboLoai_Phong1.Properties.GetDataSourceValue(cboLoai_Phong1.Properties.ValueMember, 0);

        }

        private void SetControlKS()
        {
            sMaKS = "";
            txtTen_KS.Text = "";
            txtGhiChu_KS.Text = "";
            chkSuDung_KS.Checked = true;
        }

        private void SetControlLau()
        {
            sMaLau = "";
            cboKhachSan_Lau.EditValue = cboKhachSan_Lau.Properties.GetDataSourceValue(cboKhachSan_Lau.Properties.ValueMember, 0);
            txtTen_Lau.Text = "";
            txtGhiChu_Lau.Text = "";
            chkSuDung_Lau.Checked = true;
            img_Lau.Image = null;
            img_Lau.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
        }

        private void SetControlPhong()
        {
            sMaPhong = "";
            cboLau_Phong.EditValue = cboLau_Phong.Properties.GetDataSourceValue(cboLau_Phong.Properties.ValueMember, 0);
            cboLoai_Phong.EditValue = cboLoai_Phong.Properties.GetDataSourceValue(cboLoai_Phong.Properties.ValueMember, 0);
            txtTen_Phong.Text = "";
            cboKichThuoc_Phong.Text = "Kích thước 1";
            chkSuDung_Phong.Checked = true;
            img_Phong.Image = null;
            img_Phong.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
        }

        private void CloseOpenControlKS(bool tf)
        {
            txtTen_KS.Properties.ReadOnly = tf;
            txtGhiChu_KS.Properties.ReadOnly = tf;
            chkSuDung_KS.Properties.ReadOnly = tf;
        }

        private void CloseOpenControlLau(bool tf)
        {
            cboKhachSan_Lau.Properties.ReadOnly = tf;
            txtTen_Lau.Properties.ReadOnly = tf;
            txtGhiChu_Lau.Properties.ReadOnly = tf;
            chkSuDung_Lau.Properties.ReadOnly = tf;
            img_Lau.Enabled = !tf;
        }

        private void CloseOpenControlPhong(bool tf)
        {
            cboLau_Phong.Properties.ReadOnly = tf;
            cboLoai_Phong.Properties.ReadOnly = tf;
            txtTen_Phong.Properties.ReadOnly = tf;
            cboKichThuoc_Phong.Properties.ReadOnly = tf;
            chkSuDung_Phong.Properties.ReadOnly = tf;
            img_Phong.Enabled = !tf;
        }

        DataTable myDT_KS = new DataTable();
        private void LoadDataKS()
        {
            SetControlKS();
            CloseOpenControlKS(true);
            String sSQL = "SELECT MAKHACHSAN AS ID, TENKHACHSAN AS NAME, GHICHU AS _STATUS, SUDUNG AS _USE FROM KS_KHACHSAN WHERE SUDUNG = 1 ORDER BY ID,NAME";
            myDT_KS = clsMain.ReturnDataTable(sSQL);
            pal_KS.Controls.Clear();
            pal_Lau.Controls.Clear();
            pal_Phong.Controls.Clear();
            foreach (DataRow dr in myDT_KS.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pal_KS.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["ID"].ToString();
                btn.Tag = dr["ID"].ToString();
                btn.Text = dr["NAME"].ToString();

                btn.Click += new EventHandler(btKS_Click);
                pal_KS.Controls.Add(btn);
            }
            if (pal_KS.VerticalScroll.Visible)
            {
                pnlLau.Width = 152;
            }
            else
            {
                pnlLau.Width = 135;
            }
        }

        private void btKS_Click(object sender, EventArgs e)
        {
            btnChonKhachSan_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_KS.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapNhat.ForeColor;
                }
            }

            sMaKS = btn.Tag.ToString();
            txtTen_KS.Text = myDT_KS.Select("ID = " + clsMain.SQLString(sMaKS))[0]["NAME"].ToString();
            txtGhiChu_KS.Text = myDT_KS.Select("ID = " + clsMain.SQLString(sMaKS))[0]["_STATUS"].ToString();
            chkSuDung_KS.Checked = bool.Parse(myDT_KS.Select("ID = " + clsMain.SQLString(sMaKS))[0]["_USE"].ToString());

            LoadDataLau(sMaKS);

            if (pal_Lau.Controls.Count > 0)
            {
                btLau_Click(pal_Lau.Controls[0], null);
            }

        }

        DataTable myDT_Lau = new DataTable();
        private void LoadDataLau(String pMaKS)
        {
            SetControlLau();
            CloseOpenControlLau(true);
            String sSQL = "SELECT MALAU AS ID, TENLAU AS NAME, GHICHU AS _STATUS, SUDUNG AS _USE, MAKHACHSAN AS ID_KHACHSAN,IMAGES FROM KS_LAU WHERE SUDUNG = 1 AND MAKHACHSAN = " + clsMain.SQLString(pMaKS) + " ORDER BY ID_KHACHSAN,ID,NAME";
            myDT_Lau = clsMain.ReturnDataTable(sSQL);
            pal_Lau.Controls.Clear();
            pal_Phong.Controls.Clear();
            foreach (DataRow dr in myDT_Lau.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pal_Lau.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["ID"].ToString();
                btn.Tag = dr["ID"].ToString();
                btn.Text = dr["NAME"].ToString();
                try{
                    byte[] tam = new byte[((byte[])dr["IMAGES"]).LongLength];
                    tam = (byte[])dr["IMAGES"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch{
                }

                btn.Click += new EventHandler(btLau_Click);
                pal_Lau.Controls.Add(btn);
            }
        }

        private void btLau_Click(object sender, EventArgs e)
        {
            btnChonLau_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_Lau.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapNhat.ForeColor; 
                }
            }

            sMaLau = btn.Tag.ToString();
            cboKhachSan_Lau.EditValue = int.Parse(myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["ID_KHACHSAN"].ToString());
            txtTen_Lau.Text = myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["NAME"].ToString();
            txtGhiChu_Lau.Text = myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["_STATUS"].ToString();
            chkSuDung_Lau.Checked = bool.Parse(myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["_USE"].ToString());
            try{
                byte[] tam = new byte[((byte[])myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["IMAGES"]).LongLength];
                tam = (byte[])myDT_Lau.Select("ID = " + clsMain.SQLString(sMaLau))[0]["IMAGES"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                img_Lau.Image = bm;
                img_Lau.BackgroundImage = null;
            }
            catch{
                img_Lau.Image = null;
                img_Lau.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
            LoadDataPhong(sMaLau, sPage);
        }

        DataTable myDT_Phong = new DataTable();
        private void LoadDataPhong(String pMaLau, String pPage)
        {
            SetControlPhong();
            CloseOpenControlPhong(true);

            String sSQL = "SELECT MAPHONG AS ID, TENPHONG AS NAME,KS_PHONG.GHICHU AS _STATUS, KS_PHONG.SUDUNG AS _USER, KS_PHONG.MALAU AS ID_LAU,KS_LAU.TENLAU AS NAME_LAU, KS_PHONG.MALOAIPHONG AS ID_LOAIPHONG,KS_LOAIPHONG.TENLOAIPHONG AS NAME_LOAIPHONG,KS_PHONG.WIDTH AS WIDTH,KS_PHONG.HEIGHT AS HEIGHT,KS_PHONG.LOCA_Y AS LOCA_Y,KS_PHONG.LOCA_X AS LOCA_X,KS_PHONG.IMAGES AS IMAGES,KS_PHONG.PAGE AS PAGE FROM KS_PHONG INNER JOIN KS_LOAIPHONG ON KS_PHONG.MALOAIPHONG = KS_LOAIPHONG.MALOAIPHONG INNER JOIN KS_LAU ON KS_LAU.MALAU = KS_PHONG.MALAU WHERE KS_PHONG.MALAU = " + clsMain.SQLString(pMaLau) + "and KS_PHONG.PAGE = " + clsMain.SQLString(pPage);
            sSQL += " ORDER BY MAPHONG";
            myDT_Phong = clsMain.ReturnDataTable(sSQL);
            pal_Phong.Controls.Clear();
            foreach (DataRow dr in myDT_Phong.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Font = pal_Phong.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["ID"].ToString();
                btn.Tag = dr["ID"].ToString();
                btn.Text = dr["NAME"].ToString();
                btn.Size = new Size(Convert.ToInt32(dr["WIDTH"].ToString()), Convert.ToInt32(dr["HEIGHT"].ToString()));
                btn.Location = new Point(Convert.ToInt32(dr["LOCA_X"].ToString()), Convert.ToInt32(dr["LOCA_Y"].ToString()));
                try
                {
                    byte[] tam = new byte[((byte[])dr["IMAGES"]).LongLength];
                    tam = (byte[])dr["IMAGES"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }

                btn.Click += new System.EventHandler(btPhong_Click);
                btn.MouseDown += new System.Windows.Forms.MouseEventHandler(btPhong_MouseDown);
                btn.MouseMove += new System.Windows.Forms.MouseEventHandler(btPhong_MouseMove);
                btn.MouseUp += new System.Windows.Forms.MouseEventHandler(btPhong_MouseUp);
                pal_Phong.Controls.Add(btn);
            }
        }

        private void btPhong_Click(object sender, EventArgs e)
        {
            btnChonPhong_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_Phong.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapNhat.ForeColor;
                }
            }

            sMaPhong = btn.Tag.ToString();
            cboLau_Phong.EditValue = int.Parse(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["ID_LAU"].ToString());
            cboLoai_Phong.EditValue = int.Parse(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["ID_LOAIPHONG"].ToString());
            txtTen_Phong.Text = myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["NAME"].ToString();
            if (Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["WIDTH"].ToString()) == 110 & Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["HEIGHT"].ToString()) == 80)
            {
                cboKichThuoc_Phong.Text = "Kích thước 1";
            }
            else if (Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["WIDTH"].ToString()) == 220 & Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["HEIGHT"].ToString()) == 80)
            {
                cboKichThuoc_Phong.Text = "Kích thước 2";
            }
            else if (Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["WIDTH"].ToString()) == 110 & Convert.ToInt32(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["HEIGHT"].ToString()) == 160)
            {
                cboKichThuoc_Phong.Text = "Kích thước 3";
            }
            else
            {
                cboKichThuoc_Phong.Text = "Kích thước 4";
            }
            chkSuDung_Phong.Checked = bool.Parse(myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["_USER"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["IMAGES"]).LongLength];
                tam = (byte[])myDT_Phong.Select("ID=" + clsMain.SQLString(sMaPhong))[0]["IMAGES"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                img_Phong.Image = bm;
                img_Phong.BackgroundImage = null;
            }
            catch
            {
                img_Phong.Image = null;
                img_Phong.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
        }

        private void btPhong_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void btPhong_MouseMove(object sender, MouseEventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                btn.Left = e.X + btn.Left - MouseDownLocation.X;
                btn.Top = e.Y + btn.Top - MouseDownLocation.Y;
            }
        }

        private void btPhong_MouseUp(object sender, MouseEventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.Left <= 0)
                btn.Left = 0;
            if (btn.Top <= 0)
                btn.Top = 0;
            if (btn.Left > pal_Phong.Width - btn.Width)
                btn.Left = pal_Phong.Width - btn.Size.Width;
            if (btn.Top > pal_Phong.Height - btn.Height)
                btn.Top = pal_Phong.Height - btn.Size.Height;
        }  

        private void btnChonLau_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_Lau;
            btnChonLau.ForeColor = Color.Red;
            btnChonPhong.ForeColor = btnCapNhat.ForeColor;
            btnChonKhachSan.ForeColor = btnCapNhat.ForeColor;
        }

        private void btnChonPhong_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_Phong;
            btnChonPhong.ForeColor = Color.Red;
            btnChonLau.ForeColor = btnCapNhat.ForeColor;
            btnChonKhachSan.ForeColor = btnCapNhat.ForeColor;
        }

        private void btnChonKhachSan_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_KhachSan;
            btnChonKhachSan.ForeColor = Color.Red;
            btnChonLau.ForeColor = btnCapNhat.ForeColor;
            btnChonPhong.ForeColor = btnCapNhat.ForeColor;
        }

        private void btnBanPhim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoadDataKS();
            if (pal_Lau.Controls.Count > 0)
            {
                btLau_Click(pal_Lau.Controls[0], null);
            }
        }

        private void btnThemLau_Click(object sender, EventArgs e)
        {
            SetControlLau();
            CloseOpenControlLau(false);
            cboKhachSan_Lau.Focus();
        }

        private void btnSuaLau_Click(object sender, EventArgs e)
        {
            if (sMaLau == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            CloseOpenControlLau(false);
            cboKhachSan_Lau.Focus();
        }

        private void btnXoaLau_Click(object sender, EventArgs e)
        {
            if (sMaLau == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (clsMain.ReturnDataTable("SELECT * FROM KS_PHONG WHERE MALAU = " + sMaLau).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lầu này đang có danh mục phòng. Không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            String sSQL = "Delete From KS_LAU Where MALAU = " + clsMain.SQLString(sMaLau);
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoadDataLau(sMaKS);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuLau_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Lau())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img_Lau.Image, typeof(Byte[]));
            SqlConnection conn = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (sMaLau == "" && btnThemLau.Enabled)
            {
                sSQL = "INSERT INTO KS_LAU(TENLAU,GHICHU,SUDUNG,MAKHACHSAN,IMAGES) VALUES (@TENLAU,@GHICHU,@SUDUNG,@MAKHACHSAN,@IMAGES)";
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENLAU", (object)txtTen_Lau.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGhiChu_Lau.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_Lau.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@MAKHACHSAN", (object)cboKhachSan_Lau.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@IMAGES", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if ((sMaLau != "" && btnSuaLau.Enabled))
            {
                sSQL = "UPDATE KS_LAU SET TENLAU = @TENLAU,GHICHU = @GHICHU,SUDUNG = @SUDUNG,MAKHACHSAN = @MAKHACHSAN,IMAGES = @IMAGES WHERE MALAU = " + clsMain.SQLString(sMaLau);
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENLAU", (object)txtTen_Lau.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGhiChu_Lau.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_Lau.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@MAKHACHSAN", (object)cboKhachSan_Lau.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@IMAGES", (object)imageData));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                conn.Open();
                SqlComMain.ExecuteNonQuery();
                conn.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoadCombo();
                LoadDataLau(sMaKS);
                btnThemLau.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput_KS()
        {
            if (txtTen_KS.Text == "")
            {
                XtraMessageBox.Show("Nhập tên Khách sạn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen_KS.Focus();
                return false;
            }
            if (sMaKS == "" && btnThemLau.Enabled && txtTen_KS.Text != "" && clsMain.ReturnDataTable("Select MAKHACHSAN From KS_KHACHSAN Where SUDUNG=1 And TENKHACHSAN = " + clsMain.SQLStringUnicode(txtTen_KS.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên Khách sạn, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_KS.Focus();
                    return false;
                }
            }
            if (sMaKS != "" && btnThemLau.Enabled && txtTen_KS.Text != "" && clsMain.ReturnDataTable("Select MAKHACHSAN From KS_KHACHSAN Where SUDUNG=1 And TENKHACHSAN = " + clsMain.SQLStringUnicode(txtTen_KS.Text) + " And MAKHACHSAN != " + clsMain.SQLString(sMaKS)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên Khách sạn, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_KS.Focus();
                    return false;
                }
            }
            return true;
        }

        private Boolean CheckInput_Lau()
        {
            if (txtTen_Lau.Text == "")
            {
                XtraMessageBox.Show("Nhập tên lầu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen_Lau.Focus();
                return false;
            }
            if (sMaLau == "" && btnThemLau.Enabled && txtTen_Lau.Text != "" && clsMain.ReturnDataTable("Select MALAU From KS_LAU Where SUDUNG=1 And TENLAU=" + clsMain.SQLStringUnicode(txtTen_Lau.Text) + " And MAKHACHSAN=" + clsMain.SQLStringUnicode(cboKhachSan_Lau.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên lầu, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_Lau.Focus();
                    return false;
                }
            }
            if (sMaLau != "" && btnThemLau.Enabled && txtTen_Lau.Text != "" && clsMain.ReturnDataTable("Select MALAU From KS_LAU Where SUDUNG=1 And TENLAU=" + clsMain.SQLStringUnicode(txtTen_Lau.Text) + " And MALAU != " + clsMain.SQLString(sMaLau) + " And MAKHACHSAN=" + clsMain.SQLStringUnicode(cboKhachSan_Lau.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên lầu, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_Lau.Focus();
                    return false;
                }
            }
            return true;
        }

        private Boolean CheckInput_Phong()
        {
            if (txtTen_Phong.Text == "")
            {
                XtraMessageBox.Show("Nhập tên phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen_Phong.Focus();
                return false;
            }
            if (sMaPhong == "" && btnThemPhong.Enabled && txtTen_Phong.Text != "" && clsMain.ReturnDataTable("Select MAPHONG From KS_PHONG Where SUDUNG=1 And TENPHONG = " + clsMain.SQLStringUnicode(txtTen_Phong.Text) + " And MALAU = " + clsMain.SQLStringUnicode(cboLau_Phong.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên bàn, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_Phong.Focus();
                    return false;
                }
            }
            if (sMaPhong != "" && btnThemPhong.Enabled && txtTen_Phong.Text != "" && clsMain.ReturnDataTable("Select MAPHONG From KS_PHONG Where SUDUNG=1 And TENPHONG = " + clsMain.SQLStringUnicode(txtTen_Phong.Text) + " And MAPHONG != " + clsMain.SQLString(sMaPhong) + " And MALAU = " + clsMain.SQLStringUnicode(cboLau_Phong.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Trùng tên bàn, bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTen_Phong.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnLauLen_Click(object sender, EventArgs e)
        {
            try{
                pal_Lau.AutoScrollPosition = new Point(0, pal_Lau.VerticalScroll.Value - pal_Lau.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnLauXuong_Click(object sender, EventArgs e)
        {
            try{
                pal_Lau.AutoScrollPosition = new Point(0, pal_Lau.VerticalScroll.Value + pal_Lau.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnKSLen_Click(object sender, EventArgs e)
        {
            try{
                pal_KS.AutoScrollPosition = new Point(0, pal_KS.VerticalScroll.Value - pal_KS.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnKSXuong_Click(object sender, EventArgs e)
        {
            try{
                pal_KS.AutoScrollPosition = new Point(0, pal_KS.VerticalScroll.Value + pal_KS.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnPhongLen_Click(object sender, EventArgs e)
        {
            String[] arr;
            arr = lblPageL.Text.Split('/');
            if (Convert.ToInt32(arr[0]) == 1)
                return;
            else
            {
                sPage = (Convert.ToInt32(arr[0]) - 1).ToString();
                lblPageL.Text = sPage + "/5";
                lblPageX.Text = sPage + "/5";
                pal_Phong.Controls.Clear();
                LoadDataPhong(sMaLau, sPage);
            }
        }

        private void btnPhongXuong_Click(object sender, EventArgs e)
        {
            String[] arr;
            arr = lblPageL.Text.Split('/');
            if (Convert.ToInt32(arr[0]) == 5)
                return;
            else
            {
                sPage = (Convert.ToInt32(arr[0]) + 1).ToString();
                lblPageL.Text = sPage + "/5";
                lblPageL.Text = sPage + "/5";
                pal_Phong.Controls.Clear();
                LoadDataPhong(sMaLau, sPage);
            }
        }

        private void btnLuuKS_Click(object sender, EventArgs e)
        {
            if (!CheckInput_KS())
            {
                return;
            }
            SqlConnection conn = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (sMaKS == "" && btnThemKS.Enabled)
            {
                sSQL = "INSERT INTO KS_KHACHSAN(TENKHACHSAN,GHICHU,SUDUNG) VALUES (@TENKS,@GHICHU,@SUDUNG)";
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENKS", (object)txtTen_KS.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGhiChu_KS.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_KS.Checked));
                SqlComMain = SqlCom;
            }
            else if (sMaKS != "" && btnSuaKS.Enabled)
            {
                sSQL = "UPDATE KS_KHACHSAN SET TENKHACHSAN = @TENKS,GHICHU = @GHICHU,SUDUNG = @SUDUNG WHERE MAKHACHSAN = " + clsMain.SQLString(sMaKS);
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENKS", (object)txtTen_KS.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGhiChu_KS.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_KS.Checked));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                conn.Open();
                SqlComMain.ExecuteNonQuery();
                conn.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoadCombo();
                LoadDataKS();
                btnThemKS.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThemKS_Click(object sender, EventArgs e)
        {
            SetControlKS();
            CloseOpenControlKS(false);
            txtTen_KS.Focus();
        }

        private void btnSuaKS_Click(object sender, EventArgs e)
        {
            if (sMaKS == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            CloseOpenControlKS(false);
        }

        private void btnXoaKS_Click(object sender, EventArgs e)
        {
            if (sMaKS == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            if (clsMain.ReturnDataTable("SELECT * FROM KS_LAU WHERE MAKHACHSAN = " + sMaKS).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Khách sạn này đang có danh mục lầu. Không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            String sSQL = "Delete From KS_KHACHSAN Where MAKHACHSAN = " + clsMain.SQLString(sMaKS);
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoadDataKS();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void img_Lau_Click(object sender, EventArgs e)
        {
            if (img_Lau.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        img_Lau.ImageLocation = openFileDialog1.FileName;
                        img_Lau.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        img_Lau.Image = null;
                        img_Lau.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnThemNhieuPhong_Click(object sender, EventArgs e)
        {
            Frame_ThemPhong.Visible = !Frame_ThemPhong.Visible;
            cboKichThuoc_Phong1.Text = "Kích thước 1";
            txtSo_Phong.Focus();
        }

        private void btn_PhongUp_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y - 1);
                    break;
                }
            }
        }

        private void btn_PhongUp_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y - 1);
                    break;
                }
            }
            timer_Up.Start();
        }

        private void btn_PhongUp_MouseUp(object sender, MouseEventArgs e)
        {
            timer_Up.Stop();
        }
        
        private void btn_PhongDown_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y + 1);
                    break;
                }
            }
        }

        private void btn_PhongDown_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y + 1);
                    break;
                }
            }
            timer_Down.Start();
        }

        private void btn_PhongDown_MouseUp(object sender, MouseEventArgs e)
        {
            timer_Down.Stop();
        }

        private void btn_PhongRigh_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X + 1, btn.Location.Y);
                    break;
                }
            }
        }

        private void btn_PhongRigh_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X + 1, btn.Location.Y);
                    break;
                }
            }
            timer_Righ.Start();
        }

        private void btn_PhongRigh_MouseUp(object sender, MouseEventArgs e)
        {
            timer_Righ.Stop();
        }

        private void btn_PhongLeft_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X - 1, btn.Location.Y);
                    break;
                }
            }
        }

        private void btn_PhongLeft_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                if (btn.Tag.ToString() == sMaPhong)
                {
                    btn.Location = new Point(btn.Location.X - 1, btn.Location.Y);
                    break;
                }
            }
            timer_Left.Start();
        }

        private void btn_PhongLeft_MouseUp(object sender, MouseEventArgs e)
        {
            timer_Left.Stop();
        }

        private void btnLuuSoDoPhong_Click(object sender, EventArgs e)
        {
            String sSQL = "";
            foreach (SimpleButton btn in pal_Phong.Controls)
            {
                sSQL += "UPDATE KS_PHONG SET LOCA_X = " + btn.Location.X + ", LOCA_Y = " + btn.Location.Y + " Where MAPHONG =" + clsMain.SQLString(btn.Tag.ToString()) + "\n";
            }
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Cập nhật thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLuuPhong_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Phong())
            {
                return;
            }
            float width = 110;
            float height = 80;
            if (cboKichThuoc_Phong.Text.Equals("Kích thước 2"))
                width *= 2;
            else if (cboKichThuoc_Phong.Text.Equals("Kích thước 3"))
                height *= 2;
            else if (cboKichThuoc_Phong.Text.Equals("Kích thước 4"))
            {
                width *= 2;
                height *= 2;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img_Phong.Image, typeof(Byte[]));
            SqlConnection conn = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (sMaPhong == "" && btnThemPhong.Enabled)
            {
                sSQL = "INSERT INTO KS_PHONG(TENPHONG,GHICHU,SUDUNG,MALAU,MALOAIPHONG,WIDTH,HEIGHT,LOCA_Y,LOCA_X,IMAGES,PAGE) values (@TENPHONG,@GHICHU,@SUDUNG,@MALAU,@MALOAIPHONG,@WIDTH,@HEIGHT,@LOCA_Y,@LOCA_X,@IMAGES,@PAGE)";
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENPHONG", (object)txtTen_Phong.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_Phong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@MALAU", (object)cboLau_Phong.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MALOAIPHONG", (object)cboLoai_Phong.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@WIDTH", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@HEIGHT", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@LOCA_Y", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@LOCA_X", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@IMAGES", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@PAGE", (object)sPage));
                SqlComMain = SqlCom;
            }
            else if (sMaPhong != "" && btnSuaPhong.Enabled)
            {
                sSQL = "UPDATE KS_PHONG SET TENPHONG=@TENPHONG,GHICHU=@GHICHU,SUDUNG=@SUDUNG,MALAU=@MALAU,MALOAIPHONG=@MALOAIPHONG,WIDTH=@WIDTH,HEIGHT=@HEIGHT,IMAGES=@IMAGES WHERE MAPHONG = " + clsMain.SQLString(sMaPhong);
                SqlCommand SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENPHONG", (object)txtTen_Phong.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_Phong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@MALAU", (object)cboLau_Phong.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MALOAIPHONG", (object)cboLoai_Phong.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@WIDTH", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@HEIGHT", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@IMAGES", (object)imageData));
                SqlComMain = SqlCom;
            }
            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                conn.Open();
                SqlComMain.ExecuteNonQuery();
                conn.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }
            if (bRunSQL)
            {

                pal_Phong.Controls.Clear();
                LoadDataPhong(sMaLau, sPage);
                btnThemPhong.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThemPhong_Click(object sender, EventArgs e)
        {
            SetControlPhong();
            CloseOpenControlPhong(false);
            cboLau_Phong.Focus();
        }

        private void btnSuaPhong_Click(object sender, EventArgs e)
        {
            if (sMaPhong == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            CloseOpenControlPhong(false);
            cboLau_Phong.Focus();
        }

        private void btnXoaPhong_Click(object sender, EventArgs e)
        {
            if (sMaPhong == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            //if (clsMain.ReturnDataTable("SELECT * FROM CT_MOBAN_KARAOKE WHERE MA_BAN = " + sMaPhong).Rows.Count > 0)
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("Phòng này đã được sử dụng. Không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            string sSQL = "";
            sSQL = "Delete From KS_PHONG Where MAPHONG = " + clsMain.SQLString(sMaPhong) + "\n";
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                pal_Phong.Controls.Clear();
                LoadDataPhong(sMaLau, sPage);
            }
            else
            {
                XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            Default(true);
        }

        private void btnLuuThemNhanh_Click(object sender, EventArgs e)
        {
            float width = 110;
            float height = 80;
            if (cboKichThuoc_Phong1.Text.Equals("Kích thước 2"))
            {
                width *= 2;
            }
            else if (cboKichThuoc_Phong1.Text.Equals("Kích thước 3"))
            {
                height *= 2;
            }
            else if (cboKichThuoc_Phong1.Text.Equals("Kích thước 4"))
            {
                width *= 2;
                height *= 2;
            }

            if (txtSo_Phong.Text == "")
            {
                XtraMessageBox.Show("Nhập số phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSo_Phong.Focus();
                return;
            }
            if (Convert.ToInt32(txtSo_Phong.Text) <= 0)
            {
                XtraMessageBox.Show("Số phòng phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSo_Phong.Focus();
                return;
            }

            Boolean bRunSQL = true;
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img_Phong1.Image, typeof(Byte[]));
            SqlConnection conn = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlCom = null;
            String sSQL = "";
            int n = Convert.ToInt32(txtSo_Phong.Text);
            int tenban = 1;
            string sqlTONTAI = "select COUNT(MAPHONG) AS SOPHONG from KS_PHONG where MALAU = " + clsMain.SQLString(sMaLau);
            DataTable dtTONTAI = clsMain.ReturnDataTable(sqlTONTAI);
            if (dtTONTAI.Rows.Count > 0)
            {
                tenban = Convert.ToInt32(dtTONTAI.Rows[0]["SOPHONG"].ToString()) + 1;
            }
            else
            {
            }
            for (int i = 0; i < n; i++)
            {
                tenban = tenban + i;
                sSQL = "INSERT INTO KS_PHONG(TENPHONG,GHICHU,SUDUNG,MALAU,MALOAIPHONG,WIDTH,HEIGHT,LOCA_Y,LOCA_X,IMAGES,PAGE) values (@TENPHONG,@GHICHU,@SUDUNG,@MALAU,@MALOAIPHONG,@WIDTH,@HEIGHT,@LOCA_Y,@LOCA_X,@IMAGES,@PAGE)";
                SqlCom = new SqlCommand(sSQL, conn);
                SqlCom.Parameters.Add(new SqlParameter("@TENPHONG", (object)"Phòng " + tenban.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSuDung_Phong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@MALAU", (object)sMaLau));
                SqlCom.Parameters.Add(new SqlParameter("@MALOAIPHONG", (object)cboLoai_Phong1.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@WIDTH", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@HEIGHT", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@LOCA_Y", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@LOCA_X", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@IMAGES", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@PAGE", (object)sPage));
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    conn.Open();
                    SqlCom.ExecuteNonQuery();
                    SqlCom = null;
                    conn.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                    tenban = tenban - i;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            if (bRunSQL)
            {
                Default(false);
                Frame_ThemPhong.Visible = false;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Default(bool ShowMessage)
        {
            bool dongy = true;
            if (ShowMessage)
            {
                if (DialogResult.Yes == (XtraMessageBox.Show("Quay về mặc định phòng ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    dongy = true;
                }
                else
                {
                    dongy = false;
                }
            }
            if (dongy)
            {
                String sqldefault = "";
                for (int h = 1; h <= 5; h++)
                {
                    string sql = "select MAPHONG from KS_PHONG where MALAU=" + clsMain.SQLString(sMaLau) + "and SUDUNG=1 and PAGE = " + clsMain.SQLString(h.ToString());
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    int wid = pal_Phong.Width / 115;
                    int du = pal_Phong.Width - (wid * 115);
                    int hei = pal_Phong.Height / 85;
                    if (dt.Rows.Count > 0)
                    {
                        int count = dt.Rows.Count;
                        int tempX = 1;
                        int tempY = 1;
                        int x = 2;
                        int y = 2;
                        for (int i = 0; i < count; i++)
                        {
                            if (tempX > wid)
                            {
                                y += 85;
                                x = 2;
                                tempX = 1;
                                tempY += 1;
                            }
                            if (tempY <= hei)
                                sqldefault += "UPDATE KS_PHONG SET WIDTH = 110, HEIGHT = 80, LOCA_X = " + x + ", LOCA_Y = " + y + " where MAPHONG=" + clsMain.SQLString(dt.Rows[i]["MAPHONG"].ToString()) + "\n";
                            else
                            {
                                int pt;//nhảy sang trang pt nếu bàn đầy ở trang h trừ khi h=5
                                if (h < 5)
                                    pt = h + 1;
                                else
                                    pt = h;
                                sqldefault += "UPDATE KS_PHONG SET WIDTH = 110, HEIGHT = 80, LOCA_X = " + (pal_Phong.Width - 115) + ", LOCA_Y = " + (pal_Phong.Height - 85) + ",PAGE = " + clsMain.SQLString(pt.ToString()) + " where MAPHONG=" + clsMain.SQLString(dt.Rows[i]["MAPHONG"].ToString()) + "\n";
                            }

                            x += 115 + (du / wid);
                            tempX += 1;
                        }
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sqldefault);
                this.Cursor = Cursors.Default;
                if (bRunSQL)
                {
                    pal_Phong.Controls.Clear();
                    LoadDataPhong(sMaLau, sPage);
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void img_Phong_Click(object sender, EventArgs e)
        {
            if (img_Phong.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        img_Phong.ImageLocation = openFileDialog1.FileName;
                        img_Phong.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        img_Phong.Image = null;
                        img_Phong.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void img_Phong1_Click(object sender, EventArgs e)
        {
            if (img_Phong1.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        img_Phong1.ImageLocation = openFileDialog1.FileName;
                        img_Phong1.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        img_Phong1.Image = null;
                        img_Phong1.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void timer_Up_Tick(object sender, EventArgs e)
        {
            String Second;
            Second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(Second);
            if (a % 1 == 0)
                btn_PhongUp_Click(sender, e);
        }

        private void timer_Down_Tick(object sender, EventArgs e)
        {
            String Second;
            Second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(Second);
            if (a % 1 == 0)
                btn_PhongDown_Click(sender, e);
        }

        private void timer_Left_Tick(object sender, EventArgs e)
        {
            String Second;
            Second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(Second);
            if (a % 1 == 0)
                btn_PhongLeft_Click(sender, e);
        }

        private void timer_Righ_Tick(object sender, EventArgs e)
        {
            String Second;
            Second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(Second);
            if (a % 1 == 0)
                btn_PhongRigh_Click(sender, e);
        }

       

        

        

        

       






     
    }
}