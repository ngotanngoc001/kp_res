﻿namespace KP_RES 
{
    partial class Frm_PhongKS_test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_PhongKS_test));
            this.palTop = new DevExpress.XtraEditors.PanelControl();
            this.palphong = new DevExpress.XtraEditors.PanelControl();
            this.txtDadat = new DevExpress.XtraEditors.TextEdit();
            this.txtDangsua = new DevExpress.XtraEditors.TextEdit();
            this.txtBanban = new DevExpress.XtraEditors.TextEdit();
            this.txtBansach = new DevExpress.XtraEditors.TextEdit();
            this.txtTrongsach = new DevExpress.XtraEditors.TextEdit();
            this.txtTrongban = new DevExpress.XtraEditors.TextEdit();
            this.txtCokhach = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabNhomphong = new DevExpress.XtraTab.XtraTabPage();
            this.palbttNhomphong = new DevExpress.XtraEditors.PanelControl();
            this.palNhom = new System.Windows.Forms.FlowLayoutPanel();
            this.bttTrai = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_Phai = new DevExpress.XtraEditors.SimpleButton();
            this.tabThongtinphong = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_thongtinphong = new DevExpress.XtraEditors.PanelControl();
            this.bttTTPTTG = new DevExpress.XtraEditors.SimpleButton();
            this.bttTKP = new DevExpress.XtraEditors.SimpleButton();
            this.bttKNCUP = new DevExpress.XtraEditors.SimpleButton();
            this.bttSDPHT = new DevExpress.XtraEditors.SimpleButton();
            this.tabBooking = new DevExpress.XtraTab.XtraTabPage();
            this.palBttBooking = new DevExpress.XtraEditors.PanelControl();
            this.btt_Book_DaiLy = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_DS = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_LS_luutru = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.bttBook_DK = new DevExpress.XtraEditors.SimpleButton();
            this.tabLetan = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_Letan = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.bttLetan_Vesinhphong = new DevExpress.XtraEditors.SimpleButton();
            this.bttLetan_Nguoidikem = new DevExpress.XtraEditors.SimpleButton();
            this.bttLetan_QLTamTru = new DevExpress.XtraEditors.SimpleButton();
            this.bttLetan_DSKhachDoan = new DevExpress.XtraEditors.SimpleButton();
            this.bttChuyenghep_phong = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton29 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton30 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton31 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton33 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton34 = new DevExpress.XtraEditors.SimpleButton();
            this.TabThungan = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_Thungan = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton40 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton32 = new DevExpress.XtraEditors.SimpleButton();
            this.bttThungan_Chothue = new DevExpress.XtraEditors.SimpleButton();
            this.bttThungan_Hoboi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton37 = new DevExpress.XtraEditors.SimpleButton();
            this.bttThungan_Spa = new DevExpress.XtraEditors.SimpleButton();
            this.bttThungan_Nhahang = new DevExpress.XtraEditors.SimpleButton();
            this.tabNghiepvu = new DevExpress.XtraTab.XtraTabPage();
            this.palbtt_Nghiepvu = new DevExpress.XtraEditors.PanelControl();
            this.bttThoat = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton42 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton43 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton44 = new DevExpress.XtraEditors.SimpleButton();
            this.bttNghiepvu_BCDoanhthu = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton47 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_luuban = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.Tabcontrol = new DevExpress.XtraTab.XtraTabControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabNgay = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.palTop)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palphong)).BeginInit();
            this.palphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDadat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangsua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBansach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrongsach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrongban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCokhach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            this.tabNhomphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbttNhomphong)).BeginInit();
            this.palbttNhomphong.SuspendLayout();
            this.tabThongtinphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_thongtinphong)).BeginInit();
            this.palbtt_thongtinphong.SuspendLayout();
            this.tabBooking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palBttBooking)).BeginInit();
            this.palBttBooking.SuspendLayout();
            this.tabLetan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Letan)).BeginInit();
            this.palbtt_Letan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.TabThungan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Thungan)).BeginInit();
            this.palbtt_Thungan.SuspendLayout();
            this.tabNghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Nghiepvu)).BeginInit();
            this.palbtt_Nghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tabcontrol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.palphong);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(1036, 48);
            this.palTop.TabIndex = 0;
            // 
            // palphong
            // 
            this.palphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palphong.Controls.Add(this.txtDadat);
            this.palphong.Controls.Add(this.txtDangsua);
            this.palphong.Controls.Add(this.txtBanban);
            this.palphong.Controls.Add(this.txtBansach);
            this.palphong.Controls.Add(this.txtTrongsach);
            this.palphong.Controls.Add(this.txtTrongban);
            this.palphong.Controls.Add(this.txtCokhach);
            this.palphong.Controls.Add(this.pictureEdit7);
            this.palphong.Controls.Add(this.pictureEdit1);
            this.palphong.Controls.Add(this.pictureEdit6);
            this.palphong.Controls.Add(this.pictureEdit2);
            this.palphong.Controls.Add(this.pictureEdit5);
            this.palphong.Controls.Add(this.pictureEdit3);
            this.palphong.Controls.Add(this.pictureEdit4);
            this.palphong.Location = new System.Drawing.Point(7, 3);
            this.palphong.Name = "palphong";
            this.palphong.Size = new System.Drawing.Size(1001, 41);
            this.palphong.TabIndex = 0;
            // 
            // txtDadat
            // 
            this.txtDadat.EditValue = "20";
            this.txtDadat.Enabled = false;
            this.txtDadat.EnterMoveNextControl = true;
            this.txtDadat.Location = new System.Drawing.Point(966, 8);
            this.txtDadat.Name = "txtDadat";
            this.txtDadat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDadat.Properties.Appearance.Options.UseFont = true;
            this.txtDadat.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDadat.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtDadat.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtDadat.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtDadat.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtDadat.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtDadat.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtDadat.Size = new System.Drawing.Size(27, 26);
            this.txtDadat.TabIndex = 18;
            // 
            // txtDangsua
            // 
            this.txtDangsua.EditValue = "23";
            this.txtDangsua.Enabled = false;
            this.txtDangsua.EnterMoveNextControl = true;
            this.txtDangsua.Location = new System.Drawing.Point(822, 8);
            this.txtDangsua.Name = "txtDangsua";
            this.txtDangsua.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDangsua.Properties.Appearance.Options.UseFont = true;
            this.txtDangsua.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDangsua.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtDangsua.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtDangsua.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtDangsua.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtDangsua.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtDangsua.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtDangsua.Size = new System.Drawing.Size(27, 26);
            this.txtDangsua.TabIndex = 17;
            // 
            // txtBanban
            // 
            this.txtBanban.EditValue = "15";
            this.txtBanban.Enabled = false;
            this.txtBanban.EnterMoveNextControl = true;
            this.txtBanban.Location = new System.Drawing.Point(678, 8);
            this.txtBanban.Name = "txtBanban";
            this.txtBanban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBanban.Properties.Appearance.Options.UseFont = true;
            this.txtBanban.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBanban.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtBanban.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtBanban.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtBanban.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtBanban.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtBanban.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtBanban.Size = new System.Drawing.Size(27, 26);
            this.txtBanban.TabIndex = 16;
            // 
            // txtBansach
            // 
            this.txtBansach.EditValue = "7";
            this.txtBansach.Enabled = false;
            this.txtBansach.EnterMoveNextControl = true;
            this.txtBansach.Location = new System.Drawing.Point(534, 8);
            this.txtBansach.Name = "txtBansach";
            this.txtBansach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBansach.Properties.Appearance.Options.UseFont = true;
            this.txtBansach.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBansach.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtBansach.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtBansach.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtBansach.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtBansach.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtBansach.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtBansach.Size = new System.Drawing.Size(27, 26);
            this.txtBansach.TabIndex = 15;
            // 
            // txtTrongsach
            // 
            this.txtTrongsach.EditValue = "30";
            this.txtTrongsach.Enabled = false;
            this.txtTrongsach.EnterMoveNextControl = true;
            this.txtTrongsach.Location = new System.Drawing.Point(246, 8);
            this.txtTrongsach.Name = "txtTrongsach";
            this.txtTrongsach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongsach.Properties.Appearance.Options.UseFont = true;
            this.txtTrongsach.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongsach.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtTrongsach.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrongsach.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtTrongsach.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrongsach.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrongsach.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrongsach.Size = new System.Drawing.Size(27, 26);
            this.txtTrongsach.TabIndex = 14;
            // 
            // txtTrongban
            // 
            this.txtTrongban.EditValue = "0";
            this.txtTrongban.Enabled = false;
            this.txtTrongban.EnterMoveNextControl = true;
            this.txtTrongban.Location = new System.Drawing.Point(390, 8);
            this.txtTrongban.Name = "txtTrongban";
            this.txtTrongban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongban.Properties.Appearance.Options.UseFont = true;
            this.txtTrongban.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongban.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtTrongban.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrongban.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtTrongban.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrongban.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrongban.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrongban.Size = new System.Drawing.Size(27, 26);
            this.txtTrongban.TabIndex = 13;
            // 
            // txtCokhach
            // 
            this.txtCokhach.EditValue = "10";
            this.txtCokhach.Enabled = false;
            this.txtCokhach.EnterMoveNextControl = true;
            this.txtCokhach.Location = new System.Drawing.Point(101, 8);
            this.txtCokhach.Name = "txtCokhach";
            this.txtCokhach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCokhach.Properties.Appearance.Options.UseFont = true;
            this.txtCokhach.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCokhach.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtCokhach.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtCokhach.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtCokhach.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtCokhach.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCokhach.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtCokhach.Size = new System.Drawing.Size(27, 26);
            this.txtCokhach.TabIndex = 4;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Enabled = false;
            this.pictureEdit7.Location = new System.Drawing.Point(871, 3);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(123)))), ((int)(((byte)(79)))));
            this.pictureEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.NullText = "P.Đã Đặt";
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit7.TabIndex = 12;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Enabled = false;
            this.pictureEdit1.Location = new System.Drawing.Point(7, 3);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.pictureEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.NullText = "P.Có Khách";
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit1.TabIndex = 0;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Enabled = false;
            this.pictureEdit6.Location = new System.Drawing.Point(727, 3);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(64)))), ((int)(((byte)(54)))));
            this.pictureEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit6.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit6.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.NullText = "P.Đang Sửa";
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit6.TabIndex = 11;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Enabled = false;
            this.pictureEdit2.Location = new System.Drawing.Point(151, 3);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(239)))));
            this.pictureEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit2.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.NullText = "P.Trống Sạch";
            this.pictureEdit2.Properties.ReadOnly = true;
            this.pictureEdit2.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit2.TabIndex = 7;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Enabled = false;
            this.pictureEdit5.Location = new System.Drawing.Point(583, 3);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(35)))));
            this.pictureEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.NullText = "P.Bận Bẩn";
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit5.TabIndex = 10;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Enabled = false;
            this.pictureEdit3.Location = new System.Drawing.Point(295, 3);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(165)))), ((int)(((byte)(80)))));
            this.pictureEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit3.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.NullText = "P.Trống Bẩn";
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit3.TabIndex = 8;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Enabled = false;
            this.pictureEdit4.Location = new System.Drawing.Point(439, 3);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(147)))), ((int)(((byte)(29)))));
            this.pictureEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit4.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.NullText = "P.Bận Sạch";
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Size = new System.Drawing.Size(89, 35);
            this.pictureEdit4.TabIndex = 9;
            // 
            // XtraTabControl1
            // 
            this.XtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.XtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
            this.XtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XtraTabControl1.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.XtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.SelectedTabPage = this.tabNhomphong;
            this.XtraTabControl1.Size = new System.Drawing.Size(852, 126);
            this.XtraTabControl1.TabIndex = 1;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabNhomphong,
            this.tabThongtinphong,
            this.tabBooking,
            this.tabLetan,
            this.TabThungan,
            this.tabNghiepvu});
            // 
            // tabNhomphong
            // 
            this.tabNhomphong.Controls.Add(this.palbttNhomphong);
            this.tabNhomphong.Name = "tabNhomphong";
            this.tabNhomphong.Size = new System.Drawing.Size(846, 92);
            this.tabNhomphong.Text = "Nhóm Phòng";
            // 
            // palbttNhomphong
            // 
            this.palbttNhomphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbttNhomphong.Controls.Add(this.palNhom);
            this.palbttNhomphong.Controls.Add(this.bttTrai);
            this.palbttNhomphong.Controls.Add(this.bnt_Phai);
            this.palbttNhomphong.Location = new System.Drawing.Point(8, 10);
            this.palbttNhomphong.Name = "palbttNhomphong";
            this.palbttNhomphong.Size = new System.Drawing.Size(822, 67);
            this.palbttNhomphong.TabIndex = 13;
            // 
            // palNhom
            // 
            this.palNhom.AutoScroll = true;
            this.palNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palNhom.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.palNhom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.palNhom.Location = new System.Drawing.Point(29, 0);
            this.palNhom.Name = "palNhom";
            this.palNhom.Size = new System.Drawing.Size(764, 67);
            this.palNhom.TabIndex = 0;
            // 
            // bttTrai
            // 
            this.bttTrai.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttTrai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttTrai.Appearance.Options.UseBackColor = true;
            this.bttTrai.Appearance.Options.UseFont = true;
            this.bttTrai.Dock = System.Windows.Forms.DockStyle.Left;
            this.bttTrai.Image = global::KP_RES.Properties.Resources.up_26;
            this.bttTrai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttTrai.Location = new System.Drawing.Point(0, 0);
            this.bttTrai.Margin = new System.Windows.Forms.Padding(4);
            this.bttTrai.Name = "bttTrai";
            this.bttTrai.Size = new System.Drawing.Size(29, 67);
            this.bttTrai.TabIndex = 28;
            this.bttTrai.Click += new System.EventHandler(this.bttTrai_Click);
            // 
            // bnt_Phai
            // 
            this.bnt_Phai.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Phai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Phai.Appearance.Options.UseBackColor = true;
            this.bnt_Phai.Appearance.Options.UseFont = true;
            this.bnt_Phai.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_Phai.Image = global::KP_RES.Properties.Resources.up33_26;
            this.bnt_Phai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Phai.Location = new System.Drawing.Point(793, 0);
            this.bnt_Phai.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Phai.Name = "bnt_Phai";
            this.bnt_Phai.Size = new System.Drawing.Size(29, 67);
            this.bnt_Phai.TabIndex = 27;
            this.bnt_Phai.Click += new System.EventHandler(this.bnt_Phai_Click);
            // 
            // tabThongtinphong
            // 
            this.tabThongtinphong.Controls.Add(this.palbtt_thongtinphong);
            this.tabThongtinphong.Name = "tabThongtinphong";
            this.tabThongtinphong.Size = new System.Drawing.Size(846, 92);
            this.tabThongtinphong.Text = "Thông Tin Phòng";
            // 
            // palbtt_thongtinphong
            // 
            this.palbtt_thongtinphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_thongtinphong.Controls.Add(this.bttTTPTTG);
            this.palbtt_thongtinphong.Controls.Add(this.bttTKP);
            this.palbtt_thongtinphong.Controls.Add(this.bttKNCUP);
            this.palbtt_thongtinphong.Controls.Add(this.bttSDPHT);
            this.palbtt_thongtinphong.Location = new System.Drawing.Point(73, 9);
            this.palbtt_thongtinphong.Name = "palbtt_thongtinphong";
            this.palbtt_thongtinphong.Size = new System.Drawing.Size(670, 76);
            this.palbtt_thongtinphong.TabIndex = 14;
            // 
            // bttTTPTTG
            // 
            this.bttTTPTTG.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttTTPTTG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttTTPTTG.Appearance.Options.UseBackColor = true;
            this.bttTTPTTG.Appearance.Options.UseFont = true;
            this.bttTTPTTG.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttTTPTTG.Location = new System.Drawing.Point(166, 9);
            this.bttTTPTTG.Margin = new System.Windows.Forms.Padding(4);
            this.bttTTPTTG.Name = "bttTTPTTG";
            this.bttTTPTTG.Size = new System.Drawing.Size(177, 58);
            this.bttTTPTTG.TabIndex = 31;
            this.bttTTPTTG.Text = "Thông Tin Phòng\r\nTheo Thời Gian";
            this.bttTTPTTG.Click += new System.EventHandler(this.bttTTPTTG_Click);
            // 
            // bttTKP
            // 
            this.bttTKP.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttTKP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttTKP.Appearance.Options.UseBackColor = true;
            this.bttTKP.Appearance.Options.UseFont = true;
            this.bttTKP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttTKP.Location = new System.Drawing.Point(532, 9);
            this.bttTKP.Margin = new System.Windows.Forms.Padding(4);
            this.bttTKP.Name = "bttTKP";
            this.bttTKP.Size = new System.Drawing.Size(134, 58);
            this.bttTKP.TabIndex = 34;
            this.bttTKP.Text = "Thống Kê\r\nPhòng";
            this.bttTKP.Click += new System.EventHandler(this.bttTKP_Click);
            // 
            // bttKNCUP
            // 
            this.bttKNCUP.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttKNCUP.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttKNCUP.Appearance.Options.UseBackColor = true;
            this.bttKNCUP.Appearance.Options.UseFont = true;
            this.bttKNCUP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttKNCUP.Location = new System.Drawing.Point(361, 9);
            this.bttKNCUP.Margin = new System.Windows.Forms.Padding(4);
            this.bttKNCUP.Name = "bttKNCUP";
            this.bttKNCUP.Size = new System.Drawing.Size(153, 58);
            this.bttKNCUP.TabIndex = 33;
            this.bttKNCUP.Text = "Khả Năng Cung\r\nỨng Phòng";
            this.bttKNCUP.Click += new System.EventHandler(this.bttKNCUP_Click);
            // 
            // bttSDPHT
            // 
            this.bttSDPHT.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttSDPHT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttSDPHT.Appearance.Options.UseBackColor = true;
            this.bttSDPHT.Appearance.Options.UseFont = true;
            this.bttSDPHT.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttSDPHT.Location = new System.Drawing.Point(9, 9);
            this.bttSDPHT.Margin = new System.Windows.Forms.Padding(4);
            this.bttSDPHT.Name = "bttSDPHT";
            this.bttSDPHT.Size = new System.Drawing.Size(139, 58);
            this.bttSDPHT.TabIndex = 29;
            this.bttSDPHT.Text = "Sơ Đồ Phòng\r\nHiện Tại";
            this.bttSDPHT.Click += new System.EventHandler(this.bttSDPHT_Click);
            // 
            // tabBooking
            // 
            this.tabBooking.Controls.Add(this.palBttBooking);
            this.tabBooking.Name = "tabBooking";
            this.tabBooking.Size = new System.Drawing.Size(846, 92);
            this.tabBooking.Text = "Booking";
            // 
            // palBttBooking
            // 
            this.palBttBooking.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palBttBooking.Controls.Add(this.btt_Book_DaiLy);
            this.palBttBooking.Controls.Add(this.simpleButton15);
            this.palBttBooking.Controls.Add(this.bttBook_DS);
            this.palBttBooking.Controls.Add(this.bttBook_LS_luutru);
            this.palBttBooking.Controls.Add(this.simpleButton19);
            this.palBttBooking.Controls.Add(this.simpleButton20);
            this.palBttBooking.Controls.Add(this.bttBook_DK);
            this.palBttBooking.Location = new System.Drawing.Point(8, 19);
            this.palBttBooking.Name = "palBttBooking";
            this.palBttBooking.Size = new System.Drawing.Size(822, 58);
            this.palBttBooking.TabIndex = 14;
            // 
            // btt_Book_DaiLy
            // 
            this.btt_Book_DaiLy.Appearance.BackColor = System.Drawing.Color.Silver;
            this.btt_Book_DaiLy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btt_Book_DaiLy.Appearance.Options.UseBackColor = true;
            this.btt_Book_DaiLy.Appearance.Options.UseFont = true;
            this.btt_Book_DaiLy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btt_Book_DaiLy.Location = new System.Drawing.Point(709, 0);
            this.btt_Book_DaiLy.Margin = new System.Windows.Forms.Padding(4);
            this.btt_Book_DaiLy.Name = "btt_Book_DaiLy";
            this.btt_Book_DaiLy.Size = new System.Drawing.Size(108, 58);
            this.btt_Book_DaiLy.TabIndex = 35;
            this.btt_Book_DaiLy.Text = "Đại Lý";
            this.btt_Book_DaiLy.Click += new System.EventHandler(this.btt_Book_DaiLy_Click);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton15.Appearance.Options.UseBackColor = true;
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton15.Location = new System.Drawing.Point(584, 0);
            this.simpleButton15.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(108, 58);
            this.simpleButton15.TabIndex = 34;
            this.simpleButton15.Text = "Xem\r\nChi Tiết";
            // 
            // bttBook_DS
            // 
            this.bttBook_DS.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_DS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_DS.Appearance.Options.UseBackColor = true;
            this.bttBook_DS.Appearance.Options.UseFont = true;
            this.bttBook_DS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_DS.Location = new System.Drawing.Point(465, 0);
            this.bttBook_DS.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_DS.Name = "bttBook_DS";
            this.bttBook_DS.Size = new System.Drawing.Size(102, 58);
            this.bttBook_DS.TabIndex = 33;
            this.bttBook_DS.Text = "Danh Sách\r\nBooking";
            this.bttBook_DS.Click += new System.EventHandler(this.bttBook_DS_Click);
            // 
            // bttBook_LS_luutru
            // 
            this.bttBook_LS_luutru.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_LS_luutru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_LS_luutru.Appearance.Options.UseBackColor = true;
            this.bttBook_LS_luutru.Appearance.Options.UseFont = true;
            this.bttBook_LS_luutru.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_LS_luutru.Location = new System.Drawing.Point(343, 0);
            this.bttBook_LS_luutru.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_LS_luutru.Name = "bttBook_LS_luutru";
            this.bttBook_LS_luutru.Size = new System.Drawing.Size(105, 58);
            this.bttBook_LS_luutru.TabIndex = 32;
            this.bttBook_LS_luutru.Text = "Lịch Sử\r\nLưu Trú";
            this.bttBook_LS_luutru.Click += new System.EventHandler(this.bttBook_LS_luutru_Click);
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton19.Appearance.Options.UseBackColor = true;
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton19.Location = new System.Drawing.Point(227, 0);
            this.simpleButton19.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(99, 58);
            this.simpleButton19.TabIndex = 31;
            this.simpleButton19.Text = "Sửa\r\nĐăng Ký";
            // 
            // simpleButton20
            // 
            this.simpleButton20.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton20.Appearance.Options.UseBackColor = true;
            this.simpleButton20.Appearance.Options.UseFont = true;
            this.simpleButton20.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton20.Location = new System.Drawing.Point(111, 0);
            this.simpleButton20.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(99, 58);
            this.simpleButton20.TabIndex = 30;
            this.simpleButton20.Text = "Hủy\r\nĐăng Ký";
            // 
            // bttBook_DK
            // 
            this.bttBook_DK.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttBook_DK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttBook_DK.Appearance.Options.UseBackColor = true;
            this.bttBook_DK.Appearance.Options.UseFont = true;
            this.bttBook_DK.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttBook_DK.Location = new System.Drawing.Point(2, 0);
            this.bttBook_DK.Margin = new System.Windows.Forms.Padding(4);
            this.bttBook_DK.Name = "bttBook_DK";
            this.bttBook_DK.Size = new System.Drawing.Size(92, 58);
            this.bttBook_DK.TabIndex = 29;
            this.bttBook_DK.Text = "Đăng Ký";
            this.bttBook_DK.Click += new System.EventHandler(this.bttBook_DK_Click);
            // 
            // tabLetan
            // 
            this.tabLetan.Controls.Add(this.palbtt_Letan);
            this.tabLetan.Name = "tabLetan";
            this.tabLetan.Size = new System.Drawing.Size(846, 92);
            this.tabLetan.Text = "Lễ Tân";
            // 
            // palbtt_Letan
            // 
            this.palbtt_Letan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_Letan.Controls.Add(this.panelControl8);
            this.palbtt_Letan.Controls.Add(this.panelControl9);
            this.palbtt_Letan.Location = new System.Drawing.Point(4, 4);
            this.palbtt_Letan.Name = "palbtt_Letan";
            this.palbtt_Letan.Size = new System.Drawing.Size(835, 87);
            this.palbtt_Letan.TabIndex = 36;
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.bttLetan_Vesinhphong);
            this.panelControl8.Controls.Add(this.bttLetan_Nguoidikem);
            this.panelControl8.Controls.Add(this.bttLetan_QLTamTru);
            this.panelControl8.Controls.Add(this.bttLetan_DSKhachDoan);
            this.panelControl8.Controls.Add(this.bttChuyenghep_phong);
            this.panelControl8.Controls.Add(this.simpleButton29);
            this.panelControl8.Location = new System.Drawing.Point(242, 4);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(590, 77);
            this.panelControl8.TabIndex = 15;
            // 
            // bttLetan_Vesinhphong
            // 
            this.bttLetan_Vesinhphong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttLetan_Vesinhphong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttLetan_Vesinhphong.Appearance.Options.UseBackColor = true;
            this.bttLetan_Vesinhphong.Appearance.Options.UseFont = true;
            this.bttLetan_Vesinhphong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttLetan_Vesinhphong.Location = new System.Drawing.Point(495, 10);
            this.bttLetan_Vesinhphong.Margin = new System.Windows.Forms.Padding(4);
            this.bttLetan_Vesinhphong.Name = "bttLetan_Vesinhphong";
            this.bttLetan_Vesinhphong.Size = new System.Drawing.Size(91, 58);
            this.bttLetan_Vesinhphong.TabIndex = 35;
            this.bttLetan_Vesinhphong.Text = "Vệ Sinh\r\nPhòng";
            this.bttLetan_Vesinhphong.Click += new System.EventHandler(this.bttLetan_Vesinhphong_Click);
            // 
            // bttLetan_Nguoidikem
            // 
            this.bttLetan_Nguoidikem.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttLetan_Nguoidikem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttLetan_Nguoidikem.Appearance.Options.UseBackColor = true;
            this.bttLetan_Nguoidikem.Appearance.Options.UseFont = true;
            this.bttLetan_Nguoidikem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttLetan_Nguoidikem.Location = new System.Drawing.Point(391, 10);
            this.bttLetan_Nguoidikem.Margin = new System.Windows.Forms.Padding(4);
            this.bttLetan_Nguoidikem.Name = "bttLetan_Nguoidikem";
            this.bttLetan_Nguoidikem.Size = new System.Drawing.Size(96, 58);
            this.bttLetan_Nguoidikem.TabIndex = 34;
            this.bttLetan_Nguoidikem.Text = "Người\r\nĐi Kèm";
            this.bttLetan_Nguoidikem.Click += new System.EventHandler(this.bttLetan_Nguoidikem_Click);
            // 
            // bttLetan_QLTamTru
            // 
            this.bttLetan_QLTamTru.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttLetan_QLTamTru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttLetan_QLTamTru.Appearance.Options.UseBackColor = true;
            this.bttLetan_QLTamTru.Appearance.Options.UseFont = true;
            this.bttLetan_QLTamTru.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttLetan_QLTamTru.Location = new System.Drawing.Point(295, 10);
            this.bttLetan_QLTamTru.Margin = new System.Windows.Forms.Padding(4);
            this.bttLetan_QLTamTru.Name = "bttLetan_QLTamTru";
            this.bttLetan_QLTamTru.Size = new System.Drawing.Size(88, 58);
            this.bttLetan_QLTamTru.TabIndex = 33;
            this.bttLetan_QLTamTru.Text = "Quản Lý\r\nTạm Trú";
            this.bttLetan_QLTamTru.Click += new System.EventHandler(this.bttLetan_QLTamTru_Click);
            // 
            // bttLetan_DSKhachDoan
            // 
            this.bttLetan_DSKhachDoan.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttLetan_DSKhachDoan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttLetan_DSKhachDoan.Appearance.Options.UseBackColor = true;
            this.bttLetan_DSKhachDoan.Appearance.Options.UseFont = true;
            this.bttLetan_DSKhachDoan.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttLetan_DSKhachDoan.Location = new System.Drawing.Point(198, 10);
            this.bttLetan_DSKhachDoan.Margin = new System.Windows.Forms.Padding(4);
            this.bttLetan_DSKhachDoan.Name = "bttLetan_DSKhachDoan";
            this.bttLetan_DSKhachDoan.Size = new System.Drawing.Size(89, 58);
            this.bttLetan_DSKhachDoan.TabIndex = 32;
            this.bttLetan_DSKhachDoan.Text = "DS Khách\r\nĐoàn";
            this.bttLetan_DSKhachDoan.Click += new System.EventHandler(this.bttLetan_DSKhachDoan_Click);
            // 
            // bttChuyenghep_phong
            // 
            this.bttChuyenghep_phong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttChuyenghep_phong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttChuyenghep_phong.Appearance.Options.UseBackColor = true;
            this.bttChuyenghep_phong.Appearance.Options.UseFont = true;
            this.bttChuyenghep_phong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttChuyenghep_phong.Location = new System.Drawing.Point(86, 10);
            this.bttChuyenghep_phong.Margin = new System.Windows.Forms.Padding(4);
            this.bttChuyenghep_phong.Name = "bttChuyenghep_phong";
            this.bttChuyenghep_phong.Size = new System.Drawing.Size(106, 58);
            this.bttChuyenghep_phong.TabIndex = 30;
            this.bttChuyenghep_phong.Text = "Chuyển/Ghép\r\nPhòng";
            this.bttChuyenghep_phong.Click += new System.EventHandler(this.bttChuyenghep_phong_Click);
            // 
            // simpleButton29
            // 
            this.simpleButton29.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton29.Appearance.Options.UseBackColor = true;
            this.simpleButton29.Appearance.Options.UseFont = true;
            this.simpleButton29.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton29.Location = new System.Drawing.Point(5, 10);
            this.simpleButton29.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton29.Name = "simpleButton29";
            this.simpleButton29.Size = new System.Drawing.Size(73, 58);
            this.simpleButton29.TabIndex = 29;
            this.simpleButton29.Text = "Walk In";
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.simpleButton30);
            this.panelControl9.Controls.Add(this.simpleButton31);
            this.panelControl9.Controls.Add(this.simpleButton33);
            this.panelControl9.Controls.Add(this.simpleButton34);
            this.panelControl9.Location = new System.Drawing.Point(1, 1);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(239, 85);
            this.panelControl9.TabIndex = 36;
            // 
            // simpleButton30
            // 
            this.simpleButton30.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton30.Appearance.Options.UseBackColor = true;
            this.simpleButton30.Appearance.Options.UseFont = true;
            this.simpleButton30.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton30.Location = new System.Drawing.Point(117, 45);
            this.simpleButton30.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton30.Name = "simpleButton30";
            this.simpleButton30.Size = new System.Drawing.Size(117, 36);
            this.simpleButton30.TabIndex = 34;
            this.simpleButton30.Text = "Hủy Check Out";
            // 
            // simpleButton31
            // 
            this.simpleButton31.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton31.Appearance.Options.UseBackColor = true;
            this.simpleButton31.Appearance.Options.UseFont = true;
            this.simpleButton31.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton31.Location = new System.Drawing.Point(4, 46);
            this.simpleButton31.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton31.Name = "simpleButton31";
            this.simpleButton31.Size = new System.Drawing.Size(105, 35);
            this.simpleButton31.TabIndex = 33;
            this.simpleButton31.Text = "Hủy Check In";
            // 
            // simpleButton33
            // 
            this.simpleButton33.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton33.Appearance.Options.UseBackColor = true;
            this.simpleButton33.Appearance.Options.UseFont = true;
            this.simpleButton33.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton33.Location = new System.Drawing.Point(117, 3);
            this.simpleButton33.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton33.Name = "simpleButton33";
            this.simpleButton33.Size = new System.Drawing.Size(117, 35);
            this.simpleButton33.TabIndex = 30;
            this.simpleButton33.Text = "Check Out";
            // 
            // simpleButton34
            // 
            this.simpleButton34.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton34.Appearance.Options.UseBackColor = true;
            this.simpleButton34.Appearance.Options.UseFont = true;
            this.simpleButton34.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton34.Location = new System.Drawing.Point(4, 3);
            this.simpleButton34.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton34.Name = "simpleButton34";
            this.simpleButton34.Size = new System.Drawing.Size(105, 35);
            this.simpleButton34.TabIndex = 29;
            this.simpleButton34.Text = "Check In";
            // 
            // TabThungan
            // 
            this.TabThungan.Controls.Add(this.palbtt_Thungan);
            this.TabThungan.Name = "TabThungan";
            this.TabThungan.Size = new System.Drawing.Size(846, 92);
            this.TabThungan.Text = "Thu Ngân";
            // 
            // palbtt_Thungan
            // 
            this.palbtt_Thungan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_Thungan.Controls.Add(this.simpleButton40);
            this.palbtt_Thungan.Controls.Add(this.simpleButton27);
            this.palbtt_Thungan.Controls.Add(this.simpleButton32);
            this.palbtt_Thungan.Controls.Add(this.bttThungan_Chothue);
            this.palbtt_Thungan.Controls.Add(this.bttThungan_Hoboi);
            this.palbtt_Thungan.Controls.Add(this.simpleButton37);
            this.palbtt_Thungan.Controls.Add(this.bttThungan_Spa);
            this.palbtt_Thungan.Controls.Add(this.bttThungan_Nhahang);
            this.palbtt_Thungan.Location = new System.Drawing.Point(9, 10);
            this.palbtt_Thungan.Name = "palbtt_Thungan";
            this.palbtt_Thungan.Size = new System.Drawing.Size(822, 71);
            this.palbtt_Thungan.TabIndex = 15;
            // 
            // simpleButton40
            // 
            this.simpleButton40.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton40.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton40.Appearance.Options.UseBackColor = true;
            this.simpleButton40.Appearance.Options.UseFont = true;
            this.simpleButton40.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton40.Location = new System.Drawing.Point(730, 6);
            this.simpleButton40.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton40.Name = "simpleButton40";
            this.simpleButton40.Size = new System.Drawing.Size(83, 58);
            this.simpleButton40.TabIndex = 36;
            this.simpleButton40.Text = "Hủy\r\nDịch Vụ";
            // 
            // simpleButton27
            // 
            this.simpleButton27.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton27.Appearance.Options.UseBackColor = true;
            this.simpleButton27.Appearance.Options.UseFont = true;
            this.simpleButton27.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton27.Location = new System.Drawing.Point(621, 6);
            this.simpleButton27.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(97, 58);
            this.simpleButton27.TabIndex = 35;
            this.simpleButton27.Text = "Tổng Đài\r\nĐiện Thoại";
            // 
            // simpleButton32
            // 
            this.simpleButton32.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton32.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton32.Appearance.Options.UseBackColor = true;
            this.simpleButton32.Appearance.Options.UseFont = true;
            this.simpleButton32.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton32.Location = new System.Drawing.Point(525, 6);
            this.simpleButton32.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton32.Name = "simpleButton32";
            this.simpleButton32.Size = new System.Drawing.Size(84, 58);
            this.simpleButton32.TabIndex = 34;
            this.simpleButton32.Text = "Dịch Vụ\r\nKhác";
            // 
            // bttThungan_Chothue
            // 
            this.bttThungan_Chothue.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThungan_Chothue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThungan_Chothue.Appearance.Options.UseBackColor = true;
            this.bttThungan_Chothue.Appearance.Options.UseFont = true;
            this.bttThungan_Chothue.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThungan_Chothue.Location = new System.Drawing.Point(417, 6);
            this.bttThungan_Chothue.Margin = new System.Windows.Forms.Padding(4);
            this.bttThungan_Chothue.Name = "bttThungan_Chothue";
            this.bttThungan_Chothue.Size = new System.Drawing.Size(96, 58);
            this.bttThungan_Chothue.TabIndex = 33;
            this.bttThungan_Chothue.Text = "Dịch Vụ\r\nCho Thuê";
            this.bttThungan_Chothue.Click += new System.EventHandler(this.bttThungan_Chothue_Click);
            // 
            // bttThungan_Hoboi
            // 
            this.bttThungan_Hoboi.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThungan_Hoboi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThungan_Hoboi.Appearance.Options.UseBackColor = true;
            this.bttThungan_Hoboi.Appearance.Options.UseFont = true;
            this.bttThungan_Hoboi.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThungan_Hoboi.Location = new System.Drawing.Point(314, 6);
            this.bttThungan_Hoboi.Margin = new System.Windows.Forms.Padding(4);
            this.bttThungan_Hoboi.Name = "bttThungan_Hoboi";
            this.bttThungan_Hoboi.Size = new System.Drawing.Size(91, 58);
            this.bttThungan_Hoboi.TabIndex = 32;
            this.bttThungan_Hoboi.Text = "Dịch Vụ\r\nHồ Bơi";
            this.bttThungan_Hoboi.Click += new System.EventHandler(this.bttThungan_Hoboi_Click);
            // 
            // simpleButton37
            // 
            this.simpleButton37.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton37.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton37.Appearance.Options.UseBackColor = true;
            this.simpleButton37.Appearance.Options.UseFont = true;
            this.simpleButton37.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton37.Location = new System.Drawing.Point(218, 6);
            this.simpleButton37.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton37.Name = "simpleButton37";
            this.simpleButton37.Size = new System.Drawing.Size(84, 58);
            this.simpleButton37.TabIndex = 31;
            this.simpleButton37.Text = "Dịch Vụ\r\nBida";
            // 
            // bttThungan_Spa
            // 
            this.bttThungan_Spa.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThungan_Spa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThungan_Spa.Appearance.Options.UseBackColor = true;
            this.bttThungan_Spa.Appearance.Options.UseFont = true;
            this.bttThungan_Spa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThungan_Spa.Location = new System.Drawing.Point(116, 6);
            this.bttThungan_Spa.Margin = new System.Windows.Forms.Padding(4);
            this.bttThungan_Spa.Name = "bttThungan_Spa";
            this.bttThungan_Spa.Size = new System.Drawing.Size(90, 58);
            this.bttThungan_Spa.TabIndex = 30;
            this.bttThungan_Spa.Text = "Dịch Vụ\r\nSPA";
            this.bttThungan_Spa.Click += new System.EventHandler(this.bttThungan_Spa_Click);
            // 
            // bttThungan_Nhahang
            // 
            this.bttThungan_Nhahang.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThungan_Nhahang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThungan_Nhahang.Appearance.Options.UseBackColor = true;
            this.bttThungan_Nhahang.Appearance.Options.UseFont = true;
            this.bttThungan_Nhahang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThungan_Nhahang.Location = new System.Drawing.Point(6, 6);
            this.bttThungan_Nhahang.Margin = new System.Windows.Forms.Padding(4);
            this.bttThungan_Nhahang.Name = "bttThungan_Nhahang";
            this.bttThungan_Nhahang.Size = new System.Drawing.Size(98, 58);
            this.bttThungan_Nhahang.TabIndex = 29;
            this.bttThungan_Nhahang.Text = "Dịch Vụ\r\nNhà Hàng";
            this.bttThungan_Nhahang.Click += new System.EventHandler(this.bttThungan_Nhahang_Click);
            // 
            // tabNghiepvu
            // 
            this.tabNghiepvu.Controls.Add(this.palbtt_Nghiepvu);
            this.tabNghiepvu.Name = "tabNghiepvu";
            this.tabNghiepvu.Size = new System.Drawing.Size(846, 92);
            this.tabNghiepvu.Text = "Nghiệp vụ";
            // 
            // palbtt_Nghiepvu
            // 
            this.palbtt_Nghiepvu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbtt_Nghiepvu.Controls.Add(this.bttThoat);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton42);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton43);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton44);
            this.palbtt_Nghiepvu.Controls.Add(this.bttNghiepvu_BCDoanhthu);
            this.palbtt_Nghiepvu.Controls.Add(this.simpleButton47);
            this.palbtt_Nghiepvu.Location = new System.Drawing.Point(7, 17);
            this.palbtt_Nghiepvu.Name = "palbtt_Nghiepvu";
            this.palbtt_Nghiepvu.Size = new System.Drawing.Size(822, 58);
            this.palbtt_Nghiepvu.TabIndex = 15;
            // 
            // bttThoat
            // 
            this.bttThoat.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttThoat.Appearance.Options.UseBackColor = true;
            this.bttThoat.Appearance.Options.UseFont = true;
            this.bttThoat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttThoat.Location = new System.Drawing.Point(690, 1);
            this.bttThoat.Margin = new System.Windows.Forms.Padding(4);
            this.bttThoat.Name = "bttThoat";
            this.bttThoat.Size = new System.Drawing.Size(129, 57);
            this.bttThoat.TabIndex = 35;
            this.bttThoat.Text = "Thoát";
            this.bttThoat.Click += new System.EventHandler(this.bttThoat_Click);
            // 
            // simpleButton42
            // 
            this.simpleButton42.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton42.Appearance.Options.UseBackColor = true;
            this.simpleButton42.Appearance.Options.UseFont = true;
            this.simpleButton42.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton42.Location = new System.Drawing.Point(553, 0);
            this.simpleButton42.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton42.Name = "simpleButton42";
            this.simpleButton42.Size = new System.Drawing.Size(125, 58);
            this.simpleButton42.TabIndex = 34;
            this.simpleButton42.Text = "Trợ Giúp";
            // 
            // simpleButton43
            // 
            this.simpleButton43.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton43.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton43.Appearance.Options.UseBackColor = true;
            this.simpleButton43.Appearance.Options.UseFont = true;
            this.simpleButton43.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton43.Location = new System.Drawing.Point(415, 0);
            this.simpleButton43.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton43.Name = "simpleButton43";
            this.simpleButton43.Size = new System.Drawing.Size(126, 58);
            this.simpleButton43.TabIndex = 33;
            this.simpleButton43.Text = "Mở Cash";
            // 
            // simpleButton44
            // 
            this.simpleButton44.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton44.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton44.Appearance.Options.UseBackColor = true;
            this.simpleButton44.Appearance.Options.UseFont = true;
            this.simpleButton44.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton44.Location = new System.Drawing.Point(283, 0);
            this.simpleButton44.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton44.Name = "simpleButton44";
            this.simpleButton44.Size = new System.Drawing.Size(120, 58);
            this.simpleButton44.TabIndex = 32;
            this.simpleButton44.Text = "Kết Ca";
            // 
            // bttNghiepvu_BCDoanhthu
            // 
            this.bttNghiepvu_BCDoanhthu.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bttNghiepvu_BCDoanhthu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttNghiepvu_BCDoanhthu.Appearance.Options.UseBackColor = true;
            this.bttNghiepvu_BCDoanhthu.Appearance.Options.UseFont = true;
            this.bttNghiepvu_BCDoanhthu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bttNghiepvu_BCDoanhthu.Location = new System.Drawing.Point(135, 0);
            this.bttNghiepvu_BCDoanhthu.Margin = new System.Windows.Forms.Padding(4);
            this.bttNghiepvu_BCDoanhthu.Name = "bttNghiepvu_BCDoanhthu";
            this.bttNghiepvu_BCDoanhthu.Size = new System.Drawing.Size(136, 58);
            this.bttNghiepvu_BCDoanhthu.TabIndex = 31;
            this.bttNghiepvu_BCDoanhthu.Text = "Báo Cáo\r\nDoanh Thu";
            this.bttNghiepvu_BCDoanhthu.Click += new System.EventHandler(this.bttNghiepvu_BCDoanhthu_Click);
            // 
            // simpleButton47
            // 
            this.simpleButton47.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton47.Appearance.Options.UseBackColor = true;
            this.simpleButton47.Appearance.Options.UseFont = true;
            this.simpleButton47.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton47.Location = new System.Drawing.Point(2, 0);
            this.simpleButton47.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton47.Name = "simpleButton47";
            this.simpleButton47.Size = new System.Drawing.Size(121, 58);
            this.simpleButton47.TabIndex = 29;
            this.simpleButton47.Text = "In Lại\r\nHóa Đơn";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.XtraTabControl1);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 502);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1036, 130);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.panelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(854, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(180, 126);
            this.panelControl4.TabIndex = 2;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.simpleButton3);
            this.panelControl5.Controls.Add(this.simpleButton2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(2, 58);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(176, 66);
            this.panelControl5.TabIndex = 4;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton3.Location = new System.Drawing.Point(0, 0);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(88, 66);
            this.simpleButton3.TabIndex = 12;
            this.simpleButton3.Text = "Xóa";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton2.Location = new System.Drawing.Point(88, 0);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(88, 66);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "Lưu";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.bnt_luuban);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(176, 56);
            this.panelControl1.TabIndex = 3;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton1.Location = new System.Drawing.Point(0, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(88, 56);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Thêm";
            // 
            // bnt_luuban
            // 
            this.bnt_luuban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luuban.Appearance.Options.UseFont = true;
            this.bnt_luuban.Dock = System.Windows.Forms.DockStyle.Right;
            this.bnt_luuban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_luuban.Location = new System.Drawing.Point(88, 0);
            this.bnt_luuban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_luuban.Name = "bnt_luuban";
            this.bnt_luuban.Size = new System.Drawing.Size(88, 56);
            this.bnt_luuban.TabIndex = 10;
            this.bnt_luuban.Text = "Sửa";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.Tabcontrol);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 48);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1036, 454);
            this.panelControl3.TabIndex = 1;
            // 
            // Tabcontrol
            // 
            this.Tabcontrol.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tabcontrol.AppearancePage.Header.Options.UseFont = true;
            this.Tabcontrol.AppearancePage.Header.Options.UseTextOptions = true;
            this.Tabcontrol.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tabcontrol.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Tabcontrol.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_1366x768_5;
            this.Tabcontrol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Tabcontrol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabcontrol.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.False;
            this.Tabcontrol.Location = new System.Drawing.Point(2, 2);
            this.Tabcontrol.Margin = new System.Windows.Forms.Padding(0);
            this.Tabcontrol.Name = "Tabcontrol";
            this.Tabcontrol.Size = new System.Drawing.Size(1032, 450);
            this.Tabcontrol.TabIndex = 2;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.panelControl3);
            this.panelControl6.Controls.Add(this.panelControl2);
            this.panelControl6.Controls.Add(this.palTop);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(0, 64);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1036, 632);
            this.panelControl6.TabIndex = 62;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.labelControl4);
            this.panel1.Controls.Add(this.labelControl3);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.LabNgay);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.lbgiay);
            this.panel1.Controls.Add(this.lbGio);
            this.panel1.Controls.Add(this.lbtimeby);
            this.panel1.Controls.Add(this.lb_NhanVien);
            this.panel1.Controls.Add(this.lbCaBan);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1036, 64);
            this.panel1.TabIndex = 63;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Brush Script MT", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelControl4.Location = new System.Drawing.Point(326, -1);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(282, 65);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "Paradise Hotel";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(685, 39);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(99, 19);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "7:00 - 14:00";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(685, 8);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(186, 19);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Nguyễn Thị Bích Tuyền";
            // 
            // LabNgay
            // 
            this.LabNgay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LabNgay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.LabNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LabNgay.Location = new System.Drawing.Point(940, 8);
            this.LabNgay.Name = "LabNgay";
            this.LabNgay.Size = new System.Drawing.Size(39, 19);
            this.LabNgay.TabIndex = 14;
            this.LabNgay.Text = "ngay";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(887, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(33, 19);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Giờ:";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbgiay.Location = new System.Drawing.Point(1001, 31);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 11;
            this.lbgiay.Text = "00";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbGio.Location = new System.Drawing.Point(950, 41);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 12;
            this.lbGio.Text = "00:00";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(652, 39);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(27, 19);
            this.lbtimeby.TabIndex = 8;
            this.lbtimeby.Text = "Ca:";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(652, 8);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(29, 19);
            this.lb_NhanVien.TabIndex = 10;
            this.lb_NhanVien.Text = "NV:";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(887, 8);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(47, 19);
            this.lbCaBan.TabIndex = 9;
            this.lbCaBan.Text = "Ngày:";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Frm_PhongKS_test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 696);
            this.Controls.Add(this.panelControl6);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_PhongKS_test";
            this.Text = "Khách sạn";
            this.Load += new System.EventHandler(this.Frm_PhongKS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.palTop)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palphong)).EndInit();
            this.palphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDadat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangsua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBansach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrongsach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrongban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCokhach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            this.tabNhomphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbttNhomphong)).EndInit();
            this.palbttNhomphong.ResumeLayout(false);
            this.tabThongtinphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_thongtinphong)).EndInit();
            this.palbtt_thongtinphong.ResumeLayout(false);
            this.tabBooking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palBttBooking)).EndInit();
            this.palBttBooking.ResumeLayout(false);
            this.tabLetan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Letan)).EndInit();
            this.palbtt_Letan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.TabThungan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Thungan)).EndInit();
            this.palbtt_Thungan.ResumeLayout(false);
            this.tabNghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbtt_Nghiepvu)).EndInit();
            this.palbtt_Nghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Tabcontrol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl palTop;
        private DevExpress.XtraTab.XtraTabControl XtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabNhomphong;
        private DevExpress.XtraTab.XtraTabPage tabThongtinphong;
        private DevExpress.XtraTab.XtraTabPage tabBooking;
        private DevExpress.XtraTab.XtraTabPage tabLetan;
        private DevExpress.XtraTab.XtraTabPage TabThungan;
        private DevExpress.XtraTab.XtraTabPage tabNghiepvu;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl palphong;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl palbttNhomphong;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton bnt_luuban;
        private DevExpress.XtraEditors.SimpleButton bttTrai;
        private DevExpress.XtraEditors.SimpleButton bnt_Phai;
        private DevExpress.XtraEditors.PanelControl palbtt_thongtinphong;
        private DevExpress.XtraEditors.SimpleButton bttTTPTTG;
        private DevExpress.XtraEditors.SimpleButton bttTKP;
        private DevExpress.XtraEditors.SimpleButton bttKNCUP;
        private DevExpress.XtraEditors.SimpleButton bttSDPHT;
        private DevExpress.XtraEditors.PanelControl palBttBooking;
        private DevExpress.XtraEditors.SimpleButton btt_Book_DaiLy;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton bttBook_DS;
        private DevExpress.XtraEditors.SimpleButton bttBook_LS_luutru;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton bttBook_DK;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton simpleButton30;
        private DevExpress.XtraEditors.SimpleButton simpleButton31;
        private DevExpress.XtraEditors.SimpleButton simpleButton33;
        private DevExpress.XtraEditors.SimpleButton simpleButton34;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton bttLetan_Vesinhphong;
        private DevExpress.XtraEditors.SimpleButton bttLetan_Nguoidikem;
        private DevExpress.XtraEditors.SimpleButton bttLetan_QLTamTru;
        private DevExpress.XtraEditors.SimpleButton bttLetan_DSKhachDoan;
        private DevExpress.XtraEditors.SimpleButton bttChuyenghep_phong;
        private DevExpress.XtraEditors.SimpleButton simpleButton29;
        private DevExpress.XtraEditors.PanelControl palbtt_Thungan;
        private DevExpress.XtraEditors.SimpleButton simpleButton40;
        private DevExpress.XtraEditors.SimpleButton simpleButton27;
        private DevExpress.XtraEditors.SimpleButton simpleButton32;
        private DevExpress.XtraEditors.SimpleButton bttThungan_Chothue;
        private DevExpress.XtraEditors.SimpleButton bttThungan_Hoboi;
        private DevExpress.XtraEditors.SimpleButton simpleButton37;
        private DevExpress.XtraEditors.SimpleButton bttThungan_Spa;
        private DevExpress.XtraEditors.SimpleButton bttThungan_Nhahang;
        private DevExpress.XtraEditors.PanelControl palbtt_Nghiepvu;
        private DevExpress.XtraEditors.SimpleButton bttThoat;
        private DevExpress.XtraEditors.SimpleButton simpleButton42;
        private DevExpress.XtraEditors.SimpleButton simpleButton43;
        private DevExpress.XtraEditors.SimpleButton simpleButton44;
        private DevExpress.XtraEditors.SimpleButton bttNghiepvu_BCDoanhthu;
        private DevExpress.XtraEditors.SimpleButton simpleButton47;
        private DevExpress.XtraEditors.TextEdit txtDadat;
        private DevExpress.XtraEditors.TextEdit txtDangsua;
        private DevExpress.XtraEditors.TextEdit txtBanban;
        private DevExpress.XtraEditors.TextEdit txtBansach;
        private DevExpress.XtraEditors.TextEdit txtTrongsach;
        private DevExpress.XtraEditors.TextEdit txtTrongban;
        private DevExpress.XtraEditors.TextEdit txtCokhach;
        private DevExpress.XtraEditors.PanelControl palbtt_Letan;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.LabelControl LabNgay;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraTab.XtraTabControl Tabcontrol;
        private System.Windows.Forms.FlowLayoutPanel palNhom;




    }
}