﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_TTP_TKP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.palTTP_TKP = new DevExpress.XtraEditors.PanelControl();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit54 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit55 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit50 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit52 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit49 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit51 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.palTTP_TKP)).BeginInit();
            this.palTTP_TKP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // palTTP_TKP
            // 
            this.palTTP_TKP.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palTTP_TKP.Controls.Add(this.panelControl22);
            this.palTTP_TKP.Controls.Add(this.panelControl21);
            this.palTTP_TKP.Controls.Add(this.panelControl18);
            this.palTTP_TKP.Location = new System.Drawing.Point(3, 3);
            this.palTTP_TKP.Name = "palTTP_TKP";
            this.palTTP_TKP.Size = new System.Drawing.Size(1012, 495);
            this.palTTP_TKP.TabIndex = 66;
            // 
            // panelControl22
            // 
            this.panelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl22.Controls.Add(this.textEdit33);
            this.panelControl22.Controls.Add(this.textEdit48);
            this.panelControl22.Controls.Add(this.labelControl24);
            this.panelControl22.Controls.Add(this.textEdit54);
            this.panelControl22.Controls.Add(this.textEdit55);
            this.panelControl22.Controls.Add(this.labelControl25);
            this.panelControl22.Location = new System.Drawing.Point(708, 9);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(299, 190);
            this.panelControl22.TabIndex = 70;
            // 
            // textEdit33
            // 
            this.textEdit33.EditValue = "10 %";
            this.textEdit33.Enabled = false;
            this.textEdit33.EnterMoveNextControl = true;
            this.textEdit33.Location = new System.Drawing.Point(227, 5);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.Appearance.Options.UseFont = true;
            this.textEdit33.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit33.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit33.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit33.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit33.Size = new System.Drawing.Size(54, 26);
            this.textEdit33.TabIndex = 71;
            // 
            // textEdit48
            // 
            this.textEdit48.EditValue = "10 %";
            this.textEdit48.Enabled = false;
            this.textEdit48.EnterMoveNextControl = true;
            this.textEdit48.Location = new System.Drawing.Point(227, 49);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.Appearance.Options.UseFont = true;
            this.textEdit48.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit48.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit48.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit48.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit48.Size = new System.Drawing.Size(54, 26);
            this.textEdit48.TabIndex = 70;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(4, 8);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(68, 19);
            this.labelControl24.TabIndex = 54;
            this.labelControl24.Text = "Khách Nữ";
            // 
            // textEdit54
            // 
            this.textEdit54.EditValue = "10";
            this.textEdit54.Enabled = false;
            this.textEdit54.EnterMoveNextControl = true;
            this.textEdit54.Location = new System.Drawing.Point(158, 49);
            this.textEdit54.Name = "textEdit54";
            this.textEdit54.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit54.Properties.Appearance.Options.UseFont = true;
            this.textEdit54.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit54.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit54.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit54.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit54.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit54.Size = new System.Drawing.Size(54, 26);
            this.textEdit54.TabIndex = 58;
            // 
            // textEdit55
            // 
            this.textEdit55.EditValue = "10";
            this.textEdit55.Enabled = false;
            this.textEdit55.EnterMoveNextControl = true;
            this.textEdit55.Location = new System.Drawing.Point(158, 5);
            this.textEdit55.Name = "textEdit55";
            this.textEdit55.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit55.Properties.Appearance.Options.UseFont = true;
            this.textEdit55.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit55.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit55.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit55.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit55.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit55.Size = new System.Drawing.Size(54, 26);
            this.textEdit55.TabIndex = 55;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(4, 52);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(80, 19);
            this.labelControl25.TabIndex = 57;
            this.labelControl25.Text = "Khách Nam";
            // 
            // panelControl21
            // 
            this.panelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl21.Controls.Add(this.textEdit31);
            this.panelControl21.Controls.Add(this.textEdit29);
            this.panelControl21.Controls.Add(this.textEdit50);
            this.panelControl21.Controls.Add(this.textEdit27);
            this.panelControl21.Controls.Add(this.textEdit52);
            this.panelControl21.Controls.Add(this.textEdit49);
            this.panelControl21.Controls.Add(this.labelControl21);
            this.panelControl21.Controls.Add(this.textEdit47);
            this.panelControl21.Controls.Add(this.labelControl14);
            this.panelControl21.Controls.Add(this.labelControl22);
            this.panelControl21.Controls.Add(this.textEdit28);
            this.panelControl21.Controls.Add(this.textEdit51);
            this.panelControl21.Controls.Add(this.textEdit34);
            this.panelControl21.Controls.Add(this.labelControl11);
            this.panelControl21.Controls.Add(this.labelControl13);
            this.panelControl21.Controls.Add(this.textEdit30);
            this.panelControl21.Controls.Add(this.textEdit32);
            this.panelControl21.Controls.Add(this.labelControl12);
            this.panelControl21.Controls.Add(this.textEdit38);
            this.panelControl21.Controls.Add(this.labelControl16);
            this.panelControl21.Controls.Add(this.textEdit39);
            this.panelControl21.Controls.Add(this.textEdit37);
            this.panelControl21.Controls.Add(this.textEdit42);
            this.panelControl21.Controls.Add(this.labelControl17);
            this.panelControl21.Controls.Add(this.textEdit43);
            this.panelControl21.Controls.Add(this.textEdit40);
            this.panelControl21.Controls.Add(this.textEdit46);
            this.panelControl21.Controls.Add(this.textEdit41);
            this.panelControl21.Controls.Add(this.labelControl18);
            this.panelControl21.Controls.Add(this.labelControl19);
            this.panelControl21.Controls.Add(this.textEdit44);
            this.panelControl21.Controls.Add(this.textEdit45);
            this.panelControl21.Controls.Add(this.labelControl20);
            this.panelControl21.Location = new System.Drawing.Point(345, 10);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(299, 476);
            this.panelControl21.TabIndex = 71;
            // 
            // textEdit31
            // 
            this.textEdit31.EditValue = "10 %";
            this.textEdit31.Enabled = false;
            this.textEdit31.EnterMoveNextControl = true;
            this.textEdit31.Location = new System.Drawing.Point(229, 48);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.Appearance.Options.UseFont = true;
            this.textEdit31.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit31.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit31.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit31.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit31.Size = new System.Drawing.Size(54, 26);
            this.textEdit31.TabIndex = 84;
            // 
            // textEdit29
            // 
            this.textEdit29.EditValue = "10 %";
            this.textEdit29.Enabled = false;
            this.textEdit29.EnterMoveNextControl = true;
            this.textEdit29.Location = new System.Drawing.Point(229, 92);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.Appearance.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit29.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit29.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit29.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit29.Size = new System.Drawing.Size(54, 26);
            this.textEdit29.TabIndex = 83;
            // 
            // textEdit50
            // 
            this.textEdit50.EditValue = "10 %";
            this.textEdit50.Enabled = false;
            this.textEdit50.EnterMoveNextControl = true;
            this.textEdit50.Location = new System.Drawing.Point(229, 444);
            this.textEdit50.Name = "textEdit50";
            this.textEdit50.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit50.Properties.Appearance.Options.UseFont = true;
            this.textEdit50.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit50.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit50.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit50.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit50.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit50.Size = new System.Drawing.Size(54, 26);
            this.textEdit50.TabIndex = 69;
            // 
            // textEdit27
            // 
            this.textEdit27.EditValue = "10 %";
            this.textEdit27.Enabled = false;
            this.textEdit27.EnterMoveNextControl = true;
            this.textEdit27.Location = new System.Drawing.Point(229, 136);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.Appearance.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit27.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit27.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit27.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit27.Size = new System.Drawing.Size(54, 26);
            this.textEdit27.TabIndex = 82;
            // 
            // textEdit52
            // 
            this.textEdit52.EditValue = "10 %";
            this.textEdit52.Enabled = false;
            this.textEdit52.EnterMoveNextControl = true;
            this.textEdit52.Location = new System.Drawing.Point(229, 400);
            this.textEdit52.Name = "textEdit52";
            this.textEdit52.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit52.Properties.Appearance.Options.UseFont = true;
            this.textEdit52.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit52.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit52.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit52.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit52.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit52.Size = new System.Drawing.Size(54, 26);
            this.textEdit52.TabIndex = 68;
            // 
            // textEdit49
            // 
            this.textEdit49.EditValue = "10 %";
            this.textEdit49.Enabled = false;
            this.textEdit49.EnterMoveNextControl = true;
            this.textEdit49.Location = new System.Drawing.Point(229, 4);
            this.textEdit49.Name = "textEdit49";
            this.textEdit49.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit49.Properties.Appearance.Options.UseFont = true;
            this.textEdit49.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit49.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit49.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit49.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit49.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit49.Size = new System.Drawing.Size(54, 26);
            this.textEdit49.TabIndex = 81;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(5, 403);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(84, 19);
            this.labelControl21.TabIndex = 48;
            this.labelControl21.Text = "Khách Đoàn";
            // 
            // textEdit47
            // 
            this.textEdit47.EditValue = "10";
            this.textEdit47.Enabled = false;
            this.textEdit47.EnterMoveNextControl = true;
            this.textEdit47.Location = new System.Drawing.Point(159, 400);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.Appearance.Options.UseFont = true;
            this.textEdit47.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit47.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit47.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit47.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit47.Size = new System.Drawing.Size(54, 26);
            this.textEdit47.TabIndex = 49;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(5, 7);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(146, 19);
            this.labelControl14.TabIndex = 73;
            this.labelControl14.Text = "Khách Đang Lưu Trú";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(5, 447);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(108, 19);
            this.labelControl22.TabIndex = 51;
            this.labelControl22.Text = "Khách Vãng Lai";
            // 
            // textEdit28
            // 
            this.textEdit28.EditValue = "10";
            this.textEdit28.Enabled = false;
            this.textEdit28.EnterMoveNextControl = true;
            this.textEdit28.Location = new System.Drawing.Point(159, 136);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.Appearance.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit28.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit28.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit28.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit28.Size = new System.Drawing.Size(54, 26);
            this.textEdit28.TabIndex = 80;
            // 
            // textEdit51
            // 
            this.textEdit51.EditValue = "10";
            this.textEdit51.Enabled = false;
            this.textEdit51.EnterMoveNextControl = true;
            this.textEdit51.Location = new System.Drawing.Point(159, 444);
            this.textEdit51.Name = "textEdit51";
            this.textEdit51.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit51.Properties.Appearance.Options.UseFont = true;
            this.textEdit51.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit51.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit51.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit51.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit51.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit51.Size = new System.Drawing.Size(54, 26);
            this.textEdit51.TabIndex = 52;
            // 
            // textEdit34
            // 
            this.textEdit34.EditValue = "10";
            this.textEdit34.Enabled = false;
            this.textEdit34.EnterMoveNextControl = true;
            this.textEdit34.Location = new System.Drawing.Point(159, 4);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.Appearance.Options.UseFont = true;
            this.textEdit34.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit34.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit34.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit34.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit34.Size = new System.Drawing.Size(54, 26);
            this.textEdit34.TabIndex = 74;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(5, 139);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(136, 19);
            this.labelControl11.TabIndex = 79;
            this.labelControl11.Text = "Khách Đến Dự Kiến";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(5, 51);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(75, 19);
            this.labelControl13.TabIndex = 75;
            this.labelControl13.Text = "Khách Đến";
            // 
            // textEdit30
            // 
            this.textEdit30.EditValue = "10";
            this.textEdit30.Enabled = false;
            this.textEdit30.EnterMoveNextControl = true;
            this.textEdit30.Location = new System.Drawing.Point(159, 92);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.Appearance.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit30.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit30.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit30.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit30.Size = new System.Drawing.Size(54, 26);
            this.textEdit30.TabIndex = 78;
            // 
            // textEdit32
            // 
            this.textEdit32.EditValue = "10";
            this.textEdit32.Enabled = false;
            this.textEdit32.EnterMoveNextControl = true;
            this.textEdit32.Location = new System.Drawing.Point(159, 48);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.Appearance.Options.UseFont = true;
            this.textEdit32.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit32.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit32.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit32.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit32.Size = new System.Drawing.Size(54, 26);
            this.textEdit32.TabIndex = 76;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(5, 95);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(65, 19);
            this.labelControl12.TabIndex = 77;
            this.labelControl12.Text = "Khách Về";
            // 
            // textEdit38
            // 
            this.textEdit38.EditValue = "10 %";
            this.textEdit38.Enabled = false;
            this.textEdit38.EnterMoveNextControl = true;
            this.textEdit38.Location = new System.Drawing.Point(229, 268);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.Appearance.Options.UseFont = true;
            this.textEdit38.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit38.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit38.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit38.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit38.Size = new System.Drawing.Size(54, 26);
            this.textEdit38.TabIndex = 72;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(4, 183);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(126, 19);
            this.labelControl16.TabIndex = 48;
            this.labelControl16.Text = "Khách Về Dự Kiến";
            // 
            // textEdit39
            // 
            this.textEdit39.EditValue = "10 %";
            this.textEdit39.Enabled = false;
            this.textEdit39.EnterMoveNextControl = true;
            this.textEdit39.Location = new System.Drawing.Point(229, 312);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.Appearance.Options.UseFont = true;
            this.textEdit39.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit39.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit39.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit39.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit39.Size = new System.Drawing.Size(54, 26);
            this.textEdit39.TabIndex = 71;
            // 
            // textEdit37
            // 
            this.textEdit37.EditValue = "10";
            this.textEdit37.Enabled = false;
            this.textEdit37.EnterMoveNextControl = true;
            this.textEdit37.Location = new System.Drawing.Point(159, 180);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.Appearance.Options.UseFont = true;
            this.textEdit37.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit37.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit37.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit37.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit37.Size = new System.Drawing.Size(54, 26);
            this.textEdit37.TabIndex = 49;
            // 
            // textEdit42
            // 
            this.textEdit42.EditValue = "10 %";
            this.textEdit42.Enabled = false;
            this.textEdit42.EnterMoveNextControl = true;
            this.textEdit42.Location = new System.Drawing.Point(229, 356);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.Appearance.Options.UseFont = true;
            this.textEdit42.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit42.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit42.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit42.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit42.Size = new System.Drawing.Size(54, 26);
            this.textEdit42.TabIndex = 70;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(4, 227);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(74, 19);
            this.labelControl17.TabIndex = 51;
            this.labelControl17.Text = "Người Lớn";
            // 
            // textEdit43
            // 
            this.textEdit43.EditValue = "10 %";
            this.textEdit43.Enabled = false;
            this.textEdit43.EnterMoveNextControl = true;
            this.textEdit43.Location = new System.Drawing.Point(229, 224);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.Appearance.Options.UseFont = true;
            this.textEdit43.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit43.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit43.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit43.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit43.Size = new System.Drawing.Size(54, 26);
            this.textEdit43.TabIndex = 69;
            // 
            // textEdit40
            // 
            this.textEdit40.EditValue = "10";
            this.textEdit40.Enabled = false;
            this.textEdit40.EnterMoveNextControl = true;
            this.textEdit40.Location = new System.Drawing.Point(159, 356);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.Appearance.Options.UseFont = true;
            this.textEdit40.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit40.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit40.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit40.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit40.Size = new System.Drawing.Size(54, 26);
            this.textEdit40.TabIndex = 61;
            // 
            // textEdit46
            // 
            this.textEdit46.EditValue = "10 %";
            this.textEdit46.Enabled = false;
            this.textEdit46.EnterMoveNextControl = true;
            this.textEdit46.Location = new System.Drawing.Point(229, 180);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.Appearance.Options.UseFont = true;
            this.textEdit46.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit46.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit46.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit46.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit46.Size = new System.Drawing.Size(54, 26);
            this.textEdit46.TabIndex = 68;
            // 
            // textEdit41
            // 
            this.textEdit41.EditValue = "10";
            this.textEdit41.Enabled = false;
            this.textEdit41.EnterMoveNextControl = true;
            this.textEdit41.Location = new System.Drawing.Point(159, 224);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.Appearance.Options.UseFont = true;
            this.textEdit41.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit41.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit41.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit41.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit41.Size = new System.Drawing.Size(54, 26);
            this.textEdit41.TabIndex = 52;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(5, 359);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(81, 19);
            this.labelControl18.TabIndex = 60;
            this.labelControl18.Text = "Nước ngoài";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(5, 271);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(52, 19);
            this.labelControl19.TabIndex = 54;
            this.labelControl19.Text = "Trẻ Em";
            // 
            // textEdit44
            // 
            this.textEdit44.EditValue = "10";
            this.textEdit44.Enabled = false;
            this.textEdit44.EnterMoveNextControl = true;
            this.textEdit44.Location = new System.Drawing.Point(159, 312);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.Appearance.Options.UseFont = true;
            this.textEdit44.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit44.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit44.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit44.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit44.Size = new System.Drawing.Size(54, 26);
            this.textEdit44.TabIndex = 58;
            // 
            // textEdit45
            // 
            this.textEdit45.EditValue = "10";
            this.textEdit45.Enabled = false;
            this.textEdit45.EnterMoveNextControl = true;
            this.textEdit45.Location = new System.Drawing.Point(159, 268);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.Appearance.Options.UseFont = true;
            this.textEdit45.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit45.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit45.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit45.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit45.Size = new System.Drawing.Size(54, 26);
            this.textEdit45.TabIndex = 55;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(5, 315);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(65, 19);
            this.labelControl20.TabIndex = 57;
            this.labelControl20.Text = "Việt Nam";
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.textEdit35);
            this.panelControl18.Controls.Add(this.labelControl15);
            this.panelControl18.Controls.Add(this.textEdit36);
            this.panelControl18.Controls.Add(this.labelControl10);
            this.panelControl18.Controls.Add(this.textEdit26);
            this.panelControl18.Controls.Add(this.textEdit25);
            this.panelControl18.Controls.Add(this.labelControl9);
            this.panelControl18.Controls.Add(this.textEdit24);
            this.panelControl18.Controls.Add(this.textEdit23);
            this.panelControl18.Controls.Add(this.labelControl8);
            this.panelControl18.Controls.Add(this.textEdit22);
            this.panelControl18.Controls.Add(this.textEdit21);
            this.panelControl18.Controls.Add(this.labelControl7);
            this.panelControl18.Controls.Add(this.textEdit20);
            this.panelControl18.Controls.Add(this.textEdit19);
            this.panelControl18.Controls.Add(this.labelControl6);
            this.panelControl18.Controls.Add(this.textEdit18);
            this.panelControl18.Controls.Add(this.textEdit17);
            this.panelControl18.Controls.Add(this.labelControl5);
            this.panelControl18.Controls.Add(this.textEdit7);
            this.panelControl18.Controls.Add(this.textEdit8);
            this.panelControl18.Controls.Add(this.labelControl1);
            this.panelControl18.Controls.Add(this.textEdit10);
            this.panelControl18.Controls.Add(this.textEdit9);
            this.panelControl18.Controls.Add(this.labelControl2);
            this.panelControl18.Controls.Add(this.textEdit12);
            this.panelControl18.Controls.Add(this.textEdit11);
            this.panelControl18.Controls.Add(this.labelControl3);
            this.panelControl18.Controls.Add(this.textEdit14);
            this.panelControl18.Controls.Add(this.textEdit13);
            this.panelControl18.Controls.Add(this.labelControl4);
            this.panelControl18.Controls.Add(this.textEdit16);
            this.panelControl18.Controls.Add(this.textEdit15);
            this.panelControl18.Location = new System.Drawing.Point(5, 9);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(276, 477);
            this.panelControl18.TabIndex = 69;
            // 
            // textEdit35
            // 
            this.textEdit35.EditValue = "10 %";
            this.textEdit35.Enabled = false;
            this.textEdit35.EnterMoveNextControl = true;
            this.textEdit35.Location = new System.Drawing.Point(206, 445);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.Appearance.Options.UseFont = true;
            this.textEdit35.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit35.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit35.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit35.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit35.Size = new System.Drawing.Size(54, 26);
            this.textEdit35.TabIndex = 66;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(4, 448);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(75, 19);
            this.labelControl15.TabIndex = 64;
            this.labelControl15.Text = "Phòng VIP";
            // 
            // textEdit36
            // 
            this.textEdit36.EditValue = "10";
            this.textEdit36.Enabled = false;
            this.textEdit36.EnterMoveNextControl = true;
            this.textEdit36.Location = new System.Drawing.Point(136, 445);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.Appearance.Options.UseFont = true;
            this.textEdit36.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit36.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit36.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit36.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit36.Size = new System.Drawing.Size(54, 26);
            this.textEdit36.TabIndex = 65;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(4, 228);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(107, 19);
            this.labelControl10.TabIndex = 48;
            this.labelControl10.Text = "Phòng bận bẩn";
            // 
            // textEdit26
            // 
            this.textEdit26.EditValue = "15";
            this.textEdit26.Enabled = false;
            this.textEdit26.EnterMoveNextControl = true;
            this.textEdit26.Location = new System.Drawing.Point(136, 225);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.Appearance.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit26.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit26.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit26.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit26.Size = new System.Drawing.Size(54, 26);
            this.textEdit26.TabIndex = 49;
            // 
            // textEdit25
            // 
            this.textEdit25.EditValue = "15 %";
            this.textEdit25.Enabled = false;
            this.textEdit25.EnterMoveNextControl = true;
            this.textEdit25.Location = new System.Drawing.Point(206, 225);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.Appearance.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit25.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit25.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit25.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit25.Size = new System.Drawing.Size(54, 26);
            this.textEdit25.TabIndex = 50;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(4, 272);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(115, 19);
            this.labelControl9.TabIndex = 51;
            this.labelControl9.Text = "Phòng đang sửa";
            // 
            // textEdit24
            // 
            this.textEdit24.EditValue = "23";
            this.textEdit24.Enabled = false;
            this.textEdit24.EnterMoveNextControl = true;
            this.textEdit24.Location = new System.Drawing.Point(136, 269);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.Appearance.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit24.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit24.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit24.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit24.Size = new System.Drawing.Size(54, 26);
            this.textEdit24.TabIndex = 52;
            // 
            // textEdit23
            // 
            this.textEdit23.EditValue = "23 %";
            this.textEdit23.Enabled = false;
            this.textEdit23.EnterMoveNextControl = true;
            this.textEdit23.Location = new System.Drawing.Point(206, 269);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.Appearance.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit23.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit23.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit23.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit23.Size = new System.Drawing.Size(54, 26);
            this.textEdit23.TabIndex = 53;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(4, 316);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(74, 19);
            this.labelControl8.TabIndex = 54;
            this.labelControl8.Text = "Phòng Đặt";
            // 
            // textEdit22
            // 
            this.textEdit22.EditValue = "30";
            this.textEdit22.Enabled = false;
            this.textEdit22.EnterMoveNextControl = true;
            this.textEdit22.Location = new System.Drawing.Point(136, 313);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit22.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit22.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit22.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit22.Size = new System.Drawing.Size(54, 26);
            this.textEdit22.TabIndex = 55;
            // 
            // textEdit21
            // 
            this.textEdit21.EditValue = "30 %";
            this.textEdit21.Enabled = false;
            this.textEdit21.EnterMoveNextControl = true;
            this.textEdit21.Location = new System.Drawing.Point(206, 313);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit21.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit21.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit21.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit21.Size = new System.Drawing.Size(54, 26);
            this.textEdit21.TabIndex = 56;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(4, 360);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(79, 19);
            this.labelControl7.TabIndex = 57;
            this.labelControl7.Text = "Phòng Đơn";
            // 
            // textEdit20
            // 
            this.textEdit20.EditValue = "70";
            this.textEdit20.Enabled = false;
            this.textEdit20.EnterMoveNextControl = true;
            this.textEdit20.Location = new System.Drawing.Point(136, 357);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit20.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit20.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit20.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit20.Size = new System.Drawing.Size(54, 26);
            this.textEdit20.TabIndex = 58;
            // 
            // textEdit19
            // 
            this.textEdit19.EditValue = "70 %";
            this.textEdit19.Enabled = false;
            this.textEdit19.EnterMoveNextControl = true;
            this.textEdit19.Location = new System.Drawing.Point(206, 357);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit19.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit19.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit19.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit19.Size = new System.Drawing.Size(54, 26);
            this.textEdit19.TabIndex = 59;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(4, 404);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 19);
            this.labelControl6.TabIndex = 60;
            this.labelControl6.Text = "Phòng Đôi";
            // 
            // textEdit18
            // 
            this.textEdit18.EditValue = "30";
            this.textEdit18.Enabled = false;
            this.textEdit18.EnterMoveNextControl = true;
            this.textEdit18.Location = new System.Drawing.Point(136, 401);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit18.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit18.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit18.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit18.Size = new System.Drawing.Size(54, 26);
            this.textEdit18.TabIndex = 61;
            // 
            // textEdit17
            // 
            this.textEdit17.EditValue = "30 %";
            this.textEdit17.Enabled = false;
            this.textEdit17.EnterMoveNextControl = true;
            this.textEdit17.Location = new System.Drawing.Point(206, 401);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit17.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit17.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit17.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit17.Size = new System.Drawing.Size(54, 26);
            this.textEdit17.TabIndex = 62;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(4, 8);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(108, 19);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Tổng số phòng";
            // 
            // textEdit7
            // 
            this.textEdit7.EditValue = "100";
            this.textEdit7.Enabled = false;
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(138, 5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit7.Size = new System.Drawing.Size(54, 26);
            this.textEdit7.TabIndex = 19;
            // 
            // textEdit8
            // 
            this.textEdit8.EditValue = "100 %";
            this.textEdit8.Enabled = false;
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(206, 5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit8.Size = new System.Drawing.Size(54, 26);
            this.textEdit8.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(4, 52);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(112, 19);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "Phòng có khách";
            // 
            // textEdit10
            // 
            this.textEdit10.EditValue = "50";
            this.textEdit10.Enabled = false;
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(138, 49);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Size = new System.Drawing.Size(54, 26);
            this.textEdit10.TabIndex = 22;
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "50 %";
            this.textEdit9.Enabled = false;
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(206, 49);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Size = new System.Drawing.Size(54, 26);
            this.textEdit9.TabIndex = 23;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(4, 96);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(124, 19);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Phòng trống sạch";
            // 
            // textEdit12
            // 
            this.textEdit12.EditValue = "10";
            this.textEdit12.Enabled = false;
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(138, 93);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Size = new System.Drawing.Size(54, 26);
            this.textEdit12.TabIndex = 25;
            // 
            // textEdit11
            // 
            this.textEdit11.EditValue = "10 %";
            this.textEdit11.Enabled = false;
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(206, 93);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Size = new System.Drawing.Size(54, 26);
            this.textEdit11.TabIndex = 26;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(4, 140);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(119, 19);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "Phòng trống bẩn";
            // 
            // textEdit14
            // 
            this.textEdit14.EditValue = "0";
            this.textEdit14.Enabled = false;
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(138, 137);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit14.Size = new System.Drawing.Size(54, 26);
            this.textEdit14.TabIndex = 28;
            // 
            // textEdit13
            // 
            this.textEdit13.EditValue = "0 %";
            this.textEdit13.Enabled = false;
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(206, 137);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit13.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit13.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit13.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit13.Size = new System.Drawing.Size(54, 26);
            this.textEdit13.TabIndex = 29;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(4, 184);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(112, 19);
            this.labelControl4.TabIndex = 30;
            this.labelControl4.Text = "Phòng bận sạch";
            // 
            // textEdit16
            // 
            this.textEdit16.EditValue = "7";
            this.textEdit16.Enabled = false;
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(138, 181);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit16.Size = new System.Drawing.Size(54, 26);
            this.textEdit16.TabIndex = 31;
            // 
            // textEdit15
            // 
            this.textEdit15.EditValue = "7 %";
            this.textEdit15.Enabled = false;
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(206, 181);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Size = new System.Drawing.Size(54, 26);
            this.textEdit15.TabIndex = 32;
            // 
            // Frm_TTP_TKP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 500);
            this.Controls.Add(this.palTTP_TKP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TTP_TKP";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.palTTP_TKP)).EndInit();
            this.palTTP_TKP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            this.panelControl22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            this.panelControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl palTTP_TKP;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit33;
        private DevExpress.XtraEditors.TextEdit textEdit48;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit54;
        private DevExpress.XtraEditors.TextEdit textEdit55;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit31;
        private DevExpress.XtraEditors.TextEdit textEdit29;
        private DevExpress.XtraEditors.TextEdit textEdit50;
        private DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.TextEdit textEdit52;
        private DevExpress.XtraEditors.TextEdit textEdit49;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit47;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit28;
        private DevExpress.XtraEditors.TextEdit textEdit51;
        private DevExpress.XtraEditors.TextEdit textEdit34;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit textEdit30;
        private DevExpress.XtraEditors.TextEdit textEdit32;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEdit38;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit textEdit39;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private DevExpress.XtraEditors.TextEdit textEdit42;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit textEdit43;
        private DevExpress.XtraEditors.TextEdit textEdit40;
        private DevExpress.XtraEditors.TextEdit textEdit46;
        private DevExpress.XtraEditors.TextEdit textEdit41;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit44;
        private DevExpress.XtraEditors.TextEdit textEdit45;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.TextEdit textEdit35;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit36;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEdit26;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit24;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.TextEdit textEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.TextEdit textEdit15;





    }
}