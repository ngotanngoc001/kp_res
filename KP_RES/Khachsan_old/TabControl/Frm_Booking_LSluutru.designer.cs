﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Booking_LSluutru
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Booking_LSluutru));
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton53 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton54 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton55 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton56 = new DevExpress.XtraEditors.SimpleButton();
            this.palBook_LSluutru_Top = new DevExpress.XtraEditors.PanelControl();
            this.pal_Book_LSluutru_Search = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton57 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit82 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit80 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit81 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palBook_LSluutru_Top)).BeginInit();
            this.palBook_LSluutru_Top.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_Book_LSluutru_Search)).BeginInit();
            this.pal_Book_LSluutru_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit81.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl33
            // 
            this.panelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl33.Controls.Add(this.gridControl5);
            this.panelControl33.Controls.Add(this.panelControl29);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl33.Location = new System.Drawing.Point(0, 54);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(1019, 423);
            this.panelControl33.TabIndex = 24;
            // 
            // gridControl5
            // 
            this.gridControl5.AllowDrop = true;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl5.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(980, 423);
            this.gridControl5.TabIndex = 11;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView5.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView5.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView5.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView5.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView5.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.GroupRow.Options.UseFont = true;
            this.gridView5.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView5.Appearance.Row.Options.UseFont = true;
            this.gridView5.ColumnPanelRowHeight = 30;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn40,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39});
            this.gridView5.FooterPanelHeight = 30;
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.RowHeight = 30;
            this.gridView5.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn16.AppearanceCell.Options.UseFont = true;
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn16.Caption = "STT";
            this.gridColumn16.FieldName = "TONGSOPHONG";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.AllowMove = false;
            this.gridColumn16.OptionsColumn.FixedWidth = true;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 49;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn17.AppearanceCell.Options.UseFont = true;
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Số Booking";
            this.gridColumn17.FieldName = "LOAIPHONG";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.FixedWidth = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            this.gridColumn17.Width = 101;
            // 
            // gridColumn40
            // 
            this.gridColumn40.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn40.AppearanceCell.Options.UseFont = true;
            this.gridColumn40.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn40.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn40.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn40.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn40.AppearanceHeader.Options.UseFont = true;
            this.gridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn40.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn40.Caption = "Ngày";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 2;
            this.gridColumn40.Width = 127;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn19.AppearanceCell.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "Số ĐT";
            this.gridColumn19.FieldName = "TEMP2";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.FixedWidth = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            this.gridColumn19.Width = 113;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn20.AppearanceCell.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Loại phòng";
            this.gridColumn20.FieldName = "TEMP3";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.FixedWidth = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            this.gridColumn20.Width = 121;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn21.AppearanceCell.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn21.AppearanceHeader.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "Số phòng";
            this.gridColumn21.FieldName = "TEMP4";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.FixedWidth = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            this.gridColumn21.Width = 87;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn22.AppearanceCell.Options.UseFont = true;
            this.gridColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "Số khách";
            this.gridColumn22.FieldName = "TEMP5";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.FixedWidth = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 6;
            this.gridColumn22.Width = 85;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn23.AppearanceCell.Options.UseFont = true;
            this.gridColumn23.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn23.AppearanceHeader.Options.UseFont = true;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.Caption = "Ngày đến";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.AllowMove = false;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 7;
            this.gridColumn23.Width = 107;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn32.AppearanceCell.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn32.AppearanceHeader.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn32.Caption = "Ngày đi";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 8;
            this.gridColumn32.Width = 99;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn33.AppearanceCell.Options.UseFont = true;
            this.gridColumn33.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn33.AppearanceHeader.Options.UseFont = true;
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.Caption = "Số Ngày Lưu Trú";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 9;
            this.gridColumn33.Width = 152;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn34.AppearanceCell.Options.UseFont = true;
            this.gridColumn34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn34.AppearanceHeader.Options.UseFont = true;
            this.gridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn34.Caption = "CMT/Passport";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 10;
            this.gridColumn34.Width = 132;
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn35.AppearanceCell.Options.UseFont = true;
            this.gridColumn35.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn35.AppearanceHeader.Options.UseFont = true;
            this.gridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn35.Caption = "Ngày CMT/Passport";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 11;
            this.gridColumn35.Width = 180;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn36.AppearanceCell.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn36.AppearanceHeader.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn36.Caption = "Visa";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 12;
            this.gridColumn36.Width = 121;
            // 
            // gridColumn37
            // 
            this.gridColumn37.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn37.AppearanceCell.Options.UseFont = true;
            this.gridColumn37.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn37.AppearanceHeader.Options.UseFont = true;
            this.gridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn37.Caption = "Ngày Visa";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 13;
            this.gridColumn37.Width = 103;
            // 
            // gridColumn38
            // 
            this.gridColumn38.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn38.AppearanceCell.Options.UseFont = true;
            this.gridColumn38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn38.AppearanceHeader.Options.UseFont = true;
            this.gridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn38.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn38.Caption = "Quốc Tịch";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 14;
            this.gridColumn38.Width = 117;
            // 
            // gridColumn39
            // 
            this.gridColumn39.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn39.AppearanceCell.Options.UseFont = true;
            this.gridColumn39.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn39.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn39.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn39.AppearanceHeader.Options.UseFont = true;
            this.gridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn39.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn39.Caption = "Tổng Tiền";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 15;
            this.gridColumn39.Width = 148;
            // 
            // panelControl29
            // 
            this.panelControl29.Controls.Add(this.panelControl30);
            this.panelControl29.Controls.Add(this.simpleButton55);
            this.panelControl29.Controls.Add(this.simpleButton56);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl29.Location = new System.Drawing.Point(980, 0);
            this.panelControl29.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(39, 423);
            this.panelControl29.TabIndex = 12;
            // 
            // panelControl30
            // 
            this.panelControl30.AutoSize = true;
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.panelControl31);
            this.panelControl30.Controls.Add(this.simpleButton17);
            this.panelControl30.Controls.Add(this.simpleButton18);
            this.panelControl30.Controls.Add(this.simpleButton53);
            this.panelControl30.Controls.Add(this.simpleButton54);
            this.panelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl30.Location = new System.Drawing.Point(2, 82);
            this.panelControl30.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(35, 259);
            this.panelControl30.TabIndex = 14;
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl31.Location = new System.Drawing.Point(0, 160);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(35, 0);
            this.panelControl31.TabIndex = 15;
            // 
            // simpleButton17
            // 
            this.simpleButton17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton17.Appearance.Options.UseFont = true;
            this.simpleButton17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton17.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton17.Image")));
            this.simpleButton17.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton17.Location = new System.Drawing.Point(0, 99);
            this.simpleButton17.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(35, 80);
            this.simpleButton17.TabIndex = 12;
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton18.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton18.Image")));
            this.simpleButton18.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton18.Location = new System.Drawing.Point(0, 179);
            this.simpleButton18.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(35, 80);
            this.simpleButton18.TabIndex = 1;
            // 
            // simpleButton53
            // 
            this.simpleButton53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton53.Appearance.Options.UseFont = true;
            this.simpleButton53.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton53.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton53.Image")));
            this.simpleButton53.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton53.Location = new System.Drawing.Point(0, 80);
            this.simpleButton53.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton53.Name = "simpleButton53";
            this.simpleButton53.Size = new System.Drawing.Size(35, 80);
            this.simpleButton53.TabIndex = 13;
            // 
            // simpleButton54
            // 
            this.simpleButton54.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton54.Appearance.Options.UseFont = true;
            this.simpleButton54.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton54.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton54.Image")));
            this.simpleButton54.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton54.Location = new System.Drawing.Point(0, 0);
            this.simpleButton54.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton54.Name = "simpleButton54";
            this.simpleButton54.Size = new System.Drawing.Size(35, 80);
            this.simpleButton54.TabIndex = 11;
            // 
            // simpleButton55
            // 
            this.simpleButton55.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton55.Appearance.Options.UseFont = true;
            this.simpleButton55.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton55.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton55.Image")));
            this.simpleButton55.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton55.Location = new System.Drawing.Point(2, 341);
            this.simpleButton55.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton55.Name = "simpleButton55";
            this.simpleButton55.Size = new System.Drawing.Size(35, 80);
            this.simpleButton55.TabIndex = 9;
            // 
            // simpleButton56
            // 
            this.simpleButton56.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton56.Appearance.Options.UseFont = true;
            this.simpleButton56.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton56.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton56.Image")));
            this.simpleButton56.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton56.Location = new System.Drawing.Point(2, 2);
            this.simpleButton56.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton56.Name = "simpleButton56";
            this.simpleButton56.Size = new System.Drawing.Size(35, 80);
            this.simpleButton56.TabIndex = 0;
            // 
            // palBook_LSluutru_Top
            // 
            this.palBook_LSluutru_Top.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palBook_LSluutru_Top.Controls.Add(this.pal_Book_LSluutru_Search);
            this.palBook_LSluutru_Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.palBook_LSluutru_Top.Location = new System.Drawing.Point(0, 0);
            this.palBook_LSluutru_Top.Name = "palBook_LSluutru_Top";
            this.palBook_LSluutru_Top.Size = new System.Drawing.Size(1019, 54);
            this.palBook_LSluutru_Top.TabIndex = 23;
            // 
            // pal_Book_LSluutru_Search
            // 
            this.pal_Book_LSluutru_Search.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_Book_LSluutru_Search.Controls.Add(this.simpleButton57);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit82);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl55);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit80);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl53);
            this.pal_Book_LSluutru_Search.Controls.Add(this.textEdit81);
            this.pal_Book_LSluutru_Search.Controls.Add(this.labelControl54);
            this.pal_Book_LSluutru_Search.Location = new System.Drawing.Point(23, 6);
            this.pal_Book_LSluutru_Search.Name = "pal_Book_LSluutru_Search";
            this.pal_Book_LSluutru_Search.Size = new System.Drawing.Size(970, 43);
            this.pal_Book_LSluutru_Search.TabIndex = 47;
            // 
            // simpleButton57
            // 
            this.simpleButton57.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton57.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton57.Appearance.Options.UseBackColor = true;
            this.simpleButton57.Appearance.Options.UseFont = true;
            this.simpleButton57.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton57.Location = new System.Drawing.Point(839, 3);
            this.simpleButton57.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton57.Name = "simpleButton57";
            this.simpleButton57.Size = new System.Drawing.Size(108, 38);
            this.simpleButton57.TabIndex = 37;
            this.simpleButton57.Text = "Tìm kiếm";
            // 
            // textEdit82
            // 
            this.textEdit82.EditValue = "";
            this.textEdit82.EnterMoveNextControl = true;
            this.textEdit82.Location = new System.Drawing.Point(673, 8);
            this.textEdit82.Name = "textEdit82";
            this.textEdit82.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit82.Properties.Appearance.Options.UseFont = true;
            this.textEdit82.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit82.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit82.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit82.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit82.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit82.Size = new System.Drawing.Size(144, 26);
            this.textEdit82.TabIndex = 36;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Location = new System.Drawing.Point(573, 11);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(93, 19);
            this.labelControl55.TabIndex = 35;
            this.labelControl55.Text = "Số điện thoại";
            // 
            // textEdit80
            // 
            this.textEdit80.EditValue = "";
            this.textEdit80.EnterMoveNextControl = true;
            this.textEdit80.Location = new System.Drawing.Point(355, 8);
            this.textEdit80.Name = "textEdit80";
            this.textEdit80.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.Appearance.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit80.Size = new System.Drawing.Size(211, 26);
            this.textEdit80.TabIndex = 34;
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(4, 11);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(109, 19);
            this.labelControl53.TabIndex = 1;
            this.labelControl53.Text = "Mã Khách Hàng";
            // 
            // textEdit81
            // 
            this.textEdit81.EditValue = "";
            this.textEdit81.EnterMoveNextControl = true;
            this.textEdit81.Location = new System.Drawing.Point(120, 8);
            this.textEdit81.Name = "textEdit81";
            this.textEdit81.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit81.Properties.Appearance.Options.UseFont = true;
            this.textEdit81.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit81.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit81.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit81.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit81.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit81.Size = new System.Drawing.Size(103, 26);
            this.textEdit81.TabIndex = 19;
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(230, 11);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(116, 19);
            this.labelControl54.TabIndex = 21;
            this.labelControl54.Text = "Tên Khách Hàng";
            // 
            // Frm_Booking_LSluutru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.panelControl33);
            this.Controls.Add(this.palBook_LSluutru_Top);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Booking_LSluutru";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palBook_LSluutru_Top)).EndInit();
            this.palBook_LSluutru_Top.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_Book_LSluutru_Search)).EndInit();
            this.pal_Book_LSluutru_Search.ResumeLayout(false);
            this.pal_Book_LSluutru_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit81.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton53;
        private DevExpress.XtraEditors.SimpleButton simpleButton54;
        private DevExpress.XtraEditors.SimpleButton simpleButton55;
        private DevExpress.XtraEditors.SimpleButton simpleButton56;
        private DevExpress.XtraEditors.PanelControl palBook_LSluutru_Top;
        private DevExpress.XtraEditors.PanelControl pal_Book_LSluutru_Search;
        private DevExpress.XtraEditors.SimpleButton simpleButton57;
        private DevExpress.XtraEditors.TextEdit textEdit82;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.TextEdit textEdit80;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit textEdit81;
        private DevExpress.XtraEditors.LabelControl labelControl54;






    }
}