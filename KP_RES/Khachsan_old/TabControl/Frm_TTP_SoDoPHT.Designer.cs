﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_TTP_SoDoPHT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.richTextBox11 = new System.Windows.Forms.RichTextBox();
            this.richTextBox12 = new System.Windows.Forms.RichTextBox();
            this.richTextBox13 = new System.Windows.Forms.RichTextBox();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBox10
            // 
            this.richTextBox10.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox10.Location = new System.Drawing.Point(751, 325);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.ReadOnly = true;
            this.richTextBox10.Size = new System.Drawing.Size(212, 125);
            this.richTextBox10.TabIndex = 47;
            this.richTextBox10.Text = "Phòng 403 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox11
            // 
            this.richTextBox11.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox11.Location = new System.Drawing.Point(523, 325);
            this.richTextBox11.Name = "richTextBox11";
            this.richTextBox11.ReadOnly = true;
            this.richTextBox11.Size = new System.Drawing.Size(212, 125);
            this.richTextBox11.TabIndex = 46;
            this.richTextBox11.Text = "Phòng 303 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox12
            // 
            this.richTextBox12.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox12.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox12.Location = new System.Drawing.Point(286, 325);
            this.richTextBox12.Name = "richTextBox12";
            this.richTextBox12.ReadOnly = true;
            this.richTextBox12.Size = new System.Drawing.Size(212, 125);
            this.richTextBox12.TabIndex = 45;
            this.richTextBox12.Text = "Phòng 203 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox13
            // 
            this.richTextBox13.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox13.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox13.Location = new System.Drawing.Point(56, 325);
            this.richTextBox13.Name = "richTextBox13";
            this.richTextBox13.ReadOnly = true;
            this.richTextBox13.Size = new System.Drawing.Size(212, 125);
            this.richTextBox13.TabIndex = 44;
            this.richTextBox13.Text = "Phòng 103 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox8
            // 
            this.richTextBox8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox8.Location = new System.Drawing.Point(751, 176);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.ReadOnly = true;
            this.richTextBox8.Size = new System.Drawing.Size(212, 125);
            this.richTextBox8.TabIndex = 43;
            this.richTextBox8.Text = "Phòng 402 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox7
            // 
            this.richTextBox7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox7.Location = new System.Drawing.Point(523, 176);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.ReadOnly = true;
            this.richTextBox7.Size = new System.Drawing.Size(212, 125);
            this.richTextBox7.TabIndex = 42;
            this.richTextBox7.Text = "Phòng 302 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.Location = new System.Drawing.Point(286, 176);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.ReadOnly = true;
            this.richTextBox6.Size = new System.Drawing.Size(212, 125);
            this.richTextBox6.TabIndex = 41;
            this.richTextBox6.Text = "Phòng 202 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(56, 176);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(212, 125);
            this.richTextBox5.TabIndex = 40;
            this.richTextBox5.Text = "Phòng 102 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.Location = new System.Drawing.Point(751, 27);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(212, 125);
            this.richTextBox4.TabIndex = 39;
            this.richTextBox4.Text = "Phòng 401 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(523, 27);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(212, 125);
            this.richTextBox3.TabIndex = 38;
            this.richTextBox3.Text = "Phòng 301 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(286, 27);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(212, 125);
            this.richTextBox2.TabIndex = 37;
            this.richTextBox2.Text = "Phòng 201 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(56, 27);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(212, 125);
            this.richTextBox1.TabIndex = 36;
            this.richTextBox1.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // Frm_TTP_SoDoPHT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.richTextBox10);
            this.Controls.Add(this.richTextBox11);
            this.Controls.Add(this.richTextBox12);
            this.Controls.Add(this.richTextBox13);
            this.Controls.Add(this.richTextBox8);
            this.Controls.Add(this.richTextBox7);
            this.Controls.Add(this.richTextBox6);
            this.Controls.Add(this.richTextBox5);
            this.Controls.Add(this.richTextBox4);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TTP_SoDoPHT";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.RichTextBox richTextBox11;
        private System.Windows.Forms.RichTextBox richTextBox12;
        private System.Windows.Forms.RichTextBox richTextBox13;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;





    }
}