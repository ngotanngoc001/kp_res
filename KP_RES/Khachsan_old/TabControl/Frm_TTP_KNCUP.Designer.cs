﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_TTP_KNCUP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TTP_KNCUP));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim_1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton52 = new DevExpress.XtraEditors.SimpleButton();
            this.palSearch = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palSearch)).BeginInit();
            this.palSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(881, 489);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.gridView1.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.GroupFooter.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_LOAI,
            this.SoPhong,
            this.TEN_LOAI});
            this.gridView1.FooterPanelHeight = 40;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 40;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // MA_LOAI
            // 
            this.MA_LOAI.Caption = "MA_LOAI";
            this.MA_LOAI.FieldName = "MA_LOAI";
            this.MA_LOAI.Name = "MA_LOAI";
            // 
            // SoPhong
            // 
            this.SoPhong.AppearanceCell.Options.UseTextOptions = true;
            this.SoPhong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoPhong.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SoPhong.AppearanceHeader.Options.UseFont = true;
            this.SoPhong.AppearanceHeader.Options.UseTextOptions = true;
            this.SoPhong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoPhong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoPhong.Caption = "Tổng số phòng";
            this.SoPhong.FieldName = "SoPhong";
            this.SoPhong.Name = "SoPhong";
            this.SoPhong.OptionsColumn.AllowEdit = false;
            this.SoPhong.OptionsColumn.AllowFocus = false;
            this.SoPhong.OptionsColumn.AllowMove = false;
            this.SoPhong.OptionsColumn.FixedWidth = true;
            this.SoPhong.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.SoPhong.Visible = true;
            this.SoPhong.VisibleIndex = 0;
            this.SoPhong.Width = 79;
            // 
            // TEN_LOAI
            // 
            this.TEN_LOAI.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_LOAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TEN_LOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_LOAI.AppearanceHeader.Options.UseFont = true;
            this.TEN_LOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_LOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_LOAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_LOAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_LOAI.Caption = "Loại phòng";
            this.TEN_LOAI.FieldName = "TEN_LOAI";
            this.TEN_LOAI.Name = "TEN_LOAI";
            this.TEN_LOAI.OptionsColumn.AllowEdit = false;
            this.TEN_LOAI.OptionsColumn.AllowFocus = false;
            this.TEN_LOAI.OptionsColumn.FixedWidth = true;
            this.TEN_LOAI.Visible = true;
            this.TEN_LOAI.VisibleIndex = 1;
            this.TEN_LOAI.Width = 125;
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.panelControl16);
            this.panelControl15.Controls.Add(this.btnBanphim_1);
            this.panelControl15.Controls.Add(this.simpleButton52);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl15.Location = new System.Drawing.Point(881, 0);
            this.panelControl15.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(39, 489);
            this.panelControl15.TabIndex = 8;
            // 
            // panelControl16
            // 
            this.panelControl16.AutoSize = true;
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl17);
            this.panelControl16.Controls.Add(this.btnXuongit_1);
            this.panelControl16.Controls.Add(this.btnXuongnhieu_1);
            this.panelControl16.Controls.Add(this.btnLenit_1);
            this.panelControl16.Controls.Add(this.btnLennhieu_1);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(2, 82);
            this.panelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(35, 325);
            this.panelControl16.TabIndex = 14;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 160);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(35, 5);
            this.panelControl17.TabIndex = 15;
            // 
            // btnXuongit_1
            // 
            this.btnXuongit_1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit_1.Appearance.Options.UseFont = true;
            this.btnXuongit_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            //this.btnXuongit_1.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit_1.Image")));
            this.btnXuongit_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit_1.Location = new System.Drawing.Point(0, 165);
            this.btnXuongit_1.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit_1.Name = "btnXuongit_1";
            this.btnXuongit_1.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit_1.TabIndex = 12;
            this.btnXuongit_1.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu_1
            // 
            this.btnXuongnhieu_1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu_1.Appearance.Options.UseFont = true;
            this.btnXuongnhieu_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            //this.btnXuongnhieu_1.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu_1.Image")));
            this.btnXuongnhieu_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu_1.Location = new System.Drawing.Point(0, 245);
            this.btnXuongnhieu_1.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu_1.Name = "btnXuongnhieu_1";
            this.btnXuongnhieu_1.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu_1.TabIndex = 1;
            this.btnXuongnhieu_1.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit_1
            // 
            this.btnLenit_1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit_1.Appearance.Options.UseFont = true;
            this.btnLenit_1.Dock = System.Windows.Forms.DockStyle.Top;
            //this.btnLenit_1.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit_1.Image")));
            this.btnLenit_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit_1.Location = new System.Drawing.Point(0, 80);
            this.btnLenit_1.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit_1.Name = "btnLenit_1";
            this.btnLenit_1.Size = new System.Drawing.Size(35, 80);
            this.btnLenit_1.TabIndex = 13;
            this.btnLenit_1.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu_1
            // 
            this.btnLennhieu_1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu_1.Appearance.Options.UseFont = true;
            this.btnLennhieu_1.Dock = System.Windows.Forms.DockStyle.Top;
            //this.btnLennhieu_1.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu_1.Image")));
            this.btnLennhieu_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu_1.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu_1.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu_1.Name = "btnLennhieu_1";
            this.btnLennhieu_1.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu_1.TabIndex = 11;
            this.btnLennhieu_1.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim_1
            // 
            this.btnBanphim_1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim_1.Appearance.Options.UseFont = true;
            this.btnBanphim_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            //this.btnBanphim_1.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim_1.Image")));
            this.btnBanphim_1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim_1.Location = new System.Drawing.Point(2, 407);
            this.btnBanphim_1.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim_1.Name = "btnBanphim_1";
            this.btnBanphim_1.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim_1.TabIndex = 9;
            this.btnBanphim_1.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // simpleButton52
            // 
            this.simpleButton52.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton52.Appearance.Options.UseFont = true;
            this.simpleButton52.Dock = System.Windows.Forms.DockStyle.Top;
            //this.simpleButton52.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton52.Image")));
            this.simpleButton52.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton52.Location = new System.Drawing.Point(2, 2);
            this.simpleButton52.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton52.Name = "simpleButton52";
            this.simpleButton52.Size = new System.Drawing.Size(35, 80);
            this.simpleButton52.TabIndex = 0;
            // 
            // palSearch
            // 
            this.palSearch.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palSearch.Controls.Add(this.gridControl1);
            this.palSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palSearch.Location = new System.Drawing.Point(0, 0);
            this.palSearch.Name = "palSearch";
            this.palSearch.Size = new System.Drawing.Size(881, 489);
            this.palSearch.TabIndex = 37;
            // 
            // Frm_TTP_KNCUP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 489);
            this.Controls.Add(this.palSearch);
            this.Controls.Add(this.panelControl15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TTP_KNCUP";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palSearch)).EndInit();
            this.palSearch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SoPhong;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_LOAI;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.SimpleButton btnXuongit_1;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu_1;
        private DevExpress.XtraEditors.SimpleButton btnLenit_1;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu_1;
        private DevExpress.XtraEditors.SimpleButton btnBanphim_1;
        private DevExpress.XtraEditors.SimpleButton simpleButton52;
        private DevExpress.XtraGrid.Columns.GridColumn MA_LOAI;
        private DevExpress.XtraEditors.PanelControl palSearch;



    }
}