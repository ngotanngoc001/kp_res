﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Booking_Dangky
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit77 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit78 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit79 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit76 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit73 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit75 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit53 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit59 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit66 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit67 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit72 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIPHONG1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHONG1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SLKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYVISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUOCTICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit68 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit71 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit65 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit69 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit70 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit74 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.dtp_ngayden = new DevExpress.XtraEditors.DateEdit();
            this.dtp_ngaydi = new DevExpress.XtraEditors.DateEdit();
            this.textEdit64 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit63 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit62 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit61 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit60 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit58 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit57 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit56 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.palbook_Dangky = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palbook_Dangky)).BeginInit();
            this.palbook_Dangky.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.xtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Location = new System.Drawing.Point(356, 67);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(654, 400);
            this.xtraTabControl1.TabIndex = 51;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.richTextBox9);
            this.xtraTabPage1.Controls.Add(this.labelControl52);
            this.xtraTabPage1.Controls.Add(this.textEdit77);
            this.xtraTabPage1.Controls.Add(this.textEdit78);
            this.xtraTabPage1.Controls.Add(this.textEdit79);
            this.xtraTabPage1.Controls.Add(this.labelControl44);
            this.xtraTabPage1.Controls.Add(this.labelControl45);
            this.xtraTabPage1.Controls.Add(this.labelControl51);
            this.xtraTabPage1.Controls.Add(this.textEdit76);
            this.xtraTabPage1.Controls.Add(this.labelControl37);
            this.xtraTabPage1.Controls.Add(this.textEdit73);
            this.xtraTabPage1.Controls.Add(this.textEdit75);
            this.xtraTabPage1.Controls.Add(this.textEdit53);
            this.xtraTabPage1.Controls.Add(this.labelControl36);
            this.xtraTabPage1.Controls.Add(this.textEdit59);
            this.xtraTabPage1.Controls.Add(this.labelControl43);
            this.xtraTabPage1.Controls.Add(this.textEdit66);
            this.xtraTabPage1.Controls.Add(this.textEdit67);
            this.xtraTabPage1.Controls.Add(this.textEdit72);
            this.xtraTabPage1.Controls.Add(this.labelControl46);
            this.xtraTabPage1.Controls.Add(this.labelControl47);
            this.xtraTabPage1.Controls.Add(this.labelControl48);
            this.xtraTabPage1.Controls.Add(this.labelControl49);
            this.xtraTabPage1.Controls.Add(this.labelControl50);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(648, 366);
            this.xtraTabPage1.Text = "Thông Tin Khách";
            // 
            // richTextBox9
            // 
            this.richTextBox9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox9.Location = new System.Drawing.Point(411, 145);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(226, 214);
            this.richTextBox9.TabIndex = 73;
            this.richTextBox9.Text = "";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Location = new System.Drawing.Point(330, 150);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(54, 19);
            this.labelControl52.TabIndex = 72;
            this.labelControl52.Text = "Ghi chú";
            // 
            // textEdit77
            // 
            this.textEdit77.EditValue = "";
            this.textEdit77.EnterMoveNextControl = true;
            this.textEdit77.Location = new System.Drawing.Point(411, 9);
            this.textEdit77.Name = "textEdit77";
            this.textEdit77.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit77.Properties.Appearance.Options.UseFont = true;
            this.textEdit77.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit77.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit77.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit77.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit77.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit77.Size = new System.Drawing.Size(226, 26);
            this.textEdit77.TabIndex = 71;
            // 
            // textEdit78
            // 
            this.textEdit78.EditValue = "";
            this.textEdit78.EnterMoveNextControl = true;
            this.textEdit78.Location = new System.Drawing.Point(411, 101);
            this.textEdit78.Name = "textEdit78";
            this.textEdit78.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit78.Properties.Appearance.Options.UseFont = true;
            this.textEdit78.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit78.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit78.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit78.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit78.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit78.Size = new System.Drawing.Size(226, 26);
            this.textEdit78.TabIndex = 70;
            // 
            // textEdit79
            // 
            this.textEdit79.EditValue = "";
            this.textEdit79.EnterMoveNextControl = true;
            this.textEdit79.Location = new System.Drawing.Point(411, 55);
            this.textEdit79.Name = "textEdit79";
            this.textEdit79.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit79.Properties.Appearance.Options.UseFont = true;
            this.textEdit79.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit79.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit79.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit79.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit79.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit79.Size = new System.Drawing.Size(226, 26);
            this.textEdit79.TabIndex = 69;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Location = new System.Drawing.Point(329, 12);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(69, 19);
            this.labelControl44.TabIndex = 66;
            this.labelControl44.Text = "Mã Đại Lý";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Location = new System.Drawing.Point(329, 58);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(76, 19);
            this.labelControl45.TabIndex = 67;
            this.labelControl45.Text = "Tên Đại Lý";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Location = new System.Drawing.Point(330, 104);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(36, 19);
            this.labelControl51.TabIndex = 68;
            this.labelControl51.Text = "Ngày";
            // 
            // textEdit76
            // 
            this.textEdit76.EditValue = "";
            this.textEdit76.EnterMoveNextControl = true;
            this.textEdit76.Location = new System.Drawing.Point(116, 331);
            this.textEdit76.Name = "textEdit76";
            this.textEdit76.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit76.Properties.Appearance.Options.UseFont = true;
            this.textEdit76.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit76.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit76.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit76.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit76.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit76.Size = new System.Drawing.Size(201, 26);
            this.textEdit76.TabIndex = 65;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(5, 334);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(51, 19);
            this.labelControl37.TabIndex = 64;
            this.labelControl37.Text = "Địa Chỉ";
            // 
            // textEdit73
            // 
            this.textEdit73.EditValue = "";
            this.textEdit73.EnterMoveNextControl = true;
            this.textEdit73.Location = new System.Drawing.Point(115, 55);
            this.textEdit73.Name = "textEdit73";
            this.textEdit73.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit73.Properties.Appearance.Options.UseFont = true;
            this.textEdit73.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit73.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit73.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit73.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit73.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit73.Size = new System.Drawing.Size(201, 26);
            this.textEdit73.TabIndex = 63;
            // 
            // textEdit75
            // 
            this.textEdit75.EditValue = "";
            this.textEdit75.EnterMoveNextControl = true;
            this.textEdit75.Location = new System.Drawing.Point(115, 9);
            this.textEdit75.Name = "textEdit75";
            this.textEdit75.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit75.Properties.Appearance.Options.UseFont = true;
            this.textEdit75.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit75.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit75.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit75.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit75.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit75.Size = new System.Drawing.Size(201, 26);
            this.textEdit75.TabIndex = 62;
            // 
            // textEdit53
            // 
            this.textEdit53.EditValue = "";
            this.textEdit53.EnterMoveNextControl = true;
            this.textEdit53.Location = new System.Drawing.Point(116, 285);
            this.textEdit53.Name = "textEdit53";
            this.textEdit53.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit53.Properties.Appearance.Options.UseFont = true;
            this.textEdit53.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit53.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit53.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit53.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit53.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit53.Size = new System.Drawing.Size(201, 26);
            this.textEdit53.TabIndex = 61;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(5, 288);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(39, 19);
            this.labelControl36.TabIndex = 60;
            this.labelControl36.Text = "Email";
            // 
            // textEdit59
            // 
            this.textEdit59.EditValue = "";
            this.textEdit59.EnterMoveNextControl = true;
            this.textEdit59.Location = new System.Drawing.Point(116, 239);
            this.textEdit59.Name = "textEdit59";
            this.textEdit59.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit59.Properties.Appearance.Options.UseFont = true;
            this.textEdit59.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit59.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit59.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit59.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit59.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit59.Size = new System.Drawing.Size(201, 26);
            this.textEdit59.TabIndex = 58;
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Location = new System.Drawing.Point(5, 242);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(72, 19);
            this.labelControl43.TabIndex = 57;
            this.labelControl43.Text = "Quốc Tịch";
            // 
            // textEdit66
            // 
            this.textEdit66.EditValue = "";
            this.textEdit66.EnterMoveNextControl = true;
            this.textEdit66.Location = new System.Drawing.Point(116, 193);
            this.textEdit66.Name = "textEdit66";
            this.textEdit66.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit66.Properties.Appearance.Options.UseFont = true;
            this.textEdit66.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit66.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit66.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit66.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit66.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit66.Size = new System.Drawing.Size(201, 26);
            this.textEdit66.TabIndex = 54;
            // 
            // textEdit67
            // 
            this.textEdit67.EditValue = "";
            this.textEdit67.EnterMoveNextControl = true;
            this.textEdit67.Location = new System.Drawing.Point(116, 147);
            this.textEdit67.Name = "textEdit67";
            this.textEdit67.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit67.Properties.Appearance.Options.UseFont = true;
            this.textEdit67.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit67.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit67.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit67.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit67.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit67.Size = new System.Drawing.Size(201, 26);
            this.textEdit67.TabIndex = 53;
            // 
            // textEdit72
            // 
            this.textEdit72.EditValue = "";
            this.textEdit72.EnterMoveNextControl = true;
            this.textEdit72.Location = new System.Drawing.Point(116, 101);
            this.textEdit72.Name = "textEdit72";
            this.textEdit72.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit72.Properties.Appearance.Options.UseFont = true;
            this.textEdit72.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit72.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit72.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit72.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit72.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit72.Size = new System.Drawing.Size(201, 26);
            this.textEdit72.TabIndex = 52;
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Location = new System.Drawing.Point(5, 12);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(103, 19);
            this.labelControl46.TabIndex = 47;
            this.labelControl46.Text = "Đoàn/Công Ty";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Location = new System.Drawing.Point(5, 58);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(83, 19);
            this.labelControl47.TabIndex = 48;
            this.labelControl47.Text = "Người Book";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Location = new System.Drawing.Point(4, 104);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(77, 19);
            this.labelControl48.TabIndex = 49;
            this.labelControl48.Text = "Điện Thoại";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Location = new System.Drawing.Point(5, 150);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(98, 19);
            this.labelControl49.TabIndex = 50;
            this.labelControl49.Text = "CMT/Passport";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(4, 196);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(29, 19);
            this.labelControl50.TabIndex = 51;
            this.labelControl50.Text = "Visa";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(648, 366);
            this.xtraTabPage3.Text = "Chi Tiết Phòng";
            // 
            // gridControl3
            // 
            this.gridControl3.AllowDrop = true;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(648, 366);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.LOAIPHONG1,
            this.PHONG1,
            this.SLKHACH,
            this.TENKHACH,
            this.CMT,
            this.NGAYCMT,
            this.VISA,
            this.NGAYVISA,
            this.QUOCTICH});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 38;
            // 
            // LOAIPHONG1
            // 
            this.LOAIPHONG1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LOAIPHONG1.AppearanceCell.Options.UseFont = true;
            this.LOAIPHONG1.AppearanceCell.Options.UseTextOptions = true;
            this.LOAIPHONG1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.LOAIPHONG1.AppearanceHeader.Options.UseFont = true;
            this.LOAIPHONG1.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAIPHONG1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LOAIPHONG1.Caption = "Loại phòng";
            this.LOAIPHONG1.FieldName = "LOAIPHONG1";
            this.LOAIPHONG1.Name = "LOAIPHONG1";
            this.LOAIPHONG1.OptionsColumn.AllowEdit = false;
            this.LOAIPHONG1.OptionsColumn.AllowFocus = false;
            this.LOAIPHONG1.OptionsColumn.FixedWidth = true;
            this.LOAIPHONG1.Visible = true;
            this.LOAIPHONG1.VisibleIndex = 1;
            this.LOAIPHONG1.Width = 80;
            // 
            // PHONG1
            // 
            this.PHONG1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.PHONG1.AppearanceCell.Options.UseFont = true;
            this.PHONG1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.PHONG1.AppearanceHeader.Options.UseFont = true;
            this.PHONG1.AppearanceHeader.Options.UseTextOptions = true;
            this.PHONG1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHONG1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHONG1.Caption = "Phòng";
            this.PHONG1.FieldName = "PHONG1";
            this.PHONG1.Name = "PHONG1";
            this.PHONG1.OptionsColumn.AllowEdit = false;
            this.PHONG1.OptionsColumn.AllowFocus = false;
            this.PHONG1.OptionsColumn.FixedWidth = true;
            this.PHONG1.Visible = true;
            this.PHONG1.VisibleIndex = 2;
            this.PHONG1.Width = 51;
            // 
            // SLKHACH
            // 
            this.SLKHACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.SLKHACH.AppearanceCell.Options.UseFont = true;
            this.SLKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.SLKHACH.AppearanceHeader.Options.UseFont = true;
            this.SLKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.SLKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SLKHACH.Caption = "SL Khách";
            this.SLKHACH.FieldName = "SLKHACH";
            this.SLKHACH.Name = "SLKHACH";
            this.SLKHACH.OptionsColumn.AllowEdit = false;
            this.SLKHACH.OptionsColumn.AllowFocus = false;
            this.SLKHACH.OptionsColumn.FixedWidth = true;
            this.SLKHACH.Visible = true;
            this.SLKHACH.VisibleIndex = 3;
            this.SLKHACH.Width = 66;
            // 
            // TENKHACH
            // 
            this.TENKHACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TENKHACH.AppearanceCell.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TENKHACH.AppearanceHeader.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKHACH.Caption = "Tên Khách";
            this.TENKHACH.FieldName = "TENKHACH";
            this.TENKHACH.Name = "TENKHACH";
            this.TENKHACH.OptionsColumn.AllowEdit = false;
            this.TENKHACH.OptionsColumn.AllowFocus = false;
            this.TENKHACH.OptionsColumn.FixedWidth = true;
            this.TENKHACH.Visible = true;
            this.TENKHACH.VisibleIndex = 4;
            this.TENKHACH.Width = 81;
            // 
            // CMT
            // 
            this.CMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.CMT.AppearanceCell.Options.UseFont = true;
            this.CMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.CMT.AppearanceHeader.Options.UseFont = true;
            this.CMT.AppearanceHeader.Options.UseTextOptions = true;
            this.CMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMT.Caption = "CMT/Passport";
            this.CMT.FieldName = "CMT";
            this.CMT.Name = "CMT";
            this.CMT.OptionsColumn.AllowEdit = false;
            this.CMT.OptionsColumn.AllowFocus = false;
            this.CMT.OptionsColumn.FixedWidth = true;
            this.CMT.Visible = true;
            this.CMT.VisibleIndex = 5;
            this.CMT.Width = 100;
            // 
            // NGAYCMT
            // 
            this.NGAYCMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.NGAYCMT.AppearanceCell.Options.UseFont = true;
            this.NGAYCMT.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.NGAYCMT.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMT.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.Caption = "Ngày CMT/Passport";
            this.NGAYCMT.FieldName = "NGAYCMT";
            this.NGAYCMT.Name = "NGAYCMT";
            this.NGAYCMT.OptionsColumn.AllowEdit = false;
            this.NGAYCMT.OptionsColumn.AllowFocus = false;
            this.NGAYCMT.OptionsColumn.FixedWidth = true;
            this.NGAYCMT.Visible = true;
            this.NGAYCMT.VisibleIndex = 6;
            this.NGAYCMT.Width = 137;
            // 
            // VISA
            // 
            this.VISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.VISA.AppearanceCell.Options.UseFont = true;
            this.VISA.AppearanceCell.Options.UseTextOptions = true;
            this.VISA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.VISA.AppearanceHeader.Options.UseFont = true;
            this.VISA.AppearanceHeader.Options.UseTextOptions = true;
            this.VISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.Caption = "Visa";
            this.VISA.FieldName = "VISA";
            this.VISA.Name = "VISA";
            this.VISA.OptionsColumn.AllowEdit = false;
            this.VISA.OptionsColumn.AllowFocus = false;
            this.VISA.OptionsColumn.AllowMove = false;
            this.VISA.Visible = true;
            this.VISA.VisibleIndex = 7;
            this.VISA.Width = 47;
            // 
            // NGAYVISA
            // 
            this.NGAYVISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.NGAYVISA.AppearanceCell.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.NGAYVISA.AppearanceHeader.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYVISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYVISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYVISA.Caption = "Ngày Visa";
            this.NGAYVISA.FieldName = "NGAYVISA";
            this.NGAYVISA.Name = "NGAYVISA";
            this.NGAYVISA.Visible = true;
            this.NGAYVISA.VisibleIndex = 8;
            this.NGAYVISA.Width = 72;
            // 
            // QUOCTICH
            // 
            this.QUOCTICH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.QUOCTICH.AppearanceCell.Options.UseFont = true;
            this.QUOCTICH.AppearanceCell.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.QUOCTICH.AppearanceHeader.Options.UseFont = true;
            this.QUOCTICH.AppearanceHeader.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.Caption = "Quốc Tịch";
            this.QUOCTICH.FieldName = "QUOCTICH";
            this.QUOCTICH.Name = "QUOCTICH";
            this.QUOCTICH.Visible = true;
            this.QUOCTICH.VisibleIndex = 9;
            this.QUOCTICH.Width = 71;
            // 
            // panelControl25
            // 
            this.panelControl25.Controls.Add(this.textEdit68);
            this.panelControl25.Controls.Add(this.labelControl38);
            this.panelControl25.Controls.Add(this.textEdit71);
            this.panelControl25.Controls.Add(this.labelControl42);
            this.panelControl25.Location = new System.Drawing.Point(356, 6);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(649, 43);
            this.panelControl25.TabIndex = 50;
            // 
            // textEdit68
            // 
            this.textEdit68.EditValue = "";
            this.textEdit68.EnterMoveNextControl = true;
            this.textEdit68.Location = new System.Drawing.Point(494, 8);
            this.textEdit68.Name = "textEdit68";
            this.textEdit68.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit68.Properties.Appearance.Options.UseFont = true;
            this.textEdit68.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit68.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit68.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit68.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit68.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit68.Size = new System.Drawing.Size(135, 26);
            this.textEdit68.TabIndex = 34;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(4, 11);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(103, 19);
            this.labelControl38.TabIndex = 1;
            this.labelControl38.Text = "Tên Khách Đặt";
            // 
            // textEdit71
            // 
            this.textEdit71.EditValue = "";
            this.textEdit71.EnterMoveNextControl = true;
            this.textEdit71.Location = new System.Drawing.Point(120, 8);
            this.textEdit71.Name = "textEdit71";
            this.textEdit71.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit71.Properties.Appearance.Options.UseFont = true;
            this.textEdit71.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit71.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit71.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit71.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit71.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit71.Size = new System.Drawing.Size(249, 26);
            this.textEdit71.TabIndex = 19;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(401, 11);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(80, 19);
            this.labelControl42.TabIndex = 21;
            this.labelControl42.Text = "Số Booking";
            // 
            // panelControl24
            // 
            this.panelControl24.Controls.Add(this.textEdit65);
            this.panelControl24.Controls.Add(this.labelControl35);
            this.panelControl24.Controls.Add(this.textEdit69);
            this.panelControl24.Controls.Add(this.labelControl39);
            this.panelControl24.Controls.Add(this.textEdit70);
            this.panelControl24.Controls.Add(this.labelControl40);
            this.panelControl24.Controls.Add(this.textEdit74);
            this.panelControl24.Controls.Add(this.labelControl41);
            this.panelControl24.Location = new System.Drawing.Point(4, 340);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(337, 122);
            this.panelControl24.TabIndex = 49;
            // 
            // textEdit65
            // 
            this.textEdit65.EditValue = "";
            this.textEdit65.EnterMoveNextControl = true;
            this.textEdit65.Location = new System.Drawing.Point(94, 89);
            this.textEdit65.Name = "textEdit65";
            this.textEdit65.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit65.Properties.Appearance.Options.UseFont = true;
            this.textEdit65.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit65.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit65.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit65.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit65.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit65.Size = new System.Drawing.Size(108, 26);
            this.textEdit65.TabIndex = 38;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(6, 92);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(53, 19);
            this.labelControl35.TabIndex = 37;
            this.labelControl35.Text = "Còn Lại";
            // 
            // textEdit69
            // 
            this.textEdit69.EditValue = "";
            this.textEdit69.EnterMoveNextControl = true;
            this.textEdit69.Location = new System.Drawing.Point(212, 45);
            this.textEdit69.Name = "textEdit69";
            this.textEdit69.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit69.Properties.Appearance.Options.UseFont = true;
            this.textEdit69.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit69.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit69.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit69.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit69.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit69.Size = new System.Drawing.Size(112, 32);
            this.textEdit69.TabIndex = 36;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(215, 23);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(79, 19);
            this.labelControl39.TabIndex = 35;
            this.labelControl39.Text = "Tổng Cộng";
            // 
            // textEdit70
            // 
            this.textEdit70.EditValue = "";
            this.textEdit70.EnterMoveNextControl = true;
            this.textEdit70.Location = new System.Drawing.Point(94, 48);
            this.textEdit70.Name = "textEdit70";
            this.textEdit70.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit70.Properties.Appearance.Options.UseFont = true;
            this.textEdit70.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit70.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit70.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit70.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit70.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit70.Size = new System.Drawing.Size(108, 26);
            this.textEdit70.TabIndex = 34;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(6, 10);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(81, 19);
            this.labelControl40.TabIndex = 1;
            this.labelControl40.Text = "Tiền Phòng";
            // 
            // textEdit74
            // 
            this.textEdit74.EditValue = "";
            this.textEdit74.EnterMoveNextControl = true;
            this.textEdit74.Location = new System.Drawing.Point(94, 7);
            this.textEdit74.Name = "textEdit74";
            this.textEdit74.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit74.Properties.Appearance.Options.UseFont = true;
            this.textEdit74.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit74.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit74.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit74.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit74.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit74.Size = new System.Drawing.Size(108, 26);
            this.textEdit74.TabIndex = 19;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(6, 49);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(86, 19);
            this.labelControl41.TabIndex = 21;
            this.labelControl41.Text = "Thanh Toán";
            // 
            // panelControl23
            // 
            this.panelControl23.Controls.Add(this.dtp_ngayden);
            this.panelControl23.Controls.Add(this.dtp_ngaydi);
            this.panelControl23.Controls.Add(this.textEdit64);
            this.panelControl23.Controls.Add(this.labelControl34);
            this.panelControl23.Controls.Add(this.textEdit63);
            this.panelControl23.Controls.Add(this.textEdit62);
            this.panelControl23.Controls.Add(this.labelControl33);
            this.panelControl23.Controls.Add(this.textEdit61);
            this.panelControl23.Controls.Add(this.labelControl32);
            this.panelControl23.Controls.Add(this.labelControl31);
            this.panelControl23.Controls.Add(this.textEdit60);
            this.panelControl23.Controls.Add(this.labelControl30);
            this.panelControl23.Controls.Add(this.textEdit58);
            this.panelControl23.Controls.Add(this.textEdit57);
            this.panelControl23.Controls.Add(this.textEdit56);
            this.panelControl23.Controls.Add(this.labelControl23);
            this.panelControl23.Controls.Add(this.labelControl26);
            this.panelControl23.Controls.Add(this.labelControl27);
            this.panelControl23.Controls.Add(this.labelControl28);
            this.panelControl23.Controls.Add(this.labelControl29);
            this.panelControl23.Location = new System.Drawing.Point(4, 6);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(337, 328);
            this.panelControl23.TabIndex = 48;
            // 
            // dtp_ngayden
            // 
            this.dtp_ngayden.EditValue = null;
            this.dtp_ngayden.EnterMoveNextControl = true;
            this.dtp_ngayden.Location = new System.Drawing.Point(94, 5);
            this.dtp_ngayden.Name = "dtp_ngayden";
            this.dtp_ngayden.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngayden.Properties.Appearance.Options.UseFont = true;
            this.dtp_ngayden.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngayden.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtp_ngayden.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_ngayden.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngayden.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngayden.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtp_ngayden.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp_ngayden.Size = new System.Drawing.Size(108, 26);
            this.dtp_ngayden.TabIndex = 45;
            // 
            // dtp_ngaydi
            // 
            this.dtp_ngaydi.EditValue = null;
            this.dtp_ngaydi.EnterMoveNextControl = true;
            this.dtp_ngaydi.Location = new System.Drawing.Point(94, 50);
            this.dtp_ngaydi.Name = "dtp_ngaydi";
            this.dtp_ngaydi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaydi.Properties.Appearance.Options.UseFont = true;
            this.dtp_ngaydi.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaydi.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtp_ngaydi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_ngaydi.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngaydi.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_ngaydi.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtp_ngaydi.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtp_ngaydi.Size = new System.Drawing.Size(108, 26);
            this.dtp_ngaydi.TabIndex = 46;
            // 
            // textEdit64
            // 
            this.textEdit64.EditValue = "";
            this.textEdit64.EnterMoveNextControl = true;
            this.textEdit64.Location = new System.Drawing.Point(94, 275);
            this.textEdit64.Name = "textEdit64";
            this.textEdit64.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit64.Properties.Appearance.Options.UseFont = true;
            this.textEdit64.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit64.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit64.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit64.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit64.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit64.Size = new System.Drawing.Size(231, 26);
            this.textEdit64.TabIndex = 44;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(6, 278);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(57, 19);
            this.labelControl34.TabIndex = 43;
            this.labelControl34.Text = "Ghi Chú";
            // 
            // textEdit63
            // 
            this.textEdit63.EditValue = "";
            this.textEdit63.EnterMoveNextControl = true;
            this.textEdit63.Location = new System.Drawing.Point(212, 204);
            this.textEdit63.Name = "textEdit63";
            this.textEdit63.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit63.Properties.Appearance.Options.UseFont = true;
            this.textEdit63.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit63.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit63.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit63.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit63.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit63.Size = new System.Drawing.Size(112, 32);
            this.textEdit63.TabIndex = 42;
            // 
            // textEdit62
            // 
            this.textEdit62.EditValue = "";
            this.textEdit62.EnterMoveNextControl = true;
            this.textEdit62.Location = new System.Drawing.Point(212, 119);
            this.textEdit62.Name = "textEdit62";
            this.textEdit62.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit62.Properties.Appearance.Options.UseFont = true;
            this.textEdit62.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit62.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit62.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit62.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit62.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit62.Size = new System.Drawing.Size(112, 32);
            this.textEdit62.TabIndex = 41;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(214, 183);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(107, 19);
            this.labelControl33.TabIndex = 40;
            this.labelControl33.Text = "Tổng Số Khách";
            // 
            // textEdit61
            // 
            this.textEdit61.EditValue = "";
            this.textEdit61.EnterMoveNextControl = true;
            this.textEdit61.Location = new System.Drawing.Point(94, 230);
            this.textEdit61.Name = "textEdit61";
            this.textEdit61.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit61.Properties.Appearance.Options.UseFont = true;
            this.textEdit61.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit61.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit61.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit61.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit61.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit61.Size = new System.Drawing.Size(108, 26);
            this.textEdit61.TabIndex = 39;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(6, 233);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(52, 19);
            this.labelControl32.TabIndex = 38;
            this.labelControl32.Text = "Trẻ Em";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(212, 97);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(110, 19);
            this.labelControl31.TabIndex = 37;
            this.labelControl31.Text = "Tổng Số Phòng";
            // 
            // textEdit60
            // 
            this.textEdit60.EditValue = "";
            this.textEdit60.EnterMoveNextControl = true;
            this.textEdit60.Location = new System.Drawing.Point(212, 29);
            this.textEdit60.Name = "textEdit60";
            this.textEdit60.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit60.Properties.Appearance.Options.UseFont = true;
            this.textEdit60.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit60.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit60.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit60.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit60.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit60.Size = new System.Drawing.Size(112, 32);
            this.textEdit60.TabIndex = 36;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(215, 7);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(110, 19);
            this.labelControl30.TabIndex = 35;
            this.labelControl30.Text = "Số ngày lưu trú";
            // 
            // textEdit58
            // 
            this.textEdit58.EditValue = "";
            this.textEdit58.EnterMoveNextControl = true;
            this.textEdit58.Location = new System.Drawing.Point(94, 185);
            this.textEdit58.Name = "textEdit58";
            this.textEdit58.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit58.Properties.Appearance.Options.UseFont = true;
            this.textEdit58.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit58.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit58.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit58.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit58.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit58.Size = new System.Drawing.Size(108, 26);
            this.textEdit58.TabIndex = 33;
            // 
            // textEdit57
            // 
            this.textEdit57.EditValue = "";
            this.textEdit57.EnterMoveNextControl = true;
            this.textEdit57.Location = new System.Drawing.Point(94, 140);
            this.textEdit57.Name = "textEdit57";
            this.textEdit57.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit57.Properties.Appearance.Options.UseFont = true;
            this.textEdit57.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit57.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit57.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit57.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit57.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit57.Size = new System.Drawing.Size(108, 26);
            this.textEdit57.TabIndex = 32;
            // 
            // textEdit56
            // 
            this.textEdit56.EditValue = "";
            this.textEdit56.EnterMoveNextControl = true;
            this.textEdit56.Location = new System.Drawing.Point(94, 95);
            this.textEdit56.Name = "textEdit56";
            this.textEdit56.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit56.Properties.Appearance.Options.UseFont = true;
            this.textEdit56.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit56.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit56.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit56.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit56.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit56.Size = new System.Drawing.Size(108, 26);
            this.textEdit56.TabIndex = 31;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(6, 8);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(67, 19);
            this.labelControl23.TabIndex = 1;
            this.labelControl23.Text = "Ngày đến";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Location = new System.Drawing.Point(6, 53);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(54, 19);
            this.labelControl26.TabIndex = 21;
            this.labelControl26.Text = "Ngày đi";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(6, 98);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(79, 19);
            this.labelControl27.TabIndex = 24;
            this.labelControl27.Text = "Loại Phòng";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(6, 143);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(68, 19);
            this.labelControl28.TabIndex = 27;
            this.labelControl28.Text = "Số Phòng";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(6, 188);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(74, 19);
            this.labelControl29.TabIndex = 30;
            this.labelControl29.Text = "Người Lớn";
            // 
            // palbook_Dangky
            // 
            this.palbook_Dangky.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbook_Dangky.Controls.Add(this.panelControl23);
            this.palbook_Dangky.Controls.Add(this.xtraTabControl1);
            this.palbook_Dangky.Controls.Add(this.panelControl24);
            this.palbook_Dangky.Controls.Add(this.panelControl25);
            this.palbook_Dangky.Location = new System.Drawing.Point(3, 3);
            this.palbook_Dangky.Name = "palbook_Dangky";
            this.palbook_Dangky.Size = new System.Drawing.Size(1013, 472);
            this.palbook_Dangky.TabIndex = 52;
            // 
            // Frm_Booking_Dangky
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.palbook_Dangky);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Booking_Dangky";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.panelControl25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            this.panelControl24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngayden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_ngaydi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palbook_Dangky)).EndInit();
            this.palbook_Dangky.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.TextEdit textEdit77;
        private DevExpress.XtraEditors.TextEdit textEdit78;
        private DevExpress.XtraEditors.TextEdit textEdit79;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.TextEdit textEdit76;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit textEdit73;
        private DevExpress.XtraEditors.TextEdit textEdit75;
        private DevExpress.XtraEditors.TextEdit textEdit53;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit textEdit59;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit textEdit66;
        private DevExpress.XtraEditors.TextEdit textEdit67;
        private DevExpress.XtraEditors.TextEdit textEdit72;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIPHONG1;
        private DevExpress.XtraGrid.Columns.GridColumn PHONG1;
        private DevExpress.XtraGrid.Columns.GridColumn SLKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn TENKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn CMT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMT;
        private DevExpress.XtraGrid.Columns.GridColumn VISA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYVISA;
        private DevExpress.XtraGrid.Columns.GridColumn QUOCTICH;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.TextEdit textEdit68;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit textEdit71;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit65;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit textEdit69;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit textEdit70;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit textEdit74;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.DateEdit dtp_ngayden;
        private DevExpress.XtraEditors.DateEdit dtp_ngaydi;
        private DevExpress.XtraEditors.TextEdit textEdit64;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit textEdit63;
        private DevExpress.XtraEditors.TextEdit textEdit62;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit textEdit61;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit textEdit60;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit textEdit58;
        private DevExpress.XtraEditors.TextEdit textEdit57;
        private DevExpress.XtraEditors.TextEdit textEdit56;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.PanelControl palbook_Dangky;





    }
}