﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Letan_Nguoidikem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pal_1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.pal_khuvuc = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Xuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYVISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUOCTICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).BeginInit();
            this.pal_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            this.pal_khuvuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.panel);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1019, 477);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pal_1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(450, 477);
            this.panelControl5.TabIndex = 4;
            // 
            // pal_1
            // 
            this.pal_1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pal_1.Appearance.Options.UseBackColor = true;
            this.pal_1.Controls.Add(this.panelControl9);
            this.pal_1.Controls.Add(this.panelControl6);
            this.pal_1.Controls.Add(this.pal_sotrang);
            this.pal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_1.Location = new System.Drawing.Point(0, 0);
            this.pal_1.Name = "pal_1";
            this.pal_1.Size = new System.Drawing.Size(450, 477);
            this.pal_1.TabIndex = 1;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.pal_ban);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(2, 37);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(446, 403);
            this.panelControl9.TabIndex = 56;
            // 
            // pal_ban
            // 
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.richTextBox5);
            this.pal_ban.Controls.Add(this.richTextBox6);
            this.pal_ban.Controls.Add(this.richTextBox1);
            this.pal_ban.Controls.Add(this.richTextBox3);
            this.pal_ban.Controls.Add(this.richTextBox4);
            this.pal_ban.Controls.Add(this.richTextBox2);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(442, 399);
            this.pal_ban.TabIndex = 55;
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(222, 269);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(212, 125);
            this.richTextBox5.TabIndex = 42;
            this.richTextBox5.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.Location = new System.Drawing.Point(4, 269);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.ReadOnly = true;
            this.richTextBox6.Size = new System.Drawing.Size(212, 125);
            this.richTextBox6.TabIndex = 41;
            this.richTextBox6.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(4, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(212, 125);
            this.richTextBox1.TabIndex = 37;
            this.richTextBox1.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(222, 137);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(212, 125);
            this.richTextBox3.TabIndex = 40;
            this.richTextBox3.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.Location = new System.Drawing.Point(4, 137);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(212, 125);
            this.richTextBox4.TabIndex = 39;
            this.richTextBox4.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(222, 5);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(212, 125);
            this.richTextBox2.TabIndex = 38;
            this.richTextBox2.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bnt_trangxuong);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 440);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(446, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(446, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.bnt_tranglen);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(446, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(0, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(446, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.pal_khuvuc);
            this.panel.Controls.Add(this.panelControl7);
            this.panel.Controls.Add(this.panelControl11);
            this.panel.Controls.Add(this.pal_len);
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel.Location = new System.Drawing.Point(450, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 477);
            this.panel.TabIndex = 2;
            // 
            // pal_khuvuc
            // 
            this.pal_khuvuc.BackColor = System.Drawing.Color.Transparent;
            this.pal_khuvuc.Controls.Add(this.simpleButton4);
            this.pal_khuvuc.Controls.Add(this.simpleButton5);
            this.pal_khuvuc.Controls.Add(this.simpleButton1);
            this.pal_khuvuc.Controls.Add(this.simpleButton2);
            this.pal_khuvuc.Controls.Add(this.simpleButton3);
            this.pal_khuvuc.Controls.Add(this.simpleButton6);
            this.pal_khuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_khuvuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_khuvuc.Location = new System.Drawing.Point(2, 37);
            this.pal_khuvuc.Margin = new System.Windows.Forms.Padding(0);
            this.pal_khuvuc.Name = "pal_khuvuc";
            this.pal_khuvuc.Size = new System.Drawing.Size(131, 403);
            this.pal_khuvuc.TabIndex = 29;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(4, 4);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(122, 58);
            this.simpleButton4.TabIndex = 34;
            this.simpleButton4.Text = "Phòng Đơn";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton5.Location = new System.Drawing.Point(4, 70);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(122, 58);
            this.simpleButton5.TabIndex = 35;
            this.simpleButton5.Text = "Phòng Đôi";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(4, 136);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(122, 58);
            this.simpleButton1.TabIndex = 36;
            this.simpleButton1.Text = "Phòng Vip";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(4, 202);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(122, 58);
            this.simpleButton2.TabIndex = 37;
            this.simpleButton2.Text = "Phòng Delux";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton3.Location = new System.Drawing.Point(4, 268);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(122, 58);
            this.simpleButton3.TabIndex = 38;
            this.simpleButton3.Text = "Tầng 1";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton6.Location = new System.Drawing.Point(4, 334);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(122, 58);
            this.simpleButton6.TabIndex = 39;
            this.simpleButton6.Text = "Tầng 2";
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(915, 291);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(77, 168);
            this.panelControl7.TabIndex = 1;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.bnt_Xuong);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 440);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(131, 35);
            this.panelControl11.TabIndex = 28;
            // 
            // bnt_Xuong
            // 
            this.bnt_Xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Xuong.Appearance.Options.UseBackColor = true;
            this.bnt_Xuong.Appearance.Options.UseFont = true;
            this.bnt_Xuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bnt_Xuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Xuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_Xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Xuong.Name = "bnt_Xuong";
            this.bnt_Xuong.Size = new System.Drawing.Size(131, 35);
            this.bnt_Xuong.TabIndex = 2;
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.bnt_Len);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(0, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(131, 35);
            this.bnt_Len.TabIndex = 26;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.groupControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(585, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(434, 477);
            this.panelControl4.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.groupControl1.Controls.Add(this.gridControl3);
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(430, 473);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông Tin Khách";
            // 
            // gridControl3
            // 
            this.gridControl3.AllowDrop = true;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(2, 37);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(426, 434);
            this.gridControl3.TabIndex = 7;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENKHACH,
            this.CMT,
            this.NGAYCMT,
            this.VISA,
            this.NGAYVISA,
            this.QUOCTICH});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 38;
            // 
            // TENKHACH
            // 
            this.TENKHACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TENKHACH.AppearanceCell.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TENKHACH.AppearanceHeader.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKHACH.Caption = "Tên Khách";
            this.TENKHACH.FieldName = "TENKHACH";
            this.TENKHACH.Name = "TENKHACH";
            this.TENKHACH.OptionsColumn.AllowEdit = false;
            this.TENKHACH.OptionsColumn.AllowFocus = false;
            this.TENKHACH.OptionsColumn.FixedWidth = true;
            this.TENKHACH.Visible = true;
            this.TENKHACH.VisibleIndex = 1;
            this.TENKHACH.Width = 90;
            // 
            // CMT
            // 
            this.CMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.CMT.AppearanceCell.Options.UseFont = true;
            this.CMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.CMT.AppearanceHeader.Options.UseFont = true;
            this.CMT.AppearanceHeader.Options.UseTextOptions = true;
            this.CMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMT.Caption = "CMT/Passport";
            this.CMT.FieldName = "CMT";
            this.CMT.Name = "CMT";
            this.CMT.OptionsColumn.AllowEdit = false;
            this.CMT.OptionsColumn.AllowFocus = false;
            this.CMT.OptionsColumn.FixedWidth = true;
            this.CMT.Visible = true;
            this.CMT.VisibleIndex = 2;
            this.CMT.Width = 100;
            // 
            // NGAYCMT
            // 
            this.NGAYCMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.NGAYCMT.AppearanceCell.Options.UseFont = true;
            this.NGAYCMT.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.NGAYCMT.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMT.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.Caption = "Ngày CMT/Passport";
            this.NGAYCMT.FieldName = "NGAYCMT";
            this.NGAYCMT.Name = "NGAYCMT";
            this.NGAYCMT.OptionsColumn.AllowEdit = false;
            this.NGAYCMT.OptionsColumn.AllowFocus = false;
            this.NGAYCMT.OptionsColumn.FixedWidth = true;
            this.NGAYCMT.Visible = true;
            this.NGAYCMT.VisibleIndex = 3;
            this.NGAYCMT.Width = 137;
            // 
            // VISA
            // 
            this.VISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.VISA.AppearanceCell.Options.UseFont = true;
            this.VISA.AppearanceCell.Options.UseTextOptions = true;
            this.VISA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.VISA.AppearanceHeader.Options.UseFont = true;
            this.VISA.AppearanceHeader.Options.UseTextOptions = true;
            this.VISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.Caption = "Visa";
            this.VISA.FieldName = "VISA";
            this.VISA.Name = "VISA";
            this.VISA.OptionsColumn.AllowEdit = false;
            this.VISA.OptionsColumn.AllowFocus = false;
            this.VISA.OptionsColumn.AllowMove = false;
            this.VISA.Visible = true;
            this.VISA.VisibleIndex = 4;
            this.VISA.Width = 66;
            // 
            // NGAYVISA
            // 
            this.NGAYVISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.NGAYVISA.AppearanceCell.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.NGAYVISA.AppearanceHeader.Options.UseFont = true;
            this.NGAYVISA.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYVISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYVISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYVISA.Caption = "Ngày Visa";
            this.NGAYVISA.FieldName = "NGAYVISA";
            this.NGAYVISA.Name = "NGAYVISA";
            this.NGAYVISA.Visible = true;
            this.NGAYVISA.VisibleIndex = 5;
            this.NGAYVISA.Width = 87;
            // 
            // QUOCTICH
            // 
            this.QUOCTICH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.QUOCTICH.AppearanceCell.Options.UseFont = true;
            this.QUOCTICH.AppearanceCell.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.QUOCTICH.AppearanceHeader.Options.UseFont = true;
            this.QUOCTICH.AppearanceHeader.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QUOCTICH.Caption = "Quốc Tịch";
            this.QUOCTICH.FieldName = "QUOCTICH";
            this.QUOCTICH.Name = "QUOCTICH";
            this.QUOCTICH.Visible = true;
            this.QUOCTICH.VisibleIndex = 6;
            this.QUOCTICH.Width = 102;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 27);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(426, 10);
            this.panelControl2.TabIndex = 8;
            // 
            // Frm_Letan_Nguoidikem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Letan_Nguoidikem";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).EndInit();
            this.pal_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            this.pal_khuvuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pal_1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.PanelControl panel;
        private System.Windows.Forms.FlowLayoutPanel pal_khuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton bnt_Xuong;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TENKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn CMT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMT;
        private DevExpress.XtraGrid.Columns.GridColumn VISA;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYVISA;
        private DevExpress.XtraGrid.Columns.GridColumn QUOCTICH;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.PanelControl panelControl2;







    }
}