﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Letan_Vesinhphong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pal_1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.pal_khuvuc = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Xuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgaycapnhat = new DevExpress.XtraEditors.DateEdit();
            this.timeBD = new DevExpress.XtraEditors.TimeEdit();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).BeginInit();
            this.pal_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            this.pal_khuvuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaycapnhat.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaycapnhat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBD.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.panel);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1019, 477);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pal_1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(450, 477);
            this.panelControl5.TabIndex = 4;
            // 
            // pal_1
            // 
            this.pal_1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pal_1.Appearance.Options.UseBackColor = true;
            this.pal_1.Controls.Add(this.panelControl9);
            this.pal_1.Controls.Add(this.panelControl6);
            this.pal_1.Controls.Add(this.pal_sotrang);
            this.pal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_1.Location = new System.Drawing.Point(0, 0);
            this.pal_1.Name = "pal_1";
            this.pal_1.Size = new System.Drawing.Size(450, 477);
            this.pal_1.TabIndex = 1;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.pal_ban);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(2, 37);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(446, 403);
            this.panelControl9.TabIndex = 56;
            // 
            // pal_ban
            // 
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.richTextBox5);
            this.pal_ban.Controls.Add(this.richTextBox6);
            this.pal_ban.Controls.Add(this.richTextBox1);
            this.pal_ban.Controls.Add(this.richTextBox3);
            this.pal_ban.Controls.Add(this.richTextBox4);
            this.pal_ban.Controls.Add(this.richTextBox2);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(442, 399);
            this.pal_ban.TabIndex = 55;
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(222, 269);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(212, 125);
            this.richTextBox5.TabIndex = 42;
            this.richTextBox5.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.Location = new System.Drawing.Point(4, 269);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.ReadOnly = true;
            this.richTextBox6.Size = new System.Drawing.Size(212, 125);
            this.richTextBox6.TabIndex = 41;
            this.richTextBox6.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(4, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(212, 125);
            this.richTextBox1.TabIndex = 37;
            this.richTextBox1.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(222, 137);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(212, 125);
            this.richTextBox3.TabIndex = 40;
            this.richTextBox3.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.Location = new System.Drawing.Point(4, 137);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(212, 125);
            this.richTextBox4.TabIndex = 39;
            this.richTextBox4.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(222, 5);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(212, 125);
            this.richTextBox2.TabIndex = 38;
            this.richTextBox2.Text = "Phòng 101 - Phòng Đơn \nĐoàn : Sài Gòn Tour\nKH : Võ Huy Trung (03)\nNgày vào : 13h0" +
                "0 27/6/2014\nNgày Trả : 30/06/2014\nQuốc Tịch : Việt Nam";
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bnt_trangxuong);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 440);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(446, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(446, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.bnt_tranglen);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(446, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(0, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(446, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.pal_khuvuc);
            this.panel.Controls.Add(this.panelControl7);
            this.panel.Controls.Add(this.panelControl11);
            this.panel.Controls.Add(this.pal_len);
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel.Location = new System.Drawing.Point(450, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 477);
            this.panel.TabIndex = 2;
            // 
            // pal_khuvuc
            // 
            this.pal_khuvuc.BackColor = System.Drawing.Color.Transparent;
            this.pal_khuvuc.Controls.Add(this.simpleButton4);
            this.pal_khuvuc.Controls.Add(this.simpleButton5);
            this.pal_khuvuc.Controls.Add(this.simpleButton1);
            this.pal_khuvuc.Controls.Add(this.simpleButton2);
            this.pal_khuvuc.Controls.Add(this.simpleButton3);
            this.pal_khuvuc.Controls.Add(this.simpleButton6);
            this.pal_khuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_khuvuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_khuvuc.Location = new System.Drawing.Point(2, 37);
            this.pal_khuvuc.Margin = new System.Windows.Forms.Padding(0);
            this.pal_khuvuc.Name = "pal_khuvuc";
            this.pal_khuvuc.Size = new System.Drawing.Size(131, 403);
            this.pal_khuvuc.TabIndex = 29;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(4, 4);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(122, 58);
            this.simpleButton4.TabIndex = 34;
            this.simpleButton4.Text = "Phòng Đơn";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton5.Location = new System.Drawing.Point(4, 70);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(122, 58);
            this.simpleButton5.TabIndex = 35;
            this.simpleButton5.Text = "Phòng Đôi";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(4, 136);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(122, 58);
            this.simpleButton1.TabIndex = 36;
            this.simpleButton1.Text = "Phòng Vip";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(4, 202);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(122, 58);
            this.simpleButton2.TabIndex = 37;
            this.simpleButton2.Text = "Phòng Delux";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton3.Location = new System.Drawing.Point(4, 268);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(122, 58);
            this.simpleButton3.TabIndex = 38;
            this.simpleButton3.Text = "Tầng 1";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton6.Location = new System.Drawing.Point(4, 334);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(122, 58);
            this.simpleButton6.TabIndex = 39;
            this.simpleButton6.Text = "Tầng 2";
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(915, 291);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(77, 168);
            this.panelControl7.TabIndex = 1;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.bnt_Xuong);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 440);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(131, 35);
            this.panelControl11.TabIndex = 28;
            // 
            // bnt_Xuong
            // 
            this.bnt_Xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Xuong.Appearance.Options.UseBackColor = true;
            this.bnt_Xuong.Appearance.Options.UseFont = true;
            this.bnt_Xuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bnt_Xuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Xuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_Xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Xuong.Name = "bnt_Xuong";
            this.bnt_Xuong.Size = new System.Drawing.Size(131, 35);
            this.bnt_Xuong.TabIndex = 2;
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.bnt_Len);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(0, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(131, 35);
            this.bnt_Len.TabIndex = 26;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.groupControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(585, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(434, 477);
            this.panelControl4.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.groupControl1.Controls.Add(this.simpleButton19);
            this.groupControl1.Controls.Add(this.simpleButton18);
            this.groupControl1.Controls.Add(this.simpleButton17);
            this.groupControl1.Controls.Add(this.simpleButton16);
            this.groupControl1.Controls.Add(this.simpleButton15);
            this.groupControl1.Controls.Add(this.simpleButton14);
            this.groupControl1.Controls.Add(this.simpleButton13);
            this.groupControl1.Controls.Add(this.simpleButton12);
            this.groupControl1.Controls.Add(this.simpleButton11);
            this.groupControl1.Controls.Add(this.simpleButton10);
            this.groupControl1.Controls.Add(this.simpleButton9);
            this.groupControl1.Controls.Add(this.simpleButton8);
            this.groupControl1.Controls.Add(this.simpleButton7);
            this.groupControl1.Controls.Add(this.timeBD);
            this.groupControl1.Controls.Add(this.dtpNgaycapnhat);
            this.groupControl1.Controls.Add(this.labelControl53);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(430, 473);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông Tin Phòng";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(66, 78);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(135, 19);
            this.labelControl53.TabIndex = 20;
            this.labelControl53.Text = "Ngày Giờ Cập Nhật";
            // 
            // dtpNgaycapnhat
            // 
            this.dtpNgaycapnhat.EditValue = null;
            this.dtpNgaycapnhat.EnterMoveNextControl = true;
            this.dtpNgaycapnhat.Location = new System.Drawing.Point(15, 117);
            this.dtpNgaycapnhat.Name = "dtpNgaycapnhat";
            this.dtpNgaycapnhat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaycapnhat.Properties.Appearance.Options.UseFont = true;
            this.dtpNgaycapnhat.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaycapnhat.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgaycapnhat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgaycapnhat.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaycapnhat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaycapnhat.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaycapnhat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaycapnhat.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgaycapnhat.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgaycapnhat.Size = new System.Drawing.Size(123, 26);
            this.dtpNgaycapnhat.TabIndex = 39;
            // 
            // timeBD
            // 
            this.timeBD.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeBD.EnterMoveNextControl = true;
            this.timeBD.Location = new System.Drawing.Point(158, 117);
            this.timeBD.Name = "timeBD";
            this.timeBD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeBD.Properties.Appearance.Options.UseFont = true;
            this.timeBD.Properties.Appearance.Options.UseTextOptions = true;
            this.timeBD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timeBD.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.timeBD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeBD.Properties.DisplayFormat.FormatString = "HH:mm";
            this.timeBD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeBD.Properties.EditFormat.FormatString = "HH:mm";
            this.timeBD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeBD.Properties.Mask.EditMask = "HH:mm";
            this.timeBD.Size = new System.Drawing.Size(86, 26);
            this.timeBD.TabIndex = 41;
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton7.Location = new System.Drawing.Point(15, 409);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(71, 58);
            this.simpleButton7.TabIndex = 42;
            this.simpleButton7.Text = "1";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton8.Location = new System.Drawing.Point(15, 257);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(71, 58);
            this.simpleButton8.TabIndex = 43;
            this.simpleButton8.Text = "7";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseBackColor = true;
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton9.Location = new System.Drawing.Point(188, 332);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(71, 58);
            this.simpleButton9.TabIndex = 44;
            this.simpleButton9.Text = "6";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseBackColor = true;
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton10.Location = new System.Drawing.Point(104, 332);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(71, 58);
            this.simpleButton10.TabIndex = 45;
            this.simpleButton10.Text = "5";
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton11.Appearance.Options.UseBackColor = true;
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton11.Location = new System.Drawing.Point(15, 332);
            this.simpleButton11.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(71, 58);
            this.simpleButton11.TabIndex = 46;
            this.simpleButton11.Text = "4";
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton12.Appearance.Options.UseBackColor = true;
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton12.Location = new System.Drawing.Point(188, 409);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(71, 58);
            this.simpleButton12.TabIndex = 47;
            this.simpleButton12.Text = "3";
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseBackColor = true;
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton13.Location = new System.Drawing.Point(104, 409);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(71, 58);
            this.simpleButton13.TabIndex = 48;
            this.simpleButton13.Text = "2";
            // 
            // simpleButton14
            // 
            this.simpleButton14.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton14.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton14.Appearance.Options.UseBackColor = true;
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton14.Location = new System.Drawing.Point(188, 257);
            this.simpleButton14.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(71, 58);
            this.simpleButton14.TabIndex = 49;
            this.simpleButton14.Text = "9";
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton15.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton15.Appearance.Options.UseBackColor = true;
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton15.Location = new System.Drawing.Point(104, 257);
            this.simpleButton15.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(71, 58);
            this.simpleButton15.TabIndex = 50;
            this.simpleButton15.Text = "8";
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton16.Appearance.Options.UseBackColor = true;
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton16.Location = new System.Drawing.Point(275, 332);
            this.simpleButton16.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(142, 135);
            this.simpleButton16.TabIndex = 51;
            this.simpleButton16.Text = "OK";
            this.simpleButton16.Click += new System.EventHandler(this.simpleButton16_Click);
            // 
            // simpleButton17
            // 
            this.simpleButton17.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton17.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton17.Appearance.Options.UseBackColor = true;
            this.simpleButton17.Appearance.Options.UseFont = true;
            this.simpleButton17.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton17.Location = new System.Drawing.Point(275, 257);
            this.simpleButton17.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(142, 58);
            this.simpleButton17.TabIndex = 52;
            this.simpleButton17.Text = "ESC";
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(161)))), ((int)(((byte)(74)))));
            this.simpleButton18.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton18.Appearance.Options.UseBackColor = true;
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton18.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton18.Location = new System.Drawing.Point(275, 143);
            this.simpleButton18.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(142, 58);
            this.simpleButton18.TabIndex = 53;
            this.simpleButton18.Text = "Phòng Bận";
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(239)))));
            this.simpleButton19.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton19.Appearance.Options.UseBackColor = true;
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton19.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton19.Location = new System.Drawing.Point(275, 58);
            this.simpleButton19.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(142, 58);
            this.simpleButton19.TabIndex = 54;
            this.simpleButton19.Text = "Phòng Sạch";
            // 
            // Frm_Letan_Vesinhphong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Letan_Vesinhphong";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).EndInit();
            this.pal_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            this.pal_khuvuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaycapnhat.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaycapnhat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBD.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pal_1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.PanelControl panel;
        private System.Windows.Forms.FlowLayoutPanel pal_khuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton bnt_Xuong;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.DateEdit dtpNgaycapnhat;
        private DevExpress.XtraEditors.TimeEdit timeBD;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;







    }
}