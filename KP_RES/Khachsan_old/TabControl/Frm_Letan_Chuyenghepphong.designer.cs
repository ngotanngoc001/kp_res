﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Booking_Chuyenghepphong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.palbook_tachphong = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.HANGBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.palbook_DaiLy = new DevExpress.XtraEditors.PanelControl();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.palbook_tachphong)).BeginInit();
            this.palbook_tachphong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palbook_DaiLy)).BeginInit();
            this.palbook_DaiLy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // palbook_tachphong
            // 
            this.palbook_tachphong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbook_tachphong.Controls.Add(this.gridControl1);
            this.palbook_tachphong.Controls.Add(this.gridControl3);
            this.palbook_tachphong.Controls.Add(this.panelControl3);
            this.palbook_tachphong.Controls.Add(this.palbook_DaiLy);
            this.palbook_tachphong.Location = new System.Drawing.Point(5, 6);
            this.palbook_tachphong.Name = "palbook_tachphong";
            this.palbook_tachphong.Size = new System.Drawing.Size(1002, 510);
            this.palbook_tachphong.TabIndex = 61;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(546, 158);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(440, 350);
            this.gridControl1.TabIndex = 68;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Hàng bán";
            this.gridColumn1.FieldName = "HANGBAN";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 160;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "SL";
            this.gridColumn2.FieldName = "SL";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 45;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Đơn Giá";
            this.gridColumn3.FieldName = "DONGIA";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 80;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Thành Tiền";
            this.gridColumn4.FieldName = "THANHTIEN";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 80;
            // 
            // gridControl3
            // 
            this.gridControl3.AllowDrop = true;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl3.Location = new System.Drawing.Point(17, 158);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(440, 350);
            this.gridControl3.TabIndex = 67;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.HANGBAN,
            this.SL,
            this.DONGIA,
            this.THANHTIEN});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // HANGBAN
            // 
            this.HANGBAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.HANGBAN.AppearanceCell.Options.UseFont = true;
            this.HANGBAN.AppearanceCell.Options.UseTextOptions = true;
            this.HANGBAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.HANGBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.HANGBAN.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.HANGBAN.AppearanceHeader.Options.UseFont = true;
            this.HANGBAN.AppearanceHeader.Options.UseForeColor = true;
            this.HANGBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HANGBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HANGBAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HANGBAN.Caption = "Hàng bán";
            this.HANGBAN.FieldName = "HANGBAN";
            this.HANGBAN.Name = "HANGBAN";
            this.HANGBAN.OptionsColumn.AllowEdit = false;
            this.HANGBAN.OptionsColumn.AllowFocus = false;
            this.HANGBAN.OptionsColumn.AllowMove = false;
            this.HANGBAN.OptionsColumn.FixedWidth = true;
            this.HANGBAN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.HANGBAN.Visible = true;
            this.HANGBAN.VisibleIndex = 0;
            this.HANGBAN.Width = 160;
            // 
            // SL
            // 
            this.SL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.SL.AppearanceCell.Options.UseFont = true;
            this.SL.AppearanceCell.Options.UseTextOptions = true;
            this.SL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseForeColor = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SL.Caption = "SL";
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowEdit = false;
            this.SL.OptionsColumn.AllowFocus = false;
            this.SL.OptionsColumn.FixedWidth = true;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 1;
            this.SL.Width = 45;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.DONGIA.AppearanceCell.Options.UseFont = true;
            this.DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DONGIA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseForeColor = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DONGIA.Caption = "Đơn Giá";
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowEdit = false;
            this.DONGIA.OptionsColumn.AllowFocus = false;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 2;
            this.DONGIA.Width = 80;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseForeColor = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.Caption = "Thành Tiền";
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.FixedWidth = true;
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 3;
            this.THANHTIEN.Width = 80;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton5);
            this.panelControl3.Controls.Add(this.simpleButton4);
            this.panelControl3.Controls.Add(this.simpleButton3);
            this.panelControl3.Controls.Add(this.simpleButton2);
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Controls.Add(this.simpleButton13);
            this.panelControl3.Location = new System.Drawing.Point(457, 158);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(89, 350);
            this.panelControl3.TabIndex = 66;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton5.Location = new System.Drawing.Point(8, 297);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(72, 41);
            this.simpleButton5.TabIndex = 69;
            this.simpleButton5.Text = "ESC";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(7, 7);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(72, 41);
            this.simpleButton4.TabIndex = 68;
            this.simpleButton4.Text = "OK";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Image = global::KP_RES.Properties.Resources.up33_26;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton3.Location = new System.Drawing.Point(8, 181);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(72, 41);
            this.simpleButton3.TabIndex = 67;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Image = global::KP_RES.Properties.Resources.rewind_26;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(9, 65);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(72, 41);
            this.simpleButton2.TabIndex = 66;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = global::KP_RES.Properties.Resources.rewind_26;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(7, 239);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(72, 41);
            this.simpleButton1.TabIndex = 65;
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseBackColor = true;
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.Image = global::KP_RES.Properties.Resources.up33_26;
            this.simpleButton13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton13.Location = new System.Drawing.Point(8, 123);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(72, 41);
            this.simpleButton13.TabIndex = 64;
            // 
            // palbook_DaiLy
            // 
            this.palbook_DaiLy.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbook_DaiLy.Controls.Add(this.textEdit8);
            this.palbook_DaiLy.Controls.Add(this.textEdit6);
            this.palbook_DaiLy.Controls.Add(this.textEdit5);
            this.palbook_DaiLy.Controls.Add(this.textEdit4);
            this.palbook_DaiLy.Controls.Add(this.textEdit1);
            this.palbook_DaiLy.Controls.Add(this.textEdit2);
            this.palbook_DaiLy.Controls.Add(this.textEdit3);
            this.palbook_DaiLy.Controls.Add(this.textEdit16);
            this.palbook_DaiLy.Controls.Add(this.textEdit15);
            this.palbook_DaiLy.Controls.Add(this.textEdit7);
            this.palbook_DaiLy.Controls.Add(this.textEdit9);
            this.palbook_DaiLy.Controls.Add(this.textEdit14);
            this.palbook_DaiLy.Location = new System.Drawing.Point(7, 4);
            this.palbook_DaiLy.Name = "palbook_DaiLy";
            this.palbook_DaiLy.Size = new System.Drawing.Size(992, 154);
            this.palbook_DaiLy.TabIndex = 64;
            // 
            // textEdit8
            // 
            this.textEdit8.EditValue = "";
            this.textEdit8.Enabled = false;
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(426, 118);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit8.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit8.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit8.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit8.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit8.Size = new System.Drawing.Size(118, 34);
            this.textEdit8.TabIndex = 79;
            // 
            // textEdit6
            // 
            this.textEdit6.EditValue = "";
            this.textEdit6.Enabled = false;
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(426, 81);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit6.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit6.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit6.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit6.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit6.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit6.Size = new System.Drawing.Size(118, 34);
            this.textEdit6.TabIndex = 78;
            // 
            // textEdit5
            // 
            this.textEdit5.EditValue = "";
            this.textEdit5.Enabled = false;
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(426, 44);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit5.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit5.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit5.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit5.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit5.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit5.Size = new System.Drawing.Size(118, 34);
            this.textEdit5.TabIndex = 77;
            // 
            // textEdit4
            // 
            this.textEdit4.EditValue = "";
            this.textEdit4.Enabled = false;
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(426, 3);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit4.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit4.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit4.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit4.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit4.Size = new System.Drawing.Size(123, 38);
            this.textEdit4.TabIndex = 69;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "Bích Tuyền";
            this.textEdit1.Enabled = false;
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(540, 118);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit1.Size = new System.Drawing.Size(439, 34);
            this.textEdit1.TabIndex = 76;
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "VIP";
            this.textEdit2.Enabled = false;
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(540, 81);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit2.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit2.Size = new System.Drawing.Size(439, 34);
            this.textEdit2.TabIndex = 75;
            // 
            // textEdit3
            // 
            this.textEdit3.EditValue = "A2";
            this.textEdit3.Enabled = false;
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(540, 44);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit3.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit3.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit3.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit3.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit3.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit3.Size = new System.Drawing.Size(439, 34);
            this.textEdit3.TabIndex = 74;
            // 
            // textEdit16
            // 
            this.textEdit16.EditValue = "Phòng Tách";
            this.textEdit16.Enabled = false;
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(540, 3);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit16.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit16.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit16.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit16.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit16.Size = new System.Drawing.Size(439, 38);
            this.textEdit16.TabIndex = 73;
            // 
            // textEdit15
            // 
            this.textEdit15.EditValue = "Phòng Gốc";
            this.textEdit15.Enabled = false;
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(10, 3);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit15.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit15.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit15.Size = new System.Drawing.Size(420, 38);
            this.textEdit15.TabIndex = 68;
            // 
            // textEdit7
            // 
            this.textEdit7.EditValue = "A1";
            this.textEdit7.Enabled = false;
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(10, 44);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit7.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit7.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit7.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit7.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit7.Size = new System.Drawing.Size(420, 34);
            this.textEdit7.TabIndex = 61;
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "VIP";
            this.textEdit9.Enabled = false;
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(10, 81);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit9.Size = new System.Drawing.Size(420, 34);
            this.textEdit9.TabIndex = 64;
            // 
            // textEdit14
            // 
            this.textEdit14.EditValue = "Huy Trung";
            this.textEdit14.Enabled = false;
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(10, 118);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit14.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit14.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.LightGray;
            this.textEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit14.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit14.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit14.Size = new System.Drawing.Size(420, 34);
            this.textEdit14.TabIndex = 67;
            // 
            // Frm_Booking_Chuyenghepphong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 527);
            this.Controls.Add(this.palbook_tachphong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Booking_Chuyenghepphong";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.palbook_tachphong)).EndInit();
            this.palbook_tachphong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palbook_DaiLy)).EndInit();
            this.palbook_DaiLy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl palbook_tachphong;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn HANGBAN;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.PanelControl palbook_DaiLy;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit16;






    }
}