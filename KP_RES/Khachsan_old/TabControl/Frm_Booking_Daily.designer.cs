﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Booking_Daily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.palbook_DaiLy = new DevExpress.XtraEditors.PanelControl();
            this.textEdit96 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit93 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit94 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit95 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit87 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit91 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit92 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit84 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit85 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit83 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit86 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit88 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit89 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit90 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.palbook_DaiLy)).BeginInit();
            this.palbook_DaiLy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit90.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // palbook_DaiLy
            // 
            this.palbook_DaiLy.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palbook_DaiLy.Controls.Add(this.textEdit96);
            this.palbook_DaiLy.Controls.Add(this.labelControl69);
            this.palbook_DaiLy.Controls.Add(this.textEdit93);
            this.palbook_DaiLy.Controls.Add(this.labelControl66);
            this.palbook_DaiLy.Controls.Add(this.textEdit94);
            this.palbook_DaiLy.Controls.Add(this.labelControl67);
            this.palbook_DaiLy.Controls.Add(this.textEdit95);
            this.palbook_DaiLy.Controls.Add(this.labelControl68);
            this.palbook_DaiLy.Controls.Add(this.textEdit87);
            this.palbook_DaiLy.Controls.Add(this.labelControl57);
            this.palbook_DaiLy.Controls.Add(this.textEdit91);
            this.palbook_DaiLy.Controls.Add(this.labelControl59);
            this.palbook_DaiLy.Controls.Add(this.textEdit92);
            this.palbook_DaiLy.Controls.Add(this.labelControl60);
            this.palbook_DaiLy.Controls.Add(this.textEdit84);
            this.palbook_DaiLy.Controls.Add(this.textEdit85);
            this.palbook_DaiLy.Controls.Add(this.textEdit83);
            this.palbook_DaiLy.Controls.Add(this.labelControl56);
            this.palbook_DaiLy.Controls.Add(this.textEdit86);
            this.palbook_DaiLy.Controls.Add(this.labelControl58);
            this.palbook_DaiLy.Controls.Add(this.textEdit88);
            this.palbook_DaiLy.Controls.Add(this.textEdit89);
            this.palbook_DaiLy.Controls.Add(this.textEdit90);
            this.palbook_DaiLy.Controls.Add(this.labelControl61);
            this.palbook_DaiLy.Controls.Add(this.labelControl62);
            this.palbook_DaiLy.Controls.Add(this.labelControl63);
            this.palbook_DaiLy.Controls.Add(this.labelControl64);
            this.palbook_DaiLy.Controls.Add(this.labelControl65);
            this.palbook_DaiLy.Location = new System.Drawing.Point(12, 29);
            this.palbook_DaiLy.Name = "palbook_DaiLy";
            this.palbook_DaiLy.Size = new System.Drawing.Size(986, 354);
            this.palbook_DaiLy.TabIndex = 22;
            // 
            // textEdit96
            // 
            this.textEdit96.EditValue = "";
            this.textEdit96.EnterMoveNextControl = true;
            this.textEdit96.Location = new System.Drawing.Point(705, 294);
            this.textEdit96.Name = "textEdit96";
            this.textEdit96.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit96.Properties.Appearance.Options.UseFont = true;
            this.textEdit96.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit96.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit96.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit96.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit96.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit96.Size = new System.Drawing.Size(232, 26);
            this.textEdit96.TabIndex = 60;
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl69.Location = new System.Drawing.Point(521, 295);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(150, 19);
            this.labelControl69.TabIndex = 59;
            this.labelControl69.Text = "Phần trăm chiết khấu";
            // 
            // textEdit93
            // 
            this.textEdit93.EditValue = "";
            this.textEdit93.EnterMoveNextControl = true;
            this.textEdit93.Location = new System.Drawing.Point(705, 249);
            this.textEdit93.Name = "textEdit93";
            this.textEdit93.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit93.Properties.Appearance.Options.UseFont = true;
            this.textEdit93.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit93.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit93.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit93.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit93.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit93.Size = new System.Drawing.Size(232, 26);
            this.textEdit93.TabIndex = 58;
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl66.Location = new System.Drawing.Point(521, 250);
            this.labelControl66.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(149, 19);
            this.labelControl66.TabIndex = 57;
            this.labelControl66.Text = "Chính sách hoa hồng";
            // 
            // textEdit94
            // 
            this.textEdit94.EditValue = "";
            this.textEdit94.EnterMoveNextControl = true;
            this.textEdit94.Location = new System.Drawing.Point(705, 204);
            this.textEdit94.Name = "textEdit94";
            this.textEdit94.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit94.Properties.Appearance.Options.UseFont = true;
            this.textEdit94.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit94.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit94.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit94.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit94.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit94.Size = new System.Drawing.Size(232, 26);
            this.textEdit94.TabIndex = 56;
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl67.Location = new System.Drawing.Point(521, 205);
            this.labelControl67.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(170, 19);
            this.labelControl67.TabIndex = 55;
            this.labelControl67.Text = "Điện thoại người liên hệ";
            // 
            // textEdit95
            // 
            this.textEdit95.EditValue = "";
            this.textEdit95.EnterMoveNextControl = true;
            this.textEdit95.Location = new System.Drawing.Point(705, 159);
            this.textEdit95.Name = "textEdit95";
            this.textEdit95.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit95.Properties.Appearance.Options.UseFont = true;
            this.textEdit95.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit95.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit95.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit95.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit95.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit95.Size = new System.Drawing.Size(232, 26);
            this.textEdit95.TabIndex = 54;
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl68.Location = new System.Drawing.Point(521, 160);
            this.labelControl68.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(95, 19);
            this.labelControl68.TabIndex = 53;
            this.labelControl68.Text = "Người liên hệ";
            // 
            // textEdit87
            // 
            this.textEdit87.EditValue = "";
            this.textEdit87.EnterMoveNextControl = true;
            this.textEdit87.Location = new System.Drawing.Point(705, 114);
            this.textEdit87.Name = "textEdit87";
            this.textEdit87.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit87.Properties.Appearance.Options.UseFont = true;
            this.textEdit87.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit87.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit87.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit87.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit87.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit87.Size = new System.Drawing.Size(232, 26);
            this.textEdit87.TabIndex = 52;
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(521, 115);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(39, 19);
            this.labelControl57.TabIndex = 51;
            this.labelControl57.Text = "Email";
            // 
            // textEdit91
            // 
            this.textEdit91.EditValue = "";
            this.textEdit91.EnterMoveNextControl = true;
            this.textEdit91.Location = new System.Drawing.Point(705, 69);
            this.textEdit91.Name = "textEdit91";
            this.textEdit91.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit91.Properties.Appearance.Options.UseFont = true;
            this.textEdit91.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit91.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit91.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit91.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit91.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit91.Size = new System.Drawing.Size(232, 26);
            this.textEdit91.TabIndex = 50;
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Location = new System.Drawing.Point(521, 70);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(24, 19);
            this.labelControl59.TabIndex = 49;
            this.labelControl59.Text = "Fax";
            // 
            // textEdit92
            // 
            this.textEdit92.EditValue = "";
            this.textEdit92.EnterMoveNextControl = true;
            this.textEdit92.Location = new System.Drawing.Point(705, 24);
            this.textEdit92.Name = "textEdit92";
            this.textEdit92.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit92.Properties.Appearance.Options.UseFont = true;
            this.textEdit92.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit92.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit92.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit92.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit92.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit92.Size = new System.Drawing.Size(232, 26);
            this.textEdit92.TabIndex = 48;
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Location = new System.Drawing.Point(521, 25);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(72, 19);
            this.labelControl60.TabIndex = 47;
            this.labelControl60.Text = "Điện thoại";
            // 
            // textEdit84
            // 
            this.textEdit84.EditValue = "";
            this.textEdit84.EnterMoveNextControl = true;
            this.textEdit84.Location = new System.Drawing.Point(154, 69);
            this.textEdit84.Name = "textEdit84";
            this.textEdit84.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit84.Properties.Appearance.Options.UseFont = true;
            this.textEdit84.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit84.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit84.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit84.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit84.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit84.Size = new System.Drawing.Size(232, 26);
            this.textEdit84.TabIndex = 46;
            // 
            // textEdit85
            // 
            this.textEdit85.EditValue = "";
            this.textEdit85.EnterMoveNextControl = true;
            this.textEdit85.Location = new System.Drawing.Point(154, 24);
            this.textEdit85.Name = "textEdit85";
            this.textEdit85.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit85.Properties.Appearance.Options.UseFont = true;
            this.textEdit85.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit85.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit85.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit85.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit85.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit85.Size = new System.Drawing.Size(232, 26);
            this.textEdit85.TabIndex = 45;
            // 
            // textEdit83
            // 
            this.textEdit83.EditValue = "";
            this.textEdit83.EnterMoveNextControl = true;
            this.textEdit83.Location = new System.Drawing.Point(154, 294);
            this.textEdit83.Name = "textEdit83";
            this.textEdit83.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit83.Properties.Appearance.Options.UseFont = true;
            this.textEdit83.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit83.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit83.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit83.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit83.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit83.Size = new System.Drawing.Size(232, 26);
            this.textEdit83.TabIndex = 44;
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Location = new System.Drawing.Point(17, 295);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(65, 19);
            this.labelControl56.TabIndex = 43;
            this.labelControl56.Text = "Quốc Gia";
            // 
            // textEdit86
            // 
            this.textEdit86.EditValue = "";
            this.textEdit86.EnterMoveNextControl = true;
            this.textEdit86.Location = new System.Drawing.Point(154, 249);
            this.textEdit86.Name = "textEdit86";
            this.textEdit86.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit86.Properties.Appearance.Options.UseFont = true;
            this.textEdit86.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit86.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit86.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit86.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit86.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit86.Size = new System.Drawing.Size(232, 26);
            this.textEdit86.TabIndex = 39;
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Location = new System.Drawing.Point(17, 250);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(125, 19);
            this.labelControl58.TabIndex = 38;
            this.labelControl58.Text = "Thành phố / Tỉnh";
            // 
            // textEdit88
            // 
            this.textEdit88.EditValue = "";
            this.textEdit88.EnterMoveNextControl = true;
            this.textEdit88.Location = new System.Drawing.Point(154, 204);
            this.textEdit88.Name = "textEdit88";
            this.textEdit88.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit88.Properties.Appearance.Options.UseFont = true;
            this.textEdit88.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit88.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit88.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit88.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit88.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit88.Size = new System.Drawing.Size(232, 26);
            this.textEdit88.TabIndex = 33;
            // 
            // textEdit89
            // 
            this.textEdit89.EditValue = "";
            this.textEdit89.EnterMoveNextControl = true;
            this.textEdit89.Location = new System.Drawing.Point(154, 159);
            this.textEdit89.Name = "textEdit89";
            this.textEdit89.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit89.Properties.Appearance.Options.UseFont = true;
            this.textEdit89.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit89.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit89.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit89.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit89.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit89.Size = new System.Drawing.Size(232, 26);
            this.textEdit89.TabIndex = 32;
            // 
            // textEdit90
            // 
            this.textEdit90.EditValue = "";
            this.textEdit90.EnterMoveNextControl = true;
            this.textEdit90.Location = new System.Drawing.Point(154, 114);
            this.textEdit90.Name = "textEdit90";
            this.textEdit90.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit90.Properties.Appearance.Options.UseFont = true;
            this.textEdit90.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit90.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit90.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit90.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit90.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit90.Size = new System.Drawing.Size(232, 26);
            this.textEdit90.TabIndex = 31;
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Location = new System.Drawing.Point(17, 25);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(63, 19);
            this.labelControl61.TabIndex = 1;
            this.labelControl61.Text = "Mã đại lý";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Location = new System.Drawing.Point(17, 70);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(70, 19);
            this.labelControl62.TabIndex = 21;
            this.labelControl62.Text = "Tên đại lý";
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Location = new System.Drawing.Point(17, 115);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(31, 19);
            this.labelControl63.TabIndex = 24;
            this.labelControl63.Text = "MST";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Location = new System.Drawing.Point(17, 160);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(48, 19);
            this.labelControl64.TabIndex = 27;
            this.labelControl64.Text = "Địa chỉ";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl65.Location = new System.Drawing.Point(17, 205);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(97, 19);
            this.labelControl65.TabIndex = 30;
            this.labelControl65.Text = "Quận / huyện";
            // 
            // Frm_Booking_Daily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.palbook_DaiLy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Booking_Daily";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.palbook_DaiLy)).EndInit();
            this.palbook_DaiLy.ResumeLayout(false);
            this.palbook_DaiLy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit90.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl palbook_DaiLy;
        private DevExpress.XtraEditors.TextEdit textEdit96;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.TextEdit textEdit93;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.TextEdit textEdit94;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.TextEdit textEdit95;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.TextEdit textEdit87;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.TextEdit textEdit91;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.TextEdit textEdit92;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.TextEdit textEdit84;
        private DevExpress.XtraEditors.TextEdit textEdit85;
        private DevExpress.XtraEditors.TextEdit textEdit83;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.TextEdit textEdit86;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.TextEdit textEdit88;
        private DevExpress.XtraEditors.TextEdit textEdit89;
        private DevExpress.XtraEditors.TextEdit textEdit90;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl65;






    }
}