﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_RES.KHACHSAN
{
    public partial class Frm_Nhomphong : Form
    {
        public string smanhom;
        public Frm_Nhomphong()
        {
            InitializeComponent();
        }
        public Frm_Nhomphong(string manhom)
        {
            InitializeComponent();
            smanhom = manhom;
        }
        private void FrmNhomphong_Load(object sender, EventArgs e)
        {
            LoadPhong(smanhom);
        }
        DataTable dtNhom = new DataTable();
        private void LoadPhong(string manhom)
        {
            string sqlban = "select MA_PHONG As MA,TEN_PHONG As TEN,HINHANH from PHONG where MA_NHOM=" + clsMain.SQLString(manhom) + "and SUDUNG=1 \n";
            sqlban += "order by TEN ASC";
            dtNhom = clsMain.ReturnDataTable(sqlban);
            palNhom.Controls.Clear();
            foreach (DataRow dr in dtNhom.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Font = palNhom.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Size = new Size(110, 80);
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Click += new System.EventHandler(btPhong_Click);
                palNhom.Controls.Add(btn);
            }
        }
        private void btPhong_Click(object sender, EventArgs e)
        {
            //đưa vào biến toàn cục
        }
        
    }
}
