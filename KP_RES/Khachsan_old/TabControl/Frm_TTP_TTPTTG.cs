﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;

namespace KP_RES.KHACHSAN
{
    public partial class Frm_TTP_TTPTTG : Form
    {
        public Frm_TTP_TTPTTG()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            //dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void FrmNhomphong_Load(object sender, EventArgs e)
        {
            Loadcombo();
            Loadgridview(DateTime.Now);
        }

        private void Loadcombo()
        {
            string sSQL = "";
            sSQL += "Select MA_NHOM As MA,TEN_NHOM As TEN" + "\n";
            sSQL += "From NHOMPHONG" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order By TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboNhom.Properties.DataSource = dt;
           // cboNhom.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());

            sSQL = "Select MA_LOAI As MA,TEN_LOAI As TEN" + "\n";
            sSQL += "From LOAIPHONG" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order By TEN" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboLoai.Properties.DataSource = dt;
           // cboLoai.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());
        }

        private void bttOpen_search_Click(object sender, EventArgs e)
        {
            if (palTop.Visible)
                palTop.Visible = false;
            else
            {
                palTop.Visible = true;
                palSearch.Left = palTop.Width / 2 - palSearch.Width / 2;
            }
        }
        private void Loadgridview(DateTime dtime)
        {
            gridControl2.ForceInitialize();

            GridColumn[] unbColumn = new GridColumn[8];
            string[] dayweek = new string[8];
            string[] tweek = new string[8];
            for (int i = 0; i < 8; i++)
            {
                dayweek[i] = string.Format("{0:dd/MM/yy}", dtime.AddDays(i));
                tweek[i] = dtime.AddDays(i).DayOfWeek.ToString();
                unbColumn[i] = gridView2.Columns.AddField(dayweek[i] + "\n" + tweek[i]);
                unbColumn[i].VisibleIndex = gridView2.Columns.Count;
                unbColumn[i].UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                // Disable editing.
                unbColumn[i].OptionsColumn.AllowEdit = false;
                unbColumn[i].OptionsColumn.AllowFocus = false;

                unbColumn[i].Width = 90;
                unbColumn[i].AppearanceCell.Font = new Font("sd", 12, FontStyle.Regular);
                unbColumn[i].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                unbColumn[i].AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                unbColumn[i].AppearanceHeader.Font = new Font("sd", 12, FontStyle.Bold);
                unbColumn[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                unbColumn[i].AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            }
            gridView2.OptionsView.ColumnAutoWidth = true;
            string sql = "SELECT MA_PHONG,TEN_PHONG,TEN_LOAI FROM PHONG P,LOAIPHONG L WHERE P.MA_LOAI=L.MA_LOAI AND P.SUDUNG=1";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl2.DataSource = dt;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void bttTien_Click_1(object sender, EventArgs e)
        {
            Loadgridview(DateTime.Now.AddDays(7));
        }

        private void bttLui_Click_1(object sender, EventArgs e)
        {
            Loadgridview(DateTime.Now);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            gridView2.Appearance.FocusedCell.BackColor = Color.Aqua;
        }
    }
}
