﻿namespace KP_RES.KHACHSAN
{
    partial class Frm_Thungan_DichvuChoThue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Thungan_DichvuChoThue));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pal_1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.pal_khuvuc = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Xuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TENKHACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit80 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btn_Tru = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).BeginInit();
            this.pal_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.pal_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            this.pal_khuvuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.panel);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1019, 477);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pal_1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(493, 477);
            this.panelControl5.TabIndex = 4;
            // 
            // pal_1
            // 
            this.pal_1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pal_1.Appearance.Options.UseBackColor = true;
            this.pal_1.Controls.Add(this.panelControl9);
            this.pal_1.Controls.Add(this.panelControl6);
            this.pal_1.Controls.Add(this.pal_sotrang);
            this.pal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_1.Location = new System.Drawing.Point(0, 0);
            this.pal_1.Name = "pal_1";
            this.pal_1.Size = new System.Drawing.Size(493, 477);
            this.pal_1.TabIndex = 1;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.pal_ban);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(2, 37);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(489, 403);
            this.panelControl9.TabIndex = 56;
            // 
            // pal_ban
            // 
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Controls.Add(this.simpleButton13);
            this.pal_ban.Controls.Add(this.simpleButton12);
            this.pal_ban.Controls.Add(this.simpleButton11);
            this.pal_ban.Controls.Add(this.simpleButton10);
            this.pal_ban.Controls.Add(this.simpleButton9);
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(485, 399);
            this.pal_ban.TabIndex = 55;
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseBackColor = true;
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton13.Location = new System.Drawing.Point(153, 6);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(122, 84);
            this.simpleButton13.TabIndex = 45;
            this.simpleButton13.Text = "Xe đạp leo núi";
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton12.Appearance.Options.UseBackColor = true;
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton12.Location = new System.Drawing.Point(153, 108);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(122, 84);
            this.simpleButton12.TabIndex = 44;
            this.simpleButton12.Text = "Xe hơi Force";
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton11.Appearance.Options.UseBackColor = true;
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton11.Location = new System.Drawing.Point(9, 108);
            this.simpleButton11.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(122, 84);
            this.simpleButton11.TabIndex = 43;
            this.simpleButton11.Text = "Xe hơi Camry";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseBackColor = true;
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton10.Location = new System.Drawing.Point(300, 6);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(122, 84);
            this.simpleButton10.TabIndex = 42;
            this.simpleButton10.Text = "Xe máy";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseBackColor = true;
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton9.Location = new System.Drawing.Point(9, 6);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(122, 84);
            this.simpleButton9.TabIndex = 41;
            this.simpleButton9.Text = "Xe đạp đôi";
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bnt_trangxuong);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 440);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(489, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(489, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.bnt_tranglen);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(489, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(0, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(489, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.pal_khuvuc);
            this.panel.Controls.Add(this.panelControl7);
            this.panel.Controls.Add(this.panelControl11);
            this.panel.Controls.Add(this.pal_len);
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel.Location = new System.Drawing.Point(493, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 477);
            this.panel.TabIndex = 2;
            // 
            // pal_khuvuc
            // 
            this.pal_khuvuc.BackColor = System.Drawing.Color.Transparent;
            this.pal_khuvuc.Controls.Add(this.simpleButton4);
            this.pal_khuvuc.Controls.Add(this.simpleButton1);
            this.pal_khuvuc.Controls.Add(this.simpleButton2);
            this.pal_khuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_khuvuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_khuvuc.Location = new System.Drawing.Point(2, 37);
            this.pal_khuvuc.Margin = new System.Windows.Forms.Padding(0);
            this.pal_khuvuc.Name = "pal_khuvuc";
            this.pal_khuvuc.Size = new System.Drawing.Size(131, 403);
            this.pal_khuvuc.TabIndex = 29;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(4, 4);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(122, 58);
            this.simpleButton4.TabIndex = 34;
            this.simpleButton4.Text = "Xe";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(4, 70);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(122, 58);
            this.simpleButton1.TabIndex = 36;
            this.simpleButton1.Text = "Đồ tắm";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(4, 136);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(122, 58);
            this.simpleButton2.TabIndex = 37;
            this.simpleButton2.Text = "Hướng Dẫn Viên";
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(915, 291);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(77, 168);
            this.panelControl7.TabIndex = 1;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.bnt_Xuong);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 440);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(131, 35);
            this.panelControl11.TabIndex = 28;
            // 
            // bnt_Xuong
            // 
            this.bnt_Xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Xuong.Appearance.Options.UseBackColor = true;
            this.bnt_Xuong.Appearance.Options.UseFont = true;
            this.bnt_Xuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bnt_Xuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Xuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_Xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Xuong.Name = "bnt_Xuong";
            this.bnt_Xuong.Size = new System.Drawing.Size(131, 35);
            this.bnt_Xuong.TabIndex = 2;
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.bnt_Len);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(0, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(131, 35);
            this.bnt_Len.TabIndex = 26;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.panelControl2);
            this.panelControl4.Controls.Add(this.panelControl3);
            this.panelControl4.Controls.Add(this.panelControl14);
            this.panelControl4.Controls.Add(this.panelControl13);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(628, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(391, 477);
            this.panelControl4.TabIndex = 3;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.gridControl3);
            this.panelControl2.Controls.Add(this.simpleButton5);
            this.panelControl2.Controls.Add(this.panelControl8);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(387, 350);
            this.panelControl2.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.AllowDrop = true;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(360, 350);
            this.gridControl3.TabIndex = 8;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TENKHACH,
            this.CMT,
            this.NGAYCMT,
            this.VISA});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // TENKHACH
            // 
            this.TENKHACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TENKHACH.AppearanceCell.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TENKHACH.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.TENKHACH.AppearanceHeader.Options.UseFont = true;
            this.TENKHACH.AppearanceHeader.Options.UseForeColor = true;
            this.TENKHACH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKHACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKHACH.Caption = "Mặt Hàng";
            this.TENKHACH.FieldName = "TENKHACH";
            this.TENKHACH.Name = "TENKHACH";
            this.TENKHACH.OptionsColumn.AllowEdit = false;
            this.TENKHACH.OptionsColumn.AllowFocus = false;
            this.TENKHACH.OptionsColumn.FixedWidth = true;
            this.TENKHACH.Visible = true;
            this.TENKHACH.VisibleIndex = 0;
            this.TENKHACH.Width = 125;
            // 
            // CMT
            // 
            this.CMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.CMT.AppearanceCell.Options.UseFont = true;
            this.CMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.CMT.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.CMT.AppearanceHeader.Options.UseFont = true;
            this.CMT.AppearanceHeader.Options.UseForeColor = true;
            this.CMT.AppearanceHeader.Options.UseTextOptions = true;
            this.CMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMT.Caption = "SL";
            this.CMT.FieldName = "CMT";
            this.CMT.Name = "CMT";
            this.CMT.OptionsColumn.AllowEdit = false;
            this.CMT.OptionsColumn.AllowFocus = false;
            this.CMT.OptionsColumn.FixedWidth = true;
            this.CMT.Visible = true;
            this.CMT.VisibleIndex = 1;
            this.CMT.Width = 47;
            // 
            // NGAYCMT
            // 
            this.NGAYCMT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.NGAYCMT.AppearanceCell.Options.UseFont = true;
            this.NGAYCMT.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.NGAYCMT.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.NGAYCMT.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMT.AppearanceHeader.Options.UseForeColor = true;
            this.NGAYCMT.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMT.Caption = "Đơn Giá";
            this.NGAYCMT.FieldName = "NGAYCMT";
            this.NGAYCMT.Name = "NGAYCMT";
            this.NGAYCMT.OptionsColumn.AllowEdit = false;
            this.NGAYCMT.OptionsColumn.AllowFocus = false;
            this.NGAYCMT.OptionsColumn.FixedWidth = true;
            this.NGAYCMT.Visible = true;
            this.NGAYCMT.VisibleIndex = 2;
            this.NGAYCMT.Width = 79;
            // 
            // VISA
            // 
            this.VISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.VISA.AppearanceCell.Options.UseFont = true;
            this.VISA.AppearanceCell.Options.UseTextOptions = true;
            this.VISA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.VISA.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.VISA.AppearanceHeader.Options.UseFont = true;
            this.VISA.AppearanceHeader.Options.UseForeColor = true;
            this.VISA.AppearanceHeader.Options.UseTextOptions = true;
            this.VISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VISA.Caption = "Thành Tiền";
            this.VISA.FieldName = "VISA";
            this.VISA.Name = "VISA";
            this.VISA.OptionsColumn.AllowEdit = false;
            this.VISA.OptionsColumn.AllowFocus = false;
            this.VISA.OptionsColumn.AllowMove = false;
            this.VISA.Visible = true;
            this.VISA.VisibleIndex = 3;
            this.VISA.Width = 95;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton5.Location = new System.Drawing.Point(-27, 355);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(122, 58);
            this.simpleButton5.TabIndex = 35;
            this.simpleButton5.Text = "Phòng Đôi";
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl10);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl8.Location = new System.Drawing.Point(360, 0);
            this.panelControl8.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(27, 350);
            this.panelControl8.TabIndex = 3;
            // 
            // panelControl10
            // 
            this.panelControl10.AutoSize = true;
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.panelControl12);
            this.panelControl10.Controls.Add(this.btnXuongit);
            this.panelControl10.Controls.Add(this.btnXuongnhieu);
            this.panelControl10.Controls.Add(this.btnLenit);
            this.panelControl10.Controls.Add(this.btnLennhieu);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(23, 346);
            this.panelControl10.TabIndex = 14;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(0, 160);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(23, 26);
            this.panelControl12.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            //this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 186);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(23, 80);
            this.btnXuongit.TabIndex = 12;
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            //this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 266);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(23, 80);
            this.btnXuongnhieu.TabIndex = 1;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            //this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(23, 80);
            this.btnLenit.TabIndex = 13;
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
           // this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(23, 80);
            this.btnLennhieu.TabIndex = 11;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.textEdit80);
            this.panelControl3.Controls.Add(this.labelControl53);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 352);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(387, 39);
            this.panelControl3.TabIndex = 1;
            // 
            // textEdit80
            // 
            this.textEdit80.EditValue = "";
            this.textEdit80.Enabled = false;
            this.textEdit80.EnterMoveNextControl = true;
            this.textEdit80.Location = new System.Drawing.Point(215, 7);
            this.textEdit80.Name = "textEdit80";
            this.textEdit80.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.Appearance.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit80.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEdit80.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit80.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit80.Size = new System.Drawing.Size(146, 26);
            this.textEdit80.TabIndex = 35;
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(129, 10);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(79, 19);
            this.labelControl53.TabIndex = 21;
            this.labelControl53.Text = "Tổng Cộng";
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.btn_Tru);
            this.panelControl14.Controls.Add(this.btnDeleteAll);
            this.panelControl14.Controls.Add(this.btn_Cong);
            this.panelControl14.Controls.Add(this.btn_xoa);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl14.Location = new System.Drawing.Point(2, 391);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(387, 42);
            this.panelControl14.TabIndex = 3;
            // 
            // btn_Tru
            // 
            this.btn_Tru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Tru.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Tru.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Tru.Appearance.Options.UseFont = true;
            this.btn_Tru.Appearance.Options.UseForeColor = true;
            this.btn_Tru.Location = new System.Drawing.Point(317, 4);
            this.btn_Tru.Name = "btn_Tru";
            this.btn_Tru.Size = new System.Drawing.Size(65, 34);
            this.btn_Tru.TabIndex = 11;
            this.btn_Tru.Text = "-";
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteAll.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnDeleteAll.Appearance.Options.UseFont = true;
            this.btnDeleteAll.Appearance.Options.UseForeColor = true;
            this.btnDeleteAll.Location = new System.Drawing.Point(5, 4);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(101, 34);
            this.btnDeleteAll.TabIndex = 13;
            this.btnDeleteAll.Text = "Xóa tất cả";
            // 
            // btn_Cong
            // 
            this.btn_Cong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cong.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cong.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Cong.Appearance.Options.UseFont = true;
            this.btn_Cong.Appearance.Options.UseForeColor = true;
            this.btn_Cong.Location = new System.Drawing.Point(246, 4);
            this.btn_Cong.Name = "btn_Cong";
            this.btn_Cong.Size = new System.Drawing.Size(65, 34);
            this.btn_Cong.TabIndex = 10;
            this.btn_Cong.Text = "+";
            // 
            // btn_xoa
            // 
            this.btn_xoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_xoa.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Appearance.Options.UseForeColor = true;
            this.btn_xoa.Location = new System.Drawing.Point(164, 4);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(76, 34);
            this.btn_xoa.TabIndex = 12;
            this.btn_xoa.Text = "Xóa";
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.simpleButton6);
            this.panelControl13.Controls.Add(this.simpleButton8);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl13.Location = new System.Drawing.Point(2, 433);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(387, 42);
            this.panelControl13.TabIndex = 2;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton6.Location = new System.Drawing.Point(131, 2);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(254, 38);
            this.simpleButton6.TabIndex = 38;
            this.simpleButton6.Text = "Ghi Đơn Vào Phòng";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = System.Drawing.Color.Silver;
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Dock = System.Windows.Forms.DockStyle.Left;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton8.Location = new System.Drawing.Point(2, 2);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(129, 38);
            this.simpleButton8.TabIndex = 39;
            this.simpleButton8.Text = "In vé";
            // 
            // Frm_Thungan_DichvuChoThue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 477);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Thungan_DichvuChoThue";
            this.Text = "FrmNhomphong";
            this.Load += new System.EventHandler(this.FrmNhomphong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).EndInit();
            this.pal_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.pal_ban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            this.pal_khuvuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pal_1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.PanelControl panel;
        private System.Windows.Forms.FlowLayoutPanel pal_khuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton bnt_Xuong;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn TENKHACH;
        private DevExpress.XtraGrid.Columns.GridColumn CMT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMT;
        private DevExpress.XtraGrid.Columns.GridColumn VISA;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btn_Tru;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAll;
        private DevExpress.XtraEditors.SimpleButton btn_Cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit textEdit80;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;







    }
}