﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using DevExpress.XtraGrid.Columns;

namespace KP_RES.KHACHSAN
{
    public partial class Frm_TTP_KNCUP : Form
    {
        public Frm_TTP_KNCUP()
        {
            InitializeComponent();
        }

        private void FrmNhomphong_Load(object sender, EventArgs e)
        {
            Loadgridview();
        }
        private void Loadgridview()
        {
            gridControl1.ForceInitialize();

            GridColumn[] unbColumn = new GridColumn[8];
            string[] dayweek = new string[8];
            string[] tweek = new string[8];
            for (int i = 0; i < 8; i++)
            {
                dayweek[i] = string.Format("{0:dd/MM/yy}", DateTime.Now.AddDays(i));
                tweek[i] = DateTime.Now.AddDays(i).DayOfWeek.ToString();
                unbColumn[i] = gridView1.Columns.AddField(dayweek[i] + "\n" + tweek[i]);
                unbColumn[i].VisibleIndex = gridView1.Columns.Count;
                unbColumn[i].UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                // Disable editing.
                unbColumn[i].OptionsColumn.AllowEdit = false;
                unbColumn[i].OptionsColumn.AllowFocus = false;

                unbColumn[i].Width = 90;
                unbColumn[i].Summary.Add(DevExpress.Data.SummaryItemType.Sum);
                unbColumn[i].AppearanceCell.Font = new Font("sd", 12, FontStyle.Regular);
                unbColumn[i].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                unbColumn[i].AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                unbColumn[i].AppearanceHeader.Font = new Font("sd", 12, FontStyle.Bold);
                unbColumn[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                unbColumn[i].AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            }
            gridView1.OptionsView.ColumnAutoWidth = true;
            SoPhong.Caption = "Tổng Số \n" + "Phòng";
            string sql = "select L.MA_LOAI,L.TEN_LOAI,count(MA_PHONG) as SoPhong from PHONG P ";
                   sql+= "RIGHT JOIN LOAIPHONG L ON P.MA_LOAI=L.MA_LOAI group by L.MA_LOAI,L.TEN_LOAI";
            DataTable dt = clsMain.ReturnDataTable(sql);
            //int n=dt.Rows.Count;
            //dt.Rows.Add();
            //int tongphong = 0;
            ////int[] arrcol = new int[8];
            ////for (int j = 0; j < 8; j++)
            ////{
            ////    arrcol[j] = 0;
            //for (int i = 0; i < n; i++)
            //{
            //    tongphong += int.Parse(dt.Rows[i]["SoPhong"].ToString());
            //    //arrcol[j] += int.Parse(dt.Rows[i][unbColumn[j].FieldName].ToString());
            //}
            ////dt.Rows[n][unbColumn[j].FieldName] = arrcol[j];
            ////}
            //dt.Rows[n]["SoPhong"] = tongphong;
            //dt.Rows[n]["TEN_LOAI"] = "Tổng Số";

            gridControl1.DataSource = dt;
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit_1.Enabled = true;
                btnXuongnhieu_1.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit_1.Enabled = false;
                    btnLennhieu_1.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit_1.Enabled = false;
                btnLennhieu_1.Enabled = false;
                btnXuongit_1.Enabled = true;
                btnXuongnhieu_1.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit_1.Enabled = true;
                btnLennhieu_1.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit_1.Enabled = false;
                    btnXuongnhieu_1.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit_1.Enabled = true;
                btnLennhieu_1.Enabled = true;
                btnXuongit_1.Enabled = false;
                btnXuongnhieu_1.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }
    }
}
