﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraTab;

namespace KP_RES 
{
    public partial class Frm_PhongKS : DevExpress.XtraEditors.XtraForm
    {
        public Frm_PhongKS()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
            dtp_ngayden.EditValue = clsGlobal.gdServerDate;
            dtp_ngaydi.EditValue = clsGlobal.gdServerDate.AddDays(1);
        }

        private void Frm_PhongKS_Load(object sender, EventArgs e)
        {
            palphong.Left = palTop.Width / 2 - palphong.Width / 2;
            palbttNhomphong.Width = tabNhomphong.Width - 20;
            palbttNhomphong.Left = 10;
            palBttBooking.Left = tabBooking.Width / 2 - palBttBooking.Width / 2;
            pal_Book_LSluutru_Search.Left = palBook_LSluutru_Top.Width / 2 - pal_Book_LSluutru_Search.Width / 2;
            palBookDangky.Left = TabsubBooking_Dangky.Width / 2 - palBookDangky.Width / 2;
            palbtt_Thungan.Left = TabThungan.Width / 2 - palbtt_Thungan.Width / 2;                   //canh giữa panel button tab Thu ngân
            palbtt_Nghiepvu.Left = tabNghiepvu.Width / 2 - palbtt_Nghiepvu.Width / 2;                //canh giữa panel button tab Nghiệp vụ

            cbbKhuvuc.Text = "Tầng trệt";
            cbbNhom.Text = "Phòng thường";
        }

        public bool addpageinTabpage(string name)
        {
            foreach (XtraTabPage tb in this.Tabcontrol.TabPages)
            {
                if (tb.Name == name)
                {
                    Tabcontrol.SelectedTabPage = tb;
                    return true;
                }
            }
            return false;
        }
        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void bttTTPTTG_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = tabsub_TTP_theotg;
        }

        private void bttKNCUP_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_TTP_khanangcungungphong;
        }

        private void bttTKP_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_TTP_thongkephong;
        }

        private void bttSDPHT_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_TTP_sodophonghientai;
        }

        private void bttBook_DK_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = TabsubBooking_Dangky;
        }

        private void bttNhom_PDon_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_nhomphong;
        }

        private void bttBook_DS_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_Booking_DSBooking;
        }

        private void bttBook_LS_luutru_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_Booking_LSLuutru;
        }

        private void btt_Book_DaiLy_Click(object sender, EventArgs e)
        {
            this.Tabcontrol.SelectedTabPage = Tabsub_Booking_Daily;
        }

        private void bttThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       

    }
}