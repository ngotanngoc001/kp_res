﻿namespace KP_RES
{
    partial class PF_Thanhtoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PF_Thanhtoan));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlBanve = new DevExpress.XtraEditors.PanelControl();
            this.panelControl58 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl57 = new DevExpress.XtraEditors.PanelControl();
            this.grdTiente = new DevExpress.XtraGrid.GridControl();
            this.grvtiente = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_TIENTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_TIENTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl59 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongtiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnLentiente = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdPhuthu = new DevExpress.XtraGrid.GridControl();
            this.grvPhuthu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongphuthu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenphuthu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl62 = new DevExpress.XtraEditors.PanelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnInve1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhaplaitienkhachtra = new DevExpress.XtraEditors.SimpleButton();
            this.btnTTTNB = new DevExpress.XtraEditors.SimpleButton();
            this.btnInve = new DevExpress.XtraEditors.SimpleButton();
            this.btnKHTT = new DevExpress.XtraEditors.SimpleButton();
            this.pnlMakhachhang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl60 = new DevExpress.XtraEditors.PanelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.txtMakhachhang = new DevExpress.XtraEditors.TextEdit();
            this.panelControl61 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn5t = new DevExpress.XtraEditors.SimpleButton();
            this.btn1k = new DevExpress.XtraEditors.SimpleButton();
            this.btn2k = new DevExpress.XtraEditors.SimpleButton();
            this.btn5k = new DevExpress.XtraEditors.SimpleButton();
            this.btn10k = new DevExpress.XtraEditors.SimpleButton();
            this.btn20k = new DevExpress.XtraEditors.SimpleButton();
            this.btn50k = new DevExpress.XtraEditors.SimpleButton();
            this.btn100k = new DevExpress.XtraEditors.SimpleButton();
            this.btn200k = new DevExpress.XtraEditors.SimpleButton();
            this.btn500k = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienThoiLai = new DevExpress.XtraEditors.TextEdit();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pnlQuydoi = new DevExpress.XtraEditors.PanelControl();
            this.panelControl63 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienquydoi = new DevExpress.XtraEditors.TextEdit();
            this.panelControl64 = new DevExpress.XtraEditors.PanelControl();
            this.lblTienquydoi = new DevExpress.XtraEditors.LabelControl();
            this.pnlTienkhachtra = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienKhachTra = new DevExpress.XtraEditors.TextEdit();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienve = new DevExpress.XtraEditors.TextEdit();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnBanve1 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBanve)).BeginInit();
            this.pnlBanve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).BeginInit();
            this.panelControl58.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).BeginInit();
            this.panelControl57.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvtiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).BeginInit();
            this.panelControl59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPhuthu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPhuthu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).BeginInit();
            this.panelControl62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMakhachhang)).BeginInit();
            this.pnlMakhachhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).BeginInit();
            this.panelControl60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMakhachhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).BeginInit();
            this.panelControl61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThoiLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuydoi)).BeginInit();
            this.pnlQuydoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).BeginInit();
            this.panelControl63.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienquydoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).BeginInit();
            this.panelControl64.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTienkhachtra)).BeginInit();
            this.pnlTienkhachtra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienKhachTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).BeginInit();
            this.btnBanve1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl2.Controls.Add(this.panel1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 43);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1026, 635);
            this.panelControl2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pnlBanve);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1022, 631);
            this.panel1.TabIndex = 0;
            // 
            // pnlBanve
            // 
            this.pnlBanve.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBanve.Controls.Add(this.panelControl58);
            this.pnlBanve.Controls.Add(this.pnlMakhachhang);
            this.pnlBanve.Controls.Add(this.panelControl15);
            this.pnlBanve.Controls.Add(this.panelControl16);
            this.pnlBanve.Controls.Add(this.pnlQuydoi);
            this.pnlBanve.Controls.Add(this.pnlTienkhachtra);
            this.pnlBanve.Controls.Add(this.panelControl8);
            this.pnlBanve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBanve.Location = new System.Drawing.Point(0, 0);
            this.pnlBanve.Name = "pnlBanve";
            this.pnlBanve.Size = new System.Drawing.Size(1022, 631);
            this.pnlBanve.TabIndex = 5;
            // 
            // panelControl58
            // 
            this.panelControl58.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl58.Controls.Add(this.panelControl57);
            this.panelControl58.Controls.Add(this.panelControl1);
            this.panelControl58.Controls.Add(this.panelControl62);
            this.panelControl58.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl58.Location = new System.Drawing.Point(0, 383);
            this.panelControl58.Name = "panelControl58";
            this.panelControl58.Size = new System.Drawing.Size(1022, 248);
            this.panelControl58.TabIndex = 44;
            // 
            // panelControl57
            // 
            this.panelControl57.Controls.Add(this.grdTiente);
            this.panelControl57.Controls.Add(this.panelControl59);
            this.panelControl57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl57.Location = new System.Drawing.Point(0, 0);
            this.panelControl57.Name = "panelControl57";
            this.panelControl57.Size = new System.Drawing.Size(332, 248);
            this.panelControl57.TabIndex = 45;
            // 
            // grdTiente
            // 
            this.grdTiente.AllowDrop = true;
            this.grdTiente.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grdTiente.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdTiente.Location = new System.Drawing.Point(2, 2);
            this.grdTiente.MainView = this.grvtiente;
            this.grdTiente.Name = "grdTiente";
            this.grdTiente.Size = new System.Drawing.Size(293, 244);
            this.grdTiente.TabIndex = 43;
            this.grdTiente.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvtiente});
            // 
            // grvtiente
            // 
            this.grvtiente.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvtiente.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
            this.grvtiente.Appearance.FocusedRow.Options.UseFont = true;
            this.grvtiente.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grvtiente.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvtiente.Appearance.FooterPanel.Options.UseFont = true;
            this.grvtiente.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvtiente.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvtiente.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvtiente.Appearance.GroupFooter.Options.UseFont = true;
            this.grvtiente.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvtiente.Appearance.Row.Options.UseFont = true;
            this.grvtiente.ColumnPanelRowHeight = 30;
            this.grvtiente.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTTT,
            this.MA_TIENTE,
            this.TEN_TIENTE,
            this.TYGIA});
            this.grvtiente.FooterPanelHeight = 30;
            this.grvtiente.GridControl = this.grdTiente;
            this.grvtiente.Name = "grvtiente";
            this.grvtiente.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvtiente.OptionsView.EnableAppearanceEvenRow = true;
            this.grvtiente.OptionsView.ShowGroupPanel = false;
            this.grvtiente.OptionsView.ShowIndicator = false;
            this.grvtiente.RowHeight = 50;
            this.grvtiente.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.grvtiente.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvtiente.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.grvtiente.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvtiente_RowCellClick);
            this.grvtiente.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvtiente_CustomDrawCell);
            // 
            // STTTT
            // 
            this.STTTT.AppearanceCell.Options.UseTextOptions = true;
            this.STTTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STTTT.AppearanceHeader.Options.UseFont = true;
            this.STTTT.AppearanceHeader.Options.UseTextOptions = true;
            this.STTTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTTT.Caption = "STT";
            this.STTTT.Name = "STTTT";
            this.STTTT.OptionsColumn.AllowEdit = false;
            this.STTTT.OptionsColumn.AllowFocus = false;
            this.STTTT.OptionsColumn.AllowMove = false;
            this.STTTT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STTTT.Visible = true;
            this.STTTT.VisibleIndex = 0;
            this.STTTT.Width = 43;
            // 
            // MA_TIENTE
            // 
            this.MA_TIENTE.AppearanceCell.Options.UseTextOptions = true;
            this.MA_TIENTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MA_TIENTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MA_TIENTE.AppearanceHeader.Options.UseFont = true;
            this.MA_TIENTE.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_TIENTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_TIENTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_TIENTE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_TIENTE.Caption = "Mã tiền tệ";
            this.MA_TIENTE.FieldName = "MA_TIENTE";
            this.MA_TIENTE.Name = "MA_TIENTE";
            this.MA_TIENTE.OptionsColumn.AllowEdit = false;
            this.MA_TIENTE.OptionsColumn.AllowFocus = false;
            this.MA_TIENTE.OptionsColumn.AllowMove = false;
            this.MA_TIENTE.Visible = true;
            this.MA_TIENTE.VisibleIndex = 1;
            this.MA_TIENTE.Width = 100;
            // 
            // TEN_TIENTE
            // 
            this.TEN_TIENTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_TIENTE.AppearanceHeader.Options.UseFont = true;
            this.TEN_TIENTE.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_TIENTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_TIENTE.Caption = "Tên tiền tệ";
            this.TEN_TIENTE.FieldName = "TEN_TIENTE";
            this.TEN_TIENTE.Name = "TEN_TIENTE";
            this.TEN_TIENTE.OptionsColumn.AllowEdit = false;
            this.TEN_TIENTE.OptionsColumn.AllowFocus = false;
            this.TEN_TIENTE.Visible = true;
            this.TEN_TIENTE.VisibleIndex = 2;
            this.TEN_TIENTE.Width = 180;
            // 
            // TYGIA
            // 
            this.TYGIA.AppearanceCell.Options.UseTextOptions = true;
            this.TYGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TYGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TYGIA.AppearanceHeader.Options.UseFont = true;
            this.TYGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.TYGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TYGIA.Caption = "Tỷ giá";
            this.TYGIA.DisplayFormat.FormatString = "N0";
            this.TYGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TYGIA.FieldName = "TYGIA";
            this.TYGIA.Name = "TYGIA";
            this.TYGIA.OptionsColumn.AllowEdit = false;
            this.TYGIA.OptionsColumn.AllowFocus = false;
            this.TYGIA.OptionsColumn.AllowMove = false;
            this.TYGIA.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TYGIA.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DONGIA", "{0:#,###0}")});
            this.TYGIA.Visible = true;
            this.TYGIA.VisibleIndex = 3;
            this.TYGIA.Width = 100;
            // 
            // panelControl59
            // 
            this.panelControl59.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl59.Controls.Add(this.btnXuongtiente);
            this.panelControl59.Controls.Add(this.btnLentiente);
            this.panelControl59.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl59.Location = new System.Drawing.Point(295, 2);
            this.panelControl59.Name = "panelControl59";
            this.panelControl59.Size = new System.Drawing.Size(35, 244);
            this.panelControl59.TabIndex = 42;
            // 
            // btnXuongtiente
            // 
            this.btnXuongtiente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuongtiente.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongtiente.Image")));
            this.btnXuongtiente.Location = new System.Drawing.Point(2, 177);
            this.btnXuongtiente.Name = "btnXuongtiente";
            this.btnXuongtiente.Size = new System.Drawing.Size(32, 67);
            this.btnXuongtiente.TabIndex = 6;
            this.btnXuongtiente.Click += new System.EventHandler(this.btnXuongtiente_Click);
            // 
            // btnLentiente
            // 
            this.btnLentiente.Image = ((System.Drawing.Image)(resources.GetObject("btnLentiente.Image")));
            this.btnLentiente.Location = new System.Drawing.Point(2, 1);
            this.btnLentiente.Name = "btnLentiente";
            this.btnLentiente.Size = new System.Drawing.Size(31, 67);
            this.btnLentiente.TabIndex = 5;
            this.btnLentiente.Text = "simpleButton7";
            this.btnLentiente.Click += new System.EventHandler(this.btnLentiente_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.grdPhuthu);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(332, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(341, 248);
            this.panelControl1.TabIndex = 46;
            // 
            // grdPhuthu
            // 
            this.grdPhuthu.AllowDrop = true;
            this.grdPhuthu.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.grdPhuthu.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grdPhuthu.Location = new System.Drawing.Point(2, 2);
            this.grdPhuthu.MainView = this.grvPhuthu;
            this.grdPhuthu.Name = "grdPhuthu";
            this.grdPhuthu.Size = new System.Drawing.Size(302, 244);
            this.grdPhuthu.TabIndex = 43;
            this.grdPhuthu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvPhuthu});
            // 
            // grvPhuthu
            // 
            this.grvPhuthu.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvPhuthu.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
            this.grvPhuthu.Appearance.FocusedRow.Options.UseFont = true;
            this.grvPhuthu.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grvPhuthu.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvPhuthu.Appearance.FooterPanel.Options.UseFont = true;
            this.grvPhuthu.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvPhuthu.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvPhuthu.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvPhuthu.Appearance.GroupFooter.Options.UseFont = true;
            this.grvPhuthu.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvPhuthu.Appearance.Row.Options.UseFont = true;
            this.grvPhuthu.ColumnPanelRowHeight = 30;
            this.grvPhuthu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT12,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.grvPhuthu.FooterPanelHeight = 30;
            this.grvPhuthu.GridControl = this.grdPhuthu;
            this.grvPhuthu.Name = "grvPhuthu";
            this.grvPhuthu.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvPhuthu.OptionsView.EnableAppearanceEvenRow = true;
            this.grvPhuthu.OptionsView.ShowGroupPanel = false;
            this.grvPhuthu.OptionsView.ShowIndicator = false;
            this.grvPhuthu.RowHeight = 50;
            this.grvPhuthu.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.grvPhuthu.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvPhuthu.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.grvPhuthu.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvPhuthu_RowCellClick);
            this.grvPhuthu.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvPhuthu_CustomDrawCell);
            // 
            // STT12
            // 
            this.STT12.AppearanceCell.Options.UseTextOptions = true;
            this.STT12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT12.AppearanceHeader.Options.UseFont = true;
            this.STT12.AppearanceHeader.Options.UseTextOptions = true;
            this.STT12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT12.Caption = "STT";
            this.STT12.Name = "STT12";
            this.STT12.OptionsColumn.AllowEdit = false;
            this.STT12.OptionsColumn.AllowFocus = false;
            this.STT12.OptionsColumn.AllowMove = false;
            this.STT12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT12.Visible = true;
            this.STT12.VisibleIndex = 0;
            this.STT12.Width = 43;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Mã tiền tệ";
            this.gridColumn5.FieldName = "MA_TIENTE";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Phụ phí";
            this.gridColumn6.FieldName = "TENPHUPHI";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 180;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Giá";
            this.gridColumn7.DisplayFormat.FormatString = "N0";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "GIATRI";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DONGIA", "{0:#,###0}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 100;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongphuthu);
            this.panelControl3.Controls.Add(this.btnLenphuthu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(304, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 244);
            this.panelControl3.TabIndex = 42;
            // 
            // btnXuongphuthu
            // 
            this.btnXuongphuthu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuongphuthu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongphuthu.Image")));
            this.btnXuongphuthu.Location = new System.Drawing.Point(2, 177);
            this.btnXuongphuthu.Name = "btnXuongphuthu";
            this.btnXuongphuthu.Size = new System.Drawing.Size(32, 67);
            this.btnXuongphuthu.TabIndex = 6;
            this.btnXuongphuthu.Click += new System.EventHandler(this.btnXuongphuthu_Click);
            // 
            // btnLenphuthu
            // 
            this.btnLenphuthu.Image = ((System.Drawing.Image)(resources.GetObject("btnLenphuthu.Image")));
            this.btnLenphuthu.Location = new System.Drawing.Point(2, 1);
            this.btnLenphuthu.Name = "btnLenphuthu";
            this.btnLenphuthu.Size = new System.Drawing.Size(31, 67);
            this.btnLenphuthu.TabIndex = 5;
            this.btnLenphuthu.Text = "simpleButton7";
            this.btnLenphuthu.Click += new System.EventHandler(this.btnLenphuthu_Click);
            // 
            // panelControl62
            // 
            this.panelControl62.Controls.Add(this.btnBanphim);
            this.panelControl62.Controls.Add(this.btnDong);
            this.panelControl62.Controls.Add(this.btnInve1);
            this.panelControl62.Controls.Add(this.btnNhaplaitienkhachtra);
            this.panelControl62.Controls.Add(this.btnTTTNB);
            this.panelControl62.Controls.Add(this.btnInve);
            this.panelControl62.Controls.Add(this.btnKHTT);
            this.panelControl62.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl62.Location = new System.Drawing.Point(673, 0);
            this.panelControl62.Name = "panelControl62";
            this.panelControl62.Size = new System.Drawing.Size(349, 248);
            this.panelControl62.TabIndex = 44;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseTextOptions = true;
            this.btnBanphim.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBanphim.Location = new System.Drawing.Point(235, 86);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(110, 79);
            this.btnBanphim.TabIndex = 41;
            this.btnBanphim.Text = "Bàn phím";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Appearance.Options.UseTextOptions = true;
            this.btnDong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDong.Location = new System.Drawing.Point(235, 170);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 78);
            this.btnDong.TabIndex = 40;
            this.btnDong.Text = "Hủy thanh toán";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnInve1
            // 
            this.btnInve1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInve1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInve1.Appearance.Options.UseFont = true;
            this.btnInve1.Appearance.Options.UseTextOptions = true;
            this.btnInve1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInve1.Location = new System.Drawing.Point(119, 2);
            this.btnInve1.Name = "btnInve1";
            this.btnInve1.Size = new System.Drawing.Size(110, 162);
            this.btnInve1.TabIndex = 39;
            this.btnInve1.Tag = "0";
            this.btnInve1.Text = "Thanh toán\r\n(Không in vé)";
            this.btnInve1.Click += new System.EventHandler(this.btnInve_Click);
            // 
            // btnNhaplaitienkhachtra
            // 
            this.btnNhaplaitienkhachtra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNhaplaitienkhachtra.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnNhaplaitienkhachtra.Appearance.Options.UseFont = true;
            this.btnNhaplaitienkhachtra.Appearance.Options.UseTextOptions = true;
            this.btnNhaplaitienkhachtra.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnNhaplaitienkhachtra.Location = new System.Drawing.Point(235, 2);
            this.btnNhaplaitienkhachtra.Name = "btnNhaplaitienkhachtra";
            this.btnNhaplaitienkhachtra.Size = new System.Drawing.Size(110, 79);
            this.btnNhaplaitienkhachtra.TabIndex = 36;
            this.btnNhaplaitienkhachtra.Text = "Nhập lại tiền khách trả";
            this.btnNhaplaitienkhachtra.Click += new System.EventHandler(this.btnNhaplaitienkhachtra_Click);
            // 
            // btnTTTNB
            // 
            this.btnTTTNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTTTNB.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTTTNB.Appearance.Options.UseFont = true;
            this.btnTTTNB.Appearance.Options.UseTextOptions = true;
            this.btnTTTNB.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTTTNB.Location = new System.Drawing.Point(119, 170);
            this.btnTTTNB.Name = "btnTTTNB";
            this.btnTTTNB.Size = new System.Drawing.Size(110, 78);
            this.btnTTTNB.TabIndex = 38;
            this.btnTTTNB.Text = "Thẻ thanh toán nội bộ";
            this.btnTTTNB.Click += new System.EventHandler(this.btnTTTNB_Click);
            // 
            // btnInve
            // 
            this.btnInve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInve.Appearance.Options.UseFont = true;
            this.btnInve.Appearance.Options.UseTextOptions = true;
            this.btnInve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInve.Location = new System.Drawing.Point(3, 2);
            this.btnInve.Name = "btnInve";
            this.btnInve.Size = new System.Drawing.Size(110, 162);
            this.btnInve.TabIndex = 11;
            this.btnInve.Tag = "1";
            this.btnInve.Text = "Thanh toán \r\n  (In vé)";
            this.btnInve.Click += new System.EventHandler(this.btnInve_Click);
            // 
            // btnKHTT
            // 
            this.btnKHTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKHTT.Appearance.Options.UseFont = true;
            this.btnKHTT.Appearance.Options.UseTextOptions = true;
            this.btnKHTT.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKHTT.Location = new System.Drawing.Point(3, 170);
            this.btnKHTT.Name = "btnKHTT";
            this.btnKHTT.Size = new System.Drawing.Size(110, 78);
            this.btnKHTT.TabIndex = 37;
            this.btnKHTT.Text = "Khách hàng thân thiết";
            this.btnKHTT.Click += new System.EventHandler(this.btnKHTT_Click);
            // 
            // pnlMakhachhang
            // 
            this.pnlMakhachhang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMakhachhang.Controls.Add(this.panelControl60);
            this.pnlMakhachhang.Controls.Add(this.panelControl61);
            this.pnlMakhachhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMakhachhang.Location = new System.Drawing.Point(0, 348);
            this.pnlMakhachhang.Name = "pnlMakhachhang";
            this.pnlMakhachhang.Size = new System.Drawing.Size(1022, 119);
            this.pnlMakhachhang.TabIndex = 42;
            this.pnlMakhachhang.Visible = false;
            // 
            // panelControl60
            // 
            this.panelControl60.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl60.Controls.Add(this.btnXoa);
            this.panelControl60.Controls.Add(this.btn5);
            this.panelControl60.Controls.Add(this.btn6);
            this.panelControl60.Controls.Add(this.btn7);
            this.panelControl60.Controls.Add(this.btn8);
            this.panelControl60.Controls.Add(this.btn9);
            this.panelControl60.Controls.Add(this.btn0);
            this.panelControl60.Controls.Add(this.btn1);
            this.panelControl60.Controls.Add(this.btn2);
            this.panelControl60.Controls.Add(this.btn3);
            this.panelControl60.Controls.Add(this.btn4);
            this.panelControl60.Controls.Add(this.txtMakhachhang);
            this.panelControl60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl60.Location = new System.Drawing.Point(138, 0);
            this.panelControl60.Name = "panelControl60";
            this.panelControl60.Size = new System.Drawing.Size(884, 119);
            this.panelControl60.TabIndex = 18;
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Location = new System.Drawing.Point(763, 55);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(114, 60);
            this.btnXoa.TabIndex = 46;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(383, 55);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(70, 60);
            this.btn5.TabIndex = 45;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(459, 55);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(70, 60);
            this.btn6.TabIndex = 42;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(535, 55);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(70, 60);
            this.btn7.TabIndex = 43;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(611, 55);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(70, 60);
            this.btn8.TabIndex = 44;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(687, 54);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(70, 60);
            this.btn9.TabIndex = 41;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(3, 55);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(70, 60);
            this.btn0.TabIndex = 40;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(79, 55);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(70, 60);
            this.btn1.TabIndex = 37;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(155, 55);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(70, 60);
            this.btn2.TabIndex = 38;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(231, 55);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(70, 60);
            this.btn3.TabIndex = 39;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(307, 55);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(70, 60);
            this.btn4.TabIndex = 36;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn0_Click);
            // 
            // txtMakhachhang
            // 
            this.txtMakhachhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMakhachhang.Location = new System.Drawing.Point(0, 0);
            this.txtMakhachhang.Name = "txtMakhachhang";
            this.txtMakhachhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMakhachhang.Properties.Appearance.Options.UseFont = true;
            this.txtMakhachhang.Properties.PasswordChar = '*';
            this.txtMakhachhang.Size = new System.Drawing.Size(884, 48);
            this.txtMakhachhang.TabIndex = 4;
            this.txtMakhachhang.Tag = "0";
            // 
            // panelControl61
            // 
            this.panelControl61.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl61.Controls.Add(this.labelControl21);
            this.panelControl61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl61.Location = new System.Drawing.Point(0, 0);
            this.panelControl61.Name = "panelControl61";
            this.panelControl61.Size = new System.Drawing.Size(138, 119);
            this.panelControl61.TabIndex = 17;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl21.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl21.Location = new System.Drawing.Point(10, 18);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(122, 19);
            this.labelControl21.TabIndex = 15;
            this.labelControl21.Text = "Mã khách hàng";
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.flowLayoutPanel1);
            this.panelControl15.Controls.Add(this.panelControl19);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl15.Location = new System.Drawing.Point(0, 208);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(1022, 140);
            this.panelControl15.TabIndex = 39;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn5t);
            this.flowLayoutPanel1.Controls.Add(this.btn1k);
            this.flowLayoutPanel1.Controls.Add(this.btn2k);
            this.flowLayoutPanel1.Controls.Add(this.btn5k);
            this.flowLayoutPanel1.Controls.Add(this.btn10k);
            this.flowLayoutPanel1.Controls.Add(this.btn20k);
            this.flowLayoutPanel1.Controls.Add(this.btn50k);
            this.flowLayoutPanel1.Controls.Add(this.btn100k);
            this.flowLayoutPanel1.Controls.Add(this.btn200k);
            this.flowLayoutPanel1.Controls.Add(this.btn500k);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(138, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(884, 140);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn5t
            // 
            this.btn5t.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5t.Appearance.Options.UseFont = true;
            this.btn5t.Location = new System.Drawing.Point(3, 3);
            this.btn5t.Name = "btn5t";
            this.btn5t.Size = new System.Drawing.Size(170, 60);
            this.btn5t.TabIndex = 35;
            this.btn5t.Text = "500";
            this.btn5t.Click += new System.EventHandler(this.btn5t_Click);
            // 
            // btn1k
            // 
            this.btn1k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn1k.Appearance.Options.UseFont = true;
            this.btn1k.Location = new System.Drawing.Point(179, 3);
            this.btn1k.Name = "btn1k";
            this.btn1k.Size = new System.Drawing.Size(170, 60);
            this.btn1k.TabIndex = 32;
            this.btn1k.Text = "1,000";
            this.btn1k.Click += new System.EventHandler(this.btn1k_Click);
            // 
            // btn2k
            // 
            this.btn2k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn2k.Appearance.Options.UseFont = true;
            this.btn2k.Location = new System.Drawing.Point(355, 3);
            this.btn2k.Name = "btn2k";
            this.btn2k.Size = new System.Drawing.Size(170, 60);
            this.btn2k.TabIndex = 33;
            this.btn2k.Text = "2,000";
            this.btn2k.Click += new System.EventHandler(this.btn2k_Click);
            // 
            // btn5k
            // 
            this.btn5k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5k.Appearance.Options.UseFont = true;
            this.btn5k.Location = new System.Drawing.Point(531, 3);
            this.btn5k.Name = "btn5k";
            this.btn5k.Size = new System.Drawing.Size(170, 60);
            this.btn5k.TabIndex = 34;
            this.btn5k.Text = "5,000";
            this.btn5k.Click += new System.EventHandler(this.btn5k_Click);
            // 
            // btn10k
            // 
            this.btn10k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn10k.Appearance.Options.UseFont = true;
            this.btn10k.Location = new System.Drawing.Point(707, 3);
            this.btn10k.Name = "btn10k";
            this.btn10k.Size = new System.Drawing.Size(170, 60);
            this.btn10k.TabIndex = 26;
            this.btn10k.Text = "10,000";
            this.btn10k.Click += new System.EventHandler(this.btn10k_Click);
            // 
            // btn20k
            // 
            this.btn20k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn20k.Appearance.Options.UseFont = true;
            this.btn20k.Location = new System.Drawing.Point(3, 69);
            this.btn20k.Name = "btn20k";
            this.btn20k.Size = new System.Drawing.Size(170, 60);
            this.btn20k.TabIndex = 27;
            this.btn20k.Text = "20,000";
            this.btn20k.Click += new System.EventHandler(this.btn20k_Click);
            // 
            // btn50k
            // 
            this.btn50k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn50k.Appearance.Options.UseFont = true;
            this.btn50k.Location = new System.Drawing.Point(179, 69);
            this.btn50k.Name = "btn50k";
            this.btn50k.Size = new System.Drawing.Size(170, 60);
            this.btn50k.TabIndex = 28;
            this.btn50k.Text = "50,000";
            this.btn50k.Click += new System.EventHandler(this.btn50k_Click);
            // 
            // btn100k
            // 
            this.btn100k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn100k.Appearance.Options.UseFont = true;
            this.btn100k.Location = new System.Drawing.Point(355, 69);
            this.btn100k.Name = "btn100k";
            this.btn100k.Size = new System.Drawing.Size(170, 60);
            this.btn100k.TabIndex = 29;
            this.btn100k.Text = "100,000";
            this.btn100k.Click += new System.EventHandler(this.btn100k_Click);
            // 
            // btn200k
            // 
            this.btn200k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn200k.Appearance.Options.UseFont = true;
            this.btn200k.Location = new System.Drawing.Point(531, 69);
            this.btn200k.Name = "btn200k";
            this.btn200k.Size = new System.Drawing.Size(170, 60);
            this.btn200k.TabIndex = 30;
            this.btn200k.Text = "200,000";
            this.btn200k.Click += new System.EventHandler(this.btn200k_Click);
            // 
            // btn500k
            // 
            this.btn500k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn500k.Appearance.Options.UseFont = true;
            this.btn500k.Location = new System.Drawing.Point(707, 69);
            this.btn500k.Name = "btn500k";
            this.btn500k.Size = new System.Drawing.Size(170, 60);
            this.btn500k.TabIndex = 31;
            this.btn500k.Text = "500,000";
            this.btn500k.Click += new System.EventHandler(this.btn500k_Click);
            // 
            // panelControl19
            // 
            this.panelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl19.Location = new System.Drawing.Point(0, 0);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(138, 140);
            this.panelControl19.TabIndex = 18;
            // 
            // panelControl16
            // 
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl17);
            this.panelControl16.Controls.Add(this.panelControl18);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl16.Location = new System.Drawing.Point(0, 156);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(1022, 52);
            this.panelControl16.TabIndex = 40;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Controls.Add(this.txtTienThoiLai);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(138, 0);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(884, 52);
            this.panelControl17.TabIndex = 18;
            // 
            // txtTienThoiLai
            // 
            this.txtTienThoiLai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienThoiLai.EditValue = "0";
            this.txtTienThoiLai.EnterMoveNextControl = true;
            this.txtTienThoiLai.Location = new System.Drawing.Point(0, 0);
            this.txtTienThoiLai.Name = "txtTienThoiLai";
            this.txtTienThoiLai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThoiLai.Properties.Appearance.Options.UseFont = true;
            this.txtTienThoiLai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienThoiLai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienThoiLai.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienThoiLai.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienThoiLai.Properties.EditFormat.FormatString = "N0";
            this.txtTienThoiLai.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienThoiLai.Properties.Mask.EditMask = "N0";
            this.txtTienThoiLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienThoiLai.Properties.ReadOnly = true;
            this.txtTienThoiLai.Size = new System.Drawing.Size(884, 48);
            this.txtTienThoiLai.TabIndex = 4;
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.labelControl8);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl18.Location = new System.Drawing.Point(0, 0);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(138, 52);
            this.panelControl18.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl8.Location = new System.Drawing.Point(10, 18);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(98, 19);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Tiền thối lại";
            // 
            // pnlQuydoi
            // 
            this.pnlQuydoi.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlQuydoi.Controls.Add(this.panelControl63);
            this.pnlQuydoi.Controls.Add(this.panelControl64);
            this.pnlQuydoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlQuydoi.Location = new System.Drawing.Point(0, 104);
            this.pnlQuydoi.Name = "pnlQuydoi";
            this.pnlQuydoi.Size = new System.Drawing.Size(1022, 52);
            this.pnlQuydoi.TabIndex = 45;
            this.pnlQuydoi.Visible = false;
            // 
            // panelControl63
            // 
            this.panelControl63.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl63.Controls.Add(this.txtTienquydoi);
            this.panelControl63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl63.Location = new System.Drawing.Point(138, 0);
            this.panelControl63.Name = "panelControl63";
            this.panelControl63.Size = new System.Drawing.Size(884, 52);
            this.panelControl63.TabIndex = 18;
            // 
            // txtTienquydoi
            // 
            this.txtTienquydoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienquydoi.EditValue = "0";
            this.txtTienquydoi.EnterMoveNextControl = true;
            this.txtTienquydoi.Location = new System.Drawing.Point(0, 0);
            this.txtTienquydoi.Name = "txtTienquydoi";
            this.txtTienquydoi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienquydoi.Properties.Appearance.Options.UseFont = true;
            this.txtTienquydoi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienquydoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienquydoi.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienquydoi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienquydoi.Properties.EditFormat.FormatString = "N0";
            this.txtTienquydoi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienquydoi.Properties.Mask.EditMask = "N0";
            this.txtTienquydoi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienquydoi.Properties.ReadOnly = true;
            this.txtTienquydoi.Size = new System.Drawing.Size(884, 48);
            this.txtTienquydoi.TabIndex = 4;
            this.txtTienquydoi.EditValueChanged += new System.EventHandler(this.txtTienquydoi_EditValueChanged);
            // 
            // panelControl64
            // 
            this.panelControl64.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl64.Controls.Add(this.lblTienquydoi);
            this.panelControl64.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl64.Location = new System.Drawing.Point(0, 0);
            this.panelControl64.Name = "panelControl64";
            this.panelControl64.Size = new System.Drawing.Size(138, 52);
            this.panelControl64.TabIndex = 17;
            // 
            // lblTienquydoi
            // 
            this.lblTienquydoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTienquydoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTienquydoi.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lblTienquydoi.Location = new System.Drawing.Point(10, 7);
            this.lblTienquydoi.Name = "lblTienquydoi";
            this.lblTienquydoi.Size = new System.Drawing.Size(240, 19);
            this.lblTienquydoi.TabIndex = 15;
            this.lblTienquydoi.Text = "Quy đổi thành Việt Nam Đồng";
            // 
            // pnlTienkhachtra
            // 
            this.pnlTienkhachtra.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTienkhachtra.Controls.Add(this.panelControl12);
            this.pnlTienkhachtra.Controls.Add(this.panelControl14);
            this.pnlTienkhachtra.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTienkhachtra.Location = new System.Drawing.Point(0, 52);
            this.pnlTienkhachtra.Name = "pnlTienkhachtra";
            this.pnlTienkhachtra.Size = new System.Drawing.Size(1022, 52);
            this.pnlTienkhachtra.TabIndex = 38;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.txtTienKhachTra);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(138, 0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(884, 52);
            this.panelControl12.TabIndex = 18;
            // 
            // txtTienKhachTra
            // 
            this.txtTienKhachTra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienKhachTra.EditValue = "0";
            this.txtTienKhachTra.EnterMoveNextControl = true;
            this.txtTienKhachTra.Location = new System.Drawing.Point(0, 0);
            this.txtTienKhachTra.Name = "txtTienKhachTra";
            this.txtTienKhachTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienKhachTra.Properties.Appearance.Options.UseFont = true;
            this.txtTienKhachTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienKhachTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienKhachTra.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienKhachTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienKhachTra.Properties.EditFormat.FormatString = "N0";
            this.txtTienKhachTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienKhachTra.Properties.Mask.EditMask = "N0";
            this.txtTienKhachTra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienKhachTra.Size = new System.Drawing.Size(884, 48);
            this.txtTienKhachTra.TabIndex = 4;
            this.txtTienKhachTra.EditValueChanged += new System.EventHandler(this.txtTienKhachTra_EditValueChanged);
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.labelControl7);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(138, 52);
            this.panelControl14.TabIndex = 17;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl7.Location = new System.Drawing.Point(10, 18);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(118, 19);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Tiền khách trả";
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Controls.Add(this.panelControl10);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1022, 52);
            this.panelControl8.TabIndex = 37;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.txtTienve);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(138, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(884, 52);
            this.panelControl9.TabIndex = 18;
            // 
            // txtTienve
            // 
            this.txtTienve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienve.EditValue = "0";
            this.txtTienve.EnterMoveNextControl = true;
            this.txtTienve.Location = new System.Drawing.Point(0, 0);
            this.txtTienve.Name = "txtTienve";
            this.txtTienve.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienve.Properties.Appearance.Options.UseFont = true;
            this.txtTienve.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienve.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienve.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienve.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienve.Properties.EditFormat.FormatString = "N0";
            this.txtTienve.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienve.Properties.Mask.EditMask = "N0";
            this.txtTienve.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienve.Properties.ReadOnly = true;
            this.txtTienve.Size = new System.Drawing.Size(884, 48);
            this.txtTienve.TabIndex = 4;
            this.txtTienve.EditValueChanged += new System.EventHandler(this.txtTienve_EditValueChanged);
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.labelControl6);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(138, 52);
            this.panelControl10.TabIndex = 17;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl6.Location = new System.Drawing.Point(10, 18);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(78, 19);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Tổng tiền";
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Tên hành khách";
            this.gridColumn1.FieldName = "MAKHUVUC";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            this.gridColumn1.Width = 150;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Tên hành khách";
            this.gridColumn2.FieldName = "MAKHUVUC";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.AllowSize = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            this.gridColumn2.Width = 150;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Tên hành khách";
            this.gridColumn3.FieldName = "MAKHUVUC";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.AllowSize = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 150;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemLookUpEdit1.AppearanceDropDownHeader.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIVE", 30, "Loại vé")});
            this.repositoryItemLookUpEdit1.DisplayMember = "TENLOAIVE";
            this.repositoryItemLookUpEdit1.DropDownItemHeight = 40;
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "Chọn loại vé";
            this.repositoryItemLookUpEdit1.ValueMember = "MALOAIVE";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Controls.Add(this.btnBanve1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1026, 43);
            this.panelControl4.TabIndex = 48;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.pictureEdit2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(820, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(204, 39);
            this.panelControl5.TabIndex = 13;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit2.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Ticket;
            this.pictureEdit2.Location = new System.Drawing.Point(-179, 2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(381, 35);
            this.pictureEdit2.TabIndex = 2;
            // 
            // btnBanve1
            // 
            this.btnBanve1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnBanve1.Controls.Add(this.title);
            this.btnBanve1.Controls.Add(this.home);
            this.btnBanve1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanve1.Location = new System.Drawing.Point(2, 2);
            this.btnBanve1.Name = "btnBanve1";
            this.btnBanve1.Size = new System.Drawing.Size(818, 39);
            this.btnBanve1.TabIndex = 8;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(49, 11);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(93, 19);
            this.title.TabIndex = 2;
            this.title.Text = "Thanh toán";
            // 
            // home
            // 
            this.home.Image = global::KP_RES.Properties.Resources.card_in_use_26;
            this.home.Location = new System.Drawing.Point(10, 4);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            // 
            // PF_Thanhtoan
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1026, 678);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "PF_Thanhtoan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thanh toán";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PF_Thanhtoan_FormClosing);
            this.Shown += new System.EventHandler(this.PF_Thanhtoan_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PF_Thanhtoan_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlBanve)).EndInit();
            this.pnlBanve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).EndInit();
            this.panelControl58.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).EndInit();
            this.panelControl57.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvtiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).EndInit();
            this.panelControl59.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdPhuthu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPhuthu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).EndInit();
            this.panelControl62.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMakhachhang)).EndInit();
            this.pnlMakhachhang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).EndInit();
            this.panelControl60.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMakhachhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).EndInit();
            this.panelControl61.ResumeLayout(false);
            this.panelControl61.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThoiLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuydoi)).EndInit();
            this.pnlQuydoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).EndInit();
            this.panelControl63.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienquydoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).EndInit();
            this.panelControl64.ResumeLayout(false);
            this.panelControl64.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTienkhachtra)).EndInit();
            this.pnlTienkhachtra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienKhachTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).EndInit();
            this.btnBanve1.ResumeLayout(false);
            this.btnBanve1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.PanelControl pnlBanve;
        private DevExpress.XtraEditors.PanelControl panelControl58;
        private DevExpress.XtraEditors.PanelControl panelControl57;
        private DevExpress.XtraGrid.GridControl grdTiente;
        private DevExpress.XtraGrid.Views.Grid.GridView grvtiente;
        private DevExpress.XtraGrid.Columns.GridColumn STTTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_TIENTE;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_TIENTE;
        private DevExpress.XtraGrid.Columns.GridColumn TYGIA;
        private DevExpress.XtraEditors.PanelControl panelControl59;
        private DevExpress.XtraEditors.SimpleButton btnXuongtiente;
        private DevExpress.XtraEditors.SimpleButton btnLentiente;
        private DevExpress.XtraEditors.PanelControl panelControl62;
        private DevExpress.XtraEditors.SimpleButton btnNhaplaitienkhachtra;
        private DevExpress.XtraEditors.SimpleButton btnTTTNB;
        private DevExpress.XtraEditors.SimpleButton btnInve;
        private DevExpress.XtraEditors.SimpleButton btnKHTT;
        private DevExpress.XtraEditors.PanelControl pnlMakhachhang;
        private DevExpress.XtraEditors.PanelControl panelControl60;
        private DevExpress.XtraEditors.TextEdit txtMakhachhang;
        private DevExpress.XtraEditors.PanelControl panelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.TextEdit txtTienThoiLai;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl pnlQuydoi;
        private DevExpress.XtraEditors.PanelControl panelControl63;
        private DevExpress.XtraEditors.TextEdit txtTienquydoi;
        private DevExpress.XtraEditors.PanelControl panelControl64;
        private DevExpress.XtraEditors.LabelControl lblTienquydoi;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn5t;
        private DevExpress.XtraEditors.SimpleButton btn1k;
        private DevExpress.XtraEditors.SimpleButton btn2k;
        private DevExpress.XtraEditors.SimpleButton btn5k;
        private DevExpress.XtraEditors.SimpleButton btn10k;
        private DevExpress.XtraEditors.SimpleButton btn20k;
        private DevExpress.XtraEditors.SimpleButton btn50k;
        private DevExpress.XtraEditors.SimpleButton btn100k;
        private DevExpress.XtraEditors.SimpleButton btn200k;
        private DevExpress.XtraEditors.SimpleButton btn500k;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.PanelControl pnlTienkhachtra;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.TextEdit txtTienKhachTra;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.TextEdit txtTienve;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnInve1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl btnBanve1;
        private DevExpress.XtraEditors.LabelControl title;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdPhuthu;
        private DevExpress.XtraGrid.Views.Grid.GridView grvPhuthu;
        private DevExpress.XtraGrid.Columns.GridColumn STT12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnXuongphuthu;
        private DevExpress.XtraEditors.SimpleButton btnLenphuthu;
    }
}