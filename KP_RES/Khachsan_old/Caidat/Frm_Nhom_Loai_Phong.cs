﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace KP_RES 
{
    public partial class Frm_Nhom_Loai_Phong : DevExpress.XtraEditors.XtraForm
    {
        string smaNhom = "";
        string smaPhong = "";
        string smaLoai = "";

        public Frm_Nhom_Loai_Phong()
        {
            InitializeComponent();
            LoadCombo();
            LoaddataGridView();
            LoaddatagridviewLoai();
            LoadPermission();
        }

        private void Frm_Nhom_Loai_Phong_Load(object sender, EventArgs e)
        {
            if (pal_Nhom.Controls.Count > 0)
            {
                btKhuvuc_Click(pal_Nhom.Controls[0], null);
            }
            if (pal_Nhom.VerticalScroll.Visible)
            {
                panel.Width = 152;
            }
            else
            {
                panel.Width = 135;
            }
        }

        private void Frm_Nhom_Loai_Phong_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void Frm_Nhom_Loai_Phong_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (this.ActiveControl.Parent == palPhong)
                {
                    if (this.ActiveControl.Location.Y == 0)
                        return;
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X, this.ActiveControl.Location.Y - 1);
                }
            }
            if (e.KeyCode == Keys.Down)
            {
                if (this.ActiveControl.Parent == palPhong)
                {
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X, this.ActiveControl.Location.Y + 1);
                }
            }
            if (e.KeyCode == Keys.Left)
            {
                if (this.ActiveControl.Parent == palPhong)
                {
                    if (this.ActiveControl.Location.X == 0)
                        return;
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X - 1, this.ActiveControl.Location.Y);
                }
            }
            if (e.KeyCode == Keys.Right)
            {
                if (this.ActiveControl.Parent == palPhong)
                {
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X + 1, this.ActiveControl.Location.Y);
                }
            }
        }

        private void btnChonNhom_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_Nhom;
        }
        private void bttLoai_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = Tab_Loai;
        }
        private void btnChonPhong_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_Phong;
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void bnt_capnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            if (pal_Nhom.Controls.Count > 0)
            {
                btKhuvuc_Click(pal_Nhom.Controls[0], null);
            }
        }

        private void bnt_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pal_Nhom.AutoScrollPosition = new Point(0, pal_Nhom.VerticalScroll.Value - pal_Nhom.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btn_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pal_Nhom.AutoScrollPosition = new Point(0, pal_Nhom.VerticalScroll.Value + pal_Nhom.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnThemKhuvuc_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
        }

        private void btnSuaKhuvuc_Click(object sender, EventArgs e)
        {
            if (smaNhom == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
        }

        private void btnXoaKhuvuc_Click(object sender, EventArgs e)
        {
            if (smaNhom == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From NHOMPHONG" + "\n";
            sSQL += "Where MA_NHOM=" + clsMain.SQLString(smaNhom) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuKhuvuc_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picKhuvuc.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaNhom == "" && btnThemKhuvuc.Enabled)
            {
                sSQL += "Insert into NHOMPHONG (TEN_NHOM,GHICHU,SUDUNG,STT,HINHANH) Values (@TEN_NHOM,@GHICHU,@SUDUNG,@STT,@HINHANH)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOM", (object)txt_Nhom.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txt_ghichu.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTT.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if ((smaNhom != "" && btnSuaKhuvuc.Enabled))
            {
                sSQL += "Update NHOMPHONG Set TEN_NHOM=@TEN_NHOM,GHICHU=@GHICHU,SUDUNG=@SUDUNG,STT=@STT,HINHANH=@HINHANH " + "\n";
                sSQL += "Where MA_NHOM=" + clsMain.SQLString(smaNhom) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOM", (object)txt_Nhom.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txt_ghichu.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTT.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView();
                btnThemKhuvuc.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bnt_tranglen_Click(object sender, EventArgs e)
        {
            try
            {
                palPhong.AutoScrollPosition = new Point(0, palPhong.VerticalScroll.Value - palPhong.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void bnt_trangxuong_Click(object sender, EventArgs e)
        {
            try
            {
                palPhong.AutoScrollPosition = new Point(0, palPhong.VerticalScroll.Value + palPhong.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void pic_hinhnen_Click(object sender, EventArgs e)
        {
            if (pic_hinhnen.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        pic_hinhnen.ImageLocation = openFileDialog1.FileName;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        pic_hinhnen.Image = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void picKhuvuc_Click(object sender, EventArgs e)
        {
            if (picKhuvuc.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picKhuvuc.ImageLocation = openFileDialog1.FileName;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picKhuvuc.Image = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }


        private void bnt_themPhong_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            cboNhom.Focus();
        }

        private void bnt_suaPhong_Click(object sender, EventArgs e)
        {
            if (smaPhong == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboNhom.Focus();
        }

        private void bnt_xoaPhong_Click(object sender, EventArgs e)
        {
            if (smaPhong == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From PHONG" + "\n";
            sSQL += "Where MA_PHONG=" + clsMain.SQLString(smaPhong) + "\n";
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                palPhong.Controls.Clear();
                LoaddataGridView1(smaNhom);
            }
            else
            {
                XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bnt_luuPhong_Click(object sender, EventArgs e)
        {
            if (!CheckInput_Phong())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(pic_hinhnen.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sql = "";
            if (smaPhong == "" && bnt_themban.Enabled )
            {
                sql += "INSERT INTO PHONG(TEN_PHONG,MA_NHOM,MA_LOAI,MA_TT,SUDUNG,HINHANH) values ";
                sql += "(@TEN_PHONG,@MA_NHOM,@MA_LOAI,@MA_TT,@SUDUNG,@HINHANH)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sql, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_PHONG", (object)txt_Phong.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOM", (object)cboNhom.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MA_LOAI", (object)cboLoai.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MA_TT", (object)cboTinhtrang.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSudungban.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if(smaPhong != "" && bnt_suaban.Enabled)
            {
                sql += "UPDATE PHONG SET TEN_PHONG=@TEN_PHONG,MA_NHOM=@MA_NHOM,MA_LOAI=@MA_LOAI,MA_TT=@MA_TT,SUDUNG=@SUDUNG,HINHANH=@HINHANH" + "\n";
                sql += "Where MA_PHONG=" + clsMain.SQLString(smaPhong) + "\n";
                SqlCommand SqlCom = new SqlCommand(sql, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_PHONG", (object)txt_Phong.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOM", (object)cboNhom.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MA_LOAI", (object)cboLoai.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MA_TT", (object)cboTinhtrang.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }
            if (bRunSQL)
            {

                palPhong.Controls.Clear();
                LoaddataGridView1(smaNhom);
                bnt_themban.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btKhuvuc_Click(object sender, EventArgs e)
        {
            btnChonNhom_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_Nhom.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaNhom = btn.Tag.ToString();
            txt_Nhom.Text = dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["TEN"].ToString();
            txtSTT.Text = dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["STT"].ToString();
            txt_ghichu.Text = dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["SUDUNG"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtNhom.Select("MA=" + clsMain.SQLString(smaNhom))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picKhuvuc.Image = bm;
            }
            catch
            {
                picKhuvuc.Image = null;
            }
            palPhong.Controls.Clear();
            LoaddataGridView1(smaNhom);
        }

        private void btban_Click(object sender, EventArgs e)
        {
            btnChonPhong_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in palPhong.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaPhong = btn.Tag.ToString();
            cboNhom.EditValue = int.Parse(dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["MA_NHOM"].ToString());
            cboLoai.EditValue = int.Parse(dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["MA_LOAI"].ToString());
            cboTinhtrang.EditValue = int.Parse(dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["MA_TT"].ToString());
            txt_Phong.Text = dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["TEN"].ToString();

            chkSudungban.Checked = bool.Parse(dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["SUDUNG"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtPhong.Select("MA=" + clsMain.SQLString(smaPhong))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                pic_hinhnen.Image = bm;
            }
            catch
            {
                pic_hinhnen.Image = null;
            }
        }
        private void SetRongControl()
        {
            smaNhom = "";
            txt_Nhom.Text = "";
            txt_ghichu.Text = "";
            txtSTT.Text = "1";
            chkSUDUNG.Checked = true;
            picKhuvuc.Image = null;
        }

        private void SetRongControl1()
        {
            smaPhong = "";
            cboNhom.EditValue = (smaNhom != "") ? int.Parse(smaNhom) : cboNhom.Properties.GetDataSourceValue(cboNhom.Properties.ValueMember, 0);
            txt_Phong.Text = "";
            chkSudungban.Checked = true;
            pic_hinhnen.Image = null;
        }

        private void SetRongControlLoai()
        {
            smaLoai = "";
            txtTenloai.Text = "";
            txtGhichuloai.Text = "";
            chkSudungLoai.Checked = true;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//mở
            {
                txt_Nhom.Properties.ReadOnly = true;
                txt_ghichu.Properties.ReadOnly = true;
                txtSTT.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                picKhuvuc.Enabled  = false;
            }
            else//đóng
            {
                txt_Nhom.Properties.ReadOnly = false;
                txt_ghichu.Properties.ReadOnly = false;
                txtSTT.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                picKhuvuc.Enabled = true ;
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//mở
            {
                cboNhom.Properties.ReadOnly = true;
                cboLoai.Properties.ReadOnly = true;
                cboTinhtrang.Properties.ReadOnly = true;
                txt_Phong.Properties.ReadOnly = true;
                chkSudungban.Properties.ReadOnly = true;
                pic_hinhnen.Enabled = false ;
            }
            else//đóng
            {
                cboNhom.Properties.ReadOnly = false;
                cboLoai.Properties.ReadOnly = false;
                cboTinhtrang.Properties.ReadOnly = false;
                txt_Phong.Properties.ReadOnly = false;
                chkSudungban.Properties.ReadOnly = false;
                pic_hinhnen.Enabled = true  ;
            }
        }

        private void KhoaMoControl2(bool tf)
        {
            if (tf == true)//mở
            {
                txtTenloai.Properties.ReadOnly = true;
                txtGhichuloai.Properties.ReadOnly = true;
                chkSudungLoai.Properties.ReadOnly = true;
            }
            else//đóng
            {
                txtTenloai.Properties.ReadOnly = false;
                txtGhichuloai.Properties.ReadOnly = false;
                chkSudungLoai.Properties.ReadOnly = false;
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA_NHOM As MA,TEN_NHOM As TEN" + "\n";
            sSQL += "From NHOMPHONG" + "\n";
            sSQL += "Order By TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboNhom.Properties.DataSource = dt;
            cboNhom.EditValue = cboNhom.Properties.GetDataSourceValue(cboNhom.Properties.ValueMember, 0);

            sSQL = "Select MA_LOAI As MA,TEN_LOAI As TEN" + "\n";
            sSQL += "From LOAIPHONG" + "\n";
            sSQL += "Order By TEN" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboLoai.Properties.DataSource = dt;
            cboLoai.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());

            sSQL = "Select MA_TT As MA,TEN_TT As TEN" + "\n";
            sSQL += "From TINHTRANG_PHONG" + "\n";
            sSQL += "Order By TEN" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboTinhtrang.Properties.DataSource = dt;
            cboTinhtrang.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());
        }

        private void LoadPermission()
        {
            btnThemKhuvuc.Enabled = clsUserManagement.AllowAdd("7");
            btnLuuKhuvuc.Enabled = clsUserManagement.AllowEdit("7");
            btnXoaKhuvuc.Enabled = clsUserManagement.AllowDelete("7");
            btnLuuKhuvuc.Enabled = btnThemKhuvuc.Enabled || btnSuaKhuvuc.Enabled;

            btnThemLoai.Enabled = clsUserManagement.AllowAdd("7");
            btnLuuLoai.Enabled = clsUserManagement.AllowEdit("7");
            btnXoaLoai.Enabled = clsUserManagement.AllowDelete("7");
            btnLuuLoai.Enabled = btnThemLoai.Enabled || btnSuaLoai.Enabled;

            bnt_themban.Enabled = clsUserManagement.AllowAdd("7");
            bnt_suaban.Enabled = clsUserManagement.AllowEdit("7");
            bnt_xoaban.Enabled = clsUserManagement.AllowDelete("7");
            bnt_luuban.Enabled = bnt_themban.Enabled || bnt_suaban.Enabled;
        }

        DataTable dtNhom = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select MA_NHOM As MA,TEN_NHOM As TEN,GHICHU,SUDUNG,STT,HINHANH" + "\n";
            sSQL += "From NHOMPHONG" + "\n";
            sSQL += "Order by SUDUNG DESC,STT,TEN_NHOM" + "\n";
            dtNhom = clsMain.ReturnDataTable(sSQL);
            pal_Nhom.Controls.Clear();
            palPhong.Controls.Clear();
            foreach (DataRow dr in dtNhom.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pal_Nhom.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Click += new EventHandler(btKhuvuc_Click);
                pal_Nhom.Controls.Add(btn);
            }
            if (pal_Nhom.VerticalScroll.Visible)
            {
                panel.Width = 152;
            }
            else
            {
                panel.Width = 135;
            }
        }

        DataTable dtPhong = new DataTable();
        private void LoaddataGridView1(string manhom)
        {
            SetRongControl1();
            KhoaMoControl1(true);

            string sqlphong = "select MA_PHONG As MA,TEN_PHONG As TEN,MA_NHOM,MA_LOAI,MA_TT,SUDUNG,HINHANH from PHONG where MA_NHOM=" + clsMain.SQLString(manhom) + " \n";
            sqlphong += "order by MA ASC";
            dtPhong = clsMain.ReturnDataTable(sqlphong);
            palPhong.Controls.Clear();
            foreach (DataRow dr in dtPhong.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Font = palPhong.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Size = new Size(110,80);
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Click += new System.EventHandler(btban_Click);
                palPhong.Controls.Add(btn);
            }
        }
        private void LoaddatagridviewLoai()
        {
            SetRongControlLoai();
            KhoaMoControl2(true);

            string sql = "select MA_LOAI,TEN_LOAI,GHICHU,SUDUNG from LOAIPHONG";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl1.DataSource = dt;
        }
        private Boolean CheckInput()
        {
            if (txt_Nhom.Text == "")
            {
                XtraMessageBox.Show("Nhập nhóm phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_Nhom.Focus();
                return false;
            }
            return true;
        }

        private Boolean CheckInput_Phong()
        {
            if (txt_Phong.Text == "")
            {
                XtraMessageBox.Show("Nhập tên phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_Phong.Focus();
                return false;
            }
            return true;
        }

        private void bttSuaLoai_Click(object sender, EventArgs e)
        {
            if (smaNhom == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl2(false);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            smaLoai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_LOAI").ToString();
            txtTenloai.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TEN_LOAI").ToString();
            txtGhichuloai.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GHICHU").ToString();
            chkSudungLoai.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void bttThemLoai_Click(object sender, EventArgs e)
        {
            SetRongControlLoai();
            KhoaMoControl2(false);
        }

        private void bttXoaLoai_Click(object sender, EventArgs e)
        {
            if (smaLoai == "")
            {
                XtraMessageBox.Show("Chưa chọn dòng muốn xóa","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From LOAIPHONG" + "\n";
            sSQL += "Where MA_LOAI=" + clsMain.SQLString(txtTenloai.Tag.ToString()) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddatagridviewLoai();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInputLoai()
        {
            if (txtTenloai.Text == "")
            {
                XtraMessageBox.Show("Nhập tên loại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenloai.Focus();
                return false;
            }
            return true;
        }

        private void bttLuuLoai_Click(object sender, EventArgs e)
        {
            if (!CheckInputLoai())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picKhuvuc.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaLoai == "" && btnThemLoai.Enabled)
            {
                sSQL += "Insert into LOAIPHONG (TEN_LOAI,GHICHU,SUDUNG) Values (@TEN_LOAI,@GHICHU,@SUDUNG)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_LOAI", (object)txt_Nhom.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txt_ghichu.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlComMain = SqlCom;
            }
            else if ((smaLoai != "" && btnSuaLoai.Enabled))
            {
                sSQL += "Update LOAIPHONG Set TEN_LOAI=@TEN_LOAI,GHICHU=@GHICHU,SUDUNG=@SUDUNG " + "\n";
                sSQL += "Where MA_LOAI=" + clsMain.SQLString(smaLoai) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_LOAI", (object)txtTenloai.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGhichuloai.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSudungLoai.Checked));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoaddatagridviewLoai();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
  
    }
}