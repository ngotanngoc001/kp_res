﻿namespace KP_RES 
{
    partial class Frm_Nhom_Loai_Phong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Nhom_Loai_Phong));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pal_1 = new DevExpress.XtraEditors.PanelControl();
            this.palPhong = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.pal_Nhom = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Xuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tab_Option = new DevExpress.XtraTab.XtraTabControl();
            this.tab_Phong = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cboTinhtrang = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cboLoai = new DevExpress.XtraEditors.LookUpEdit();
            this.bnt_suaban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_xoaban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_luuban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_themban = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.pic_hinhnen = new System.Windows.Forms.PictureBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cboNhom = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSudungban = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt_Phong = new DevExpress.XtraEditors.TextEdit();
            this.tab_Nhom = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.picKhuvuc = new System.Windows.Forms.PictureBox();
            this.btnSuaKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.txt_ghichu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnThemKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.txtSTT = new DevExpress.XtraEditors.TextEdit();
            this.txt_Nhom = new DevExpress.XtraEditors.TextEdit();
            this.btnLuuKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Tab_Loai = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkSD = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnSuaLoai = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaLoai = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuLoai = new DevExpress.XtraEditors.SimpleButton();
            this.chkSudungLoai = new DevExpress.XtraEditors.CheckEdit();
            this.txtTenloai = new DevExpress.XtraEditors.TextEdit();
            this.txtGhichuloai = new DevExpress.XtraEditors.TextEdit();
            this.btnThemLoai = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonNhom = new DevExpress.XtraEditors.SimpleButton();
            this.bttLoai = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonPhong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).BeginInit();
            this.pal_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).BeginInit();
            this.tab_Option.SuspendLayout();
            this.tab_Phong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTinhtrang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_hinhnen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phong.Properties)).BeginInit();
            this.tab_Nhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picKhuvuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ghichu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Nhom.Properties)).BeginInit();
            this.Tab_Loai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenloai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhichuloai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.panel);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 615);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pal_1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(535, 615);
            this.panelControl5.TabIndex = 4;
            // 
            // pal_1
            // 
            this.pal_1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pal_1.Appearance.Options.UseBackColor = true;
            this.pal_1.Controls.Add(this.palPhong);
            this.pal_1.Controls.Add(this.panelControl6);
            this.pal_1.Controls.Add(this.pal_sotrang);
            this.pal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_1.Location = new System.Drawing.Point(0, 0);
            this.pal_1.Name = "pal_1";
            this.pal_1.Size = new System.Drawing.Size(535, 615);
            this.pal_1.TabIndex = 1;
            // 
            // palPhong
            // 
            this.palPhong.AutoScroll = true;
            this.palPhong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palPhong.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.palPhong.Location = new System.Drawing.Point(2, 37);
            this.palPhong.Name = "palPhong";
            this.palPhong.Size = new System.Drawing.Size(531, 541);
            this.palPhong.TabIndex = 55;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bnt_trangxuong);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 578);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(531, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(531, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.bnt_tranglen);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(531, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(0, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(531, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.pal_Nhom);
            this.panel.Controls.Add(this.panelControl11);
            this.panel.Controls.Add(this.pal_len);
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel.Location = new System.Drawing.Point(535, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 615);
            this.panel.TabIndex = 2;
            // 
            // pal_Nhom
            // 
            this.pal_Nhom.AutoScroll = true;
            this.pal_Nhom.BackColor = System.Drawing.Color.Transparent;
            this.pal_Nhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_Nhom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_Nhom.Location = new System.Drawing.Point(2, 37);
            this.pal_Nhom.Margin = new System.Windows.Forms.Padding(0);
            this.pal_Nhom.Name = "pal_Nhom";
            this.pal_Nhom.Size = new System.Drawing.Size(131, 541);
            this.pal_Nhom.TabIndex = 29;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.bnt_Xuong);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 578);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(131, 35);
            this.panelControl11.TabIndex = 28;
            // 
            // bnt_Xuong
            // 
            this.bnt_Xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Xuong.Appearance.Options.UseBackColor = true;
            this.bnt_Xuong.Appearance.Options.UseFont = true;
            this.bnt_Xuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bnt_Xuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Xuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_Xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Xuong.Name = "bnt_Xuong";
            this.bnt_Xuong.Size = new System.Drawing.Size(131, 35);
            this.bnt_Xuong.TabIndex = 2;
            this.bnt_Xuong.Click += new System.EventHandler(this.btn_Xuong_Click);
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.bnt_Len);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(0, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(131, 35);
            this.bnt_Len.TabIndex = 26;
            this.bnt_Len.Click += new System.EventHandler(this.bnt_Len_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.panelControl3);
            this.panelControl4.Controls.Add(this.panelControl12);
            this.panelControl4.Controls.Add(this.pal_BUTTON);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(670, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(350, 615);
            this.panelControl4.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.tab_Option);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 37);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(346, 541);
            this.panelControl3.TabIndex = 18;
            // 
            // tab_Option
            // 
            this.tab_Option.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_Option.Location = new System.Drawing.Point(0, 0);
            this.tab_Option.Name = "tab_Option";
            this.tab_Option.SelectedTabPage = this.tab_Phong;
            this.tab_Option.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tab_Option.Size = new System.Drawing.Size(346, 541);
            this.tab_Option.TabIndex = 0;
            this.tab_Option.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab_Nhom,
            this.tab_Phong,
            this.Tab_Loai});
            // 
            // tab_Phong
            // 
            this.tab_Phong.Controls.Add(this.panelControl2);
            this.tab_Phong.Name = "tab_Phong";
            this.tab_Phong.Size = new System.Drawing.Size(340, 535);
            this.tab_Phong.Text = "xtraTabPage2";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panel_Sandard);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(340, 535);
            this.panelControl2.TabIndex = 0;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.labelControl6);
            this.panel_Sandard.Controls.Add(this.cboTinhtrang);
            this.panel_Sandard.Controls.Add(this.labelControl3);
            this.panel_Sandard.Controls.Add(this.cboLoai);
            this.panel_Sandard.Controls.Add(this.bnt_suaban);
            this.panel_Sandard.Controls.Add(this.bnt_xoaban);
            this.panel_Sandard.Controls.Add(this.bnt_luuban);
            this.panel_Sandard.Controls.Add(this.bnt_themban);
            this.panel_Sandard.Controls.Add(this.panelControl8);
            this.panel_Sandard.Controls.Add(this.labelControl4);
            this.panel_Sandard.Controls.Add(this.cboNhom);
            this.panel_Sandard.Controls.Add(this.chkSudungban);
            this.panel_Sandard.Controls.Add(this.labelControl2);
            this.panel_Sandard.Controls.Add(this.txt_Phong);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Sandard.Location = new System.Drawing.Point(0, 0);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(340, 535);
            this.panel_Sandard.TabIndex = 0;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 79);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 19);
            this.labelControl6.TabIndex = 43;
            this.labelControl6.Text = "Tình trạng";
            // 
            // cboTinhtrang
            // 
            this.cboTinhtrang.EnterMoveNextControl = true;
            this.cboTinhtrang.Location = new System.Drawing.Point(106, 76);
            this.cboTinhtrang.Name = "cboTinhtrang";
            this.cboTinhtrang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTinhtrang.Properties.Appearance.Options.UseFont = true;
            this.cboTinhtrang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTinhtrang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTinhtrang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboTinhtrang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTinhtrang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboTinhtrang.Properties.DisplayMember = "TEN";
            this.cboTinhtrang.Properties.DropDownItemHeight = 40;
            this.cboTinhtrang.Properties.NullText = "";
            this.cboTinhtrang.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTinhtrang.Properties.ShowHeader = false;
            this.cboTinhtrang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTinhtrang.Properties.ValueMember = "MA";
            this.cboTinhtrang.Size = new System.Drawing.Size(228, 26);
            this.cboTinhtrang.TabIndex = 44;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(6, 44);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(79, 19);
            this.labelControl3.TabIndex = 41;
            this.labelControl3.Text = "Loại phòng";
            // 
            // cboLoai
            // 
            this.cboLoai.EnterMoveNextControl = true;
            this.cboLoai.Location = new System.Drawing.Point(106, 41);
            this.cboLoai.Name = "cboLoai";
            this.cboLoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai.Properties.Appearance.Options.UseFont = true;
            this.cboLoai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoai.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLoai.Properties.DisplayMember = "TEN";
            this.cboLoai.Properties.DropDownItemHeight = 40;
            this.cboLoai.Properties.NullText = "";
            this.cboLoai.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoai.Properties.ShowHeader = false;
            this.cboLoai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoai.Properties.ValueMember = "MA";
            this.cboLoai.Size = new System.Drawing.Size(228, 26);
            this.cboLoai.TabIndex = 42;
            // 
            // bnt_suaban
            // 
            this.bnt_suaban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_suaban.Appearance.Options.UseFont = true;
            this.bnt_suaban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_suaban.Image")));
            this.bnt_suaban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_suaban.Location = new System.Drawing.Point(172, 215);
            this.bnt_suaban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_suaban.Name = "bnt_suaban";
            this.bnt_suaban.Size = new System.Drawing.Size(80, 35);
            this.bnt_suaban.TabIndex = 11;
            this.bnt_suaban.Text = "&Sửa";
            this.bnt_suaban.Click += new System.EventHandler(this.bnt_suaPhong_Click);
            // 
            // bnt_xoaban
            // 
            this.bnt_xoaban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_xoaban.Appearance.Options.UseFont = true;
            this.bnt_xoaban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_xoaban.Image")));
            this.bnt_xoaban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_xoaban.Location = new System.Drawing.Point(255, 215);
            this.bnt_xoaban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_xoaban.Name = "bnt_xoaban";
            this.bnt_xoaban.Size = new System.Drawing.Size(80, 35);
            this.bnt_xoaban.TabIndex = 12;
            this.bnt_xoaban.Text = "&Xóa";
            this.bnt_xoaban.Click += new System.EventHandler(this.bnt_xoaPhong_Click);
            // 
            // bnt_luuban
            // 
            this.bnt_luuban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luuban.Appearance.Options.UseFont = true;
            this.bnt_luuban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_luuban.Image")));
            this.bnt_luuban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_luuban.Location = new System.Drawing.Point(6, 215);
            this.bnt_luuban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_luuban.Name = "bnt_luuban";
            this.bnt_luuban.Size = new System.Drawing.Size(80, 35);
            this.bnt_luuban.TabIndex = 9;
            this.bnt_luuban.Text = "&Lưu";
            this.bnt_luuban.Click += new System.EventHandler(this.bnt_luuPhong_Click);
            // 
            // bnt_themban
            // 
            this.bnt_themban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_themban.Appearance.Options.UseFont = true;
            this.bnt_themban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_themban.Image")));
            this.bnt_themban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_themban.Location = new System.Drawing.Point(89, 215);
            this.bnt_themban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_themban.Name = "bnt_themban";
            this.bnt_themban.Size = new System.Drawing.Size(80, 35);
            this.bnt_themban.TabIndex = 10;
            this.bnt_themban.Text = "&Thêm";
            this.bnt_themban.Click += new System.EventHandler(this.bnt_themPhong_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.pic_hinhnen);
            this.panelControl8.Location = new System.Drawing.Point(216, 111);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(118, 90);
            this.panelControl8.TabIndex = 13;
            // 
            // pic_hinhnen
            // 
            this.pic_hinhnen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pic_hinhnen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pic_hinhnen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_hinhnen.Location = new System.Drawing.Point(2, 2);
            this.pic_hinhnen.Name = "pic_hinhnen";
            this.pic_hinhnen.Size = new System.Drawing.Size(114, 86);
            this.pic_hinhnen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_hinhnen.TabIndex = 23;
            this.pic_hinhnen.TabStop = false;
            this.pic_hinhnen.Click += new System.EventHandler(this.pic_hinhnen_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 9);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(93, 19);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Nhóm phòng";
            // 
            // cboNhom
            // 
            this.cboNhom.EnterMoveNextControl = true;
            this.cboNhom.Location = new System.Drawing.Point(106, 6);
            this.cboNhom.Name = "cboNhom";
            this.cboNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhom.Properties.Appearance.Options.UseFont = true;
            this.cboNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhom.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhom.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboNhom.Properties.DisplayMember = "TEN";
            this.cboNhom.Properties.DropDownItemHeight = 40;
            this.cboNhom.Properties.NullText = "";
            this.cboNhom.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhom.Properties.ShowHeader = false;
            this.cboNhom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhom.Properties.ValueMember = "MA";
            this.cboNhom.Size = new System.Drawing.Size(228, 26);
            this.cboNhom.TabIndex = 1;
            // 
            // chkSudungban
            // 
            this.chkSudungban.EditValue = true;
            this.chkSudungban.EnterMoveNextControl = true;
            this.chkSudungban.Location = new System.Drawing.Point(87, 176);
            this.chkSudungban.Name = "chkSudungban";
            this.chkSudungban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSudungban.Properties.Appearance.Options.UseFont = true;
            this.chkSudungban.Properties.Caption = "Sử dụng";
            this.chkSudungban.Size = new System.Drawing.Size(104, 24);
            this.chkSudungban.TabIndex = 8;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(9, 114);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 19);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Tên phòng";
            // 
            // txt_Phong
            // 
            this.txt_Phong.EnterMoveNextControl = true;
            this.txt_Phong.Location = new System.Drawing.Point(106, 111);
            this.txt_Phong.Name = "txt_Phong";
            this.txt_Phong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Phong.Properties.Appearance.Options.UseFont = true;
            this.txt_Phong.Size = new System.Drawing.Size(106, 26);
            this.txt_Phong.TabIndex = 3;
            // 
            // tab_Nhom
            // 
            this.tab_Nhom.Controls.Add(this.panelControl16);
            this.tab_Nhom.Name = "tab_Nhom";
            this.tab_Nhom.Size = new System.Drawing.Size(340, 535);
            this.tab_Nhom.Text = "xtraTabPage1";
            // 
            // panelControl16
            // 
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl15);
            this.panelControl16.Controls.Add(this.btnSuaKhuvuc);
            this.panelControl16.Controls.Add(this.lblGHICHU);
            this.panelControl16.Controls.Add(this.chkSUDUNG);
            this.panelControl16.Controls.Add(this.txt_ghichu);
            this.panelControl16.Controls.Add(this.labelControl7);
            this.panelControl16.Controls.Add(this.btnThemKhuvuc);
            this.panelControl16.Controls.Add(this.txtSTT);
            this.panelControl16.Controls.Add(this.txt_Nhom);
            this.panelControl16.Controls.Add(this.btnLuuKhuvuc);
            this.panelControl16.Controls.Add(this.btnXoaKhuvuc);
            this.panelControl16.Controls.Add(this.labelControl1);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(0, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(340, 535);
            this.panelControl16.TabIndex = 15;
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.picKhuvuc);
            this.panelControl15.Location = new System.Drawing.Point(217, 11);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(118, 90);
            this.panelControl15.TabIndex = 14;
            // 
            // picKhuvuc
            // 
            this.picKhuvuc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picKhuvuc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picKhuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picKhuvuc.Location = new System.Drawing.Point(2, 2);
            this.picKhuvuc.Name = "picKhuvuc";
            this.picKhuvuc.Size = new System.Drawing.Size(114, 86);
            this.picKhuvuc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picKhuvuc.TabIndex = 23;
            this.picKhuvuc.TabStop = false;
            this.picKhuvuc.Click += new System.EventHandler(this.picKhuvuc_Click);
            // 
            // btnSuaKhuvuc
            // 
            this.btnSuaKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaKhuvuc.Appearance.Options.UseFont = true;
            this.btnSuaKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaKhuvuc.Image")));
            this.btnSuaKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaKhuvuc.Location = new System.Drawing.Point(173, 138);
            this.btnSuaKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaKhuvuc.Name = "btnSuaKhuvuc";
            this.btnSuaKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnSuaKhuvuc.TabIndex = 11;
            this.btnSuaKhuvuc.Text = "&Sửa";
            this.btnSuaKhuvuc.Click += new System.EventHandler(this.btnSuaKhuvuc_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(7, 14);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(75, 19);
            this.lblGHICHU.TabIndex = 2;
            this.lblGHICHU.Text = "Tên Nhóm";
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EditValue = true;
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(78, 107);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(99, 24);
            this.chkSUDUNG.TabIndex = 8;
            // 
            // txt_ghichu
            // 
            this.txt_ghichu.EnterMoveNextControl = true;
            this.txt_ghichu.Location = new System.Drawing.Point(89, 75);
            this.txt_ghichu.Name = "txt_ghichu";
            this.txt_ghichu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ghichu.Properties.Appearance.Options.UseFont = true;
            this.txt_ghichu.Size = new System.Drawing.Size(124, 26);
            this.txt_ghichu.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(7, 46);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(29, 19);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "STT";
            // 
            // btnThemKhuvuc
            // 
            this.btnThemKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemKhuvuc.Appearance.Options.UseFont = true;
            this.btnThemKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnThemKhuvuc.Image")));
            this.btnThemKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemKhuvuc.Location = new System.Drawing.Point(90, 138);
            this.btnThemKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemKhuvuc.Name = "btnThemKhuvuc";
            this.btnThemKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnThemKhuvuc.TabIndex = 10;
            this.btnThemKhuvuc.Text = "&Thêm";
            this.btnThemKhuvuc.Click += new System.EventHandler(this.btnThemKhuvuc_Click);
            // 
            // txtSTT
            // 
            this.txtSTT.EnterMoveNextControl = true;
            this.txtSTT.Location = new System.Drawing.Point(89, 43);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTT.Properties.Appearance.Options.UseFont = true;
            this.txtSTT.Properties.Mask.EditMask = "d";
            this.txtSTT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTT.Size = new System.Drawing.Size(124, 26);
            this.txtSTT.TabIndex = 5;
            // 
            // txt_Nhom
            // 
            this.txt_Nhom.EnterMoveNextControl = true;
            this.txt_Nhom.Location = new System.Drawing.Point(89, 11);
            this.txt_Nhom.Name = "txt_Nhom";
            this.txt_Nhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Nhom.Properties.Appearance.Options.UseFont = true;
            this.txt_Nhom.Size = new System.Drawing.Size(124, 26);
            this.txt_Nhom.TabIndex = 3;
            // 
            // btnLuuKhuvuc
            // 
            this.btnLuuKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuKhuvuc.Appearance.Options.UseFont = true;
            this.btnLuuKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuKhuvuc.Image")));
            this.btnLuuKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuKhuvuc.Location = new System.Drawing.Point(7, 138);
            this.btnLuuKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuKhuvuc.Name = "btnLuuKhuvuc";
            this.btnLuuKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnLuuKhuvuc.TabIndex = 9;
            this.btnLuuKhuvuc.Text = "&Lưu";
            this.btnLuuKhuvuc.Click += new System.EventHandler(this.btnLuuKhuvuc_Click);
            // 
            // btnXoaKhuvuc
            // 
            this.btnXoaKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaKhuvuc.Appearance.Options.UseFont = true;
            this.btnXoaKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaKhuvuc.Image")));
            this.btnXoaKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaKhuvuc.Location = new System.Drawing.Point(256, 138);
            this.btnXoaKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaKhuvuc.Name = "btnXoaKhuvuc";
            this.btnXoaKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnXoaKhuvuc.TabIndex = 12;
            this.btnXoaKhuvuc.Text = "&Xóa";
            this.btnXoaKhuvuc.Click += new System.EventHandler(this.btnXoaKhuvuc_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(7, 78);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Ghi chú";
            // 
            // Tab_Loai
            // 
            this.Tab_Loai.Controls.Add(this.panelControl18);
            this.Tab_Loai.Controls.Add(this.panelControl9);
            this.Tab_Loai.Name = "Tab_Loai";
            this.Tab_Loai.Size = new System.Drawing.Size(340, 535);
            this.Tab_Loai.Text = "xtraTabPage1";
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.gridControl1);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl18.Location = new System.Drawing.Point(0, 154);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(340, 381);
            this.panelControl18.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkSD});
            this.gridControl1.Size = new System.Drawing.Size(340, 381);
            this.gridControl1.TabIndex = 26;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_LOAI,
            this.TEN_LOAI,
            this.GHICHU,
            this.SUDUNG});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 39;
            // 
            // MA_LOAI
            // 
            this.MA_LOAI.AppearanceCell.Options.UseTextOptions = true;
            this.MA_LOAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_LOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_LOAI.AppearanceHeader.Options.UseFont = true;
            this.MA_LOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_LOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_LOAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_LOAI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_LOAI.Caption = "MA_LOAI";
            this.MA_LOAI.FieldName = "MA_LOAI";
            this.MA_LOAI.Name = "MA_LOAI";
            this.MA_LOAI.OptionsColumn.AllowEdit = false;
            this.MA_LOAI.OptionsColumn.AllowFocus = false;
            this.MA_LOAI.OptionsColumn.FixedWidth = true;
            this.MA_LOAI.Width = 100;
            // 
            // TEN_LOAI
            // 
            this.TEN_LOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_LOAI.AppearanceCell.Options.UseFont = true;
            this.TEN_LOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_LOAI.AppearanceHeader.Options.UseFont = true;
            this.TEN_LOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_LOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_LOAI.Caption = "Loại Phòng";
            this.TEN_LOAI.FieldName = "TEN_LOAI";
            this.TEN_LOAI.Name = "TEN_LOAI";
            this.TEN_LOAI.OptionsColumn.AllowEdit = false;
            this.TEN_LOAI.OptionsColumn.AllowFocus = false;
            this.TEN_LOAI.OptionsColumn.FixedWidth = true;
            this.TEN_LOAI.Visible = true;
            this.TEN_LOAI.VisibleIndex = 1;
            this.TEN_LOAI.Width = 120;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceCell.Options.UseFont = true;
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 2;
            this.GHICHU.Width = 109;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceCell.Options.UseFont = true;
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.ColumnEdit = this.chkSD;
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 3;
            this.SUDUNG.Width = 70;
            // 
            // chkSD
            // 
            this.chkSD.AutoHeight = false;
            this.chkSD.Name = "chkSD";
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.labelControl5);
            this.panelControl9.Controls.Add(this.labelControl8);
            this.panelControl9.Controls.Add(this.btnSuaLoai);
            this.panelControl9.Controls.Add(this.btnXoaLoai);
            this.panelControl9.Controls.Add(this.btnLuuLoai);
            this.panelControl9.Controls.Add(this.chkSudungLoai);
            this.panelControl9.Controls.Add(this.txtTenloai);
            this.panelControl9.Controls.Add(this.txtGhichuloai);
            this.panelControl9.Controls.Add(this.btnThemLoai);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(0, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(340, 154);
            this.panelControl9.TabIndex = 27;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(4, 14);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(61, 19);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Tên Loại";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(4, 48);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(54, 19);
            this.labelControl8.TabIndex = 19;
            this.labelControl8.Text = "Ghi chú";
            // 
            // btnSuaLoai
            // 
            this.btnSuaLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaLoai.Appearance.Options.UseFont = true;
            this.btnSuaLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaLoai.Image")));
            this.btnSuaLoai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaLoai.Location = new System.Drawing.Point(170, 108);
            this.btnSuaLoai.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaLoai.Name = "btnSuaLoai";
            this.btnSuaLoai.Size = new System.Drawing.Size(80, 35);
            this.btnSuaLoai.TabIndex = 24;
            this.btnSuaLoai.Text = "&Sửa";
            this.btnSuaLoai.Click += new System.EventHandler(this.bttSuaLoai_Click);
            // 
            // btnXoaLoai
            // 
            this.btnXoaLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLoai.Appearance.Options.UseFont = true;
            this.btnXoaLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaLoai.Image")));
            this.btnXoaLoai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaLoai.Location = new System.Drawing.Point(253, 108);
            this.btnXoaLoai.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaLoai.Name = "btnXoaLoai";
            this.btnXoaLoai.Size = new System.Drawing.Size(80, 35);
            this.btnXoaLoai.TabIndex = 25;
            this.btnXoaLoai.Text = "&Xóa";
            this.btnXoaLoai.Click += new System.EventHandler(this.bttXoaLoai_Click);
            // 
            // btnLuuLoai
            // 
            this.btnLuuLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuLoai.Appearance.Options.UseFont = true;
            this.btnLuuLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuLoai.Image")));
            this.btnLuuLoai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuLoai.Location = new System.Drawing.Point(4, 108);
            this.btnLuuLoai.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuLoai.Name = "btnLuuLoai";
            this.btnLuuLoai.Size = new System.Drawing.Size(80, 35);
            this.btnLuuLoai.TabIndex = 22;
            this.btnLuuLoai.Text = "&Lưu";
            this.btnLuuLoai.Click += new System.EventHandler(this.bttLuuLoai_Click);
            // 
            // chkSudungLoai
            // 
            this.chkSudungLoai.EditValue = true;
            this.chkSudungLoai.EnterMoveNextControl = true;
            this.chkSudungLoai.Location = new System.Drawing.Point(75, 77);
            this.chkSudungLoai.Name = "chkSudungLoai";
            this.chkSudungLoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSudungLoai.Properties.Appearance.Options.UseFont = true;
            this.chkSudungLoai.Properties.Caption = "Sử dụng";
            this.chkSudungLoai.Size = new System.Drawing.Size(99, 24);
            this.chkSudungLoai.TabIndex = 21;
            // 
            // txtTenloai
            // 
            this.txtTenloai.EnterMoveNextControl = true;
            this.txtTenloai.Location = new System.Drawing.Point(86, 11);
            this.txtTenloai.Name = "txtTenloai";
            this.txtTenloai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenloai.Properties.Appearance.Options.UseFont = true;
            this.txtTenloai.Size = new System.Drawing.Size(225, 26);
            this.txtTenloai.TabIndex = 16;
            // 
            // txtGhichuloai
            // 
            this.txtGhichuloai.EnterMoveNextControl = true;
            this.txtGhichuloai.Location = new System.Drawing.Point(86, 45);
            this.txtGhichuloai.Name = "txtGhichuloai";
            this.txtGhichuloai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhichuloai.Properties.Appearance.Options.UseFont = true;
            this.txtGhichuloai.Size = new System.Drawing.Size(225, 26);
            this.txtGhichuloai.TabIndex = 20;
            // 
            // btnThemLoai
            // 
            this.btnThemLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLoai.Appearance.Options.UseFont = true;
            this.btnThemLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnThemLoai.Image")));
            this.btnThemLoai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemLoai.Location = new System.Drawing.Point(87, 108);
            this.btnThemLoai.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemLoai.Name = "btnThemLoai";
            this.btnThemLoai.Size = new System.Drawing.Size(80, 35);
            this.btnThemLoai.TabIndex = 23;
            this.btnThemLoai.Text = "&Thêm";
            this.btnThemLoai.Click += new System.EventHandler(this.bttThemLoai_Click);
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnChonNhom);
            this.panelControl12.Controls.Add(this.bttLoai);
            this.panelControl12.Controls.Add(this.btnChonPhong);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(346, 35);
            this.panelControl12.TabIndex = 19;
            // 
            // btnChonNhom
            // 
            this.btnChonNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonNhom.Appearance.Options.UseFont = true;
            this.btnChonNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChonNhom.Image = global::KP_RES.Properties.Resources.control_panel_26;
            this.btnChonNhom.Location = new System.Drawing.Point(0, 0);
            this.btnChonNhom.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonNhom.Name = "btnChonNhom";
            this.btnChonNhom.Size = new System.Drawing.Size(116, 35);
            this.btnChonNhom.TabIndex = 1;
            this.btnChonNhom.Text = "Nhóm";
            this.btnChonNhom.Click += new System.EventHandler(this.btnChonNhom_Click);
            // 
            // bttLoai
            // 
            this.bttLoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttLoai.Appearance.Options.UseFont = true;
            this.bttLoai.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttLoai.Image = global::KP_RES.Properties.Resources.large_icons_26;
            this.bttLoai.Location = new System.Drawing.Point(116, 0);
            this.bttLoai.Margin = new System.Windows.Forms.Padding(4);
            this.bttLoai.Name = "bttLoai";
            this.bttLoai.Size = new System.Drawing.Size(115, 35);
            this.bttLoai.TabIndex = 11;
            this.bttLoai.Text = "Loại";
            this.bttLoai.Click += new System.EventHandler(this.bttLoai_Click);
            // 
            // btnChonPhong
            // 
            this.btnChonPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonPhong.Appearance.Options.UseFont = true;
            this.btnChonPhong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonPhong.Image = global::KP_RES.Properties.Resources.large_icons_26;
            this.btnChonPhong.Location = new System.Drawing.Point(231, 0);
            this.btnChonPhong.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonPhong.Name = "btnChonPhong";
            this.btnChonPhong.Size = new System.Drawing.Size(115, 35);
            this.btnChonPhong.TabIndex = 10;
            this.btnChonPhong.Text = "Phòng";
            this.btnChonPhong.Click += new System.EventHandler(this.btnChonPhong_Click);
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btnCapnhat);
            this.pal_BUTTON.Controls.Add(this.btnBanphim);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(2, 578);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(346, 35);
            this.pal_BUTTON.TabIndex = 2;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(0, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(173, 35);
            this.btnCapnhat.TabIndex = 33;
            this.btnCapnhat.Click += new System.EventHandler(this.bnt_capnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanphim.Appearance.Options.UseBackColor = true;
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseForeColor = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(173, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(173, 35);
            this.btnBanphim.TabIndex = 32;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // Frm_Nhom_Loai_Phong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 615);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Nhom_Loai_Phong";
            this.Text = "Nhóm - Loại - Phòng";
            this.Load += new System.EventHandler(this.Frm_Nhom_Loai_Phong_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Nhom_Loai_Phong_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Nhom_Loai_Phong_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).EndInit();
            this.pal_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).EndInit();
            this.tab_Option.ResumeLayout(false);
            this.tab_Phong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            this.panel_Sandard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTinhtrang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_hinhnen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phong.Properties)).EndInit();
            this.tab_Nhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picKhuvuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ghichu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Nhom.Properties)).EndInit();
            this.Tab_Loai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenloai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhichuloai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pal_1;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panel;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.SimpleButton bnt_Xuong;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txt_Nhom;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt_ghichu;
        private DevExpress.XtraEditors.CheckEdit chkSudungban;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt_Phong;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.SimpleButton btnLuuKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnXoaKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnThemKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnChonNhom;
        private DevExpress.XtraEditors.SimpleButton btnChonPhong;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.SimpleButton btnSuaKhuvuc;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtSTT;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboNhom;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox pic_hinhnen;
        private DevExpress.XtraEditors.SimpleButton bnt_suaban;
        private DevExpress.XtraEditors.SimpleButton bnt_xoaban;
        private DevExpress.XtraEditors.SimpleButton bnt_luuban;
        private DevExpress.XtraEditors.SimpleButton bnt_themban;
        private System.Windows.Forms.FlowLayoutPanel pal_Nhom;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private System.Windows.Forms.PictureBox picKhuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraTab.XtraTabControl tab_Option;
        private DevExpress.XtraTab.XtraTabPage tab_Phong;
        private DevExpress.XtraTab.XtraTabPage tab_Nhom;
        private DevExpress.XtraEditors.SimpleButton bttLoai;
        private DevExpress.XtraTab.XtraTabPage Tab_Loai;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit cboLoai;
        private System.Windows.Forms.FlowLayoutPanel palPhong;
        private DevExpress.XtraEditors.SimpleButton btnSuaLoai;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit chkSudungLoai;
        private DevExpress.XtraEditors.TextEdit txtGhichuloai;
        private DevExpress.XtraEditors.SimpleButton btnThemLoai;
        private DevExpress.XtraEditors.TextEdit txtTenloai;
        private DevExpress.XtraEditors.SimpleButton btnLuuLoai;
        private DevExpress.XtraEditors.SimpleButton btnXoaLoai;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_LOAI;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_LOAI;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSD;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LookUpEdit cboTinhtrang;




    }
}