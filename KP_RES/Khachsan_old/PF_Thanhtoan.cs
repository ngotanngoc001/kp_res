﻿using System;
using System.Data;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;
using DevExpress.XtraEditors;

namespace KP_RES
{
    public partial class PF_Thanhtoan : DevExpress.XtraEditors.XtraForm
    {
        public string  MaLICHCHIEU;
        public bool bInve=false ;
        bool bFirst = true;
        public DataTable dtVe ;
        public int  iTongtien = 0;

        public PF_Thanhtoan()
        {
            InitializeComponent();
            cls_KP_RES.bThanhtoan = true;
            txtTienve.EditValue = 0;
            txtTienKhachTra.EditValue = 0;
            txtTienquydoi.EditValue = 0;
            txtTienThoiLai.EditValue = 0;
            LoadTiente();
        }

        private void PF_Thanhtoan_Shown(object sender, EventArgs e)
        {
            txtTienve.EditValue = iTongtien;
            txtTienKhachTra.EditValue = iTongtien;
        }

        private void PF_Thanhtoan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnDong_Click(null, null);
            }
        }

        private void PF_Thanhtoan_FormClosing(object sender, FormClosingEventArgs e)
        {
            cls_KP_RES.bThanhtoan = false;
        }

        private void txtTienve_EditValueChanged(object sender, EventArgs e)
        {
            txtTienThoiLai.EditValue = int.Parse (txtTienKhachTra.EditValue.ToString ()) - int.Parse (txtTienve.EditValue.ToString ());
        }

        private void txtTienKhachTra_EditValueChanged(object sender, EventArgs e)
        {
            txtTienThoiLai.EditValue = int.Parse (txtTienKhachTra.EditValue.ToString ()) - int.Parse (txtTienve.EditValue.ToString ());
            if (pnlQuydoi.Visible)
            {
                txtTienquydoi.EditValue = int.Parse (txtTienKhachTra.EditValue.ToString ()) * int.Parse(grvtiente.GetFocusedRowCellValue("TYGIA").ToString());
            }
        }

        private void txtTienquydoi_EditValueChanged(object sender, EventArgs e)
        {
            txtTienThoiLai.EditValue = int.Parse (txtTienquydoi.EditValue.ToString ()) - int.Parse (txtTienve.EditValue.ToString ());
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnNhaplaitienkhachtra_Click(object sender, EventArgs e)
        {
            txtTienve.EditValue = iTongtien ;
            txtTienKhachTra.EditValue = 0;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtMakhachhang.Text = txtMakhachhang.Text + btn.Text;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            txtMakhachhang.Text = "";
        }

        private void btn5t_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 500;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 500;
            }
        }

        private void btn1k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 1000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 1000;
            }
        }

        private void btn2k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 2000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 2000;
            }
        }

        private void btn5k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 5000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 5000;
            }
        }

        private void btn10k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 10000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 10000;
            }
        }

        private void btn20k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 20000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 20000;
            }
        }

        private void btn50k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 50000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 50000;
            }
        }

        private void btn100k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 100000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 100000;
            }
        }

        private void btn200k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 200000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 200000;
            }
        }

        private void btn500k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 500000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 500000;
            }
        }

        private void btnKHTT_Click(object sender, EventArgs e)
        {
            if (btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
            {
                cls_KP_RES.SetBackgroudOnClick(btnKHTT);
                pnlMakhachhang.Visible = true;
                txtMakhachhang.Focus();
            }
            else
            {
                cls_KP_RES.SetDefautlBackgroud(btnKHTT);
                if (btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
                {
                    pnlMakhachhang.Visible = false;
                    txtMakhachhang.Text = "";
                }
            }
        }

        private void btnTTTNB_Click(object sender, EventArgs e)
        {
            if (btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
            {
                cls_KP_RES.SetBackgroudOnClick(btnTTTNB);
                pnlMakhachhang.Visible = true;
                txtMakhachhang.Focus();
            }
            else
            {
                cls_KP_RES.SetDefautlBackgroud(btnTTTNB);
                if (btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
                {
                    pnlMakhachhang.Visible = false;
                    txtMakhachhang.Text = "";
                }
            }
        }

        private void btnLentiente_Click(object sender, EventArgs e)
        {
            try
            {
                grvtiente.Focus();
                grvtiente.FocusedRowHandle = grvtiente.FocusedRowHandle - 1;
                btnLentiente.Enabled = true;
                btnXuongtiente.Enabled = true;
                if (grvtiente.FocusedRowHandle == 0)
                {
                    btnLentiente.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongtiente_Click(object sender, EventArgs e)
        {
            try
            {
                grvtiente.Focus();
                grvtiente.FocusedRowHandle = grvtiente.FocusedRowHandle + 1;
                btnXuongtiente.Enabled = true;
                btnLentiente.Enabled = true;
                if (grvtiente.FocusedRowHandle == grvtiente.RowCount - 1)
                {
                    btnXuongtiente.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void grvtiente_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STTTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void grvtiente_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            pnlQuydoi.Visible = true;
            lblTienquydoi.Text = "   Quy đổi \n" + grvtiente.GetFocusedRowCellValue("MA_TIENTE").ToString() + " -> VNĐ";
            txtTienquydoi.EditValue = (int)txtTienKhachTra.EditValue * int.Parse(grvtiente.GetFocusedRowCellValue("TYGIA").ToString());
        }

        private void LoadTiente()
        {
            string sSQL = "";
            sSQL += "Select MA_TIENTE,TEN_TIENTE,TYGIA" + "\n";
            sSQL += "From TIENTE" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by STT,TEN_TIENTE" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            grdTiente.DataSource = dt;

            sSQL = "";
            sSQL += "Select MAPHUPHI,TENPHUPHI,GIATRI,GHICHU,SUDUNG" + "\n";
            sSQL += "From PHUPHI" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by TENPHUPHI" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            grdPhuthu.DataSource = dt;
        }

        private void btnLenphuthu_Click(object sender, EventArgs e)
        {
            try
            {
                grvPhuthu.Focus();
                grvPhuthu.FocusedRowHandle = grvPhuthu.FocusedRowHandle - 1;
                btnLenphuthu.Enabled = true;
                btnXuongphuthu.Enabled = true;
                if (grvPhuthu.FocusedRowHandle == 0)
                {
                    btnLenphuthu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongphuthu_Click(object sender, EventArgs e)
        {
            try
            {
                grvPhuthu.Focus();
                grvPhuthu.FocusedRowHandle = grvPhuthu.FocusedRowHandle + 1;
                btnXuongphuthu.Enabled = true;
                btnLenphuthu.Enabled = true;
                if (grvPhuthu.FocusedRowHandle == grvPhuthu.RowCount - 1)
                {
                    btnXuongphuthu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void grvPhuthu_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STT12)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        bool bPhuphi = false;
        private void grvPhuthu_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (bPhuphi)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Cảnh báo đã chọn phụ phí rồi.Bạn muốn tiếp tục tính thêm phụ phí ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            else
            {
                bPhuphi = true;
            }
            iTongtien =0;
            int iPhuthu = int.Parse(grvPhuthu.GetFocusedRowCellValue("GIATRI").ToString());
            foreach (DataRow dr in dtVe.Rows)
            {
                dr["PHUTHU"] = int.Parse ( dr["PHUTHU"].ToString ()) + iPhuthu ;
               
            }
            foreach (DataRow dr in dtVe.Rows)
            {
                iTongtien = iTongtien + int.Parse(dr["PHUTHU"].ToString());
            }
            txtTienve.EditValue = iTongtien;
            txtTienKhachTra.EditValue = iTongtien;

            cls_KP_RES.dtDanhSachVe = dtVe;
        }

        bool runkhtt = true;
        bool runttnb = true;
        private void btnInve_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.Tag.ToString() == "0")
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận thanh toán và không in vé?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            if ((int)txtTienThoiLai.EditValue < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa thanh toán hết không thể in vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (txtMakhachhang.Text != "")
            {
                if (!btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
                {
                    if (clsUserManagement.CheckActive("4"))
                    {
                        if (!NgocSang.CheckKhachHangThanThiet(txtMakhachhang.Text))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Mã khách hàng không đúng. Kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtMakhachhang.Focus();
                            return;
                        }
                        else
                        {
                            if (runkhtt)
                            {
                                NgocSang.CongDiemKhachHangThanThiet(txtMakhachhang.Text, decimal.Parse(txtTienve.EditValue.ToString()));//KHTT
                                runkhtt = false;
                            }
                        }
                    }
                }
                if (!btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
                {
                    if (clsUserManagement.CheckActive("8"))
                    {
                        if (!NgocSang.CheckThanhToanNoiBo(txtMakhachhang.Text))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Mã khách hàng không đúng. Kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtMakhachhang.Focus();
                            return;
                        }
                        else
                        {
                            if (runttnb)
                            {
                                decimal TienTT = NgocSang.ThanhToanNoiBo(txtMakhachhang.Text, decimal.Parse(txtTienve.EditValue.ToString()));
                                runttnb = false;
                                if (TienTT != decimal.Parse(txtTienve.EditValue.ToString()))
                                {
                                    txtTienKhachTra.EditValue = int.Parse(TienTT.ToString());
                                    bFirst = false;
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            btnInve.Enabled = false;
            btnInve1.Enabled = false;
            foreach (DataRow dr in dtVe.Rows)
            {
                string sSQL = "";
                sSQL = "";
                sSQL += "Select A.MAVE,A.MABARCODE,A.MAGHE,A.SOGHE,C.MALOAIGHE,C.TENLOAIGHE,A.MALOAIVE,(Select TENLOAIVE From DM_LOAIVE Where MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + ") As TENLOAIVE,A.DONGIA,D.MALICHCHIEU,D.NGAYCHIEU,SUBSTRING(D.BATDAU,1,5) As BATDAU,E.MAPHONG,E.TENPHONG,F.MAPHIM,F.TENPHIM,D.MACHATLUONGPHIM,G.TENCHATLUONGPHIM,A.NGUOITAO As NGUOIBAN,H.TENNHANVIEN AS NGUOITAO,A.NGAYTAO" + "\n";
                sSQL += "From VE A , DM_GHE B , DM_LOAIGHE C , LICHCHIEU D , DM_PHONG E,DM_PHIM F,DM_CHATLUONGPHIM G,DM_NHANVIEN H,SYS_USER I" + "\n";
                sSQL += "Where A.MAGHE = B.MAGHE" + "\n";
                sSQL += "And B.MALOAIGHE = C.MALOAIGHE" + "\n";
                sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
                sSQL += "And D.MAPHONG = E.MAPHONG" + "\n";
                sSQL += "And D.MAPHIM = F.MAPHIM" + "\n";
                sSQL += "And D.MACHATLUONGPHIM = G.MACHATLUONGPHIM" + "\n";
                sSQL += "And A.NGUOITAO = I.UserID" + "\n";
                sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
                sSQL += "And A.MAVE =" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                dt1.Rows[0]["MALOAIVE"] = dr["MALOAIVE"];
                dt1.Rows[0]["DONGIA"] = dr["PHUTHU"];
                //16/5/2015 bổ sung recheck trùng vé trước khi đưa vào vé tổng hợp
                sSQL = "";
                sSQL += "Select MAVETONGHOP " + "\n";
                sSQL += "From VETONGHOP " + "\n";
                sSQL += "Where MALICHCHIEU = " + clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + "\n";
                sSQL += "And SOGHE = " + clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + "\n";
                DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                if (dt2.Rows.Count % 2 != 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Vé đã xuất.Vui lòng chọn ghế khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                }
                //21/09/2015 bổ sung sửa lại lổi nhãy mã vé
                string sMabarcode_Final = cls_KP_RES.CreateBarcode_Final("VE");
                dt1.Rows[0]["MABARCODE"] = sMabarcode_Final;
                sSQL = "";
                sSQL += "Update VE Set " + "\n";
                sSQL += "MABARCODE=" + clsMain.SQLString(sMabarcode_Final) + "," + "\n";
                sSQL += "MALOAIVE=" + clsMain.SQLString(dr["MALOAIVE"].ToString()) + "," + "\n";
                sSQL += "DONGIA=" + clsMain.SQLString(dr["PHUTHU"].ToString()) + "," + "\n";
                sSQL += "PHUTHU=" + clsMain.SQLString("0") + "," + "\n";
                sSQL += "DATHANHTOAN=" + clsMain.SQLString(dr["PHUTHU"].ToString()) + "," + "\n";
                sSQL += "CONLAI=" + clsMain.SQLString("0") + "\n";
                sSQL += "Where MAVE=" + clsMain.SQLString(dr["MAVE"].ToString()) + "\n";
                //25/2/2014 bổ sung ghi vào sổ bán vé tổng hợp để báo cáo tiền cho chính xác
                sSQL += "Insert into VETONGHOP (LOAIVE,NGUOIBAN,NGAYBAN,MABARCODE,MALICHCHIEU,SOGHE,MAPHONG,MAPHIM,MALOAIGHE,MALOAIVE,DONGIA,MAQUAY,MA_KHACHHANG,NGUOITAO,NGAYTAO)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode("Vé bán") + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["NGUOIBAN"].ToString()) + ",";
                sSQL += clsMain.SQLString(((DateTime)dt1.Rows[0]["NGAYTAO"]).ToString("dd/MM/yyyy HH : mm")) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MABARCODE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALICHCHIEU"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["SOGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHONG"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MAPHIM"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIGHE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["MALOAIVE"].ToString()) + ",";
                sSQL += clsMain.SQLString(dt1.Rows[0]["DONGIA"].ToString()) + ",";
                sSQL += clsMain.SQLString(ConfigCashier.idCashier) + ",";
                sSQL += clsMain.SQLString(txtMakhachhang.Text) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                sSQL += "GETDATE()" + ")";

                Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                if (bRunSQL)
                {
                    if (btn.Tag.ToString() == "1")
                    {
                        System.Threading.Thread.Sleep(2000);
                        Frm_Report1 frm = new Frm_Report1();
                        frm.ReportName = "rptVerapphim";
                        frm.DataSource = dt1;
                        frm.IsPrint = true;
                        frm.PrinterName = clsKP_Terminal.Mayinve;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.ExportName = "In ve";
                        frm.ShowDialog(this);
                        frm.Dispose();
                    }
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Lưu vé tổng hợp không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            bInve = true;
            this.Close();
        }
    }
}