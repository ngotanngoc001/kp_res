﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Lichsu_Diem : DevExpress.XtraEditors.XtraForm
    {

        public Frm_Lichsu_Diem( string maKH)
        {
            InitializeComponent();
            LoadCombo();
            LoadDuLieu(maKH);
            LoaddataGridView();
        }

        private void LoadCombo()
        {
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
            dt1.Rows.Add("1", "Nam");
            dt1.Rows.Add("0", "Nữ");
            cboGioitinh.Properties.DataSource = dt1;
            cboGioitinh.EditValue = null;
        }

        private void LoadDuLieu(string makhachhang)
        {
            DataTable dt = cls_KHTT.ReturnKhachHangThanThiet(makhachhang);
            if (dt.Rows.Count > 0)
            {
                txtMaKH.Text = dt.Rows[0]["MA"].ToString();
                txtTEN.Text = dt.Rows[0]["TEN"].ToString();
                txtDIACHI.Text = dt.Rows[0]["DIACHI"].ToString();
                txtDIENTHOAI.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                txtEMAIL.Text = dt.Rows[0]["EMAIL"].ToString();
                txtWebsite.Text = dt.Rows[0]["WEBSITE"].ToString();
                chkSUDUNG.Checked = bool.Parse(dt.Rows[0]["SUDUNG"].ToString());
                txtCMND.Text = dt.Rows[0]["CMND"].ToString();
                txtSodiem.Text = dt.Rows[0]["Sodiem"].ToString();
                txtSodiemDoiqua.Text = dt.Rows[0]["SoDiemHuuDung"].ToString();
                dtpNgaysinh.EditValue = DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                cboGioitinh.EditValue = bool.Parse(dt.Rows[0]["GIOITINH"].ToString()) == true ? "1" : "0";
            }
            else
            {
                XtraMessageBox.Show("Không tìm thấy!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoaddataGridView()
        {
            string sSQL = "select MAKH, TENNHANVIEN,SODIEM,(case MALOAI when '01' then N'Cộng Điểm' else N'Trừ Điểm' end)as TENLOAI ,NGAYCONG,LYDO from KHTT_CONG_TRU_DIEM cd inner join DM_NHANVIEN nv on cd.MANV=nv.MANHANVIEN where MAKH='" + txtMaKH.Text + "'";
            gvLichSuDiem.DataSource = clsMain.ReturnDataTable(sSQL);

            sSQL ="";
            sSQL += "Select TEN_QT,B.SODIEM,TENNHANVIEN,NGAYDOI,B.SOLUONG" + "\n";
            sSQL += "From KHTT_DIEMTHUONG_QUATANG A,KHTT_LICHSUDOIQUA B,DM_NHANVIEN C" + "\n";
            sSQL += "Where A.MA_QT=B.MA_QT And C.MANHANVIEN=B.MANV And B.MA_KHTT='" + txtMaKH.Text + "'";
            gridControl1.DataSource = clsMain.ReturnDataTable(sSQL);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.RowCount > 0)
                {
                    DataTable dtkh = new DataTable();
                    String sSQL = "Select A.MA,A.TEN,A.CMND,A.NGAYSINH,A.GIOITINH,case A.GIOITINH when 0 then N'Nữ' else N'Nam' end as GIOITINH1,A.DIACHI,A.DIENTHOAI,A.EMAIL,A.WEBSITE,A.NGAYTAO,A.SUDUNG, B.Ma_KHTT,B.SoDiem AS TongDiem,B.SoLe,B.SoLanTinh,B.SoLanChoThuong,B.MACAPDO,C.TENCAPDO,C.CHIETKHAU,D.SoDuTaiKhoan,E.MAKH, F.TENNHANVIEN,E.SODIEM,(case E.MALOAI when '01' then N'Cộng Điểm' else N'Trừ Điểm' end)as TENLOAI ,E.NGAYCONG,E.LYDO" + "\n";
                    sSQL += "from KHTT A ,KHACHHANG_THANTHIET B , KHTT_CAPDO C,KHACHHANG_THANHTOAN_NOIBO D,KHTT_CONG_TRU_DIEM E,DM_NHANVIEN F" + "\n";
                    sSQL += "WHERE A.MA =  B.Ma_KHTT" + "\n";
                    sSQL += "AND A.MA =  D.Ma_KHTT" + "\n";
                    sSQL += "AND B.MACAPDO = C.MACAPDO" + "\n";
                    sSQL += "AND A.MA = E.MAKH" + "\n";
                    sSQL += "AND E.MANV = F.MANHANVIEN" + "\n";
                    sSQL += "AND A.MA = " + clsMain.SQLString(txtMaKH.Text) + "\n";
                    dtkh = clsMain.ReturnDataTable(sSQL);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dtkh;
                    frm.Mode = 62;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }

        private void gridView1_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnLennhieu1_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLenit1_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit1_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu1_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXemtruockhiin1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataTable dtkh = new DataTable();
                    String sSQL = "";
                    sSQL += "Select A.MA,A.TEN,A.CMND,A.NGAYSINH,A.GIOITINH,case A.GIOITINH when 0 then N'Nữ' else N'Nam' end as GIOITINH1,";
                    sSQL += "A.DIACHI,A.DIENTHOAI,A.EMAIL,A.WEBSITE,A.NGAYTAO,A.SUDUNG, B.Ma_KHTT,B.SoDiem AS TongDiem,B.SoLe,B.SoLanTinh,";
                    sSQL += "E.TENNHANVIEN,C.SODIEM,C.SOLUONG,D.TEN_QT,C.NGAYDOI" + "\n";
                    sSQL += "From KHTT A ,KHACHHANG_THANTHIET B ,KHTT_LICHSUDOIQUA C,KHTT_DIEMTHUONG_QUATANG D,DM_NHANVIEN E" + "\n";
                    sSQL += "WHERE A.MA =  B.Ma_KHTT" + "\n";
                    sSQL += "AND B.Ma_KHTT =  C.Ma_KHTT AND D.MA_QT=C.MA_QT" + "\n";
                    sSQL += "AND C.MANV = E.MANHANVIEN" + "\n";
                    sSQL += "AND A.MA = " + clsMain.SQLString(txtMaKH.Text) + "\n";
                    dtkh = clsMain.ReturnDataTable(sSQL);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dtkh;
                    frm.Mode = 63;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
            }
        }



    }
}