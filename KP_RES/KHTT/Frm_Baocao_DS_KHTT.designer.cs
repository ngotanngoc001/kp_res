﻿namespace KP_RES
{
    partial class Frm_Baocao_DS_KHTT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Baocao_DS_KHTT));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.pnChoose = new DevExpress.XtraEditors.PanelControl();
            this.lbGT = new DevExpress.XtraEditors.LabelControl();
            this.chkNam = new DevExpress.XtraEditors.CheckEdit();
            this.chkNu = new DevExpress.XtraEditors.CheckEdit();
            this.date_tungay = new System.Windows.Forms.DateTimePicker();
            this.date_denngay = new System.Windows.Forms.DateTimePicker();
            this.Look_data = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNgayTu = new DevExpress.XtraEditors.LabelControl();
            this.lbl_title = new DevExpress.XtraEditors.LabelControl();
            this.lbNgayDen = new DevExpress.XtraEditors.LabelControl();
            this.txtDiemDen = new DevExpress.XtraEditors.TextEdit();
            this.lbTuDiem = new DevExpress.XtraEditors.LabelControl();
            this.txtDiemTu = new DevExpress.XtraEditors.TextEdit();
            this.lbDenDiem = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.btnChitiet = new DevExpress.XtraEditors.SimpleButton();
            this.gvcShowKHTT = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoDiem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENCAPDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WEBSITE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayKichHoat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.nhanvien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ngaytao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLANPHIMTHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHGIOITHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LoaiThanhVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TTHONNHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayThamGia = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).BeginInit();
            this.pnChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Look_data.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(223, 674);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.pnChoose);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 228);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(219, 409);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 80);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 327);
            this.panelControl8.TabIndex = 21;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 47);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(130, 4);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // pnChoose
            // 
            this.pnChoose.Controls.Add(this.lbGT);
            this.pnChoose.Controls.Add(this.chkNam);
            this.pnChoose.Controls.Add(this.chkNu);
            this.pnChoose.Controls.Add(this.date_tungay);
            this.pnChoose.Controls.Add(this.date_denngay);
            this.pnChoose.Controls.Add(this.Look_data);
            this.pnChoose.Controls.Add(this.lbNgayTu);
            this.pnChoose.Controls.Add(this.lbl_title);
            this.pnChoose.Controls.Add(this.lbNgayDen);
            this.pnChoose.Controls.Add(this.txtDiemDen);
            this.pnChoose.Controls.Add(this.lbTuDiem);
            this.pnChoose.Controls.Add(this.txtDiemTu);
            this.pnChoose.Controls.Add(this.lbDenDiem);
            this.pnChoose.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChoose.Location = new System.Drawing.Point(2, 2);
            this.pnChoose.Name = "pnChoose";
            this.pnChoose.Size = new System.Drawing.Size(215, 78);
            this.pnChoose.TabIndex = 20;
            this.pnChoose.Visible = false;
            // 
            // lbGT
            // 
            this.lbGT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbGT.Location = new System.Drawing.Point(8, 8);
            this.lbGT.Name = "lbGT";
            this.lbGT.Size = new System.Drawing.Size(113, 19);
            this.lbGT.TabIndex = 20;
            this.lbGT.Text = "Chọn Giới Tính:";
            this.lbGT.Visible = false;
            // 
            // chkNam
            // 
            this.chkNam.EditValue = true;
            this.chkNam.Location = new System.Drawing.Point(9, 40);
            this.chkNam.Name = "chkNam";
            this.chkNam.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkNam.Properties.Appearance.Options.UseFont = true;
            this.chkNam.Properties.Caption = "Nam";
            this.chkNam.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkNam.Properties.RadioGroupIndex = 1;
            this.chkNam.Size = new System.Drawing.Size(75, 24);
            this.chkNam.TabIndex = 18;
            this.chkNam.Visible = false;
            this.chkNam.CheckedChanged += new System.EventHandler(this.chkNam_CheckedChanged);
            // 
            // chkNu
            // 
            this.chkNu.Location = new System.Drawing.Point(115, 42);
            this.chkNu.Name = "chkNu";
            this.chkNu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkNu.Properties.Appearance.Options.UseFont = true;
            this.chkNu.Properties.Caption = "Nữ";
            this.chkNu.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkNu.Properties.RadioGroupIndex = 1;
            this.chkNu.Size = new System.Drawing.Size(75, 24);
            this.chkNu.TabIndex = 19;
            this.chkNu.TabStop = false;
            this.chkNu.Visible = false;
            this.chkNu.CheckedChanged += new System.EventHandler(this.chkNu_CheckedChanged);
            // 
            // date_tungay
            // 
            this.date_tungay.CalendarFont = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.CustomFormat = "dd/MM/yyyy";
            this.date_tungay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_tungay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_tungay.Location = new System.Drawing.Point(90, 3);
            this.date_tungay.Name = "date_tungay";
            this.date_tungay.Size = new System.Drawing.Size(126, 27);
            this.date_tungay.TabIndex = 8;
            this.date_tungay.Visible = false;
            // 
            // date_denngay
            // 
            this.date_denngay.CustomFormat = "dd/MM/yyyy";
            this.date_denngay.Font = new System.Drawing.Font("Tahoma", 12F);
            this.date_denngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_denngay.Location = new System.Drawing.Point(90, 39);
            this.date_denngay.Name = "date_denngay";
            this.date_denngay.Size = new System.Drawing.Size(126, 27);
            this.date_denngay.TabIndex = 9;
            this.date_denngay.Visible = false;
            // 
            // Look_data
            // 
            this.Look_data.Location = new System.Drawing.Point(4, 36);
            this.Look_data.Name = "Look_data";
            this.Look_data.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.Look_data.Properties.Appearance.Options.UseFont = true;
            this.Look_data.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.Look_data.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Look_data.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.Look_data.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Look_data.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Look_data.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.Look_data.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            this.Look_data.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Look_data.Properties.DisplayMember = "TENCAPDO";
            this.Look_data.Properties.DropDownItemHeight = 40;
            this.Look_data.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.Look_data.Properties.NullText = "";
            this.Look_data.Properties.ShowFooter = false;
            this.Look_data.Properties.ShowHeader = false;
            this.Look_data.Properties.ValueMember = "MACAPDO";
            this.Look_data.Size = new System.Drawing.Size(208, 26);
            this.Look_data.TabIndex = 17;
            this.Look_data.Visible = false;
            // 
            // lbNgayTu
            // 
            this.lbNgayTu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayTu.Location = new System.Drawing.Point(4, 8);
            this.lbNgayTu.Name = "lbNgayTu";
            this.lbNgayTu.Size = new System.Drawing.Size(72, 19);
            this.lbNgayTu.TabIndex = 10;
            this.lbNgayTu.Text = "Từ Ngày :";
            this.lbNgayTu.Visible = false;
            // 
            // lbl_title
            // 
            this.lbl_title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbl_title.Location = new System.Drawing.Point(4, 7);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(84, 19);
            this.lbl_title.TabIndex = 16;
            this.lbl_title.Text = "Tên Cấp Độ";
            this.lbl_title.Visible = false;
            // 
            // lbNgayDen
            // 
            this.lbNgayDen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNgayDen.Location = new System.Drawing.Point(4, 40);
            this.lbNgayDen.Name = "lbNgayDen";
            this.lbNgayDen.Size = new System.Drawing.Size(80, 19);
            this.lbNgayDen.TabIndex = 11;
            this.lbNgayDen.Text = "Đến Ngày :";
            this.lbNgayDen.Visible = false;
            // 
            // txtDiemDen
            // 
            this.txtDiemDen.Location = new System.Drawing.Point(90, 38);
            this.txtDiemDen.Name = "txtDiemDen";
            this.txtDiemDen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDiemDen.Properties.Appearance.Options.UseFont = true;
            this.txtDiemDen.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtDiemDen.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtDiemDen.Properties.Mask.EditMask = "N0";
            this.txtDiemDen.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiemDen.Size = new System.Drawing.Size(123, 26);
            this.txtDiemDen.TabIndex = 15;
            this.txtDiemDen.Visible = false;
            // 
            // lbTuDiem
            // 
            this.lbTuDiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbTuDiem.Location = new System.Drawing.Point(4, 7);
            this.lbTuDiem.Name = "lbTuDiem";
            this.lbTuDiem.Size = new System.Drawing.Size(68, 19);
            this.lbTuDiem.TabIndex = 12;
            this.lbTuDiem.Text = "Từ Điểm:";
            this.lbTuDiem.Visible = false;
            // 
            // txtDiemTu
            // 
            this.txtDiemTu.Location = new System.Drawing.Point(90, 4);
            this.txtDiemTu.Name = "txtDiemTu";
            this.txtDiemTu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDiemTu.Properties.Appearance.Options.UseFont = true;
            this.txtDiemTu.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtDiemTu.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtDiemTu.Properties.Mask.EditMask = "N0";
            this.txtDiemTu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiemTu.Size = new System.Drawing.Size(123, 26);
            this.txtDiemTu.TabIndex = 14;
            this.txtDiemTu.Visible = false;
            // 
            // lbDenDiem
            // 
            this.lbDenDiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbDenDiem.Location = new System.Drawing.Point(4, 41);
            this.lbDenDiem.Name = "lbDenDiem";
            this.lbDenDiem.Size = new System.Drawing.Size(76, 19);
            this.lbDenDiem.TabIndex = 13;
            this.lbDenDiem.Text = "Đến Điểm:";
            this.lbDenDiem.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(219, 226);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = "TC";
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TC", "Tất cả"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CD", "Theo cấp độ"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("DTL", "Theo điểm tích lũy"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Ngay", "Theo ngày tham gia"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("GT", "Theo giới tính"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("HUY", "Theo danh sách hủy")});
            this.optGroup.Size = new System.Drawing.Size(215, 220);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 637);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(219, 35);
            this.btnThunho.TabIndex = 10;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnXuatfile);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Controls.Add(this.btnChitiet);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1325, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 674);
            this.panelControl2.TabIndex = 8;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 317);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 275);
            this.panelControl3.TabIndex = 14;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 115);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 195);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 163);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 112);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 83);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 592);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 237);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 16;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(2, 157);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 14;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 77);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 17;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // btnChitiet
            // 
            this.btnChitiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChitiet.Appearance.Options.UseFont = true;
            this.btnChitiet.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnChitiet.Image = global::KP_RES.Properties.Resources.View_Details_26;
            this.btnChitiet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnChitiet.Location = new System.Drawing.Point(2, 2);
            this.btnChitiet.Margin = new System.Windows.Forms.Padding(4);
            this.btnChitiet.Name = "btnChitiet";
            this.btnChitiet.Size = new System.Drawing.Size(35, 75);
            this.btnChitiet.TabIndex = 18;
            this.btnChitiet.Click += new System.EventHandler(this.btn_lichSu_Click);
            // 
            // gvcShowKHTT
            // 
            this.gvcShowKHTT.AllowDrop = true;
            this.gvcShowKHTT.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gvcShowKHTT.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gvcShowKHTT.Location = new System.Drawing.Point(223, 0);
            this.gvcShowKHTT.MainView = this.gridView1;
            this.gvcShowKHTT.Name = "gvcShowKHTT";
            this.gvcShowKHTT.Size = new System.Drawing.Size(1102, 674);
            this.gvcShowKHTT.TabIndex = 50;
            this.gvcShowKHTT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAKHTT,
            this.TEN,
            this.SoDiem,
            this.TENCAPDO,
            this.CMND,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.DIENTHOAI,
            this.EMAIL,
            this.WEBSITE,
            this.NgayKichHoat,
            this.SUDUNG,
            this.nhanvien,
            this.ngaytao,
            this.fill,
            this.NGAYCMND,
            this.SOLANPHIMTHANG,
            this.MAKHGIOITHIEU,
            this.LoaiThanhVien,
            this.LOAI,
            this.TTHONNHAN,
            this.NgayThamGia});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gvcShowKHTT;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MAKHTT
            // 
            this.MAKHTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAKHTT.AppearanceHeader.Options.UseFont = true;
            this.MAKHTT.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHTT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MAKHTT.Caption = "Mã";
            this.MAKHTT.FieldName = "MA";
            this.MAKHTT.Name = "MAKHTT";
            this.MAKHTT.OptionsColumn.AllowEdit = false;
            this.MAKHTT.OptionsColumn.AllowFocus = false;
            this.MAKHTT.OptionsColumn.FixedWidth = true;
            this.MAKHTT.Visible = true;
            this.MAKHTT.VisibleIndex = 1;
            this.MAKHTT.Width = 110;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 200;
            // 
            // SoDiem
            // 
            this.SoDiem.AppearanceCell.Options.UseTextOptions = true;
            this.SoDiem.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SoDiem.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SoDiem.AppearanceHeader.Options.UseFont = true;
            this.SoDiem.AppearanceHeader.Options.UseTextOptions = true;
            this.SoDiem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoDiem.Caption = "Số điểm";
            this.SoDiem.FieldName = "SoDiem";
            this.SoDiem.Name = "SoDiem";
            this.SoDiem.OptionsColumn.AllowEdit = false;
            this.SoDiem.OptionsColumn.AllowFocus = false;
            this.SoDiem.OptionsColumn.FixedWidth = true;
            this.SoDiem.Visible = true;
            this.SoDiem.VisibleIndex = 3;
            this.SoDiem.Width = 100;
            // 
            // TENCAPDO
            // 
            this.TENCAPDO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENCAPDO.AppearanceHeader.Options.UseFont = true;
            this.TENCAPDO.AppearanceHeader.Options.UseTextOptions = true;
            this.TENCAPDO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENCAPDO.Caption = "Cấp độ";
            this.TENCAPDO.FieldName = "TENCAPDO";
            this.TENCAPDO.Name = "TENCAPDO";
            this.TENCAPDO.OptionsColumn.AllowEdit = false;
            this.TENCAPDO.OptionsColumn.AllowFocus = false;
            this.TENCAPDO.OptionsColumn.FixedWidth = true;
            this.TENCAPDO.Visible = true;
            this.TENCAPDO.VisibleIndex = 4;
            this.TENCAPDO.Width = 150;
            // 
            // CMND
            // 
            this.CMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMND.AppearanceHeader.Options.UseFont = true;
            this.CMND.AppearanceHeader.Options.UseTextOptions = true;
            this.CMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.Caption = "CMND";
            this.CMND.FieldName = "CMND";
            this.CMND.Name = "CMND";
            this.CMND.OptionsColumn.AllowEdit = false;
            this.CMND.OptionsColumn.AllowFocus = false;
            this.CMND.OptionsColumn.FixedWidth = true;
            this.CMND.Visible = true;
            this.CMND.VisibleIndex = 5;
            this.CMND.Width = 120;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 6;
            this.NGAYSINH.Width = 110;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH1";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 7;
            this.GIOITINH.Width = 90;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 8;
            this.DIACHI.Width = 250;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 9;
            this.DIENTHOAI.Width = 110;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 10;
            this.EMAIL.Width = 200;
            // 
            // WEBSITE
            // 
            this.WEBSITE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WEBSITE.AppearanceHeader.Options.UseFont = true;
            this.WEBSITE.AppearanceHeader.Options.UseTextOptions = true;
            this.WEBSITE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WEBSITE.Caption = "Website";
            this.WEBSITE.FieldName = "WEBSITE";
            this.WEBSITE.Name = "WEBSITE";
            this.WEBSITE.OptionsColumn.AllowEdit = false;
            this.WEBSITE.OptionsColumn.AllowFocus = false;
            this.WEBSITE.OptionsColumn.FixedWidth = true;
            this.WEBSITE.Visible = true;
            this.WEBSITE.VisibleIndex = 11;
            this.WEBSITE.Width = 200;
            // 
            // NgayKichHoat
            // 
            this.NgayKichHoat.AppearanceCell.Options.UseTextOptions = true;
            this.NgayKichHoat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayKichHoat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NgayKichHoat.AppearanceHeader.Options.UseFont = true;
            this.NgayKichHoat.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayKichHoat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayKichHoat.Caption = "Ngày tham gia";
            this.NgayKichHoat.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayKichHoat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayKichHoat.FieldName = "NGAYTAO";
            this.NgayKichHoat.Name = "NgayKichHoat";
            this.NgayKichHoat.OptionsColumn.AllowEdit = false;
            this.NgayKichHoat.OptionsColumn.AllowFocus = false;
            this.NgayKichHoat.OptionsColumn.FixedWidth = true;
            this.NgayKichHoat.Visible = true;
            this.NgayKichHoat.VisibleIndex = 12;
            this.NgayKichHoat.Width = 140;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 13;
            this.SUDUNG.Width = 90;
            // 
            // nhanvien
            // 
            this.nhanvien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.nhanvien.AppearanceHeader.Options.UseFont = true;
            this.nhanvien.AppearanceHeader.Options.UseTextOptions = true;
            this.nhanvien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nhanvien.Caption = "Nhân viên";
            this.nhanvien.FieldName = "nhanvien";
            this.nhanvien.Name = "nhanvien";
            this.nhanvien.OptionsColumn.AllowEdit = false;
            this.nhanvien.OptionsColumn.AllowFocus = false;
            this.nhanvien.OptionsColumn.FixedWidth = true;
            this.nhanvien.Visible = true;
            this.nhanvien.VisibleIndex = 14;
            this.nhanvien.Width = 200;
            // 
            // ngaytao
            // 
            this.ngaytao.AppearanceCell.Options.UseTextOptions = true;
            this.ngaytao.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ngaytao.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ngaytao.AppearanceHeader.Options.UseFont = true;
            this.ngaytao.AppearanceHeader.Options.UseTextOptions = true;
            this.ngaytao.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ngaytao.Caption = "Ngày";
            this.ngaytao.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.ngaytao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ngaytao.FieldName = "NGAYTAO";
            this.ngaytao.Name = "ngaytao";
            this.ngaytao.OptionsColumn.AllowEdit = false;
            this.ngaytao.OptionsColumn.AllowFocus = false;
            this.ngaytao.OptionsColumn.FixedWidth = true;
            this.ngaytao.Visible = true;
            this.ngaytao.VisibleIndex = 15;
            this.ngaytao.Width = 160;
            // 
            // fill
            // 
            this.fill.Name = "fill";
            this.fill.OptionsColumn.AllowEdit = false;
            this.fill.OptionsColumn.AllowFocus = false;
            this.fill.Visible = true;
            this.fill.VisibleIndex = 16;
            this.fill.Width = 20;
            // 
            // NGAYCMND
            // 
            this.NGAYCMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCMND.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMND.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMND.Caption = "Ngày Cấp CMND";
            this.NGAYCMND.FieldName = "NGAYCMND";
            this.NGAYCMND.Name = "NGAYCMND";
            this.NGAYCMND.OptionsColumn.AllowEdit = false;
            this.NGAYCMND.OptionsColumn.AllowFocus = false;
            this.NGAYCMND.OptionsColumn.FixedWidth = true;
            this.NGAYCMND.Width = 140;
            // 
            // SOLANPHIMTHANG
            // 
            this.SOLANPHIMTHANG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLANPHIMTHANG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLANPHIMTHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLANPHIMTHANG.AppearanceHeader.Options.UseFont = true;
            this.SOLANPHIMTHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLANPHIMTHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLANPHIMTHANG.Caption = "Số Lần Xem/Tháng";
            this.SOLANPHIMTHANG.FieldName = "SOLANPHIMTHANG";
            this.SOLANPHIMTHANG.Name = "SOLANPHIMTHANG";
            this.SOLANPHIMTHANG.OptionsColumn.AllowEdit = false;
            this.SOLANPHIMTHANG.OptionsColumn.AllowFocus = false;
            this.SOLANPHIMTHANG.OptionsColumn.FixedWidth = true;
            this.SOLANPHIMTHANG.Width = 180;
            // 
            // MAKHGIOITHIEU
            // 
            this.MAKHGIOITHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.MAKHGIOITHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHGIOITHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAKHGIOITHIEU.AppearanceHeader.Options.UseFont = true;
            this.MAKHGIOITHIEU.Caption = "Mã KH Giới Thiệu";
            this.MAKHGIOITHIEU.DisplayFormat.FormatString = "00000000";
            this.MAKHGIOITHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MAKHGIOITHIEU.FieldName = "MAKHGIOITHIEU";
            this.MAKHGIOITHIEU.Name = "MAKHGIOITHIEU";
            this.MAKHGIOITHIEU.OptionsColumn.AllowEdit = false;
            this.MAKHGIOITHIEU.OptionsColumn.AllowFocus = false;
            this.MAKHGIOITHIEU.OptionsColumn.AllowMove = false;
            this.MAKHGIOITHIEU.Width = 150;
            // 
            // LoaiThanhVien
            // 
            this.LoaiThanhVien.AppearanceCell.Options.UseTextOptions = true;
            this.LoaiThanhVien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiThanhVien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LoaiThanhVien.AppearanceHeader.Options.UseFont = true;
            this.LoaiThanhVien.AppearanceHeader.Options.UseTextOptions = true;
            this.LoaiThanhVien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiThanhVien.Caption = "Loại Thành Viên";
            this.LoaiThanhVien.FieldName = "LoaiThanhVien";
            this.LoaiThanhVien.Name = "LoaiThanhVien";
            this.LoaiThanhVien.OptionsColumn.AllowEdit = false;
            this.LoaiThanhVien.OptionsColumn.AllowFocus = false;
            this.LoaiThanhVien.OptionsColumn.FixedWidth = true;
            this.LoaiThanhVien.Width = 140;
            // 
            // LOAI
            // 
            this.LOAI.Caption = "LOAI";
            this.LOAI.FieldName = "LOAI";
            this.LOAI.Name = "LOAI";
            this.LOAI.OptionsColumn.AllowEdit = false;
            this.LOAI.OptionsColumn.AllowFocus = false;
            // 
            // TTHONNHAN
            // 
            this.TTHONNHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTHONNHAN.AppearanceHeader.Options.UseFont = true;
            this.TTHONNHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TTHONNHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TTHONNHAN.Caption = "TT Hôn Nhân";
            this.TTHONNHAN.FieldName = "TTHONNHAN1";
            this.TTHONNHAN.Name = "TTHONNHAN";
            this.TTHONNHAN.OptionsColumn.AllowEdit = false;
            this.TTHONNHAN.OptionsColumn.AllowFocus = false;
            this.TTHONNHAN.OptionsColumn.FixedWidth = true;
            this.TTHONNHAN.Width = 120;
            // 
            // NgayThamGia
            // 
            this.NgayThamGia.Caption = "NgayThamGia";
            this.NgayThamGia.FieldName = "NgayThamGia";
            this.NgayThamGia.Name = "NgayThamGia";
            // 
            // Frm_Baocao_DS_KHTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 674);
            this.Controls.Add(this.gvcShowKHTT);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Baocao_DS_KHTT";
            this.Text = "Danh sách KHTT";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChoose)).EndInit();
            this.pnChoose.ResumeLayout(false);
            this.pnChoose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Look_data.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvcShowKHTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private System.Windows.Forms.DateTimePicker date_denngay;
        private System.Windows.Forms.DateTimePicker date_tungay;
        private DevExpress.XtraEditors.LabelControl lbNgayTu;
        private DevExpress.XtraEditors.LabelControl lbNgayDen;
        private DevExpress.XtraEditors.LabelControl lbTuDiem;
        private DevExpress.XtraEditors.LabelControl lbDenDiem;
        private DevExpress.XtraEditors.TextEdit txtDiemTu;
        private DevExpress.XtraEditors.TextEdit txtDiemDen;
        private DevExpress.XtraEditors.LabelControl lbl_title;
        private DevExpress.XtraEditors.LookUpEdit Look_data;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.CheckEdit chkNu;
        private DevExpress.XtraEditors.CheckEdit chkNam;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl pnChoose;
        private DevExpress.XtraEditors.LabelControl lbGT;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraGrid.GridControl gvcShowKHTT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHTT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn TTHONNHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMND;
        private DevExpress.XtraGrid.Columns.GridColumn WEBSITE;
        private DevExpress.XtraGrid.Columns.GridColumn SOLANPHIMTHANG;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHGIOITHIEU;
        private DevExpress.XtraGrid.Columns.GridColumn LoaiThanhVien;
        private DevExpress.XtraGrid.Columns.GridColumn NgayKichHoat;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn LOAI;
        private DevExpress.XtraGrid.Columns.GridColumn SoDiem;
        private DevExpress.XtraGrid.Columns.GridColumn TENCAPDO;
        private DevExpress.XtraGrid.Columns.GridColumn NgayThamGia;
        private DevExpress.XtraGrid.Columns.GridColumn fill;
        private DevExpress.XtraEditors.SimpleButton btnChitiet;
        private DevExpress.XtraGrid.Columns.GridColumn nhanvien;
        private DevExpress.XtraGrid.Columns.GridColumn ngaytao;



    }
}