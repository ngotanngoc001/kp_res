﻿namespace KP_RES
{
    partial class Frm_Congtrudiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_luu = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDiem = new DevExpress.XtraEditors.TextEdit();
            this.bnt_dong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtCapdo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaKH = new DevExpress.XtraEditors.TextEdit();
            this.lbLyDo = new DevExpress.XtraEditors.LabelControl();
            this.txtSoDiem = new DevExpress.XtraEditors.TextEdit();
            this.lbDiem = new DevExpress.XtraEditors.LabelControl();
            this.txtTenKhachHang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtLyDo = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCapdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.bnt_luu);
            this.panelControl1.Controls.Add(this.panel2);
            this.panelControl1.Controls.Add(this.txtDiem);
            this.panelControl1.Controls.Add(this.bnt_dong);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtCapdo);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtMaKH);
            this.panelControl1.Controls.Add(this.lbLyDo);
            this.panelControl1.Controls.Add(this.txtSoDiem);
            this.panelControl1.Controls.Add(this.lbDiem);
            this.panelControl1.Controls.Add(this.txtTenKhachHang);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtLyDo);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(614, 278);
            this.panelControl1.TabIndex = 0;
            // 
            // bnt_luu
            // 
            this.bnt_luu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luu.Appearance.Options.UseFont = true;
            this.bnt_luu.Image = global::KP_RES.Properties.Resources.save_26;
            this.bnt_luu.Location = new System.Drawing.Point(423, 230);
            this.bnt_luu.Name = "bnt_luu";
            this.bnt_luu.Size = new System.Drawing.Size(80, 35);
            this.bnt_luu.TabIndex = 12;
            this.bnt_luu.Text = "&1.Lưu";
            this.bnt_luu.Click += new System.EventHandler(this.bnt_luu_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(610, 35);
            this.panel2.TabIndex = 0;
            // 
            // txtDiem
            // 
            this.txtDiem.Enabled = false;
            this.txtDiem.EnterMoveNextControl = true;
            this.txtDiem.Location = new System.Drawing.Point(157, 107);
            this.txtDiem.Name = "txtDiem";
            this.txtDiem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtDiem.Properties.Appearance.Options.UseFont = true;
            this.txtDiem.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtDiem.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtDiem.Properties.Mask.EditMask = "d";
            this.txtDiem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiem.Size = new System.Drawing.Size(441, 26);
            this.txtDiem.TabIndex = 7;
            // 
            // bnt_dong
            // 
            this.bnt_dong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_dong.Appearance.Options.UseFont = true;
            this.bnt_dong.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bnt_dong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.bnt_dong.Location = new System.Drawing.Point(509, 230);
            this.bnt_dong.Name = "bnt_dong";
            this.bnt_dong.Size = new System.Drawing.Size(89, 35);
            this.bnt_dong.TabIndex = 14;
            this.bnt_dong.Text = "&2.Đóng";
            this.bnt_dong.Click += new System.EventHandler(this.bnt_dong_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(12, 110);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(58, 19);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Số điểm";
            // 
            // txtCapdo
            // 
            this.txtCapdo.Enabled = false;
            this.txtCapdo.EnterMoveNextControl = true;
            this.txtCapdo.Location = new System.Drawing.Point(157, 75);
            this.txtCapdo.Name = "txtCapdo";
            this.txtCapdo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtCapdo.Properties.Appearance.Options.UseFont = true;
            this.txtCapdo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtCapdo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCapdo.Size = new System.Drawing.Size(441, 26);
            this.txtCapdo.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(12, 78);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(50, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Cấp độ";
            // 
            // txtMaKH
            // 
            this.txtMaKH.Enabled = false;
            this.txtMaKH.EnterMoveNextControl = true;
            this.txtMaKH.Location = new System.Drawing.Point(157, 43);
            this.txtMaKH.Name = "txtMaKH";
            this.txtMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtMaKH.Properties.Appearance.Options.UseFont = true;
            this.txtMaKH.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtMaKH.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtMaKH.Properties.Mask.EditMask = "N0";
            this.txtMaKH.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMaKH.Size = new System.Drawing.Size(144, 26);
            this.txtMaKH.TabIndex = 2;
            // 
            // lbLyDo
            // 
            this.lbLyDo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbLyDo.Location = new System.Drawing.Point(12, 174);
            this.lbLyDo.Name = "lbLyDo";
            this.lbLyDo.Size = new System.Drawing.Size(41, 19);
            this.lbLyDo.TabIndex = 10;
            this.lbLyDo.Text = "Lý Do";
            // 
            // txtSoDiem
            // 
            this.txtSoDiem.EnterMoveNextControl = true;
            this.txtSoDiem.Location = new System.Drawing.Point(157, 139);
            this.txtSoDiem.Name = "txtSoDiem";
            this.txtSoDiem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSoDiem.Properties.Appearance.Options.UseFont = true;
            this.txtSoDiem.Properties.Mask.EditMask = "N0";
            this.txtSoDiem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoDiem.Size = new System.Drawing.Size(441, 26);
            this.txtSoDiem.TabIndex = 9;
            // 
            // lbDiem
            // 
            this.lbDiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbDiem.Location = new System.Drawing.Point(12, 142);
            this.lbDiem.Name = "lbDiem";
            this.lbDiem.Size = new System.Drawing.Size(60, 19);
            this.lbDiem.TabIndex = 8;
            this.lbDiem.Text = "Số Điểm";
            // 
            // txtTenKhachHang
            // 
            this.txtTenKhachHang.Enabled = false;
            this.txtTenKhachHang.EnterMoveNextControl = true;
            this.txtTenKhachHang.Location = new System.Drawing.Point(307, 43);
            this.txtTenKhachHang.Name = "txtTenKhachHang";
            this.txtTenKhachHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTenKhachHang.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhachHang.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtTenKhachHang.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTenKhachHang.Size = new System.Drawing.Size(291, 26);
            this.txtTenKhachHang.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(12, 46);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(82, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Khách hàng";
            // 
            // txtLyDo
            // 
            this.txtLyDo.Location = new System.Drawing.Point(157, 171);
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtLyDo.Properties.Appearance.Options.UseFont = true;
            this.txtLyDo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtLyDo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtLyDo.Size = new System.Drawing.Size(441, 53);
            this.txtLyDo.TabIndex = 11;
            // 
            // Frm_Congtrudiem
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 278);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Congtrudiem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cộng Trừ Điểm Khách Hàng Thân Thiết";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCapdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoDiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLyDo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton bnt_luu;
        private DevExpress.XtraEditors.SimpleButton bnt_dong;
        private DevExpress.XtraEditors.TextEdit txtDiem;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCapdo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtMaKH;
        private DevExpress.XtraEditors.LabelControl lbLyDo;
        private DevExpress.XtraEditors.TextEdit txtSoDiem;
        private DevExpress.XtraEditors.LabelControl lbDiem;
        private DevExpress.XtraEditors.TextEdit txtTenKhachHang;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit txtLyDo;
        private System.Windows.Forms.Panel panel2;
    }
}