﻿namespace KP_RES 
{
    partial class Frm_Khachhang 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnInfor = new DevExpress.XtraEditors.PanelControl();
            this.btnTaccaKH = new DevExpress.XtraEditors.SimpleButton();
            this.btnDoima = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtMatkhau = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaikhoan = new DevExpress.XtraEditors.TextEdit();
            this.btnRuttien = new DevExpress.XtraEditors.SimpleButton();
            this.btnNoptien = new DevExpress.XtraEditors.SimpleButton();
            this.btnTrudiem = new DevExpress.XtraEditors.SimpleButton();
            this.btnCongdiem = new DevExpress.XtraEditors.SimpleButton();
            this.btnInthe = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaKH = new DevExpress.XtraEditors.TextEdit();
            this.cboGioitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgaysinh = new DevExpress.XtraEditors.DateEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtCMND = new DevExpress.XtraEditors.TextEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtEMAIL = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtDIENTHOAI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDIACHI = new DevExpress.XtraEditors.TextEdit();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WEBSITE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TAIKHOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LoaiThanhVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayKichHoat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENTHELOAIPHIM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAKHGIOITHIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).BeginInit();
            this.pnInfor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatkhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaikhoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMAIL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENTHOAI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIACHI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // pnInfor
            // 
            this.pnInfor.Controls.Add(this.btnTaccaKH);
            this.pnInfor.Controls.Add(this.btnDoima);
            this.pnInfor.Controls.Add(this.labelControl3);
            this.pnInfor.Controls.Add(this.txtMatkhau);
            this.pnInfor.Controls.Add(this.labelControl5);
            this.pnInfor.Controls.Add(this.txtTaikhoan);
            this.pnInfor.Controls.Add(this.btnRuttien);
            this.pnInfor.Controls.Add(this.btnNoptien);
            this.pnInfor.Controls.Add(this.btnTrudiem);
            this.pnInfor.Controls.Add(this.btnCongdiem);
            this.pnInfor.Controls.Add(this.btnInthe);
            this.pnInfor.Controls.Add(this.labelControl13);
            this.pnInfor.Controls.Add(this.labelControl7);
            this.pnInfor.Controls.Add(this.txtMaKH);
            this.pnInfor.Controls.Add(this.cboGioitinh);
            this.pnInfor.Controls.Add(this.labelControl11);
            this.pnInfor.Controls.Add(this.labelControl12);
            this.pnInfor.Controls.Add(this.dtpNgaysinh);
            this.pnInfor.Controls.Add(this.labelControl9);
            this.pnInfor.Controls.Add(this.txtCMND);
            this.pnInfor.Controls.Add(this.btnLuu);
            this.pnInfor.Controls.Add(this.btnSua);
            this.pnInfor.Controls.Add(this.btnXoa);
            this.pnInfor.Controls.Add(this.btnThem);
            this.pnInfor.Controls.Add(this.txtWebsite);
            this.pnInfor.Controls.Add(this.labelControl4);
            this.pnInfor.Controls.Add(this.txtEMAIL);
            this.pnInfor.Controls.Add(this.labelControl2);
            this.pnInfor.Controls.Add(this.txtDIENTHOAI);
            this.pnInfor.Controls.Add(this.labelControl1);
            this.pnInfor.Controls.Add(this.txtDIACHI);
            this.pnInfor.Controls.Add(this.chkSUDUNG);
            this.pnInfor.Controls.Add(this.lblTEN);
            this.pnInfor.Controls.Add(this.txtTEN);
            this.pnInfor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnInfor.Location = new System.Drawing.Point(0, 0);
            this.pnInfor.Name = "pnInfor";
            this.pnInfor.Size = new System.Drawing.Size(1368, 218);
            this.pnInfor.TabIndex = 0;
            // 
            // btnTaccaKH
            // 
            this.btnTaccaKH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaccaKH.Appearance.Options.UseFont = true;
            this.btnTaccaKH.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnTaccaKH.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTaccaKH.Location = new System.Drawing.Point(591, 176);
            this.btnTaccaKH.Margin = new System.Windows.Forms.Padding(4);
            this.btnTaccaKH.Name = "btnTaccaKH";
            this.btnTaccaKH.Size = new System.Drawing.Size(125, 35);
            this.btnTaccaKH.TabIndex = 33;
            this.btnTaccaKH.Text = "&Tất cả KH";
            this.btnTaccaKH.Click += new System.EventHandler(this.btnTaccaKH_Click);
            // 
            // btnDoima
            // 
            this.btnDoima.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoima.Appearance.Options.UseFont = true;
            this.btnDoima.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnDoima.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDoima.Location = new System.Drawing.Point(409, 133);
            this.btnDoima.Margin = new System.Windows.Forms.Padding(4);
            this.btnDoima.Name = "btnDoima";
            this.btnDoima.Size = new System.Drawing.Size(125, 35);
            this.btnDoima.TabIndex = 32;
            this.btnDoima.Text = "&Đổi mã";
            this.btnDoima.Click += new System.EventHandler(this.btnDoima_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(645, 105);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(64, 19);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Mật khẩu";
            // 
            // txtMatkhau
            // 
            this.txtMatkhau.EnterMoveNextControl = true;
            this.txtMatkhau.Location = new System.Drawing.Point(724, 100);
            this.txtMatkhau.Name = "txtMatkhau";
            this.txtMatkhau.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatkhau.Properties.Appearance.Options.UseFont = true;
            this.txtMatkhau.Properties.PasswordChar = '*';
            this.txtMatkhau.Size = new System.Drawing.Size(248, 26);
            this.txtMatkhau.TabIndex = 22;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(313, 105);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(70, 19);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Tài khoản";
            // 
            // txtTaikhoan
            // 
            this.txtTaikhoan.EnterMoveNextControl = true;
            this.txtTaikhoan.Location = new System.Drawing.Point(390, 102);
            this.txtTaikhoan.Name = "txtTaikhoan";
            this.txtTaikhoan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaikhoan.Properties.Appearance.Options.UseFont = true;
            this.txtTaikhoan.Size = new System.Drawing.Size(248, 26);
            this.txtTaikhoan.TabIndex = 20;
            // 
            // btnRuttien
            // 
            this.btnRuttien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRuttien.Appearance.Options.UseFont = true;
            this.btnRuttien.Image = global::KP_RES.Properties.Resources.edit_image_26;
            this.btnRuttien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnRuttien.Location = new System.Drawing.Point(458, 176);
            this.btnRuttien.Margin = new System.Windows.Forms.Padding(4);
            this.btnRuttien.Name = "btnRuttien";
            this.btnRuttien.Size = new System.Drawing.Size(125, 35);
            this.btnRuttien.TabIndex = 31;
            this.btnRuttien.Text = "&Rút tiền";
            this.btnRuttien.Click += new System.EventHandler(this.btnRuttien_Click);
            // 
            // btnNoptien
            // 
            this.btnNoptien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoptien.Appearance.Options.UseFont = true;
            this.btnNoptien.Image = global::KP_RES.Properties.Resources.add_image_26;
            this.btnNoptien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnNoptien.Location = new System.Drawing.Point(325, 176);
            this.btnNoptien.Margin = new System.Windows.Forms.Padding(4);
            this.btnNoptien.Name = "btnNoptien";
            this.btnNoptien.Size = new System.Drawing.Size(125, 35);
            this.btnNoptien.TabIndex = 30;
            this.btnNoptien.Text = "&Nạp tiền";
            this.btnNoptien.Click += new System.EventHandler(this.btnNoptien_Click);
            // 
            // btnTrudiem
            // 
            this.btnTrudiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrudiem.Appearance.Options.UseFont = true;
            this.btnTrudiem.Image = global::KP_RES.Properties.Resources.zoom_out_26;
            this.btnTrudiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTrudiem.Location = new System.Drawing.Point(192, 176);
            this.btnTrudiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTrudiem.Name = "btnTrudiem";
            this.btnTrudiem.Size = new System.Drawing.Size(125, 35);
            this.btnTrudiem.TabIndex = 29;
            this.btnTrudiem.Text = "&Trừ điểm";
            this.btnTrudiem.Click += new System.EventHandler(this.brnTrudiem_Click);
            // 
            // btnCongdiem
            // 
            this.btnCongdiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCongdiem.Appearance.Options.UseFont = true;
            this.btnCongdiem.Image = global::KP_RES.Properties.Resources.zoom_in_26;
            this.btnCongdiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCongdiem.Location = new System.Drawing.Point(59, 176);
            this.btnCongdiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnCongdiem.Name = "btnCongdiem";
            this.btnCongdiem.Size = new System.Drawing.Size(125, 35);
            this.btnCongdiem.TabIndex = 28;
            this.btnCongdiem.Text = "&Cộng điểm";
            this.btnCongdiem.Click += new System.EventHandler(this.btnCongdiem_Click);
            // 
            // btnInthe
            // 
            this.btnInthe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInthe.Appearance.Options.UseFont = true;
            this.btnInthe.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btnInthe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnInthe.Location = new System.Drawing.Point(542, 133);
            this.btnInthe.Margin = new System.Windows.Forms.Padding(4);
            this.btnInthe.Name = "btnInthe";
            this.btnInthe.Size = new System.Drawing.Size(80, 35);
            this.btnInthe.TabIndex = 27;
            this.btnInthe.Text = "&In thẻ";
            this.btnInthe.Click += new System.EventHandler(this.btnInthe_Click);
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(645, 75);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(55, 19);
            this.labelControl13.TabIndex = 16;
            this.labelControl13.Text = "Website";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(6, 9);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(20, 19);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Mã";
            // 
            // txtMaKH
            // 
            this.txtMaKH.EnterMoveNextControl = true;
            this.txtMaKH.Location = new System.Drawing.Point(59, 6);
            this.txtMaKH.Name = "txtMaKH";
            this.txtMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaKH.Properties.Appearance.Options.UseFont = true;
            this.txtMaKH.Size = new System.Drawing.Size(248, 26);
            this.txtMaKH.TabIndex = 1;
            this.txtMaKH.Leave += new System.EventHandler(this.txtMaKH_Leave);
            // 
            // cboGioitinh
            // 
            this.cboGioitinh.EnterMoveNextControl = true;
            this.cboGioitinh.Location = new System.Drawing.Point(390, 38);
            this.cboGioitinh.Name = "cboGioitinh";
            this.cboGioitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioitinh.Properties.Appearance.Options.UseFont = true;
            this.cboGioitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboGioitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboGioitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboGioitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboGioitinh.Properties.DisplayMember = "TEN";
            this.cboGioitinh.Properties.DropDownItemHeight = 40;
            this.cboGioitinh.Properties.NullText = "";
            this.cboGioitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboGioitinh.Properties.ShowHeader = false;
            this.cboGioitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboGioitinh.Properties.ValueMember = "MA";
            this.cboGioitinh.Size = new System.Drawing.Size(248, 26);
            this.cboGioitinh.TabIndex = 9;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(313, 41);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 19);
            this.labelControl11.TabIndex = 8;
            this.labelControl11.Text = "Giới tính";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(313, 9);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(70, 19);
            this.labelControl12.TabIndex = 6;
            this.labelControl12.Text = "Ngày sinh";
            // 
            // dtpNgaysinh
            // 
            this.dtpNgaysinh.EditValue = null;
            this.dtpNgaysinh.EnterMoveNextControl = true;
            this.dtpNgaysinh.Location = new System.Drawing.Point(390, 6);
            this.dtpNgaysinh.Name = "dtpNgaysinh";
            this.dtpNgaysinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaysinh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgaysinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaysinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgaysinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgaysinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaysinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaysinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgaysinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgaysinh.Size = new System.Drawing.Size(248, 26);
            this.dtpNgaysinh.TabIndex = 7;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(6, 75);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(44, 19);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "CMND";
            // 
            // txtCMND
            // 
            this.txtCMND.EnterMoveNextControl = true;
            this.txtCMND.Location = new System.Drawing.Point(59, 70);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMND.Properties.Appearance.Options.UseFont = true;
            this.txtCMND.Size = new System.Drawing.Size(248, 26);
            this.txtCMND.TabIndex = 5;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(59, 133);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(78, 35);
            this.btnLuu.TabIndex = 23;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(233, 133);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 25;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(321, 133);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 26;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(145, 133);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 24;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtWebsite
            // 
            this.txtWebsite.EnterMoveNextControl = true;
            this.txtWebsite.Location = new System.Drawing.Point(724, 70);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebsite.Properties.Appearance.Options.UseFont = true;
            this.txtWebsite.Size = new System.Drawing.Size(248, 26);
            this.txtWebsite.TabIndex = 17;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(645, 41);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 19);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Email";
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.EnterMoveNextControl = true;
            this.txtEMAIL.Location = new System.Drawing.Point(724, 38);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEMAIL.Properties.Appearance.Options.UseFont = true;
            this.txtEMAIL.Size = new System.Drawing.Size(248, 26);
            this.txtEMAIL.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(645, 9);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(72, 19);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Điện thoại";
            // 
            // txtDIENTHOAI
            // 
            this.txtDIENTHOAI.EnterMoveNextControl = true;
            this.txtDIENTHOAI.Location = new System.Drawing.Point(724, 6);
            this.txtDIENTHOAI.Name = "txtDIENTHOAI";
            this.txtDIENTHOAI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDIENTHOAI.Properties.Appearance.Options.UseFont = true;
            this.txtDIENTHOAI.Size = new System.Drawing.Size(248, 26);
            this.txtDIENTHOAI.TabIndex = 13;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(313, 75);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 19);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Địa chỉ";
            // 
            // txtDIACHI
            // 
            this.txtDIACHI.EnterMoveNextControl = true;
            this.txtDIACHI.Location = new System.Drawing.Point(390, 72);
            this.txtDIACHI.Name = "txtDIACHI";
            this.txtDIACHI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDIACHI.Properties.Appearance.Options.UseFont = true;
            this.txtDIACHI.Size = new System.Drawing.Size(248, 26);
            this.txtDIACHI.TabIndex = 11;
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(57, 102);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(89, 24);
            this.chkSUDUNG.TabIndex = 18;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 41);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(27, 19);
            this.lblTEN.TabIndex = 2;
            this.lblTEN.Text = "Tên";
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(59, 38);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 3;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 218);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1329, 433);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MAKHTT,
            this.TEN,
            this.CMND,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.DIENTHOAI,
            this.EMAIL,
            this.WEBSITE,
            this.TAIKHOAN,
            this.NGAYTAO,
            this.SUDUNG,
            this.LoaiThanhVien,
            this.NgayKichHoat,
            this.TENTHELOAIPHIM,
            this.NGAYCMND,
            this.LOAI,
            this.MAKHGIOITHIEU});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.NGAYTAO, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MAKHTT
            // 
            this.MAKHTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAKHTT.AppearanceHeader.Options.UseFont = true;
            this.MAKHTT.AppearanceHeader.Options.UseTextOptions = true;
            this.MAKHTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MAKHTT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MAKHTT.Caption = "Mã";
            this.MAKHTT.FieldName = "MA";
            this.MAKHTT.Name = "MAKHTT";
            this.MAKHTT.OptionsColumn.AllowEdit = false;
            this.MAKHTT.OptionsColumn.AllowFocus = false;
            this.MAKHTT.OptionsColumn.FixedWidth = true;
            this.MAKHTT.Visible = true;
            this.MAKHTT.VisibleIndex = 1;
            this.MAKHTT.Width = 110;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 200;
            // 
            // CMND
            // 
            this.CMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMND.AppearanceHeader.Options.UseFont = true;
            this.CMND.AppearanceHeader.Options.UseTextOptions = true;
            this.CMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.Caption = "CMND";
            this.CMND.FieldName = "CMND";
            this.CMND.Name = "CMND";
            this.CMND.OptionsColumn.AllowEdit = false;
            this.CMND.OptionsColumn.AllowFocus = false;
            this.CMND.OptionsColumn.FixedWidth = true;
            this.CMND.Visible = true;
            this.CMND.VisibleIndex = 3;
            this.CMND.Width = 120;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 4;
            this.NGAYSINH.Width = 110;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH1";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 5;
            this.GIOITINH.Width = 90;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 6;
            this.DIACHI.Width = 300;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 7;
            this.DIENTHOAI.Width = 120;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 8;
            this.EMAIL.Width = 180;
            // 
            // WEBSITE
            // 
            this.WEBSITE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WEBSITE.AppearanceHeader.Options.UseFont = true;
            this.WEBSITE.AppearanceHeader.Options.UseTextOptions = true;
            this.WEBSITE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WEBSITE.Caption = "Website";
            this.WEBSITE.FieldName = "WEBSITE";
            this.WEBSITE.Name = "WEBSITE";
            this.WEBSITE.OptionsColumn.AllowEdit = false;
            this.WEBSITE.OptionsColumn.AllowFocus = false;
            this.WEBSITE.OptionsColumn.FixedWidth = true;
            this.WEBSITE.Visible = true;
            this.WEBSITE.VisibleIndex = 9;
            this.WEBSITE.Width = 180;
            // 
            // TAIKHOAN
            // 
            this.TAIKHOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TAIKHOAN.AppearanceHeader.Options.UseFont = true;
            this.TAIKHOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TAIKHOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TAIKHOAN.Caption = "Tài khoản";
            this.TAIKHOAN.FieldName = "TAIKHOAN";
            this.TAIKHOAN.Name = "TAIKHOAN";
            this.TAIKHOAN.OptionsColumn.AllowEdit = false;
            this.TAIKHOAN.OptionsColumn.AllowFocus = false;
            this.TAIKHOAN.OptionsColumn.FixedWidth = true;
            this.TAIKHOAN.Visible = true;
            this.TAIKHOAN.VisibleIndex = 10;
            this.TAIKHOAN.Width = 120;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày tham gia";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 11;
            this.NGAYTAO.Width = 140;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 12;
            this.SUDUNG.Width = 90;
            // 
            // LoaiThanhVien
            // 
            this.LoaiThanhVien.AppearanceCell.Options.UseTextOptions = true;
            this.LoaiThanhVien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiThanhVien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LoaiThanhVien.AppearanceHeader.Options.UseFont = true;
            this.LoaiThanhVien.AppearanceHeader.Options.UseTextOptions = true;
            this.LoaiThanhVien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LoaiThanhVien.Caption = "Loại Thành Viên";
            this.LoaiThanhVien.FieldName = "LoaiThanhVien";
            this.LoaiThanhVien.Name = "LoaiThanhVien";
            this.LoaiThanhVien.OptionsColumn.AllowEdit = false;
            this.LoaiThanhVien.OptionsColumn.AllowFocus = false;
            this.LoaiThanhVien.OptionsColumn.FixedWidth = true;
            this.LoaiThanhVien.Width = 140;
            // 
            // NgayKichHoat
            // 
            this.NgayKichHoat.AppearanceCell.Options.UseTextOptions = true;
            this.NgayKichHoat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayKichHoat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NgayKichHoat.AppearanceHeader.Options.UseFont = true;
            this.NgayKichHoat.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayKichHoat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayKichHoat.Caption = "Ngày Kích Hoạt";
            this.NgayKichHoat.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NgayKichHoat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NgayKichHoat.FieldName = "NgayKichHoat";
            this.NgayKichHoat.Name = "NgayKichHoat";
            this.NgayKichHoat.OptionsColumn.AllowEdit = false;
            this.NgayKichHoat.OptionsColumn.AllowFocus = false;
            this.NgayKichHoat.OptionsColumn.FixedWidth = true;
            this.NgayKichHoat.Width = 150;
            // 
            // TENTHELOAIPHIM
            // 
            this.TENTHELOAIPHIM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENTHELOAIPHIM.AppearanceHeader.Options.UseFont = true;
            this.TENTHELOAIPHIM.AppearanceHeader.Options.UseTextOptions = true;
            this.TENTHELOAIPHIM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENTHELOAIPHIM.Caption = "Thể Loại Yêu Thích";
            this.TENTHELOAIPHIM.FieldName = "TENTHELOAIPHIM";
            this.TENTHELOAIPHIM.Name = "TENTHELOAIPHIM";
            this.TENTHELOAIPHIM.OptionsColumn.AllowEdit = false;
            this.TENTHELOAIPHIM.OptionsColumn.AllowFocus = false;
            this.TENTHELOAIPHIM.OptionsColumn.FixedWidth = true;
            this.TENTHELOAIPHIM.Width = 180;
            // 
            // NGAYCMND
            // 
            this.NGAYCMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYCMND.AppearanceHeader.Options.UseFont = true;
            this.NGAYCMND.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYCMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYCMND.Caption = "Ngày Cấp CMND";
            this.NGAYCMND.FieldName = "NGAYCMND";
            this.NGAYCMND.Name = "NGAYCMND";
            this.NGAYCMND.OptionsColumn.AllowEdit = false;
            this.NGAYCMND.OptionsColumn.AllowFocus = false;
            this.NGAYCMND.OptionsColumn.FixedWidth = true;
            this.NGAYCMND.Width = 140;
            // 
            // LOAI
            // 
            this.LOAI.Caption = "LOAI";
            this.LOAI.FieldName = "LOAI";
            this.LOAI.Name = "LOAI";
            // 
            // MAKHGIOITHIEU
            // 
            this.MAKHGIOITHIEU.AppearanceCell.Options.UseTextOptions = true;
            this.MAKHGIOITHIEU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAKHGIOITHIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAKHGIOITHIEU.AppearanceHeader.Options.UseFont = true;
            this.MAKHGIOITHIEU.Caption = "Mã KH Giới Thiệu";
            this.MAKHGIOITHIEU.DisplayFormat.FormatString = "00000000";
            this.MAKHGIOITHIEU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MAKHGIOITHIEU.FieldName = "MAKHGIOITHIEU";
            this.MAKHGIOITHIEU.Name = "MAKHGIOITHIEU";
            this.MAKHGIOITHIEU.OptionsColumn.AllowEdit = false;
            this.MAKHGIOITHIEU.OptionsColumn.AllowFocus = false;
            this.MAKHGIOITHIEU.OptionsColumn.AllowMove = false;
            this.MAKHGIOITHIEU.Width = 150;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1329, 218);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 433);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 269);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 0);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 109);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 189);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 351);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_Khachhang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1368, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnInfor);
            this.Name = "Frm_Khachhang";
            this.Text = "Khách hàng";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_THEM_KHTT_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).EndInit();
            this.pnInfor.ResumeLayout(false);
            this.pnInfor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatkhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaikhoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMAIL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENTHOAI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIACHI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnInfor;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHTT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn TENTHELOAIPHIM;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraGrid.Columns.GridColumn MAKHGIOITHIEU;
        private DevExpress.XtraEditors.TextEdit txtWebsite;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtEMAIL;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtDIENTHOAI;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn WEBSITE;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYCMND;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn LOAI;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboGioitinh;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.DateEdit dtpNgaysinh;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtCMND;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn LoaiThanhVien;
        private DevExpress.XtraGrid.Columns.GridColumn NgayKichHoat;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtMaKH;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton btnInthe;
        private DevExpress.XtraEditors.SimpleButton btnCongdiem;
        private DevExpress.XtraEditors.SimpleButton btnTrudiem;
        private DevExpress.XtraEditors.SimpleButton btnRuttien;
        private DevExpress.XtraEditors.SimpleButton btnNoptien;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtMatkhau;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtTaikhoan;
        private DevExpress.XtraGrid.Columns.GridColumn TAIKHOAN;
        private DevExpress.XtraEditors.SimpleButton btnDoima;
        private DevExpress.XtraEditors.SimpleButton btnTaccaKH;




    }
}