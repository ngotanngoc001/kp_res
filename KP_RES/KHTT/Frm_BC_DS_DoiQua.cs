﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BC_DS_DoiQua : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_BC_DS_DoiQua()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;
            String sSQL = "";

            if (optGroup.SelectedIndex == 0)
                pnChoose.Visible = false;
            else
                pnChoose.Visible = true;

            if (optGroup.SelectedIndex == 1)
            {
                sSQL = "SELECT MA, TEN FROM KHTT WHERE SUDUNG = 1 ORDER BY TEN";
                LoadCombo(sSQL);
            }
            else if (optGroup.SelectedIndex == 2)
            {
                sSQL = "SELECT MANHANVIEN AS MA,TENNHANVIEN AS TEN FROM DM_NHANVIEN WHERE SUDUNG = 1 ORDER BY TEN";
                LoadCombo(sSQL);
            }
            else if (optGroup.SelectedIndex == 3)
            {
                sSQL = "SELECT MA_QT AS MA,TEN_QT AS TEN FROM KHTT_DIEMTHUONG_QUATANG WHERE SUDUNG = 1 ORDER BY TEN";
                LoadCombo(sSQL);
            }
        }

        private void LoadCombo(String sSQL)
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable(sSQL);
            cboData.Properties.DataSource = myDT;
            cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }
        
        private static string chuanHoaXau(string xau)
        {
            StringBuilder kq = new StringBuilder();
            xau = xau.Trim();
            for (int i = 0; i < xau.Length; i++)
            {
                kq.Append(xau[i]);
                if (xau[i] == ' ')
                {
                    while (xau[i] == ' ')
                    {
                        i++;
                    }
                    kq.Append(xau[i]);
                }
            }
            return kq.ToString();
        }
        
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sSQL = "";

                if (optGroup.SelectedIndex == 0)
                {
                    sSQL += "EXEC sp_Tim_KHTT_DS_DOIQUA1 " + clsMain.SQLString("") + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue)) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue)) + ",0";
                }
                else if (optGroup.SelectedIndex == 1)
                {
                    sSQL += "EXEC sp_Tim_KHTT_DS_DOIQUA1 " + clsMain.SQLString(cboData.EditValue.ToString()) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue)) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue)) + ",1";
                }
                else if (optGroup.SelectedIndex == 2)
                {
                    sSQL += "EXEC sp_Tim_KHTT_DS_DOIQUA1 " + clsMain.SQLString(cboData.EditValue.ToString()) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue)) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue)) + ",2";
                }
                else if (optGroup.SelectedIndex == 3)
                {
                    sSQL += "EXEC sp_Tim_KHTT_DS_DOIQUA1 " + clsMain.SQLString(cboData.EditValue.ToString()) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue)) + "," + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue)) + ",3";
                }
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount >0)
                {
                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");

                    if (optGroup.SelectedIndex == 0)
                    {
                        All.DefaultValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description + " )";
                    }
                    else if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2 || optGroup.SelectedIndex == 3)
                    {
                        All.DefaultValue = "( " + optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboData.Text + " )";
                    }

                    DataColumn colTUNGAY = new DataColumn();
                    colTUNGAY.ColumnName = "TUNGAY";
                    colTUNGAY.DataType = System.Type.GetType("System.String");
                    colTUNGAY.DefaultValue = dtpTungay.Text;
                    DataColumn colDENNGAY = new DataColumn();
                    colDENNGAY.ColumnName = "DENNGAY";
                    colDENNGAY.DataType = System.Type.GetType("System.String");
                    colDENNGAY.DefaultValue = dtpDenngay.Text;

                    if (ContainColumn("HEARDER", dt) == false)
                        dt.Columns.Add(All);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(colTUNGAY);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(colDENNGAY);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 64;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
                panelControl8.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
                panelControl8.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        string _thongbao = "Thông báo";
        string _kocodulieu = "Không có dữ liệu";

        private void Frm_BC_Banhang_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            //this.Text = rm.GetString("baocaobanhang", culture);

        }
    }
}