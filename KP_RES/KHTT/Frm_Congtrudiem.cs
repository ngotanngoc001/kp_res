﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Congtrudiem : DevExpress.XtraEditors.XtraForm
    {
        bool _flag;
        string _maKH;
        public Frm_Congtrudiem(string maKH,bool flag)
        {
            InitializeComponent();
            _flag = flag;
            _maKH = maKH;
            DataTable dtKH = cls_KHTT .ReturnKhachHangThanThiet (maKH);
            txtMaKH.Text = dtKH.Rows[0]["MA"].ToString();
            txtTenKhachHang.Text = dtKH.Rows[0]["TEN"].ToString();
            txtCapdo.Text = dtKH.Rows[0]["TENCAPDO"].ToString();
            txtDiem.Text = dtKH.Rows[0]["SoDiem"].ToString();
            if (_flag)
            {
                this.Text = "CỘNG ĐIỂM KHÁCH HÀNG THÂN THIẾT";
                lbDiem.Text = "Số Điểm Cộng";
                lbLyDo.Text = "Lý Do Cộng Điểm";
            }
            else
            {
                this.Text = "TRỪ ĐIỂM KHÁCH HÀNG THÂN THIẾT";
                lbDiem.Text = "Số Điểm Trừ";
                lbLyDo.Text = "Lý Do Trừ Điểm";
            }
        }

        private void bnt_dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnt_luu_Click(object sender, EventArgs e)
        {
            if (txtSoDiem.Text.Trim() == "")
            {
                txtSoDiem.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Số Điểm ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return ;
            }
            if (txtLyDo.Text.Trim() == "")
            {
                txtLyDo.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Lý Do ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return ;
            }

            try
            {
                int soThuongNho = 0;
                int soLanTinh = 0;
                int soThuongLon = 0;
                if (_flag)//cộng điểm
                {
                    bool  bSQL = cls_KHTT.CongDiemKhachHangThanThiet_Tructiep(_maKH,decimal .Parse ( txtSoDiem.Text.Replace(",", "")),clsGlobal .gsUserID +"_"+ txtLyDo.Text);
                    if (bSQL )
                    {
                        XtraMessageBox.Show("Đã Cộng Điểm Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else//trừ điểm
                {
                    int diemht = int.Parse(txtDiem.Text .Replace (",",""));
                    if (int.Parse(txtSoDiem.Text) > diemht)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Số Điểm Trừ Không Thể Lớn Hơn Số Điểm Hiện Tại ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    bool bSQL = cls_KHTT.TruDiemKhachHangThanThiet_Tructiep(_maKH, decimal.Parse(txtSoDiem.Text.Replace(",", "")), clsGlobal.gsUserID + "_" + txtLyDo.Text);
                    if (bSQL)
                    {
                        XtraMessageBox.Show("Đã Trừ Điểm Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}