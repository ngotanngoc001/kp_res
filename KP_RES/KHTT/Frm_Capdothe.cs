﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Capdothe : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Capdothe()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Capdothe_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtTEN.SelectAll();
        }

        string sma = "";

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From KHTT_CAPDO" + "\n";
            sSQL += "Where MACAPDO=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into KHTT_CAPDO (TENCAPDO,	SODIEM_TU	,SODIEM_DEN	,CHIETKHAU	,SUDUNG	)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLString(txtSODIEM_TU.EditValue.ToString () ) + ",";
                sSQL += clsMain.SQLString(txtSODIEM_DEN.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(txtCHIETKHAU.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update KHTT_CAPDO Set " + "\n";
                sSQL += "TENCAPDO=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "SODIEM_TU=" + clsMain.SQLString(txtSODIEM_TU.EditValue.ToString()) + "," + "\n";
                sSQL += "SODIEM_DEN=" + clsMain.SQLString(txtSODIEM_DEN.EditValue.ToString()) + "," + "\n";
                sSQL += "CHIETKHAU=" + clsMain.SQLString(txtCHIETKHAU.EditValue.ToString()) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MACAPDO=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MACAPDO").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TENCAPDO").ToString();
            txtSODIEM_TU.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SODIEM_TU");
            txtSODIEM_DEN.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SODIEM_DEN");
            txtCHIETKHAU.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CHIETKHAU");
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MACAPDO,	TENCAPDO,	SODIEM_TU,	SODIEM_DEN	,CHIETKHAU,	SUDUNG" + "\n";
            sSQL += "From KHTT_CAPDO" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("53");
            btnSua.Enabled = clsUserManagement.AllowEdit("53");
            btnXoa.Enabled = clsUserManagement.AllowDelete("53");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtSODIEM_TU.Properties.ReadOnly = true;
                txtSODIEM_DEN.Properties.ReadOnly = true;
                txtCHIETKHAU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtSODIEM_TU.Properties.ReadOnly = false;
                txtSODIEM_DEN.Properties.ReadOnly = false;
                txtCHIETKHAU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtSODIEM_TU.EditValue =0;
            txtSODIEM_DEN.EditValue = 0;
            txtCHIETKHAU.EditValue = 0;
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            if (txtSODIEM_TU.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSODIEM_TU.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSODIEM_TU.Focus();
                return false;
            }
            if (txtSODIEM_DEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblSODIEM_DEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSODIEM_DEN.Focus();
                return false;
            }
            if (txtCHIETKHAU.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblCHIETKHAU.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCHIETKHAU.Focus();
                return false;
            }
            return true;
        }

    }
}