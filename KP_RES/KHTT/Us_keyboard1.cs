﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace KP_RES
{
    public partial class Us_Keyboard1 : UserControl
    {
        /// <summary>
        /// keyboard
        /// </summary>
        public Us_Keyboard1()
        {
            InitializeComponent();          
        }

        private TextEdit _textbox;
        bool _isNumber;
        private int indexshift = 1;

        /// <summary>
        /// Gán giá trị
        /// </summary>
        /// <param name="edit"></param>
        public void SetTextbox(TextEdit edit,bool isNumber)
        {
            try
            {
                _isNumber = isNumber;
                _textbox = edit;
            }
            catch
            {
            }
        }
        public void SetSize(Size newSize)
        {
            try
            {
                //hàng 1
                btn_Acute.Size = btn_Grave.Size = btn_questionmark.Size = btn_tilde.Size = btn_dotbelow.Size = btn_AW.Size = btn_AA.Size = btn_EE.Size = btn_OO.Size = btn_UW.Size = btn_OW.Size = btn_DD.Size = newSize;
                btn_searchNext.Size = new Size(newSize.Width * 3 + 1, newSize.Height);
                btn_Acute.Location = new Point(1,0);
                btn_Grave.Location = new Point(btn_Acute.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_questionmark.Location = new Point(btn_Grave.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_tilde.Location = new Point(btn_questionmark.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_dotbelow.Location = new Point(btn_tilde.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_AW.Location = new Point(btn_dotbelow.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_AA.Location = new Point(btn_AW.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_EE.Location = new Point(btn_AA.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_OO.Location = new Point(btn_EE.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_UW.Location = new Point(btn_OO.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_OW.Location = new Point(btn_UW.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_DD.Location = new Point(btn_OW.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                btn_Shift.Location = new Point(btn_DD.Location.X + newSize.Width + 1, btn_Acute.Location.Y);
                //hàng 2
                btn_0.Size = btn_1.Size = btn_2.Size = btn_3.Size = btn_4.Size = btn_5.Size = btn_6.Size = btn_7.Size = btn_8.Size = btn_9.Size = btn_delete.Size= newSize;
                btn_ESC.Size = new Size(newSize.Width*2+1,newSize.Height);
                btn_0.Location = new Point(btn_Acute.Location.X, btn_Acute.Location.Y + newSize.Height + 1);
                btn_1.Location = new Point(btn_0.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_2.Location = new Point(btn_1.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_3.Location = new Point(btn_2.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_4.Location = new Point(btn_3.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_5.Location = new Point(btn_4.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_6.Location = new Point(btn_5.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_7.Location = new Point(btn_6.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_8.Location = new Point(btn_7.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_9.Location = new Point(btn_8.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);

                btn_ESC.Location = new Point(btn_9.Location.X + newSize.Width + 1, btn_Acute.Location.Y + newSize.Height + 1);
                btn_delete.Location = new Point(btn_ESC.Location.X + 2*newSize.Width + 2, btn_Acute.Location.Y + newSize.Height + 1);
                //hàng 3
                btn_Q.Size = btn_W.Size = btn_R.Size = btn_T.Size = btn_Y.Size = btn_U.Size = btn_I.Size = btn_O.Size = btn_P.Size =  btn_Shift.Size = btn_E.Size = newSize;
                btn_Q.Location = new Point(btn_Acute.Location.X, 2 * newSize.Height + 2);
                btn_W.Location = new Point(btn_Q.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2*newSize.Height + 2);
                btn_E.Location = new Point(btn_W.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_R.Location = new Point(btn_E.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_T.Location = new Point(btn_R.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_Y.Location = new Point(btn_T.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_U.Location = new Point(btn_Y.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_I.Location = new Point(btn_U.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_O.Location = new Point(btn_I.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                btn_P.Location = new Point(btn_O.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
              
                btn_searchNext.Location = new Point(btn_P.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 2 * newSize.Height + 2);
                //hang 4
                btn_A.Size = btn_S.Size = btn_D.Size = btn_F.Size = btn_G.Size = btn_H.Size = btn_J.Size = btn_K.Size = btn_L.Size = btn_colon.Size =  btn_SRight.Size = newSize;
                btn_enter.Size=new Size(newSize.Width,2*newSize.Height);

                btn_A.Location = new Point(btn_Acute.Location.X, 3 * newSize.Height + 3);
                btn_S.Location = new Point(btn_A.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_D.Location = new Point(btn_S.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_F.Location = new Point(btn_D.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_G.Location = new Point(btn_F.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_H.Location = new Point(btn_G.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_J.Location = new Point(btn_H.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_K.Location = new Point(btn_J.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_L.Location = new Point(btn_K.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_colon.Location = new Point(btn_L.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 2);
                btn_SLeft.Location = new Point(btn_colon.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_SRight.Location = new Point(btn_SLeft.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                btn_enter.Location = new Point(btn_SRight.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 3 * newSize.Height + 3);
                //HÀNG 5
                btn_Z.Size = btn_X.Size = btn_C.Size = btn_V.Size = btn_B.Size = btn_N.Size = btn_M.Size = btn_Comma.Size = btn_Stop.Size = btn_SLeft.Size = newSize;
                btn_Space.Size = new Size(3*newSize.Width+1,newSize.Height);
                btn_Z.Location = new Point(btn_Acute.Location.X, 4 * newSize.Height + 4);
                btn_X.Location = new Point(btn_Z.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_C.Location = new Point(btn_X.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_V.Location = new Point(btn_C.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_B.Location = new Point(btn_V.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_Space.Location = new Point(btn_B.Location.X + newSize.Width + 1, btn_Acute.Location.Y +4 * newSize.Height + 4);
                btn_N.Location = new Point(btn_Space.Location.X +3* newSize.Width + 3, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_M.Location = new Point(btn_N.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                btn_Comma.Location = new Point(btn_M.Location.X + newSize.Width + 1, btn_Acute.Location.Y +4 * newSize.Height + 4);
                btn_Stop.Location = new Point(btn_Comma.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                //btn_SLeft.Location = new Point(btn_Stop.Location.X + newSize.Width + 1, btn_Acute.Location.Y + 4 * newSize.Height + 4);
                
                
            }
            catch
            {
            }
        }

        #region bàn phím số

        private void btn_AW_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("ă");
                            break;
                        case 1:
                            Click_buttonNumber("Ă");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_AA_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("â");
                            break;
                        case 1:
                            Click_buttonNumber("Â");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_EE_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("ê");
                            break;
                        case 1:
                            Click_buttonNumber("Ê");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_OO_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("ô");
                            break;
                        case 1:
                            Click_buttonNumber("Ô");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_UW_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("ư");
                            break;
                        case 1:
                            Click_buttonNumber("Ư");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_OW_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("ơ");
                            break;
                        case 1:
                            Click_buttonNumber("Ơ");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_DD_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("đ");
                            break;
                        case 1:
                            Click_buttonNumber("Đ");
                            break;
                    }
                }
            }
            catch
            {
            }
        }
        private void btn_ESC_Click(object sender, EventArgs e)
        {
            try
            {
                _textbox.Text = "";
                _textbox.Focus();
            }
            catch
            {
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (select != 0)
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = select - 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = _textbox.Text.Length;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("1");
                        break;
                    case 1:
                        Click_buttonNumber("1");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("2");
                        break;
                    case 1:
                        Click_buttonNumber("2");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("3");
                        break;
                    case 1:
                        Click_buttonNumber("3");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("4");
                        break;
                    case 1:
                        Click_buttonNumber("4");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("5");
                        break;
                    case 1:
                        Click_buttonNumber("5");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("6");
                        break;
                    case 1:
                        Click_buttonNumber("6");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("7");
                        break;
                    case 1:
                        Click_buttonNumber("7");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("8");
                        break;
                    case 1:
                        Click_buttonNumber("8");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("9");
                        break;
                    case 1:
                        Click_buttonNumber("9");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("0");
                        break;
                    case 1:
                        Click_buttonNumber("0");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_enter_Click(object sender, EventArgs e)
        {
            try
            {
                _textbox.Focus();
                SendKeys.Send("{ENTER}");
            }
            catch
            {
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (select != 0)
                {
                    if (_textbox.SelectionLength < 1)
                    {
                        _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                        _textbox.Focus();
                        _textbox.SelectionStart = select - 1;
                        _textbox.SelectionLength = 0;
                    }
                    else
                    {
                        int length = _textbox.SelectionLength;
                        _textbox.Text = _textbox.Text.Remove(select, length);
                        _textbox.Focus();
                        _textbox.SelectionStart = select;
                        _textbox.SelectionLength = 0;
                    }
                }
                else
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = 0;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        private void btn_A_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("a");
                            break;
                        case 1:
                            Click_buttonNumber("A");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_B_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("b");
                            break;
                        case 1:
                            Click_buttonNumber("B");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_C_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("c");
                            break;
                        case 1:
                            Click_buttonNumber("C");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_D_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("d");
                            break;
                        case 1:
                            Click_buttonNumber("D");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_E_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("e");
                            break;
                        case 1:
                            Click_buttonNumber("E");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_F_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("f");
                            break;
                        case 1:
                            Click_buttonNumber("F");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_G_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("g");
                            break;
                        case 1:
                            Click_buttonNumber("G");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_H_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("h");
                            break;
                        case 1:
                            Click_buttonNumber("H");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_I_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {

                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("i");
                            break;
                        case 1:
                            Click_buttonNumber("I");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_J_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("j");
                            break;
                        case 1:
                            Click_buttonNumber("J");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_K_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("k");
                            break;
                        case 1:
                            Click_buttonNumber("K");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_L_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("l");
                            break;
                        case 1:
                            Click_buttonNumber("L");
                            break;
                    }
                }
            }
            catch 
            {
            }
        }

        private void btn_M_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("m");
                            break;
                        case 1:
                            Click_buttonNumber("M");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_N_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("n");
                            break;
                        case 1:
                            Click_buttonNumber("N");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_O_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("o");
                            break;
                        case 1:
                            Click_buttonNumber("O");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_P_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("p");
                            break;
                        case 1:
                            Click_buttonNumber("P");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Q_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("q");
                            break;
                        case 1:
                            Click_buttonNumber("Q");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_R_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("r");
                            break;
                        case 1:
                            Click_buttonNumber("R");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_S_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("s");
                            break;
                        case 1:
                            Click_buttonNumber("S");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_T_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("t");
                            break;
                        case 1:
                            Click_buttonNumber("T");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_U_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("u");
                            break;
                        case 1:
                            Click_buttonNumber("U");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_V_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("v");
                            break;
                        case 1:
                            Click_buttonNumber("V");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_W_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("w");
                            break;
                        case 1:
                            Click_buttonNumber("W");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Y_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("y");
                            break;
                        case 1:
                            Click_buttonNumber("Y");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Z_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("z");
                            break;
                        case 1:
                            Click_buttonNumber("Z");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_SLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("\\");
                            break;
                        case 1:
                            Click_buttonNumber("\\");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber(".");
                            break;
                        case 1:
                            Click_buttonNumber(".");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_SRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("/");
                            break;
                        case 1:
                            Click_buttonNumber("/");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_X_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("x");
                            break;
                        case 1:
                            Click_buttonNumber("X");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Cross_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("-");
                        break;
                    case 1:
                        Click_buttonNumber("_");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Leftcurlybrackets_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("[");
                            break;
                        case 1:
                            Click_buttonNumber("[");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Rightcurlybrackets_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber("]");
                            break;
                        case 1:
                            Click_buttonNumber("]");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_colon_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber(";");
                            break;
                        case 1:
                            Click_buttonNumber(";");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_quotationmarks_Click(object sender, EventArgs e)
        {
            try
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("'");
                        break;
                    case 1:
                        Click_buttonNumber("\"");
                        break;
                }
            }
            catch
            {
            }
        }

        private void btn_Space_Click(object sender, EventArgs e)
        {
            try
            {
                Click_buttonNumber(" ");
            }
            catch
            {
            }
        }

        private void btn_Comma_Click(object sender, EventArgs e)
        {
            try
            {
                if (_isNumber)
                {
                    switch (indexshift)
                    {
                        case 0:
                            Click_buttonNumber(",");
                            break;
                        case 1:
                            Click_buttonNumber(",");
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_Shift_Click(object sender, EventArgs e)
        {
            try
            {
                if (indexshift == 0)
                    indexshift = 1;
                else if (indexshift == 1)
                    indexshift = 0;
                if (indexshift == 1)
                {
                    btn_Shift.ForeColor = Color.Maroon;
                }
                else
                {
                    btn_Shift.ForeColor = Color.DarkOliveGreen;
                }
            }
            catch
            {
            }
        }

        private void Click_buttonNumber(string number)
        {
            try
            {
                int select = _textbox.SelectionStart;
                if (_textbox.SelectionLength < 1)
                {
                    _textbox.Text = _textbox.Text.Insert(_textbox.SelectionStart, number);
                    _textbox.Focus();
                    _textbox.SelectionStart = select + 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    int length = _textbox.SelectionLength;
                    _textbox.Text = _textbox.Text.Remove(select, length);
                    _textbox.Text = _textbox.Text.Insert(select, number);
                    _textbox.Focus();
                    _textbox.SelectionStart = select + 1;
                    _textbox.SelectionLength = 0;
                }
            }
            catch
            {
            }
        }

        #endregion

       
        private char fn_Bodau(char schar)
        {
            char result=new char();
            switch (schar)
            {
                case 'à':
                case 'á':
                case 'ạ':
                case 'ả':
                case 'ã':
                    result='a';
                    break;
                case 'ấ':
                case 'ầ':
                case 'ẩ':
                case 'ậ':
                case 'ẫ':
                    result = 'â';
                    break;
                case 'ắ':
                case 'ằ':
                case 'ẳ':
                case 'ặ':
                case 'ẵ':
                    result = 'ă';
                    break;
                case 'À':
                case 'Á':
                case 'Ạ':
                case 'Ả':
                case 'Ã':
                    result = 'A';
                    break;
                case 'Ấ':
                case 'Ầ':
                case 'Ẩ':
                case 'Ậ':
                case 'Ẫ':
                    result = 'Â';
                    break;
                case 'Ắ':
                case 'Ằ':
                case 'Ẳ':
                case 'Ặ':
                case 'Ẵ':
                    result = 'Ă';
                    break;
                case 'é':
                case 'è':
                case 'ẻ':
                case 'ẹ':
                case 'ẽ':
                    result = 'e';
                    break;
                case 'ế':
                case 'ề':
                case 'ể':
                case 'ễ':
                case 'ệ':
                    result = 'ê';
                    break;
                case 'É':
                case 'È':
                case 'Ẻ':
                case 'Ẹ':
                case 'Ẽ':
                    result = 'E';
                    break;
                case 'Ế':
                case 'Ề':
                case 'Ể':
                case 'Ễ':
                case 'Ệ':
                    result = 'Ê';
                    break;
                case 'ó':
                case 'ò':
                case 'ỏ':
                case 'ọ':
                case 'õ':
                    result = 'o';
                    break;
                case 'ố':
                case 'ồ':
                case 'ổ':
                case 'ộ':
                case 'ỗ':
                    result = 'ô';
                    break;
                case 'ớ':
                case 'ờ':
                case 'ợ':
                case 'ở':
                case 'ỡ':
                    result = 'ơ';
                    break;
                case 'Ó':
                case 'Ò':
                case 'Ỏ':
                case 'Ọ':
                case 'Õ':
                    result = 'O';
                    break;
                case 'Ố':
                case 'Ồ':
                case 'Ổ':
                case 'Ộ':
                case 'Ỗ':
                    result = 'Ô';
                    break;
                case 'Ớ':
                case 'Ờ':
                case 'Ợ':
                case 'Ở':
                case 'Ỡ':
                    result = 'Ơ';
                    break;
                case 'ú':
                case 'ù':
                case 'ủ':
                case 'ụ':
                case 'ũ':
                    result = 'u';
                    break;
                case 'ứ':
                case 'ừ':
                case 'ử':
                case 'ự':
                case 'ữ':
                    result = 'ư';
                    break;
                case 'Ú':
                case 'Ù':
                case 'Ủ':
                case 'Ụ':
                case 'Ũ':
                    result = 'U';
                    break;
                case 'Ứ':
                case 'Ừ':
                case 'Ử':
                case 'Ự':
                case 'Ữ':
                    result = 'Ư';
                    break;
                case 'Ý':
                case 'Ỳ':
                case 'Ỷ':
                case 'Ỵ':
                case 'Ỹ':
                    result = 'Y';
                    break;
                case 'ý':
                case 'ỳ':
                case 'ỵ':
                case 'ỷ':
                case 'ỹ':
                    result = 'y';
                    break;
                default:
                    result = schar;
                    break;
            }
            return result;
        }
        private void btn_Acute_Click(object sender, EventArgs e)
        {
            try
            {
                
                SimpleButton btn = (SimpleButton)sender;
                string stext = btn.Text;


                int select = _textbox.SelectionStart-1;
                switch (fn_Bodau(_textbox.Text[select]))
                {
                    case 'a':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch(stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "á");
                                    break;
                                case "`":
                                     _textbox.Text = _textbox.Text.Insert(select, "à");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ả");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ã");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ạ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                             break;
                        }
                    case 'e':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "é");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "è");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẻ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẽ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẹ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'i':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "í");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ì");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỉ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ĩ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ị");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'o':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ó");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ò");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỏ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "õ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ọ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'u':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ú");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ù");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ủ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ũ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ụ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'ă':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ắ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ằ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẳ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẵ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ặ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'â':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ấ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ầ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẩ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ẫ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ậ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'ê':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ế");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ề");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ể");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ễ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ệ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'ô':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ố");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ồ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ổ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỗ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ộ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'ư':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ứ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ừ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ử");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ữ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ự");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'ơ':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ớ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ờ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ở");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỡ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ợ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'y':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "ý");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỳ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỷ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỹ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "ỵ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                        //chữ hoa
                    case 'A':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Á");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "À");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ả");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ã");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ạ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'E':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "É");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "È");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẻ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẽ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẹ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'I':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Í");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ì");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỉ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ĩ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ị");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'O':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ó");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ò");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỏ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Õ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ọ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'U':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ú");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ù");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ủ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ũ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ụ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Ă':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ắ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ằ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẳ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẵ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ặ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Â':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ấ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ầ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẩ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ẫ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ậ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Ê':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ế");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ề");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ể");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ễ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ệ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Ô':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ố");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ồ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ổ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỗ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ộ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Ư':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ứ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ừ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ử");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ữ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ự");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Ơ':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ớ");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ờ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ở");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỡ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ợ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    case 'Y':
                        {
                            _textbox.Text = _textbox.Text.Remove(select, 1);
                            switch (stext)
                            {
                                case "'":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ý");
                                    break;
                                case "`":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỳ");
                                    break;
                                case "?":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỷ");
                                    break;
                                case "~":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỷ");
                                    break;
                                case ".":
                                    _textbox.Text = _textbox.Text.Insert(select, "Ỵ");
                                    break;
                            }
                            _textbox.Focus();
                            _textbox.SelectionStart = select+1;
                            _textbox.SelectionLength = 0;
                            break;
                        }
                    default:
                        break;
                }
            }
            catch
            {
            }
        }
        public delegate void NextPlease();
        public NextPlease reset;
        private void btn_searchNext_Click(object sender, EventArgs e)
        {
            //reset();
        }

        private void btn_Shift_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (indexshift == 1)
                    indexshift = 0;
                else
                    indexshift = 1;


            }
            catch
            {
            }
        }
    }
}
