﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Text.RegularExpressions;

namespace KP_RES 
{
    public partial class Frm_DoiDiemThuong : DevExpress.XtraEditors.XtraForm
    {
        public Frm_DoiDiemThuong()
        {
            InitializeComponent();

            txtSDT.GotFocus += new EventHandler(txtSDT_GotFocus);
            txtSoCMND.GotFocus += new EventHandler(txtHoTen_GotFocus);
            txtMaKH.GotFocus += new EventHandler(txtMaKH_GotFocus);
        }

        private void LoadQT(string diem)
        {
            string sSQL = "";
            sSQL += "Select MA_QT AS MA,TEN_QT AS TEN,SODIEM,0 as SOLUONG" + "\n";
            sSQL += "From KHTT_DIEMTHUONG_QUATANG" + "\n";
            sSQL += "Where SUDUNG=1 And SODIEM<="+clsMain.SQLString(diem);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gcKHTT.DataSource = dt;
            
        }

        private void KHTT_DoiDiemThuong_Load(object sender, EventArgs e)
        {
            Size sSize = new Size();
            sSize.Width = pn_Keyboard.Size.Width / 13 - 1;
            sSize.Height = pn_Keyboard.Size.Height / 5 - 1;
            us_Keyboard2.SetSize(sSize);

            panelControl1.Width = (panelControl1.Parent.Width / 7) * 4;
        }

        private void txtSDT_GotFocus(object sender, System.EventArgs e)
        {
            us_Keyboard2.SetTextbox(txtSDT, false);
        }

        private void txtHoTen_GotFocus(object sender, System.EventArgs e)
        {
            us_Keyboard2.SetTextbox(txtSoCMND, true);
        }

        private void txtMaKH_GotFocus(object sender, System.EventArgs e)
        {
            us_Keyboard2.SetTextbox(txtMaKH, true );
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSDT.Text.Trim() == "" && txtSoCMND.Text.Trim() == "" && txtMaKH.Text.Trim() == "")
                {
                    throw new Exception("Mời Nhập Thông Tin Tìm Kiếm");
                }
                DataTable dt = new DataTable();
                string sdt = chuanHoaXau(txtSDT.Text);
                string cmnd = chuanHoaXau(txtSoCMND.Text);
                string makh = chuanHoaXau(txtMaKH.Text);

                string sql = string.Format("exec sp_Tim_KHTT @SDT='{0}',@CMND='{1}',@MaKH='{2}'", sdt, cmnd, makh);
                dt = clsMain.ReturnDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    lbMaKH.Text = dt.Rows[0]["MA"].ToString();
                    lbTenKH.Text = dt.Rows[0]["TEN"].ToString();
                    lbDiaChi.Text = dt.Rows[0]["DIACHI"].ToString();
                    lbSDT.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                    lbSoDiemHuuDung.Text = dt.Rows[0]["SODIEMHUUDUNG"].ToString();
                    lbSoDiemTichLuy.Text = dt.Rows[0]["SODIEM"].ToString();
                    lbSoDiem.Text = dt.Rows[0]["SODIEMHUUDUNG"].ToString();
                    LoadQT(dt.Rows[0]["SODIEMHUUDUNG"].ToString());
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private static string chuanHoaXau(string xau)
        {
            StringBuilder kq = new StringBuilder();
            xau = xau.Trim();
            for (int i = 0; i < xau.Length; i++)
            {
                kq.Append(xau[i]);
                if (xau[i] == ' ')
                {
                    while (xau[i] == ' ')
                    {
                        i++;
                    }
                    kq.Append(xau[i]);
                }
            }
            return kq.ToString();
        }

        private void btnDoi_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (double.Parse(gridView1.GetFocusedRowCellValue("SODIEM").ToString()) <= double.Parse(lbSoDiem.Text))
                {
                    lbSoDiem.Text = (double.Parse(lbSoDiem.Text) - double.Parse(gridView1.GetFocusedRowCellValue("SODIEM").ToString())).ToString();
                    gridView1.SetFocusedRowCellValue("SOLUONG", int.Parse(gridView1.GetFocusedRowCellValue("SOLUONG").ToString()) + 1);

                }
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnTru_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (int.Parse(gridView1.GetFocusedRowCellValue("SOLUONG").ToString())>0)
                {
                    lbSoDiem.Text = (double.Parse(lbSoDiem.Text) + double.Parse(gridView1.GetFocusedRowCellValue("SODIEM").ToString()) * double.Parse(gridView1.GetFocusedRowCellValue("SOLUONG").ToString())).ToString();
                    gridView1.SetFocusedRowCellValue("SOLUONG", 0);

                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ReSet()
        {
            txtSDT.Text = txtSoCMND.Text = txtMaKH.Text = "";
            lbMaKH.Text = lbTenKH.Text = lbDiaChi.Text = lbSDT.Text = lbSoDiemHuuDung.Text = lbSoDiemTichLuy.Text = lbSoDiem.Text = "";
            gcKHTT.DataSource = null;
        }

        private void btn_XacNhan_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbMaKH.Text == "")
                {
                    XtraMessageBox.Show("Không tìm thấy khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (gridView1.RowCount <= 0)
                {
                    XtraMessageBox.Show("Số điểm hiện có không đủ để thực hiện đổi quà", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                int n = gridView1.RowCount;
                bool flag = false;
                for (int i = 0; i < n; i++)
                {
                    if (int.Parse(gridView1.GetRowCellValue(i, "SOLUONG").ToString()) > 0)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    XtraMessageBox.Show("Chưa chọn quà để đổi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (DialogResult.Yes == (XtraMessageBox.Show("Xác nhận thực hiện đổi quà ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    double tongdiem = 0;
                    for (int i = 0; i < n; i++)
                    {
                        if (int.Parse(gridView1.GetRowCellValue(i, "SOLUONG").ToString()) > 0)
                        {
                            tongdiem += (double.Parse(gridView1.GetRowCellValue(i, "SOLUONG").ToString()) * double.Parse(gridView1.GetRowCellValue(i, "SODIEM").ToString()));
                        }
                    }

                    string sSQL = "";
                    for (int i = 0; i < n; i++)
                    {
                        if (int.Parse(gridView1.GetRowCellValue(i, "SOLUONG").ToString()) > 0)
                        {
                            sSQL += "INSERT INTO KHTT_LICHSUDOIQUA(MA_KHTT,MA_QT,SOLUONG,SODIEM,TONGDIEM,MANV,NGAYDOI)" + "\n";
                            sSQL += "Values(";
                            sSQL += clsMain.SQLString(lbMaKH.Text) + ",";
                            sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "MA").ToString()) + ",";
                            sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "SOLUONG").ToString()) + ",";
                            sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "SODIEM").ToString()) + ",";
                            sSQL += clsMain.SQLString(tongdiem.ToString()) + ",";
                            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                            sSQL += "GETDATE()" + ")" + "\n";
                        }
                    }
                    sSQL += "UPDATE KHACHHANG_THANTHIET Set" + "\n";
                    sSQL += "SoDiemHuuDung= " + clsMain.SQLString(lbSoDiem.Text) + "\n";
                    sSQL += "where MAKHTT=" + clsMain.SQLString(lbMaKH.Text);

                    if (clsMain.ExecuteSQL(sSQL))
                    {
                        ReSet();
                        XtraMessageBox.Show("Thực hiện đổi quà thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Thực hiện đổi quà thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (View.GetRowCellValue(e.RowHandle, View.Columns["SOLUONG"]).ToString() !="0")
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.Black;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}