﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Khachhang : DevExpress.XtraEditors.XtraForm
    {
        string sma = "";
       
         CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        public Frm_Khachhang()
        {
            InitializeComponent();
            this.Text = rm.GetString("khachhang", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";

        string _chuanhapmakh = "";
        string _makhphainhohon10kitu = "";
        string _chuanhaptenkh = "";
        string _chuanhapcmndkh = "";
        string _chuanhapngaysinhkh = "";
        string _chuanhapsdtkh = "";
        string _dathamgiactktxoa = "";
        string _emailtontai = "";
        string _taikhoantontai = "";
        string _sdttontai = "";
        string _cmndtontai = "";
        string _makhtontai = "";
        private void Frm_THEM_KHTT_Paint(object sender, PaintEventArgs e)
        {
            labelControl7.Text = rm.GetString("ma", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            labelControl9.Text = rm.GetString("cmnd", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            labelControl12.Text = rm.GetString("ngaysinh", culture);
            labelControl11.Text = rm.GetString("gioitinh", culture);
            labelControl1.Text = rm.GetString("diachi", culture);
            labelControl5.Text = rm.GetString("taikhoan", culture);
            labelControl2.Text = rm.GetString("dienthoai", culture);
            //labelControl4.Text = rm.GetString("themmoikh",culture);
            //labelControl13.Text = rm.GetString("themmoikh",culture);


            labelControl3.Text = rm.GetString("matkhau", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            btnDoima.Text = rm.GetString("doima", culture);
            btnInthe.Text = rm.GetString("inthe", culture);
            btnCongdiem.Text = rm.GetString("congdiem", culture);
            btnTrudiem.Text = rm.GetString("trudiem", culture);
            btnNoptien.Text = rm.GetString("naptien", culture);
            btnRuttien.Text = rm.GetString("ruttien", culture);
            btnTaccaKH.Text = rm.GetString("tatcakh", culture);
           // gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["CMND"].Caption = rm.GetString("cmnd", culture);
            gridView2.Columns["NGAYSINH"].Caption = rm.GetString("ngaysinh", culture);
            //gridView2.Columns["GIOITINH"].Caption = rm.GetString("gioitinh", culture);
            gridView2.Columns["DIACHI"].Caption = rm.GetString("diachi", culture);
            gridView2.Columns["DIENTHOAI"].Caption = rm.GetString("dienthoai", culture);
            gridView2.Columns["TAIKHOAN"].Caption = rm.GetString("taikhoan", culture);
            gridView2.Columns["NGAYTAO"].Caption = rm.GetString("ngaythamgia", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);

            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _chuanhapmakh = rm.GetString("cnmkh", culture);
            _makhphainhohon10kitu = rm.GetString("makhphainhohon10kitu", culture);
            _chuanhaptenkh = rm.GetString("cntkh", culture);
            _chuanhapcmndkh = rm.GetString("chuanhapcmndkh", culture);
            _chuanhapngaysinhkh = rm.GetString("chuanhapngaysinhkh", culture);
            _chuanhapsdtkh = rm.GetString("cnsdtkh", culture);
            _dathamgiactktxoa = rm.GetString("dathamgiactktxoa", culture);
            _emailtontai = rm.GetString("emailtontai", culture);
            _taikhoantontai = rm.GetString("taikhoantontai", culture);
            _sdttontai = rm.GetString("sdttontai", culture);
            _cmndtontai = rm.GetString("cmndtontai", culture);
            _makhtontai = rm.GetString("makhtontai", culture);
            
            LoadPermission();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = (clsUserManagement.AllowAdd("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowAdd("58") && clsUserManagement.CheckActive("8")); 
            btnInthe.Enabled = (clsUserManagement.AllowAdd("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowAdd("58") && clsUserManagement.CheckActive("8"));
            btnSua.Enabled = (clsUserManagement.AllowEdit("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowEdit("58") && clsUserManagement.CheckActive("8"));
            btnXoa.Enabled = (clsUserManagement.AllowDelete("52") && clsUserManagement.CheckActive("4")) || (clsUserManagement.AllowDelete("58") && clsUserManagement.CheckActive("8"));
            btnDoima.Enabled = btnXoa.Enabled;
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
            btnCongdiem.Enabled = (clsUserManagement.AllowDelete("52") && clsUserManagement.CheckActive("4")) ;
            btnTrudiem.Enabled = (clsUserManagement.AllowDelete("52") && clsUserManagement.CheckActive("4"));
            btnNoptien.Enabled = (clsUserManagement.AllowDelete("58") && clsUserManagement.CheckActive("8"));
            btnRuttien.Enabled = (clsUserManagement.AllowDelete("58") && clsUserManagement.CheckActive("8"));
        }

        private void LoadCombo()
        {
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
            dt1.Rows.Add("1", "Nam");
            dt1.Rows.Add("0", "Nữ");
            cboGioitinh.Properties.DataSource = dt1;
            cboGioitinh.EditValue = null;
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();
            string sSQL = "";
            sSQL += "Select Top 1000 ID	,MA	,TEN,	CMND,	NGAYSINH,GIOITINH,	case GIOITINH when 0 then N'Nữ' else N'Nam' end as  GIOITINH1	,DIACHI,	DIENTHOAI,	EMAIL,	WEBSITE,	NGAYTAO	,NGUOITAO	,FLAGKHTT,TAIKHOAN,MATKHAU	,SUDUNG" + "\n";
            sSQL += "From KHTT " + "\n";
            sSQL += "Order by NGAYTAO desc " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private void SetRongControl()
        {
            sma = "";
            txtMaKH.Text = "";
            cboGioitinh.EditValue = cboGioitinh.Properties.GetDataSourceValue(cboGioitinh.Properties.ValueMember, 0);
            txtTEN.Text = "";
            txtDIACHI.Text = "";
            txtDIENTHOAI.Text = "";
            txtEMAIL.Text = "";
            chkSUDUNG.Checked = true;
            txtCMND.Text = "";
            txtTaikhoan .Text = "";
            txtMatkhau .Text = "";
            dtpNgaysinh.EditValue = DateTime.Now;
            txtWebsite.Text = "";
        }

        private void KhoaMoControl(bool tf)
        {

            if (tf == true)//khóa
            {
                txtMaKH.Properties.ReadOnly = true;
                txtTEN.Properties.ReadOnly = true;
                txtDIACHI.Properties.ReadOnly = true;
                txtDIENTHOAI.Properties.ReadOnly = true;
                txtEMAIL.Properties.ReadOnly = true;
                txtWebsite.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                txtCMND.Properties.ReadOnly = true;
                txtTaikhoan .Properties.ReadOnly = true;
                txtMatkhau .Properties.ReadOnly = true;
                dtpNgaysinh.Properties.ReadOnly = true;
                cboGioitinh.Properties.ReadOnly = true;

            }
            else//mở
            {
                txtMaKH.Properties.ReadOnly = false;
                txtTEN.Properties.ReadOnly = false;
                txtDIACHI.Properties.ReadOnly = false;
                txtDIENTHOAI.Properties.ReadOnly = false;
                txtEMAIL.Properties.ReadOnly = false;
                txtWebsite.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                txtCMND.Properties.ReadOnly = false;
                txtTaikhoan .Properties.ReadOnly = false;
                txtMatkhau .Properties.ReadOnly = false;
                dtpNgaysinh.Properties.ReadOnly = false;
                cboGioitinh.Properties.ReadOnly = false;
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;
            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtMaKH.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtDIACHI.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIACHI").ToString();
            txtDIENTHOAI.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            txtEMAIL.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            txtWebsite.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "WEBSITE").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            txtCMND.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CMND").ToString();
            txtTaikhoan .Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TAIKHOAN").ToString();
            txtMatkhau .Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MATKHAU").ToString();
            dtpNgaysinh.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString());
            cboGioitinh.EditValue = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOITINH").ToString()) == true ? "1" : "0";
            KhoaMoControl(true);
        }
       
        string smaKH = "";
        private void txtMaKH_Leave(object sender, EventArgs e)
        {
            smaKH = "";
            if (txtMaKH.Text.Trim() == "" && txtMaKH.Properties.ReadOnly == false)
            {
                DataTable dtMaKH = clsMain.ReturnDataTable("select top(1) MA from KHTT where FLAGKHTT=1 order by MA desc");
                if (dtMaKH.Rows.Count == 0)
                {
                    smaKH = "KH00000001";
                }
                else
                {
                    smaKH = "KH" + string.Format("{0:00000000}", int.Parse(dtMaKH.Rows[0][0].ToString().Substring(2, dtMaKH.Rows[0][0].ToString().Length - 2)) + 1);
                }
                txtMaKH.Text = smaKH;
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtMaKH.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
           
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (!CheckXoa())
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQLKHTT = "delete from KHACHHANG_THANTHIET where MAKHTT="+clsMain.SQLString(sma);
            string sSQLKHTTNB = "delete from KHACHHANG_THANHTOAN_NOIBO where MAKHTTNB=" + clsMain.SQLString(sma);
            string sKHTT = "delete from KHTT where MA=" + clsMain.SQLString(sma);
            string sSQL =  sSQLKHTT + "\n" + sSQLKHTTNB + "\n" + sKHTT;
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công" ,"Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
    }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtMaKH.Properties.ReadOnly = true;
            txtTEN.Focus();
            txtTEN.SelectionStart = txtTEN.Text.Length;
        }

        private void btnInthe_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            DataTable dt = new DataTable();
            dt.Columns.Add("MAKH", typeof(string));
            dt.Columns.Add("TENKH", typeof(string));
            dt.Columns.Add("NGAYSINH", typeof(string));
            DataRow row = dt.NewRow();
            row["MAKH"] = txtMaKH .Text ;
            row["TENKH"] = txtTEN .Text ;
            row["NGAYSINH"] = dtpNgaysinh.Text ;
            dt.Rows.Add(row);
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.dtSource = dt;
            frm.Mode = 20;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnCongdiem_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Congtrudiem frm = new Frm_Congtrudiem(sma, true);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void brnTrudiem_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Congtrudiem frm = new Frm_Congtrudiem(sma, false );
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            if (!CheckMAKH(txtMaKH.Text))
            {
                txtMaKH.Focus();
                return;
            }
            if (!CheckCMND(txtCMND.Text))
            {
                txtCMND.Focus();
                return;
            }
            if (!CheckSDT(txtDIENTHOAI.Text))
            {
                txtDIENTHOAI.Focus();
                return;
            }
            if (!CheckTaiKhoan(txtTaikhoan .Text))
            {
                txtTaikhoan.Focus();
                return;
            }
            if (!CheckEmail(txtEMAIL.Text))
            {
                txtEMAIL.Focus();
                return;
            }

            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into KHTT (MA,TEN,CMND,NGAYSINH,GIOITINH,DIACHI,DIENTHOAI,EMAIL,WEBSITE,NGAYTAO,NGUOITAO,FLAGKHTT,TAIKHOAN,MATKHAU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(txtMaKH.Text ) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLString(txtCMND.Text) + ",";
                sSQL += clsMain.SQLString(dtpNgaysinh.Text) + ",";
                sSQL += clsMain.SQLString(cboGioitinh.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDIACHI.Text) + ",";
                sSQL += clsMain.SQLString(txtDIENTHOAI.Text) + ",";
                sSQL += clsMain.SQLString(txtEMAIL.Text) + ",";
                sSQL += clsMain.SQLString(txtWebsite.Text) + ",";
                sSQL += "GetDate()" + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                if (txtMaKH.Text == smaKH)
                {
                    sSQL += clsMain.SQLString("1") + ",";
                }
                else
                {
                    sSQL += clsMain.SQLString("0") + ",";
                }
                sSQL += clsMain.SQLString(txtTaikhoan .Text) + ",";
                sSQL += clsMain.SQLString(txtMatkhau.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")" +"\n";

                sSQL += "Insert into KHACHHANG_THANTHIET (Ma_KHTT,SoLe,SoDiem,MACAPDO,SoLanChoThuong,SoLanDaDuocThuong,SuDung,SoLanReset,SoLanThuongReSet,SoLanTinh,NgayThamGia,LastUpdate,LoaiThanhVien,MAKHTT,FLAGKHTT)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString(cls_KHTT .ReturnCapDoThe (0)) + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += "GetDate()" + ",";
                sSQL += "GetDate()" + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += clsMain.SQLString("0") +")" + "\n";

                sSQL += "Insert into KHACHHANG_THANHTOAN_NOIBO(Ma_KHTT,SoDuTaiKhoan,SuDung,NgayThamGia,MAKHTTNB,FLAGKHTTNB) " + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ",";
                sSQL += "GetDate()" + ",";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += clsMain.SQLString("0") + ")" + "\n";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update KHTT Set " + "\n";
                sSQL += "TEN=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "CMND=" + clsMain.SQLString(txtCMND.Text) + "," + "\n";
                sSQL += "NGAYSINH=" + clsMain.SQLString(dtpNgaysinh.Text) + "," + "\n";
                sSQL += "GIOITINH=" + clsMain.SQLString(cboGioitinh.EditValue.ToString()) + "," + "\n";
                sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtDIACHI.Text) + "," + "\n";
                sSQL += "DIENTHOAI=" + clsMain.SQLString(txtDIENTHOAI.Text) + "," + "\n";
                sSQL += "EMAIL=" + clsMain.SQLString(txtEMAIL.Text) + "," + "\n";
                sSQL += "WEBSITE=" + clsMain.SQLString(txtWebsite.Text) + "," + "\n";
                sSQL += "TAIKHOAN=" + clsMain.SQLString(txtTaikhoan .Text) + "," + "\n";
                sSQL += "MATKHAU=" + clsMain.SQLString(txtMatkhau .Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA ="+ clsMain .SQLString (sma )+"\n";

                sSQL += "Update KHACHHANG_THANTHIET Set " + "\n";
                sSQL += "LastUpdate=getdate()"  + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_KHTT =" + clsMain.SQLString(sma) + "\n";

                sSQL += "Update KHACHHANG_THANHTOAN_NOIBO Set " + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_KHTT =" + clsMain.SQLString(sma) + "\n";
            }

            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput()
        {
            if (txtMaKH.Text.Trim() == "")
            {
                txtMaKH.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Mã Khách Hàng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);               
                return false;
            }
            if (txtMaKH.Text.Length > 10)
            {
                XtraMessageBox.Show("Mã Khách Hàng Không Thể Lớn Hơn 10 Ký Tự . Yêu Cầu Nhập Lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaKH.Focus();
                return false;
            }
            if ( txtTEN.Text.Trim() == "" )
            {
                txtTEN.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Tên Khách Hàng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);               
                return false;
            }
            if (txtCMND.Text.Trim() == "")
            {
                txtCMND.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Số Chứng Minh Nhân Dân  Khách Hàng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (((DateTime)dtpNgaysinh.EditValue).Year < 1901)
            {
                dtpNgaysinh.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Ngày Sinh Khách Hàng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (txtDIENTHOAI.Text.Trim() == "")
            {
                txtDIENTHOAI.Focus();
                DevExpress.XtraEditors.XtraMessageBox.Show("Bạn Chưa Nhập Số Điện Thoại Khách Hàng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (txtTaikhoan.Text.Trim() == "")
            {
                txtTaikhoan.Text = txtMaKH.Text;
            }
            if (txtMatkhau.Text.Trim() == "")
            {
                txtMatkhau.Text = txtMaKH.Text;
            }
            if (txtEMAIL.Text.Trim() == "")
            {
                txtEMAIL.Text = txtMaKH.Text;
            }
           
            return true;
        }

        private Boolean CheckMAKH(string maKH)
        {
            string sSQL = "";
            if (sma == "")
            {
                sSQL += "select MA from  KHTT  where MA=" + clsMain.SQLString(maKH);
            }
            else
            {
                sSQL += "select MA from  KHTT  where MA=" + clsMain.SQLString("");
            }
            DataTable dtsdt = clsMain.ReturnDataTable(sSQL);
            if (dtsdt.Rows.Count > 0)
            {
                XtraMessageBox.Show("Mã Khách Hàng Này Đã Tồn Tại,Bạn Không Thể Tạo Mới Được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private Boolean CheckCMND(string cmnd)
        {
            string sSQL = "";
            if (sma == "")
            {
                sSQL += "select CMND from KHTT where CMND=" + clsMain.SQLString(cmnd);
            }
            else
            {
                sSQL += "select CMND from KHTT where CMND=" + clsMain.SQLString(cmnd) + " and MA<>" + clsMain.SQLString(sma);
                
            }
            DataTable dtcmnd = clsMain.ReturnDataTable(sSQL);
            if (dtcmnd.Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Số CMND Này Đã Đăng Ký Thành Viên,Bạn Không Thể Đăng Ký Được Nữa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private Boolean CheckSDT(string sdt)
        {
            string sSQL = "";
            if (sma == "")
            {
                sSQL += "select DIENTHOAI from KHTT where DIENTHOAI=" + clsMain.SQLString(sdt);
            }
            else
            {
                sSQL += "select DIENTHOAI from KHTT where DIENTHOAI=" + clsMain.SQLString(sdt) + " and MA<>" + clsMain.SQLString(sma);
                
            }
            DataTable dtsdt = clsMain.ReturnDataTable(sSQL);
            if (dtsdt.Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Số Điện Thoại Này Đã Đăng Ký Thành Viên,Bạn Không Thể Đăng Ký Được Nữa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private Boolean CheckTaiKhoan(string TaiKhoan)
        {
            string sSQL = "";
            if (sma == "")
            {
                sSQL += "select TAIKHOAN from  KHTT  where TAIKHOAN=" + clsMain.SQLString(TaiKhoan);
            }
            else
            {
                sSQL += "select TAIKHOAN from KHTT where TAIKHOAN=" + clsMain.SQLString(TaiKhoan) + " and MA<>" + clsMain.SQLString(sma);

            }
            DataTable dtsdt = clsMain.ReturnDataTable(sSQL);
            if (dtsdt.Rows.Count > 0)
            {
                XtraMessageBox.Show("Tài Khoản Khách Hàng Này Đã Tồn Tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private Boolean CheckEmail(string Email)
        {
            string sSQL = "";
            if (sma == "")
            {
                sSQL += "select EMAIL from  KHTT  where EMAIL=" + clsMain.SQLString(Email);
            }
            else
            {
                sSQL += "select EMAIL from KHTT where EMAIL=" + clsMain.SQLString(Email) + " and MA<>" + clsMain.SQLString(sma);

            }
            DataTable dtEmail = clsMain.ReturnDataTable(sSQL);
            if (dtEmail.Rows.Count > 0)
            {
                XtraMessageBox.Show("Email Khách Hàng Này Đã Tồn Tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private Boolean CheckXoa()
        {
            string sSQL = "";
            sSQL += "select Ma_KHTT from KHACHHANG_THANTHIET where SoDiem>0 and MAKHTT =" + clsMain.SQLString(sma);
            DataTable dtKHTT = clsMain.ReturnDataTable(sSQL);
            sSQL += "select Ma_KHTT from KHACHHANG_THANHTOAN_NOIBO where SoDuTaiKhoan >0 and MAKHTTNB =" + clsMain.SQLString(sma);
            DataTable dtTTNB = clsMain.ReturnDataTable(sSQL);
            if (dtKHTT.Rows.Count > 0 || dtTTNB.Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Khách hàng này có đã tham gia chương trình không thể xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void btnNoptien_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Congtrutien frm = new Frm_Congtrutien(sma, true);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnRuttien_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Congtrutien frm = new Frm_Congtrutien(sma, false);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnDoima_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Doima frm = new Frm_Doima(sma);
            frm.ShowDialog();
            frm.Dispose();
            LoaddataGridView();
            btnThem.Focus();
        }

        private void btnTaccaKH_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();
            string sSQL = "";
            sSQL += "Select ID	,MA	,TEN,	CMND,	NGAYSINH,GIOITINH,	case GIOITINH when 0 then N'Nữ' else N'Nam' end as  GIOITINH1	,DIACHI,	DIENTHOAI,	EMAIL,	WEBSITE,	NGAYTAO	,NGUOITAO	,FLAGKHTT,TAIKHOAN,MATKHAU	,SUDUNG" + "\n";
            sSQL += "From KHTT " + "\n";
            sSQL += "Order by NGAYTAO desc " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }
    }
}