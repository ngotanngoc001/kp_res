﻿namespace KP_RES 
{
    partial class Frm_DoiDiemThuong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_DoiDiemThuong));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.lbSoDiem = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lbSoDiemTichLuy = new DevExpress.XtraEditors.TextEdit();
            this.lbMaKH = new DevExpress.XtraEditors.TextEdit();
            this.lbTenKH = new DevExpress.XtraEditors.TextEdit();
            this.lbSDT = new DevExpress.XtraEditors.TextEdit();
            this.lbDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.lbSoDiemHuuDung = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gcKHTT = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCong = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SODIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HUY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnTru = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pn_Keyboard = new DevExpress.XtraEditors.PanelControl();
            this.us_Keyboard2 = new KP_RES.Us_Keyboard1();
            this.pn_search = new DevExpress.XtraEditors.PanelControl();
            this.pnThongTin = new DevExpress.XtraEditors.PanelControl();
            this.txtSDT = new DevExpress.XtraEditors.TextEdit();
            this.txtSoCMND = new DevExpress.XtraEditors.TextEdit();
            this.txtMaKH = new DevExpress.XtraEditors.TextEdit();
            this.lbMaKhacHang = new DevExpress.XtraEditors.LabelControl();
            this.lbHoTen = new DevExpress.XtraEditors.LabelControl();
            this.lbSoDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.pnSearch = new DevExpress.XtraEditors.PanelControl();
            this.btn_search = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XacNhan = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiemTichLuy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTenKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiemHuuDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcKHTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Keyboard)).BeginInit();
            this.pn_Keyboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_search)).BeginInit();
            this.pn_search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThongTin)).BeginInit();
            this.pnThongTin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnSearch)).BeginInit();
            this.pnSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Controls.Add(this.pn_Keyboard);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 108);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1091, 564);
            this.panelControl5.TabIndex = 16;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1087, 285);
            this.panelControl2.TabIndex = 18;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.lbSoDiem);
            this.panelControl7.Controls.Add(this.labelControl9);
            this.panelControl7.Controls.Add(this.lbSoDiemTichLuy);
            this.panelControl7.Controls.Add(this.lbMaKH);
            this.panelControl7.Controls.Add(this.lbTenKH);
            this.panelControl7.Controls.Add(this.lbSDT);
            this.panelControl7.Controls.Add(this.lbDiaChi);
            this.panelControl7.Controls.Add(this.lbSoDiemHuuDung);
            this.panelControl7.Controls.Add(this.labelControl2);
            this.panelControl7.Controls.Add(this.labelControl1);
            this.panelControl7.Controls.Add(this.labelControl3);
            this.panelControl7.Controls.Add(this.labelControl4);
            this.panelControl7.Controls.Add(this.labelControl5);
            this.panelControl7.Controls.Add(this.labelControl6);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(454, 281);
            this.panelControl7.TabIndex = 16;
            // 
            // lbSoDiem
            // 
            this.lbSoDiem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSoDiem.Location = new System.Drawing.Point(158, 208);
            this.lbSoDiem.Name = "lbSoDiem";
            this.lbSoDiem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.lbSoDiem.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbSoDiem.Properties.Appearance.Options.UseFont = true;
            this.lbSoDiem.Properties.Appearance.Options.UseForeColor = true;
            this.lbSoDiem.Properties.Appearance.Options.UseTextOptions = true;
            this.lbSoDiem.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbSoDiem.Size = new System.Drawing.Size(292, 28);
            this.lbSoDiem.TabIndex = 23;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl9.Location = new System.Drawing.Point(8, 212);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(115, 19);
            this.labelControl9.TabIndex = 22;
            this.labelControl9.Text = "Số điểm còn lại:";
            // 
            // lbSoDiemTichLuy
            // 
            this.lbSoDiemTichLuy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSoDiemTichLuy.Location = new System.Drawing.Point(158, 140);
            this.lbSoDiemTichLuy.Name = "lbSoDiemTichLuy";
            this.lbSoDiemTichLuy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lbSoDiemTichLuy.Properties.Appearance.Options.UseFont = true;
            this.lbSoDiemTichLuy.Properties.Appearance.Options.UseTextOptions = true;
            this.lbSoDiemTichLuy.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbSoDiemTichLuy.Size = new System.Drawing.Size(292, 28);
            this.lbSoDiemTichLuy.TabIndex = 19;
            // 
            // lbMaKH
            // 
            this.lbMaKH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMaKH.Location = new System.Drawing.Point(158, 4);
            this.lbMaKH.Name = "lbMaKH";
            this.lbMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lbMaKH.Properties.Appearance.Options.UseFont = true;
            this.lbMaKH.Size = new System.Drawing.Size(292, 28);
            this.lbMaKH.TabIndex = 18;
            // 
            // lbTenKH
            // 
            this.lbTenKH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTenKH.Location = new System.Drawing.Point(158, 38);
            this.lbTenKH.Name = "lbTenKH";
            this.lbTenKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lbTenKH.Properties.Appearance.Options.UseFont = true;
            this.lbTenKH.Size = new System.Drawing.Size(292, 28);
            this.lbTenKH.TabIndex = 17;
            // 
            // lbSDT
            // 
            this.lbSDT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSDT.Location = new System.Drawing.Point(158, 72);
            this.lbSDT.Name = "lbSDT";
            this.lbSDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lbSDT.Properties.Appearance.Options.UseFont = true;
            this.lbSDT.Size = new System.Drawing.Size(292, 28);
            this.lbSDT.TabIndex = 16;
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDiaChi.Location = new System.Drawing.Point(158, 106);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.lbDiaChi.Properties.Appearance.Options.UseFont = true;
            this.lbDiaChi.Size = new System.Drawing.Size(292, 28);
            this.lbDiaChi.TabIndex = 15;
            // 
            // lbSoDiemHuuDung
            // 
            this.lbSoDiemHuuDung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSoDiemHuuDung.Location = new System.Drawing.Point(158, 174);
            this.lbSoDiemHuuDung.Name = "lbSoDiemHuuDung";
            this.lbSoDiemHuuDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.lbSoDiemHuuDung.Properties.Appearance.Options.UseFont = true;
            this.lbSoDiemHuuDung.Properties.Appearance.Options.UseTextOptions = true;
            this.lbSoDiemHuuDung.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbSoDiemHuuDung.Size = new System.Drawing.Size(292, 28);
            this.lbSoDiemHuuDung.TabIndex = 14;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(7, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(111, 19);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Mã khách kàng:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(8, 76);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Số điện thoại:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(7, 42);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 19);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Họ tên:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Location = new System.Drawing.Point(7, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 19);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Địa chỉ:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(8, 178);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(138, 19);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Số điểm hữu dụng:";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl6.Location = new System.Drawing.Point(7, 144);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(120, 19);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Số điểm tích lũy:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(456, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(629, 281);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.gcKHTT);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(625, 277);
            this.panelControl3.TabIndex = 15;
            // 
            // gcKHTT
            // 
            this.gcKHTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcKHTT.Location = new System.Drawing.Point(0, 0);
            this.gcKHTT.MainView = this.gridView1;
            this.gcKHTT.Name = "gcKHTT";
            this.gcKHTT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnCong,
            this.btnTru});
            this.gcKHTT.Size = new System.Drawing.Size(625, 277);
            this.gcKHTT.TabIndex = 14;
            this.gcKHTT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 35;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DOI,
            this.STT,
            this.MA,
            this.TEN,
            this.SODIEM,
            this.SOLUONG,
            this.HUY});
            this.gridView1.GridControl = this.gcKHTT;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.PreviewLineCount = 25;
            this.gridView1.RowHeight = 45;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // DOI
            // 
            this.DOI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DOI.AppearanceHeader.Options.UseFont = true;
            this.DOI.AppearanceHeader.Options.UseTextOptions = true;
            this.DOI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DOI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DOI.Caption = "Chọn";
            this.DOI.ColumnEdit = this.btnCong;
            this.DOI.FieldName = "DOI";
            this.DOI.MinWidth = 83;
            this.DOI.Name = "DOI";
            this.DOI.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.DOI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.DOI.OptionsColumn.AllowMove = false;
            this.DOI.OptionsColumn.AllowShowHide = false;
            this.DOI.OptionsColumn.AllowSize = false;
            this.DOI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.DOI.OptionsFilter.AllowAutoFilter = false;
            this.DOI.OptionsFilter.AllowFilter = false;
            this.DOI.Visible = true;
            this.DOI.VisibleIndex = 0;
            this.DOI.Width = 83;
            // 
            // btnCong
            // 
            this.btnCong.AutoHeight = false;
            this.btnCong.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnCong.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnCong.Name = "btnCong";
            this.btnCong.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnCong.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDoi_ButtonClick);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.Width = 48;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA.AppearanceCell.Options.UseFont = true;
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.Caption = "Mã ";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.Width = 142;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Quà tặng";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.AllowMove = false;
            this.TEN.OptionsColumn.AllowShowHide = false;
            this.TEN.OptionsFilter.AllowAutoFilter = false;
            this.TEN.OptionsFilter.AllowFilter = false;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 286;
            // 
            // SODIEM
            // 
            this.SODIEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SODIEM.AppearanceCell.Options.UseFont = true;
            this.SODIEM.AppearanceCell.Options.UseTextOptions = true;
            this.SODIEM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SODIEM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SODIEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SODIEM.AppearanceHeader.Options.UseFont = true;
            this.SODIEM.AppearanceHeader.Options.UseTextOptions = true;
            this.SODIEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SODIEM.Caption = "Số điểm";
            this.SODIEM.FieldName = "SODIEM";
            this.SODIEM.MinWidth = 90;
            this.SODIEM.Name = "SODIEM";
            this.SODIEM.OptionsColumn.AllowEdit = false;
            this.SODIEM.OptionsColumn.AllowFocus = false;
            this.SODIEM.OptionsFilter.AllowAutoFilter = false;
            this.SODIEM.OptionsFilter.AllowFilter = false;
            this.SODIEM.Visible = true;
            this.SODIEM.VisibleIndex = 3;
            this.SODIEM.Width = 90;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "Số lượng";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.MinWidth = 81;
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsFilter.AllowAutoFilter = false;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 4;
            this.SOLUONG.Width = 81;
            // 
            // HUY
            // 
            this.HUY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.HUY.AppearanceHeader.Options.UseFont = true;
            this.HUY.AppearanceHeader.Options.UseTextOptions = true;
            this.HUY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HUY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HUY.Caption = "Bỏ chọn";
            this.HUY.ColumnEdit = this.btnTru;
            this.HUY.FieldName = "HUY";
            this.HUY.MinWidth = 83;
            this.HUY.Name = "HUY";
            this.HUY.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.HUY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.HUY.OptionsColumn.AllowSize = false;
            this.HUY.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.HUY.OptionsFilter.AllowAutoFilter = false;
            this.HUY.OptionsFilter.AllowFilter = false;
            this.HUY.Visible = true;
            this.HUY.VisibleIndex = 1;
            this.HUY.Width = 83;
            // 
            // btnTru
            // 
            this.btnTru.AutoHeight = false;
            this.btnTru.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnTru.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnTru.Name = "btnTru";
            this.btnTru.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnTru.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnTru_ButtonClick);
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(625, 0);
            this.panelControl4.TabIndex = 16;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(115, 5);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(245, 19);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Danh sách quà tặng có thể đổi";
            // 
            // pn_Keyboard
            // 
            this.pn_Keyboard.Controls.Add(this.us_Keyboard2);
            this.pn_Keyboard.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_Keyboard.Location = new System.Drawing.Point(2, 287);
            this.pn_Keyboard.Name = "pn_Keyboard";
            this.pn_Keyboard.Size = new System.Drawing.Size(1087, 275);
            this.pn_Keyboard.TabIndex = 17;
            // 
            // us_Keyboard2
            // 
            this.us_Keyboard2.BackColor = System.Drawing.Color.LightGray;
            this.us_Keyboard2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.us_Keyboard2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.us_Keyboard2.Location = new System.Drawing.Point(2, 2);
            this.us_Keyboard2.Name = "us_Keyboard2";
            this.us_Keyboard2.Size = new System.Drawing.Size(1083, 271);
            this.us_Keyboard2.TabIndex = 0;
            // 
            // pn_search
            // 
            this.pn_search.Controls.Add(this.pnThongTin);
            this.pn_search.Controls.Add(this.pnSearch);
            this.pn_search.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_search.Location = new System.Drawing.Point(0, 0);
            this.pn_search.Name = "pn_search";
            this.pn_search.Size = new System.Drawing.Size(1091, 108);
            this.pn_search.TabIndex = 6;
            // 
            // pnThongTin
            // 
            this.pnThongTin.Controls.Add(this.txtSDT);
            this.pnThongTin.Controls.Add(this.txtSoCMND);
            this.pnThongTin.Controls.Add(this.txtMaKH);
            this.pnThongTin.Controls.Add(this.lbMaKhacHang);
            this.pnThongTin.Controls.Add(this.lbHoTen);
            this.pnThongTin.Controls.Add(this.lbSoDienThoai);
            this.pnThongTin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThongTin.Location = new System.Drawing.Point(2, 2);
            this.pnThongTin.Name = "pnThongTin";
            this.pnThongTin.Size = new System.Drawing.Size(827, 104);
            this.pnThongTin.TabIndex = 0;
            // 
            // txtSDT
            // 
            this.txtSDT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSDT.Location = new System.Drawing.Point(158, 38);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txtSDT.Properties.Appearance.Options.UseFont = true;
            this.txtSDT.Properties.DisplayFormat.FormatString = "d";
            this.txtSDT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSDT.Properties.EditFormat.FormatString = "d";
            this.txtSDT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSDT.Size = new System.Drawing.Size(664, 28);
            this.txtSDT.TabIndex = 3;
            // 
            // txtSoCMND
            // 
            this.txtSoCMND.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoCMND.Location = new System.Drawing.Point(158, 72);
            this.txtSoCMND.Name = "txtSoCMND";
            this.txtSoCMND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txtSoCMND.Properties.Appearance.Options.UseFont = true;
            this.txtSoCMND.Size = new System.Drawing.Size(664, 28);
            this.txtSoCMND.TabIndex = 5;
            // 
            // txtMaKH
            // 
            this.txtMaKH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaKH.Location = new System.Drawing.Point(158, 4);
            this.txtMaKH.Name = "txtMaKH";
            this.txtMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txtMaKH.Properties.Appearance.Options.UseFont = true;
            this.txtMaKH.Size = new System.Drawing.Size(664, 28);
            this.txtMaKH.TabIndex = 1;
            // 
            // lbMaKhacHang
            // 
            this.lbMaKhacHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbMaKhacHang.Location = new System.Drawing.Point(9, 5);
            this.lbMaKhacHang.Name = "lbMaKhacHang";
            this.lbMaKhacHang.Size = new System.Drawing.Size(115, 19);
            this.lbMaKhacHang.TabIndex = 0;
            this.lbMaKhacHang.Text = "Mã Khách Hàng:";
            // 
            // lbHoTen
            // 
            this.lbHoTen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbHoTen.Location = new System.Drawing.Point(9, 76);
            this.lbHoTen.Name = "lbHoTen";
            this.lbHoTen.Size = new System.Drawing.Size(73, 19);
            this.lbHoTen.TabIndex = 4;
            this.lbHoTen.Text = "Số CMND:";
            // 
            // lbSoDienThoai
            // 
            this.lbSoDienThoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbSoDienThoai.Location = new System.Drawing.Point(9, 42);
            this.lbSoDienThoai.Name = "lbSoDienThoai";
            this.lbSoDienThoai.Size = new System.Drawing.Size(106, 19);
            this.lbSoDienThoai.TabIndex = 2;
            this.lbSoDienThoai.Text = "Số Điện Thoại:";
            // 
            // pnSearch
            // 
            this.pnSearch.Controls.Add(this.btn_search);
            this.pnSearch.Controls.Add(this.btn_XacNhan);
            this.pnSearch.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnSearch.Location = new System.Drawing.Point(829, 2);
            this.pnSearch.Name = "pnSearch";
            this.pnSearch.Size = new System.Drawing.Size(260, 104);
            this.pnSearch.TabIndex = 5;
            // 
            // btn_search
            // 
            this.btn_search.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btn_search.Appearance.Options.UseFont = true;
            this.btn_search.Image = global::KP_RES.Properties.Resources.search_26;
            this.btn_search.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn_search.Location = new System.Drawing.Point(3, 4);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(123, 96);
            this.btn_search.TabIndex = 0;
            this.btn_search.Text = "Tìm";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // btn_XacNhan
            // 
            this.btn_XacNhan.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btn_XacNhan.Appearance.Options.UseFont = true;
            this.btn_XacNhan.Appearance.Options.UseTextOptions = true;
            this.btn_XacNhan.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btn_XacNhan.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btn_XacNhan.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn_XacNhan.Location = new System.Drawing.Point(132, 4);
            this.btn_XacNhan.Name = "btn_XacNhan";
            this.btn_XacNhan.Size = new System.Drawing.Size(123, 96);
            this.btn_XacNhan.TabIndex = 5;
            this.btn_XacNhan.Text = "Xác nhận đổi quà";
            this.btn_XacNhan.Click += new System.EventHandler(this.btn_XacNhan_Click);
            // 
            // Frm_DoiDiemThuong
            // 
            this.AcceptButton = this.btn_search;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 672);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.pn_search);
            this.KeyPreview = true;
            this.Name = "Frm_DoiDiemThuong";
            this.Text = "Đổi điểm thưởng";
            this.Load += new System.EventHandler(this.KHTT_DoiDiemThuong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiemTichLuy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTenKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSoDiemHuuDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcKHTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Keyboard)).EndInit();
            this.pn_Keyboard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_search)).EndInit();
            this.pn_search.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThongTin)).EndInit();
            this.pnThongTin.ResumeLayout(false);
            this.pnThongTin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnSearch)).EndInit();
            this.pnSearch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pn_search;
        private DevExpress.XtraEditors.PanelControl pnThongTin;
        private DevExpress.XtraEditors.TextEdit txtSDT;
        private DevExpress.XtraEditors.TextEdit txtSoCMND;
        private DevExpress.XtraEditors.TextEdit txtMaKH;
        private DevExpress.XtraEditors.LabelControl lbMaKhacHang;
        private DevExpress.XtraEditors.LabelControl lbHoTen;
        private DevExpress.XtraEditors.LabelControl lbSoDienThoai;
        private DevExpress.XtraEditors.PanelControl pnSearch;
        private DevExpress.XtraEditors.SimpleButton btn_search;
        private DevExpress.XtraEditors.SimpleButton btn_XacNhan;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl pn_Keyboard;
        private Us_Keyboard1 us_Keyboard2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gcKHTT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SODIEM;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnCong;
        private DevExpress.XtraGrid.Columns.GridColumn DOI;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnTru;
        private DevExpress.XtraGrid.Columns.GridColumn HUY;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.TextEdit lbSoDiemTichLuy;
        private DevExpress.XtraEditors.TextEdit lbMaKH;
        private DevExpress.XtraEditors.TextEdit lbTenKH;
        private DevExpress.XtraEditors.TextEdit lbSDT;
        private DevExpress.XtraEditors.TextEdit lbDiaChi;
        private DevExpress.XtraEditors.TextEdit lbSoDiemHuuDung;
        private DevExpress.XtraEditors.TextEdit lbSoDiem;
        private DevExpress.XtraEditors.LabelControl labelControl9;




    }
}