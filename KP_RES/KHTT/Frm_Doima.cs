﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Doima : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Doima(string maKH)
        {
            InitializeComponent();
            maKH_Old = maKH;
        }

        string maKH_Old;
        private void btnChapNhan_Click(object sender, EventArgs e)
        {
            if ( txtPassNew.Text == "" || txtReWritPassNew.Text == "")
            {
                XtraMessageBox.Show("Chưa nhập đủ thông tin", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtPassNew.Text != txtReWritPassNew.Text)
            {
                XtraMessageBox.Show("Mã mới không khớp", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!CheckMAKH(txtPassNew.Text))
            {
                txtPassNew.Focus();
                return;
            }
            string sSQL = "";
            sSQL += "Update KHTT Set " + "\n";
            sSQL += "MA=" + clsMain.SQLString(txtPassNew.Text) + "\n";
            sSQL += "Where MA =" + clsMain.SQLString(maKH_Old) + "\n";

            sSQL += "Update KHACHHANG_THANTHIET Set " + "\n";
            sSQL += "Ma_KHTT=" + clsMain.SQLString(txtPassNew.Text) + "," + "\n";
            sSQL += "MAKHTT=" + clsMain.SQLString(txtPassNew.Text) + "\n";
            sSQL += "Where MA_KHTT =" + clsMain.SQLString(maKH_Old) + "\n";

            sSQL += "Update KHACHHANG_THANHTOAN_NOIBO Set " + "\n";
            sSQL += "Ma_KHTT=" + clsMain.SQLString(txtPassNew.Text) + "," + "\n";
            sSQL += "MAKHTTNB=" + clsMain.SQLString(txtPassNew.Text) + "\n";
            sSQL += "Where MA_KHTT =" + clsMain.SQLString(maKH_Old) + "\n";

            sSQL += "EXEC SP_InsertLichSuDoiMaTheKHTT " + "\n";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + "," + "\n";
            sSQL += clsMain.SQLString(maKH_Old) + "," + "\n";
            sSQL += clsMain.SQLString(txtPassNew.Text) + "\n";

            if (clsMain.ExecuteSQL(sSQL))
            {
                XtraMessageBox.Show("Đổi mã thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassNew.Text = "";
                txtReWritPassNew.Text = "";
                this.Close();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckMAKH(string maKH)
        {
            string sSQL = "";
            sSQL += "select MA from  KHTT  where MA=" + clsMain.SQLString(maKH);
            DataTable dtsdt = clsMain.ReturnDataTable(sSQL);
            if (dtsdt.Rows.Count > 0)
            {
                XtraMessageBox.Show("Mã Khách Hàng Này Đã Tồn Tại,Bạn Không Thể Tạo Mới Được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }
    }
}