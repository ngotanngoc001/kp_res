﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_CauhinhSole : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhSole()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Sole_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Update KHTT_SOLE Set " + "\n";
            sSQL += "SUDUNG=" + clsMain.SQLString("0") + "\n";

            if (chkSUDUNG.Checked )
            {
                sSQL += "Update KHTT_SOLE Set " + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLString("1") + "\n";
                sSQL += "where TenSoLe=N'Tính Điểm Cho Số lẻ'"  + "\n";
            }

            if (chkKOSUDUNG.Checked)
            {
                sSQL += "Update KHTT_SOLE Set " + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLString("1") + "\n";
                sSQL += "where TenSoLe=N'Không Tính Điểm Cho Số lẻ'" + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoaddataGridView()
        {
            string sSQL = "";
            sSQL += "select SuDung from KHTT_SOLE where TenSoLe=N'Tính Điểm Cho Số lẻ'";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            bool boolsoLe = bool.Parse(dt.Rows[0][0].ToString());
            chkSUDUNG.Checked = boolsoLe;
            chkKOSUDUNG.Checked = !chkSUDUNG.Checked;
        }

        private void LoadPermission()
        {
            btnLuu.Enabled = clsUserManagement.AllowEdit("53");
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}