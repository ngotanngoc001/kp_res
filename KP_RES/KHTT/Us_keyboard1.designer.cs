﻿namespace KP_RES
{
    /// <summary>
    /// Ban phim
    /// </summary>
    partial class Us_Keyboard1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_N = new DevExpress.XtraEditors.SimpleButton();
            this.btn_K = new DevExpress.XtraEditors.SimpleButton();
            this.btn_S = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Y = new DevExpress.XtraEditors.SimpleButton();
            this.btn_T = new DevExpress.XtraEditors.SimpleButton();
            this.btn_B = new DevExpress.XtraEditors.SimpleButton();
            this.btn_J = new DevExpress.XtraEditors.SimpleButton();
            this.btn_A = new DevExpress.XtraEditors.SimpleButton();
            this.btn_P = new DevExpress.XtraEditors.SimpleButton();
            this.btn_R = new DevExpress.XtraEditors.SimpleButton();
            this.btn_V = new DevExpress.XtraEditors.SimpleButton();
            this.btn_H = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Stop = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Comma = new DevExpress.XtraEditors.SimpleButton();
            this.btn_SRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_C = new DevExpress.XtraEditors.SimpleButton();
            this.btn_SLeft = new DevExpress.XtraEditors.SimpleButton();
            this.btn_G = new DevExpress.XtraEditors.SimpleButton();
            this.btn_X = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Z = new DevExpress.XtraEditors.SimpleButton();
            this.btn_F = new DevExpress.XtraEditors.SimpleButton();
            this.btn_O = new DevExpress.XtraEditors.SimpleButton();
            this.btn_M = new DevExpress.XtraEditors.SimpleButton();
            this.btn_I = new DevExpress.XtraEditors.SimpleButton();
            this.btn_colon = new DevExpress.XtraEditors.SimpleButton();
            this.btn_L = new DevExpress.XtraEditors.SimpleButton();
            this.btn_D = new DevExpress.XtraEditors.SimpleButton();
            this.btn_E = new DevExpress.XtraEditors.SimpleButton();
            this.btn_U = new DevExpress.XtraEditors.SimpleButton();
            this.btn_W = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Q = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_enter = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Space = new DevExpress.XtraEditors.SimpleButton();
            this.btn_searchNext = new DevExpress.XtraEditors.SimpleButton();
            this.btn_AW = new DevExpress.XtraEditors.SimpleButton();
            this.btn_dotbelow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DD = new DevExpress.XtraEditors.SimpleButton();
            this.btn_OW = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UW = new DevExpress.XtraEditors.SimpleButton();
            this.btn_tilde = new DevExpress.XtraEditors.SimpleButton();
            this.btn_OO = new DevExpress.XtraEditors.SimpleButton();
            this.btn_EE = new DevExpress.XtraEditors.SimpleButton();
            this.btn_questionmark = new DevExpress.XtraEditors.SimpleButton();
            this.btn_AA = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Grave = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Acute = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Shift = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btn_N
            // 
            this.btn_N.AllowFocus = false;
            this.btn_N.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_N.Appearance.Options.UseFont = true;
            this.btn_N.Appearance.Options.UseTextOptions = true;
            this.btn_N.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_N.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_N.Location = new System.Drawing.Point(595, 243);
            this.btn_N.Name = "btn_N";
            this.btn_N.Size = new System.Drawing.Size(73, 59);
            this.btn_N.TabIndex = 105;
            this.btn_N.Text = "N";
            this.btn_N.Click += new System.EventHandler(this.btn_N_Click);
            // 
            // btn_K
            // 
            this.btn_K.AllowFocus = false;
            this.btn_K.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_K.Appearance.Options.UseFont = true;
            this.btn_K.Appearance.Options.UseTextOptions = true;
            this.btn_K.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_K.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_K.Location = new System.Drawing.Point(520, 183);
            this.btn_K.Name = "btn_K";
            this.btn_K.Size = new System.Drawing.Size(73, 59);
            this.btn_K.TabIndex = 106;
            this.btn_K.Text = "K";
            this.btn_K.Click += new System.EventHandler(this.btn_K_Click);
            // 
            // btn_S
            // 
            this.btn_S.AllowFocus = false;
            this.btn_S.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_S.Appearance.Options.UseFont = true;
            this.btn_S.Appearance.Options.UseTextOptions = true;
            this.btn_S.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_S.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_S.Location = new System.Drawing.Point(76, 183);
            this.btn_S.Name = "btn_S";
            this.btn_S.Size = new System.Drawing.Size(73, 59);
            this.btn_S.TabIndex = 107;
            this.btn_S.Text = "S";
            this.btn_S.Click += new System.EventHandler(this.btn_S_Click);
            // 
            // btn_Y
            // 
            this.btn_Y.AllowFocus = false;
            this.btn_Y.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Y.Appearance.Options.UseFont = true;
            this.btn_Y.Appearance.Options.UseTextOptions = true;
            this.btn_Y.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Y.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Y.Location = new System.Drawing.Point(372, 123);
            this.btn_Y.Name = "btn_Y";
            this.btn_Y.Size = new System.Drawing.Size(73, 59);
            this.btn_Y.TabIndex = 104;
            this.btn_Y.Text = "Y";
            this.btn_Y.Click += new System.EventHandler(this.btn_Y_Click);
            // 
            // btn_T
            // 
            this.btn_T.AllowFocus = false;
            this.btn_T.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_T.Appearance.Options.UseFont = true;
            this.btn_T.Appearance.Options.UseTextOptions = true;
            this.btn_T.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_T.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_T.Location = new System.Drawing.Point(298, 123);
            this.btn_T.Name = "btn_T";
            this.btn_T.Size = new System.Drawing.Size(73, 59);
            this.btn_T.TabIndex = 101;
            this.btn_T.Text = "T";
            this.btn_T.Click += new System.EventHandler(this.btn_T_Click);
            // 
            // btn_B
            // 
            this.btn_B.AllowFocus = false;
            this.btn_B.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_B.Appearance.Options.UseFont = true;
            this.btn_B.Appearance.Options.UseTextOptions = true;
            this.btn_B.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_B.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_B.Location = new System.Drawing.Point(298, 243);
            this.btn_B.Name = "btn_B";
            this.btn_B.Size = new System.Drawing.Size(73, 59);
            this.btn_B.TabIndex = 102;
            this.btn_B.Text = "B";
            this.btn_B.Click += new System.EventHandler(this.btn_B_Click);
            // 
            // btn_J
            // 
            this.btn_J.AllowFocus = false;
            this.btn_J.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_J.Appearance.Options.UseFont = true;
            this.btn_J.Appearance.Options.UseTextOptions = true;
            this.btn_J.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_J.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_J.Location = new System.Drawing.Point(446, 183);
            this.btn_J.Name = "btn_J";
            this.btn_J.Size = new System.Drawing.Size(73, 59);
            this.btn_J.TabIndex = 103;
            this.btn_J.Text = "J";
            this.btn_J.Click += new System.EventHandler(this.btn_J_Click);
            // 
            // btn_A
            // 
            this.btn_A.AllowFocus = false;
            this.btn_A.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_A.Appearance.Options.UseFont = true;
            this.btn_A.Appearance.Options.UseTextOptions = true;
            this.btn_A.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_A.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_A.Location = new System.Drawing.Point(2, 183);
            this.btn_A.Name = "btn_A";
            this.btn_A.Size = new System.Drawing.Size(73, 59);
            this.btn_A.TabIndex = 113;
            this.btn_A.Text = "A";
            this.btn_A.Click += new System.EventHandler(this.btn_A_Click);
            // 
            // btn_P
            // 
            this.btn_P.AllowFocus = false;
            this.btn_P.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_P.Appearance.Options.UseFont = true;
            this.btn_P.Appearance.Options.UseTextOptions = true;
            this.btn_P.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_P.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_P.Location = new System.Drawing.Point(668, 123);
            this.btn_P.Name = "btn_P";
            this.btn_P.Size = new System.Drawing.Size(73, 59);
            this.btn_P.TabIndex = 114;
            this.btn_P.Text = "P";
            this.btn_P.Click += new System.EventHandler(this.btn_P_Click);
            // 
            // btn_R
            // 
            this.btn_R.AllowFocus = false;
            this.btn_R.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_R.Appearance.Options.UseFont = true;
            this.btn_R.Appearance.Options.UseTextOptions = true;
            this.btn_R.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_R.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_R.Location = new System.Drawing.Point(224, 123);
            this.btn_R.Name = "btn_R";
            this.btn_R.Size = new System.Drawing.Size(73, 59);
            this.btn_R.TabIndex = 117;
            this.btn_R.Text = "R";
            this.btn_R.Click += new System.EventHandler(this.btn_R_Click);
            // 
            // btn_V
            // 
            this.btn_V.AllowFocus = false;
            this.btn_V.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_V.Appearance.Options.UseFont = true;
            this.btn_V.Appearance.Options.UseTextOptions = true;
            this.btn_V.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_V.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_V.Location = new System.Drawing.Point(224, 243);
            this.btn_V.Name = "btn_V";
            this.btn_V.Size = new System.Drawing.Size(73, 59);
            this.btn_V.TabIndex = 112;
            this.btn_V.Text = "V";
            this.btn_V.Click += new System.EventHandler(this.btn_V_Click);
            // 
            // btn_H
            // 
            this.btn_H.AllowFocus = false;
            this.btn_H.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_H.Appearance.Options.UseFont = true;
            this.btn_H.Appearance.Options.UseTextOptions = true;
            this.btn_H.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_H.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_H.Location = new System.Drawing.Point(372, 183);
            this.btn_H.Name = "btn_H";
            this.btn_H.Size = new System.Drawing.Size(73, 59);
            this.btn_H.TabIndex = 108;
            this.btn_H.Text = "H";
            this.btn_H.Click += new System.EventHandler(this.btn_H_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.AllowFocus = false;
            this.btn_Stop.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Stop.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_Stop.Appearance.Options.UseFont = true;
            this.btn_Stop.Appearance.Options.UseForeColor = true;
            this.btn_Stop.Appearance.Options.UseTextOptions = true;
            this.btn_Stop.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Stop.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Stop.Location = new System.Drawing.Point(817, 243);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(73, 59);
            this.btn_Stop.TabIndex = 109;
            this.btn_Stop.Text = ".";
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // btn_Comma
            // 
            this.btn_Comma.AllowFocus = false;
            this.btn_Comma.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Comma.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_Comma.Appearance.Options.UseFont = true;
            this.btn_Comma.Appearance.Options.UseForeColor = true;
            this.btn_Comma.Appearance.Options.UseTextOptions = true;
            this.btn_Comma.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Comma.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Comma.Location = new System.Drawing.Point(743, 243);
            this.btn_Comma.Name = "btn_Comma";
            this.btn_Comma.Size = new System.Drawing.Size(73, 59);
            this.btn_Comma.TabIndex = 110;
            this.btn_Comma.Text = ",\r\n";
            this.btn_Comma.Click += new System.EventHandler(this.btn_Comma_Click);
            // 
            // btn_SRight
            // 
            this.btn_SRight.AllowFocus = false;
            this.btn_SRight.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SRight.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_SRight.Appearance.Options.UseFont = true;
            this.btn_SRight.Appearance.Options.UseForeColor = true;
            this.btn_SRight.Appearance.Options.UseTextOptions = true;
            this.btn_SRight.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_SRight.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_SRight.Location = new System.Drawing.Point(816, 183);
            this.btn_SRight.Name = "btn_SRight";
            this.btn_SRight.Size = new System.Drawing.Size(73, 59);
            this.btn_SRight.TabIndex = 111;
            this.btn_SRight.Text = "\\";
            this.btn_SRight.Click += new System.EventHandler(this.btn_SLeft_Click);
            // 
            // btn_C
            // 
            this.btn_C.AllowFocus = false;
            this.btn_C.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_C.Appearance.Options.UseFont = true;
            this.btn_C.Appearance.Options.UseTextOptions = true;
            this.btn_C.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_C.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_C.Location = new System.Drawing.Point(150, 243);
            this.btn_C.Name = "btn_C";
            this.btn_C.Size = new System.Drawing.Size(73, 59);
            this.btn_C.TabIndex = 88;
            this.btn_C.Text = "C";
            this.btn_C.Click += new System.EventHandler(this.btn_C_Click);
            // 
            // btn_SLeft
            // 
            this.btn_SLeft.AllowFocus = false;
            this.btn_SLeft.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SLeft.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_SLeft.Appearance.Options.UseFont = true;
            this.btn_SLeft.Appearance.Options.UseForeColor = true;
            this.btn_SLeft.Appearance.Options.UseTextOptions = true;
            this.btn_SLeft.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_SLeft.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_SLeft.Location = new System.Drawing.Point(742, 183);
            this.btn_SLeft.Name = "btn_SLeft";
            this.btn_SLeft.Size = new System.Drawing.Size(73, 59);
            this.btn_SLeft.TabIndex = 90;
            this.btn_SLeft.Text = "/";
            this.btn_SLeft.Click += new System.EventHandler(this.btn_SRight_Click);
            // 
            // btn_G
            // 
            this.btn_G.AllowFocus = false;
            this.btn_G.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_G.Appearance.Options.UseFont = true;
            this.btn_G.Appearance.Options.UseTextOptions = true;
            this.btn_G.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_G.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_G.Location = new System.Drawing.Point(298, 183);
            this.btn_G.Name = "btn_G";
            this.btn_G.Size = new System.Drawing.Size(73, 59);
            this.btn_G.TabIndex = 91;
            this.btn_G.Text = "G";
            this.btn_G.Click += new System.EventHandler(this.btn_G_Click);
            // 
            // btn_X
            // 
            this.btn_X.AllowFocus = false;
            this.btn_X.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_X.Appearance.Options.UseFont = true;
            this.btn_X.Appearance.Options.UseTextOptions = true;
            this.btn_X.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_X.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_X.Location = new System.Drawing.Point(76, 243);
            this.btn_X.Name = "btn_X";
            this.btn_X.Size = new System.Drawing.Size(73, 59);
            this.btn_X.TabIndex = 86;
            this.btn_X.Text = "X";
            this.btn_X.Click += new System.EventHandler(this.btn_X_Click);
            // 
            // btn_Z
            // 
            this.btn_Z.AllowFocus = false;
            this.btn_Z.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Z.Appearance.Options.UseFont = true;
            this.btn_Z.Appearance.Options.UseTextOptions = true;
            this.btn_Z.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Z.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Z.Location = new System.Drawing.Point(2, 243);
            this.btn_Z.Name = "btn_Z";
            this.btn_Z.Size = new System.Drawing.Size(73, 59);
            this.btn_Z.TabIndex = 87;
            this.btn_Z.Text = "Z";
            this.btn_Z.Click += new System.EventHandler(this.btn_Z_Click);
            // 
            // btn_F
            // 
            this.btn_F.AllowFocus = false;
            this.btn_F.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_F.Appearance.Options.UseFont = true;
            this.btn_F.Appearance.Options.UseTextOptions = true;
            this.btn_F.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_F.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_F.Location = new System.Drawing.Point(224, 183);
            this.btn_F.Name = "btn_F";
            this.btn_F.Size = new System.Drawing.Size(73, 59);
            this.btn_F.TabIndex = 83;
            this.btn_F.Text = "F";
            this.btn_F.Click += new System.EventHandler(this.btn_F_Click);
            // 
            // btn_O
            // 
            this.btn_O.AllowFocus = false;
            this.btn_O.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_O.Appearance.Options.UseFont = true;
            this.btn_O.Appearance.Options.UseTextOptions = true;
            this.btn_O.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_O.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_O.Location = new System.Drawing.Point(594, 123);
            this.btn_O.Name = "btn_O";
            this.btn_O.Size = new System.Drawing.Size(73, 59);
            this.btn_O.TabIndex = 84;
            this.btn_O.Text = "O";
            this.btn_O.Click += new System.EventHandler(this.btn_O_Click);
            // 
            // btn_M
            // 
            this.btn_M.AllowFocus = false;
            this.btn_M.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_M.Appearance.Options.UseFont = true;
            this.btn_M.Appearance.Options.UseTextOptions = true;
            this.btn_M.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_M.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_M.Location = new System.Drawing.Point(669, 243);
            this.btn_M.Name = "btn_M";
            this.btn_M.Size = new System.Drawing.Size(73, 59);
            this.btn_M.TabIndex = 85;
            this.btn_M.Text = "M";
            this.btn_M.Click += new System.EventHandler(this.btn_M_Click);
            // 
            // btn_I
            // 
            this.btn_I.AllowFocus = false;
            this.btn_I.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_I.Appearance.Options.UseFont = true;
            this.btn_I.Appearance.Options.UseTextOptions = true;
            this.btn_I.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_I.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_I.Location = new System.Drawing.Point(520, 123);
            this.btn_I.Name = "btn_I";
            this.btn_I.Size = new System.Drawing.Size(73, 59);
            this.btn_I.TabIndex = 96;
            this.btn_I.Text = "I";
            this.btn_I.Click += new System.EventHandler(this.btn_I_Click);
            // 
            // btn_colon
            // 
            this.btn_colon.AllowFocus = false;
            this.btn_colon.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_colon.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_colon.Appearance.Options.UseFont = true;
            this.btn_colon.Appearance.Options.UseForeColor = true;
            this.btn_colon.Appearance.Options.UseTextOptions = true;
            this.btn_colon.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_colon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_colon.Location = new System.Drawing.Point(668, 183);
            this.btn_colon.Name = "btn_colon";
            this.btn_colon.Size = new System.Drawing.Size(73, 59);
            this.btn_colon.TabIndex = 97;
            this.btn_colon.Text = ";";
            this.btn_colon.Click += new System.EventHandler(this.btn_colon_Click);
            // 
            // btn_L
            // 
            this.btn_L.AllowFocus = false;
            this.btn_L.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_L.Appearance.Options.UseFont = true;
            this.btn_L.Appearance.Options.UseTextOptions = true;
            this.btn_L.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_L.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_L.Location = new System.Drawing.Point(594, 183);
            this.btn_L.Name = "btn_L";
            this.btn_L.Size = new System.Drawing.Size(73, 59);
            this.btn_L.TabIndex = 99;
            this.btn_L.Text = "L";
            this.btn_L.Click += new System.EventHandler(this.btn_L_Click);
            // 
            // btn_D
            // 
            this.btn_D.AllowFocus = false;
            this.btn_D.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_D.Appearance.Options.UseFont = true;
            this.btn_D.Appearance.Options.UseTextOptions = true;
            this.btn_D.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_D.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_D.Location = new System.Drawing.Point(150, 183);
            this.btn_D.Name = "btn_D";
            this.btn_D.Size = new System.Drawing.Size(73, 59);
            this.btn_D.TabIndex = 100;
            this.btn_D.Text = "D";
            this.btn_D.Click += new System.EventHandler(this.btn_D_Click);
            // 
            // btn_E
            // 
            this.btn_E.AllowFocus = false;
            this.btn_E.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_E.Appearance.Options.UseFont = true;
            this.btn_E.Appearance.Options.UseTextOptions = true;
            this.btn_E.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_E.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_E.Location = new System.Drawing.Point(150, 123);
            this.btn_E.Name = "btn_E";
            this.btn_E.Size = new System.Drawing.Size(73, 59);
            this.btn_E.TabIndex = 95;
            this.btn_E.Text = "E";
            this.btn_E.Click += new System.EventHandler(this.btn_E_Click);
            // 
            // btn_U
            // 
            this.btn_U.AllowFocus = false;
            this.btn_U.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_U.Appearance.Options.UseFont = true;
            this.btn_U.Appearance.Options.UseTextOptions = true;
            this.btn_U.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_U.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_U.Location = new System.Drawing.Point(446, 123);
            this.btn_U.Name = "btn_U";
            this.btn_U.Size = new System.Drawing.Size(73, 59);
            this.btn_U.TabIndex = 92;
            this.btn_U.Text = "U";
            this.btn_U.Click += new System.EventHandler(this.btn_U_Click);
            // 
            // btn_W
            // 
            this.btn_W.AllowFocus = false;
            this.btn_W.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_W.Appearance.Options.UseFont = true;
            this.btn_W.Appearance.Options.UseTextOptions = true;
            this.btn_W.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_W.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_W.Location = new System.Drawing.Point(76, 123);
            this.btn_W.Name = "btn_W";
            this.btn_W.Size = new System.Drawing.Size(73, 59);
            this.btn_W.TabIndex = 93;
            this.btn_W.Text = "W";
            this.btn_W.Click += new System.EventHandler(this.btn_W_Click);
            // 
            // btn_Q
            // 
            this.btn_Q.AllowFocus = false;
            this.btn_Q.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Q.Appearance.Options.UseFont = true;
            this.btn_Q.Appearance.Options.UseTextOptions = true;
            this.btn_Q.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Q.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Q.Location = new System.Drawing.Point(2, 123);
            this.btn_Q.Name = "btn_Q";
            this.btn_Q.Size = new System.Drawing.Size(73, 59);
            this.btn_Q.TabIndex = 94;
            this.btn_Q.Text = "Q";
            this.btn_Q.Click += new System.EventHandler(this.btn_Q_Click);
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Appearance.Options.UseForeColor = true;
            this.btn_0.Appearance.Options.UseTextOptions = true;
            this.btn_0.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_0.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_0.Location = new System.Drawing.Point(667, 63);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(73, 59);
            this.btn_0.TabIndex = 79;
            this.btn_0.Text = "0";
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_enter
            // 
            this.btn_enter.AllowFocus = false;
            this.btn_enter.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_enter.Appearance.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_enter.Appearance.Options.UseFont = true;
            this.btn_enter.Appearance.Options.UseForeColor = true;
            this.btn_enter.Appearance.Options.UseTextOptions = true;
            this.btn_enter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_enter.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_enter.Location = new System.Drawing.Point(890, 183);
            this.btn_enter.Name = "btn_enter";
            this.btn_enter.Size = new System.Drawing.Size(73, 119);
            this.btn_enter.TabIndex = 82;
            this.btn_enter.Text = "Enter";
            this.btn_enter.Click += new System.EventHandler(this.btn_enter_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Appearance.Options.UseTextOptions = true;
            this.btn_delete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_delete.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_delete.Location = new System.Drawing.Point(890, 63);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(73, 59);
            this.btn_delete.TabIndex = 81;
            this.btn_delete.Text = "Xóa";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Appearance.Options.UseForeColor = true;
            this.btn_6.Appearance.Options.UseTextOptions = true;
            this.btn_6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_6.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_6.Location = new System.Drawing.Point(371, 63);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(73, 59);
            this.btn_6.TabIndex = 75;
            this.btn_6.Text = "6";
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Appearance.Options.UseForeColor = true;
            this.btn_9.Appearance.Options.UseTextOptions = true;
            this.btn_9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_9.Location = new System.Drawing.Point(593, 63);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(73, 59);
            this.btn_9.TabIndex = 78;
            this.btn_9.Text = "9\r\n";
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Appearance.Options.UseForeColor = true;
            this.btn_8.Appearance.Options.UseTextOptions = true;
            this.btn_8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_8.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_8.Location = new System.Drawing.Point(519, 63);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(73, 59);
            this.btn_8.TabIndex = 77;
            this.btn_8.Text = "8";
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Appearance.Options.UseForeColor = true;
            this.btn_7.Appearance.Options.UseTextOptions = true;
            this.btn_7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_7.Location = new System.Drawing.Point(445, 63);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(73, 59);
            this.btn_7.TabIndex = 76;
            this.btn_7.Text = "7";
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Appearance.Options.UseForeColor = true;
            this.btn_5.Appearance.Options.UseTextOptions = true;
            this.btn_5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_5.Location = new System.Drawing.Point(297, 63);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(73, 59);
            this.btn_5.TabIndex = 73;
            this.btn_5.Text = "5";
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Appearance.Options.UseForeColor = true;
            this.btn_4.Appearance.Options.UseTextOptions = true;
            this.btn_4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_4.Location = new System.Drawing.Point(223, 63);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(73, 59);
            this.btn_4.TabIndex = 72;
            this.btn_4.Text = "4";
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Appearance.Options.UseForeColor = true;
            this.btn_3.Appearance.Options.UseTextOptions = true;
            this.btn_3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_3.Location = new System.Drawing.Point(149, 63);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(73, 59);
            this.btn_3.TabIndex = 71;
            this.btn_3.Text = "3";
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Appearance.Options.UseForeColor = true;
            this.btn_2.Appearance.Options.UseTextOptions = true;
            this.btn_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_2.Location = new System.Drawing.Point(75, 63);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(73, 59);
            this.btn_2.TabIndex = 70;
            this.btn_2.Text = "2";
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_1
            // 
            this.btn_1.AccessibleDescription = "";
            this.btn_1.AllowFocus = false;
            this.btn_1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Appearance.Options.UseForeColor = true;
            this.btn_1.Appearance.Options.UseTextOptions = true;
            this.btn_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_1.Location = new System.Drawing.Point(1, 63);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(73, 59);
            this.btn_1.TabIndex = 69;
            this.btn_1.Text = "1";
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Appearance.Options.UseTextOptions = true;
            this.btn_ESC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_ESC.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_ESC.Location = new System.Drawing.Point(742, 63);
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(147, 59);
            this.btn_ESC.TabIndex = 68;
            this.btn_ESC.Text = "Xóa Hết";
            this.btn_ESC.Click += new System.EventHandler(this.btn_ESC_Click);
            // 
            // btn_Space
            // 
            this.btn_Space.AllowFocus = false;
            this.btn_Space.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Space.Appearance.Options.UseFont = true;
            this.btn_Space.Appearance.Options.UseTextOptions = true;
            this.btn_Space.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Space.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Space.Location = new System.Drawing.Point(372, 243);
            this.btn_Space.Name = "btn_Space";
            this.btn_Space.Size = new System.Drawing.Size(221, 59);
            this.btn_Space.TabIndex = 102;
            this.btn_Space.Text = "Space";
            this.btn_Space.Click += new System.EventHandler(this.btn_Space_Click);
            // 
            // btn_searchNext
            // 
            this.btn_searchNext.AllowFocus = false;
            this.btn_searchNext.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_searchNext.Appearance.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_searchNext.Appearance.Options.UseFont = true;
            this.btn_searchNext.Appearance.Options.UseForeColor = true;
            this.btn_searchNext.Appearance.Options.UseTextOptions = true;
            this.btn_searchNext.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_searchNext.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_searchNext.Location = new System.Drawing.Point(838, 118);
            this.btn_searchNext.Name = "btn_searchNext";
            this.btn_searchNext.Size = new System.Drawing.Size(122, 59);
            this.btn_searchNext.TabIndex = 119;
            this.btn_searchNext.Text = "Tìm Tiếp";
            this.btn_searchNext.Click += new System.EventHandler(this.btn_searchNext_Click);
            // 
            // btn_AW
            // 
            this.btn_AW.AllowFocus = false;
            this.btn_AW.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AW.Appearance.Options.UseFont = true;
            this.btn_AW.Appearance.Options.UseTextOptions = true;
            this.btn_AW.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_AW.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_AW.Location = new System.Drawing.Point(372, 0);
            this.btn_AW.Name = "btn_AW";
            this.btn_AW.Size = new System.Drawing.Size(73, 59);
            this.btn_AW.TabIndex = 126;
            this.btn_AW.Text = "Ă";
            this.btn_AW.Click += new System.EventHandler(this.btn_AW_Click);
            // 
            // btn_dotbelow
            // 
            this.btn_dotbelow.AllowFocus = false;
            this.btn_dotbelow.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dotbelow.Appearance.Options.UseFont = true;
            this.btn_dotbelow.Appearance.Options.UseTextOptions = true;
            this.btn_dotbelow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_dotbelow.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_dotbelow.Location = new System.Drawing.Point(298, 0);
            this.btn_dotbelow.Name = "btn_dotbelow";
            this.btn_dotbelow.Size = new System.Drawing.Size(73, 59);
            this.btn_dotbelow.TabIndex = 125;
            this.btn_dotbelow.Text = ".";
            this.btn_dotbelow.Click += new System.EventHandler(this.btn_Acute_Click);
            // 
            // btn_DD
            // 
            this.btn_DD.AllowFocus = false;
            this.btn_DD.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DD.Appearance.Options.UseFont = true;
            this.btn_DD.Appearance.Options.UseTextOptions = true;
            this.btn_DD.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_DD.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_DD.Location = new System.Drawing.Point(816, 0);
            this.btn_DD.Name = "btn_DD";
            this.btn_DD.Size = new System.Drawing.Size(73, 59);
            this.btn_DD.TabIndex = 129;
            this.btn_DD.Text = "Đ";
            this.btn_DD.Click += new System.EventHandler(this.btn_DD_Click);
            // 
            // btn_OW
            // 
            this.btn_OW.AllowFocus = false;
            this.btn_OW.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OW.Appearance.Options.UseFont = true;
            this.btn_OW.Appearance.Options.UseTextOptions = true;
            this.btn_OW.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_OW.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_OW.Location = new System.Drawing.Point(742, 0);
            this.btn_OW.Name = "btn_OW";
            this.btn_OW.Size = new System.Drawing.Size(73, 59);
            this.btn_OW.TabIndex = 128;
            this.btn_OW.Text = "Ơ";
            this.btn_OW.Click += new System.EventHandler(this.btn_OW_Click);
            // 
            // btn_UW
            // 
            this.btn_UW.AllowFocus = false;
            this.btn_UW.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_UW.Appearance.Options.UseFont = true;
            this.btn_UW.Appearance.Options.UseTextOptions = true;
            this.btn_UW.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_UW.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_UW.Location = new System.Drawing.Point(668, 0);
            this.btn_UW.Name = "btn_UW";
            this.btn_UW.Size = new System.Drawing.Size(73, 59);
            this.btn_UW.TabIndex = 127;
            this.btn_UW.Text = "Ư";
            this.btn_UW.Click += new System.EventHandler(this.btn_UW_Click);
            // 
            // btn_tilde
            // 
            this.btn_tilde.AllowFocus = false;
            this.btn_tilde.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tilde.Appearance.Options.UseFont = true;
            this.btn_tilde.Appearance.Options.UseTextOptions = true;
            this.btn_tilde.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_tilde.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_tilde.Location = new System.Drawing.Point(224, 0);
            this.btn_tilde.Name = "btn_tilde";
            this.btn_tilde.Size = new System.Drawing.Size(73, 59);
            this.btn_tilde.TabIndex = 130;
            this.btn_tilde.Text = "~";
            this.btn_tilde.Click += new System.EventHandler(this.btn_Acute_Click);
            // 
            // btn_OO
            // 
            this.btn_OO.AllowFocus = false;
            this.btn_OO.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OO.Appearance.Options.UseFont = true;
            this.btn_OO.Appearance.Options.UseTextOptions = true;
            this.btn_OO.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_OO.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_OO.Location = new System.Drawing.Point(594, 0);
            this.btn_OO.Name = "btn_OO";
            this.btn_OO.Size = new System.Drawing.Size(73, 59);
            this.btn_OO.TabIndex = 118;
            this.btn_OO.Text = "Ô";
            this.btn_OO.Click += new System.EventHandler(this.btn_OO_Click);
            // 
            // btn_EE
            // 
            this.btn_EE.AllowFocus = false;
            this.btn_EE.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EE.Appearance.Options.UseFont = true;
            this.btn_EE.Appearance.Options.UseTextOptions = true;
            this.btn_EE.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_EE.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_EE.Location = new System.Drawing.Point(520, 0);
            this.btn_EE.Name = "btn_EE";
            this.btn_EE.Size = new System.Drawing.Size(73, 59);
            this.btn_EE.TabIndex = 124;
            this.btn_EE.Text = "Ê";
            this.btn_EE.Click += new System.EventHandler(this.btn_EE_Click);
            // 
            // btn_questionmark
            // 
            this.btn_questionmark.AllowFocus = false;
            this.btn_questionmark.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_questionmark.Appearance.Options.UseFont = true;
            this.btn_questionmark.Appearance.Options.UseTextOptions = true;
            this.btn_questionmark.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_questionmark.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_questionmark.Location = new System.Drawing.Point(150, 0);
            this.btn_questionmark.Name = "btn_questionmark";
            this.btn_questionmark.Size = new System.Drawing.Size(73, 59);
            this.btn_questionmark.TabIndex = 123;
            this.btn_questionmark.Text = "?";
            this.btn_questionmark.Click += new System.EventHandler(this.btn_Acute_Click);
            // 
            // btn_AA
            // 
            this.btn_AA.AllowFocus = false;
            this.btn_AA.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AA.Appearance.Options.UseFont = true;
            this.btn_AA.Appearance.Options.UseTextOptions = true;
            this.btn_AA.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_AA.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_AA.Location = new System.Drawing.Point(446, 0);
            this.btn_AA.Name = "btn_AA";
            this.btn_AA.Size = new System.Drawing.Size(73, 59);
            this.btn_AA.TabIndex = 120;
            this.btn_AA.Text = "Â";
            this.btn_AA.Click += new System.EventHandler(this.btn_AA_Click);
            // 
            // btn_Grave
            // 
            this.btn_Grave.AllowFocus = false;
            this.btn_Grave.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Grave.Appearance.Options.UseFont = true;
            this.btn_Grave.Appearance.Options.UseTextOptions = true;
            this.btn_Grave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Grave.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Grave.Location = new System.Drawing.Point(76, 0);
            this.btn_Grave.Name = "btn_Grave";
            this.btn_Grave.Size = new System.Drawing.Size(73, 59);
            this.btn_Grave.TabIndex = 121;
            this.btn_Grave.Text = "`";
            this.btn_Grave.Click += new System.EventHandler(this.btn_Acute_Click);
            // 
            // btn_Acute
            // 
            this.btn_Acute.AllowFocus = false;
            this.btn_Acute.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Acute.Appearance.Options.UseFont = true;
            this.btn_Acute.Appearance.Options.UseTextOptions = true;
            this.btn_Acute.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Acute.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Acute.Location = new System.Drawing.Point(2, 0);
            this.btn_Acute.Name = "btn_Acute";
            this.btn_Acute.Size = new System.Drawing.Size(73, 59);
            this.btn_Acute.TabIndex = 122;
            this.btn_Acute.Text = "\'";
            this.btn_Acute.Click += new System.EventHandler(this.btn_Acute_Click);
            // 
            // btn_Shift
            // 
            this.btn_Shift.AllowFocus = false;
            this.btn_Shift.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Shift.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_Shift.Appearance.Options.UseFont = true;
            this.btn_Shift.Appearance.Options.UseForeColor = true;
            this.btn_Shift.Appearance.Options.UseTextOptions = true;
            this.btn_Shift.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Shift.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btn_Shift.Location = new System.Drawing.Point(890, 3);
            this.btn_Shift.Name = "btn_Shift";
            this.btn_Shift.Size = new System.Drawing.Size(73, 59);
            this.btn_Shift.TabIndex = 131;
            this.btn_Shift.Text = "Shift";
            this.btn_Shift.Click += new System.EventHandler(this.btn_Shift_Click_1);
            // 
            // Us_Keyboard1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.btn_Shift);
            this.Controls.Add(this.btn_searchNext);
            this.Controls.Add(this.btn_AW);
            this.Controls.Add(this.btn_dotbelow);
            this.Controls.Add(this.btn_DD);
            this.Controls.Add(this.btn_OW);
            this.Controls.Add(this.btn_UW);
            this.Controls.Add(this.btn_tilde);
            this.Controls.Add(this.btn_OO);
            this.Controls.Add(this.btn_EE);
            this.Controls.Add(this.btn_questionmark);
            this.Controls.Add(this.btn_AA);
            this.Controls.Add(this.btn_Grave);
            this.Controls.Add(this.btn_Acute);
            this.Controls.Add(this.btn_N);
            this.Controls.Add(this.btn_K);
            this.Controls.Add(this.btn_S);
            this.Controls.Add(this.btn_Y);
            this.Controls.Add(this.btn_T);
            this.Controls.Add(this.btn_Space);
            this.Controls.Add(this.btn_B);
            this.Controls.Add(this.btn_J);
            this.Controls.Add(this.btn_A);
            this.Controls.Add(this.btn_P);
            this.Controls.Add(this.btn_R);
            this.Controls.Add(this.btn_V);
            this.Controls.Add(this.btn_H);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.btn_Comma);
            this.Controls.Add(this.btn_SRight);
            this.Controls.Add(this.btn_C);
            this.Controls.Add(this.btn_SLeft);
            this.Controls.Add(this.btn_G);
            this.Controls.Add(this.btn_X);
            this.Controls.Add(this.btn_Z);
            this.Controls.Add(this.btn_F);
            this.Controls.Add(this.btn_O);
            this.Controls.Add(this.btn_M);
            this.Controls.Add(this.btn_I);
            this.Controls.Add(this.btn_colon);
            this.Controls.Add(this.btn_L);
            this.Controls.Add(this.btn_D);
            this.Controls.Add(this.btn_E);
            this.Controls.Add(this.btn_U);
            this.Controls.Add(this.btn_W);
            this.Controls.Add(this.btn_Q);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_enter);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_ESC);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "Us_Keyboard1";
            this.Size = new System.Drawing.Size(963, 322);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn_N;
        private DevExpress.XtraEditors.SimpleButton btn_K;
        private DevExpress.XtraEditors.SimpleButton btn_S;
        private DevExpress.XtraEditors.SimpleButton btn_Y;
        private DevExpress.XtraEditors.SimpleButton btn_T;
        private DevExpress.XtraEditors.SimpleButton btn_B;
        private DevExpress.XtraEditors.SimpleButton btn_J;
        private DevExpress.XtraEditors.SimpleButton btn_A;
        private DevExpress.XtraEditors.SimpleButton btn_P;
        private DevExpress.XtraEditors.SimpleButton btn_R;
        private DevExpress.XtraEditors.SimpleButton btn_V;
        private DevExpress.XtraEditors.SimpleButton btn_H;
        private DevExpress.XtraEditors.SimpleButton btn_Stop;
        private DevExpress.XtraEditors.SimpleButton btn_Comma;
        private DevExpress.XtraEditors.SimpleButton btn_SRight;
        private DevExpress.XtraEditors.SimpleButton btn_C;
        private DevExpress.XtraEditors.SimpleButton btn_SLeft;
        private DevExpress.XtraEditors.SimpleButton btn_G;
        private DevExpress.XtraEditors.SimpleButton btn_X;
        private DevExpress.XtraEditors.SimpleButton btn_Z;
        private DevExpress.XtraEditors.SimpleButton btn_F;
        private DevExpress.XtraEditors.SimpleButton btn_O;
        private DevExpress.XtraEditors.SimpleButton btn_M;
        private DevExpress.XtraEditors.SimpleButton btn_I;
        private DevExpress.XtraEditors.SimpleButton btn_colon;
        private DevExpress.XtraEditors.SimpleButton btn_L;
        private DevExpress.XtraEditors.SimpleButton btn_D;
        private DevExpress.XtraEditors.SimpleButton btn_E;
        private DevExpress.XtraEditors.SimpleButton btn_U;
        private DevExpress.XtraEditors.SimpleButton btn_W;
        private DevExpress.XtraEditors.SimpleButton btn_Q;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_enter;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.SimpleButton btn_Space;
        private DevExpress.XtraEditors.SimpleButton btn_searchNext;
        private DevExpress.XtraEditors.SimpleButton btn_AW;
        private DevExpress.XtraEditors.SimpleButton btn_dotbelow;
        private DevExpress.XtraEditors.SimpleButton btn_DD;
        private DevExpress.XtraEditors.SimpleButton btn_OW;
        private DevExpress.XtraEditors.SimpleButton btn_UW;
        private DevExpress.XtraEditors.SimpleButton btn_tilde;
        private DevExpress.XtraEditors.SimpleButton btn_OO;
        private DevExpress.XtraEditors.SimpleButton btn_EE;
        private DevExpress.XtraEditors.SimpleButton btn_questionmark;
        private DevExpress.XtraEditors.SimpleButton btn_AA;
        private DevExpress.XtraEditors.SimpleButton btn_Grave;
        private DevExpress.XtraEditors.SimpleButton btn_Acute;
        private DevExpress.XtraEditors.SimpleButton btn_Shift;
    }
}
