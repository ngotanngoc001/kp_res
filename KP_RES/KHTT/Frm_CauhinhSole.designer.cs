﻿namespace KP_RES 
{
    partial class Frm_CauhinhSole 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkKOSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkKOSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnDong);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.chkKOSUDUNG);
            this.panelControl1.Controls.Add(this.chkSUDUNG);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(472, 130);
            this.panelControl1.TabIndex = 0;
            // 
            // chkKOSUDUNG
            // 
            this.chkKOSUDUNG.EnterMoveNextControl = true;
            this.chkKOSUDUNG.Location = new System.Drawing.Point(218, 45);
            this.chkKOSUDUNG.Name = "chkKOSUDUNG";
            this.chkKOSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkKOSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkKOSUDUNG.Properties.Caption = "Không tính điểm cho số lẻ";
            this.chkKOSUDUNG.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkKOSUDUNG.Properties.RadioGroupIndex = 1;
            this.chkKOSUDUNG.Size = new System.Drawing.Size(254, 24);
            this.chkKOSUDUNG.TabIndex = 9;
            this.chkKOSUDUNG.TabStop = false;
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EditValue = true;
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(34, 45);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Tính điểm cho số lẻ";
            this.chkSUDUNG.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkSUDUNG.Properties.RadioGroupIndex = 1;
            this.chkSUDUNG.Size = new System.Drawing.Size(188, 24);
            this.chkSUDUNG.TabIndex = 4;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 7);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(124, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Quy định về số lẻ";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 35);
            this.panel2.TabIndex = 8;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(349, 89);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 11;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(231, 89);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 10;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_Sole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 165);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Sole";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình tính điểm cho số lẻ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Sole_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkKOSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.CheckEdit chkKOSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnLuu;




    }
}