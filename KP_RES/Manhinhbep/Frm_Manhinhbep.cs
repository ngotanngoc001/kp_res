﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Text.RegularExpressions;

namespace KP_RES 
{
    public partial class Frm_Manhinhbep : DevExpress.XtraEditors.XtraForm
    {
        string ma = string.Empty;//mã bếp
        string ma_hanghoa = string.Empty;
        string ma_ban = string.Empty;
        string stt = string.Empty;
        //
        //TRANGTHAI, 0:chưa chọn order; 1:chọn đang làm; 2:Hết nguyên liệu; 3:đã làm xong
        //
        public Frm_Manhinhbep()
        {
            InitializeComponent();
        
            LoaddataGridView();
            Load_MucTG();
            timer1.Start();
        }

        private void LoaddataGridView()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            if (File.Exists(Application.StartupPath + "\\BepSD.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\BepSD.xml");
                    dt = ds.Tables[0];
                    ma = dt.Rows[0]["MA_BEP"].ToString();
                }
                catch
                {
                }
            }
            string sql = "";
            sql += "select h.MA_HANGHOA,h.TEN_HANGHOA,k.TEN_KHUVUC,b.MA_BAN,b.TEN_BAN,d.MA_HOADON,d.GIOORDER,d.YEUCAU,d.SOLUONG,d.NHANVIEN,d.STT AS STTODER,d.TRANGTHAI as TT" + "\n";
            sql += "From HANGHOA h " + "\n";
            if (cls_KP_RES.Mode == 4)
            {
                sql += "inner join DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND d.FLAG=1 AND CONVERT(VARCHAR(10),d.GIOORDER,111)>=CONVERT(VARCHAR(10),GETDATE(),111) AND TRANGTHAI !=2 AND TRANGTHAI!=3" + "\n";
            }
            else
            {
                sql += "inner join DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND d.FLAG=1 AND CONVERT(VARCHAR(10),d.GIOORDER,111)>=CONVERT(VARCHAR(10),GETDATE(),111) AND TRANGTHAI !=2 AND TRANGTHAI!=3 AND h.MA_BEP='" + ma + "'" + "\n";
            }
            sql += "inner join BAN b on b.MA_BAN=d.MA_BAN" + "\n";
            sql += "inner join KHUVUC k  on k.MA_KHUVUC=b.MA_KHUVUC " + "\n";
            sql += "ORDER by d.GIOORDER" + "\n";

            dt = clsMain.ReturnDataTable(sql);
            dt.Columns.Add("TRANGTHAI");
            dt.Columns.Add("GAP");
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TT"].ToString() == "0")
                    dr["TRANGTHAI"] = "ORDER";
                else
                    dr["TRANGTHAI"] = "Đang làm";

                if (dr["YEUCAU"].ToString().ToLower() .Contains("lam gap") || dr["YEUCAU"].ToString().ToLower ().Contains("làm gấp"))
                {
                    dr["GAP"] = "1";
                }
            }
            gridControl2.DataSource = dt;
            gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            int n=gridView2.RowCount;
            int i;
            for (i = 0; i < n; i++)
            {
                if (gridView2.GetRowCellValue(i, STTODER).ToString() == stt)
                {
                    gridView2.FocusedRowHandle = i;
                    break;
                }
            }
            gridView2.MakeRowVisible(i);
        }

        private void gridView2_RowClick(object sender, RowClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;
            ma_hanghoa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA_HANGHOA).ToString();
            ma_ban = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA_BAN).ToString();
            stt = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, STTODER).ToString();

            lbHeader.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_KHUVUC).ToString()  + " > " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_BAN).ToString()     + " > " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_HANGHOA).ToString() + " : " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, SOLUONG).ToString();
            lbHeader.Visible = true;

            GridView View = sender as GridView;
            if (View.GetRowCellValue(e.RowHandle, View.Columns["GAP"]).ToString() == "1")
            {
                gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(237, 28, 36);
            }
            else
            {
                TimeSpan tp = DateTime.Now - Convert.ToDateTime(View.GetRowCellValue(e.RowHandle, View.Columns["GIOORDER"]));
                if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(0, 173, 239);
                else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(214, 223, 35);
                else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(247, 147, 29);
            }
        }

        private void Load_MucTG()
        {
            DataTable dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER1'");
            if (dt.Rows.Count > 0)
            {
                btnMucTG1.Properties.NullText = "Món >" + dt.Rows[0][0].ToString() + " P";
                btnMucTG1.Tag = dt.Rows[0][0].ToString();
            }
            dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER2'");
            if (dt.Rows.Count > 0)
            {
                btnMucTG2L.Properties.NullText = "Món >" + dt.Rows[0][0].ToString() + " P";
                btnMucTG2N.Properties.NullText = "Món Mới <" + dt.Rows[0][0].ToString() + " P";
                btnMucTG2L.Tag = dt.Rows[0][0].ToString();
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();

                gridView2.FocusedRowHandle = gridView2.RowCount - 1;

                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GAP).ToString() == "1")
                {
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(237, 28, 36);
                }
                else
                {
                    TimeSpan tp = DateTime.Now - Convert.ToDateTime((gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GIOORDER)));
                    if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(0, 173, 239);
                    else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(214, 223, 35);
                    else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(247, 147, 29);
                }

                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;

                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GAP).ToString() == "1")
                {
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(237, 28, 36);
                }
                else
                {
                    TimeSpan tp = DateTime.Now - Convert.ToDateTime((gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GIOORDER)));
                    if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(0, 173, 239);
                    else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(214, 223, 35);
                    else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(247, 147, 29);
                }

                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;

                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GAP).ToString() == "1")
                {
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(237, 28, 36);
                }
                else
                {
                    TimeSpan tp = DateTime.Now - Convert.ToDateTime((gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GIOORDER)));
                    if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(0, 173, 239);
                    else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(214, 223, 35);
                    else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(247, 147, 29);
                }

                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;

                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GAP).ToString() == "1")
                {
                    gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(237, 28, 36);
                }
                else
                {
                    TimeSpan tp = DateTime.Now - Convert.ToDateTime((gridView2.GetRowCellValue(gridView2.FocusedRowHandle, GIOORDER)));
                    if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(0, 173, 239);
                    else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(214, 223, 35);
                    else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                        gridView2.Appearance.FocusedRow.BackColor = Color.FromArgb(247, 147, 29);
                }

                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnMonlamxong_Click(object sender, EventArgs e)
        {
            Frm_Bep_xemlai frm = new Frm_Bep_xemlai(cbbTG.Text);
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
        }

        private void btnMonhetNL_Click(object sender, EventArgs e)
        {
            Frm_Bep_xem_monhetNL frm = new Frm_Bep_xem_monhetNL(cbbTG.Text);
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
            Load_MucTG();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void cbbMucTG_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void gridView2_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (View.GetRowCellValue(e.RowHandle, View.Columns["GAP"]).ToString()=="1")
                    {
                        e.Appearance.BackColor = Color.FromArgb(237, 28, 36);
                    }
                    else
                    {
                        TimeSpan tp = DateTime.Now - Convert.ToDateTime(View.GetRowCellValue(e.RowHandle, View.Columns["GIOORDER"]));
                        if (tp.TotalMinutes < Convert.ToDouble( btnMucTG2L.Tag))
                            e.Appearance.BackColor = Color.FromArgb(0, 173, 239);
                        else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                            e.Appearance.BackColor = Color.FromArgb(214, 223, 35);
                        else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                            e.Appearance.BackColor = Color.FromArgb(247, 147, 29);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnXong_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "update DISPLAY_ORDER set TRANGTHAI=3,GIOLAMXONG=GETDATE() where MA_HANGHOA='" + ma_hanghoa + "' and MA_BAN='"
                    + ma_ban + "' AND STT='" + stt + "'";
                if (clsMain.ExecuteSQL(sql))
                    LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDanglam_Click(object sender, EventArgs e)
        {
            try
            {
                string sql=string.Empty;
                if (gridView2.GetFocusedRowCellValue(TRANGTHAI).ToString() == "Đang làm")
                {
                    sql = "update DISPLAY_ORDER set TRANGTHAI=0 where MA_HANGHOA='" + ma_hanghoa + "' and MA_BAN='"
                    + ma_ban + "' AND STT='" + stt + "'";
                }
                else
                {
                    sql = "update DISPLAY_ORDER set TRANGTHAI=1 where MA_HANGHOA='" + ma_hanghoa + "' and MA_BAN='"
                    + ma_ban + "' AND STT='" + stt + "'";
                }
                if (clsMain.ExecuteSQL(sql))
                    LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnHetNL_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "update DISPLAY_ORDER set TRANGTHAI=2 where MA_HANGHOA='" + ma_hanghoa + "' and MA_BAN='"
                   + ma_ban + "' AND STT='" + stt + "'";
                if (clsMain.ExecuteSQL(sql))
                    LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Frm_Manhinhbep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                btnDanglam_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F3)
            {
                btnXong_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F4)
            {
                btnHetNL_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F5)
            {
                btnMonlamxong_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F6)
            {
                btnMonhetNL_Click(sender, e);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }
    }
}