﻿namespace KP_RES
{
    partial class UC_Ban_Bep
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.YEUCAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOORDER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGSL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STTODER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblGio = new DevExpress.XtraEditors.LabelControl();
            this.lblBan = new DevExpress.XtraEditors.LabelControl();
            this.lblSTT = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.gridControl2);
            this.panelControl8.Controls.Add(this.panelControl1);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(310, 300);
            this.panelControl8.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 33);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(306, 265);
            this.gridControl2.TabIndex = 24;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView2.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 25;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.YEUCAU,
            this.GIOORDER,
            this.TEN_KHUVUC,
            this.TEN_BAN,
            this.TRANGTHAI,
            this.TONGSL,
            this.GAP,
            this.STTODER,
            this.MA_HOADON});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GAP, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GIOORDER, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_BAN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "X";
            this.gridColumn1.FieldName = "CHON";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 25;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.Caption = "Hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.OptionsFilter.AllowAutoFilter = false;
            this.TEN_HANGHOA.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 152;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowAutoFilter = false;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 31;
            // 
            // YEUCAU
            // 
            this.YEUCAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.YEUCAU.AppearanceCell.Options.UseFont = true;
            this.YEUCAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.YEUCAU.AppearanceHeader.Options.UseFont = true;
            this.YEUCAU.AppearanceHeader.Options.UseTextOptions = true;
            this.YEUCAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.YEUCAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.YEUCAU.Caption = "YC Thêm";
            this.YEUCAU.FieldName = "YEUCAU";
            this.YEUCAU.Name = "YEUCAU";
            this.YEUCAU.OptionsColumn.AllowEdit = false;
            this.YEUCAU.OptionsColumn.AllowFocus = false;
            this.YEUCAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.YEUCAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.YEUCAU.OptionsFilter.AllowAutoFilter = false;
            this.YEUCAU.Visible = true;
            this.YEUCAU.VisibleIndex = 3;
            this.YEUCAU.Width = 96;
            // 
            // GIOORDER
            // 
            this.GIOORDER.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIOORDER.AppearanceCell.Options.UseFont = true;
            this.GIOORDER.AppearanceCell.Options.UseTextOptions = true;
            this.GIOORDER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOORDER.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOORDER.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOORDER.AppearanceHeader.Options.UseFont = true;
            this.GIOORDER.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOORDER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOORDER.Caption = "Giờ Order";
            this.GIOORDER.DisplayFormat.FormatString = "HH:mm:ss";
            this.GIOORDER.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GIOORDER.FieldName = "GIOORDER";
            this.GIOORDER.Name = "GIOORDER";
            this.GIOORDER.OptionsColumn.AllowEdit = false;
            this.GIOORDER.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIOORDER.OptionsColumn.FixedWidth = true;
            this.GIOORDER.Width = 128;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Phục Vụ";
            this.TEN_KHUVUC.FieldName = "NHANVIEN";
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_KHUVUC.OptionsColumn.FixedWidth = true;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.Width = 110;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.FixedWidth = true;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.Width = 69;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRANGTHAI.AppearanceCell.Options.UseFont = true;
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng Thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TRANGTHAI.OptionsColumn.AllowMove = false;
            this.TRANGTHAI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Width = 100;
            // 
            // TONGSL
            // 
            this.TONGSL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TONGSL.AppearanceCell.Options.UseFont = true;
            this.TONGSL.AppearanceCell.Options.UseTextOptions = true;
            this.TONGSL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGSL.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGSL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONGSL.AppearanceHeader.Options.UseFont = true;
            this.TONGSL.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGSL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGSL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGSL.Caption = "Tổng SL";
            this.TONGSL.FieldName = "TONGSL";
            this.TONGSL.Name = "TONGSL";
            this.TONGSL.OptionsColumn.AllowEdit = false;
            this.TONGSL.OptionsColumn.AllowFocus = false;
            this.TONGSL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TONGSL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TONGSL.OptionsColumn.FixedWidth = true;
            this.TONGSL.OptionsFilter.AllowAutoFilter = false;
            this.TONGSL.OptionsFilter.AllowFilter = false;
            this.TONGSL.Width = 80;
            // 
            // GAP
            // 
            this.GAP.Caption = "GAP";
            this.GAP.FieldName = "GAP";
            this.GAP.Name = "GAP";
            this.GAP.OptionsColumn.AllowEdit = false;
            this.GAP.OptionsColumn.AllowFocus = false;
            // 
            // STTODER
            // 
            this.STTODER.Caption = "STTODER";
            this.STTODER.FieldName = "STTODER";
            this.STTODER.Name = "STTODER";
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.Caption = "Hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.Width = 204;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblGio);
            this.panelControl1.Controls.Add(this.lblBan);
            this.panelControl1.Controls.Add(this.lblSTT);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(306, 31);
            this.panelControl1.TabIndex = 25;
            // 
            // lblGio
            // 
            this.lblGio.Appearance.BackColor = System.Drawing.Color.White;
            this.lblGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGio.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGio.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGio.Location = new System.Drawing.Point(167, 2);
            this.lblGio.Margin = new System.Windows.Forms.Padding(4);
            this.lblGio.Name = "lblGio";
            this.lblGio.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.lblGio.Size = new System.Drawing.Size(145, 27);
            this.lblGio.TabIndex = 26;
            this.lblGio.Text = "1";
            this.lblGio.Click += new System.EventHandler(this.lblBan_Click);
            // 
            // lblBan
            // 
            this.lblBan.Appearance.BackColor = System.Drawing.Color.White;
            this.lblBan.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBan.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblBan.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblBan.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblBan.Location = new System.Drawing.Point(50, 2);
            this.lblBan.Margin = new System.Windows.Forms.Padding(4);
            this.lblBan.Name = "lblBan";
            this.lblBan.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.lblBan.Size = new System.Drawing.Size(117, 27);
            this.lblBan.TabIndex = 25;
            this.lblBan.Text = "1";
            this.lblBan.Click += new System.EventHandler(this.lblBan_Click);
            // 
            // lblSTT
            // 
            this.lblSTT.Appearance.BackColor = System.Drawing.Color.White;
            this.lblSTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblSTT.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSTT.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSTT.Location = new System.Drawing.Point(2, 2);
            this.lblSTT.Margin = new System.Windows.Forms.Padding(4);
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.lblSTT.Size = new System.Drawing.Size(48, 27);
            this.lblSTT.TabIndex = 24;
            this.lblSTT.Text = "1";
            this.lblSTT.Click += new System.EventHandler(this.lblBan_Click);
            // 
            // UC_Ban_Bep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl8);
            this.Name = "UC_Ban_Bep";
            this.Size = new System.Drawing.Size(310, 300);
            this.Load += new System.EventHandler(this.UC_Ban_Bep_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn YEUCAU;
        private DevExpress.XtraGrid.Columns.GridColumn GIOORDER;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TONGSL;
        private DevExpress.XtraGrid.Columns.GridColumn GAP;
        private DevExpress.XtraGrid.Columns.GridColumn STTODER;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblGio;
        private DevExpress.XtraEditors.LabelControl lblBan;
        private DevExpress.XtraEditors.LabelControl lblSTT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
