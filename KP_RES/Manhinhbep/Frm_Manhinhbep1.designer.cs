﻿namespace KP_RES 
{
    partial class Frm_Manhinhbep1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Manhinhbep1));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lbHeader = new DevExpress.XtraEditors.LabelControl();
            this.palXem = new DevExpress.XtraEditors.PanelControl();
            this.cbbTG = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnMonlamxong = new DevExpress.XtraEditors.SimpleButton();
            this.btnMonhetNL = new DevExpress.XtraEditors.SimpleButton();
            this.cboBep = new DevExpress.XtraEditors.LookUpEdit();
            this.btnDanglam = new DevExpress.XtraEditors.SimpleButton();
            this.btnHetNL = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.fpnMain = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.btnMucTG2N = new DevExpress.XtraEditors.PictureEdit();
            this.btnMucTG2L = new DevExpress.XtraEditors.PictureEdit();
            this.btnMucTG1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnXong = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnUutien = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palXem)).BeginInit();
            this.palXem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG2N.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG2L.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lbHeader);
            this.panelControl1.Controls.Add(this.palXem);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1028, 48);
            this.panelControl1.TabIndex = 0;
            // 
            // lbHeader
            // 
            this.lbHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeader.Location = new System.Drawing.Point(5, 12);
            this.lbHeader.Margin = new System.Windows.Forms.Padding(4);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(77, 23);
            this.lbHeader.TabIndex = 21;
            this.lbHeader.Text = "Khu vực";
            this.lbHeader.Visible = false;
            // 
            // palXem
            // 
            this.palXem.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palXem.Controls.Add(this.cbbTG);
            this.palXem.Controls.Add(this.btnMonlamxong);
            this.palXem.Controls.Add(this.btnMonhetNL);
            this.palXem.Controls.Add(this.cboBep);
            this.palXem.Dock = System.Windows.Forms.DockStyle.Right;
            this.palXem.Location = new System.Drawing.Point(336, 2);
            this.palXem.Margin = new System.Windows.Forms.Padding(0);
            this.palXem.Name = "palXem";
            this.palXem.Size = new System.Drawing.Size(690, 44);
            this.palXem.TabIndex = 20;
            // 
            // cbbTG
            // 
            this.cbbTG.EditValue = "5 phút";
            this.cbbTG.Location = new System.Drawing.Point(271, 7);
            this.cbbTG.Name = "cbbTG";
            this.cbbTG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTG.Properties.Appearance.Options.UseFont = true;
            this.cbbTG.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTG.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbbTG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbTG.Properties.DropDownItemHeight = 40;
            this.cbbTG.Properties.Items.AddRange(new object[] {
            "5 phút",
            "15 phút",
            "30 phút",
            "60 phút",
            "2 tiếng",
            "1 ngày"});
            this.cbbTG.Properties.MaxLength = 30;
            this.cbbTG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbbTG.Size = new System.Drawing.Size(98, 30);
            this.cbbTG.TabIndex = 21;
            this.cbbTG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbMucTG_KeyPress);
            // 
            // btnMonlamxong
            // 
            this.btnMonlamxong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonlamxong.Appearance.Options.UseFont = true;
            this.btnMonlamxong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMonlamxong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnMonlamxong.Location = new System.Drawing.Point(372, 0);
            this.btnMonlamxong.Margin = new System.Windows.Forms.Padding(0);
            this.btnMonlamxong.Name = "btnMonlamxong";
            this.btnMonlamxong.Size = new System.Drawing.Size(159, 44);
            this.btnMonlamxong.TabIndex = 17;
            this.btnMonlamxong.Text = "Món làm xong (F5)";
            this.btnMonlamxong.Click += new System.EventHandler(this.btnMonlamxong_Click);
            // 
            // btnMonhetNL
            // 
            this.btnMonhetNL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonhetNL.Appearance.Options.UseFont = true;
            this.btnMonhetNL.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMonhetNL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnMonhetNL.Location = new System.Drawing.Point(531, 0);
            this.btnMonhetNL.Margin = new System.Windows.Forms.Padding(0);
            this.btnMonhetNL.Name = "btnMonhetNL";
            this.btnMonhetNL.Size = new System.Drawing.Size(159, 44);
            this.btnMonhetNL.TabIndex = 22;
            this.btnMonhetNL.Text = "Món hết NL (F6)";
            this.btnMonhetNL.Click += new System.EventHandler(this.btnMonhetNL_Click);
            // 
            // cboBep
            // 
            this.cboBep.EditValue = "TEN";
            this.cboBep.Location = new System.Drawing.Point(75, 7);
            this.cboBep.Name = "cboBep";
            this.cboBep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.Appearance.Options.UseFont = true;
            this.cboBep.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBep.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboBep.Properties.DisplayMember = "TEN";
            this.cboBep.Properties.DropDownItemHeight = 40;
            this.cboBep.Properties.MaxLength = 30;
            this.cboBep.Properties.NullText = "";
            this.cboBep.Properties.PopupSizeable = false;
            this.cboBep.Properties.ShowHeader = false;
            this.cboBep.Properties.ValueMember = "MA";
            this.cboBep.Size = new System.Drawing.Size(193, 30);
            this.cboBep.TabIndex = 23;
            this.cboBep.EditValueChanged += new System.EventHandler(this.cboBep_EditValueChanged);
            // 
            // btnDanglam
            // 
            this.btnDanglam.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDanglam.Appearance.Options.UseFont = true;
            this.btnDanglam.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDanglam.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDanglam.Location = new System.Drawing.Point(187, 0);
            this.btnDanglam.Margin = new System.Windows.Forms.Padding(0);
            this.btnDanglam.Name = "btnDanglam";
            this.btnDanglam.Size = new System.Drawing.Size(128, 44);
            this.btnDanglam.TabIndex = 11;
            this.btnDanglam.Text = "Đang Làm (F2)";
            this.btnDanglam.Click += new System.EventHandler(this.btnDanglam_Click);
            // 
            // btnHetNL
            // 
            this.btnHetNL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHetNL.Appearance.Options.UseFont = true;
            this.btnHetNL.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHetNL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHetNL.Location = new System.Drawing.Point(443, 0);
            this.btnHetNL.Margin = new System.Windows.Forms.Padding(0);
            this.btnHetNL.Name = "btnHetNL";
            this.btnHetNL.Size = new System.Drawing.Size(128, 44);
            this.btnHetNL.TabIndex = 12;
            this.btnHetNL.Text = "Hết NL (F4)";
            this.btnHetNL.Click += new System.EventHandler(this.btnHetNL_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.fpnMain);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 48);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1028, 645);
            this.panelControl5.TabIndex = 16;
            // 
            // fpnMain
            // 
            this.fpnMain.AutoScroll = true;
            this.fpnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnMain.Location = new System.Drawing.Point(2, 2);
            this.fpnMain.Name = "fpnMain";
            this.fpnMain.Size = new System.Drawing.Size(985, 641);
            this.fpnMain.TabIndex = 5;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(987, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 641);
            this.panelControl2.TabIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 477);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 157);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 317);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 397);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 559);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.btnMucTG2N);
            this.panelControl6.Controls.Add(this.btnMucTG2L);
            this.panelControl6.Controls.Add(this.btnMucTG1);
            this.panelControl6.Controls.Add(this.pictureEdit5);
            this.panelControl6.Controls.Add(this.panelControl7);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(0, 693);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1028, 48);
            this.panelControl6.TabIndex = 17;
            // 
            // btnMucTG2N
            // 
            this.btnMucTG2N.Enabled = false;
            this.btnMucTG2N.Location = new System.Drawing.Point(236, 5);
            this.btnMucTG2N.Name = "btnMucTG2N";
            this.btnMucTG2N.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(239)))));
            this.btnMucTG2N.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2N.Properties.Appearance.Options.UseBackColor = true;
            this.btnMucTG2N.Properties.Appearance.Options.UseFont = true;
            this.btnMucTG2N.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2N.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.btnMucTG2N.Properties.AppearanceDisabled.Options.UseFont = true;
            this.btnMucTG2N.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.btnMucTG2N.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2N.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.btnMucTG2N.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnMucTG2N.Properties.NullText = "Món ";
            this.btnMucTG2N.Properties.ReadOnly = true;
            this.btnMucTG2N.Size = new System.Drawing.Size(77, 35);
            this.btnMucTG2N.TabIndex = 30;
            // 
            // btnMucTG2L
            // 
            this.btnMucTG2L.Enabled = false;
            this.btnMucTG2L.Location = new System.Drawing.Point(159, 5);
            this.btnMucTG2L.Name = "btnMucTG2L";
            this.btnMucTG2L.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(35)))));
            this.btnMucTG2L.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2L.Properties.Appearance.Options.UseBackColor = true;
            this.btnMucTG2L.Properties.Appearance.Options.UseFont = true;
            this.btnMucTG2L.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2L.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.btnMucTG2L.Properties.AppearanceDisabled.Options.UseFont = true;
            this.btnMucTG2L.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.btnMucTG2L.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG2L.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.btnMucTG2L.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnMucTG2L.Properties.NullText = "Món ";
            this.btnMucTG2L.Properties.ReadOnly = true;
            this.btnMucTG2L.Size = new System.Drawing.Size(77, 35);
            this.btnMucTG2L.TabIndex = 29;
            // 
            // btnMucTG1
            // 
            this.btnMucTG1.Enabled = false;
            this.btnMucTG1.Location = new System.Drawing.Point(82, 5);
            this.btnMucTG1.Name = "btnMucTG1";
            this.btnMucTG1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(147)))), ((int)(((byte)(29)))));
            this.btnMucTG1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG1.Properties.Appearance.Options.UseBackColor = true;
            this.btnMucTG1.Properties.Appearance.Options.UseFont = true;
            this.btnMucTG1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.btnMucTG1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.btnMucTG1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.btnMucTG1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMucTG1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.btnMucTG1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnMucTG1.Properties.NullText = "Món ";
            this.btnMucTG1.Properties.ReadOnly = true;
            this.btnMucTG1.Size = new System.Drawing.Size(77, 35);
            this.btnMucTG1.TabIndex = 28;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Enabled = false;
            this.pictureEdit5.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(28)))), ((int)(((byte)(36)))));
            this.pictureEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.pictureEdit5.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.pictureEdit5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.NullText = "Món gấp";
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Size = new System.Drawing.Size(77, 35);
            this.pictureEdit5.TabIndex = 27;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.btnUutien);
            this.panelControl7.Controls.Add(this.btnDanglam);
            this.panelControl7.Controls.Add(this.btnXong);
            this.panelControl7.Controls.Add(this.btnHetNL);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl7.Location = new System.Drawing.Point(455, 2);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(571, 44);
            this.panelControl7.TabIndex = 23;
            // 
            // btnXong
            // 
            this.btnXong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXong.Appearance.Options.UseFont = true;
            this.btnXong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXong.Location = new System.Drawing.Point(315, 0);
            this.btnXong.Margin = new System.Windows.Forms.Padding(0);
            this.btnXong.Name = "btnXong";
            this.btnXong.Size = new System.Drawing.Size(128, 44);
            this.btnXong.TabIndex = 16;
            this.btnXong.Text = "Xong (F3)";
            this.btnXong.Click += new System.EventHandler(this.btnXong_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnUutien
            // 
            this.btnUutien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUutien.Appearance.Options.UseFont = true;
            this.btnUutien.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnUutien.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnUutien.Location = new System.Drawing.Point(59, 0);
            this.btnUutien.Margin = new System.Windows.Forms.Padding(0);
            this.btnUutien.Name = "btnUutien";
            this.btnUutien.Size = new System.Drawing.Size(128, 44);
            this.btnUutien.TabIndex = 17;
            this.btnUutien.Text = "Ưu tiên (F7)";
            this.btnUutien.Click += new System.EventHandler(this.btnUutien_Click);
            // 
            // Frm_Manhinhbep1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 741);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl6);
            this.KeyPreview = true;
            this.Name = "Frm_Manhinhbep1";
            this.Text = "Màn Hình Bếp 1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Manhinhbep1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palXem)).EndInit();
            this.palXem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbTG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG2N.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG2L.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMucTG1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDanglam;
        private DevExpress.XtraEditors.SimpleButton btnHetNL;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnXong;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl palXem;
        private DevExpress.XtraEditors.SimpleButton btnMonlamxong;
        private DevExpress.XtraEditors.ComboBoxEdit cbbTG;
        private DevExpress.XtraEditors.SimpleButton btnMonhetNL;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl lbHeader;
        private DevExpress.XtraEditors.PictureEdit btnMucTG1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit btnMucTG2N;
        private DevExpress.XtraEditors.PictureEdit btnMucTG2L;
        private System.Windows.Forms.FlowLayoutPanel fpnMain;
        private DevExpress.XtraEditors.LookUpEdit cboBep;
        private DevExpress.XtraEditors.SimpleButton btnUutien;




    }
}