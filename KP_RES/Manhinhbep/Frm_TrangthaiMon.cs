﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.Grid;

namespace KP_RES 
{
    public partial class Frm_TrangthaiMon : DevExpress.XtraEditors.XtraForm
    {
        //
        //TRANGTHAI, 0:chưa chọn; 1:chọn đang làm; 2:Hết nguyên liệu; 3:đã làm xong
        //
        public Frm_TrangthaiMon()
        {
            InitializeComponent();
            LoadCombo();
            LoaddataGridView();
            timer1.Start();
        }

        private void LoadCombo()
        {
            string sql = "select MA_KHUVUC AS MA,TEN_KHUVUC AS TEN from KHUVUC where SUDUNG=1";
            DataTable dt = clsMain.ReturnDataTable(sql);
            dt.Rows.Add("0", "Tấc cả");
            cboKhuvuc.Properties.DataSource = dt;
            cboKhuvuc.EditValue = 0;
        }

        private void LoaddataGridView()
        {
            try
            {
                string sSQL = "";
                sSQL += "Select h.MA_HANGHOA,h.TEN_HANGHOA,GIOORDER,TEN_KHUVUC,TEN_BAN,YEUCAU,d.SOLUONG,TRANGTHAI AS TT" + "\n";
                sSQL += "From HANGHOA h inner join DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA" + "\n";
                sSQL += "AND FLAG=1 AND CONVERT(VARCHAR(10),GIOORDER,111)>=CONVERT(VARCHAR(10),GETDATE(),111)" + "\n";
                sSQL += "inner join BAN b on b.MA_BAN=d.MA_BAN ";
                if(int.Parse(cboKhuvuc.EditValue.ToString()) == 0)
                    sSQL += "AND ISMO_BAN=1 inner join KHUVUC k  on k.MA_KHUVUC=b.MA_KHUVUC order by GIOORDER";
                else
                    sSQL += "AND MA_KHUVUC='" + cboKhuvuc.EditValue + "' AND ISMO_BAN=1 inner join KHUVUC k  on k.MA_KHUVUC=b.MA_KHUVUC order by GIOORDER";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                dt.Columns.Add("TRANGTHAI");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["TT"].ToString() == "0")
                        dr["TRANGTHAI"] = "ORDER";
                    else if (dr["TT"].ToString() == "1")
                        dr["TRANGTHAI"] = "Đang làm";
                    else if (dr["TT"].ToString() == "2")
                        dr["TRANGTHAI"] = "Hết NL";
                    else if (dr["TT"].ToString() == "3")
                        dr["TRANGTHAI"] = "Đã xong";

                    dr["YEUCAU"] = dr["YEUCAU"].ToString().Replace(", LAM GAP", "");
                    dr["YEUCAU"] = dr["YEUCAU"].ToString().Replace("LAM GAP,", "");
                    dr["YEUCAU"] = dr["YEUCAU"].ToString().Replace("LAM GAP", "");
                }
                gridControl2.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView2_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (View.GetRowCellValue(e.RowHandle, View.Columns["TRANGTHAI"]).ToString() == "ORDER")
                    e.Appearance.BackColor = Color.FromArgb(0, 173, 239);
                if (View.GetRowCellValue(e.RowHandle, View.Columns["TRANGTHAI"]).ToString() == "Đang làm")
                    e.Appearance.BackColor = Color.FromArgb(214, 223, 35);
                if (View.GetRowCellValue(e.RowHandle, View.Columns["TRANGTHAI"]).ToString() == "Hết NL")
                    e.Appearance.BackColor = Color.FromArgb(237, 28, 36);
                if (View.GetRowCellValue(e.RowHandle, View.Columns["TRANGTHAI"]).ToString() == "Đã xong")
                    e.Appearance.BackColor = Color.FromArgb(33, 175, 7);
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }


        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            //string second;
            //second = DateTime.Now.Second.ToString();
            //int a = Convert.ToInt32(second);
            //if (a % 30 == 0)
            //{
                LoaddataGridView(); 
            //}
        }

        private void btnKhuvuc_Click(object sender, EventArgs e)
        {
            if (palXem.Visible)
                palXem.Visible = false;
            else
            {
                palXem.Visible = true;
            }
        }

        private void cboKhuvuc_EditValueChanged(object sender, EventArgs e)
        {
            LoaddataGridView();
        }
    }
}