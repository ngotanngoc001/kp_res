﻿namespace  KP_RES 
{
    partial class Frm_Goikhach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnTren = new DevExpress.XtraEditors.PanelControl();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.pnNgoai = new DevExpress.XtraEditors.PanelControl();
            this.pnGiua = new DevExpress.XtraEditors.PanelControl();
            this.pnHanghoa = new DevExpress.XtraEditors.PanelControl();
            this.pnlNhomhang = new System.Windows.Forms.FlowLayoutPanel();
            this.pnDuoi = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.lblTrang = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnTren)).BeginInit();
            this.pnTren.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNgoai)).BeginInit();
            this.pnNgoai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnGiua)).BeginInit();
            this.pnGiua.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).BeginInit();
            this.pnHanghoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDuoi)).BeginInit();
            this.pnDuoi.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTren
            // 
            this.pnTren.Controls.Add(this.btnLenHangHoa);
            this.pnTren.Controls.Add(this.btnCapNhat);
            this.pnTren.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTren.Location = new System.Drawing.Point(2, 2);
            this.pnTren.Name = "pnTren";
            this.pnTren.Size = new System.Drawing.Size(1045, 35);
            this.pnTren.TabIndex = 0;
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Appearance.Options.UseTextOptions = true;
            this.btnLenHangHoa.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLenHangHoa.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(142, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(901, 31);
            this.btnLenHangHoa.TabIndex = 52;
            this.btnLenHangHoa.Text = "Khu vực";
            this.btnLenHangHoa.Click += new System.EventHandler(this.btnLenHangHoa_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhat.Appearance.Options.UseFont = true;
            this.btnCapNhat.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCapNhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapNhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCapNhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapNhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(140, 31);
            this.btnCapNhat.TabIndex = 53;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // pnNgoai
            // 
            this.pnNgoai.Controls.Add(this.pnGiua);
            this.pnNgoai.Controls.Add(this.pnDuoi);
            this.pnNgoai.Controls.Add(this.pnTren);
            this.pnNgoai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNgoai.Location = new System.Drawing.Point(0, 0);
            this.pnNgoai.Name = "pnNgoai";
            this.pnNgoai.Size = new System.Drawing.Size(1049, 612);
            this.pnNgoai.TabIndex = 1;
            // 
            // pnGiua
            // 
            this.pnGiua.Controls.Add(this.pnHanghoa);
            this.pnGiua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGiua.Location = new System.Drawing.Point(2, 37);
            this.pnGiua.Name = "pnGiua";
            this.pnGiua.Size = new System.Drawing.Size(1045, 538);
            this.pnGiua.TabIndex = 1;
            // 
            // pnHanghoa
            // 
            this.pnHanghoa.Controls.Add(this.pnlNhomhang);
            this.pnHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHanghoa.Location = new System.Drawing.Point(2, 2);
            this.pnHanghoa.Name = "pnHanghoa";
            this.pnHanghoa.Size = new System.Drawing.Size(1041, 534);
            this.pnHanghoa.TabIndex = 1;
            // 
            // pnlNhomhang
            // 
            this.pnlNhomhang.AutoScroll = true;
            this.pnlNhomhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNhomhang.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pnlNhomhang.Location = new System.Drawing.Point(2, 2);
            this.pnlNhomhang.Name = "pnlNhomhang";
            this.pnlNhomhang.Size = new System.Drawing.Size(1037, 530);
            this.pnlNhomhang.TabIndex = 59;
            // 
            // pnDuoi
            // 
            this.pnDuoi.Controls.Add(this.btnXuongHangHoa);
            this.pnDuoi.Controls.Add(this.lblTrang);
            this.pnDuoi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnDuoi.Location = new System.Drawing.Point(2, 575);
            this.pnDuoi.Name = "pnDuoi";
            this.pnDuoi.Size = new System.Drawing.Size(1045, 35);
            this.pnDuoi.TabIndex = 2;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Appearance.Options.UseTextOptions = true;
            this.btnXuongHangHoa.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(142, 2);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(901, 31);
            this.btnXuongHangHoa.TabIndex = 52;
            this.btnXuongHangHoa.Text = "Khu vực";
            this.btnXuongHangHoa.Click += new System.EventHandler(this.btnXuongHangHoa_Click);
            // 
            // lblTrang
            // 
            this.lblTrang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrang.Appearance.Options.UseFont = true;
            this.lblTrang.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTrang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lblTrang.Location = new System.Drawing.Point(2, 2);
            this.lblTrang.Margin = new System.Windows.Forms.Padding(4);
            this.lblTrang.Name = "lblTrang";
            this.lblTrang.Size = new System.Drawing.Size(140, 31);
            this.lblTrang.TabIndex = 51;
            this.lblTrang.Text = "1/1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Frm_Goikhach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 612);
            this.Controls.Add(this.pnNgoai);
            this.Name = "Frm_Goikhach";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gọi khách nhận hàng";
            this.Load += new System.EventHandler(this.Frm_Goikhach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnTren)).EndInit();
            this.pnTren.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNgoai)).EndInit();
            this.pnNgoai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnGiua)).EndInit();
            this.pnGiua.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).EndInit();
            this.pnHanghoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDuoi)).EndInit();
            this.pnDuoi.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnTren;
        private DevExpress.XtraEditors.PanelControl pnNgoai;
        private DevExpress.XtraEditors.PanelControl pnDuoi;
        private DevExpress.XtraEditors.PanelControl pnGiua;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton lblTrang;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.PanelControl pnHanghoa;
        private System.Windows.Forms.FlowLayoutPanel pnlNhomhang;
        private System.Windows.Forms.Timer timer1;



    }
}
