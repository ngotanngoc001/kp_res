﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Text.RegularExpressions;
using DevExpress.XtraGrid;

namespace KP_RES 
{
    public partial class Frm_Manhinhbep1 : DevExpress.XtraEditors.XtraForm
    {
        
        //TRANGTHAI, 0:chưa chọn order; 1:chọn đang làm; 2:Hết nguyên liệu; 3:đã làm xong
        public Frm_Manhinhbep1()
        {
            InitializeComponent();
            Load_MucTG();
            LoaddataGridView();
        }

        private void Frm_Manhinhbep1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                btnUutien_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                btnDanglam_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F3)
            {
                btnXong_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F4)
            {
                btnHetNL_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F5)
            {
                btnMonlamxong_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F6)
            {
                btnMonhetNL_Click(sender, e);
            }
        }

        private void Load_MucTG()
        {
            DataTable dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER1'");
            if (dt.Rows.Count > 0)
            {
                btnMucTG1.Properties.NullText = "Món>" + dt.Rows[0][0].ToString() + "P";
                btnMucTG1.Tag = dt.Rows[0][0].ToString();
            }
            dt = clsMain.ReturnDataTable("select GIATRI From CAUHINH where TEN='TIMEORDER2'");
            if (dt.Rows.Count > 0)
            {
                btnMucTG2L.Properties.NullText = "Món>" + dt.Rows[0][0].ToString() + "P";
                btnMucTG2N.Properties.NullText = "Món<" + dt.Rows[0][0].ToString() + "P";
                btnMucTG2L.Tag = dt.Rows[0][0].ToString();
            }

            cls_ConfigCashier.LoadCauHinh();
            DataTable dt1 = clsMain.ReturnDataTable("select MA_BEP as MA,	TEN_BEP AS TEN	 From dm_bep where MA_CUAHANG="+clsMain .SQLString (cls_ConfigCashier .idShop));
            dt1.Rows.Add("0", "Tất cả");
            cboBep.Properties.DataSource = dt1;
            cboBep.EditValue = 0;
        }

        private void LoaddataGridView()
        {
            DataTable dt = new DataTable();
            string sql = "";
            sql += "select h.MA_HANGHOA,h.TEN_HANGHOA,k.TEN_KHUVUC,b.MA_BAN,b.TEN_BAN,d.MA_HOADON,d.GIOORDER,d.YEUCAU,d.SOLUONG,d.NHANVIEN,d.STT AS STTODER,d.TRANGTHAI as TT" + "\n";
            sql += "From HANGHOA h " + "\n";
            if (cboBep .EditValue .ToString ()=="0")
            {
                sql += "inner join DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND d.FLAG=1 AND CONVERT(VARCHAR(10),d.GIOORDER,111)>=CONVERT(VARCHAR(10),GETDATE(),111) AND TRANGTHAI !=2 AND TRANGTHAI!=3" + "\n";
            }
            else
            {
                sql += "inner join DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND d.FLAG=1 AND CONVERT(VARCHAR(10),d.GIOORDER,111)>=CONVERT(VARCHAR(10),GETDATE(),111) AND TRANGTHAI !=2 AND TRANGTHAI!=3 AND h.MA_BEP='" + cboBep.EditValue .ToString () + "'" + "\n";
            }
            sql += "inner join BAN b on b.MA_BAN=d.MA_BAN" + "\n";
            sql += "inner join KHUVUC k  on k.MA_KHUVUC=b.MA_KHUVUC " + "\n";
            sql += "ORDER by d.GIOORDER" + "\n";

            dt = clsMain.ReturnDataTable(sql);
            dt.Columns.Add("TRANGTHAI");
            dt.Columns.Add("GAP");
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TT"].ToString() == "0")
                    dr["TRANGTHAI"] = "ORDER";
                else
                    dr["TRANGTHAI"] = "Đang làm";

                if (KP_Terminal .clsKP_Terminal .RemoveUnicode ( dr["YEUCAU"].ToString()).ToLower().Replace(" ","") .Contains("lamgap"))
                {
                    dr["GAP"] = "1";
                }
            }
            dt.DefaultView.Sort = "GAP desc,GIOORDER asc";
            dt = dt.DefaultView.ToTable(true);

            cls_KP_RES.sTT_DISPLAY_ORDER.Clear ();
            while (fpnMain.Controls.Count > 0)
            {
                fpnMain.Controls[0].Dispose();
            }
            fpnMain.Controls.Clear();
            string sMaban = "";
            int i = 0;
            DataTable dtTemp = dt.Copy();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["MA_BAN"].ToString() != sMaban)
                {
                    sMaban = dr["MA_BAN"].ToString();
                    i++;
                    dtTemp.Clear();
                    for (int j = dt.Rows.IndexOf(dr); j < dt.Rows.Count; j++)
                    {
                        if (dt.Rows[j]["MA_BAN"].ToString() == sMaban)
                        {
                            dtTemp.Rows.Add(dt.Rows[j].ItemArray);
                        }
                        else
                        {
                            break;
                        }
                    }
                  
                    UC_Ban_Bep bb = new UC_Ban_Bep();
                    bb.sMaban = dr["MA_BAN"].ToString();
                    bb.sTenban = dr["TEN_BAN"].ToString();
                    bb.sSTT = i.ToString();
                    bb.sGio = ((DateTime)dr["GIOORDER"]).ToString("HH:mm") + "-" + dr["NHANVIEN"].ToString();
                    bb.dt = dtTemp.Copy ();
                    bb.btnMucTG1.Tag = btnMucTG1.Tag;
                    bb.btnMucTG2L.Tag = btnMucTG2L.Tag; 
                    fpnMain.Controls.Add(bb);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            LoaddataGridView();
            timer1.Enabled = true;
        }

        private void cboBep_EditValueChanged(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void cbbMucTG_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                fpnMain.AutoScrollPosition = new Point(0, fpnMain.VerticalScroll.Maximum);
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                fpnMain.AutoScrollPosition = new Point(0, fpnMain.VerticalScroll.Value + fpnMain.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                fpnMain.AutoScrollPosition = new Point(0, fpnMain.VerticalScroll.Value - fpnMain.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                fpnMain.AutoScrollPosition = new Point(0, 0);
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnMonlamxong_Click(object sender, EventArgs e)
        {
            Frm_Bep_xemlai frm = new Frm_Bep_xemlai(cbbTG.Text);
            frm.ma = cboBep.EditValue.ToString();
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
        }

        private void btnMonhetNL_Click(object sender, EventArgs e)
        {
            Frm_Bep_xem_monhetNL frm = new Frm_Bep_xem_monhetNL(cbbTG.Text);
            frm.ma = cboBep.EditValue.ToString();
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
        }

        private void btnXong_Click(object sender, EventArgs e)
        {
            try
            {
                //cho 1  so mon
                string sql1 = "";
                if (cls_KP_RES.sTT_DISPLAY_ORDER.Count >0)
                {
                    foreach (string sSTTODER in cls_KP_RES.sTT_DISPLAY_ORDER)
                    {
                        sql1 += "update DISPLAY_ORDER set TRANGTHAI=3,GIOLAMXONG=GETDATE() where  STT=" + clsMain.SQLString(sSTTODER) + "\n";
                    }
                }
                //cho ca ban
                string sql = "";
                foreach (UC_Ban_Bep ucbb in fpnMain.Controls)
                {
                    if (ucbb.BorderStyle == BorderStyle.Fixed3D)
                    {
                        DataTable dtTmp = ucbb.dt;
                        foreach (DataRow dr in dtTmp.Rows)
                        {
                            sql += "update DISPLAY_ORDER set TRANGTHAI=3,GIOLAMXONG=GETDATE() where MA_HANGHOA=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + " and MA_BAN=" + clsMain.SQLString(dr["MA_BAN"].ToString()) + " AND STT=" + clsMain.SQLString(dr["STTODER"].ToString()) + "\n";
                        }
                    }
                }
                if (sql != "")
                {
                    clsMain.ExecuteSQL(sql);
                }
                else if (sql1 != "")
                {
                    clsMain.ExecuteSQL(sql1);
                }
                LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDanglam_Click(object sender, EventArgs e)
        {
            try
            {
                //cho 1  so mon
                string sql1 = "";
                if (cls_KP_RES.sTT_DISPLAY_ORDER.Count > 0)
                {
                    foreach (string sSTTODER in cls_KP_RES.sTT_DISPLAY_ORDER)
                    {
                        sql1 += "update DISPLAY_ORDER set TRANGTHAI=1,GIOLAMXONG=GETDATE() where  STT=" + clsMain.SQLString(sSTTODER) + "\n";
                    }
                }
                //cho ca ban
                string sql = "";
                foreach (UC_Ban_Bep ucbb in fpnMain.Controls)
                {
                    if (ucbb.BorderStyle == BorderStyle.Fixed3D)
                    {
                        DataTable dtTmp = ucbb.dt;
                        foreach (DataRow dr in dtTmp.Rows)
                        {
                            sql += "update DISPLAY_ORDER set TRANGTHAI=1,GIOLAMXONG=GETDATE() where MA_HANGHOA=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + " and MA_BAN=" + clsMain.SQLString(dr["MA_BAN"].ToString()) + " AND STT=" + clsMain.SQLString(dr["STTODER"].ToString()) + "\n";
                        }
                    }
                }
                if (sql != "")
                {
                    clsMain.ExecuteSQL(sql);
                }
                else if (sql1 != "")
                {
                    clsMain.ExecuteSQL(sql1);
                }
                LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnHetNL_Click(object sender, EventArgs e)
        {
            try
            {
                //cho 1  so mon
                string sql1 = "";
                if (cls_KP_RES.sTT_DISPLAY_ORDER.Count > 0)
                {
                    foreach (string sSTTODER in cls_KP_RES.sTT_DISPLAY_ORDER)
                    {
                        sql1 += "update DISPLAY_ORDER set TRANGTHAI=2,GIOLAMXONG=GETDATE() where  STT=" + clsMain.SQLString(sSTTODER) + "\n";
                    }
                }
                //cho ca ban
                string sql = "";
                foreach (UC_Ban_Bep ucbb in fpnMain.Controls)
                {
                    if (ucbb.BorderStyle == BorderStyle.Fixed3D)
                    {
                        DataTable dtTmp = ucbb.dt;
                        foreach (DataRow dr in dtTmp.Rows)
                        {
                            sql += "update DISPLAY_ORDER set TRANGTHAI=2,GIOLAMXONG=GETDATE() where MA_HANGHOA=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + " and MA_BAN=" + clsMain.SQLString(dr["MA_BAN"].ToString()) + " AND STT=" + clsMain.SQLString(dr["STTODER"].ToString()) + "\n";
                        }
                    }
                }
                if (sql != "")
                {
                    clsMain.ExecuteSQL(sql);
                }
                else if (sql1 != "")
                {
                    clsMain.ExecuteSQL(sql1);
                }
                LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnUutien_Click(object sender, EventArgs e)
        {
            try
            {
                //cho 1  so mon
                string sql1 = "";
                if (cls_KP_RES.sTT_DISPLAY_ORDER.Count > 0)
                {
                    foreach (string sSTTODER in cls_KP_RES.sTT_DISPLAY_ORDER)
                    {
                        sql1 += "update DISPLAY_ORDER set YEUCAU=YEUCAU+' lam gap' where  STT=" + clsMain.SQLString(sSTTODER) + "\n";
                    }
                }
                //cho ca ban
                string sql = "";
                foreach (UC_Ban_Bep ucbb in fpnMain.Controls)
                {
                    if (ucbb.BorderStyle == BorderStyle.Fixed3D)
                    {
                        DataTable dtTmp = ucbb.dt;
                        foreach (DataRow dr in dtTmp.Rows)
                        {
                            sql += "update DISPLAY_ORDER set YEUCAU=YEUCAU+ ' lam gap' where MA_HANGHOA=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + " and MA_BAN=" + clsMain.SQLString(dr["MA_BAN"].ToString()) + " AND STT=" + clsMain.SQLString(dr["STTODER"].ToString()) + "\n";
                        }
                    }
                }
                if (sql != "")
                {
                    clsMain.ExecuteSQL(sql);
                }
                else if (sql1 != "")
                {
                    clsMain.ExecuteSQL(sql1);
                }
                LoaddataGridView();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}