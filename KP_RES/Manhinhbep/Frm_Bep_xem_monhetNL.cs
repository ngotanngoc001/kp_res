﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using System.IO;
using DevExpress.XtraEditors;

namespace KP_RES
{
    public partial class Frm_Bep_xem_monhetNL : Form
    {
        public string ma = "";//Mã bếp
        int _tg = 0;

        public Frm_Bep_xem_monhetNL(string tg)
        {
            InitializeComponent();
            try
            {
                int tem = 0;

                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                string[] arr;
                arr = tg.Split(' ');
                tem = int.Parse(arr[0]);
                if (tem == 2)
                    _tg = 120;
                else if (tem == 1)
                    _tg = 1440;
                else
                    _tg = tem;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void loadData(int n)
        {
            if (ma == "")
            {
                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                if (File.Exists(Application.StartupPath + "\\BepSD.xml"))
                {
                    try
                    {
                        ds.ReadXml(Application.StartupPath + "\\BepSD.xml");
                        dt1 = ds.Tables[0];
                        ma = dt1.Rows[0]["MA_BEP"].ToString();
                    }
                    catch
                    {
                    }
                }
            }
            string sql = "select d.MA_HANGHOA,d.MA_BAN,d.STT as STTODER,h.TEN_HANGHOA,GIOORDER,TEN_KHUVUC,TEN_BAN,YEUCAU,d.SOLUONG from HANGHOA h inner join ";
            if (ma == "0")
            {
                sql += "DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND TRANGTHAI =2 and DATEDIFF(minute, GIOORDER, GETDATE())<='" + n + "' ";
            }
            else
            {
                sql += "DISPLAY_ORDER d on h.MA_HANGHOA=d.MA_HANGHOA AND TRANGTHAI =2 AND h.MA_BEP='" + ma + "' and DATEDIFF(minute, GIOORDER, GETDATE())<='" + n + "' ";
            }
            sql += "inner join BAN b on b.MA_BAN=d.MA_BAN inner join KHUVUC k  on k.MA_KHUVUC=b.MA_KHUVUC ORDER by GIOORDER";
            DataTable dt = clsMain.ReturnDataTable(sql);
            foreach (DataRow dr in dt.Rows)
            {
                dr["YEUCAU"] = dr["YEUCAU"].ToString().Replace("LAM GAP", "làm gấp");
            }
            gridControl2.DataSource = dt;
        }
        
        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnDanglam_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.RowCount > 0)
                {
                    string sql = "update DISPLAY_ORDER set TRANGTHAI=1 where MA_HANGHOA='" + gridView2.GetFocusedRowCellValue(MA_HANGHOA).ToString() + "' and MA_BAN='"
                       + gridView2.GetFocusedRowCellValue(MA_BAN).ToString() + "' AND STT='" + gridView2.GetFocusedRowCellValue(STTODER).ToString() + "'";
                    if (clsMain.ExecuteSQL(sql))
                        loadData(_tg);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;
            lbHeader.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_KHUVUC).ToString() + " > " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_BAN).ToString() + " > " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, TEN_HANGHOA).ToString() + " : " +
                            gridView2.GetRowCellValue(gridView2.FocusedRowHandle, SOLUONG).ToString();
            lbHeader.Visible = true;
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            loadData(_tg);
        }

        private void Frm_Bep_xem_monhetNL_Load(object sender, EventArgs e)
        {
            loadData(_tg);
        }
    }   
}
