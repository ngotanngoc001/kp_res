﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using System.IO;
using WMPLib;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;

namespace  KP_RES 
{
    public partial class Frm_Goikhach : XtraForm
    {
        public Frm_Goikhach()
        {
            InitializeComponent();
        }

        private void LoadImage()
        {
            try
            {
                pnlNhomhang.Controls.Clear();
                String sSQL = "EXEC SP_SelectHoaDon_GiaoHang ";
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                string sMaHoaDon = "";
                foreach (DataRow dr in myDT.Rows)
                {
                    if (sMaHoaDon == dr["MA_HOADON"].ToString())
                    {
                        continue;
                    }
                    else
                    {
                        sMaHoaDon = dr["MA_HOADON"].ToString();
                    }
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dr["MA_HOADON"].ToString();
                    btnBan_Mon.Text = dr["NUMBER"].ToString().ToUpper() + "\n" + dr["GHICHU1"].ToString().ToUpper() + "\n" + dr["MA_HOADON"].ToString();
                    btnBan_Mon.Tag = dr["GOI_KHACHHANG"].ToString().ToLower();
                    btnBan_Mon.Font = pnlNhomhang.Font;
                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;
                    btnBan_Mon.Size = new Size(168, 130);

                    btnBan_Mon.Click += new EventHandler(HangHoa_Click);
                    pnlNhomhang.Controls.Add(btnBan_Mon);
                    if (btnBan_Mon.Tag.ToString() == "true")
                    {
                        SetBackgroudOnClick(btnBan_Mon);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.Tag.ToString () == "false")
            {
                SetBackgroudOnClick(btn);
                btn.Tag = "1";
                string sql = "update HOADON set GOI_KHACHHANG=1 where MA_HOADON=" + clsMain.SQLString(btn.Name);
                clsMain.ExecuteSQL(sql );
            }
            else
            {
                string sql = "update HOADON set GIAOHANG=1 where MA_HOADON=" + clsMain.SQLString(btn.Name);
                clsMain.ExecuteSQL(sql);
                btn.Visible = false;
            }
        }

        private void btnLenHangHoa_Click(object sender, EventArgs e)
        {
            if (pnlNhomhang.HorizontalScroll.Visible)
            {
                try
                {
                    pnlNhomhang.AutoScrollPosition = new Point(pnlNhomhang.HorizontalScroll.Value - pnlNhomhang.HorizontalScroll.Maximum / 10, 0);
                    return;
                }
                catch
                {
                    pnlNhomhang.HorizontalScroll.Value = pnlNhomhang.HorizontalScroll.Minimum;
                    return;
                }
            }
        }

        private void btnXuongHangHoa_Click(object sender, EventArgs e)
        {
            if (pnlNhomhang.HorizontalScroll.Visible)
            {
                try
                {
                    pnlNhomhang.AutoScrollPosition = new Point(pnlNhomhang.HorizontalScroll.Value + pnlNhomhang.HorizontalScroll.Maximum / 10, 0);
                    return;
                }
                catch
                {
                    pnlNhomhang.HorizontalScroll.Value = pnlNhomhang.HorizontalScroll.Maximum;
                    return;
                }
            }
        }



        private void Frm_Goikhach_Load(object sender, EventArgs e)
        {
            LoadImage();
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            LoadImage();
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.Empty;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            LoadImage();
            timer1.Enabled = true;
        }
        
    }
}