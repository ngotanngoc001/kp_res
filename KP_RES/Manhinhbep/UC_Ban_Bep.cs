﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace KP_RES
{
    public partial class UC_Ban_Bep : DevExpress.XtraEditors.XtraUserControl
    {
        public string sMaban = "";
        public string sTenban = "";
        public string sSTT = "";
        public string sGio = "";
        public PictureEdit btnMucTG1=new PictureEdit();
        public PictureEdit btnMucTG2L =new PictureEdit();
        public DataTable dt = new DataTable ();
        public UC_Ban_Bep()
        {
            InitializeComponent();
        }

        Color time = new Color();
        private void UC_Ban_Bep_Load(object sender, EventArgs e)
        {
            lblSTT.Text  = sSTT ;
            lblBan.Text = sTenban;
            lblGio.Text = sGio;
            DataColumn dc = new DataColumn();
            dc.ColumnName = "CHON";
            dc.DataType = System.Type.GetType("System.Boolean");
            dc.DefaultValue = false;
            dt.Columns.Add(dc);
            gridControl2.DataSource = dt;
            if (dt.Rows[0]["GAP"].ToString() == "1")
            {
                time = Color.FromArgb(237, 28, 36);
            }
            else
            {
                TimeSpan tp = DateTime.Now - Convert.ToDateTime(dt.Rows[0]["GIOORDER"]);
                if (tp.TotalMinutes < Convert.ToDouble(btnMucTG2L.Tag))
                    time = Color.FromArgb(0, 173, 239);
                else if (tp.TotalMinutes >= Convert.ToDouble(btnMucTG2L.Tag) && tp.TotalMinutes < Convert.ToDouble(btnMucTG1.Tag))
                    time = Color.FromArgb(214, 223, 35);
                else if (tp.TotalMinutes > Convert.ToDouble(btnMucTG1.Tag))
                    time = Color.FromArgb(247, 147, 29);
            }
            lblSTT.BackColor = lblBan.BackColor = lblGio.BackColor = time;
        }

        private void lblBan_Click(object sender, EventArgs e)
        {
            if (this.BorderStyle == System.Windows.Forms.BorderStyle.None)
            {
                this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
                lblSTT.BackColor = lblBan.BackColor = lblGio.BackColor = Color.Green;
            }
            else
            {
                this.BorderStyle = System.Windows.Forms.BorderStyle.None;
                lblSTT.BackColor = lblBan.BackColor = lblGio.BackColor = time;
            }
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if ((Boolean)gridView2.GetFocusedRowCellValue("CHON"))
            {
                if (!cls_KP_RES.sTT_DISPLAY_ORDER.Contains(gridView2.GetFocusedRowCellValue("STTODER").ToString()))
                {
                    cls_KP_RES.sTT_DISPLAY_ORDER.Add(gridView2.GetFocusedRowCellValue("STTODER").ToString());
                }
            }
            else
            {
                if (cls_KP_RES.sTT_DISPLAY_ORDER.Contains(gridView2.GetFocusedRowCellValue("STTODER").ToString()))
                {
                    cls_KP_RES.sTT_DISPLAY_ORDER.Remove(gridView2.GetFocusedRowCellValue("STTODER").ToString());
                }
            }
        }
    }
}
