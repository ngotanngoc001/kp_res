﻿namespace KP_RES 
{
    partial class Frm_Main 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame1 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame2 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame3 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame4 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame5 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame6 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame7 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame8 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame9 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame10 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame11 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame12 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame13 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame14 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement29 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame15 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement30 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame16 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement31 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement32 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame17 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement33 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame18 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement34 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement35 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame19 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement36 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame20 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement37 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement38 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement39 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame21 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement40 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame22 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement41 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement42 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame23 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement43 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame24 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement44 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement45 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement46 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement47 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement48 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement49 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement50 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement51 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement52 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.timerGiaiphongbonho = new System.Windows.Forms.Timer(this.components);
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itDangxuat = new DevExpress.XtraEditors.TileItem();
            this.itTrogiup = new DevExpress.XtraEditors.TileItem();
            this.itCauhinh = new DevExpress.XtraEditors.TileItem();
            this.itHethong = new DevExpress.XtraEditors.TileItem();
            this.itThoat = new DevExpress.XtraEditors.TileItem();
            this.itThunho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itCaidat = new DevExpress.XtraEditors.TileItem();
            this.itBaocao = new DevExpress.XtraEditors.TileItem();
            this.itNhapxuatton = new DevExpress.XtraEditors.TileItem();
            this.itCongno = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itFastFood = new DevExpress.XtraEditors.TileItem();
            this.itNhahang = new DevExpress.XtraEditors.TileItem();
            this.itKaraok = new DevExpress.XtraEditors.TileItem();
            this.itMenu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itKhachsan = new DevExpress.XtraEditors.TileItem();
            this.itKhuvuichoi = new DevExpress.XtraEditors.TileItem();
            this.itVerapphim = new DevExpress.XtraEditors.TileItem();
            this.itShowTime = new DevExpress.XtraEditors.TileItem();
            this.tileGroup11 = new DevExpress.XtraEditors.TileGroup();
            this.itManhinhbep = new DevExpress.XtraEditors.TileItem();
            this.itKHTT = new DevExpress.XtraEditors.TileItem();
            this.itCongvan = new DevExpress.XtraEditors.TileItem();
            this.itNhansu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup13 = new DevExpress.XtraEditors.TileGroup();
            this.itDanhgiaKH = new DevExpress.XtraEditors.TileItem();
            this.itTheodoihopdong = new DevExpress.XtraEditors.TileItem();
            this.itSieuthi = new DevExpress.XtraEditors.TileItem();
            this.itTrasua = new DevExpress.XtraEditors.TileItem();
            this.tileGroup14 = new DevExpress.XtraEditors.TileGroup();
            this.itTrasua_Ban = new DevExpress.XtraEditors.TileItem();
            this.itBuffet = new DevExpress.XtraEditors.TileItem();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::KP_RES.WaitForm1), true, true);
            this.SuspendLayout();
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // timerGiaiphongbonho
            // 
            this.timerGiaiphongbonho.Enabled = true;
            this.timerGiaiphongbonho.Interval = 120000;
            this.timerGiaiphongbonho.Tick += new System.EventHandler(this.timerGiaiphongbonho_Tick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Name = "tileGroup10";
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_1366x768_Dealer;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup11);
            this.tileControl1.Groups.Add(this.tileGroup13);
            this.tileControl1.Groups.Add(this.tileGroup14);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(0, 0);
            this.tileControl1.MaxId = 32;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RowCount = 4;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(984, 562);
            this.tileControl1.TabIndex = 3;
            this.tileControl1.Text = "Trang chủ";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itDangxuat);
            this.tileGroup2.Items.Add(this.itTrogiup);
            this.tileGroup2.Items.Add(this.itCauhinh);
            this.tileGroup2.Items.Add(this.itHethong);
            this.tileGroup2.Items.Add(this.itThoat);
            this.tileGroup2.Items.Add(this.itThunho);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itDangxuat
            // 
            this.itDangxuat.BackgroundImage = global::KP_RES.Properties.Resources.dangxuat3;
            this.itDangxuat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Đăng xuất";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDangxuat.Elements.Add(tileItemElement1);
            this.itDangxuat.Id = 2;
            this.itDangxuat.Name = "itDangxuat";
            this.itDangxuat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDangxuat_ItemClick);
            // 
            // itTrogiup
            // 
            this.itTrogiup.BackgroundImage = global::KP_RES.Properties.Resources.huongdan;
            this.itTrogiup.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Trợ giúp";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrogiup.Elements.Add(tileItemElement2);
            this.itTrogiup.Id = 3;
            this.itTrogiup.Name = "itTrogiup";
            this.itTrogiup.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrogiup_ItemClick);
            // 
            // itCauhinh
            // 
            this.itCauhinh.BackgroundImage = global::KP_RES.Properties.Resources.cauhinh;
            this.itCauhinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cấu hình";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinh.Elements.Add(tileItemElement3);
            this.itCauhinh.Id = 4;
            this.itCauhinh.Name = "itCauhinh";
            this.itCauhinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinh_ItemClick);
            // 
            // itHethong
            // 
            this.itHethong.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itHethong.BackgroundImage")));
            this.itHethong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Hệ thống";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHethong.Elements.Add(tileItemElement4);
            this.itHethong.Id = 5;
            this.itHethong.Name = "itHethong";
            this.itHethong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHethong_ItemClick);
            // 
            // itThoat
            // 
            this.itThoat.BackgroundImage = global::KP_RES.Properties.Resources.dong2;
            this.itThoat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thoát";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itThoat.Elements.Add(tileItemElement5);
            this.itThoat.Id = 0;
            this.itThoat.Name = "itThoat";
            this.itThoat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThoat_ItemClick);
            // 
            // itThunho
            // 
            this.itThunho.BackgroundImage = global::KP_RES.Properties.Resources.thunho;
            this.itThunho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Thu nhỏ";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itThunho.Elements.Add(tileItemElement6);
            this.itThunho.Id = 1;
            this.itThunho.Name = "itThunho";
            this.itThunho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThunho_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itCaidat);
            this.tileGroup3.Items.Add(this.itBaocao);
            this.tileGroup3.Items.Add(this.itNhapxuatton);
            this.tileGroup3.Items.Add(this.itCongno);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itCaidat
            // 
            this.itCaidat.BackgroundImage = global::KP_RES.Properties.Resources.caidat3;
            this.itCaidat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement7.Text = "Cài đặt";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itCaidat.Elements.Add(tileItemElement7);
            this.itCaidat.FrameAnimationInterval = 15000;
            tileItemFrame1.AnimateBackgroundImage = true;
            tileItemFrame1.AnimateImage = true;
            tileItemFrame1.AnimateText = true;
            tileItemFrame1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tileItemFrame1.BackgroundImage")));
            tileItemElement8.Text = "Cài đặt";
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame1.Elements.Add(tileItemElement8);
            tileItemFrame1.UseBackgroundImage = true;
            tileItemFrame1.UseImage = true;
            tileItemFrame1.UseText = true;
            tileItemFrame2.AnimateBackgroundImage = true;
            tileItemFrame2.AnimateImage = true;
            tileItemFrame2.AnimateText = true;
            tileItemFrame2.BackgroundImage = global::KP_RES.Properties.Resources.caidat3;
            tileItemElement9.Text = "Cài đặt";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame2.Elements.Add(tileItemElement9);
            tileItemFrame2.UseBackgroundImage = true;
            tileItemFrame2.UseImage = true;
            tileItemFrame2.UseText = true;
            this.itCaidat.Frames.Add(tileItemFrame1);
            this.itCaidat.Frames.Add(tileItemFrame2);
            this.itCaidat.Id = 6;
            this.itCaidat.IsLarge = true;
            this.itCaidat.Name = "itCaidat";
            this.itCaidat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCaidat_ItemClick);
            // 
            // itBaocao
            // 
            this.itBaocao.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            this.itBaocao.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement10.Text = "Báo cáo";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaocao.Elements.Add(tileItemElement10);
            this.itBaocao.FrameAnimationInterval = 25000;
            tileItemFrame3.AnimateBackgroundImage = true;
            tileItemFrame3.AnimateImage = true;
            tileItemFrame3.AnimateText = true;
            tileItemFrame3.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            tileItemElement11.Text = "Báo cáo";
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame3.Elements.Add(tileItemElement11);
            tileItemFrame3.UseBackgroundImage = true;
            tileItemFrame3.UseImage = true;
            tileItemFrame3.UseText = true;
            tileItemFrame4.AnimateBackgroundImage = true;
            tileItemFrame4.AnimateImage = true;
            tileItemFrame4.AnimateText = true;
            tileItemFrame4.BackgroundImage = global::KP_RES.Properties.Resources.baocao7;
            tileItemElement12.Text = "Báo cáo";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame4.Elements.Add(tileItemElement12);
            tileItemFrame4.UseBackgroundImage = true;
            tileItemFrame4.UseImage = true;
            tileItemFrame4.UseText = true;
            this.itBaocao.Frames.Add(tileItemFrame3);
            this.itBaocao.Frames.Add(tileItemFrame4);
            this.itBaocao.Id = 8;
            this.itBaocao.IsLarge = true;
            this.itBaocao.Name = "itBaocao";
            this.itBaocao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaocao_ItemClick);
            // 
            // itNhapxuatton
            // 
            this.itNhapxuatton.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho2;
            this.itNhapxuatton.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.Text = "Kho - nhập - xuất";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhapxuatton.Elements.Add(tileItemElement13);
            this.itNhapxuatton.FrameAnimationInterval = 20000;
            tileItemFrame5.AnimateBackgroundImage = true;
            tileItemFrame5.AnimateImage = true;
            tileItemFrame5.AnimateText = true;
            tileItemFrame5.BackgroundImage = global::KP_RES.Properties.Resources.nhapkho2;
            tileItemElement14.Text = "Kho - nhập - xuất";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame5.Elements.Add(tileItemElement14);
            tileItemFrame5.UseBackgroundImage = true;
            tileItemFrame5.UseImage = true;
            tileItemFrame5.UseText = true;
            tileItemFrame6.AnimateBackgroundImage = true;
            tileItemFrame6.AnimateImage = true;
            tileItemFrame6.AnimateText = true;
            tileItemFrame6.BackgroundImage = global::KP_RES.Properties.Resources.nhapxuatton;
            tileItemElement15.Text = "Kho - nhập - xuất";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame6.Elements.Add(tileItemElement15);
            tileItemFrame6.UseBackgroundImage = true;
            tileItemFrame6.UseImage = true;
            tileItemFrame6.UseText = true;
            this.itNhapxuatton.Frames.Add(tileItemFrame5);
            this.itNhapxuatton.Frames.Add(tileItemFrame6);
            this.itNhapxuatton.Id = 7;
            this.itNhapxuatton.IsLarge = true;
            this.itNhapxuatton.Name = "itNhapxuatton";
            this.itNhapxuatton.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhapxuatton_ItemClick);
            // 
            // itCongno
            // 
            this.itCongno.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itCongno.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement16.Text = "Công nợ - Thu chi";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement16.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongno.Elements.Add(tileItemElement16);
            this.itCongno.FrameAnimationInterval = 22000;
            tileItemFrame7.AnimateBackgroundImage = true;
            tileItemFrame7.AnimateImage = true;
            tileItemFrame7.AnimateText = true;
            tileItemFrame7.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            tileItemElement17.Text = "Công nợ - Thu chi";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement17.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame7.Elements.Add(tileItemElement17);
            tileItemFrame7.UseBackgroundImage = true;
            tileItemFrame7.UseImage = true;
            tileItemFrame7.UseText = true;
            tileItemFrame8.AnimateBackgroundImage = true;
            tileItemFrame8.AnimateImage = true;
            tileItemFrame8.AnimateText = true;
            tileItemFrame8.BackgroundImage = global::KP_RES.Properties.Resources.congno1;
            tileItemElement18.Text = "Công nợ - Thu chi";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement18.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame8.Elements.Add(tileItemElement18);
            tileItemFrame8.UseBackgroundImage = true;
            tileItemFrame8.UseImage = true;
            tileItemFrame8.UseText = true;
            this.itCongno.Frames.Add(tileItemFrame7);
            this.itCongno.Frames.Add(tileItemFrame8);
            this.itCongno.Id = 17;
            this.itCongno.IsLarge = true;
            this.itCongno.Name = "itCongno";
            this.itCongno.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongno_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itFastFood);
            this.tileGroup5.Items.Add(this.itNhahang);
            this.tileGroup5.Items.Add(this.itKaraok);
            this.tileGroup5.Items.Add(this.itMenu);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itFastFood
            // 
            this.itFastFood.BackgroundImage = global::KP_RES.Properties.Resources.banhang9;
            this.itFastFood.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement19.Text = "FastFood";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement19.TextLocation = new System.Drawing.Point(0, 0);
            this.itFastFood.Elements.Add(tileItemElement19);
            tileItemFrame9.AnimateBackgroundImage = true;
            tileItemFrame9.AnimateImage = true;
            tileItemFrame9.AnimateText = true;
            tileItemFrame9.BackgroundImage = global::KP_RES.Properties.Resources.banhang9;
            tileItemElement20.Text = "FastFood";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement20.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame9.Elements.Add(tileItemElement20);
            tileItemFrame9.UseBackgroundImage = true;
            tileItemFrame9.UseImage = true;
            tileItemFrame9.UseText = true;
            tileItemFrame10.AnimateBackgroundImage = true;
            tileItemFrame10.AnimateImage = true;
            tileItemFrame10.AnimateText = true;
            tileItemFrame10.BackgroundImage = global::KP_RES.Properties.Resources.fastfood;
            tileItemElement21.Text = "FastFood";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement21.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame10.Elements.Add(tileItemElement21);
            tileItemFrame10.UseBackgroundImage = true;
            tileItemFrame10.UseImage = true;
            tileItemFrame10.UseText = true;
            this.itFastFood.Frames.Add(tileItemFrame9);
            this.itFastFood.Frames.Add(tileItemFrame10);
            this.itFastFood.Id = 19;
            this.itFastFood.IsLarge = true;
            this.itFastFood.Name = "itFastFood";
            this.itFastFood.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itFastFood_ItemClick);
            // 
            // itNhahang
            // 
            this.itNhahang.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            this.itNhahang.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement22.Text = "Nhà hàng - Cà phê";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement22.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhahang.Elements.Add(tileItemElement22);
            this.itNhahang.FrameAnimationInterval = 10000;
            tileItemFrame11.AnimateBackgroundImage = true;
            tileItemFrame11.AnimateImage = true;
            tileItemFrame11.AnimateText = true;
            tileItemFrame11.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            tileItemElement23.Text = "Nhà hàng - Cà phê";
            tileItemElement23.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement23.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame11.Elements.Add(tileItemElement23);
            tileItemFrame11.UseBackgroundImage = true;
            tileItemFrame11.UseImage = true;
            tileItemFrame11.UseText = true;
            tileItemFrame12.AnimateBackgroundImage = true;
            tileItemFrame12.AnimateImage = true;
            tileItemFrame12.AnimateText = true;
            tileItemFrame12.BackgroundImage = global::KP_RES.Properties.Resources.nhahangcafe;
            tileItemElement24.Text = "Nhà hàng - Cà phê";
            tileItemElement24.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement24.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame12.Elements.Add(tileItemElement24);
            tileItemFrame12.UseBackgroundImage = true;
            tileItemFrame12.UseImage = true;
            tileItemFrame12.UseText = true;
            this.itNhahang.Frames.Add(tileItemFrame11);
            this.itNhahang.Frames.Add(tileItemFrame12);
            this.itNhahang.Id = 9;
            this.itNhahang.IsLarge = true;
            this.itNhahang.Name = "itNhahang";
            this.itNhahang.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhahang_ItemClick);
            // 
            // itKaraok
            // 
            this.itKaraok.BackgroundImage = global::KP_RES.Properties.Resources.karaokbida;
            this.itKaraok.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement25.Text = "Karaoke - Bida";
            tileItemElement25.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement25.TextLocation = new System.Drawing.Point(0, 0);
            this.itKaraok.Elements.Add(tileItemElement25);
            this.itKaraok.FrameAnimationInterval = 12000;
            tileItemFrame13.AnimateBackgroundImage = true;
            tileItemFrame13.AnimateImage = true;
            tileItemFrame13.AnimateText = true;
            tileItemFrame13.BackgroundImage = global::KP_RES.Properties.Resources.banhang3;
            tileItemElement26.Text = "Karaoke - Bida";
            tileItemElement26.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement26.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame13.Elements.Add(tileItemElement26);
            tileItemFrame13.UseBackgroundImage = true;
            tileItemFrame13.UseImage = true;
            tileItemFrame13.UseText = true;
            tileItemFrame14.AnimateBackgroundImage = true;
            tileItemFrame14.AnimateImage = true;
            tileItemFrame14.AnimateText = true;
            tileItemFrame14.BackgroundImage = global::KP_RES.Properties.Resources.karaokbida;
            tileItemElement27.Text = "Karaoke - Bida";
            tileItemElement27.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement27.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame14.Elements.Add(tileItemElement27);
            tileItemFrame14.UseBackgroundImage = true;
            tileItemFrame14.UseImage = true;
            tileItemFrame14.UseText = true;
            this.itKaraok.Frames.Add(tileItemFrame13);
            this.itKaraok.Frames.Add(tileItemFrame14);
            this.itKaraok.Id = 10;
            this.itKaraok.IsLarge = true;
            this.itKaraok.Name = "itKaraok";
            this.itKaraok.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKaraok_ItemClick);
            // 
            // itMenu
            // 
            this.itMenu.BackgroundImage = global::KP_RES.Properties.Resources.loaighe;
            this.itMenu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement28.Text = "Menu Display";
            tileItemElement28.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement28.TextLocation = new System.Drawing.Point(0, 0);
            this.itMenu.Elements.Add(tileItemElement28);
            this.itMenu.Id = 20;
            this.itMenu.IsLarge = true;
            this.itMenu.Name = "itMenu";
            this.itMenu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itMenu_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itKhachsan);
            this.tileGroup9.Items.Add(this.itKhuvuichoi);
            this.tileGroup9.Items.Add(this.itVerapphim);
            this.tileGroup9.Items.Add(this.itShowTime);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itKhachsan
            // 
            this.itKhachsan.BackgroundImage = global::KP_RES.Properties.Resources.khachsan;
            this.itKhachsan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement29.Text = "Khách sạn";
            tileItemElement29.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement29.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhachsan.Elements.Add(tileItemElement29);
            this.itKhachsan.FrameAnimationInterval = 16000;
            tileItemFrame15.AnimateBackgroundImage = true;
            tileItemFrame15.AnimateImage = true;
            tileItemFrame15.AnimateText = true;
            tileItemFrame15.BackgroundImage = global::KP_RES.Properties.Resources.banhang5;
            tileItemElement30.Text = "Khách sạn";
            tileItemElement30.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement30.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame15.Elements.Add(tileItemElement30);
            tileItemFrame15.UseBackgroundImage = true;
            tileItemFrame15.UseImage = true;
            tileItemFrame15.UseText = true;
            tileItemFrame16.AnimateBackgroundImage = true;
            tileItemFrame16.AnimateImage = true;
            tileItemFrame16.AnimateText = true;
            tileItemFrame16.BackgroundImage = global::KP_RES.Properties.Resources.khachsan;
            tileItemElement31.Text = "Khách sạn";
            tileItemElement31.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement31.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame16.Elements.Add(tileItemElement31);
            tileItemFrame16.UseBackgroundImage = true;
            tileItemFrame16.UseImage = true;
            tileItemFrame16.UseText = true;
            this.itKhachsan.Frames.Add(tileItemFrame15);
            this.itKhachsan.Frames.Add(tileItemFrame16);
            this.itKhachsan.Id = 14;
            this.itKhachsan.IsLarge = true;
            this.itKhachsan.Name = "itKhachsan";
            this.itKhachsan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhachsan_ItemClick);
            // 
            // itKhuvuichoi
            // 
            this.itKhuvuichoi.BackgroundImage = global::KP_RES.Properties.Resources.khuvuichoi;
            this.itKhuvuichoi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement32.Text = "Khu vui chơi";
            tileItemElement32.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement32.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhuvuichoi.Elements.Add(tileItemElement32);
            this.itKhuvuichoi.FrameAnimationInterval = 18000;
            tileItemFrame17.AnimateBackgroundImage = true;
            tileItemFrame17.AnimateImage = true;
            tileItemFrame17.AnimateText = true;
            tileItemFrame17.BackgroundImage = global::KP_RES.Properties.Resources.banhang6;
            tileItemElement33.Text = "Khu vui chơi";
            tileItemElement33.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement33.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame17.Elements.Add(tileItemElement33);
            tileItemFrame17.UseBackgroundImage = true;
            tileItemFrame17.UseImage = true;
            tileItemFrame17.UseText = true;
            tileItemFrame18.AnimateBackgroundImage = true;
            tileItemFrame18.AnimateImage = true;
            tileItemFrame18.AnimateText = true;
            tileItemFrame18.BackgroundImage = global::KP_RES.Properties.Resources.khuvuichoi;
            tileItemElement34.Text = "Khu vui chơi";
            tileItemElement34.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement34.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame18.Elements.Add(tileItemElement34);
            tileItemFrame18.UseBackgroundImage = true;
            tileItemFrame18.UseImage = true;
            tileItemFrame18.UseText = true;
            this.itKhuvuichoi.Frames.Add(tileItemFrame17);
            this.itKhuvuichoi.Frames.Add(tileItemFrame18);
            this.itKhuvuichoi.Id = 15;
            this.itKhuvuichoi.IsLarge = true;
            this.itKhuvuichoi.Name = "itKhuvuichoi";
            this.itKhuvuichoi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhuvuichoi_ItemClick);
            // 
            // itVerapphim
            // 
            this.itVerapphim.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            this.itVerapphim.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement35.Text = "Rạp phim";
            tileItemElement35.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement35.TextLocation = new System.Drawing.Point(0, 0);
            this.itVerapphim.Elements.Add(tileItemElement35);
            this.itVerapphim.FrameAnimationInterval = 20000;
            tileItemFrame19.AnimateBackgroundImage = true;
            tileItemFrame19.AnimateImage = true;
            tileItemFrame19.AnimateText = true;
            tileItemFrame19.BackgroundImage = global::KP_RES.Properties.Resources.banhang8;
            tileItemElement36.Text = "Rạp phim";
            tileItemElement36.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement36.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame19.Elements.Add(tileItemElement36);
            tileItemFrame19.UseBackgroundImage = true;
            tileItemFrame19.UseImage = true;
            tileItemFrame19.UseText = true;
            tileItemFrame20.AnimateBackgroundImage = true;
            tileItemFrame20.AnimateImage = true;
            tileItemFrame20.AnimateText = true;
            tileItemFrame20.BackgroundImage = global::KP_RES.Properties.Resources.raphim3;
            tileItemElement37.Text = "Rạp phim";
            tileItemElement37.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement37.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame20.Elements.Add(tileItemElement37);
            tileItemFrame20.UseBackgroundImage = true;
            tileItemFrame20.UseImage = true;
            tileItemFrame20.UseText = true;
            this.itVerapphim.Frames.Add(tileItemFrame19);
            this.itVerapphim.Frames.Add(tileItemFrame20);
            this.itVerapphim.Id = 16;
            this.itVerapphim.IsLarge = true;
            this.itVerapphim.Name = "itVerapphim";
            this.itVerapphim.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itVerapphim_ItemClick);
            // 
            // itShowTime
            // 
            this.itShowTime.BackgroundImage = global::KP_RES.Properties.Resources.thoigian;
            this.itShowTime.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement38.Text = "ShowTime";
            tileItemElement38.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement38.TextLocation = new System.Drawing.Point(0, 0);
            this.itShowTime.Elements.Add(tileItemElement38);
            this.itShowTime.Id = 21;
            this.itShowTime.IsLarge = true;
            this.itShowTime.Name = "itShowTime";
            this.itShowTime.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itShowTime_ItemClick);
            // 
            // tileGroup11
            // 
            this.tileGroup11.Items.Add(this.itManhinhbep);
            this.tileGroup11.Items.Add(this.itKHTT);
            this.tileGroup11.Items.Add(this.itCongvan);
            this.tileGroup11.Items.Add(this.itNhansu);
            this.tileGroup11.Name = "tileGroup11";
            // 
            // itManhinhbep
            // 
            this.itManhinhbep.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep1;
            this.itManhinhbep.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement39.Text = "Màn hình bếp";
            tileItemElement39.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement39.TextLocation = new System.Drawing.Point(0, 0);
            this.itManhinhbep.Elements.Add(tileItemElement39);
            this.itManhinhbep.FrameAnimationInterval = 14000;
            tileItemFrame21.AnimateBackgroundImage = true;
            tileItemFrame21.AnimateImage = true;
            tileItemFrame21.AnimateText = true;
            tileItemFrame21.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep;
            tileItemElement40.Text = "Màn hình bếp";
            tileItemElement40.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement40.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame21.Elements.Add(tileItemElement40);
            tileItemFrame21.UseBackgroundImage = true;
            tileItemFrame21.UseImage = true;
            tileItemFrame21.UseText = true;
            tileItemFrame22.AnimateBackgroundImage = true;
            tileItemFrame22.AnimateImage = true;
            tileItemFrame22.AnimateText = true;
            tileItemFrame22.BackgroundImage = global::KP_RES.Properties.Resources.manhinhbep1;
            tileItemElement41.Text = "Màn hình bếp";
            tileItemElement41.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement41.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame22.Elements.Add(tileItemElement41);
            tileItemFrame22.UseBackgroundImage = true;
            tileItemFrame22.UseImage = true;
            tileItemFrame22.UseText = true;
            this.itManhinhbep.Frames.Add(tileItemFrame21);
            this.itManhinhbep.Frames.Add(tileItemFrame22);
            this.itManhinhbep.Id = 11;
            this.itManhinhbep.IsLarge = true;
            this.itManhinhbep.Name = "itManhinhbep";
            this.itManhinhbep.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itManhinhbep_ItemClick);
            // 
            // itKHTT
            // 
            this.itKHTT.BackgroundImage = global::KP_RES.Properties.Resources.khtt;
            this.itKHTT.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement42.Text = "Khách hàng thân thiết";
            tileItemElement42.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement42.TextLocation = new System.Drawing.Point(0, 0);
            this.itKHTT.Elements.Add(tileItemElement42);
            this.itKHTT.FrameAnimationInterval = 14000;
            tileItemFrame23.AnimateBackgroundImage = true;
            tileItemFrame23.AnimateImage = true;
            tileItemFrame23.AnimateText = true;
            tileItemFrame23.BackgroundImage = global::KP_RES.Properties.Resources.nhomnguoidung;
            tileItemElement43.Text = "Khách hàng thân thiết";
            tileItemElement43.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement43.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame23.Elements.Add(tileItemElement43);
            tileItemFrame23.UseBackgroundImage = true;
            tileItemFrame23.UseImage = true;
            tileItemFrame23.UseText = true;
            tileItemFrame24.AnimateBackgroundImage = true;
            tileItemFrame24.AnimateImage = true;
            tileItemFrame24.AnimateText = true;
            tileItemFrame24.BackgroundImage = global::KP_RES.Properties.Resources.khtt;
            tileItemElement44.Text = "Khách hàng thân thiết";
            tileItemElement44.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement44.TextLocation = new System.Drawing.Point(0, 0);
            tileItemFrame24.Elements.Add(tileItemElement44);
            tileItemFrame24.UseBackgroundImage = true;
            tileItemFrame24.UseImage = true;
            tileItemFrame24.UseText = true;
            this.itKHTT.Frames.Add(tileItemFrame23);
            this.itKHTT.Frames.Add(tileItemFrame24);
            this.itKHTT.Id = 22;
            this.itKHTT.IsLarge = true;
            this.itKHTT.Name = "itKHTT";
            this.itKHTT.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKHTT_ItemClick);
            // 
            // itCongvan
            // 
            this.itCongvan.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itCongvan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itCongvan.Checked = true;
            tileItemElement45.Text = "Công văn";
            tileItemElement45.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement45.TextLocation = new System.Drawing.Point(0, 0);
            this.itCongvan.Elements.Add(tileItemElement45);
            this.itCongvan.Id = 23;
            this.itCongvan.IsLarge = true;
            this.itCongvan.Name = "itCongvan";
            this.itCongvan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCongvan_ItemClick);
            // 
            // itNhansu
            // 
            this.itNhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement46.Text = "Nhân sự";
            tileItemElement46.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement46.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhansu.Elements.Add(tileItemElement46);
            this.itNhansu.Id = 24;
            this.itNhansu.IsLarge = true;
            this.itNhansu.Name = "itNhansu";
            this.itNhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // tileGroup13
            // 
            this.tileGroup13.Items.Add(this.itDanhgiaKH);
            this.tileGroup13.Items.Add(this.itTheodoihopdong);
            this.tileGroup13.Items.Add(this.itSieuthi);
            this.tileGroup13.Items.Add(this.itTrasua);
            this.tileGroup13.Name = "tileGroup13";
            // 
            // itDanhgiaKH
            // 
            this.itDanhgiaKH.BackgroundImage = global::KP_RES.Properties.Resources.kichhoat;
            this.itDanhgiaKH.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement47.Text = "Đánh giá khách hàng";
            tileItemElement47.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement47.TextLocation = new System.Drawing.Point(0, 0);
            this.itDanhgiaKH.Elements.Add(tileItemElement47);
            this.itDanhgiaKH.Id = 25;
            this.itDanhgiaKH.IsLarge = true;
            this.itDanhgiaKH.Name = "itDanhgiaKH";
            this.itDanhgiaKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDanhgiaKH_ItemClick);
            // 
            // itTheodoihopdong
            // 
            this.itTheodoihopdong.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itTheodoihopdong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement48.Text = "Theo dõi hợp đồng";
            tileItemElement48.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement48.TextLocation = new System.Drawing.Point(0, 0);
            this.itTheodoihopdong.Elements.Add(tileItemElement48);
            this.itTheodoihopdong.Id = 26;
            this.itTheodoihopdong.IsLarge = true;
            this.itTheodoihopdong.Name = "itTheodoihopdong";
            this.itTheodoihopdong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTheodoihopdong_ItemClick);
            // 
            // itSieuthi
            // 
            this.itSieuthi.BackgroundImage = global::KP_RES.Properties.Resources.banhang3;
            this.itSieuthi.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement49.Text = "Siêu thị";
            tileItemElement49.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement49.TextLocation = new System.Drawing.Point(0, 0);
            this.itSieuthi.Elements.Add(tileItemElement49);
            this.itSieuthi.Id = 27;
            this.itSieuthi.IsLarge = true;
            this.itSieuthi.Name = "itSieuthi";
            this.itSieuthi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itSieuthi_ItemClick);
            // 
            // itTrasua
            // 
            this.itTrasua.BackgroundImage = global::KP_RES.Properties.Resources.banhang6;
            this.itTrasua.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement50.Text = "Trà sữa";
            tileItemElement50.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement50.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrasua.Elements.Add(tileItemElement50);
            this.itTrasua.Id = 29;
            this.itTrasua.IsLarge = true;
            this.itTrasua.Name = "itTrasua";
            this.itTrasua.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrasua_ItemClick);
            // 
            // tileGroup14
            // 
            this.tileGroup14.Items.Add(this.itTrasua_Ban);
            this.tileGroup14.Items.Add(this.itBuffet);
            this.tileGroup14.Name = "tileGroup14";
            // 
            // itTrasua_Ban
            // 
            this.itTrasua_Ban.BackgroundImage = global::KP_RES.Properties.Resources.banhang;
            this.itTrasua_Ban.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement51.Text = "Trà sữa theo sơ đồ bàn";
            tileItemElement51.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement51.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrasua_Ban.Elements.Add(tileItemElement51);
            this.itTrasua_Ban.Id = 30;
            this.itTrasua_Ban.IsLarge = true;
            this.itTrasua_Ban.Name = "itTrasua_Ban";
            this.itTrasua_Ban.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrasua_Ban_ItemClick);
            // 
            // itBuffet
            // 
            this.itBuffet.BackgroundImage = global::KP_RES.Properties.Resources.banhang5;
            this.itBuffet.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement52.Text = "Nhà hàng - Buffet";
            tileItemElement52.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement52.TextLocation = new System.Drawing.Point(0, 0);
            this.itBuffet.Elements.Add(tileItemElement52);
            this.itBuffet.Id = 31;
            this.itBuffet.IsLarge = true;
            this.itBuffet.Name = "itBuffet";
            this.itBuffet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBuffet_ItemClick);
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // Frm_Main
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.ControlBox = false;
            this.Controls.Add(this.tileControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KingPos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Main_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itThoat;
        private DevExpress.XtraEditors.TileItem itThunho;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private System.Windows.Forms.Timer timerGiaiphongbonho;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileItem itHethong;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itCauhinh;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TileItem itNhapxuatton;
        private DevExpress.XtraEditors.TileItem itDangxuat;
        private DevExpress.XtraEditors.TileItem itTrogiup;
        private DevExpress.XtraEditors.TileItem itCaidat;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itKhuvuichoi;
        private DevExpress.XtraEditors.TileItem itVerapphim;
        private DevExpress.XtraEditors.TileItem itBaocao;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itKaraok;
        private DevExpress.XtraEditors.TileItem itCongno;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileItem itFastFood;
        private DevExpress.XtraEditors.TileItem itNhahang;
        private DevExpress.XtraEditors.TileItem itShowTime;
        private DevExpress.XtraEditors.TileGroup tileGroup11;
        private DevExpress.XtraEditors.TileItem itKHTT;
        private DevExpress.XtraEditors.TileItem itKhachsan;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileItem itCongvan;
        private DevExpress.XtraEditors.TileItem itNhansu;
        private DevExpress.XtraEditors.TileGroup tileGroup13;
        private DevExpress.XtraEditors.TileItem itDanhgiaKH;
        private DevExpress.XtraEditors.TileItem itMenu;
        private DevExpress.XtraEditors.TileItem itManhinhbep;
        private DevExpress.XtraEditors.TileItem itTheodoihopdong;
        private DevExpress.XtraEditors.TileItem itSieuthi;
        private DevExpress.XtraEditors.TileItem itTrasua;
        private DevExpress.XtraEditors.TileGroup tileGroup14;
        private DevExpress.XtraEditors.TileItem itTrasua_Ban;
        private DevExpress.XtraEditors.TileItem itBuffet;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}