﻿namespace KP_RES 
{
    partial class Frm_Main1 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main1));
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.timerGiaiphongbonho = new System.Windows.Forms.Timer(this.components);
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tileGroup8 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup10 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup12 = new DevExpress.XtraEditors.TileGroup();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itDangxuat = new DevExpress.XtraEditors.TileItem();
            this.itTrogiup = new DevExpress.XtraEditors.TileItem();
            this.itCauhinh = new DevExpress.XtraEditors.TileItem();
            this.itHethong = new DevExpress.XtraEditors.TileItem();
            this.itThoat = new DevExpress.XtraEditors.TileItem();
            this.itThunho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup7 = new DevExpress.XtraEditors.TileGroup();
            this.itGiamdoc = new DevExpress.XtraEditors.TileItem();
            this.itHanhchinh = new DevExpress.XtraEditors.TileItem();
            this.itNhansu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup9 = new DevExpress.XtraEditors.TileGroup();
            this.itTaichinh = new DevExpress.XtraEditors.TileItem();
            this.itKinhdoanh = new DevExpress.XtraEditors.TileItem();
            this.itKetoan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongve = new DevExpress.XtraEditors.TileItem();
            this.itKho = new DevExpress.XtraEditors.TileItem();
            this.itThuquy = new DevExpress.XtraEditors.TileItem();
            this.SuspendLayout();
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // tileGroup4
            // 
            this.tileGroup4.Name = "tileGroup4";
            // 
            // timerGiaiphongbonho
            // 
            this.timerGiaiphongbonho.Enabled = true;
            this.timerGiaiphongbonho.Interval = 120000;
            this.timerGiaiphongbonho.Tick += new System.EventHandler(this.timerGiaiphongbonho_Tick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Name = "tileGroup6";
            // 
            // tileGroup8
            // 
            this.tileGroup8.Name = "tileGroup8";
            // 
            // tileGroup10
            // 
            this.tileGroup10.Name = "tileGroup10";
            // 
            // tileGroup12
            // 
            this.tileGroup12.Name = "tileGroup12";
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_1366x768_5;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup7);
            this.tileControl1.Groups.Add(this.tileGroup9);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(0, 0);
            this.tileControl1.MaxId = 37;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RowCount = 4;
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(984, 562);
            this.tileControl1.TabIndex = 3;
            this.tileControl1.Text = "Trang chủ";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itDangxuat);
            this.tileGroup2.Items.Add(this.itTrogiup);
            this.tileGroup2.Items.Add(this.itCauhinh);
            this.tileGroup2.Items.Add(this.itHethong);
            this.tileGroup2.Items.Add(this.itThoat);
            this.tileGroup2.Items.Add(this.itThunho);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itDangxuat
            // 
            this.itDangxuat.BackgroundImage = global::KP_RES.Properties.Resources.dangxuat3;
            this.itDangxuat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Đăng xuất";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDangxuat.Elements.Add(tileItemElement1);
            this.itDangxuat.Id = 2;
            this.itDangxuat.Name = "itDangxuat";
            this.itDangxuat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDangxuat_ItemClick);
            // 
            // itTrogiup
            // 
            this.itTrogiup.BackgroundImage = global::KP_RES.Properties.Resources.huongdan;
            this.itTrogiup.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement2.Text = "Trợ giúp";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itTrogiup.Elements.Add(tileItemElement2);
            this.itTrogiup.Id = 3;
            this.itTrogiup.Name = "itTrogiup";
            this.itTrogiup.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itTrogiup_ItemClick);
            // 
            // itCauhinh
            // 
            this.itCauhinh.BackgroundImage = global::KP_RES.Properties.Resources.cauhinh;
            this.itCauhinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement3.Text = "Cấu hình";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itCauhinh.Elements.Add(tileItemElement3);
            this.itCauhinh.Id = 4;
            this.itCauhinh.Name = "itCauhinh";
            this.itCauhinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itCauhinh_ItemClick);
            // 
            // itHethong
            // 
            this.itHethong.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("itHethong.BackgroundImage")));
            this.itHethong.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement4.Text = "Hệ thống";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itHethong.Elements.Add(tileItemElement4);
            this.itHethong.Id = 5;
            this.itHethong.Name = "itHethong";
            this.itHethong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHethong_ItemClick);
            // 
            // itThoat
            // 
            this.itThoat.BackgroundImage = global::KP_RES.Properties.Resources.dong2;
            this.itThoat.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Thoát";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itThoat.Elements.Add(tileItemElement5);
            this.itThoat.Id = 0;
            this.itThoat.Name = "itThoat";
            this.itThoat.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThoat_ItemClick);
            // 
            // itThunho
            // 
            this.itThunho.BackgroundImage = global::KP_RES.Properties.Resources.thunho;
            this.itThunho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement6.Text = "Thu nhỏ";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itThunho.Elements.Add(tileItemElement6);
            this.itThunho.Id = 1;
            this.itThunho.Name = "itThunho";
            this.itThunho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThunho_ItemClick);
            // 
            // tileGroup7
            // 
            this.tileGroup7.Items.Add(this.itGiamdoc);
            this.tileGroup7.Items.Add(this.itHanhchinh);
            this.tileGroup7.Items.Add(this.itNhansu);
            this.tileGroup7.Name = "tileGroup7";
            // 
            // itGiamdoc
            // 
            this.itGiamdoc.BackgroundImage = global::KP_RES.Properties.Resources.baocao3;
            this.itGiamdoc.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itGiamdoc.Checked = true;
            tileItemElement7.Text = "Giám đốc";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiamdoc.Elements.Add(tileItemElement7);
            this.itGiamdoc.Id = 28;
            this.itGiamdoc.IsLarge = true;
            this.itGiamdoc.Name = "itGiamdoc";
            this.itGiamdoc.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiamdoc_ItemClick);
            // 
            // itHanhchinh
            // 
            this.itHanhchinh.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itHanhchinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itHanhchinh.Checked = true;
            tileItemElement8.Text = "Hành chính";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itHanhchinh.Elements.Add(tileItemElement8);
            this.itHanhchinh.Id = 29;
            this.itHanhchinh.IsLarge = true;
            this.itHanhchinh.Name = "itHanhchinh";
            this.itHanhchinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHanhchinh_ItemClick);
            // 
            // itNhansu
            // 
            this.itNhansu.BackgroundImage = global::KP_RES.Properties.Resources.nhansu;
            this.itNhansu.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itNhansu.Checked = true;
            tileItemElement9.Text = "Nhân sự";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itNhansu.Elements.Add(tileItemElement9);
            this.itNhansu.Id = 30;
            this.itNhansu.IsLarge = true;
            this.itNhansu.Name = "itNhansu";
            this.itNhansu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itNhansu_ItemClick);
            // 
            // tileGroup9
            // 
            this.tileGroup9.Items.Add(this.itTaichinh);
            this.tileGroup9.Items.Add(this.itKinhdoanh);
            this.tileGroup9.Items.Add(this.itKetoan);
            this.tileGroup9.Name = "tileGroup9";
            // 
            // itTaichinh
            // 
            this.itTaichinh.BackgroundImage = global::KP_RES.Properties.Resources.Caban;
            this.itTaichinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itTaichinh.Checked = true;
            tileItemElement10.Text = "Tài chính";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itTaichinh.Elements.Add(tileItemElement10);
            this.itTaichinh.Id = 32;
            this.itTaichinh.IsLarge = true;
            this.itTaichinh.Name = "itTaichinh";
            // 
            // itKinhdoanh
            // 
            this.itKinhdoanh.BackgroundImage = global::KP_RES.Properties.Resources.kinhdoanh;
            this.itKinhdoanh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itKinhdoanh.Checked = true;
            tileItemElement11.Text = "Kinh doanh";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itKinhdoanh.Elements.Add(tileItemElement11);
            this.itKinhdoanh.Id = 31;
            this.itKinhdoanh.IsLarge = true;
            this.itKinhdoanh.Name = "itKinhdoanh";
            // 
            // itKetoan
            // 
            this.itKetoan.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itKetoan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itKetoan.Checked = true;
            tileItemElement12.Text = "Kế toán";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itKetoan.Elements.Add(tileItemElement12);
            this.itKetoan.Id = 33;
            this.itKetoan.IsLarge = true;
            this.itKetoan.Name = "itKetoan";
            this.itKetoan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKetoan_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itPhongve);
            this.tileGroup5.Items.Add(this.itKho);
            this.tileGroup5.Items.Add(this.itThuquy);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itPhongve
            // 
            this.itPhongve.BackgroundImage = global::KP_RES.Properties.Resources.phongve;
            this.itPhongve.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itPhongve.Checked = true;
            tileItemElement13.Text = "Phòng vé";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongve.Elements.Add(tileItemElement13);
            this.itPhongve.Id = 34;
            this.itPhongve.IsLarge = true;
            this.itPhongve.Name = "itPhongve";
            // 
            // itKho
            // 
            this.itKho.BackgroundImage = global::KP_RES.Properties.Resources.hanghoa;
            this.itKho.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itKho.Checked = true;
            tileItemElement14.Text = "Kho";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itKho.Elements.Add(tileItemElement14);
            this.itKho.Id = 35;
            this.itKho.IsLarge = true;
            this.itKho.Name = "itKho";
            this.itKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKho_ItemClick);
            // 
            // itThuquy
            // 
            this.itThuquy.BackgroundImage = global::KP_RES.Properties.Resources.tientetigia1;
            this.itThuquy.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            this.itThuquy.Checked = true;
            tileItemElement15.Text = "Thủ quỷ";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itThuquy.Elements.Add(tileItemElement15);
            this.itThuquy.Id = 36;
            this.itThuquy.IsLarge = true;
            this.itThuquy.Name = "itThuquy";
            // 
            // Frm_Main1
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.ControlBox = false;
            this.Controls.Add(this.tileControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Main1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KingPos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main1_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Main1_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private System.Windows.Forms.Timer timerGiaiphongbonho;
        private DevExpress.XtraEditors.TileGroup tileGroup8;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TileGroup tileGroup10;
        private DevExpress.XtraEditors.TileGroup tileGroup12;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itDangxuat;
        private DevExpress.XtraEditors.TileItem itTrogiup;
        private DevExpress.XtraEditors.TileItem itCauhinh;
        private DevExpress.XtraEditors.TileItem itHethong;
        private DevExpress.XtraEditors.TileItem itThoat;
        private DevExpress.XtraEditors.TileItem itThunho;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup7;
        private DevExpress.XtraEditors.TileItem itGiamdoc;
        private DevExpress.XtraEditors.TileItem itHanhchinh;
        private DevExpress.XtraEditors.TileItem itNhansu;
        private DevExpress.XtraEditors.TileGroup tileGroup9;
        private DevExpress.XtraEditors.TileItem itKinhdoanh;
        private DevExpress.XtraEditors.TileItem itKetoan;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itPhongve;
        private DevExpress.XtraEditors.TileItem itKho;
        private DevExpress.XtraEditors.TileItem itThuquy;
        private DevExpress.XtraEditors.TileItem itTaichinh;
    }
}