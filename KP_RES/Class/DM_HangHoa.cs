﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;

namespace KP_RES
{
    public class convert
    {
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
    class DM_HangHoa
    {
        public string @TenNhanVien { get; set; } 
        public string @GhiChu { get; set; }

        public string @MA_VACH { get; set; } 
        public string @MA_HANGHOA { get; set; } 
        public string @TEN_HANGHOA { get; set; }
        public decimal @SOLUONG { get; set; }
        public decimal @GIABAN { get; set; }        
        public decimal @Thanh_Tien { get; set; }
        public string @GHICHU { get; set; }
        public string @STT { get; set; }
        public string @MA_BEP { get; set; }
		public string @TEN_BAN { get; set; }
        public bool @TRANGTHAI { get; set; }
        public bool @IS_COMBO { get; set; }
        public bool @IS_INBEP { get; set; }
        public bool @IS_KHUYENMAI { get; set; }
        public decimal @CHIETKHAU { get; set; }
        public bool @MONTHEM { get; set; }
        public int @TEM { get; set; }
        public string @Number { get; set; }
        public bool @INTEM { get; set; }
        public string @MACP { get; set; }
        public int @MACB { get; set; }
        public string @COMBO_CHINH { get; set; }
        public decimal @VOUCHER { get; set; }
        public decimal @SOLUONG_COMBO { get; set; }
        public string @GHICHU_COMBO { get; set; }
        public int @DA_DUYET { get; set; }
        //Karaoke
        public string @TienGio { get; set; }
        public DateTime @Giovao { get; set; }
        public DateTime @Giora { get; set; }

        //Khachsan
        public DateTime @CheckIn { get; set; }
        public DateTime @CheckOut { get; set; }

        //print bill 
        public string @Ten_HoaDon { get; set; }
        public string @Giam_Gia { get; set; }
        public string @Tong_Cong { get; set; }
        public string @Tien_KhachTra { get; set; }
        public string @Tien_TraKhach { get; set; }
        public string @Phiphucvu { get; set; }
        public string @Tiendatcoc { get; set; }
        public decimal @Tong_Cong_Tru_Voucher { get; set; }

        public string @DIACHIGIAOHANG { get; set; }
        public string @TENKH { get; set; }
        public string @DIENTHOAI_KH { get; set; } 
        

        //infor of Office Table
        public string @Ma_Quay { get; set; }
        public string @Ma_HoaDon { get; set;}        
        public string @TenNV { get; set; }
        public string @ngayHD { get; set; } 

        // print Bill Tables check 
        public string @TenKhachHang { get; set; }
        public string @DienThoai { get; set; }
        public string @MaBan { get; set; }
        public string @GIOVAO { get; set; } 
        public string @KhuVuc{ get; set; }
        public decimal @TienDatCoc { get; set; }

        //Order 
        public string @MaQuay { get; set; }
        public string @NhanVien { get; set; } 
        public string @Ngay { get; set; }
        public string @SOHOADON { get; set; }
        public double @THUE { get; set; }
        

    }

    class DM_ListDisheTableOne
    {
        public string @_MA_HANGHOA { get; set; }
        public string @_TEN_HANGHOA { get; set; }
        public decimal @_SOLUONG { get; set; }
        public decimal @_GIABAN { get; set; }
        public decimal @_MA_BEP { get; set; }
        public decimal @_Thanh_Tien { get; set; }
        public string @_GHICHU { get; set; }
        public bool @_IS_COMBO { get; set; }
        public bool @_IS_INBEP { get; set; }
        public bool @_MONTHEM { get; set; }
        public int @_TEM { get; set; }
        public string @_MACP { get; set; }
        public int @_MACB { get; set; }
        public bool @_INTEM { get; set; }
        public string @_COMBO_CHINH { get; set; }
        public double @_THUE { get; set; }
        public decimal @_CHIETKHAU { get; set; }
        public bool @_TRANGTHAI { get; set; }
        public bool @_IS_XULY { get; set; }
        public string @_GHICHU_COMBO { get; set; }
    }
    class DM_ListDisheTableTwo
    {
        public string @__Ma_Quay { get; set; }
        public string @__Ma_HoaDon { get; set; }
        public string @__TenNV { get; set; }
        public string @__ngayHD { get; set; }
        public string @__Ten_HoaDon { get; set; }
        public string @__STT { get; set; } 


        public string @__MA_HANGHOA { get; set; }
        public string @__TEN_HANGHOA { get; set; }
        public decimal @__SOLUONG { get; set; }
        public decimal @__GIABAN { get; set; }
        public decimal @__MA_BEP { get; set; }
        public decimal @__Thanh_Tien { get; set; }
        public string @__GHICHU { get; set; }
        public bool @__IS_COMBO { get; set; }
        public bool @__IS_INBEP { get; set; }
        public bool @__MONTHEM { get; set; }
        public int @__TEM { get; set; }
        public string @__MACP { get; set; }
        public int @__MACB { get; set; }
        public bool @__INTEM { get; set; }
        public string @__COMBO_CHINH { get; set; }
        public double @__THUE { get; set; }
        public decimal @__CHIETKHAU { get; set; }
        public bool @__TRANGTHAI { get; set; }
        public bool @__IS_XULY { get; set; }
        public string @__GHICHU_COMBO { get; set; }
    }
}
