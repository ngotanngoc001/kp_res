﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace KP_RES.Class
{
    public class cls_Import
    {
        public DataTable loaddulieu(string path, string COMMAND)
        {
            DataTable DT = new DataTable();
            OleDbConnection con;
            try
            {
                DT = new DataTable();
                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 8.0;Data Source=" + path;
                con = new OleDbConnection(connectionString);
                con.Open();
                OleDbCommand cmd = new OleDbCommand(COMMAND, con);
                OleDbDataAdapter dataadapter = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                dataadapter.Fill(ds);
                DT = ds.Tables[0];
                //DT.Rows.Remove(DT.Rows[0]);
            }
            catch (Exception Ex) 
            {
                DT = new DataTable();
                string connectionString = "Provider=Microsoft.JET.OLEDB.4.0;Extended Properties=Excel 8.0;Data Source=" + path;
                con = new OleDbConnection(connectionString);
                con.Open();
                OleDbCommand cmd = new OleDbCommand(COMMAND, con);
                OleDbDataAdapter dataadapter = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                dataadapter.Fill(ds);
                DT = ds.Tables[0];
                //DT.Rows.Remove(DT.Rows[0]);
            }
            con.Close();
            return DT;
        }
    }
}
