﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.Windows.Forms;
using System.Data.SqlClient;
using KP_UserManagement;
using System.IO;
using System.Data;
using System.Collections;
using System.Drawing;

namespace KP_RES
{
    class clsQLCongTy
    {
        public static String CheckedComboBox(CheckedComboBoxEdit sCbo)
        {
            String sStr = "";
            try
            {
                foreach (CheckedListBoxItem sItem in sCbo.Properties.Items)
                {
                    if (sItem.CheckState == CheckState.Checked)
                    {
                        if (sStr != "")
                            sStr += ",";

                        sStr += sItem.Value.ToString();
                    }
                }
            }
            catch (Exception)
            {
            }

            return sStr;
        }

        public static void CheckedComboBox(CheckedComboBoxEdit sCbo, String[] sStr)
        {
            foreach (CheckedListBoxItem sItem in sCbo.Properties.Items)
            {
                sItem.CheckState = CheckState.Unchecked;
                foreach (var sArray in sStr)
                {
                    if (sItem.Value.ToString() == sArray.ToString())
                        sItem.CheckState = CheckState.Checked;
                }
            }
        }

        public static void UnCheckedComboBox(CheckedComboBoxEdit sCbo)
        {
            foreach (CheckedListBoxItem sItem in sCbo.Properties.Items)
                sItem.CheckState = CheckState.Unchecked;
        }

        public static Boolean ExecuteParameter(String sSQL, SqlParameter[] sSQLParameter)
        {
            Boolean sBoolean = true;
            try
            {
                using (SqlConnection sConn = new SqlConnection(clsGlobal.gsConnectionString))
                {
                    using (SqlCommand sSQLCom = new SqlCommand(sSQL, sConn))
                    {
                        foreach (SqlParameter Parameter in sSQLParameter)
                            sSQLCom.Parameters.Add(Parameter);

                        sConn.Open();
                        sSQLCom.ExecuteNonQuery();
                        sConn.Close();
                    }
                }
            }
            catch (Exception){
                sBoolean = false;
            }

            return sBoolean;
        }

        public static Boolean ExecuteParameterSPs(String sSPs, SqlParameter[] sSQLParameter)
        {
            Boolean sBoolean = true;
            try
            {
                using (SqlConnection sConn = new SqlConnection(clsGlobal.gsConnectionString))
                {
                    using (SqlCommand sSQLCom = new SqlCommand(sSPs, sConn))
                    {
                        sSQLCom.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter Parameter in sSQLParameter)
                            sSQLCom.Parameters.Add(Parameter);

                        sConn.Open();
                        sSQLCom.ExecuteNonQuery();
                        sConn.Close();
                    }
                }
            }
            catch (Exception){
                sBoolean = false;
            }

            return sBoolean;
        }

        public static DataTable ReturnDataTableSPs(String sSPs, SqlParameter[] sSQLParameter)
        {
            DataTable myDT = new DataTable();
            try
            {
                using (SqlConnection sConn = new SqlConnection(clsGlobal.gsConnectionString))
                {
                    using (SqlCommand sSQLCom = new SqlCommand(sSPs, sConn))
                    {
                        sSQLCom.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter Parameter in sSQLParameter)
                            sSQLCom.Parameters.Add(Parameter);

                        sConn.Open();
                        using (SqlDataAdapter myDA = new SqlDataAdapter(sSQLCom))
                        {
                            sSQLCom.CommandTimeout = 0;
                            myDA.Fill(myDT);
                        }
                        sConn.Close();
                    }
                }
            }
            catch (Exception){
            }

            return myDT;
        }

        public static DataTable ReturnDataTableSPs(String sSPs)
        {
            DataTable myDT = new DataTable();
            try
            {
                using (SqlConnection sConn = new SqlConnection(clsGlobal.gsConnectionString))
                {
                    using (SqlCommand sCmd = new SqlCommand(sSPs, sConn))
                    {
                        sCmd.CommandType = CommandType.StoredProcedure;
                        sConn.Open();
                        using (SqlDataAdapter myDA = new SqlDataAdapter(sCmd))
                        {
                            sCmd.CommandTimeout = 0;
                            myDA.Fill(myDT);
                        }
                        sConn.Close();
                    }
                }
            }
            catch (Exception){
            }

            return myDT;
        }

        public static Byte[] ReturnByteFile(String sPathFile)
        {
            Byte[] sFileByte = new Byte[1];
            try
            {
                using (FileStream sStream = File.OpenRead(sPathFile))
                {
                    sFileByte = new Byte[sStream.Length];
                    sStream.Read(sFileByte, 0, (int)sStream.Length);
                    sStream.Close();
                }
            }
            catch (Exception){
            }

            return sFileByte;
        }

        public static Byte[] ReturnByteImage(PictureBox sImage)
        {
            Byte[] sImageByte = new Byte[1];
            try {
                sImageByte = (Byte[])new ImageConverter().ConvertTo(sImage.Image, typeof(Byte[]));
            }
            catch (Exception){
            }

            return sImageByte;
        }

        public static Byte[] ReturnByte(Object sObject)
        {
            Byte[] sByte = new Byte[1];
            try {
                sByte = (Byte[])sObject;
            }
            catch (Exception) {
            }

            return sByte;
        }

        public static Bitmap ReturnBitmap(Byte[] sByte)
        {
            Bitmap sBitmap;
            try{
                Byte[] pImage = new Byte[sByte.LongLength];
                pImage = sByte;
                MemoryStream sMemory = new MemoryStream(pImage);
                sBitmap = new Bitmap(sMemory);
            }
            catch{
                sBitmap = global::KP_RES.Properties.Resources.slr_camera_26;
            }

            return sBitmap;
        }

        public static Boolean ReadByteFile(String sPathFile, Byte[] sByteFile)
        {
            Boolean sBoolean = true;
            try
            {
                using (FileStream sStream = new FileStream(sPathFile, FileMode.Create, FileAccess.ReadWrite))
                {
                    using (BinaryWriter bw = new BinaryWriter(sStream))
                    {
                        bw.Write(sByteFile);
                        bw.Close();
                    }
                }
            }
            catch (Exception){
                sBoolean = false;
            }

            return sBoolean;
        }

        public static String Filter()
        {
            String Filter = "";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }


        public static void LoadCombo(DataTable sMyDT, LookUpEdit sCbo)
        {
            sCbo.Properties.DataSource = sMyDT;
            sCbo.EditValue = sCbo.Properties.GetDataSourceValue(sCbo.Properties.ValueMember, 0);
        }

        public static void LoadCombo(String sSPs, LookUpEdit sCbo)
        {
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs(sSPs);
            sCbo.Properties.DataSource = myDT;
            sCbo.EditValue = sCbo.Properties.GetDataSourceValue(sCbo.Properties.ValueMember, 0);
        }

        public static void LoadCombo(String sSPs, SqlParameter[] sSQLParameter, LookUpEdit sCbo)
        {
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs(sSPs, sSQLParameter);
            sCbo.Properties.DataSource = myDT;
            sCbo.EditValue = sCbo.Properties.GetDataSourceValue(sCbo.Properties.ValueMember, 0);
        }

        public static void LoadCombo(String sSPs, CheckedComboBoxEdit sCbo)
        {
            sCbo.Properties.Items.Clear();
            DataTable myDT = new DataTable();
            myDT = clsQLCongTy.ReturnDataTableSPs(sSPs);
            sCbo.Properties.DataSource = myDT;
        }

        public static void LoadCombo(DataTable sMyDT, CheckedComboBoxEdit sCbo)
        {
            sCbo.Properties.Items.Clear();
            sCbo.Properties.DataSource = sMyDT;
        }

        public static void LoadCombo(Dictionary<String, String> sDictionary, LookUpEdit sCbo)
        {
            sCbo.Properties.Columns.Add(new LookUpColumnInfo("Value"));
            sCbo.Properties.DisplayMember = "Value";
            sCbo.Properties.ValueMember = "Key";
            sCbo.Properties.DataSource = sDictionary.ToList();
        }

        public static void MessageInformation(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void MessageWarning(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult MessageQuestion(String sString, String sCaption)
        {
            return XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

    }
}
