﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;
using System.IO;

namespace KP_RES
{
    public class cls_Huyvietnam
    {
        public class Category
        {
            public String id { get; set; }
            public String name { get; set; }
            public String lisT_ORDER { get; set; }
            public String active { get; set; }
        }

        public class Item
        {
            public String id { get; set; }
            public String name { get; set; }
            public String description { get; set; }
            public String uom { get; set; }
            public String catelgorY_ID { get; set; }
            public String branD_ID { get; set; }
            public String subItems { get; set; }
            public String price { get; set; }
            public String tax { get; set; }
            public String lisT_ORDER { get; set; }
            public String flaG_SERVICE { get; set; }
            public String flaG_TOPPING { get; set; }
            public String active { get; set; }
        }

        public class BILL_HEADER
        {
            public String storE_ID { get; set; }
            public String bilL_NO { get; set; }
            public String bilL_DATE { get; set; }
            public String ordeR_CODE { get; set; }
            public String ordeR_DATE { get; set; }
            public String cusT_ACC { get; set; }
            public String addresS_SHIPTO { get; set; }
            public String carD_NO { get; set; }
            public String merchanT_ID { get; set; }
            public String paY_CASH { get; set; }
            public String paY_BANK { get; set; }
            public String paY_CARD { get; set; }
            public String paY_COUPON { get; set; }
            public String paY_DEBIT { get; set; }
            public String paY_NOTE { get; set; }
            public String voucheR_CODE { get; set; }
            public String cashier { get; set; }
            public String comment { get; set; }
        }

        public class BILL_LINE
        {
            public String storE_ID { get; set; }
            public String bilL_NO { get; set; }
            public String bilL_DATE { get; set; }
            public String iteM_ID { get; set; }
            public String uniT_PRICE { get; set; }
            public String quantity { get; set; }
            public String discount { get; set; }
        }

        public class Card
        {
            public String CARD_NO { get; set; }
            public String CARD_NAME { get; set; }
            public String DOB { get; set; }
            public String PHONE { get; set; }
            public String ADDRESS { get; set; }
            public String MEMBERSHIP_TYPE_ID { get; set; }
            public String MEMBERSHIP_TYPE_NAME { get; set; }
            public String DISCOUNT { get; set; }
            public String POINT_TO_AMOUNT { get; set; }
            public String CURRENT_POINT { get; set; }
            public String ACCOUNT_BALANCE { get; set; }
            public String IS_ONCE { get; set; }
            public String ACTIVE { get; set; }
            public String categories { get; set; }
            public String CARD_TYPE { get; set; }
        }


        public class Voucher
        {
            public String voucheR_CODE { get; set; }
            public String flaG_USED { get; set; }
            public String description { get; set; }
            public String datE_START { get; set; }
            public String datE_END { get; set; }
            public String flaG_ONCE { get; set; }
            public String flaG_ISDISCOUNT { get; set; }
            public String percenT_VALUE { get; set; }
            public String amounT_VALUE { get; set; }
            public String flaG_ALLITEM { get; set; }
            public String apply_Stores { get; set; }
            public String apply_Categories { get; set; }
            public String apply_Items { get; set; }
            public String used_StoreID { get; set; }
            public String used_BillNo { get; set; }
            public String used_Time  { get; set; }
            public String used_Comment { get; set; }
        }

        public class Card_POST
        {
            public String Card_No { get; set; }
            public String Amount { get; set; }
            public String Bill_No { get; set; }
            public String Store_ID { get; set; }
        }

        public class Voucher_POST
        {
            public String Voucher_Code { get; set; }
            public String Bill_No { get; set; }
            public String Store_ID { get; set; }
        }

        public class ItemNote
        {
            public String ACTIVE { get; set; }
            public String Description { get; set; }
            public String ID { get; set; }
            public String Item_ID { get; set; }
        }

        public static string getNhomhang(string MenuID)
        {
            if (MenuID == "0")
            {
                return "Error: ID 0";
            }

            string  kq = "1";
            try
            {
                //Nhom hang
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/Category_?Store_ID="+MenuID ;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;

                List<Category> CategoryList = JsonConvert.DeserializeObject<List<Category>>(sResult);

                foreach (Category item in CategoryList)
                {
                    string sql = "EXEC SP_INSERT_NHOMHANG " + clsMain.SQLString(item.id) + " , " + clsMain.SQLStringUnicode(item.name) + " , " + clsMain.SQLString(item.lisT_ORDER) + " , " + clsMain.SQLString(item.active);
                    clsMain.ExecuteSQL(sql);
                }
            }
            catch(Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static string getHanghoa(string MenuID)
        {
            if (MenuID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "2";
            try
            {
                //hang hoa
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/Item_?Store_ID="+MenuID ;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;

                List<Item> ItemList = JsonConvert.DeserializeObject<List<Item>>(sResult);

                foreach (Item item in ItemList)
                {
                    string sql = "EXEC SP_INSERT_HANGHOA " + clsMain.SQLString(item.id) + " , " + clsMain.SQLStringUnicode(item.name) + " , " + clsMain.SQLString(item.description) + " , " + clsMain.SQLString(item.uom) + " , " + clsMain.SQLString(item.catelgorY_ID) + " , " + clsMain.SQLString(item.price) + " , " + clsMain.SQLString(item.tax) + " , " + clsMain.SQLString(item.lisT_ORDER) + " , " + clsMain.SQLString(item.flaG_SERVICE) + " , " + clsMain.SQLString(item.flaG_TOPPING) + " , " + clsMain.SQLString(item.active);
                    clsMain.ExecuteSQL(sql);
                }
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static string getYeucauthem(string Store_ID)
        {
            if (Store_ID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "1";
            try
            {
                //Nhom hang
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/ItemNote_?Store_ID=" + Store_ID;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;

                List<ItemNote> ItemNote = JsonConvert.DeserializeObject<List<ItemNote>>(sResult);

                foreach (ItemNote item in ItemNote)
                {
                    string sql = "EXEC SP_INSERT_YEUCAUTHEM " + clsMain.SQLString(item.ID) + " , " + clsMain.SQLStringUnicode(item.Description) + " , " + clsMain.SQLString(item.ACTIVE) + " , " + clsMain.SQLString(item.Item_ID);
                    clsMain.ExecuteSQL(sql);
                }
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static string postBILL(string Store_ID)
        {
            if (Store_ID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "3";
            try
            {
                //bill 
                string sSQL = "EXEC SP_GET_HOADON ";
                DataTable dtBill = clsMain.ReturnDataTable(sSQL);
                string sMaHoaDOn = "";
                foreach (DataRow dr in dtBill.Rows)
                {
                    BILL_HEADER oBILL = new BILL_HEADER();
                    oBILL.storE_ID = Store_ID;
                    oBILL.bilL_NO = dr["MA_HOADON"].ToString();
                    oBILL.bilL_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIORA"]);
                    oBILL.ordeR_CODE = dr["TEN_BAN"].ToString();
                    oBILL.ordeR_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIOVAO"]);
                    oBILL.cusT_ACC = "";
                    oBILL.addresS_SHIPTO = "";
                    oBILL.carD_NO = dr["MA_KHACHHANG"].ToString();
                    oBILL.merchanT_ID = "";
                    oBILL.paY_CASH = dr["MONEYCASH"].ToString();
                    oBILL.paY_BANK = dr["MONEYVISA"].ToString();
                    oBILL.paY_CARD = dr["MONEYINNERCARD"].ToString();
                    oBILL.paY_COUPON = dr["MONEYVOUCHER"].ToString();
                    oBILL.paY_DEBIT = "0";
                    oBILL.paY_NOTE = dr["SOTHETT"].ToString();
                    oBILL.voucheR_CODE = "";
                    oBILL.cashier = dr["TENNHANVIEN"].ToString();
                    oBILL.comment = "";

                    string output = JsonConvert.SerializeObject(oBILL);

                    string uri = "http://service.huyvietnam.vn:8080/api/BILL_HEADER";
                    string parameter = output;

                    var client = new RestClient(uri);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", parameter, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    var content = response.Content;

                    string sResult = content;
                    System.Net.HttpStatusCode sHttpStatusCode = response.StatusCode;

                    BILL_LINE oBILL_LINE = new BILL_LINE();
                    oBILL_LINE.storE_ID = Store_ID;
                    oBILL_LINE.bilL_NO = dr["MA_HOADON"].ToString();
                    oBILL_LINE.bilL_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIORA"]);
                    oBILL_LINE.iteM_ID = dr["MA_HANGHOA"].ToString();
                    oBILL_LINE.uniT_PRICE = dr["GIABAN"].ToString();
                    oBILL_LINE.quantity = dr["SOLUONG"].ToString();
                    oBILL_LINE.discount = dr["CHIETKHAU"].ToString();

                    string output1 = JsonConvert.SerializeObject(oBILL_LINE);

                    string uri1 = "http://service.huyvietnam.vn:8080/api/BILL_LINE";
                    string parameter1 = output1;

                    var client1 = new RestClient(uri1);
                    var request1 = new RestRequest(Method.POST);
                    request1.AddHeader("Content-Type", "application/json");
                    request1.AddParameter("application/json", parameter1, ParameterType.RequestBody);
                    IRestResponse response1 = client1.Execute(request1);
                    var content1 = response1.Content;

                    string sResult1 = content1;

                    System.Net.HttpStatusCode sHttpStatusCode1 = response1.StatusCode;

                    string sLog = output + " - " + sResult + " - " + output1 + " - " + sResult1;

                    File.AppendAllText(Application.StartupPath + "\\LogFile.txt", sLog + Environment.NewLine);

                    if (sHttpStatusCode == System.Net.HttpStatusCode.Accepted && sHttpStatusCode1 == System.Net.HttpStatusCode.Accepted)
                    {
                        if (dr["MA_HOADON"].ToString() != sMaHoaDOn)
                        {
                            sMaHoaDOn = dr["MA_HOADON"].ToString();
                            string sql = "UPDATE HOADON SET IS_SEND = 1 WHERE MA_HOADON =" + clsMain.SQLString(sMaHoaDOn);
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static string postBILL_One(string Store_ID)
        {
            if (Store_ID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "3";
            try
            {
                //bill 
                string sSQL = "EXEC SP_GET_HOADON_ONE ";
                DataTable dtBill = clsMain.ReturnDataTable(sSQL);
                string sMaHoaDOn = "";
                foreach (DataRow dr in dtBill.Rows)
                {
                    BILL_HEADER oBILL = new BILL_HEADER();
                    oBILL.storE_ID = Store_ID;
                    oBILL.bilL_NO = dr["MA_HOADON"].ToString();
                    oBILL.bilL_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIORA"]);
                    oBILL.ordeR_CODE = dr["TEN_BAN"].ToString();
                    oBILL.ordeR_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIOVAO"]);
                    oBILL.cusT_ACC = "";
                    oBILL.addresS_SHIPTO = "";
                    oBILL.carD_NO = dr["MA_KHACHHANG"].ToString();
                    oBILL.merchanT_ID = "";
                    oBILL.paY_CASH = dr["MONEYCASH"].ToString();
                    oBILL.paY_BANK = dr["MONEYVISA"].ToString();
                    oBILL.paY_CARD = dr["MONEYINNERCARD"].ToString();
                    oBILL.paY_COUPON = dr["MONEYVOUCHER"].ToString();
                    oBILL.paY_DEBIT = "0";
                    oBILL.paY_NOTE = dr["SOTHETT"].ToString();
                    oBILL.voucheR_CODE = "";
                    oBILL.cashier = dr["TENNHANVIEN"].ToString();
                    oBILL.comment = "";

                    string output = JsonConvert.SerializeObject(oBILL);

                    string uri = "http://service.huyvietnam.vn:8080/api/BILL_HEADER";
                    string parameter = output;

                    var client = new RestClient(uri);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", parameter, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    var content = response.Content;

                    string sResult = content;
                    System.Net.HttpStatusCode sHttpStatusCode = response.StatusCode;

                    BILL_LINE oBILL_LINE = new BILL_LINE();
                    oBILL_LINE.storE_ID = Store_ID;
                    oBILL_LINE.bilL_NO = dr["MA_HOADON"].ToString();
                    oBILL_LINE.bilL_DATE = String.Format("{0:yyyy-MM-dd HH:mm:ss}", dr["GIORA"]);
                    oBILL_LINE.iteM_ID = dr["MA_HANGHOA"].ToString();
                    oBILL_LINE.uniT_PRICE = dr["GIABAN"].ToString();
                    oBILL_LINE.quantity = dr["SOLUONG"].ToString();
                    oBILL_LINE.discount = dr["CHIETKHAU"].ToString();

                    string output1 = JsonConvert.SerializeObject(oBILL_LINE);

                    string uri1 = "http://service.huyvietnam.vn:8080/api/BILL_LINE";
                    string parameter1 = output1;

                    var client1 = new RestClient(uri1);
                    var request1 = new RestRequest(Method.POST);
                    request1.AddHeader("Content-Type", "application/json");
                    request1.AddParameter("application/json", parameter1, ParameterType.RequestBody);
                    IRestResponse response1 = client1.Execute(request1);
                    var content1 = response1.Content;

                    string sResult1 = content1;

                    System.Net.HttpStatusCode sHttpStatusCode1 = response1.StatusCode;

                    string sLog = output + " - " + sResult + " - " + output1 + " - " + sResult1;

                    File.AppendAllText(Application.StartupPath + "\\LogFile.txt", sLog + Environment.NewLine);

                    if (sHttpStatusCode == System.Net.HttpStatusCode.Accepted && sHttpStatusCode1 == System.Net.HttpStatusCode.Accepted)
                    {
                        if (dr["MA_HOADON"].ToString() != sMaHoaDOn)
                        {
                            sMaHoaDOn = dr["MA_HOADON"].ToString();
                            string sql = "UPDATE HOADON SET IS_SEND = 1 WHERE MA_HOADON =" + clsMain.SQLString(sMaHoaDOn);
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static DataTable getCard(string maKH)
        {
            DataTable  kq = new DataTable () ;
            kq.Columns.Add("MA");
            kq.Columns.Add("TEN");
            kq.Columns.Add("NGAYSINH");
            kq.Columns.Add("DIENTHOAI");
            kq.Columns.Add("DIACHI");
            kq.Columns.Add("SoDiem");
            kq.Columns.Add("SoDiemHuuDung");
            kq.Columns.Add("SoDuTaiKhoan");
            kq.Columns.Add("CHIETKHAU");
            kq.Columns.Add("categories");
            try
            {
                //Khach hang
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/Card/" + maKH;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;
                
                Card oCard = JsonConvert.DeserializeObject<Card>(sResult);
                if (double .Parse ( oCard.DISCOUNT) <= 1)
                {
                    oCard.DISCOUNT = (double.Parse(oCard.DISCOUNT) * 100).ToString ();
                }

                kq.Rows.Add(oCard.CARD_NO, oCard.CARD_NAME, oCard.DOB, oCard.PHONE, oCard.ADDRESS, oCard.CURRENT_POINT, oCard.CURRENT_POINT, oCard.ACCOUNT_BALANCE, oCard.DISCOUNT,oCard.categories);
            }
            catch (Exception ex)
            {
            }
            return kq;
        }

        public static string postCard(string Card_No, string Amount, string Bill_No, string Store_ID)
        {
            if (Store_ID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "4";
            try
            {
                //Card_POST 
                Card_POST oCard_POST = new Card_POST();
                oCard_POST.Card_No = Card_No;
                oCard_POST.Amount = Amount;
                oCard_POST.Bill_No = Bill_No ;
                oCard_POST.Store_ID = Store_ID;

                string output = JsonConvert.SerializeObject(oCard_POST);

                string uri = "http://service.huyvietnam.vn:8080/api/Card";
                string parameter = output;

                var client = new RestClient(uri);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", parameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                string sResult = content;

                System.Net.HttpStatusCode sHttpStatusCode = response.StatusCode;

                if (sHttpStatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    kq = "00";
                }
                else
                {
                    kq = sHttpStatusCode.ToString();
                }

                string sLog = output + " - " + sResult;

                File.AppendAllText(Application.StartupPath + "\\LogFile.txt", sLog + Environment.NewLine);
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static DataTable getVoucher(string maVoucher)
        {
            DataTable kq = new DataTable();
            kq.Columns.Add("MA");
            kq.Columns.Add("flaG_USED");
            kq.Columns.Add("datE_START");
            kq.Columns.Add("datE_END");
            kq.Columns.Add("GIATRI_PHANTRAM");
            kq.Columns.Add("GIATRI_TIEN");
            try
            {
                //Khach hang
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/Voucher/" + maVoucher;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;

                Voucher oVoucher = JsonConvert.DeserializeObject<Voucher>(sResult);
                if (double.Parse(oVoucher.percenT_VALUE) <= 1)
                {
                    oVoucher.percenT_VALUE = (double.Parse(oVoucher.percenT_VALUE) * 100).ToString();
                }

                if (bool.Parse(oVoucher.flaG_ISDISCOUNT.Trim()) == false)
                {
                    kq.Rows.Add(oVoucher.voucheR_CODE, oVoucher.flaG_USED, oVoucher.datE_START, oVoucher.datE_END, oVoucher.percenT_VALUE, oVoucher.amounT_VALUE);
                }
            }
            catch (Exception ex)
            {
            }
            return kq;
        }

        public static string postVoucher(string Voucher_Code, string Bill_No, string Store_ID)
        {
            if (Store_ID == "0")
            {
                return "Error: ID 0";
            }

            string kq = "5";
            try
            {
                //Card_POST 
                Voucher_POST oVoucher_POST = new Voucher_POST();
                oVoucher_POST.Voucher_Code = Voucher_Code;
                oVoucher_POST.Bill_No = Bill_No;
                oVoucher_POST.Store_ID = Store_ID;

                string output = JsonConvert.SerializeObject(oVoucher_POST);

                string uri = "http://service.huyvietnam.vn:8080/api/Voucher";
                string parameter = output;

                var client = new RestClient(uri);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", parameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                string sResult = content;

                System.Net.HttpStatusCode sHttpStatusCode = response.StatusCode;

                if (sHttpStatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    kq = "00";
                }
                else
                {
                    kq = sHttpStatusCode.ToString();
                }

                string sLog = output + " - " + sResult;

                File.AppendAllText(Application.StartupPath + "\\LogFile.txt", sLog + Environment.NewLine);
            }
            catch (Exception ex)
            {
                kq = ex.ToString();
            }
            return kq;
        }

        public static DataTable getMaGiamGia(string maGiamGia)
        {
            DataTable kq = new DataTable();
            kq.Columns.Add("MA");
            kq.Columns.Add("flaG_USED");
            kq.Columns.Add("datE_START");
            kq.Columns.Add("datE_END");
            kq.Columns.Add("GIATRI_PHANTRAM");
            kq.Columns.Add("GIATRI_TIEN");
            kq.Columns.Add("flaG_ONCE");
            try
            {
                //Khach hang
                string sResult = "";
                string uri = "http://service.huyvietnam.vn:8080/api/Voucher/" + maGiamGia;
                var client = new RestClient(uri);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                sResult = content;

                Voucher oVoucher = JsonConvert.DeserializeObject<Voucher>(sResult);
                if (double.Parse(oVoucher.percenT_VALUE) <= 1)
                {
                    oVoucher.percenT_VALUE = (double.Parse(oVoucher.percenT_VALUE) * 100).ToString();
                }

                if (bool.Parse(oVoucher.flaG_ISDISCOUNT.Trim()) == true )
                {
                    kq.Rows.Add(oVoucher.voucheR_CODE, oVoucher.flaG_USED, oVoucher.datE_START, oVoucher.datE_END, oVoucher.percenT_VALUE, oVoucher.amounT_VALUE, oVoucher.flaG_ONCE);
                }
            }
            catch (Exception ex)
            {
            }
            return kq;
        }
    
    }
}
