﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;

namespace KP_RES
{
    class cls_KHTT
    {
        #region Khach hang than thiet

        public static bool CheckKhachHangThanThiet(string maKH)
        {
            DataTable dtkh = new DataTable();
            if (cls_ConfigCashier.sAutodaybill == "1")
            {
                DataTable dtCard = cls_Huyvietnam.getCard(maKH);
                dtkh = dtCard;
            }
            else 
            {
                string sSQL = "select Ma_KHTT from KHACHHANG_THANTHIET  where SUDUNG=1 and Ma_KHTT =" + clsMain.SQLString(maKH);
                dtkh = clsMain.ReturnDataTable(sSQL);
               
            }
            if (dtkh.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static DataTable ReturnKhachHangThanThiet(string maKH)
        {
            DataTable dtkh = new DataTable();
            if (cls_ConfigCashier.sAutodaybill == "1")
            {
                DataTable dtCard = cls_Huyvietnam.getCard(maKH);
                dtkh = dtCard;
            }
            else
            {
                string sSQL = "";
                sSQL += "Select dt.MA,dt.TEN,dt.CMND,dt.NGAYSINH,dt.GIOITINH,case dt.GIOITINH when 0 then N'Nữ' else N'Nam' end as GIOITINH1,";
                sSQL += "dt.DIACHI,dt.DIENTHOAI,dt.EMAIL,dt.WEBSITE,dt.NGAYTAO,dt.SUDUNG, khtt.Ma_KHTT,khtt.SoDiem,ISNULL(khtt.SoDiemHuuDung,0) as SoDiemHuuDung,";
                sSQL += "khtt.SoLe,khtt.SoLanTinh,khtt.SoLanChoThuong,khtt.MACAPDO,cd.TENCAPDO,cd.CHIETKHAU,TTTNB.SoDuTaiKhoan" + "\n";
                sSQL += "From KHTT dt ,KHACHHANG_THANTHIET khtt , KHTT_CAPDO cd,KHACHHANG_THANHTOAN_NOIBO TTTNB" + "\n";
                sSQL += "Where dt.MA = khtt.Ma_KHTT " + "\n";
                sSQL += "and dt.MA = TTTNB.Ma_KHTT " + "\n";
                sSQL += "and khtt.MACAPDO = cd.MACAPDO" + "\n";
                sSQL += "and dt.MA= " + clsMain.SQLString(maKH) + "\n";
                dtkh = clsMain.ReturnDataTable(sSQL);
            }
            
            return dtkh;
        }

        public static string ReturnCapDoThe(int iDiem)
        {
            string sCapDoThe = "";
            string sSQL = "select dbo.[fn_DoiDiemRaCapDo_NS](" + iDiem.ToString() + ")";
            sCapDoThe = clsMain.ReturnDataTable(sSQL).Rows[0][0].ToString();
            return sCapDoThe;
        }

        public static void CongDiemKhachHangThanThiet(string maKH, decimal tongTien)
        {
            try
            {
                if (clsUserManagement.CheckActive("4") == true)
                {
                    DataTable dtKHTT = ReturnKhachHangThanThiet(maKH);
                    if (dtKHTT.Rows.Count > 0)
                    {
                        if (clsMain.ReturnDataTable("select KHTT_SoTien from KHTT_DIEM where KHTT_Flag='true'").Rows.Count > 0)
                        {
                            if (cls_ConfigCashier.sAutodaybill == "1")
                            {
                                string sql = "";
                                sql += "Insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI)" + "\n";
                                sql += "Values(";
                                sql += clsMain.SQLString(maKH) + ",";
                                sql += clsMain.SQLString(tongTien.ToString()) + ",";
                                sql += clsMain.SQLStringUnicode("Cộng điểm bán hàng trên hóa đơn") + ",";
                                sql += "GETDATE()" + ",";
                                sql += clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + ",";
                                sql += clsMain.SQLString("01") + ")" + "\n";
                                clsMain.ExecuteSQL(sql);
                            }
                            else
                            {
                                int soThuongNho = 0;
                                int soThuongLon = 0;
                                int soLanTinh = 0;
                                int diem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                                int diemHuuDung = int.Parse(dtKHTT.Rows[0]["SoDiemHuuDung"].ToString());
                                string maCapDo = "";
                                decimal soLe;
                                bool boolsoLe = bool.Parse(clsMain.ReturnDataTable("select SuDung from KHTT_SOLE where TenSoLe=N'Tính Điểm Cho Số lẻ'").Rows[0][0].ToString());
                                if (boolsoLe)
                                {

                                    tongTien += decimal.Parse(dtKHTT.Rows[0]["SoLe"].ToString());
                                    soLe = decimal.Parse(clsMain.ReturnDataTable("select dbo.[fn_LayTienLe_NS](" + tongTien + ")").Rows[0][0].ToString());
                                }
                                else
                                {
                                    soLe = 0;
                                }
                                int sodiemmcong = int.Parse(clsMain.ReturnDataTable("select dbo.[fn_DoiTienRaDiem_NS](" + tongTien + ")").Rows[0][0].ToString());
                                diem += sodiemmcong;
                                diemHuuDung += sodiemmcong;

                                maCapDo = clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](" + diem + ")").Rows[0][0].ToString();

                                DataTable dtcheckthuong = clsMain.ReturnDataTable("select MaDiemThuong from KHTT_DIEMTHUONG where SuDung=1");
                                if (dtcheckthuong.Rows.Count > 0)
                                {
                                    soLanTinh = int.Parse(dtKHTT.Rows[0]["SoLanTinh"].ToString());
                                    soThuongNho = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongNho_NS(" + diem + "," + soLanTinh + ")").Rows[0][0].ToString());
                                    soThuongLon = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongLon_NS(" + diem + ")").Rows[0][0].ToString());
                                    soLanTinh += soThuongNho;
                                    soThuongNho += int.Parse(dtKHTT.Rows[0]["SoLanChoThuong"].ToString());
                                }

                                string sql = "";
                                sql += "Insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI)" + "\n";
                                sql += "Values(";
                                sql += clsMain.SQLString(maKH) + ",";
                                sql += clsMain.SQLString(sodiemmcong.ToString()) + ",";
                                sql += clsMain.SQLStringUnicode("Cộng điểm bán hàng trên hóa đơn") + ",";
                                sql += "GETDATE()" + ",";
                                sql += clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + ",";
                                sql += clsMain.SQLString("01") + ")" + "\n";

                                sql += "Update KHACHHANG_THANTHIET Set" + "\n";
                                sql += "SoDiem= " + clsMain.SQLString(diem.ToString()) + ",";
                                sql += "SoDiemHuuDung= " + clsMain.SQLString(diemHuuDung.ToString()) + ",";
                                sql += "SoLe= " + clsMain.SQLString(soLe.ToString()) + ",";
                                sql += "MaCapDo=" + clsMain.SQLStringUnicode(maCapDo) + ",";
                                sql += "SoLanChoThuong=" + clsMain.SQLString(soThuongNho.ToString()) + ",";
                                sql += "SoLanReset=" + clsMain.SQLString(soThuongLon.ToString()) + ",";
                                sql += "SoLanTinh=" + clsMain.SQLString(soLanTinh.ToString()) + ",";
                                sql += "LastUpdate=GETDATE()" + "\n";
                                sql += "where MAKHTT=" + clsMain.SQLString(maKH);
                                clsMain.ExecuteSQL(sql);
                            }
                           
                        }
                        else
                        {
                            throw new Exception("Bạn chưa cài đặt quy định về điểm,hãy vào quy định để cài đặt!");
                        }

                    }
                    else
                    {
                        throw new Exception("Mã thẻ khách hàng thân thiết không tồn tại! vui lòng nhập lại");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static void CongDiemKhachHangThanThiet_Nhanvien(string maKH, decimal tongTien,string nhanvien)
        {
            try
            {
                if (clsUserManagement.CheckActive("4") == true)
                {
                    DataTable dtKHTT = ReturnKhachHangThanThiet(maKH);
                    if (dtKHTT.Rows.Count > 0)
                    {
                        if (clsMain.ReturnDataTable("select KHTT_SoTien from KHTT_DIEM where KHTT_Flag='true'").Rows.Count > 0)
                        {
                            int soThuongNho = 0;
                            int soThuongLon = 0;
                            int soLanTinh = 0;
                            int diem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                            int diemHuuDung = int.Parse(dtKHTT.Rows[0]["SoDiemHuuDung"].ToString());
                            string maCapDo = "";
                            decimal soLe;
                            bool boolsoLe = bool.Parse(clsMain.ReturnDataTable("select SuDung from KHTT_SOLE where TenSoLe=N'Tính Điểm Cho Số lẻ'").Rows[0][0].ToString());
                            if (boolsoLe)
                            {

                                tongTien += decimal.Parse(dtKHTT.Rows[0]["SoLe"].ToString());
                                soLe = decimal.Parse(clsMain.ReturnDataTable("select dbo.[fn_LayTienLe_NS](" + tongTien + ")").Rows[0][0].ToString());
                            }
                            else
                            {
                                soLe = 0;
                            }
                            int sodiemmcong = int.Parse(clsMain.ReturnDataTable("select dbo.[fn_DoiTienRaDiem_NS](" + tongTien + ")").Rows[0][0].ToString());
                            diem += sodiemmcong;
                            diemHuuDung += sodiemmcong;

                            maCapDo = clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](" + diem + ")").Rows[0][0].ToString();

                            DataTable dtcheckthuong = clsMain.ReturnDataTable("select MaDiemThuong from KHTT_DIEMTHUONG where SuDung=1");
                            if (dtcheckthuong.Rows.Count > 0)
                            {
                                soLanTinh = int.Parse(dtKHTT.Rows[0]["SoLanTinh"].ToString());
                                soThuongNho = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongNho_NS(" + diem + "," + soLanTinh + ")").Rows[0][0].ToString());
                                soThuongLon = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongLon_NS(" + diem + ")").Rows[0][0].ToString());
                                soLanTinh += soThuongNho;
                                soThuongNho += int.Parse(dtKHTT.Rows[0]["SoLanChoThuong"].ToString());
                            }

                            string sql = "";
                            sql += "Insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI)" + "\n";
                            sql += "Values(";
                            sql += clsMain.SQLString(maKH) + ",";
                            sql += clsMain.SQLString(sodiemmcong.ToString()) + ",";
                            sql += clsMain.SQLStringUnicode("Cộng điểm bán hàng trên hóa đơn") + ",";
                            sql += "GETDATE()" + ",";
                            sql += clsMain.SQLString(nhanvien ) + ",";
                            sql += clsMain.SQLString("01") + ")" + "\n";

                            sql += "Update KHACHHANG_THANTHIET Set" + "\n";
                            sql += "SoDiem= " + clsMain.SQLString(diem.ToString()) + ",";
                            sql += "SoDiemHuuDung= " + clsMain.SQLString(diemHuuDung.ToString()) + ",";
                            sql += "SoLe= " + clsMain.SQLString(soLe.ToString()) + ",";
                            sql += "MaCapDo=" + clsMain.SQLStringUnicode(maCapDo) + ",";
                            sql += "SoLanChoThuong=" + clsMain.SQLString(soThuongNho.ToString()) + ",";
                            sql += "SoLanReset=" + clsMain.SQLString(soThuongLon.ToString()) + ",";
                            sql += "SoLanTinh=" + clsMain.SQLString(soLanTinh.ToString()) + ",";
                            sql += "LastUpdate=GETDATE()" + "\n";
                            sql += "where MAKHTT=" + clsMain.SQLString(maKH);
                            clsMain.ExecuteSQL(sql);
                        }
                        else
                        {
                            throw new Exception("Bạn chưa cài đặt quy định về điểm,hãy vào quy định để cài đặt!");
                        }

                    }
                    else
                    {
                        throw new Exception("Mã thẻ khách hàng thân thiết không tồn tại! vui lòng nhập lại");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static void TruDiemKhachHangThanThiet(string maKH, decimal tongTien)
        {
            try
            {
                if (clsUserManagement.CheckActive("4"))//nếu có sử dụng KHTT
                {
                    DataTable dtKHTT = ReturnKhachHangThanThiet(maKH);
                    if (dtKHTT.Rows.Count > 0)
                    {
                        if (clsMain.ReturnDataTable("select KHTT_SoTien from KHTT_DIEM where KHTT_Flag='true'").Rows.Count > 0)
                        {
                            decimal soLe;
                            decimal soledata = decimal.Parse(dtKHTT.Rows[0]["SoLe"].ToString());
                            bool boolsoLe = bool.Parse(clsMain.ReturnDataTable("select SuDung from KHTT_SOLE where TenSoLe=N'Tính Điểm Cho Số lẻ'").Rows[0][0].ToString());
                            if (boolsoLe)
                            {
                                soLe = decimal.Parse(clsMain.ReturnDataTable("select dbo.[fn_LayTienLe_NS](" + tongTien + ")").Rows[0][0].ToString());
                                soLe = soledata - soLe;
                            }
                            else
                            {
                                soLe = 0;
                            }

                            int soThuongNho = 0;
                            int sodiemmtru = int.Parse(clsMain.ReturnDataTable("select dbo.[fn_DoiTienRaDiem_NS](" + tongTien + ")").Rows[0][0].ToString());
                            decimal diemtru = sodiemmtru;
                            string sql = "update KHACHHANG_THANTHIET set SoDiem-=" + diemtru + ",SoDiemHuuDung-=" + diemtru + ",LastUpdate=getdate() where MAKHTT='" + maKH + "'";
                            clsMain.ExecuteSQL(sql);
                            
                            dtKHTT = ReturnKhachHangThanThiet(maKH);
                            int diem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                            string maCapDo = clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](" + diem + ")").Rows[0][0].ToString();

                            int soLanTinh = int.Parse(dtKHTT.Rows[0]["SoLanTinh"].ToString());
                            soThuongNho = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongNho_NS(" + diem + "," + soLanTinh + ")").Rows[0][0].ToString());
                            int soThuongLon = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongLon_NS(" + diem + ")").Rows[0][0].ToString());
                            soLanTinh += soThuongNho;
                            soThuongNho += int.Parse(dtKHTT.Rows[0]["SoLanChoThuong"].ToString());
                            sql = "update KHACHHANG_THANTHIET set SoDiem= " + diem + ",SoLe= " + soLe + ",MaCapDo=N'" + maCapDo + "',SoLanChoThuong=" + soThuongNho + ",SoLanReset=" + soThuongLon + ",SoLanTinh=" + soLanTinh + " ,LastUpdate=getdate() where MAKHTT='" + maKH + "'" +"\n";
                            sql += "insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI) values('" + maKH + "'," + sodiemmtru + ",N'Trừ điểm bán hàng trên hóa đơn',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','02')";
                            clsMain.ExecuteSQL(sql);
                        }
                        else
                        {
                            throw new Exception("Bạn chưa cài đặt quy định về điểm,hãy vào quy định để cài đặt!");
                        }
                    }
                    else
                    {
                        throw new Exception("Mã thẻ khách hàng thân thiết không tồn tại! vui lòng nhập lại");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static bool  CongDiemKhachHangThanThiet_Tructiep(string maKH, decimal iSoDiem,string sLydo)
        {
            bool bKq = false;
            try
            {
                string sql = "";
                int soThuongNho = 0;
                int soLanTinh = 0;
                int soThuongLon = 0;

                sql += "insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI) values('" + maKH + "'," + int.Parse(iSoDiem.ToString ()) + ",N'" + sLydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','01')" + "\n";
                sql += "update KHACHHANG_THANTHIET set SoDiem+=" + int.Parse(iSoDiem.ToString()) + ", SoDiemHuuDung+=" + int.Parse(iSoDiem.ToString()) + " ,LastUpdate=getdate() where MAKHTT='" + maKH + "'";
                bool bSQL = clsMain.ExecuteSQL(sql);
                DataTable dtKHTT = cls_KHTT.ReturnKhachHangThanThiet(maKH);
                int diem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                string maCapDo = clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](" + diem + ")").Rows[0][0].ToString();
                DataTable dtcheckthuong = clsMain.ReturnDataTable("select MaDiemThuong from KHTT_DIEMTHUONG where SuDung=1");
                if (dtcheckthuong.Rows.Count > 0)
                {
                    soLanTinh = int.Parse(dtKHTT.Rows[0]["SoLanTinh"].ToString());
                    soThuongNho = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongNho_NS(" + diem + "," + soLanTinh + ")").Rows[0][0].ToString());
                    soThuongLon = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongLon_NS(" + diem + ")").Rows[0][0].ToString());
                    soLanTinh += soThuongNho;
                    soThuongNho += int.Parse(dtKHTT.Rows[0]["SoLanChoThuong"].ToString());
                }
                sql = "update KHACHHANG_THANTHIET set SoDiem= " + diem + ",MACAPDO='" + maCapDo + "',SoLanChoThuong=" + soThuongNho + ",SoLanReset=" + soThuongLon + ",SoLanTinh=" + soLanTinh + " ,LastUpdate=getdate() where MAKHTT='" + maKH  + "'";
                bSQL = clsMain.ExecuteSQL(sql);
                if (bSQL)
                {
                    bKq = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bKq =false ;
            }
            return bKq ;
        }

        public static bool TruDiemKhachHangThanThiet_Tructiep(string maKH, decimal iSoDiem, string sLydo)
        {
            bool bKq = false;
            try
            {
                string sql = "";
                int soThuongNho = 0;
                int soLanTinh = 0;
                int soThuongLon = 0;

                sql += "insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI)  values('" + maKH + "'," + int.Parse(iSoDiem.ToString()) + ",N'" + sLydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','02')" + "\n";
                sql += "update KHACHHANG_THANTHIET set SoDiem-=" + int.Parse(iSoDiem.ToString()) + ", SoDiemHuuDung-=" + int.Parse(iSoDiem.ToString()) + " ,LastUpdate=getdate() where MAKHTT='" + maKH + "'";
                bool bSQL = clsMain.ExecuteSQL(sql);
                DataTable dtKHTT = cls_KHTT.ReturnKhachHangThanThiet(maKH);
                int diem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                string maCapDo = clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](" + diem + ")").Rows[0][0].ToString();
                DataTable dtcheckthuong = clsMain.ReturnDataTable("select MaDiemThuong from KHTT_DIEMTHUONG where SuDung=1");
                if (dtcheckthuong.Rows.Count > 0)
                {
                    soLanTinh = int.Parse(dtKHTT.Rows[0]["SoLanTinh"].ToString());
                    soThuongNho = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongNho_NS(" + diem + "," + soLanTinh + ")").Rows[0][0].ToString());
                    soThuongLon = int.Parse(clsMain.ReturnDataTable("select dbo.fn_DoiDiemRaThuongLon_NS(" + diem + ")").Rows[0][0].ToString());
                    soLanTinh += soThuongNho;
                    soThuongNho += int.Parse(dtKHTT.Rows[0]["SoLanChoThuong"].ToString());
                }
                sql = "update KHACHHANG_THANTHIET set SoDiem= " + diem + ",MACAPDO='" + maCapDo + "',SoLanChoThuong=" + soThuongNho + ",SoLanReset=" + soThuongLon + ",SoLanTinh=" + soLanTinh + " ,LastUpdate=getdate() where MAKHTT='" + maKH  + "'";
                bSQL = clsMain.ExecuteSQL(sql);
                if (bSQL)
                {
                    bKq = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bKq = false;
            }
            return bKq;
        }

        public static DataTable ReturnCauHinhKHTT()
        {
            DataTable dtkh = new DataTable();
            string sSQL = "Select GIATRI From CAUHINH Where TEN='CONGDIEM_KHTT'";
            dtkh = clsMain.ReturnDataTable(sSQL);
            return dtkh;
        }

        #endregion

        #region The thanh toan noi bo

        public static bool CheckThanhToanNoiBo(string maKH)
        {
            bool kq = false;
            DataTable dtkh = new DataTable();
            if (cls_ConfigCashier.sAutodaybill == "1")
            {
                DataTable dtCard = cls_Huyvietnam.getCard(maKH);
                dtkh = dtCard;
            }
            else
            {
                string sSQL = "select * from KHACHHANG_THANHTOAN_NOIBO  where SUDUNG=1 and Ma_KHTT =" + clsMain.SQLString(maKH);
                dtkh = clsMain.ReturnDataTable(sSQL);
                
            }

            if (dtkh.Rows.Count > 0)
            {
                kq = true;
            }
            return kq ;

        }

        public static bool CongTien_TTTNB(string maKH, decimal sotien, string lydo)
        {
            bool bKq = false;
            try
            {
                string sql = "";
                if (lydo == "")
                {
                    lydo = "Nạp tiền vào tài khoản ";
                }
                sql += "insert into KHTT_CHITIET_NOPTIEN(MA_KH,SOTIEN,LYDO,NGAYNOP,MANV,FLAG) values('" + maKH + "'," + int.Parse(sotien.ToString().Replace(",", "")) + ",N'" + lydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','true')" + "\n";
                sql += "update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=SoDuTaiKhoan+" + int.Parse(sotien.ToString().Replace (",","")) + " where MAKHTTNB='" + maKH  + "'";
                bool bSQL = clsMain.ExecuteSQL(sql);
                if (bSQL)
                {
                    bKq = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bKq = false;
            }
            return bKq;
        }

        public static bool TruTien_TTTNB(string maKH, decimal sotien, string lydo)
        {
            bool bKq = false;
            try
            {
                string sql = "";
                if (lydo == "")
                {
                    lydo = "Rút tiền khỏi tài khoản ";
                }
                sql += "insert into KHTT_CHITIET_NOPTIEN(MA_KH,SOTIEN,LYDO,NGAYNOP,MANV,FLAG) values('" + maKH + "'," + int.Parse(sotien.ToString().Replace(",", "")) + ",N'" + lydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','false')" + "\n";
                sql += "update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=SoDuTaiKhoan-" + int.Parse(sotien.ToString().Replace(",", "")) + " where MAKHTTNB='" + maKH + "'";
                bool bSQL = clsMain.ExecuteSQL(sql);
                if (bSQL)
                {
                    bKq = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bKq = false;
            }
            return bKq;
        }

        public static decimal ReturnMoneyMustPay(string maKH, decimal tongTien)
        {
            decimal sotientra = 0;
            try
            {
                if (clsUserManagement.CheckActive("8") == true)//nếu sử dụng KHTTNB
                {
                    if (CheckThanhToanNoiBo(maKH))
                    {
                        DataTable dtSoDu = new DataTable();
                        if (cls_ConfigCashier.sAutodaybill == "1")
                        {
                            DataTable dtCard = cls_Huyvietnam.getCard(maKH);

                            dtSoDu.Columns.Add("SoDuTaiKhoan");
                            if (dtCard.Rows.Count > 0)
                            {
                                dtSoDu.Rows.Add(dtCard.Rows[0]["SoDuTaiKhoan"].ToString ());
                            }
                            else
                            {
                                dtSoDu.Rows.Add("0");
                            }
                        }
                        else
                        {
                            dtSoDu = clsMain.ReturnDataTable("select SoDuTaiKhoan from KHACHHANG_THANHTOAN_NOIBO where MAKHTTNB='" + maKH + "' and SUDUNG=1");
                        }
                   
                        decimal sodutaikhoan = decimal.Parse(dtSoDu.Rows[0][0].ToString());
                        if (sodutaikhoan == 0)
                        {
                            throw new Exception("Số dư tài khoản đang là 0 đồng,bạn không thể thanh toán");
                        }

                        if (sodutaikhoan < tongTien)
                        {
                            if (DialogResult.Yes == XtraMessageBox.Show("Số tiền thanh toán lớn hơn số dư tài khoản của bạn hiện có là " + string.Format("{0:#,0}", sodutaikhoan) + ",bạn có muốn tiếp tục thanh toán không?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                sotientra = sodutaikhoan;

                            }
                        }
                        else
                        {
                            sotientra = tongTien;

                        }

                    }
                    else
                    {
                        throw new Exception("Mã thẻ khách hàng thanh toán nội bộ này không tồn tại! vui lòng nhập lại");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return sotientra;
        }

        public static void UpdateInnerCustomer(string maKH, decimal tongTien)
        {
            try
            {
                if (clsUserManagement.CheckActive("8") == true)//nếu sử dụng KHTTNB
                {
                    if (cls_ConfigCashier.sAutodaybill == "1")
                    {
                    }
                    else
                    {
                        DataTable dtSoDu = clsMain.ReturnDataTable("select SoDuTaiKhoan from KHACHHANG_THANHTOAN_NOIBO where MAKHTTNB='" + maKH + "' and SUDUNG=1");
                        decimal sodutaikhoan = decimal.Parse(dtSoDu.Rows[0][0].ToString());
                        clsMain.ExecuteSQL("update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=" + (sodutaikhoan - tongTien) + " where MAKHTTNB='" + maKH + "'");
                    }
                    clsMain.ExecuteSQL("insert KHTT_CHITIET_NOPTIEN(MA_KH,SoTien,NgayNop,Flag,MANV,LYDO) values ('" + maKH + "','" + tongTien + "',getdate(),0,'" + clsUserManagement.ReturnMaNVOfUserLogin() + "',N'Trừ tiền mua hàng')");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static decimal ThanhToanNoiBo(string maKH, decimal tongTien)
        {
            decimal sotientra = 0;
            try
            {
                if (clsUserManagement.CheckActive("8") == true)//nếu sử dụng KHTTNB
                {
                    if (CheckThanhToanNoiBo(maKH))
                    {
                        DataTable dtSoDu = clsMain.ReturnDataTable("select SoDuTaiKhoan from KHACHHANG_THANHTOAN_NOIBO where MAKHTTNB='" + maKH + "' and SUDUNG=1");
                        decimal sodutaikhoan = decimal.Parse(dtSoDu.Rows[0][0].ToString());
                        if (sodutaikhoan == 0)
                        {
                            throw new Exception("Số dư tài khoản đang là 0 đồng,bạn không thể thanh toán");
                        }

                        if (sodutaikhoan < tongTien)
                        {
                            if (DialogResult.Yes == XtraMessageBox.Show("Số tiền thanh toán lớn hơn số dư tài khoản của bạn hiện có là " + string.Format("{0:#,0}", sodutaikhoan) + ",bạn có muốn tiếp tục thanh toán không?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                sotientra = sodutaikhoan;
                                clsMain.ExecuteSQL("update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=0 where MAKHTTNB='" + maKH + "'");
                                string sql = "insert KHTT_CHITIET_NOPTIEN(MA_KH,SoTien,NgayNop,Flag,MANV,LYDO) values ('" + maKH + "','" + sotientra + "',getdate(),0,'" + clsUserManagement.ReturnMaNVOfUserLogin() + "',N'Trừ tiền mua hàng')";
                                clsMain.ExecuteSQL(sql);
                            }
                        }
                        else
                        {
                            sotientra = tongTien;
                            clsMain.ExecuteSQL("update KHACHHANG_THANHTOAN_NOIBO set SoDuTaiKhoan=" + (sodutaikhoan - tongTien) + " where MAKHTTNB='" + maKH + "'");
                            clsMain.ExecuteSQL("insert KHTT_CHITIET_NOPTIEN(MA_KH,SoTien,NgayNop,Flag,MANV,LYDO) values ('" + maKH + "','" + sotientra + "',getdate(),0,'" + clsUserManagement.ReturnMaNVOfUserLogin() + "',N'Trừ tiền mua hàng')");
                        }
                    }
                    else
                    {
                        throw new Exception("Mã thẻ khách hàng thanh toán nội bộ này không tồn tại! vui lòng nhập lại");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return sotientra;
        }

        public static void ActiveKHTT(string maKH)
        {
            if (clsUserManagement.CheckActive("4") == true)//kiểm tra có dùng khtt ko?
            {
                DataTable dtKHTT = clsMain.ReturnDataTable("select LoaiThanhVien,SoDiem from KHACHHANG_THANTHIET where SuDung=1 and MAKHTT='" + maKH + "'");
                bool LoaiKH = bool.Parse(dtKHTT.Rows[0]["LoaiThanhVien"].ToString());
                int sodiem = int.Parse(dtKHTT.Rows[0]["SoDiem"].ToString());
                if (!LoaiKH)
                {
                    if (sodiem >= 20)
                    {
                        clsMain.ExecuteSQL("update KHACHHANG_THANTHIET set LoaiThanhVien=1,SoDiem=SoDiem-20,NgayKichHoat=getdate() where MAKHTT='" + maKH + "'");
                        string lydo = "Trừ Điểm Khi Gia Hạn Thành Viên Chính Thức";
                        string sqltrudiem = "insert into KHTT_CONG_TRU_DIEM(MAKH,SODIEM,LYDO,NGAYCONG,MANV,MALOAI) values('" + maKH + "'," + 20 + ",N'" + lydo + "',GETDATE(),'" + clsUserManagement.ReturnMaNVOfUserLogin() + "','02')";
                        clsMain.ExecuteSQL(sqltrudiem);
                    }
                }
            }

        }

        #endregion

        #region Voucher

        // 0:thẻ hợp lệ
        // 1:mã thẻ không tồn tại
        // 2:thẻ đã được sử dụng
        // 3:thẻ chưa tới ngày sử dụng
        // 4:thẻ có ngày sử dụng không hợp lệ(hết hạn)
        public static int CheckTheVoucher(string maVoucher)
        {
            DataTable dtkh = clsMain.ReturnDataTable("Select NGAY_KICHHOAT,GIO_KICHHOAT,NGAY_HETHAN,GIO_HETHAN,LOAI_VOUCHER,GIATRI_PHANTRAM,GIATRI_TIEN,STATUS From VOUCHER_DETAIL Where MA ='" + maVoucher + "'");
            if (dtkh.Rows.Count > 0)
            {
                if (bool.Parse(dtkh.Rows[0]["STATUS"].ToString()))
                {
                    return 2;
                }
                else if (DateTime.Parse(dtkh.Rows[0]["NGAY_KICHHOAT"].ToString()).Date > clsMain.GetServerDate().Date ||
                        (DateTime.Parse(dtkh.Rows[0]["NGAY_KICHHOAT"].ToString()).Date == clsMain.GetServerDate().Date && Convert.ToDateTime(dtkh.Rows[0]["GIO_KICHHOAT"].ToString()).TimeOfDay > DateTime.Now.TimeOfDay))
                {
                    return 3;
                }
                else if (DateTime.Parse(dtkh.Rows[0]["NGAY_HETHAN"].ToString()).Date < clsMain.GetServerDate().Date ||
                        (DateTime.Parse(dtkh.Rows[0]["NGAY_HETHAN"].ToString()).Date == clsMain.GetServerDate().Date && Convert.ToDateTime(dtkh.Rows[0]["GIO_HETHAN"].ToString()).TimeOfDay < DateTime.Now.TimeOfDay))
                {
                    return 4;
                }
                else
                    return 0;
            }
            else
                return 1;
        }

        #endregion

        #region Cong no

        public static void CongCongNoNhaCungCap(string maKH, string maPhieu, decimal tongTien, decimal tiendatra, DateTime ngayHenTra, bool httt)//true là tiền mặt;fale là chuyển khoản
        {
            if (clsUserManagement.CheckActive("12") == true)//nếu có sử dụng công nợ thì tính
            {

                decimal tienconlai = tongTien - tiendatra;
                string tam = "0";

                if (httt)
                {
                    string sqlchitietthanhtoan = "";
                    if (tienconlai == 0)
                    {
                        sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYTREHEN,ISTREHEN)";
                        sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",N'" + tam + "',GETDATE(),'true','false',NULL,'false')";
                    }
                    else
                    {
                        sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN)";
                        sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",N'" + tam + "',GETDATE(),'true','false',(Select Convert(datetime,'" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',4)),NULL,'false')";
                    }
                    clsMain.ExecuteSQL(sqlchitietthanhtoan);

                    //Tính tiền Công Nợ
                    string sSQL = "";
                    sSQL += "Update NHACUNGCAP " + "\n";
                    sSQL += "Set TIEN_CONGNO=ISNULL(TIEN_CONGNO,0) + " + tienconlai + "\n";
                    sSQL += " Where MA=" + clsMain.SQLString(maKH);
                    clsMain.ExecuteSQL(sSQL);

                    if (tiendatra == 0)
                    {
                        clsMain.ExecuteSQL("update CHITIETTHANHTOAN set FLAG = 'False' where PK_PHIEUKHOID='" + maPhieu + "'");
                        if (tienconlai == 0)
                        {
                            sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN)";
                            sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",'" + 0 + "',GETDATE(),'false','false',(Select Convert(datetime,'" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',4)),NULL,'false')";
                            clsMain.ExecuteSQL(sqlchitietthanhtoan);
                        }
                        else
                        {
                            sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN)";
                            sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",'" + 0 + "',GETDATE(),'true','false',(Select Convert(datetime,'" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',4)),NULL,'false')";
                            clsMain.ExecuteSQL(sqlchitietthanhtoan);
                        }
                    }
                }
                else
                {
                    string sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYTREHEN,ISTREHEN)";
                    sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",N'" + tam + "',GETDATE(),'true','false',NULL,'false')";
                    clsMain.ExecuteSQL(sqlchitietthanhtoan);
                    clsMain.ExecuteSQL("update CHITIETTHANHTOAN set FLAG = 'False' where PK_PHIEUKHOID='" + maPhieu + "'");
                    bool trahet;
                    if (tienconlai == 0)
                    {
                        trahet = false;
                    }
                    else
                    {
                        trahet = true;
                    }
                    if (trahet)
                    {
                        sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN)";
                        sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",'" + 0 + "',GETDATE(),'" + trahet + "','false',(Select Convert(datetime,'" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',4)),NULL,'false')";
                    }
                    else
                    {
                        sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYTREHEN,ISTREHEN)";
                        sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + tiendatra + "," + tienconlai + ",'" + 0 + "',GETDATE(),'" + trahet + "','false',NULL,'false')";
                    }
                    clsMain.ExecuteSQL(sqlchitietthanhtoan);
                    decimal tongtienmat = 0;
                    DataTable dttienmat = clsMain.ReturnDataTable("select top(1) TONGTIEN from CN_CONGNO where SUDUNG='true' order by NGAYLAPPHIEU desc");
                    if (dttienmat.Rows.Count > 0)
                    {
                        tongtienmat = decimal.Parse(dttienmat.Rows[0][0].ToString());
                    }
                    string maphieu = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_CHI_NS()").Rows[0][0].ToString();
                    string hoten = "";
                    string diachi = "";
                    DataTable dtkh = clsMain.ReturnDataTable("select TEN, DIACHI from NHACUNGCAP where MA ='" + maKH + "'");
                    if (dtkh.Rows.Count > 0)
                    {
                        hoten = dtkh.Rows[0]["TEN"].ToString();
                        diachi = dtkh.Rows[0]["DIACHI"].ToString();
                    }
                    string sqltienmat = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}', @TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@TIENMAT='{13}',@NGAYTHANHTOAN='{14}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC='{22}'",
                                        maphieu, maKH, "", maPhieu, clsUserManagement.ReturnMaNVOfUserLogin(), "03", "Chi tiền hàng bằng chuyển khoản", tiendatra, "", tongtienmat, hoten, diachi, "", false, string.Format("{0: yyyyMMdd HH:mm:ss}", KP_UserManagement.clsMain.GetServerDate()), "", "NULL", "", "NULL", "NULL", "", "NULL", "");
                    if (tiendatra > 0)
                    {
                        clsMain.ExecuteSQL(sqltienmat);
                    }
                }
            }
        }

        public static void CongNoKhachHangPhieuXuat(string maKH, string maPhieu, decimal tongTien, decimal soTienTra, DateTime ngayHenTra, bool Chuoi_Httt)//true là tiền mặt;fale là chuyển khoản
        {
            decimal soTienConLai = tongTien - soTienTra;

            if (clsUserManagement.CheckActive("12") == true)//nếu sử dụng công nợ thì làm
            {
                string sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN)";
                //tinh cong no
                if (soTienConLai == 0)
                {
                    sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + soTienTra + "," + soTienConLai + ",N'0',GETDATE(),'false','true',NULL,NULL,'false')";
                }
                else
                {
                    sqlchitietthanhtoan += " values ('" + maPhieu + "'," + tongTien + "," + soTienTra + "," + soTienConLai + ",'" + 0 + "',GETDATE(),'true','true',(Select Convert(datetime,'" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',4)),NULL,'false')";
                }
                clsMain.ExecuteSQL(sqlchitietthanhtoan);

                //Tính tiền Công Nợ
                string sSQL = "";
                sSQL += "Update NHACUNGCAP " + "\n";
                sSQL += "Set TIEN_CONGNO=ISNULL(TIEN_CONGNO,0) + " + soTienConLai + "\n";
                sSQL += " Where MA=" + clsMain.SQLString(maKH);
                clsMain.ExecuteSQL(sSQL);

                //tinh quy tien mat va khong co hoa don, khi co hoa don lam tren hoa don
                if (soTienTra > 0)
                {
                    decimal tongTienmat = 0;

                    DataTable dttienmat = clsMain.ReturnDataTable("select top(1) TONGTIEN from CN_CONGNO where SUDUNG='True' order by NGAYLAPPHIEU desc");
                    string httt = "";
                    if (dttienmat.Rows.Count > 0)
                    {
                        tongTienmat = decimal.Parse(dttienmat.Rows[0][0].ToString());

                        if (Chuoi_Httt) //01: tien mat (tien mat moi cong vao quy tien mat)
                        {
                            tongTienmat = tongTienmat + soTienTra;
                            httt = "01";
                        }
                        else
                        {
                            httt = "02";
                        }

                    }
                    else
                    {
                        if (Chuoi_Httt) //01: tien mat (tien mat moi cong vao quy tien mat)
                        {
                            tongTienmat = soTienTra;
                            httt = "01";
                        }
                        else
                        {
                            httt = "02";
                        }

                    }
                    string maphieu = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_NS()").Rows[0][0].ToString();
                    string hoten = "";
                    string diachi = "";
                    DataTable dtkh = clsMain.ReturnDataTable("select TEN, DIACHI from NHACUNGCAP where MA ='" + maKH + "'");
                    if (dtkh.Rows.Count > 0)
                    {
                        hoten = dtkh.Rows[0]["TEN"].ToString();
                        diachi = dtkh.Rows[0]["DIACHI"].ToString();
                    }
                    string lydo;
                    if (httt == "01")
                    {
                        lydo = "Thu tiền bán hàng trực tiếp bằng tiền mặt";
                    }
                    else
                    {
                        lydo = "Thu tiền bán hàng trực tiếp bằng chuyển khoản";
                    }
                    string sqltienmat = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}', @TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC='{22}'",
                                                    maphieu, maKH, "", maPhieu, clsUserManagement.ReturnMaNVOfUserLogin(), "01", lydo, soTienTra, "", tongTienmat, hoten, diachi, "", httt, string.Format("{0: yyyyMMdd HH:mm:ss}", KP_UserManagement.clsMain.GetServerDate()), "", "NULL", "", "NULL", "NULL", "", "NULL", "");
                   
                    clsMain.ExecuteSQL(sqltienmat);
                }
            }
        }

        public static void CongNoKhachHangHoaDon(string maKH, string maPhieu, decimal tongTien, decimal soTienTra, DateTime ngayHenTra)
        {
            decimal soTienConLai = tongTien - soTienTra;

            if (clsUserManagement.CheckActive("12") == true)//nếu sử dụng công nợ thì làm
            {

                string sqlchitietthanhtoan = "insert into CHITIETTHANHTOAN (PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN,MA_HOADON)";
                //tinh cong no
                if (soTienConLai == 0)
                {
                    sqlchitietthanhtoan += " values (''," + tongTien + "," + soTienTra + "," + soTienConLai + ",N'0',GETDATE(),'false','true',NULL,NULL,'false','" + maPhieu + "')";
                }
                else
                {
                    sqlchitietthanhtoan += " values (''," + tongTien + "," + soTienTra + "," + soTienConLai + ",'" + 0 + "',GETDATE(),'true','true','" + string.Format("{0:yyyyMMdd}", ngayHenTra) + "',NULL,'false','" + maPhieu + "')";
                }
                clsMain.ExecuteSQL(sqlchitietthanhtoan);
                //Tính tiền Công Nợ
                string sSQL = "";
                sSQL += "Update NHACUNGCAP " + "\n";
                sSQL += "Set TIEN_CONGNO=ISNULL(TIEN_CONGNO,0) + " + soTienConLai + "\n";
                sSQL += " Where MA=" + clsMain.SQLString(maKH);
                clsMain.ExecuteSQL(sSQL);
                //tinh quy tien mat 
                if (soTienTra > 0)
                {
                    decimal tongTienmat = 0;
                    DataTable dttienmat = clsMain.ReturnDataTable("select top(1) TONGTIEN from CN_CONGNO where SUDUNG='True' order by NGAYLAPPHIEU desc");
                    if (dttienmat.Rows.Count > 0)
                    {
                        tongTienmat = decimal.Parse(dttienmat.Rows[0][0].ToString());
                        tongTienmat = tongTienmat + soTienTra;
                    }
                    else
                    {
                        tongTienmat = soTienTra;
                    }
                    string maphieu = clsMain.ReturnDataTable("select dbo.NewCodeCongNo_NS()").Rows[0][0].ToString();

                    string hoten = "";
                    string diachi = "";
                    DataTable dtKH = clsMain.ReturnDataTable("select TEN,DIACHI from NHACUNGCAP where MA='" + maKH + "'");
                    hoten = dtKH.Rows[0]["TEN"].ToString();
                    diachi = dtKH.Rows[0]["DIACHI"].ToString();
                    string lydo = "Thu tiền bán hàng trên hóa đơn";
                    string sqltienmat = string.Format("exec sp_CN_CONGNO_INSERT_NS @MAPCN='{0}',@MADOITUONG='{1}',@MA_HOADON='{2}',@PHIEUKHO_ID='{3}',@MANV='{4}',@MALOAI='{5}',@LYDO=N'{6}',@SOTIEN='{7}',@KEMTHEO=N'{8}', @TONGTIEN='{9}',@HOTEN=N'{10}',@DIACHI=N'{11}',@MANVMUON='{12}',@MATT='{13}',@NGAYTHANHTOAN='{14}',@TENNGANHANGCHUYEN=N'{15}',@SOTAIKHOANCHUYEN={16},@TENNGANHANGNHAN=N'{17}',@SOTAIKHOANNHAN={18},@SOGIAODICH={19},@HOTENGIAODICH=N'{20}',@NGUONCHI={21},@MAPDNDC='{22}'",
                                                        maphieu, maKH, maPhieu, "", clsUserManagement.ReturnMaNVOfUserLogin(), "01", lydo, soTienTra, "", tongTienmat, hoten, diachi, "", "01", string.Format("{0: yyyyMMdd HH:mm:ss}", KP_UserManagement.clsMain.GetServerDate()), "", "NULL", "", "NULL", "NULL", "", "NULL", "");
                    
                    clsMain.ExecuteSQL(sqltienmat);
                }
            }

        }

        public static string ChuyenSo(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];
            string c = doc[0].ToString();

            doc = doc.Remove(0, 1);
            doc = doc.Insert(0, c.ToUpper());
            return doc;
        }

        #endregion

        #region Dai ly

        public static bool CheckDaiLy(string ID)
        {
            string sSQL = "select ID from NHACUNGCAP where SUDUNG=1 and ID =" + clsMain.SQLString(ID);
            DataTable dtkh = clsMain.ReturnDataTable(sSQL);
            if (dtkh.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static DataTable ReturnDaiLy(string maKH)
        {
            DataTable dtkh = new DataTable();
            string sSQL = "";
            sSQL += "Select A.MA,A.TEN,B.CHIETKHAU" + "\n";
            sSQL += "from NHACUNGCAP A,CAPDODAILY B" + "\n";
            sSQL += "where A.CAPDO = B.MA" + "\n";
            sSQL += "and ID= " + clsMain.SQLString(maKH) + "\n";
            dtkh = clsMain.ReturnDataTable(sSQL);
            return dtkh;
        }

        #endregion

        #region Khong thay sai

        public static void ResetControlValue(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    if (c.GetType().ToString() == "DevExpress.XtraEditors.TextEdit")
                    {
                        ((TextEdit)c).Text = "";
                        // break;
                    }
                    else
                        switch (c.GetType().ToString())
                        {
                            case "DevExpress.XtraEditors.DateEdit":
                                ((DateEdit)c).EditValue = clsMain.GetServerDate();
                                break;
                            case "DevExpress.XtraEditors.CheckEdit":
                                ((CheckEdit)c).Checked = true;
                                break;
                            case "DevExpress.XtraEditors.LookupEdit":
                                ((LookUpEdit)(c)).EditValue = ((LookUpEdit)c).Properties.GetDataSourceValue(((LookUpEdit)c).Properties.ValueMember, 0);
                                break;
                            case "DevExpress.XtraEditors.GridLookUpEdit":
                                ((GridLookUpEdit)(c)).EditValue = ((GridLookUpEdit)c).Properties.GetKeyValue(0);
                                break;
                            default:
                                ResetControlValue(c);
                                break;
                        }
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.DateEdit":
                            ((DateEdit)c).EditValue = clsMain.GetServerDate();
                            break;
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        public static void TruCongNoKhachHangHoaDon(string maPhieu, decimal soTienTra)//true là tiền mặt;fale là chuyển khoản
        {
            if (clsUserManagement.CheckActive("12") == true)//nếu có sử dụng công nờ thì trừ
            {
                decimal tongtien = decimal.Parse(clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where MA_HOADON='" + maPhieu + "'   order by CTTT_STT desc").Rows[0][0].ToString());
                decimal soTienConLai = tongtien - soTienTra;
                string sqlFlag = "";
                string sqlChiTietThanhToan = "";
                bool _flag = true;
                string dotthanhtoan = clsMain.ReturnDataTable("select top(1) DOTTHANHTOAN  from CHITIETTHANHTOAN  where MA_HOADON='" + maPhieu + "' order by CTTT_STT desc").Rows[0][0].ToString();
                string dotTT = "";
                if (dotthanhtoan.ToString().Length > 1)
                {
                    dotTT = (int.Parse(dotthanhtoan.ToString().Substring(0, 2).Replace("-", "")) + 1).ToString() + "-Trả Hàng";
                }
                else
                {
                    dotTT = (int.Parse(dotthanhtoan.ToString().Substring(0, 1).Replace("-", "")) + 1).ToString() + "-Trả Hàng";
                }

                if (Math.Round(soTienConLai) == 0)
                {
                    _flag = false;
                }
                sqlFlag = string.Format("update CHITIETTHANHTOAN set FLAG='false' where  MA_HOADON='{0}' and flag='true'", maPhieu);
                sqlChiTietThanhToan = string.Format("insert into CHITIETTHANHTOAN(PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN,MA_HOADON) values('{0}','{1}','{2}','{3}',N'{4}','{5: yyyyMMdd HH:mm:ss}','{6}','{7}',", "", tongtien, soTienTra, soTienConLai, dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "true");
                if (Math.Round(soTienConLai) <= 0)
                {
                    sqlChiTietThanhToan += "NULL,null,'false','" + maPhieu + "')";
                }
                else
                {
                    string sqlngayhentra = "select top(1) NGAYHENTRA from CHITIETTHANHTOAN where MA_HOADON ='" + maPhieu + "' order by CTTT_STT desc";
                    string ngayhentra = string.Format("{0:yyyyMMdd}", clsMain.ReturnDataTable(sqlngayhentra).Rows[0][0]);
                    sqlChiTietThanhToan += "'" + ngayhentra + "',null,'false','" + maPhieu + "')";
                }
                if (sqlFlag != "")
                    clsMain.ReturnDataTable(sqlFlag);
                clsMain.ExecuteSQL(sqlChiTietThanhToan);
            }
        }

        public static void TruCongNoKhachHangPhieuXuat(string maPhieu, decimal soTienTra)//true là tiền mặt;fale là chuyển khoản
        {
            decimal tongTien = decimal.Parse(clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maPhieu + "'   order by CTTT_STT desc").Rows[0][0].ToString());
            decimal soTienConLai = tongTien - soTienTra;
            if (clsUserManagement.CheckActive("12") == true)//nếu có sử dụng công nờ thì trừ
            {
                string sqlFlag = "";
                string sqlChiTietThanhToan = "";
                bool _flag = true;
                string dotthanhtoan = clsMain.ReturnDataTable("select top(1) DOTTHANHTOAN  from CHITIETTHANHTOAN  where PK_PHIEUKHOID='" + maPhieu + "' order by CTTT_STT desc").Rows[0][0].ToString();
                string dotTT = "";
                if (dotthanhtoan.ToString().Length > 1)
                {
                    dotTT = (int.Parse(dotthanhtoan.ToString().Substring(0, 2).Replace("-", "")) + 1).ToString() + "-Trả Hàng";
                }
                else
                {
                    dotTT = (int.Parse(dotthanhtoan.ToString().Substring(0, 1).Replace("-", "")) + 1).ToString() + "-Trả Hàng";
                }

                decimal tongtien = decimal.Parse(clsMain.ReturnDataTable("select top(1) SOTIENCONLAI from CHITIETTHANHTOAN where PK_PHIEUKHOID='" + maPhieu + "'      order by CTTT_STT desc").Rows[0][0].ToString());

                if (Math.Round(soTienConLai) == 0)
                {
                    _flag = false;
                }
                sqlFlag = string.Format("update CHITIETTHANHTOAN set FLAG='false' where  PK_PHIEUKHOID='{0}' and flag='true'", maPhieu);
                sqlChiTietThanhToan = string.Format("insert into CHITIETTHANHTOAN(PK_PHIEUKHOID,TONGTIENCANTT,SOTIENTHANHTOAN,SOTIENCONLAI,DOTTHANHTOAN,NGAYTHANHTOAN,FLAG,THU_CHI,NGAYHENTRA,NGAYTREHEN,ISTREHEN) values('{0}','{1}','{2}','{3}',N'{4}','{5: yyyyMMdd HH:mm:ss}','{6}','{7}',", maPhieu, tongtien, soTienTra, soTienConLai, dotTT, KP_UserManagement.clsMain.GetServerDate(), _flag, "true");
                if (Math.Round(soTienConLai) <= 0)
                {
                    sqlChiTietThanhToan += "NULL,null,'false')";
                }
                else
                {
                    string sqlngayhentra = "select top(1) NGAYHENTRA from CHITIETTHANHTOAN where PK_PHIEUKHOID ='" + maPhieu + "' order by CTTT_STT desc";
                    string ngayhentra = string.Format("{0:yyyyMMdd}", clsMain.ReturnDataTable(sqlngayhentra).Rows[0][0]);
                    sqlChiTietThanhToan += "'" + ngayhentra + "',null,'false')";
                }
                if (sqlFlag != "")
                    clsMain.ReturnDataTable(sqlFlag);
                clsMain.ExecuteSQL(sqlChiTietThanhToan);
            }
        }

        #endregion
    
    }
}
