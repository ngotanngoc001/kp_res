﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KP_UserManagement;
using DevExpress.XtraEditors;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace KP_RES
{
    public class cls_ConfigCashier
    {
        /// <summary>
        /// id Shop
        /// </summary>
        public static string idShop = "";
        /// <summary>
        /// Name Shop
        /// </summary>
        public static String nameShop = "";
        ///<summary>
        ///id Cashier 
        ///
        ///</summary>
        public static string idCashier = "";
        ///<summary>
        ///Name Cashier 
        ///
        ///</summary>
        public static string nameCashier = ""; 
        ///<summary>
        ///id warehouse
        ///</summary>
        public static string idWarehouse = "";
        ///<summary>
        ///name warehouse
        ///</summary>
        public static string nameWarehouse = "";
        ///<summary>
        ///id sector
        ///
        ///</summary>
        public static string idSector = "";
        /// <summary>
        /// id member 
        /// </summary>
        public static string idmember = "";
        /// <summary>
        /// Ten ca ban
        /// </summary>
        public static string sTenCB = "";
        /// <summary>
        /// Gio bat dau ca ban
        /// </summary>
        public static string sGioBD = "";
        /// <summary>
        /// Gio ket thuc ca ban
        /// </summary>
        public static string sGioKT = "";
        /// <summary>
        /// Check full ca ban
        /// </summary>
        public static string scheckCaban = "";
        /// <summary>
        /// Chuyen ban co in xuong bep
        /// </summary>
        public static string sInbep_Chuyenban = "0";
        /// <summary>
        /// Có dùng mật khẩu bán hàng hay không
        /// </summary>
        public static string sDungmatkhaubanhang = "0";
        /// <summary>
        /// Có in mã vạch khi bán hàng hay không
        /// </summary>
        public static string sIn_Mavach_Fastfood = "0";
        /// <summary>
        /// Có in số number khi bán hàng hay không
        /// </summary>
        public static string sIn_Number_Fastfood = "0";
        /// <summary>
        /// Chi mã vạch khi bán hàng có giao hàng
        /// </summary>
        public static string sIn_Mavach_Fastfood_Giaohang = "0";
        /// <summary>
        /// Load giá 2 khi bán hàng có giao hàng
        /// </summary>
        public static string sGiaban2_Giaohang = "0";
        /// <summary>
        /// In tạm tính ngay không cần hiện form in tam tinh nữa
        /// </summary>
        public static string sShow_Intamtinh = "0";
        /// <summary>
        /// Auto hiện form ghi chú mà ko cần bấm nút nữa
        /// </summary>
        public static string sShow_Ghichu = "0";
        /// <summary>
        /// cấu hình thời gian ngồi free buffet
        /// </summary>
        public static string sCauHinhThoiGian = "0";
        /// <summary>
        /// auto day bill ve cho huy viet nam
        /// </summary>
        public static string sAutodaybill = "0";
        /// <summary>
        /// auto day bill ve cho huy viet nam
        /// </summary>
        public static string sInbilltong = "0";
        /// Có dùng mật khẩu IN LẠI HÓA ĐƠN
        /// </summary>
        public static string sINLAIHOADON = "0";
        /// Có dùng mật khẩu LẤY LẠI HÓA ĐƠN
        /// </summary>
        public static string sLAYLAIHOADON = "0";
        /// Có dùng mật khẩu HỦY BÀN
        /// </summary>
        public static string sHUYBAN = "0";
        /// Có dùng mật khẩu HỦY MÓN
        /// </summary>
        public static string sHUYMON = "0";
        /// Có dùng mật khẩu MỞ KÉT
        /// </summary>
        public static string sMOKET = "0";
        /// Có dùng mật khẩu SỬA GIÁ
        /// </summary>
        public static string sSUAGIA = "0";
        /// Có dùng mật khẩu CHỌN GIÁ
        /// </summary>
        public static string sCHONGIA = "0";
        /// Có dùng mật khẩu BỎ KHÓA BÀN
        /// </summary>
        public static string sBOKHOABAN = "0";
        /// Có dùng mật khẩu CHIẾT KHẤU
        /// </summary>
        public static string sCHIETKHAU = "0";
        /// Có dùng mật khẩu KẾT CA
        /// </summary>
        public static string sKETCA = "0";
        /// Có dùng mật khẩu BÁN GIÁ 2
        /// </summary>
        public static string sBANGIA2 = "0";
        /// <summary>
        /// ID cua hang huy viet nam 202: Foodhall,203: IKI,204: Shilla
        /// </summary>
        public static string sStore_ID = "0";
        /// <summary>
        /// SOLUONGKHACH có lưu thông tin số lượng khách hay không
        /// </summary>
        public static string sSOLUONGKHACH = "0";
        /// <summary>
        /// HUYMONNHANH có cho huy mon nhanh hay ko
        /// </summary>
        public static string sHUYMONNHANH = "0";
        /// <summary>
        /// KHOANHAPTHE có cho huy mon nhanh hay ko
        /// </summary>
        public static string sKHOANHAPTHE = "0";
        /// <summary>
        /// ID menu  cua hang huy viet nam 202: Foodhall,203: IKI,204: Shilla
        /// </summary>
        public static string sMenuID = "0";
        /// <summary>
        /// Check full ca ban
        /// </summary>
        public static bool checkCaban()
        {
            bool  bResult = false;
            if (scheckCaban == "")
            {
                try
                {
                    scheckCaban = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='CAUHINHCABAN'").Rows[0][0].ToString();
                }
                catch
                {
                }
            }
            bResult = scheckCaban == "1" ? true : false;
            return bResult;
        }
        /// <summary>
        /// kiểm tra có đủ thông tin trước khi vô bán hàng
        /// </summary>
        public static bool checkBanHang()
        {
            //check cấu hình quầy
            if (cls_ConfigCashier.idShop == "")
            {
                LoadCauHinh();
                if (cls_ConfigCashier.idShop == "")
                {
                    XtraMessageBox.Show("Quầy bán chưa được cấu hình vui lòng liên hệ với admin !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            //Check cửa hàng , kho , quầy
            string sql = "";
            sql += "select a.ten_cuahang,b.ten_kho,c.ten_quay" + "\n";
            sql += "from cuahang a, kho b, quay c" + "\n";
            sql += "where a.ma_cuahang=b.ma_cuahang" + "\n";
            sql += "and b.ma_kho=c.ma_kho" + "\n";
            sql += "and a.ma_cuahang=" + clsMain.SQLString(cls_ConfigCashier.idShop) + "\n";
            sql += "and b.ma_kho=" + clsMain.SQLString(cls_ConfigCashier.idWarehouse) + "\n";
            sql += "and c.ma_quay=" + clsMain.SQLString(cls_ConfigCashier.idCashier) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sql);
            if (dt.Rows.Count <= 0)
            {
                XtraMessageBox.Show("Cấu hình quầy không đúng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false ;
            }
            //Check ca bán
            if (checkCaban())
            {
                sql = "";
                sql += "select cb.TEN_CABAN, Convert(varchar(8),cb.GIOBATDAU,108) as GIOBATDAU,  Convert(varchar(8),cb.GIOKETTHU,108) as GIOKETTHU  from CABAN cb inner join THIETLAPCABAN tlcb  on cb.MA_CABAN = tlcb.MA_CABAN inner join QUAY q on tlcb.MA_QUAY= q.MA_QUAY";
                sql += " where tlcb.MA_NHANVIEN = '" + clsGlobal.gsUserID + "' and ( Convert(varchar(8),cb.GIOBATDAU,108) <= Convert(varchar(8),GETDATE(),108) and Convert(varchar(8),GETDATE(),108) < Convert(varchar(8),cb.GIOKETTHU,108) ) and q.MA_QUAY =" + int.Parse(cls_ConfigCashier.idCashier) + " Order by cb.TEN_CABAN ";
            }
            else
            {
                sql = "";
                sql += "select cb.TEN_CABAN, Convert(varchar(8),cb.GIOBATDAU,108) as GIOBATDAU,  Convert(varchar(8),cb.GIOKETTHU,108) as GIOKETTHU  from CABAN cb inner join THIETLAPCABAN tlcb  on cb.MA_CABAN = tlcb.MA_CABAN inner join QUAY q on tlcb.MA_QUAY= q.MA_QUAY";
                sql += " where tlcb.MA_NHANVIEN is not null and ( Convert(varchar(8),cb.GIOBATDAU,108) <= Convert(varchar(8),GETDATE(),108) and Convert(varchar(8),GETDATE(),108) < Convert(varchar(8),cb.GIOKETTHU,108) ) and q.MA_QUAY is not null" + " Order by cb.TEN_CABAN ";
            }
            dt = clsMain.ReturnDataTable(sql);
            if (dt.Rows.Count <= 0)
            {
                XtraMessageBox.Show("Bạn không có quyền bán hàng ca này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false ;
            }
            else
            {
                cls_ConfigCashier.sTenCB = dt.Rows[0]["TEN_CABAN"].ToString();
                cls_ConfigCashier.sGioBD = dt.Rows[0]["GIOBATDAU"].ToString();
                cls_ConfigCashier.sGioKT = dt.Rows[0]["GIOKETTHU"].ToString();
            }
            return true ;
        }
        /// <summary>
        /// Lay thong tin cau hinh ban hang
        /// </summary>
        public static void LoadCauHinh()
        {
            try
            {
                if (File.Exists(Application.StartupPath + "/systeminfo.txt"))
                {
                    StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
                    string line = str.ReadLine();
                    string[] value = line.Split(',');
                    cls_ConfigCashier.idShop = value[0].ToString();
                    cls_ConfigCashier.nameShop = value[1].ToString();
                    cls_ConfigCashier.idCashier = value[2].ToString();
                    cls_ConfigCashier.nameCashier = value[3].ToString();
                    cls_ConfigCashier.idWarehouse = value[4].ToString();
                    cls_ConfigCashier.nameWarehouse = value[5].ToString();
                }
                try
                {
                    //08/07/2016 them lay thong tin co in bep khi chuyen ban
                    sInbep_Chuyenban = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INBEP_CHUYENBAN'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //20/12/2016 them lay thong tin co dung mat khau ban hang hay khong
                    sDungmatkhaubanhang = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='DUNGMATKHAUBANHANG'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //31/08/2017 them lay thong tin co in ma vach hang khi ban fastfood hay khong
                    sIn_Mavach_Fastfood = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INMAVACHFASTFOOD'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //31/08/2017 them lay thong tin co in ma vach hang khi ban fastfood hay khong
                    sIn_Number_Fastfood = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INNUMBERFASTFOOD'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //14/06/2018 cau hinh chi in ma vach khi có giao hàng 
                    sIn_Mavach_Fastfood_Giaohang = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INMAVACHFASTFOOD_GIAOHANG'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //14/06/2018 cau hinh chi in ma vach khi có giao hàng 
                    sGiaban2_Giaohang = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='GIABAN2_GIAOHANG'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //21/06/2018 show from in tạm tính hay không
                    sShow_Intamtinh = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='SHOW_INTAMTINH'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //05/07/2018 auto show from ghi chú hay không
                    sShow_Ghichu = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='LUONHIENGHICHU'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //13/06/2018 them lay thong tin co cau hinh thoi gian hay khong
                    sCauHinhThoiGian = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='CAUHINHTHOIGIAN'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //31/07/2018 them lay thong tin auto day bill 
                    sAutodaybill = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='AUTODAYBILL'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //23/08/2018 them lay thong tin in bill tong khi order 
                    sInbilltong = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='INBILLTONG'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU IN LẠI HÓA ĐƠN
                    sINLAIHOADON = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_INLAIHD'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU LẤY LẠI HÓA ĐƠN
                    sLAYLAIHOADON = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_LAYLAIHD'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU HỦY BÀN
                    sHUYBAN = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_HUYBAN'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU HỦY MÓN
                    sHUYMON = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_HUYMON'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU MỞ KÉT
                    sMOKET = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_MOKET'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU SỬA GIÁ
                    sSUAGIA = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_SUAGIA'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU CHỌN GIÁ
                    sCHONGIA = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_CHONGIA'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU BỎ KHÓA BÀN
                    sBOKHOABAN = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_BOKHOABAN'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU CHIẾT KHẤU
                    sCHIETKHAU = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_CHIETKHAU'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU KẾT CA
                    sKETCA = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_KETCA'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //29/08/2018 them lay thong tin co dung MẬT KHẨU BÁN GIÁ 2
                    sBANGIA2 = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MK_BANGIA2'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //12/09/2018 them lay thong tin Store_ID cho API huy viet nam
                    sStore_ID = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='Store_ID'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //22/09/2018 them lay thong tin so luong khach
                    sSOLUONGKHACH = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='SOLUONGKHACH'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //27/09/2018 them lay thong tin huy mon nhanh
                    sHUYMONNHANH = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='HUYMONNHANH'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //03/10/2018 khóa nhập tay thẻ KHTT, TTNB
                    sKHOANHAPTHE = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='KHOANHAPTHE'").Rows[0][0].ToString();
                }
                catch
                {
                }
                try
                {
                    //15/10/2018 menu id của cua hang huy vit nam
                    sMenuID = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='MenuID'").Rows[0][0].ToString();
                }
                catch
                {
                }
            }
            catch
            {
            }
        }
    } 
}