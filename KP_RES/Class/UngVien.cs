﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KP_RES.Class
{
    public class UngVien
    {
        public int pMa { get; set; }
        public String pTen { get; set; }
        public Byte[] pImage { get; set; }
        public DateTime pNgaySinh { get; set; }
        public String pGioiTinh { get; set; }
        public String pDiaChi { get; set; }
        public String pDienThoai { get; set; }
        public String pEmail { get; set; }
        public String pHocVan { get; set; }
        public String pNgoaiNgu { get; set; }
        public String pTinHoc { get; set; }
        public String pKinhNghiem { get; set; }
        public String pKyNang { get; set; }
        public Byte[] pFileCV { get; set; }
        public String pTenFileCV { get; set; }

        public UngVien()
        {
            pMa = 0;
            pTen = "";
            pImage = new Byte[1];
            pNgaySinh = DateTime.Now;
            pGioiTinh = "1";
            pDiaChi = "";
            pDienThoai = "";
            pEmail = "";
            pHocVan = "";
            pNgoaiNgu = "";
            pTinHoc = "";
            pKinhNghiem = "";
            pKyNang = "";
            pFileCV = new Byte[1];
            pTenFileCV = "";
        }

    }
}
