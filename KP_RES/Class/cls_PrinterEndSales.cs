﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KP_RES
{
    class cls_PrinterEndSales
    {
            public string @maCuaHang { get; set; }
            public string @tenCuaHang { get; set; }
            public string @maNV { get; set; }
            public string @tenNV { get; set; }
            public string @maCaBan { get; set; }
            public string @tenCaBan { get; set; }
            public string @maQuay { get; set; }
            public string @tenQuay { get; set; }
            public double @giamgia { get; set; } 
            public double @tongTienTra { get; set; }
            public double @tongTienBan { get; set; }
            public double @tongTien { get; set; }
            public double @phuthu { get; set; }
            public double @tiengio { get; set; }
            public double @tienmat { get; set; }
            public double @tienthe { get; set; }
            public double @voucher { get; set; }
            public double @TTNB { get; set; }
            public double @TienTamUng { get; set; }
    }
}
