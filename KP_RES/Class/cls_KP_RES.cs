﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;
using System.Drawing;
using KP_UserManagement;
using System.Collections;
using DevExpress.Skins;
using DevExpress.XtraGrid.Views.Grid;

namespace KP_RES
{
    /// <summary>
    /// Lớp dùng chung cho toàn bộ project chỉ tạo biến hay hàm trong lớp này
    /// </summary>
    public class cls_KP_RES
    {
        #region "Biến toàn cục" 
        public static bool Fcandoi = false;//True:Nhập-xuất cân đối thành công, False:Nhập-xuất cân đối thành công
        public static bool bSaveLayout = true ;//True:Save , False: không Save
        public static bool bFormSodoghe = false ;//True:Hiện , False: Ẩn
        public static bool bFormQuangCao = false;//Tự tắt form quảng cáo
        public static int iFormSodoghe = 0;//Tự tắt from sơ đồ ghế
        //public static string sMaLichChieu = "";//Mã lịch chiếu cho form load so đồ ghế tự động
        public static DataTable dtDanhSachVe;//Danh sách vé cho form load so đồ ghế tự động
        public static DataTable dtSodoghe;//Danh sách vé cho form load so đồ ghế tự động
        public static bool bThanhtoan = false;//True:Ngừng load ghế , ds vé, False:Load ghế , DS vé tự động
        public static DataTable dtLoaiGhe;//Danh sách loại ghế cho màn hình phía sau
        public static string sMaThoiGian = "";//Mã thời gian lưu chung để khỏi phải truy xuất nhiều lần
        public static Bitmap  Anhnen ;//Anh nền man hinh sau
        public static int iThoigianngungbanve = -1;//Thời gian ngừng bán vé
        public static bool  bAutoSkin = true ;//Giao diện tự động
        public static string sTenRap = "";//Rạp - tăng tốc màn hình 2
        public static string sThongtinxuatchieu = "";//Thongtinxuatchieu - tăng tốc màn hình 2
        public static string sThongtinxuatchieu1 = "";//Thongtinxuatchieu1 - tăng tốc màn hình 2
        //public static string sLichchieu = "";//sLichchieu - tăng tốc màn hình 2
        public static string sTenPhong = "";//sMaRap - tăng tốc màn hình 2
        public static int Mode = 0;//Mode chạy chương trình theo khách hàng xem them ở program.cs
        public static List<string > sTT_DISPLAY_ORDER =new List<string>();
        public static int iHinhanh = 0;//Mode có hiển thị hình ảnh cho sơ đồ bàn . danh sách món hay ko

        public static int iChietkhau_Manhinhhai = 0;//Hiển thị chiết khấu màn hình 2
        #endregion

       public static string LoadSkin()
       {
            string skin = "DevExpress Style";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            if (File.Exists(Application.StartupPath + "\\Skin.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\Skin.xml");
                    dt = ds.Tables[0];
                    skin = dt.Rows[0]["Skin"].ToString();
                    cls_KP_RES.bAutoSkin = bool.Parse(dt.Rows[0]["Auto"].ToString());
                    if (cls_KP_RES.bAutoSkin)
                    {
                        ArrayList ListSkin = new ArrayList();
                        foreach (SkinContainer cnt in SkinManager.Default.Skins)
                        {
                            ListSkin.Add(cnt.SkinName);
                        }
                        var r = new Random();
                        int i = r.Next(ListSkin.Count-1);
                        skin = ListSkin[i].ToString();
                    }
                }
                catch
                {
                }
            }
            return skin;
        }

       public static void SaveSkin(string SkinName)
       {
           String Skin = SkinName;
           DataSet ds = new DataSet();
           DataTable dt = new DataTable();
           try
           {
               dt.Columns.Add("Skin");
               dt.Columns.Add("Auto");
               dt.Rows.Add(Skin, cls_KP_RES.bAutoSkin.ToString());
               ds.Tables.Add(dt);
               if (File.Exists(Application.StartupPath + "\\Skin.xml"))
               {
                   FileInfo file = new FileInfo(Application.StartupPath + "\\Skin.xml");
                   file.Attributes = FileAttributes.Normal;
                   ds.WriteXml(Application.StartupPath + "\\Skin.xml");
                   file.Attributes = FileAttributes.Hidden | FileAttributes.System;
               }
               else
               {
                   ds.WriteXml(Application.StartupPath + "\\Skin.xml");
                   FileInfo file = new FileInfo(Application.StartupPath + "\\Skin.xml");
                   file.Attributes = FileAttributes.Hidden | FileAttributes.System;
               }
           }
           catch (Exception ex)
           {
               DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
           }
       }

       public static void LoadLayout(TileControl tilCon)
       {
           if (File.Exists(Application.StartupPath + "\\Layout.xml"))
           {
               try
               {
                   tilCon.RestoreLayoutFromXml(Application.StartupPath + "\\Layout.xml");

               }
               catch
               {
               }
           }
       }

       public static void SaveLayout(TileControl tilCon)
       {
           if (cls_KP_RES.bSaveLayout)
           {
               try
               {
                   if (File.Exists(Application.StartupPath + "\\Layout.xml"))
                   {
                       FileInfo file = new FileInfo(Application.StartupPath + "\\Layout.xml");
                       file.Attributes = FileAttributes.Normal;
                       tilCon.SaveLayoutToXml(Application.StartupPath + "\\Layout.xml");
                       file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                   }
                   else
                   {
                       tilCon.SaveLayoutToXml(Application.StartupPath + "\\Layout.xml");
                       FileInfo file = new FileInfo(Application.StartupPath + "\\Layout.xml");
                       file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                   }
               }
               catch (Exception ex)
               {
                   DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
               }
           }
       }

       public static bool isValidEmail(string inputEmail)
       {
           string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                               @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                               @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
           Regex re = new Regex(strRegex);
           if (re.IsMatch(inputEmail))
               return (true);
           else
               return (false);
       }

       public static void RemoveTileItem(TileItem ti)
       {
           if (!ti.Checked)
           {
               ti.Group.Items.Remove(ti);
           }
       }

       public static void LoadDefaultLayout()
       {
           cls_KP_RES.bSaveLayout = false;
           if (File.Exists(Application.StartupPath + "\\Layout.xml"))
           {
               try
               {
                   File.Delete(Application.StartupPath + "\\Layout.xml");
               }
               catch
               {
               }
           }
       }

       public static Color ConvertStringToColor(string str)
       {
           ColorConverter clc = new ColorConverter();
           Color cl;
           try
           {
               cl = (Color)clc.ConvertFromString(str);
           }
           catch
           {
               try
               {
                   cl = (Color)clc.ConvertFrom("#" + str);
               }
               catch
               {
                   cl = (Color)clc.ConvertFromInvariantString(str);
               }
           }
           return cl;
       }

       public static string GetThuofDateTime(DateTime dti)
       {
           if (dti.DayOfWeek == DayOfWeek.Monday)
           {
               return "Thứ hai";
           }
           else if (dti.DayOfWeek == DayOfWeek.Tuesday)
           {
               return "Thứ ba";
           }
           else if (dti.DayOfWeek == DayOfWeek.Wednesday)
           {
               return "Thứ tư";
           }
           else if (dti.DayOfWeek == DayOfWeek.Thursday)
           {
               return "Thứ năm";
           }
           else if (dti.DayOfWeek == DayOfWeek.Friday)
           {
               return "Thứ sáu";
           }
           else if (dti.DayOfWeek == DayOfWeek.Saturday)
           {
               return "Thứ bảy";
           }
           else
           {
               return "Chủ nhật";
           }
       }

       public static void SetBackgroudOnClick(SimpleButton _btn)
       {
           _btn.Appearance.BackColor = Color.YellowGreen;
           _btn.Appearance.BackColor2 = Color.YellowGreen;
           if (_btn.Appearance.Options.UseBackColor == false)
               _btn.Appearance.Options.UseBackColor = true;
           _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
           if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
               _btn.LookAndFeel.UseDefaultLookAndFeel = false;
       }

       public static  void SetDefautlBackgroud(SimpleButton _btn)
       {
           _btn.Appearance.BackColor = Color.White;
           if (_btn.Appearance.Options.UseBackColor == true)
               _btn.Appearance.Options.UseBackColor = false;
           _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
           if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
               _btn.LookAndFeel.UseDefaultLookAndFeel = true;
       }

       public  static Image resizeImage(Image imgToResize, Size size)
       {
           return (Image)(new Bitmap(imgToResize, size));
       }

       public static string CreateBarcode(string sTableName)
       {
           try
           {
               int iMaBarCode=0;
               string MaBarCode = "";
               string sSQL1 = "";
               sSQL1 += "EXECUTE SP_CreateBarcode " + clsMain.SQLString(sTableName) + "\n";
               try
               {
                   iMaBarCode = int.Parse(clsMain.ReturnDataTable(sSQL1).Rows[0][0].ToString());
               }
               catch
               {
               }
               iMaBarCode = iMaBarCode + 1;
               MaBarCode = string.Format("{0:0000000}", iMaBarCode);
               return MaBarCode;
           }
           catch (Exception ex)
           {
               DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               return null;
           }
       }

       public static string CreateBarcode_Final(string sTableName)
       {
           try
           {
               int newbarcode = 0;
               int iMaBarCode = 1000000;
               string MaBarCode = "";
               string sSQL1 = "";
               //07/10/2015 Sua trung ma barcode ve 
               do
               {
                   Random random = new Random();
                   int randomNumber = random.Next(10, 1000);
                   System.Threading.Thread.Sleep(randomNumber);
                   sSQL1 = "EXECUTE SP_CreateBarcode_Final " + clsMain.SQLString(sTableName) + "\n";
                   try
                   {
                       iMaBarCode = int.Parse(clsMain.ReturnDataTable(sSQL1).Rows[0][0].ToString());
                   }
                   catch
                   {
                   }
                   MaBarCode = string.Format("{0:0000000}", iMaBarCode);
                   newbarcode = 0;
                   sSQL1 = "Select MABARCODE" + "\n";
                   if (sTableName == "VE")
                   {
                       sSQL1 += "From VETONGHOP " + "\n";
                   }
                   else
                   {
                       sSQL1 += "From " + sTableName + "\n";
                   }
                   sSQL1 += "Where MABARCODE=" + clsMain.SQLString(MaBarCode) + "\n";
                   if (clsMain.ReturnDataTable(sSQL1).Rows.Count > 0)
                   {
                       newbarcode = 1;
                   }
               } while (newbarcode == 1);

               return MaBarCode;
           }
           catch (Exception ex)
           {
               DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               return null;
           }
       }

       public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
       {
           MemoryStream ms = new MemoryStream();
           imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
           return ms.ToArray();
       }

       public static bool  CheckScreen()//Kiem tra may co 2 man hinh . True : co 2 man hinh . False : ko co 2 man hinh
       {
           Screen[] sc = Screen.AllScreens;
           if (sc.Length > 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       public static string DateToString_Date(DateTime  dti)
       {
           string sResult = "";
           sResult = dti.Day .ToString ("00")+"/"+dti.Month .ToString ("00")+"/"+dti.Year .ToString ("0000");
           return sResult;
       }

       public static string DateToString_Time(DateTime dti)
       {
           string sResult = "";
           sResult = dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00") + ":" + dti.Second.ToString("00");
           return sResult;
       }

       public static string DateToString_Full(DateTime dti)
       {
           string sResult = "";
           sResult = dti.Day.ToString("00") + "/" + dti.Month.ToString("00") + "/" + dti.Year.ToString("0000") + " " + dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00") + ":" + dti.Second.ToString("00");
           return sResult;
       }

       public static string DateToString_Full_NoSecond(DateTime dti)
       {
           string sResult = "";
           sResult = dti.Day.ToString("00") + "/" + dti.Month.ToString("00") + "/" + dti.Year.ToString("0000") + " " + dti.Hour.ToString("00") + ":" + dti.Minute.ToString("00") ;
           return sResult;
       }

       public static bool ContainColumn(string columnName, DataTable table)
       {
           DataColumnCollection columns = table.Columns;

           if (columns.Contains(columnName))
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       public static DataView GetFilteredData(GridView view)
       {
           if (view.ActiveFilter == null || view.ActiveFilter.Expression == string.Empty)
               return view.DataSource as DataView;

           DataView filter = view.DataSource as DataView;
           filter.RowFilter = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetDataSetWhere(view.ActiveFilterCriteria);
           return filter;
       }

       public static void  LoadBangCauHinh()
       {
           DataTable dt = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='SUDUNGHINHANH'");
           if (dt.Rows.Count > 0)
           {
               cls_KP_RES.iHinhanh = int.Parse (dt.Rows[0][0].ToString());
           }
       }
    }
}
