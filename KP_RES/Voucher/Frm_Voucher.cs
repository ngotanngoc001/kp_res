﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Diagnostics;

namespace KP_RES 
{
    public partial class Frm_Voucher : DevExpress.XtraEditors.XtraForm
    {
        String sMa = "";
        String sVoucher = "";
        Boolean sStatus = false;

        public Frm_Voucher()
        {
            InitializeComponent();
        }

        private void LoadCombo()
        {
            DataTable myDT = clsMain.ReturnDataTable("EXEC SP_ComBoVoucherStyle");
            cboLoai.Properties.DataSource = myDT;
            cboLoai.EditValue = cboLoai.Properties.GetDataSourceValue(cboLoai.Properties.ValueMember, 0);
        }

        private void Frm_Voucher_Paint(object sender, PaintEventArgs e)
        {
            chkReadOnly();
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            chkReadOnly();
            txtMa.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (sStatus == true)
            {
                MessageInformation("Voucher này đã sử dụng không được xóa Voucher này", "Thông báo");
                return;
            }

            if (MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            String sSQL = "";

            if (MessageQuestion("Chọn Yes để xóa tất cả Voucher cùng đợt phát hành. Chọn No xóa Voucher bạn đã chọn ?", "Thông báo") == DialogResult.Yes)
            {
                sSQL = "EXEC SP_DeleteVoucherAll " + clsMain.SQLString(sVoucher);
            }
            else
            {
                if (sStatus == false)
                    sSQL = "EXEC SP_DeleteVoucher " + clsMain.SQLString(sMa);
            }

            this.Cursor = Cursors.WaitCursor;
            clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            LoaddataGridView();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;
            if (sStatus == true)
            {
                MessageInformation("Voucher này đã sử dụng không được sửa thông tin", "Thông báo");
                return;
            }

            KhoaMoControl(false);
            txtMa.Properties.ReadOnly = true;
            rdSoLe.Properties.ReadOnly = true;
            rdSoLe.SelectedIndex = 0;
            chkReadOnly();
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            String sSQL = "";

            int Percent = 0;
            int Amount = 0;
            if (chk_PhanTram.Checked == true)
                Percent = int.Parse(txtGiaTri.Text);
            else
                Amount = int.Parse(txtGiaTri.Text);

            if (sMa == "" && btnThem.Enabled)
            {
                if (rdSoLe.SelectedIndex == 2)
                {
                    string[] ListVoucher = txtListvoucher.Text .Split(',');
                    
                    sSQL = "SELECT IDENT_CURRENT('VOUCHER') AS MA";
                    DataTable myDT = clsMain.ReturnDataTable(sSQL);
                    int Voucher = 1;
                    if (myDT.Rows[0]["MA"].ToString() != "1")
                        Voucher = int.Parse(myDT.Rows[0]["MA"].ToString()) + 1;
                   
                    sSQL = "";
                    for (int i = 0; i < ListVoucher.Length; i++)
                    {
                        if (ListVoucher[i].ToString() == "")
                        {
                            continue;
                        }
                        sSQL += "EXEC SP_InsertVoucherDetail ";
                        sSQL += clsMain.SQLString(ListVoucher [i].ToString ()) + ",";
                        sSQL += clsMain.SQLString(Voucher.ToString()) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                        sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                        sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                        sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                        sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                        sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                        sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                        sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
                    }

                    sSQL += "EXEC SP_InsertVoucher ";
                    sSQL += clsMain.SQLString(dtpNgayPhatHanh.Text) + ",";
                    sSQL += clsMain.SQLString("1") + ",";
                    sSQL += clsMain.SQLString(txtMa.Text) + ",";
                    sSQL += clsMain.SQLString(txtMa.Text) + ",";
                    sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
 
                }
                else if (rdSoLe.SelectedIndex == 1)
                {
                    sSQL = "SELECT IDENT_CURRENT('VOUCHER') AS MA";
                    DataTable myDT = clsMain.ReturnDataTable(sSQL);
                    int Voucher = 1;
                    if (myDT.Rows[0]["MA"].ToString() != "1")
                        Voucher = int.Parse(myDT.Rows[0]["MA"].ToString()) + 1;


                    String sWord = txtKiTu.Text;
                    String sNumeric = txtSo.Text;
                    int Flat = 0;
                    int sLenght = sNumeric.Length;
                    sSQL = "";
                    List<string> listID = new List<string>();
                    while (Flat < int.Parse(txtSoVoucher.Text))
                    {
                        if (sNumeric.Length > sLenght)
                            sLenght = sNumeric.Length;
                        sNumeric = String.Format("{0:0000000000000000000000000000000000000}", int.Parse(sNumeric));
                        sNumeric = sNumeric.Substring(sNumeric.Length - sLenght);

                        String ID = sWord + sNumeric;

                        if (chkSS_Ngaunhien.Checked)
                        {
                            string ID_NEW = sWord + new Random().Next(0, 999999999).ToString("000000000");
                            if (listID.Count == 0)
                            {
                                listID.Add(ID_NEW);
                            }
                            else
                            {
                                while (listID.Contains(ID_NEW))
                                {
                                    ID_NEW = sWord + new Random().Next(0, 999999999).ToString("000000000");
                                }
                                listID.Add(ID_NEW);
                            }
                            ID = ID_NEW;
                        }

                        if (clsMain.ReturnDataTable("EXEC SP_CheckVoucher " + clsMain.SQLString(ID)).Rows.Count == 0)
                        {
                            sSQL += "EXEC SP_InsertVoucherDetail ";
                            sSQL += clsMain.SQLString(ID) + ",";
                            sSQL += clsMain.SQLString(Voucher.ToString()) + ",";
                            sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                            sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                            sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                            sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                            sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                            sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                            sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                            sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                            sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";

                            Flat++;
                        }
                        sNumeric = (int.Parse(sNumeric) + 1).ToString();
                    }

                    sNumeric = (int.Parse(sNumeric) - 1).ToString();
                    if (sNumeric.Length > sLenght)
                        sLenght = sNumeric.Length;
                    sNumeric = String.Format("{0:0000000000000000000000000000000000000}", int.Parse(sNumeric));
                    sNumeric = sNumeric.Substring(sNumeric.Length - sLenght);


                    sSQL += "EXEC SP_InsertVoucher ";
                    sSQL += clsMain.SQLString(dtpNgayPhatHanh.Text) + ",";
                    sSQL += clsMain.SQLString(txtSoVoucher.Text) + ",";
                    sSQL += clsMain.SQLString(sWord + txtSo.Text) + ",";
                    sSQL += clsMain.SQLString(sWord + sNumeric) + ",";
                    sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
                }
                else
                {
                    sSQL = "SELECT IDENT_CURRENT('VOUCHER') AS MA";
                    DataTable myDT = clsMain.ReturnDataTable(sSQL);
                    int Voucher = 1;
                    if (myDT.Rows[0]["MA"].ToString() != "1")
                        Voucher = int.Parse(myDT.Rows[0]["MA"].ToString()) + 1;

                    sSQL = "";
                    sSQL = "EXEC SP_InsertVoucherDetail ";
                    sSQL += clsMain.SQLString(txtMa.Text) + ",";
                    sSQL += clsMain.SQLString(Voucher.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                    sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                    sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";

                    sSQL += "EXEC SP_InsertVoucher ";
                    sSQL += clsMain.SQLString(dtpNgayPhatHanh.Text) + ",";
                    sSQL += clsMain.SQLString("1") + ",";
                    sSQL += clsMain.SQLString(txtMa.Text) + ",";
                    sSQL += clsMain.SQLString(txtMa.Text) + ",";
                    sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
                }
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                if (MessageQuestion("Chọn Yes để sửa tất cả Voucher cùng đợt phát hành. Chọn No sửa Voucher bạn đã chọn ?", "Thông báo") == DialogResult.Yes)
                {
                    sSQL = "EXEC SP_UpdateVoucherAll ";
                    sSQL += clsMain.SQLString(sVoucher) + ",";
                    sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                    sSQL += clsMain.SQLString(dtpNgayPhatHanh.Text) + ",";
                    sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                    sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                    sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                    sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
                }
                else
                {
                    if (sStatus == false)
                    {
                        sSQL = "EXEC SP_UpdateVoucher ";
                        sSQL += clsMain.SQLString(sMa) + ",";
                        sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                        sSQL += clsMain.SQLString(dtpKichHoat.Text) + ",";
                        sSQL += clsMain.SQLString(timeKichHoat.Text) + ",";
                        sSQL += clsMain.SQLString(dtpHetHan.Text) + ",";
                        sSQL += clsMain.SQLString(timeHetHan.Text) + ",";
                        sSQL += clsMain.SQLString(Percent.ToString()) + ",";
                        sSQL += clsMain.SQLString(Amount.ToString()) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtGhiChu.Text) + "\n";
                    }
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                MessageError("Lưu không thành công", "Thông báo");
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            sVoucher = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "VOUCHER").ToString();
            sStatus = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "STATUS").ToString());
            txtMa.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtGhiChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            cboLoai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAI_VOUCHER_ID").ToString());
            dtpKichHoat.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_KICHHOAT").ToString());
            timeKichHoat.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIO_KICHHOAT").ToString());
            dtpHetHan.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAY_HETHAN").ToString());
            timeHetHan.EditValue = DateTime.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIO_HETHAN").ToString());

            if (int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_PHANTRAM").ToString()) == 0)
            {
                chk_Tien.Checked = true;
                txtGiaTri.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_TIEN").ToString();
            }
            else {
                chk_PhanTram.Checked = true;
                txtGiaTri.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIATRI_PHANTRAM").ToString();
            }
            rdSoLe.SelectedIndex = 0;
            chkReadOnly();
            KhoaMoControl(true);  
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable dt = clsMain.ReturnDataTable("EXEC SP_SelectVoucher");
            gridControl2.DataSource = dt;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("12082015");
            btnSua.Enabled = clsUserManagement.AllowEdit("12082015");
            btnXoa.Enabled = clsUserManagement.AllowDelete("12082015");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void chkReadOnly()
        {
            lblMa.Visible = false ;
            txtMa.Visible = false;
            btnAuto.Visible = false;

            lblKyTu.Visible = false;
            lblSo.Visible =  false;
            lblSoVoucher.Visible =  false;
            txtKiTu.Visible = false;
            txtSo.Visible = false;
            txtSoVoucher.Visible =  false;
            btn_Auto1.Visible =  false;
            chkSS_Ngaunhien.Visible =  false;

            lblListvoucher.Visible = false;
            txtListvoucher .Visible = false;
            btnListvoucher.Visible = false;
            btnFile_Mo.Visible = false;

            if (rdSoLe.SelectedIndex == 0)
            {
                lblMa.Visible = true ;
                txtMa.Visible = true;
                btnAuto.Visible = true;
            }
            else if (rdSoLe.SelectedIndex == 1)
            {
                lblKyTu.Visible = true;
                lblSo.Visible = true;
                lblSoVoucher.Visible = true;
                txtKiTu.Visible = true;
                txtSo.Visible = true;
                txtSoVoucher.Visible = true;
                btn_Auto1.Visible = true;
                chkSS_Ngaunhien.Visible = true;
            }
            else if (rdSoLe.SelectedIndex == 2)
            {
                lblListvoucher.Visible = true ;
                txtListvoucher.Visible = true ;
                btnListvoucher.Visible = true ;
                btnFile_Mo.Visible = true ;
            }

        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            rdSoLe.Properties.ReadOnly = sBoolean;
            txtMa.Properties.ReadOnly = sBoolean;
            btnAuto.Enabled = !sBoolean;
            btn_Auto1.Enabled = !sBoolean;
            txtKiTu.Properties.ReadOnly = sBoolean;
            txtSo.Properties.ReadOnly = sBoolean;
            txtSoVoucher.Properties.ReadOnly = sBoolean;
            txtTen.Properties.ReadOnly = sBoolean;
            cboLoai.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            dtpKichHoat.Properties.ReadOnly = sBoolean;
            timeKichHoat.Properties.ReadOnly = sBoolean;
            timeHetHan.Properties.ReadOnly = sBoolean;
            dtpHetHan.Properties.ReadOnly = sBoolean;
            txtGiaTri.Properties.ReadOnly = sBoolean;
            chk_PhanTram.Properties.ReadOnly = sBoolean;
            dtpNgayPhatHanh.Properties.ReadOnly = sBoolean;
            chkSS_Ngaunhien.Properties.ReadOnly = sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            txtMa.Text = "";
            txtTen.Text = "";
            txtKiTu.Text = "";
            txtSo.Text = "";
            txtSoVoucher.Text = "";
            cboLoai.EditValue = cboLoai.Properties.GetDataSourceValue(cboLoai.Properties.ValueMember, 0);
            txtGhiChu.Text = "";
            txtGiaTri.Text = "";
            timeKichHoat.EditValue = "00:00:00";
            timeHetHan.EditValue = "00:00:00";
            dtpKichHoat.EditValue = clsMain.GetServerDate();
            dtpHetHan.EditValue = clsMain.GetServerDate();
            dtpNgayPhatHanh.EditValue = clsMain.GetServerDate();
        }

        private Boolean CheckInput()
        {
            if (sMa == "" && btnThem.Enabled)
            {
                if (rdSoLe.SelectedIndex == 1)
                {
                    int n;
                    bool isNumeric = int.TryParse(txtSo.Text, out n);
                    if (!isNumeric)
                    {
                        txtSo.Focus();
                        MessageInformation("Vui Lòng Nhập Lại! Bạn Nhập Số Bắt Đầu Voucher Không Đúng", "Thông báo");
                        return false;
                    }

                    if (txtSoVoucher.Text.Trim() == "")
                    {
                        txtSoVoucher.Focus();
                        MessageInformation("Bạn Chưa Nhập Số Voucher Cần Tạo", "Thông báo");
                        return false;
                    }
                }
                else if (rdSoLe.SelectedIndex == 0)
                {
                    if (txtMa.Text != "" && clsMain.ReturnDataTable("EXEC SP_CheckVoucher " + clsMain.SQLString(txtMa.Text)).Rows.Count > 0)
                    {
                        txtMa.Focus();
                        MessageInformation("Mã Voucher Đã Tồn Tại", "Thông báo");
                        return false;
                    }
                }
                else if (rdSoLe.SelectedIndex == 2)
                {
                    if (txtListvoucher .Text == "")
                    {
                        txtListvoucher.Focus();
                        MessageInformation("Bạn Chưa Nhập Voucher Cần Tạo", "Thông báo");
                        return false;
                    }

                    string[] ListVoucher = txtListvoucher.Text .Split(',');
                    for (int i = 0; i < ListVoucher.Length; i++)
                    {
                        if (clsMain.ReturnDataTable("EXEC SP_CheckVoucher " + clsMain.SQLString(ListVoucher [i].ToString ())).Rows.Count > 0)
                        {
                            txtListvoucher.Focus();
                            MessageInformation("Mã Voucher Đã Tồn Tại", "Thông báo");
                            return false;
                        }
                    }

                }
            }
            if (txtTen.Text.Trim() == "" )
            {
                txtTen.Focus();
                MessageInformation("Bạn Chưa Nhập Tên Voucher", "Thông báo");           
                return false;
            }
            if (txtGiaTri.Text.Trim() == "")
            {
                txtGiaTri.Focus();
                MessageInformation("Bạn Chưa Nhập Số Tiền Hoặc Phần Trăm Voucher", "Thông báo");
                return false;
            }
            if (chk_PhanTram.Checked == true)
            {
                if (int.Parse(txtGiaTri.Text) > 100 || int.Parse(txtGiaTri.Text) < 0)
                {
                    txtGiaTri.Focus();
                    MessageInformation("Vui Lòng Nhập Phần Trăm Voucher Nhỏ Hơn 100 Và Lớn Hơn 0", "Thông báo");
                    return false;
                }
            }
            else
            {
                if (int.Parse(txtGiaTri.Text) < 1000)
                {
                    txtGiaTri.Focus();
                    MessageInformation("Vui Lòng Nhập Số Tiền Voucher Lớn Hơn 1000", "Thông báo");
                    return false;
                }
            }

            return true;
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            txtMa.Text = clsMain.ReturnDataTable("select [dbo].[fc_NewCodeMaVoucher]()").Rows[0][0].ToString();
        }

        private void btn_Auto1_Click(object sender, EventArgs e)
        {
            String pMa = clsMain.ReturnDataTable("select [dbo].[fc_NewCodeMaVoucher]()").Rows[0][0].ToString();
            txtSo.Text = pMa.Substring(3);
            txtKiTu.Text = pMa.Substring(0,3);
        }

        private void rdSoLe_SelectedIndexChanged(object sender, EventArgs e)
        {
            chkReadOnly();
        }

        private void MessageInformation(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MessageError(String sString, String sCaption)
        {
            XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private DialogResult MessageQuestion(String sString, String sCaption)
        {
            return XtraMessageBox.Show(sString, sCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void btnListvoucher_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            txtListvoucher.Text = "";
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sOpenFile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel._Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(1);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                for (int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    try
                    {
                        String sMa = (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 : "";

                        if (sMa != "")
                        {
                            txtListvoucher.Text = txtListvoucher.Text + sMa + ",";
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.ToString (), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                xlWorkbook.Close(true, null, null);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorksheet);
                Marshal.ReleaseComObject(xlWorkbook);
                Marshal.ReleaseComObject(xlApp);
            }
            this.Cursor = Cursors.Default;
        }

        public static String Filter()
        {
            String Filter = "";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }

        private void btnFile_Mo_Click(object sender, EventArgs e)
        {
            String filename = Application.StartupPath + "\\FileVoucher.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
                Process.Start(filename);

        }






    }
}