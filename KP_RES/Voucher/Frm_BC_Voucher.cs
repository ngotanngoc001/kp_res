﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_BC_Voucher : DevExpress.XtraEditors.XtraForm
    {
        DataTable myDT;
        DataTable dtCapDo;
        public Frm_BC_Voucher()
        {
            InitializeComponent();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisibleChoose();
        }

        private void VisibleChoose()
        {
            lblTitle.Text = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

            pnChoose.Visible = (optGroup.SelectedIndex == 0) ? false : true;

            if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2 || optGroup.SelectedIndex == 3)
            {
                Visible(false);
            }
            else if (optGroup.SelectedIndex == 4)
            {
                Visible(true);
                DataTable myDT = clsMain.ReturnDataTable("EXEC SP_ComBoVoucherStyle");
                cboData.Properties.DataSource = myDT;
                cboData.EditValue = cboData.Properties.GetDataSourceValue(cboData.Properties.ValueMember, 0);
            }
            
        }

        private void Visible(Boolean sBoolean)
        {
            lblTitle.Visible = cboData.Visible = sBoolean;
            lblTuNgay.Visible = lblDenNgay.Visible = dtpDenNgay.Visible = dtpTuNgay.Visible = !sBoolean;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sMa = "";
                String sTuNgay = "";
                String sDenNgay = "";
                String sStatus = "";

                if (optGroup.EditValue.Equals("1") || optGroup.EditValue.Equals("2") || optGroup.EditValue.Equals("3"))
                {
                    sTuNgay = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.Value);
                    sDenNgay = String.Format("{0:dd/MM/yyyy}", dtpDenNgay.Value);
                }
                else if (optGroup.EditValue.Equals("4"))
                    sMa = cboData.EditValue.ToString();

                String sSQL = "EXEC SP_SelectBCVoucher ";
                sSQL += clsMain.SQLString(sMa) + ",";
                sSQL += clsMain.SQLString(sTuNgay) + ",";
                sSQL += clsMain.SQLString(sDenNgay) + ",";
                sSQL += clsMain.SQLString(optGroup.SelectedIndex.ToString());

                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                gridControl2.DataSource = myDT;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView2.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl2.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl2.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (Pan_Left.Width == 223)
            {
                Pan_Left.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                Pan_Left.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (gridView2.RowCount > 0)
                {
                    DataTable myDT = (DataTable)gridControl2.DataSource;

                    String sValue = "";
                    sValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description;

                    if (optGroup.SelectedIndex == 0)
                    {
                        sValue = "( " + sValue + " )";
                    }
                    else if (optGroup.SelectedIndex == 4)
                    {
                        sValue = "( " + sValue + " : " + cboData.Text + " )";
                    }
                    else if (optGroup.SelectedIndex == 1 || optGroup.SelectedIndex == 2 || optGroup.SelectedIndex == 3)
                    {
                        sValue = "( " + sValue + " - Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";
                    }

                    DataColumn All = new DataColumn();
                    All.ColumnName = "HEARDER";
                    All.DataType = System.Type.GetType("System.String");
                    All.DefaultValue = sValue;

                    if (ContainColumn("HEARDER", myDT) == false)
                        myDT.Columns.Add(All);

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = myDT;
                    frm.Mode = 72;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch{
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }



    }
}