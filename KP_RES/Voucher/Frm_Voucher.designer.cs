﻿namespace KP_RES 
{
    partial class Frm_Voucher 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnInfor = new DevExpress.XtraEditors.PanelControl();
            this.txtListvoucher = new DevExpress.XtraEditors.MemoEdit();
            this.btnFile_Mo = new DevExpress.XtraEditors.SimpleButton();
            this.btnListvoucher = new DevExpress.XtraEditors.SimpleButton();
            this.lblListvoucher = new DevExpress.XtraEditors.LabelControl();
            this.chkSS_Ngaunhien = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgayPhatHanh = new DevExpress.XtraEditors.DateEdit();
            this.rdSoLe = new DevExpress.XtraEditors.RadioGroup();
            this.btn_Auto1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblSoVoucher = new DevExpress.XtraEditors.LabelControl();
            this.txtSoVoucher = new DevExpress.XtraEditors.TextEdit();
            this.lblSo = new DevExpress.XtraEditors.LabelControl();
            this.txtSo = new DevExpress.XtraEditors.TextEdit();
            this.lblKyTu = new DevExpress.XtraEditors.LabelControl();
            this.txtKiTu = new DevExpress.XtraEditors.TextEdit();
            this.btnAuto = new DevExpress.XtraEditors.SimpleButton();
            this.cboLoai = new DevExpress.XtraEditors.LookUpEdit();
            this.timeHetHan = new DevExpress.XtraEditors.TimeEdit();
            this.timeKichHoat = new DevExpress.XtraEditors.TimeEdit();
            this.chk_Tien = new DevExpress.XtraEditors.CheckEdit();
            this.chk_PhanTram = new DevExpress.XtraEditors.CheckEdit();
            this.dtpHetHan = new DevExpress.XtraEditors.DateEdit();
            this.lblMa = new DevExpress.XtraEditors.LabelControl();
            this.txtMa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.dtpKichHoat = new DevExpress.XtraEditors.DateEdit();
            this.txtGiaTri = new DevExpress.XtraEditors.TextEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.txtGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.VOUCHER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAI_VOUCHER_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_KICHHOAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIO_KICHHOAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_HETHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIO_HETHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAI_VOUCHER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAY_PHATHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KICHHOAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HETHAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI_PHANTRAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATRI_TIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).BeginInit();
            this.pnInfor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtListvoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSS_Ngaunhien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayPhatHanh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayPhatHanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdSoLe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKiTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeHetHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKichHoat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Tien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_PhanTram.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHetHan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHetHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpKichHoat.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpKichHoat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // pnInfor
            // 
            this.pnInfor.Controls.Add(this.txtListvoucher);
            this.pnInfor.Controls.Add(this.btnFile_Mo);
            this.pnInfor.Controls.Add(this.btnListvoucher);
            this.pnInfor.Controls.Add(this.lblListvoucher);
            this.pnInfor.Controls.Add(this.chkSS_Ngaunhien);
            this.pnInfor.Controls.Add(this.labelControl1);
            this.pnInfor.Controls.Add(this.dtpNgayPhatHanh);
            this.pnInfor.Controls.Add(this.rdSoLe);
            this.pnInfor.Controls.Add(this.btn_Auto1);
            this.pnInfor.Controls.Add(this.lblSoVoucher);
            this.pnInfor.Controls.Add(this.txtSoVoucher);
            this.pnInfor.Controls.Add(this.lblSo);
            this.pnInfor.Controls.Add(this.txtSo);
            this.pnInfor.Controls.Add(this.lblKyTu);
            this.pnInfor.Controls.Add(this.txtKiTu);
            this.pnInfor.Controls.Add(this.btnAuto);
            this.pnInfor.Controls.Add(this.cboLoai);
            this.pnInfor.Controls.Add(this.timeHetHan);
            this.pnInfor.Controls.Add(this.timeKichHoat);
            this.pnInfor.Controls.Add(this.chk_Tien);
            this.pnInfor.Controls.Add(this.chk_PhanTram);
            this.pnInfor.Controls.Add(this.dtpHetHan);
            this.pnInfor.Controls.Add(this.lblMa);
            this.pnInfor.Controls.Add(this.txtMa);
            this.pnInfor.Controls.Add(this.labelControl11);
            this.pnInfor.Controls.Add(this.labelControl12);
            this.pnInfor.Controls.Add(this.dtpKichHoat);
            this.pnInfor.Controls.Add(this.txtGiaTri);
            this.pnInfor.Controls.Add(this.btnLuu);
            this.pnInfor.Controls.Add(this.btnSua);
            this.pnInfor.Controls.Add(this.btnXoa);
            this.pnInfor.Controls.Add(this.btnThem);
            this.pnInfor.Controls.Add(this.labelControl4);
            this.pnInfor.Controls.Add(this.labelControl2);
            this.pnInfor.Controls.Add(this.lblTEN);
            this.pnInfor.Controls.Add(this.txtTen);
            this.pnInfor.Controls.Add(this.txtGhiChu);
            this.pnInfor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnInfor.Location = new System.Drawing.Point(0, 0);
            this.pnInfor.Name = "pnInfor";
            this.pnInfor.Size = new System.Drawing.Size(1350, 324);
            this.pnInfor.TabIndex = 0;
            // 
            // txtListvoucher
            // 
            this.txtListvoucher.Location = new System.Drawing.Point(325, 65);
            this.txtListvoucher.Name = "txtListvoucher";
            this.txtListvoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtListvoucher.Properties.Appearance.Options.UseFont = true;
            this.txtListvoucher.Size = new System.Drawing.Size(554, 42);
            this.txtListvoucher.TabIndex = 99;
            // 
            // btnFile_Mo
            // 
            this.btnFile_Mo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile_Mo.Appearance.Options.UseFont = true;
            this.btnFile_Mo.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnFile_Mo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFile_Mo.Location = new System.Drawing.Point(963, 67);
            this.btnFile_Mo.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile_Mo.Name = "btnFile_Mo";
            this.btnFile_Mo.Size = new System.Drawing.Size(51, 26);
            this.btnFile_Mo.TabIndex = 98;
            this.btnFile_Mo.Click += new System.EventHandler(this.btnFile_Mo_Click);
            // 
            // btnListvoucher
            // 
            this.btnListvoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListvoucher.Appearance.Options.UseFont = true;
            this.btnListvoucher.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnListvoucher.Location = new System.Drawing.Point(886, 67);
            this.btnListvoucher.Margin = new System.Windows.Forms.Padding(4);
            this.btnListvoucher.Name = "btnListvoucher";
            this.btnListvoucher.Size = new System.Drawing.Size(69, 26);
            this.btnListvoucher.TabIndex = 35;
            this.btnListvoucher.Text = "Import";
            this.btnListvoucher.Click += new System.EventHandler(this.btnListvoucher_Click);
            // 
            // lblListvoucher
            // 
            this.lblListvoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListvoucher.Location = new System.Drawing.Point(236, 70);
            this.lblListvoucher.Margin = new System.Windows.Forms.Padding(4);
            this.lblListvoucher.Name = "lblListvoucher";
            this.lblListvoucher.Size = new System.Drawing.Size(83, 19);
            this.lblListvoucher.TabIndex = 34;
            this.lblListvoucher.Text = "Mã Voucher";
            // 
            // chkSS_Ngaunhien
            // 
            this.chkSS_Ngaunhien.EditValue = true;
            this.chkSS_Ngaunhien.EnterMoveNextControl = true;
            this.chkSS_Ngaunhien.Location = new System.Drawing.Point(833, 37);
            this.chkSS_Ngaunhien.Name = "chkSS_Ngaunhien";
            this.chkSS_Ngaunhien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSS_Ngaunhien.Properties.Appearance.Options.UseFont = true;
            this.chkSS_Ngaunhien.Properties.Caption = "Sinh số ngẫu nhiên";
            this.chkSS_Ngaunhien.Size = new System.Drawing.Size(169, 24);
            this.chkSS_Ngaunhien.TabIndex = 32;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(14, 248);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(112, 19);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "Ngày phát hành";
            // 
            // dtpNgayPhatHanh
            // 
            this.dtpNgayPhatHanh.EditValue = null;
            this.dtpNgayPhatHanh.EnterMoveNextControl = true;
            this.dtpNgayPhatHanh.Location = new System.Drawing.Point(128, 245);
            this.dtpNgayPhatHanh.Name = "dtpNgayPhatHanh";
            this.dtpNgayPhatHanh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayPhatHanh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayPhatHanh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayPhatHanh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayPhatHanh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayPhatHanh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayPhatHanh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayPhatHanh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayPhatHanh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayPhatHanh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayPhatHanh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgayPhatHanh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayPhatHanh.Size = new System.Drawing.Size(236, 26);
            this.dtpNgayPhatHanh.TabIndex = 23;
            // 
            // rdSoLe
            // 
            this.rdSoLe.EditValue = "01";
            this.rdSoLe.EnterMoveNextControl = true;
            this.rdSoLe.Location = new System.Drawing.Point(14, 3);
            this.rdSoLe.Name = "rdSoLe";
            this.rdSoLe.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.rdSoLe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.rdSoLe.Properties.Appearance.Options.UseBackColor = true;
            this.rdSoLe.Properties.Appearance.Options.UseFont = true;
            this.rdSoLe.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rdSoLe.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("01", "Phát Hành Voucher"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("02", "Phát Hành Nhiều Voucher"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("03", "Mã voucher có sẵn")});
            this.rdSoLe.Size = new System.Drawing.Size(213, 95);
            this.rdSoLe.TabIndex = 0;
            this.rdSoLe.SelectedIndexChanged += new System.EventHandler(this.rdSoLe_SelectedIndexChanged);
            // 
            // btn_Auto1
            // 
            this.btn_Auto1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Auto1.Appearance.Options.UseFont = true;
            this.btn_Auto1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Auto1.Location = new System.Drawing.Point(775, 35);
            this.btn_Auto1.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Auto1.Name = "btn_Auto1";
            this.btn_Auto1.Size = new System.Drawing.Size(51, 26);
            this.btn_Auto1.TabIndex = 10;
            this.btn_Auto1.Text = "Auto";
            this.btn_Auto1.Click += new System.EventHandler(this.btn_Auto1_Click);
            // 
            // lblSoVoucher
            // 
            this.lblSoVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVoucher.Location = new System.Drawing.Point(571, 38);
            this.lblSoVoucher.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoVoucher.Name = "lblSoVoucher";
            this.lblSoVoucher.Size = new System.Drawing.Size(81, 19);
            this.lblSoVoucher.TabIndex = 8;
            this.lblSoVoucher.Text = "Số Voucher";
            // 
            // txtSoVoucher
            // 
            this.txtSoVoucher.EnterMoveNextControl = true;
            this.txtSoVoucher.Location = new System.Drawing.Point(659, 35);
            this.txtSoVoucher.Name = "txtSoVoucher";
            this.txtSoVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVoucher.Properties.Appearance.Options.UseFont = true;
            this.txtSoVoucher.Properties.Mask.EditMask = "d";
            this.txtSoVoucher.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSoVoucher.Size = new System.Drawing.Size(109, 26);
            this.txtSoVoucher.TabIndex = 9;
            // 
            // lblSo
            // 
            this.lblSo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSo.Location = new System.Drawing.Point(395, 38);
            this.lblSo.Margin = new System.Windows.Forms.Padding(4);
            this.lblSo.Name = "lblSo";
            this.lblSo.Size = new System.Drawing.Size(53, 19);
            this.lblSo.TabIndex = 6;
            this.lblSo.Text = "Bắt đầu";
            // 
            // txtSo
            // 
            this.txtSo.EnterMoveNextControl = true;
            this.txtSo.Location = new System.Drawing.Point(455, 35);
            this.txtSo.Name = "txtSo";
            this.txtSo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSo.Properties.Appearance.Options.UseFont = true;
            this.txtSo.Size = new System.Drawing.Size(109, 26);
            this.txtSo.TabIndex = 7;
            // 
            // lblKyTu
            // 
            this.lblKyTu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKyTu.Location = new System.Drawing.Point(236, 38);
            this.lblKyTu.Margin = new System.Windows.Forms.Padding(4);
            this.lblKyTu.Name = "lblKyTu";
            this.lblKyTu.Size = new System.Drawing.Size(33, 19);
            this.lblKyTu.TabIndex = 4;
            this.lblKyTu.Text = "Kí tự";
            // 
            // txtKiTu
            // 
            this.txtKiTu.EnterMoveNextControl = true;
            this.txtKiTu.Location = new System.Drawing.Point(279, 35);
            this.txtKiTu.Name = "txtKiTu";
            this.txtKiTu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKiTu.Properties.Appearance.Options.UseFont = true;
            this.txtKiTu.Size = new System.Drawing.Size(109, 26);
            this.txtKiTu.TabIndex = 5;
            // 
            // btnAuto
            // 
            this.btnAuto.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuto.Appearance.Options.UseFont = true;
            this.btnAuto.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAuto.Location = new System.Drawing.Point(510, 3);
            this.btnAuto.Margin = new System.Windows.Forms.Padding(4);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(51, 26);
            this.btnAuto.TabIndex = 3;
            this.btnAuto.Text = "Auto";
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // cboLoai
            // 
            this.cboLoai.EnterMoveNextControl = true;
            this.cboLoai.Location = new System.Drawing.Point(127, 113);
            this.cboLoai.Name = "cboLoai";
            this.cboLoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboLoai.Properties.Appearance.Options.UseFont = true;
            this.cboLoai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboLoai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoai.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboLoai.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboLoai.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            this.cboLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboLoai.Properties.DisplayMember = "TEN";
            this.cboLoai.Properties.DropDownItemHeight = 40;
            this.cboLoai.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.cboLoai.Properties.NullText = "";
            this.cboLoai.Properties.ShowFooter = false;
            this.cboLoai.Properties.ShowHeader = false;
            this.cboLoai.Properties.ValueMember = "MA";
            this.cboLoai.Size = new System.Drawing.Size(236, 26);
            this.cboLoai.TabIndex = 12;
            // 
            // timeHetHan
            // 
            this.timeHetHan.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeHetHan.EnterMoveNextControl = true;
            this.timeHetHan.Location = new System.Drawing.Point(271, 213);
            this.timeHetHan.Name = "timeHetHan";
            this.timeHetHan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeHetHan.Properties.Appearance.Options.UseFont = true;
            this.timeHetHan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeHetHan.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeHetHan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeHetHan.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeHetHan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeHetHan.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeHetHan.Size = new System.Drawing.Size(92, 26);
            this.timeHetHan.TabIndex = 21;
            // 
            // timeKichHoat
            // 
            this.timeKichHoat.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKichHoat.EnterMoveNextControl = true;
            this.timeKichHoat.Location = new System.Drawing.Point(271, 181);
            this.timeKichHoat.Name = "timeKichHoat";
            this.timeKichHoat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKichHoat.Properties.Appearance.Options.UseFont = true;
            this.timeKichHoat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKichHoat.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKichHoat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKichHoat.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKichHoat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKichHoat.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKichHoat.Size = new System.Drawing.Size(92, 26);
            this.timeKichHoat.TabIndex = 18;
            // 
            // chk_Tien
            // 
            this.chk_Tien.Location = new System.Drawing.Point(120, 145);
            this.chk_Tien.Name = "chk_Tien";
            this.chk_Tien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk_Tien.Properties.Appearance.Options.UseFont = true;
            this.chk_Tien.Properties.Caption = "Tiền";
            this.chk_Tien.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk_Tien.Properties.RadioGroupIndex = 1;
            this.chk_Tien.Size = new System.Drawing.Size(63, 24);
            this.chk_Tien.TabIndex = 14;
            this.chk_Tien.TabStop = false;
            // 
            // chk_PhanTram
            // 
            this.chk_PhanTram.EditValue = true;
            this.chk_PhanTram.Location = new System.Drawing.Point(14, 145);
            this.chk_PhanTram.Name = "chk_PhanTram";
            this.chk_PhanTram.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk_PhanTram.Properties.Appearance.Options.UseFont = true;
            this.chk_PhanTram.Properties.Caption = "Phần Trăm";
            this.chk_PhanTram.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk_PhanTram.Properties.RadioGroupIndex = 1;
            this.chk_PhanTram.Size = new System.Drawing.Size(104, 24);
            this.chk_PhanTram.TabIndex = 13;
            // 
            // dtpHetHan
            // 
            this.dtpHetHan.EditValue = null;
            this.dtpHetHan.EnterMoveNextControl = true;
            this.dtpHetHan.Location = new System.Drawing.Point(128, 213);
            this.dtpHetHan.Name = "dtpHetHan";
            this.dtpHetHan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHetHan.Properties.Appearance.Options.UseFont = true;
            this.dtpHetHan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHetHan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpHetHan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpHetHan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpHetHan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpHetHan.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpHetHan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpHetHan.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpHetHan.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpHetHan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpHetHan.Size = new System.Drawing.Size(137, 26);
            this.dtpHetHan.TabIndex = 20;
            // 
            // lblMa
            // 
            this.lblMa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMa.Location = new System.Drawing.Point(236, 6);
            this.lblMa.Margin = new System.Windows.Forms.Padding(4);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(83, 19);
            this.lblMa.TabIndex = 1;
            this.lblMa.Text = "Mã Voucher";
            // 
            // txtMa
            // 
            this.txtMa.EnterMoveNextControl = true;
            this.txtMa.Location = new System.Drawing.Point(325, 3);
            this.txtMa.Name = "txtMa";
            this.txtMa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Properties.Appearance.Options.UseFont = true;
            this.txtMa.Size = new System.Drawing.Size(178, 26);
            this.txtMa.TabIndex = 2;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(14, 216);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(94, 19);
            this.labelControl11.TabIndex = 19;
            this.labelControl11.Text = "Ngày hết hạn";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(14, 184);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(105, 19);
            this.labelControl12.TabIndex = 16;
            this.labelControl12.Text = "Ngày kích hoạt";
            // 
            // dtpKichHoat
            // 
            this.dtpKichHoat.EditValue = null;
            this.dtpKichHoat.EnterMoveNextControl = true;
            this.dtpKichHoat.Location = new System.Drawing.Point(128, 181);
            this.dtpKichHoat.Name = "dtpKichHoat";
            this.dtpKichHoat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpKichHoat.Properties.Appearance.Options.UseFont = true;
            this.dtpKichHoat.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpKichHoat.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpKichHoat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpKichHoat.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpKichHoat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpKichHoat.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpKichHoat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpKichHoat.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpKichHoat.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpKichHoat.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpKichHoat.Size = new System.Drawing.Size(137, 26);
            this.dtpKichHoat.TabIndex = 17;
            // 
            // txtGiaTri
            // 
            this.txtGiaTri.EnterMoveNextControl = true;
            this.txtGiaTri.Location = new System.Drawing.Point(188, 145);
            this.txtGiaTri.Name = "txtGiaTri";
            this.txtGiaTri.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTri.Properties.Appearance.Options.UseFont = true;
            this.txtGiaTri.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaTri.Size = new System.Drawing.Size(175, 26);
            this.txtGiaTri.TabIndex = 15;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(128, 278);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(78, 35);
            this.btnLuu.TabIndex = 28;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(302, 278);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 30;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(390, 278);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 31;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(214, 278);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 29;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(370, 148);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 19);
            this.labelControl4.TabIndex = 26;
            this.labelControl4.Text = "Ghi chú";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(14, 116);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(92, 19);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Loại Voucher";
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(368, 116);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(88, 19);
            this.lblTEN.TabIndex = 24;
            this.lblTEN.Text = "Tên voucher";
            // 
            // txtTen
            // 
            this.txtTen.EnterMoveNextControl = true;
            this.txtTen.Location = new System.Drawing.Point(463, 113);
            this.txtTen.Name = "txtTen";
            this.txtTen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Properties.Appearance.Options.UseFont = true;
            this.txtTen.Size = new System.Drawing.Size(492, 26);
            this.txtTen.TabIndex = 25;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(463, 145);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu.Size = new System.Drawing.Size(492, 126);
            this.txtGhiChu.TabIndex = 27;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 324);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1311, 363);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.VOUCHER,
            this.LOAI_VOUCHER_ID,
            this.NGAY_KICHHOAT,
            this.GIO_KICHHOAT,
            this.NGAY_HETHAN,
            this.GIO_HETHAN,
            this.STT,
            this.MA,
            this.TEN,
            this.LOAI_VOUCHER,
            this.NGAY_PHATHANH,
            this.KICHHOAT,
            this.HETHAN,
            this.GIATRI_PHANTRAM,
            this.GIATRI_TIEN,
            this.GHICHU,
            this.STATUS,
            this.Fill});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // VOUCHER
            // 
            this.VOUCHER.FieldName = "VOUCHER";
            this.VOUCHER.Name = "VOUCHER";
            // 
            // LOAI_VOUCHER_ID
            // 
            this.LOAI_VOUCHER_ID.FieldName = "LOAI_VOUCHER_ID";
            this.LOAI_VOUCHER_ID.Name = "LOAI_VOUCHER_ID";
            // 
            // NGAY_KICHHOAT
            // 
            this.NGAY_KICHHOAT.FieldName = "NGAY_KICHHOAT";
            this.NGAY_KICHHOAT.Name = "NGAY_KICHHOAT";
            // 
            // GIO_KICHHOAT
            // 
            this.GIO_KICHHOAT.FieldName = "GIO_KICHHOAT";
            this.GIO_KICHHOAT.Name = "GIO_KICHHOAT";
            // 
            // NGAY_HETHAN
            // 
            this.NGAY_HETHAN.FieldName = "NGAY_HETHAN";
            this.NGAY_HETHAN.Name = "NGAY_HETHAN";
            // 
            // GIO_HETHAN
            // 
            this.GIO_HETHAN.FieldName = "GIO_HETHAN";
            this.GIO_HETHAN.Name = "GIO_HETHAN";
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 100;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 250;
            // 
            // LOAI_VOUCHER
            // 
            this.LOAI_VOUCHER.AppearanceCell.Options.UseTextOptions = true;
            this.LOAI_VOUCHER.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LOAI_VOUCHER.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOAI_VOUCHER.AppearanceHeader.Options.UseFont = true;
            this.LOAI_VOUCHER.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAI_VOUCHER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAI_VOUCHER.Caption = "Loại";
            this.LOAI_VOUCHER.FieldName = "LOAI_VOUCHER";
            this.LOAI_VOUCHER.Name = "LOAI_VOUCHER";
            this.LOAI_VOUCHER.OptionsColumn.AllowEdit = false;
            this.LOAI_VOUCHER.OptionsColumn.AllowFocus = false;
            this.LOAI_VOUCHER.OptionsColumn.FixedWidth = true;
            this.LOAI_VOUCHER.Visible = true;
            this.LOAI_VOUCHER.VisibleIndex = 3;
            this.LOAI_VOUCHER.Width = 150;
            // 
            // NGAY_PHATHANH
            // 
            this.NGAY_PHATHANH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAY_PHATHANH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_PHATHANH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_PHATHANH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAY_PHATHANH.AppearanceHeader.Options.UseFont = true;
            this.NGAY_PHATHANH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAY_PHATHANH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAY_PHATHANH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAY_PHATHANH.Caption = "Ngày phát hành";
            this.NGAY_PHATHANH.DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
            this.NGAY_PHATHANH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAY_PHATHANH.FieldName = "NGAY_PHATHANH";
            this.NGAY_PHATHANH.Name = "NGAY_PHATHANH";
            this.NGAY_PHATHANH.OptionsColumn.AllowEdit = false;
            this.NGAY_PHATHANH.OptionsColumn.AllowFocus = false;
            this.NGAY_PHATHANH.OptionsColumn.FixedWidth = true;
            this.NGAY_PHATHANH.Visible = true;
            this.NGAY_PHATHANH.VisibleIndex = 4;
            this.NGAY_PHATHANH.Width = 150;
            // 
            // KICHHOAT
            // 
            this.KICHHOAT.AppearanceCell.Options.UseTextOptions = true;
            this.KICHHOAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KICHHOAT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KICHHOAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KICHHOAT.AppearanceHeader.Options.UseFont = true;
            this.KICHHOAT.AppearanceHeader.Options.UseTextOptions = true;
            this.KICHHOAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KICHHOAT.Caption = "Ngày kích hoạt";
            this.KICHHOAT.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm:ss}";
            this.KICHHOAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.KICHHOAT.FieldName = "KICHHOAT";
            this.KICHHOAT.Name = "KICHHOAT";
            this.KICHHOAT.OptionsColumn.AllowEdit = false;
            this.KICHHOAT.OptionsColumn.AllowFocus = false;
            this.KICHHOAT.OptionsColumn.FixedWidth = true;
            this.KICHHOAT.Visible = true;
            this.KICHHOAT.VisibleIndex = 5;
            this.KICHHOAT.Width = 200;
            // 
            // HETHAN
            // 
            this.HETHAN.AppearanceCell.Options.UseTextOptions = true;
            this.HETHAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HETHAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HETHAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HETHAN.AppearanceHeader.Options.UseFont = true;
            this.HETHAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HETHAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HETHAN.Caption = "Ngày hết hạn";
            this.HETHAN.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm:ss}";
            this.HETHAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.HETHAN.FieldName = "HETHAN";
            this.HETHAN.Name = "HETHAN";
            this.HETHAN.OptionsColumn.AllowEdit = false;
            this.HETHAN.OptionsColumn.AllowFocus = false;
            this.HETHAN.OptionsColumn.FixedWidth = true;
            this.HETHAN.Visible = true;
            this.HETHAN.VisibleIndex = 6;
            this.HETHAN.Width = 200;
            // 
            // GIATRI_PHANTRAM
            // 
            this.GIATRI_PHANTRAM.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI_PHANTRAM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI_PHANTRAM.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI_PHANTRAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI_PHANTRAM.AppearanceHeader.Options.UseFont = true;
            this.GIATRI_PHANTRAM.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI_PHANTRAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI_PHANTRAM.Caption = "Phần trăm";
            this.GIATRI_PHANTRAM.DisplayFormat.FormatString = "N0";
            this.GIATRI_PHANTRAM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI_PHANTRAM.FieldName = "GIATRI_PHANTRAM";
            this.GIATRI_PHANTRAM.Name = "GIATRI_PHANTRAM";
            this.GIATRI_PHANTRAM.OptionsColumn.AllowEdit = false;
            this.GIATRI_PHANTRAM.OptionsColumn.AllowFocus = false;
            this.GIATRI_PHANTRAM.OptionsColumn.FixedWidth = true;
            this.GIATRI_PHANTRAM.Visible = true;
            this.GIATRI_PHANTRAM.VisibleIndex = 7;
            this.GIATRI_PHANTRAM.Width = 100;
            // 
            // GIATRI_TIEN
            // 
            this.GIATRI_TIEN.AppearanceCell.Options.UseTextOptions = true;
            this.GIATRI_TIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIATRI_TIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIATRI_TIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIATRI_TIEN.AppearanceHeader.Options.UseFont = true;
            this.GIATRI_TIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIATRI_TIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIATRI_TIEN.Caption = "Tiền";
            this.GIATRI_TIEN.DisplayFormat.FormatString = "N0";
            this.GIATRI_TIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIATRI_TIEN.FieldName = "GIATRI_TIEN";
            this.GIATRI_TIEN.Name = "GIATRI_TIEN";
            this.GIATRI_TIEN.OptionsColumn.AllowEdit = false;
            this.GIATRI_TIEN.OptionsColumn.AllowFocus = false;
            this.GIATRI_TIEN.OptionsColumn.FixedWidth = true;
            this.GIATRI_TIEN.Visible = true;
            this.GIATRI_TIEN.VisibleIndex = 8;
            this.GIATRI_TIEN.Width = 100;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 9;
            this.GHICHU.Width = 250;
            // 
            // STATUS
            // 
            this.STATUS.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STATUS.AppearanceHeader.Options.UseFont = true;
            this.STATUS.AppearanceHeader.Options.UseTextOptions = true;
            this.STATUS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STATUS.Caption = "Sử dụng";
            this.STATUS.FieldName = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.OptionsColumn.AllowEdit = false;
            this.STATUS.OptionsColumn.AllowFocus = false;
            this.STATUS.OptionsColumn.FixedWidth = true;
            this.STATUS.Visible = true;
            this.STATUS.VisibleIndex = 10;
            this.STATUS.Width = 90;
            // 
            // Fill
            // 
            this.Fill.Name = "Fill";
            this.Fill.OptionsColumn.AllowEdit = false;
            this.Fill.OptionsColumn.AllowFocus = false;
            this.Fill.Visible = true;
            this.Fill.VisibleIndex = 11;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1311, 324);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 363);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 54);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 255);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 104);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 47);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 151);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 52);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 203);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 52);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 52);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 52);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 52);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 309);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 52);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 52);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_Voucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 687);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnInfor);
            this.Name = "Frm_Voucher";
            this.Text = "Phát Hành Voucher";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Voucher_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pnInfor)).EndInit();
            this.pnInfor.ResumeLayout(false);
            this.pnInfor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtListvoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSS_Ngaunhien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayPhatHanh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayPhatHanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdSoLe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKiTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeHetHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKichHoat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Tien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_PhanTram.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHetHan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHetHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpKichHoat.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpKichHoat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnInfor;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn STATUS;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn LOAI_VOUCHER;
        private DevExpress.XtraGrid.Columns.GridColumn KICHHOAT;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn HETHAN;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn Fill;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.DateEdit dtpKichHoat;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI_PHANTRAM;
        private DevExpress.XtraGrid.Columns.GridColumn GIATRI_TIEN;
        private DevExpress.XtraEditors.LabelControl lblMa;
        private DevExpress.XtraEditors.TextEdit txtMa;
        private DevExpress.XtraEditors.DateEdit dtpHetHan;
        private DevExpress.XtraEditors.TextEdit txtGiaTri;
        private DevExpress.XtraEditors.CheckEdit chk_Tien;
        private DevExpress.XtraEditors.CheckEdit chk_PhanTram;
        private DevExpress.XtraEditors.TimeEdit timeHetHan;
        private DevExpress.XtraEditors.TimeEdit timeKichHoat;
        private DevExpress.XtraEditors.LookUpEdit cboLoai;
        private DevExpress.XtraEditors.SimpleButton btnAuto;
        private DevExpress.XtraEditors.LabelControl lblSoVoucher;
        private DevExpress.XtraEditors.TextEdit txtSoVoucher;
        private DevExpress.XtraEditors.LabelControl lblSo;
        private DevExpress.XtraEditors.TextEdit txtSo;
        private DevExpress.XtraEditors.LabelControl lblKyTu;
        private DevExpress.XtraEditors.TextEdit txtKiTu;
        private DevExpress.XtraEditors.SimpleButton btn_Auto1;
        private DevExpress.XtraEditors.RadioGroup rdSoLe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dtpNgayPhatHanh;
        private DevExpress.XtraGrid.Columns.GridColumn LOAI_VOUCHER_ID;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_KICHHOAT;
        private DevExpress.XtraGrid.Columns.GridColumn GIO_KICHHOAT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_HETHAN;
        private DevExpress.XtraGrid.Columns.GridColumn GIO_HETHAN;
        private DevExpress.XtraGrid.Columns.GridColumn NGAY_PHATHANH;
        private DevExpress.XtraEditors.MemoEdit txtGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn VOUCHER;
        private DevExpress.XtraEditors.CheckEdit chkSS_Ngaunhien;
        private DevExpress.XtraEditors.LabelControl lblListvoucher;
        private DevExpress.XtraEditors.SimpleButton btnListvoucher;
        private DevExpress.XtraEditors.SimpleButton btnFile_Mo;
        private DevExpress.XtraEditors.MemoEdit txtListvoucher;




    }
}