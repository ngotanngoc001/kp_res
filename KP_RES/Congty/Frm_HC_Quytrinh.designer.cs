﻿namespace KP_RES 
{
    partial class Frm_HC_Quytrinh 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itKho = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.itPhongHanhChinh = new DevExpress.XtraEditors.TileItem();
            this.itPhongNhanSu = new DevExpress.XtraEditors.TileItem();
            this.itPhongKinhDoanh = new DevExpress.XtraEditors.TileItem();
            this.itPhongTaiChinh = new DevExpress.XtraEditors.TileItem();
            this.itPhongKeToan = new DevExpress.XtraEditors.TileItem();
            this.itPhongVe = new DevExpress.XtraEditors.TileItem();
            this.itPhongQuy = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 67;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itPhongHanhChinh);
            this.tileGroup4.Items.Add(this.itPhongNhanSu);
            this.tileGroup4.Items.Add(this.itPhongKinhDoanh);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itPhongTaiChinh);
            this.tileGroup5.Items.Add(this.itPhongKeToan);
            this.tileGroup5.Items.Add(this.itPhongVe);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itKho);
            this.tileGroup2.Items.Add(this.itPhongQuy);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itKho
            // 
            tileItemElement7.Text = "Kho";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itKho.Elements.Add(tileItemElement7);
            this.itKho.Id = 59;
            this.itKho.IsLarge = true;
            this.itKho.Name = "itKho";
            this.itKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKho_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // itPhongHanhChinh
            // 
            tileItemElement1.Text = "Phòng hành chính";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongHanhChinh.Elements.Add(tileItemElement1);
            this.itPhongHanhChinh.Id = 60;
            this.itPhongHanhChinh.IsLarge = true;
            this.itPhongHanhChinh.Name = "itPhongHanhChinh";
            this.itPhongHanhChinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongHanhChinh_ItemClick);
            // 
            // itPhongNhanSu
            // 
            tileItemElement2.Text = "Phòng nhân sự";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongNhanSu.Elements.Add(tileItemElement2);
            this.itPhongNhanSu.Id = 61;
            this.itPhongNhanSu.IsLarge = true;
            this.itPhongNhanSu.Name = "itPhongNhanSu";
            this.itPhongNhanSu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongNhanSu_ItemClick);
            // 
            // itPhongKinhDoanh
            // 
            tileItemElement3.Text = "Phòng kinh doanh";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongKinhDoanh.Elements.Add(tileItemElement3);
            this.itPhongKinhDoanh.Id = 62;
            this.itPhongKinhDoanh.IsLarge = true;
            this.itPhongKinhDoanh.Name = "itPhongKinhDoanh";
            this.itPhongKinhDoanh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongKinhDoanh_ItemClick);
            // 
            // itPhongTaiChinh
            // 
            tileItemElement4.Text = "Phòng tài chính";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongTaiChinh.Elements.Add(tileItemElement4);
            this.itPhongTaiChinh.Id = 63;
            this.itPhongTaiChinh.IsLarge = true;
            this.itPhongTaiChinh.Name = "itPhongTaiChinh";
            this.itPhongTaiChinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongTaiChinh_ItemClick);
            // 
            // itPhongKeToan
            // 
            tileItemElement5.Text = "Phòng kế toán";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongKeToan.Elements.Add(tileItemElement5);
            this.itPhongKeToan.Id = 64;
            this.itPhongKeToan.IsLarge = true;
            this.itPhongKeToan.Name = "itPhongKeToan";
            this.itPhongKeToan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongKeToan_ItemClick);
            // 
            // itPhongVe
            // 
            tileItemElement6.Text = "Phòng vé";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongVe.Elements.Add(tileItemElement6);
            this.itPhongVe.Id = 65;
            this.itPhongVe.IsLarge = true;
            this.itPhongVe.Name = "itPhongVe";
            this.itPhongVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongVe_ItemClick);
            // 
            // itPhongQuy
            // 
            tileItemElement8.Text = "Phòng quỹ";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongQuy.Elements.Add(tileItemElement8);
            this.itPhongQuy.Id = 66;
            this.itPhongQuy.IsLarge = true;
            this.itPhongQuy.Name = "itPhongQuy";
            this.itPhongQuy.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongQuy_ItemClick);
            // 
            // Frm_HC_Quytrinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_HC_Quytrinh";
            this.Text = "Quy trình làm việc";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_HC_Quytrinh_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itKho;
        private DevExpress.XtraEditors.TileItem itPhongHanhChinh;
        private DevExpress.XtraEditors.TileItem itPhongNhanSu;
        private DevExpress.XtraEditors.TileItem itPhongKinhDoanh;
        private DevExpress.XtraEditors.TileItem itPhongTaiChinh;
        private DevExpress.XtraEditors.TileItem itPhongKeToan;
        private DevExpress.XtraEditors.TileItem itPhongVe;
        private DevExpress.XtraEditors.TileItem itPhongQuy;
    }
}