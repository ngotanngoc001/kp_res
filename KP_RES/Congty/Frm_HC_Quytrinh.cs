﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_HC_Quytrinh : DevExpress.XtraEditors.XtraForm
    {
        public Frm_HC_Quytrinh()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_HC_Quytrinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itPhongHanhChinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongHanhChinh.Checked)
            {
                //this.Parent.GoTo<Frm_Hosocongty1>();
            }
        }

        private void itPhongNhanSu_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongKinhDoanh_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongTaiChinh_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongKeToan_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongVe_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itKho_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongQuy_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itPhongHanhChinh);
            cls_KP_RES.RemoveTileItem(itPhongNhanSu);
            cls_KP_RES.RemoveTileItem(itPhongKinhDoanh);
            cls_KP_RES.RemoveTileItem(itPhongTaiChinh);
            cls_KP_RES.RemoveTileItem(itPhongKeToan);
            cls_KP_RES.RemoveTileItem(itPhongVe);
            cls_KP_RES.RemoveTileItem(itKho);
            cls_KP_RES.RemoveTileItem(itPhongQuy);
        }

        private void LoadPermission()
        {
            itPhongHanhChinh.Checked = true;
            itPhongNhanSu.Checked = true;
            itPhongKinhDoanh.Checked = true;
            itPhongTaiChinh.Checked = true;
            itPhongKeToan.Checked = true;
            itPhongVe.Checked = true;
            itKho.Checked = true;
            itPhongQuy.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

      

       
        

       

      

       

        

        

        

        

    }
}