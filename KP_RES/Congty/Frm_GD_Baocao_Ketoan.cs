﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_GD_Baocao_Ketoan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_GD_Baocao_Ketoan()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_GD_Baocao_Ketoan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

       

        private void itThuchi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThuchi.Checked)
            {
                this.Parent.GoTo<Frm_BaoCaoQuyTienMat>();
            }
        }


        private void VisibleTileItem()
        {
           
           
            cls_KP_RES.RemoveTileItem(itThuchi);
           
        }

        private void LoadPermission()
        {
            
           
            itThuchi.Checked = true;
         
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

    }
}