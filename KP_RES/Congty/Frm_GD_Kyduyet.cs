﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_GD_Kyduyet : DevExpress.XtraEditors.XtraForm
    {
        public Frm_GD_Kyduyet()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_GD_Kyduyet_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itDeNghiChi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDeNghiChi.Checked)
            {
                //this.Parent.GoTo<Frm_GD_Kyduyet>();
            }
        }

        private void itDeNghiCapVatTu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itDeNghiCapVatTu.Checked)
            {
                //this.Parent.GoTo<Frm_GD_Kyduyet>();
            }
        }

        private void itUyNhiemChi_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itUyNhiemChi.Checked)
            {
                //this.Parent.GoTo<Frm_GD_Kyduyet>();
            }
        }

        private void itGiayRutTien_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiayRutTien.Checked)
            {
                //this.Parent.GoTo<Frm_GD_Kyduyet>();
            }
        } 

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itDeNghiChi);
            cls_KP_RES.RemoveTileItem(itDeNghiCapVatTu);
            cls_KP_RES.RemoveTileItem(itUyNhiemChi);
            cls_KP_RES.RemoveTileItem(itGiayRutTien);
        }

        private void LoadPermission()
        {
            itDeNghiChi.Checked = true;
            itDeNghiCapVatTu.Checked = true;
            itUyNhiemChi.Checked = true;
            itGiayRutTien.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

    }
}