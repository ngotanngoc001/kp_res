﻿namespace KP_RES 
{
    partial class Frm_GD_Baocao 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongHanhChinh = new DevExpress.XtraEditors.TileItem();
            this.itPhongNhanSu = new DevExpress.XtraEditors.TileItem();
            this.itPhongKinhDoanh = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itPhongTaiChinh = new DevExpress.XtraEditors.TileItem();
            this.itPhongKeToan = new DevExpress.XtraEditors.TileItem();
            this.itPhongVe = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itThuQuy = new DevExpress.XtraEditors.TileItem();
            this.itKhoVatTu = new DevExpress.XtraEditors.TileItem();
            this.itToThoHan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup6 = new DevExpress.XtraEditors.TileGroup();
            this.itXayDung = new DevExpress.XtraEditors.TileItem();
            this.itUTau = new DevExpress.XtraEditors.TileItem();
            this.itBaoVe = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem7 = new DevExpress.XtraEditors.TileItem();
            this.tileItem6 = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Groups.Add(this.tileGroup6);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 80;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itPhongHanhChinh);
            this.tileGroup4.Items.Add(this.itPhongNhanSu);
            this.tileGroup4.Items.Add(this.itPhongKinhDoanh);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itPhongHanhChinh
            // 
            this.itPhongHanhChinh.BackgroundImage = global::KP_RES.Properties.Resources.Live_Hotmail;
            this.itPhongHanhChinh.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement1.Text = "Phòng hành chính ";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongHanhChinh.Elements.Add(tileItemElement1);
            this.itPhongHanhChinh.Id = 62;
            this.itPhongHanhChinh.IsLarge = true;
            this.itPhongHanhChinh.Name = "itPhongHanhChinh";
            this.itPhongHanhChinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongHanhChinh_ItemClick);
            // 
            // itPhongNhanSu
            // 
            tileItemElement2.Text = "Phòng nhân sự";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongNhanSu.Elements.Add(tileItemElement2);
            this.itPhongNhanSu.Id = 63;
            this.itPhongNhanSu.IsLarge = true;
            this.itPhongNhanSu.Name = "itPhongNhanSu";
            this.itPhongNhanSu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongNhanSu_ItemClick);
            // 
            // itPhongKinhDoanh
            // 
            tileItemElement3.Text = "Phòng kinh doanh";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongKinhDoanh.Elements.Add(tileItemElement3);
            this.itPhongKinhDoanh.Id = 64;
            this.itPhongKinhDoanh.IsLarge = true;
            this.itPhongKinhDoanh.Name = "itPhongKinhDoanh";
            this.itPhongKinhDoanh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongKinhDoanh_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itPhongTaiChinh);
            this.tileGroup2.Items.Add(this.itPhongKeToan);
            this.tileGroup2.Items.Add(this.itPhongVe);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itPhongTaiChinh
            // 
            tileItemElement4.Text = "Phòng tài chính";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongTaiChinh.Elements.Add(tileItemElement4);
            this.itPhongTaiChinh.Id = 71;
            this.itPhongTaiChinh.IsLarge = true;
            this.itPhongTaiChinh.Name = "itPhongTaiChinh";
            this.itPhongTaiChinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongTaiChinh_ItemClick);
            // 
            // itPhongKeToan
            // 
            this.itPhongKeToan.BackgroundImage = global::KP_RES.Properties.Resources.thietlapbanggia;
            this.itPhongKeToan.BackgroundImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement5.Text = "Phòng kế toán";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongKeToan.Elements.Add(tileItemElement5);
            this.itPhongKeToan.Id = 72;
            this.itPhongKeToan.IsLarge = true;
            this.itPhongKeToan.Name = "itPhongKeToan";
            this.itPhongKeToan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongKeToan_ItemClick);
            // 
            // itPhongVe
            // 
            tileItemElement6.Text = "Phòng vé";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhongVe.Elements.Add(tileItemElement6);
            this.itPhongVe.Id = 73;
            this.itPhongVe.IsLarge = true;
            this.itPhongVe.Name = "itPhongVe";
            this.itPhongVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhongVe_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itThuQuy);
            this.tileGroup5.Items.Add(this.itKhoVatTu);
            this.tileGroup5.Items.Add(this.itToThoHan);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itThuQuy
            // 
            tileItemElement7.Text = "Thủ quỹ";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itThuQuy.Elements.Add(tileItemElement7);
            this.itThuQuy.Id = 74;
            this.itThuQuy.IsLarge = true;
            this.itThuQuy.Name = "itThuQuy";
            this.itThuQuy.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThuQuy_ItemClick);
            // 
            // itKhoVatTu
            // 
            tileItemElement8.Text = "Kho vật tư";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itKhoVatTu.Elements.Add(tileItemElement8);
            this.itKhoVatTu.Id = 75;
            this.itKhoVatTu.IsLarge = true;
            this.itKhoVatTu.Name = "itKhoVatTu";
            this.itKhoVatTu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKhoVatTu_ItemClick);
            // 
            // itToThoHan
            // 
            tileItemElement9.Text = "Tổ thợ hàn";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itToThoHan.Elements.Add(tileItemElement9);
            this.itToThoHan.Id = 76;
            this.itToThoHan.IsLarge = true;
            this.itToThoHan.Name = "itToThoHan";
            this.itToThoHan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itToThoHan_ItemClick);
            // 
            // tileGroup6
            // 
            this.tileGroup6.Items.Add(this.itXayDung);
            this.tileGroup6.Items.Add(this.itUTau);
            this.tileGroup6.Items.Add(this.itBaoVe);
            this.tileGroup6.Name = "tileGroup6";
            // 
            // itXayDung
            // 
            tileItemElement10.Text = "Xây dựng";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itXayDung.Elements.Add(tileItemElement10);
            this.itXayDung.Id = 77;
            this.itXayDung.IsLarge = true;
            this.itXayDung.Name = "itXayDung";
            this.itXayDung.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itXayDung_ItemClick);
            // 
            // itUTau
            // 
            tileItemElement11.Text = "Ụ tàu";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itUTau.Elements.Add(tileItemElement11);
            this.itUTau.Id = 78;
            this.itUTau.IsLarge = true;
            this.itUTau.Name = "itUTau";
            this.itUTau.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itUTau_ItemClick);
            // 
            // itBaoVe
            // 
            tileItemElement12.Text = "Bảo vệ";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itBaoVe.Elements.Add(tileItemElement12);
            this.itBaoVe.Id = 79;
            this.itBaoVe.IsLarge = true;
            this.itBaoVe.Name = "itBaoVe";
            this.itBaoVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itBaoVe_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileItem7
            // 
            tileItemElement13.Text = "tileItem7";
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.tileItem7.Elements.Add(tileItemElement13);
            this.tileItem7.Id = 70;
            this.tileItem7.IsLarge = true;
            this.tileItem7.Name = "tileItem7";
            // 
            // tileItem6
            // 
            tileItemElement14.Text = "tileItem6";
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.tileItem6.Elements.Add(tileItemElement14);
            this.tileItem6.Id = 69;
            this.tileItem6.IsLarge = true;
            this.tileItem6.Name = "tileItem6";
            // 
            // Frm_GD_Baocao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_GD_Baocao";
            this.Text = "Báo cáo";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_GD_Baocao_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itPhongHanhChinh;
        private DevExpress.XtraEditors.TileItem itPhongNhanSu;
        private DevExpress.XtraEditors.TileItem itPhongKinhDoanh;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itPhongTaiChinh;
        private DevExpress.XtraEditors.TileItem itPhongKeToan;
        private DevExpress.XtraEditors.TileItem itPhongVe;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itThuQuy;
        private DevExpress.XtraEditors.TileItem itKhoVatTu;
        private DevExpress.XtraEditors.TileItem itToThoHan;
        private DevExpress.XtraEditors.TileGroup tileGroup6;
        private DevExpress.XtraEditors.TileItem itXayDung;
        private DevExpress.XtraEditors.TileItem itUTau;
        private DevExpress.XtraEditors.TileItem itBaoVe;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem tileItem7;
        private DevExpress.XtraEditors.TileItem tileItem6;
    }
}