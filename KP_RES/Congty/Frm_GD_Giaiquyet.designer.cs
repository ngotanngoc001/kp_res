﻿namespace KP_RES 
{
    partial class Frm_GD_Giaiquyet 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itKyDuyet = new DevExpress.XtraEditors.TileItem();
            this.itDeNghiChiTien = new DevExpress.XtraEditors.TileItem();
            this.itDeNghiTamUng = new DevExpress.XtraEditors.TileItem();
            this.itDeNghiCapVatTu = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.itPhieuChi = new DevExpress.XtraEditors.TileItem();
            this.itPhieuThu = new DevExpress.XtraEditors.TileItem();
            this.itPhieuNhapKho = new DevExpress.XtraEditors.TileItem();
            this.itPhieuXuatKho = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itDuyetGiaMua = new DevExpress.XtraEditors.TileItem();
            this.itDuyetGiaBan = new DevExpress.XtraEditors.TileItem();
            this.itHopDong = new DevExpress.XtraEditors.TileItem();
            this.itQuyetDinh = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.itThongBao = new DevExpress.XtraEditors.TileItem();
            this.itKyBaoCao = new DevExpress.XtraEditors.TileItem();
            this.itDoanhThuBanVe = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 82;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itKyDuyet);
            this.tileGroup4.Items.Add(this.itDeNghiChiTien);
            this.tileGroup4.Items.Add(this.itDeNghiTamUng);
            this.tileGroup4.Items.Add(this.itDeNghiCapVatTu);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itKyDuyet
            // 
            tileItemElement1.Text = "Ký duyệt";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itKyDuyet.Elements.Add(tileItemElement1);
            this.itKyDuyet.Id = 63;
            this.itKyDuyet.IsLarge = true;
            this.itKyDuyet.Name = "itKyDuyet";
            this.itKyDuyet.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKyDuyet_ItemClick);
            // 
            // itDeNghiChiTien
            // 
            tileItemElement2.Text = "Đề nghị chi tiền";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDeNghiChiTien.Elements.Add(tileItemElement2);
            this.itDeNghiChiTien.Id = 64;
            this.itDeNghiChiTien.IsLarge = true;
            this.itDeNghiChiTien.Name = "itDeNghiChiTien";
            this.itDeNghiChiTien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDeNghiChiTien_ItemClick);
            // 
            // itDeNghiTamUng
            // 
            tileItemElement3.Text = "Đề nghị tạm ứng";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itDeNghiTamUng.Elements.Add(tileItemElement3);
            this.itDeNghiTamUng.Id = 65;
            this.itDeNghiTamUng.IsLarge = true;
            this.itDeNghiTamUng.Name = "itDeNghiTamUng";
            this.itDeNghiTamUng.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDeNghiTamUng_ItemClick);
            // 
            // itDeNghiCapVatTu
            // 
            tileItemElement4.Text = "Đề nghị cấp vật tư";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itDeNghiCapVatTu.Elements.Add(tileItemElement4);
            this.itDeNghiCapVatTu.Id = 72;
            this.itDeNghiCapVatTu.IsLarge = true;
            this.itDeNghiCapVatTu.Name = "itDeNghiCapVatTu";
            this.itDeNghiCapVatTu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDeNghiCapVatTu_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.itPhieuChi);
            this.tileGroup2.Items.Add(this.itPhieuThu);
            this.tileGroup2.Items.Add(this.itPhieuNhapKho);
            this.tileGroup2.Items.Add(this.itPhieuXuatKho);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // itPhieuChi
            // 
            tileItemElement5.Text = "Phiếu chi";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement5.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhieuChi.Elements.Add(tileItemElement5);
            this.itPhieuChi.Id = 66;
            this.itPhieuChi.IsLarge = true;
            this.itPhieuChi.Name = "itPhieuChi";
            this.itPhieuChi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhieuChi_ItemClick);
            // 
            // itPhieuThu
            // 
            tileItemElement6.Text = "Phiếu thu";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement6.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhieuThu.Elements.Add(tileItemElement6);
            this.itPhieuThu.Id = 67;
            this.itPhieuThu.IsLarge = true;
            this.itPhieuThu.Name = "itPhieuThu";
            this.itPhieuThu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhieuThu_ItemClick);
            // 
            // itPhieuNhapKho
            // 
            tileItemElement7.Text = "Phiếu nhập kho";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement7.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhieuNhapKho.Elements.Add(tileItemElement7);
            this.itPhieuNhapKho.Id = 68;
            this.itPhieuNhapKho.IsLarge = true;
            this.itPhieuNhapKho.Name = "itPhieuNhapKho";
            this.itPhieuNhapKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhieuNhapKho_ItemClick);
            // 
            // itPhieuXuatKho
            // 
            tileItemElement8.Text = "Phiếu xuất kho";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement8.TextLocation = new System.Drawing.Point(0, 0);
            this.itPhieuXuatKho.Elements.Add(tileItemElement8);
            this.itPhieuXuatKho.Id = 73;
            this.itPhieuXuatKho.IsLarge = true;
            this.itPhieuXuatKho.Name = "itPhieuXuatKho";
            this.itPhieuXuatKho.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itPhieuXuatKho_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itDuyetGiaMua);
            this.tileGroup3.Items.Add(this.itDuyetGiaBan);
            this.tileGroup3.Items.Add(this.itHopDong);
            this.tileGroup3.Items.Add(this.itQuyetDinh);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itDuyetGiaMua
            // 
            tileItemElement9.Text = "Duyệt giá mua";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, 0);
            this.itDuyetGiaMua.Elements.Add(tileItemElement9);
            this.itDuyetGiaMua.Id = 75;
            this.itDuyetGiaMua.IsLarge = true;
            this.itDuyetGiaMua.Name = "itDuyetGiaMua";
            this.itDuyetGiaMua.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDuyetGiaMua_ItemClick);
            // 
            // itDuyetGiaBan
            // 
            tileItemElement10.Text = "Duyệt giá bán";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement10.TextLocation = new System.Drawing.Point(0, 0);
            this.itDuyetGiaBan.Elements.Add(tileItemElement10);
            this.itDuyetGiaBan.Id = 76;
            this.itDuyetGiaBan.IsLarge = true;
            this.itDuyetGiaBan.Name = "itDuyetGiaBan";
            this.itDuyetGiaBan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDuyetGiaBan_ItemClick);
            // 
            // itHopDong
            // 
            tileItemElement11.Text = "Hợp đồng";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement11.TextLocation = new System.Drawing.Point(0, 0);
            this.itHopDong.Elements.Add(tileItemElement11);
            this.itHopDong.Id = 77;
            this.itHopDong.IsLarge = true;
            this.itHopDong.Name = "itHopDong";
            this.itHopDong.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itHopDong_ItemClick);
            // 
            // itQuyetDinh
            // 
            tileItemElement12.Text = "Quyết định";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(0, 0);
            this.itQuyetDinh.Elements.Add(tileItemElement12);
            this.itQuyetDinh.Id = 78;
            this.itQuyetDinh.IsLarge = true;
            this.itQuyetDinh.Name = "itQuyetDinh";
            this.itQuyetDinh.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itQuyetDinh_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.itThongBao);
            this.tileGroup5.Items.Add(this.itKyBaoCao);
            this.tileGroup5.Items.Add(this.itDoanhThuBanVe);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // itThongBao
            // 
            tileItemElement13.Text = "Thông báo";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement13.TextLocation = new System.Drawing.Point(0, 0);
            this.itThongBao.Elements.Add(tileItemElement13);
            this.itThongBao.Id = 79;
            this.itThongBao.IsLarge = true;
            this.itThongBao.Name = "itThongBao";
            this.itThongBao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itThongBao_ItemClick);
            // 
            // itKyBaoCao
            // 
            tileItemElement14.Text = "Ký báo cáo";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement14.TextLocation = new System.Drawing.Point(0, 0);
            this.itKyBaoCao.Elements.Add(tileItemElement14);
            this.itKyBaoCao.Id = 80;
            this.itKyBaoCao.IsLarge = true;
            this.itKyBaoCao.Name = "itKyBaoCao";
            this.itKyBaoCao.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itKyBaoCao_ItemClick);
            // 
            // itDoanhThuBanVe
            // 
            tileItemElement15.Text = "Doanh thu bán vé";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement15.TextLocation = new System.Drawing.Point(0, 0);
            this.itDoanhThuBanVe.Elements.Add(tileItemElement15);
            this.itDoanhThuBanVe.Id = 81;
            this.itDoanhThuBanVe.IsLarge = true;
            this.itDoanhThuBanVe.Name = "itDoanhThuBanVe";
            this.itDoanhThuBanVe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDoanhThuBanVe_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // Frm_GD_Giaiquyet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_GD_Giaiquyet";
            this.Text = "Giải quyết công việc";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_GD_Giaiquyet_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem itKyDuyet;
        private DevExpress.XtraEditors.TileItem itDeNghiChiTien;
        private DevExpress.XtraEditors.TileItem itDeNghiTamUng;
        private DevExpress.XtraEditors.TileItem itDeNghiCapVatTu;
        private DevExpress.XtraEditors.TileItem itPhieuChi;
        private DevExpress.XtraEditors.TileItem itPhieuThu;
        private DevExpress.XtraEditors.TileItem itPhieuNhapKho;
        private DevExpress.XtraEditors.TileItem itPhieuXuatKho;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itDuyetGiaMua;
        private DevExpress.XtraEditors.TileItem itDuyetGiaBan;
        private DevExpress.XtraEditors.TileItem itHopDong;
        private DevExpress.XtraEditors.TileItem itQuyetDinh;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itThongBao;
        private DevExpress.XtraEditors.TileItem itKyBaoCao;
        private DevExpress.XtraEditors.TileItem itDoanhThuBanVe;
    }
}