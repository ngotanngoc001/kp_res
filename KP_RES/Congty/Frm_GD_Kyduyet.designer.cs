﻿namespace KP_RES 
{
    partial class Frm_GD_Kyduyet 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.itDeNghiChi = new DevExpress.XtraEditors.TileItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.itUyNhiemChi = new DevExpress.XtraEditors.TileItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.itDeNghiCapVatTu = new DevExpress.XtraEditors.TileItem();
            this.itGiayRutTien = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AllowItemHover = true;
            this.tileControl1.AppearanceItem.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.tileControl1.AppearanceItem.Hovered.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 5;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(12, 8, 12, 4);
            this.tileControl1.Location = new System.Drawing.Point(2, 2);
            this.tileControl1.MaxId = 64;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileControl1.Size = new System.Drawing.Size(1016, 675);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.tileControl1_Paint);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.itDeNghiChi);
            this.tileGroup4.Items.Add(this.itDeNghiCapVatTu);
            this.tileGroup4.Name = "tileGroup4";
            // 
            // itDeNghiChi
            // 
            tileItemElement1.Text = "Đề nghị chi";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement1.TextLocation = new System.Drawing.Point(0, 0);
            this.itDeNghiChi.Elements.Add(tileItemElement1);
            this.itDeNghiChi.Id = 62;
            this.itDeNghiChi.IsLarge = true;
            this.itDeNghiChi.Name = "itDeNghiChi";
            this.itDeNghiChi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDeNghiChi_ItemClick);
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.itUyNhiemChi);
            this.tileGroup3.Items.Add(this.itGiayRutTien);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // itUyNhiemChi
            // 
            tileItemElement3.Text = "Uỷ nhiệm chi";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement3.TextLocation = new System.Drawing.Point(0, 0);
            this.itUyNhiemChi.Elements.Add(tileItemElement3);
            this.itUyNhiemChi.Id = 61;
            this.itUyNhiemChi.IsLarge = true;
            this.itUyNhiemChi.Name = "itUyNhiemChi";
            this.itUyNhiemChi.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itUyNhiemChi_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tileControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 679);
            this.panelControl1.TabIndex = 1;
            // 
            // tileGroup5
            // 
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // itDeNghiCapVatTu
            // 
            tileItemElement2.Text = "Đề nghị cấp vật tư";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement2.TextLocation = new System.Drawing.Point(0, 0);
            this.itDeNghiCapVatTu.Elements.Add(tileItemElement2);
            this.itDeNghiCapVatTu.Id = 60;
            this.itDeNghiCapVatTu.IsLarge = true;
            this.itDeNghiCapVatTu.Name = "itDeNghiCapVatTu";
            this.itDeNghiCapVatTu.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itDeNghiCapVatTu_ItemClick);
            // 
            // itGiayRutTien
            // 
            tileItemElement4.Text = "Giấy rút tiền";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileItemElement4.TextLocation = new System.Drawing.Point(0, 0);
            this.itGiayRutTien.Elements.Add(tileItemElement4);
            this.itGiayRutTien.Id = 63;
            this.itGiayRutTien.IsLarge = true;
            this.itGiayRutTien.Name = "itGiayRutTien";
            this.itGiayRutTien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.itGiayRutTien_ItemClick);
            // 
            // Frm_GD_Kyduyet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 679);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_GD_Kyduyet";
            this.Text = "Giấy tờ đã ký duyệt";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_GD_Kyduyet_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem itDeNghiChi;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem itUyNhiemChi;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem itDeNghiCapVatTu;
        private DevExpress.XtraEditors.TileItem itGiayRutTien;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
    }
}