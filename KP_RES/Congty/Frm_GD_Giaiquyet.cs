﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_GD_Giaiquyet : DevExpress.XtraEditors.XtraForm
    {
        public Frm_GD_Giaiquyet()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_GD_Giaiquyet_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itKyDuyet_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDeNghiChiTien_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDeNghiTamUng_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDeNghiCapVatTu_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhieuChi_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhieuThu_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhieuNhapKho_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhieuXuatKho_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDuyetGiaMua_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDuyetGiaBan_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itHopDong_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itQuyetDinh_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itThongBao_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itKyBaoCao_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itDoanhThuBanVe_ItemClick(object sender, TileItemEventArgs e)
        {

        } 
        
        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKyDuyet);
            cls_KP_RES.RemoveTileItem(itDeNghiChiTien);
            cls_KP_RES.RemoveTileItem(itDeNghiTamUng);
            cls_KP_RES.RemoveTileItem(itDeNghiCapVatTu);
            cls_KP_RES.RemoveTileItem(itPhieuChi);
            cls_KP_RES.RemoveTileItem(itPhieuThu);
            cls_KP_RES.RemoveTileItem(itPhieuNhapKho);
            cls_KP_RES.RemoveTileItem(itPhieuXuatKho);
            cls_KP_RES.RemoveTileItem(itDuyetGiaMua);
            cls_KP_RES.RemoveTileItem(itDuyetGiaBan);
            cls_KP_RES.RemoveTileItem(itHopDong);
            cls_KP_RES.RemoveTileItem(itQuyetDinh);
            cls_KP_RES.RemoveTileItem(itThongBao);
            cls_KP_RES.RemoveTileItem(itKyBaoCao);
            cls_KP_RES.RemoveTileItem(itDoanhThuBanVe);
        }

        private void LoadPermission()
        {
            itKyDuyet.Checked = true;
            itDeNghiChiTien.Checked = true;
            itDeNghiTamUng.Checked = true;
            itDeNghiCapVatTu.Checked = true;
            itPhieuChi.Checked = true;
            itPhieuThu.Checked = true;
            itPhieuNhapKho.Checked = true;
            itPhieuXuatKho.Checked = true;
            itDuyetGiaMua.Checked = true;
            itDuyetGiaBan.Checked = true;
            itHopDong.Checked = true;
            itQuyetDinh.Checked = true;
            itThongBao.Checked = true;
            itKyBaoCao.Checked = true;
            itDoanhThuBanVe.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        
    }
}