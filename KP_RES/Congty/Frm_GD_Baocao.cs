﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class  Frm_GD_Baocao : DevExpress.XtraEditors.XtraForm
    {
        public Frm_GD_Baocao()
        {
            InitializeComponent();
          
            LoadPermission();
            VisibleTileItem();
        }

        private void Frm_GD_Baocao_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void itPhongHanhChinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongHanhChinh.Checked)
            {
                this.Parent.GoTo<Frm_GD_Baocao_Hanhchinh>();
            }
        }

        private void itPhongNhanSu_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongKinhDoanh_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongTaiChinh_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itPhongKeToan_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itPhongKeToan.Checked)
            {
                this.Parent.GoTo<Frm_GD_Baocao_Ketoan>();
            }
        }

        private void itPhongVe_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itThuQuy_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itKhoVatTu_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itToThoHan_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itXayDung_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itUTau_ItemClick(object sender, TileItemEventArgs e)
        {

        }

        private void itBaoVe_ItemClick(object sender, TileItemEventArgs e)
        {

        } 

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itPhongHanhChinh);
            cls_KP_RES.RemoveTileItem(itPhongNhanSu);
            cls_KP_RES.RemoveTileItem(itPhongKinhDoanh);
            cls_KP_RES.RemoveTileItem(itPhongTaiChinh);
            cls_KP_RES.RemoveTileItem(itPhongKeToan);
            cls_KP_RES.RemoveTileItem(itPhongVe);
            cls_KP_RES.RemoveTileItem(itThuQuy);
            cls_KP_RES.RemoveTileItem(itKhoVatTu);
            cls_KP_RES.RemoveTileItem(itToThoHan);
            cls_KP_RES.RemoveTileItem(itXayDung);
            cls_KP_RES.RemoveTileItem(itUTau);
            cls_KP_RES.RemoveTileItem(itBaoVe);
        }

        private void LoadPermission()
        {
            itPhongHanhChinh.Checked = true;
            itPhongNhanSu.Checked = true;
            itPhongKinhDoanh.Checked = true;
            itPhongTaiChinh.Checked = true;
            itPhongKeToan.Checked = true;
            itPhongVe.Checked = true;
            itThuQuy.Checked = true;
            itKhoVatTu.Checked = true;
            itToThoHan.Checked = true;
            itXayDung.Checked = true;
            itUTau.Checked = true;
            itBaoVe.Checked = true;
        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        }

        

    }
}