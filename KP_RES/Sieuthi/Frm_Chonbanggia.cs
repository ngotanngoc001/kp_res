﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using KP_Terminal;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace KP_RES
{
    public partial class Frm_Chonbanggia : DevExpress.XtraEditors.XtraForm
    {
        public string banggia;
        public bool bThoat = false;

        public Frm_Chonbanggia()
        {
            InitializeComponent();
        }

       private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MABANGGIA as MA,TENBANGGIA as TEN" + "\n";
            sSQL += "From banggia" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "AND MODE=0" + "\n";
            sSQL += "Order by TENBANGGIA";
            DataTable  dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            banggia  = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["MA"]).ToString();
            this.Close();
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
            bThoat = true;
        }

       private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.FocusedRowHandle > 0)
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                }
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.FocusedRowHandle < gridView2.RowCount)
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                }
            }
            catch
            {
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
            }
            catch
            {
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

    }
}