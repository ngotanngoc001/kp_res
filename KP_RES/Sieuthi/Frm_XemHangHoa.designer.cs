﻿namespace KP_RES
{
    partial class Frm_XemHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_XemHangHoa));
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lblTongTien = new DevExpress.XtraEditors.LabelControl();
            this.lblNgayTao = new DevExpress.XtraEditors.LabelControl();
            this.lblMaHoaDon = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pn_listProduct = new DevExpress.XtraEditors.PanelControl();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_KHUYENMAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDowGv = new DevExpress.XtraEditors.PanelControl();
            this.btnUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnDow = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.pnBtnDeleteGv = new DevExpress.XtraEditors.PanelControl();
            this.btn_logout = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).BeginInit();
            this.pn_listProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).BeginInit();
            this.pnUpDowGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).BeginInit();
            this.pnBtnDeleteGv.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(8, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(87, 16);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Mã Hóa Đơn :";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.lblTongTien);
            this.panelControl2.Controls.Add(this.lblNgayTao);
            this.panelControl2.Controls.Add(this.lblMaHoaDon);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(650, 66);
            this.panelControl2.TabIndex = 13;
            // 
            // lblTongTien
            // 
            this.lblTongTien.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTien.Location = new System.Drawing.Point(84, 35);
            this.lblTongTien.Name = "lblTongTien";
            this.lblTongTien.Size = new System.Drawing.Size(0, 16);
            this.lblTongTien.TabIndex = 8;
            // 
            // lblNgayTao
            // 
            this.lblNgayTao.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayTao.Location = new System.Drawing.Point(391, 12);
            this.lblNgayTao.Name = "lblNgayTao";
            this.lblNgayTao.Size = new System.Drawing.Size(0, 16);
            this.lblNgayTao.TabIndex = 7;
            // 
            // lblMaHoaDon
            // 
            this.lblMaHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHoaDon.Location = new System.Drawing.Point(101, 12);
            this.lblMaHoaDon.Name = "lblMaHoaDon";
            this.lblMaHoaDon.Size = new System.Drawing.Size(0, 16);
            this.lblMaHoaDon.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(8, 35);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 16);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Tổng Tiền :";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(317, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 16);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Ngày Tạo :";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pn_listProduct);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 66);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(650, 449);
            this.panelControl1.TabIndex = 4;
            // 
            // pn_listProduct
            // 
            this.pn_listProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_listProduct.Controls.Add(this.gr_SelectProduct);
            this.pn_listProduct.Controls.Add(this.pnUpDowGv);
            this.pn_listProduct.Controls.Add(this.pnBtnDeleteGv);
            this.pn_listProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_listProduct.Location = new System.Drawing.Point(2, 2);
            this.pn_listProduct.Name = "pn_listProduct";
            this.pn_listProduct.Size = new System.Drawing.Size(646, 445);
            this.pn_listProduct.TabIndex = 1;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gr_SelectProduct.Location = new System.Drawing.Point(0, 0);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.Size = new System.Drawing.Size(613, 402);
            this.gr_SelectProduct.TabIndex = 19;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gv_SelectProduct.Appearance.Row.Options.UseFont = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 43;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.CHIETKHAU,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.TRANGTHAI,
            this.IS_COMBO,
            this.IS_INBEP,
            this.IS_KHUYENMAI});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.ShowFooter = true;
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 35;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "TT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.OptionsFilter.AllowFilter = false;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "STT", "{0:#,###0}")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 37;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsFilter.AllowFilter = false;
            this.TEN_HANGHOA.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 297;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOLUONG", "{0:#,###0}")});
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 50;
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.OptionsFilter.AllowFilter = false;
            this.GIABAN.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            this.GIABAN.Width = 80;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "CK";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}%";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.OptionsFilter.AllowFilter = false;
            this.CHIETKHAU.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 4;
            this.CHIETKHAU.Width = 80;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.OptionsFilter.AllowFilter = false;
            this.Thanh_Tien.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Thanh_Tien", "{0:#,###0}")});
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 5;
            this.Thanh_Tien.Width = 90;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.Caption = "TRANGTHAI";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // IS_KHUYENMAI
            // 
            this.IS_KHUYENMAI.Caption = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.FieldName = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.Name = "IS_KHUYENMAI";
            // 
            // pnUpDowGv
            // 
            this.pnUpDowGv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnUpDowGv.Controls.Add(this.btnUp);
            this.pnUpDowGv.Controls.Add(this.btnDow);
            this.pnUpDowGv.Controls.Add(this.btnDowLast);
            this.pnUpDowGv.Controls.Add(this.btnUpFirst);
            this.pnUpDowGv.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDowGv.Location = new System.Drawing.Point(613, 0);
            this.pnUpDowGv.Name = "pnUpDowGv";
            this.pnUpDowGv.Size = new System.Drawing.Size(33, 402);
            this.pnUpDowGv.TabIndex = 17;
            // 
            // btnUp
            // 
            this.btnUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
            this.btnUp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnUp.Location = new System.Drawing.Point(0, 70);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(33, 70);
            this.btnUp.TabIndex = 4;
            this.btnUp.Text = "simpleButton7";
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDow
            // 
            this.btnDow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDow.Image = ((System.Drawing.Image)(resources.GetObject("btnDow.Image")));
            this.btnDow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDow.Location = new System.Drawing.Point(0, 262);
            this.btnDow.Name = "btnDow";
            this.btnDow.Size = new System.Drawing.Size(33, 70);
            this.btnDow.TabIndex = 3;
            this.btnDow.Click += new System.EventHandler(this.btnDow_Click);
            // 
            // btnDowLast
            // 
            this.btnDowLast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowLast.Image = ((System.Drawing.Image)(resources.GetObject("btnDowLast.Image")));
            this.btnDowLast.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowLast.Location = new System.Drawing.Point(0, 332);
            this.btnDowLast.Name = "btnDowLast";
            this.btnDowLast.Size = new System.Drawing.Size(33, 70);
            this.btnDowLast.TabIndex = 2;
            this.btnDowLast.Text = "simpleButton4";
            this.btnDowLast.Click += new System.EventHandler(this.btnDowLast_Click);
            // 
            // btnUpFirst
            // 
            this.btnUpFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnUpFirst.Image")));
            this.btnUpFirst.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnUpFirst.Location = new System.Drawing.Point(0, 0);
            this.btnUpFirst.Name = "btnUpFirst";
            this.btnUpFirst.Size = new System.Drawing.Size(33, 70);
            this.btnUpFirst.TabIndex = 0;
            this.btnUpFirst.Text = "simpleButton1";
            this.btnUpFirst.Click += new System.EventHandler(this.btnUpFirst_Click);
            // 
            // pnBtnDeleteGv
            // 
            this.pnBtnDeleteGv.Controls.Add(this.btn_logout);
            this.pnBtnDeleteGv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBtnDeleteGv.Location = new System.Drawing.Point(0, 402);
            this.pnBtnDeleteGv.Name = "pnBtnDeleteGv";
            this.pnBtnDeleteGv.Size = new System.Drawing.Size(646, 43);
            this.pnBtnDeleteGv.TabIndex = 18;
            // 
            // btn_logout
            // 
            this.btn_logout.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_logout.Appearance.Options.UseFont = true;
            this.btn_logout.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_logout.Location = new System.Drawing.Point(544, 2);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(100, 39);
            this.btn_logout.TabIndex = 21;
            this.btn_logout.Text = "Đóng";
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // Frm_XemHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 515);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_XemHangHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xem hàng hóa";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).EndInit();
            this.pn_listProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).EndInit();
            this.pnUpDowGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).EndInit();
            this.pnBtnDeleteGv.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pn_listProduct;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraGrid.Columns.GridColumn IS_KHUYENMAI;
        private DevExpress.XtraEditors.PanelControl pnUpDowGv;
        private DevExpress.XtraEditors.SimpleButton btnUp;
        private DevExpress.XtraEditors.SimpleButton btnDow;
        private DevExpress.XtraEditors.SimpleButton btnDowLast;
        private DevExpress.XtraEditors.SimpleButton btnUpFirst;
        private DevExpress.XtraEditors.PanelControl pnBtnDeleteGv;
        private DevExpress.XtraEditors.SimpleButton btn_logout;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblMaHoaDon;
        private DevExpress.XtraEditors.LabelControl lblTongTien;
        private DevExpress.XtraEditors.LabelControl lblNgayTao;
    }
}