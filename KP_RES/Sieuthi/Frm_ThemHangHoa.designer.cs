﻿namespace KP_RES
{
    partial class Frm_ThemHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ThemHangHoa));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThoát = new DevExpress.XtraEditors.SimpleButton();
            this.cboNhomhang = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNhom = new DevExpress.XtraEditors.LabelControl();
            this.lblTENHANGHOA = new DevExpress.XtraEditors.LabelControl();
            this.txtTENHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.lblDVT = new DevExpress.XtraEditors.LabelControl();
            this.cboDonvitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.txtGIABAN1 = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaBan1 = new DevExpress.XtraEditors.LabelControl();
            this.lbMa = new DevExpress.XtraEditors.LabelControl();
            this.txtMAHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.txtMAVACH = new DevExpress.XtraEditors.TextEdit();
            this.lbMaVach = new DevExpress.XtraEditors.LabelControl();
            this.chkMayinbep = new DevExpress.XtraEditors.CheckEdit();
            this.chkTONKHO = new DevExpress.XtraEditors.CheckEdit();
            this.cboBep = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTONKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnThoát);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 327);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(366, 41);
            this.panelControl1.TabIndex = 16;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Appearance.Options.UseForeColor = true;
            this.btnLuu.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLuu.Location = new System.Drawing.Point(179, 2);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(94, 37);
            this.btnLuu.TabIndex = 0;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnThoát
            // 
            this.btnThoát.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoát.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnThoát.Appearance.Options.UseFont = true;
            this.btnThoát.Appearance.Options.UseForeColor = true;
            this.btnThoát.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnThoát.Location = new System.Drawing.Point(273, 2);
            this.btnThoát.Name = "btnThoát";
            this.btnThoát.Size = new System.Drawing.Size(91, 37);
            this.btnThoát.TabIndex = 1;
            this.btnThoát.Text = "Đóng";
            this.btnThoát.Click += new System.EventHandler(this.btnThoát_Click);
            // 
            // cboNhomhang
            // 
            this.cboNhomhang.EnterMoveNextControl = true;
            this.cboNhomhang.Location = new System.Drawing.Point(96, 12);
            this.cboNhomhang.Name = "cboNhomhang";
            this.cboNhomhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.Appearance.Options.UseFont = true;
            this.cboNhomhang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhomhang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhomhang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhomhang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboNhomhang.Properties.DisplayMember = "TEN";
            this.cboNhomhang.Properties.DropDownItemHeight = 40;
            this.cboNhomhang.Properties.NullText = "";
            this.cboNhomhang.Properties.ShowHeader = false;
            this.cboNhomhang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhomhang.Properties.ValueMember = "MA";
            this.cboNhomhang.Size = new System.Drawing.Size(253, 26);
            this.cboNhomhang.TabIndex = 1;
            // 
            // lbNhom
            // 
            this.lbNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhom.Location = new System.Drawing.Point(6, 15);
            this.lbNhom.Margin = new System.Windows.Forms.Padding(4);
            this.lbNhom.Name = "lbNhom";
            this.lbNhom.Size = new System.Drawing.Size(43, 19);
            this.lbNhom.TabIndex = 0;
            this.lbNhom.Text = "Nhóm";
            // 
            // lblTENHANGHOA
            // 
            this.lblTENHANGHOA.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENHANGHOA.Location = new System.Drawing.Point(6, 47);
            this.lblTENHANGHOA.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENHANGHOA.Name = "lblTENHANGHOA";
            this.lblTENHANGHOA.Size = new System.Drawing.Size(27, 19);
            this.lblTENHANGHOA.TabIndex = 2;
            this.lblTENHANGHOA.Text = "Tên";
            // 
            // txtTENHANGHOA
            // 
            this.txtTENHANGHOA.EnterMoveNextControl = true;
            this.txtTENHANGHOA.Location = new System.Drawing.Point(96, 44);
            this.txtTENHANGHOA.Name = "txtTENHANGHOA";
            this.txtTENHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtTENHANGHOA.Size = new System.Drawing.Size(253, 26);
            this.txtTENHANGHOA.TabIndex = 3;
            // 
            // lblDVT
            // 
            this.lblDVT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Location = new System.Drawing.Point(6, 202);
            this.lblDVT.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Size = new System.Drawing.Size(31, 19);
            this.lblDVT.TabIndex = 11;
            this.lblDVT.Text = "ĐVT";
            // 
            // cboDonvitinh
            // 
            this.cboDonvitinh.EnterMoveNextControl = true;
            this.cboDonvitinh.Location = new System.Drawing.Point(96, 199);
            this.cboDonvitinh.Name = "cboDonvitinh";
            this.cboDonvitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.Appearance.Options.UseFont = true;
            this.cboDonvitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDonvitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDonvitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDonvitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboDonvitinh.Properties.DisplayMember = "TEN";
            this.cboDonvitinh.Properties.DropDownItemHeight = 40;
            this.cboDonvitinh.Properties.NullText = "";
            this.cboDonvitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDonvitinh.Properties.ShowHeader = false;
            this.cboDonvitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDonvitinh.Properties.ValueMember = "MA";
            this.cboDonvitinh.Size = new System.Drawing.Size(253, 26);
            this.cboDonvitinh.TabIndex = 12;
            // 
            // txtGIABAN1
            // 
            this.txtGIABAN1.EnterMoveNextControl = true;
            this.txtGIABAN1.Location = new System.Drawing.Point(96, 76);
            this.txtGIABAN1.Name = "txtGIABAN1";
            this.txtGIABAN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN1.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN1.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.Mask.EditMask = "N0";
            this.txtGIABAN1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN1.Size = new System.Drawing.Size(253, 26);
            this.txtGIABAN1.TabIndex = 5;
            // 
            // lbGiaBan1
            // 
            this.lbGiaBan1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaBan1.Location = new System.Drawing.Point(6, 79);
            this.lbGiaBan1.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaBan1.Name = "lbGiaBan1";
            this.lbGiaBan1.Size = new System.Drawing.Size(54, 19);
            this.lbGiaBan1.TabIndex = 4;
            this.lbGiaBan1.Text = "Giá bán";
            // 
            // lbMa
            // 
            this.lbMa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMa.Location = new System.Drawing.Point(6, 138);
            this.lbMa.Margin = new System.Windows.Forms.Padding(4);
            this.lbMa.Name = "lbMa";
            this.lbMa.Size = new System.Drawing.Size(47, 19);
            this.lbMa.TabIndex = 7;
            this.lbMa.Text = "Mã HH";
            // 
            // txtMAHANGHOA
            // 
            this.txtMAHANGHOA.EnterMoveNextControl = true;
            this.txtMAHANGHOA.Location = new System.Drawing.Point(96, 135);
            this.txtMAHANGHOA.Name = "txtMAHANGHOA";
            this.txtMAHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtMAHANGHOA.Size = new System.Drawing.Size(253, 26);
            this.txtMAHANGHOA.TabIndex = 8;
            // 
            // txtMAVACH
            // 
            this.txtMAVACH.EnterMoveNextControl = true;
            this.txtMAVACH.Location = new System.Drawing.Point(96, 167);
            this.txtMAVACH.Name = "txtMAVACH";
            this.txtMAVACH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAVACH.Properties.Appearance.Options.UseFont = true;
            this.txtMAVACH.Size = new System.Drawing.Size(253, 26);
            this.txtMAVACH.TabIndex = 10;
            // 
            // lbMaVach
            // 
            this.lbMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaVach.Location = new System.Drawing.Point(6, 170);
            this.lbMaVach.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaVach.Name = "lbMaVach";
            this.lbMaVach.Size = new System.Drawing.Size(57, 19);
            this.lbMaVach.TabIndex = 9;
            this.lbMaVach.Text = "Mã vạch";
            // 
            // chkMayinbep
            // 
            this.chkMayinbep.EnterMoveNextControl = true;
            this.chkMayinbep.Location = new System.Drawing.Point(94, 261);
            this.chkMayinbep.Name = "chkMayinbep";
            this.chkMayinbep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMayinbep.Properties.Appearance.Options.UseFont = true;
            this.chkMayinbep.Properties.Caption = "Máy in bếp";
            this.chkMayinbep.Size = new System.Drawing.Size(104, 24);
            this.chkMayinbep.TabIndex = 14;
            // 
            // chkTONKHO
            // 
            this.chkTONKHO.EnterMoveNextControl = true;
            this.chkTONKHO.Location = new System.Drawing.Point(94, 231);
            this.chkTONKHO.Name = "chkTONKHO";
            this.chkTONKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTONKHO.Properties.Appearance.Options.UseFont = true;
            this.chkTONKHO.Properties.Caption = "Tồn kho";
            this.chkTONKHO.Size = new System.Drawing.Size(92, 24);
            this.chkTONKHO.TabIndex = 13;
            // 
            // cboBep
            // 
            this.cboBep.EnterMoveNextControl = true;
            this.cboBep.Location = new System.Drawing.Point(96, 291);
            this.cboBep.Name = "cboBep";
            this.cboBep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.Appearance.Options.UseFont = true;
            this.cboBep.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBep.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboBep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBep.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "TEN_CUAHANG"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAYINBEP", "Máy in")});
            this.cboBep.Properties.DisplayMember = "TEN";
            this.cboBep.Properties.DropDownItemHeight = 40;
            this.cboBep.Properties.NullText = "";
            this.cboBep.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboBep.Properties.ShowHeader = false;
            this.cboBep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboBep.Properties.ValueMember = "MA";
            this.cboBep.Size = new System.Drawing.Size(258, 26);
            this.cboBep.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(0, 109);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(372, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "--------------------------------------------------------------";
            // 
            // Frm_ThemHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 368);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.chkMayinbep);
            this.Controls.Add(this.chkTONKHO);
            this.Controls.Add(this.cboBep);
            this.Controls.Add(this.lbMa);
            this.Controls.Add(this.txtMAHANGHOA);
            this.Controls.Add(this.txtMAVACH);
            this.Controls.Add(this.lbMaVach);
            this.Controls.Add(this.txtGIABAN1);
            this.Controls.Add(this.lbGiaBan1);
            this.Controls.Add(this.cboNhomhang);
            this.Controls.Add(this.lbNhom);
            this.Controls.Add(this.lblTENHANGHOA);
            this.Controls.Add(this.txtTENHANGHOA);
            this.Controls.Add(this.lblDVT);
            this.Controls.Add(this.cboDonvitinh);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ThemHangHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm hàng hóa";
            this.Load += new System.EventHandler(this.Frm_ThemHangHoa_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_ThemHangHoa_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTONKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnThoát;
        private DevExpress.XtraEditors.LookUpEdit cboNhomhang;
        private DevExpress.XtraEditors.LabelControl lbNhom;
        private DevExpress.XtraEditors.LabelControl lblTENHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtTENHANGHOA;
        private DevExpress.XtraEditors.LabelControl lblDVT;
        private DevExpress.XtraEditors.LookUpEdit cboDonvitinh;
        private DevExpress.XtraEditors.TextEdit txtGIABAN1;
        private DevExpress.XtraEditors.LabelControl lbGiaBan1;
        private DevExpress.XtraEditors.LabelControl lbMa;
        private DevExpress.XtraEditors.TextEdit txtMAHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtMAVACH;
        private DevExpress.XtraEditors.LabelControl lbMaVach;
        private DevExpress.XtraEditors.CheckEdit chkMayinbep;
        private DevExpress.XtraEditors.CheckEdit chkTONKHO;
        private DevExpress.XtraEditors.LookUpEdit cboBep;
        private DevExpress.XtraEditors.LabelControl labelControl1;

    }
}