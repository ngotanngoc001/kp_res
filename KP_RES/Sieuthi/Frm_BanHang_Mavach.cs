﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Xml;
using System.IO;
using System.Collections;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using Microsoft.Win32;
using DevExpress.XtraBars.Helpers;
using System.Xml.Linq;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils;
using DevExpress.LookAndFeel;
using KP_Terminal;
using System.Security.Cryptography;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using KP_UserManagement;
using System.Diagnostics;
using DevExpress.XtraTab;
using KP_Report;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_BanHang_Mavach : DevExpress.XtraEditors.XtraForm
    {
        DataTable myDTHH = new DataTable("HANGHOA"); 
        DataTable myDTNhom = new DataTable();
        DataTable myDTMon = new DataTable();
        int cPageDish = 1;
        int cPageMon = 1;
        String sMaMon = "";
        int sMaKV = 1;
        int sMaNH = 1;
        bool Flag_soluong = false;
        int PageMon = 15;
        int PageNhom = 15;
        decimal NumberOfPages;
        decimal NumberOfPagesNhom;
        public int denSoNhom;
        bool testID = false;
        public int index = 5;
        public int fountion = 0;
        public int sale_price = 0;
        public bool exitReprint = false;
        public string idBill = "";
        DataTable divide_Table1 = new DataTable();
        DataTable divide_Table2 = new DataTable();

        public delegate void GetString(DataTable dtshow, int ctrol, int iFocusRow);
        public GetString MyGetData;
        public int contrDelegate = 0;
        public int iFocusRow = 0;

        public Frm_BanHang_Mavach()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
        }
        
        private void Frm_BanHang_Mavach_Load(object sender, EventArgs e)
        {
            LoadPermission();
            AddColumnDatatable();

            btn_5cot_Click(null, null);
            btn_ThuNgan_Click(null, null);

            pnBanHang.BringToFront();
            pnViewProduct.BringToFront();
            pn_banphim_so.BringToFront();

            txt_Mahang.Focus();
            SetTextbox(txt_Mahang);
           
        }

        private void Frm_BanHang_Mavach_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (gVDSHangMua.RowCount > 0)
                {
                    btn_Payments_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F2)
            {
                btnLaytrongluong_Click (null, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btnLogOut_Click(null, null);
            }
        }

#region Dùng Chung

        private TextEdit _textbox;
        public void SetTextbox(TextEdit edit)
        {
            _textbox = edit;
        }

        private void Click_buttonNumber(string number)
        {
            int select = _textbox.SelectionStart;
            if (_textbox.SelectedText == _textbox.Text)
            {
                _textbox.Text = "";
            }
            _textbox.Text = _textbox.Text.Insert(_textbox.SelectionStart, number);
            _textbox.Focus();
            if (number.Length == 3)
                _textbox.SelectionStart = select + 3;
            else
            {
                if (number.Length == 2)
                    _textbox.SelectionStart = select + 2;
                else
                    _textbox.SelectionStart = select + 1;
            }
            _textbox.SelectionLength = 0;
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void DeleteTextBox()
        {
            int iPosition = _textbox.SelectionStart;
            int iLenght = _textbox.Text.Length;
            if (iPosition > 1)
            {
                if (_textbox.Text[iPosition - 1].ToString() == ",")
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 2, 1);

                    _textbox.SelectionStart = iPosition - 2;

                }
                else
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                    if (iLenght - _textbox.Text.Length == 1)
                    {
                        _textbox.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition - 2;
                    }
                }
            }
            else
                if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                        _textbox.SelectionStart = iPosition - 1;
                        if (_textbox.Text == "0")
                            _textbox.SelectionStart = 1;
                    }
                    else
                    {
                        _textbox.Text = "0";
                        _textbox.SelectionStart = 1;
                    }
                }
        }

        private void LoadPermission()
        {
            btn_Payments.Enabled = clsUserManagement.AllowAdd("22041701");
            btn_PaymentsChar.Enabled = clsUserManagement.AllowAdd("22041701");
            btnInVeOnline.Enabled = clsUserManagement.AllowAdd("22041701");
            btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("22041701");
            btn_HuyHoaDon.Enabled = clsUserManagement.AllowDelete("22041701");
            btnTraHang.Enabled = clsUserManagement.AllowDelete("22041701");
        }

        private void AddColumnDatatable()
        {
            myDTHH.Columns.Add("MA_VACH", Type.GetType("System.String"));
            myDTHH.Columns.Add("MA_HANGHOA", typeof(Int32));
            myDTHH.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            myDTHH.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            myDTHH.Columns.Add("GIABAN", Type.GetType("System.Double"));
            myDTHH.Columns.Add("GHICHU", Type.GetType("System.String"));
            myDTHH.Columns.Add("MA_BEP", typeof(Int32));
            myDTHH.Columns.Add("THUE", Type.GetType("System.Double"));
            myDTHH.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));
            myDTHH.Columns.Add("TRANGTHAI", typeof(bool));
            myDTHH.Columns.Add("IS_COMBO", typeof(bool));
            myDTHH.Columns.Add("IS_INBEP", typeof(bool));
            myDTHH.Columns.Add("IS_KHUYENMAI", typeof(bool));
            myDTHH.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));

            divide_Table1.Columns.Add("MA_HOADON", Type.GetType("System.String"));
            divide_Table1.Columns.Add("MA_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("THANHTIEN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("THUE", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("IS_COMBO", typeof(bool));

            divide_Table2.Columns.Add("MA_HOADON", Type.GetType("System.String"));
            divide_Table2.Columns.Add("MA_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("THANHTIEN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("THUE", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("IS_COMBO", typeof(bool));
        }

        #endregion

#region Nhóm - Món
        private void btn_5cot_Click(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_5cot);
            SetDefautlBackgroud(btn_7cot);
            SetDefautlBackgroud(btn_9Cot);
            if (index != 5)
            {
                cPageMon = 1;
                cPageDish = 1;
            }
            index = 5;
            SetWidthParentAndPageSize();
            LoadSizeKeyNumber();
            GetList20Product();
        }

        private void btn_7cot_Click(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_7cot);
            SetDefautlBackgroud(btn_5cot);
            SetDefautlBackgroud(btn_9Cot);
            if (index != 7)
            {
                cPageMon = 1;
                cPageDish = 1;
            }
            index = 7;
            SetWidthParentAndPageSize();
            LoadSizeKeyNumber();
            GetList20Product();
        }

        private void btn_9Cot_Click(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_9Cot);
            SetDefautlBackgroud(btn_7cot);
            SetDefautlBackgroud(btn_5cot);
            if (index != 10)
            {
                cPageMon = 1;
                cPageDish = 1;
            }
            index = 10;
            SetWidthParentAndPageSize();
            LoadSizeKeyNumber();
            GetList20Product();
        }

        private void btn_Last_Click(object sender, EventArgs e)
        {
            cPageMon = Convert.ToInt32(NumberOfPages);
            GetList20Product();
        }

        private void btn_Next_Click(object sender, EventArgs e)
        {
            cPageMon += 1;
            GetList20Product();
        }

        private void btn_Prev_Click(object sender, EventArgs e)
        {
            cPageMon -= 1;
            GetList20Product();
        }

        private void btn_First_Click(object sender, EventArgs e)
        {
            cPageMon = 1;
            GetList20Product();
        }

        private void txtScrolNhom_Click(object sender, EventArgs e)
        {
            GetGroup();
            cPageMon = 1;
            pnCtrlNgNhPhNhom.BringToFront();
            gr_ChonNhom.BringToFront();
            txtSarech.Text = "Nhập tên hàng hóa...";
        }

        private void btn_NextNhom_Click(object sender, EventArgs e)
        {
            btnBackNhom.Enabled = true;
            if (denSoNhom > cPageDish)
            {
                cPageDish = cPageDish + 1;
                GetGroup();
            }
            else
                btn_NextNhom.Enabled = false;
        }

        private void btnBackNhom_Click(object sender, EventArgs e)
        {
            btn_NextNhom.Enabled = true;
            if (1 < cPageDish && cPageDish <= denSoNhom)
            {
                cPageDish = cPageDish - 1;
                GetGroup();
            }
            else
                btnBackNhom.Enabled = false;
        }

        private void LoadSizeKeyNumber()
        {
            int Size = int.Parse(pn_bigparent.Width.ToString());
            if (Size <= 405)
            {
                #region key board number Size 5

                btn_7.Location = new Point(5, 4);
                btn_7.Size = new Size(60, 55);
                btn_8.Location = new Point(71, 4);
                btn_8.Size = new Size(60, 55);
                btn_9.Location = new Point(137, 4);
                btn_9.Size = new Size(60, 55);
                btn_ESC.Location = new Point(203, 4);
                btn_ESC.Size = new Size(60, 55);
                btnLeft.Location = new Point(269, 4);
                btnLeft.Size = new Size(60, 55);
                btnRight.Location = new Point(335, 4);
                btnRight.Size = new Size(60, 55);

                btn_4.Location = new Point(5, 66);
                btn_4.Size = new Size(60, 55);
                btn_5.Location = new Point(71, 66);
                btn_5.Size = new Size(60, 55);
                btn_6.Location = new Point(137, 66);
                btn_6.Size = new Size(60, 55);
                btn_delete.Location = new Point(203, 66);
                btn_delete.Size = new Size(60, 55);
                btn_enter.Location = new Point(269, 66);
                btn_enter.Size = new Size(126, 55);

                btn_1.Location = new Point(5, 128);
                btn_1.Size = new Size(60, 55);
                btn_2.Location = new Point(71, 128);
                btn_2.Size = new Size(60, 55);
                btn_3.Location = new Point(137, 128);
                btn_3.Size = new Size(60, 55);
                btn_000.Location = new Point(203, 128);
                btn_000.Size = new Size(60, 55);

                btn_ABC.Location = new Point(5, 190);
                btn_ABC.Size = new Size(60, 55);
                btn_dot.Location = new Point(71, 190);
                btn_dot.Size = new Size(60, 55);
                btn_0.Location = new Point(137, 190);
                btn_0.Size = new Size(60, 55);
                btn_00.Location = new Point(203, 190);
                btn_00.Size = new Size(60, 55);

                btn_Payments.Location = new Point(269, 128);
                btn_Payments.Size = new Size(126, 117);

                #endregion
                #region Key board character Size 5
                btn_Q.Location = new Point(0, 7);
                btn_Q.Size = new Size(40, 45);
                btn_W.Location = new Point(40, 7);
                btn_W.Size = new Size(40, 45);
                btn_E.Location = new Point(80, 7);
                btn_E.Size = new Size(40, 45);
                btn_R.Location = new Point(120, 7);
                btn_R.Size = new Size(40, 45);
                btn_T.Location = new Point(160, 7);
                btn_T.Size = new Size(40, 45);
                btn_Y.Location = new Point(200, 7);
                btn_Y.Size = new Size(40, 45);
                btn_U.Location = new Point(240, 7);
                btn_U.Size = new Size(40, 45);
                btn_I.Location = new Point(280, 7);
                btn_I.Size = new Size(40, 45);
                btn_O.Location = new Point(320, 7);
                btn_O.Size = new Size(40, 45);
                btn_P.Location = new Point(360, 7);
                btn_P.Size = new Size(40, 45);

                btn_A.Location = new Point(11, 65);
                btn_A.Size = new Size(40, 45);
                btn_S.Location = new Point(53, 65);
                btn_S.Size = new Size(40, 45);
                btn_D.Location = new Point(94, 65);
                btn_D.Size = new Size(40, 45);
                btn_F.Location = new Point(135, 65);
                btn_F.Size = new Size(40, 45);
                btn_G.Location = new Point(176, 65);
                btn_G.Size = new Size(40, 45);
                btn_H.Location = new Point(217, 65);
                btn_H.Size = new Size(40, 45);
                btn_J.Location = new Point(258, 65);
                btn_J.Size = new Size(40, 45);
                btn_K.Location = new Point(299, 65);
                btn_K.Size = new Size(40, 45);
                btn_L.Location = new Point(340, 65);
                btn_L.Size = new Size(40, 45);

                btn_Z.Location = new Point(0, 124);
                btn_Z.Size = new Size(40, 45);
                btn_X.Location = new Point(45, 124);
                btn_X.Size = new Size(40, 45);
                btn_C.Location = new Point(90, 124);
                btn_C.Size = new Size(40, 45);
                btn_V.Location = new Point(135, 124);
                btn_V.Size = new Size(40, 45);
                btn_B.Location = new Point(180, 124);
                btn_B.Size = new Size(40, 45);
                btn_N.Location = new Point(225, 124);
                btn_N.Size = new Size(40, 45);
                btn_M.Location = new Point(270, 124);
                btn_M.Size = new Size(40, 45);
                btn_backchar.Location = new Point(315, 124);
                btn_backchar.Size = new Size(85, 45);

                btn_ESCChar.Location = new Point(346, 175);
                btn_ESCChar.Size = new Size(50, 73);
                btn_PaymentsChar.Location = new Point(64, 175);
                btn_PaymentsChar.Size = new Size(125, 73);
                btn_OKChar.Location = new Point(195, 175);
                btn_OKChar.Size = new Size(85, 74);
                btn_123.Location = new Point(0, 175);
                btn_123.Size = new Size(60, 73);
                btn_DeleteChar.Location = new Point(286, 175);
                btn_DeleteChar.Size = new Size(56, 73);
                #endregion
            }
            if (Size > 405 && Size <= 570)
            {
                #region key board number Size 7

                btn_7.Location = new Point(5, 4);
                btn_7.Size = new Size(87, 55);
                btn_8.Location = new Point(97, 4);
                btn_8.Size = new Size(87, 55);
                btn_9.Location = new Point(190, 4);
                btn_9.Size = new Size(87, 55);
                btn_ESC.Location = new Point(283, 4);
                btn_ESC.Size = new Size(87, 55);
                btnLeft.Location = new Point(376, 4);
                btnLeft.Size = new Size(87, 55);
                btnRight.Location = new Point(470, 4);
                btnRight.Size = new Size(87, 55);

                btn_4.Location = new Point(5, 66);
                btn_4.Size = new Size(87, 55);
                btn_5.Location = new Point(97, 66);
                btn_5.Size = new Size(87, 55);
                btn_6.Location = new Point(190, 66);
                btn_6.Size = new Size(87, 55);
                btn_delete.Location = new Point(283, 66);
                btn_delete.Size = new Size(87, 55);
                btn_enter.Location = new Point(376, 66);
                btn_enter.Size = new Size(181, 55);

                btn_1.Location = new Point(5, 128);
                btn_1.Size = new Size(87, 55);
                btn_2.Location = new Point(97, 128);
                btn_2.Size = new Size(87, 55);
                btn_3.Location = new Point(190, 128);
                btn_3.Size = new Size(87, 55);
                btn_000.Location = new Point(283, 128);
                btn_000.Size = new Size(87, 55);

                btn_ABC.Location = new Point(5, 190);
                btn_ABC.Size = new Size(87, 55);
                btn_dot.Location = new Point(97, 190);
                btn_dot.Size = new Size(87, 55);
                btn_0.Location = new Point(190, 190);
                btn_0.Size = new Size(87, 55);
                btn_00.Location = new Point(283, 190);
                btn_00.Size = new Size(87, 55);

                btn_Payments.Location = new Point(376, 128);
                btn_Payments.Size = new Size(181, 117);

                #endregion
                #region Key board character Size 7
                btn_Q.Location = new Point(0, 7);
                btn_Q.Size = new Size(55, 45);
                btn_W.Location = new Point(56, 7);
                btn_W.Size = new Size(55, 45);
                btn_E.Location = new Point(113, 7);
                btn_E.Size = new Size(55, 45);
                btn_R.Location = new Point(170, 7);
                btn_R.Size = new Size(55, 45);
                btn_T.Location = new Point(227, 7);
                btn_T.Size = new Size(55, 45);
                btn_Y.Location = new Point(284, 7);
                btn_Y.Size = new Size(55, 45);
                btn_U.Location = new Point(341, 7);
                btn_U.Size = new Size(55, 45);
                btn_I.Location = new Point(398, 7);
                btn_I.Size = new Size(55, 45);
                btn_O.Location = new Point(454, 7);
                btn_O.Size = new Size(55, 45);
                btn_P.Location = new Point(510, 7);
                btn_P.Size = new Size(55, 45);

                btn_A.Location = new Point(11, 65);
                btn_A.Size = new Size(55, 45);
                btn_S.Location = new Point(71, 65);
                btn_S.Size = new Size(55, 45);
                btn_D.Location = new Point(131, 65);
                btn_D.Size = new Size(55, 45);
                btn_F.Location = new Point(191, 65);
                btn_F.Size = new Size(55, 45);
                btn_G.Location = new Point(251, 65);
                btn_G.Size = new Size(55, 45);
                btn_H.Location = new Point(311, 65);
                btn_H.Size = new Size(55, 45);
                btn_J.Location = new Point(371, 65);
                btn_J.Size = new Size(55, 45);
                btn_K.Location = new Point(431, 65);
                btn_K.Size = new Size(55, 45);
                btn_L.Location = new Point(491, 65);
                btn_L.Size = new Size(55, 45);

                btn_Z.Location = new Point(0, 124);
                btn_Z.Size = new Size(60, 45);
                btn_X.Location = new Point(65, 124);
                btn_X.Size = new Size(60, 45);
                btn_C.Location = new Point(130, 124);
                btn_C.Size = new Size(60, 45);
                btn_V.Location = new Point(195, 124);
                btn_V.Size = new Size(60, 45);
                btn_B.Location = new Point(260, 124);
                btn_B.Size = new Size(60, 45);
                btn_N.Location = new Point(325, 124);
                btn_N.Size = new Size(60, 45);
                btn_M.Location = new Point(390, 124);
                btn_M.Size = new Size(60, 45);
                btn_backchar.Location = new Point(460, 124);
                btn_backchar.Size = new Size(95, 45);

                btn_ESCChar.Location = new Point(497, 183);
                btn_ESCChar.Size = new Size(60, 62);
                btn_PaymentsChar.Location = new Point(65, 183);
                btn_PaymentsChar.Size = new Size(200, 62);
                btn_OKChar.Location = new Point(270, 183);
                btn_OKChar.Size = new Size(150, 62);
                btn_123.Location = new Point(0, 183);
                btn_123.Size = new Size(60, 62);
                btn_DeleteChar.Location = new Point(428, 183);
                btn_DeleteChar.Size = new Size(60, 62);

                #endregion
            }
            if (Size > 570 && Size <= 720)
            {
                #region key board number Size 9

                btn_7.Location = new Point(5, 4);
                btn_7.Size = new Size(111, 55);
                btn_8.Location = new Point(123, 4);
                btn_8.Size = new Size(111, 55);
                btn_9.Location = new Point(241, 4);
                btn_9.Size = new Size(111, 55);
                btn_ESC.Location = new Point(359, 4);
                btn_ESC.Size = new Size(111, 55);
                btnLeft.Location = new Point(477, 4);
                btnLeft.Size = new Size(111, 55);
                btnRight.Location = new Point(596, 4);
                btnRight.Size = new Size(111, 55);

                btn_4.Location = new Point(5, 66);
                btn_4.Size = new Size(111, 55);
                btn_5.Location = new Point(123, 66);
                btn_5.Size = new Size(111, 55);
                btn_6.Location = new Point(241, 66);
                btn_6.Size = new Size(111, 55);
                btn_delete.Location = new Point(359, 66);
                btn_delete.Size = new Size(111, 55);
                btn_enter.Location = new Point(477, 66);
                btn_enter.Size = new Size(230, 55);

                btn_1.Location = new Point(5, 128);
                btn_1.Size = new Size(111, 55);
                btn_2.Location = new Point(123, 128);
                btn_2.Size = new Size(111, 55);
                btn_3.Location = new Point(241, 128);
                btn_3.Size = new Size(111, 55);
                btn_000.Location = new Point(359, 128);
                btn_000.Size = new Size(111, 55);

                btn_ABC.Location = new Point(5, 190);
                btn_ABC.Size = new Size(111, 55);
                btn_dot.Location = new Point(123, 190);
                btn_dot.Size = new Size(111, 55);
                btn_0.Location = new Point(241, 190);
                btn_0.Size = new Size(111, 55);
                btn_00.Location = new Point(359, 190);
                btn_00.Size = new Size(111, 55);

                //btnDatCoc.Location = new Point(477, 128);
                //btnDatCoc.Size = new Size(230, 55);
                btn_Payments.Location = new Point(477, 128);
                btn_Payments.Size = new Size(230, 117);

                #endregion
                #region Key board character Size 9
                btn_Q.Location = new Point(0, 7);
                btn_Q.Size = new Size(70, 45);
                btn_W.Location = new Point(71, 7);
                btn_W.Size = new Size(70, 45);
                btn_E.Location = new Point(142, 7);
                btn_E.Size = new Size(70, 45);
                btn_R.Location = new Point(213, 7);
                btn_R.Size = new Size(70, 45);
                btn_T.Location = new Point(284, 7);
                btn_T.Size = new Size(70, 45);
                btn_Y.Location = new Point(355, 7);
                btn_Y.Size = new Size(70, 45);
                btn_U.Location = new Point(426, 7);
                btn_U.Size = new Size(70, 45);
                btn_I.Location = new Point(497, 7);
                btn_I.Size = new Size(70, 45);
                btn_O.Location = new Point(568, 7);
                btn_O.Size = new Size(70, 45);
                btn_P.Location = new Point(640, 7);
                btn_P.Size = new Size(70, 45);

                btn_A.Location = new Point(10, 65);
                btn_A.Size = new Size(74, 45);
                btn_S.Location = new Point(86, 65);
                btn_S.Size = new Size(74, 45);
                btn_D.Location = new Point(162, 65);
                btn_D.Size = new Size(74, 45);
                btn_F.Location = new Point(238, 65);
                btn_F.Size = new Size(74, 45);
                btn_G.Location = new Point(314, 65);
                btn_G.Size = new Size(74, 45);
                btn_H.Location = new Point(390, 65);
                btn_H.Size = new Size(74, 45);
                btn_J.Location = new Point(466, 65);
                btn_J.Size = new Size(74, 45);
                btn_K.Location = new Point(542, 65);
                btn_K.Size = new Size(74, 45);
                btn_L.Location = new Point(618, 65);
                btn_L.Size = new Size(74, 45);

                btn_Z.Location = new Point(31, 124);
                btn_Z.Size = new Size(80, 45);
                btn_X.Location = new Point(113, 124);
                btn_X.Size = new Size(80, 45);
                btn_C.Location = new Point(195, 124);
                btn_C.Size = new Size(80, 45);
                btn_V.Location = new Point(277, 124);
                btn_V.Size = new Size(80, 45);
                btn_B.Location = new Point(359, 124);
                btn_B.Size = new Size(80, 45);
                btn_N.Location = new Point(441, 124);
                btn_N.Size = new Size(80, 45);
                btn_M.Location = new Point(523, 124);
                btn_M.Size = new Size(80, 45);
                btn_backchar.Location = new Point(605, 124);
                btn_backchar.Size = new Size(95, 45);

                btn_ESCChar.Location = new Point(635, 183);
                btn_ESCChar.Size = new Size(78, 62);
                btn_PaymentsChar.Location = new Point(95, 183);
                btn_PaymentsChar.Size = new Size(200, 62);
                btn_OKChar.Location = new Point(310, 183);
                btn_OKChar.Size = new Size(220, 62);
                btn_123.Location = new Point(0, 183);
                btn_123.Size = new Size(80, 62);
                btn_DeleteChar.Location = new Point(545, 183);
                btn_DeleteChar.Size = new Size(80, 62);

                #endregion
            }

        }

        private void SetWidthParentAndPageSize()
        {
            switch (index)
            {
                case 5:
                    PageMon = 20;
                    PageNhom = 24;
                    break;
                case 7:
                    PageMon = 30;
                    PageNhom = 36;
                    break;
                case 10:
                    PageMon = 40;
                    PageNhom = 48;
                    break;
            }
            pnctrlParent.Controls.Clear();
            if (index == 5)
            {
                pn_bigparent.Width = 405;

                pnctrlParent.Width = 405;
                pnCtrlNgNhPhNhom.Width = 405;
                PageMon = 20;
                PageNhom = 24;
            }
            if (index == 7)
            {
                pn_bigparent.Width = 570;

                pnctrlParent.Width = 570;
                pnCtrlNgNhPhNhom.Width = 570;
                PageMon = 30;
                PageNhom = 36;
            }
            if (index == 10)
            {
                pn_bigparent.Width = 720;

                pnctrlParent.Width = 720;
                pnCtrlNgNhPhNhom.Width = 720;
                PageMon = 40;
                PageNhom = 48;
            }
        }

        private void txtSarech_TextChanged(object sender, EventArgs e)
        {
            GetList20Product();
        }

        private void txtSarech_Click(object sender, EventArgs e)
        {
            cPageMon = 1;
            txtSarech.Text = "";
            SetTextbox(txtSarech);
            pn_banphim_chu.BringToFront();
        }

        private void txtSarech_Leave(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pnctrlParent.Controls)
            {
                if (btn.Focused)
                {
                    return;
                }
            }
            foreach (SimpleButton btn in pn_banphim_so.Controls)
            {
                if (btn.Focused)
                {
                    return;
                }
            }
            foreach (SimpleButton btn in pn_banphim_chu.Controls)
            {
                if (btn.Focused)
                {
                    return;
                }
            }
            txtSarech.Text = "Nhập tên hàng hóa...";
            pn_banphim_so.BringToFront();
        }

        private void GetGroup()
        {
            try
            {
                string _getPages = "";
                myDTNhom.Rows.Clear();
                String SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageDish, PageNhom, int.Parse(cls_ConfigCashier.idCashier));
                _getPages = "SELECT COUNT(*) AS soDong FROM NHOMHANG WHERE SUDUNG = 1";

                myDTNhom = clsMain.ReturnDataTable(SQLMON);
                DataTable CountRow = clsMain.ReturnDataTable(_getPages);
                string Tg = CountRow.Rows[0]["soDong"].ToString();

                int w = 4, h = 6;
                int numbutton = 0;
                int numline = 1;

                NumberOfPagesNhom = Math.Ceiling((decimal)long.Parse(Tg) / PageNhom);

                denSoNhom = Convert.ToInt32(NumberOfPagesNhom);

                if (NumberOfPagesNhom == 0)
                    lblSoTrangNhom.Text = String.Format("0/0");
                else
                    lblSoTrangNhom.Text = String.Format("{0}/{1}", cPageDish, NumberOfPagesNhom);

                Scrol_ShowNhom.Controls.Clear();
                if (myDTNhom.Rows.Count > 0)
                {
                    for (int i = 0; i < myDTNhom.Rows.Count; i++)
                    {
                        numbutton = numbutton + 1;
                        SimpleButton btnNhom = new SimpleButton();
                        btnNhom.Name = myDTNhom.Rows[i]["MA_NHOMHANG"].ToString();
                        if (i == 0)
                        {
                            sMaNH = int.Parse(myDTNhom.Rows[0]["MA_NHOMHANG"].ToString());
                            SetBackgroudOnClick(btnNhom);
                        }
                        btnNhom.Text = "" + myDTNhom.Rows[i]["TEN_NHOMHANG"].ToString();
                        try
                        {
                            byte[] tam = new byte[((byte[])myDTNhom.Rows[i]["HINHANH"]).LongLength];
                            tam = (byte[])myDTNhom.Rows[i]["HINHANH"];
                            MemoryStream ms = new MemoryStream(tam);
                            Bitmap bm = new Bitmap(ms);

                            if (cls_KP_RES.iHinhanh == 1)
                            {
                                btnNhom.BackgroundImage = bm;
                                btnNhom.BackgroundImageLayout = ImageLayout.Stretch;
                                btnNhom.Appearance.BackColor = Color.Transparent;
                                btnNhom.Appearance.Options.UseBackColor = true;
                                btnNhom.ButtonStyle = BorderStyles.NoBorder;
                                btnNhom.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                            }
                            else
                            {
                                btnNhom.Image = bm;
                            }
                        }
                        catch
                        {

                        }
                        btnNhom.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btnNhom.ImageLocation = ImageLocation.TopCenter;
                        btnNhom.RightToLeft = RightToLeft.No;
                        btnNhom.Size = new Size(125, 80);
                        btnNhom.Click += new EventHandler(btnNhom_Click);

                        switch (index)
                        {
                            case 5:
                                btnNhom.Width = 96;
                                btnNhom.Height = 62;
                                btnNhom.Location = new System.Drawing.Point(w, h);
                                w = int.Parse(btnNhom.Location.X.ToString()) + 100;
                                break;
                            case 7:
                                btnNhom.Width = 90;
                                btnNhom.Height = 62;
                                btnNhom.Location = new System.Drawing.Point(w, h);
                                w = int.Parse(btnNhom.Location.X.ToString()) + 93;
                                break;
                            case 10:
                                btnNhom.Width = 86;
                                btnNhom.Height = 62;
                                btnNhom.Location = new System.Drawing.Point(w, h);
                                w = int.Parse(btnNhom.Location.X.ToString()) + 88;
                                break;
                        }
                        switch (index)
                        {
                            case 5:
                                if (numbutton == 4)
                                {
                                    h = int.Parse(btnNhom.Location.Y.ToString()) + 65;
                                    w = 5;

                                    numbutton = 0;
                                    numline = numline + 1;
                                }
                                break;
                            case 7:
                                if (numbutton == 6)
                                {
                                    h = int.Parse(btnNhom.Location.Y.ToString()) + 65;
                                    w = 5;

                                    numbutton = 0;
                                    numline = numline + 1;
                                }
                                break;
                            case 10:
                                if (numbutton == 8)
                                {
                                    h = int.Parse(btnNhom.Location.Y.ToString()) + 65;
                                    w = 5;

                                    numbutton = 0;
                                    numline = numline + 1;
                                }
                                break;
                        }
                        Scrol_ShowNhom.Controls.Add(btnNhom);
                    }
                }
            }
            catch
            {

            }
        }

        private void GetList20Product()
        {
            try
            {
                myDTMon.Clear();
                String sSQL = "";
                String _getPages = "";
                String Tg = "";
                if (txtSarech.Text == "Nhập tên hàng hóa..." || txtSarech.Text == "")
                {
                    sSQL = string.Format("Exec SelectMON_ST @PageNumber={0},@PageSize={1}, @maKV={2}, @maCH={3}", cPageMon, PageMon, sMaKV, cls_ConfigCashier.idShop);
                    _getPages = "select count(*) as soDong from HANGHOA A  INNER JOIN NHOMHANG C ON A.MA_NHOMHANG = C.MA_NHOMHANG WHERE A.SUDUNG = 1 AND C.THUCDON = 1 ";

                    myDTMon = clsMain.ReturnDataTable(sSQL);
                    Tg = clsMain.ReturnDataTable(_getPages).Rows[0]["soDong"].ToString();
                }
                else
                {
                    sSQL = string.Format("Exec SelectHHSearch @MAVACH=N'{0}', @PageNumber={1}, @PageSize={2}, @maKV={3},@maCH={4}", txtSarech.Text, cPageMon, PageMon, sMaKV, cls_ConfigCashier.idShop);
                    _getPages = "select count(*) as soDong from HANGHOA A INNER JOIN NHOMHANG C ON A.MA_NHOMHANG = C.MA_NHOMHANG WHERE A.SUDUNG = 1 AND C.THUCDON = 1 and (A.MAVACH LIKE '%" + txtSarech.Text + "%' OR A.TEN_HANGHOA LIKE N'%" + txtSarech.Text + "%') ";

                    myDTMon = clsMain.ReturnDataTable(sSQL);
                    Tg = clsMain.ReturnDataTable(_getPages).Rows[0]["soDong"].ToString();
                }


                int w = 4, h = 4;
                int numbutton = 0;
                int numline = 1;

                NumberOfPages = Math.Ceiling((decimal)long.Parse(Tg) / PageMon);

                if (NumberOfPages == 0)
                    lblPageMon.Text = String.Format("0/0");
                else
                    lblPageMon.Text = String.Format("{0}/{1}", cPageMon, NumberOfPages);

                pnctrlParent.Controls.Clear();
                if (myDTMon.Rows.Count > 0)
                {
                    for (int i = 0; i < myDTMon.Rows.Count; i++)
                    {
                        numbutton = numbutton + 1;
                        SimpleButton btnMon = new SimpleButton();
                        btnMon.Name = myDTMon.Rows[i]["MA_HANGHOA"].ToString();
                        btnMon.Text = myDTMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(myDTMon.Rows[i]["GIABAN"].ToString())) + "";
                        btnMon.Tag = myDTMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(myDTMon.Rows[i]["GIABAN"].ToString())) + "";
                        try
                        {
                            byte[] tam = new byte[((byte[])myDTMon.Rows[i]["HINHANH"]).LongLength];
                            tam = (byte[])myDTMon.Rows[i]["HINHANH"];
                            MemoryStream ms = new MemoryStream(tam);
                            Bitmap bm = new Bitmap(ms);
                            //btnMon.Image = bm;
                            if (cls_KP_RES.iHinhanh == 1)
                            {
                                btnMon.BackgroundImage = bm;
                                btnMon.BackgroundImageLayout = ImageLayout.Stretch;
                                btnMon.Appearance.BackColor = Color.Transparent;
                                btnMon.Appearance.Options.UseBackColor = true;
                                btnMon.ButtonStyle = BorderStyles.NoBorder;
                                btnMon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                            }
                            else
                            {
                                btnMon.Image = bm;
                            }
                        }
                        catch
                        {

                        }
                        btnMon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btnMon.ImageLocation = ImageLocation.TopCenter;
                        btnMon.RightToLeft = RightToLeft.No;
                        btnMon.Size = new Size(110, 80);
                        btnMon.Click += new EventHandler(btnMon_Click);

                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            switch (index)
                            {
                                case 5:
                                    btnMon.Width = 190;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 193;
                                    if (numbutton == 2)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 7:
                                    btnMon.Width = 180;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 183;
                                    if (numbutton == 3)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 10:
                                    btnMon.Width = 170;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 173;
                                    if (numbutton == 4)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            switch (index)
                            {
                                case 5:
                                    btnMon.Width = 96;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 100;
                                    if (numbutton == 4)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 7:
                                    btnMon.Width = 90;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 93;
                                    if (numbutton == 6)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 10:
                                    btnMon.Width = 86;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 88;
                                    if (numbutton == 8)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                            }
                        }

                        pnctrlParent.Controls.Add(btnMon);
                    }
                }
            }
            catch
            {

            }
            SetNavigateState();
        }

        private void SetNavigateState()
        {
            btn_Next.Enabled = (cPageMon < NumberOfPages);
            btn_Last.Enabled = (cPageMon < NumberOfPages);
            btn_Prev.Enabled = cPageMon > 1;
            btn_First.Enabled = cPageMon > 1;
        }

        private void btnNhom_Click(object sender, EventArgs e)
        {
            try
            {
                cPageMon = 1;
                foreach (SimpleButton btnNhom in Scrol_ShowNhom.Controls)
                {
                    if (btnNhom.Name == this.ActiveControl.Name)
                    {
                        sMaNH = int.Parse(btnNhom.Name.ToString());
                        SetBackgroudOnClick(btnNhom);
                        GetListProduct();
                    }
                    else
                        SetDefautlBackgroud(btnNhom);
                }

                pnCtrlNgNhPhNhom.SendToBack();
            }
            catch (Exception ex)
            {

            }
        }

        private void GetListProduct()
        {
            try
            {
                myDTMon.Clear();
                String sSQL = "";
                string _getPages = "";

                sSQL = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1},@maNH={2}, @maKV={3}, @maCH={4}", cPageMon, PageMon, sMaNH, sMaKV, cls_ConfigCashier.idShop);
                _getPages = "select count(*) as soDong from HANGHOA WHERE SUDUNG = 1 and MA_NHOMHANG = '" + sMaNH + "'";

                myDTMon = clsMain.ReturnDataTable(sSQL);
                DataTable CountRow = clsMain.ReturnDataTable(_getPages);
                string Tg = CountRow.Rows[0]["soDong"].ToString();

                int w = 4, h = 6;
                int numbutton = 0;
                int numline = 1;

                NumberOfPages = Math.Ceiling((decimal)long.Parse(Tg) / PageMon);

                if (NumberOfPages == 0)
                    lblPageMon.Text = String.Format("0/0");
                else
                    lblPageMon.Text = String.Format("{0}/{1}", cPageMon, NumberOfPages);

                pnctrlParent.Controls.Clear();
                if (myDTMon.Rows.Count > 0)
                {
                    for (int i = 0; i < myDTMon.Rows.Count; i++)
                    {
                        numbutton = numbutton + 1;
                        SimpleButton btnMon = new SimpleButton();
                        btnMon.Name = myDTMon.Rows[i]["MA_HANGHOA"].ToString();
                        btnMon.Text = myDTMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(myDTMon.Rows[i]["GIABAN"].ToString())) + "";
                        btnMon.Tag = myDTMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(myDTMon.Rows[i]["GIABAN"].ToString())) + "";
                        try
                        {
                            byte[] tam = new byte[((byte[])myDTMon.Rows[i]["HINHANH"]).LongLength];
                            tam = (byte[])myDTMon.Rows[i]["HINHANH"];
                            MemoryStream ms = new MemoryStream(tam);
                            Bitmap bm = new Bitmap(ms);
                            //btnMon.Image = bm;
                            if (cls_KP_RES.iHinhanh == 1)
                            {
                                btnMon.BackgroundImage = bm;
                                btnMon.BackgroundImageLayout = ImageLayout.Stretch;
                                btnMon.Appearance.BackColor = Color.Transparent;
                                btnMon.Appearance.Options.UseBackColor = true;
                                btnMon.ButtonStyle = BorderStyles.NoBorder;
                                btnMon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                            }
                            else
                            {
                                btnMon.Image = bm;
                            }
                        }
                        catch
                        {

                        }
                        btnMon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                        btnMon.ImageLocation = ImageLocation.TopCenter;
                        btnMon.RightToLeft = RightToLeft.No;
                        btnMon.Size = new Size(110, 80);
                        btnMon.Click += new EventHandler(btnMon_Click);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            switch (index)
                            {
                                case 5:
                                    btnMon.Width = 190;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 193;
                                    if (numbutton == 2)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 7:
                                    btnMon.Width = 180;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 183;
                                    if (numbutton == 3)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 10:
                                    btnMon.Width = 170;
                                    btnMon.Height = 130;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 173;
                                    if (numbutton == 4)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 133;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            switch (index)
                            {
                                case 5:
                                    btnMon.Width = 96;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 100;
                                    if (numbutton == 4)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 7:
                                    btnMon.Width = 90;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 93;
                                    if (numbutton == 6)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                                case 10:
                                    btnMon.Width = 86;
                                    btnMon.Height = 62;
                                    btnMon.Location = new System.Drawing.Point(w, h);
                                    w = int.Parse(btnMon.Location.X.ToString()) + 88;
                                    if (numbutton == 8)
                                    {
                                        h = int.Parse(btnMon.Location.Y.ToString()) + 65;
                                        w = 5;

                                        numbutton = 0;
                                        numline = numline + 1;
                                    }
                                    break;
                            }
                        }
                        
                        pnctrlParent.Controls.Add(btnMon);
                    }
                }
            }
            catch
            {

            }
            SetNavigateState();
        }

        private void btnMon_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (SimpleButton btnMon in pnctrlParent.Controls)
                {
                    if (btnMon.Name == this.ActiveControl.Name)
                    {
                        sMaMon = btnMon.Name;
                        SetBackgroudOnClick(btnMon);
                        GetDishGoToBuy(sMaMon, sMaKV);
                    }
                    else
                    {
                        SetDefautlBackgroud(btnMon);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void GetDishGoToBuy(String sMaHangHoa, int sMaKV)
        {
            try
            {
                String sSQL = String.Format("Exec SelectByMON @MAHANG='{0}',@MAVACH='{1}',@maKV={2},@maCH={3}", sMaHangHoa, sMaHangHoa, sMaKV, cls_ConfigCashier.idShop);
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                if (myDT.Rows.Count < 1)
                {
                    string sMahang_PLU = sMaHangHoa.Substring(2, 5);
                    string sTrongluong = ((decimal.Parse (sMaHangHoa.Substring(7, 5))/1000)).ToString ();
                    sSQL = String.Format("Exec SelectByMON @MAHANG='{0}',@MAVACH='{1}',@maKV={2},@maCH={3}", sMahang_PLU, sMahang_PLU, sMaKV, cls_ConfigCashier.idShop);
                    myDT = clsMain.ReturnDataTable(sSQL);
                    if (myDT.Rows.Count < 1)
                    {
                        return;
                    }
                    else
                    {
                        sMaHangHoa = sMahang_PLU;
                        txt_soluong.Text = sTrongluong;
                    }
                }

                //sửa định lượng
                if (bool.Parse(myDT.Rows[0]["SUADINHLUONG"].ToString()))
                {
                    bool flag_Exist = false;
                    if (gVDSHangMua.RowCount > 0)
                    {
                        for (int i = 0; i < gVDSHangMua.RowCount; i++)
                        {
                            if (gVDSHangMua.GetRowCellValue(i, "MA_VACH").ToString() == sMaHangHoa && gVDSHangMua.GetRowCellValue(i, "MA_HANGHOA").ToString() == sMaHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        sSQL = "Select MANGUYENLIEU" + "\n";
                        sSQL += "From DINHLUONG" + "\n";
                        sSQL += "Where MATHANHPHAM=" + clsMain.SQLString(myDT.Rows[0]["MA_HANGHOA"].ToString());
                        DataTable dtDL = clsMain.ReturnDataTable(sSQL);
                        if (dtDL.Rows.Count > 0)
                        {
                            Frm_Suadinhluong frm = new Frm_Suadinhluong(myDT.Rows[0]["MA_HANGHOA"].ToString());
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                        else
                        {
                            XtraMessageBox.Show("Chưa khai báo định lượng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                //
                decimal Giaban_Cur = 0;
                if (bool.Parse(myDT.Rows[0]["SUAGIA"].ToString()))
                {
                    bool flag_Exist = false;
                    if (gVDSHangMua.RowCount > 0)
                    {
                        for (int i = 0; i < gVDSHangMua.RowCount; i++)
                        {
                            if (gVDSHangMua.GetRowCellValue(i, "MA_VACH").ToString() == sMaHangHoa && gVDSHangMua.GetRowCellValue(i, "MA_HANGHOA").ToString() == sMaHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                        frm.ShowDialog();
                        Giaban_Cur = frm.tmp_Thoigia;
                        frm.Dispose();
                    }
                }
                else
                {
                    Giaban_Cur = decimal.Parse(myDT.Rows[0]["GIABAN"].ToString());
                }
                
                DM_HangHoa hanghoa = new DM_HangHoa();
                hanghoa.MA_VACH = myDT.Rows[0]["MAVACH"].ToString();
                hanghoa.MA_HANGHOA = myDT.Rows[0]["MA_HANGHOA"].ToString();
                hanghoa.TEN_HANGHOA = myDT.Rows[0]["TEN_HANGHOA"].ToString();
                hanghoa.SOLUONG = decimal.Parse(txt_soluong.Text);
                hanghoa.MA_BEP = myDT.Rows[0]["MA_BEP"].ToString();
                hanghoa.GIABAN = Giaban_Cur;
                hanghoa.GHICHU = myDT.Rows[0]["GHICHU"].ToString();
                hanghoa.THUE = double.Parse(myDT.Rows[0]["THUE"].ToString());
                hanghoa.TRANGTHAI = false;

                bool testID = false;
                if (myDTHH.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    for (int i = 0; i < myDTHH.Rows.Count; i++)
                    {
                        if ((myDTHH.Rows[i]["MA_VACH"].ToString() == hanghoa.MA_VACH && myDTHH.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA) && "cb" != myDTHH.Rows[i]["GHICHU"].ToString() && !bool.Parse(myDTHH.Rows[i]["IS_KHUYENMAI"].ToString()))
                        {
                            iFocusRow = i;
                            myDTHH.Rows[i]["SOLUONG"] = double.Parse(myDTHH.Rows[i]["SOLUONG"].ToString()) + double.Parse(hanghoa.SOLUONG.ToString());
                            testID = true;
                            break;
                        }
                        else
                        {
                            testID = false;
                        }
                    }
                }
                
                if (testID == false)
                {
                    DataRow dr = myDTHH.NewRow();
                    dr[0] = hanghoa.MA_VACH;
                    dr[1] = hanghoa.MA_HANGHOA;
                    dr[2] = hanghoa.TEN_HANGHOA;
                    dr[3] = hanghoa.SOLUONG;
                    dr[4] = hanghoa.GIABAN;
                    dr[5] = hanghoa.GHICHU;
                    dr[6] = hanghoa.MA_BEP;
                    dr[7] = hanghoa.THUE;
                    dr["CHIETKHAU"] = int.Parse(txtdiscount.Text);
                    dr["TRANGTHAI"] = false;
                    dr["IS_COMBO"] = false;
                    dr["IS_INBEP"] = bool.Parse(myDT.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    myDTHH.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = myDTHH.NewRow();
                            drr[0] = decimal.Parse(dtCombo.Rows[i]["MAVACH"].ToString());
                            drr[1] = int.Parse(dtCombo.Rows[i]["MA_HANGHOA"].ToString());
                            drr[2] = dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                            drr[3] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[4] = 0;
                            drr[5] = "cb";
                            drr[6] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[7] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;
                            drr["IS_COMBO"] = true;
                            drr["IS_INBEP"] = bool.Parse(myDT.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            myDTHH.Rows.Add(drr);
                        }
                    }
                    iFocusRow = myDTHH.Rows.Count - 1 - dtCombo.Rows.Count;
                }
                grCtrlDSHangMua.DataSource = myDTHH;
                if (gVDSHangMua.RowCount > 0)
                    TotalMoney();
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = iFocusRow;
                txt_soluong.Text = "1";
            }
            catch (Exception ex)
            {
                
            }
        }

        private void GetDishGoToBuy(String sMaHangHoa, int sMaKV, decimal  sSoLuong, int sChietKhau)
        {
            try
            {
                String sSQL = String.Format("Exec SelectByMON @MAHANG={0},@MAVACH='{1}',@maKV={2},@maCH={3}", sMaHangHoa, sMaHangHoa, sMaKV, cls_ConfigCashier.idShop);
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                if (myDT.Rows.Count < 1)
                    return;

                //sửa định lượng
                if (bool.Parse(myDT.Rows[0]["SUADINHLUONG"].ToString()))
                {
                    bool flag_Exist = false;
                    if (gVDSHangMua.RowCount > 0)
                    {
                        for (int i = 0; i < gVDSHangMua.RowCount; i++)
                        {
                            if (gVDSHangMua.GetRowCellValue(i, "MA_VACH").ToString() == sMaHangHoa && gVDSHangMua.GetRowCellValue(i, "MA_HANGHOA").ToString() == sMaHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        sSQL = "Select MANGUYENLIEU" + "\n";
                        sSQL += "From DINHLUONG" + "\n";
                        sSQL += "Where MATHANHPHAM=" + clsMain.SQLString(myDT.Rows[0]["MA_HANGHOA"].ToString());
                        DataTable dtDL = clsMain.ReturnDataTable(sSQL);
                        if (dtDL.Rows.Count > 0)
                        {
                            Frm_Suadinhluong frm = new Frm_Suadinhluong(myDT.Rows[0]["MA_HANGHOA"].ToString());
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                        else
                        {
                            XtraMessageBox.Show("Chưa khai báo định lượng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }

                decimal Giaban_Cur = 0;
                if (bool.Parse(myDT.Rows[0]["SUAGIA"].ToString()))
                {
                    bool flag_Exist = false;
                    if (gVDSHangMua.RowCount > 0)
                    {
                        for (int i = 0; i < gVDSHangMua.RowCount; i++)
                        {
                            if (gVDSHangMua.GetRowCellValue(i, "MA_VACH").ToString() == sMaHangHoa && gVDSHangMua.GetRowCellValue(i, "MA_HANGHOA").ToString() == sMaHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                        frm.ShowDialog();
                        Giaban_Cur = frm.tmp_Thoigia;
                        frm.Dispose();
                    }
                }
                else
                {
                    Giaban_Cur = decimal.Parse(myDT.Rows[0]["GIABAN"].ToString());
                }

                DM_HangHoa hanghoa = new DM_HangHoa();
                hanghoa.MA_VACH = myDT.Rows[0]["MAVACH"].ToString();
                hanghoa.MA_HANGHOA = myDT.Rows[0]["MA_HANGHOA"].ToString();
                hanghoa.TEN_HANGHOA = myDT.Rows[0]["TEN_HANGHOA"].ToString();
                hanghoa.SOLUONG = sSoLuong;
                hanghoa.MA_BEP = myDT.Rows[0]["MA_BEP"].ToString();
                hanghoa.GIABAN = Giaban_Cur;
                hanghoa.GHICHU = myDT.Rows[0]["GHICHU"].ToString();
                hanghoa.THUE = double.Parse(myDT.Rows[0]["THUE"].ToString());
                hanghoa.TRANGTHAI = false;

                bool testID = false;
                if (myDTHH.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    for (int i = 0; i < myDTHH.Rows.Count; i++)
                    {
                        if ((myDTHH.Rows[i]["MA_VACH"].ToString() == hanghoa.MA_VACH && myDTHH.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA) && "cb" != myDTHH.Rows[i]["GHICHU"].ToString() && !bool.Parse(myDTHH.Rows[i]["IS_KHUYENMAI"].ToString()))
                        {
                            iFocusRow = i;
                            myDTHH.Rows[i]["SOLUONG"] = double.Parse(sSoLuong.ToString());
                            myDTHH.Rows[i]["CHIETKHAU"] = double.Parse(sChietKhau.ToString());
                            testID = true;
                            break;
                        }
                        else
                        {
                            testID = false;
                        }
                    }
                }

                if (testID == false)
                {
                    DataRow dr = myDTHH.NewRow();
                    dr[0] = hanghoa.MA_VACH;
                    dr[1] = hanghoa.MA_HANGHOA;
                    dr[2] = hanghoa.TEN_HANGHOA;
                    dr[3] = hanghoa.SOLUONG;
                    dr[4] = hanghoa.GIABAN;
                    dr[5] = hanghoa.GHICHU;
                    dr[6] = hanghoa.MA_BEP;
                    dr[7] = hanghoa.THUE;
                    dr["CHIETKHAU"] = sChietKhau;
                    dr["TRANGTHAI"] = false;
                    dr["IS_COMBO"] = false;
                    dr["IS_INBEP"] = bool.Parse(myDT.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    myDTHH.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = myDTHH.NewRow();
                            drr[0] = int.Parse(dtCombo.Rows[i]["MAVACH"].ToString());
                            drr[1] = int.Parse(dtCombo.Rows[i]["MA_HANGHOA"].ToString());
                            drr[2] = dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                            drr[3] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[4] = 0;
                            drr[5] = "cb";
                            drr[6] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[7] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;
                            drr["IS_COMBO"] = true;
                            drr["IS_INBEP"] = bool.Parse(myDT.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            myDTHH.Rows.Add(drr);
                        }
                    }
                    iFocusRow = myDTHH.Rows.Count - 1 - dtCombo.Rows.Count;
                }
                grCtrlDSHangMua.DataSource = myDTHH;
                if (gVDSHangMua.RowCount > 0)
                    TotalMoney();
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = iFocusRow;
            }
            catch (Exception ex)
            {

            }
        }

        private DataTable GetProductInCombo(String maHangHoa)
        {
            string sSQL = "";
            sSQL += "Select cb.MA_COMBO, hh.MA_HANGHOA, hh.TEN_HANGHOA, hh.MAVACH, cb.SOLUONG,HH.MA_BEP" + "\n";
            sSQL += "From COMBO cb inner join HANGHOA hh on cb.MA_HANGHOA = hh.MA_HANGHOA" + "\n";
            sSQL += "Where MA_COMBO = " + maHangHoa + "";
            var dtCombo = clsMain.ReturnDataTable(sSQL);
            return dtCombo;
        }

        DataTable checkVAT = new DataTable();
        private void TotalMoney()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double Total = 0;
            double TotalCK = 0;
            double TotalVAT = 0;
            double TotalApVAT = 0;
            double Qty = 0;
            try
            {
                for (int i = 0; i < gVDSHangMua.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["GIABAN"]).ToString());
                    double ck = double.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - ((sl * gia) * ck) / 100;
                    if (kt == true)
                    {
                        vat = (double.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    }
                    Qty += sl;
                    TotalCK += ((sl * gia) * ck) / 100;
                    TotalVAT += vat;
                    TotalApVAT += thanhTien;
                    Total += thanhTien + vat;
                    gVDSHangMua.SetRowCellValue(i, gVDSHangMua.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                
            }
            lbdiscount.Text = String.Format("{0:#,###0}", TotalCK);
            labelThueVAT.Text = String.Format("{0:#,###0}", TotalVAT);
            lbTienTruocThue.Text = String.Format("{0:#,###0}", TotalApVAT);
            lb_tongcong.Text = String.Format("{0:#,###0}", Total);
        }

#endregion

#region Bàn Phím số

        private void btn_ABC_Click(object sender, EventArgs e)
        {
            pn_banphim_chu.BringToFront();
            btn_123.Text = "123";
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            _textbox.Text = "";
            _textbox.Focus();
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                _textbox.Focus();
                _textbox.SelectionStart = select - 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = _textbox.Text.Length;
                _textbox.SelectionLength = 0;
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            int iPosition = _textbox.SelectionStart;
            if (iPosition < _textbox.Text.Length - 1)
            {
                if (_textbox.Text[iPosition + 1].ToString() == ",")
                {
                    _textbox.SelectionStart = iPosition + 2;
                }
                else
                {
                    _textbox.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                _textbox.SelectionStart = _textbox.Text.Length;
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btn_enter_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
                return;

            GetDishGoToBuy(txt_Mahang.Text.Replace ("*",""), sMaKV, decimal.Parse(txt_soluong.Text), int.Parse(txtdiscount.Text));
            txt_soluong.Text = "1";
            txtdiscount.Text = "0";
            txt_Mahang.Text = "";
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            try
            {
                if (exitReprint == true)
                    return;

                if (gVDSHangMua.RowCount <= 0)
                    return;

                //btnComboOption.Visible = false;
                //pnBtnDeleteGv.Visible = false;
                //Lấy mã bàn
                String sMaBan = "";
                String sTenBan = "";
                String sIdBill = "";
                decimal Tiendatcoc = 0;

                DataTable myDT = (DataTable)grCtrlDSHangMua.DataSource;

                Boolean sCheck;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lb_tongcong.Text + " VND");
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(sMaBan, sIdBill, myDT, Tiendatcoc, sTenBan);
                frm.ShowDialog();
                sCheck = frm.bHoanTacThanhToan;
                frm.Dispose();

                if (sCheck == true)
                {
                    myDTHH.Rows.Clear();
                    lb_tongcong.Text = "0";
                    lbTienTruocThue.Text = "0";
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);

                    txt_soluong.Text = "1";
                    txt_soluong.Focus();
                    txt_soluong.SelectAll();
                    txt_Mahang.Focus();
                    SetTextbox(txt_soluong);
                }
            }
            catch (Exception es)
            {

            }
        }

#endregion

#region Bàn Phím chữ

        private void btn_BanPhim_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            Click_buttonNumber(btn.Text);
        }

        private void btn_123_Click(object sender, EventArgs e)
        {
            pn_banphim_so.BringToFront();
        }

        private void btn_backchar_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                _textbox.Focus();
                _textbox.SelectionStart = select - 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = _textbox.Text.Length;
                _textbox.SelectionLength = 0;
            }
        }

        private void btn_ESCChar_Click(object sender, EventArgs e)
        {
            _textbox.Text = "";
            _textbox.Focus();
        }

        private void btn_OKChar_Click(object sender, EventArgs e)
        {
        }

        private void btn_DeleteChar_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                if (_textbox.SelectionLength < 1)
                {
                    _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                    _textbox.Focus();
                    _textbox.SelectionStart = select - 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    int length = _textbox.SelectionLength;
                    _textbox.Text = _textbox.Text.Remove(select, length);
                    _textbox.Focus();
                    _textbox.SelectionStart = select;
                    _textbox.SelectionLength = 0;
                }
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = 0;
                _textbox.SelectionLength = 0;
            }
        }

        private void btn_PaymentsChar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gVDSHangMua.RowCount <= 0)
                    return;

                //btnComboOption.Visible = false;
                //pnBtnDeleteGv.Visible = false;
                //Lấy mã bàn
                String sMaBan = "";
                String sTenBan = "";
                String sIdBill = "";
                decimal Tiendatcoc = 0;

                DataTable myDT = (DataTable)grCtrlDSHangMua.DataSource;

                Boolean sCheck;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lb_tongcong.Text + " VND");
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(sMaBan, sIdBill, myDT, Tiendatcoc, sTenBan);
                frm.ShowDialog();
                sCheck = frm.bHoanTacThanhToan;
                frm.Dispose();

                if (sCheck == true)
                {
                    myDTHH.Rows.Clear();
                    lb_tongcong.Text = "0";
                    lbTienTruocThue.Text = "0";
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);

                    txt_soluong.Text = "1";
                    txt_soluong.Focus();
                    txt_soluong.SelectAll();
                    txt_Mahang.Focus();
                    SetTextbox(txt_soluong);
                }
            }
            catch (Exception es)
            {

            }
        }
#endregion

#region Show Thông Tin

        private void txt_soluong_Click(object sender, EventArgs e)
        {
            pn_banphim_chu.SendToBack();
            if (pnWapperDeletePlusBut.Visible == true)
                pnWapperDeletePlusBut.Visible = false;
            txt_soluong.Text = "";
            SetTextbox(txt_soluong);
            txt_soluong.Focus();
        }

        private void txt_soluong_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_soluong.Text.Trim() == "")
                        return;

                    if (txt_soluong.Text.Length > 3)
                    {
                        txt_Mahang.Text = txt_soluong.Text;
                        txt_soluong.Text = "1";
                    }

                    GetDishGoToBuy(txt_Mahang.Text.Replace("*", ""), sMaKV);
                    txt_soluong.Text = "1";
                    txtdiscount.Text = "0";
                    txt_Mahang.Text = "";
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void txt_soluong_Leave(object sender, EventArgs e)
        {
            if (btn_Tru.Focused || btn_xoa.Focused || btn_Cong.Focused)
                pnWapperDeletePlusBut.Visible = true;
            else
            {
                if (pnWapperDeletePlusBut.Visible == true)
                    pnWapperDeletePlusBut.Visible = false;
            }

            if (txt_soluong.Text == "")
            {
                txt_soluong.Text = "1";
            }
        }

        private void txt_soluong_TextChanged(object sender, EventArgs e)
        {
            if (txt_soluong.Focused == true)
                pn_banphim_chu.SendToBack();

            double mode;
            if (!double.TryParse(txt_soluong.Text, out mode) && txt_soluong.Text != "" || txt_soluong.Text.Length >= 16)
            {
                MessageBox.Show("Số lượng nhập quá giới hạn hoặc không đúng định dạng !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_soluong.Text = "";
                pn_banphim_chu.SendToBack();
            }

        }

        private void txt_Mahang_Click(object sender, EventArgs e)
        {
            txt_Mahang.Text = "";
            SetTextbox(txt_Mahang);
            txt_Mahang.Focus();
        }

        private void txt_Mahang_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_Mahang.Text.Trim() == "")
                        return;

                    GetDishGoToBuy(txt_Mahang.Text.Trim().Replace("*",""), sMaKV);
                    txt_soluong.Text = "1";
                    txtdiscount.Text = "0";
                    txt_Mahang.Text = "";
                }
                txt_Mahang.Focus();
            }
            catch (Exception ex)
            {

            }
            
        }

        private void txtdiscount_KeyDown(object sender, KeyEventArgs e)
        {
            txt_soluong_KeyDown(sender, e);
        }

        private void txtdiscount_Click(object sender, EventArgs e)
        {
            txtdiscount.Text = "";
            SetTextbox(txtdiscount);
            txtdiscount.Focus();
        }

        private void txtdiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtdiscount_Leave(object sender, EventArgs e)
        {
            if (txtdiscount.Text == "")
            {
                txtdiscount.Text = "0";
            }
        }

#endregion

#region Controll
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;

            if (MyGetData != null)
            {
                MyGetData(myDTHH, contrDelegate, iFocusRow);
            }
            timer1.Enabled = true;
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            Frm_Ketca frm = new Frm_Ketca();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            if (gVDSHangMua.RowCount <= 0 || sMaMon == null || sMaMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            String sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
                {
                    MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            sSQL = "Select ISNULL(SUAGIA,0) AS SUAGIA From HANGHOA Where MA_HANGHOA = " + clsMain.SQLString(sMaMon);
            dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
                {
                    MessageBox.Show("Đây là hàng hóa theo thời giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            sSQL = "select  ISNULL(GIABAN1,0) as GIABAN1,ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(sMaMon);
            DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
            if (dtgiaban.Rows.Count > 0)
            {
                for (int i = 0; i < gVDSHangMua.RowCount; i++)
                {
                    if (gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["MA_HANGHOA"]).ToString() == sMaMon)
                    {
                        if (gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["GIABAN"]).ToString() == dtgiaban.Rows[0]["GIABAN1"].ToString())
                            gVDSHangMua.SetRowCellValue(i, gVDSHangMua.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN2"].ToString());
                        else
                            gVDSHangMua.SetRowCellValue(i, gVDSHangMua.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN1"].ToString());
                    }
                }
                if (gVDSHangMua.RowCount > 0)
                    TotalMoney();
            }
        }

        private void btnSuaGia_Click(object sender, EventArgs e)
        {
            if (gVDSHangMua.RowCount <= 0 || sMaMon == null || sMaMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            bool thoat = false;
            Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
            frm.ShowDialog();
            Giamoi = frm.tmp_Thoigia;
            thoat = frm.bThoat;
            frm.Dispose();

            if (thoat)
                return;

            for (int i = 0; i < gVDSHangMua.RowCount; i++)
            {
                if (gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["MA_HANGHOA"]).ToString() == sMaMon)
                    gVDSHangMua.SetRowCellValue(i, gVDSHangMua.Columns["GIABAN"], Giamoi.ToString());
            }

            if (gVDSHangMua.RowCount > 0)
                TotalMoney();
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (gVDSHangMua.RowCount <= 0)
                    return;

                //btnComboOption.Visible = false;
                //pnBtnDeleteGv.Visible = false;
                //Lấy mã bàn
                String sMaBan = "";
                String sTenBan = "";
                String sIdBill = "";
                decimal Tiendatcoc = 0;

                DataTable myDT = (DataTable)grCtrlDSHangMua.DataSource;

                Boolean sCheck;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lb_tongcong.Text + " VND");
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(sMaBan, sIdBill, myDT, Tiendatcoc, sTenBan);
                frm.ShowDialog();
                sCheck = frm.bHoanTacThanhToan;
                frm.Dispose();

                if (sCheck == true)
                {
                    myDTHH.Rows.Clear();
                    lb_tongcong.Text = "0";
                    lbTienTruocThue.Text = "0";
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);

                    txt_soluong.Text = "1";
                    txt_soluong.Focus();
                    txt_soluong.SelectAll();
                    txt_Mahang.Focus();
                    SetTextbox(txt_soluong);
                }
            }
            catch (Exception es)
            {
                
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            contrDelegate = 1;
            if (MyGetData != null)
            {
                MyGetData(myDTHH, contrDelegate, 0);
            }
            while (gVDSHangMua.RowCount > 0)
                gVDSHangMua.DeleteRow(0);

            this.Close();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Frm_ThemHangHoa frm = new Frm_ThemHangHoa();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnInVeOnline_Click(object sender, EventArgs e)
        {
            Frm_InBillMuaTruoc frm = new Frm_InBillMuaTruoc();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
                return;
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_Nghiepvu);
            SetDefautlBackgroud(btn_Datthu );
            pnThuNgan.BringToFront();
        }

        private void btn_Nghiepvu_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
                return;
            SetBackgroudOnClick(btn_Nghiepvu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_Datthu);
            pnNghiepvu.BringToFront();
        }


        private void btn_Datthu_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
                return;
            SetBackgroudOnClick(btn_Datthu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_Nghiepvu);
            pnDacthu.BringToFront();
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_InLaiHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                pObject = "1";
                panelControl2.Visible = false;
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;

                SetBackgroudOnClick(btn_InLaiHoaDon);
                IN.Visible = true;
                HUY.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btn_InLaiHoaDon")
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_HuyHoaDon_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_HuyHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                pObject = "2";
                panelControl2.Visible = true;
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;

                SetBackgroudOnClick(btn_HuyHoaDon);
                HUY.Visible = true;
                IN.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btn_HuyHoaDon")
                    {
                        btn.Visible  = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btnThoatReprint_Click(object sender, EventArgs e)
        {
            pObject = "";
            pnInlaihoadon.SendToBack();
            foreach (SimpleButton btn in pnThuNgan.Controls)
                btn.Visible = true;

            foreach (SimpleButton btn in pnNghiepvu.Controls)
                btn.Visible = true;

            exitReprint = false;
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString("00");
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString("00");
            }
        }

        String pObject = "";
        private void gvBillPaymented_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString(); ;
                //GetDishOfTable(idBill);
                //GetDistcountABill(idBill);

                string sSQL;
                if (e.Column == IN)
                {
                    sSQL = "";
                    sSQL += "Insert into HOADON_INLAI(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE())";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon,ISNULL(REPLACE((SELECT TENNHANVIEN + ',' FROM DM_NHANVIEN WHERE MANHANVIEN IN(SELECT data  FROM  [dbo].[SplitStringToTable](cthd.NHANVIEN, ',')) for xml path('')) + ',',',,',''),'') AS TenNhanVien,ISNULL(hd.GHICHU1,'') AS GhiChu" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY LEFT join DM_NHANVIEN NV on ISNULL(cthd.NHANVIEN,'') = NV.MANHANVIEN" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                            }
                            else if (cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        //dt.Rows.Clear();
                        //lbTotal.Text = "0";
                        idBill = "";

                        return;
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_inhoadon_Bill_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_thaomoc";
                            }
                            else if (cls_KP_RES.Mode == 10)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_MaVach";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        idBill = "";

                        return;
                    }
                }
                else if (e.Column == HUY)
                {
                    sSQL = "";
                    sSQL += "Update HOADON Set " + "\n";
                    sSQL += "NHANVIEN_HUYBILL=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "ISPAYMENT=2 " + "\n";
                    sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);
                    //Trừ kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    //Công nợ
                    sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                    clsMain.ExecuteSQL(sSQL);
                    //KHTT
                    sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                        {
                            DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                            if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            {
                                int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                                decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                            }
                            else
                            {
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                            }
                        }
                    }
                    //In bill
                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (HỦY)' as Ten_HoaDon" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_KP_RES.Mode == 3)
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                        }
                        else if (cls_KP_RES.Mode == 4)
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                        }
                        else
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban";
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        //dt.Rows.Clear();
                        //lbTotal.Text = "0";
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_KP_RES.Mode == 10)
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_MaVach";
                        }
                        else
                        {
                            frm.ReportName = "rpt_inhoadon_Bill";
                        }

                       
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        //dt.Rows.Clear();
                        //lbTotal.Text = "0";
                    }
                    //load luoi hoa don
                    sSQL = "exec SP_DANHSACHHOADON";
                    DataTable dts = clsMain.ReturnDataTable(sSQL);
                    gr_billPaymented.DataSource = dts;
                    return;
                }
                else if (e.Column == LAYLAI)
                {
                    try
                    {
                        sSQL = "";
                        sSQL += "SELECT ISNULL(C.MAVACH,0) AS MA_VACH, B.MA_HANGHOA, C.TEN_HANGHOA, B.SOLUONG, B.GIABAN, '' AS GHICHU, MA_BEP, B.THUE, B.CHIETKHAU, ISNULL(TRANGTHAI,0) AS TRANGTHAI, B.IS_COMBO, IS_INBEP, CONVERT(BIT,0) AS IS_KHUYENMAI, B.GIABAN_THAT AS Thanh_Tien, 0 AS TONGTIEN, CONVERT(BIT,0) AS  IS_QUATANG" + "\n";
                        sSQL += "FROM HOADON_DOI A" + "\n";
                        sSQL += "INNER JOIN CT_HOADON_DOI B ON A.MA_HOADON = B.MA_HOADON" + "\n";
                        sSQL += "INNER JOIN HANGHOA C ON B.MA_HANGHOA = C.MA_HANGHOA" + "\n";
                        sSQL += "WHERE A.MA_HOADON = " + clsMain.SQLString(idBill);
                        DataTable myDT = clsMain.ReturnDataTable(sSQL);
                        if (myDT.Rows.Count > 0)
                        {
                            myDTHH.Rows.Clear();
                            for (int i = 0; i < myDT.Rows.Count; i++)
                            {
                                DataRow dr = myDTHH.NewRow();
                                dr[0] = myDT.Rows[i]["MA_VACH"].ToString();
                                dr[1] = myDT.Rows[i]["MA_HANGHOA"].ToString();
                                dr[2] = myDT.Rows[i]["TEN_HANGHOA"].ToString();
                                dr[3] = myDT.Rows[i]["SOLUONG"].ToString();
                                dr[4] = myDT.Rows[i]["GIABAN"].ToString();
                                dr[5] = myDT.Rows[i]["GHICHU"].ToString();
                                dr[6] = myDT.Rows[i]["MA_BEP"].ToString();
                                dr[7] = myDT.Rows[i]["THUE"].ToString();
                                dr["CHIETKHAU"] = int.Parse(myDT.Rows[i]["CHIETKHAU"].ToString());
                                dr["TRANGTHAI"] = bool.Parse(myDT.Rows[0]["TRANGTHAI"].ToString());
                                dr["IS_COMBO"] = bool.Parse(myDT.Rows[0]["IS_COMBO"].ToString());
                                dr["IS_INBEP"] = bool.Parse(myDT.Rows[0]["IS_INBEP"].ToString());
                                dr["IS_KHUYENMAI"] = bool.Parse(myDT.Rows[0]["IS_KHUYENMAI"].ToString());
                                myDTHH.Rows.Add(dr);
                            }
                        }

                        grCtrlDSHangMua.DataSource = myDTHH;
                        if (gVDSHangMua.RowCount > 0)
                        {
                            TotalMoney();

                            sSQL = "";
                            sSQL += "DELETE CT_HOADON_DOI WHERE MA_HOADON = " + clsMain.SQLString(gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString()) + "\n";
                            sSQL += "DELETE HOADON_DOI WHERE MA_HOADON = " + clsMain.SQLString(gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString()) + "\n";
                            clsMain.ExecuteSQL(sSQL);

                            txt_soluong.Text = "1";
                            txt_Mahang.Enabled = true;
                            txt_soluong.Enabled = true;
                            btn_xoa.Enabled = true;
                            btnDeleteAll.Enabled = true;
                            btn_Cong.Enabled = true;
                            btn_Tru.Enabled = true;

                            SetDefautlBackgroud(btnLayHDDoi);
                            btnThoatReprint_Click(null, null);
                            btn_ThuNgan_Click(null, null);
                        }
                    }
                    catch
                    {

                    }
                }
                else if (e.Column == XEM)
                {
                    try
                    {
                        String pMaHoaDon = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString();
                        String pNgayTao = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "NGAYTAO").ToString();
                        String pTongTien = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "TONGTIEN").ToString();

                        Frm_XemHangHoa frm = new Frm_XemHangHoa(pMaHoaDon, pNgayTao, pTongTien, pObject);
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    catch
                    {

                    }
                }
            }
            catch
            {
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            idBill = txtMaHoaDon.Text.ToString();

            if (idBill == "")
            {
                MessageBox.Show("Vui lòng nhập mã hóa đơn cần hủy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            String sSQL = "";
            sSQL += "SELECT MA_HOADON FROM HOADON WHERE ISPAYMENT<>2 and  MA_HOADON = " + clsMain.SQLString(idBill) + "\n";

            if (clsMain.ReturnDataTable(sSQL).Rows.Count > 0)
            {
                if (XtraMessageBox.Show("Xác nhận bạn muốn hủy hóa đơn", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                sSQL = "";
                sSQL += "Update HOADON Set " + "\n";
                sSQL += "NHANVIEN_HUYBILL=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                sSQL += "ISPAYMENT=2 " + "\n";
                sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                clsMain.ExecuteSQL(sSQL);
                //Trừ kho
                sSQL = String.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                clsMain.ExecuteSQL(sSQL);
                sSQL = String.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                clsMain.ExecuteSQL(sSQL);
                //Công nợ
                sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                clsMain.ExecuteSQL(sSQL);
                //KHTT
                sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                if (dt1.Rows.Count > 0)
                {
                    if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                    {
                        DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                        if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                        {
                            int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                            decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                            cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                        }
                        else
                        {
                            cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                        }
                    }
                }

                sSQL = "";
                sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (HỦY)' as Ten_HoaDon" + "\n";
                sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                dtReport.Columns.Add("Giam_Gia", typeof(String));

                string TenNV = "";
                if (dtReport.Rows.Count > 0)
                {
                    sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                    DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                    if (dtTenNV.Rows.Count > 0)
                        TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                }

                sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                if (dtTenban.Rows.Count > 0)
                {
                    dtReport.Columns.Add("TEN_BAN", typeof(String));

                    int n = dtReport.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                        dtReport.Rows[i]["TenNV"] = TenNV;
                        if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                            dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                        else
                            dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                    }

                    Frm_Report1 frm = new Frm_Report1();
                    if (cls_KP_RES.Mode == 3)
                        frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                    else if (cls_KP_RES.Mode == 4)
                        frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                    else
                        frm.ReportName = "rpt_inhoadon_Bill_ban";

                    frm.DataSource = dtReport;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    //dt.Rows.Clear();
                    //lbTotal.Text = "0";
                }
                else
                {
                    int n = dtReport.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        dtReport.Rows[i]["TenNV"] = TenNV;
                        if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                            dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                        else
                            dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                    }

                    Frm_Report1 frm = new Frm_Report1();
                    frm.ReportName = "rpt_inhoadon_Bill";
                    frm.DataSource = dtReport;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    //dt.Rows.Clear();
                    //lbTotal.Text = "0";
                }
                txtMaHoaDon.Text = "";

                sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                return;
            }
            else
                MessageBox.Show("Mã hóa đơn không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {

            }
        }

        private String GetNewCodeBillDoi()
        {
            String sMaHoaDon = "";
            String ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDonDoi] ('" + ngay + "')");
            sMaHoaDon = dtMA.Rows[0][0].ToString();
            return sMaHoaDon;
        }

        private void btnLuuHDDoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (gVDSHangMua.RowCount > 0)
                {
                    if (XtraMessageBox.Show("Bạn muốn lưu hóa đơn đợi", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                        String idBill = GetNewCodeBillDoi();
                        String sSQL = "";
                        sSQL += "INSERT INTO HOADON_DOI(MA_HOADON, TONGTIEN, NGAY)" + "\n";
                        sSQL += "VALUES(";
                        sSQL += clsMain.SQLString(idBill) + ",";
                        sSQL += clsMain.SQLString(lb_tongcong.Text.Replace(",", "").Replace(".", "")) + ",";
                        sSQL += "GETDATE()" + ")";
                        clsMain.ExecuteSQL(sSQL);

                        DataTable dt = new DataTable();
                        decimal sl_combo = 0;
                        bool flag_IsCombo = false;
                        int dem = 0;
                        for (int i = 0; i < gVDSHangMua.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["GIABAN"]).ToString());
                            dmHangHoa.THUE = double.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["THUE"]).ToString());
                            dmHangHoa.Thanh_Tien = decimal.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["Thanh_Tien"]).ToString());
                            dmHangHoa.CHIETKHAU = decimal.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["CHIETKHAU"]).ToString());
                            dmHangHoa.IS_COMBO = bool.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["IS_COMBO"]).ToString());

                            sSQL = "";
                            sSQL += "INSERT INTO CT_HOADON_DOI(MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN, THUE,IS_COMBO,GIABAN_THAT,CHIETKHAU,NHANVIEN)" + "\n";
                            sSQL += "Values(";
                            sSQL += clsMain.SQLString(idBill) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                            sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.Thanh_Tien.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                            sSQL += clsMain.SQLString("0") + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }

                        myDTHH.Rows.Clear();
                        lb_tongcong.Text = "0";
                        label_GiamGia.Text = "0";
                        lbdiscount.Text = "0";
                        labelThueVAT.Text = "0";
                        lbTienTruocThue.Text = "0";
                        //labelSoLuong.Text = "0";
                        //labelThanhTien.Text = "0";

                        txt_soluong.Text = "1";
                        txt_soluong.Focus();
                        txt_soluong.SelectAll();
                        txt_Mahang.Focus();
                        SetTextbox(txt_soluong);
                        btn_ThuNgan_Click(null, null);
                    }
                }
                else
                    throw new Exception("Chưa chọn hàng cần mua!!!");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLayHDDoi_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btnLayHDDoi);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                pObject = "3";
                panelControl2.Visible = false;

                String sSQL = "EXEC SP_DANHSACHHOADON_DOI";
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = myDT;

                SetBackgroudOnClick(btnLayHDDoi);
                HUY.Visible = false;
                IN.Visible = false;
                LAYLAI.Visible = true;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btnLayHDDoi")
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btnBaoCaoBanHang_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 103;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnXemTonKho_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 104;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnThemKHTT_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 105;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnPhatThuong_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 106;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnTraCuu_Click(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + "\\KP_KHTT.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("KP_KHTT"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\KP_KHTT.exe");
            }
            else if (File.Exists(Application.StartupPath + "\\CoopMart.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("CoopMart"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\CoopMart.exe");
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Thiếu file chạy . Liên hệ nhà cung cấp phần mềm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnTraHang_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btnTraHang);
                pObject = "";
                pnBanHang.BringToFront();
                pnViewProduct.BringToFront();
                txtMaHoaDonTra.Text = "";

                foreach (SimpleButton btn in pnThuNgan.Controls)
                    btn.Visible = true;

                foreach (SimpleButton btn in pnNghiepvu.Controls)
                    btn.Visible = true;

                exitReprint = false;
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }
                pObject = "3";
                panelControl2.Visible = false;
                SetBackgroudOnClick(btnTraHang);
                pnMaHoaDonTraHang.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btnTraHang")
                        btn.Visible = false;
                }
                exitReprint = true;
                txtMaHoaDonTra.Focus();
            }
        }

        private void btnTra_Click(object sender, EventArgs e)
        {
            TraHang();
        }

        private void txtMaHoaDonTra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                TraHang();
        }

        private void TraHang()
        {
            if (txtMaHoaDonTra.Text.Trim().Replace("*", "") == "")
                return;

            divide_Table1.Rows.Clear();
            divide_Table2.Rows.Clear();

            pnTachban.BringToFront();
            ResizeTraHang();
            String sSQL = "";
            sSQL += "EXEC SP_SelectHoaDonTraHang " + clsMain.SQLString(txtMaHoaDonTra.Text.Replace("*",""));
            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            if (myDT.Rows.Count > 0)
            {
                divide_Table1.Rows.Clear();
                for (int i = 0; i < myDT.Rows.Count; i++)
                {
                    DataRow dr = divide_Table1.NewRow();
                    dr[0] = myDT.Rows[i]["MA_HOADON"].ToString();
                    dr[1] = myDT.Rows[i]["MA_HANGHOA"].ToString();
                    dr[2] = myDT.Rows[i]["TEN_HANGHOA"].ToString();
                    dr[3] = myDT.Rows[i]["SOLUONG"].ToString();
                    dr[4] = Double.Parse(myDT.Rows[i]["GIABAN"].ToString());
                    dr[5] = Double.Parse(myDT.Rows[i]["THANHTIEN"].ToString());
                    dr[6] = Double.Parse(myDT.Rows[i]["THUE"].ToString());
                    dr[7] = Double.Parse(myDT.Rows[i]["CHIETKHAU"].ToString());
                    dr[8] = Boolean.Parse(myDT.Rows[i]["IS_COMBO"].ToString());
                    divide_Table1.Rows.Add(dr);
                }
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

            TotalMoney1();

            
        }

        private void ResizeTraHang()
        {
            int w = (pn_GroupTable.Width - 110) / 2;
            panelControl15.Width = w;
            pn_TableTow.Width = w;
        }

        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            while (gv_divide_Table_1.RowCount > 0)
                ThrowTable1ToTable2();

            TotalMoney2();
        }

        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            ThrowTable1ToTable2();
            TotalMoney2();
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            while (gv_divide_Table_2.RowCount > 0)
                Throw_Table2ToTable1();

            TotalMoney1();
        }

        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            Throw_Table2ToTable1();
            TotalMoney1();
        }

        private String GetNewCodeBill()
        {
            String sMaHoaDon = "";
            String sNgay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable myDT = clsMain.ReturnDataTable("SELECT [dbo].[FC_NewCodeHoaDonTraHang] (" + clsMain.SQLString(sNgay) + ")");
            sMaHoaDon = myDT.Rows[0][0].ToString();
            return sMaHoaDon;
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;

            String sSQL = "";
            List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
            String idBill = GetNewCodeBill();
            sSQL = "";
            sSQL += "INSERT INTO HOADON_TRAHANG(MA_HOADON, MA_HOADON1, MANHANVIEN,TONGTIEN, NGAYTAO, GIOVAO, GIORA, ";
            sSQL += "NHANVIEN_RABILL, ISPAYMENT, MA_CUAHANG, MA_KHO, MA_QUAY) " + "\n";
            sSQL += "VALUES(";
            sSQL += clsMain.SQLString(idBill) + ",";
            sSQL += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["MA_HOADON"]).ToString()) + ",";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
            sSQL += clsMain.SQLString(Total1.ToString()) + ",";
            sSQL += "GETDATE()" + ",";
            sSQL += "GETDATE()" + ",";
            sSQL += "GETDATE()" + ",";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + "," + 1 + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
            sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ")";
            clsMain.ExecuteSQL(sSQL);

            DataTable dt = new DataTable();
            decimal sl_combo = 0;
            bool flag_IsCombo = false;
            int dem = 0;
            decimal per_Discount = 0;
            decimal per_Extra = 0;

            for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
            {
                DM_HangHoa dmHangHoa = new DM_HangHoa();
                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["MA_HANGHOA"]).ToString();
                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["TEN_HANGHOA"]).ToString();
                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["SOLUONG"]).ToString());
                dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["GIABAN"]).ToString());
                dmHangHoa.THUE = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["THUE"]).ToString());
                dmHangHoa.Thanh_Tien = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["THANHTIEN"]).ToString());
                dmHangHoa.CHIETKHAU = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["CHIETKHAU"]).ToString());
                dmHangHoa.IS_COMBO = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["IS_COMBO"]).ToString());

                decimal sChietKhau = dmHangHoa.CHIETKHAU;
                decimal GiaBan_Thuc = dmHangHoa.GIABAN - dmHangHoa.GIABAN * per_Discount / 100 + dmHangHoa.GIABAN * per_Extra / 100;
                GiaBan_Thuc -= GiaBan_Thuc * sChietKhau / 100;
                sSQL = "";
                sSQL += "INSERT INTO CT_HOADON_TRAHANG(MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN, THUE, IS_COMBO, GIABAN_THAT, CHIETKHAU)" + "\n";
                sSQL += "VALUES(";
                sSQL += clsMain.SQLString(idBill) + ",";
                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                sSQL += clsMain.SQLString(GiaBan_Thuc.ToString() == "" ? dmHangHoa.GIABAN.ToString() : GiaBan_Thuc.ToString()) + ",";
                sSQL += clsMain.SQLString(sChietKhau.ToString()) + ")";
                clsMain.ExecuteSQL(sSQL);
            }

            XtraMessageBox.Show("Trả hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnTraHang_Click(null, null);
        }

        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            btnTraHang_Click(null, null);
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                    btn_UpDivide.Enabled = false;
            }
            catch
            {

            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                    btn_DowDivide.Enabled = false;
            }
            catch
            {

            }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                if (e.Column == __STT)
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                if (e.Column == _SSTT)
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void ThrowTable1ToTable2()
        {
            if (gv_divide_Table_1.RowCount <= 0)
                return;

            bool checkProductOnGv = false;
            if (divide_Table2.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table2.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["MA_HANGHOA"]).ToString();
                    if (divide_Table2.Rows[i]["MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table2.Rows[i]["SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table2.NewRow();
                dr[0] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["MA_HOADON"]).ToString();
                dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["MA_HANGHOA"]).ToString();
                dr[2] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["TEN_HANGHOA"]).ToString();
                dr[3] = 1;
                dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["GIABAN"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["THANHTIEN"]).ToString());
                dr[6] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["THUE"]).ToString());
                dr[7] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["CHIETKHAU"]).ToString());
                dr[8] = false;
                divide_Table2.Rows.Add(dr);
            }
            gr_divide_Table_2.DataSource = divide_Table2;
            gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["SOLUONG"]).ToString()) == 1)
            {
                gv_divide_Table_1.DeleteSelectedRows();
            }
            else
            {
                int sl = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["SOLUONG"]).ToString());
                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["SOLUONG"], sl - 1);
            }
        }

        private void Throw_Table2ToTable1()
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            if (divide_Table1.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table1.Rows.Count; i++)
                {
                    String maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["MA_HANGHOA"]).ToString();
                    if (divide_Table1.Rows[i]["MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table1.Rows[i]["SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table1.NewRow();
                dr[0] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["MA_HOADON"]).ToString();
                dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["MA_HANGHOA"]).ToString();
                dr[2] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["TEN_HANGHOA"]).ToString();
                dr[3] = 1;
                dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["GIABAN"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["THANHTIEN"]).ToString());
                dr[6] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["THUE"]).ToString());
                dr[7] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["CHIETKHAU"]).ToString());
                dr[8] = false;
                divide_Table1.Rows.Add(dr);
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["SOLUONG"]).ToString()) == 1)
            {
                gv_divide_Table_2.DeleteSelectedRows();
            }
            else
            {
                int sl = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["SOLUONG"]).ToString());
                gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["SOLUONG"], sl - 1);
            }
        }

        double Total1 = 0;
        private void TotalMoney2()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double Total = 0;
            double TotalCK = 0;
            double TotalVAT = 0;
            double TotalApVAT = 0;
            double Qty = 0;
            try
            {
                for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["GIABAN"]).ToString());
                    double ck = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - ((sl * gia) * ck) / 100;
                    if (kt == true)
                        vat = (double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    Qty += sl;
                    TotalCK += ((sl * gia) * ck) / 100;
                    TotalVAT += vat;
                    TotalApVAT += thanhTien;
                    Total += thanhTien + vat;
                    gv_divide_Table_2.SetRowCellValue(i, gv_divide_Table_2.Columns["THANHTIEN"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {

            }

            Total1 = Total;
        }

        private void TotalMoney1()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double Total = 0;
            double TotalCK = 0;
            double TotalVAT = 0;
            double TotalApVAT = 0;
            double Qty = 0;
            try
            {
                for (int i = 0; i < gv_divide_Table_1.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["GIABAN"]).ToString());
                    double ck = double.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - ((sl * gia) * ck) / 100;
                    if (kt == true)
                        vat = (double.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    Qty += sl;
                    TotalCK += ((sl * gia) * ck) / 100;
                    TotalVAT += vat;
                    TotalApVAT += thanhTien;
                    Total += thanhTien + vat;
                    gv_divide_Table_1.SetRowCellValue(i, gv_divide_Table_1.Columns["THANHTIEN"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnBaoCaoTraHang_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 107;
            frm.ShowDialog();
            frm.Dispose();
        }


#endregion

#region Data

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = gVDSHangMua.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = gVDSHangMua.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gVDSHangMua.FocusedRowHandle == gVDSHangMua.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }

            }
            catch
            {

            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                gVDSHangMua.Focus();
                gVDSHangMua.FocusedRowHandle = gVDSHangMua.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gVDSHangMua.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void gVDSHangMua_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            if (gVDSHangMua.RowCount > 0)
            {
                lb_RowsCount.Text = gVDSHangMua.RowCount.ToString("00");
                lb_STT.Text = (gVDSHangMua.FocusedRowHandle + 1).ToString("00");
            }
        }

        private void gVDSHangMua_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gVDSHangMua.FocusedRowHandle >= 0)
                clsKP_Terminal.ShowCustomerDisplayOneLine(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TEN_HANGHOA"]).ToString() + " : " + (int.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["GIABAN"]).ToString())).ToString("N0") + " VND");
            else
                clsKP_Terminal.ShowCustomerDisplayOneLine("Welcome to " + cls_ConfigCashier.nameShop);

            XuLyGv();
        }

        bool flag_hh = false;
        private void gVDSHangMua_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            sMaMon = gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["MA_HANGHOA"]).ToString();
            txt_soluong.Text = gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["SOLUONG"]).ToString();
            txt_Mahang.Text = gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["MA_VACH"]).ToString();
            txtdiscount.Text = gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["CHIETKHAU"]).ToString();
            XuLyGv();

            pnBanHang.BringToFront();
            pn_banphim_so.BringToFront();
            if (pnWapperDeletePlusBut.Visible == false)
                pnWapperDeletePlusBut.Visible = true;
            if (bool.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
            {
                btnComboOption.Visible = true;
            }
            else
            {
                btnComboOption.Visible = false;
            }
        }

        private void XuLyGv()
        {
            if (gVDSHangMua.RowCount > 8)
                pnUpDow.Visible = true;
            else
                pnUpDow.Visible = false;
        }

        private void btn_Cong_Click(object sender, EventArgs e)
        {
            if (pnWapperDeletePlusBut.Visible == false)
                pnWapperDeletePlusBut.Visible = true;

            try
            {
                SimpleButton btn = (SimpleButton)sender;
                if (!Boolean.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["SOLUONG"]).ToString());
                    gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["SOLUONG"], soluong + 1);
                    TotalMoney();
                }
            }
            catch
            {

            }
        }

        private void btn_Cong_Leave(object sender, EventArgs e)
        {
            if (btn_Tru.Focused || btn_xoa.Focused)
                return;

            pnWapperDeletePlusBut.Visible = false;
        }

        private void btn_Tru_Click(object sender, EventArgs e)
        {
            if (pnWapperDeletePlusBut.Visible == false)
                pnWapperDeletePlusBut.Visible = true;
            try
            {
                SimpleButton btn = (SimpleButton)sender;
                if (!Boolean.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["SOLUONG"]).ToString());
                    if (soluong > 1)
                    {
                        gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["SOLUONG"], soluong - 1);
                        TotalMoney();
                    }
                }
            }
            catch
            {

            }
        }

        private void btn_Tru_Leave(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_Cong.Focused)
                return;
            pnWapperDeletePlusBut.Visible = false;
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (pnWapperDeletePlusBut.Visible == false)
                pnWapperDeletePlusBut.Visible = true;

            if (!bool.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
            {
                DataTable dtCombo = GetProductInCombo(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["MA_HANGHOA"]).ToString());
                if (dtCombo.Rows.Count > 0)
                {
                    int n = dtCombo.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        gVDSHangMua.DeleteRow(gVDSHangMua.FocusedRowHandle + 1);
                    }
                }
                gVDSHangMua.DeleteSelectedRows();
                
                TotalMoney();
                if (gVDSHangMua.RowCount <= 0)
                    pnWapperDeletePlusBut.Visible = false;
            }
        }

        private void btn_xoa_Leave(object sender, EventArgs e)
        {
            if (btn_Tru.Focused || btn_Cong.Focused)
                return;

            pnWapperDeletePlusBut.Visible = false;
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Bạn muốn xóa tất cả các mặt hàng trên lưới không?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                myDTHH.Rows.Clear();
                DataTable s = new DataTable();
                s = myDTHH;
                TotalMoney();
                pnWapperDeletePlusBut.Visible = false;
            }
        }

        private void grCtrlDSHangMua_Leave(object sender, EventArgs e)
        {
            if (btn_Tru.Focused || btn_xoa.Focused || btn_Cong.Focused || grCtrlDSHangMua.Focused || txt_soluong.Focused)
            {
                return;
            }
            pnWapperDeletePlusBut.Visible = false;
        }

#endregion

        private void btnComboOption_Click(object sender, EventArgs e)
        {
            try
            {
                string co_mahanghoa = "";
                string co_tenhanghoa = "";
                string co_soluong = "";
                string co_phuthu = "";

                if (bool.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
                {
                    string maCombo = "";
                    int i = 0;
                    for (i = gVDSHangMua.FocusedRowHandle - 1; i >= 0; i--)
                    {
                        if (!bool.Parse(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["TRANGTHAI"]).ToString()))
                        {
                            maCombo = gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["MA_HANGHOA"]).ToString();
                            break;
                        }
                    }

                    string sSQL = "";
                    sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                    sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                    sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                    sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                    sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["MA_HANGHOA"]).ToString());
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Frm_Combo_Option frm = new Frm_Combo_Option(maCombo, gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, gVDSHangMua.Columns["MA_HANGHOA"]).ToString());
                        frm.ShowDialog();
                        co_mahanghoa = frm.s_mahanghoa;
                        co_tenhanghoa = frm.s_tenhanghoa;
                        co_soluong = (int.Parse(frm.s_soluong) * int.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, "SOLUONG").ToString())).ToString();
                        co_phuthu = frm.s_phuthu;
                        if (co_mahanghoa != "")
                        {
                            float tt = float.Parse(gVDSHangMua.GetRowCellValue(i, "GIABAN").ToString()) + float.Parse(co_phuthu);
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "MA_HANGHOA", co_mahanghoa);
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "TEN_HANGHOA", co_tenhanghoa);
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "SOLUONG", int.Parse(co_soluong));
                            gVDSHangMua.SetRowCellValue(i, "GIABAN", tt);
                            gVDSHangMua.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gVDSHangMua.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                        sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                        sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                        sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                        sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, "MA_HANGHOA").ToString());
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int sl = int.Parse(gVDSHangMua.GetRowCellValue(gVDSHangMua.FocusedRowHandle, "SOLUONG").ToString()) / int.Parse(dt.Rows[0]["SOLUONG"].ToString());
                            float tt = float.Parse(gVDSHangMua.GetRowCellValue(i, "GIABAN").ToString()) - float.Parse(dt.Rows[0]["GIATHEM"].ToString());
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "MA_HANGHOA", dt.Rows[0]["MA"].ToString());
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "TEN_HANGHOA", dt.Rows[0]["TEN"].ToString());
                            gVDSHangMua.SetRowCellValue(gVDSHangMua.FocusedRowHandle, "SOLUONG", sl);
                            gVDSHangMua.SetRowCellValue(i, "GIABAN", tt);
                            gVDSHangMua.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gVDSHangMua.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                        else
                        {
                            XtraMessageBox.Show("Không có hàng hóa thay thế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show("Không có hàng hóa thay thế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("hàng hóa không thuộc combo", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch
            {
            }
        }

        private void btnLaytrongluong_Click(object sender, EventArgs e)
        {
            string sTrongluong = "1";
            try
            {
                string a = clsKP_Terminal.ReadDataCOM1(false);
                a = a.TrimEnd();
                string b = "";
                if (a.Length >= 8)
                {
                    b = a.Substring(a.Length - 8).Replace("kg", "").Trim();
                }
                else
                {
                    b = a.Replace("kg", "").Trim();
                }
                string c = b.Replace("g", "").Trim();
                sTrongluong = c;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            txt_soluong.Text = sTrongluong;
            txt_Mahang.Focus();
        }

        private void btnChonbanggia_Click(object sender, EventArgs e)
        {
            if (gVDSHangMua.RowCount <= 0 || sMaMon == null || sMaMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string Banggia = "0";
            bool thoat = false;
            Frm_Chonbanggia frm = new Frm_Chonbanggia();
            frm.ShowDialog();
            Banggia = frm.banggia;
            thoat = frm.bThoat;
            frm.Dispose();

            if (thoat)
                return;
            string sql = "select * from thietlapbanggia where ma_banggia=" + clsMain.SQLString(Banggia.ToString());
            DataTable dtBanggia = clsMain.ReturnDataTable(sql);
            for (int i = 0; i < gVDSHangMua.RowCount; i++)
            {
                DataRow[] dr = dtBanggia.Select("MA_HANGHOA=" + clsMain.SQLString(gVDSHangMua.GetRowCellValue(i, gVDSHangMua.Columns["MA_HANGHOA"]).ToString()));

                if (dr.Length >0)
                    gVDSHangMua.SetRowCellValue(i, gVDSHangMua.Columns["GIABAN"], dr[0]["GIABAN1"]);
            }

            if (gVDSHangMua.RowCount > 0)
                TotalMoney();
        }

    }
}